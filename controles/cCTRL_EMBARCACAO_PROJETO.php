<?php

class cCTRL_EMBARCACAO_PROJETO {

    public static function Editar() {
        $msg = '';
        $retorno = '';
        $tmpl = new cTEMPLATE_EMBARCACAO_PROJETO_EDIT();
        $tmpl->CarregueDoHTTP();
        $embarcacao = new cEMBARCACAO_PROJETO();
        $embarcacao->mNU_EMPRESA = $tmpl->mCampo['NU_EMPRESA']->mValor;
        $embarcacao->mNU_EMBARCACAO_PROJETO = $tmpl->mCampo['NU_EMBARCACAO_PROJETO']->mValor;
        if (intval($embarcacao->mNU_EMBARCACAO_PROJETO) > 0) {
            $embarcacao->RecupereSe();
        }
        if ($tmpl->mFoiPostado) {
            $tmpl->utf_decodeCampos();
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        $tmpl->AtribuaAoOBJETO($embarcacao);
                        $embarcacao->Salve();
                        if (intval($tmpl->mCampo['NU_EMBARCACAO_PROJETO']->mValor) == 0) {
                            $tmpl->mCampo['NU_EMBARCACAO_PROJETO']->mValor = $embarcacao->mNU_EMBARCACAO_PROJETO;
                        }
                        $msg = "Embarcação/projeto atualizados com sucesso!";
                    } catch (Exception $e) {
                        $msg = htmlentities($e->getMessage());
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {
            $tmpl->CarregueDoOBJETO($embarcacao);
        }
        if (intval($embarcacao->mNU_EMBARCACAO_PROJETO) > 0) {
            $embarcacao->RecupereSe();
            $tmpl->mCampo[cTEMPLATE::CMP_TITULO]->mValor = "Alterar embarcação/projeto";
        } else {
            $tmpl->mCampo[cTEMPLATE::CMP_TITULO]->mValor = "Cadastrar nova embarcação/projeto";
        }
        $tmpl->mCampo[cTEMPLATE::CMP_MSG]->mValor = $msg;
        $conteudo = cINTERFACE::RenderizeTemplateComAcoesNoRodape($tmpl);
        return $conteudo;
    }

    public function EmbarcacoesDaEmpresaDoCandidatoJSON() {
        $id = cHTTP::ParametroValido('id');
        $tokens = explode(":", $id);

        $NU_EMPRESA = $tokens[4];
        $sql = "select NU_EMBARCACAO_PROJETO, NO_EMBARCACAO_PROJETO from EMBARCACAO_PROJETO where NU_EMPRESA=" . $NU_EMPRESA . " and embp_fl_ativo = 1 order by NO_EMBARCACAO_PROJETO";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $lista = array();
        while ($rs = $res->fetch()) {
            $lista[$rs['NU_EMBARCACAO_PROJETO']] = $rs['NO_EMBARCACAO_PROJETO'];
        }
        return json_encode($lista);
    }

    public function daEmpresa(){
        $nu_empresa = $_GET['nu_empresa'];
        $nome = $_GET['nome'];
        $nu_embarcacao_projeto = $_GET['nu_embarcacao_projeto'];

        $rs = cEMBARCACAO_PROJETO::daEmpresa($nu_empresa);
        $data = array (
                    'regs' => $rs
                ,   'nome' => $nome
                ,   'valor' => $nu_embarcacao_projeto
                );
        $combo = cTWIG::Render('intranet/campos/campo_combo.html', $data);

        $x = utf8_encode($combo);

        $ret = array('embproj' => $embproj, 'combo' => $x );

        return json_encode($ret);

    }

}
