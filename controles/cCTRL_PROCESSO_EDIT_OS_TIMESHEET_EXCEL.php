<?php 
class cCTRL_PROCESSO_EDIT_OS_TIMESHEET_EXCEL extends cCTRL_MODELO_TWIG_LIST{
	public function __construct(){
		cHTTP::$MIME = cHTTP::mmXLS;
		$this->nomeTemplate = 'public/timesheet.html';
	}
	
	public function InicializeModelo(){
		$this->modelo = new cdespesa();
	}
	
	public function InicializeTemplate(){
		$this->template = new cTMPL_PROCESSO_EDIT_OS_TIMESHEET_EXCEL();
	}

	
}
