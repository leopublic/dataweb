<?php

class cCTRL_OS_HIST_LIST extends cCTRL_MODELO_LIST {

    public $id_solicita_visto;

    public function InicializeModelo() {
        $this->modelo = new cHISTORICO_OS();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_OS_HIST_LIST();
    }

    public function get_cursorListagem() {
        $id_solicita_visto = $this->id_solicita_visto;
        if ($id_solicita_visto == '') {
            $id_solicita_visto = cHTTP::ParametroValido('id_solicita_visto');
        }
        return $this->modelo->Cursor($id_solicita_visto);
    }

}
