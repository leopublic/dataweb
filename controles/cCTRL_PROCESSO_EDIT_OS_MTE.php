<?php

/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_EDIT_OS_MTE extends cCTRL_PROCESSO_EDIT_OS {

    public function InicializeModelo() {
        $this->modelo = new cprocesso_mte();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_PROCESSO_EDIT_OS_MTE();
        $this->template->set_estiloTituloInterno();
    }

    public function ProcessePost() {
        switch ($this->template->get_acaoPostada()) {
            case 'SALVAR':
            case 'SALVAR_E_FECHAR':
            case 'SALVAR_E_LIBERAR':
            case 'OS_CONCLUIR_PROTOCOLO':
                $this->ProcesseSalvar();
                break;
        }
    }

    public function ProcesseSalvar() {
        $this->template->AtribuaAoOBJETO($this->modelo);
        try {
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $this->template->getValorCampo('id_solicita_visto');
            $os->Recuperar();
            $os->mtppr_id = $this->template->getValorCampo('tppr_id');
            $os->msoli_tx_obs_cobranca = $this->template->getValorCampo('soli_tx_obs_cobranca');

            // Atualiza os processos, só se a OS não estiver encerrada.
            if (!$os->mReadonly){
                $this->modelo->AtualizeProtocoloPelaOs($os->mID_SOLICITA_VISTO);
                $this->modelo->AtualizeAndamentoPelaOs($os->mID_SOLICITA_VISTO);
                $this->modelo->AtualizeConclusaoPelaOs($os->mID_SOLICITA_VISTO);
            }


            if ($this->template->getValorCampo('nu_processo') != '') {
                $os->Protocola(cSESSAO::$mcd_usuario);
            }
            if ($this->template->getValorCampo('id_status_sol') > 0) {
                $os->AtualizarStatus($this->template->getValorCampo('id_status_sol'), cSESSAO::$mcd_usuario);
            }
            $os->AtualizarTipoEnvio($this->template->getValorCampo('id_tipo_envio'), cSESSAO::$mcd_usuario);
            if ($os->mstco_id < 3) {
                $os->AtualizeInfoCobranca();
            }
            cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
            switch ($this->template->get_acaoPostada()) {
                case "SALVAR_E_LIBERAR":
                    $os->LibereParaCobranca(cSESSAO::$mcd_usuario);
                    cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
                    break;
            }
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $this->CarregueTemplatePeloModelo($this->template, $this->modelo);
    }

}
