<?php
class cCTRL_CAND_CLIE_EDIT_VISA extends cCTRL_CAND_CLIE_EDIT{
	public function __construct(){
		parent::__construct();
		$this->nomeTemplate = 'cliente/formulario_vistoatual.html';
		$this->nomeAbaAtiva = 'VISA';
	}
	
	public function InicializeModelo(){
		$this->modelo = new cCANDIDATO();
	}
	
	public function InicializeTemplate(){
		$this->template = new cTMPL_CAND_CLIE_EDIT_VISA();
		parent::InicializeTemplate();
	}

	public function Complementos(){
		$x = parent::Complementos();
		if($this->template->getValorCampo('NU_CANDIDATO') > 0){
			$controle = new cCTRL_PROCESSO();
			$telas = $controle->TemplatesVistoCliente($this->modelo->VistoAtual());
			$x['candidato']['processos'] = $telas; 
		}
		return $x;		
	}

	public function ProcessePost(){
		$this->modelo->Recuperar($this->template->getValorCampo('NU_CANDIDATO'));
		parent::ProcessePost();		
	}

	public function ProcesseSalvar(){
		$this->modelo->AtualizarExterno();
	}
}
