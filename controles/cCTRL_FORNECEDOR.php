<?php

/**
 */
class cCTRL_FORNECEDOR extends cCTRL_MODELO {

    public function getListar() {
        $regs = cfornecedor::cursorTodos();
        return $this->renderListar($regs);
    }

    public function postListar() {
        $filtros = array();
        $regs = cfornecedor::cursorTodos($filtros);
        return $this->renderListar($regs);
    }

    public function renderListar($regs) {
        $menu = cLAYOUT::Menu();
        $data = array(
            'regs' => $regs,
            'menu' => $menu,
        );
        return cTWIG::Render('intranet/cadastros/fornecedor_list.html', $data);
    }

}
