<?php
class cCTRL_CONTROLE{
	public static function ModificacoesRecentes(){
		$ret = '<h4><b>Melhorias/correções disponibilizadas recentemente</h4>' ;
		$sql = "     select *, date_format(logm_data, '%d/%m/%y') logm_data_fmt from log_modificacoes order by logm_data desc";
		$res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo log de modificacoes');
		$ret .= '<table class="log" cellspacing="0" celpadding="0">';
		$ret .= '<colgroup><col width="80px"/><col width="auto"/></colgroup>';
		$ret .= '<tr><td>Em</td><td>Modificação</td></tr>';
		$i = 0;
		while (( $rs = mysql_fetch_array($res)) && $i < 10) {
			$classe = '' ;
			if ($rs['logm_data_fmt'] == date('d/m/y')){
				$classe = ' class="recente" ';
			}
			$ret .= '<tr'.$classe.'><td>'.$rs['logm_data_fmt'].'</td><td><b>'.$rs['logm_titulo'].'</b><br/>'.$rs['logm_descricao'].'</td></tr>';
			$i++;
		}
		$ret .= '<tr><td colspan="2" style="text-align:center;">...</td></tr>';
		$ret .= '</table>';
		return $ret;
	}
}
