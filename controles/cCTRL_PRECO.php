<?php
class cCTRL_PRECO extends cCTRL_MODELO{
	public  function ComplementarLinhaListagem(&$pTmpl) {

	}
	public  function Liste(){
		$tmpl = new cTMPL_PRECO_LISTAR();
		$tmpl->CarregueDoHTTP();
		$tabela = new ctabela_precos();
		$tabela->RecuperePeloId(cHTTP::ParametroValido('tbpc_id'));
		$tmpl->mTitulo = 'Tabela "'. $tabela->mtbpc_tx_nome . '" - Preços por serviço';
		return parent::Liste($tmpl, new cpreco());
	}

	public  function Edite() {
		$pTemplateEditar = new cTMPL_PRECO_EDITAR();
		$pModelo = new cpreco();
		// Carrega o ID informado no get ou o form postado anteriormente, se houver
		$pTemplateEditar->CarregueDoHTTP();
		// Carrega o usuario correspondente aa tela exibida
		$tabelapreco = new ctabela_precos();
		$tabelapreco->RecuperePeloId($pTemplateEditar->mCampo['tbpc_id']->mValor);
		$pTemplateEditar->mTitulo = $tabelapreco->mtbpc_tx_nome.' ('.$tabelapreco->mtbpc_nu_ano.') - Informar pre&ccedil;o';
		if ($pTemplateEditar->mFoiPostado){
			// A tela ja foi apresentada e isso eh um POST
			// Testa acoes
			switch ($pTemplateEditar->mAcaoPostada){
				case 'SALVAR':
					$pTemplateEditar->AtribuaAoOBJETO($pModelo);
					try{
						$pModelo->Salvar();
						cHTTP::$SESSION['msg'] = 'Opera&ccedil;&atilde;o realizada com sucesso!';
						cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
						cHTTP::RedirecionePara('cCTRL_PRECO', 'Liste', 'tbpc_id='.$pModelo->mtbpc_id);
					}
					catch (cERRO_CONSISTENCIA $e) {
						cHTTP::$SESSION['msg'] = $e->getMessage()	;
						cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
						cHTTP::$SESSION['msgTitulo'] = "Aten&ccedil;&atilde;o";
					}
					catch (Exception $e) {
						cHTTP::$SESSION['msg'] = 'N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o pois:<br/>'.htmlentities($e->getMessage());
						cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ERROR;
						}
					break;
			}
		}
		else{
			// Se nao houve o post, entao eh a primeira vez que estah exibindo
			$pTemplateEditar->AtribuaAoOBJETO($pModelo);
			$pModelo->RecuperePeloId();
			if ($pModelo->mprec_id == 0){
				$pModelo->mprec_tx_descricao_tabela_precos = $pModelo->mNO_SERVICO_RESUMIDO;
			}
			$pTemplateEditar->CarregueDoOBJETO($pModelo);
		}
		if ($pTemplateEditar->mlocalAcoes == cTEMPLATE::lca_TOPO){
			$acoes_no_topo = true;
		}
		else{
			$acoes_no_topo = false;
		}
		$conteudo = '';
		$conteudo .= cINTERFACE::RenderizeTitulo($pTemplateEditar, $acoes_no_topo, cINTERFACE::titPAGINA);
		$conteudo .= '<div class="conteudoInterno">';
		$conteudo .= cINTERFACE::formTemplate($pTemplateEditar);
		$conteudo .= cINTERFACE::RenderizeTemplateEdicao($pTemplateEditar);
		$conteudo .= '</table>';
		$conteudo .= $pTemplateEditar->TratamentoPadraoDeMensagens();
		$conteudo .= '</div>';

		$conteudo .= cINTERFACE::RenderizeBarraAcoesRodape($pTemplateEditar->mAcoes);
		return $conteudo;
	}

	public function Exclua()
	{
		try{
			$prec_id = cHTTP::ParametroValido('prec_id');
			$tbpc_id = cHTTP::ParametroValido('tbpc_id');
			$prec = new cpreco();
			$prec->mprec_id = $prec_id;
			$prec->ExcluirPeloId();
			cNOTIFICACAO::singleton('O serviço foi excluído da tabela de preços com sucesso.".', cNOTIFICACAO::TM_SUCCESS);
			cHTTP::RedirecionePara('cCTRL_PREC_LIST', 'Liste');
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton('Não foi possível excluir o serviço:'.$e->getMessage()."<br/>".cAMBIENTE::$multimo_sql, cNOTIFICACAO::TM_ERROR);				
		}			

	}
}
