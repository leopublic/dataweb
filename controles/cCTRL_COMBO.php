<?php
/**
 * Classe que fornece combos para carga via javascript
 */
class cCTRL_COMBO
{
	public function servicosDaEmpresa(){
		$nu_empresa = $_POST['nu_empresa'];
		$nome = $_POST['nome'];
		$exibeDefault = $_POST['exibeDefault'];
		$default = $_POST['default'];
		$readonly = $_POST['readonly'];
		if ($exibeDefault == '0'){
			$exibeDefault = false;
		} else {
			$exibeDefault = true;
		}
		if ($nome == ''){
			$nome = 'nu_servico';
		}

		$data = array(
			'nome' => $nome,
			'valor_atual' => '',
			'valores' => cSERVICO::comboDisponiveisEmpresa($nu_empresa),
			'readonly' => $readonly
			);
		$ret = cTWIG::Render('html/select.twig', $data);
        return $ret;
	}

	public function embarcacoesDaEmpresa(){
		$nu_empresa = $_POST['nu_empresa'];
		$nome = $_POST['nome'];
		$exibeDefault = $_POST['exibeDefault'];
		$default = $_POST['default'];
		$readonly = $_POST['readonly'];
		if ($exibeDefault == '0'){
			$exibeDefault = false;
		} else {
			$exibeDefault = true;
		}
		if ($nome == ''){
			$nome = 'nu_embarcacao_projeto';
		}

		$data = array(
			'nome' => $nome,
			'valor_atual' => '',
			'valores' => cEMBARCACAO_PROJETO::daEmpresa($nu_empresa),
			'readonly' => $readonly
			);
		$ret = cTWIG::Render('html/select.twig', $data);
        return $ret;
	}
}
