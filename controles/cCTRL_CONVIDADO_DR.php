<?php
/**
 * Controller para as funÃ§Ãµes de DR do usuÃ¡rio tipo "convidado"
 */
class cCTRL_CONVIDADO_DR extends cCTRL_MODELO {

    public static $repo;
    public $cand;
    public $conv;

    public function repo() {
        if (!isset(self::$repo)) {
            self::$repo = new cREPOSITORIO_CONVIDADO();
        }
        return self::$repo;
    }

    public function ativo(){
        if ($_SESSION['conv_id'] > 0){
            $rep = $this->repo();
            $conv = new cconvidado();
            $conv->RecuperePeloId($_SESSION['conv_id']);
            cSESSAO::$mcd_empresa = $conv->mnu_empresa;
            return true;
        } else {
            cNOTIFICACAO::singleton('Your access expired.', cNOTIFICACAO::TM_SUCCESS, "Attention");
            $link = '/index.php';
            cHTTP::RedirecionaLinkInterno($link);
            return false;
        }
    }


    public function getLogin(){
        $conv_id = $_GET['conv_id'];
        $chave = $_GET['chave'];
        $redirect = $_GET['redirect'];
        $rep = $this->repo();
        $conv = $rep->login($conv_id, $chave);
        if ($_GET['nu_candidato_tmp'] != ''){
            $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        } else {
            $nu_candidato_tmp = $conv->mnu_candidato_tmp;
        }
        if ($conv){
            $_SESSION['conv_id'] = $conv->conv_id;
            if ($redirect){
                $link = "/paginas/carregueConvidado.php?metodo=".$redirect."&nu_candidato_tmp=".$nu_candidato_tmp;
            } else {
                if ($conv->mnu_candidato_tmp > 0){
                    $link = "/paginas/carregueConvidado.php?metodo=getEditarInfo&nu_candidato_tmp=".$nu_candidato_tmp;
                } else {
                    $link = '/paginas/carregueConvidado.php?metodo=getPreenchimentos';
                }
            }
            cHTTP::RedirecionaLinkInterno($link);
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function getPreenchimentos(){
        $data = $this->dataPadrao();
        try {
            $rep = $this->repo();
            $regs = $rep->preenchimentos($_SESSION['conv_id']);
            foreach($regs as &$reg){
                switch($reg['id_status_edicao']){
                    case '':
                    case '1':
                        $reg['status'] = 'Pending';
                        break;
                    case '2':
                        $reg['status'] = 'Updated, but not sent yet';
                        break;
                    default:
                        $reg['status'] = 'Sent!';
                        break;
                }
            }
            $data['linhas'] = $regs;

        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Atention");
        }
        return cTWIG::Render('convidado/preenchimentos.twig', $data);
    }

    public function dataPadrao(){
        $data = array();
        $data['nu_empresa_prestadora'] = 1;
        return $data;
    }

    public function dataInicial() {
        $data = array();

        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
//        $cand = new cCANDIDATO_TMP;
//        $cand->RecuperarPeloTmp($nu_candidato_tmp);

        $data['pagina']['titulo'] = 'Mundivisas::Dataweb';
        $data['conv'] = $this->conv;
        $data['nu_empresa_prestadora'] = 1;
        $rep = new cREPOSITORIO_CANDIDATO;
        if (isset($this->cand)){
            if ($rep->edicaoHabilitada($this->cand, cSESSAO::$mcd_usuario)) {
                $disabled = '';
                $readonly = '';
            } else {
                $disabled = ' disabled="disabled" ';
                $readonly = true;
            }
            $data['nu_candidato_tmp'] = $nu_candidato_tmp;
            $data['candidato'] = $this->cand;
        }
        $data['disabled'] = $disabled;
        $data['readonly'] = $readonly;
        $data['sexos'] = array(array("valor" => "F", "descricao" => "Feminine"), array("valor" => "M", "descricao" => "Masculine"));
        return $data;
    }

    public function candidatoAutorizado(){
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        $this->cand = new cCANDIDATO_TMP;
        $this->cand->RecuperarPeloTmp($nu_candidato_tmp);
        if ($_SESSION['conv_id'] > 0){
            $this->conv = new cconvidado;
            $this->conv->RecuperePeloId($_SESSION['conv_id']);
            cSESSAO::$mcd_empresa = $this->conv->mnu_empresa;
            if (intval($this->conv->mnu_candidato_tmp) > 0){
                if ($this->conv->mnu_candidato_tmp != $nu_candidato_tmp){
                    cNOTIFICACAO::singleton("You don't have access to this information", cNOTIFICACAO::TM_ERROR);
                    return false;
                } else {
                    return true;
                }
            } else {
                if ($this->cand->mconv_id != $_SESSION['conv_id']){
                    cNOTIFICACAO::singleton("You don't have access to this information", cNOTIFICACAO::TM_ERROR);
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            cNOTIFICACAO::singleton("Your session has expired. Please access the original link again.", cNOTIFICACAO::TM_ERROR);
            return false;
        }
    }

    public function redirecionaPara($metodo, $nu_candidato_tmp = '') {
        $link = '/paginas/carregueConvidado.php?controller=cCTRL_CONVIDADO_DR&metodo=' . $metodo . '&nu_candidato_tmp=' . $nu_candidato_tmp;
        cHTTP::RedirecionaLinkInterno($link);
    }

    public function getEditarInfo() {
        if ($this->candidatoAutorizado()){
            try {
                $data = $this->dataInicial();
                $data['nacionalidades'] = cPAIS_NACIONALIDADE::comboMustacheIngles('', '(select...)');
                $data['paises'] = cPAIS_NACIONALIDADE::comboMustachePaisIngles('', '(select...)');
                $data['sexos'] = array(array("valor" => "F", "descricao" => "Feminine"), array("valor" => "M", "descricao" => "Masculine"));
            } catch (Exception $ex) {
                cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
            }
            $data['ativa'] = 'INFO';
            $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
            return cTWIG::Render('convidado/dr_dados_pessoais.twig', $data);
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function salvarAlteracoes(){
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            $cand = $rep->salvaAlteracoesTmp($_POST['nu_candidato_tmp'],$_POST);
            cNOTIFICACAO::singleton('Changes updated succesfully.', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
        }

    }


    public function postEditarInfo() {
        $this->salvarAlteracoes();
        $this->redirecionaPara('getEditarInfo', $_POST['nu_candidato_tmp']);
    }

    public function getEditarDocumentos() {
        if ($this->candidatoAutorizado()){
            try {
                $data = $this->dataInicial();
                $data['paises'] = cPAIS_NACIONALIDADE::comboMustachePaisIngles('', '(select...)');
            } catch (Exception $ex) {
                cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
            }
            $data['ativa'] = 'DOCS';
            $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
            return cTWIG::Render('convidado/dr_documentos.twig', $data);
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function postEditarDocumentos() {
        $this->salvarAlteracoes();
        $this->redirecionaPara('getEditarDocumentos', $_POST['nu_candidato_tmp']);
    }

    public function getEditarExperiencia() {
        if ($this->candidatoAutorizado()){
            try {
                $data = $this->dataInicial();

                $data['nacionalidades'] = cPAIS_NACIONALIDADE::comboMustacheIngles('', '(select...)');
                $data['paises'] = cPAIS_NACIONALIDADE::comboMustachePaisIngles('', '(select...)');
                $data['locais'] = clocal_projeto::comboMustache('', '(select...)');
                $data['embarcacoes'] = cEMBARCACAO_PROJETO::comboMustache($data['candidato']->mNU_EMPRESA, '', '(select...)');
                $data['experiencias'] = cexperiencia_profissional::cursorDoCandidatoTmp($_GET['nu_candidato_tmp']);

            } catch (Exception $ex) {
                cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
            }
            $data['ativa'] = 'EXPE';
            $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
            return cTWIG::Render('convidado/dr_experiencia.twig', $data);
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function postEditarExperiencia() {
        $this->salvarAlteracoes();
        $this->redirecionaPara('getEditarExperiencia', $_POST['nu_candidato_tmp']);
    }

    public function getEditarArquivos() {
        if ($this->candidatoAutorizado()){
            try {
                $data = $this->dataInicial();
                $data['tipos_arquivo'] = cTIPO_ARQUIVO::comboMustacheIngles('', '(select...)');
                $data['ativa'] = 'FILES';
                $rep = new cREPOSITORIO_DR;
                $data['arquivos'] = $rep->cursorArquivos($data['candidato']);
            } catch (Exception $ex) {
                cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Attention");
            }
            $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
            return cTWIG::Render('convidado/dr_arquivos.twig', $data);
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function postEditarArquivos() {
        try {
            $nu_candidato_tmp = $_POST['nu_candidato_tmp'];
            $rep = new cREPOSITORIO_ARQUIVO;
            $rep->novoArquivoDoCandidatoPeloCandidato('', $_FILES['CMP_arquivo'], $nu_candidato_tmp);
            cNOTIFICACAO::singleton('File sent succesfully.', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Attention");
        }
        $this->redirecionaPara('getEditarArquivos', $_POST['nu_candidato_tmp']);
    }

    public function getEnviar() {
        if ($this->candidatoAutorizado()){
            $data = $this->dataInicial();
            $data['ativa'] = 'ENVIAR';
            return cTWIG::Render('convidado/dr_enviar.twig', $data);
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function postEnviar() {
        try {
            $rep = new cREPOSITORIO_DR;
            $nu_candidato_tmp = $_POST['nu_candidato_tmp'];
            $rep->enviaAlteracoes('', $nu_candidato_tmp);
            cNOTIFICACAO::singleton('Changes updated succesfully.', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Attention");
        }
        $this->redirecionaPara('getEditarInfo', $_POST['nu_candidato_tmp']);
    }

    public function getCopiaDr() {
        if ($this->candidatoAutorizado()){
            $rep = new cREPOSITORIO_ARQUIVO;
            $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
            $arq = $rep->recuperaArquivoTemporarioDoTipo($nu_candidato_tmp, cTIPO_ARQUIVO::COPIA_DR_PREENCHIDO_PELO_CLIENTE);
            if (is_object($arq)){
                $rep->downloadParaCliente($arq->mNU_SEQUENCIAL);
            }
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function getConfirmaEnvio() {
        if ($this->candidatoAutorizado()){
            try {
                $rep = new cREPOSITORIO_DR;
                $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
                $rep->confirmaEnvio($nu_candidato_tmp);
                cNOTIFICACAO::singleton('Confirmation registered. Thank you.', cNOTIFICACAO::TM_SUCCESS, "Attention");
            } catch (Exception $ex) {
                cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Attention");
            }
            $this->redirecionaPara('getEditarInfo', $_GET['nu_candidato_tmp']);
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function getCancelaEnvio() {
        if ($this->candidatoAutorizado()){
            try {
                $rep = new cREPOSITORIO_DR;
                $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
                $rep->cancelaEnvio($nu_candidato_tmp);
                cNOTIFICACAO::singleton('You can now make the desired changes. When finished, click on Send Changes.', cNOTIFICACAO::TM_SUCCESS, "Attention");
            } catch (Exception $ex) {
                cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Attention");
            }
            $this->redirecionaPara('getEditarInfo', $_GET['nu_candidato_tmp']);
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function download() {
        if ($this->candidatoAutorizado()){
            $NU_SEQUENCIAL = $_GET['NU_SEQUENCIAL'];
            $rep = new cREPOSITORIO_ARQUIVO();
            $rep->downloadParaCliente($NU_SEQUENCIAL);
        } else {
            cHTTP::RedirecionaLinkInterno('/');
        }
    }

    public function postAdicionarExperiencia(){
        $rep = new cREPOSITORIO_DR;
        $post = $_POST;
        $exp = $rep->adicionarExperiencia($post['nu_candidato_tmp'], $post['no_companhia'], $post['no_funcao'], $post['dt_inicio'], $post['dt_fim'], $post['tx_atribuicoes']) ;
        $data = array(
            'experiencias' => cexperiencia_profissional::cursorDoCandidatoTmp($post['nu_candidato_tmp']),
        );
        $ret = cTWIG::Render('cliente/candidato/experiencia_list.twig', $data);
        return $ret;
    }

    public function postExcluirExperiencia(){
        $rep = new cREPOSITORIO_DR;
        $post = $_POST;
        $exp = $rep->excluirExperiencia($post['epro_id']);
        $data = array(
            'experiencias' => cexperiencia_profissional::cursorDoCandidatoTmp($post['nu_candidato_tmp']),
        );
        $ret = cTWIG::Render('cliente/candidato/experiencia_list.twig', $data);
        return $ret;
    }


}
