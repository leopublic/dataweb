<?php
class cCTRL_CANDIDATO_STATUS extends cCTRL_MODELO{
	public function Inative(){
		return $this->AltereStatus(cHTTP::ParametroValido('NU_CANDIDATO'), "Inative");
	}

	public function Ative(){
		return $this->AltereStatus(cHTTP::ParametroValido('NU_CANDIDATO'), "Ative");
	}

	public function Revise(){
		return $this->AltereStatus(cHTTP::ParametroValido('NU_CANDIDATO'), "Revise");
	}

	public function Desrevise(){
		return $this->AltereStatus(cHTTP::ParametroValido('NU_CANDIDATO'), "Desrevise");
	}

	public function Pendente(){
		return $this->AltereStatus(cHTTP::ParametroValido('NU_CANDIDATO'), "Pendente");
	}

	public function Exclua(){
		return $this->AltereStatus(cHTTP::ParametroValido('NU_CANDIDATO'), "Exclua");
	}

	public function AltereStatus($pNU_CANDIDATO, $pOperacao){
		$cand = new cCANDIDATO();
		$cand->mNU_CANDIDATO = $pNU_CANDIDATO;

		$ret = new cRET_AJAX_PADRAO();
		try{
			switch($pOperacao){
				case "Exclua":
					$cand->SoliciteExclusao(cSESSAO::$mcd_usuario);
					$msg = 'Candidato marcado para exclusÃ£o com sucesso!';
					break;
				case "Inative":
					$cand->Inative(cSESSAO::$mcd_usuario);
					$msg = 'Candidato inativado com sucesso!';
					break;
				case "Ative":
					$cand->Ative(cSESSAO::$mcd_usuario);
					$msg = 'Candidato ativado com sucesso!';
					break;
				case "Revise":
					$cand->RegistreQueEstaRevisado(cSESSAO::$mcd_usuario);
					$msg = 'Candidato revisado com sucesso!';
					break;
				case "Desrevise":
					$cand->RegistreQueNaoEstaRevisado(cSESSAO::$mcd_usuario);
					$msg = 'Candidato desrevisado com sucesso!';
					break;
				case "Pendente":
					$cand->ColocarRevisaoPendente(cSESSAO::$mcd_usuario);
					$msg = 'Candidato alterado com sucesso!';
					break;
			}
			$ret->FoiSucesso($msg);
		}
		catch (cERRO_CONSISTENCIA $e) {
			$ret->FoiAlerta('NÃ£o foi possÃ­vel realizar a operaÃ§Ã£o pois:<br/>'.$e->getMessage());
		}
		catch (Exception $e) {
			$ret->FoiErro('NÃ£o foi possÃ­vel realizar a operaÃ§Ã£o pois:<br/>'.$e->getMessage());
		}
		return $ret->EmJSON();
	}
}
