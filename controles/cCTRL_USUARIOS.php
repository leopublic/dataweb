<?php

class cCTRL_USUARIOS extends cCTRL_MODELO {

    public function ComplementarLinhaListagem(cTEMPLATE &$pTmpl) {
        
    }

    public function Liste() {
        cHTTP::comCabecalhoFixo();
        $tmpl = new cTMPL_USUARIO_LISTE();
        $tmpl->CarregueDoHTTP();
        $tmpl->AdicionarGridHighlight();
        if (cHTTP::HouvePost()) {
            cHTTP::RedirecionePara('cCTRL_USUARIOS', 'Liste');
        } else {
            $usuario = new cusuarios();
            if ($tmpl->getValorFiltro('flagativo') != '') {
                if ($tmpl->getValorFiltro('flagativo') == 1) {
                    $tmpl->mFiltro['flagativo']->mWhere = " flagativo = 'Y'";
                } else {
                    $tmpl->mFiltro['flagativo']->mWhere = " flagativo = 'N'";
                }
            }
            $cursor = $usuario->Listar($tmpl->mFiltro, $tmpl->get_ordenacao());
            $tmpl->Renderize($cursor);
            $html = $tmpl->outputBuffer;
            $html = cINTERFACE::formTemplate($tmpl) . $html . '</form>';
            $html.= $tmpl->TratamentoPadraoDeMensagens();
            return $html;
        }
    }

    public function desbloquear() {
        $cd_usuario = $_GET['cd_usuario'];
        $sql = "update usuarios set usua_qtd_acesso_negado = 0 where cd_usuario =" . $cd_usuario;
        cAMBIENTE::ExecuteQuery($sql);
        cHTTP::$SESSION['msg'] = 'Operação realizada com sucesso!';
        cHTTP::RedirecionePara("cCTRL_USUARIOS", "Liste", "");
    }

    public function enviarNovaSenha() {
        $cd_usuario = $_GET['cd_usuario'];
        $usuario = new cusuarios;
        $usuario->RecuperePeloId($cd_usuario);
        $usuario->EnvieNovaSenha();
        cHTTP::$SESSION['msg'] = 'Operação realizada com sucesso!';
        cHTTP::RedirecionePara("cCTRL_USUARIOS", "Liste", "");
    }

    public function ListeRevisoes() {
        return parent::Liste(new cTMPL_USUARIO_LISTAR_REVISOES(), new cusuarios(), "CursorRevisoes");
    }

    public function Edite() {
        $tmpl = new cTMPL_USUARIO_EDITE();
        $tmpl->CarregueDoHTTP();
        $cd_usuario = cHTTP::ParametroValido('cd_usuario');
        $usuario = new cusuarios();
        if (cHTTP::HouvePost()) {
            try {
                switch ($tmpl->get_acaoPostada()) {
                    case "SALVAR":
                        // Consiste senha informada
                        if ($tmpl->getValorCampo('nm_senha') != '') {
                            $usuario->NovaSenhaValida($tmpl->getValorCampo('nm_senha'), $tmpl->getValorCampo('nm_senha_rep'));
                        }
                        $tmpl->AtribuaAoOBJETO($usuario);
                        $usuario->Salvar();
                        if ($tmpl->getValorCampo('nm_senha') != '') {
                            $usuario->AltereSenha($tmpl->getValorCampo('nm_senha'));
                        }

                        $cd_perfil = $_POST['CMP_cd_perfil_sel'];
                        $cd_usuario = $usuario->cd_usuario;
                        $perfil = new cusuario_perfil;
                        $perfil->atualizaAcessosDoUsuario($cd_perfil, $cd_usuario);

                        cHTTP::$SESSION['msg'] = 'Operação realizada com sucesso!';
                        cHTTP::RedirecionePara("cCTRL_USUARIOS", "Liste", "");
                        break;
                    default:
                        throw new exception(__CLASS__ . "->" . __FUNCTION__ . ": Operação (" . $tmpl->get_acaoPostada() . ") não esperada.");
                        break;
                }
            } catch (cERRO_CONSISTENCIA $e) {
                cHTTP::$SESSION['msg'] = $e->mMsg;
            } catch (Exception $e) {
                cHTTP::$SESSION['msg'] = $e->getMessage();
            }
        } else {
            $usuario->cd_usuario = $cd_usuario;
            if (intval($usuario->cd_usuario) > 0) {
                $usuario->RecuperePeloId();
            } else {
                $usuario->flagativo = 1;
            }
            $tmpl->CarregueDoOBJETO($usuario);
            $tmpl->setValorCampo('nm_senha', '');
            $tmpl->setValorCampo('nm_senha_rep', '');
        }
        $tmpl->enveloparEmForm = false;
        $tmpl->enveloparEmConteudoInterno = false;
        $tmpl->enveloparEmDiv = false;
        $html = $tmpl->HTML();
        $barra = cINTERFACE::RenderizeBarraAcoesRodape($tmpl->mAcoes);

        $tmpl2 = new cTMPL_USUARIO_PERFIL_LIST();
        $tmpl2->enveloparEmForm = false;
        $tmpl2->enveloparEmConteudoInterno = false;
        $perfil = new cusuario_perfil;
        $cursor = $perfil->acessosDoUsuario($cd_usuario);
        $tmpl2->Renderize($cursor);

        $ret = '<div class="conteudoInterno">' . cINTERFACE::formTemplate($tmpl) . $html;
        $ret.= '<tr class="even">'
                . '<th>Acessos</th>'
                . '<td>' . $tmpl2->outputBuffer . '</td>'
                . '</tr>'
                . '</table>';
        $ret.= $barra . '</form>' . '</div>';
        $ret .= $tmpl->TratamentoPadraoDeMensagens();
        return $ret;
    }

    public function RenoveSenha() {
        $tmpl = new cTMPL_USUARIO_RENOVE_SENHA();
        $tmpl->CarregueDoHTTP();
        $usuario = new cusuarios();
        $usuario->cd_usuario = $tmpl->getValorCampo('cd_usuario');
        $usuario->RecuperePeloId();
        $tmpl->setValorCampo('nome', $usuario->nome);
        if (cHTTP::HouvePost()) {
            try {
                $usuario->AltereSenha($tmpl->getValorCampo('nm_senha'), $tmpl->getValorCampo('nm_senha_rep'));
                cHTTP::$SESSION['msg'] = 'Senha alterada com sucesso!';
                cHTTP::RedirecionePara("cCTRL_USUARIOS", "Liste", "");
            } catch (cERRO_CONSISTENCIA $e) {
                cHTTP::$SESSION['msg'] = $e->mMsg;
            } catch (Exception $e) {
                cHTTP::$SESSION['msg'] = $e->getMessage();
            }
        }
        $html = $tmpl->HTML();
        return $html;
    }

    public function RenoveMinhaSenha() {
        cINTERFACE_AMBIENTE::ControleLinguagem();
        $tmpl = new cTMPL_USUARIO_RENOVE_MINHA_SENHA();
        $tmpl->CarregueDoHTTP();
        $usuario = new cusuarios();
        $usuario->cd_usuario = cSESSAO::$mcd_usuario;
        $usuario->RecuperePeloId();
        $msg = '';
        if (cHTTP::HouvePost()) {
            try {
                if ($tmpl->get_acaoPostada() == 'SALVAR') {
                    if ($tmpl->getValorCampo('nm_senha_atual') == '') {
                        if (cHTTP::getLang() == 'pt_br') {
                            $msg = 'Por favor informe a senha atual.';
                        } else {
                            $msg = 'Please inform your current password.';
                        }
                    } else {
                        if ($usuario->SenhaValida($tmpl->getValorCampo('nm_senha_atual'))) {
                            $usuario->AltereSenha($tmpl->getValorCampo('nm_senha'), $tmpl->getValorCampo('nm_senha_rep'));
                            if (cHTTP::getLang() == 'pt_br') {
                                $msg = 'Senha alterada com sucesso!';
                            } else {
                                $msg = 'Password changed sucessfully!';
                            }
                        } else {
                            if (cHTTP::getLang() == 'pt_br') {
                                $msg = 'Senha atual não confere.';
                            } else {
                                $msg = "Current password don't match. Please verify and try again.";
                            }
                        }
                    }
                }
            } catch (cERRO_CONSISTENCIA $e) {
                $msg = $e->mMsg;
            } catch (Exception $e) {
                $msg = $e->getMessage();
            }
        }
        cHTTP::$SESSION['msg'] = $msg;
        $html = $tmpl->HTML();
        return $html;
    }

    public function criptografe() {
        $sql = "select * from usuarios order by cd_usuario";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            print '<br/>Usuário ' . $rs['cd_usuario'] . ' - ' . $rs['nome'] . ' - senha="' . $rs['nm_senha'];
            if (strlen(trim($rs['nm_senha'])) < 20) {
                print ' ... alterando ...';
                $senha = md5($rs['nm_senha']);
                $sql = "update usuarios set nm_senha = '" . $senha . "' where cd_usuario=" . $rs['cd_usuario'];
                cAMBIENTE::ExecuteQuery($sql);
                print ' OK!!';
            } else {
                print ' ja estava alterada.';
            }
        }
    }

}
