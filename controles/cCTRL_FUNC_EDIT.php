<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_FUNC_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cFUNCAO_CARGO();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_FUNC_EDIT();
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_FUNC_LIST', 'Liste');		
	}

}
