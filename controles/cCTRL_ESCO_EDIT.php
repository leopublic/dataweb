<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_ESCO_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cESCOLARIDADE();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_ESCO_EDIT();
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_ESCO_LIST', 'Liste');		
	}

}
