<?php
class cCTRL_EMPRESA extends cCTRL_MODELO{
	public function ComplementarLinhaListagem(&$pTmpl) {

	}
	public function Liste(){
		return parent::Liste(new cTMPL_EMPRESA_LISTAR(), new cEMPRESA());
	}

	public function Edite() {
		return parent::Edite(new cTMPL_EMPRESA_EDITAR(), new cEMPRESA(), "cCTRL_EMPRESA", "Liste");
	}

	public function Popule(){
		$empOrig = cHTTP::ParametroValido('empOrig');
		$embOrig = cHTTP::ParametroValido('embOrig');
		$empDest = cHTTP::ParametroValido('empDest');
		$embDest = cHTTP::ParametroValido('embDest');
		cREPOSITORIO_EMPRESA::PopularEmpresa($empOrig, $embOrig, $empDest, $embDest);
	}

	public function responsaveis(){
		$nu_empresa = $_GET['nu_empresa'];
		$emp = new cEMPRESA;
		$emp->RecuperePeloId($nu_empresa);
		$ponto_focal = new cusuarios;
		$gestor = new cusuarios;
		if ($emp->musua_id_responsavel > 0){
			$ponto_focal->cd_usuario = $emp->musua_id_responsavel;
			$ponto_focal->RecuperePeloId();
		} else {
			$ponto_focal->nome = '(não informado)';
		}
		if ($emp->musua_id_responsavel2 > 0){
			$gestor->cd_usuario = $emp->musua_id_responsavel2;
			$gestor->RecuperePeloId();
		} else {
			$gestor->nome = '(não informado)';
		}
		$ret = array('pontofocal' => $ponto_focal->nome, 'gestor' => $gestor->nome);
		return json_encode($ret);

	}
}
