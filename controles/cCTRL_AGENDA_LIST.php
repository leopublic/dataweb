<?php
class cCTRL_AGENDA_LIST extends cCTRL_MODELO_TWIG_LIST {

    public function __construct() {
        cHTTP::$MIME = cHTTP::mmXLS;
        $this->nomeTemplate = 'intranet/agenda.html';
    }

    public function InicializeModelo() {
        $this->modelo = new cORDEMSERVICO();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_AGENDA_LIST();
    }

    public function get_cursorListagem(cTEMPLATE_LISTAGEM_TWIG $ptmpl) {
        return $cursor;
    }

    public function Liste() {
        $this->InicializeModelo();
        $this->InicializeTemplate();
        $this->CarregueTemplatePeloHTTP();
        // Tratar post
        $this->ProcessePost();

        $status_solicitacao = \cREPOSITORIO_OS::listaStatusSolicitacao();
        $funcionarios = \cREPOSITORIO_USUARIO::listaFuncionarios();
        
        $filtros = array(
            'sv.id_status_sol' => $_POST['FILTRO_id_status_sol'],
            'sv.cd_tecnico' => $_POST['FILTRO_cd_usuario']
        );
        
        $os = \cREPOSITORIO_OS::cursorAgenda(cSESSAO::$mcd_usuario, $filtros);
        
        $menu = cLAYOUT::Menu();
        $data = array(
                'template' => $this->template
                , 'menu' =>$menu
                , 'os' => $os
                , 'status_solicitacao' =>$status_solicitacao
                , 'funcionarios'  => $funcionarios
                , 'id_status_sol' => $_POST['FILTRO_id_status_sol']
                , 'cd_usuario' => $_POST['FILTRO_cd_usuario']
                );
        
        return cTWIG::Render($this->nomeTemplate, $data);
    }

    public function CarregueTemplatePeloHTTP() {
        $this->template->CarregueDoHTTP();
    }

    public function RenderizaSaida() {
        if (!cHTTP::TemRedirecionamento() && isset($this->data)) {
            $this->data['nu_empresa_prestadora'] = $_SESSION['nu_empresa_prestadora'];
            return cTWIG::Render($this->nomeTemplate, $this->data);
        }
    }

    public function ProcessePost() {
        
    }
    
}
