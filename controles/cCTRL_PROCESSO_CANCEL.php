<?php

/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_CANCEL extends cCTRL_PROCESSO{
	public function FormEdicaoOS(cORDEMSERVICO $pOS) {
		$tela = $this->FormEdicaoPadrao($pOS, 'processo_cancel_edit');		
		$tela->mCampoNome['codigo_processo_mte']->mCombo->mExtra = ' where cd_candidato='.$tela->mCampoNome['cd_candidato']->mValor;
		$tela->mTitulo = $pOS->mNO_SERVICO_RESUMIDO;
		
		return $this->RenderizacaoPadraoEdicao($tela);

	}

	public function log(){
		$codigo = $_GET['codigo'];
		$processo = new cprocesso_cancel();
		$processo->mcodigo = $codigo;
		$processo->RecupereSe();
		if ($processo->mid_solicita_visto != ''){
			$os = new cORDEMSERVICO();
			$os->mID_SOLICITA_VISTO = $processo->mid_solicita_visto;
			$os->Recuperar();
		} else {
			$os = null;
		}
		$candidato = new cCANDIDATO();
		$candidato->Recuperar($processo->mcd_candidato);
		$cursor = cprocesso_cancel::cursorLog($codigo);
		$regs = $cursor->fetchAll();
		$menu = cLAYOUT::Menu();
		$data = array(
				'regs' => $regs,
				'menu' => $menu,
				'processo' => $processo,
				'os' => $os,
				'candidato' => $candidato
		);
		return cTWIG::Render('intranet/processo/cancel_log.html', $data);
	
	}
	
}
