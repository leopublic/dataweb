<?php

abstract class cCTRL_MODELO {

//	const EXTENDE_MODELO = true;
    protected $modelo;

    /**
     * Template utilizado pelo controller.
     * @var cTEMPLATE 
     */
    protected $template;

    public function calculaPaginacao(){
        $qtdReg = 10;
        $pag = $_GET['pag'];
        $inicio = $pag * $qtdReg;
        $qtdPendentes = $repDr->qtdPendentes($where);
        $cadastros_pendentes = $repDr->pendentes($where, $inicio, $qtdReg);

        $paginas = array();
        $qtdPag = ($qtdPendentes / $qtdReg) - 1;
    }
    public static function ExtendeModelo() {
        return true;
    }

    public function InicializeModelo() {
        throw new Exception("Modelo não inicializado");
    }

    public function InicializeTemplate() {
        throw new Exception("Template não inicializado");
    }
    /**
     * Recupera o valor do campo do input se houve post, caso contrário retorna o valor da SESSION
     * @param  [type] $nomeCampo [description]
     * @return [type]            [description]
     */
    public function valorInputSalvo($nomeCampo){
        if (array_key_exists('foipostado', $_POST)){
            $ret = $_POST[$nomeCampo];
            $_SESSION[$nomeCampo] = $_POST[$nomeCampo];
        } elseif (array_key_exists($nomeCampo, $_SESSION)){
            $ret = $_SESSION[$nomeCampo];
        } else {
            $ret = '';
        }
        return $ret;
    }

    public function destroiInpuSalvo($nomeCampo){
        unset($_SESSION[$nomeCampo]);
    }
    public function Edite(cTEMPLATE $pTemplateEditar, cMODELO $pModelo, $pSucessoController = '', $pSucessoMetodo = '', $pParametrosAdicionais = '', $pExtensaoPropriedades = 'm') {
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        if ($pExtensaoPropriedades == ' ') {
            $pExtensaoPropriedades = '';
        }
        $pTemplateEditar->CarregueDoHTTP();
        // Carrega o usuario correspondente aa tela exibida
        if ($pTemplateEditar->mFoiPostado) {
            // A tela ja foi apresentada e isso eh um POST
            // Testa acoes
            switch ($pTemplateEditar->mAcaoPostada) {
                case 'SALVAR':
                    $pTemplateEditar->AtribuaAoOBJETO($pModelo, $pExtensaoPropriedades);
                    try {
                        $pModelo->Salvar();
                        cHTTP::$SESSION['msg'] = 'Opera&ccedil;&atilde;o realizada com sucesso!';
                        cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_SUCCESS;
                        $pParametrosAdicionais .= '&' . $pModelo->campoid() . '=' . $pModelo->getid();
                        if ($pSucessoController != '' && $pSucessoMetodo != '') {
                            cHTTP::RedirecionePara($pSucessoController, $pSucessoMetodo, $pParametrosAdicionais);
                        }
                    } catch (cERRO_CONSISTENCIA $e) {
                        cHTTP::$SESSION['msg'] = $e->getMessage();
                        cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
                    } catch (Exception $e) {
                        cHTTP::$SESSION['msg'] = 'N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o pois:<br/>' . htmlentities($e->getMessage());
                        cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ERROR;
                    }
                    break;
            }
        } else {
            // Se nao houve o post, entao eh a primeira vez que estah exibindo
            $pTemplateEditar->AtribuaAoOBJETO($pModelo, $pExtensaoPropriedades);
            if (intval($pModelo->getid()) > 0) {
                $pModelo->RecuperePeloId();
                $pTemplateEditar->CarregueDoOBJETO($pModelo);
            }
        }
        if ($pTemplateEditar->mlocalAcoes == cTEMPLATE::lca_TOPO) {
            $acoes_no_topo = true;
        } else {
            $acoes_no_topo = false;
        }
        $conteudo = '';
        $conteudo .= cINTERFACE::formTemplate($pTemplateEditar);
        $conteudo .= cINTERFACE::RenderizeTitulo($pTemplateEditar, $acoes_no_topo, cINTERFACE::titPAGINA);
        $conteudo .= '<div class="conteudoInterno">';
        $conteudo .= cINTERFACE::RenderizeTemplateEdicao($pTemplateEditar);
        $conteudo .= '</table>';
        $conteudo .= '</div>';

        $conteudo .= cINTERFACE::RenderizeBarraAcoesRodape($pTemplateEditar->mAcoes);
        $conteudo .= "</form>";
        return $this->EnveloparParaTela($conteudo, $pTemplateEditar);
    }

    /**
     *
     * @param cTEMPLATE $pTemplateListar
     * @param cMODELO $pModelo
     * @param string $pFonteDados
     * @param string $pCustomizarLinha
     * @return type
     */
    public function Liste($pTemplateListar, $pModelo, $pFonteDados = "Listar", $pCustomizarLinha = "ComplementarLinhaListagem") {
        $ret = '';
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $pTemplateListar->CarregueDoHTTP();
        $pTemplateListar->AtribuaAoOBJETO($pModelo);

        // Configura tela de acordo com as caracteristicas da instancia recebido.
        $ret .= cINTERFACE::RenderizeTitulo($pTemplateListar, true, $pTemplateListar->mEstiloTitulo);
        $ret .= '<div class="conteudoInterno">';
        $ret .= cINTERFACE::RenderizeListagem_Filtros($pTemplateListar);
        $qtdLinhas = 0;
        $conteudo = $this->GereLoopListagem($pTemplateListar, $pModelo, $pFonteDados, $pCustomizarLinha, $qtdLinhas);
        $ret .= $conteudo;
        $pTemplateListar->mCampo[cTEMPLATE::CMP_TOTAL_LINHAS]->mValor = $qtdLinhas;
        $ret .= '</div>';
        $ret = cINTERFACE::formTemplate($pTemplateListar) . $ret . "</form>";
        return $this->EnveloparParaTela($ret, $pTemplateListar);
    }

    public function GereLoopListagem($pTemplateListar, $pModelo, $pFonteDados = "Listar", $pCustomizarLinha = "ComplementarLinhaListagem", &$pIndLinha = 0) {
        $conteudo = '';
        $cursor = $pModelo->$pFonteDados($pTemplateListar->mFiltro, $pTemplateListar->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor);
        $conteudo.= cINTERFACE::RenderizeListagem_Cabecalho($pTemplateListar);
        $pIndLinha = 0;
        while ($rs = mysql_fetch_array($cursor)) {
            $pIndLinha++;
            $pTemplateListar->CarregueDoRecordset($rs, true);
            // Fazer configuracoes quando necessario
            $this->$pCustomizarLinha($pTemplateListar);

            $conteudo .= cINTERFACE::RenderizeListagem_Linha($pTemplateListar, "", $pIndLinha);
        }
        $conteudo .= cINTERFACE::RenderizeListagem_Rodape($pTemplateListar);
        $conteudo .= cINTERFACE::RenderizeListagem_RodapePagina($pTemplateListar);
        return $conteudo;
    }

    /**
     *
     * @param cTEMPLATE $pTemplateListar
     * @param sting $pAbaAtiva Nome da aba que estÃ¡ aberta.
     * @return type
     */
    public function MonteEmAbas(cTEMPLATE $pTemplateAbas, $pNomeAbaAtiva, $pHtmlAbaAtiva) {
        $ret = '';
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $pTemplateAbas->CarregueDoHTTP();
        // Adiciona a chave se houver como parametro nas abas
        $ret .= cINTERFACE::RenderizeTitulo($pTemplateAbas);
        $ret .= $pTemplateAbas->RepliqueChavesParaAbasAjax();
        $ret .= '<div id="tabs">';
        $ret .= cINTERFACE::RenderizeIndiceDeAbas($pTemplateAbas, $pNomeAbaAtiva);
        $ret .= '<div id="abaAtiva">';
        $ret .= $pHtmlAbaAtiva;
        $ret .= '</div>';
        $ret .= '</div>';
        //$ret = cINTERFACE::RenderizeForm($pTemplateAbas, $ret);
        $ret .= $pTemplateAbas->TratamentoPadraoDeMensagens();
        return $ret;
    }

    public function Grid(cTEMPLATE $pTemplateListar, cMODELO $pModelo, $pMetodoCarga = "Listar", $pMetodoCustomizacao = "ComplementarLinhaListagem", $pMetodoAtualizacao = 'Salvar') {
        $ret = '';
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $pTemplateListar->CarregueDoHTTP();
        // Configura tela de acordo com as caracteristicas do usuario recebido.
        if ($pTemplateListar->mFoiPostado) {
            self::ProcessarPostGrid($pTemplateListar, $pModelo, $pMetodoAtualizacao);
            cHTTP::setMsg('Atualiza&ccedil;&atilde;o realizada com sucesso');
        }
        $cursor = $pModelo->$pMetodoCarga($pTemplateListar->mFiltro, $pTemplateListar->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor);
        $ret .= cINTERFACE::RenderizeTitulo($pTemplateListar);
        $ret .= cINTERFACE::RenderizeListagem_Filtros($pTemplateListar);
        $conteudo = '';
        if (is_object($cursor)) {
            if ($cursor->rowCount() > 0) {
                $conteudo.= cINTERFACE::RenderizeListagem_Cabecalho($pTemplateListar);
                $i = 0;
                while ($rs = $cursor->fetch(PDO::FETCH_BOTH)) {
                    $pTemplateListar->CarregueDoRecordset($rs);
                    // Fazer configuraï¿½ï¿½es quando necessï¿½rio
                    $this->$pMetodoCustomizacao($pTemplateListar);
                    $conteudo .= cINTERFACE::RenderizeListagem_Linha($pTemplateListar, $i);
                    $i++;
                }
                $pTemplateListar->mCampo[cTEMPLATE::CMP_QTDLINHAS]->mValor = $i;
                $conteudo .= cINTERFACE::RenderizeListagem_Rodape($pTemplateListar);
            }
        }
        $conteudo .= cINTERFACE::RenderizeListagem_RodapePagina($pTemplateListar);
        return $ret . cINTERFACE::ConteudoInterno($conteudo);
    }

    public function ProcessarPostGrid(cTEMPLATE $pTemplate, cMODELO $pModelo, $pMetodoAtualizacao) {
        $qtdLinhas = $pTemplate->qtdLinhas();
        $classeModelo = get_class($pModelo);
        for ($index = 0; $index < $qtdLinhas; $index++) {
            $pModelo = new $classeModelo;
            $pTemplate->AtribuaDoArrayAoOBJETO($pModelo, $index);
            $pModelo->$pMetodoAtualizacao();
        }
    }

    public function InicializarTemplateListagem(&$pTmpl) {
        
    }

    public function Exclua(cTEMPLATE $pTemplateEditar, cMODELO $pModelo, $pSucessoController = '', $pSucessoMetodo = '', $pParametrosAdicionais = '') {
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $pTemplateEditar->CarregueDoHTTP();
        // Carrega o usuario correspondente aa tela exibida
        if ($pTemplateEditar->mFoiPostado) {
            // A tela ja foi apresentada e isso eh um POST
            // Testa acoes
            switch ($pTemplateEditar->mAcaoPostada) {
                case 'SIM':
                    $pTemplateEditar->AtribuaAoOBJETO($pModelo);
                    try {
                        $pModelo->Exclua();
                        cHTTP::$SESSION['msg'] = 'Opera&ccedil;&atilde;o realizada com sucesso!';
                        cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_SUCESS;
                        $pParametrosAdicionais .= '&' . $pModelo->campoid() . '=' . $pModelo->getid();
                        if ($pSucessoController != '' && $pSucessoMetodo != '') {
                            cHTTP::RedirecionePara($pSucessoController, $pSucessoMetodo, $pParametrosAdicionais);
                        }
                    } catch (cERRO_CONSISTENCIA $e) {
                        $pModelo->RecuperePeloId();
                        $pTemplateEditar->CarregueDoOBJETO($pModelo);
                        cHTTP::$SESSION['msg'] = $e->getMessage();
                        cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
                    } catch (Exception $e) {
                        $pModelo->RecuperePeloId();
                        $pTemplateEditar->CarregueDoOBJETO($pModelo);
                        cHTTP::$SESSION['msg'] = 'N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o pois:<br/>' . htmlentities($e->getMessage());
                        cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ERROR;
                    }
                    break;
                case 'NAO':
                    cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_SUCESS;
                    cHTTP::$SESSION['msg'] = 'Operação cancelada!';
                    if ($pSucessoController != '' && $pSucessoMetodo != '') {
                        cHTTP::RedirecionePara($pSucessoController, $pSucessoMetodo, $pParametrosAdicionais);
                    }
                    break;
            }
        } else {
            // Se nao houve o post, entao eh a primeira vez que estah exibindo
            $pTemplateEditar->AtribuaAoOBJETO($pModelo);
            if (intval($pModelo->getid()) > 0) {
                $pModelo->RecuperePeloId();
                $pTemplateEditar->CarregueDoOBJETO($pModelo);
            }
        }
        if ($pTemplateEditar->mlocalAcoes == cTEMPLATE::lca_TOPO) {
            $acoes_no_topo = true;
        } else {
            $acoes_no_topo = false;
        }
        $conteudo = '';
        $conteudo .= cINTERFACE::RenderizeTitulo($pTemplateEditar, $acoes_no_topo, cINTERFACE::titPAGINA);
        $conteudo .= '<div class="conteudoInterno">';
        $conteudo .= cINTERFACE::formTemplate($pTemplateEditar);
        if ($pTemplateEditar->mMsgInicial != '') {
            $conteudo .= '<div class="msgInicial">' . $pTemplateEditar->mMsgInicial . '</div>';
        }
        $conteudo .= cINTERFACE::RenderizeTemplateEdicao($pTemplateEditar);
        $conteudo .= '</table>';
        $conteudo .= '</div>';

        $conteudo .= cINTERFACE::RenderizeBarraAcoesRodape($pTemplateEditar->mAcoes);
        return $this->EnveloparParaTela($pConteudo, $pTemplateEditar);
    }

    public function EnveloparParaTela($pConteudo, cTEMPLATE $pTemplate) {
        $ret = '<div id="' . $pTemplate->mnome . '">';
        $ret.= $pConteudo;
        $ret.= $pTemplate->TratamentoPadraoDeMensagens();
        $ret.= '</div>';
        cHTTP::AdicionarScript('ExibirMsg("#' . $pTemplate->mnome . ' #dialog", "' . cHTTP::TituloMsg() . '" );');
        return $ret;
    }

}
