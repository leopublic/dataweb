<?php

class cCTRL_CAND_CLIE_EDIT_FILE extends cCTRL_CAND_CLIE_EDIT {

    public function __construct() {
        parent::__construct();
        $this->nomeTemplate = 'cliente/arquivos_cand.html';
        $this->nomeAbaAtiva = 'FILE';
    }

    public function InicializeModelo() {
        $this->modelo = new cCANDIDATO();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_CAND_CLIE_EDIT_FILE();
        parent::InicializeTemplate();
    }

    public function ProcessePost() {
        try {
            $rep = new cREPOSITORIO_ARQUIVO;
            $rep->novoArquivoDoCandidatoPeloOperacional($this->template->getValorCampo('NU_CANDIDATO'), $_FILES['CMP_arquivo'], $this->template->getValorCampo('ID_TIPO_ARQUIVO'));
            cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_ALERT);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
    }

}
