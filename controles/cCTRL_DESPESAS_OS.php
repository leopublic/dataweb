<?php

/**
 */
class cCTRL_DESPESAS_OS extends cCTRL_MODELO {

    public function getListar() {
        $regs = cdespesa::cursorDaOS($_GET['id_solicita_visto']);
        return $this->renderListar($regs);
    }

    public function postListar() {
        $filtros = array();
        $regs = cfornecedor::cursorTodos($filtros);
        return $this->renderListar($regs);
    }

    public function renderListar($regs) {
        $id_solicita_visto = $_GET['id_solicita_visto'];
        $menu = cLAYOUT::Menu();
        $abas = cABAS::novaAbaOS($id_solicita_visto);
        $os = new cORDEMSERVICO();
        $os->Recuperar($id_solicita_visto);
        $os->RecuperarCandidatos();
        $os->RecuperaAtributosDoServico();
        $tipos = ctipo_despesa::cursorComboJson();
        $candidatos =  $os->cursorCandidatosComboJson();
        array_unshift($candidatos, array('chave' => '0', 'descricao'=> '(não informado)'));
        $abas->configuraPelaOs($os);
        $abas->abas['Despesas']->ativa = true;
        $data = array(
            'regs' => $regs,
            'menu' => $menu,
            'abas' => $abas,
            'subtitulo' => ' - Despesas',
            'os' => $os, 
            'tipos' => $tipos,
            'candidatos' => $candidatos
        );
        return cTWIG::Render('intranet/os/despesas_list.html', $data);
    }

    public function adicionar(){
        $id_solicita_visto = $_GET['id_solicita_visto'];
        cdespesa::novaDaOs($id_solicita_visto);
        header("Location: /paginas/carregue.php?controller=cCTRL_DESPESAS_OS&metodo=getListar&ID_SOLICITA_VISTO=".$id_solicita_visto."&id_solicita_visto=".$id_solicita_visto);
    }

    public function excluir(){
        $id_solicita_visto = $_GET['id_solicita_visto'];
        $desp = new cdespesa();
        $desp->mdesp_id = $_GET['desp_id'];
        $desp->Excluir();
        header("Location: /paginas/carregue.php?controller=cCTRL_DESPESAS_OS&metodo=getListar&ID_SOLICITA_VISTO=".$id_solicita_visto."&id_solicita_visto=".$id_solicita_visto);
        
    }
    
    }
