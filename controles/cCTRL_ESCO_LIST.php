<?php

class cCTRL_ESCO_LIST extends cCTRL_MODELO_LIST {

    public function InicializeModelo() {
        $this->modelo = new cESCOLARIDADE();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_ESCO_LIST();
    }

}
