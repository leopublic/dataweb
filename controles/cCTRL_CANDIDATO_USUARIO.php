<?php

class cCTRL_CANDIDATO_USUARIO extends cCTRL_MODELO {
    public function dataBasico($dr, $abaAtiva){
        $data = array();
        $data['ativa'] = $abaAtiva;
        $data['pagina']['titulo'] = 'Mundivisas::Dataweb';
        $menu = cmenu::NovoMenuCandidato();
        $menuDir = cmenu::NovoMenuCandidatoSystem();
        $data['pagina']['menu']['esquerdo'] = $menu->JSON();
        $data['pagina']['menu']['direito'] = $menuDir->JSON();
        $data['nu_empresa_prestadora'] = 1;
        if ($rep->edicaoHabilitada(cSESSAO::$candidato)) {
            $disabled = '';
            $readonly = '';
        } else {
            $disabled = ' disabled="disabled" ';
            $readonly = true;
        }
        $data['disabled'] = $disabled;
        $data['readonly'] = $readonly;
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        return $data;
    }

    public function redirecionaPara($metodo) {
        $link = '/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO_USUARIO&metodo=' . $metodo;
        cHTTP::RedirecionaLinkInterno($link);
    }

    public function editarInfo() {
        $data = $this->dataBasico(cSESSAO::$candidato, 'INFO');

        $rep = new cREPOSITORIO_CANDIDATO;
        $data['candidato'] = $rep->candidatoProprio(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp);

        $data['nacionalidades'] = cPAIS_NACIONALIDADE::comboMustacheIngles('', '(select...)');
        $data['paises'] = cPAIS_NACIONALIDADE::comboMustachePaisIngles('', '(select...)');
        $data['sexos'] = array(array("valor" => "F", "descricao" => "Feminine"), array("valor" => "M", "descricao" => "Masculine"));

        return cTWIG::Render('cliente/candidato/dados_pessoais.twig', $data);
    }

    public function postEditarInfo() {
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            $rep->salvaAlteracoesCliente(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp, $_POST);
            $_SESSION['msg'] = 'Changes saved!';
            cNOTIFICACAO::singleton('Changes updated succesfully.', cNOTIFICACAO::TM_SUCCESS, "Atention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Atention");
        }
        $this->redirecionaPara('editarInfo');
    }

    public function getEditarDocumentos() {
        $data = $this->dataBasico(cSESSAO::$candidato, 'DOCS');

        $data['paises'] = cPAIS_NACIONALIDADE::comboMustachePaisIngles('', '(select...)');

        $rep = new cREPOSITORIO_CANDIDATO;
        $data['candidato'] = $rep->candidatoProprio(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp);
        return cTWIG::Render('cliente/candidato/documentos.twig', $data);
    }

    public function postEditarDocumentos() {
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            $rep->salvaAlteracoesCliente(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp, $_POST);
            $_SESSION['msg'] = 'Changes saved!';
            cNOTIFICACAO::singleton('Changes updated succesfully.', cNOTIFICACAO::TM_SUCCESS, "Atention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Atention");
        }
        $this->redirecionaPara('getEditarDocumentos');
    }

    public function getEditarExperiencia() {
        $data = $this->dataBasico(cSESSAO::$candidato, 'EXPE');
        $rep = new cREPOSITORIO_CANDIDATO;
        $data['candidato'] = $rep->candidatoProprio(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp);

        $data['nacionalidades'] = cPAIS_NACIONALIDADE::comboMustacheIngles('', '(select...)');
        $data['paises'] = cPAIS_NACIONALIDADE::comboMustachePaisIngles('', '(select...)');
        $data['locais'] = clocal_projeto::comboMustache('', '(select...)');
        return cTWIG::Render('cliente/candidato/experiencia.twig', $data);
    }

    public function postEditarExperiencia() {
        $rep = new cREPOSITORIO_CANDIDATO;

        try {
            $rep->salvaAlteracoesCliente(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp, $_POST);
            $_SESSION['msg'] = 'Changes saved!';
            cNOTIFICACAO::singleton('Changes updated succesfully.', cNOTIFICACAO::TM_SUCCESS, "Atention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Atention");
        }
        $this->redirecionaPara('getEditarExperiencia');
    }

    public function getEditarArquivos() {
        $data = $this->dataBasico(cSESSAO::$candidato, 'FILES');
        $rep = new cREPOSITORIO_CANDIDATO;
        $data['tipos_arquivo'] = cTIPO_ARQUIVO::comboMustacheIngles('', '(select...)');
        $data['candidato'] = $rep->candidatoProprio(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp);
        $data['arquivos'] = $res->fetchAll(PDO::FETCH_ASSOC);
        return cTWIG::Render('cliente/candidato/arquivos.twig', $data);
    }

    public function postEditarArquivos() {
        try {
            $rep = new cREPOSITORIO_ARQUIVO;
            $rep->novoArquivoDoCandidatoPeloCandidato(cSESSAO::$mnu_candidato, $_FILES['CMP_arquivo']);
            cNOTIFICACAO::singleton('File sent succesfully.', cNOTIFICACAO::TM_SUCCESS, "Atention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Atention");
        }
        $this->redirecionaPara('getEditarArquivos');
    }

    public function visaDetails() {
        $data = $this->dataBasico(cSESSAO::$candidato, 'VISA');

        $rep = new cREPOSITORIO_CANDIDATO;
        $processos = cSESSAO::$candidato->VistoAtual();
        $data['nacionalidades'] = cPAIS_NACIONALIDADE::comboMustacheIngles('', '(select...)');
        $data['candidato'] = $rep->candidatoProprio(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp);
        $data['autorizacao'] = $processos['autorizacao'];
        $data['processos'] = $processos['processos'];
        return cTWIG::Render('cliente/candidato/visa_details.twig', $data);
    }

    public function getEnviar() {
        $data = $this->dataBasico(cSESSAO::$candidato, 'ENVIAR');
        $rep = new cREPOSITORIO_CANDIDATO;
        $data['candidato'] = $rep->candidatoProprio(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp);
        return cTWIG::Render('cliente/candidato/enviar.twig', $data);
    }

    public function postEnviar() {
        try {
            $rep = new cREPOSITORIO_DR;
            $rep->enviaAlteracoes(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp);
            $_SESSION['msg'] = 'Changes sent!';
            cNOTIFICACAO::singleton('Changes sent succesfully.', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Attention");
        }
        $this->redirecionaPara('editarInfo');
    }

    public function downloadParaCliente($nu_sequencial) {
        $arq = new cARQUIVO;
        $arq->RecuperePeloId();
        $rep = new cREPOSITORIO_CANDIDATO;
        $cand = $rep->candidatoProprio(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp);

        if ($cand->mNU_EMPRESA == cSESSAO::$mcd_empresa) {
            
        } else {
            
        }
    }

    public function postAdicionarExperiencia(){
        $rep = new cREPOSITORIO_DR;
        $post = $_POST;
        $exp = $rep->adicionarExperiencia(cSESSAO::$mnu_candidato_tmp, $post['no_companhia'], $post['no_funcao'], $post['dt_inicio'], $post['dt_fim'], $post['tx_atribuicoes']) ;
        $data = array(
            'experiencias' => cexperiencia_profissional::cursorDoCandidatoTmp(cSESSAO::$mnu_candidato_tmp),
            );
        $ret = cTWIG::Render('cliente/candidato/experiencia_list.twig', $data);
        return $ret;
    }

    public function postExcluirExperiencia(){
        $rep = new cREPOSITORIO_DR;
        $post = $_POST;
        $exp = $rep->excluirExperiencia($post['epro_id']);
        $data = array(
            'experiencias' => cexperiencia_profissional::cursorDoCandidatoTmp(cSESSAO::$mnu_candidato_tmp),
        );
        $ret = cTWIG::Render('cliente/candidato/experiencia_list.twig', $data);
        return $ret;
    }

    public function copiaDr() {
        $rep = new cREPOSITORIO_CANDIDATO;
        $cand = $rep->candidatoProprio(cSESSAO::$mnu_candidato, cSESSAO::$mnu_candidato_tmp);
        $data['candidato'] = $cand;
        $nacionalidade = new cPAIS_NACIONALIDADE;
        if ($cand->mCO_NACIONALIDADE > 0) {
            $nacionalidade->mCO_PAIS = $cand->mCO_NACIONALIDADE;
            $nacionalidade->RecuperePeloId();
        }
        $data['nacionalidade'] = $nacionalidade;
        $html = cTWIG::Render('cliente/candidato/copia_dr.twig', $data);
        
        $mpdf = new mPDF();
        $mpdf->img_dpi = 200;
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

}
