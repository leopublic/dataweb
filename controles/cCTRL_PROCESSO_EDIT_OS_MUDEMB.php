<?php

class cCTRL_PROCESSO_EDIT_OS_MUDEMB extends cCTRL_PROCESSO_EDIT_OS {

    public function InicializeModelo() {
        $this->modelo = new cprocesso_mudanca_embarcacao();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_PROCESSO_EDIT_OS_MUDEMB();
        $this->template->set_estiloTituloInterno();
    }

    public function CarregueTemplatePeloModelo(cTEMPLATE_EDICAO &$ptmpl, &$pmodelo) {
        // Recupera a chave
        $ptmpl->AtribuaAoOBJETO($pmodelo, "");

        $this->modelo->id_solicita_visto = $ptmpl->getValorCampo('id_solicita_visto');

        $this->modelo->RecupereSePelaOsSemCand($ptmpl->getValorCampo('id_solicita_visto'));
        // Carrega os campos
        $ptmpl->setValorCampo('codigo', $this->modelo->codigo);
        $ptmpl->setValorCampo('dt_requerimento', $this->modelo->get_dt_requerimento());
        $ptmpl->setValorCampo('dt_envio_bsb', $this->modelo->get_dt_envio_bsb());
        $ptmpl->setValorCampo('nu_empresa', $this->modelo->nu_empresa);
        $ptmpl->setValorCampo('nu_embarcacao_projeto_anterior', $this->modelo->get_nu_embarcacao_projeto_anterior());
        $ptmpl->setValorCampo('nu_embarcacao_projeto_novo', $this->modelo->get_nu_embarcacao_projeto_novo());
        $ptmpl->setValorCampo('nu_servico', $this->modelo->nu_servico);
        $ptmpl->setValorCampo('observacao', $this->modelo->observacao);
        //
        $os = new cORDEMSERVICO();
        $os->mID_SOLICITA_VISTO = $ptmpl->getValorCampo('id_solicita_visto');
        $os->Recuperar();
        if ($os->mID_STATUS_SOL == 6){
            $ptmpl->ProtejaCampo('dt_requerimento');
            $ptmpl->ProtejaCampo('dt_envio_bsb');
            $ptmpl->ProtejaCampo('dt_requerimento');
            $ptmpl->mCampo['nu_empresa']->mDisabled = true;
            $ptmpl->mCampo['nu_embarcacao_projeto_anterior']->mDisabled = true;
            $ptmpl->mCampo['nu_embarcacao_projeto_novo']->mDisabled = true;
            $ptmpl->mCampo['nu_servico']->mDisabled = true;
            $ptmpl->mCampo['observacao']->mDisabled = true;

        }
    }

    public function ProcessePost() {
        switch ($this->template->get_acaoPostada()) {
            case 'SALVAR':
            case 'SALVAR_E_FECHAR':
            case 'SALVAR_E_LIBERAR':
            case 'OS_CONCLUIR_PROTOCOLO':
                $this->ProcesseSalvar();
                break;
        }
    }

    public function ProcesseSalvar() {
        try {
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $this->template->getValorCampo('id_solicita_visto');
            $os->Recuperar();

            $this->modelo->codigo = $this->template->getValorCampo('codigo');
            $this->modelo->RecuperePeloId();
            $this->modelo->observacao = $this->template->getValorCampo('observacao');
            $this->modelo->set_dt_requerimento($this->template->getValorCampo('dt_requerimento'));
            $this->modelo->set_dt_envio_bsb($this->template->getValorCampo('dt_envio_bsb'));
            $this->modelo->set_nu_embarcacao_projeto_anterior($this->template->getValorCampo('nu_embarcacao_projeto_anterior'));
            $this->modelo->set_nu_embarcacao_projeto_novo($this->template->getValorCampo('nu_embarcacao_projeto_novo'));

            $this->modelo->Salvar();

            if ($this->template->getValorCampo('dt_requerimento') != ''){
                $os->Fechar();
            }

            cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
            switch ($this->template->get_acaoPostada()) {
                case "SALVAR_E_LIBERAR":
                    $os->LibereParaCobranca(cSESSAO::$mcd_usuario);
                    cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
                    break;
            }
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $this->CarregueTemplatePeloModelo($this->template, $this->modelo);
    }

}
