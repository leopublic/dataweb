<?php

class cCTRL_PRODUTIVIDADE_ANALISTAS_LIST extends cCTRL_MODELO_LIST {
    protected $id_tipo_acompanhamento;
    public function InicializeModelo() {
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_PRODUTIVIDADE_ANALISTAS_LIST();
    }

    public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) {
        return cREPOSITORIO_OS::CursorProdutividade();
    }
}
