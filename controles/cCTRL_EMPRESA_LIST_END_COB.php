<?
class cCTRL_EMPRESA_LIST_END_COB extends cCTRL_MODELO_LIST{
	public function InicializeTemplate(){
		$this->template = new cTMPL_EMPRESA_LIST_END_COB();		
	}

	public function InicializeModelo(){
		$this->modelo = new cEMPRESA();
	}

	public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl){
		$modelo = $this->modelo;
		$rep = new cREPOSITORIO_EMPRESA;
		$cursor = $rep->CursorEnderecoCobranca($ptmpl->mFiltro, $ptmpl->get_ordenacao());
		return $cursor;
	}
	
}
