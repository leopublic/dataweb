<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_TARQ_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cTIPO_ARQUIVO();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_TARQ_EDIT();
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_TARQ_LIST', 'Liste');		
	}

}
