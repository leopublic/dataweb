<?php 
class cCTRL_PACS_LIST extends cCTRL_MODELO_LIST{
	public function InicializeModelo(){
		$this->modelo = new cpacote_servico();
	}
	
	public function InicializeTemplate(){
		$this->template = new cTMPL_PACS_LIST();
	}

	public function ProcessePost() {
		if($this->template->get_acaoPostada() == 'ADICIONAR'){
			try{				
				if(intval($this->template->getValorCampo('NU_SERVICO_NOVO')) == 0){
					cNOTIFICACAO::singleton('Informe o serviço a ser adicionado', cNOTIFICACAO::TM_ALERT);
				}
				else{
					cpacote_servico::IncluaServicoNoPacote($this->template->getValorFiltro('NU_SERVICO'), $this->template->getValorCampo('NU_SERVICO_NOVO'));
					cNOTIFICACAO::singleton('O serviço foi adicionado ao pacote com sucesso!', cNOTIFICACAO::TM_SUCCESS);					
				}
			}
			catch(Exception $e){
				cNOTIFICACAO::singleton('Não foi possível adicionar o serviço:'.$e->getMessage(), cNOTIFICACAO::TM_ERROR);				
			}
			
		}
		if($this->template->get_acaoPostada() == 'ATIVO'){
			try{
				$parametros = cHTTP::$POST['CMP___parametros'];
				$parmx = explode('&', $parametros);
				$parm = array();
				foreach($parmx as $x){
					if($x != ''){
						$tokens = explode('=', $x);
						$parm[$tokens[0]] = $tokens[1];						
					}
				}
				cpacote_servico::ServicoPrincipal($parm['nu_servico_pacote'], $parm['nu_servico_incluido']);
				cNOTIFICACAO::singleton('O serviço selecionado foi marcado como o principal".', cNOTIFICACAO::TM_SUCCESS);					
			}
			catch(Exception $e){
				cNOTIFICACAO::singleton('Não foi possível adicionar o serviço:'.$e->getMessage(), cNOTIFICACAO::TM_ERROR);				
			}
			
		}
		if($this->template->get_acaoPostada() == 'EXCLUIR'){
			try{
				$parametros = cHTTP::$POST['CMP___parametros'];
				$parmx = explode('&', $parametros);
				$parm = array();
				foreach($parmx as $x){
					if($x != ''){
						$tokens = explode('=', $x);
						$parm[$tokens[0]] = $tokens[1];						
					}
				}
				cpacote_servico::RetireServicoDoPacote($parm['nu_servico_pacote'], $parm['nu_servico_incluido']);
				cNOTIFICACAO::singleton('O serviço foi excluído do pacote".', cNOTIFICACAO::TM_SUCCESS);					
			}
			catch(Exception $e){
				cNOTIFICACAO::singleton('Não foi possível excluir o serviço:'.$e->getMessage()."<br/>".cAMBIENTE::$multimo_sql, cNOTIFICACAO::TM_ERROR);				
			}
			
		}
	}

	public function Redirecione() {
//		cHTTP::RedirecionePara('cCTRL_PACS_LIST', 'Liste', "NU_SERVICO=".$this->template->getValorFiltro('NU_SERVICO'));
	}

	public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) {
		return cpacote_servico::CursorServicosDoPacote($this->template->getValorFiltro('NU_SERVICO'));
	}
}
