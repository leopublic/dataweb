<?php
class cCTRL_PETICAO_ARQUIVAMENTO extends cCTRL_MODELO{
	public function testa(){
		$_POST['nu_candidato'] = '35904';
		$_POST['id_solicita_visto'] = '106948';
		$_POST['idProcurador'] = '3';
		$this->Gerar();
	}


	public function Gerar(){
		$nu_procurador = $_POST['idProcurador'];
		$id_solicita_visto = $_POST['id_solicita_visto'];
		$nu_candidato = $_POST['nu_candidato'];
		$exibe_assinatura = $_POST['assinatura'];
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO = $id_solicita_visto;
		$os->RecuperarSolicitaVisto();

		$cand = new cCANDIDATO;
		$cand->mNU_CANDIDATO = $nu_candidato;
		$cand->Recuperar();

		$processo = new cprocesso_cancel;
		$processo->RecupereSePelaOs($id_solicita_visto, $nu_candidato);

		$processo_prorrog = new cprocesso_prorrog;
		if($processo->mcodigo_processo_prorrog > 0){
			$processo_prorrog->RecupereSe($processo->mcodigo_processo_prorrog);
		}

		$empresa = new cEMPRESA;
		$empresa->mNU_EMPRESA = $os->mNU_EMPRESA;
		$empresa->RecuperePeloId();

		$procurador = new cprocurador_empresa;
		$procurador->nu_empresa = $os->mNU_EMPRESA;
		$procurador->nu_procurador = $nu_procurador;
		$procurador->RecupereSe();

		$funcao = new cFUNCAO_CARGO;
		$funcao->mCO_FUNCAO = $procurador->co_cargo_funcao;
		$funcao->RecuperePeloId();

		$meses = array("", "Janeiro", "Fevereiro", "MarÃ§o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro","Novembro", "Dezembro");
		$mesnum = intval(date('m'));
		$data = array(
				'processo' => $processo
				,'processo_prorrog' => $processo_prorrog
				,'cand'		=> $cand
				,'procurador' => $procurador
				,'empresa'	=> $empresa
				,'funcao'	=> $funcao
				,'dia'			=> date('d')
				, 'mes'	=> $meses[$mesnum]
				,'ano'	=> date('Y')
				,'exibe_assinatura' => $exibe_assinatura
			);
		$html =  cTWIG::Render('/intranet/processo/peticao_arquivamento.twig', $data);
		$mpdf = new mPDF('','', 0, '', 25, 25, 25, 5, 9, 9, 'P');
        $mpdf->img_dpi = 150;
        $mpdf->WriteHTML( $html );
        $mpdf->Output();

	}
}
