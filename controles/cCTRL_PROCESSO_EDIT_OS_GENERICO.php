<?php
/**
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_EDIT_OS_GENERICO extends cCTRL_PROCESSO_EDIT_OS{
	public function InicializeModelo() {
		$this->modelo = new cprocesso();
	}
	public function InicializeTemplate() {
		$this->template = new cTMPL_PROCESSO_EDIT_OS_GENERICO();
		$this->template->set_estiloTituloInterno();
	}	
	public function CarregueTemplatePeloModelo(cTEMPLATE_EDICAO &$ptmpl, cprocesso &$pmodelo) {
		// Recupera a chave
		$os = new cORDEMSERVICO();
		$os->Recuperar($this->template->getValorCampo('id_solicita_visto'));
		$ptmpl->CarregueDoOBJETO($os);
		//
	}

	
	public function ProcesseSalvar() {
		try{
			$os = new cORDEMSERVICO();
			$os->mID_SOLICITA_VISTO = $this->template->getValorCampo('id_solicita_visto');
			$this->template->AtribuaAoOBJETO($os);
			switch($this->template->mAcaoPostada){
				case "SALVAR":
					$os->SalvarObservacao();
					$os->AtualizeInfoCobranca();
					break;
				case "SALVAR_E_FECHAR":
					$os->AtualizeInfoCobranca();
					$os->Fechar();
					break;
				case "SALVAR_E_LIBERAR":
					$os->LibereParaCobranca(cSESSAO::$mcd_usuario);
					break;
			}
			cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
			$this->RedirecioneEmSucesso();
		}
		catch (cERRO_CONSISTENCIA $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch (Exception $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$this->CarregueTemplatePeloModelo($this->template, $this->modelo);
	}
	
}
