<?php

class cCTRL_PIT extends cCTRL_MODELO {

    public function listarRecibosCliente(){
        $data = array();
        $menu = cmenu::NovoMenuCandidato();
        $menuDir = cmenu::NovoMenuCandidatoSystem();
        $data['pagina']['menu']['esquerdo'] = $menu->JSON();
        $data['pagina']['menu']['direito'] = $menuDir->JSON();
        $data['pagina']['titulo'] = 'Mundivisas::Dataweb';
        $data['candidato'] = cSESSAO::$candidato;
        $data['nu_empresa_prestadora'] = 1;
        return cTWIG::Render('cliente/candidato/pit_listar.twig', $data);
        
    }

    public function listarRecibosAtendente(){
        $data = array();
        return cTWIG::Render('cliente/recibos', $data);
    }
}
