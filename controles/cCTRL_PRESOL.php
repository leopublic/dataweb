<?php

class cCTRL_PRESOL extends cCTRL_MODELO {

    public function dataPadrao() {
        $data = array();
        $data['pagina']['titulo'] = 'Mundivisas::Dataweb';
        $menu = cmenu::NovoMenuCliente();
        cHTTP::set_menu_esquerdo($menu);
        $data['pagina']['menu']['esquerdo'] = $menu->JSON();
        $menu = cmenu::NovoMenuClienteSystem();
        cHTTP::set_menu_Direito($menu);
        $data['pagina']['menu']['direito'] = $menu->JSON();
        $data['nu_empresa_prestadora'] = 1;
        return $data;
    }

    public function getNova() {
        $data = $this->dataPadrao();
        $data['embarcacoes'] = cEMBARCACAO_PROJETO::comboMustache(cSESSAO::$mcd_empresa, '', '(select...)');
        $data['psol_id'] = 0;
        $data['nu_empresa'] = cSESSAO::$mcd_empresa;
        $data['servicos'] = cSERVICO::comboDisponiveisClientes('', '(select...)');
                
        
        $data['notificacao'] = cNOTIFICACAO::singleton();
        return cTWIG::Render('cliente/abrir_pre_sol.twig', $data);
    }

}
