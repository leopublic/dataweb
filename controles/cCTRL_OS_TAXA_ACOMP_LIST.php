
<?php

class cCTRL_OS_TAXA_ACOMP_LIST extends cCTRL_MODELO_LIST {

    public function __construct() {
        
    }

    public function InicializeModelo() {
        $this->modelo = new cREPOSITORIO_OS();
    }

    public function InicializeTemplate() {
        cHTTP::$comCabecalhoFixo = true;
        $this->template = new cTMPL_OS_TAXA_ACOMP_LIST();
    }

    public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl) {
        if (!cHTTP::HouvePost()) {
            $date = date_create(date('Y-m-d'));
            $this->template->mFiltro['data_taxa_fim']->mValor = date('d/m/Y');
            $this->template->mFiltro['data_taxa_ini']->mValor = date_format(date_sub($date, date_interval_create_from_date_string("30 days")), 'd/m/Y');
        }
        if ($this->template->getValorFiltro('data_taxa_ini') != '') {
            $this->template->mFiltro['data_taxa_ini']->mWhere = ' data_taxa >= ' . cBANCO::DataOk($this->template->getValorFiltro('data_taxa_ini'));
        }
        if ($this->template->getValorFiltro('data_taxa_fim') != '') {
            $this->template->mFiltro['data_taxa_fim']->mWhere = ' data_taxa <= ' . cBANCO::DataOk($this->template->getValorFiltro('data_taxa_fim'));
        }
        
        $ordenacao = $this->template->get_ordenacao();
        if ($ordenacao == "emp_emb"){
            $ordenacao = 'no_razao_social';
        }
        
        $cursor = $this->modelo->cursorTaxasPagas($this->template->mFiltro, $ordenacao);
        return $cursor;
    }

    public function Redirecione() {
        
    }

}
