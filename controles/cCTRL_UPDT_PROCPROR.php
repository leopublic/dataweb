<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class cCTRL_UPDT_PROCPROR{
	public function nu_pre_cadastro()
	{
		$ret = array();
		try{
			$nu_pre_cadastro = cHTTP::$POST['valor'];
			$codigo = cHTTP::$POST['codigo'];
			$cd_usuario_pre_cadastro = cHTTP::$POST['cd_usuario_pre_cadastro'];
			cprocesso_prorrog::Atualiza_nu_pre_cadastro($codigo, $nu_pre_cadastro, $cd_usuario_pre_cadastro);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function nu_protocolo()
	{
		$ret = array();
		try{
			$nu_protocolo = cHTTP::$POST['valor'];
			$codigo = cHTTP::$POST['codigo'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cprocesso_prorrog::Atualiza_nu_protocolo($codigo, $nu_protocolo, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function dt_requerimento_pre_cadastro()
	{
		$ret = array();
		try{
			$dt_requerimento_pre_cadastro = cHTTP::$POST['valor'];
			$codigo = cHTTP::$POST['codigo'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			$pror = new cprocesso_prorrog();
			$pror->mcodigo = $codigo;


			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function dt_publicacao_dou()
	{
		$ret = array();
		try{
			$valor = cHTTP::$POST['valor'];
			$codigo = cHTTP::$POST['codigo'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cprocesso_prorrog::Atualiza_dt_publicacao_dou($codigo, $valor, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}


	public function nu_pagina_dou()
	{
		$ret = array();
		try{
			$valor = cHTTP::$POST['valor'];
			$codigo = cHTTP::$POST['codigo'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cprocesso_prorrog::Atualiza_nu_pagina_dou($codigo, $valor, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function dt_pre_cadastro()
	{
		$ret = array();
		try{
			$dt_pre_cadastro = cHTTP::$POST['valor'];
			$codigo = cHTTP::$POST['codigo'];
			$cd_usuario_pre_cadastro = cHTTP::$POST['cd_usuario_pre_cadastro'];
			cprocesso_prorrog::Atualiza_dt_pre_cadastro($codigo, $dt_pre_cadastro, $cd_usuario_pre_cadastro);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function dt_envio_bsb()
	{
		$ret = array();
		try{
			$dt_envio_bsb = cHTTP::$POST['dt_envio_bsb'];
			$codigo = cHTTP::$POST['codigo'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cprocesso_prorrog::Atualiza_dt_envio_bsb($codigo, $dt_envio_bsb, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function observacao()
	{
		$ret = array();
		try{
			$observacao = cHTTP::$POST['valor'];
			$codigo = cHTTP::$POST['codigo'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cprocesso_prorrog::Atualiza_observacao($codigo, $observacao, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function AtualizeCampoPadrao()
	{
		$ret = array();
		try{
			$valor		= cHTTP::$POST['valor'];
			$chave		= cHTTP::$POST['chave'];
			$nome_chave = cHTTP::$POST['xnome_chave'];
			$nome_campo = cHTTP::$POST['xnome_campo'];
			$tipo_campo	= cHTTP::$POST['tipo_campo'];
			cprocesso_prorrog::AtualizeCampoPadrao($chave, $nome_campo, $valor, $tipo_campo, $nome_chave);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);

	}
}
