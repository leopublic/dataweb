<?php

class cCTRL_USUARIO_EXTERNO extends cCTRL_MODELO {

    public function getNewPassword(){
        $data['email_pessoal'] = htmlspecialchars($_GET['email_pessoal']);
        $data['ativa'] = 'DOCS';
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        return cTWIG::Render('public/nova_senha.twig', $data);        
    }

    public function getEnvieNovaSenhaId(){
        $rep = new cREPOSITORIO_CANDIDATO;
        return $rep->envieNovaSenhaId($_GET['NU_CANDIDATO']);
        
    }
    
    public function postNewPassword(){
        $email_pessoal = $_POST['CMP_email_pessoal'];
        $rep = new cREPOSITORIO_CANDIDATO;
        try{
            $rep->envieNovaSenha($email_pessoal);
            cNOTIFICACAO::singleton('Email sent successfully', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
            
        }
        cHTTP::RedirecionaLinkInterno('/paginas/carreguePublico.php?controller=cCTRL_USUARIO_EXTERNO&metodo=getNewPassword&email_pessoal='.$email_pessoal);
    }
    
}
