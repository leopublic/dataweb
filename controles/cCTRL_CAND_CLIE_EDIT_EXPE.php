<?php
class cCTRL_CAND_CLIE_EDIT_EXPE extends cCTRL_CAND_CLIE_EDIT{
	public function __construct(){
		parent::__construct();
		$this->nomeTemplate = 'cliente/formulario_cand.html';
		$this->nomeAbaAtiva = 'EXPE';
	}
	
	public function InicializeModelo(){
		$this->modelo = new cCANDIDATO();
	}
	
	public function InicializeTemplate(){
		$this->template = new cTMPL_CAND_CLIE_EDIT_EXPE();
		parent::InicializeTemplate();
	}

	public function ProcessePost(){
		$this->modelo->Recuperar($this->template->getValorCampo('NU_CANDIDATO'));
		parent::ProcessePost();		
	}

	public function ProcesseSalvar(){
		$this->modelo->AtualizarExterno();
	}
}
