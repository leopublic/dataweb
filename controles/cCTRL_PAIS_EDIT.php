<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_PAIS_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cPAIS_NACIONALIDADE();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_PAIS_EDIT();
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_PAIS_LIST', 'Liste');		
	}

}
