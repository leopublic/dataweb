<?php

class cCTRL_ARQUIVO extends cCTRL_MODELO {

    /**
     * Carrega um arquivo novo no sistema
     *
     * @param type $arquivo
     * @param type $nu_candidato
     * @param type $id_solicita_visto
     * @param type $id_usuario
     * @param type $id_tipo_arquivo
     */
    public function carregaNovoArquivoCandidatoOs($arquivo, \cCANDIDATO $candidato, \cORDEMSERVICO $os, $id_usuario, $id_tipo_arquivo) {

        $this->DiretorioUploadOk($candidato->mNU_EMPRESA, $candidato->mNU_CANDIDATO);
    }

    public static function DiretorioUploadOk($pNU_EMPRESA, $pNU_CANDIDATO) {
        $mydocdir = cCONFIGURACAO::$configuracoes[AMBIENTE]['raiz'] . DIRECTORY_SEPARATOR . 'arquivos' . DIRECTORY_SEPARATOR . 'documentos';
        if (!is_dir($mydocdir)) {
            mkdir($mydocdir, 0770);
        }
        $updir = $mydocdir . DIRECTORY_SEPARATOR . $pNU_EMPRESA;
        if (!is_dir($updir)) {
            mkdir($updir, 0770);
        }
        $updir = $updir . DIRECTORY_SEPARATOR . $pNU_CANDIDATO;

        if (!is_dir($updir)) {
            mkdir($updir, 0770);
        }
        return $updir;
    }

    public static function ListaArquivosDeletaveisSol() {
        $mydocdir = cCONFIGURACAO::$configuracoes[AMBIENTE]['raiz'] . DIRECTORY_SEPARATOR . 'arquivos' . DIRECTORY_SEPARATOR . 'solicitacoes';
        $raiz = $mydocdir;

        $dh = opendir($raiz);
        self::verificaConteudoDiretorioSol($raiz, $dh);
    }

    public static function verificaConteudoDiretorioSol($nomedir, $dir) {
        print "<br/>Iniciando a verificação do diretorio " . $nomedir;
        while ($arq = readdir($dir)) {
            print "<br/>Iniciando a verificação do arquivo " . $nomedir . DIRECTORY_SEPARATOR . $arq;
            if ($arq != '.' && $arq != '..') {
                $novodir = $nomedir . DIRECTORY_SEPARATOR . $arq;
                if (is_dir($novodir)) {
                    self::verificaConteudoDiretorioSol($novodir, opendir($novodir));
                } else {
                    $nome = explode('_', $arq);
                    //SOLIC000061412_98765.msg
                    $id_solicita_visto = substr($nome[0], 5);
                    $candidato = explode(".", $nome[1]);
                    $nu_candidato = $candidato[0];
                    $sql = "select nu_candidato from candidato where nu_candidato = " . $nu_candidato;
                    $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
                    $rw = $res->fetch();
                    if ($rw[0] != $nu_candidato) {
                        print "<br/>Arquivo " . $nomedir . DIRECTORY_SEPARATOR . $arq . " pode ser excluído  <------------------";
                    } else {
                        print "<br/>Arquivo " . $nomedir . DIRECTORY_SEPARATOR . $arq . " ok";
                    }
                }
            }
        }
    }

    public function verificaIntegridadePeriodo() {
        set_time_limit(0);
        $sql = "select * from arquivos where dt_inclusao >= '2013-04-11' and dt_inclusao <= '2014-01-10' order by dt_inclusao desc";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $total = 0;
        $erro = 0;
        $raiz = '/var/www/dataweb/arquivos/';
        while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $total++;
            if ($rs['ID_SOLICITA_VISTO'] > 0) {
                $dir = $raiz . 'solicitacoes/' . $rs['NU_EMPRESA'] . '/' . $rs['NO_ARQUIVO'];
            } else {
                if ($rs['NU_EMPRESA'] == '') {
                    $rs["NU_EMPRESA"] = 0;
                }
                $dir = $raiz . "documentos/" . $rs['NU_EMPRESA'] . "/" . $rs['NU_CANDIDATO'] . "/" . $rs['NO_ARQUIVO'];
            }
            if (!file_exists($dir)) {
                $erro++;
//                print '<br>'. $dir ;
            } else {
//                print '<br>Arquivo ' . $rs['NU_SEQUENCIAL'] . ' - ' . $dir . ' (' . $rs['DT_INCLUSAO'] . ') OK.';
            }
        }
        print "<br>Total de arquivos: " . $total;
        print "<br>Total nao encontrado: " . $erro;
    }

    public function verificaIntegridade() {
        set_time_limit(0);
        $sql = "select * from arquivos order by dt_inclusao desc";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $total = 0;
        $erro = 0;
        $raiz = '/var/www/dataweb/arquivos/';
        while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $total++;
            if ($rs['ID_SOLICITA_VISTO'] > 0) {
                $dir = $raiz . 'solicitacoes/' . $rs['NU_EMPRESA'] . '/' . $rs['NO_ARQUIVO'];
            } else {
                if ($rs['NU_EMPRESA'] == '') {
                    $rs["NU_EMPRESA"] = 0;
                }
                $dir = $raiz . "documentos/" . $rs['NU_EMPRESA'] . "/" . $rs['NU_CANDIDATO'] . "/" . $rs['NO_ARQUIVO'];
            }
            if (!file_exists($dir)) {
                $erro++;
                print '<br>' . $dir;
            } else {
//                print '<br>Arquivo ' . $rs['NU_SEQUENCIAL'] . ' - ' . $dir . ' (' . $rs['DT_INCLUSAO'] . ') OK.';
            }
        }
        print "<br>Total de arquivos: " . $total;
        print "<br>Total nao encontrado: " . $erro;
    }

    public function adicionaTamanho(){

    }

    public function verificaDiretorio(){
        $rep = new cREPOSITORIO_ARQUIVO;
        $caminho = cAMBIENTE::getDirDoc();
        $i = 0;
        $erro = 0;
        $tamanho_total;
        $rep->verificaDiretorio($caminho, $i, $erro, $tamanho_total);
        $caminho = cAMBIENTE::getDirSol();
        $rep->verificaDiretorio($caminho, $i, $erro, $tamanho_total);
        print "<br/>Total verificado: ".$i;
        print "<br/>Total com erros: ".$erro;
        print "<br/>Tamanho total dos excluídos: ".$tamanho_total/1024/1024;
    }

    public function getArquivosDoCandidato() {
        return utf8_encode($this->arquivosDoCandidato());
    }

    public function postArquivosDoCandidato() {
        try{
            $rep = new cREPOSITORIO_ARQUIVO;
            $rep->consisteArquivoEnviado($_FILES, 'ARQUIVO');
            $rep->novoArquivoDoCandidatoPeloOperacional($_POST['NU_CANDIDATO'], $_FILES['ARQUIVO'], $_POST['CMP_ID_TIPO_ARQUIVO']);
            $_GET['NU_CANDIDATO'] = $_POST['NU_CANDIDATO'];
            $_SESSION['msg_arquivos'] = "Arquivo adicionado com sucesso!";
        } catch (Exception $ex) {
            $_SESSION['msg_arquivos'] = "Não foi possível adicionar o arquivo. <br/>".$ex->getMessage();
        }
        cHTTP::RedirecionaLinkInterno('/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $_POST['NU_CANDIDATO'] . '#pnlArquivos');
    }

    public function arquivosDoCandidato() {
        $NU_CANDIDATO = $_GET['NU_CANDIDATO'];

        $cand = new cCANDIDATO();
        $cand->mNU_CANDIDATO = $NU_CANDIDATO;
        $res = $cand->CursorArquivos();
        $rw_all = $res->fetchAll(PDO::FETCH_ASSOC);
        $arquivos = array();

        foreach ($rw_all as &$rw) {
            $arquivo = new cARQUIVO();
            $arquivo->CarreguePropriedadesPeloRecordset($rw);
            $arquivos[] = $arquivo;
        }

        $msg = $_SESSION['msg_arquivos'];
        unset($_SESSION['msg_arquivos']);

        $data = array(
            'podeExcluir' => true
            , 'arquivos' => $arquivos
            , 'tipos_arquivo' => cTIPO_ARQUIVO::comboMustache()
            , 'NU_CANDIDATO' => $NU_CANDIDATO
            , 'msg' => $msg
        );
        return cTWIG::Render('intranet/candidato/arquivos.twig', $data);
    }

    public function arquivosDoCandidatoPorEmpresa() {
        // Recupera empresas

        $NU_CANDIDATO = $_GET['NU_CANDIDATO'];
        $NomeDiv = $_GET['NomeDiv'];

        $cand = new cCANDIDATO();
        $cand->mNU_CANDIDATO = $NU_CANDIDATO;
        $res = $cand->CursorArquivos();
        $rw_all = $res->fetchAll(PDO::FETCH_ASSOC);
        $arquivos = array();

        foreach ($rw_all as &$rw) {
            $arquivo = new cARQUIVO();
            $arquivo->CarreguePropriedadesPeloRecordset($rw);
            $arquivos[] = $arquivo;
        }
        $data = array(
            'podeExcluir' => true
            , 'arquivos' => $arquivos
            , 'tipos_arquivo' => cTIPO_ARQUIVO::comboMustache()
        );
        return cTWIG::Render('intranet/candidato/arquivos.twig', $data);
    }

    public function removerDoCandidato(){
        $nu_sequencial = $_GET['nu_sequencial'];
        $nu_candidato = $_GET['nu_candidato'];
        try{
            $rep = new cREPOSITORIO_ARQUIVO;
            $rep->remover($nu_sequencial);
            $_SESSION['msg_arquivos'] = "Arquivo removido com sucesso!";
        } catch (Exception $ex) {
            $_SESSION['msg_arquivos'] = "Não foi possível adicionar o arquivo. <br/>".$ex->getMessage();
        }
        $_GET['NU_CANDIDATO'] = $_GET['nu_candidato'];
        return utf8_encode($this->arquivosDoCandidato());

    }

    public function getAdicionarArquivoOs(){
        $id = $_GET['id_solicita_visto'];
        if ($id > 0){
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id;
            $candidatos = $os->comboCandidatosMustache();
            $titulo = "Adicionar arquivo na Ordem de Serviço ".$_GET['nu_solicitacao'];
        } else {
            $titulo = "Adicionar arquivo em nova Ordem de Serviço ";
            $candidatos = array(array('valor' => '', 'descricao' => '(geral)'));
        }

        if (isset($_SESSION['msg'])){
            $msg = $_SESSION['msg'];
            unset($_SESSION['msg']);
        } else {
            $msg = null;
        }
        $rep = new cREPOSITORIO_ARQUIVO;
        $arquivos = $rep->arquivos_da_os($id);
        $data = array(
             'tipos_arquivo' => cTIPO_ARQUIVO::comboMustache()
            , 'id_solicita_visto' => $id
            , 'nu_solicitacao' => $_GET['nu_solicitacao']
            , 'titulo' => $titulo
            , 'candidatos' => $candidatos
            , 'arquivos' => $arquivos
            , 'msg' => $msg
        );
        return cTWIG::Render('intranet/os/arquivo_adicionar.twig', $data);
    }

    public function postAdicionarArquivoOs(){
        $rep = new cREPOSITORIO_ARQUIVO;
        try{
            $rep->consisteArquivoEnviado($_FILES, 'ARQUIVO');
            $rep->novoArquivoDaOs($_POST['CMP_nu_candidato'], $_FILES['ARQUIVO'], $_POST['CMP_ID_TIPO_ARQUIVO'], $_POST['id_solicita_visto'], cSESSAO::$mcd_usuario);
            $msg = array('tipo' => 'OK', 'texto' => 'Arquivo carregado com sucesso!');
        } catch (Exception $ex) {
            $msg = array('tipo' => 'error', 'texto' => 'Não foi possível carregar o arquivo: '.$ex->getMessage());
        }
        $_GET['id_solicita_visto'] = $_POST['id_solicita_visto'];
        $_GET['nu_solicitacao'] = $_POST['nu_solicitacao'];
        $_SESSION['msg'] = $msg;
        return $this->getAdicionarArquivoOs();
    }


    public function getAdicionarArquivoOsFoto(){
        $id = $_GET['id_solicita_visto'];
        if ($id > 0){
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id;
            $candidatos = $os->comboCandidatosMustache();
            $titulo = "Adicionar foto na Ordem de Serviço ".$_GET['nu_solicitacao'];
        } else {
            $titulo = "Adicionar foto em nova Ordem de Serviço ";
            $candidatos = array(array('valor' => '', 'descricao' => '(geral)'));
        }

        if (isset($_SESSION['msg'])){
            $msg = $_SESSION['msg'];
            unset($_SESSION['msg']);
        } else {
            $msg = null;
        }
        $rep = new cREPOSITORIO_ARQUIVO;
        $arquivos = $rep->arquivos_da_os($id);
        $data = array(
             'tipos_arquivo' => cTIPO_ARQUIVO::comboMustache()
            , 'id_solicita_visto' => $id
            , 'nu_solicitacao' => $_GET['nu_solicitacao']
            , 'titulo' => $titulo
            , 'candidatos' => $candidatos
            , 'arquivos' => $arquivos
            , 'msg' => $msg
        );
        return cTWIG::Render('intranet/os/arquivo_adicionarFoto.twig', $data);
    }


    public function corrigeTamanho(){
        set_time_limit(0);
        $rep = new cREPOSITORIO_ARQUIVO;
        $rep->corrigeTamanho();
    }

    public function criaCampos(){
        $sql = "alter table arquivos add NU_TAMANHO int null";
        cAMBIENTE::$db_pdo->exec($sql);
        $sql = "alter table arquivos add FL_DISPONIVEL tinyint null";
        cAMBIENTE::$db_pdo->exec($sql);
    }
    public function monitoramentoTamanho(){
        $menu = cLAYOUT::Menu();
        $rep = new cREPOSITORIO_ARQUIVO;
        $porEmpresa = $rep->todosArquivosPorEmpresa();


        $data = array(
            'regs' => $porEmpresa
            ,'menu' => $menu
        );
        return cTWIG::Render('intranet/arquivo/total_empresa.twig', $data);

    }

    public function download() {
        $NU_SEQUENCIAL = $_GET['NU_SEQUENCIAL'];
        $rep = new cREPOSITORIO_ARQUIVO();
        $rep->download($NU_SEQUENCIAL);
    }

    public function corrigir_extensoes(){
        $sql = "select * from arquivos where no_arquivo not like '%.%' and NU_SEQUENCIAL > 165300 order by nu_sequencial desc";
        $res = cAMBIENTE::$db_pdo->query($sql);
        while($rs = $res->fetch()){
            $arq = new cARQUIVO;
            $arq->CarreguePropriedadesPeloRecordset($rs);
            $this->corrige_extensao($arq);
        }
    }

    public function corrige_extensao_um(){
        $nu_sequencial= $_GET['NU_SEQUENCIAL'];
        $sql = "select * from arquivos where nu_sequencial = ".$nu_sequencial;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch();
        $arq = new cARQUIVO;
        $arq->CarreguePropriedadesPeloRecordset($rs);
        $this->corrige_extensao($arq);
    }

    public function corrige_extensao($arq){
        $ext = strtolower(substr($arq->mNO_ARQ_ORIGINAL, -3));
        $caminho_antes = $arq->caminhoRealInstancia();
        if ($ext == 'pdf' || $ext == 'jpg' || $ext == 'msg' || $ext == 'doc' || $ext == 'png' || $ext == 'xls' || $ext == 'msg' || $ext == 'eml'){
            $arq->mNO_ARQUIVO = $arq->mNO_ARQUIVO.".".$ext;
            $arq->mNO_ARQ_ORIGINAL = substr($arq->mNO_ARQ_ORIGINAL, 0, strlen($arq->mNO_ARQ_ORIGINAL) - 3).".".$ext;
            $arq->salvar();
        } else {
            $ext = strtolower(substr($arq->mNO_ARQ_ORIGINAL, -4));
            if ($ext == 'xlsx' || $ext == 'jpeg' ){
                $arq->mNO_ARQUIVO = $arq->mNO_ARQUIVO.".".$ext;
                $arq->mNO_ARQ_ORIGINAL = substr($arq->mNO_ARQ_ORIGINAL, 0, strlen($arq->mNO_ARQ_ORIGINAL) - 4).".".$ext;
                $arq->salvar();
            }
        }
        $caminho_depois = $arq->caminhoRealInstancia();
        print "<br/>".$caminho_antes." ====> ".$caminho_depois;
        if (file_exists($caminho_antes)){
            rename($caminho_antes, $caminho_depois);
        }
    }

}
