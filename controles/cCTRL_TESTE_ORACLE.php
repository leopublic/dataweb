<?php

class cCTRL_TESTE_ORACLE extends cCTRL_MODELO {

    public function EMBP() {
        $pdo_string = 'mysql:host=localhost;dbname=my_database';
        $tns = "
			(DESCRIPTION =
			  (ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.22.57)(PORT = 1521))
			  (CONNECT_DATA =
				(SERVER = DEDICATED)
				(SERVICE_NAME = XE)
			  )
			)
		";
        $pdo_string = 'oci:dbname=' . $tns;

        try {
            $dbh = new PDO($pdo_string, 'DATAWEB', 'dataweb');
        } catch (PDOException $e) {
            echo "Failed to obtain database handle: " . $e->getMessage();
            exit;
        }
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = "SELECT * FROM DW_TCSPRJ_EMBARCACAO_PROJETO;";

        $stmt = $dbh->prepare($query);

        if ($stmt->execute()) {
            echo "<h4>$query</h4>";
            echo "<pre>";
            while ($row = $stmt->fetch()) {
                print_r($row);
            }
            echo "</pre>";
        }
    }

    public function embp2() {
        $db = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.22.57)(PORT = 1521)))(CONNECT_DATA=(SID=XE)))";
//        $db = "10.10.22.57/XE";
        $conn = oci_connect("DATAWEB", "dataweb", $db, 'WE8ISO8859P1');
        if (!$conn) {
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

// Prepare the statement
        $stid = oci_parse($conn, 'SELECT * FROM SANKHYA.TGFPAR');
        if (!$stid) {
            $e = oci_error($conn);
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

// Perform the logic of the query
        $r = oci_execute($stid);
        if (!$r) {
            $e = oci_error($stid);
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

// Fetch the results of the query
        print "<table border='1'>\n";
        while ($row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS)) {
            print "<tr>\n";
            foreach ($row as $item) {
                print "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
            }
            print "</tr>\n";
        }
        print "</table>\n";

        oci_free_statement($stid);
        oci_close($conn);
    }

    public function testeInsert() {
        $db = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.10.22.57)(PORT = 1521)))(CONNECT_DATA=(SID=XE)))";
        $db = "10.10.22.57/XE";
        $conn = oci_connect("DATAWEB", "dataweb", $db);
        if (!$conn) {
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        $sql = "INSERT ALL  "
                . "INTO SANKHYA.AD_EXTRATOITE (CODPARC, NUNICO, CODTIPTIT, DTOS, NUMOS, VLRTIT, CODIGO,CODPROJ) VALUES (11,1,91,to_date('2014-07-18', 'YYYY-MM-DD'),981,8.55,1,1100002) "
                . "INTO SANKHYA.AD_EXTRATOITE (CODPARC, NUNICO, CODTIPTIT, DTOS, NUMOS, VLRTIT, CODIGO,CODPROJ) VALUES (21,1,90,to_date('2014-07-18', 'YYYY-MM-DD'),976,5.55,1,1200006) "
                . "INTO SANKHYA.AD_EXTRATOITE (CODPARC, NUNICO, CODTIPTIT, DTOS, NUMOS, VLRTIT, CODIGO,CODPROJ) VALUES (11,3,93,to_date('2014-07-18', 'YYYY-MM-DD'),845,10.55,1,1100002) "
                . "SELECT * FROM DUAL";

        $stid = oci_parse($conn, $sql);

        $r = oci_execute($stid);  // executes and commits

        if ($r) {
            print "One row inserted";
        } else {
            $e = oci_error($stid);
            print htmlentities($e['message'], ENT_QUOTES);
        }
    }
}
