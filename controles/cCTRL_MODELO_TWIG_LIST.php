<?php

class cCTRL_MODELO_TWIG_LIST extends cCTRL_MODELO {

    protected $data;
    protected $nomeTemplate;

    public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl) {
        $modelo = $this->modelo;
        $cursor = $modelo->Listar($ptmpl->mFiltro, $ptmpl->get_ordenacao());
        return $cursor;
    }

    public function Liste() {
        $this->InicializeModelo();
        $this->InicializeTemplate();
        $this->CarregueTemplatePeloHTTP();
        // Tratar post
        $this->ProcessePost();

        $cursor = $this->get_cursorListagem($this->template);
        $this->data = $this->template->JSON_Data($cursor);
        $this->complementaData();
        return $this->RenderizaSaida();
    }

    public function complementaData(){
        
    }
    
    public function CarregueTemplatePeloHTTP() {
        $this->template->CarregueDoHTTP();
    }

    public function RenderizaSaida() {
        if (!cHTTP::TemRedirecionamento() && isset($this->data)) {
            $this->data['nu_empresa_prestadora'] = $_SESSION['nu_empresa_prestadora'];
            return cTWIG::Render($this->nomeTemplate, $this->data);
        }
    }

    public function ProcessePost() {
        
    }

    public function ProcessarPostGrid(cTEMPLATE $pTemplate, cMODELO $pModelo, $pMetodoAtualizacao) {
        $qtdLinhas = $pTemplate->qtdLinhas();
        $classeModelo = get_class($pModelo);
        for ($index = 0; $index < $qtdLinhas; $index++) {
            $pModelo = new $classeModelo;
            $pTemplate->AtribuaDoArrayAoOBJETO($pModelo, $index);
            $pModelo->$pMetodoAtualizacao();
        }
    }

    public function ProcessaPostLinha(cTEMPLATE $pTemplate, cMODELO $pModelo, $pMetodoAtualizacao) {
        $pModelo = new $classeModelo;
        $pTemplate->AtribuaDoArrayAoOBJETO($pModelo, $index);
        $pModelo->$pMetodoAtualizacao();
    }

    public function InicializarTemplateListagem(&$pTmpl) {
        
    }

    /**
     *
     * @param cTEMPLATE $pTemplateListar
     * @param sting $pAbaAtiva Nome da aba que está aberta.
     * @return type
     */
    public function EnveloparParaTela($pConteudo, cTEMPLATE $pTemplate) {
        $ret = '<div id="' . $pTemplate->mnome . '">';
        $ret.= $pConteudo;
        $ret.= $pTemplate->TratamentoPadraoDeMensagens();
        $ret.= '</div>';
//		cHTTP::AdicionarScript('ExibirMsg("#'.$pTemplate->mnome.' #dialog", "'.cHTTP::TituloMsg().'" );');
        return $ret;
    }

    /**
     * Por enquanto não está sendo utilizado pois está usando o renderize form do layout.
     * @param type $pConteudo
     * @param cTEMPLATE $pTemplate
     * @return string
     */
    public function EnveloparEmForm($pConteudo, cTEMPLATE $pTemplate) {
        $ret = cLAYOUT::RenderizeCabecalhoForm($pTemplate);
        $ret.= $pConteudo;
        $ret.= '</form>';
//		cHTTP::AdicionarScript('ExibirMsg("#'.$pTemplate->mnome.' #dialog", "'.cHTTP::TituloMsg().'" );');
        return $ret;
    }

}
