<?php

/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_EDIT_OS extends cCTRL_MODELO_EDIT {

    protected $os;

    public function CarregueAdicionais(){
        
    }
    public function CarregueTemplatePeloModelo(cTEMPLATE_EDICAO &$ptmpl, &$pmodelo) {
        // Recupera a chave
        $ptmpl->AtribuaAoOBJETO($pmodelo, "");
        $pmodelo->RecupereSePelaOsSemCand($ptmpl->getValorCampo('id_solicita_visto'));
        // Carrega os campos
        if (intval($pmodelo->mcodigo) == 0 ){
            $os = new cORDEMSERVICO();
            $os->Recuperar($ptmpl->getValorCampo('id_solicita_visto'));
            $os->AdicionaProcessoATodosCandidatos();
            $pmodelo->RecupereSePelaOsSemCand($ptmpl->getValorCampo('id_solicita_visto'));
        }
        $ptmpl->CarregueDoOBJETO($pmodelo);
        // 
        $this->CarregueAdicionais();
    }

    public function Edite() {
        if ($this->AcessoLiberado($this->get_AcessoNecessario)) {
            $this->InicializeModelo();
            $this->InicializeTemplate();
            // Carrega o ID informado no get ou o form postado anteriormente, se houver
            $this->template->CarregueDoHTTP();
            // Para capturar as inclusões e alterações de processo se houverem
            $msg = cINTERFACE::ProcessePost(cHTTP::$POST, cHTTP::$GET, cHTTP::$FILES, cHTTP::$SESSION);
            cHTTP::$SESSION['msg'] = $msg;
            // Carrega o usuário correspondente à tela exibida
            if ($this->template->mFoiPostado) {
                $this->houvePost = true;
                $this->ProcessePost();
            } else {
                $this->houvePost = false;
            }
            // Sempre recarregar pelo modelo...
            $this->CarregueTemplatePeloModelo($this->template, $this->modelo);
            $conteudo = $this->template->HTML();
            return $conteudo;
        }
    }

    public function ProcessePost() {

        switch ($this->template->get_acaoPostada()) {
            case 'SALVAR':
            case 'SALVAR_E_FECHAR':
            case 'SALVAR_E_LIBERAR':
                $this->ProcesseSalvar();
                break;
        }
    }

    public function AtualizacaoAdicional(){
        
    }
    
    public function ProcesseSalvar() {
        $this->template->AtribuaAoOBJETO($this->modelo);
        try {
            $this->os = new cORDEMSERVICO();
            $this->os->mID_SOLICITA_VISTO = $this->template->getValorCampo('id_solicita_visto');
            $this->os->Recuperar();
            // Só salva se a OS estiver aberta...
            if (!$this->os->get_fechada()) {
                $this->modelo->Salve_ProcessoEdit();
            }
            $this->AtualizacaoAdicional();
            if (array_key_exists('id_tipo_envio', $this->template->mCampo)) {
                $this->os->AtualizarTipoEnvio($this->template->getValorCampo('id_tipo_envio'), cSESSAO::$mcd_usuario);
            }


            if (array_key_exists('id_status_sol', $this->template->mCampo) && $this->template->getValorCampo('id_status_sol') > 0) {
                $this->os->AtualizarStatus($this->template->getValorCampo('id_status_sol'), cSESSAO::$mcd_usuario);
            }

            $this->ProcessaFechamento();
            if (intval($this->os->mstco_id) < 2) {
                $this->os->mtppr_id = $this->template->getValorCampo('tppr_id');
                $this->os->msoli_tx_obs_cobranca = $this->template->getValorCampo('soli_tx_obs_cobranca');

                if ($this->template->get_acaoPostada() == "SALVAR_E_LIBERAR") {
                    $this->os->LibereParaCobranca(cSESSAO::$mcd_usuario);
                } else {
                    $this->os->AtualizeInfoCobranca();
                }
            }
            $this->ProcessaSalvarEFechar();
            cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
            $this->RedirecioneEmSucesso();
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
    }

    public function ProcessaFechamento() {
        if ($this->modelo->PodeFechar()) {
            $this->os->Fechar();
        }
    }

    public function AcoesAdicionaisNoFechamento(){

    }

    public function ProcessaSalvarEFechar() {
        if ($this->template->get_acaoPostada() == "SALVAR_E_FECHAR") {
            $this->os->Fechar();
            $this->AcoesAdicionaisNoFechamento();
        }
    }

    public function RedirecioneEmSucesso() {
        
    }

}
