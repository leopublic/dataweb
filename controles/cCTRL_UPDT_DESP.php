<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class cCTRL_UPDT_DESP {

    public function AtualizeCampoPadrao() {
        $ret = array();
        try {
            $valor = cHTTP::$POST['valor'];
            ;
            $chave = cHTTP::$POST['chave'];
            $nome_campo = cHTTP::$POST['xnome_campo'];
            $tipo_campo = cHTTP::$POST['tipo_campo'];
            cdespesa::AtualizeCampoPadrao($chave, $nome_campo, $valor, $tipo_campo);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

}
