<?php 
class cCTRL_DEPENDENTE_LIST extends cCTRL_MODELO_LIST{
	public function __construct()
	{
		$this->nomeTemplate = 'cliente/formulario.html';
	}
	
	public function Exclua()
	{
		$nu_candidato = cHTTP::$GET['NU_CANDIDATO'];
		$nu_candidato_parente = cHTTP::$GET['nu_candidato_parente'];
		try{
			$cand = new cCANDIDATO();
			$cand->mNU_CANDIDATO = $nu_candidato;
			$cand->Excluir();
			cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
			$this->template->mCampo['__acao_sucesso']->mValor = 1;
			cHTTP::$SESSION['msg'] = 'Operação realizada com sucesso!';
		}
		catch (cERRO_CONSISTENCIA $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
			cHTTP::$SESSION['msg'] = 'Não foi possível realizar a operação!';
		}
		catch (Exception $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
			cHTTP::$SESSION['msg'] = 'Não foi possível realizar a operação!';
		}
		cHTTP::RedirecionaLinkInterno('operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$nu_candidato_parente.'&painelAtivo=6');
	}
	
	public function InicializeModelo()
	{
		$this->modelo = new cCANDIDATO();
	}
	
	public function InicializeTemplate()
	{
		$this->template = new cTMPL_DEPENDENTE_LISTAR();
	}

	public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) {
		$this->modelo->mNU_CANDIDATO = $this->template->getValorFiltro('NU_CANDIDATO');
		$cursor = $this->modelo->CursorDependentes();
		return $cursor;
	}
	
	public function ProcessePost()
	{
	}
}
