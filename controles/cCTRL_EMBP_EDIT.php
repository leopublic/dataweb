<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_EMBP_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cEMBARCACAO_PROJETO();
	}

	public function InicializeTemplate() {
		$this->template = new cTMPL_EMBP_EDIT();
	}

	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_EMBP_LIST', 'Liste');
	}
}
