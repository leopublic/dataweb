<?php

/**
 * Description of cCTRL_EMBP_EDIT
 * @author leonardo
 */
class cCTRL_EMPR_EDIT extends cCTRL_MODELO_EDIT {

    public function get_AcessoNecessario() {
        return Acesso::tp_Cadastro_EmpresaAlterar;
    }

    public function InicializeModelo() {
        $this->modelo = new cEMPRESA();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_EMPR_EDIT();
    }

    public function RedirecioneEmSucesso() {
//		cHTTP::RedirecionePara('cCTRL_REPC_LIST', 'Liste');		
    }

}
