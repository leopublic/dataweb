<?php

/**
 * Description of cCTRL_PROCESSO
 *
 * @author leonardo
 */
class cCTRL_PROCESSO extends cCTRL_MODELO {

    public function FormEdicaoOS(cORDEMSERVICO $pOS) {
        throw new Exception('Formulário de edição não implementado!');
    }

    public function FormEdicaoPadrao(cORDEMSERVICO $pOS, $pNomeEdicao) {
        $tela = new cEDICAO($pNomeEdicao);
        $tela->CarregarConfiguracao();

        $tela->mCampoNome['id_solicita_visto']->mValor = $pOS->mID_SOLICITA_VISTO;
        $tela->ResseteChavePara('id_solicita_visto');
        if ($pOS->mReadonly) {
            $tela->ProtejaTodosCampos();
        } else {
            $tela->mAcoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Clique para salvar as alterações");
            $tela->mAcoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar e Fechar a OS", "SALVAR_E_FECHAR", "Clique para salvar as alterações e fechar a OS");
        }

        if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . cINTERFACE::CMP_EDICAO]) && cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . cINTERFACE::CMP_EDICAO] == $tela->mno_edicao && !cHTTP::getResultadoOperacao()
        ) {
            cINTERFACE::RecupereValoresEdicao($tela, cHTTP::$POST);
        } else {
            $tela->RecupereRegistro();
        }
        return $tela;
    }

    public function RenderizacaoPadraoEdicao(cEDICAO $pTela) {
        $ret = '';
        if ($pTela->mTitulo != '') {
            $ret .= '<div class="ui-widget ui-widget-header tituloProcesso">' . $pTela->mTitulo . '</div>';
        }
        $ret .= cINTERFACE::formEdicao($pTela);
        $ret .= cINTERFACE::RenderizeEdicaoComPaineis($pTela);
        $ret .= '</table>' . "\n";
        if (count($pTela->mAcoes) > 0) {
            $ret .= '<div style="margin-top:10px;text-align:center">';
            $espaco = "";
            foreach ($pTela->mAcoes as $xacao) {
                $ret .= $espaco . cINTERFACE::RenderizeAcao($xacao);
                $espaco = "&nbsp;";
            }
            $ret .= '</div>';
        }
        $ret .= '</form>' . "\n";
        return $ret;
    }

    /**
     *
     * @param cORDEMSERVICO $pOS
     * @return cCTRL_PROCESSO Controller para o processo do serviço da OS.
     */
    public static function FabricaControllerProcessoDaOS(cORDEMSERVICO $pOS) {
        return self::FabricaControllerPeloAcompanhamento($pOS->mID_TIPO_ACOMPANHAMENTO);
    }

    public static function FabricaControllerPeloAcompanhamento($pID_TIPO_ACOMPANHAMENTO) {
        switch ($pID_TIPO_ACOMPANHAMENTO) {
            case 1:
                $processo = new cCTRL_PROCESSO_EDIT_OS_MTE();
                break;
            case 3:
                $processo = new cCTRL_PROCESSO_EDIT_OS_PRORROG();
                break;
            case 4:
                $processo = new cCTRL_PROCESSO_EDIT_OS_REGCIE();
                break;
            case 5:
                $processo = new cCTRL_PROCESSO_EDIT_OS_EMISCIE();
                break;
            case 6:
                $processo = new cCTRL_PROCESSO_EDIT_OS_CANCEL();
                break;
            case 8:
                $processo = new cCTRL_PROCESSO_EDIT_OS_COLETA();
                break;
            case 9:
                $processo = new cCTRL_PROCESSO_EDIT_OS_CPF();
                break;
            case 10:
                $processo = new cCTRL_PROCESSO_EDIT_OS_CNH();
                break;
            case 11:
                $processo = new cCTRL_PROCESSO_EDIT_OS_CTPS();
                break;
            case 12:
                $processo = new cCTRL_PROCESSO_EDIT_OS_TIMESHEET();
                break;
            case 13:
                $processo = new cCTRL_PROCESSO_EDIT_OS_MUDEMB();
                break;
//			case nn:
//				$processo = new cCTRL_PROCESSO_EDIT_OS_COLETA_CIE();
//				break;
            default:
                $processo = new cCTRL_PROCESSO_EDIT_OS_GENERICO();
                break;
        }
        return $processo;
    }

    public function TemplatesVisto($pvisto) {
        $templates = array();
        foreach ($pvisto['processos'] as $processo) {
            switch ($processo->mnomeTabela) {
                case "mte":
                    $tmpl = new cTMPL_PROCESSO_MTE_EDIT();
                    break;
                case "coleta":
                    $tmpl = new cTMPL_PROCESSO_COLETA_EDIT();
                    break;
                case "regcie":
                    $tmpl = new cTMPL_PROCESSO_REGCIE_EDIT();
                    break;
                case "emiscie":
                    $tmpl = new cTMPL_PROCESSO_EMISCIE_EDIT();
                    break;
                case "prorrog":
                    $tmpl = new cTMPL_PROCESSO_PRORROG_EDIT();
                    break;
                case "cancel":
                    $tmpl = new cTMPL_PROCESSO_CANCEL_EDIT();
                    break;
            }

            if (is_object($tmpl)) {
                $tmpl->CarregueDoObjeto($processo);
                $servico = new cSERVICO();
                $servico->mNU_SERVICO = $processo->mnu_servico;
                $servico->RecuperePeloId();
                $tmpl->set_titulo($servico->mNO_SERVICO_RESUMIDO);
                $api = new cTEMPLATE_API();
                $api->CarregaEdicao($tmpl);
                $templates[] = $api->get_data();
            }
        }
        $tmpl = new cTMPL_PROCESSO_MTE_EDIT();
        $tmpl->CarregueDoObjeto($visto['autorizacao']);
        $servico = new cSERVICO();
        $servico->mNU_SERVICO = $processo->mnu_servico;
        $servico->RecuperePeloId();
        $tmpl->set_titulo($servico->mNO_SERVICO_RESUMIDO);
        $api = new cTEMPLATE_API();
        $api->CarregaEdicao($tmpl);
        $templates[] = $api->get_data();
        return $templates;
    }

    public function TemplatesVistoCliente($pvisto) {
        $templates = array();
        foreach ($pvisto['processos'] as $processo) {
            if ($processo->mnu_servico > 0) {
                $servico = new cSERVICO();
                $servico->mNU_SERVICO = $processo->mnu_servico;
                $servico->RecuperePeloId();
            }

            switch ($processo->mnomeTabela) {
                case "mte":
                    $tmpl = new cTMPL_PROCESSO_MTE_CLIE_EDIT();
                    $tmpl->set_titulo($servico->mNO_SERVICO_RESUMIDO);
                    break;
                case "coleta":
                    $tmpl = new cTMPL_PROCESSO_COLETA_CLIE_EDIT();
                    $tmpl->set_titulo('Coleta de visto');
                    break;
                case "regcie":
                    $tmpl = new cTMPL_PROCESSO_REGCIE_CLIE_EDIT();
                    $tmpl->set_titulo($servico->mNO_SERVICO_RESUMIDO);
                    break;
                case "emiscie":
                    $tmpl = new cTMPL_PROCESSO_EMISCIE_CLIE_EDIT();
                    $tmpl->set_titulo($servico->mNO_SERVICO_RESUMIDO);
                    break;
                case "prorrog":
                    $tmpl = new cTMPL_PROCESSO_PRORROG_CLIE_EDIT();
                    $tmpl->set_titulo($servico->mNO_SERVICO_RESUMIDO);
                    break;
                case "cancel":
                    $tmpl = new cTMPL_PROCESSO_CANCEL_CLIE_EDIT();
                    $tmpl->set_titulo($servico->mNO_SERVICO_RESUMIDO);
                    break;
            }

            if (is_object($tmpl)) {
                $tmpl->CarregueDoObjeto($processo);
                $api = new cTEMPLATE_API();
                $api->CarregaEdicao($tmpl);
                $templates[] = $api->get_data();
            }
        }
        $tmpl = new cTMPL_PROCESSO_MTE_CLIE_EDIT();
        $processo = $pvisto['autorizacao'];
        $tmpl->CarregueDoObjeto($processo);
        if ($processo->mnu_servico > 0) {
            $servico = new cSERVICO();
            $servico->mNU_SERVICO = $processo->mnu_servico;
            $servico->RecuperePeloId();
            $tmpl->set_titulo($servico->mNO_SERVICO_RESUMIDO);
        } else {
            $tmpl->set_titulo('Autorização');
        }
        $api = new cTEMPLATE_API();
        $api->CarregaEdicao($tmpl);
        $templates[] = $api->get_data();
        return $templates;
    }

    public function QtdProtocolosDoDia() {
        $dia = cHTTP::ParametroValido('dt_envio_bsb');

        $sql = "select count(*) from processo_mte where dt_envio_bsb = " . cBANCO::DataOk($dia);
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        return $rs[0];
    }

    public function reordenarProcessos(){
        $ordem = $_GET['ordem'];
        $x = explode(",",$ordem);
        $processos =array_reverse($x);
        $novaOrdem = 10;
        foreach($processos as $processo){
            $nome_codigo = explode(":", $processo);
            $nome_processo = explode("_", $nome_codigo[0]);
            $proc = cprocesso::FabricaProcessoPeloHistorico($nome_codigo[1], $nome_processo[0]);
            $proc->atualizaOrdem($novaOrdem);
            $novaOrdem+=10;
        }
        
    }
    
}
