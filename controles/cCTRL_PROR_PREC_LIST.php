<?php
class cCTRL_PROR_PREC_LIST extends cCTRL_MODELO_LIST{
	public function __construct()
	{

	}

	public function InicializeModelo()
	{
		$this->modelo = new cORDEMSERVICO();
	}

	public function InicializeTemplate()
	{
		cHTTP::$comCabecalhoFixo = true;
		$this->template = new cTMPL_PROR_PREC_LIST();
	}

	public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl)
	{
		if(!$this->template->mFoiPostado){
			if(cBANCO::WhereFiltros($this->template->mFiltro) == ''){
				$this->template->mFiltro['nu_pre_cadastro_preenchido']->mValor = "0";
				$this->template->mFiltro['ID_STATUS_SOL']->mValor = "5";
				$hoje =  new DateTime('NOW');
				$hoje->sub(new DateInterval('P7D'));
				$this->template->mFiltro['dt_solicitacao_ini']->mValor = $hoje->format('d/m/Y');
			}
		}

		if($this->template->getValorFiltro('dt_solicitacao_ini') != ''){
			$this->template->mFiltro['dt_solicitacao_ini']->mWhere = ' s.dt_solicitacao >= '.cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_ini'));
//			$this->mFiltro['dt_solicitacao_ini']->mWhere = ' 1=1 ';
		}
		if($this->template->getValorFiltro('dt_solicitacao_fim') != ''){
			$this->template->mFiltro['dt_solicitacao_fim']->mWhere = ' s.dt_solicitacao <= '.cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_fim'));
//			$this->mFiltro['dt_solicitacao_fim']->mWhere = ' 1=1 ';
		}

		if($this->template->getValorFiltro('dt_requerimento_ini') != ''){
			$this->template->mFiltro['dt_requerimento_ini']->mWhere = ' dt_requerimento >= '.cBANCO::DataOk($this->template->getValorFiltro('dt_requerimento_ini'));
//			$this->mFiltro['dt_requerimento_ini']->mWhere = ' 1=1 ';
		}
		if($this->template->getValorFiltro('dt_requerimento_fim') != ''){
			$this->template->mFiltro['dt_requerimento_fim']->mWhere = ' dt_requerimento <= '.cBANCO::DataOk($this->template->getValorFiltro('dt_requerimento_fim'));
//			$this->mFiltro['dt_solicitacao_fim']->mWhere = ' 1=1 ';
		}
		if($this->template->getValorFiltro('nu_pre_cadastro_preenchido') == '1'){
			$this->template->mFiltro['nu_pre_cadastro_preenchido']->mWhere = ' p.nu_pre_cadastro is not null ';
//			$this->mFiltro['dt_solicitacao_fim']->mWhere = ' 1=1 ';
		}
		elseif($this->template->getValorFiltro('nu_pre_cadastro_preenchido') == '0'){
			$this->template->mFiltro['nu_pre_cadastro_preenchido']->mWhere = " coalesce(trim(p.nu_pre_cadastro), '') ='' ";
//			$this->mFiltro['dt_solicitacao_fim']->mWhere = ' 1=1 ';
		}

		$cursor = $this->modelo->CursorProrrogacoesPreCadastro($this->template->mFiltro, $this->template->get_ordenacao());
		return $cursor;
	}
}
