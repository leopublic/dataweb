<?php
/**
 * Controller de montagem do protocolo de BSB
 */
class cCTRL_OS_PBSB_PROTOCOLO_EXCEL extends cCTRL_OS_PBSB_PROTOCOLO{
	public function __construct(){
		cHTTP::$MIME = cHTTP::mmXLS;
	}

	public function InicializeTemplate() {
		parent::InicializeTemplate();
		$this->template->EscondaCampo('acao');
		$this->template->exibirPaginacao = false;
		cHTTP::$POST['CMP__offset'] = 0;
		foreach($this->template->mCampo as &$campo){
			$campo->mOrdenavel = false;
			$campo->mReadonly = true;
		}

	}
	public function configura_paginacao(){
		cHTTP::$POST['CMP__offset'] = '0';
		$this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS, 0);
    }

}
