<?php 
class cCTRL_OS_ACOMP_LIST extends cCTRL_MODELO_LIST{
	public function __construct(){
	}
	public function InicializeModelo(){
		$this->modelo = new cORDEMSERVICO();
	}
	
	public function InicializeTemplate(){
		cHTTP::$comCabecalhoFixo = true;
		$this->template = new cTMPL_OS_ACOMP_LIST();
	}

	public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl){
		// Coloca o filtro default caso nenhum tenha sido informado
		if($this->template->getValorFiltro('nu_empresa') == ''
		&& $this->template->getValorFiltro('ID_STATUS_SOL') == ''		
		&& $this->template->getValorFiltro('nu_solicitacao') == ''
		&& $this->template->getValorFiltro('nome_completo') == ''
		&& $this->template->getValorFiltro('dt_solicitacao_ini') == ''
		&& $this->template->getValorFiltro('dt_solicitacao_fim') == ''
		){
			$hoje = new DateTime(date('Y-m-d'));
			$menos7 = new DateTime(date('Y-m-d'));
			$menos7->sub(new DateInterval('P7D'));

			$this->template->setValorFiltro('dt_solicitacao_ini', $menos7->format('d/m/Y')); 
			$this->template->setValorFiltro('dt_solicitacao_fim', $hoje->format('d/m/Y')); 
		}
		
		if($this->template->getValorFiltro('dt_solicitacao_ini') != ''){
			$this->template->mFiltro['dt_solicitacao_ini']->mWhere = "dt_solicitacao_orig >=".cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_ini'));
		}

		if($this->template->getValorFiltro('dt_solicitacao_fim') != ''){
			$this->template->mFiltro['dt_solicitacao_fim']->mWhere = "dt_solicitacao_orig <=".cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_fim'));
		}
		
		$cursor = $this->modelo->CursorAcompanhamento($this->template->mFiltro, $this->template->get_ordenacao());
		return $cursor;
	}

	public function ProcessePost(){
		if($this->template->get_acaoPostada() == 'ADICIONAR'){
			$dep = new cdependente();
			$this->template->AtribuaAoOBJETO($dep);
		}
	}
}
