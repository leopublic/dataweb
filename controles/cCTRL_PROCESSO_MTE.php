<?php
class cCTRL_PROCESSO_MTE extends cCTRL_PROCESSO{
	/**
	 *
	 * @param cORDEMSERVICO $pOS Recebendo a OS como objeto para otimizar o acesso ao banco
	 * @return string O formulário para edição em HTML
	 */

	public static function ExibirProcessoNoVisto($pcodigo = ""){
		$tela = new cTEMPLATE_PROCESSO_MTE_VIEW();
		$tela->CarregueDoHTTP();
		$processo = new cprocesso_mte($tela->mCampo['codigo']->mValor);
		$tela->CarregueDoOBJETO($processo);
		$tela->mTitulo = "Processo MTE";
		if ($processo->mfl_visto_atual){
			$tela->mAcoes['TORNAR_ATUAL']->mVisivel = false;
		}
		$tela->mAcoes['ALTERAR']->mParametros = 'codigo='.$processo->mcodigo;
		if (intval($processo->mid_solicita_visto) > 0 ){
			$tela->mTitulo .= ' (<a href="/operador/os_Processo.php?ID_SOLICITA_VISTO='.$processo->mid_solicita_visto.'&NU_CANDIDATO='.$processo->mcd_candidato.'">OS '.$processo->mcd_solicitacao.'</a>)';
		}
		$ret = cINTERFACE::RenderizeTemplateComAcoesNoCabecalho($tela);
		return $ret;
	}

	public static function Editar(){
		$tela = new cTEMPLATE_PROCESSO_MTE_EDIT_VISTO();
		$tela->CarregueDoHTTP();
		$processo = new cprocesso_mte($tela->mCampo['codigo']->mValor);
		//
		// Ajusta o form de acordo com as características do processo
		if ($processo->mfl_visto_atual){
			$tela->mAcoes['TORNAR_ATUAL']->mVisivel = false;
		}

		$Titulo = "Alterar Processo MTE";
		if ($processo->mid_solicita_visto == '' ){
			$Titulo .= ' (avulso)';
			$tela->mCampo['NO_SERVICO_RESUMIDO']->mVisivel = false;
		}
		else{
			if (intval($processo->mid_solicita_visto) > 0 ){
				$Titulo .= ' (OS '.$processo->mcd_solicitacao.')';
				$tela->mCampo['nu_servico']->mTipo = cCAMPO::cpHIDDEN;
				$tela->mCampo['nu_servico']->mValor = $processo->mnu_servico;
				$tela->mCampo['NO_SERVICO_RESUMIDO']->mVisivel = true;
			}
			else{
				$Titulo .= ' (OS 0)';
				$tela->mCampo['NO_SERVICO_RESUMIDO']->mVisivel = false;
			}
		}
		$tela->mCampo[cTEMPLATE::CMP_TITULO]->mValor = $Titulo;
		//
		// Trata o post
		if ($tela->mFoiPostado){
			switch ($tela->mAcaoPostada){
				case 'SALVAR':
					try{
						$tela->mCampo['NO_SERVICO_RESUMIDO']->mValor = $processo->mNO_SERVICO_RESUMIDO;
						$tela->AtribuaAoOBJETO($processo);
						$processo->Salve_ProcessoEdit();
						$msg =  "Processo salvo com sucesso!";
					}
					catch(Exception $e){
						$msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
					}
					break;
				case 'SALVAR_E_FECHAR':
					try{
						$tela->mCampo['NO_SERVICO_RESUMIDO']->mValor = $processo->mNO_SERVICO_RESUMIDO;
						$tela->AtribuaAoOBJETO($processo);
						$processo->Salve_ProcessoEdit();
						$OS = new cORDEMSERVICO();
						$OS->mID_SOLICITA_VISTO = $processo->mid_solicita_visto;
						$OS->Fechar();
						$msg =  "Processo salvo com sucesso! OS fechada com sucesso!";
					}
					catch(Exception $e){
						$msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
					}
					break;
				default:
					$msg = "Operação não encontrada (".$tmpl->mAcaoPostada.")";
					break;
			}
		}
		else{
			if (intval($processo->mid_solicita_visto) > 0 ){
				$msg = "Os campos que são originários da ordem de serviço estão protegidos.";
			}
			$tela->CarregueDoOBJETO($processo);
		}
		//
		// Habilita as ações de acordo com o status do processo.
		if ($processo->mID_STATUS_SOL != "6"){
			$tela->mAcoes['SALVAR_E_FECHAR']->mVisivel = true;
		}
		else{
			$tela->mAcoes['SALVAR_E_FECHAR']->mVisivel = false;
		}
		$tela->mCampo[cTEMPLATE::CMP_MSG]->mValor = $msg;

		//
		// Renderiza o formulário
		$ret = cINTERFACE::RenderizeTemplateComAcoesNoRodape($tela);
		return $ret;
	}

	public function log(){
		$codigo = $_GET['codigo'];
		$processo = new cprocesso_mte();
		$processo->mcodigo = $codigo;
		$processo->RecupereSe();
		if ($processo->mid_solicita_visto != ''){
			$os = new cORDEMSERVICO();
			$os->mID_SOLICITA_VISTO = $processo->mid_solicita_visto;
			$os->Recuperar();
		} else {
			$os = null;
		}
		$candidato = new cCANDIDATO();
		$candidato->Recuperar($processo->mcd_candidato);
		$cursor = cprocesso_mte::cursorLog($codigo);
		$regs = $cursor->fetchAll();
		$menu = cLAYOUT::Menu();
		$data = array(
				'regs' => $regs,
				'menu' => $menu,
				'processo' => $processo,
				'os' => $os,
				'candidato' => $candidato
		);
		return cTWIG::Render('intranet/processo/mte_log.html', $data);
	
	}
	
}
