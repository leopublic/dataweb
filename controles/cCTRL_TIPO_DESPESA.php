<?php
/**
 */
class cCTRL_TIPO_DESPESA extends cCTRL_MODELO{
	public function getListar() {
		$regs = ctipo_despesa::cursorTodos();
		return $this->renderListar($regs);
	}
	
	public function postListar(){
		$filtros = array();
		$regs = ctipo_despesa::cursorTodos($filtros);
		return $this->renderListar($regs);
	}
	
	public function renderListar($regs){
		$menu = cLAYOUT::Menu();
		$data = array(
				'regs' => $regs,
				'menu' => $menu,
		);
		return cTWIG::Render('intranet/cadastros/tipo_despesa_list.html', $data);
	}
}
