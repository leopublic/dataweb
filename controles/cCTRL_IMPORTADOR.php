<?php
/**
 * Layout de importação
 * Colunas em ordem
 * CANDIDATO
 * A (0) nome empresa
 * B (1) nome embarcação projeto
 * C (2) nome candidato
 * D (3) nome profissão
 * E (4) nome função
 * F (5) email
 * G (6) telefone
 * H (7) nome da nacionalidade
 * I (8) número passaporte
 * J (9) data emissão do passaporte (dd-mmm-aa)
 * K (10) data de validade do passaporte (dd-mmm-aa)
 * L (11) nome do pais de emissao passaporte
 * M (12) reservado
 * N (13) reservado
 * O (14) reservado
 * P (15) reservado
 * Q (16) reservado
 *
 * Processo MTE
 * R (17) nome do serviço
 * S (18) nome da repartição consular
 * T (19) número do processo
 * U (20) data do requerimento
 * V (21) data conclusão
 * W (22) status conclusão
 * X (23) reservado
 *
 * Processo REGISTRO
 * Y (24) data de requerimento (dd-mmm-aa)
 * Z (25) número do protocolo
 * AA (26) data do prazo de estada (dd-mmm-aa)
 * AB (27) reservado
 *
 * Processo COLETA DE VISTO
 * AC (28) data de emissão (dd-mmm-aa)
 * AD (29) data de entrada (dd-mmm-aa)
 * AE (30) reservado
 *
 * Processo COLETA DE CIE
 * AF (31) data de expedição (dd-mmm-aa)
 * AG (32) reservado
 *
 * Processo PRORROGAÇÃO
 * AH (33) número do protocolo
 * AI (34) reservado
 */
class cCTRL_IMPORTADOR extends cCTRL_MODELO{

    public $cand_nome_empresa = 0;
    public $cand_nome_embarcacao_projeto = 1;
    public $cand_nome_candidato = 2;
    public $cand_nome_profissao = 3;
    public $cand_nome_funcao = 4;
    public $cand_email= 5;
    public $cand_telefone = 6;
    public $cand_nome_da_nacionalidade = 7;
    public $cand_nu_passaporte = 8;
    public $cand_dt_emissao_passaporte = 9;
    public $cand_dt_validade_passaporte = 10;
    public $cand_nome_do_pais_de_emissao_passaporte = 11;
    //public $cand_reservado = 12;
    //public $cand_reservado = 13;
    //public $cand_reservado = 14;
    //public $cand_reservado = 15;
    //public $cand_reservado = 16;
    public $mte_nome_do_servico = 17;
    public $mte_nome_da_reparticao_consular = 18;
    public $mte_nu_processo = 19;
    public $mte_dt_requerimento = 20;
    public $mte_dt_conclusao = 21;
    public $mte_status_conclusao = 22;
    // public $mte_reservado = 23;
    public $regcie_dt_requerimento = 24;
    public $regcie_nu_protocolo = 25;
    public $regcie_dt_prazo_estada = 26;
    public $regcie_reservado = 27;
    public $coleta_visto_dt_emissao = 28;
    public $coleta_visto_dt_entrada = 29;
    //public $coleta_visto_reservado = 30;
    public $coleta_cie_dt_expedicao = 31;
    //public $coleta_cie_reservado = 32;
    public $prorrog_nu_protocolo = 33;
    //public $prorrog_reservado = 34;

    public function getNovoarquivo(){

    }

    public function postNovoarquivo(){

    }

    public function getPreviaimportacao(){
        return cTWIG::Render('intranet/candidato/enviar_convite.twig', $this->data);
    }

    public function importar() {
        global $myinstdir;
        $nome_arquivo = $_GET['nome_arquivo'];
        $linha_inicial = $_GET['linha_inicial'];
        $dir = $myinstdir."/arquivos/".$nome_arquivo;
        print $dir;
        $i = 1;
        $handle = @fopen($dir, "r");
        if ($handle) {
            while (!feof($handle)) {
                $buffer = fgets($handle);
                if ($i >= $linha_inicial){
                    print "<br/>Importando linha ".$i."==================================";
                    //Processa importação
                    $ret = $this->importaRegistro($buffer);
                    print "<br/>".$ret;
                } else {
                    print "<br/>Linha ".$i." ignorada";
                }
                $i++;
            }
            fclose($handle);
        }
    }

    public function importaRegistro($reg){
        $cols = explode("\t", $reg);
        $msg = "";
        $rep = new cREPOSITORIO_IMPORTACAO();
        // Consiste preenchimentos obrigatorios

        // Recupera empresa
        $emp = $rep->recupera_empresa($cols[$this->cand_nome_empresa]);
        // Recupera embarcação projeto
        $embp = $rep->recupera_embarcacao($emp->mNU_EMPRESA, $cols[$this->cand_nome_embarcacao_projeto]);
        // Recupera profissao
        if ($embp->mNU_EMBARCACAO_PROJETO == ''){
            print "<br> ==> Embarcação não encontrada:".$cols[$this->cand_nome_embarcacao_projeto];
        }
        $profissao = $rep->recupera_profissao(trim($cols[$this->cand_nome_profissao]));
        if ($profissao->mCO_PROFISSAO == ''){
            print "<br> ==> Profissão não encontrada:".$cols[$this->cand_nome_profissao];
        }
        // var_dump($profissao);
        // Recupera funcao
        $funcao = $rep->recupera_funcao(trim($cols[$this->cand_nome_funcao]));
        if ($funcao->mCO_FUNCAO == ''){
            print "<br> ==> Função não encontrada:".$cols[$this->cand_nome_funcao];
        }
        // Recupera nacionalidade
        // var_dump($funcao);
        $nacionalidade = $rep->recupera_pais_nacionalidade(trim($cols[$this->cand_nome_da_nacionalidade]));
        if ($nacionalidade->mCO_PAIS == ''){
            print "<br> ==> Nacionalidade não encontrada:".$cols[$this->cand_nome_da_nacionalidade];
        }
        // Recupera pais passaporte
        // var_dump($nacionalidade);

        $pais_passaporte = $rep->recupera_pais_nacionalidade(trim($cols[$this->cand_nome_do_pais_de_emissao_passaporte]));
        // Recupera servico
        $servico = $rep->recupera_servico($cols[$this->mte_nome_do_servico]);
        if ($servico->mNU_SERVICO == ''){
            print "<br> ==> Serviço não encontrado:".$cols[$this->mte_nome_do_servico];
        }
        // var_dump($servico);
        // Recupera servico
        $status = $rep->recupera_status($cols[$this->mte_status_conclusao]);
        if ($status->mid_status_conclusao == ''){
            print "<br> ==> Status não encontrada:".$cols[$this->mte_status_conclusao];
        }
        // var_dump($status);
        // Recupera servico
        $rep_cons = $rep->recupera_reparticao_consular($cols[$this->mte_nome_da_reparticao_consular]);
        if ($rep_cons->mCO_REPARTICAO_CONSULAR == ''){
            print "<br> ==> Reparticao não encontrada:".$cols[$this->mte_nome_da_reparticao_consular];
        }
        // var_dump($rep_cons);
        // Recupera candidato pelo nome/empresa
        $cand = $rep->recuperar_candidato($cols[$this->cand_nome_candidato], $emp->mNU_EMPRESA, true);
        // Atualiza dados do candidato
        $valores = array(
            'mNU_EMPRESA' => $emp->mNU_EMPRESA,
            'mNU_EMBARCACAO_PROJETO' => $embp->mNU_EMBARCACAO_PROJETO,
            'mCO_FUNCAO' => $funcao->mCO_FUNCAO,
            'mCO_NACIONALIDADE' => $nacionalidade->mCO_PAIS,
            'mNO_EMAIL_CANDIDATO' => $cols[$this->cand_email],
            'mNU_TELEFONE_CANDIDATO' => $cols[$this->cand_telefone],
            'mNU_PASSAPORTE' => $cols[$this->cand_nu_passaporte],
            'mDT_EMISSAO_PASSAPORTE' => $this->formataData($cols[$this->cand_dt_emissao_passaporte]),
            'mDT_VALIDADE_PASSAPORTE' => $this->formataData($cols[$this->cand_dt_validade_passaporte]),
            'mCO_PAIS_EMISSOR_PASSAPORTE' => $pais_passaporte->mCO_PAIS,
        );
        $rep->atualiza_candidato($cand, $valores);

        // Cria o processo MTE
        $valores = array(
            'mnu_servico' => $servico->mNU_SERVICO,
            'mcd_reparticao' => $rep_cons->mCO_REPARTICAO_CONSULAR,
            'mcd_funcao' => $funcao->mCO_FUNCAO,
            'mnu_processo' => $cols[$this->mte_nu_processo],
            'mdt_requerimento' => $this->formataData($cols[$this->mte_dt_requerimento]),
            'mdt_deferimento' => $this->formataData($cols[$this->mte_dt_conclusao]),
            'mID_STATUS_CONCLUSAO' => $status->mid_status_conclusao,
            'mobservacao' => 'Processo importado a partir de planilha em '.date('d/m/Y H:i:s'),
            'mobservacao_visto' => 'Processo importado a partir de planilha em '.date('d/m/Y H:i:s'),
        );
        $mte = $rep->cria_processo_mte($cand, $valores);

        $cand->mcodigo_processo_mte_atual = $mte->mcodigo;
        $cand->salveTodosAtributos();
        // Cria o REGISTRO
        $valores = array(
            'mdt_requerimento' => $this->formataData($cols[$this->regcie_dt_requerimento]),
            'mnu_protocolo' => $cols[$this->regcie_nu_protocolo],
            'mdt_prazo_estada' => $this->formataData($cols[$this->regcie_dt_prazo_estada]),
            'mobservacao' => 'Processo importado a partir de planilha em '.date('d/m/Y H:i:s'),
        );
        $rep->cria_registro($cand, $valores);
        //
        // Cria a coleta de VISTO
        $valores = array(
            'mdt_emissao_visto' => $this->formataData($cols[$this->coleta_visto_dt_emissao]),
            'mdt_entrada' => $this->formataData($cols[$this->coleta_visto_dt_entrada]),
            'mobservacao' => 'Processo importado a partir de planilha em '.date('d/m/Y H:i:s')
        );
        $rep->cria_coleta_visto($cand, $valores);
        // Cria a coleta de CIE
        $valores = array(
            'mdt_emissao' => $this->formataData($cols[$this->coleta_cie_dt_expedicao]),
            'mobservacao' => 'Processo importado a partir de planilha em '.date('d/m/Y H:i:s')
        );
        $rep->cria_coleta_cie($cand, $valores);
        //
        // Cria a prorrogação
        $valores = array(
            'mnu_protocolo' => $cols[$this->prorrog_nu_protocolo],
            'mobservacao' => 'Processo importado a partir de planilha em '.date('d/m/Y H:i:s')
        );
        $rep->cria_prorrogacao($cand, $valores);
        return 'Candidato '.$cols[$this->cand_nome_candidato].' importado com sucesso ID='.$cand->mNU_CANDIDATO;
    }

    public function formataData($campo){
        $meses = array('jan' =>1, 'fev'=>2, 'feb'=>2, 'mar'=>3, 'abr'=>4, 'apr'=>4, 'abril'=>4,'abri'=>4, 'mai'=>5,'may'=>5, 'jun'=>6, 'jul'=>7, 'ago'=>8, 'aug'=>8, 'set'=>9, 'sep'=>9, 'out'=>10, 'oct'=>10, 'nov'=>11, 'dez'=>12, 'dec'=>12);
        if (trim($campo) != '' && trim($campo) != '-'){
            if (strstr($campo, '-')){
                $partes = explode('-', $campo);
            } else {
                $partes = explode('/', $campo);
            }
            if (strlen($partes[0]) == 4){
                // Layout aaaa-mm-dd
                return $campo;
            } else {
                $partes[0] = trim($partes[0]);
                $partes[1] = trim($partes[1]);
                $partes[2] = trim($partes[2]);
                if (!is_numeric($partes[1])){
                    $xx = $partes[1];
                    $partes[1] = $meses[strtolower(trim($partes[1]))];
                    if ($partes[1] == ''){
                        throw new exception('Mês '.$xx.' não encontrado');
                    }
                }
                if (strlen($partes[2]) <= 2){
                    $partes[2] = '20'.$partes[2];
                }
                return $partes[2].'-'.$partes[1].'-'.$partes[0];

            }

        } else {
            return '';
        }
    }
}
