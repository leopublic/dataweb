<?php

/**
 * Controller de montagem do protocolo de BSB
 */
class cCTRL_OS_PBSB_PROTOCOLO extends cCTRL_MODELO_LIST {

    public function __construct() {

    }

    public function InicializeModelo() {
        $this->modelo = new cORDEMSERVICO();
    }

    public function InicializeTemplate() {
        cHTTP::$comCabecalhoFixo = true;
        $this->template = new cTMPL_OS_PBSB_PROTOCOLO();
    }

    public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl) {
        if (!$this->template->mFoiPostado) {
            $this->template->mFiltro['id_status_sol']->mValor = 4;
        }

        if (intval($this->template->mFiltro['id_status_sol']->mValor) == 0) {
            $this->template->mFiltro['id_status_sol']->mValor = 'x';
            $this->template->mFiltro['id_status_sol']->mWhere = ' s.id_status_sol in (4,7,8,11) ';
        }
        if($this->template->getValorFiltro('dt_solicitacao_ini') != ''){
            $this->template->mFiltro['dt_solicitacao_ini']->mWhere = ' dt_solicitacao >= '.cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_ini'));
        }
        if($this->template->getValorFiltro('dt_solicitacao_fim') != ''){
            $this->template->mFiltro['dt_solicitacao_fim']->mWhere = ' dt_solicitacao <= '.cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_fim'));
        }

        if($this->template->getValorFiltro('dt_envio_bsb_ini') != ''){
            $this->template->mFiltro['dt_envio_bsb_ini']->mWhere = ' p.dt_envio_bsb >= '.cBANCO::DataOk($this->template->getValorFiltro('dt_envio_bsb_ini'));
        }
        if($this->template->getValorFiltro('dt_envio_bsb_fim') != ''){
            $this->template->mFiltro['dt_envio_bsb_fim']->mWhere = ' p.dt_envio_bsb <= '.cBANCO::DataOk($this->template->getValorFiltro('dt_envio_bsb_fim'));
        }
        $this->configura_paginacao();
        
        $cursor = $this->modelo->CursorProtocoloDoDia($this->template->mFiltro, $this->template->get_ordenacao(), $this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL), $this->template->get_offset());
        return $cursor;
    }
    public function configura_paginacao(){
        $qtdReg = $this->modelo->qtdProtocoloDoDia($this->template->mFiltro);

        $offset = $this->template->get_offset();
        if ($offset > 0) {
            $qtdPag = floor($qtdReg / $this->template->get_offset());
        } else {
            $qtdPag = 1;
        }
        $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS, $qtdPag);
    }
    public function ProcessePost() {
        if ($this->template->get_acaoPostada() == 'PRIMEIRA') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, 1);
        }
        if ($this->template->get_acaoPostada() == 'PROXIMA') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) + 1);
        }
        if ($this->template->get_acaoPostada() == 'ANTERIOR') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) - 1);
        }
        if ($this->template->get_acaoPostada() == 'ULTIMA') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS)));
        }
    }

    public function Redirecione() {

    }

}
