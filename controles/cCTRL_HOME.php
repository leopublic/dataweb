<?php
class cCTRL_HOME{
    public function home(){
        $show = $_GET['showMissao'];
        $menu = cLAYOUT::Menu();
        $rep = new cREPOSITORIO_DR;
        $cadastros_pendentes = $rep->pendentesDoUsuario(cSESSAO::$mcd_usuario);
        $data = array(
            'menu' => $menu,
            'usuario' => cSESSAO::$mnome,
            'cadastros_pendentes' => $cadastros_pendentes,
        );
        return cTWIG::Render('intranet/home.twig', $data);

    }

    public function teste(){
        return printf(DEBUG);
    }
}
