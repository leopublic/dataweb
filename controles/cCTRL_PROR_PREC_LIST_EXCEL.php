<?php 
class cCTRL_PROR_PREC_LIST_EXCEL extends cCTRL_PROR_PREC_LIST{
	public function __construct()
	{
		cHTTP::$MIME = cHTTP::mmXLS;		
	}
	public function InicializeTemplate() {
		parent::InicializeTemplate();
		foreach($this->template->mCampo as &$campo){
			$campo->mOrdenavel = false;
			$campo->mReadonly = true;
		}

	}
}
