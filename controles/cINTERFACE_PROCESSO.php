<?php

class cINTERFACE_PROCESSO {

    public $mcodigo;
    public $mtipo;
    public $mprocesso;

    const PROCESSO_MTE = 'mte';
    const PROCESSO_REGCIE = 'regcie';
    const PROCESSO_EMISCIE = 'emiscie';
    const PROCESSO_PRORROG = 'prorrog';
    const PROCESSO_CANCEL = 'cancel';
    const PROCESSO_COLETA = 'coleta';
    const PROCESSO_COLETA_CIE = 'coleta_cie';

    public static $mTitulos = array(
        'processo_mte_edit' => 'Processo MTE'
        , 'processo_prorrog_edit' => 'Prorrogação'
        , 'processo_coleta_edit' => 'Coleta de visto'
        , 'processo_regcie_edit' => 'Registro'
        , 'processo_emiscie_edit' => 'Coleta de CIE'
        , 'processo_cancel_edit' => 'Cancelamento'
        , 'processo_coleta_cie_edit' => 'Coleta CIE'
        , 'processo_mte_view' => 'Processo MTE'
        , 'processo_prorrog_view' => 'Prorrogação'
        , 'processo_coleta_view' => 'Coleta de visto'
        , 'processo_regcie_view' => 'Registro'
        , 'processo_emiscie_view' => 'Coleta de CIE'
        , 'processo_cancel_view' => 'Cancelamento'
    );

    /**
     * Retorna o nome da tela que deve ser utilizada para apresentar um processo a partir do tipo do acompanhamento dele
     * @param type $pID_TIPO_ACOMPANHAMENTO
     */
    public static function NomeTelaDoAcompanhamento($pID_TIPO_ACOMPANHAMENTO) {
        return $mTitulos[intval($pID_TIPO_ACOMPANHAMENTO)];
    }

    /**
     * Gera o nome da tela a ser utilizada para apresentar um processo
     * @param string $pTipo "mte", "prorrog", "regcie", "emiscie", "coleta" ou "cancel". Existem constantes com esses valores.
     * @param string $pModo "view" ou "edit"
     */
    public static function TelaProcesso($pTipo, $pModo) {
        switch ($pTipo) {
            case 'CANDIDATO_CPF':
            case 'CANDIDATO_CTPS':
            case 'CANDIDATO_CNH':
                $nomeTela = $pTipo;
                $tela = new cEDICAO($nomeTela);
                $tela->CarregarConfiguracao();
                break;
            case 'outros':
                $nomeTela = 'processo_' . $pModo;
                $tela = new cEDICAO($nomeTela);
                $tela->CarregarConfiguracao();
                break;
            case 'generico_13':
                $tela = new cCTRL_PROCESSO_EDIT_MUDEMB();
                break;
            default:
                $nomeTela = 'processo_' . $pTipo . '_' . $pModo;
                $tela = new cEDICAO($nomeTela);
                $tela->CarregarConfiguracao();
                break;
        }
        switch ($pTipo) {
            case 'CANDIDATO_CPF':
            case 'CANDIDATO_CTPS':
            case 'CANDIDATO_CNH':
            case 'outros':
            case 'generico_13':
                break;
            default:
                $tela->ResseteChavePara('codigo');
        }
        return $tela;
    }

    /**
     * Gera a apresentação de um processo
     * @param cEDICAO $pTela
     * @param boolen $pExibirLinkOs indica se deve apresentar o link para a OS do processo
     */
    public static function FormProcesso(&$pTela, $pExibirLinkOs = true, $pExibirAcoes = true) {
        $ret = '';

        $pTela->RecupereRegistro();
        $status_cobranca = '';
        if (Acesso::permitido(Acesso::tp_Cadastro_Candidato_Alterar_Processos)) {
            if ($pTela->mCampoNome['id_solicita_visto']->mValor != '') {
                $OS = new cORDEMSERVICO();


                try {
                    $OS->Recuperar($pTela->mCampoNome['id_solicita_visto']->mValor);
                } catch (Exception $e) {
                    
                }
                if ($pExibirLinkOs) {
                    if ($OS->mNU_SOLICITACAO > 0) {
                        $linkOS = '<a href="os_DadosOS.php?ID_SOLICITA_VISTO=' . $OS->mID_SOLICITA_VISTO . '&id_solicita_visto=' . $OS->mID_SOLICITA_VISTO . '">OS ' . $OS->mNU_SOLICITACAO . '</a>';
                    } else {
                        $linkOS = '<a href="os_Processo.php?ID_SOLICITA_VISTO=' . $OS->mID_SOLICITA_VISTO . '&id_solicita_visto=' . $OS->mID_SOLICITA_VISTO . '&NU_CANDIDATO=' . $pTela->mCampoNome['cd_candidato']->mValor . '">OS ' . $OS->mNU_SOLICITACAO . '</a>';
                    }
                } else {
                    $linkOS = 'OS ' . $OS->mNU_SOLICITACAO ;
                }
                if ($OS->mstco_id > 0){
                    $stco = new cstatus_cobranca_os();
                    $stco->mstco_id = $OS->mstco_id;
                    $stco->RecuperePeloId();
                    $status_cobranca= "&nbsp;&nbsp;<< ".strtoupper($stco->mstco_tx_nome)." >>";
                } else {
                    $status_cobranca= "&nbsp;&nbsp;<< indefinido >>";
                }
            } else {
                $linkOS = '&nbsp;&nbsp;--&nbsp;&nbsp;';
                $status_cobranca = '';
            }
        } else {
            $linkOS = '&nbsp;&nbsp;--&nbsp;&nbsp;';
            $stco = new cstatus_cobranca_os();
        }
        $titulo = self::$mTitulos[$pTela->mno_edicao];

        // Especial para prorrogações
        if ($titulo == 'Prorrogação') {
            if ($pTela->mCampoNome['id_solicita_visto']->mValor != '') {
                $pTela->mCampoNome['no_status_conclusao']->mVisivel = false;
                $pTela->mCampoNome['no_status_sol_res']->mVisivel = true;

                $pTela->mCampoNome['ID_STATUS_CONCLUSAO']->mVisivel = false;
                $pTela->mCampoNome['ID_STATUS_SOL']->mVisivel = true;
            } else {
                $pTela->mCampoNome['no_status_conclusao']->mVisivel = true;
                $pTela->mCampoNome['no_status_sol_res']->mVisivel = false;

                $pTela->mCampoNome['ID_STATUS_CONCLUSAO']->mVisivel = true;
                $pTela->mCampoNome['ID_STATUS_SOL']->mVisivel = false;
            }
        }

        // Especial para regcie
        if ($titulo == 'Coleta de CIE') {
            $titulo = $OS->mNO_SERVICO_RESUMIDO;
        }
        if ($titulo == 'Registro') {
            $pTela->mCampoNome['nu_servico']->mCombo->mExtra = " where ID_TIPO_ACOMPANHAMENTO = 4";
            if ($pTela->mCampoNome['ID_TIPO_ACOMPANHAMENTO']->mValor == '4') {
                $titulo = $pTela->mCampoNome['NO_SERVICO_RESUMIDO']->mValor;
            } else {
                $titulo = "Registro (tipo de serviço não definido)";
            }
        }
        if ($pTela->mCampoNome['nu_servico']->mValor == '22') {
            $titulo = 'Coleta CIE';
            $pTela->mCampoNome['nu_protocolo']->mClasse = '';
        }
        if ($pTela->mCampoNome['nu_servico']->mValor == '27') {
            $titulo = 'Arquivamento';
        }

        if ($pTela->mCampoNome['id_solicita_visto']->mValor == '') {
            $pTela->mCampoNome['NO_SERVICO_RESUMIDO']->mVisivel = false;
//			$pTela->mCampoNome['nu_servico']->mTipo = cCAMPO::cpCHAVE_ESTR;
        } elseif ($pTela->mCampoNome['NU_SERVICO_SV']->mValor == '') {
            $pTela->mCampoNome['NO_SERVICO_RESUMIDO']->mVisivel = false;
//			$pTela->mCampoNome['nu_servico']->mTipo = cCAMPO::cpCHAVE_ESTR;
        } else {
            $pTela->mCampoNome['NO_SERVICO_RESUMIDO']->mVisivel = true;
            $pTela->mCampoNome['nu_servico']->mTipo = cCAMPO::cpHIDDEN;
        }
        // Especial para cancelamentos
        if ($pTela->mno_edicao == 'processo_cancel_edit') {
            $pTela->mCampoNome['codigo_processo_mte']->mCombo->mExtra = ' where cd_candidato = ' . $pTela->mCampoNome['cd_candidato']->mValor;
        }

        // Especial para mte
        $pTela->mCampoNome['__altura']->mValor = '300';
        $ehProcessoMte = false;
        if ($titulo == "Processo MTE") {
            //$pTela->mCampoNome['fl_visto_atual']->mVisivel = true;
            $pTela->mCampoNome['nu_servico']->mVisivel = true;
            $pTela->mCampoNome['nu_servico']->mCombo->mExtra = " WHERE ID_TIPO_ACOMPANHAMENTO = 1";
            $pTela->mCampoNome['__altura']->mValor = '500';
            $ehProcessoMte = true;
            if ($pTela->mCampoNome['nu_servico']->mValor == '31') {
                $titulo = "Mudança de embarcação";
            }
        }
        $pTela->mCampoNome['__largura']->mValor = '800';

        if ($titulo == '') {
            $titulo = $OS->mNO_SERVICO;
        }
        $titulo = $linkOS. " " . $titulo." ".$status_cobranca;

        //$pTela->mCampoNome['__titulo']->mValor = $titulo;
        //$pTela->mCampoNome['__titulo']->mValor = 'Processos';

        $ret .= cINTERFACE::formEdicao($pTela);
        $acoesHTML = '';
        if ($pExibirAcoes && Acesso::permitido(Acesso::tp_Cadastro_Candidato_Alterar_Processos)) {
            $acoes = self::Acoes($pTela);
            if ($ehProcessoMte) {
                // Desabilita a adição de coleta se já houver alguma
                $processo_mte = new cprocesso_mte($pTela->mCampoNome['codigo']->mValor);
                if ($processo_mte->QtdColetas() > 0) {

                    $acoes['ADICIONAR_COLETA']->mOnClick = "jAlert('Esse visto já possui uma coleta de visto. Não será permitido adicionar outra. Altere os dados da coleta existente ou retire a coleta existente do visto para adicionar uma outra.', 'O Visto já possui uma coleta!!');";
                }
            }
            $acoesHTML = cINTERFACE::RenderizeBarraAcoesTitulo($acoes);
        }

        $estiloTitulo = '';
        if ($stco->mstco_id == "1"  || $stco->mstco_id == "2" ){
            $estiloTitulo = 'titulopendente';
        } elseif ($stco->mstco_id == "3"){
            $estiloTitulo = 'titulofaturada';
        } else {
            $estiloTitulo = "";
        }
        $ret .= '<div class="ui-widget tituloProcesso '. $estiloTitulo .'">' . $titulo . $acoesHTML . '</div>';


        $estilo = '';

        if ($pTela->mno_edicao == 'processo_mte_view' && $pTela->mCampoNome['nu_servico']->mValor != '31' && $pTela->mCampoNome['nu_servico']->mValor != '49') {
            $estilo = "atual";
        } else {
            if (substr($pTela->mno_edicao, 0, -4) == 'view') {
                $estilo = "subprocesso";
            }
        }
        $ret .= '<div class="detalheProcesso ' . $estilo . '">';
        $ret .= cINTERFACE::RenderizeEdicaoComPaineis($pTela, 2);
        $ret .= '</table>';
        $ret .= '</div>';
        $ret .= '</form>';

        return $ret;
    }

    public static function FormProcessoEdit($pTela) {
        return self::FormProcesso($pTela, false);
    }

    public static function FormProcessoView($pTela) {
        return self::FormProcesso($pTela, true);
    }

    /**
     * Monta um array com as ações que devem estar disponíveis na tela.
     * @param cEDICAO $pTela
     * @return array() Array de cACAO() da tela informada
     */
    public static function Acoes($pTela) {
        $acoes = array();
        switch ($pTela->mno_edicao) {
            case "processo_mte_view":
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Alterar", "ALTERAR", "Alterar os dados do visto", "");
                $acao->mOnClick = "carregaFormPopup('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=mte&modo=edit');";
                $acoes['ALTERAR'] = $acao;
                if ($pTela->mCampoNome['nu_servico']->mValor != '31' && $pTela->mCampoNome['nu_servico']->mValor != '49') {
                    $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Coleta", "ADICIONAR", "Adicionar uma coleta a esse visto", "");
                    $acao->mOnClick = "carregaFormPopup('codigo=0&cd_candidato=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=coleta&modo=edit');";
                    $acoes['ADICIONAR_COLETA'] = $acao;
                    $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Registro", "ADICIONAR", "Adicionar um registro", "");
                    $acao->mOnClick = "carregaFormPopup('codigo=0&cd_candidato=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=regcie&modo=edit');";
                    $acoes['ADICIONAR_REGISTRO'] = $acao;
                    $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Emissão", "ADICIONAR", "Adicionar uma emissão de CIE", "");
                    $acao->mOnClick = "carregaFormPopup('codigo=0&cd_candidato=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=emiscie&modo=edit');";
                    $acoes['ADICIONAR_EMISSAO'] = $acao;
                    $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Prorrogação", "ADICIONAR", "Adicionar uma prorrogação", "");
                    $acao->mOnClick = "carregaFormPopup('codigo=0&cd_candidato=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=prorrog&modo=edit');";
                    $acoes['ADICIONAR_PRORROGACAO'] = $acao;
                    $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Cancelamento", "ADICIONAR", "Adicionar um cancelamento", "");
                    $acao->mOnClick = "carregaFormPopup('codigo=0&cd_candidato=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=cancel&modo=edit');";
                    $acoes['ADICIONAR_CANCELAMENTO'] = $acao;
                    if ($pTela->mCampoNome['fl_visto_atual']->mValor != '1') {
                        $acoes['TORNAR_VISTO_ATUAL'] = new cACAO(cACAO::ACAO_FORM, "Tornar ATUAL", "TORNAR_ATUAL", "Tornar esse visto o visto atual do candidato", "Deseja mesmo tornar esse processo o visto mais atual do candidato?", "Modificação de visto atual");
                    }
                } else {
                    $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Mudar de visto", "MUDA_VISTO", "", "");
                    $acao->mOnClick = "carregaMudaVisto('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=mte');";
                    $acoes[] = $acao;
                }
                
                $acao = new cACAO(cACAO::ACAO_LINK, "Ver log", "LOG", "Visualizar o log de alterações", "");
                $acao->mLink = "/paginas/carregue.php?controller=cCTRL_PROCESSO_MTE&metodo=log&codigo=" . $pTela->mCampoNome['codigo']->mValor;
                $acao->mTarget = "_blank";
                $acoes[] = $acao;
                
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Migrar", "MIGRAR_CAND", "Migrar esse processo para outro candidato", "");
                $acao->mOnClick = "CarregueAjax('cINTERFACE_CANDIDATO', 'MigrarProcessoAjax', 'NU_CANDIDATO_ORIGEM=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tabela=processo_mte', 'formPopup');";
                $acoes['MIGRAR'] = $acao;
                if (intval($pTela->mCampoNome['id_solicita_visto']->mValor) == 0) {
                    $acoes['EXCLUIR'] = new cACAO(cACAO::ACAO_FORM, "Excluir", "EXCLUIR", "Excluir o visto", "Deseja mesmo excluir esse processo? Os processos associados ficarão órfãos. Essa operação não pode ser desfeita", "Exclusão de processo MTE");
                }
                break;
            case "processo_regcie_view":
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Alterar", "ALTERAR", "", "");
                $acao->mOnClick = "carregaFormPopup('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=regcie&modo=edit');";
                $acoes[] = $acao;
                
                $acao = new cACAO(cACAO::ACAO_LINK, "Ver log", "LOG", "Visualizar o log de alterações", "");
                $acao->mLink = "/paginas/carregue.php?controller=cCTRL_PROCESSO_REGCIE&metodo=log&codigo=" . $pTela->mCampoNome['codigo']->mValor;
                $acao->mTarget = "_blank";
                $acoes[] = $acao;
                
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Mudar de visto", "MUDA_VISTO", "", "");
                $acao->mOnClick = "carregaMudaVisto('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=regcie');";
                $acoes[] = $acao;
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Migrar", "MIGRAR_CAND", "Migrar esse processo para outro candidato", "");
                $acao->mOnClick = "CarregueAjax('cINTERFACE_CANDIDATO', 'MigrarProcessoAjax', 'NU_CANDIDATO_ORIGEM=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tabela=processo_regcie', 'formPopup');";
                $acoes[] = $acao;
                if (intval($pTela->mCampoNome['id_solicita_visto']->mValor) == 0) {
                    $acoes[] = new cACAO(cACAO::ACAO_FORM, "Excluir", "EXCLUIR", "Excluir o processo do visto", "Deseja mesmo excluir esse processo? Essa operação não pode ser desfeita", "Exclusão de Registro");
                }
                break;
            case "processo_coleta_view":
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Alterar", "ALTERAR", "", "");
                $acao->mOnClick = "carregaFormPopup('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=coleta&modo=edit');";
                $acoes[] = $acao;
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Mudar de visto", "MUDA_VISTO", "", "");
                $acao->mOnClick = "carregaMudaVisto('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=coleta');";
                $acoes[] = $acao;
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Migrar", "MIGRAR_CAND", "Migrar esse processo para outro candidato", "");
                $acao->mOnClick = "CarregueAjax('cINTERFACE_CANDIDATO', 'MigrarProcessoAjax', 'NU_CANDIDATO_ORIGEM=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tabela=processo_coleta', 'formPopup');";
                $acoes[] = $acao;
                if (intval($pTela->mCampoNome['id_solicita_visto']->mValor) == 0) {
                    $acoes[] = new cACAO(cACAO::ACAO_FORM, "Excluir", "EXCLUIR", "Excluir o processo do visto", "Deseja mesmo excluir esse processo? Essa operação não pode ser desfeita", "Exclusão de Coleta de visto");
                }
                break;
            case "processo_coleta_cie_view":
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Alterar", "ALTERAR", "", "");
                $acao->mOnClick = "carregaFormPopup('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=coleta_cie&modo=edit');";
                $acoes[] = $acao;
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Mudar de visto", "MUDA_VISTO", "", "");
                $acao->mOnClick = "carregaMudaVisto('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=coleta_cie');";
                $acoes[] = $acao;
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Migrar", "MIGRAR_CAND", "Migrar esse processo para outro candidato", "");
                $acao->mOnClick = "CarregueAjax('cINTERFACE_CANDIDATO', 'MigrarProcessoAjax', 'NU_CANDIDATO_ORIGEM=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tabela=processo_coleta_cie', 'formPopup');";
                $acoes[] = $acao;
                if (intval($pTela->mCampoNome['id_solicita_visto']->mValor) == 0) {
                    $acoes[] = new cACAO(cACAO::ACAO_FORM, "Excluir", "EXCLUIR", "Excluir o processo do visto", "Deseja mesmo excluir esse processo? Essa operação não pode ser desfeita", "Exclusão de Coleta de CIE");
                }
                break;
            case "processo_emiscie_view":
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Alterar", "ALTERAR", "", "");
                $acao->mOnClick = "carregaFormPopup('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=emiscie&modo=edit');";
                $acoes[] = $acao;
                
                $acao = new cACAO(cACAO::ACAO_LINK, "Ver log", "LOG", "Visualizar o log de alterações", "");
                $acao->mLink = "/paginas/carregue.php?controller=cCTRL_PROCESSO_EMISCIE&metodo=log&codigo=" . $pTela->mCampoNome['codigo']->mValor;
                $acao->mTarget = "_blank";
                $acoes[] = $acao;
                
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Mudar de visto", "MUDA_VISTO", "", "");
                $acao->mOnClick = "carregaMudaVisto('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=emiscie');";
                $acoes[] = $acao;
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Migrar", "MIGRAR_CAND", "Migrar esse processo para outro candidato", "");
                $acao->mOnClick = "CarregueAjax('cINTERFACE_CANDIDATO', 'MigrarProcessoAjax', 'NU_CANDIDATO_ORIGEM=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tabela=processo_emiscie', 'formPopup');";
                $acoes[] = $acao;
                if (intval($pTela->mCampoNome['id_solicita_visto']->mValor) == 0) {
                    $acoes[] = new cACAO(cACAO::ACAO_FORM, "Excluir", "EXCLUIR", "Excluir o processo do visto", "Deseja mesmo excluir esse processo? Essa operação não pode ser desfeita", "Exclusão de Emissão CIE");
                }
                break;
            case "processo_prorrog_view":
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Alterar", "ALTERAR", "", "");
                $acao->mOnClick = "carregaFormPopup('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=prorrog&modo=edit&modo=edit');";
                $acoes[] = $acao;
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Reestab.", "ADICIONAR", "Adicionar um restabelecimento", "");
                $acao->mOnClick = "carregaFormPopup('codigo=0&codigo_processo_prorrog=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=regcie&modo=edit');";
                $acoes[] = $acao;
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Mudar de visto", "MUDA_VISTO", "", "");
                $acao->mOnClick = "carregaMudaVisto('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=prorrog');";
                $acoes[] = $acao;
                
                $acao = new cACAO(cACAO::ACAO_LINK, "Ver log", "LOG", "Visualizar o log de alterações", "");
                $acao->mLink = "/paginas/carregue.php?controller=cCTRL_PROCESSO_PRORROG&metodo=log&codigo=" . $pTela->mCampoNome['codigo']->mValor;
                $acao->mTarget = "_blank";
                $acoes[] = $acao;
                
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Migrar", "MIGRAR_CAND", "Migrar esse processo para outro candidato", "");
                $acao->mOnClick = "CarregueAjax('cINTERFACE_CANDIDATO', 'MigrarProcessoAjax', 'NU_CANDIDATO_ORIGEM=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tabela=processo_prorrog', 'formPopup');";
                $acoes[] = $acao;
                if (intval($pTela->mCampoNome['id_solicita_visto']->mValor) == 0) {
                    $acoes[] = new cACAO(cACAO::ACAO_FORM, "Excluir", "EXCLUIR", "Excluir o processo do visto", "Deseja mesmo excluir esse processo? Essa operação não pode ser desfeita", "Exclusão de prorrogação");
                }
                break;
            case "processo_cancel_view":
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Alterar", "ALTERAR", "", "");
                $acao->mOnClick = "carregaFormPopup('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=cancel&modo=edit');";
                $acoes[] = $acao;

                $acao = new cACAO(cACAO::ACAO_LINK, "Ver log", "LOG", "Visualizar o log de alterações", "");
                $acao->mLink = "/paginas/carregue.php?controller=cCTRL_PROCESSO_CANCEL&metodo=log&codigo=" . $pTela->mCampoNome['codigo']->mValor;
                $acao->mTarget = "_blank";
                $acoes[] = $acao;
                
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Mudar de visto", "MUDA_VISTO", "", "");
                $acao->mOnClick = "carregaMudaVisto('codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=cancel');";
                $acoes[] = $acao;
                $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Migrar", "MIGRAR_CAND", "Migrar esse processo para outro candidato", "");
                $acao->mOnClick = "CarregueAjax('cINTERFACE_CANDIDATO', 'MigrarProcessoAjax', 'NU_CANDIDATO_ORIGEM=" . $pTela->mCampoNome['cd_candidato']->mValor . "&codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tabela=processo_cancel', 'formPopup');";
                $acoes[] = $acao;
                if (intval($pTela->mCampoNome['id_solicita_visto']->mValor) == 0) {
                    $acoes[] = new cACAO(cACAO::ACAO_FORM, "Excluir", "EXCLUIR", "Excluir o processo do visto", "Deseja mesmo excluir esse processo? Essa operação não pode ser desfeita", "Exclusão de Cancelamento");
                }
                break;
            case "processo_emiscie_edit":
            case "processo_mte_edit":
            case "processo_regcie_edit":
            case "processo_emiscie_edit":
            case "processo_prorrog_edit":
            case "processo_coleta_edit":
            case "processo_cancel_edit":
            case "processo_coleta_cie_edit":
                if ($pTela->mCampoNome['codigo']->mValor == '' || $pTela->mCampoNome['codigo']->mValor == 0) {
                    $acoes[] = new cACAO(cACAO::ACAO_FORM, "Incluir", "ADICIONAR", "Incluir o processo", "", "");
                } else {
                    $acoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Salvar as alterações", "", "");
                    if ($pTela->mCampoNome['id_solicita_visto']->mValor != '' && $pTela->mCampoNome['ID_STATUS_SOL']->mValor != '6') {
                        $acoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar e Fechar OS", "SALVAR_E_FECHAR", "Salvar as alterações e fechar a OS correspondente", "", "");
                    }
                }
                break;
            case "processo_edit":
            case "CANDIDATO_CTPS":
            case "CANDIDATO_CNH":
            case "CANDIDATO_CPF":
                $acoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Salvar as alterações", "", "");
                if ($pTela->mCampoNome['id_solicita_visto']->mValor != '' && $pTela->mCampoNome['ID_STATUS_SOL']->mValor != '6') {
                    $acoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar e Fechar OS", "SALVAR_E_FECHAR", "Salvar as alterações e fechar a OS correspondente", "", "");
                }
                break;
        }
        return $acoes;
    }

    /**
     * Obtem os parametros necessários para a execução dos serviços
     */
    public function ObterParametros($pPOST, $pGET) {
        if (isset($pGET['codigo'])) {
            $this->mcodigo = $pGET['codigo'];
        }
        if (isset($pGET['tipo'])) {
            $this->mtipo = $pGET['tipo'];
        }
    }

    public static function PostPROCESSO_MTE($pTela, $pPOST, $pFILES, &$pSESSION) {
        try {
            $OrdemServico = new cORDEMSERVICO();
            $OrdemServico->Recuperar($pTela->mCampoNome['id_solicita_visto']->mValor);
            $msg = '';
            if (!cBANCO::DataValida($pTela->mCampoNome['dt_recebimento_bsb']->mValor)) {
                $msg .= '<br/>- A data do recebimento em BSB é inválida';
            }
            if (!cBANCO::DataValida($pTela->mCampoNome['dt_requerimento']->mValor)) {
                $msg .= '<br/>- A data do requerimento é inválida';
            }
            if ($msg != '') {
                $msg = 'Não foi possível executar a operação pois:' . $msg;
                throw new cERRO_CONSISTENCIA($msg);
            }
            switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
                case 'OS_CONCLUIR_PROTOCOLO':
                    $OrdemServico->AtualizarStatus(5);
                    return 'Protocolo concluído com sucesso!';
                    break;
                case 'SALVAR':
                    $pTela->AtualizarBanco();
                    return 'Processo atualizado com sucesso!';
                    break;
                case 'OS_COBRAR':
                    // Uma pequena anomalia essa ação, que é da OS, estar sendo processada aqui
                    // dentro do processo, mas é assim pois a disponibilidade dessa ação depende do
                    // status do processo. Poderia ser diferente, enfim...
                    $OrdemServico->Cobre();
                    return "Cobrança indicada com sucesso na OS.";
                    break;
            }
        } catch (cERRO_CONSISTENCIA $exc) {
            $msg = $exc->getMessage();
            $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
            cHTTP::setResultadoNaoOk();
            return $msg;
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
            $msg = "Não foi possível atender à sua solicitação pois houve um erro. Para acelerar a resolução, apresente essas informações ao suporte:" . $msg;
            cHTTP::setResultadoNaoOk();
            return $msg;
        }
    }

    public static function PostANDAMENTO_MTE($pTela, $pPOST, $pFILES, &$pSESSION) {
        try {
            $OrdemServico = new cORDEMSERVICO();
            $OrdemServico->Recuperar($pTela->mCampoNome['id_solicita_visto']->mValor);
            $msg = '';
            if (!cBANCO::DataValida($pTela->mCampoNome['dt_recebimento_bsb']->mValor)) {
                $msg .= '<br/>- A data do recebimento em BSB é inválida';
            }
            if (!cBANCO::DataValida($pTela->mCampoNome['dt_requerimento']->mValor)) {
                $msg .= '<br/>- A data do requerimento é inválida';
            }
            if (!cBANCO::DataValida($pTela->mCampoNome['dt_ult_atu_andamento']->mValor)) {
                $msg .= '<br/>- A data da última atualização é inválida';
            }
            if (!cBANCO::DataValida($pTela->mCampoNome['dt_deferimento']->mValor)) {
                $msg .= '<br/>- A data de conclusão é inválida';
            }
            if (!cBANCO::DataValida($pTela->mCampoNome['dt_publicacao_dou']->mValor)) {
                $msg .= '<br/>- A data de publicação DOU é inválida';
            }
            if ($msg != '') {
                $msg = 'Não foi possível executar a operação pois:' . $msg;
                throw new cERRO_CONSISTENCIA($msg);
            }
            switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
                case 'SALVAR':
                    // Valida campos data
                    $pTela->AtualizarBanco();
                    if ($pTela->mCampoNome['ID_STATUS_CONCLUSAO']->mValor == 2) { // SE FOI DEFERIDO. ENTÃO:
                        $OrdemServico->AtualizarStatus(6);   // ENCERRO
                        $OrdemServico->Cobre();   // COBRO
                    }
                    cHTTP::setResultadoOk();
                    return 'Processo atualizado com sucesso!';

                    break;
                case 'OS_ENCERRAR':
                    $pTela->AtualizarBanco();
                    $OrdemServico->AtualizarStatus(6);
                    return "Informações do processo atualizadas com sucesso.<br/>OS encerrada com sucesso.";
                    break;
                case 'OS_COBRAR':
                    // Uma pequena anomalia essa ação, que é da OS, estar sendo processada aqui
                    // dentro do processo, mas é assim pois a disponibilidade dessa ação depende do
                    // status do processo. Poderia ser diferente, enfim...
                    $OrdemServico->Cobre();
                    return "Cobrança indicada com sucesso na OS.";
                    break;
            }
        } catch (cERRO_CONSISTENCIA $exc) {
            $msg = $exc->getMessage();
            $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
            cHTTP::setResultadoNaoOk();
            return $msg;
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
            $msg = "Não foi possível atender à sua solicitação pois houve um erro. Para acelerar a resolução, apresente essas informações ao suporte:" . $msg;
            cHTTP::setResultadoNaoOk();
            return $msg;
        }
    }

    public static function PostCONCLUSAO_MTE($pTela, $pPOST, $pFILES, &$pSESSION) {
        try {
            $OrdemServico = new cORDEMSERVICO();
            $OrdemServico->Recuperar($pTela->mCampoNome['id_solicita_visto']->mValor);
            switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
                case 'SALVAR':
                    try {
                        // Valida campos data
                        $msg = '';
                        if (!cBANCO::DataValida($pTela->mCampoNome['dt_recebimento_bsb']->mValor)) {
                            $msg .= '<br/>- A data do requerimento é inválida';
                        }
                        if (!cBANCO::DataValida($pTela->mCampoNome['dt_requerimento']->mValor)) {
                            $msg .= '<br/>- A data do requerimento é inválida';
                        }
                        if (!cBANCO::DataValida($pTela->mCampoNome['dt_ult_atu_andamento']->mValor)) {
                            $msg .= '<br/>- A data da última atualização é inválida';
                        }
                        if (!cBANCO::DataValida($pTela->mCampoNome['dt_deferimento']->mValor)) {
                            $msg .= '<br/>- A data de conclusão é inválida';
                        }
                        if (!cBANCO::DataValida($pTela->mCampoNome['dt_publicacao_dou']->mValor)) {
                            $msg .= '<br/>- A data de publicação DOU é inválida';
                        }
                        if ($msg != '') {
                            $msg = 'Não foi possível executar a operação pois:' . $msg;
                            throw new cERRO_CONSISTENCIA($msg);
                        }
                        $pTela->AtualizarBanco();
                        if ($pTela->mCampoNome['ID_STATUS_CONCLUSAO']->mValor == 2) { // SE FOI DEFERIDO. ENTÃO:
                            $OrdemServico->AtualizarStatus(6);   // ENCERRO
                            $OrdemServico->Cobre();   // COBRO
                        }
                        return 'Processo atualizado com sucesso!';
                    } catch (cERRO_CONSISTENCIA $exc) {
                        $msg = $exc->getMessage();
                        $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
                        cHTTP::setResultadoNaoOk();
                        return $msg;
                    } catch (Exception $exc) {
                        $msg = $exc->getMessage();
                        $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
                        $msg = "Não foi possível atender à sua solicitação pois houve um erro. Para acelerar a resolução, apresente essas informações ao suporte:" . $msg;
                        cHTTP::setResultadoNaoOk();
                        return $msg;
                    }

                    break;
                case 'OS_COBRAR':
                    $OrdemServico->Cobre();
                    return "Cobrança indicada com sucesso na OS.";
                    break;
            }
        } catch (Exception $exc) {
            error_log($exc->getMessage() . $exc->getTraceAsString());
            return "Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage()));
        }
    }

    public static function Postprocesso_mte_view($pTela, $pPOST, $pFILES, &$pSESSION) {
        $pTela->ResseteChavePara('codigo');
        $tipo = split("_", $pTela->mno_edicao);
        eval('$xprocesso = new cprocesso_' . $tipo[1] . '();');
        cINTERFACE::MapeiaEdicaoClasse($pTela, $xprocesso);
        switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
            case "EXCLUIR":
                $xprocesso->Exclua();
                return 'Processo excluído com sucesso!';
                break;
            case "TORNAR_ATUAL":
                $xprocesso->TorneSeVistoAtual();
                return 'Processo atual alterado com sucesso!';
                break;
        }
    }

    public static function Postprocesso_prorrog_view($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_view($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_regcie_view($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_view($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_coleta_view($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_view($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_coleta_cie_view($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_view($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_emiscie_view($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_view($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_cancel_view($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_view($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_view($pTela, $pPOST, $pFILES, &$pSESSION) {
        $pTela->ResseteChavePara('codigo');
        $tipo = split("_", $pTela->mno_edicao);
        eval('$xprocesso = new cprocesso_' . $tipo[1] . '();');
        cINTERFACE::MapeiaEdicaoClasse($pTela, $xprocesso);
        if ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor == 'EXCLUIR') {
            $xprocesso->Exclua();
            return 'Processo excluído com sucesso!';
        }
    }

    public static function Postprocesso_mte_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        $pTela->ResseteChavePara('codigo');
        $xprocesso = new cprocesso_mte();
        cINTERFACE::MapeiaEdicaoClasse($pTela, $xprocesso);
        switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
            case 'SALVAR':
                $xprocesso->Salve_ProcessoEdit();
                return 'Processo atualizado com sucesso!';
                break;
            case 'ADICIONAR':
                $xprocesso->Salve_ProcessoADICIONAR();
                return 'Processo atualizado com sucesso!';
                break;
        }
    }

    public static function Postprocesso_prorrog_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_edit($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_regcie_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_edit($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_coleta_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_edit($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_coleta_cie_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_edit($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_emiscie_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_edit($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_cancel_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        return self::Postprocesso_edit($pTela, $pPOST, $pFILES, $pSESSION);
    }

    public static function Postprocesso_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        $pTela->ResseteChavePara('codigo');

        $tipo = split("_", $pTela->mno_edicao);
        eval('$xprocesso = new cprocesso_' . $tipo[1] . '();');
        cINTERFACE::MapeiaEdicaoClasse($pTela, $xprocesso);
        switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
            case 'SALVAR':
                try {
                    $xprocesso->Salve_ProcessoEdit();
                    cHTTP::setResultadoOk();
                    return 'Processo atualizado com sucesso!';
                } catch (cERRO_CONSISTENCIA $exc) {
                    $msg = $exc->getMessage();
                    $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
                    cHTTP::setResultadoNaoOk();
                    return $msg;
                } catch (Exception $exc) {
                    $msg = $exc->getMessage();
                    $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
                    $msg = "Não foi possível atender à sua solicitação pois houve um erro. Para acelerar a resolução, apresente essas informações ao suporte:" . $msg;
                    cHTTP::setResultadoNaoOk();
                    return $msg;
                }
                break;
            case 'SALVAR_E_FECHAR':
                try {
                    $xprocesso->Salve_ProcessoEdit();
                    $OS = new cORDEMSERVICO();
                    $OS->mID_SOLICITA_VISTO = $pTela->mCampoNome['id_solicita_visto']->mValor;
                    $OS->Fechar();
                    cHTTP::setResultadoOk();
                    return 'Processo atualizado com sucesso! OS fechada com sucesso!';
                } catch (cERRO_CONSISTENCIA $exc) {
                    $msg = $exc->getMessage();
                    $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
                    cHTTP::setResultadoNaoOk();
                    return $msg;
                } catch (Exception $exc) {
                    $msg = $exc->getMessage();
                    $msg = str_replace("\n", "<br/>", str_replace("'", "\'", $msg));
                    $msg = "Não foi possível atender à sua solicitação pois houve um erro. Para acelerar a resolução, apresente essas informações ao suporte:" . $msg;
                    cHTTP::setResultadoNaoOk();
                    return $msg;
                }
                break;
            case 'ADICIONAR':
                $xprocesso->IncluaNovoFilho();
                return 'Processo incluído com sucesso!';
                break;
        }
        /*
         * TODO: Incluir atualização do status do visto
         */
    }

    /**
     * Processa o form de observações de processos
     * @param cEDICAO $pTela
     * @param array $pPOST		$_POST
     * @param array $pFILES		$_FILES
     * @param array $pSESSION	$_SESSION
     */
    public static function Postprocesso_mte_observacao($pTela, $pPOST, $pFILES, &$pSESSION) {
        $proc = new cprocesso_mte($pTela->mCampoNome['codigo']->mValor);
        $proc->mobservacao_visto = $pTela->mCampoNome['observacao_visto']->mValor;
        $proc->AtualizeObservacaoVisto();
        return "Observações alteradas com sucesso.";
    }

    /**
     * Gera o formulário paa alteração do visto de um determinado processo
     * @param type $pGET
     * @return string HTML do processo.
     */
    public static function Formprocesso_mte_do_processo_edit($pGET) {
        $nomeTela = 'processo_mte_do_processo_edit';
        $tela = new cEDICAO($nomeTela);
        $tela->CarregarConfiguracao();
        $tela->ResseteChavePara('codigo');
        cINTERFACE::RecupereValoresEdicao($tela, '', $pGET);
        $tela->mView = 'vprocesso_' . $tela->mCampoNome['tipo']->mValor;
        $tela->RecupereRegistro();
        cINTERFACE::RecupereValoresEdicao($tela, '', $pGET);
        $tela->mCampoNome['codigo_processo_mte']->mCombo->mValorDefault = "(processos órfãos)";
        $tela->mCampoNome['codigo_processo_mte']->mCombo->mExtra = 'WHERE fl_vazio = 0 and (nu_servico not in (49, 31) or nu_servico is null ) and cd_candidato = ' . $tela->mCampoNome['cd_candidato']->mValor;
        $tela->mCampoNome['codigo_processo_mte']->mCombo->mOrdem = ' order by codigo desc';
        $ret = cINTERFACE::formEdicao($tela);
        $ret.= cINTERFACE::RenderizeEdicaoComPaineis($tela);
        $acoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Salvar as alterações", "", "");
        $ret .= '</table>';
        $ret.= cINTERFACE::RenderizeBarraAcoesRodape($acoes);
        $ret.= '</form>';
        return $ret;
    }

    /**
     * Processa o form de mudanca de visto
     * @param cEDICAO $pTela
     * @param array $pPOST		$_POST
     * @param array $pFILES		$_FILES
     * @param array $pSESSION	$_SESSION
     */
    public static function Postprocesso_mte_do_processo_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        $tipo = $pTela->mCampoNome['tipo']->mValor;
        eval('$xprocesso = new cprocesso_' . $tipo . '();');
        cINTERFACE::MapeiaEdicaoClasse($pTela, $xprocesso);
        switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
            case 'SALVAR':
                $xprocesso->Salve_ProcessoMte();
                return 'Processo atualizado com sucesso!';
                break;
        }
        return "Observações alteradas com sucesso.";
    }

}
