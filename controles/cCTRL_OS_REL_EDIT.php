<?php
class cCTRL_OS_REL_EDIT extends cCTRL_MODELO_TWIG_EDIT{
	protected $template_menu;
	protected $houvePost;
	protected $data;
	protected $nomeTemplate;
	protected $nomeAbaAtiva;

	public function set_nomeTemplate($pValor){
		$this->nomeTemplate = $pValor;
	}

	public function __construct() {
		$this->nomeTemplate = 'public/os_capa.html';
	}
	
	public function Edite(){
		// Carrega o ID informado no get ou o form postado anteriormente, se houver
		$this->GeraDataDoTemplate();
		return cTWIG::Render($this->nomeTemplate, $this->data);

		// $mpdf->SetWatermarkText('DRAFT');
		// $mpdf->showWatermarkText = true;
	}

	public function GeraDataDoTemplate(){
		cHTTP::setLang('en_us');
		$this->data = array();
		$pID_SOLICITA_VISTO = cHTTP::ParametroValido('ID_SOLICITA_VISTO');
		$rs = mysql_query("SELECT DISTINCT v.*, e.NO_ENDERECO, e.NU_CNPJ FROM vSOLICITA_VISTO v LEFT JOIN EMPRESA e ON e.NU_EMPRESA = v.NU_EMPRESA WHERE v.ID_SOLICITA_VISTO = ".$pID_SOLICITA_VISTO) or die( mysql_error() );
		$tela = mysql_fetch_object($rs);

		$this->data['tela'] = $tela;
	}

	public function CarregueTemplatePeloModelo(){
		if(intval($this->template->getValorCampo('NU_CANDIDATO')) > 0 ){
			$this->modelo->RecuperarDR($this->template->getValorCampo('NU_CANDIDATO'));
			$this->template->CarregueDoOBJETO($this->modelo);
		}
		$this->GeraDataDoTemplate();
	}
	
	public function get_msgErroPadrao(){
		return 'Não foi possí­vel realizar a operação pois ';
	}
	
	
	public function get_parametrosAdicionaisListagem(){
		return "";
	}
}
