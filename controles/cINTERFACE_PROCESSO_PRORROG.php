<?php
/**
 * Description of cINTERFACE_PROCESSO_PRORROG
 * @author Leonardo Medeiros
 */
class cINTERFACE_PROCESSO_PRORROG
	extends cINTERFACE_PROCESSO{

	public function __construct($pcodigo = "" ) {
		if ($pcodigo != "") {
			$this->mprocesso = new cprocesso_prorrog($pcodigo);
		}
	}
}
