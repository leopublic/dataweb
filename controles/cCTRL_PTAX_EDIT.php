<?php
/**
 * @author leonardo
 */
class cCTRL_PTAX_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cperfiltaxa();
	}

	public function InicializeTemplate() {
		$this->template = new cTMPL_PTAX_EDIT();
	}

	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_PTAX_LIST', 'Liste');
	}

}
