<?

class cCTRL_CAND_CLIE_EDIT extends cCTRL_MODELO_TWIG_EDIT {

    public function __construct() {
        cHTTP::setLang('en_us');
    }

    public function InicializeModelo() {
        $rep = new cREPOSITORIO_CANDIDATO;
        if (cHTTP::ParametroValido('NU_CANDIDATO')> 0){
            $this->modelo = $rep->candidatoParaCliente(cHTTP::ParametroValido('NU_CANDIDATO'));
        } else {
            $this->modelo = new cCANDIDATO_TMP();
        }
    }

    public function InicializeTemplate() {
        $menu = cmenu::NovoMenuCliente();
        cHTTP::set_menu_esquerdo($menu);
        $menu = cmenu::NovoMenuClienteSystem();
        cHTTP::set_menu_Direito($menu);
        $this->template_menu = new cTMPL_CAND_CLIE_EDIT_ABA_LATERAL();
        $this->template_menu->mAbas[$this->nomeAbaAtiva]->mno_classe = "active";
        $this->template_menu->CarregueDoHTTP();
        $this->template_menu->RepliqueChavesParaAbasAjax();
    }

    public function CarregueTemplatePeloModelo() {
            $this->template->CarregueDoOBJETO($this->modelo);
    }

    public function ApiTemplate() {
        if ($this->template->getValorCampo('NU_CANDIDATO') > 0) {
            $this->template->set_titulo($this->template->getValorCampo('NOME_COMPLETO'));
        } else {
            $this->template->set_titulo('New Applicant');
        }
        $api = parent::ApiTemplate();

        $api->AdicionaComplemento($this->Complementos());

        return $api;
    }

    public function Complementos() {
        $x = array();
        if ($this->template->getValorCampo('NU_CANDIDATO') > 0) {
            $this->modelo->Recuperar();

            $arquivos = cARQUIVO::JSON_ArquivosDoCandidato($this->template->getValorCampo('NU_CANDIDATO'));
            $x['arquivos'] = $arquivos;

            $x['candidato']['nu_candidato'] = $this->template->getValorCampo('NU_CANDIDATO');

            $empresa = new cEMPRESA();
            $empresa->mNU_EMPRESA = $this->modelo->mNU_EMPRESA;
            $empresa->RecuperePeloId();
            $x['candidato']['empresa']['NO_RAZAO_SOCIAL'] = $empresa->mNO_RAZAO_SOCIAL;

            $embp = new cEMBARCACAO_PROJETO();
            $embp->mNU_EMPRESA = $this->modelo->mNU_EMPRESA;
            $embp->mNU_EMBARCACAO_PROJETO = $this->modelo->mNU_EMBARCACAO_PROJETO;
            $embp->RecuperePeloId();
            $x['candidato']['embarcacao']['NO_EMBARCACAO_PROJETO'] = $embp->mNO_EMBARCACAO_PROJETO;

            if ($this->modelo->mcodigo_processo_mte_atual > 0) {
                $autorizacao = new cprocesso_mte();
                $autorizacao->mcodigo = $this->modelo->mcodigo_processo_mte_atual;
                $autorizacao->RecupereSe();
                $x['candidato']['prazo_estada'] = $autorizacao->get_situacao_prazo_estada();
                $x['candidato']['status'] = $autorizacao->get_situacao_texto();
            } else {
                $x['candidato']['prazo_estada'] = '--';
                $x['candidato']['status'] = 'sem visto';
            }
        }
        return $x;
    }

    public function ProcessePost() {
        $this->modelo->Recuperar($this->template->getValorCampo('NU_CANDIDATO'));
        parent::ProcessePost();
    }

    public function ProcesseSalvar() {
        $rep = new 
        $this->modelo->AtualizarExterno();
    }

}
