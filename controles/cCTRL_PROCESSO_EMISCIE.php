<?php
/**
 * Description of cCTRL_PROCESSO_EMISCIE
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_EMISCIE extends cCTRL_PROCESSO{
	public function FormEdicaoOS(cORDEMSERVICO $pOS){
		$tela = $this->FormEdicaoPadrao($pOS, 'processo_emiscie_edit');
		if ($pOS->mReadonly){
			$tela->mCampoNome['CO_CLASSIFICACAO_VISTO']->mVisivel = false;
			$tela->mCampoNome['NO_CLASSIFICACAO_VISTO']->mVisivel = true;
			if ($tela->mCampoNome['NO_CLASSIFICACAO_VISTO']->mValor == ''){
				$tela->mCampoNome['NO_CLASSIFICACAO_VISTO']->mValor = '(não informado)';
			}
		} 

		$tela->mTitulo = $pOS->mNO_SERVICO_RESUMIDO;
		return $this->RenderizacaoPadraoEdicao($tela);
	}

	public function log(){
		$codigo = $_GET['codigo'];
		$processo = new cprocesso_emiscie();
		$processo->mcodigo = $codigo;
		$processo->RecupereSe();
		if ($processo->mid_solicita_visto != ''){
			$os = new cORDEMSERVICO();
			$os->mID_SOLICITA_VISTO = $processo->mid_solicita_visto;
			$os->Recuperar();
		} else {
			$os = null;
		}
		$candidato = new cCANDIDATO();
		$candidato->Recuperar($processo->mcd_candidato);
		$cursor = cprocesso_emiscie::cursorLog($codigo);
		$regs = $cursor->fetchAll();
		$menu = cLAYOUT::Menu();
		$data = array(
				'regs' => $regs,
				'menu' => $menu,
				'processo' => $processo,
				'os' => $os,
				'candidato' => $candidato
		);
		return cTWIG::Render('intranet/processo/emiscie_log.html', $data);
	
	}
	
}
