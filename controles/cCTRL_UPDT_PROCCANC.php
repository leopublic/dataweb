<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class cCTRL_UPDT_PROCCANC{

	public function dt_envio_bsb()
	{
		$ret = array();
		try{
			$dt_envio_bsb = cHTTP::$POST['dt_envio_bsb'];
			$codigo = cHTTP::$POST['codigo'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cprocesso_mte::Atualiza_dt_envio_bsb($codigo, $dt_envio_bsb, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function AtualizeCampoPadrao()
	{
		$ret = array();
		try{
			$valor		= cHTTP::$POST['valor'];
			$chave		= cHTTP::$POST['chave'];
			$nome_chave = cHTTP::$POST['xnome_chave'];
			$nome_campo = cHTTP::$POST['xnome_campo'];
			$tipo_campo	= cHTTP::$POST['tipo_campo'];
			cprocesso_cancel::AtualizeCampoPadrao($chave, $nome_campo, $valor, $tipo_campo, $nome_chave);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);

	}}
