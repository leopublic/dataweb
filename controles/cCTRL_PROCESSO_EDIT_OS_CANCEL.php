<?php
/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_EDIT_OS_CANCEL extends cCTRL_PROCESSO_EDIT_OS{
	public function InicializeModelo() {
		$this->modelo = new cprocesso_cancel();
	}
	public function InicializeTemplate() {
		$this->template = new cTMPL_PROCESSO_EDIT_OS_CANCEL();
		$this->template->set_estiloTituloInterno();
	}

	public function AcoesAdicionaisNoFechamento(){
		$cand = new cCANDIDATO;
		$cand->RecuperePeloId($this->modelo->mcd_candidato);
		$cand->Inative(cSESSAO::$mcd_usuario, "Fechamento da OS de cancelamento ".$this->os->mNU_SOLICITACAO);
	}
}
