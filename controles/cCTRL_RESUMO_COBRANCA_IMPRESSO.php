<?php
class cCTRL_RESUMO_COBRANCA_IMPRESSO extends cCTRL_MODELO{
	public function Gerar(){
		ini_set("memory_limit","128M");
		$resc_id = cHTTP::ParametroValido('resc_id');
		$resumo = new cresumo_cobranca();
		$resumo->RecuperePeloId($resc_id);
		$empresa = new cEMPRESA();
		$empresa->RecuperePeloId($resumo->mnu_empresa_requerente);

		$data = array();
		$data['nome'] = cSESSAO::$mnome;
		$data['data'] = date('d/m/Y');
		$data['hora'] = date('H:i:s');
		$data['resc_dt_faturamento'] = $resumo->mresc_dt_faturamento;
		$data['resc_nu_numero_mv'] = $resumo->mresc_nu_numero_mv;
		$data['resc_tx_observacoes'] = str_replace("\n", "<br/>", htmlentities($resumo->mresc_tx_observacoes));
		$data['resc_tx_mes_referencia'] = substr($resumo->mresc_dt_faturamento,3);
		$emb = new cEMBARCACAO_PROJETO();
		if ($resumo->mNU_EMBARCACAO_PROJETO_COBRANCA > 0){
			$emb->mNU_EMPRESA = $resumo->mnu_empresa_requerente;
			$emb->mNU_EMBARCACAO_PROJETO = $resumo->mNU_EMBARCACAO_PROJETO_COBRANCA;
			$emb->RecupereSe();
		}
		$data['NO_EMBARCACAO_PROJETO'] = htmlentities($emb->mNO_EMBARCACAO_PROJETO);
		$data['resc_tx_solicitante_cobranca'] = htmlentities($resumo->mresc_tx_solicitante);

		$data['empresa'] = array(
			"empr_tx_razao_social_cobranca" => htmlentities($empresa->mempr_tx_razao_social_cobranca)
			,"empr_tx_cnpj_cobranca" => $empresa->mempr_tx_cnpj_cobranca
			,'empr_tx_endereco_cobranca' => htmlentities($empresa->mempr_tx_endereco_cobranca)
			,'empr_tx_complemento_endereco_cobranca' => htmlentities($empresa->mempr_tx_complemento_endereco_cobranca)
			,'empr_tx_bairro_cobranca' => htmlentities($empresa->mempr_tx_bairro_cobranca)
			,'empr_tx_municipio_cobranca' => htmlentities($empresa->mempr_tx_municipio_cobranca)
			,'empr_tx_cep_cobranca' => htmlentities($empresa->mempr_tx_cep_cobranca)
			,'co_uf_cobranca'=> $empresa->mco_uf_cobranca
		);
		$data['candidatos'] = $resumo->JSON_OsParaRelatorio($total_cand, $total_geral_fmt);
		$data['total_cand'] = $total_cand;
		$data['total_geral_fmt'] = $total_geral_fmt;

		$ret = '';
                if ($resumo->mnu_empresa_prestadora == 2){
                    $tmpl = cFABRICA_TEMPLATE::singleton()->loadTemplate('rel_resumo_cobranca_header_BCS.mustache');
                } else {
                    $tmpl = cFABRICA_TEMPLATE::singleton()->loadTemplate('rel_resumo_cobranca_header.mustache');
                }
		$ret = $tmpl->render($data);
		$header = cBANCO::SemCaracteresEspeciaisMS($ret);
		$header = $ret;

		$tmpl = cFABRICA_TEMPLATE::singleton()->loadTemplate('rel_resumo_cobranca_footer.mustache');
		$ret = $tmpl->render($data);
		$footer = cBANCO::SemCaracteresEspeciaisMS($ret);
		$footer = $ret;

		$mpdf=new mPDF('en-x','A4','','',12,12,40,20,10,10);
		$mpdf->img_dpi = 200;
		$mpdf->simpleTables = true;
		$mpdf->packTableData = true;
		$mpdf->SetHTMLHeader($header);
		$mpdf->SetHTMLFooter($footer);

		$tmpl = cFABRICA_TEMPLATE::singleton()->loadTemplate('rel_resumo_cobranca_styles.mustache');
		$ret = $tmpl->render($data);
		$mpdf->WriteHTML($ret, 1);

		$tmpl = cFABRICA_TEMPLATE::singleton()->loadTemplate('rel_resumo_cobranca_topo.mustache');
		$ret = $tmpl->render($data);
		$html = cBANCO::SemCaracteresEspeciaisMS($ret);
		$mpdf->WriteHTML($html,2);

		foreach($data['candidatos'] as $candidato){
			$tmpl = cFABRICA_TEMPLATE::singleton()->loadTemplate('rel_resumo_cobranca_detalhe.mustache');
			$ret = $tmpl->render($candidato);
			$html = cBANCO::SemCaracteresEspeciaisMS($ret);
			$mpdf->WriteHTML($html,2);
		}

		$tmpl = cFABRICA_TEMPLATE::singleton()->loadTemplate('rel_resumo_cobranca_base.mustache');
		$ret = $tmpl->render($data);
		$html = cBANCO::SemCaracteresEspeciaisMS($ret);
		$mpdf->WriteHTML($html,2);
		$mpdf->Output();
	}
}
