<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_PROF_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cPROFISSAO();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_PROF_EDIT();
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_PROF_LIST', 'Liste');		
	}

}
