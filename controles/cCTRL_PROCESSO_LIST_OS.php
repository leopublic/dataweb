<?php
class cCTRL_PROCESSO_LIST_OS extends cCTRL_MODELO_LIST{

	public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl){
		$modelo = $this->modelo;
		$cursor = $modelo->Listar($ptmpl->mFiltro, $ptmpl->get_ordenacao());
		return $cursor;
	}
	/**
	 *
	 */
	public function Liste(){
		if($this->AcessoLiberado()){
			$this->InicializeModelo();
			$this->InicializeTemplate();
			$this->template->CarregueDoHTTP();

			// Tratar post
			if($this->template->mFoiPostado){
				$this->ProcessePost();
				$this->Redirecione();
			}

			$cursor = $this->get_cursorListagem($this->template);
			$this->template->Renderize($cursor);
			$html = $this->template->outputBuffer;
			$html.= $this->template->TratamentoPadraoDeMensagens();

			return $html;			
		}
	}

	public function AcessoLiberado()
	{
		return true;
	}
	public function Redirecione()
	{
		cHTTP::RedirecionePara(get_class($this), 'Liste');		
	}
	public function ProcessePost(){
	}

	public function ProcessarPostGrid(cTEMPLATE $pTemplate, cMODELO $pModelo, $pMetodoAtualizacao){
		$qtdLinhas = $pTemplate->qtdLinhas();
		$classeModelo = get_class($pModelo);
		for ($index = 0; $index < $qtdLinhas; $index++) {
			$pModelo=  new $classeModelo;
			$pTemplate->AtribuaDoArrayAoOBJETO($pModelo, $index);
			$pModelo->$pMetodoAtualizacao();
		}
	}

	public function ProcessaPostLinha(cTEMPLATE $pTemplate, cMODELO $pModelo, $pMetodoAtualizacao){
		$pModelo=  new $classeModelo;
		$pTemplate->AtribuaDoArrayAoOBJETO($pModelo, $index);
		$pModelo->$pMetodoAtualizacao();
	}

	public function InicializarTemplateListagem(&$pTmpl){
	}

		/**
	 *
	 * @param cTEMPLATE $pTemplateListar
	 * @param sting $pAbaAtiva Nome da aba que está aberta.
	 * @return type
	 */

	public function EnveloparParaTela($pConteudo, cTEMPLATE $pTemplate){
		$ret = '<div id="'.$pTemplate->mnome.'">';
		$ret.= $pConteudo;
		$ret.= $pTemplate->TratamentoPadraoDeMensagens();
		$ret.= '</div>';
//		cHTTP::AdicionarScript('ExibirMsg("#'.$pTemplate->mnome.' #dialog", "'.cHTTP::TituloMsg().'" );');
		return $ret;
	}
	/**
	 * Por enquanto não está sendo utilizado pois está usando o renderize form do layout.
	 * @param type $pConteudo
	 * @param cTEMPLATE $pTemplate
	 * @return string
	 */
	public function EnveloparEmForm($pConteudo, cTEMPLATE $pTemplate){
		$ret = cLAYOUT::RenderizeCabecalhoForm($pTemplate);
		$ret.= $pConteudo;
		$ret.= '</form>';
//		cHTTP::AdicionarScript('ExibirMsg("#'.$pTemplate->mnome.' #dialog", "'.cHTTP::TituloMsg().'" );');
		return $ret;
	}

}
