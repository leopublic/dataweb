<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_CAND_DOCS_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cCANDIDATO();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_CAND_DOCS_EDIT();
	}
	
	public function ProcesseSalvar() 
	{
		$this->modelo->mNU_CANDIDATO = $this->template->getValorCampo('NU_CANDIDATO');
		
		try{
			//Atualiza copia passaporte
			$postdoc = $this->template->getValorCampo('cand_fl_doc_1');
			$doc = new cdocumento();
			$temdoc = $doc->TemDisponivel($this->modelo->mNU_CANDIDATO, 1);
			if($temdoc != $postdoc){
				if($postdoc == "1"){
					$valor = "Sim";
				}
				else{
					$valor = "Não";
				}
				$this->modelo->AtualizaCopiaPassaporte($this->modelo->mNU_CANDIDATO, $valor);
			}
			//Atualiza form 1344
			$postdoc = $this->template->getValorCampo('cand_fl_doc_2');
			$doc = new cdocumento();
			$temdoc = $doc->TemDisponivel($this->modelo->mNU_CANDIDATO, 2);
			if($temdoc != $postdoc){
				if($postdoc == "1"){
					$valor = "Sim";
				}
				else{
					$valor = "Não";
				}
				$this->modelo->AtualizaForm1344($this->modelo->mNU_CANDIDATO, $valor);
			}
		
			$this->CarregueTemplatePeloModelo($this->template, $this->modelo);
			cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
		}
		catch (cERRO_CONSISTENCIA $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch (Exception $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}		
	}
	
	public function CarregueTemplatePeloModelo(&$ptmpl, &$pmodelo) {
		$nu_candidato = cHTTP::ParametroValido('NU_CANDIDATO');
		$doc = new cdocumento();
		$temdoc = $doc->TemDisponivel($nu_candidato, 1);
		if($temdoc){
			$this->template->setValorCampo('cand_fl_doc_1', 1);
		}
		else{
			$this->template->setValorCampo('cand_fl_doc_1', 0);			
		}
		$doc = new cdocumento();
		$temdoc = $doc->TemDisponivel($nu_candidato, 2);
		if($temdoc){
			$this->template->setValorCampo('cand_fl_doc_2', 1);
		}
		else{
			$this->template->setValorCampo('cand_fl_doc_2', 0);			
		}
		
	}
	
	public function RedirecioneEmSucesso(){
	}

}
