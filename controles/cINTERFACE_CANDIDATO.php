<?php

/**
 * Gerencia as interfaces relacionadas a classe cCANDIDATO
 * @author Leonardo Medeiros
 */
// TODO: Verificar diferencas em relacao a versao de producao
class cINTERFACE_CANDIDATO extends cCANDIDATO {

    const lbExperienciaPrevia = 0;
    const lbDadosCadastrais = 1;
    const lbCursos = 2;

    public static $labels = array(
        array("pt_br" => "Experiência prévia", "en_us" => "Professional Experience")
        , array("pt_br" => "Dados cadastrais", "en_us" => "Data required")
        , array("pt_br" => "Formação", "en_us" => "Education background")
    );

    public static function getLabel($pLabel) {
        return self::$labels[$pLabel][cHTTP::getLang()];
    }

    /**
     * Gera cabeçalho da OS com as ações habilitadas
     * @return string HTML pronto do cabeçalho
     */
    public function FormCabecalho() {
        $tela = new cEDICAO("operacoes_candidato");
        $tela->CarregarConfiguracao();
        $tela->mCampoNome['NU_CANDIDATO']->mValor = $this->mNU_CANDIDATO;
        $ret = '<div class="titulo" style="position:relative;">';
        $ret .= '<div style="float:right;margin-left:10px;">';
        $ret .= cINTERFACE::formEdicao($tela);
        $ret .= cINTERFACE::RenderizeBarraAcoesTitulo($this->Acoes());
        $ret .= "</form>";
        $ret .= '</div>';
        $ret .= '<div class="texto">Candidato "' . $this->mNOME_COMPLETO . '"&nbsp;&nbsp;(' . $this->mNU_CANDIDATO . ') ' . $this->Parentesco() . '</div>';
        $ret .= '</div>';
        return $ret;
    }

    public function Parentesco() {
        $ret = '';
        if ($this->mnu_candidato_parente != '') {
            $cand = new cCANDIDATO();
            $cand->Recuperar($this->mnu_candidato_parente);

            $sql = "select * from grau_parentesco where co_grau_parentesco = " . $this->mco_grau_parentesco;
            $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
            $rs = $res->fetch(PDO::FETCH_ASSOC);
            if (is_array($rs)) {
                $grau = strtolower($rs['NO_GRAU_PARENTESCO']);
            }
            $ret = '<br/><span class="detalheParente">é ' . $grau . ' de "<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $this->mnu_candidato_parente . '">' . $cand->mNOME_COMPLETO . ' " (' . $this->mnu_candidato_parente . ')</a></span>';
        }
        return $ret;
    }

    public function FormCPF() {
        $cpf = $this->mNU_CPF;
        if (trim($cpf) == '') {
            $cpf = '(não informado)';
        }
        return '<div class="ui-widget ui-widget-header tituloProcesso cpf">CPF : ' . $cpf . '</div>';
    }

    public static function Postoperacoes_candidato($pTela, $pPOST, $pFILES, &$pSESSION) {
        $cand = new cCANDIDATO();
        cINTERFACE::MapeiaEdicaoClasse($pTela, $cand);
        switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
            case "ADICIONAR_VISTO":
                return "Processo adicionado com sucesso!";
                break;
        }
    }

    public function Acoes() {
        $acoes = array();
        if (Acesso::permitido(Acesso::tp_Cadastro_Candidato_Alterar_Processos)) {
            $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Adicionar VISTO", "ADICIONAR_VISTO", "Clique para adicionar um novo visto ao candidato");
            $acao->mOnClick = "carregaFormPopup('codigo=0&cd_candidato=" . $this->mNU_CANDIDATO . "&tipo=mte&modo=edit');";
            $acoes[] = $acao;
        }
        return $acoes;
    }

    /**
     * Apresenta o visto atual com todos os seus processos
     * @param int $pNU_CANDIDATO Id do candidato
     * @param int $pcodigo
     */
    public function FormVistoAtual() {
        $ret = '';
        if ($this->mcodigo_processo_mte_atual > 0) {
            $ret .= self::FormVisualizarVisto($this->mcodigo_processo_mte_atual);
        } else {
            $ret .= "Visto atual não atribuído.";
        }
        return $ret;
    }

    /**
     * Apresenta o formulário para visualização de um visto qualquer...
     * @param int $pcodigo Chave do processo mte
     * @
     */
    public static function FormVisualizarVisto($pcodigo) {
        $tela = cINTERFACE_PROCESSO::TelaProcesso("mte", "view");
        $tela->mCampoNome['codigo']->mValor = $pcodigo;
        $ret = cINTERFACE_PROCESSO::FormProcesso($tela, true);
        //
        // Carrega processos do visto selecionado
        $sql = "  select 'mte' AS tipo
					    ,mte.cd_candidato AS cd_candidato
					    ,mte.codigo AS codigo
					    ,null as codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,mte.id_solicita_visto AS id_solicita_visto
					    ,mte.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,1 AS ordem_temp
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_mte mte
					where codigo_processo_mte = " . $pcodigo . " and fl_vazio = 0 and nu_servico = 31
					union
					select 'coleta' AS tipo
					    ,col.cd_candidato AS cd_candidato
					    ,col.codigo AS codigo
					    ,col.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,col.id_solicita_visto AS id_solicita_visto
					    ,0 AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,2 AS ordem_temp
					    , dt_cad
					    , nu_servico
					    , null as dt_ordem
					  from processo_coleta col
					where codigo_processo_mte = " . $pcodigo . "
					union
					  select 'regcie' AS tipo
					    ,reg.cd_candidato AS cd_candidato
					    ,reg.codigo AS codigo
					    ,reg.codigo_processo_mte AS codigo_processo_mte
					    ,reg.codigo_processo_prorrog AS codigo_processo_prorrog
					    ,reg.id_solicita_visto AS id_solicita_visto
					    ,reg.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,(case reg.nu_servico when 58 then 6 else 3 end) AS ordem_temp
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_regcie reg
					where codigo_processo_mte = " . $pcodigo . " and fl_vazio = 0
					union
					  select 'emiscie' AS tipo
					    ,emi.cd_candidato AS cd_candidato
					    ,emi.codigo AS codigo
					    ,emi.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,emi.id_solicita_visto AS id_solicita_visto
					    ,emi.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,4 AS ordem_temp
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_emissao, dt_cad) as dt_ordem
					  from processo_emiscie emi
					where codigo_processo_mte = " . $pcodigo . " and fl_vazio = 0
					union
					  select 'prorrog' AS tipo
					    ,pro.cd_candidato AS cd_candidato
					    ,pro.codigo AS codigo
					    ,pro.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,pro.id_solicita_visto AS id_solicita_visto
					    ,pro.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,5 AS ordem_temp
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_prorrog pro
					where codigo_processo_mte = " . $pcodigo . " and fl_vazio = 0
					union
					  select 'cancel' AS tipo
					    ,can.cd_candidato AS cd_candidato
					    ,can.codigo AS codigo
					    ,can.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,can.id_solicita_visto AS id_solicita_visto
					    ,can.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,7 AS ordem_temp
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_processo, dt_cad) as dt_ordem
					from processo_cancel can
					where codigo_processo_mte = " . $pcodigo . " and fl_vazio = 0
					union
					  select concat('generico_', id_tipoprocesso) AS tipo
					    ,gen.cd_candidato AS cd_candidato
					    ,gen.codigo AS codigo
					    ,gen.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,gen.id_solicita_visto AS id_solicita_visto
					    ,0 AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,10 AS ordem_temp
					    , dt_cad
					    , nu_servico
					    , coalesce(date_1, dt_cad) as dt_ordem
					from processo_generico gen
					where codigo_processo_mte = " . $pcodigo;
        $sql .= "  order by ordem, dt_ordem, ordem_temp asc";
        // $sql = "     select vProcesso.*, ifnull(sv.dt_solicitacao, '2099-12-31') dt_solicitacao";
        // $sql .= "      from vProcesso";
        // $sql .= "      left join solicita_visto sv on sv.id_solicita_visto = vProcesso.id_solicita_visto";
        // $sql .= "     where codigo_processo_mte = " . $pcodigo;
        // $sql .= "       and (tipo <> 'mte' or (tipo = 'mte' and vProcesso.nu_servico = 31)  )";
        // $sql .= "       and fl_vazio            = 0";
        // $sql .= "  order by dt_ordem, ordem asc";
        $rs = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo processos satelites');
        $processos = '';
        while ($rw = mysql_fetch_array($rs)) {
            $tela = cINTERFACE_PROCESSO::TelaProcesso($rw['tipo'], "view");
            if (get_class($tela) == 'cEDICAO') {
                $tela->mCampoNome['codigo']->mValor = $rw['codigo'];
                $processos = '<div class="processo" id="' . $rw['tipo'] . ':' . $rw['codigo'] . '">' . cINTERFACE_PROCESSO::FormProcesso($tela, true) . '</div>' . $processos;
            } else {
                $tela->codigo = $rw['codigo'];
                $processos = '<div class="processo" id="' . $rw['tipo'] . ':' . $rw['codigo'] . '">' . $tela->Edite() . '</div>' . $processos;
            }
            if ($rw['tipo'] == 'prorrog') {
                //
                // Carrega restabelecimentos da prorrogacao
                $sql = "     select *";
                $sql .= "      from processo_regcie";
                $sql .= "      left join solicita_visto sv on sv.id_solicita_visto = processo_regcie.id_solicita_visto";
                $sql .= "     where codigo_processo_prorrog = " . $rw['codigo'];
                $sql .= "       and fl_vazio            = 0";
                $rs_restab = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo processos satelites');
                while ($rw_restab = mysql_fetch_array($rs_restab)) {
                    $tela = cINTERFACE_PROCESSO::TelaProcesso("regcie", "view");
                    $tela->mCampoNome['codigo']->mValor = $rw_restab['codigo'];
                    $processos = cINTERFACE_PROCESSO::FormProcesso($tela, true) . $processos;
                }
            }
        }

        $ret = '<div id="processos">' . $processos . '</div>' . $ret;
        return $ret;
    }

    public function QtdOutrosServicos() {
        $sql = "     select ifnull(count(*), 0) qtd ";
        $sql .= "      from solicita_visto sv join servico s on s.nu_servico = sv.nu_servico join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto ";
        $sql .= "     where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        $sql .= "       and (  ID_TIPO_ACOMPANHAMENTO not in (1,3,4,5,6,8) or ID_TIPO_ACOMPANHAMENTO IS NULL   ";
        $sql .= "            or (ID_TIPO_ACOMPANHAMENTO =1 and sv.NU_SERVICO = 49)  )";
        $rs = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros processos');
        $ret = '';
        if ($rw = mysql_fetch_array($rs)) {
            return $rw['qtd'];
        } else {
            return 0;
        }
    }

    public function QtdOutrosVistos() {
        $sql = "     select ifnull(count(*), 0) qtd ";
        $sql .= "      from processo_mte";
        $sql .= "     where cd_candidato = " . $this->mNU_CANDIDATO;
        $sql .= "       and (fl_visto_atual = 0 or fl_visto_atual is null)";
        $sql .= "       and (fl_vazio = 0 or fl_vazio is null)";
        $sql .= "       and (nu_servico not in (49,31) or nu_servico is null)";
        $rs = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $ret = '';
        if ($rw = mysql_fetch_array($rs)) {
            return $rw['qtd'];
        } else {
            return 0;
        }
    }

    public function QtdOutrosProcessos() {
        $qtd = 0;
        $sql = "select count(*) from processo_prorrog where cd_candidato = " . $this->mNU_CANDIDATO . " and (fl_vazio = 0 or fl_vazio is null) and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        $sql = "select count(*) from processo_regcie where cd_candidato = " . $this->mNU_CANDIDATO . " and codigo_processo_prorrog is null and (fl_vazio = 0 or fl_vazio is null) and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        $sql = "select count(*) from processo_coleta where cd_candidato = " . $this->mNU_CANDIDATO . " and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        $sql = "select count(*) from processo_cancel where cd_candidato = " . $this->mNU_CANDIDATO . " and (fl_vazio = 0 or fl_vazio is null) and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        $sql = "select count(*) from processo_emiscie where cd_candidato = " . $this->mNU_CANDIDATO . " and (fl_vazio = 0 or fl_vazio is null) and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        return $qtd;
//		$sql = "     select ifnull(count(*), 0) qtd ";
//		$sql .= "      from vProcesso";
//		$sql .= "     where cd_candidato = " . $this->mNU_CANDIDATO;
//		$sql .= "       and (fl_vazio = 0 or fl_vazio is null)";
//		$sql .= "       and codigo_processo_mte is not null";
//		if ($this->mcodigo_processo_mte_atual != '' ){
//			$sql .= "       and codigo_processo_mte <> ".$this->mcodigo_processo_mte_atual;
//		}
//		$sql .= "       and tipo <> 'mte'";
//		$rs = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
//		$ret = '' ;
//		if ($rw = mysql_fetch_array($rs)) {
//			return $rw['qtd'];
//		}
//		else{
//			return 0;
//		}
    }

    public function QtdOutrosProcessosOrfaos() {
        $qtd = 0;
        $sql = "select count(*) from processo_prorrog where cd_candidato = " . $this->mNU_CANDIDATO . " and (fl_vazio = 0 or fl_vazio is null) and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        $sql = "select count(*) from processo_regcie where cd_candidato = " . $this->mNU_CANDIDATO . " and codigo_processo_prorrog is null and (fl_vazio = 0 or fl_vazio is null) and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        $sql = "select count(*) from processo_coleta where cd_candidato = " . $this->mNU_CANDIDATO . " and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        $sql = "select count(*) from processo_cancel where cd_candidato = " . $this->mNU_CANDIDATO . " and (fl_vazio = 0 or fl_vazio is null) and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        $sql = "select count(*) from processo_emiscie where cd_candidato = " . $this->mNU_CANDIDATO . " and (fl_vazio = 0 or fl_vazio is null) and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);
        $sql = "select count(*) from processo_mte where cd_candidato = " . $this->mNU_CANDIDATO . " and (codigo_processo_mte is null or codigo_processo_mte = codigo) and nu_servico in (49,31) and (fl_vazio = 0 or fl_vazio is null) and codigo_processo_mte is null";
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
        $rs = mysql_fetch_array($res);
        $qtd += intval($rs[0]);

        return $qtd;
//		$sql = "     select ifnull(count(*), 0) qtd ";
//		$sql .= "      from vProcesso";
//		$sql .= "     where cd_candidato = " . $this->mNU_CANDIDATO;
//		$sql .= "       and codigo_processo_prorrog is null";
//		$sql .= "       and (fl_vazio = 0 or fl_vazio is null)";
//		$sql .= "   and (    (tipo <> 'mte' and codigo_processo_mte is null)
//						  or (tipo  = 'mte' and (codigo_processo_mte is null or codigo_processo_mte = codigo) and nu_servico in (49,31) )  )";
//		$rs = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros vistos');
//		$ret = '' ;
//		if ($rw = mysql_fetch_array($rs)) {
//			return $rw['qtd'];
//		}
//		else{
//			return 0;
//		}
    }

    /**
     * Mostra os outros vistos do candidato
     * @param int $pcodigo Chave do processo mte
     * @
     */
    public function FormOutrosProcessos() {
        //
        // Carrega processos do visto selecionado
        $sql = "     select vordemservico.*, pm.observacao ";
        $sql .= "      from vordemservico left join processo_mte pm on pm.id_solicita_visto = vordemservico.id_solicita_visto";
        $sql .= "     where vordemservico.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        $sql .= "       and (  vordemservico.ID_TIPO_ACOMPANHAMENTO not in (1,3,4,5,6,8) or vordemservico.ID_TIPO_ACOMPANHAMENTO IS NULL   ";
        $sql .= "            or (vordemservico.ID_TIPO_ACOMPANHAMENTO =1 and vordemservico.NU_SERVICO = 49)  )";
        $sql .= "       and vordemservico.NU_SERVICO is not null";
        $sql .= "  order by  vordemservico.NU_SOLICITACAO DESC";
        $rs = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo outros processos');
        $ret = '';
        while ($rw = mysql_fetch_array($rs)) {
            switch ($rw['ID_TIPO_ACOMPANHAMENTO']) {
                case '9':
                    $nomeTela = 'CANDIDATO_CPF';
                    break;
                case '10':
                    $nomeTela = 'CANDIDATO_CNH';
                    break;
                case '11':
                    $nomeTela = 'CANDIDATO_CTPS';
                    break;
                default:
                    $nomeTela = 'processo_view';
            }
            $tela = new cEDICAO($nomeTela);
            $tela->CarregarConfiguracao();
            switch ($rw['ID_TIPO_ACOMPANHAMENTO']) {
                case '9':
                case '10':
                case '11':
                    $tela->ProtejaTodosCampos();
                    $tipo = $nomeTela;
                    $tela->mCampoNome['id_solicita_visto']->mValor = $rw['id_solicita_visto'];
                    $tela->mCampoNome['de_observacao']->mValor = $rw['de_observacao'];
                    $tela->RecupereRegistro();
                    break;
                default:
                    $tipo = 'outros';
                    $tela->mCampoNome['id_solicita_visto']->mValor = $rw['id_solicita_visto'];
                    $tela->mCampoNome['de_observacao']->mValor = $rw['de_observacao'];
            }
            if ($rw['NU_SERVICO'] == 49) {
                if (trim($tela->mCampoNome['de_observacao']->mValor) != '') {
                    $br = '<br/>';
                }
                $tela->mCampoNome['de_observacao']->mValor = $tela->mCampoNome['de_observacao']->mValor . $br . $rw['observacao'];
            }

            $titulo = $rw['NO_SERVICO'] . ' (<a href="os_DadosOS.php?ID_SOLICITA_VISTO=' . $rw['id_solicita_visto'] . '">OS ' . $rw['nu_solicitacao'] . '</a>)';

            $ret .= cINTERFACE::formEdicao($tela);
            $acoesHTML = '';
            $acoes = array();
            $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Alterar", "ALTERAR", "Alterar os dados do processo", "");
            $parametros = 'id_solicita_visto=' . $rw['id_solicita_visto'];
            $classe = 'cINTERFACE_OS';
            $metodo = 'FormProcesso';
            $xtitulo = '';
            $acao->mOnClick = "carregaFormPopup('" . $parametros . "&tipo=" . $tipo . "&modo=edit')";
            $acoes[] = $acao;
            $acoesHTML = cINTERFACE::RenderizeBarraAcoesTitulo($acoes);
            $ret .= '<div class="ui-widget ui-widget-header tituloProcesso">' . $titulo . $acoesHTML . '</div>';
            $estilo = '';
            $ret .= '<div class="detalheProcesso ' . $estilo . '">';
            $ret .= cINTERFACE::RenderizeEdicaoComPaineis($tela, 2);
            $ret .= '</table>';
            $ret .= '</div>';
            $ret .= '</form>';
        }
        return $ret;
    }

    /**
     * Gera o painel de edição das observações dos processos
     * @param array $pPOST $_POST
     * @param array $pGET	$_GET
     */
    public function FormObservacaoProcessos($pPOST, $pGET) {
        $visto = $this->VistoAtual();
        $ret = '';
        $nomeTela = 'processo_mte_observacao';
        $tela = new cEDICAO($nomeTela);
        $tela->CarregarConfiguracao();
        $tela->mCampoNome['codigo']->mValor = $this->mcodigo_processo_mte_atual;
        $tela->RecupereRegistro();
        cINTERFACE::RecupereValoresEdicao($tela, '', $pGET);
//		var_dump($tela);
        if (isset($visto['autorizacao'])) {
            $tela->mCampoNome['situacao_prazo_estada']->mValor = $visto['autorizacao']->get_situacao_prazo_estada();
            $tela->mCampoNome['situacao_texto']->mValor = $visto['autorizacao']->get_situacao_texto() . '<br/>Porque?: ' . $visto['autorizacao']->get_situacao_origem();
        }
        $ret .= cINTERFACE::formEdicao($tela);
        $ret .= cINTERFACE::RenderizeEdicaoComPaineis($tela);
        $ret .= '</table>';
        $acoes = array();
        $acoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Clique para salvar a observação");
        $ret .= cINTERFACE::RenderizeBarraAcoesRodape($acoes);
        $ret .= '</form>';
        return $ret;
    }

    public static function FormProcessosOrfaos($pNU_CANDIDATO) {
        $sql = "  select 'mte' AS tipo
					    ,mte.cd_candidato AS cd_candidato
					    ,mte.codigo AS codigo
					    ,null as codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,mte.id_solicita_visto AS id_solicita_visto
					    ,mte.fl_vazio AS fl_vazio
					    ,1 AS ordem
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_mte mte
					where cd_candidato = " . $pNU_CANDIDATO . " and fl_vazio = 0
						and (codigo_processo_mte is null or codigo_processo_mte = codigo)
						and nu_servico in (49,31)
					union
					select 'coleta' AS tipo
					    ,col.cd_candidato AS cd_candidato
					    ,col.codigo AS codigo
					    ,col.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,col.id_solicita_visto AS id_solicita_visto
					    ,0 AS fl_vazio
					    ,2 AS ordem
					    , dt_cad
					    , nu_servico
					    , null as dt_ordem
					  from processo_coleta col
					where cd_candidato = " . $pNU_CANDIDATO . " and codigo_processo_mte is null
					union
					  select 'regcie' AS tipo
					    ,reg.cd_candidato AS cd_candidato
					    ,reg.codigo AS codigo
					    ,reg.codigo_processo_mte AS codigo_processo_mte
					    ,reg.codigo_processo_prorrog AS codigo_processo_prorrog
					    ,reg.id_solicita_visto AS id_solicita_visto
					    ,reg.fl_vazio AS fl_vazio
					    ,(case reg.nu_servico when 58 then 6 else 3 end) AS ordem
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_regcie reg
					where cd_candidato = " . $pNU_CANDIDATO . " and codigo_processo_mte is null and fl_vazio = 0
					union
					  select 'emiscie' AS tipo
					    ,emi.cd_candidato AS cd_candidato
					    ,emi.codigo AS codigo
					    ,emi.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,emi.id_solicita_visto AS id_solicita_visto
					    ,emi.fl_vazio AS fl_vazio
					    ,4 AS ordem
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_emissao, dt_cad) as dt_ordem
					  from processo_emiscie emi
					where cd_candidato = " . $pNU_CANDIDATO . " and codigo_processo_mte is null and fl_vazio = 0
					union
					  select 'prorrog' AS tipo
					    ,pro.cd_candidato AS cd_candidato
					    ,pro.codigo AS codigo
					    ,pro.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,pro.id_solicita_visto AS id_solicita_visto
					    ,pro.fl_vazio AS fl_vazio
					    ,5 AS ordem
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_prorrog pro
					where cd_candidato = " . $pNU_CANDIDATO . " and codigo_processo_mte is null and fl_vazio = 0
					union
					  select 'cancel' AS tipo
					    ,can.cd_candidato AS cd_candidato
					    ,can.codigo AS codigo
					    ,can.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,can.id_solicita_visto AS id_solicita_visto
					    ,can.fl_vazio AS fl_vazio
					    ,7 AS ordem
					    , dt_cad
					    , nu_servico
					    , coalesce(dt_processo, dt_cad) as dt_ordem
					from processo_cancel can
					where cd_candidato = " . $pNU_CANDIDATO . " and codigo_processo_mte is null and fl_vazio = 0";
        $sql .= " ORDER BY codigo desc";
        // $sql = " SELECT * from vProcesso ";
        // $sql .= " WHERE cd_candidato = ".$pNU_CANDIDATO;
        // $sql .= "   AND codigo_processo_prorrog is null";
        // $sql .= "   and (fl_vazio = 0 or fl_vazio is null)";
        // $sql .= "   and (    (tipo <> 'mte' and codigo_processo_mte is null)
        // 				  or (tipo  = 'mte' and (codigo_processo_mte is null or codigo_processo_mte = codigo) and nu_servico in (49,31) )  )";
        // $sql .= " ORDER BY codigo desc";
        $orfaos = '';
        if ($rs = mysql_query($sql)) {
            while ($rw = mysql_fetch_array($rs)) {
                $tela = cINTERFACE_PROCESSO::TelaProcesso($rw['tipo'], 'view');
                $tela->mCampoNome['codigo']->mValor = $rw['codigo'];
                $orfaos .= cINTERFACE_PROCESSO::FormProcesso($tela);
            }
        }
        return $orfaos;
    }

    public static function FormHistoricoVistoDeOs($pid_solicita_visto) {
        $ret = '';

        $OS = new cORDEMSERVICO();
        $OS->Recuperar($pid_solicita_visto);
        switch ($OS->mID_TIPO_ACOMPANHAMENTO) {
            case 3:    // Prorrogação
                $nomeTabela = 'prorrog';
                break;
            case 4:    // Registro CIE
                $nomeTabela = 'regcie';
                break;
            case 5:    // Emissão CIE
                $nomeTabela = 'emiscie';
                break;
            case 6:    // Cancelamento
                $nomeTabela = 'cancel';
                break;
            case 8:    // Coleta visto
                $nomeTabela = 'coleta';
                break;
            case 12:    // Coleta visto
                $nomeTabela = 'coleta_cie';
                break;
        }
        // Obtem o código do processo e do visto a partir da OS
        $sql = "select codigo, codigo_processo_mte from processo_" . $nomeTabela . " where id_solicita_visto = " . $pid_solicita_visto;
        $rs = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__ . '->Obtendo processo mte do processo');
        if ($rw = mysql_fetch_array($rs)) {
            $codigo = $rw['codigo'];
            $codigo_mte = $rw['codigo_processo_mte'];
            // Monta form do visto
            $tela = cINTERFACE_PROCESSO::TelaProcesso("mte", "view");
            $tela->mCampoNome['codigo']->mValor = $codigo_mte;
            $mte = cINTERFACE_PROCESSO::FormProcesso($tela, true);
            if ($codigo_mte != '') {
                //
                // Carrega processos do visto selecionado
                $sql = "  select 'coleta' AS tipo
							    ,col.cd_candidato AS cd_candidato
							    ,col.codigo AS codigo
							    ,col.codigo_processo_mte AS codigo_processo_mte
							    ,NULL AS codigo_processo_prorrog
							    ,col.id_solicita_visto AS id_solicita_visto
							    ,0 AS fl_vazio
							    ,2 AS ordem
							    , dt_cad
							    , nu_servico
							    , null as dt_ordem
							  from processo_coleta col
							where codigo_processo_mte = " . $codigo_mte . "
							union
							  select 'regcie' AS tipo
							    ,reg.cd_candidato AS cd_candidato
							    ,reg.codigo AS codigo
							    ,reg.codigo_processo_mte AS codigo_processo_mte
							    ,reg.codigo_processo_prorrog AS codigo_processo_prorrog
							    ,reg.id_solicita_visto AS id_solicita_visto
							    ,reg.fl_vazio AS fl_vazio
							    ,(case reg.nu_servico when 58 then 6 else 3 end) AS ordem
							    , dt_cad
							    , nu_servico
							    , coalesce(dt_requerimento, dt_cad) as dt_ordem
							  from processo_regcie reg
							where codigo_processo_mte = " . $codigo_mte . " and fl_vazio = 0
							union
							  select 'emiscie' AS tipo
							    ,emi.cd_candidato AS cd_candidato
							    ,emi.codigo AS codigo
							    ,emi.codigo_processo_mte AS codigo_processo_mte
							    ,NULL AS codigo_processo_prorrog
							    ,emi.id_solicita_visto AS id_solicita_visto
							    ,emi.fl_vazio AS fl_vazio
							    ,4 AS ordem
							    , dt_cad
							    , nu_servico
							    , coalesce(dt_emissao, dt_cad) as dt_ordem
							  from processo_emiscie emi
							where codigo_processo_mte = " . $codigo_mte . " and fl_vazio = 0
							union
							  select 'prorrog' AS tipo
							    ,pro.cd_candidato AS cd_candidato
							    ,pro.codigo AS codigo
							    ,pro.codigo_processo_mte AS codigo_processo_mte
							    ,NULL AS codigo_processo_prorrog
							    ,pro.id_solicita_visto AS id_solicita_visto
							    ,pro.fl_vazio AS fl_vazio
							    ,5 AS ordem
							    , dt_cad
							    , nu_servico
							    , coalesce(dt_requerimento, dt_cad) as dt_ordem
							  from processo_prorrog pro
							where codigo_processo_mte = " . $codigo_mte . " and fl_vazio = 0
							union
							  select 'cancel' AS tipo
							    ,can.cd_candidato AS cd_candidato
							    ,can.codigo AS codigo
							    ,can.codigo_processo_mte AS codigo_processo_mte
							    ,NULL AS codigo_processo_prorrog
							    ,can.id_solicita_visto AS id_solicita_visto
							    ,can.fl_vazio AS fl_vazio
							    ,7 AS ordem
							    , dt_cad
							    , nu_servico
							    , coalesce(dt_processo, dt_cad) as dt_ordem
							from processo_cancel can
							where codigo_processo_mte = " . $codigo_mte . " and fl_vazio = 0
							";
                $sql .= "  order by dt_ordem, ordem asc";
                // $sql = "     select vProcesso.*, ifnull(sv.dt_solicitacao, '2099-12-31') dt_solicitacao";
                // $sql .= "      from vProcesso";
                // $sql .= "      left join solicita_visto sv on sv.id_solicita_visto = vProcesso.id_solicita_visto";
                // $sql .= "     where codigo_processo_mte = ".$codigo_mte;
                // $sql .= "       and tipo <> 'mte'";
                // $sql .= "       and (( codigo <> ".$codigo." and tipo = '".$nomeTabela."')
                // 				OR ( tipo <> '".$nomeTabela."'))";
                // $sql .= "       and fl_vazio            = 0";
                // $sql .= "  order by dt_solicitacao desc, ordem desc ";
                $rs = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__ . '->Obtendo processos satelites');

                while ($rw = mysql_fetch_array($rs)) {
                    if (($rw['codigo'] != $codigo && $rw['tipo'] == $nomeTabela) || $rw['tipo'] != $nomeTabela) {
                        $tela = cINTERFACE_PROCESSO::TelaProcesso($rw['tipo'], "view");
                        $tela->mCampoNome['codigo']->mValor = $rw['codigo'];
                        $ret .= cINTERFACE_PROCESSO::FormProcesso($tela, true);
                    }
                }
                $ret .= $mte;
            } else {
                $ret = 'Esse processo não está associado a nenhum visto';
            }
        } else {
            $codigo_mte = 0;
            $ret .= "Nenhuma solicitação encontrada.";
        }
        return $ret;
    }

    public function MontaHistorico($pOrdemServico = null) {
        $sql = "     select AC.id_solicita_visto";
        $sql .= "         , ifnull(S.ID_TIPO_ACOMPANHAMENTO, 0) ID_TIPO_ACOMPANHAMENTO ";
        $sql .= "         , AC.NU_SOLICITACAO";
        $sql .= "         , sv.NU_SERVICO";
        $sql .= "         , S.NO_SERVICO_RESUMIDO";
        $sql .= "      from CANDIDATO C";
        $sql .= "      join AUTORIZACAO_CANDIDATO AC	ON AC.NU_CANDIDATO = C.NU_CANDIDATO";
        $sql .= "      join solicita_visto sv			ON sv.id_solicita_visto = AC.id_solicita_visto";
        $sql .= "      left join SERVICO S 					ON S.NU_SERVICO = sv.NU_SERVICO";
        $sql .= "     where C.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        if (isset($pOrdemServico)) {
            $sql .= "       and AC.id_solicita_visto <>" . $pOrdemServico->mID_SOLICITA_VISTO;
        }
        $sql .= "  order by sv.nu_solicitacao desc";
        print '<!--SQL=' . $sql . '-->';
        $rs = mysql_query($sql);
        $ret = '';
        //$ret .= $sql;
        $parar = false;
        while (($rw = mysql_fetch_array($rs)) && (!$parar)) {
            $ret .= self::ExibeSolicitacao($rw['id_solicita_visto'], $this->mNU_CANDIDATO, $rw['NU_SOLICITACAO'], $rw['ID_TIPO_ACOMPANHAMENTO'], true, true, $rw['NU_SERVICO'], $rw['NO_SERVICO_RESUMIDO']);
            if ($rw['ID_TIPO_ACOMPANHAMENTO'] == '1' || $rw['NU_SERVICO'] == '0' || $rw['NU_SERVICO'] == '') {
                $parar = true;
            }
        }
        if ($ret == '') {
            $ret .= "Nenhuma solicitação encontrada.";
        }
        return $ret;
    }

    public static function ExibeSolicitacao($pid_solicita_visto, $pNU_CANDIDATO, $pNU_SOLICITACAO, $pID_TIPO_ACOMPANHAMENTO, $pExibirAlerta = false, $pExibirLinkOs = true, $pNU_SERVICO = 0, $pNO_SERVICO_RESUMIDO = '') {
        $ret = '';
        if ($pExibirLinkOs) {
            if ($pNU_SOLICITACAO > 0) {
                $link = '(<a href="os_DadosOS.php?ID_SOLICITA_VISTO=' . $pid_solicita_visto . '">OS ' . $pNU_SOLICITACAO . '</a>)';
            } else {
                $link = '(<a href="os_Processo.php?ID_SOLICITA_VISTO=' . $pid_solicita_visto . '&NU_CANDIDATO=' . $pNU_CANDIDATO . '">OS ' . $pNU_SOLICITACAO . '</a>)';
            }
        } else {
            $link = '(OS ' . $pNU_SOLICITACAO . ')';
        }

        if ($pID_TIPO_ACOMPANHAMENTO == 1) {
            $nomeTela = 'processo_mte_view';
            $titulo = "Processo MTE  " . $link . " - " . $pNO_SERVICO_RESUMIDO;
            $tela = new cEDICAO($nomeTela);
            $tela->CarregarConfiguracao();
            $tela->mCampoNome['id_solicita_visto']->mValor = $pid_solicita_visto;
            $tela->mCampoNome['cd_candidato']->mValor = $pNU_CANDIDATO;
        } elseif ($pID_TIPO_ACOMPANHAMENTO == 3) {
            $nomeTela = 'processo_prorrog_view';
            $titulo = "Prorrogação  " . $link . " - " . $pNO_SERVICO_RESUMIDO;
            $tela = new cEDICAO($nomeTela);
            $tela->CarregarConfiguracao();
            $tela->mCampoNome['id_solicita_visto']->mValor = $pid_solicita_visto;
            $tela->mCampoNome['cd_candidato']->mValor = $pNU_CANDIDATO;
        } elseif ($pID_TIPO_ACOMPANHAMENTO == 4) {
            $nomeTela = 'processo_regcie_view';
            $titulo = "Registro CIE  " . $link . " - " . $pNO_SERVICO_RESUMIDO;
            $tela = new cEDICAO($nomeTela);
            $tela->CarregarConfiguracao();
            $tela->mCampoNome['id_solicita_visto']->mValor = $pid_solicita_visto;
            $tela->mCampoNome['cd_candidato']->mValor = $pNU_CANDIDATO;
        } elseif ($pID_TIPO_ACOMPANHAMENTO == 5) {
            $nomeTela = 'processo_emiscie_view';
            $titulo = "Emissão CIE  " . $link . " - " . $pNO_SERVICO_RESUMIDO;
            $tela = new cEDICAO($nomeTela);
            $tela->CarregarConfiguracao();
            $tela->mCampoNome['id_solicita_visto']->mValor = $pid_solicita_visto;
            $tela->mCampoNome['cd_candidato']->mValor = $pNU_CANDIDATO;
        } elseif ($pID_TIPO_ACOMPANHAMENTO == 6) {
            $nomeTela = 'processo_cancel_view';
            $titulo = "Cancelamento  " . $link . " - " . $pNO_SERVICO_RESUMIDO;
            $tela = new cEDICAO($nomeTela);
            $tela->CarregarConfiguracao();
            $tela->mCampoNome['id_solicita_visto']->mValor = $pid_solicita_visto;
            $tela->mCampoNome['cd_candidato']->mValor = $pNU_CANDIDATO;
        } else {
            if ($pNU_SERVICO == '0' || $pNU_SERVICO == '') {
                $nomeTela = 'SOLICITACAO_ANTIGA_view';
                $titulo = "Versão antiga  " . $link;
                $tela = new cEDICAO($nomeTela);
                $tela->CarregarConfiguracao();
                $tela->mCampoNome['id_solicita_visto']->mValor = $pid_solicita_visto;
                $tela->mCampoNome['NU_CANDIDATO']->mValor = $pNU_CANDIDATO;
            } else {
                $nomeTela = 'processo_view';
                $titulo = '"' . $pNO_SERVICO_RESUMIDO . '" ' . $link;
                $tela = new cEDICAO($nomeTela);
                $tela->CarregarConfiguracao();
                $tela->mCampoNome['id_solicita_visto']->mValor = $pid_solicita_visto;
                $tela->mCampoNome['NU_CANDIDATO']->mValor = $pNU_CANDIDATO;
            }
        }

        $tela->RecupereRegistro();
        $ret .= cINTERFACE::RenderizeEdicaoComPaineis($tela, 2);
        $ret .= '</table>';
        return $ret;
    }

    /**
     *
     * Gera a lista de formulários
     */
    public static function ListaDeFormularios() {
        $acCand->mCampoNome['id_solicita_visto']->mValor = $OrdemServico->mID_SOLICITA_VISTO;
        $acCand->mCampoNome['NU_CANDIDATO']->mValor = $ids[$i];
        $acCand->RecupereRegistro();

        $tabs .= '<li><a href="#cand' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . $acCand->mCampoNome['NOME_COMPLETO']->mValor . '</a></li>';
        $divs .= '<div id="cand' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
        $divs .= '<form id="form' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" method="post">' . "\n";
        //$divs .= '<input type="hidden" id="form" name="IdentificadorCampos" value="'.$acCand->mCampoNome['NU_CANDIDATO']->mValor.'"/>';
        $divs .= cINTERFACE::RenderizeCampo($acCand->mCampoCtrlEnvio);
        $divs .= cINTERFACE::inputHidden('CMP_' . $acCand->mCampoNome["id_solicita_visto"]->mCampoBD, $acCand->mCampoNome["id_solicita_visto"]->mValor);
        $divs .= cINTERFACE::inputHidden('CMP_' . $acCand->mCampoNome["NU_CANDIDATO"]->mCampoBD, $acCand->mCampoNome["NU_CANDIDATO"]->mValor);
        $divs .= cINTERFACE::inputHidden('tabsIntx_selected', $i);

        $divs .= cINTERFACE::RenderizeEdicao($acCand);
        $divs .= '</table>' . "\n";
        if (!$OrdemServico->mReadonly) {
            $divs .= '<center><input type="submit" value="Salvar" style="width:auto"/></center>' . "\n";
        }
        $divs .= '</form>' . "\n";
        $divs .= '<div class="duplasubTitulo">Dependentes</div>' . "\n";
        $divs .= '<div>';
        $divs .= '<form method="post" action="/intranet/geral/dependentes.php">';
        $divs .= '<input type="hidden" name="idCandidato" value="' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '"/>';
        $divs .= '<input type="hidden" name="idEmpresa" value="' . $acCand->mCampoNome['NU_EMPRESA']->mValor . '"/>';
        $divs .= '<input type="submit" value="Admnistrar dependentes"/>';
        $divs .= '</form>';
        $divs .= '</div>';

        $divs .= '<div style="clear:both;overflow: auto;">' . "\n";
        $divs .= '<div class="duplasubTitulo">Arquivos do candidato</div>' . "\n";
        $divs .= '<div id="arquivosListadosVisualizacao' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" style="float:right;border:solid 1px #999999;padding:3px;text-align:center;">' . "\n";
        $divs .= '</div>' . "\n";
        $divs .= '<div>' . "\n";
        $divs .= '	<form id="novoArquivo' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" action="arquivoPost.php" enctype="multipart/form-data" target="_blank" method="post">' . "\n";
        $divs .= cINTERFACE::inputHidden("formArquivo", "1");
        $divs .= cINTERFACE::inputHidden("NU_CANDIDATO", $acCand->mCampoNome['NU_CANDIDATO']->mValor);
        $divs .= cINTERFACE::inputHidden("NU_EMPRESA", $acCand->mCampoNome['NU_EMPRESA']->mValor);
        $divs .= '	<div id="arquivosListados' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
        $divs .= '	</div>' . "\n";
        $divs .= '	</form>' . "\n";
        $divs .= '</div>' . "\n";

        $divs .= '</div><br/>' . "\n";

        $divs .= '</div>';
    }

    /**
     * Obtem os parametros necessários para a execução dos serviços
     */
    public function ObterParametros($pPOST, $pGET) {
        if (isset($pGET['NU_CANDIDATO'])) {
            $this->mNU_CANDIDATO = $pGET['NU_CANDIDATO'];
        }
    }

    /**
     * Gera o formulário para digitar os dados de um novo candidato
     * @param array $pPOST
     * @param array $pGET
     */
    public function Formcandidato_new($pPOST, $pGET) {
        $tela = new cEDICAO("candidato_new");
        $tela->CarregarConfiguracao();
        cINTERFACE::RecupereValoresEdicao($tela, $pPOST, $pGET);
        $acao = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Clique para salvar o candidato");
        $ret = cINTERFACE::formEdicao($tela);
        $ret.= cINTERFACE::RenderizeEdicaoComPaineis($tela);
        $ret.= '</table>';
        $ret.= '<div class="barraAcoes">' . cINTERFACE::RenderizeAcao($acao) . '</div>';
        $ret.= '</form>';
        return $ret;
    }

    /**
     * Trata o formulário de novo candidato
     * @param cEDICAO $pTela
     * @param array $pPOST
     * @param array $pFILES
     * @param array $pSESSION
     * @static
     */
    public static function Postcandidato_new($pTela, $pPOST, $pFILES, &$pSESSION) {
        $cand = new cCANDIDATO();
        cINTERFACE::MapeiaEdicaoClasse($pTela, $cand);
        $cand->Salve_Novo();
        $pSESSION['msg'] = "Candidato salvo com sucesso.";
        session_write_close();
        header('Location:detalheCandidatoAuto.php?NU_CANDIDATO=' . $cand->mNU_CANDIDATO);
    }

    public static function ComboParaTrocaDeCandidato($pNU_CANDIDATO, $pPesquisar) {
        $combo = new cCOMBO(cCOMBO::cmbCANDIDATO_PROCESSO);
        $combo->mExtra = " where NOME_COMPLETO like '%" . $pPesquisar . "%' and NU_CANDIDATO <>" . $pNU_CANDIDATO;
        $ret = $combo->HTML('NU_CANDIDATO', '');
        return $ret;
    }

    public static function MigrarProcessoAjax() {
        $msg = '';
        $retorno = '';
        $tmpl = new cTEMPLATE_CANDIDATO_MIGRAR_PROCESSO();
        $tmpl->CarregueDoHTTP();
        $cand = new cCANDIDATO();
        $tmpl->mAcoes['PESQUISAR']->mControle = __CLASS__;
        $tmpl->mAcoes['PESQUISAR']->mMetodo = __FUNCTION__;
        $tmpl->mAcoes['MIGRAR']->mControle = __CLASS__;
        $tmpl->mAcoes['MIGRAR']->mMetodo = __FUNCTION__;
        $cand->Recuperar($tmpl->mCampo['NU_CANDIDATO_ORIGEM']->mValor);
        $tmpl->mCampo['NOME_COMPLETO_ORIG']->mValor = $cand->mNOME_COMPLETO . ' (' . $cand->mNU_CANDIDATO . ')';
        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mCombo->mExtra = " where 1=2";
            $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mCombo->mValorDefault = '(informe o nome...)';
            switch ($tmpl->mAcaoPostada) {
                case 'PESQUISAR':
                    if ($tmpl->mCampo['NOME_COMPLETO_DESTINO']->mValor == '') {
                        $msg = "Informe o nome do candidato destino para poder selecionar";
                        $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mCombo->mExtra = " where 1=2";
                        $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mCombo->mValorDefault = '(informe o nome...)';
                    } else {
                        $nome = utf8_decode(str_replace(' ', '%', trim($tmpl->mCampo['NOME_COMPLETO_DESTINO']->mValor)));
                        $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mCombo->mExtra = " where NOME_COMPLETO like '%" . $nome . "%' and NU_CANDIDATO <>" . $tmpl->mCampo['NU_CANDIDATO_ORIGEM']->mValor;
                        $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mCombo->mPermiteDefault = false;
                    }
                    $retorno = '-1';
                    break;
                case 'MIGRAR':
                    if ($tmpl->mCampo['NU_CANDIDATO_DESTINO']->mValor == '') {
                        $msg = 'Selecione um candidato para enviar esse processo';
                        $retorno = '-1';
                    } else {
                        try {
                            $pMigrarArquivos = $tmpl->mCampo['MIGRAR_ARQUIVOS']->mValor;
                            // Obtem os candidatos
                            $cand_origem = new cCANDIDATO();
                            $cand_origem->Recuperar($tmpl->mCampo['NU_CANDIDATO_ORIGEM']->mValor);
                            $cand_destino = new cCANDIDATO();
                            $cand_destino->Recuperar($tmpl->mCampo['NU_CANDIDATO_DESTINO']->mValor);
                            // Obtem o processo
                            $entidade = 'c' . $tmpl->mCampo['tabela']->mValor;
                            $processo = new $entidade();
                            $processo->RecupereSe($tmpl->mCampo['codigo']->mValor);
                            // Se o processo é de OS, migra pela OS, senão migra o processo avulso
                            cAMBIENTE::$db_pdo->beginTransaction();
                            if (intval($processo->mid_solicita_visto) > 0) {
                                $os = new cORDEMSERVICO();
                                $os->mID_SOLICITA_VISTO = $processo->mid_solicita_visto;
                                $os->Migre($cand_origem, $cand_destino, $pMigrarArquivos, $tmpl->mCampo['MIGRAR_ASSOCIADOS']->mValor, $tmpl->getValorCampo('tabela'));
                            } else {
                                if ($tmpl->mCampo['tabela']->mValor == 'processo_mte') {
                                    $processo->Migre($cand_origem, $cand_destino, $tmpl->mCampo['MIGRAR_ASSOCIADOS']->mValor);
                                } else {
                                    $processo->Migre($cand_origem, $cand_destino);
                                }
                            }
                            cAMBIENTE::$db_pdo->commit();

//			cprocesso::MigraProcessoEntreCandidatos($tmpl->mCampo['codigo']->mValor, $tmpl->mCampo['tabela']->mValor, $tmpl->mCampo['NU_CANDIDATO_ORIGEM']->mValor, $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mValor,$tmpl->mCampo['MIGRAR_ARQUIVOS']->mValor);
//			if ($tmpl->mCampo['MIGRAR_ARQUIVOS']->mValor == "1"){
//                          $cand->MigreArquivosParaOutroCandidato($tmpl->mCampo['codigo']->mValor, $tmpl->mCampo['tabela']->mValor, $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mValor);
//			}
                            $msg = "Processo migrado com sucesso";
                            $tmpl->mCampo[cTEMPLATE::CMP_ACAO_SUCESSO]->mValor = cTEMPLATE::CMP_ACAO_SUCESSO_FECHAR;
                        } catch (cERRO_SQL $e) {
                            cAMBIENTE::$db_pdo->rollBack();
                            $retorno = '-1';
                            $msg = "Não foi possível executar a solicitação. Encaminhe essas informações ao suporte para agilizar a resolução do problema:\n" . htmlentities($e->mMsg);
                        } catch (Exception $e) {
                            cAMBIENTE::$db_pdo->rollBack();
                            $retorno = '-1';
                            $msg = htmlentities($e->getMessage());
                        }
                    }
                    break;
                default:
                    $retorno = '-1';
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {
            $tmpl->mCampo['MIGRAR_ARQUIVOS']->mValor = "1";
            $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mCombo->mExtra = " where 1=2";
            $tmpl->mCampo['NU_CANDIDATO_DESTINO']->mCombo->mValorDefault = '(informe o nome...)';
        }
        if ($tmpl->mCampo['tabela']->mValor == 'processo_mte') {
            $tmpl->mCampo['MIGRAR_ASSOCIADOS']->mVisivel = true;
        } else {
            $tmpl->mCampo['MIGRAR_ASSOCIADOS']->mVisivel = false;
        }
        $tmpl->mCampo[cTEMPLATE::CMP_MSG]->mValor = $msg;
        $conteudo = cINTERFACE::RenderizeTemplate($tmpl);
        return $conteudo;
    }

    public static function InformacoesCandidatoExterno() {
        $msg = '';
        $retorno = '';
        cINTERFACE_AMBIENTE::ControleLinguagem();
        $tmpl = new cTEMPLATE_CANDIDATO_EXTERNO();
        $tmpl->CarregueDoHTTP();
        //var_dump($tmpl->mCampo);
        $candOrig = new cCANDIDATO();
        $candOrig->Recuperar($tmpl->mCampo['NU_CANDIDATO']->mValor);
        $cand = new cCANDIDATO();
        $tmpl->mAcoes['SALVAR']->mControle = __CLASS__;
        $tmpl->mAcoes['SALVAR']->mMetodo = __FUNCTION__;
        $cand->Recuperar_Externo($tmpl->mCampo['NU_CANDIDATO']->mValor);
        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                case 'SALVAR_E_FECHAR':
                    try {
                        cINTERFACE::MapeiaEdicaoClasse($tmpl, $cand);
                        $cand->AtualizarExterno();
                        if ($tmpl->mAcaoPostada == 'SALVAR_E_FECHAR') {

                        }
                        $msg = "Data updated sucessfully!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {
            $tmpl->CarregueDoOBJETO($cand);
            $tmpl->mCampo['DT_NASCIMENTO']->mValor = cBANCO::DataFmt($cand->mDT_NASCIMENTO);
            $tmpl->mCampo['DT_EMISSAO_PASSAPORTE']->mValor = cBANCO::DataFmt($cand->mDT_EMISSAO_PASSAPORTE);
            $tmpl->mCampo['DT_VALIDADE_PASSAPORTE']->mValor = cBANCO::DataFmt($cand->mDT_VALIDADE_PASSAPORTE);
            if (intval($candOrig->mFL_ATUALIZACAO_HABILITADA) == 0) {
                $tmpl->ProtejaTodosCampos();
            }
        }
        cHTTP::$SESSION['msg'] = $msg;
        $conteudo = cINTERFACE::RenderizeTemplate($tmpl);
        return $conteudo;
    }

    public static function ExperienciaProfissional_Edit() {
        $msg = '';
        $retorno = '';
        $tmpl = new cTEMPLATE_CANDIDATO_EXPPROF_ADD();
        $tmpl->CarregueDoHTTP();
        //var_dump($tmpl->mCampo);
        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        $exp_pro = new cexperiencia_profissional();
                        cINTERFACE::MapeiaEdicaoClasse($tmpl, $exp_pro);
                        $exp_pro->Atualizar();
                        $cand = new cCANDIDATO();
                        $cand->mNU_CANDIDATO = $tmpl->mCampo['NU_CANDIDATO']->mValor;
                        $cand->mTE_EXPERIENCIA_ANTERIOR = $tmpl->mCampo['TE_EXPERIENCIA_ANTERIOR']->mValor;
                        $cand->AtualizarExperienciaAnterior();
                        $msg = "Data updated successfully!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {

        }
        cHTTP::$SESSION['msg'] = $msg;
        $conteudo = cINTERFACE::RenderizeTemplate($tmpl);
        return $conteudo;
    }

    public static function Liste_ExperienciaProfissional() {
        $ret = '';
        $exp = new cTEMPLATE_CANDIDATO_EXPPROF();
        $tela = new cTEMPLATE_CANDIDATO_EXPPROF_VIEW();
        $tmpl = new cTEMPLATE_CANDIDATO_EXPPROF_ADD();
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $tmpl->CarregueDoHTTP();
        $tela->CarregueDoHTTP();
        $exp->CarregueDoHTTP();
        // Configura tela de acordo com as características do usuário recebido.
        $cand = new cCANDIDATO();
        $cand->Recuperar($tela->mCampo['NU_CANDIDATO']->mValor);
        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        $exp_pro = new cexperiencia_profissional();
                        cINTERFACE::MapeiaEdicaoClasse($tmpl, $exp_pro);
                        $exp_pro->Salvar();
                        $msg = "Previous experience added sucessfully!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
            $tmpl->mCampo['epro_no_companhia']->mValor = '';
            $tmpl->mCampo['epro_no_funcao']->mValor = '';
            $tmpl->mCampo['epro_no_periodo']->mValor = '';
            $tmpl->mCampo['epro_tx_atribuicoes']->mValor = '';
        }
        if ($exp->mFoiPostado) {
            switch ($exp->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        $cand = new cCANDIDATO();
                        $cand->mNU_CANDIDATO = $exp->mCampo['NU_CANDIDATO']->mValor;
                        $cand->mTE_EXPERIENCIA_ANTERIOR = $exp->mCampo['TE_EXPERIENCIA_ANTERIOR']->mValor;
                        $cand->AtualizarExperienciaAnterior();
                        $msg = "Data updated successfully!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $exp->mAcaoPostada . ")";
                    break;
            }
        }
        if ($tela->mFoiPostado) {
            switch ($tela->mAcaoPostada) {
                case 'EXCLUIR':
                    try {
                        $cand->mNU_CANDIDATO = $exp->mCampo['NU_CANDIDATO']->mValor;
                        $cand->mTE_EXPERIENCIA_ANTERIOR = $exp->mCampo['TE_EXPERIENCIA_ANTERIOR']->mValor;
                        $cand->AtualizarExperienciaAnterior();
                        $msg = "Data updated successfully!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $exp->mAcaoPostada . ")";
                    break;
            }
        }

        $exp->mCampo['TE_EXPERIENCIA_ANTERIOR']->mValor = $cand->mTE_EXPERIENCIA_ANTERIOR;
        $ret .= cINTERFACE::RenderizeTemplate($exp);
        $ret .= '<br/>';
        $ret .= cINTERFACE::RenderizeTemplate($tmpl);
        $ret .= '<br/>';
        $cursor = $cand->Cursor_ExperienciaProfissional();
        $ret .= cINTERFACE::RenderizeTitulo($tela);
        $ret.= cINTERFACE::RenderizeListagem_Cabecalho($tela);
        $estilo = '';
        while ($rs = mysql_fetch_array($cursor)) {
            $tela->CarregueDoRecordset($rs);
            $ret .= cINTERFACE::RenderizeListagem_Linha($tela, $estilo);
        }
        $ret .= cINTERFACE::RenderizeForm($tela);
        $ret .= cINTERFACE::RenderizeListagem_Rodape($tela);
        cHTTP::$SESSION['msg'] = $msg;
        return $ret;
    }

    public static function Curso_Edit() {
        $msg = '';
        $ret = '';
        $tmpl = new cTEMPLATE_CANDIDATO_CURSO_ADD();
        $tmpl->CarregueDoHTTP();
        //var_dump($tmpl->mCampo);
        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        cINTERFACE::MapeiaEdicaoClasse($tmpl, $cand);
                        $cand->AdicionarExperiencia();
                        $msg = "Data update sucessfully!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {

        }
        cHTTP::$SESSION['msg'] = $msg;
        $ret .= cINTERFACE::RenderizeTemplate($tmpl);
        return $ret;
    }

    public static function Liste_Curso() {
        $ret = '';
        $tmpl = new cTEMPLATE_CANDIDATO_CURSO_ADD();
        $tela = new cTEMPLATE_CANDIDATO_CURSO_VIEW();
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $tela->CarregueDoHTTP();
        $tmpl->CarregueDoHTTP();
        $cand = new cCANDIDATO();
        $cand->Recuperar($tela->mCampo['NU_CANDIDATO']->mValor);
        // Configura tela de acordo com as características do usuário recebido.
        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        $curso = new cCURSO();
                        cINTERFACE::MapeiaEdicaoClasse($tmpl, $curso);
                        $curso->Salvar();
                        $msg = "Data updated sucessfully!";
                    } catch (Exception $e) {
                        $msg = str_replace('"', "'", str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
            cHTTP::$SESSION['msg'] = $msg;
        }
        $ret .= cINTERFACE::RenderizeTemplate($tmpl);
        $ret .= '<br/>';
        $cursor = $cand->Cursor_Cursos();
        $ret .= cINTERFACE::RenderizeTitulo($tela);
        $ret .= cINTERFACE::RenderizeListagem_Cabecalho($tela);
        $estilo = '';
        if (mysql_num_rows($cursor) > 0) {
            while ($rs = mysql_fetch_array($cursor)) {
                $tela->CarregueDoRecordset($rs);
                $ret .= cINTERFACE::RenderizeListagem_Linha($tela, $estilo);
            }
        }
        $ret .= cINTERFACE::RenderizeListagem_Rodape($tela);
        return $ret;
    }

}
