<?php

/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_EDIT_OS_EMISCIE extends cCTRL_PROCESSO_EDIT_OS {

    public function InicializeModelo() {
        $this->modelo = new cprocesso_emiscie();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_PROCESSO_EDIT_OS_EMISCIE();
        $this->template->set_estiloTituloInterno();
    }

    public function CarregueAdicionais() {
// Carrega campos externos
        $cand = new cCANDIDATO();
        $reg = new cprocesso_emiscie;
        $reg->mcodigo = $this->template->getValorCampo('codigo');
        $reg->RecupereSe();
        $cand->mNU_CANDIDATO = $reg->mcd_candidato;
        $cand->Recuperar();
        $this->template->setValorCampo('nu_cie', $cand->mNU_RNE);
    }

    public function AtualizacaoAdicional() {
        if (array_key_exists("CMP_nu_cie", $_POST)) {
            $reg = new cprocesso_emiscie;
            $reg->mcodigo = $this->template->getValorCampo('codigo');
            $reg->RecupereSe();
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $reg->mid_solicita_visto;
            $os->Recuperar();
            $metodo = __FUNCTION__ . " (OS " . $os->mNU_SOLICITACAO . ")";
            $cand = new cCANDIDATO();
            $cand->mNU_CANDIDATO = $reg->mcd_candidato;
            $cand->Recuperar();
            $cand->mNU_RNE = $this->template->getValorCampo('nu_cie');
            $cand->AtualizeRNE(__CLASS__, $metodo, cSESSAO::$mcd_usuario);
        }
    }

}
