<?php

class cCTRL_OS_DESP_LIST extends cCTRL_MODELO_LIST {

    public function InicializeModelo() {
        $this->modelo = new cdespesa();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_OS_DESP_LIST();
        $os = new cORDEMSERVICO;
		$os->mID_SOLICITA_VISTO = cHTTP::ParametroValido('id_solicita_visto');
		$os->RecuperarSolicitaVistoCobranca();
		// Carrega os campos
		$this->template->os = $os;
    }

    public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl) {
        return null;
    }

    public function ProcessePost() {
        if ($this->template->get_acaoPostada() == 'NOVA') {
            $desp = new cdespesa();
            $desp->mid_solicita_visto = $this->template->getValorFiltro('id_solicita_visto');
            $desp->Salvar();
        }
    }
}
