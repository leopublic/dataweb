<?php

/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_GENERICO extends cCTRL_PROCESSO{
	public function FormEdicaoOS(cORDEMSERVICO $pOS){
		if ($pOS->mID_TIPO_ACOMPANHAMENTO == 9){
			$tela = $this->FormEdicaoPadrao($pOS, 'CANDIDATO_CPF');
		}
		elseif ($pOS->mID_TIPO_ACOMPANHAMENTO == 10){
			$tela = $this->FormEdicaoPadrao($pOS, 'CANDIDATO_CNH');
		}
		elseif ($pOS->mID_TIPO_ACOMPANHAMENTO == 11){
			$tela = $this->FormEdicaoPadrao($pOS, 'CANDIDATO_CTPS');
		}
		else{
			$tela = $this->FormEdicaoPadrao($pOS, 'processo_edit');			
		}
		
		$tela->mTitulo = $pOS->mNO_SERVICO_RESUMIDO;
		return $this->RenderizacaoPadraoEdicao($tela);
	}
}
