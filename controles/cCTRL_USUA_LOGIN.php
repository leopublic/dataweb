<?php

class cCTRL_USUA_LOGIN extends cCTRL_MODELO_TWIG_EDIT {

    public function __construct() {
        parent::__construct();
        $this->set_nomeTemplate('public/login.twig');
    }

    public function InicializeModelo() {
        $this->modelo = new cusuarios();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_USUA_LOGIN();
    }

    public function ProcessePost() {
        $this->template->AtribuaAoOBJETO($this->modelo);
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            if ($rep->LoginValidoCrypt($this->template->getValorCampo('login'), $this->template->getValorCampo('nm_senha'))) {
                $link = "/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO_USUARIO&metodo=editarInfo";
                cHTTP::RedirecionaLinkInterno($link);
            } else {
                if ($this->modelo->LoginValidoCrypt($this->template->getValorCampo('login'), $this->template->getValorCampo('nm_senha'))) {
                    cSESSAO::CriaSessao($this->modelo);
                    if (isset($_SESSION['url'])) {
                        $link = $_SESSION['url'];
                        unset($_SESSION['url']);
                    } else {
                        $usup = new cusuario_perfil;
                        if ($usup->ehCliente($this->modelo->cd_usuario) == 'S') {
                            $link = "/paginas/carregueCliente.php?controller=cCTRL_CAND_CLIE_LIST&metodo=Liste";
                        } else {
                            $link = "/paginas/carregue.php?controller=cCTRL_HOME&metodo=home&showMissao=1";
                        }
                    }
                    cHTTP::RedirecionaLinkInterno($link);
                }
            }
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        if (!cHTTP::TemRedirecionamento()) {
            cHTTP::RedirecionaLinkInterno($link);
        }
    }

    public function CarregueTemplatePeloModelo() {
        $this->modelo->nm_senha = '';
        parent::CarregueTemplatePeloModelo();
    }

}
