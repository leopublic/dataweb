<?php 
class cCTRL_CAMP_LIST extends cCTRL_MODELO_LIST{
	public function InicializeModelo(){
		$this->modelo = new cCAMPO();
	}
	
	public function InicializeTemplate(){
		$this->template = new cTMPL_CAMP_LISTE();
	}

	public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) {
		if($this->template->getValorFiltro('ignorar_inativos') == '1'){
			$this->template->mFiltro['ignorar_inativos']->mWhere = ' serv_fl_ativo = 1';
		}
		else{
			$this->template->mFiltro['ignorar_inativos']->mWhere = ' 1 = 1';			
		}
		return parent::get_cursorListagem($ptmpl);
	}
}
