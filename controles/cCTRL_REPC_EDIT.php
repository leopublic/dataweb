<?php
/**
 * Description of cCTRL_EMBP_EDIT
 * Ã¡
 * @author leonardo
 */
class cCTRL_REPC_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cREPARTICAO_CONSULAR();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_REPC_EDIT();
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_REPC_LIST', 'Liste');		
	}

}
