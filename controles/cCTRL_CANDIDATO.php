<?php

class cCTRL_CANDIDATO {

    public static function ListarCrewList() {
        cHTTP::$comCabecalhoFixo = true;
        $tela = new cTEMPLATE_CREW_LIST_REGISTRO();
        $tela->CarregueDoHTTP();
        if (!$tela->mFoiPostado) {
            $tela->mFiltro['TIPO_CANDIDATO']->mValor = "1";
        }

        $x = new cCANDIDATO();
        $ordenacao = '';
        $virgula = '';

        $ret = '';
        $ret .= cINTERFACE::formTemplate($tela);
        $ret .= cINTERFACE::RenderizeTitulo($tela, true, cINTERFACE::titPAGINA);
        $ret .= '<div class="conteudoInterno">';
        $ret .= cINTERFACE::RenderizeListagem_Filtros($tela);
        if (intval($tela->mFiltro['NU_EMPRESA']->mValor) > 0) {
            if (intval($tela->mFiltro['NU_EMBARCACAO_PROJETO']->mValor) > 0) {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = false;
            } else {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = true;
            }
            $cursor = $x->ListeCrewList($tela->mFiltro, $tela->ordenacao());
            $ret.= cINTERFACE::RenderizeListagem_Cabecalho($tela);
            $estilo = '';
            while ($rs = mysql_fetch_array($cursor)) {
                $tela->CarregueDoRecordset($rs);
                $tela->mCampo['nu_protocolo_pro']->mValor = str_replace('.', "", str_replace(".", "", str_replace('/', "", str_replace('-', "", $tela->mCampo['nu_protocolo_pro']->mValor))));
                $br = "";
                if ($rs['observacao_visto'] != '') {
                    $br = "<br/>";
                }
                //
                // Customiza linha
                $tela->mCampo['nu_processo_mte']->mlinkMte = true;
                if ($rs['codigo_mte'] > 0) {
                    if ($rs['nu_processo_mte'] == '') {
                        $tela->mCampo['nu_processo_mte']->mValor = '(processo em andamento)';
                        $tela->mCampo['nu_processo_mte']->mlinkMte = false;
                    }
                }
                $tela->mCampo['NOME_COMPLETO']->mValor = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $rs['NU_CANDIDATO'] . '" target="_blank">' . $rs['NOME_COMPLETO'] . ' (' . $rs['NU_CANDIDATO'] . ')</a>';
                $tela->mCampo['validade_visto']->mValor = '';
                //Validade do visto
                if (intval($rs['codigo_pro']) > 0) {
                    if ($rs['ID_STATUS_CONCLUSAO_PRO'] == '1' || $rs['ID_STATUS_CONCLUSAO_PRO'] == '') {
                        if ($rs['dt_prazo_pret_fmt'] != '') {
                            $tela->mCampo['validade_visto']->mValor = $rs['dt_prazo_pret_fmt'];
                            if ($rs['nu_protocolo_pro'] == '') {
                                $tela->mCampo['observacao_visto']->mValor .= $br . '<span style="color:red">Validade do visto a partir do prazo pretendido de prorrogação em análise<br/>Mas o processo está sem número do protocolo!!! Verifique se foi realmente enviado!!</span>';
                                $br = '<br/>';
                            } else {
                                $tela->mCampo['observacao_visto']->mValor .= $br . '<span style="color:green">Validade do visto a partir do prazo pretendido de prorrogação em análise</span>';
                                $br = '<br/>';
                            }
                        } else {
                            $tela->mCampo['observacao_visto']->mValor .= $br . '<span style="color:red">Existe uma prorrogação em análise no candidato, sem prazo pretendido. Verifique!!!</span>';
                            $br = '<br/>';
                        }
                    } elseif ($rs['ID_STATUS_CONCLUSAO_PRO'] == "2") {
                        if ($rs['dt_prazo_pret_fmt'] != '') {
                            $tela->mCampo['validade_visto']->mValor = $rs['dt_prazo_pret_fmt'];
                            if ($rs['nu_protocolo_pro'] == '') {
                                $tela->mCampo['observacao_visto']->mValor .= $br . '<span style="color:red">Validade do visto a partir do prazo pretendido de prorrogação deferida<br/>Mas o processo está sem número do protocolo!!! Verifique se foi realmente enviado!!</span>';
                                $br = '<br/>';
                            } else {
                                $tela->mCampo['observacao_visto']->mValor .= $br . '<span style="color:green">Validade do visto a partir do prazo pretendido da prorrogação deferida.</span>';
                                $br = '<br/>';
                            }
                        } else {
                            $tela->mCampo['observacao_visto']->mValor .= $br . '<span style="color:red">Existe uma prorrogação deferida no candidato sem prazo pretendido. Verifique!!!</span>';
                            $br = '<br/>';
                        }
                    }
                }

                if ($tela->mCampo['validade_visto']->mValor == '') {
                    if (intval($rs['codigo_reg']) > 0) {
                        if ($rs['dt_prazo_estada_fmt'] != '' && $rs['dt_prazo_estada_fmt'] != '00/00/00') {
                            $tela->mCampo['validade_visto']->mValor = $rs['dt_prazo_estada_fmt'];
                            $tela->mCampo['observacao_visto']->mValor .= $br . '<span style="color:green">Prazo de validade indicado a partir do prazo de estada do registro</span>';
                            $br = '<br/>';
                        } else {
                            $tela->mCampo['observacao_visto']->mValor .= $br . '<span style="color:red">Processo de registro sem prazo de estada!!! Verifique!!</span>';
                            $br = '<br/>';
                        }
                    }
                }
                if ($rs['codigo_reg'] == '') {
                    $tela->mCampo['observacao_visto']->mValor .= $br . '<span style="color:red">Candidato sem registro!</span>';
                    $br = '<br/>';
                }

                //var_dump($tela->mCampo);
                $ret .= cINTERFACE::RenderizeListagem_Linha($tela, $estilo);
            }
            $ret .= cINTERFACE::RenderizeListagem_Rodape($tela);
        } else {
            $ret .= "Por favor, selecione a empresa e a embarcação.";
        }
        //
        //
		if (!$tela->mFoiPostado) {
            cHTTP::$SESSION['msg'] = 'Atenção: Esta consulta está em desenvolvimento. Alguns resultados podem não estar corretos. Algumas funcionalidades serão complementadas. Acompanhe pelo log de modificações na home-page';
            cLAYOUT::AdicionarScriptExibicaoMensagens();
        }
        $ret.= "</form>";
        $ret.= '</div>';
        return $ret;
    }

    public static function EditarCrewList() {
        $msg = '';
        $retorno = '';
        $tmpl = new cTEMPLATE_CANDIDATO_EDIT_CREWLIST();
        $tmpl->CarregueDoHTTP();
        $cand = new cCANDIDATO();
        $cand->mNU_CANDIDATO = $tmpl->mCampo['NU_CANDIDATO']->mValor;
        $cand->Recuperar();
        $proc = new cprocesso_mte();
        if (intval($cand->mcodigo_processo_mte_atual) > 0) {
            $proc->RecupereSe($cand->mcodigo_processo_mte_atual);
        } else {
            $tmpl->mCampo['observacao_visto']->mVisivel = false;
        }
        if ($tmpl->mFoiPostado) {
            $tmpl->utf_decodeCampos();
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        $tmpl->AtribuaAoOBJETO($cand);
                        $cand->AtualizeCrewList();
                        if (intval($cand->mcodigo_processo_mte_atual) > 0) {
                            $proc->mobservacao_visto = $tmpl->mCampo['observacao_visto']->mValor;
                            $proc->AtualizeObservacaoVisto();
                        }
                        $msg = "Situação do candidato atualizada com sucesso!";
                        $tmpl->CarregueDoOBJETO($cand);
                        $tmpl->mCampo[cTEMPLATE::CMP_FINALIZOU_COM_SUCESSO]->mValor = "1";
                        $tmpl->mCampo[cTEMPLATE::CMP_ACAO_SUCESSO]->mValor = cTEMPLATE::CMP_ACAO_SUCESSO_FECHAR;
                    } catch (Exception $e) {
                        $msg = htmlentities($e->getMessage());
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {
            $tmpl->CarregueDoOBJETO($cand);
            $tmpl->mCampo['observacao_visto']->mValor = $proc->mobservacao_visto;
        }
        $tmpl->mCampo[cTEMPLATE::CMP_TITULO]->mValor = 'Atualizar situação do candidato ' . $cand->mNOME_COMPLETO . ' ';
        $tmpl->mCampo[cTEMPLATE::CMP_MSG]->mValor = $msg;
        $conteudo = cINTERFACE::RenderizeTemplateComAcoesNoRodape($tmpl);
        return $conteudo;
    }

    public static function ExibirVisto() {
        $ret = '';
        $pNU_CANDIDATO = cHTTP::$GET['NU_CANDIDATO'];
        $cand = new cINTERFACE_CANDIDATO();
        $cand->Recuperar($pNU_CANDIDATO);
        $altura = new cCAMPO_HIDDEN(cTEMPLATE::CMP_ALTURA);
        $largura = new cCAMPO_HIDDEN(cTEMPLATE::CMP_LARGURA);
        $titulo = new cCAMPO_HIDDEN(cTEMPLATE::CMP_TITULO);
        $altura->mValor = "550";
        $largura->mValor = "850";
        $titulo->mValor = 'Histórico do visto de &quot;' . $cand->mNOME_COMPLETO . '&quot;';
        $ret .= cINTERFACE::RenderizeCampo($altura);
        $ret .= cINTERFACE::RenderizeCampo($largura);
        $ret .= cINTERFACE::RenderizeCampo($titulo);

        $ret.= $cand->FormVistoAtual();
        return $ret;
    }

    public static function ListarNecessitaProrrogacao() {
        cHTTP::$comCabecalhoFixo = true;
        $tela = new cTEMPLATE_CANDIDATO_LIST_NECESSITA_PRORROGACAO();
        $tela->CarregueDoHTTP();
        if (!$tela->mFoiPostado) {
            $tela->mFiltro['TIPO_CANDIDATO']->mValor = "1";
        }
        $x = new cCANDIDATO();

        if ($tela->mAcaoPostada == 'GERAR_EXCEL') {
            cHTTP::$MIME = cHTTP::mmXLS;
        }

        if (intval($tela->mFiltro['NU_EMPRESA']->mValor) > 0) {
            if (intval($tela->mFiltro['NU_EMBARCACAO_PROJETO']->mValor) > 0) {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = false;
            } else {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = true;
            }
            if ($tela->mFiltro['MESES']->mValor != '') {
                $data_limite = new DateTime();
                $data_limite->add(new DateInterval('P' . ($tela->mFiltro['MESES']->mValor + 1) . 'M'));
                $xdata = new DateTime($data_limite->format("Y") . '-' . $data_limite->format("m") . '-01');
                $xdata->sub(new DateInterval('P1D'));
                $agora = new DateTime();

                $diff = abs(strtotime($xdata->format('Y-m-d')) - strtotime($agora->format('Y-m-d')));
                $maxdias = floor($diff / (60 * 60 * 24));
            } else {
                $maxdias = 999999999;
            }
            $tela->mCampo['dias_limite']->mValor = $maxdias;
            $tela->mFiltro['MESES']->mWhere = '1=1'; // invalida o where automático para esse campo
            $tela->mFiltro['SITUACAO_VISTO']->mWhere = '1=1'; // invalida o where automático para esse campo
            $grid = cINTERFACE::RenderizeListagem_Cabecalho($tela);
            try {
                $cursor = $x->ListeCrewList($tela->mFiltro, $tela->ordenacao());
            } catch (Exception $e) {
                cHTTP::$SESSION['msg'] = $e->getMessage();
            }
            $estilo = '';
            $qtd = 0;
            while ($rs = $cursor->fetch(PDO::FETCH_BOTH)) {
//				if ($rs['codigo_mte'] != '' ){
                $tela->CarregueDoRecordset($rs);

                $tela->mCampo['NU_PROTOCOLO_PRO']->mValor = str_replace('.', "", str_replace(".", "", str_replace('/', "", str_replace('-', "", $tela->mCampo['NU_PROTOCOLO_PRO']->mValor))));
                $tela->mCampo['dt_prazo_pret_fmt']->mClasse = "cen";
                $tela->mCampo['NU_PROTOCOLO_PRO']->mClasse = "cen";
                $tela->mCampo['NU_PROTOCOLO_PRO']->mlinkMJ = true;
                $br = "";
                if ($rs['OBSERVACAO_VISTO'] != '') {
                    $br = "<br/>";
                }
                //
                // Customiza linha
                $tela->mCampo['NU_PROCESSO_MTE']->mlinkMte = true;
                if ($rs['codigo_mte'] > 0) {
                    if ($rs['NU_PROCESSO_MTE'] == '') {
                        $tela->mCampo['NU_PROCESSO_MTE']->mValor = '(sem número)';
                        $tela->mCampo['NU_PROCESSO_MTE']->mlinkMte = false;
                    }
                    $tela->mCampo['NU_PROCESSO_MTE']->mValor = str_replace('?', "", $tela->mCampo['NU_PROCESSO_MTE']->mValor);

                    if ($tela->mCampo['DT_DEFERIMENTO']->mValor != '' && $tela->mCampo['DT_DEFERIMENTO']->mValor != '00/00/00') {
                        $tela->mCampo['NU_PROCESSO_MTE']->mValor .= '</br>(deferido em ' . $tela->mCampo['DT_DEFERIMENTO']->mValor . ')<br/>';
                    } else {
                        if ($tela->mCampo['NO_STATUS_CONCLUSAO_MTE']->mValor == 'n/a') {
                            $tela->mCampo['NO_STATUS_CONCLUSAO_MTE']->mValor = 'em andamento';
                        } elseif ($tela->mCampo['NO_STATUS_CONCLUSAO_MTE']->mValor == '') {
                            $tela->mCampo['NO_STATUS_CONCLUSAO_MTE']->mValor = 'status desconhecido';
                        }
                        $tela->mCampo['NU_PROCESSO_MTE']->mValor .= '<br/>(' . $tela->mCampo['NO_STATUS_CONCLUSAO_MTE']->mValor . ')<br/>';
                    }
                }

                $tela->mCampo['NOME_COMPLETO']->mValor = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $rs['NU_CANDIDATO'] . '" target="_blank">' . $rs['NOME_COMPLETO'] . ' (' . $rs['NU_CANDIDATO'] . ')</a>';
                $tela->mCampo['validade_visto']->mValor = '';
                if ($tela->mCampo['DT_REQUERIMENTO_CIE']->mValor == '00/00/00') {
                    $tela->mCampo['DT_REQUERIMENTO_CIE']->mValor = '';
                }
                //Validade do visto
                $dias = 0;
                if (intval($rs['codigo_pro']) > 0) {
                    if ($tela->mCampo['NO_STATUS_CONCLUSAO_pro']->mValor == 'n/a') {
                        $tela->mCampo['NO_STATUS_CONCLUSAO_pro']->mValor = 'em andamento';
                    } elseif ($tela->mCampo['NO_STATUS_CONCLUSAO_PRO']->mValor == '') {
                        $tela->mCampo['NO_STATUS_CONCLUSAO_PRO']->mValor = 'status desconhecido';
                    }
                    if ($rs['ID_STATUS_CONCLUSAO_PRO'] == '1' || $rs['ID_STATUS_CONCLUSAO_PRO'] == '') {
                        if ($rs['dt_prazo_pret_fmt'] != '') {
                            $tela->mCampo['validade_visto']->mValor = $rs['dt_prazo_pret_fmt'];
                            $dias = $rs['dt_prazo_pret_diff'];
                            if ($rs['NU_PROTOCOLO_PRO'] == '') {
                                $tela->mCampo['NU_PROTOCOLO_PRO']->mValor = '!!!!!!!!';
                                $tela->mCampo['NU_PROTOCOLO_PRO']->mClasse .= " erro";
                                $tela->mCampo['NU_PROTOCOLO_PRO']->mlinkMJ = false;
                                $tela->mCampo['OBSERVACAO_VISTO']->mValor .= $br . '<span style="color:red">Validade do visto a partir do prazo pretendido de prorrogação em análise<br/>Mas o processo está sem número do protocolo!!! Verifique se foi realmente enviado!!</span>';
                                $br = '<br/>';
                            } else {
                                $tela->mCampo['OBSERVACAO_VISTO']->mValor .= $br . '<span style="color:green">Validade do visto a partir do prazo pretendido de prorrogação em análise</span>';
                                $br = '<br/>';
                            }
                        } else {
                            $tela->mCampo['DT_PRAZO_PRET_PRO']->mValor = '!!!!!!!!';
                            $tela->mCampo['DT_PRAZO_PRET_PRO']->mClasse .= " erro";
                            $tela->mCampo['OBSERVACAO_VISTO']->mValor .= $br . '<span style="color:red">Existe uma prorrogação em análise no candidato, sem prazo pretendido. Verifique!!!</span>';
                            $br = '<br/>';
                        }
                    } elseif ($rs['ID_STATUS_CONCLUSAO_pro'] == "2") {
                        if ($rs['dt_prazo_pret_fmt'] != '') {
                            $tela->mCampo['validade_visto']->mValor = $rs['dt_prazo_pret_fmt'];
                            $dias = $rs['dt_prazo_pret_diff'];
                            if ($rs['NU_PROTOCOLO_PRO'] == '') {
                                $tela->mCampo['NU_PROTOCOLO_PRO']->mValor = '!!!!!!!!';
                                $tela->mCampo['NU_PROTOCOLO_PRO']->mClasse .= " erro";
                                $tela->mCampo['NU_PROTOCOLO_PRO']->mlinkMJ = false;
                                $tela->mCampo['OBSERVACAO_VISTO']->mValor .= $br . '<span style="color:red">Validade do visto a partir do prazo pretendido de prorrogação deferida<br/>Mas o processo está sem número do protocolo!!! Verifique se foi realmente enviado!!</span>';
                                $br = '<br/>';
                            } else {
                                $tela->mCampo['OBSERVACAO_VISTO']->mValor .= $br . '<span style="color:green">Validade do visto a partir do prazo pretendido da prorrogação deferida.</span>';
                                $br = '<br/>';
                            }
                        } else {
                            $tela->mCampo['dt_prazo_pret_fmt']->mValor = '!!!!!!!!';
                            $tela->mCampo['dt_prazo_pret_fmt']->mClasse .= " erro";
                            $tela->mCampo['OBSERVACAO_VISTO']->mValor .= $br . '<span style="color:red">Existe uma prorrogação deferida no candidato sem prazo pretendido. Verifique!!!</span>';
                            $br = '<br/>';
                        }
                    }
                    $tela->mCampo['NU_PROTOCOLO_PRO']->mValor .= '<br/>(' . $tela->mCampo['NO_STATUS_CONCLUSAO_pro']->mValor . ')';
                    if ($tela->mCampo['NU_PROTOCOLO_PRO']->mlinkMJ) {
                        $tela->mCampo['NU_PROTOCOLO_PRO']->mValor .= '<br/>';
                    }
                }

                if ($tela->mCampo['validade_visto']->mValor == '') {
                    if (intval($rs['codigo_REG']) > 0) {
                        if ($rs['DT_PRAZO_ESTADA'] != '' && $rs['DT_PRAZO_ESTADA'] != '00/00/00') {
                            $tela->mCampo['validade_visto']->mValor = $rs['DT_PRAZO_ESTADA'];
                            $dias = $rs['dt_prazo_estada_diff'];
                            $tela->mCampo['OBSERVACAO_VISTO']->mValor .= $br . '<span style="color:green">Prazo de validade indicado a partir do prazo de estada do registro</span>';
                            $br = '<br/>';
                        } else {
                            $tela->mCampo['OBSERVACAO_VISTO']->mValor .= $br . '<span style="color:red">Processo de registro sem prazo de estada!!! Verifique!!</span>';
                            $br = '<br/>';
                        }
                    }
                }
                if ($rs['codigo_REG'] == '') {
                    $tela->mCampo['OBSERVACAO_VISTO']->mValor .= $br . '<span style="color:red">Candidato sem registro!</span>';
                    $br = '<br/>';
                }
                //var_dump($tela->mCampo);
                if ($dias < $maxdias && (
                        ($tela->mFiltro['SITUACAO_VISTO']->mValor == "1" && $dias > 0) || ($tela->mFiltro['SITUACAO_VISTO']->mValor == "2" && $dias <= 0) || ($tela->mFiltro['SITUACAO_VISTO']->mValor == "")
                        )
                ) {
                    $qtd = $qtd + 1;
                    $grid .= cINTERFACE::RenderizeListagem_Linha($tela, $estilo);
                }
            }
//			}
            $grid .= cINTERFACE::RenderizeListagem_Rodape($tela);
            $grid = $qtd . " selecionados" . $grid;
        } else {
            $grid = "Por favor, selecione a empresa e a embarcação.";
        }
        //
        //
		$ret = '';
        switch ($tela->mAcaoPostada) {
            case 'Filtrar':
                $ret .= cINTERFACE::formTemplate($tela);
                $ret .= cINTERFACE::RenderizeTitulo($tela, true, cINTERFACE::titPAGINA);
                $ret .= '<div class="conteudoInterno">';
                $ret .= cINTERFACE::RenderizeListagem_Filtros($tela);
                $ret .= $grid;

                if (!$tela->mFoiPostado) {
                    cHTTP::$SESSION['msg'] = '<span style="color:red;font-size:12pt;font-weight:bold;">ATENÇÃO: O filtro "Situação do visto" só deve ser considerado depois que o histórico de todos os candidatos for revisado. Caso contrário ele poderá ignorar candidatos com visto válido mas informações incompletas. Cuidado.</span>';
                    cLAYOUT::AdicionarScriptExibicaoMensagens();
                }
                $ret.= "</form>";
                $ret.= '</div>';
                break;
            case 'GERAR_EXCEL':
                cHTTP::$MIME = cHTTP::mmXLS;
                $ret .= str_replace('<br/>', chr(10), $grid);
                break;
            default:
                $ret .= cINTERFACE::formTemplate($tela);
                $ret .= cINTERFACE::RenderizeTitulo($tela, true, cINTERFACE::titPAGINA);
                $ret .= '<div class="conteudoInterno">';
                $ret .= cINTERFACE::RenderizeListagem_Filtros($tela);
                $ret.= "</form>";
                $ret.= '</div>';
                break;
        }
        $ret .= $tela->TratamentoPadraoDeMensagens();
        return $ret;
    }

    public static function ListarNecessitaProrrogacaoDetalhe() {

    }

    public static function ListarNomes() {
        $sql = "select NOME_COMPLETO from CANDIDATO where NOME_COMPLETO like '%" . cHTTP::$GET['NOME_COMPLETO'] . "%' limit 50";
        print $sql;
        $res = mysql_query($sql);
        $nomes = array();
        while ($rs = mysql_fetch_array($res)) {
            $nomes[] = array('NOME_COMPLETO' => $rs['NOME_COMPLETO']);
        }
        return json_encode($nomes);
    }

    public static function EmpacotarDocumentos() {
        $NU_EMPRESA = "182";
        $ID_TIPO_ARQUIVO = array(20, 18);
        $timestamp = date('Y_m_d_G_i_s');
        $tmp = '..' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . $timestamp;
        mkdir($tmp);

        $sql = "select C.NU_CANDIDATO, C.NOME_COMPLETO, A.NO_ARQUIVO, A.NO_ARQ_ORIGINAL, date_format(A.DT_INCLUSAO, '%Y_%m_%d'), TA.CO_TIPO_ARQUIVO, sv.nu_solicitacao ";
        $sql.= "  from CANDIDATO C ";
        $sql.= "  left join AUTORIZACAO_CANDIDATO AC on AC.NU_CANDIDATO = C.NU_CANDIDATO";
        $sql.= "  left join solicita_visto sv on sv.id_solicita_visto= AC.id_solicita_visto";
        $sql.= "  left join ARQUIVOS A on A.ID_SOLICITA_VISTO = AC.id_solicita_visto";
        $sql.= "  left join TIPO_ARQUIVO TA on TA.ID_TIPO_ARQUIVO = A.TP_ARQUIVO";
        $sql.= " where C.NU_EMPRESA = " . $NU_EMPRESA;
        $sql.= "   and C.FL_INATIVO = 0";
        $sql.= "   and TA.ID_TIPO_ARQUIVO IN (1,20,18,3,9)";
        $sql .= " order by C.NOME_COMPLETO, A.DT_INCLUSAO";

        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $NU_CANDIDATO_ANT = '';
        $i = '';
        while ($rs = mysql_fetch_array($res)) {
            $origem = '..' . DIRECTORY_SEPARATOR . 'arquivos' . DIRECTORY_SEPARATOR . 'solicitacoes' . DIRECTORY_SEPARATOR . $NU_EMPRESA . DIRECTORY_SEPARATOR . $rs['NO_ARQUIVO'];
            if (file_exists($origem)) {

                if ($NU_CANDIDATO_ANT != $rs['NU_CANDIDATO']) {
                    $i = '';
                }
                $nome = self::LimpaNome($rs['NOME_COMPLETO']) . '_' . $rs['NU_CANDIDATO'];
                $dir = $tmp . DIRECTORY_SEPARATOR . $nome;
                if (!is_dir($dir)) {
                    mkdir($dir);
                }
                // Limpa o nome do arquivo
                $arquivo = $dir . DIRECTORY_SEPARATOR . $rs['CO_TIPO_ARQUIVO'] . '_OS' . $rs['nu_solicitacao'] . '_' . self::LimpaNome($rs['NO_ARQ_ORIGINAL']);
                $i = '';
                while (file_exists($arquivo)) {
                    if ($i == '') {
                        $i = 1;
                    } else {
                        $i++;
                    }
                    $arquivo = $dir . DIRECTORY_SEPARATOR . $rs['CO_TIPO_ARQUIVO'] . '_OS' . $rs['nu_solicitacao'] . '_' . $i . '_' . self::LimpaNome($rs['NO_ARQ_ORIGINAL']);
                }
                print '<br>' . $arquivo;
                copy($origem, $arquivo);
            }
        }
    }

    public static function LimpaNome($pNome) {
        $ret = $pNome;
        $ret = str_replace(' ', '_', $ret);
        $ret = str_replace('á', 'a', $ret);
        $ret = str_replace('à', 'a', $ret);
        $ret = str_replace('â', 'a', $ret);
        $ret = str_replace('ã', 'a', $ret);
        $ret = str_replace('é', 'e', $ret);
        $ret = str_replace('ê', 'e', $ret);
        $ret = str_replace('í', 'i', $ret);
        $ret = str_replace('î', 'i', $ret);
        $ret = str_replace('ó', 'o', $ret);
        $ret = str_replace('õ', 'o', $ret);
        $ret = str_replace('ò', 'o', $ret);
        $ret = str_replace('ú', 'u', $ret);
        $ret = str_replace('ù', 'u', $ret);
        $ret = str_replace('û', 'u', $ret);
        $ret = str_replace("'", '_', $ret);
        $ret = str_replace('\\', '_', $ret);
        $ret = str_replace('/', '_', $ret);
        $ret = str_replace('-', '_', $ret);
        return $ret;
    }

    //TODO:Refatorar para chamada única com tratamento de erros por exceção. Deve chamar somente DisponibilizeDrAoCliente. Deve passar também a empresa do usuário
    public static function VerificarCandidatoNovoExistente() {
        cINTERFACE_AMBIENTE::ControleLinguagem();
        $msg = '';
        $tmpl = new cTEMPLATE_CANDIDATO_NOVO_CLIENTE();
        $tmpl->mAcoes['VERIFICAR']->mControle = __CLASS__;
        $tmpl->mAcoes['VERIFICAR']->mMetodo = __FUNCTION__;
        $tmpl->CarregueDoHTTP();
        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'VERIFICAR':
                    try {
                        $cand = new cCANDIDATO();
                        $cand->mNOME_COMPLETO = $tmpl->mCampo['NOME_COMPLETO']->mValor;
                        $cand->DisponibilizeCadastroParaCliente($tmpl->mCampo['NOME_COMPLETO']->mValor, $tmpl->mCampo['NO_MAE']->mValor, $tmpl->mCampo['DT_NASCIMENTO']->mValor);
                        cHTTP::RedirecionePara('cCTRL_CANDIDATO', 'DadosPessoaisEdit_cliente', 'NU_CANDIDATO=' . $cand->mNU_CANDIDATO, 'carregueCliente.php');
//						if (intval($cand->mNU_CANDIDATO) == 0){
//						// Candidato não encontrado
//								$cand->Criar_cliente($_SESSION['myAdmin']['cd_empresa'], $tmpl->mCampo['NOME_COMPLETO']->mValor, $tmpl->mCampo['NO_MAE']->mValor, $tmpl->mCampo['DT_NASCIMENTO']->mValor);
//								cHTTP::RedirecionePara('cCTRL_CANDIDATO', 'DadosPessoaisEdit_cliente', 'NU_CANDIDATO='.$cand->mNU_CANDIDATO, 'carregueCliente.php');
//						}
//						else{
//						// Candidato já existe
//							if ($cand->mNU_EMPRESA != cHTTP::$SESSION['myAdmin']['cd_empresa']){
//								if (cHTTP::getLang() == 'pt_br'){
//									$msg = 'Este candidato já está cadastrado no sistema em outra empresa. Para preservar o sigilo das informações dos nossos clientes, uma empresa não pode acessar informações de candidatos que estão em outra. Para maiores informações entre em contato com a Mundivisas.';
//								}
//								else{
//									$msg = 'This person is already in our database, filed under another company. To preseve our customers data, one company is not allowed to access candidate data from another. For further information please contact Mundivisas.';
//								}
//							}
//							else{
//								if ($cand->mFL_ATUALIZACAO_HABILITADA == "1"){
//										$cand->DisponibilizeCadastroParaCliente();
//										cHTTP::RedirecionePara('cCTRL_CANDIDATO', 'DadosPessoaisEdit_cliente', 'NU_CANDIDATO='.$cand->mNU_CANDIDATO, 'carregueCliente.php');
//								}
//								else{
//									cHTTP::RedirecionePara('cCTRL_CANDIDATO', 'DadosPessoaisEdit_cliente', 'NU_CANDIDATO='.$cand->mNU_CANDIDATO, 'carregueCliente.php');
//								}
//							}
//						}
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {

        }
        cHTTP::$SESSION['msg'] = $msg;
        $ret = '';
        $ret .= cINTERFACE::formTemplate($tmpl);
        $ret .= cINTERFACE::RenderizeTitulo($tmpl, false, cINTERFACE::titPAGINA);
        $ret .= '<div class="conteudoInterno">';
        if ($tmpl->mMsgInicial != '') {
            $ret.= $tmpl->mMsgInicial;
        }
        $ret .= cINTERFACE::RenderizeTemplateEdicao($tmpl);
        $ret .= '</table>';
        $ret .= cINTERFACE::RenderizeBarraAcoesRodape($tmpl->mAcoes);
        $ret .= '</form>';
        $ret .= '</div>';
        $ret .= $tmpl->TratamentoPadraoDeMensagens();
        cHTTP::AdicionarScript('ExibirMsg("#dialog", "' . cHTTP::TituloMsg() . '" );');
        return $ret;
    }

    public static function VerificarCandidatoDR() {
        cINTERFACE_AMBIENTE::ControleLinguagem();
        $msg = '';
        $tmpl = new cTEMPLATE_CANDIDATO_NOVO_CLIENTE();
        $tmpl->mAcoes['VERIFICAR']->mControle = __CLASS__;
        $tmpl->mAcoes['VERIFICAR']->mMetodo = __FUNCTION__;
        $tmpl->mTitulo = '';
        $tmpl->CarregueDoHTTP();
        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'VERIFICAR':
                    try {
                        $cand = new cCANDIDATO();
                        $cand->mNOME_COMPLETO = $tmpl->mCampo['NOME_COMPLETO']->mValor;
                        $cand->DisponibilizeDrAoCandidato($tmpl->mCampo['NOME_COMPLETO']->mValor, $tmpl->mCampo['NO_MAE']->mValor, $tmpl->mCampo['DT_NASCIMENTO']->mValor);
                        cHTTP::RedirecionePara('cCTRL_CANDIDATO', 'DadosPessoaisEdit_DREletronico', 'NU_CANDIDATO=' . $cand->mNU_CANDIDATO, 'carregueDR.php');
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {

        }
        cAMBIENTE::InicializeAmbiente('PRD');
        cHTTP::$SESSION['msg'] = $msg;
        $ret = '';
        $ret .= cINTERFACE::formTemplate($tmpl);
        $ret .= cINTERFACE::RenderizeTitulo($tmpl, false, cINTERFACE::titPAGINA);
        $ret .= '<div class="conteudoInterno">';
        if ($tmpl->mMsgInicial != '') {
            $ret.= $tmpl->mMsgInicial;
        }
        $ret .= cINTERFACE::RenderizeTemplateEdicao($tmpl);
        $ret .= '</table>';
        $ret .= cINTERFACE::RenderizeBarraAcoesRodape($tmpl->mAcoes);
        $ret .= '</form>';
        $ret .= '</div>';
        $ret .= $tmpl->TratamentoPadraoDeMensagens();
        cHTTP::AdicionarScript('ExibirMsg("#dialog", "' . cHTTP::TituloMsg() . '" );');
        return $ret;
    }

    public function ListarCandidatos_cliente() {
        cINTERFACE_AMBIENTE::ControleLinguagem();
        cHTTP::$comCabecalhoFixo = true;
        $tela = new cTEMPLATE_CANDIDATO_LISTAR_CLIENTE();
        $tela->CarregueDoHTTP();
        if (cHTTP::HouvePost()) {
            cHTTP::RedirecionePara('cCTRL_CANDIDATO', 'ListarCandidatos_cliente', "", 'carregueCliente.php');
        }
        $tela->mFiltro['NU_EMPRESA']->mValor = $_SESSION['myAdmin']['cd_empresa'];

        $x = new cCANDIDATO();

        $ret = '';
        $ret .= cINTERFACE::RenderizeTitulo($tela, true, cINTERFACE::titPAGINA);
        $ret .= '<div class="conteudoInterno">';
        $ret .= cINTERFACE::formTemplate($tela);
        $ret .= cINTERFACE::RenderizeListagem_Filtros($tela);
        if (intval($tela->mFiltro['NU_EMPRESA']->mValor) > 0 && ($tela->mFiltro['NU_EMBARCACAO_PROJETO']->mValor > 0 || $tela->mFiltro['NOME_COMPLETO']->mValor != '' )) {
            if (intval($tela->mFiltro['NU_EMBARCACAO_PROJETO']->mValor) > 0) {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = false;
                $tela->mCols = array('70px', 'auto', 'auto', '90px', '70px', '80px', 'auto', '80px', '80px');
            } else {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = true;
                $tela->mCols = array('70px', 'auto', 'auto', 'auto', '90px', '70px', '100px', 'auto', '100px', '100px');
            }
            $cursor = $x->CursorCandidatosCliente($tela->mFiltro, $tela->ordenacao());
            $ret.= cINTERFACE::RenderizeListagem_Cabecalho($tela);
            while ($rs = mysql_fetch_array($cursor)) {
                $tela->CarregueDoRecordset($rs);
                $ret .= cINTERFACE::RenderizeListagem_Linha($tela, $estilo);
            }
            $ret .= cINTERFACE::RenderizeListagem_Rodape($tela);
        } else {
            $ret .= "Please inform the ship or at least a part of a name.";
        }
        //
        //
		$ret.= "</form>";
        $ret.= '</div>';
        $ret .= $tela->TratamentoPadraoDeMensagens();
        cHTTP::AdicionarScript('ExibirMsg("#dialog", "' . cHTTP::TituloMsg() . '" );');
        return $ret;
    }

    public function DadosPessoais_cliente() {
        cINTERFACE_AMBIENTE::ControleLinguagem();
        $tmpl = new cTEMPLATE_CANDIDATO_VIEW_CLIENTE();
        $tmpl->CarregueDoHTTP();
        $cand = new cCANDIDATO();
        $cand->Recuperar($tmpl->mCampo['NU_CANDIDATO']->mValor);
        if (cHTTP::$SESSION["usuarioPerfil"] == '10') {
            if ($cand->mNU_EMPRESA != cHTTP::$SESSION['myAdmin']['cd_empresa']) {
                cHTTP::RedirecionaLinkInterno('/index.php');
            }
        }

        $rs = $cand->CursorCandidato_cliente($tmpl->mCampo['NU_CANDIDATO']->mValor);
        $tmpl->CarregueDoRecordset($rs);
        $ret = '';
        $ret .= '<div class="conteudoInterno">';
        if ($tmpl->mMsgInicial != '') {
            $ret.= $tmpl->mMsgInicial;
        }
        $ret .= cINTERFACE::RenderizeTemplateEdicao($tmpl);
        $ret .= '</table>';
        $ret .= cINTERFACE::RenderizeBarraAcoesRodape($tmpl->mAcoes);
        $ret .= '</form>';
        $ret .= '</div>';
        $html = $ret;
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $ret = '';
        $tmplAba = new cTEMPLATE_CANDIDATO_CLIENTE_ABAS();
        $tmplAba->mCampo['NU_CANDIDATO']->mValor = cHTTP::ParametroValido('NU_CANDIDATO');
        $tmplAba->RepliqueChavesParaAbasAjax();
        // Adiciona a chave se houver como parametro nas abas
        $tmplAba->mTitulo = 'Detailed information of "' . $rs['NOME_COMPLETO'] . '"';
        $ret .= cINTERFACE::formTemplate($tmplAba);
        $ret .= cINTERFACE::RenderizeTitulo($tmplAba, false, cINTERFACE::titPAGINA);
        $ret .= '<div id="tabs">';
        $ret .= cINTERFACE::RenderizeIndiceDeAbas($tmplAba, 'DADOS_PESSOAIS');
        $ret .= '<div id="abaAtiva">';
        $ret .= $html;
        $ret .= '</div>';
        $ret .= '</div>';
        $ret .= '</form>';
        $ret .= $tmplAba->TratamentoPadraoDeMensagens();
        cHTTP::AdicionarScript('ExibirMsg("#dialog", "' . cHTTP::TituloMsg() . '" );');
        return $ret;
    }

    public function DadosPessoaisEdit_cliente() {
        $msg = '';
        cINTERFACE_AMBIENTE::ControleLinguagem();
        $cand = new cCANDIDATO();
        $cand->Recuperar(cHTTP::ParametroValido('NU_CANDIDATO'));
        if ($cand->mFL_ATUALIZACAO_HABILITADA == '1') {
            $tmpl = new cTEMPLATE_CANDIDATO_EDIT_CLIENTE();
            cHTTP::$POST['CMP___nomeTemplate'] = 'cTEMPLATE_CANDIDATO_EDIT_CLIENTE';
        } else {
            $tmpl = new cTEMPLATE_CANDIDATO_VIEW_CLIENTE();
            cHTTP::$POST['CMP___nomeTemplate'] = 'cTEMPLATE_CANDIDATO_VIEW_CLIENTE';
        }
        $tmpl->CarregueDoHTTP();

        if (cHTTP::$SESSION["usuarioPerfil"] == '10') {
            if ($cand->mNU_EMPRESA != cHTTP::$SESSION['myAdmin']['cd_empresa']) {
                if (cHTTP::getLang() == 'pt_br') {
                    cHTTP::$SESSION['msg'] = 'Você está tentando acessar um candidato que não é da sua empresa. Para preservar o sigilo das informações dos nossos clientes, uma empresa não pode acessar informações de candidatos que estão em outra. Para maiores informações entre em contato com a Mundivisas.';
                } else {
                    cHTTP::$SESSION['msg'] = 'You are trying to access information from a candidate that is not from your company. To preseve our customers data, one company is not allowed to access candidate data from another. For further information please contact Mundivisas.';
                }
                cHTTP::RedirecionePara('cCTRL_CANDIDATO', 'ListarCandidatos_cliente', "", 'carregueCliente.php');
            }
        }

        if (cHTTP::HouvePost()) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        $tmpl->AtribuaAoOBJETO($cand);
                        $cand->AtualizarExterno();
                        if (cHTTP::getLang() == 'pt_br') {
                            $msg = 'Cadastro atualizado com sucesso.';
                        } else {
                            $msg = 'Data updated sucessfully.';
                        }
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                case 'SALVAR_E_FECHAR':
                    try {
                        $tmpl->AtribuaAoOBJETO($cand);
                        $cand->AtualizarExterno();
                        $cand->FecharParaAtualizacaoDeClientes();
                        if (cHTTP::getLang() == 'pt_br') {
                            $msg = 'Cadastro atualizado com sucesso. Aguarde contato do ponto focal responsável para os próximos passos.';
                        } else {
                            $msg = 'Data request updated sucessfully. Please wait for the focal point instructions regarding the next steps.';
                        }
                        cHTTP::$SESSION['msg'] = $msg;
                        $tmpl = new cTEMPLATE_CANDIDATO_VIEW_CLIENTE();
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
            }
            cHTTP::$SESSION['msg'] = $msg;
        }
        $rs = $cand->CursorCandidato_cliente(cHTTP::ParametroValido('NU_CANDIDATO'));
        $tmpl->CarregueDoRecordset($rs);

        if (cHTTP::getLang() == 'pt_br') {
            $tmpl->mTitulo = 'Dados Pessoais';
        } else {
            $tmpl->mTitulo = 'Candidate Data Requeriments';
        }
        $ret = '';
        $ret .= cINTERFACE::formTemplate($tmpl);
        $ret .= cINTERFACE::RenderizeTitulo($tmpl, false, cINTERFACE::titPAGINA);
        $ret .= '<div class="conteudoInterno">';
        if ($tmpl->mMsgInicial != '') {
            $ret.= $tmpl->mMsgInicial;
        }
        $tmpl->mCampo['NU_EMBARCACAO_PROJETO']->mExtra .= ' and NU_EMPRESA = ' . cHTTP::$SESSION['myAdmin']['cd_empresa'];
        $ret .= cINTERFACE::RenderizeTemplateEdicao($tmpl);
        $ret .= '</table>';
        $ret .= cINTERFACE::RenderizeBarraAcoesRodape($tmpl->mAcoes);
        $ret .= '</form>';
        $ret .= '</div>';
        $html = $ret;
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $ret = '';
        $tmplAba = new cTEMPLATE_CANDIDATO_CLIENTE_ABAS();
        $tmplAba->mCampo['NU_CANDIDATO']->mValor = cHTTP::ParametroValido('NU_CANDIDATO');
        $tmplAba->RepliqueChavesParaAbasAjax();
        // Adiciona a chave se houver como parametro nas abas
        $tmplAba->mTitulo = 'Detailed information of "' . $cand->mNOME_COMPLETO . '"';
//		$ret .= cINTERFACE::formTemplate($tmplAba);
        $ret .= cINTERFACE::RenderizeTitulo($tmplAba, false, cINTERFACE::titPAGINA);
        $ret .= '<div id="tabs">';
        $ret .= cINTERFACE::RenderizeIndiceDeAbas($tmplAba, 'DADOS_PESSOAIS');
        $ret .= '<div id="abaAtiva">';
        $ret .= $html;
        $ret .= '</div>';
        $ret .= '</div>';
//		$ret .= '</form>';
        $ret .= $tmplAba->TratamentoPadraoDeMensagens();
        cHTTP::AdicionarScript('ExibirMsg("#dialog", "' . cHTTP::TituloMsg() . '" );');
        return $ret;
    }

    public function VisaDetails_cliente() {
        $msg = '';
        cINTERFACE_AMBIENTE::ControleLinguagem();
        $intCand = new cINTERFACE_CANDIDATO();
        $intCand->mNU_CANDIDATO = cHTTP::ParametroValido('NU_CANDIDATO');
        $intCand->Recuperar();
        if ($_SESSION["usuarioPerfil"] == '10') {
            if ($intCand->mNU_EMPRESA != $_SESSION['myAdmin']['cd_empresa']) {
                header('Location:/index.php');
            }
        }

        $html = $intCand->FormVistoAtual();
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $ret = '';
        $tmplAba = new cTEMPLATE_CANDIDATO_CLIENTE_ABAS();
        $tmplAba->mCampo['NU_CANDIDATO']->mValor = cHTTP::ParametroValido('NU_CANDIDATO');
        $tmplAba->RepliqueChavesParaAbasAjax();
        // Adiciona a chave se houver como parametro nas abas
        $tmplAba->mTitulo = 'Detailed information of "' . $intCand->mNOME_COMPLETO . '"';
        $ret .= cINTERFACE::formTemplate($tmplAba);
        $ret .= cINTERFACE::RenderizeTitulo($tmplAba, false, cINTERFACE::titPAGINA);
        $ret .= '<div id="tabs">';
        $ret .= cINTERFACE::RenderizeIndiceDeAbas($tmplAba, 'DADOS_VISTO');
        $ret .= '<div id="abaAtiva">';
        $ret .= $html;
        $ret .= '</div>';
        $ret .= '</div>';
        $ret .= '</form>';
        return $ret;
    }

    /**
     * Monta o relatório de estrangeiros por projeto
     * @return string
     *
     * TODO: Adicionar botao para inativar todos
     * TODO: Adicionar campo editavel para observacoes do visto
     * TODO: Opçao para marcar os candidatos por checkbox.
     */
    public function RelatorioPorProjeto() {
        $cand = new cCANDIDATO();
        $pOrdem = '';
        cHTTP::$comCabecalhoFixo = true;
        $template = new cTMPL_REL_ESTRANGEIROS_PROJETO();
        $template->CarregueDoHTTP();
        $data = array();
        $data['paineis'] = array();
        $data['titulo_mensagem'] = 'success';
        if (isset(cHTTP::$POST['enviado'])) {
            // Monta filtros
            $filtrosTexto = '';
            $virgula = '';
            switch ($template->mFiltro['FL_INATIVO']->mValor) {
                case '0':
                    $filtrosTexto .= $virgula . ' somente candidatos ativos';
                    $virgula = ',';
                    break;
                case '1':
                    $filtrosTexto .= $virgula . ' somente candidatos inativos';
                    $virgula = ',';
                    break;
            }
            if ($template->mFiltro['tipo_candidato']->mValor != '') {
                switch ($template->mFiltro['tipo_candidato']->mValor) {
                    case '1':
                        $tipo_cand = ' MTE.codigo is not null ';
                        $filtrosTexto .= $virgula . ' somente candidatos que tem visto';
                        $virgula = ',';
                        break;
                    case '2':
                        $tipo_cand = ' MTE.codigo is null ';
                        $filtrosTexto .= $virgula . ' somente candidatos que tem não tem visto';
                        $virgula = ',';
                        break;
                    default:
                        $tipo_cand = '';
                }
                $template->mFiltro['tipo_candidato']->mWhere = $tipo_cand;
            }
            // Trata acao postada
            if (cHTTP::$POST['CMP___acao'] != '') {
                $acao = cHTTP::$POST['CMP___acao'];
                $parametros = cHTTP::$POST['CMP___parametros'];
            }
            $titulo_msg = "Mensagem";
            try {
                switch ($acao) {
                    case "LISTAR":
                        break;
                    case "INATIVAR":
                        $cand = new cCANDIDATO();
                        $parms = explode("=", $parametros);
                        if ($parms[0] == 'NU_CANDIDATO') {
                            $cand->mNU_CANDIDATO = $parms[1];
                            $cand->Inative(cSESSAO::$mcd_usuario);
                            $msg = "Candidato inativado com sucesso!";
                        }
                        break;
                    case "ATIVAR":
                        $cand = new cCANDIDATO();
                        $parms = explode("=", $parametros);
                        if ($parms[0] == 'NU_CANDIDATO') {
                            $cand->mNU_CANDIDATO = $parms[1];
                            $cand->Ative(cSESSAO::$mcd_usuario);
                            $msg = "Candidato reativado com sucesso!";
                        }
                    case "REVISAR":
                        $cand = new cCANDIDATO();
                        $parms = explode("=", $parametros);
                        if ($parms[0] == 'NU_CANDIDATO') {
                            $cand->mNU_CANDIDATO = $parms[1];
                            $cand->RegistreQueEstaRevisado(cSESSAO::$mcd_usuario);
                            $msg = "Candidato revisado com sucesso!";
                        }
                        break;
                    case "DESREVISAR":
                        $cand = new cCANDIDATO();
                        $parms = explode("=", $parametros);
                        if ($parms[0] == 'NU_CANDIDATO') {
                            $cand->mNU_CANDIDATO = $parms[1];
                            $cand->RegistreQueEstaRevisado(cSESSAO::$mcd_usuario);
                            $msg = "Candidato revisado com sucesso!";
                        }
                        break;
                    case "PENDENTE":
                        $cand = new cCANDIDATO();
                        $parms = explode("=", $parametros);
                        if ($parms[0] == 'NU_CANDIDATO') {
                            $cand->mNU_CANDIDATO = $parms[1];
                            $cand->ColocarRevisaoPendente(cSESSAO::$mcd_usuario);
                            $msg = "Candidato alterado com sucesso!";
                        }
                        break;
                    case "REVISAR_TODOS":
                        $cand = new cCANDIDATO();
                        $cand->RegistreQueEstaRevisadoPorEmbarcacao(cSESSAO::$mcd_usuario, $template->mFiltro);
                        $msg = "Candidatos revisado com sucesso!";
                        break;
                    case "EXCLUIR":
                        $cand = new cCANDIDATO();
                        $parms = explode("=", $parametros);
                        if ($parms[0] == 'NU_CANDIDATO') {
                            $cand->mNU_CANDIDATO = $parms[1];
                            $cand->SoliciteExclusao(cSESSAO::$mcd_usuario);
                            $msg = "Candidato marcado para exclusão com sucesso!";
                        }
                        break;
                    case "INATIVAR_SELECIONADOS":
                        $cand = new cCANDIDATO();
                        if (isset(cHTTP::$POST[seletor])) {
                            $lote = '';
                            $virg = '';
                            foreach (cHTTP::$POST[seletor] as $NU_CANDIDATO) {
                                $lote .= $virg . intval($NU_CANDIDATO);
                                $virg = ',';
                            }
                            if ($lote != '') {
                                $cand->InativeLote(cSESSAO::$mcd_usuario, $lote);
                                $msg = "Candidato selecionados marcados como inativos com sucesso!";
                            } else {
                                throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                            }
                        } else {
                            throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                        }
                        break;
                    case "REVISAR_SELECIONADOS":
                        $cand = new cCANDIDATO();
                        if (isset(cHTTP::$POST[seletor])) {
                            $lote = '';
                            $virg = '';
                            foreach (cHTTP::$POST[seletor] as $NU_CANDIDATO) {
                                $lote .= $virg . intval($NU_CANDIDATO);
                                $virg = ',';
                            }
                            if ($lote != '') {
                                $cand->RegistreQueEstaRevisadoLote(cSESSAO::$mcd_usuario, $lote);
                                $msg = "Candidato selecionados marcados como revisados com sucesso!";
                            } else {
                                throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                            }
                        } else {
                            throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                        }
                        break;
                    case "PENDENTE_SELECIONADOS":
                        $cand = new cCANDIDATO();
                        if (isset(cHTTP::$POST[seletor])) {
                            $lote = '';
                            $virg = '';
                            foreach (cHTTP::$POST[seletor] as $NU_CANDIDATO) {
                                $lote .= $virg . intval($NU_CANDIDATO);
                                $virg = ',';
                            }
                            if ($lote != '') {
                                $cand->ColocarRevisaoPendenteLote(cSESSAO::$mcd_usuario, $lote);
                                $msg = "Candidato selecionados marcados como pendentes com sucesso!";
                            } else {
                                throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                            }
                        } else {
                            throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                        }
                        break;
                    case "EXCLUIR_SELECIONADOS":
                        $cand = new cCANDIDATO();
                        if (isset(cHTTP::$POST[seletor])) {
                            $lote = '';
                            $virg = '';
                            foreach (cHTTP::$POST[seletor] as $NU_CANDIDATO) {
                                $lote .= $virg . intval($NU_CANDIDATO);
                                $virg = ',';
                            }
                            if ($lote != '') {
                                $cand->SoliciteExclusaoLote(cSESSAO::$mcd_usuario, $lote);
                                $msg = "Candidato selecionados marcados para exclusão com sucesso!";
                            } else {
                                throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                            }
                        } else {
                            throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                        }
                        break;
                    case "EMBARCAR_SELECIONADOS":
                        $cand = new cCANDIDATO();
                        if (isset(cHTTP::$POST[seletor])) {
                            $lote = '';
                            $virg = '';
                            foreach (cHTTP::$POST[seletor] as $NU_CANDIDATO) {
                                $lote .= $virg . intval($NU_CANDIDATO);
                                $virg = ',';
                            }
                            if ($lote != '') {
                                $cand->TornarEmbarcadoLote(cSESSAO::$mcd_usuario, $lote);
                                $msg = "Candidato selecionados marcados como embarcados com sucesso!";
                            } else {
                                throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                            }
                        } else {
                            throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                        }
                        break;
                    case "DESEMBARCAR_SELECIONADOS":
                        $cand = new cCANDIDATO();
                        if (isset(cHTTP::$POST[seletor])) {
                            $lote = '';
                            $virg = '';
                            foreach (cHTTP::$POST[seletor] as $NU_CANDIDATO) {
                                $lote .= $virg . intval($NU_CANDIDATO);
                                $virg = ',';
                            }
                            if ($lote != '') {
                                $cand->TornarDesembarcadoLote(cSESSAO::$mcd_usuario, $lote);
                                $msg = "Candidato selecionados marcados como embarcados com sucesso!";
                            } else {
                                throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                            }
                        } else {
                            throw new cERRO_CONSISTENCIA('- nenhum candidato foi selecionado.');
                        }
                        break;
                    case "REVISAR_EMBARCACAO":
                        if ($template->getValorFiltro('NU_EMBARCACAO_PROJETO') != '') {
                            $embp = new cEMBARCACAO_PROJETO();
                            $embp->mNU_EMPRESA = $template->getValorFiltro('NU_EMPRESA');
                            $embp->mNU_EMBARCACAO_PROJETO = $template->getValorFiltro('NU_EMBARCACAO_PROJETO');
                            $embp->RecupereSe();
                            $embp->AtualizeUltimaRevisao(cSESSAO::$mcd_usuario);
                            $msg = "Embarcação " . $embp->mNO_EMBARCACAO_PROJETO . " marcada como revisada com sucesso!";
                        } else {
                            throw new cERRO_CONSISTENCIA('- é preciso que uma embarcação esteja selecionada no filtro para marcá-la como revisada');
                        }
                        break;
                    case "SALVAR_COLUNAS":
                        $cfgr_id = $_POST['cfgr_idx'];
                        if ($cfgr_id > 0) {
                            $template->salvarConfiguracao($_POST['cfgr_idx'], '');
                            $msg = "Configuração atualizada com sucesso";
                        } else {
                            $msg = "Selecione uma configuração";
                        }
                        break;
                    case "CRIAR_CONFIGURACAO":
                        $cfgr_id = $template->criarConfiguracao($_POST['nome_configuracao'], '');
                        $msg = "Configuração criada com sucesso";
                        $template->mCampos['cfgr_idx']->mCombo->mValor = $cfgr_id;
                        break;
                    case "USAR_COLUNAS":
                        $sql = "select * from configuracao_relatorio_coluna where cfgr_id = " . $_POST['cfgr_idx'];
                        $res = cAMBIENTE::$db_pdo->query($sql);
                        $rs = $res->fetchAll();
                        foreach ($template->mCampo as &$campo) {
                            $campo->mColunaAtiva = false;
                            $campo->mValor = false;
                        }
                        foreach ($rs as $reg) {
                            $template->mCampo[$reg['cfgc_nome_coluna']]->mColunaAtiva = 1;
                        }
                        break;
                    case "":
                        throw new cERRO_CONSISTENCIA('- a ação não foi selecionada.');
                        break;
                    default:
                        throw new cERRO_CONSISTENCIA('- a ação ' . $acao . ' não estava prevista.');
                }
            } catch (cERRO_CONSISTENCIA $e) {
                $retorno = '-1';
                $msg = "Não foi possível executar a solicitação pois:" . $e->mMsg;
                $data['titulo_mensagem'] = 'warning';
            } catch (cERRO_SQL $e) {
                $retorno = '-1';
                $msg = "Não foi possível executar a solicitação. Encaminhe essas informações ao suporte para agilizar a resolução do problema:<br/>" . htmlentities($e->mMsg);
                $data['titulo_mensagem'] = 'error';
            } catch (Exception $e) {
                $retorno = '-1';
                $msg = "Não foi possível executar a solicitação. Encaminhe essas informações ao suporte para agilizar a resolução do problema:<br>" . htmlentities($e->getMessage(), ENT_QUOTES);
                $data['titulo_mensagem'] = 'error';
            }
            // Obtem candidatos selecionados
            $candidatos = array();

            if ($template->mFiltro['excluir_brasileiros']->mValor == 1) {
                $template->mFiltro['excluir_brasileiros']->mWhere = " C.CO_NACIONALIDADE <> 30";
            } else {
                $template->mFiltro['excluir_brasileiros']->mWhere = '1=1';
            }

            if ($template->mFiltro['excluir_sem_nac']->mValor == 1) {
                $template->mFiltro['excluir_sem_nac']->mWhere = " C.CO_NACIONALIDADE is not null and C.CO_NACIONALIDADE <> ''";
            } else {
                $template->mFiltro['excluir_sem_nac']->mWhere = '1=1';
            }

            if ($template->mFiltro['NU_EMBARCACAO_PROJETO']->mValor == '-1') {
                $template->mFiltro['NU_EMBARCACAO_PROJETO']->mWhere = " C.NU_EMBARCACAO_PROJETO is null";
            }


            if ($cand->QtdCandidatos($template->mFiltro) < 1000) {
                $res = $cand->CursorCandidatosPorEmbarcacao($template->mFiltro, $pOrdem);

                // Monta dados
                $i = 0;
                while ($rw = $res->fetch(PDO::FETCH_ASSOC)) {
                    $acoes = '';

                    $template->mAcoesLinha['EXCLUIR']->mMsgConfirmacao = 'Confirma a exclusão do candidato ' . $rw['NOME_COMPLETO'] . ' (' . $rw['NU_CANDIDATO'] . ')?';
                    if ($rw['FL_INATIVO'] == 1) {
                        $template->mAcoesLinha['INATIVAR']->mVisivel = false;
                        $template->mAcoesLinha['ATIVAR']->mVisivel = true;
                    } else {
                        $template->mAcoesLinha['INATIVAR']->mVisivel = true;
                        $template->mAcoesLinha['ATIVAR']->mVisivel = false;
                    }

                    if ($rw['cand_fl_revisado'] == 1) {
                        $template->mAcoesLinha['REVISAR']->mVisivel = false;
                        $template->mAcoesLinha['DESREVISAR']->mVisivel = true;
                        $template->mAcoesLinha['PENDENTE']->mVisivel = true;
                    } elseif ($rw['cand_fl_revisado'] == 2) {
                        $template->mAcoesLinha['REVISAR']->mVisivel = true;
                        $template->mAcoesLinha['DESREVISAR']->mVisivel = true;
                        $template->mAcoesLinha['PENDENTE']->mVisivel = false;
                    } else {
                        $template->mAcoesLinha['REVISAR']->mVisivel = true;
                        $template->mAcoesLinha['DESREVISAR']->mVisivel = false;
                        $template->mAcoesLinha['PENDENTE']->mVisivel = true;
                    }
                    if ($rw['cand_fl_revisado'] != '') {
                        $rw['revisado'] .= '<br/><span style="color:#999;font-size:9px;">(' . $rw['nome_revisao'] . ' em ' . $rw['cand_dt_revisao'] . ')</span>';
                    }

                    if ($rw['NU_PROCESSO_MTE'] != '' && strlen($rw['NU_PROCESSO_MTE']) >= 17) {
                        $rw['NU_PROCESSO_MTE'] = cINTERFACE::formatarMascara(preg_replace("/[^0-9]/", "", $rw['NU_PROCESSO_MTE']), '#####.######/####-##');
                    }

                    if ($rw['NU_PROTOCOLO_REG'] != '' && strlen($rw['NU_PROTOCOLO_REG']) >= 17) {
                        $rw['NU_PROTOCOLO_REG'] = cINTERFACE::formatarMascara(preg_replace("/[^0-9]/", "", $rw['NU_PROTOCOLO_REG']), '#####.######/####-##');
                    }

                    if ($rw['NU_PROTOCOLO_PRO'] != '' && strlen($rw['NU_PROTOCOLO_PRO']) >= 17) {
                        $rw['NU_PROTOCOLO_PRO'] = cINTERFACE::formatarMascara(preg_replace("/[^0-9]/", "", $rw['NU_PROTOCOLO_PRO']), '#####.######/####-##');
                    }

                    if ($rw['nu_protocolo_rest'] != '' && strlen($rw['nu_protocolo_rest']) >= 17) {
                        $rw['nu_protocolo_rest'] = cINTERFACE::formatarMascara(preg_replace("/[^0-9]/", "", $rw['nu_protocolo_rest']), '#####.######/####-##');
                    }

                    if ($rw['codigo_PRO'] == '') {
                        $rw['NO_STATUS_CONCLUSAO_PRO'] = '';
                    } else {
                        if ($rw['NO_STATUS_CONCLUSAO_PRO'] == '') {
                            $rw['NO_STATUS_CONCLUSAO_PRO'] = 'Em análise';
                        }
                    }
                    if ($rw['FL_EMBARCADO'] == '1') {
                        $rw['FL_EMBARCADO'] = 'Sim';
                    } else {
                        $rw['FL_EMBARCADO'] = 'Não';
                    }
                    // Monta Dias Para Expiração visto
                    $xexpiracao = $rw['DIAS_EXPIRACAO_DEFERIMENTO'];
                    if ($rw['DT_ENTRADA'] != '' || $rw['NU_PROTOCOLO_REG'] != '' || $rw['DT_PRAZO_ESTADA'] != '') {
                        $xexpiracao = '<span style="color:#cccccc;">(coletado)</span>';
                    } else {
                        if ($rw['DIAS_EXPIRACAO_DEFERIMENTO'] < 0) {
                            $xexpiracao = '<span style="color:red;">' . $rw['DIAS_EXPIRACAO_DEFERIMENTO'] . '</span>';
                        } elseif ($rw['DIAS_EXPIRACAO_DEFERIMENTO'] < 30) {
                            $xexpiracao = '<span style="color:#ffaa00;">' . $rw['DIAS_EXPIRACAO_DEFERIMENTO'] . '</span>';
                        }
                    }
                    $rw['DIAS_EXPIRACAO_DEFERIMENTO'] = $xexpiracao;
                    //
                    // Monta campos de documentos
                    if (cdocumento::TemDisponivel($rw['NU_CANDIDATO'], '1')) {
                        $rw['fl_tem_copia_passaporte'] = 'Sim';
                    } else {
                        $rw['fl_tem_copia_passaporte'] = 'Não';
                    }

                    if (cdocumento::TemDisponivel($rw['NU_CANDIDATO'], '2')) {
                        $rw['fl_tem_form1344'] = 'Sim';
                    } else {
                        $rw['fl_tem_form1344'] = 'Não';
                    }
                    //
                    // Diferencia colunas quando nao há o processo
                    if ($rw['codigo_mte'] == '') {
                        $rw['NU_PROCESSO_MTE'] = 'n/d';
                        $rw['DT_REQUERIMENTO'] = 'n/d';
                        $rw['CONSULADO'] = 'n/d';
                        $rw['PRAZO_SOLICITADO'] = 'n/d';
                        $rw['DT_DEFERIMENTO'] = 'n/d';
                        $rw['DIAS_EXPIRACAO_DEFERIMENTO'] = 'n/d';
                        $rw['OBSERVACAO_VISTO'] = 'n/d';
                    }
                    //
                    // Monta o status do visto
                    $xcand = new cCANDIDATO();
                    $xcand->mNU_CANDIDATO = $rw['NU_CANDIDATO'];
                    $xcand->mcodigo_processo_mte_atual = $rw['codigo_processo_mte_atual'];
                    $visto = $xcand->VistoAtual();
                    if (isset($visto['autorizacao'])) {
                        $rw['situacao_prazo_estada'] = $visto['autorizacao']->get_situacao_prazo_estada();
                        $rw['situacao_texto'] = $visto['autorizacao']->get_situacao_texto();
                        $rw['situacao_origem'] = $visto['autorizacao']->get_situacao_origem();
                    } else {
                        $rw['situacao_prazo_estada'] = '--';
                        $rw['situacao_texto'] = '(sem visto)';
                        $rw['situacao_origem'] = '(sem visto)';
                    }

                    if (intval($rw['codigo_REG']) > 0) {
                        if ($rw['DT_PRAZO_ESTADA'] == '') {
                            $rw['DT_PRAZO_ESTADA'] = '??';
                        }
                        if ($rw['DT_VALIDADE_REG'] == '') {
                            $rw['DT_VALIDADE_REG'] = '??';
                        }
                        if ($rw['DT_REQUERIMENTO_CIE'] == '') {
                            $rw['DT_REQUERIMENTO_CIE'] = '??';
                        }
                        if ($rw['NU_PROTOCOLO_REG'] == '') {
                            $rw['NU_PROTOCOLO_REG'] = '??';
                        }
                        if ($rw['DT_VALIDADE_REG'] == '') {
                            $rw['DT_VALIDADE_REG'] = '??';
                        }
                        if ($rw['depf_no_nome'] == '') {
                            $rw['depf_no_nome'] = '??';
                        }
                    }

                    if (count($template->mAcoesLinha) > 0) {
                        $template->mAcoesLinha['REVISAR']->mOnClick = "ExecutarAcao(this, 'cCTRL_CANDIDATO_STATUS', 'Revise', 'NU_CANDIDATO=" . $rw['NU_CANDIDATO'] . "', 'fl_inativo', '')";
                        $template->mAcoesLinha['DESREVISAR']->mOnClick = "ExecutarAcao(this, 'cCTRL_CANDIDATO_STATUS', 'Desrevise', 'NU_CANDIDATO=" . $rw['NU_CANDIDATO'] . "', 'fl_inativo', '')";
                        $template->mAcoesLinha['PENDENTE']->mOnClick = "ExecutarAcao(this, 'cCTRL_CANDIDATO_STATUS', 'Pendente', 'NU_CANDIDATO=" . $rw['NU_CANDIDATO'] . "', 'fl_inativo', '')";
                        $template->mAcoesLinha['INATIVAR']->mOnClick = "ExecutarAcao(this, 'cCTRL_CANDIDATO_STATUS', 'Inative', 'NU_CANDIDATO=" . $rw['NU_CANDIDATO'] . "', 'fl_inativo', '')";
                        $template->mAcoesLinha['ATIVAR']->mOnClick = "ExecutarAcao(this, 'cCTRL_CANDIDATO_STATUS', 'Ative', 'NU_CANDIDATO=" . $rw['NU_CANDIDATO'] . "', 'fl_inativo', '')";
                        $template->mAcoesLinha['EXCLUIR']->mOnClick = "ExecutarAcao(this, 'cCTRL_CANDIDATO_STATUS', 'Exclua', 'NU_CANDIDATO=" . $rw['NU_CANDIDATO'] . "', 'fl_inativo', '')";
                        foreach ($template->mAcoesLinha as &$acao) {
                            $parametros = '';
                            if ($acao->mVisivel) {
                                foreach ($template->mCampo as $campo) {
                                    if ($campo->mChave) {
                                        $parametros = $campo->mCampoBD . '=' . $rw[$campo->mCampoBD];
                                    }
                                }
                                $acao->mParametros = $parametros;
                                $acoes.= cINTERFACE::RenderizeAcao($acao);
                            }
                        }
                    }

                    if ($acoes == '') {
                        $acoes = '&nbsp;';
                    }

                    $rw["acoes"] = $acoes;
                    $i++;
                    $rw['i'] = $i;

                    $candidatos[] = $rw;
                }
                $data['candidatos'] = $candidatos;
                $data['temcandidatos'] = true;
            }
        }
        if ($msg != '') {
//			$msg .= "<br/><br/>(pressione esc para fechar)";
        }

        $data['mensagem'] = $msg;
        $data['titulo_mensagem'] = $titulo_msg;
        $data['configuracao'] = cconfiguracao_relatorio::comboMustache($_POST['cfgr_idx']);
        $filtros = $template->FiltrosMustache();
        $data["qtdtotal"] = $i;
        $data["nome_template"] = $template->mnome;
        $data["filtros"] = $filtros;
        $data['colunas'] = $template->MustacheSeletorDeColunas();
        $ret = '';
        $tmpl = cFABRICA_TEMPLATE::singleton()->loadTemplate('rel_candidatos.mustache');
        $ret .= $tmpl->render($data);

        return $ret;

//		die($m->render($template, $data, $partials));
    }

    public function RelatorioPorProjetoExcel() {
        cHTTP::$MIME = cHTTP::mmXLS;
        $cand = new cCANDIDATO();
        $pOrdem = '';
        cHTTP::$comCabecalhoFixo = true;
        $template = new cTMPL_REL_ESTRANGEIROS_PROJETO();
        $template->CarregueDoHTTP();
        $data = array();
        // Monta filtros
        if ($template->mFiltro['situacao_cadastral']->mValor != '') {
            $template->mFiltro['situacao_cadastral']->mWhere = " C.FL_INATIVO = " . $template->mFiltro['situacao_cadastral']->mValor;
        }
        if ($template->mFiltro['tipo_candidato']->mValor != '') {
            switch ($template->mFiltro['tipo_candidato']->mValor) {
                case '1':
                    $tipo_cand = ' MTE.codigo is not null ';
                    break;
                case '2':
                    $tipo_cand = ' MTE.codigo is null ';
                    break;
                default:
                    $tipo_cand = '';
            }
            $template->mFiltro['tipo_candidato']->mWhere = $tipo_cand;
        }
        if ($template->mFiltro['excluir_brasileiros']->mValor == 1) {
            $template->mFiltro['excluir_brasileiros']->mWhere = " C.CO_NACIONALIDADE <> 30";
        } else {
            $template->mFiltro['excluir_brasileiros']->mWhere = '1=1';
        }

        if ($template->mFiltro['excluir_sem_nac']->mValor == 1) {
            $template->mFiltro['excluir_sem_nac']->mWhere = " C.CO_NACIONALIDADE is not null and C.CO_NACIONALIDADE <> ''";
        } else {
            $template->mFiltro['excluir_sem_nac']->mWhere = '1=1';
        }

        if ($template->mFiltro['NU_EMBARCACAO_PROJETO']->mValor == '-1') {
            $template->mFiltro['NU_EMBARCACAO_PROJETO']->mWhere = " C.NU_EMBARCACAO_PROJETO is null";
        }

        // Obtem candidatos selecionados
        $res = $cand->CursorCandidatosPorEmbarcacao($template->mFiltro, $pOrdem);
        $candidatos = array();

        // Monta dados
        while ($rw = $res->fetch(PDO::FETCH_ASSOC)) {
            if ($rw['cand_fl_revisado'] == 1) {
                $rw['revisado'] = "Sim";
            } elseif ($rw['cand_fl_revisado'] == 2) {
                $rw['revisado'] = "Pendente";
            } else {
                $rw['revisado'] = '--';
            }

            if ($rw['NU_PROCESSO_MTE'] != '' && strlen($rw['NU_PROCESSO_MTE']) >= 17) {
                $rw['NU_PROCESSO_MTE'] = cINTERFACE::formatarMascara(preg_replace("/[^0-9]/", "", $rw['NU_PROCESSO_MTE']), '#####.######/####-##');
            }

            if ($rw['NU_PROTOCOLO_REG'] != '' && strlen($rw['NU_PROTOCOLO_REG']) >= 17) {
                $rw['NU_PROTOCOLO_REG'] = cINTERFACE::formatarMascara(preg_replace("/[^0-9]/", "", $rw['NU_PROTOCOLO_REG']), '#####.######/####-##');
            }

            if ($rw['NU_PROTOCOLO_PRO'] != '' && strlen($rw['NU_PROTOCOLO_PRO']) >= 17) {
                $rw['NU_PROTOCOLO_PRO'] = cINTERFACE::formatarMascara(preg_replace("/[^0-9]/", "", $rw['NU_PROTOCOLO_PRO']), '#####.######/####-##');
            }

            if ($rw['nu_protocolo_rest'] != '' && strlen($rw['nu_protocolo_rest']) >= 17) {
                $rw['nu_protocolo_rest'] = cINTERFACE::formatarMascara(preg_replace("/[^0-9]/", "", $rw['nu_protocolo_rest']), '#####.######/####-##');
            }

            if ($rw['codigo_PRO'] == '') {
                $rw['NO_STATUS_CONCLUSAO_PRO'] = '';
            } else {
                if ($rw['NO_STATUS_CONCLUSAO_PRO'] == '') {
                    $rw['NO_STATUS_CONCLUSAO_PRO'] = 'Em análise';
                }
            }

            if ($rw['FL_EMBARCADO'] == '1') {
                $rw['FL_EMBARCADO'] = 'Sim';
            } else {
                $rw['FL_EMBARCADO'] = 'Não';
            }
            //
            // Monta campos de documentos
            if (cdocumento::TemDisponivel($rw['NU_CANDIDATO'], '1')) {
                $rw['fl_tem_copia_passaporte'] = 'Sim';
            } else {
                $rw['fl_tem_copia_passaporte'] = 'Não';
            }

            if (cdocumento::TemDisponivel($rw['NU_CANDIDATO'], '2')) {
                $rw['fl_tem_form1344'] = 'Sim';
            } else {
                $rw['fl_tem_form1344'] = 'Não';
            }

            if (intval($rw['codigo_REG']) > 0) {
                if ($rw['DT_PRAZO_ESTADA'] == '') {
                    $rw['DT_PRAZO_ESTADA'] = '??';
                }
                if ($rw['DT_VALIDADE_REG'] == '') {
                    $rw['DT_VALIDADE_REG'] = '??';
                }
                if ($rw['DT_REQUERIMENTO_CIE'] == '') {
                    $rw['DT_REQUERIMENTO_CIE'] = '??';
                }
                if ($rw['NU_PROTOCOLO_REG'] == '') {
                    $rw['NU_PROTOCOLO_REG'] = '??';
                }
                if ($rw['DT_VALIDADE_REG'] == '') {
                    $rw['DT_VALIDADE_REG'] = '??';
                }
                if ($rw['depf_no_nome'] == '') {
                    $rw['depf_no_nome'] = '??';
                }
            }

            $rw['TE_OBSERVACAO_CREWLIST'] = str_replace("\n", '<br>', $rw['TE_OBSERVACAO_CREWLIST']);

            //
            // Monta o status do visto
            $xcand = new cCANDIDATO();
            $xcand->mNU_CANDIDATO = $rw['NU_CANDIDATO'];
            $xcand->mcodigo_processo_mte_atual = $rw['codigo_processo_mte_atual'];
            $visto = $xcand->VistoAtual();
            if (isset($visto['autorizacao'])) {
                $rw['situacao_prazo_estada'] = $visto['autorizacao']->get_situacao_prazo_estada();
                $rw['situacao_texto'] = $visto['autorizacao']->get_situacao_texto();
                $rw['situacao_origem'] = $visto['autorizacao']->get_situacao_origem();
            } else {
                $rw['situacao_prazo_estada'] = '--';
                $rw['situacao_texto'] = '(sem visto)';
                $rw['situacao_origem'] = '(sem visto)';
            }

            $candidatos[] = $rw;
        }
        if ($template->mFiltro['NU_EMPRESA']->mValor != '') {
            $empresa = new cEMPRESA();
            $empresa->mNU_EMPRESA = $template->mFiltro['NU_EMPRESA']->mValor;
            $empresa->RecuperePeloId();
            $data['NO_RAZAO_SOCIAL'] = $empresa->mNO_RAZAO_SOCIAL;
        }

        if ($template->mFiltro['NU_EMBARCACAO_PROJETO']->mValor != '') {
            $embp = new cEMBARCACAO_PROJETO();
            $embp->mNU_EMPRESA = $template->mFiltro['NU_EMPRESA']->mValor;
            $embp->mNU_EMBARCACAO_PROJETO = $template->mFiltro['NU_EMBARCACAO_PROJETO']->mValor;
            $embp->RecupereSe();
            $data['NO_EMBARCACAO_PROJETO'] = $embp->mNO_EMBARCACAO_PROJETO;
        }

        $data['candidatos'] = $candidatos;
        $filtros = $template->FiltrosMustache();
        $data["nome_template"] = $template->mnome;
        $data["filtros"] = $filtros;
        $data['colunas'] = $template->MustacheSeletorDeColunas();
        $data['qtd_colunas'] = $template->qtd_colunas_ativas;
        $tz_object = new DateTimeZone('America/Sao_Paulo');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        $data['datahora'] =  $datetime->format('d/m/Y H:i:s');
        $ret = '';
        $tmpl = cFABRICA_TEMPLATE::singleton()->loadTemplate('rel_candidatos_excel.mustache');
        $ret .= $tmpl->render($data);
        $ret = "\xEF\xBB\xBF".$ret;
        return $ret;

//		die($m->render($template, $data, $partials));
    }

    public function GraficoRevisao() {

        $data = new DateTime('2012-10-08');
        $datas = array();
        while ($data->format('Y-m-d') < '2012-12-21') {
            $datas[] = $data->format('d/m/y');
            $data->add(new DateInterval('P1D'));
        }

        $valoresReais = array();
        $sql = "select date_format(cand_dt_revisao, '%Y-%m-%d') data, count(*) qtd
				from candidato c
				join empresa e on e.nu_empresa = c.nu_empresa
				where cand_dt_revisao is not null
				and c.dt_cadastramento < '2012-01-01'
				group by date_format(cand_dt_revisao, '%Y-%m-%d')";
        $cursor = cAMBIENTE::$db_pdo->query($sql);
        $data = new DateTime('2012-10-08');
        $valoresReais[] = 0;
        while ($rs = $cursor->fetch(PDO::FETCH_BOTH)) {
            $ult_data_banco = $rs['data'];
            if ($rs['data'] > $data->format('Y-m-d')) {
                while ($rs['data'] > $data->format('Y-m-d')) {
                    $valoresReais[] = $valoresReais[count($valoresReais) - 1];
                    $data->add(new DateInterval('P1D'));
                }
            }
            $valoresReais[] = $valoresReais[count($valoresReais) - 1] + $rs['qtd'];
            $data->add(new DateInterval('P1D'));
        }

        $valoresPrevistos = array();
        $valoresPrevistos[] = 0;
        $data = new DateTime('2012-10-08');
        while ($data->format('Y-m-d') < '2012-12-21') {
            $valoresPrevistos[] = $valoresPrevistos[count($valoresPrevistos) - 1] + 322.351351351;
            $data->add(new DateInterval('P1D'));
        }

        $default_dot = new dot();
        $default_dot->size(1)->colour('#DFC329');

        $line_dot = new line();
//		$line_dot->set_default_dot_style($default_dot);
        $line_dot->set_width(1);
        $line_dot->set_colour('#F2000C');
        $line_dot->set_values($valoresReais);
        $line_dot->set_key("Realizado", 10);


        $default_hollow_dot = new hollow_dot();
        $default_hollow_dot->size(1)->colour('#6363AC');

        $line_hollow = new line();
//		$line_hollow->set_default_dot_style($default_hollow_dot);
        $line_hollow->set_width(1);
        $line_hollow->set_colour('#20993C');
        $line_hollow->set_values($valoresPrevistos);
        $line_hollow->line_style(new line_style(4, 3));
        $line_hollow->set_key("Previsto", 10);

//		$star = new star();
//		$star->size(5);
//
//		$line = new line();
//		$line_hollow->set_default_dot_style($star);
//		$line->set_width( 1 );
//		$line->set_colour( '#5E4725' );
//		$line->set_values( $data_3 );
//		$line->set_key( "Line 3", 10 );

        $y = new y_axis();
        $y->set_range(0, 30100, 3000);

        $x_labels = new x_axis_labels();
        $x_labels->set_steps(7);
        $x_labels->set_vertical();
        $x_labels->set_colour('#A2ACBA');
        $x_labels->set_labels($datas);

        $x = new x_axis();
        $x->set_steps(7);
        $x->set_labels($x_labels);

        $chart = new open_flash_chart();
        $title = new title('Acompanhamento das revisoes do cadastro de candidatos');
        $title->set_style("{font-size: 16px; font-family: Arial; font-weight: bold; color: #333; text-align: center;}");

        $chart->set_title($title);
        $chart->set_y_axis($y);
        $chart->set_x_axis($x);

        //
        // here we add our data sets to the chart:
        //
		$chart->add_element($line_dot);
        $chart->add_element($line_hollow);
//		$chart->add_element( $line );

        echo $chart->toPrettyString();
    }

    public function CorrijaVistoAtual() {
        $NU_CANDIDATO = cHTTP::ParametroValido('NU_CANDIDATO');
        if (!intval($NU_CANDIDATO) > 0) {
            throw new Exception("NU_CANDIDATO não informado");
        }
        $cand = new cCANDIDATO();
        $cand->mNU_CANDIDATO = $NU_CANDIDATO;
        $cand->Recuperar();
        $codigoAnt = $cand->mcodigo_processo_mte_atual;
        $cand->AtualizeVistoAtual();
        $codigoDep = $cand->mcodigo_processo_mte_atual;
        print "Visto atualizado com sucesso. De " . $codigoAnt . " para " . $codigoDep;
    }

    public function CadastraDependentes() {
        $sql = 'select distinct d.NU_CANDIDATO from dependentes d, CANDIDATO c where c.NU_CANDIDATO = d.NU_CANDIDATO and coalesce(c.FL_INATIVO, 0) = 0 order by c.NU_EMPRESA';
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $cand = new cCANDIDATO();
            $cand->Recuperar($rs['NU_CANDIDATO']);
            $ret = $cand->CadastraDependentes($rs['NU_CANDIDATO']);
            print $ret;
        }
    }

    public function CorrigeVistoAtual() {
        $sql = "select nu_candidato, nome_completo from candidato where codigo_processo_mte_atual not in(select codigo from processo_mte)";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        print "<br/>Iniciando processamento";
        while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $cand = new cCANDIDATO();
            $cand->mNU_CANDIDATO = $rs['nu_candidato'];
            $cand->Recuperar();
            print "<br/>Visto do candidato " . $rs['nome_completo'];
            $codigoAnt = $cand->mcodigo_processo_mte_atual;
            $cand->AtualizeVistoAtual();
            $codigoDep = $cand->mcodigo_processo_mte_atual;
            print "... atualizado com sucesso. De " . $codigoAnt . " para " . $codigoDep;
        }
        $sql = "update processo_prorrog set codigo_processo_mte = null where codigo_processo_mte not in (select codigo from processo_mte)";
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $sql = "update processo_regcie set codigo_processo_mte = null where codigo_processo_mte not in (select codigo from processo_mte)";
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $sql = "update processo_emiscie set codigo_processo_mte = null where codigo_processo_mte not in (select codigo from processo_mte)";
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $sql = "update processo_cancel set codigo_processo_mte = null where codigo_processo_mte not in (select codigo from processo_mte)";
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function pesquisarParaOs() {
        $nome_completo = cHTTP::ParametroValido('nome_completo');
        $nu_servico = cHTTP::ParametroValido('nu_servico');
        $nu_empresa = cHTTP::ParametroValido('nu_empresa');
        $id_solicita_visto = cHTTP::ParametroValido('id_solicita_visto');
        if ($nome_completo != '') {
            $rep = new cREPOSITORIO_CANDIDATO;
            $pesquisa = $rep->pesquisaParaOs($nome_completo);
            $candidatos = $pesquisa['registros'];
            if ($pesquisa['total'] > 20) {
                $ignorados = $pesquisa['total'] - 20;
                $msg = "(" . $ignorados . " registros ignorados. Refine sua pesquisa)";
            }
        } else {
            $candidato = array();
            $msg = '';
        }

        foreach ($candidatos as &$candidato) {
            // Verifica se existe OS desse serviço aberta para esse candidato.
            $candidato['os_aberta'] = $rep->osNaoConcluida($nu_empresa, $nu_servico, $candidato['nu_candidato']);
            $candidato['os_pendente'] = $rep->osDePacotePendentes($nu_empresa, $nu_servico, $candidato['nu_candidato']);
            if ($candidato['os_aberta'] || $candidato['os_pendente']) {
                $candidato['inclusao_disponivel'] = false;
            } else {
                $candidato['inclusao_disponivel'] = true;
            }
        }

        $data = array(
            'candidatos' => $candidatos
            , 'nu_empresa' => $nu_empresa
            , 'nu_servico' => $nu_servico
            , 'nome_completo' => $nome_completo
            , 'id_solicita_visto' => $id_solicita_visto
            , 'html' => array('charset' => cLAYOUT::CHARSET)
            , 'msg' => $msg
        );

        $html = cTWIG::Render('intranet/os/candidatos_encontrados.twig', $data);
        $html = utf8_encode($html);
        return $html;
    }

    public function excluir() {
        try{
            if (cSESSAO::$mcd_perfil == 1) {
                $rep = new cREPOSITORIO_CANDIDATO;

            } else {
            }
        }
        catch (Exception $e){

        }
    }

    public function pesquisarHomonimos() {
        $nome = $_GET['nome'];
        if (trim($nome) != '') {
            if (strlen($nome) < 5) {
                $ret = 'Informe pelo menos 5 letras';
            } else {
                $rep = new cREPOSITORIO_CANDIDATO;
                $homonimos = $rep->cursorHomonimos($nome);
                $data['homonimos'] = $homonimos;
                $data['nu_candidato_tmp'] = $_GET['nu_candidato_tmp'];
                $ret = cTWIG::Render('intranet/candidato/homonimos_tmp.twig', $data);
            }
        } else {
            $ret = 'Informe um nome ';
        }
        return $ret;
    }

    public function migrarArquivos(){
        $nu_candidato_origem = $_GET['nu_candidato_origem'];
        $nu_candidato_destino = $_GET['nu_candidato_destino'];

        $rep = new cREPOSITORIO_ARQUIVO;
        $rep->migraArquivosEntreCandidatos($nu_candidato_origem, $nu_candidato_destino);

    }
}
