<?php
class cCTRL_SANKHYA extends cCTRL_MODELO{

    public function CarregaEmbproj(){
        $sql = "select nu_empresa, nu_embarcacao_projeto, dt_cadastramento, embp_fl_ativo from embarcacao_projeto";
        $res = cAMBIENTE::ConectaQuery($sql, ' ');
        while($rs = $res->fetch(PDO::FETCH_ASSOC)){
            $sql = "insert into  DW_TCSPRJ_EMBARCACAO_PROJETO(CHAVE, NO_EMBARCACAO_PROJETO, DT_CADASTRAMENTO, EMBP_FL_ATIVO)";
            $sql .= " values (:CHAVE, :NO_EMBARCACAO_PROJETO, :DT_CADASTRAMENTO, :EMBP_FL_ATIVO)";

            $stid = oci_parse($conn, $sql);

            oci_bind_by_name($stid, ':CHAVE', substr('0000'.$rs['nu_empresa'], -4).substr('000000'.$rs['nu_embarcacao_projeto'], -6));
            oci_bind_by_name($stid, ':NO_EMBARCACAO_PROJETO', utf8_encode($rs['no_embarcacao_projeto']) );
            oci_bind_by_name($stid, ':DT_CADASTRAMENTO', $rs['dt_cadastramento']);
            oci_bind_by_name($stid, ':EMBP_FL_ATIVO', $rs['embp_fl_ativo']);

            $r = oci_execute($stid);  // executes and commits

            if ($r) {
                print "One row inserted";
            }

        }
    }
}
