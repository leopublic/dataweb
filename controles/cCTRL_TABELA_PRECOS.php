<?php
class cCTRL_TABELA_PRECOS extends cCTRL_MODELO{
	public function Clonar(){
		$tabela = new ctabela_precos();
		if (intval(cHTTP::ParametroValido('tbpc_id')) > 0 ){
			$tabela->mtbpc_id = cHTTP::ParametroValido('tbpc_id');
			try{
				$tabela->Clonar();
				$msg = "Tabela de preços clonada com sucesso";
			}
			catch(Exception $e){
				$msg = "Não foi possível clonar a tabela.<br/>".str_replace("\n", '<br/>', $e->getMessage());
			}
		}
		cHTTP::$SESSION['msg'] = $msg;
		cHTTP::RedirecionePara('cCTRL_TPRC_LIST', 'Liste');
	}

	public function Exclua(){
		$tabela = new ctabela_precos();
		if (intval(cHTTP::ParametroValido('tbpc_id')) > 0 ){
			$tabela->mtbpc_id = cHTTP::ParametroValido('tbpc_id');
			try{
				$tabela->Exclua();
				$msg = "Tabela de preços excluída com sucesso";
			}
			catch(Exception $e){
				$msg = "Não foi possível excluir a tabela.<br/>".str_replace("\n", '<br/>', $e->getMessage());
			}
		}
		cHTTP::$SESSION['msg'] = $msg;
		cHTTP::RedirecionePara('cCTRL_TPRC_LIST', 'Liste');
	}
}			
