<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_TPRC_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new ctabela_precos();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_TPRC_EDIT();
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_TPRC_LIST', 'Liste');		
	}

}
