<?php 
class cCTRL_PAIS_LIST extends cCTRL_MODELO_LIST{
	public function InicializeModelo()
	{
		$this->modelo = new cPAIS_NACIONALIDADE();
	}
	
	public function InicializeTemplate()
	{
		$this->template = new cTMPL_PAIS_LIST();
	}
}
