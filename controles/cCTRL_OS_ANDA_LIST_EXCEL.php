<?php

/**
 * Controller de montagem do protocolo de BSB
 */
class cCTRL_OS_ANDA_LIST_EXCEL extends cCTRL_OS_ANDA_LIST {
	public function __construct(){
		cHTTP::$MIME = cHTTP::mmXLS;
	}

	public function InicializeTemplate() {
		parent::InicializeTemplate();
		$this->template->EscondaCampo('acao');
		foreach($this->template->mCampo as &$campo){
			$campo->mOrdenavel = false;
			$campo->mReadonly = true;
		}

	}
}
