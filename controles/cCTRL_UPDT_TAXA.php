<?php

/**
 * Metodos para atualização de atributos de ORDENS DE SERVICO
 */
class cCTRL_UPDT_TAXA {

    public function de_observacao() {
        $ret = array();
        try {
            $valor = cHTTP::$POST['valor'];
            ;
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->mde_observacao = $valor;
            $os->SalvarObservacao();

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function Pagar() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $id_taxa = cHTTP::$POST['id_taxa'];
            $nu_candidato = cHTTP::$POST['nu_candidato'];
            $taxap = new ctaxapaga();
            $taxap->mid_solicita_visto = $id_solicita_visto;
            $taxap->mid_taxa =$id_taxa;
            $taxap->mnu_candidato =$nu_candidato;
            $taxap->Criar();
            $atributos = array(
                'valor' => $taxap->mvalor
               ,'data-parametros' => cTEMPLATE_LISTAGEM::chamadaEdicaoPadrao('cCTRL_UPDT_TAXA', $taxap->id_taxapaga, 'id_taxapaga', 'valor', cCAMPO::cpVALOR, cSESSAO::$mcd_usuario)
            );
            $ret['campos']['valor'] = $atributos;
            $atributos = array(
                'valor' => $taxap->mdata_taxa
               ,'data-parametros' => cTEMPLATE_LISTAGEM::chamadaEdicaoPadrao('cCTRL_UPDT_TAXA', $taxap->id_taxapaga, 'id_taxapaga', 'data_taxa', cCAMPO::cpDATA, cSESSAO::$mcd_usuario)
            );
            $ret['campos']['data_taxa'] = $atributos;

            $atributos = array(
               'data-parametros' => cTEMPLATE_LISTAGEM::chamadaEdicaoPadrao('cCTRL_UPDT_TAXA', $taxap->id_taxapaga, 'id_taxapaga', 'observacao', cCAMPO::cpMEMO, cSESSAO::$mcd_usuario)
            );
            $ret['campos']['observacao'] = $atributos;

            $taxa = new ctaxa();
            $taxa->mid_taxa = $id_taxa;
            $taxa->RecuperePeloId();
            $links = $taxa->mdescricao;
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        $ret['novoConteudo'] = $links;
        return json_encode($ret);
    }

    public function excluir() {
        $ret = array();
        try {
            $id_taxapaga = cHTTP::$POST['id_taxapaga'];
            $taxap = new ctaxapaga();
            $taxap->mid_taxapaga = $id_taxapaga;
            $taxap->Excluir();

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }



    public function AtualizeCampoPadrao() {
        $ret = array();
        try {
            $chave = cHTTP::$POST['chave'];
            if ($chave == ''){
                throw new cERRO_CONSISTENCIA('Crie a taxa primeiro para poder alterá-la depois.');
            }
            $valor = cHTTP::$POST['valor'];
            $nome_campo = cHTTP::$POST['xnome_campo'];
            $tipo_campo = cHTTP::$POST['tipo_campo'];
            ctaxa::AtualizeCampoPadrao($chave, $nome_campo, $valor, $tipo_campo);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }


}
