<?php
/**
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_EDIT_OS_CPF extends cCTRL_PROCESSO_EDIT_OS{
	public function InicializeModelo() {
		$this->modelo = new cprocesso();
	}

	public function InicializeTemplate() {
		$this->template = new cTMPL_PROCESSO_EDIT_OS_CPF();
		$this->template->set_estiloTituloInterno();
	}

	public function CarregueTemplatePeloModelo(cTEMPLATE_EDICAO &$ptmpl, cprocesso &$pmodelo) {
		// Recupera a chave
		$os = new cORDEMSERVICO();
		$os->Recuperar($this->template->getValorCampo('id_solicita_visto'));
		$ptmpl->CarregueDoOBJETO($os);
		$cand = new cCANDIDATO();
		$os->RecuperarCandidatos();
		$cand->mNU_CANDIDATO = $os->mCandidatos;
		$cand->Recuperar();
		$this->template->setValorCampo('NOME_COMPLETO', $cand->mNOME_COMPLETO);
		$this->template->setValorCampo('NU_CPF', $cand->mNU_CPF);
		//
	}

	public function ProcesseSalvar() {
		try{
			$os = new cORDEMSERVICO();
			$os->mID_SOLICITA_VISTO = $this->template->getValorCampo('id_solicita_visto');
			$os->Recuperar();
			$this->template->AtribuaAoOBJETO($os);
			if(!$os->get_fechada()){
				$os->SalvarObservacao();
				$cand = new cCANDIDATO();
				$os->RecuperarCandidatos();
				$cand->mNU_CANDIDATO = $os->mCandidatos;
				$cand->mNU_CPF = $this->template->getValorCampo('NU_CPF');
				$cand->AtualizeCPF();
			}
			if($os->mstco_id == 1){
				$os->AtualizeInfoCobranca();
			}
			switch($this->template->mAcaoPostada){
				case "SALVAR_E_FECHAR":
					$os->Fechar();
					break;
				case "SALVAR_E_LIBERAR":
					$os->LibereParaCobranca(cSESSAO::$mcd_usuario);
					break;
			}
			cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
			$this->RedirecioneEmSucesso();
		}
		catch (cERRO_CONSISTENCIA $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch (Exception $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$this->CarregueTemplatePeloModelo($this->template, $this->modelo);
	}
}
