<?php

class cCTRL_CANDIDATO_DADOS_PESSOAIS_EDIT extends cCTRL_MODELO_EDIT {

    public function InicializeModelo() {
        $this->modelo = new cCANDIDATO();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_CANDIDATO_DADOS_PESSOAIS_EDIT();
    }

    public function CarregueTemplatePeloModelo(cTEMPLATE_EDICAO &$ptmpl, &$pmodelo) {
        if (intval($ptmpl->getValorCampo('NU_CANDIDATO')) > 0) {
            $cursor = $pmodelo->CursorIdentificacaoCandidato($ptmpl->getValorCampo('NU_CANDIDATO'));
            $rs = $cursor->fetch(PDO::FETCH_ASSOC);
            $ptmpl->CarregueDoRecordset($rs);
            $ptmpl->setValorCampo('DT_NASCIMENTO', cBANCO::DataFmt($rs['DT_NASCIMENTO']));
            $ptmpl->setValorCampo('DT_EMISSAO_PASSAPORTE', cBANCO::DataFmt($rs['DT_EMISSAO_PASSAPORTE']));
            $ptmpl->setValorCampo('DT_VALIDADE_PASSAPORTE', cBANCO::DataFmt($rs['DT_VALIDADE_PASSAPORTE']));
            $ptmpl->setValorCampo('DT_CADASTRAMENTO', cBANCO::DataFmt($rs['DT_CADASTRAMENTO']));
            $ptmpl->setValorCampo('DT_ULT_ALTERACAO', cBANCO::DataFmt($rs['DT_ULT_ALTERACAO']));
            $ptmpl->setValorCampo('empt_tx_descricao', $rs['empt_id']);
        }
    }

    public function ProcesseSalvar() {
        $this->template->AtribuaAoOBJETO($this->modelo);

        if ($this->template->mAcaoPostada == 'SALVAR'){
            if (cHTTP::$POST['CMP_empt_id'] == '' && cHTTP::$POST['CMP_empt_tx_descricao_novo'] != '') {
                $empt = new cempresa_terceirizada();
                $empt->set_empt_tx_descricao(cHTTP::$POST['CMP_empt_tx_descricao_novo']);
                $empt->Salvar();
                cHTTP::$POST['CMP_empt_id'] = $empt->get_empt_id();
            }

            $this->modelo->mempt_id = cHTTP::$POST['CMP_empt_id'];
            $this->template->setValorCampo('empt_tx_descricao', $this->modelo->mempt_id);

            try {
                $validador = new cvalidador_candidato;
                if ($validador->identificacaoOk($this->modelo)){
                    $this->modelo->Atualizar();
                    $this->modelo->VerifiqueCadastroCompleto();
                    $this->modelo->RecuperePeloId();
                    $this->CarregueTemplatePeloModelo($this->template, $this->modelo);
                    cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
                } else {
                    cNOTIFICACAO::singleton('Não foi possível salvar as alterações pois:<br/>'.$validador->msg, cNOTIFICACAO::TM_ERROR);
                }
            } catch (cERRO_CONSISTENCIA $e) {
                cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
            } catch (Exception $e) {
                cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
            }
        } else {
            if ($this->template->mAcaoPostada == 'ENVIAR'){
                
            }
        }
    }

}
