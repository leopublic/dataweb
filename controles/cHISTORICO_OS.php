<?php
class cHISTORICO_OS{

	public function Cursor($pid_solicita_visto){
		$sql = "select
                    date_format(hios_dt_evento, '%d/%m/%y %H:%i:%s') hios_dt_evento_fmt
                    , hios_tx_observacoes
                    , coalesce(usua_tx_apelido, nome) nome
                    , ss.no_status_sol_res
				from historico_os h
				left join usuarios u on u.cd_usuario = h.cd_usuario
				left join status_solicitacao ss on ss.id_status_sol = h.id_status_sol
				where id_solicita_visto = ".$pid_solicita_visto."
				order by hios_dt_evento desc";
		$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		return $res;
	}
    public static function registre(cORDEMSERVICO $os, $msg, $cd_usuario = '', $nu_candidato = 'null'){
        if ($cd_usuario ==''){
            $cd_usuario = cSESSAO::$mcd_usuario;
        }
        $sql = "INSERT INTO historico_os(
                id_solicita_visto,
                id_status_sol,
                hios_dt_evento,
                cd_usuario,
                hios_tx_observacoes,
                nu_candidato
                ) values (
                ".$os->mID_SOLICITA_VISTO
                . ", ".$os->mID_STATUS_SOL
                . ", now()"
                . ", ".$cd_usuario
                . ", ".cBANCO::StringOk($msg)
                . ", ".$nu_candidato
                . ")";
        cAMBIENTE::ExecuteQuery($sql);
    }
}
