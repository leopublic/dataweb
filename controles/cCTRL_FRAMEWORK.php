<?php

class cCTRL_FRAMEWORK {

    public static function GeraTemplateDeEdicao() {
        $msg = '';
        $retorno = '';
        $tmpl = new cTEMPLATE_FRAMEWORK_CONVERTE_EDICAO();
        $tmpl->CarregueDoHTTP();
        if ($tmpl->mFoiPostado) {
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        cFRAMEWORK::ConverteEdicaoEmTemplate($tmpl->mCampo['id_edicao']->mValor, $tmpl->mCampo['nome_template']->mValor);
                        $msg = "Template gerado com sucesso!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {
            $msg = "Tela nova";
        }
        cHTTP::$SESSION['msg'] = $msg;
        cLAYOUT::AdicionarScriptExibicaoMensagens();
        $conteudo = cINTERFACE::RenderizeTemplate($tmpl);
        return $conteudo;
    }

    public static function TestaTemplate() {
        $nomeTemplate = cHTTP::$GET['template'];
        eval('$tmpl = new ' . $nomeTemplate . '();');
        $tmpl->CarregueDoHTTP();
        cLAYOUT::AdicionarScriptExibicaoMensagens();
        $conteudo = cINTERFACE::RenderizeTemplate($tmpl);
        return $conteudo;
    }

    public static function TestaProcesso() {
        $ctrlProcesso = cHTTP::$GET['ctrlProcesso'];
        eval('$tmpl = new ' . $nomeTemplate . '();');
        $tmpl->CarregueDoHTTP();
        cLAYOUT::AdicionarScriptExibicaoMensagens();
        $conteudo = cINTERFACE::RenderizeTemplate($tmpl);
        return $conteudo;
    }

    public static function GeraClasse() {
        $msg = '';
        $tmpl = new cTEMPLATE_FRAMEWORK_GERAR_CLASSE();
        $tmpl->CarregueDoHTTP();
        $ret = '';
        $ret .= cINTERFACE::RenderizeTitulo($tmpl, false, cINTERFACE::titPAGINA);
        $ret .= '<div class="conteudoInterno">';
        if ($tmpl->mFoiPostado) {
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        //$tmpl->mCampo['resultado']->mValor = cFRAMEWORK::GerarClasse($tmpl->mCampo['tabela']->mValor);
                        print cFRAMEWORK::GerarClasse($tmpl->mCampo['tabela']->mValor);
                        $msg = "Classe gerada com sucesso!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {

        }
        cHTTP::$SESSION['msg'] = $msg;
        cLAYOUT::AdicionarScriptExibicaoMensagens();
        $ret .= cINTERFACE::RenderizeTemplate($tmpl);
        $ret .= '</form></div></div>';
        return $ret;
    }

    public static function ValoresCombo() {
        $nomeTemplate = cHTTP::$POST['nomeTemplate'];
        $comboFilho = cHTTP::$POST['comboFilho'];
        $comboPai = cHTTP::$POST['comboPai'];
        $valorPai = cHTTP::$POST['valorPai'];
        $valorFilho = cHTTP::$POST['valorFilho'];
        $tmpl = new $nomeTemplate();
        $tmpl->mCampo[$comboPai]->mValor = $valorPai;
        $tmpl->mCampo[$comboFilho]->mValor = $valorFilho;
        $tmpl->mCampo[$comboFilho]->CarregarValores();
        $valores = json_encode($tmpl->mCampo[$comboFilho]->valores());
        //var_dump($valores);
        return $valores;
    }

    public static function ValoresFiltro() {
        $nomeTemplate = cHTTP::$POST['nomeTemplate'];
        $comboFilho = cHTTP::$POST['comboFilho'];
        $comboPai = cHTTP::$POST['comboPai'];
        $valorPai = cHTTP::$POST['valorPai'];
        $tmpl = new $nomeTemplate();
        $tmpl->mFiltro[$comboPai]->mValor = $valorPai;
        $tmpl->mFiltro[$comboFilho]->CarregarValores();
        $x = $tmpl->mFiltro[$comboFilho]->valores();
        $valores = json_encode($x);
        return $valores;
    }

    public static function GetValoresFiltro() {
        $nomeTemplate = $_GET['nomeTemplate'];
        $comboFilho = $_GET['comboFilho'];
        $comboPai = $_GET['comboPai'];
        $valorPai = $_GET['valorPai'];
        $limite =  $_GET['limite'];
        $tmpl = new $nomeTemplate();
        $tmpl->mFiltro[$comboPai]->mValor = $valorPai;
        $tmpl->mFiltro[$comboFilho]->CarregarValores(false);
        $tmpl->mFiltro[$comboFilho]->mExtra .= " and nu_empresa < ".$limite;
        $x = $tmpl->mFiltro[$comboFilho]->valores();
        $valores = json_encode($x);
        return $valores;
    }
}
