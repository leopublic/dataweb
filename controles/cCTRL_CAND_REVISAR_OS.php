<?php
class cCTRL_CAND_REVISAR_OS extends cCTRL_MODELO_EDIT {

    public function InicializeModelo() {
        $this->modelo = new cCANDIDATO();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_CAND_REVISAR_OS();
    }

    public function CarregueTemplatePeloModelo(cTEMPLATE_EDICAO &$ptmpl, &$pmodelo) {
        if (intval($ptmpl->getValorCampo('NU_CANDIDATO')) > 0) {
            $cursor = $pmodelo->CursorIdentificacaoCandidato($ptmpl->getValorCampo('NU_CANDIDATO'));
            $rs = $cursor->fetch(PDO::FETCH_ASSOC);
            $ptmpl->CarregueDoRecordset($rs);
            $ptmpl->setValorCampo('DT_NASCIMENTO', cBANCO::DataFmt($rs['DT_NASCIMENTO']));
            $ptmpl->setValorCampo('DT_EMISSAO_PASSAPORTE', cBANCO::DataFmt($rs['DT_EMISSAO_PASSAPORTE']));
            $ptmpl->setValorCampo('DT_VALIDADE_PASSAPORTE', cBANCO::DataFmt($rs['DT_VALIDADE_PASSAPORTE']));
            $ptmpl->setValorCampo('empt_tx_descricao', $rs['empt_id']);
            $autc = new cAUTORIZACAO_CANDIDATO();
            $autc->Recuperar($this->template->getValorCampo('id_solicita_visto'), $this->template->getValorCampo('NU_CANDIDATO'));
            if ($autc->mCO_FUNCAO_CANDIDATO != '') {
                $funcao = new cFUNCAO_CARGO();
                $funcao->mCO_FUNCAO = $autc->mCO_FUNCAO_CANDIDATO;
                $funcao->RecuperePeloId();
                if ($autc->mTE_DESCRICAO_ATIVIDADES == '') {
                    $autc->mTE_DESCRICAO_ATIVIDADES = $funcao->mTX_DESCRICAO_ATIVIDADES;
                }
            }
            $this->template->CarregueDoOBJETO($autc);
        }
        $ptmpl->set_titulo('Revisar cadastro de candidato para OS');
        $ptmpl->enveloparEmConteudoInterno = true;
    }

    public function ProcessePost() {
        $acao = $this->template->get_acaoPostada();
        try {
            if ($acao == 'REVISAR') {
                $this->template->AtribuaAoOBJETO($this->modelo);
//		$this->modelo->mNU_CANDIDATO = $this->template->getValorCampo('NU_CANDIDATO');
                $this->modelo->RevisarCadastroNaOS($this->template->getValorCampo('id_solicita_visto'), cSESSAO::$mcd_usuario);
                $autc = new cAUTORIZACAO_CANDIDATO();
                $this->template->AtribuaAoOBJETO($autc);
                if ($autc->mCO_FUNCAO_CANDIDATO != '') {
                    $funcao = new cFUNCAO_CARGO();
                    $funcao->mCO_FUNCAO = $autc->mCO_FUNCAO_CANDIDATO;
                    $funcao->RecuperePeloId();
                    if ($autc->mTE_DESCRICAO_ATIVIDADES == '') {
                        $autc->mTE_DESCRICAO_ATIVIDADES = $funcao->mTX_DESCRICAO_ATIVIDADES;
                        $this->template->setValorCampo('TE_DESCRICAO_ATIVIDADES', $funcao->mTX_DESCRICAO_ATIVIDADES);
                    }
                }
                $autc->AtualizarCadastroOs(cSESSAO::$mcd_usuario);
                $os = new cORDEMSERVICO();
                $os->Recuperar($this->template->getValorCampo('id_solicita_visto'));
                $processo = cprocesso::FabricaProcessoDaOs($os);
                if (get_class($processo) == 'cprocesso_mte'){
                    $processo->RecupereSePelaOs($this->template->getValorCampo('id_solicita_visto'), $this->template->getValorCampo('NU_CANDIDATO'));
                    $processo->mcd_funcao = $this->template->getValorCampo('CO_FUNCAO_CANDIDATO');
                    $processo->mcd_reparticao = $this->template->getValorCampo('CO_REPARTICAO_CONSULAR');
                    $processo->Atualize_Revisao();
                }
                unset($this->template->mAcoes['REVISAR']);
            }
            cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
    }
}
