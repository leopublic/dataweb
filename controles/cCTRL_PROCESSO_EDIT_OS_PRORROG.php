<?php
/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_EDIT_OS_PRORROG extends cCTRL_PROCESSO_EDIT_OS{

	public function InicializeModelo() {
		$this->modelo = new cprocesso_prorrog();
	}
	public function InicializeTemplate() {
		$this->template = new cTMPL_PROCESSO_EDIT_OS_PRORROG();
		$this->template->set_estiloTituloInterno();
	}

    public function ProcesseSalvar() {
        $processo_antes = new cprocesso_prorrog;
        $processo_antes->mcodigo = $this->template->getValorCampo('codigo');
        $processo_antes->RecupereSe();
        $id_status_sol = $this->template->getValorCampo('id_status_sol');
        parent::ProcesseSalvar();
//        if($this->template->getValorCampo('id_status_sol') > 0){
//            $this->os->AtualizarStatus($this->template->getValorCampo('id_status_sol'), cSESSAO::$mcd_usuario);
//        }
        if ($processo_antes->mnu_protocolo != $this->modelo->mnu_protocolo){  // Verifica se preencheu o número do processo agora
            if ($id_status_sol <=4 && $this->modelo->mnu_protocolo != ''){
                $os = new cORDEMSERVICO();
                $os->mID_SOLICITA_VISTO = $this->modelo->mid_solicita_visto;
                $os->AtualizarStatus(5, cSESSAO::$mcd_usuario);
                $this->template->setValorCampo('id_status_sol', 5);
            }            
        }
    }

    public function ProcessaFechamento() {

    }
    public function ProcessaSalvarEFechar() {

    }
}
