<?php

class cCTRL_CONVIDADO extends cCTRL_MODELO {

    public static $repo;

    public function ativo(){
        if ($_SESSION['conv_id'] > 0){
            return true;
        } else {
            cNOTIFICACAO::singleton('Your access expired.', cNOTIFICACAO::TM_SUCCESS, "Attention");
            $link = '/index.php';
            cHTTP::RedirecionaLinkInterno($link);
            exit();
        }
    }
    
    public function semacesso(){
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        return cTWIG::Render('convidado/sem_acesso.twig', $data);
    }
    
    public function repo() {
        if (!isset(self::$repo)) {
            self::$repo = new cREPOSITORIO_CONVIDADO();
        }
        return self::$repo;
    }
    
}
