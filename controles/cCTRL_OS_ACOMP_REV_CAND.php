<?php
/**
 * @author leonardo
 */
class cCTRL_OS_ACOMP_REV_CAND extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cCANDIDATO();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_OS_ACOMP_REV_CAND();
	}	

	// public function CarregueTemplatePeloModelo(cTEMPLATE_EDICAO &$ptmpl, &$pmodelo){
	// 	// Recupera a chave
	// 	$ptmpl->AtribuaAoOBJETO($pmodelo);
	// 	$pmodelo->Recuperar($ptmpl->getValorCampo('NU_CANDIDATO'), $ptmpl->getValorCampo('id_solicita_visto'));
	// 	// Carrega os campos
	// 	$ptmpl->CarregueDoOBJETO($pmodelo);
	// 	//
	// }
	
	public function ProcessePost() {
		switch($this->template->get_acaoPostada()){
			case 'SALVAR':
			case 'SALVAR_E_FECHAR':
			case 'SALVAR_E_LIBERAR':
				$this->ProcesseSalvar();
				break;
		}
	}
	
	public function ProcesseSalvar() {
		$this->template->AtribuaAoOBJETO($this->modelo);
		try{
			$this->modelo->Salve_ProcessoEdit();
			$os = new cORDEMSERVICO();
			$os->mID_SOLICITA_VISTO = $this->template->getValorCampo('id_solicita_visto');
			$os->Recuperar();
			if (intval($os->mstco_id) < 2){
				$os->mtppr_id = $this->template->getValorCampo('tppr_id');
				$os->msoli_tx_obs_cobranca = $this->template->getValorCampo('soli_tx_obs_cobranca');
				if ($this->template->get_acaoPostada() == "SALVAR_E_LIBERAR"){
					$os->LibereParaCobranca(cSESSAO::$mcd_usuario);
				}
				else{
					$os->AtualizeInfoCobranca();
				}
			}
			if ($this->template->get_acaoPostada() == "SALVAR_E_FECHAR"){
				$os->Fechar();
			}
			cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
			$this->RedirecioneEmSucesso();
		}
		catch (cERRO_CONSISTENCIA $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch (Exception $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$this->CarregueTemplatePeloModelo($this->template, $this->modelo);
	}
	
	public function RedirecioneEmSucesso() {
		
	}
}
