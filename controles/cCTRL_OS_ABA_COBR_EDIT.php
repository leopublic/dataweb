<?php

/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_OS_ABA_COBR_EDIT extends cCTRL_MODELO_EDIT {

    public function InicializeModelo() {
        $this->modelo = new cORDEMSERVICO();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_OS_ABA_COBR_EDIT();
    }

    public function CarregueTemplatePeloModelo(&$ptmpl, cORDEMSERVICO &$pmodelo) {
        // Recupera a chave
        $this->template->AtribuaAoOBJETO($this->modelo);
        $this->modelo->mID_SOLICITA_VISTO = $this->template->getValorCampo('id_solicita_visto');
        $this->modelo->RecuperarSolicitaVistoCobranca();
        // Carrega os campos
        $this->template->os = $this->modelo;
        $ptmpl->CarregueDoOBJETO($this->modelo);
        //
    }

    public function ProcessePost() {
        switch ($this->template->get_acaoPostada()) {
            case 'SALVAR':
            case 'SALVAR_E_FECHAR':
            case 'SALVAR_E_LIBERAR':
                $this->ProcesseSalvar();
                break;
        }
    }

    public function ProcesseSalvar() {
        $this->template->AtribuaAoOBJETO($this->modelo);
        try {
            $this->modelo->mID_SOLICITA_VISTO = $this->template->getValorCampo('id_solicita_visto');
            $this->modelo->SalvarCobranca($this->template->getValorCampo('justificativa'));

            if ($this->template->get_acaoPostada() == 'SALVAR_E_LIBERAR') {
                $this->modelo->LibereParaCobranca();
            }

            cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);

            $this->RedirecioneEmSucesso();
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $this->CarregueTemplatePeloModelo($this->template, $this->modelo);
    }

    public function RedirecioneEmSucesso() {
        
    }

}
