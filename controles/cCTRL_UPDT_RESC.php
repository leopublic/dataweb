<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class cCTRL_UPDT_RESC{

	public function AtualizeCampoPadrao()
	{
		$valor		= cHTTP::$POST['valor'];
		$valor_orig	= HTTP::$POST['valor_orig'];
		$chave		= cHTTP::$POST['chave'];
		$nome_campo = cHTTP::$POST['xnome_campo'];
		$tipo_campo	= cHTTP::$POST['tipo_campo'];
		$ret = array();
		try{
			cresumo_cobranca::AtualizeCampoPadrao($chave, $nome_campo, $valor, $tipo_campo);
			$ret['novoConteudo'] = '';
			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			$ret['novoConteudo'] = $valor_orig;
			cNOTIFICACAO::singleton($e->mMsg, cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			$ret['novoConteudo'] = $valor_orig;
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}
}
