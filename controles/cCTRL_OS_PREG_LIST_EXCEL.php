<?php 
class cCTRL_OS_PREG_LIST_EXCEL extends cCTRL_OS_PREG_LIST{
	public function __construct()
	{
		cHTTP::$MIME = cHTTP::mmXLS;		
	}
	
	public function InicializeTemplate(){
		parent::InicializeTemplate();
		$this->template->MostreCampo('comprovante');
		$this->template->MostreCampo('ref');
		$this->template->MostreCampo('valor');
		$this->template->ProtejaCampo('comprovante');
		$this->template->ProtejaCampo('ref');
		$this->template->ProtejaCampo('valor');
	}

}
