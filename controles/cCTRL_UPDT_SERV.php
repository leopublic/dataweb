<?php
/**
 * Metodos para atualização de atributos de ORDENS DE SERVICO
 */
class cCTRL_UPDT_SERV{
	public function no_servico()
	{
		$ret = array();
		try{
			$valor		= cHTTP::$POST['valor'];
			$nu_servico = cHTTP::$POST['nu_servico'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cSERVICO::Atualize_no_servico($nu_servico, $valor, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}
	public function no_servico_resumido()
	{
		$ret = array();
		try{
			$valor		= cHTTP::$POST['valor'];
			$nu_servico = cHTTP::$POST['nu_servico'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cSERVICO::Atualize_no_servico_resumido($nu_servico, $valor, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function nu_tipo_servico()
	{
		$ret = array();
		try{
			$co_tipo_servico = cHTTP::$POST['valor'];
			$nu_servico = cHTTP::$POST['nu_servico'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cSERVICO::Atualize_nu_tipo_servico($nu_servico, $co_tipo_servico, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function serv_tx_codigo_sg()
	{
		$ret = array();
		try{
			$valor = cHTTP::$POST['valor'];
			$nu_servico = cHTTP::$POST['nu_servico'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cSERVICO::Atualize_serv_tx_codigo_sg($nu_servico, $valor, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function serv_fl_ativo()
	{
		$ret = array();
		try{
			$valor = cHTTP::$POST['valor'];
			$nu_servico = cHTTP::$POST['nu_servico'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cSERVICO::Atualize_serv_fl_ativo($nu_servico, $valor, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

	public function co_servico()
	{
		$ret = array();
		try{
			$valor = cHTTP::$POST['valor'];
			$nu_servico = cHTTP::$POST['nu_servico'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			cSERVICO::Atualize_co_servico($nu_servico, $valor, $cd_usuario);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}


	public function AtualizeCampoPadrao()
	{
		$ret = array();
		try{
			$valor		= cHTTP::$POST['valor'];
			$chave		= cHTTP::$POST['chave'];
			$nome_campo = cHTTP::$POST['xnome_campo'];
			$tipo_campo	= cHTTP::$POST['tipo_campo'];
			CSERVICO::AtualizeCampoPadrao($chave, $nome_campo, $valor, $tipo_campo);

			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}

}
