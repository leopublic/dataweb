<?php
class cINTERFACE_AMBIENTE{
	public static function ControleLinguagem(){
		$msg = '' ;
		$retorno= '' ;
		$tmpl = new cTEMPLATE_ALTERAR_LINGUAGEM();
		$tmpl->CarregueDoHTTP();
		//var_dump($tmpl->mCampo);
		if ($tmpl->mFoiPostado){
			// Limpa o combo por default
			switch ($tmpl->mAcaoPostada){
				case 'ALTERARLANG_PT_BR':
					try{
						cHTTP::setLang("pt_br");
					}
					catch(Exception $e){
						$msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
					}
					break;
				case 'ALTERARLANG_EN_US':
					try{
						cHTTP::setLang("en_us");
					}
					catch(Exception $e){
						$msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
					}
					break;
				default:
					$msg = "Operação não encontrada (".$tmpl->mAcaoPostada.")";
					break;
			}
		}
		if ($msg != '' ){
			cHTTP::$SESSION['msg'] = $msg;
		}
		//$conteudo = cINTERFACE::RenderizeAcao($tmpl->mAcoes['ALTERARLANG_PT_BR']);
		//$conteudo .= cINTERFACE::RenderizeAcao($tmpl->mAcoes['ALTERARLANG_EN_US']);
		$conteudo = cINTERFACE::RenderizeTemplateAcoes($tmpl);
		return $conteudo;
	}

}
