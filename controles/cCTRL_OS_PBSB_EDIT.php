<?php
/**
 * Controller de montagem do protocolo de BSB
 */
class cCTRL_OS_PBSB_EDIT extends cCTRL_MODELO_LIST{
	public function __construct(){
	}
	public function InicializeModelo(){
		$this->modelo = new cORDEMSERVICO();
	}

	public function InicializeTemplate(){
		cHTTP::$comCabecalhoFixo = true;
		$this->template = new cTMPL_OS_PBSB_EDIT();
	}

	public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl){
		if(!cHTTP::HouvePost()){
			if($this->template->getValorFiltro('dt_envio_bsb') == ''){
				$sql = "select date_format(max(dt_envio_bsb), '%d/%m/%Y') dt_envio_bsb from (select dt_envio_bsb from processo_prorrog union select dt_envio_bsb from processo_mte) a where dt_envio_bsb <= '".date('Y-m-d')."'";
				$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
				$rw = $res->fetch(PDO::FETCH_ASSOC);
				$this->template->mFiltro['dt_envio_bsb']->mValor = $rw['dt_envio_bsb'];
			}
		}

		if($this->template->getValorFiltro('dt_solicitacao_ini') != ''){
			$this->template->mFiltro['dt_solicitacao_ini']->mWhere = ' dt_solicitacao >= '.cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_ini'));
		}
		if($this->template->getValorFiltro('dt_solicitacao_fim') != ''){
			$this->template->mFiltro['dt_solicitacao_fim']->mWhere = ' dt_solicitacao <= '.cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_fim'));
		}
		if($this->template->getValorFiltro('dt_envio_bsb') != ''){
			$this->template->mFiltro['dt_envio_bsb']->mWhere = ' p.dt_envio_bsb = '.cBANCO::DataOk($this->template->getValorFiltro('dt_envio_bsb'));
		}

		$cursor = $this->modelo->CursorProtocoloDoDia($this->template->mFiltro, $this->template->get_ordenacao(), 1, 0);
		return $cursor;
	}
}
