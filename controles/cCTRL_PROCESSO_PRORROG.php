<?php

/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_PRORROG extends cCTRL_PROCESSO{
	public function FormEdicaoOS(cORDEMSERVICO $pOS) {
		$tela = $this->FormEdicaoPadrao($pOS, 'processo_prorrog_edit');
		return $this->RenderizacaoPadraoEdicao($tela);
	}
	
	public function log(){
		$codigo = $_GET['codigo'];
		$processo = new cprocesso_prorrog();
		$processo->mcodigo = $codigo;
		$processo->RecupereSe();
		if ($processo->mid_solicita_visto != ''){
			$os = new cORDEMSERVICO();
			$os->mID_SOLICITA_VISTO = $processo->mid_solicita_visto;
			$os->Recuperar();
		} else {
			$os = null;
		}
		$candidato = new cCANDIDATO();
		$candidato->Recuperar($processo->mcd_candidato);
		$cursor = cprocesso_prorrog::cursorLog($codigo);
		$regs = $cursor->fetchAll();
		$menu = cLAYOUT::Menu();
		$data = array(
				'regs' => $regs,
				'menu' => $menu,
				'processo' => $processo,
				'os' => $os,
				'candidato' => $candidato
		);
		return cTWIG::Render('intranet/processo/prorrog_log.html', $data);
	}

	public function get_form_candidato(){
		$codigo = $_GET['codigo'];
		$proc = new cprocesso_prorrog;
		$proc->RecupereSe($codigo);
		$os = new cORDEMSERVICO;
		if ($proc->mid_solicita_visto > 0){
			$os->Recuperar($proc->mid_solicita_visto);
		}

		$status_solicitacao = cSTATUS_SOLICITACAO::combo();
		$status_conclusao = cSTATUS_CONCLUSAO::combo();
		$status_cobranca_os = new cstatus_cobranca_os;
		$status_cobranca_os->mstco_id = $os->mstco_id;
		$status_cobranca_os->RecuperePeloId();
		$menu = cLAYOUT::Menu();
		$data = array(
				'proc' => $proc,
				'os' => $os,
				'status_conclusao' => $status_conclusao,
				'status_solicitacao' => $status_solicitacao,
				'status_cobranca_os' => $status_cobranca_os
		);
		return cTWIG::Render('intranet/processo_prorrog/edit_candidato.twig', $data);

	}



}
