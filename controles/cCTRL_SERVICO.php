<?php

class cCTRL_SERVICO extends cCTRL_MODELO {

    public function Edite() {
        $tmpl = new cTMPL_SERVICO_EDITE();
        if (cHTTP::ParametroValido('NU_SERVICO') == '0' || cHTTP::ParametroValido('NU_SERVICO') == '') {
            $tmpl->mTitulo = 'Criar novo serviço';
        } else {
            $tmpl->mTitulo = 'Alterar serviço';
        }
        return parent::Edite($tmpl, new cSERVICO(), "cCTRL_SERV_LIST", "Liste");
    }

    public function NovoPacote() {
        $serv = cpacote_servico::CrieNovo();
        $serv->Salvar();
        cNOTIFICACAO::singleton('Um novo pacote (inativo) foi criado com o nome "novo pacote".', cNOTIFICACAO::TM_SUCCESS);
        cHTTP::RedirecionePara('cCTRL_PACT_LIST', 'Liste');
    }

    public function Exclua() {
        try {
            $servico = cSERVICO::Instancie(cHTTP::ParametroValido('NU_SERVICO'));
            $servico->Exclua();
            cNOTIFICACAO::singleton('Serviço excluído com sucesso', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        cHTTP::RedirecionePara('cCTRL_SERV_LIST', 'Liste');
    }

    public function servicosDaEmpresa(){
        $nu_empresa = $_GET['nu_empresa'];
        $nu_servico = intval($_GET['nu_servico']);

        $sql = "select s.nu_servico chave, s.no_servico_resumido descricao, s.id_tipo_acompanhamento, ta.fl_multi_candidatos"
                . ", (select count(*) from pacote_servico sp, tabela_precos tp, preco p, empresa e
                        where tp.tbpc_id = e.tbpc_id
                        and p.tbpc_id = tp.tbpc_id
                        and sp.nu_servico_incluido = p.NU_SERVICO
                        and e.NU_EMPRESA = ".$nu_empresa."
                         and p.NU_SERVICO = s.nu_servico
                         ) pertence_pacote"
                . " from empresa e"
                . " join preco p on p.tbpc_id = e.tbpc_id "
                . " join servico s on s.nu_servico = p.nu_servico"
                . " left join tipo_acompanhamento ta on ta.id_tipo_acompanhamento = s.id_tipo_acompanhamento"
                . " where e.nu_empresa = ".$nu_empresa;
        if ($nu_servico != ''){
            $sql .= " and (s.serv_fl_ativo = 1 or s.nu_servico = ".cBANCO::chaveOk($nu_servico).")";
        }
        $sql .= " order by s.no_servico_resumido";
        $res  = cAMBIENTE::ConectaQuery($sql, __CLASS__.'.'.__FUNCTION__);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);

        $servicos= array();
        foreach($rs as $reg){
            $servicos[] = array(
                    'nu_servico' => $reg['chave'],
                    'no_servico_resumido' => $reg['descricao'],
                    'pertence_pacote' => $reg['pertence_pacote'],
                    'fl_multi_candidatos' => $reg['fl_multi_candidatos'],
                    'id_tipo_acompanhamento' => $reg['id_tipo_acompanhamento'],
            );
        }

        if (count($rs) == 0){
            $rs = array(array('chave' => '', 'descricao' => '(nenhum serviço disponível)'));
        } else {
            $rs = array(array('chave' => '', 'descricao' => '(selecione...)')) + $rs;
        }
        $data = array (
                    'regs' => $rs
                ,   'nome' => 'nu_servico'
                ,   'valor' => $nu_servico
                );
        $combo = cTWIG::Render('intranet/campos/campo_combo.html', $data);


        $x = utf8_encode($combo);

        $ret = array('servicos' => $servicos, 'combo' => $x );

        return json_encode($ret);
    }

}
