<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_TSRV_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cTIPO_SERVICO();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_TSRV_EDIT();
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_TSRV_LIST', 'Liste');		
	}

}
