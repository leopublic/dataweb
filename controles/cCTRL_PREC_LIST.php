<?php
class cCTRL_PREC_LIST extends cCTRL_MODELO_LIST{
	public $preco;
	public function InicializeModelo()
	{
		$this->modelo = new cpreco();
	}

	public function InicializeTemplate()
	{
		$this->template = new cTMPL_PREC_LIST();
	}

	public function ProcessePost() {
		if($this->template->get_acaoPostada() == 'ADICIONAR'){
			try{
				$parametros = cHTTP::$POST['CMP___parametros'];
				$parmx = explode('&', $parametros);
				$parm = array();
				foreach($parmx as $x){
					if($x != ''){
						$tokens = explode('=', $x);
						$parm[$tokens[0]] = $tokens[1];
					}
				}
				$this->servico = ctabela_precos::AdicioneServico($parm['tbpc_id'], $this->template->getValorCampo('NU_SERVICO_NOVO'));
				cNOTIFICACAO::singleton('O serviço foi adicionado à tabela de preços".', cNOTIFICACAO::TM_SUCCESS);
			}
			catch(Exception $e){
				cNOTIFICACAO::singleton('Não foi possível adicionar o serviço:'.$e->getMessage()."<br/>".cAMBIENTE::$multimo_sql, cNOTIFICACAO::TM_ERROR);
			}
		}
	}

    public function Redirecione() {
        cHTTP::RedirecionePara('cCTRL_PREC_LIST', 'Liste#');
    }
}
