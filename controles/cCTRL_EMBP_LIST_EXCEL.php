<?php 
class cCTRL_EMBP_LIST_EXCEL extends cCTRL_MODELO_LIST
{
	public function __construct() 
	{
		cHTTP::$MIME = cHTTP::mmXLS;
	}

	public function InicializeModelo()
	{
		$this->modelo = new cEMBARCACAO_PROJETO();
	}
	
	public function InicializeTemplate()
	{
		$this->template = new cTMPL_EMBP_LIST();
		foreach($this->template->mCampo as &$campo){
			$campo->mOrdenavel = false;
		}
	}

	public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl)
	{
		$modelo = $this->modelo;
		if(intval($this->template->mFiltro['NU_EMPRESA']->mValor) == 0){
			cNOTIFICACAO::singleton('Por favor, selecione uma empresa', cNOTIFICACAO::TM_INFO);
		}
		else{
			if($this->template->mFiltro['ATIVAS']->mValor == '1'){
				$this->template->mFiltro['ATIVAS']->mWhere = "( DT_PRAZO_CONTRATO is null or DT_PRAZO_CONTRATO = '0000-00-00' or DT_PRAZO_CONTRATO > now())";
			}
			else{
				$this->template->mFiltro['ATIVAS']->mWhere = " 1=1";
			}
			$cursor = $modelo->Listar($this->template->mFiltro, $this->template->get_ordenacao());			
			return $cursor;
		}
	}

}
