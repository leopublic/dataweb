<?php

class cCTRL_MODELO_TWIG_EDIT extends cCTRL_MODELO {

    protected $template_menu;
    protected $houvePost;
    protected $data;
    protected $nomeTemplate;
    protected $nomeAbaAtiva;

    public function __construct() {
        
    }

    public function set_nomeTemplate($pValor) {
        $this->nomeTemplate = $pValor;
    }

    public function get_houvePost() {
        return $this->houvePost;
    }

    public function Edite() {
        $this->InicializeModelo();
        $this->InicializeTemplate();
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $this->template->CarregueDoHTTP();
        // Carrega o usuÃ¡rio correspondente Ã  tela exibida
        if ($this->template->mFoiPostado) {
            $this->houvePost = true;
            $this->ProcessePost();
        } else {
            $this->houvePost = false;
            $this->CarregueTemplatePeloModelo();
        }
        return $this->RenderizaSaida();
    }

    public function RenderizaSaida() {
        $api = $this->ApiTemplate();
        $data = $api->get_data();
        if (!cHTTP::TemRedirecionamento()) {
            return cTWIG::Render($this->nomeTemplate, $data);
        }
    }

    public function ApiTemplate() {
        $api = new cTEMPLATE_API();
        $api->CarregaEdicaoTwig($this->template);
        if (is_object($this->template_menu)) {
            $api->CarregaAbaLateral($this->template_menu);
        }
        return $api;
    }

    public function ProcessePost() {
        $this->template->AtribuaAoOBJETO($this->modelo);
        try {
            $this->ProcesseSalvar();

            if ($this->template->get_acaoPostada() != 'SALVAR' && $this->template->get_acaoPostada() != 'SALVAR_E_FECHAR') {
                $link = $this->template_menu->mAbas[$this->template->get_acaoPostada()]->mLink . '&NU_CANDIDATO=' . $this->modelo->mNU_CANDIDATO;
                cHTTP::RedirecionaLinkInterno($link);
            } else {
                cNOTIFICACAO::singleton('OperaÃ§Ã£o realizada com sucesso!', cNOTIFICACAO::TM_ALERT);
            }
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        if (!cHTTP::TemRedirecionamento()) {
            if (!isset(cHTTP::$GET['NU_CANDIDATO'])) {
                $link = $this->template_menu->mAbas[$this->nomeAbaAtiva]->mLink . '&NU_CANDIDATO=' . $this->modelo->mNU_CANDIDATO;
                cHTTP::RedirecionaLinkInterno($link);
            }
        }
    }

    public function ProcesseSalvar() {
        throw new cERRO_CONSISTENCIA('Processamento do salvar nÃ£o implementado');
    }

    // public function GeraDataDoTemplate(){
    // 	cHTTP::setLang('en_us');
    // 	$this->data = array();
    // 	$this->data['lang'] = cHTTP::getLang();
    // 	$this->data['head_title'] = "Mundivisas::Dataweb";
    // 	$this->data['titulo'] = "";
    // 	$this->data['titulo_tela'] = "";
    // 	$this->data['titulo_painel'] = "";
    // 	$this->data['campos'] = $this->template->mCampo;
    // 	$this->data['template'] = array('nome' => $this->template->mnome, 'multipart' => $this->template->mMultipart, 'controles' => $this->template->controles);
    // 	$this->data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
    // 	$this->data['acoes'] = $this->template->mAcoes;
    // }

    public function CarregueTemplatePeloModelo() {
        
    }

    public function get_msgErroPadrao() {
        return 'NÃ£o foi possÃ­Â­vel realizar a operaÃ§Ã£o pois ';
    }

    public function RedirecioneEmSucesso() {
        cHTTP::RedirecionePara($this->get_controllerDestinoSucesso(), $this->get_metodoDestinoSucesso(), $this->get_parametrosAdicionaisListagem());
    }

    public function get_controllerDestinoSucesso() {
        return __CLASS__;
    }

    public function get_metodoDestinoSucesso() {
        return 'Liste';
    }

    public function get_parametrosAdicionaisListagem() {
        return "";
    }

}
