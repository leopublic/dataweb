<?php
/**
 * Controller padrÃ£o para os acessos do operacional
 */

class cCTRL_OPERACIONAL extends cCTRL_MODELO 
{
	public $data;
	/**
	 * Inicializa o data com os campos padrÃ£o que sÃ£o enviados na montagem das telas
	 * @return [type] [description]
	 */
	public function inicializarData(){
        $menu = cLAYOUT::soMenu();
		$this->data = array();
        $this->data['menu'] = $menu;
        $this->data['usuario'] = cSESSAO::$mnome;
        $this->data['msg'] = $_SESSION['msg'];
        $this->data['success'] = $_SESSION['success'];
        $this->data['error'] = $_SESSION['error'];
        $this->data['info'] = $_SESSION['info'];
        $this->data['warning'] = $_SESSION['warning'];
        $_SESSION['msg'] = '';
        $_SESSION['error'] = '';
        $_SESSION['success'] = '';
        $_SESSION['info'] = '';
        $_SESSION['warning'] = '';

	}

	public function adicionaEmpresas($chave = 'empresas', $comDefault = true, $valorDefault = ''){
		$empresas= cEMPRESA::combo($comDefault,$valorDefault, 'nu_empresa', 'no_razao_social', ' where coalesce(fl_ativa, 0) = 1');
		$this->data[$chave] = $empresas;
	}

	public function adicionaServicos($chave = 'servicos', $comDefault = true, $valorDefault = ''){
		$empresas= cSERVICO::combo($comDefault,$valorDefault, 'nu_empresa', 'no_razao_social', ' where coalesce(fl_ativa, 0) = 1');
		$this->data[$chave] = $empresas;
	}

	public function adicionaConteudo($chave, $conteudo){
		$this->data[$chave] = $conteudo;
	}
}
