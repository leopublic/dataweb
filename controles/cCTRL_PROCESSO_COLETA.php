<?php

/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_COLETA extends cCTRL_PROCESSO{
	public function FormEdicaoOS(cORDEMSERVICO $pOS){
		$tela = $this->FormEdicaoPadrao($pOS, 'processo_coleta_edit');
		$tela->mTitulo = 'Coleta de visto';
		return $this->RenderizacaoPadraoEdicao($tela);
	}
}
