<?php
class cCTRL_PBSB_PLAN_LIST extends cCTRL_MODELO_TWIG_LIST {

    public function __construct() {
        cHTTP::$MIME = cHTTP::mmXLS;
        $this->nomeTemplate = 'public/protocolo_bsb.html';
    }

    public function InicializeModelo() {
        $this->modelo = new cORDEMSERVICO();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_PBSB_PLAN_LIST();
    }

    public function get_cursorListagem(cTEMPLATE_LISTAGEM_TWIG $ptmpl) {
        return $cursor;
    }
}
