<?php

class cCTRL_OS_FORM_104 {

    public function Gerar() {
        $id_solicita_visto = cHTTP::ParametroValido('id_solicita_visto');
        $nu_candidato = cHTTP::ParametroValido('nu_candidato');
        $assinatura = cHTTP::$POST['assinatura'];

        $cand = new cCANDIDATO();
        $cand->mNU_CANDIDATO = $nu_candidato;
        $res_cand = $cand->CursorCandidatoCompleto($nu_candidato);
        $os_cand = $res_cand->fetch(PDO::FETCH_ASSOC);
        $os = new cORDEMSERVICO();
        $res = $os->CursorOSCompleta($id_solicita_visto);
        $os_rs = $res->fetch(PDO::FETCH_ASSOC);
        $empresa = new cEMPRESA();
        $servico = new cSERVICO();
        $res_serv = $servico->CursorServicoCompleto($os_rs['NU_SERVICO']);
        $rs_serv = $res_serv->fetch(PDO::FETCH_ASSOC);
        $empresa->RecuperePeloId($os_rs['NU_EMPRESA']);
        $embarcacao = new cEMBARCACAO_PROJETO();
        $embarcacao->mNU_EMPRESA = $os_rs['NU_EMPRESA'];
        $embarcacao->mNU_EMBARCACAO_PROJETO = $os_rs['NU_EMBARCACAO_PROJETO'];
        $embarcacao->RecupereSe();
//		$x = $embarcacao->mDS_JUSTIFICATIVA;
        $res_afiliadas = $empresa->Cursor_Associadas();
        $administradores = $empresa->Cursor_Administradores();
//		$afiliadas = '';
//		$br = '';
//		if(is_object($res_afiliadas)){
//			while($rs = $res_afiliadas->fetch(PDO::FETCH_ASSOC)){
//				$afiliadas .= $br.$rw['NO_ASSOCIADA'];
//				$br = '<br/>';
//			}
//		}
//		if($afiliadas == ''){
//			$afiliadas = '--';
//		}

        $autorizacao = new cAUTORIZACAO_CANDIDATO();
        $res_auto = $autorizacao->CursorCompleto($id_solicita_visto, $nu_candidato);
        $auto_rs = $res_auto->fetch(PDO::FETCH_ASSOC);

        $proc = cprocurador_empresa::RecordsetCompleto(cHTTP::ParametroValido('idEmpresa'), cHTTP::ParametroValido('idProcurador'));
        $caminho = cAMBIENTE::$m_raiz.'/imagens/assinaturas/'.$proc['imagem'];
        $proc['assinatura'] =  base64_encode(file_get_contents($caminho));

        $idProcurador2 = cHTTP::ParametroValido('idProcurador2');
        if ($idProcurador2 > 0) {
            $proc2 = cprocurador_empresa::RecordsetCompleto(cHTTP::ParametroValido('idEmpresa'), cHTTP::ParametroValido('idProcurador2'));
            $caminho = cAMBIENTE::$m_raiz.'/imagens/assinaturas/'.$proc2['imagem'];
            $proc2['assinatura'] =  base64_encode(file_get_contents($caminho));
        }

        $deps = $cand->ArrayDependentesNaOs($id_solicita_visto);

        $alinea_d = "d) Informa que o estrangeiro exercerá suas funções no(s) endereço(s) abaixo relacionados, comprometendo-se a informar a Coordenação-Geral de Imigração qualquer outro endereço onde o estrangeiro vier a atuar: ";
        if ($os_rs['NU_SERVICO'] == 1){ // RN72
            $alinea_d .= "<br/>a. a bordo da embarcação <strong>".$embarcacao->mNO_EMBARCACAO_PROJETO."</strong>";
        } else {
            $alinea_d .= "<br/>a. ".$empresa->mNO_ENDERECO." ".$empresa->mNO_COMPLEMENTO_ENDERECO.", ".$empresa->mNO_BAIRRO." - ".$empresa->mNO_MUNICIPIO." - ".$empresa->mCO_UF;
            if ($empresa->mempr_fl_tem_embarcacao == 1){
                $alinea_d.= ", bem como nas plataformas e embarcações dos principais clientes da ".$empresa->mNO_RAZAO_SOCIAL.";";
            }
        }

        $data = array();

        $os_cand['TE_TRABALHO_ANTERIOR_BRASIL'] = str_replace("\n", "<br/>", $os_cand['TE_TRABALHO_ANTERIOR_BRASIL']);
        if ($os_cand['CO_SEXO'] == 'M') {
            $os_cand['CO_SEXO'] = 'Masculino';
        } elseif ($os_cand['CO_SEXO'] == 'F') {
            $os_cand['CO_SEXO'] = 'Feminino';
        }
        $empresa->mTE_OBJETO_SOCIAL = str_replace("\n", "<br/>", $empresa->mTE_OBJETO_SOCIAL);
        $data['tamanho_letra'] = cHTTP::$POST['font-size'];
        $data['candidato'] = $os_cand;
        $data['solicita_visto'] = $os_rs;
        $data['autorizacao'] = $auto_rs;
        $data['empresa'] = $empresa;
        $data['servico'] = $rs_serv;
        $data['procurador'] = $proc;
        if ($idProcurador2 > 0) {
            $data['procurador2'] = $proc2;
        }
        $data['dependentes'] = $deps;
        $data['afiliadas'] = $res_afiliadas;
        $data['embarcacao'] = $embarcacao;
        $data['administradores'] = $empresa->Cursor_Administradores();
        $data['diretores'] = $empresa->Cursor_Diretores();
        $data['representantes'] = $empresa->Cursor_RepresentantesFuncionarios();
        $data['qtd_afiliadas'] = $empresa->qtd_Associadas();
        $data['qtd_administradores'] = $empresa->qtd_Administradores();
        $data['qtd_diretores'] = $empresa->qtd_Diretores();
        $data['qtd_representantes'] = $empresa->qtd_RepresentantesFuncionarios();
        $data['alinea_d'] = $alinea_d;

//        if($assinatura == "0" || !file_exists('/imagens/assinaturas/'.$data['procurador']['imagem'])){
        if ($assinatura == "0") {
            $data['procurador']['imagem'] = '';
        }

        $html = cTWIG::Render('public/form_104.html', $data);

        $nome = 'formulario_104_'.$os_cand['NOME_COMPLETO'];
        $nome = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $nome);
        $nome = preg_replace("([\.]{2,})", '', $nome);
        $nome = str_replace(" ", "_", $nome);
        $nome.= ".pdf";

        cHTTP::$nomeArq = $nome;
        // $html = cBANCO::SemCaracteresEspeciaisMS($html);
        // $html = utf8_encode($html);
        $mpdf = new mPDF();
        $mpdf->debug = true;
        $mpdf->showImageErrors = true;
        $mpdf->defaultfooterfontstyle = '';
        $mpdf->defaultfooterline = 0;
        $mpdf->setFooter('|{PAGENO}/{nb}|'.date('d/m/Y - H:i:s'));
        $mpdf->img_dpi = 200;
        $mpdf->WriteHTML($html);
//		$mpdf->Output();

        $updir = cAMBIENTE::getDirForm() . DIRECTORY_SEPARATOR . 'OS' . $id_solicita_visto . DIRECTORY_SEPARATOR . $nu_candidato;
        if (!is_dir($updir)) {
            mkdir($updir, 0770, true);
        }
        cARQUIVOS_FORMULARIOS::AdicionaFormulario($os_rs['NU_EMPRESA'], $id_solicita_visto, $nu_candidato, $nome, 14);
        $mpdf->Output($updir . DIRECTORY_SEPARATOR . $nome, 'F');
        header('Location:../arquivos/formularios/OS' . $id_solicita_visto . '/' . $nu_candidato . '/'.$nome);
        header('Content-Disposition: attachment; filename="'.$nome.'"');
    }

    public function Debug() {
        $id_solicita_visto = cHTTP::ParametroValido('id_solicita_visto');
        $nu_candidato = cHTTP::ParametroValido('nu_candidato');
        $assinatura = cHTTP::$POST['assinatura'];

        $cand = new cCANDIDATO();
        $cand->mNU_CANDIDATO = $nu_candidato;
        $res_cand = $cand->CursorCandidatoCompleto($nu_candidato);
        $os_cand = $res_cand->fetch(PDO::FETCH_ASSOC);
        $os = new cORDEMSERVICO();
        $res = $os->CursorOSCompleta($id_solicita_visto);
        $os_rs = $res->fetch(PDO::FETCH_ASSOC);
        $empresa = new cEMPRESA();
        $servico = new cSERVICO();
        $res_serv = $servico->CursorServicoCompleto($os_rs['NU_SERVICO']);
        $rs_serv = $res_serv->fetch(PDO::FETCH_ASSOC);
        $empresa->RecuperePeloId($os_rs['NU_EMPRESA']);
        $embarcacao = new cEMBARCACAO_PROJETO();
        $embarcacao->mNU_EMPRESA = $os_rs['NU_EMPRESA'];
        $embarcacao->mNU_EMBARCACAO_PROJETO = $os_rs['NU_EMBARCACAO_PROJETO'];
        $embarcacao->RecupereSe();
//		$x = $embarcacao->mDS_JUSTIFICATIVA;
        $res_afiliadas = $empresa->Cursor_Associadas();
        $administradores = $empresa->Cursor_Administradores();
//		$afiliadas = '';
//		$br = '';
//		if(is_object($res_afiliadas)){
//			while($rs = $res_afiliadas->fetch(PDO::FETCH_ASSOC)){
//				$afiliadas .= $br.$rw['NO_ASSOCIADA'];
//				$br = '<br/>';
//			}
//		}
//		if($afiliadas == ''){
//			$afiliadas = '--';
//		}

        $autorizacao = new cAUTORIZACAO_CANDIDATO();
        $res_auto = $autorizacao->CursorCompleto($id_solicita_visto, $nu_candidato);
        $auto_rs = $res_auto->fetch(PDO::FETCH_ASSOC);

        $proc = cprocurador_empresa::RecordsetCompleto(cHTTP::ParametroValido('idEmpresa'), cHTTP::ParametroValido('idProcurador'));
        $caminho = cAMBIENTE::$m_raiz.'/imagens/assinaturas/'.$proc['nu_procurador'].'.jpg';
        $proc['assinatura'] =  base64_encode(file_get_contents($caminho));

        $idProcurador2 = cHTTP::ParametroValido('idProcurador2');
        if ($idProcurador2 > 0) {
            $proc2 = cprocurador_empresa::RecordsetCompleto(cHTTP::ParametroValido('idEmpresa'), cHTTP::ParametroValido('idProcurador2'));
            $caminho = cAMBIENTE::$m_raiz.'/imagens/assinaturas/'.$proc2['nu_procurador'].'.jpg';
            $proc2['assinatura'] =  base64_encode(file_get_contents($caminho));
        }

        $deps = $cand->ArrayDependentesNaOs($id_solicita_visto);

        $data = array();

        $os_cand['TE_TRABALHO_ANTERIOR_BRASIL'] = str_replace("\n", "<br/>", $os_cand['TE_TRABALHO_ANTERIOR_BRASIL']);
        if ($os_cand['CO_SEXO'] == 'M') {
            $os_cand['CO_SEXO'] = 'Masculino';
        } elseif ($os_cand['CO_SEXO'] == 'F') {
            $os_cand['CO_SEXO'] = 'Feminino';
        }
        $empresa->mTE_OBJETO_SOCIAL = str_replace("\n", "<br/>", $empresa->mTE_OBJETO_SOCIAL);
        $data['tamanho_letra'] = cHTTP::$POST['font-size'];
        $data['candidato'] = $os_cand;
        $data['solicita_visto'] = $os_rs;
        $data['autorizacao'] = $auto_rs;
        $data['empresa'] = $empresa;
        $data['servico'] = $rs_serv;
        $data['procurador'] = $proc;


        if ($idProcurador2 > 0) {
            $data['procurador2'] = $proc2;
        }
        $data['dependentes'] = $deps;
        $data['afiliadas'] = $res_afiliadas;
        $data['embarcacao'] = $embarcacao;
        $data['administradores'] = $empresa->Cursor_Administradores();
        $data['diretores'] = $empresa->Cursor_Diretores();
        $data['representantes'] = $empresa->Cursor_RepresentantesFuncionarios();
        $data['qtd_afiliadas'] = $empresa->qtd_Associadas();
        $data['qtd_administradores'] = $empresa->qtd_Administradores();
        $data['qtd_diretores'] = $empresa->qtd_Diretores();
        $data['qtd_representantes'] = $empresa->qtd_RepresentantesFuncionarios();
//        if($assinatura == "0" || !file_exists('/imagens/assinaturas/'.$data['procurador']['imagem'])){
        if ($assinatura == "0") {
            $data['procurador']['imagem'] = '';
        }

        $html = cTWIG::Render('public/form_104.html', $data);
        print $html;
    }

}
