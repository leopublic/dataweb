<?php

class cCTRL_RESUMO_COBRANCA extends cCTRL_MODELO {

    public function ComplementarLinhaListagem(&$pTmpl) {
        //TODO: restringir o acesso aos botoes conforme o status do resumo.
    }

    public function Liste() {
        $resumo = new cresumo_cobranca();
        $tmpl = new cTMPL_RESUMO_COBRANCA_LISTAR();
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $tmpl->CarregueDoHTTP();
        if (!$tela->mFoiPostado) {
            $tela->mFiltro['nu_empresa_prestadora']->mValor = 1;
        }

        if ($tmpl->getValorFiltro('resc_dt_faturamento_de') != '') {
            $tmpl->mFiltro['resc_dt_faturamento_de']->mWhere = " resc_dt_faturamento >= " . cBANCO::DataOk($tmpl->getValorFiltro('resc_dt_faturamento_de'));
        }
        if ($tmpl->getValorFiltro('resc_dt_faturamento_ate') != '') {
            $tmpl->mFiltro['resc_dt_faturamento_ate']->mWhere = " resc_dt_faturamento <= " . cBANCO::DataOk($tmpl->getValorFiltro('resc_dt_faturamento_ate'));
        }
        if (intval($tmpl->getValorFiltro('pre2012')) == 0) {
            $tmpl->mFiltro['pre2012']->mWhere = " (resc_nu_numero is null or resc_nu_numero != 0)";
        } else {
            $tmpl->mFiltro['pre2012']->mWhere = " 1=1 ";
        }
        $qtdReg = $resumo->ListarQtd($tmpl->mFiltro);
        $offset = $tmpl->get_offset();
        if ($offset > 0) {
            $qtdPag = floor($qtdReg / $tmpl->get_offset());
        } else {
            $qtdPag = 1;
        }
        $tmpl->setValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS, $qtdPag);

        if (cHTTP::HouvePost()){
            if ($tmpl->get_acaoPostada() == 'PRIMEIRA') {
                $tmpl->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, 1);
                $tmpl->setValorCampo('__acao', '');
            }
            if ($tmpl->get_acaoPostada() == 'PROXIMA') {
                $tmpl->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($tmpl->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) + 1);
                $tmpl->setValorCampo('__acao', '');
            }
            if ($tmpl->get_acaoPostada() == 'ANTERIOR') {
                $tmpl->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($tmpl->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) - 1);
                $tmpl->setValorCampo('__acao', '');
            }
            if ($tmpl->get_acaoPostada() == 'ULTIMA') {
                $tmpl->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($tmpl->getValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS)));
                $tmpl->setValorCampo('__acao', '');
            }
        }
        $cursor = $resumo->Listar($tmpl->mFiltro, $tmpl->get_ordenacao(), $tmpl->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL), $tmpl->get_offset());

        $tmpl->Renderize($cursor);
        $ret = $tmpl->outputBuffer;
        $ret.= $tmpl->TratamentoPadraoDeMensagens();
        return $this->EnveloparParaTela($ret, $tmpl);
    }

    public function ListeOs() {
        $resumo = new cresumo_cobranca();
        $resumo->mresc_id = cHTTP::ParametroValido('resc_id');
        $resumo->RecuperePeloId();
        $tmpl = new cTMPL_RESUMO_COBRANCA_LISTAR_OS();
        $tmpl->mTitulo = 'Resumo de cobrança - ' . $resumo->mNO_RAZAO_SOCIAL . '-' . $resumo->mNO_EMBARCACAO_PROJETO . ' - ' . $resumo->mresc_dt_faturamento;
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $tmpl->CarregueDoHTTP();
        $cursor = $resumo->ListeOs("", $tmpl->mFiltro, $tmpl->get_ordenacao());
        $tmpl->Renderize($cursor);
        $ret = $tmpl->outputBuffer;
        $ret.= $tmpl->TratamentoPadraoDeMensagens();
        return $this->EnveloparParaTela($ret, $tmpl);
    }

    public function ListeOsExcel() {
        cHTTP::$MIME = cHTTP::mmXLS;
        $resumo = new cresumo_cobranca();
        $resumo->mresc_id = cHTTP::ParametroValido('resc_id');
        $resumo->RecuperePeloId();
        $emp = new cEMPRESA;
        $emp->mNU_EMPRESA = $resumo->mnu_empresa_requerente;
        $emp->RecuperePeloId();
        $nome = ereg_replace("[^A-Za-z0-9]", "_", $emp->mNO_RAZAO_SOCIAL);
        $tmpl = new cTMPL_RESUMO_COBRANCA_LISTAR_OS_EXCEL();
        $tmpl->CarregueDoHTTP();
        $cursor = $resumo->CursorDemonstrativoDiario("", $tmpl->mFiltro, $tmpl->get_ordenacao());
        cHTTP::set_nomeArquivo('Resumo_'.$nome."_".str_replace('/', '_', $resumo->mresc_dt_faturamento).'.xls');
        $tmpl->Renderize($cursor);
        $ret = $tmpl->outputBuffer;
        return $ret;
    }

    public function DemonstrativoDiario() {
        $resumo = new cresumo_cobranca();
        $tmpl = new cTMPL_RESUMO_COBRANCA_DEMONSTRATIVO_DIARIO();
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $tmpl->CarregueDoHTTP();
        if (!$tela->mFoiPostado) {
            $tela->mFiltro['nu_empresa_prestadora']->mValor = 1;
        }

        if (intval($tmpl->getValorFiltro('pre2012')) == 0) {
            $tmpl->mFiltro['pre2012']->mValor = '0';
            $tmpl->mFiltro['pre2012']->mWhere = " (rc.resc_nu_numero > 0 or rc.resc_nu_numero is null) ";
        } else {
            $tmpl->mFiltro['pre2012']->mWhere = " 1=1 ";
        }

        if ($tmpl->mFiltro['resc_dt_faturamento_ini']->mValor != '') {
            $tmpl->mFiltro['resc_dt_faturamento_ini']->mWhere = " resc_dt_faturamento >= " . cBANCO::DataOk($tmpl->mFiltro['resc_dt_faturamento_ini']->mValor);
        }
        if ($tmpl->mFiltro['resc_dt_faturamento_fim']->mValor != '') {
            $tmpl->mFiltro['resc_dt_faturamento_fim']->mWhere = " resc_dt_faturamento <= " . cBANCO::DataOk($tmpl->mFiltro['resc_dt_faturamento_fim']->mValor);
        }

        $cursor = $resumo->CursorDemonstrativoDiario("", $tmpl->mFiltro, $tmpl->get_ordenacao());
        $tmpl->Renderize($cursor);
        $ret = $tmpl->outputBuffer;
        $ret.= $tmpl->TratamentoPadraoDeMensagens();
        return $this->EnveloparParaTela($ret, $tmpl);
    }

    public function DemonstrativoDiarioExcel() {
        cHTTP::$MIME = cHTTP::mmXLS;
        $resumo = new cresumo_cobranca();
        $tmpl = new cTMPL_RESUMO_COBRANCA_DEMONSTRATIVO_DIARIO_EXCEL();
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $tmpl->CarregueDoHTTP();
        if (intval($tmpl->getValorFiltro('pre2012')) == 0) {
            $tmpl->mFiltro['pre2012']->mValor = '0';
            $tmpl->mFiltro['pre2012']->mWhere = " (rc.resc_nu_numero > 0 or rc.resc_nu_numero is null) ";
        } else {
            $tmpl->mFiltro['pre2012']->mWhere = " 1=1 ";
        }
        $periodo = '';
        if ($tmpl->mFiltro['resc_dt_faturamento_ini']->mValor != '') {
            $tmpl->mFiltro['resc_dt_faturamento_ini']->mWhere = " resc_dt_faturamento >= " . cBANCO::DataOk($tmpl->mFiltro['resc_dt_faturamento_ini']->mValor);
            $periodo = '_de_'.str_replace("'", '', str_replace('/', '_', cBANCO::DataOk($tmpl->mFiltro['resc_dt_faturamento_ini']->mValor)));
        }
        if ($tmpl->mFiltro['resc_dt_faturamento_fim']->mValor != '') {
            $tmpl->mFiltro['resc_dt_faturamento_fim']->mWhere = " resc_dt_faturamento <= " . cBANCO::DataOk($tmpl->mFiltro['resc_dt_faturamento_fim']->mValor);
            $periodo .= '_ate_'.str_replace("'", '', str_replace('/', '_', cBANCO::DataOk($tmpl->mFiltro['resc_dt_faturamento_fim']->mValor)));
        }
        cHTTP::$nomeArq = "movimentacao_diaria".$periodo.".xls";

        $cursor = $resumo->CursorDemonstrativoDiario("", $tmpl->mFiltro, $tmpl->get_ordenacao());
        $tmpl->Renderize($cursor);
        $ret = $tmpl->outputBuffer;
        return $ret;
    }

    public function Edite() {
        $tmpl = new cTMPL_RESUMO_COBRANCA_EDITAR();
        $tmpl->CarregueDoHTTP();
        $resumo_cobranca = new cresumo_cobranca();
        // Carrega o usuario correspondente aa tela exibida
        if (cHTTP::HouvePost()) {
            // A tela ja foi apresentada e isso eh um POST
            // Testa acoes
            switch ($tmpl->get_acaoPostada()) {
                case 'SALVAR':
                    $tmpl->AtribuaAoOBJETO($resumo_cobranca);
                    try {
                        $resc_id_ant = $tmpl->getValorCampo('resc_id');
                        $resumo_cobranca->Salvar();
                        if($resc_id_ant == 0){
                            $rep = new cREPOSITORIO_FATURAMENTO;
                            $rep->faturarTemporarias(cSESSAO::$mcd_usuario, $resumo_cobranca->mresc_id);
                        }
                        $tmpl->setValorCampo('resc_id', $resumo_cobranca->mresc_id);
                        cHTTP::$SESSION['msg'] = 'Operação realizada com sucesso!';
                        cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_SUCCESS;
                        $ParametrosAdicionais .= '&resc_id=' . $resumo_cobranca->mresc_id;
                        if ($tmpl->getValorCampo('resc_id') > 0) {
                            cHTTP::RedirecionePara('cCTRL_RESUMO_COBRANCA', 'Liste');
                        } else {
                            cHTTP::RedirecionePara('cCTRL_RESUMO_COBRANCA', 'ListeOs', $ParametrosAdicionais);
                        }
                    } catch (cERRO_CONSISTENCIA $e) {
                        cHTTP::$SESSION['msg'] = $e->getMessage();
                        cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
                    } catch (Exception $e) {
                        cHTTP::$SESSION['msg'] = 'N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o pois:<br/>' . htmlentities($e->getMessage());
                        cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ERROR;
                    }
                    break;
            }
        } else {
            // Se nao houve o post, entao eh a primeira vez que estah exibindo
            $tmpl->AtribuaAoOBJETO($resumo_cobranca);
            if (intval($resumo_cobranca->getid()) > 0) {
                $resumo_cobranca->RecuperePeloId();
            } else {
//				$resumo_cobranca->mresc_nu_numero_mv = cresumo_cobranca::get_proximoNumero();
            }
            $tmpl->CarregueDoOBJETO($resumo_cobranca);
        }
        $conteudo = '';
        $conteudo .= $tmpl->HTML();
        $conteudo .= $tmpl->TratamentoPadraoDeMensagens();
        return $conteudo;
    }

    public function IndiqueNotaFiscal() {
        return parent::Edite(new cTMPL_RESUMO_COBRANCA_INDICAR_FATURA(), new cresumo_cobranca(), "cCTRL_RESUMO_COBRANCA", "Liste");
    }

    public function AdicioneOs() {
        try {
            $resumo = new cresumo_cobranca();
            $resumo->mresc_id = cHTTP::$GET['resc_id'];
            $parametrosAdicionais = "resc_id=" . cHTTP::$GET['resc_id'];
            $resumo->AdicioneOs(cHTTP::$GET['id_solicita_visto']);
            cHTTP::$SESSION['msg'] = "Ordem de serviço adicionada ao resumo com sucesso.";
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_SUCCESS;
        } catch (cERRO_CONSISTENCIA $e) {
            cHTTP::$SESSION['msg'] = $e->getMessage();
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
        } catch (Exception $e) {
            cHTTP::$SESSION['msg'] = 'N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o pois:<br/>' . htmlentities($e->getMessage());
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ERROR;
        }
        cHTTP::RedirecionePara('cCTRL_RESUMO_COBRANCA', 'ListeOs', $parametrosAdicionais);
    }

    public function RemovaOs() {
        try {
            $resumo = new cresumo_cobranca();
            $resumo->mresc_id = cHTTP::$GET['resc_id'];
            $parametrosAdicionais = "resc_id=" . cHTTP::$GET['resc_id'];
            $resumo->RemovaOs(cHTTP::$GET['id_solicita_visto'], 'cCTRL_RESUMO_COBRANCA', 'RemovaOs');
            cHTTP::$SESSION['msg'] = "Ordem de serviço retirada do resumo com sucesso.";
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_SUCCESS;
        } catch (cERRO_CONSISTENCIA $e) {
            cHTTP::$SESSION['msg'] = $e->getMessage();
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
        } catch (Exception $e) {
            cHTTP::$SESSION['msg'] = 'N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o pois:<br/>' . htmlentities($e->getMessage());
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ERROR;
        }
        cHTTP::RedirecionePara('cCTRL_RESUMO_COBRANCA', 'ListeOs', $parametrosAdicionais);
    }

    public function Exclua() {
        try {
            $resumo = new cresumo_cobranca();
            $resumo->mresc_id = cHTTP::$GET['resc_id'];
            $resumo->Excluir();
            cHTTP::$SESSION['msg'] = "Resumo excluído com sucesso. As ordens de serviço que estavam associadas a ele estão novamente liberadas para faturamento. Verifique.";
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_SUCCESS;
        } catch (cERRO_CONSISTENCIA $e) {
            cHTTP::$SESSION['msg'] = $e->getMessage();
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
        } catch (Exception $e) {
            cHTTP::$SESSION['msg'] = 'N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o pois:<br/>' . htmlentities($e->getMessage());
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ERROR;
        }
        cHTTP::RedirecionePara('cCTRL_RESUMO_COBRANCA', 'Liste');
    }

    public static function LinkParaResumo($presc_id) {
        $link = '';
        if (intval($presc_id) > 0) {
            $resumo = new cresumo_cobranca();
            $resumo->mresc_id = $presc_id;
            $resumo->RecuperePeloId();
            if ($resumo->mresc_nu_numero_mv != '') {
                $numero = $resumo->mresc_nu_numero_mv;
            } else {
                $numero = '(resumo sem número)';
            }
            if (Acesso::permitido(Acesso::tp_Cobranca)) {
                $link = '<a href="/paginas/carregueComMenu.php?controller=cCTRL_RESUMO_COBRANCA&metodo=Edite&resc_id=' . $presc_id . '">' . $numero . '</a>';
            } else {
                $link = '<a href="#" onclick="alert(\'Seu perfil de usuário não tem acesso às funcionalidades de cobranca.\');">' . $numero . '</a>';
            }
        } else {
            $link = "(sem resumo)";
        }
        return $link;
    }

    public static function LinkParaOsDoResumo($presc_id) {
        $link = '';
        if (intval($presc_id) > 0) {
            $resumo = new cresumo_cobranca();
            $resumo->mresc_id = $presc_id;
            $resumo->RecuperePeloId();
            if ($resumo->mresc_nu_numero_mv != '') {
                $numero = $resumo->mresc_nu_numero_mv;
            } else {
                $numero = '(resumo sem número)';
            }
            if (Acesso::permitido(Acesso::tp_Cobranca)) {
                $link = '<a href="/paginas/carregueComMenu.php?controller=cCTRL_RESUMO_COBRANCA&metodo=ListeOs&resc_id=' . $presc_id . '">' . $numero . '</a>';
            } else {
                $link = '<a href="#" onclick="alert(\'Seu perfil de usuário não tem acesso às funcionalidades de cobranca.\');">' . $numero . '</a>';
            }
        } else {
            $link = "(sem resumo)";
        }
        return $link;
    }

    public function atualize_precos(){
            $resc_id = $_GET['resc_id'];
            $sql = "update solicita_visto, empresa, preco set soli_vl_cobrado = prec_vl_conceito where solicita_visto.resc_id = ".$resc_id." and empresa.nu_empresa = solicita_visto.nu_empresa and preco.tbpc_id = empresa.tbpc_id and preco.nu_servico = solicita_visto.nu_servico";
            cAMBIENTE::$db_pdo->exec($sql);
            $parametrosAdicionais = "resc_id=" . $resc_id;
            cHTTP::$SESSION['msg'] = "Preços atualizados com sucesso.";
            cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_SUCCESS;
            cHTTP::RedirecionePara('cCTRL_RESUMO_COBRANCA', 'ListeOs', $parametrosAdicionais);
    }
}
