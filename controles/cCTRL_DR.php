<?php

class cCTRL_DR extends cCTRL_OPERACIONAL {

    public function postListarCandidatos(){
        $nu_embarcacao_projeto = $this->valorInputSalvo('CMP_nu_embarcacao_projeto');
        $nome_completo = $this->valorInputSalvo('CMP_nome_completo');



    }

    public function listarPendentes() {
        $menu = cLAYOUT::soMenu();
        $repCand = new cREPOSITORIO_CANDIDATO;
        $repDr = new cREPOSITORIO_DR;

        $empresas= cEMPRESA::combo(true,'(todas)', 'nu_empresa', 'no_razao_social', ' where coalesce(fl_ativa, 0) = 1');

        $nu_empresa = $this->valorInputSalvo('nu_empresa');
        $nome_completo = $this->valorInputSalvo('nome_completo');
        $escopo = $this->valorInputSalvo('escopo');
        $where = '';
        if ($nu_empresa != ''){
            $where .= ' and c.nu_empresa = '.$nu_empresa;
        }
        if ($nome_completo != ''){
            $where .= " and c.nome_completo like '%".$nome_completo."%'";
        }
        if ($escopo != ''){
            if ($escopo == 'pendentes'){
                $where .= ' and coalesce(c.id_status_edicao, 0) < 5 and DATEDIFF(now(), c.dt_cadastramento) <= 3 ';
            }
            elseif ($escopo == 'expiradas'){
                $where .= ' and coalesce(c.id_status_edicao, 0) <> 5 and DATEDIFF(now(), c.dt_cadastramento) > 3';
            }
            elseif ($escopo == 'importados'){
                $where .= ' and coalesce(c.id_status_edicao, 0) = 5 ';
            }
        } else {
            $escopo = 'todas';
        }
        $cadastros_pendentes = $repDr->pendentes($where);

        $convs = array();
        foreach ($cadastros_pendentes as &$cand) {
            $cand['responsaveis'] = $repCand->responsaveisConcatenados($cand['nome1'], $cand['nome2'], $cand['nome3'], $cand['nome4'], $cand['nome5']);
            $cand['status'] = cstatus_edicao::descricao($cand['id_status_edicao']);

            if ($cand['nome'] != ''){
                $cand['convidado'] = $cand['nome']."<br/>".$cand['email'];
            } else {
                $cand['convidado'] = '--';
            }
            $data = '';
            $alterador = '';
            if ($cand['dt_cadastramento_fmt'] == '00/00/0000 00:00:00'){
                $data = '(n/d)';
            } else {
                $data = $cand['dt_cadastramento_fmt'];
            }
            if ($cand['nome_alterador'] == ''){
                $alterador = '(n/d)';
            } else {
                $alterador = $cand['nome_alterador'];
            }
            $cand['cadastro'] = $alterador.'<br/>'.$data;
            if ($cand['conv_id'] > 0){
                if(!array_key_exists($cand['conv_id'], $convs)){
                    $conv = new cconvidado;
                    $conv->mconv_id = $cand['conv_id'];
                    $convs[$cand['conv_id']] = $conv->qtdDrsImportadosSemOs();
                }
                $cand['qtdDrs'] = $convs[$cand['conv_id']];
            }
        }
        $data = array(
            'menu' => $menu,
            'usuario' => cSESSAO::$mnome,
            'cadastros_pendentes' => $cadastros_pendentes,
            'empresas' => $empresas,
            'escopo' => $escopo,
            'nome_completo' => $nome_completo,
            'nu_empresa' => $nu_empresa,
            'qtdPag' => $qtdPag,
            'pag' => $pag,
            'qtdReg' => $qtdReg,
            'msg' => $_SESSION['msg']

        );
        $_SESSION['msg'] = '';
        return cTWIG::Render('intranet/candidato/pendentes_list.twig', $data);
    }

    public function excluir() {
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        $rep = new cREPOSITORIO_DR;
        try {
            $rep->excluir($nu_candidato_tmp);
            $_SESSION['msg'] = 'DR excluido com sucesso';
        } catch (Exception $ex) {
            $_SESSION['msg'] = $ex->getMessage();
        }
        cHTTP::RedirecionaLinkInterno('/paginas/carregue.php?controller=cCTRL_DR&metodo=listarPendentes');
    }

    public function getImportarDr(){
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        $cand = new cCANDIDATO();
        $dr = new cCANDIDATO_TMP();
        $dr->RecuperarPeloTmp($nu_candidato_tmp);
        $emp = new cEMPRESA;
        $pais = new cPAIS_NACIONALIDADE;
        if ($dr->mconv_id > 0){
            $podeAbrirOs = false;
        } else {
            $podeAbrirOs = true;
        }
        if(intval($dr->mNU_CANDIDATO) > 0){
            $cand->Recuperar($dr->mNU_CANDIDATO);
            if ($cand->mCO_NACIONALIDADE > 0){
                $pais->RecuperePeloId($cand->mCO_NACIONALIDADE);
            }
        }
        $emp->RecuperePeloId($dr->mNU_EMPRESA);
        //
        $servico = new cSERVICO;
        if (intval($dr->mnu_servico) > 0){
            $servico->RecuperePeloId($dr->mnu_servico);
        } else {
            $servico->mNO_SERVICO_RESUMIDO = '(não informado)';
        }
        $rep = new cREPOSITORIO_CANDIDATO;
        $homonimos = $rep->cursorHomonimos($dr->mNOME_COMPLETO, intval($dr->mNU_CANDIDATO));


        $experiencias = cexperiencia_profissional::cursorDoCandidatoTmp($dr->mnu_candidato_tmp);
        $ret = '';
        $sep = '';
        foreach($experiencias as $exp){
            $ret .= $sep.'EMPRESA: '.$exp['epro_no_companhia'];
            $ret .= "\nFUNÇÃO: ".$exp['epro_no_funcao'];
            $ret .= "\nPERÍODO: de ".$exp["epro_dt_inicio_fmt"]." até ".$exp["epro_dt_fim_fmt"];
            $ret .= "\nATIVIDADES: ".$exp["epro_tx_atribuicoes"];
            $sep = "\n\n";
        }
        $dr->mTE_TRABALHO_ANTERIOR_BRASIL = $ret;

        $projetos = cEMBARCACAO_PROJETO::daEmpresa($dr->mNU_EMPRESA, "(não informado)");

        $data = array(
            'cand'              => $cand,
            'dr'                => $dr,
            'homonimos'         => $homonimos,
            'arquivos'          => $dr->arquivos(),
            'servico'           => $servico,
            'paises'            => cPAIS_NACIONALIDADE::combo(true, '(não informado)', 'co_pais', 'no_pais'),
            'nacionalidades'    => cPAIS_NACIONALIDADE::combo(),
            'profissoes'        => cPROFISSAO::combo(),
            'estados_civis'     => cESTADO_CIVIL::combo(),
            'escolaridades'     => cESCOLARIDADE::combo(),
            'funcoes'           => cFUNCAO_CARGO::combo(),
            'locais'            => clocal_projeto::combo(),
            'reparticoes'       => cREPARTICAO_CONSULAR::combo(),
            'projetos'          => $projetos,
            'experiencias'      => $experiencias,
            'empresa'           => $emp,
            'pais'              => $pais,
            'sexos'             => cSEXO::combo(),
            'podeAbrirOs'       => $podeAbrirOs,
            'menu'              => cLAYOUT::soMenu(),
            'success'               => $_SESSION['success'],
            'info'               => $_SESSION['info'],
            'warning'               => $_SESSION['warning'],
            'error'               => $_SESSION['error']
        );
        $_SESSION['success'] = '';
        $_SESSION['info'] = '';
        $_SESSION['warning'] = '';
        $_SESSION['error'] = '';
        return cTWIG::Render('intranet/candidato/importa_dr.twig', $data);
    }

    public function postImportarDr(){
        try{
            $rep = new cREPOSITORIO_DR;
            $post = $_POST;
            $rep->importaDr($post, cSESSAO::$mcd_usuario);
            $_SESSION['success'] = 'Cadastro atualizado com sucesso.';
        } catch (Exception $ex) {
            $_SESSION['error'] = 'Não foi possível importar o DR pois: '.$ex->getMessage();
        }
        cHTTP::RedirecionaLinkInterno('/paginas/carregue.php?controller=cCTRL_DR&metodo=getImportarDr&nu_candidato_tmp='.$_POST['CMP_nu_candidato_tmp']);
    }
    /**
     * Associa o DR a um candidato existente
     * @return [type] [description]
     */
    public function getAssociaDrACandidato(){
        $nu_candidato = $_GET['nu_candidato'];
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        $rep = new cREPOSITORIO_DR;
        $rep->associeCandidato($nu_candidato, $nu_candidato_tmp);
        $_SESSION['msg'] = 'Cadastro associado com sucesso.';
        cHTTP::RedirecionaLinkInterno('/paginas/carregue.php?controller=cCTRL_DR&metodo=getImportarDr&nu_candidato_tmp='.$nu_candidato_tmp);
    }

    /**
     * Desvincula o DR de qualquer candidato
     * @return [type] [description]
     */
    public function getDesvinculeCandidato(){
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        $rep = new cREPOSITORIO_DR;
        $rep->desvinculeCandidato($nu_candidato_tmp);
        $_SESSION['msg'] = 'DR atualizado com sucesso.';
        cHTTP::RedirecionaLinkInterno('/paginas/carregue.php?controller=cCTRL_DR&metodo=getImportarDr&nu_candidato_tmp='.$nu_candidato_tmp);
    }

    public function getHomonimos(){
        $nu_candidato_tmp = $_POST['nu_candidato_tmp'];
        $nome_completo = $_POST['nome_completo'];
        $dr = new cCANDIDATO_TMP();
        $dr->RecuperarPeloTmp($nu_candidato_tmp);
        $rep = new cREPOSITORIO_CANDIDATO;
        $homonimos = array();
        $msg = '';
        if (trim($nome_completo) == ''){
            $msg = 'Informe o nome para recuperar os homônimos';
        } else {
            $qtdHomonimos= $rep->cursorHomonimosQtd($nome_completo, intval($dr->mNU_CANDIDATO));
            if ($qtdHomonimos > 300){
                $msg = 'Foram recuperados mais de 300 homônimos. Refine sua busca...';
            } else {
                if ($qtdHomonimos == 0){
                    $msg = 'Nenhum estrangeiro encontrado com esse nome.';
                } else {
                    $homonimos = $rep->cursorHomonimos($nome_completo, intval($dr->mNU_CANDIDATO));
                }
            }
        }
        $data = array(
                'homonimos'     => $homonimos,
                'dr'            => $dr,
                'msg'           => $msg
            );
        $ret = cTWIG::Render('intranet/candidato/importa_dr_homonimos.twig', $data);
        return $ret;
    }
    /**
     * Gera a cópia do DR em PDF
     * @return [type] [description]
     */
    public function testeGeraCopiaDr(){
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        $cand = new cCANDIDATO_TMP();
        $cand->RecuperarPeloTmp($nu_candidato_tmp);
        $rep = new cREPOSITORIO_CANDIDATO;
        return $rep->geraCopiaDrEnviado($cand);
    }

    public function testeMigracao(){
        $rep = new cREPOSITORIO_ARQUIVO();
        $rep->migraArquivos($_GET['nu_candidato_tmp'], $_GET['nu_candidato']);
    }

    public function testeNovoTmp(){
        $dr = new cCANDIDATO_TMP;
        $dr->mNO_PRIMEIRO_NOME = 'xxx';
        $dr->salvar();
        $id = $dr->mnu_candidato_tmp;
        $dr->mNO_NOME_MEIO = 'yyy';
        $dr->salvar();
        print $id;
    }

    public function postAdicionarExperiencia(){
        $rep = new cREPOSITORIO_DR;
        $exp = $rep->adicionarExperiencia($post['nu_candidato_tmp'], $post['no_companhia'], $post['no_funcao'], $post['no_periodo'], $post['tx_atribuicoes']);
    }

    public function getAbreOsDr(){
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        $dr = new cCANDIDATO_TMP;
        $dr->RecuperarPeloTmp($nu_candidato_tmp);
        if (intval($dr->mid_solicita_visto) == 0){
            $rep = new cREPOSITORIO_OS;
            $os = $rep->criaOsDeDr($dr);
            $id_solicita_visto = $os->mID_SOLICITA_VISTO;
            $_SESSION['msg'] = 'OS criada com sucesso!';
        } else {
            $id_solicita_visto = $dr->mid_solicita_visto;
            $_SESSION['msg'] = 'A OS desse DR já foi criada anteriormente.';
        }
        cHTTP::RedirecionaLinkInterno('/operador/os_DadosOS.php?&ID_SOLICITA_VISTO='.$id_solicita_visto.'&id_solicita_visto='.$id_solicita_visto);
    }

    public function getAbreOsConvidado(){
        $conv_id = $_GET['conv_id'];
        $rep = new cREPOSITORIO_OS;
        $os = $rep->criaOsDeConvite($conv_id);
        cHTTP::RedirecionaLinkInterno('/operador/os_DadosOS.php?&ID_SOLICITA_VISTO='.$os->mID_SOLICITA_VISTO.'&id_solicita_visto='.$os->mID_SOLICITA_VISTO);
    }

    public function getEnviarNovo(){
        $this->inicializarData();
        $this->adicionaEmpresas();

        $nu_empresa = $this->valorInputSalvo('nu_empresa');
        $nome = $this->valorInputSalvo('nome');
        $this->adicionaConteudo('nome', $nome);
        $email = $this->valorInputSalvo('email');
        $this->adicionaConteudo('email', $email);
        $this->adicionaConteudo('nu_empresa', $nu_empresa);
        if ($nu_empresa > 0){
            $embarcacoes = cEMBARCACAO_PROJETO::daEmpresa($nu_empresa);
            $servicos = cSERVICO::comboDisponiveisEmpresa($nu_empresa);
        } else {
            $embarcacoes = array(array('chave' => '', 'descricao'=> '(selecione a empresa)') );
            $servicos = array(array('chave' => '', 'descricao'=> '(selecione a empresa)') );
        }
        $this->adicionaConteudo('embarcacoes_projeto', $embarcacoes);
        $this->adicionaConteudo('servicos', $servicos);



        $this->adicionaConteudo('nu_embarcacao_projeto', $this->valorInputSalvo('nu_embarcacao_projeto'));
        $this->adicionaConteudo('nu_servico', $this->valorInputSalvo('nu_servico'));

        return cTWIG::Render('intranet/candidato/enviar_convite.twig', $this->data);
    }

    public function postEnviarNovo(){
        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $nu_empresa = $_POST['nu_empresa'];
        $nu_embarcacao_projeto = $_POST['nu_embarcacao_projeto'];
        $nu_servico = $_POST['nu_servico'];
        try{
            $rep = new cREPOSITORIO_DR;
            $rep->criar($nu_empresa, $nu_embarcacao_projeto, $nu_servico, $nome, $email, cSESSAO::$mcd_usuario);
            $_SESSION['success'] = 'DR criado com sucesso! Convite enviado para o "'.$email.'".';
            $_SESSION['nome'] = '';
            $_SESSION['nu_empresa'] = '';
            $_SESSION['nu_embarcacao_projeto'] = '';
            $_SESSION['email'] = '';
            $_SESSION['nu_servico'] = '';
            cHTTP::RedirecionaLinkInterno('/paginas/carregue.php?controller=cCTRL_DR&metodo=getEnviarNovo');
        } catch(Exception $e){
            $_SESSION['error'] = $e->getMessage();
            $_SESSION['nome'] = $_POST['nome'];
            $_SESSION['nu_empresa'] = $_POST['nu_empresa'];
            $_SESSION['nu_embarcacao_projeto'] = $_POST['nu_embarcacao_projeto'];
            $_SESSION['email'] = $_POST['email'];
            $_SESSION['nu_servico'] = $_POST['nu_servico'];
            cHTTP::RedirecionaLinkInterno('/paginas/carregue.php?controller=cCTRL_DR&metodo=getEnviarNovo');
        }
    }
    /**
     * Envia o DR corrigido pelo operacional para aprovação do cliente
     * @return [type] [description]
     */
    public function getEnviarParaAprovacao(){
        try{
            $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
            $rep = new cREPOSITORIO_DR;
            $dr = new cCANDIDATO_TMP;
            $dr->RecuperarPeloTmp($nu_candidato_tmp);
            $os = $rep->enviaParaAprovacao($dr, cSESSAO::$mcd_usuario);
            $_SESSION['success'] = 'DR atualizado e enviado para aprovação!';
            cHTTP::RedirecionaLinkInterno('/paginas/carregue.php?controller=cCTRL_DR&metodo=listarPendentes');
        } catch(Exception $e){
            $_SESSION['error'] = 'Houve um problema :'.$e->getMessage();
            cHTTP::RedirecionaLinkInterno('paginas/carregue.php?controller=cCTRL_DR&metodo=getImportarDr&nu_candidato_tmp='.$nu_candidato_tmp);
        }
    }

    public function testeEnviarAnexo(){
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];

        $rep = new cREPOSITORIO_ARQUIVO;
        $arq = $rep->recuperaArquivoTemporarioDoTipo($nu_candidato_tmp, cTIPO_ARQUIVO::COPIA_DR_PREENCHIDO_PELO_CLIENTE);
        $mail = new cEMAIL;
        var_dump($arq);
        print $arq->caminhoRealInstancia();
        $caminho = '../arquivos_tmp/1/CAND00000001_183763.pdf';
        $mail->mail->AddAttachment($caminho);
        $mail->EnvieSimplificado('leomedeiros@gmail.com', "teste", 'Teste');
    }
}
