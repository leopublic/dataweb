<?php

class cCTRL_CANDIDATO_ACESSO_EXTERNO_EDIT extends cCTRL_MODELO_EDIT {

    public function InicializeModelo() {
        $this->modelo = new cCANDIDATO();
    }

    public function InicializeTemplate() {
        $this->template = new cTEMPLATE_CANDIDATO_ACESSO_EXTERNO();
    }

    public function ProcesseSalvar() {
        $this->template->AtribuaAoOBJETO($this->modelo);
        $validador = new cvalidador_candidato();
        try {
            if ($validador->acessoOk($this->modelo)){
                $this->modelo->SalvarAcesso();
                cNOTIFICACAO::singleton('Alterações atualizadas com sucesso!', cNOTIFICACAO::TM_SUCCESS);
            } else {
                cNOTIFICACAO::singleton('Não foi possível salvar as alterações pois<br/>'.$validador->msg, cNOTIFICACAO::TM_ERROR);
            }
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
    }

}
