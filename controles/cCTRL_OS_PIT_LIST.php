<?php 
class cCTRL_OS_PIT_LIST extends cCTRL_MODELO_LIST{
	public function InicializeModelo()
	{
		$this->modelo = new cORDEMSERVICO();
	}
	
	public function InicializeTemplate()
	{
		$this->template = new cTMPL_OS_PIT_LIST();
		if(isset(cHTTP::$SESSION['chaves'])){
			$this->template->chavesSelecionadas = cHTTP::$SESSION['chaves'];
		}
	}
	
	public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) 
	{	
		$this->template->mFiltro['mes']->mWhere = ' month(dt_solicitacao) = '.$this->template->getValorFiltro('mes');
		$this->template->mFiltro['ano']->mWhere = ' year(dt_solicitacao) = '.$this->template->getValorFiltro('ano');
		
		return cORDEMSERVICO::CursorOSPIT($this->template->mFiltro, $this->template->get_ordenacao());
	}
	
}
