<?php 
class cCTRL_CAND_CREW_LIST extends cCTRL_MODELO_LIST{
	public function InicializeModelo()
	{
		$this->modelo = new cCANDIDATO();
	}
	
	public function InicializeTemplate()
	{
		$this->template = new cTMPL_CAND_CREW_LIST();
	}

	public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl)
	{
		if($this->template->getValorFiltro('NU_EMPRESA') > 0 && $this->template->getValorFiltro('NU_EMBARCACAO_PROJETO') > 0){
			$cursor = $this->modelo->Cursor_CREW_LIST($this->template->mFiltro, $this->template->get_ordenacao());			
			return $cursor;
		}
	}

}
