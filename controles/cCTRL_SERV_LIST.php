<?php

class cCTRL_SERV_LIST extends cCTRL_MODELO_LIST {

    public function InicializeModelo() {
        $this->modelo = new cSERVICO();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_SERVICO_LISTE();
    }

    public function AcessoLiberado() {
        if (Acesso::permitido(Acesso::tp_Sistema_Tabelas_Servico)) {
            return true;
        } else {
            cHTTP::$SESSION['msg'] = "Você não tem acesso a essa função.";
            cHTTP::RedirecioneParaExterno('/intranet/geral/principal.php');
        }
    }

    public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) {
        if ($this->template->getValorFiltro('ignorar_inativos') == '1') {
            $this->template->mFiltro['ignorar_inativos']->mWhere = ' serv_fl_ativo = 1';
        } else {
            $this->template->mFiltro['ignorar_inativos']->mWhere = ' 1 = 1';
        }
        return parent::get_cursorListagem($ptmpl);
    }

}
