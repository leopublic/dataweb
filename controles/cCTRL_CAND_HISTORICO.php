<?php 
class cCTRL_CAND_HISTORICO extends cCTRL_MODELO_LIST{
	public function InicializeModelo(){
		$this->modelo = new cCANDIDATO();
	}	

	public function InicializeTemplate(){
		$this->template = new cTMPL_CAND_HISTORICO();
	}

	public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl){
		$modelo = $this->modelo;
		$modelo->mNU_CANDIDATO = cHTTP::ParametroValido('NU_CANDIDATO');
		$cursor = $modelo->Cursor_Historico($this->template->mFiltros, $this->template->get_ordenacao());
		return $cursor;
	}
 }
