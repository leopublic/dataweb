<?php

class cINTERFACE_OS extends cORDEMSERVICO {

    public $mOS_REF;
    public $mNU_CANDIDATO_visivel;
    public $labelNC = array(
        0 => 'Indicar não conformidade'
        , 1 => 'Suspender não conformidade'
    );
    public $estiloNC = array(
        0 => 'NC_OFF'
        , 1 => 'NC_ON'
    );

    const abPROT_NAO_ENVIADOS = 1;
    const abPROT_ENVIADOS = 2;
    const abPROT_PROTOCOLO = 3;

    /**
     * Gera o painel de visualização de OSs antigas.
     * @param type $pOS
     * @return string
     * @deprecated Substituido pela visualização por processos.
     */
    public static function Processo_Antigas($pOS) {
        $ret = '';
        if ($pOS->mReadonly) {
            $ids = explode(",", $pOS->mCandidatos);

            $nomeTela = 'SOLICITACAO_ANTIGA_view';

            $tabs = '';
            for ($i = 0; $i < count($ids); $i++) {
                $OS_ANTIGA = new cEDICAO($nomeTela);
                $OS_ANTIGA->CarregarConfiguracao();
                $OS_ANTIGA->mCampoNome['id_solicita_visto']->mValor = $pOS->mID_SOLICITA_VISTO;
                $OS_ANTIGA->mCampoNome['NU_CANDIDATO']->mValor = $ids[$i];
                $OS_ANTIGA->RecupereRegistro();

                $tabs .= '<li><a href="#processo' . $OS_ANTIGA->mCampoNome['NU_CANDIDATO']->mValor . '">' . $OS_ANTIGA->mCampoNome['NOME_COMPLETO']->mValor . '</a></li>';
                $divs .= '<div id="processo' . $OS_ANTIGA->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
                $divs .= cINTERFACE::RenderizeEdicaoComPaineis($OS_ANTIGA, 2);
                $divs .= '</table>';
                $divs .= '</div>' . "\n";
            }
            $js .= '	$("#tabsProcessos").tabs()' . ";\n";
            $tabs = '<ul>' . $tabs . '</ul>' . "\n";

            $ret.= '<div id="tabsProcessos">' . "\n";
            $ret.= $tabs;
            $ret.= $divs;
            $ret.= '</div>';
            $ret.= '<script>$(document).ready(function() {$("#tabsProcessos").tabs();});</script>';
        } else {
            $ret.= 'Essa OS foi registrada na versão 1.0 do sistema, e só poderá ter as informações sobre o processo atualizadas se for designado um tipo de serviço para ela. Na aba "Dados OS" altere o tipo de serviço, salve, para que os campos referentes ao tipo de serviço escolhido sejam exibidos.';
        }
        return $ret;
    }

    /**
     * Controla a exibição das opções da OS
     */
    function menu() {
        // desabilita class anteriores
        $btn_menu = 'style="border:none;margin:0px;padding:0px;"';


        // BOTÃO ABRIR OS
        $btn_abrir_os = '';

        if ($this->mReadonly) { // mostrar somente se for ReadOnly
            //   if($nu_servico == 1){ // mostrar somente "ABRIR OS" se o nu_servico for "1"  equivalente ao serviço:  Aut. Tab. RN72
            //if(isset($_SESSION['cd_perfil_admin']) == 1){  // mostrar somente se for administrador
            if (Acesso::permitido(Acesso::tp_Os_Menu_Acao_Abrir)) {
                $btn_abrir_os = '
                    		<form method="post" style="display:inline;">
                    		    <input type="hidden" id="ID_SOLICITA_VISTO" name="ID_SOLICITA_VISTO" value="' . $this->mID_SOLICITA_VISTO . '"/>
                    			<input type="hidden" id="abreOs" name="abreOs" value="1"/>
                    			<input type="submit" value="Abrir OS" title="Clique para abrir a OS ' . $this->mID_SOLICITA_VISTO . ' " ' . $btn_menu . ' />
                    		</form>
                            ';
            }
            // }
            //  }
        }


        // BOTÃO EXCLUIR OS
        $btn_excluir_os = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_Acao_Excluir)) {
            //if(isset($_SESSION['cd_perfil_admin']) == 1){
            if ($this->mID_SOLICITA_VISTO <> 0) {
                $btn_excluir_os = '
          	     <input type="button" value="Excluir OS" onClick="ConfirmaExclusaoOS(' . $this->mID_SOLICITA_VISTO . ');" title="Clique para excluir essa OS"  ' . $btn_menu . '  />
                 ';
            }
        }
        //}
        //DEFINO BTN_ABRIR_OS
        if ($btn_abrir_os <> '') {
            if (Acesso::permitido(Acesso::tp_Os_Menu_Acao_Abrir)) {
                $btn_abrir_os = '<li><img src="../imagens/open.png" /> ' . $btn_abrir_os . '</li>';
            } else {
                $btn_abrir_os = '';
            }
        }

        //DEFINO BTN_EXCLUIR_OS
        if ($btn_excluir_os <> '') {
            $btn_excluir_os = '<li><img src="../imagens/delete.gif" /> ' . $btn_excluir_os . '</li>';
        }


        $gerar_email = '<li><img src="../imagens/icons/email_go.png" /><a href="GeradorEmail.php?ID_SOLICITA_VISTO=' . $this->mID_SOLICITA_VISTO . '"> E-mail de confirmação</a></li>';



        if ($this->estiloNC() == 'NC_ON') {
            $style = 'background:red;color:#fff';
            $imgNC = '';
        } else {
            $imgNC = '<img src="../imagens/action_encerrado.png">';
            $style = '';
        }


        $iten_checklist = '';
        if ($this->mNU_SERVICO == 1) {
            $iten_checklist = '<li>
                  <a href="../LIB/OS_RELATORIOS.php?ID_SOLICITA_VISTO=' . $this->mID_SOLICITA_VISTO . '" target="_blank">
                  <img src="../imagens/print.gif"/> CheckList
                  </a>
              </li>';
        }



        $checklists = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_Checklist)) {
            if ($this->mNU_SERVICO <> '') {
                $sql = "
                  SELECT C.NU_CHECKLIST, CS.NU_SERVICO, C.NO_CHECKLIST
                    FROM CHECKLIST_SERVICO CS
              INNER JOIN CHECKLIST C
                      ON C.NU_CHECKLIST = CS.NU_CHECKLIST
                   WHERE NU_SERVICO = " . $this->mNU_SERVICO . "
                ";
                $exe = mysql_query($sql);
                $count = mysql_num_rows($exe);
                $item = '';
                if ($count > 0) {
                    while ($r = mysql_fetch_assoc($exe)) {
                        $item .= '
                     <li>
                        <a href="/LIB/checklist.php?ID_SOLICITA_VISTO=' . $this->mID_SOLICITA_VISTO . '&SERVICO=' . $this->mNU_SERVICO . '&CHECKLIST=' . $r['NU_CHECKLIST'] . '">
                         ' . $r['NO_CHECKLIST'] .
                                '</a>
                     </li>';
                    }

                    if ($item <> '') {

                        $checklists = '<li><img src="../imagens/print.gif"/> <u>C</u>hecklist (' . $count . ') <ul>' . $item . '</ul></li>';
                    }
                }
            }
        }


        $ordemservico = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_OrdemServico)) {
            $ordemservico = '
            <li>
               <a href="../LIB/OS_rel.php?ID_SOLICITA_VISTO=' . $this->mID_SOLICITA_VISTO . '" target="_blank">
               <img src="../imagens/icons/printer.png"/> Ordem de Serviço
               </a>
            </li>
            ';
        }


        $NC = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_NaoConformidade)) {
            $NC = '
                <li>
        			<form method="post">
        				<input type="hidden" id="naoConformidade" name="naoConformidade" value="1"/>
                        ' . $imgNC . '
        				<input type="submit" value="NC" title="' . $this->labelNC() . '" style="border:1px solid #c0c0c0;padding:0px;margin:0px;font-size:8pt;' . $style . '" />
        			</form>
                </li>';
        }


        $btn_status_inicial = '<li><a href="#">Retornar ao status inicial</a></li>';

        $acao = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_Acao)) {
            $acao = '
                <li>
                    <img src="/imagens/icons/application_cascade.png" />
                    <u>A</u>ção
                    <ul>
                        ' . $btn_abrir_os . '
                        ' . $btn_excluir_os . '
                        ' . $gerar_email . '
                        ' . $btn_status_inicial . '
                    </ul>
                </li>
            ';
        }

        $menu = '<div style="float: right;">
                <ul id="menubar" class="menubar">
                    <li>
                        <a href="../intranet/geral/os_listar.php"> <img src="/imagens/icons/layout_content.png" /> Listar OS </a>
                    </li>
                     ' . $acao . '
                     ' . $checklists . '
                     ' . $ordemservico . '
                     ' . $NC . '
                </ul>
            </div>';

        return $menu;
    }

    /*
     * Verifica chave informada para a pagina
     */

    public function IdInformado($pPOST, $pGET) {
        $msg = '';
        if (isset($pGET['ID_SOLICITA_VISTO'])) {
            $this->Recuperar($pGET['ID_SOLICITA_VISTO']);
        } else {
            if (isset($pPOST['ID_SOLICITA_VISTO'])) {
                $this->Recuperar($pPOST['ID_SOLICITA_VISTO']);
            } else {
                if (isset($pPOST['id_solicita_visto'])) {
                    $this->Recuperar($pPOST['id_solicita_visto']);
                } else {
                    $msg = "jAlert('Identificação da solicitação não informada.' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                }
            }
        }
        return $msg;
    }

    /*
     * Controla a exibição das abas da OS
     */

    function aba($pId, $pPagina) {

        $qstring = '&ID_SOLICITA_VISTO=' . $this->mID_SOLICITA_VISTO . '&id_solicita_visto=' . $this->mID_SOLICITA_VISTO;
        $paginas = array('/operador/os_DadosOS.php?',
            '/operador/os_DadosPessoais.php?',
            '/operador/DadosBasicosCandidato.php?',
            '/operador/os_Cadastro.php?',
            '/operador/os_Formularios.php?',
            '/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso',
            '/operador/os_Liberacao.php?',
            '/paginas/carregueComMenu.php?controller=cCTRL_FORM_TVS&metodo=Edite',
            '/paginas/carregue.php?controller=cCTRL_DESPESAS_OS&metodo=getListar',
            '/paginas/carregueComMenu.php?controller=cCTRL_OS_ABA_COBR_EDIT&metodo=Edite'
        );
        $titulos = array(
            'Dados OS',
            'Dados Pessoais',
            'Cadastro Básico',
            'Cadastro',
            'Formulários',
            'Processo',
            'Qualidade & Liberação',
            'Formulário TVS',
            'Despesas adicionais',
            'Financeiro'
        );
        $visivel = array(true, false, false, true, true, true, false, false, true, true);
        /**
         * Configurações das Abas
         * */
        if ($this->mID_SOLICITA_VISTO == 0) {
            $visivel[1] = false;
            $visivel[2] = false;
            $visivel[3] = false;
            $visivel[4] = false;
            $visivel[5] = false;
            $visivel[6] = false;
            $visivel[7] = false;
            $visivel[8] = false;
            $visivel[9] = false;
        } else {
            if ($this->mID_TIPO_ACOMPANHAMENTO != 1) {
                $visivel[2] = false;
            }
            if ($this->mNU_FORMULARIOS > 0 || $this->mNU_SERVICO == '') {

            } else {
                $visivel[4] = false;
            }
            if ($this->mCandidatos == "") {
                $visivel[1] = false;
                $visivel[2] = false;
                $visivel[3] = false;
            }
        }

        if ($this->mID_SOLICITA_VISTO > 0 && $this->mNU_SERVICO > 0) {
            $serv = new cSERVICO();
            $serv->RecuperePeloId($this->mNU_SERVICO);
            if ($serv->mserv_fl_form_tvs) {
                $visivel[7] = true;
            }
        } else {
            $visivel[7] = false;
        }
        //
        //       if ($this->mNU_FORMULARIOS == 0 || $this->mNU_SERVICO!=''){
        //           unset($pages[4]);  // Formulários
        //       }
        //       if (($this->mID_TIPO_ACOMPANHAMENTO!=1 && $this->mID_STATUS_SOL < 4 )|| $this->mID_TIPO_ACOMPANHAMENTO ==1){
        //           unset($pages[5]);  // Processo
        //       }

        $li = null;
        for ($i = 0; $i < count($paginas); $i++) {
            if ($visivel[$i]) {
                if (substr($paginas[$i], 0, strlen($pPagina)) == $pPagina) {
                    $li .= '<li><a href="#' . $pId . '" class="ativo">' . $titulos[$i] . '</a></li>';
                } else {
                    $li .= '<li><a href="' . $paginas[$i] . $qstring . '">' . $titulos[$i] . '</a></li>';
                }
            }
        }

        return '<ul>' . $li . '</ul>';
    }

    function Titulo() {
        if ($this->mID_SOLICITA_VISTO == 0) {
            $numero = "(nova)";
        } else {
            $numero = $this->mNU_SOLICITACAO . "&nbsp;&nbsp;(" . $this->mNO_STATUS_SOL . ")";
        }
        return "Ordem de serviço " . $numero;
    }

    /**
     * Monta a aba de Liberacao
     */
    function AbaLiberacao() {
        $x = '<div id="Liberacao" style="padding-right:10px;padding-left:10px;">';
        /*
         * Painel de Qualidade
         */
        // Carrega configuração
        $nomeTela = 'qualidade';
        $tela = new cEDICAO($nomeTela);
        $tela->CarregarConfiguracao();
        //
        // Preenche dados existentes
        $tela->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;
        $tela->RecupereRegistro();
        //
        // Ajusta exibição de acordo com situação do registro
        if ($tela->mCampoNome['fl_qualidade_fechada']->mValor == '1') {
            $tela->BloqueieCamposVisiveis();
        } else {
            $tela->mAcoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar", cINTERFACE::ACAO_SALVAR, "Clique para salvar as alterações");
            $tela->EscondaCampo('nm_usuario_qualidade');
        }
        //
        // Renderiza o form
        /*
         * TODO Revisar a utilização da classe ACAO
         */
        $x .= '<form id="' . $nomeTela . '" method="post">';
        $x .= cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio);
        $x .= cINTERFACE::inputHidden(cINTERFACE::PREFIXO_CAMPOS . $nomeTela, $nomeTela);
        $x .= cINTERFACE::inputHidden(cINTERFACE::PREFIXO_CAMPOS . 'acao', cINTERFACE::ACAO_SALVAR);
        $x .= cINTERFACE::RenderizeEdicaoComPaineis($tela);
        $x .= '	</table>';
        $x .= '</form>';
        //
        // Renderiza ações
        if ($tela->mCampoNome['fl_qualidade_conforme']->mValor != '1') {
            $x.= cINTERFACE::RenderizeAcoes($tela);
        }
        $x.= "<br/>";
        /* =====================================================================
         * Painel de LIBERACAO
         */
        // Carrega configuração
        $nomeTela = 'liberacao';
        $tela = new cEDICAO($nomeTela);
        $tela->CarregarConfiguracao();
        //
        // Preenche dados existentes
        $tela->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;
        $tela->RecupereRegistro();
        //
        // Ajusta exibição de acordo com situação do registro
        if ($tela->mCampoNome['fl_liberacao_fechada']->mValor == '1') {
            $tela->BloqueieCamposVisiveis();
        } else {
            $msgConfirmacao = '';
            if ($tela->mCampoNome['fl_conclusao_conforme']->mValor == '0') {
                $msgConfirmacao = 'Você confirma a liberação da OS não conforme?';
            }
            $tela->mAcoes[] = new cACAO(cACAO::ACAO_FORM, "Salvar", cINTERFACE::ACAO_SALVAR, "Clique para salvar as alterações", $msgConfirmacao, "Liberação de OS");
            $tela->EscondaCampo('nm_usuario_liberacao');
        }
        //
        // Renderiza o form
        $x .= '<form id="' . $nomeTela . '" method="post">';
        $x .= cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio);
        $x .= cINTERFACE::inputHidden(cINTERFACE::PREFIXO_CAMPOS . $nomeTela, $nomeTela);
        $x .= cINTERFACE::inputHidden(cINTERFACE::PREFIXO_CAMPOS . 'acao', cINTERFACE::ACAO_SALVAR);
        $x .= cINTERFACE::RenderizeEdicaoComPaineis($tela);
        $x .= '	</table>';
        $x .= '</form>';
        //
        // Renderiza ações
        if ($tela->mCampoNome['fl_liberacao_fechada']->mValor != '1') {
            $x.= cINTERFACE::RenderizeAcoes($tela);
        }


        $x .= '</div>';

        return $x;
    }

    protected function TemplatePainel() {
        /*
         * Use esse template para codificar a montagem de um novo painel
         */
        /*
         * Carregar configurações
         */
        $nomeTela = 'nome_do_painel';    // Nome da tela "Edicao.no_edicao"
        $tela = new cEDICAO($nomeTela);
        $tela->CarregarConfiguracao();
        /*
         * Preencher com dados
         */
        $tela->mCampoNome['nome_do_campo_chave']->mValor = $this->m; //Pode ser um campo da OS ou outro valor qualquer
        $tela->RecupereRegistro();
        /*
         * Ajuste fino de exibição
         */
        if ($tela->mCampoNome['xxx']->mValor != '') {
            $tela->BloqueieCamposVisiveis();
        } else {
            $tela->mAcoes[] = new cACAO(cINTERFACE::ACAO_SALVAR, "Salvar"); // Configure as ações disponíveis para o usuário.
            $tela->EscondaCampo('yyyy');
        }
        /*
         * Renderiza o form
         */
        $x .= '<form id="' . $nomeTela . '" method="post">';
        $x .= cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio);
        $x .= cINTERFACE::inputHidden(cINTERFACE::PREFIXO_CAMPOS . 'edicao', $nomeTela);
        $x .= cINTERFACE::inputHidden(cINTERFACE::PREFIXO_CAMPOS . 'acao', cINTERFACE::ACAO_SALVAR);
        $x .= cINTERFACE::RenderizeEdicaoComPaineis($tela);
        $x .= '	</table>';
        $x .= '</form>';
        /*
         * Renderiza ações
         */
        if ($tela->mCampoNome['fl_liberado']->mValor == '') {
            $x.= cINTERFACE::RenderizeAcoes($tela);
        }
        return $x;
    }

    /**
     * Processa o post de algumas telas.
     * @param type $pPOST
     * @return type
     * @deprecated Deve ser alterado para o novo esquema de tratamento centralizado de post de telas.
     */
    function ProcessaPost($pPOST) {
        /*
         * Qualidade
         */
        $nomeTela = 'qualidade';
        if (isset($pPOST[cINTERFACE::PREFIXO_CAMPOS . $nomeTela])) {
            // Tem o nome da tela
            $tela = new cEDICAO($nomeTela);
            $tela->CarregarConfiguracao();
            $tela->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;
            $tela->RecupereRegistro();
            cINTERFACE::RecupereValoresEdicao($tela, $pPOST);
            //-- Marreta para obrigar o salvamento do código do usuario.
            $tela->mCampoNome['cd_usuario_qualidade']->mValor = cSESSAO::$mcd_usuario;
            $tela->MostreCampo('cd_usuario_qualidade');
            $tela->DesprotejaCampo('cd_usuario_qualidade');
            //-- *
            if ($tela->mAcao == cINTERFACE::ACAO_SALVAR) {
                try {
                    $tela->AtualizarBanco();
                    if ($tela->mCampoNome['fl_conclusao_conforme']->mValor != '') {
                        $sql = "update solicita_visto set fl_qualidade_fechada = 1 where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
                        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
                    }

                    return "jAlert('Dados atualizados com sucesso.');";
                } catch (Exception $exc) {
                    error_log($exc->getMessage() . $exc->getTraceAsString());
                    return "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $this->mNU_SOLICITACAO . "');";
                }
            }
        }
        /*
         * Liberacao
         */
        $nomeTela = 'liberacao';
        if (isset($pPOST[cINTERFACE::PREFIXO_CAMPOS . $nomeTela])) {
            // Tem o nome da tela
            $tela = new cEDICAO($nomeTela);
            $tela->CarregarConfiguracao();
            $tela->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;
            $tela->RecupereRegistro();
            cINTERFACE::RecupereValoresEdicao($tela, $pPOST);
            //-- Marreta para obrigar o salvamento do código do usuario.
            $tela->mCampoNome['cd_usuario_liberacao']->mValor = cSESSAO::$mcd_usuario;
            $tela->MostreCampo('cd_usuario_liberacao');
            $tela->DesprotejaCampo('cd_usuario_liberacao');
            //-- *
            if ($tela->mAcao == cINTERFACE::ACAO_SALVAR) {
                try {
                    $tela->AtualizarBanco();
                    if ($tela->mCampoNome['fl_liberado']->mValor != '') {
                        $sql = "update solicita_visto set fl_liberacao_fechada = 1 where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
                        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
                    }
                    return "jAlert('Dados atualizados com sucesso.');";
                } catch (Exception $exc) {
                    error_log($exc->getMessage() . $exc->getTraceAsString());
                    return "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $this->mNU_SOLICITACAO . "');";
                }
            }
        }
        /*
         * Não conformidade
         */
        if (isset($pPOST['operacao'])) {
            $operacao = $pPOST['operacao'];
            if ($operacao == cORDEMSERVICO::op_InverterNaoConformidade) {
                $this->InvertaNaoConformidade();
                if ($this->mfl_nao_conformidade) {
                    return "jAlert('Não-conformidade indicada com sucesso.');";
                } else {
                    return "jAlert('Não-conformidade removida com sucesso.');";
                }
            }
        }
    }

    function ListarOsProtocolo($pData, $pExcel) {
        print '<table border=0 width=100% class="grid">';
        print '<thead>';
        if ($pExcel == '1') {
            print '<tr><td colspan="10" style="font-size:12pt;font-weight:bold;">Protocolo BSB ' . $pData . '</td></tr>';
        }
        print '
			 <tr align="center" height=20>
			  <th style="text-align:left">OS</th>
			 <th style="text-align:left">Empresa</th>
			  <th style="text-align:left">Candidato</th>
			  <th style="text-align:left">N&ordm; do passaporte</th>
			  <th style="text-align:left">Nacionalidade</th>
			  <th style="text-align:left">Tipo serviço</th>
			  <th style="text-align:left">N&ordm; do processo</th>
			  <th style="text-align:left">Embarcação</th>
			  <th style="text-align:left">Resolução normativa</th>
			  <th style="text-align:left">Repartição consular</th>
			  <th style="text-align:left">Prazo pretendido</th>
			  <th style="text-align:left">Data requerimento</th>
			</tr>
			</thead>
			<tbody>
		';
        $qtd = 0;
        $res = self::RecupereOsProtocolo($pData);
        while ($rw = mysql_fetch_array($res)) {
            $qtd++;
            echo '<tr width="220" height="20" onmouseover="this.style.backgroundColor=\'#dfe4ff\';" onmouseout="this.style.backgroundColor=\'#ffffff\';">' . "\n";
            echo '<td style="padding-right:3px;"><a href="http://dataweb.mundivisas.com.br/operador/os_DadosOS.php?ID_SOLICITA_VISTO=' . $rw['id_solicita_visto'] . '">' . $rw['nu_solicitacao'] . '</a></td>';
            echo '<td>' . $rw['NO_RAZAO_SOCIAL'] . '</td>';
            echo '<td>' . $rw['NOME_COMPLETO'] . '</td>';
            echo '<td>' . $rw['NU_PASSAPORTE'] . '</td>';
            echo '<td>' . $rw['NO_NACIONALIDADE'] . '</td>';
            echo '<td>' . $rw['NO_SERVICO_RESUMIDO'] . '</td>';
            echo '<td>&nbsp;</td>';
            echo '<td>' . $rw['NO_EMBARCACAO_PROJETO'] . '</td>';
            echo '<td>' . $rw['NO_REDUZIDO_TIPO_AUTORIZACAO'] . '</td>';
            echo '<td>' . $rw['NO_REPARTICAO_CONSULAR'] . '</td>';
            echo '<td>' . $rw['DT_PRAZO_ESTADA_SOLICITADO'] . '</td>';
            echo '<td>&nbsp;</td>';
            echo '</tr>' . "\n";
        }
        echo "</tbody></table>";
        echo $qtd . " processos selecionados.";
    }

    function ListarOsProtocoloColeta($pdt_solicitacao_ini, $pdt_solicitacao_fim, $pExcel, $pNU_EMPRESA, $pNU_EMBARCACAO_PROJETO) {
        print '<table border=0 width=100% class="grid">';
        print '<thead>';
        if ($pExcel == '1') {
            print '<tr><td colspan="10" style="font-size:12pt;font-weight:bold;">Protocolo de coleta ' . $pData . '</td></tr>';
        }
        print '
			 <tr align="center" height="109px">
			  <th style="text-align:left">OS</th>
              <th style="text-align:left">Solicitada em</th>
              <th style="text-align:left">Serviço</th>
			  <th style="text-align:left">Empresa<br/>(atual do cand.)</th>
			  <th style="text-align:left">Projeto/Embarca&ccedil;&atilde;o<br/>(atual do cand.)</th>
			  <th style="text-align:left">Candidato</th>
			  <th style="text-align:left;width:94px;">N&ordm; do passaporte</th>
			  <th style="text-align:left">Resolução normativa</th>
			  <th style="text-align:left">N&ordm; do processo MTE</th>
			  <th style="text-align:left;width:90px;">Data do deferimento</th>
			  <th style="text-align:left">Of&iacute;cio MRE</th>
			  <th style="text-align:left;width:79px;">Prazo solicitado</th>
			  <th style="text-align:left">Data da emiss&atilde;o</th>
			  <th style="text-align:left;width:93px;">Candidato, tipo de visto, >prazo, empresa e recebimento do VAF?<br/>Est&aacute; Ok?</th>
			  <th style="text-align:left;width:93px;">Obs</th>
			</tr>
			</thead>
			<tbody>
		';
        $qtd = 0;
        $res = self::RecupereOsProtocoloColeta($pdt_solicitacao_ini, $pdt_solicitacao_fim, $pNU_EMPRESA, $pNU_EMBARCACAO_PROJETO);
        while ($rw = mysql_fetch_array($res)) {
            $qtd++;
            echo '<tr width="220" height="20" onmouseover="this.style.backgroundColor=\'#dfe4ff\';" onmouseout="this.style.backgroundColor=\'#ffffff\';">' . "\n";
            echo '<td style="padding-right:3px;"><a href="http://dataweb.mundivisas.com.br/operador/os_DadosOS.php?ID_SOLICITA_VISTO=' . $rw['id_solicita_visto'] . '">' . $rw['nu_solicitacao'] . '</a></td>';
            echo '<td>' . $rw['dt_solicitacao_fmt'] . '</td>';
            echo '<td>' . $rw['NO_SERVICO_RESUMIDO'] . '</td>';
            echo '<td>' . $rw['NO_RAZAO_SOCIAL'] . '</td>';
            echo '<td>' . $rw['NO_EMBARCACAO_PROJETO'] . '</td>';
            echo '<td>' . $rw['NOME_COMPLETO'] . '</td>';
            echo '<td>&nbsp;' . $rw['NU_PASSAPORTE'] . '</td>';
            echo '<td>' . $rw['NO_REDUZIDO_TIPO_AUTORIZACAO'] . '</td>';
            if ($pExcel != '') {
                $processo = "&nbsp;" . htmlentities($rw['nu_processo']);
            } else {
                $processo = htmlentities($rw['nu_processo']);
            }
            echo '<td>' . $processo . '</td>';
            echo '<td>' . $rw['dt_deferimento_fmt'] . '</td>';
            echo '<td>' . $rw['nu_oficio'] . '</td>';
            echo '<td>' . $rw['DT_PRAZO_ESTADA_SOLICITADO_P'] . '</td>';
            echo '<td>&nbsp;</td>';
            echo '<td>&nbsp;</td>';
            echo '<td>&nbsp;</td>';
            echo '</tr>' . "\n";
        }
        echo "</tbody></table>";
        echo $qtd . " processos selecionados.";
    }

    function ListarChecklistProtocoloColeta($pData, $pNU_EMPRESA, $pNU_EMBARCACAO_PROJETO) {
        print '<table class="grid">';
        print '<thead>';
        $qtd = 0;
        $res = self::RecupereOsProtocoloColeta($pData, $pNU_EMPRESA, $pNU_EMBARCACAO_PROJETO);
        $primeira = true;
        $linhas = '';
        // Matriz de dimensionamento das colunas
        print '<tr class="gabarito">';
        print '<td style="width:44px;">&nbsp;</td>';
        print '<td style="width:44px;">&nbsp;</td>';
        print '<td style="width:90px;">&nbsp;</td>';
        print '<td style="width:41px;">&nbsp;</td>';
        print '<td style="width:70px;">&nbsp;</td>';
        print '<td style="width:94px;">&nbsp;</td>';
        print '<td style="width:51px;">&nbsp;</td>';
        print '<td style="width:55px;">&nbsp;</td>';
        print '<td style="width:60px;">&nbsp;</td>';
        print '<td style="width:85px;">&nbsp;</td>';
        print '<td style="width:93px;">&nbsp;</td>';
        print '<td style="width:83px;">&nbsp;</td>';
        print '<td style="width:95px;">&nbsp;</td>';
        print '</tr>';
        $usuario = '';
        while ($rw = mysql_fetch_array($res)) {
            if ($usuario == '') {
                $usuario = $rw['nome'];
            }
            $qtd++;
            $linhas .= '<tr>';
            $linhas .= '<td style="text-align:center;">' . $rw['nu_solicitacao'] . '</td>';
            $linhas .= '<td colspan="2">' . $rw['NOME_COMPLETO'] . '</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '<td>&nbsp;</td>';
            $linhas .= '</tr>';
            $rw_ok = $rw;
        }
        print '<tr class="titulo"><th colspan="13" style="font-size:10pt;font-weight:bold;text-align:center;">ITM 05 Revis&atilde;o 01  -  ANEXO II - CHECKLIST PARA COLETA DE VISTO POR PROCURADOR</th></tr>';
        print '<tr class="titulo">
					<th colspan="2" style="height:35px">Empresa requerente</th><td colspan="2">' . $rw_ok['NO_RAZAO_SOCIAL'] . '</td>
					<th colspan="2">Projeto/Embarca&ccedil;&atilde;o</th><td colspan="3">' . $rw_ok['NO_EMBARCACAO_PROJETO'] . '</td>
					<th colspan="3">Total de passaportes do PF</th><td>' . $qtd . '</td>
			  </tr>';
        print '<tr class="titulo">
					<th colspan="2" style="height:35px">Reparti&ccedil;&atilde;o consular</th><th colspan="2">' . $rw_ok ['NO_REPARTICAO_CONSULAR'] . '</td>
					<th colspan="2">Data da viagem</th><td colspan="3">' . $pData . '</td>
					<th colspan="3">Funcionamento da RCB ok?</th><td>&nbsp;</td>
			  </tr>';
        print '<tr class="titulo">
					<th colspan="2" style="height:35px">Agente MV</th><td colspan="2">&nbsp;</td>
					<th colspan="2">Ponto focal responsável</th><td colspan="3">' . $usuario . '</td>
					<th colspan="3">Número do checklist (x/y):</th><td>&nbsp;1/1</td>
			  </tr>';
        print '<tr class="titulo"><th colspan="13">&nbsp;</th></tr>';

        print '<tr align="center">
				  <th style="text-align:center;" rowspan="2">OS</th>
				  <th style="text-align:center;" rowspan="2" colspan="2">Candidato</th>
				  <th style="text-align:center;" rowspan="2">Certidão de nasc.?</th>
				  <th style="text-align:center;" colspan="3">VAF</th>
				  <th style="text-align:center;" colspan="3">Passaporte</th>
				  <th style="text-align:center;" rowspan="2">Cópia Seaman\'s book autenticada?</th>
				  <th style="text-align:center;" rowspan="2">Verificação da Police letter ou DCB<br>ok?</th>
				  <th style="text-align:center;" rowspan="2">Obs</th>
				</tr>
			  ';
        print '
			 <tr align="center" height=20>
			  <th style="text-align:center;height:59px;">Preench., assinado e válido?</th>
			  <th style="text-align:center;">Assinatura = passaporte? Núm. pass. = VAF?</th>
			  <th style="text-align:center;">Foto padr&atilde;o da RCB?</th>
			  <th style="text-align:center;">>= 6 meses?</th>
			  <th style="text-align:center;">4 pags em branco?</th>
			  <th style="text-align:center;">Possui assinatura?</th>
			  </tr>
			</thead>
			<tbody>
		';

        echo $linhas;
        print '<tr class="titulo"><th colspan="13">&nbsp;</th></tr>';
        print '<tr class="titulo"><th colspan="13" style="font-size:10pt;font-weight:bold;text-align:left;">Assinatura do PF responsável: _________________________&nbsp;&nbsp;Assinatura do Agente MV: _________________________</th></tr>';

        echo "</tbody></table>";
    }

    function labelNC() {
        if ($this->mfl_nao_conformidade) {
            return "Suspender não conformidade";
        } else {
            return "Indicar não conformidade";
        }
    }

    function estiloNC() {
        if ($this->mfl_nao_conformidade) {
            return "NC_ON";
        } else {
            return "NC_OFF";
        }
    }

    /**
     * Gera cabeçalho da OS com as ações habilitadas
     * @return string HTML pronto do cabeçalho
     */
    public function FormCabecalhoOS() {
        $tela = new cEDICAO("operacoes_os");
        $tela->CarregarConfiguracao();
        $tela->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;
        if ($this->mID_SOLICITA_VISTO != 0) {
            $acoes = $this->AcoesOS();
            $numero = '&nbsp;&nbsp;<span style="font-size:12px;">(' . $this->mNO_STATUS_SOL . ')</span>';
            $this->RecuperarSolicitaVistoCobranca();
        } else {
            $numero = '(nova)';
        }

        $ret = '<div style="clear:both;"></div><div class="titulo">';
        $ret .= '<div class="texto">';
        if ($this->mid_solicita_visto_pacote != 0) {
            $ospacote = new cORDEMSERVICO();
            $ospacote->Recuperar($this->mid_solicita_visto_pacote);
            $pacote = new cSERVICO();
            $pacote->mNU_SERVICO = $ospacote->mnu_servico_pacote;
            $pacote->RecuperePeloId();
            $ret .= '<span style="font-size:10px;">Pacote: <a href="/operador/os_DadosOS.php?&ID_SOLICITA_VISTO=' . $this->mid_solicita_visto_pacote . '&id_solicita_visto=' . $this->mid_solicita_visto_pacote . '" style="text-decoration:underline;" target="_blank">OS ' . $ospacote->mNU_SOLICITACAO . '</a> - ' . $pacote->mNO_SERVICO . ' <i class="icon-caret-down""></i><br/></span>';
        }
        $ret .= 'Ordem de serviço ' . $this->mNU_SOLICITACAO . $numero . '</div>';
        $ret .= '<div style="float:right;margin-left:10px;margin-top:4px;">';
        $ret .= cINTERFACE::formEdicao($tela);
        $ret .= $acoes;
        $ret .= "</form>";
        $ret .= '</div>';
        $ret .= '<div style="clear:both;"></div>';
        $ret .= '</div>';
        $ret .= '<div style="background-color:silver; margin:10px; padding: 10px;">';
        $statusCobranca = cstatus_cobranca_os::situacaoProgresso($this->mstco_id);
        foreach ($statusCobranca as $stco) {
            if ($stco['ok']) {
                $ret .= '<div style="display:inline;padding:5px; margin-right:5px; border: solid 2px #dadada; background-color:white; "><i class="icon-ok"></i> ' . $stco['stco_tx_nome'] . ' </div>';
            } else {
                $ret .= '<div style="display:inline;padding:5px; margin-right:5px; border:dashed 2px #dadada; color:#dadada;"> ' . $stco['stco_tx_nome'] . ' </div>';
            }
        }
        $ret .= '</div>';

        return $ret;
    }

    public function AcoesOS() {
        // BOTÃO ABRIR OS
        $btn_abrir_os = '';
        if ($this->mReadonly) { // mostrar somente se for ReadOnly
            if (Acesso::permitido(Acesso::tp_Os_Menu_Acao_Abrir)) {
                $btn_abrir_os = '<input type="button" value="Abrir OS" title="Clique para abrir a OS ' . $this->mNU_SOLICITACAO . '" class="acaoMenu" onClick="javascript:ConfirmaAcao(this, \'Abrir_OS\', \'\', \'\');"/>';
                $btn_abrir_os = '<li><img src="../imagens/open.png" /> ' . $btn_abrir_os . '</li>';
            }
        }

        $btn_abrir_qualidade = '';
        if ($this->mfl_liberacao_fechada || $this->mfl_qualidade_fechada) { // mostrar somente se for ReadOnly
            if (Acesso::permitido(Acesso::tp_Os_Menu_Acao_Abrir)) {
                $btn_abrir_qualidade = '<input type="button" value="Abrir qualidade" title="Clique para abrir a aba de controle de qualidade OS ' . $this->mNU_SOLICITACAO . '" class="acaoMenu" onClick="javascript:ConfirmaAcao(this, \'Abrir_Qualidade\', \'\', \'\');"/>';
                $btn_abrir_qualidade = '<li><img src="../imagens/open.png" /> ' . $btn_abrir_qualidade . '</li>';
            }
        }

        // BOTÃO EXCLUIR OS
        $btn_excluir_os = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_Acao_Excluir)) {
            if ($this->mNU_SOLICITACAO <> 0) {
                // Redirect cancelado pois foi alterado para ser tratado pelo post.
                //$btn_excluir_os ='<input type="button" value="Excluir OS" onClick="ConfirmaRedirect(\'<b>Você confirma a exclusão dessa OS?<br/>Todos as informações sobre esse processo registradas nos candidatos serão perdidas.<br/>Os arquivos dos candidatos não serão afetados.<br/><br/><center><i>(ATENÇÃO: Essa operação não pode ser desfeita)</i></center></b>\', \'Excluir OS\', \'/LIB/OS_EXCLUIR.php?id_solicita_visto='.$this->mID_SOLICITA_VISTO.'\');" title="Clique para excluir essa OS" class="acaoMenu"/>';
                $btn_excluir_os = '<input type="button" value="Excluir OS" onClick="ConfirmaRedirect(\'<b>Você confirma a exclusão dessa OS?<br/>Todos as informações sobre esse processo registradas nos candidatos serão perdidas.<br/>Os arquivos dos candidatos não serão afetados.<br/><br/><center><i>(ATENÇÃO: Essa operação não pode ser desfeita)</i></center></b>\', \'Excluir OS\', \'/paginas/carregue.php?controller=cCTRL_ORDEMSERVICO&metodo=excluir&id_solicita_visto='.$this->mID_SOLICITA_VISTO.'\');" title="Clique para excluir essa OS" class="acaoMenu"/>';
                $btn_excluir_os = '<li><img src="../imagens/delete.gif" /> ' . $btn_excluir_os . '</li>';
            }
        }


        $gerar_email = '<li><img src="../imagens/icons/email_go.png" /><a href="GeradorEmail.php?ID_SOLICITA_VISTO=' . $this->mID_SOLICITA_VISTO . '"> E-mail de confirmação</a></li>';


        $iten_checklist = '';
        if ($nu_servico == 1) {
            $iten_checklist = '<li>
              <a href="../LIB/OS_RELATORIOS.php?ID_SOLICITA_VISTO=' . $this->mID_SOLICITA_VISTO . '" target="_blank">
              <img src="../imagens/print.gif"/> CheckList
              </a>
          </li>';
        }


        $checklists = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_Checklist)) {
            if ($this->mNU_SERVICO <> '') {
                $sql = "
              SELECT C.NU_CHECKLIST, CS.NU_SERVICO, C.NO_CHECKLIST
                FROM CHECKLIST_SERVICO CS
          INNER JOIN CHECKLIST C
                  ON C.NU_CHECKLIST = CS.NU_CHECKLIST
               WHERE NU_SERVICO = " . $this->mNU_SERVICO . "
            ";
                $exe = mysql_query($sql);
                $count = mysql_num_rows($exe);
                $item = '';
                if ($count > 0) {
                    while ($r = mysql_fetch_assoc($exe)) {
                        $item .= '
                 <li>
                    <a href="/LIB/checklist.php?ID_SOLICITA_VISTO=' . $this->mID_SOLICITA_VISTO . '&SERVICO=' . $this->mNU_SERVICO . '&CHECKLIST=' . $r['NU_CHECKLIST'] . '">
                     ' . $r['NO_CHECKLIST'] .
                                '</a>
                 </li>';
                    }

                    if ($item <> '') {
                        $checklists = '<li><img src="../imagens/print.gif"/> <u>C</u>hecklist (' . $count . ') <ul>' . $item . '</ul></li>';
                    }
                }
            }
        }


        $ordemservico = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_OrdemServico)) {
            $ordemservico = '
        <li>
           <a href="../LIB/OS_rel.php?ID_SOLICITA_VISTO=' . $this->mID_SOLICITA_VISTO . '" target="_blank">
           <img src="../imagens/icons/printer.png"/> Ordem de Serviço
           </a>
        </li>
        ';
        }


        if ($this->estiloNC[$this->mfl_nao_conformidade] == 'NC_ON') {
            $style = 'background:red;color:#fff';
            $imgNC = '';
        } else {
            $imgNC = '<img src="../imagens/action_encerrado.png">';
            $style = '';
        }
        $NC = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_NaoConformidade)) {
            $NC = '
            <li>
    			<form method="post">
    				<input type="hidden" id="naoConformidade" name="naoConformidade" value="1"/>
                    ' . $imgNC . '
    				<input type="button" value="NC" title="' . $this->labelNC[$this->mfl_nao_conformidade] . '" style="border:1px solid #c0c0c0;padding:0px;margin:0px;font-size:8pt;' . $style . '" onClick="javascript:ConfirmaAcao(this, \'Inverter_NC\', \'\', \'\');"/>
    			</form>
            </li>';
        }

        $btn_status_inicial = '<li><a href="#" onclick="AcionaProcesso(this);return false;" data-parameters="{&quot;controller&quot;: &quot;cCTRL_OS_STATUS&quot;, &quot;metodo&quot;: &quot;RetornarAoStatusInicial&quot;, &quot;id_solicita_visto&quot;: &quot;' . $this->mID_SOLICITA_VISTO . '&quot;, &quot;cd_usuario&quot;: &quot;' . cSESSAO::$mcd_usuario . '&quot;}">Retornar ao status inicial</a></li>';

        $acao = '';
        if (Acesso::permitido(Acesso::tp_Os_Menu_Acao)) {
            $acao = '
            <li>
                <img src="/imagens/icons/application_cascade.png" />
                <u>A</u>ção
                <ul>
                    ' . $btn_abrir_os . '
                    ' . $btn_abrir_qualidade . '
                    ' . $btn_excluir_os . '
                    ' . $gerar_email . '
                    ' . $btn_status_inicial . '
                </ul>
            </li>
        ';
        }

        $menu = '<ul id="menubar" class="menubar">
        <li>
            <a href="../intranet/geral/os_listar.php"> <img src="/imagens/icons/layout_content.png" /> Listar OS </a>
        </li>
         ' . $acao . '
         ' . $checklists . '
         ' . $ordemservico . '
         ' . $NC . '
    </ul>
         ';

        return $menu;
    }

    public static function PostOPERACOES_OS($pTela, $pPOST, $pFILES, &$pSESSION) {
        $OS = new cORDEMSERVICO();
        $OS->Recuperar($pTela->mCampoNome['id_solicita_visto']->mValor);
        switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
            case "Abrir_OS":
                $msg = $OS->Abrir();
                return $msg;
                break;
            case "Abrir_Qualidade":
                $msg = $OS->AbrirQualidade();
                return $msg;
                break;
            // case "Excluir_OS":
            //     $OS->Excluir();
            //     $pSESSION['msg'] = 'Ordem de serviço ' . $OS->mNU_SOLICITACAO . ' excluída com sucesso.';
            //     header("Location:/intranet/geral/os_listar.php");
            //     break;
            case "Inverter_NC":
                $OS->InvertaNaoConformidade();
                return 'Status de não conformidade atualizado com sucesso.';
                break;
        }
    }

    public static function Excluir_OS($pTela) {
        $OS = new cORDEMSERVICO();
        $OS->Recuperar($pTela->mCampoNome['id_solicita_visto']);
        return $OS->Excluir();
    }

    /*
     * Obtem os parametros necessários para a execução dos serviços
     */

    public function ObterParametros($pPOST, $pGET) {
        if (isset($pGET['ID_SOLICITA_VISTO'])) {
            $this->mID_SOLICITA_VISTO = $pGET['ID_SOLICITA_VISTO'];
        } else {
            if (isset($pPOST['ID_SOLICITA_VISTO'])) {
                $this->mID_SOLICITA_VISTO = $pPOST['ID_SOLICITA_VISTO'];
            } else {
                $this->mID_SOLICITA_VISTO = "";
            }
        }


        if (isset($pGET['NU_CANDIDATO'])) {
            $this->mNU_CANDIDATO_visivel = $pGET['NU_CANDIDATO'];
        } else {
            if (isset($pPOST['NU_CANDIDATO'])) {
                $this->mNU_CANDIDATO_visivel = $pPOST['NU_CANDIDATO'];
            } else {
                $this->mNU_CANDIDATO_visivel = "";
            }
        }

        // Verifica se existe uma OS de referencia para a criação de uma nova.

        if ($this->mID_SOLICITA_VISTO > 0) {
            $this->Recuperar();
            $this->RecuperarCandidatos();
        } else {
            $this->mID_STATUS_SOL = 0;
            $this->mID_SOLICITA_VISTO = 0;
            if (isset($pGET['OS_REF'])) {
                $this->mOS_REF = $pGET['OS_REF'];
            } else {
                $this->mOS_REF = '';
            }
        }
    }

    public function FormDadosOS() {
        if ($this->mReadonly) {
            $nomeTela = 'SOLICITA_VISTO_view';
        } else {
            $nomeTela = 'SOLICITA_VISTO_edit';
        }
        $tela = new cEDICAO($nomeTela);
        $tela->CarregarConfiguracao();
        $tela->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;

        if ($this->mID_SOLICITA_VISTO == 0) {
            $tela->mCampoNome['NU_EMPRESA']->mVisivel = 1;
            $tela->mCampoNome['NU_EMPRESA']->mReadonly = 0;
            $tela->mCampoNome['NO_RAZAO_SOCIAL']->mVisivel = 0;

            // Tratamento do RádioButton
            if ($this->mOS_REF != '') {
                $this->Recuperar($this->mOS_REF);
                $tela->mCampoNome['id_solicita_visto']->mValor = $this->mOS_REF;
                $tela->mCampoNome['mNU_SERVICO']->mValor = $this->mNU_SERVICO;
                $tela->mCampoNome['mno_solicitador']->mValor = $this->mno_solicitador;
                $tela->mCampoNome['mdt_solicitacao']->mValor = $this->mdt_solicitacao;
                $tela->mCampoNome['mNU_EMBARCACAO_PROJETO']->mValor = $this->mNU_EMBARCACAO_PROJETO;
                $tela->mCampoNome['mcd_tecnico']->mValor = $this->mcd_tecnico;
                $tela->mCampoNome['mde_observacao']->mValor = $this->mde_observacao;
                $tela->mCampoNome['mNU_USUARIO_CAD']->mValor = $this->mNU_USUARIO_CAD;
                $tela->mCampoNome['mDT_PRAZO_ESTADA_SOLICITADO']->mValor = $this->mDT_PRAZO_ESTADA_SOLICITADO;
                $tela->mCampoNome['mnu_empresa_requerente']->mValor = $this->mnu_empresa_requerente;
                $tela->RecupereRegistro();
                $tela->mCampoNome['id_solicita_visto']->mValor = 0;
                $this->mID_SOLICITA_VISTO = 0;
            }
        } else {
            $tela->RecupereRegistro();
        }
        return $tela;
    }

    public static function PostSOLICITA_VISTO_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        $OrdemServico = new cORDEMSERVICO();
        $OrdemServico->mID_SOLICITA_VISTO = $pTela->mCampoNome['id_solicita_visto']->mValor;
        $ID_ANTES = $OrdemServico->mID_SOLICITA_VISTO;
        if ($OrdemServico->mID_SOLICITA_VISTO > 0) {
            $OrdemServico->RecuperarSolicitaVisto();
        } else {
            $OrdemServico->mNU_SERVICO = $pTela->mCampoNome['NU_SERVICO']->mValor;
        }
        $OrdemServico->mNU_EMPRESA = $pTela->mCampoNome['NU_EMPRESA']->mValor;
        $OrdemServico->mno_solicitador = $pTela->mCampoNome['no_solicitador']->mValor;
        $OrdemServico->mdt_solicitacao = $pTela->mCampoNome['dt_solicitacao']->mValor;
        $OrdemServico->mNU_EMBARCACAO_PROJETO = $pTela->mCampoNome['NU_EMBARCACAO_PROJETO']->mValor;
        $OrdemServico->mcd_tecnico = $pTela->mCampoNome['cd_tecnico']->mValor;
        $OrdemServico->mde_observacao = $pTela->mCampoNome['de_observacao']->mValor;
        $OrdemServico->mNU_USUARIO_CAD = cSESSAO::$mcd_usuario;
        $OrdemServico->mDT_PRAZO_ESTADA_SOLICITADO = $pTela->mCampoNome['DT_PRAZO_ESTADA_SOLICITADO']->mValor;
        $OrdemServico->mnu_empresa_requerente = $pTela->mCampoNome['nu_empresa_requerente']->mValor;
        $OrdemServico->mCandidatos = $pPOST['DadosOS_CANDIDATO_chaves'];
        $OrdemServico->mNU_EMBARCACAO_PROJETO_COBRANCA = $pTela->mCampoNome['NU_EMBARCACAO_PROJETO_COBRANCA']->mValor;
        $OrdemServico->mtppr_id = $pTela->mCampoNome['tppr_id']->mValor;

        try {
            $OrdemServico->SalvarInstancia();
            $qtdArquivos = $pPOST['qtdArquivos'];
            $i = 1;
            while ($i <= $qtdArquivos) {
                if (isset($pFILES['presoFile' . $i]['name']) && $pFILES['presoFile' . $i]['name'] != '') {
                    $x = cARQUIVO::CriarArquivoSolicitacao($pFILES['presoFile' . $i], $OrdemServico->mNU_EMPRESA, $OrdemServico->mID_SOLICITA_VISTO, 18, cSESSAO::$mcd_usuario);
                }
                $i++;
            }
            if ($ID_ANTES == 0) {
                $mensagem = "OS criada com sucesso.";
                if ($pPOST['continuacao'] == '1') { //Continuar
                    cHTTP::$SESSION['msg'] = $mensagem;
                    cHTTP::RedirecionaLinkInterno("/operador/os_DadosOS.php?ID_SOLICITA_VISTO=" . $OrdemServico->mID_SOLICITA_VISTO);
                } elseif ($pPOST['continuacao'] == '2') { //Nova
                    cHTTP::$SESSION['msg'] = $mensagem;
                    cHTTP::RedirecionaLinkInterno("/operador/os_DadosOS.php?ID_SOLICITA_VISTO=0&OS_REF=" . $OrdemServico->mID_SOLICITA_VISTO);
                } else { //nenhum
                    cHTTP::$SESSION['msg'] = $mensagem;
                    cHTTP::RedirecionaLinkInterno("/operador/os_DadosOS.php?ID_SOLICITA_VISTO=" . $OrdemServico->mID_SOLICITA_VISTO);
                }
            } else {
                $mensagem = "OS atualizada com sucesso";
            }
        } catch (cERRO_CONSISTENCIA $exc) {
            $mensagem = "Não foi possível salvar a OS pois:" . $exc->mMsg;
        } catch (Exception $exc) {
            $mensagem = "Não foi possível salvar a OS pois:" . addslashes(str_replace("\n", "<br/>", $exc->getMessage() . '<br/>' . $exc->getTraceAsString()));
        }
        return $mensagem;
    }

    public static function PostOS_CANDIDATOS($pTela, $pPOST, $pFILES, &$pSESSION) {
        $OrdemServico = new cORDEMSERVICO();
        $OrdemServico->mID_SOLICITA_VISTO = $pTela->mCampoNome['id_solicita_visto']->mValor;
        if ($OrdemServico->mID_SOLICITA_VISTO > 0) {
            $OrdemServico->ExcluirCandidato($pTela->mCampoNome['NU_CANDIDATO']->mValor);
        }
        return 'Candidato excluído com sucesso!';
    }

    public function FormProcesso() {
        if ($this->mNU_SERVICO == '') {
            return $this->FormSOLICITACAO_ANTIGA();
        } else {
            switch ($this->mID_TIPO_ACOMPANHAMENTO) {
                case 1:    //processo_mte
                case 2:    //processo_mte
                    return $this->FormProcessoMte();
                    break;
                case 3:    //Prorrogação
                    $nomeTela = 'processo_prorrog_edit';
                    break;
                case 4:    //Registro CIE
                    $nomeTela = 'processo_regcie_edit';
                    break;
                case 5:    //Emissão CIE
                    $nomeTela = 'processo_emiscie_edit';
                    break;
                case 6:    //Cancelamento
                    $nomeTela = 'processo_cancel_edit';
                    break;
                case 8:    //Coleta
                    $nomeTela = 'processo_coleta_edit';
                    break;
                case 9:    //CPF
                    $nomeTela = 'CANDIDATO_CPF';
                    break;
                case 10:   //CNH
                    $nomeTela = 'CANDIDATO_CNH';
                    break;
                case 11:   //CTPS
                    $nomeTela = 'CANDIDATO_CTPS';
                    break;
                case 11:   //CTPS
                    $nomeTela = 'processo_coleta_cie_edit';
                    break;
                default:
                    $nomeTela = 'processo_edit';
                    break;
            }
            $acao = array();
            $tela = new cEDICAO($nomeTela);
            $tela->CarregarConfiguracao();

            $tela->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;
            $tela->ResseteChavePara('id_solicita_visto');
            if ($this->mReadonly) {
                $tela->ProtejaTodosCampos();
            } else {
                $acao[] = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Clique para salvar as alterações");
                $acao[] = new cACAO(cACAO::ACAO_FORM, "Salvar e Fechar a OS", "SALVAR_E_FECHAR", "Clique para salvar as alterações e fechar a OS");
            }
            $tela->RecupereRegistro();
            //var_dump($tela->mCampo);
            $ret = '';
            $titulo = cINTERFACE_PROCESSO::$mTitulos[$nomeTela];
            if (($titulo == 'Registro') && ($this->mNU_SERVICO == cSERVICO::RESTABELECIMENTO)) {
                $titulo = 'Restabelecimento';
            }
            if ($this->mNU_SERVICO == '22') {
                $titulo = 'Coleta CIE';
                $tela->mCampoNome['nu_protocolo']->mClasse = '';
            }
            if ($titulo != '') {
                $ret .= '<div class="ui-widget ui-widget-header tituloProcesso">' . $titulo . '</div>';
            }

            $ret .= cINTERFACE::formEdicao($tela);
            $ret .= cINTERFACE::RenderizeEdicaoComPaineis($tela);
            $ret .= '</table>' . "\n";
            if (count($acao) > 0) {
                $ret .= '<div style="margin-top:10px;text-align:center">';
                $espaco = "";
                foreach ($acao as $xacao) {
                    $ret .= $espaco . cINTERFACE::RenderizeAcao($xacao);
                    $espaco = "&nbsp;";
                }
                $ret .= '</div>';
            }
            $ret .= '</form>' . "\n";
            return $ret;
        }
    }

    /**
     *
     * Processa o formulário de processo mte na OS.
     */
    public function FormProcessoMte() {
        $acao = array();
        if ($this->mID_STATUS_SOL <= 4) {
            $nomeTela = 'PROCESSO_MTE';
            $acao[] = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Clique para salvar as alterações");
            $acao[] = new cACAO(cACAO::ACAO_FORM, "Concluir protocolo", "OS_CONCLUIR_PROTOCOLO", "Clique para indicar a conclusão do protocolo");
        } elseif ($this->mID_STATUS_SOL == 5) {
            $nomeTela = 'ANDAMENTO_MTE';
            $acao[] = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Clique para salvar as alterações");
            $acao[] = new cACAO(cACAO::ACAO_FORM, "Concluir andamento", "OS_ENCERRAR", "Clique para indicar a conclusão do andamento do processo");
            if ($this->mfl_cobrado == 0) {
                $acao[] = new cACAO(cACAO::ACAO_FORM, "Cobrar", "OS_COBRAR", "Clique para indicar que a OS foi cobrada");
            }
        } elseif ($this->mID_STATUS_SOL == 6) {
            $nomeTela = 'CONCLUSAO_MTE';
            $acao[] = new cACAO(cACAO::ACAO_FORM, "Salvar", "SALVAR", "Clique para salvar as alterações");
            if ($this->mfl_cobrado == 0) {
                $acao[] = new cACAO(cACAO::ACAO_FORM, "Cobrar", "OS_COBRAR", "Clique para indicar que a OS foi cobrada");
            }
        }
        $tela = new cEDICAO($nomeTela);
        $tela->CarregarConfiguracao();
        $tela->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;
        $tela->ResseteChavePara("id_solicita_visto");
        $tela->RecupereRegistro();
        $ret = cINTERFACE::formEdicao($tela);
        $ret .= cINTERFACE::RenderizeEdicaoComPaineis($tela);
        $ret .= '</table>' . "\n";
        if (count($acao) > 0) {
            $ret .= '<div style="text-align:center">';
            $espaco = "";
            foreach ($acao as $xacao) {
                $ret .= $espaco . cINTERFACE::RenderizeAcao($xacao);
                $espaco = "&nbsp;";
            }
            $ret .= '</div>';
        }
        $ret .= '</form>' . "\n";
        return $ret;
    }

    public function FormSOLICITACAO_ANTIGA() {
        $ret = '<b>ATENÇÃO: Para fazer alterações e atualizações nos processos antigos, vá diretamente ao perfil do candidato.</b>';
        //print cINTERFACE_OS::Processo_Antigas($OrdemServico);
        /*
          if($this->mReadonly){
          $nomeTela = 'SOLICITACAO_ANTIGA_view';
          }
          else{
          $nomeTela = 'SOLICITACAO_ANTIGA_edit';
          }
          Tela de edição bloqueada. Toda manutenção deve ser feita pelo candidato
         */
        $nomeTela = 'SOLICITACAO_ANTIGA_view';
        $tabs = '';
        $this->RecuperarCandidatos();
        $candidatos = split(",", $this->mCandidatos);
        if ($this->mNU_SOLICITACAO > 0) { // Se for OS 0 não pode carregar todos os candidatos!!!!
            for ($i = 0; $i < count($candidatos); $i++) {
                $OS_ANTIGA = new cEDICAO($nomeTela);
                $OS_ANTIGA->CarregarConfiguracao();
                $OS_ANTIGA->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;
                $OS_ANTIGA->mCampoNome['id_solicita_visto']->mValor = $this->mID_SOLICITA_VISTO;
                $OS_ANTIGA->mCampoNome['NU_CANDIDATO']->mValor = $candidatos[$i];
                $OS_ANTIGA->RecupereRegistro();

                $tabs .= '<li><a href="#processo' . $OS_ANTIGA->mCampoNome['NU_CANDIDATO']->mValor . '">' . $OS_ANTIGA->mCampoNome['NOME_COMPLETO']->mValor . '</a></li>';
                $divs .= '<div id="processo' . $OS_ANTIGA->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
                $divs .= '<form id="formProcesso' . $OS_ANTIGA->mCampoNome['NU_CANDIDATO']->mValor . '" method="post">';
                $dif = 'CAND' . $OS_ANTIGA->mCampoNome["NU_CANDIDATO"]->mValor;
                $divs .= cINTERFACE::inputHidden('IdentificadorCampos', $dif);
                $divs .= cINTERFACE::RenderizeCampo($OS_ANTIGA->mCampoCtrlEnvio);
                $divs .= cINTERFACE::RenderizeCampo($OS_ANTIGA->mCampoNome["id_solicita_visto"], false, $dif);
                $divs .= cINTERFACE::RenderizeCampo($OS_ANTIGA->mCampoNome["NU_CANDIDATO"], false, $dif);
                $divs .= cINTERFACE::RenderizeEdicaoComPaineis($OS_ANTIGA, 2, $dif);
                $divs .= '</table>';
                if (!$this->mReadonly) {
                    $divs .= '<center><input type="submit" value="Salvar" /></center>';
                }
                $divs .= '</form>';
                $divs .= '</div>' . "\n";
            }
        }
        $js .= '$( "#tabsProcessos" ).tabs()' . ";\n";
        $js .= '$( "#tabsProcessos" ).tabs(' . $selecionada . ' )' . ";\n";
        $tabs = '<ul>' . $tabs . '</ul>' . "\n";


        $ret.= '<div id="tabsProcessos">' . "\n";
        $ret.= $tabs;
        $ret.= $divs;
        $ret.= '</div>';
        return $ret;
    }

    public static function PostSOLICITACAO_ANTIGA_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        /*
         * TODO: Fazer a chamada de salvamento para cada processo.
         */
        /*
         * O form SOLICITACAO_ANTIGA_edit foi desativado pois toda a manutenção deverá ser feita diretamente pelo perfil do candidato
         */
        //return self::PostSOLICITA_VISTO($pTela, $pPOST, $pFILES, &$pSESSION);
    }

    /**
     * Trata formulário de alteração de OS sem serviço indicado...
     * @param cEDICAO $pTela	Tela com os parametros informados
     * @param array $pPOST		$_POST
     * @param array $pFILES		$_FILES
     * @param array $pSESSION 	$_SESSION
     */
    public static function PostSOLICITACAO_ANTIGA_view($pTela, $pPOST, $pFILES, &$pSESSION) {
        if (isset($pTela->mCampoNome['pm_observacao']->mValor)) {
            $processo = new cprocesso_mte();
            $processo->mcodigo = $pTela->mCampoNome['codigo_mte']->mValor;
            $processo->mobservacao = $pTela->mCampoNome['pm_observacao']->mValor;
            $processo->AtualizeObs();
        }

        if (isset($pTela->mCampoNome['rc_observacao']->mValor)) {
            $processo = new cprocesso_regcie();
            $processo->mcodigo = $pTela->mCampoNome['codigo_regcie']->mValor;
            $processo->mobservacao = $pTela->mCampoNome['rc_observacao']->mValor;
            $processo->AtualizeObs();
        }

        if (isset($pTela->mCampoNome['pp_observacao']->mValor)) {
            $processo = new cprocesso_prorrog();
            $processo->mcodigo = $pTela->mCampoNome['codigo_prorrog']->mValor;
            $processo->mobservacao = $pTela->mCampoNome['pp_observacao']->mValor;
            $processo->AtualizeObs();
        }

        if (isset($pTela->mCampoNome['ca_observacao']->mValor)) {
            /*
             * TODO: Adicionar a observação do cancelamento na edicao
             */
            $processo = new cprocesso_cancel();
            $processo->mcodigo = $pTela->mCampoNome['codigo_cancel']->mValor;
            $processo->mobservacao = $pTela->mCampoNome['ca_observacao']->mValor;
            $processo->AtualizeObs();
        }
        return "Informações do processo de \'" . $pTela->mCampoNome['NOME_COMPLETO']->mValor . "\' atualizadas com sucesso";
    }

    /**
     *
     * Trata o formulário de processo de CPF
     * @param cEDICAO $pTela Tela com os campos informados
     * @param array $pPOST	$_POST
     * @param array $pFILES	$_FILES
     * @param array $pSESSION $_SESSION
     */
    public static function PostCANDIDATO_CPF($pTela, $pPOST, $pFILES, &$pSESSION) {
        $xcand = new cCANDIDATO();
        $xcand->mNU_CANDIDATO = $pTela->mCampoNome['NU_CANDIDATO']->mValor;
        $xcand->mNU_CPF = $pTela->mCampoNome['NU_CPF']->mValor;
        $xcand->AtualizeCPF();
        $OrdemServico = new cORDEMSERVICO();
        $OrdemServico->mID_SOLICITA_VISTO = $pTela->mCampoNome['id_solicita_visto']->mValor;
        $OrdemServico->mde_observacao = $pTela->mCampoNome['de_observacao']->mValor;
        $OrdemServico->SalvarObservacao();
        if ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor == 'SALVAR_E_FECHAR') {
            $OrdemServico->Fechar();
        }

        return 'CPF atualizado com sucesso.';
    }

    /**
     *
     * Trata o formulário de processo de CNH
     * @param cEDICAO $pTela Tela com os campos informados
     * @param array $pPOST	$_POST
     * @param array $pFILES	$_FILES
     * @param array $pSESSION $_SESSION
     */
    public static function PostCANDIDATO_CNH($pTela, $pPOST, $pFILES, &$pSESSION) {
        $xcand = new cCANDIDATO();
        $xcand->mNU_CANDIDATO = $pTela->mCampoNome['NU_CANDIDATO']->mValor;
        $xcand->mNU_CNH = $pTela->mCampoNome['NU_CNH']->mValor;
        $xcand->mDT_EXPIRACAO_CNH = $pTela->mCampoNome['DT_EXPIRACAO_CNH']->mValor;
        $xcand->AtualizeCNH();
        $OrdemServico = new cORDEMSERVICO();
        $OrdemServico->mID_SOLICITA_VISTO = $pTela->mCampoNome['id_solicita_visto']->mValor;
        $OrdemServico->mde_observacao = $pTela->mCampoNome['de_observacao']->mValor;
        $OrdemServico->SalvarObservacao();
        if ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor == 'SALVAR_E_FECHAR') {
            $OrdemServico->Fechar();
        }
        return 'Dados da CNH atualizada com sucesso.';
    }

    /**
     *
     * Trata o formulário de processo de CTPS
     * @param cEDICAO $pTela Tela com os campos informados
     * @param array $pPOST	$_POST
     * @param array $pFILES	$_FILES
     * @param array $pSESSION $_SESSION
     */
    public static function PostCANDIDATO_CTPS($pTela, $pPOST, $pFILES, &$pSESSION) {
        $xcand = new cCANDIDATO();
        $xcand->mNU_CANDIDATO = $pTela->mCampoNome['NU_CANDIDATO']->mValor;
        $xcand->mNU_CTPS = $pTela->mCampoNome['NU_CTPS']->mValor;
        $xcand->mDT_EXPIRACAO_CTPS = $pTela->mCampoNome['DT_EXPIRACAO_CTPS']->mValor;
        $xcand->AtualizeCTPS();
        $OrdemServico = new cORDEMSERVICO();
        $OrdemServico->mID_SOLICITA_VISTO = $pTela->mCampoNome['id_solicita_visto']->mValor;
        $OrdemServico->mde_observacao = $pTela->mCampoNome['de_observacao']->mValor;
        $OrdemServico->SalvarObservacao();
        if ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor == 'SALVAR_E_FECHAR') {
            $OrdemServico->Fechar();
        }
        return 'Dados da CTPS atualizada com sucesso.';
    }

    /**
     * Processa os formularios de processos sem acompanhamento específico
     * @param type $pTela
     * @param type $pPOST
     * @param type $pFILES
     * @param type $pSESSION
     * @return type
     */
    public static function Postprocesso_edit($pTela, $pPOST, $pFILES, &$pSESSION) {
        switch ($pTela->mCampoNome[cINTERFACE::CMP_ACAO]->mValor) {
            case 'SALVAR':
                $pTela->AtualizarBanco();
                return 'Processo atualizado com sucesso!';
                break;
            case 'SALVAR_E_FECHAR':
                $pTela->AtualizarBanco();
                $OS = new cORDEMSERVICO();
                $OS->mID_SOLICITA_VISTO = $pTela->mCampoNome['id_solicita_visto']->mValor;
                $OS->Fechar();
                return 'Processo atualizado com sucesso! OS fechada com sucesso!';
                break;
        }
    }

    public static function ProtocoloRegistro_Linha($pData_ini, $pData_fim, $pNU_EMPRESA = '', $pexcel) {
        if ($pData_ini == '') {
            throw new Exception('Filtro de data não informado');
            exit;
        }
        $sql = "select sv.id_solicita_visto, E.NO_RAZAO_SOCIAL, C.NOME_COMPLETO";
        $sql .= "    , C.NU_CANDIDATO, EP.NO_EMBARCACAO_PROJETO, date_format(p.dt_requerimento, '%d/%m/%y') dt_requerimento, p.nu_protocolo ";
        $sql .= "    , C.NU_PASSAPORTE , sv.nu_solicitacao, concat(depf_no_nome, ' (', depf_cd_uf, ')') depf_no_nome";
        $sql .= " from solicita_visto sv ";
        $sql .= " JOIN AUTORIZACAO_CANDIDATO AC on AC.id_solicita_visto = sv.id_solicita_visto ";
        $sql .= " JOIN CANDIDATO C				on C.NU_CANDIDATO = AC.NU_CANDIDATO";
        $sql .= " JOIN processo_regcie p		on p.id_solicita_visto = sv.id_solicita_visto and p.cd_candidato = C.nu_candidato";
        $sql .= " JOIN processo_mte pm			on pm.codigo = p.codigo_processo_mte and pm.cd_candidato = C.nu_candidato";
        $sql .= " JOIN EMPRESA E				on E.NU_EMPRESA = sv.NU_EMPRESA";
        $sql .= " JOIN EMBARCACAO_PROJETO EP	on EP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = C.NU_EMPRESA";
        $sql .= " JOIN SERVICO S				on S.NU_SERVICO = sv.NU_SERVICO";
        $sql .= " LEFT JOIN delegacia_pf d			on d.depf_cd_codigo = substr(trim(replace(p.nu_protocolo, '''', '')),1,5)";
        $sql .= " where sv.NU_SERVICO in (58, 20)";
        $sql .= "   and p.dt_requerimento >= " . cBANCO::DataOk($pData_ini);
        $sql .= "   and p.dt_requerimento <= " . cBANCO::DataOk($pData_fim);

        if ($pNU_EMPRESA != '' && $pNU_EMPRESA != '0') {
            $sql .= " and sv.NU_EMPRESA = " . $pNU_EMPRESA;
        }
        $sql .= "   order by dt_requerimento, NO_RAZAO_SOCIAL, NOME_COMPLETO";
        $ret = '';
        $empresa = '';
        if ($res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__)) {
            $i = 0;
            while ($pRS = mysql_fetch_array($res)) {
                $i++;
                if ($empresa == '') {
                    $empresa = $pRS['NO_RAZAO_SOCIAL'];
                }
                $NO_REPARTICAO_CONSULAR = $pRS['NO_REPARTICAO_CONSULAR'];
                $ret .= '<tr width="220" height="20" onmouseover="this.style.backgroundColor=\'#dfe4ff\';" onmouseout="this.style.backgroundColor=\'#ffffff\';">' . "\n";
                $ret .= '<td style="text-align:center;">' . $i . '</td>';
                if ($pexcel == '1') {
                    $ret .= '<td style="padding-right:3px;text-align:center;">' . $pRS['nu_solicitacao'] . '</td>';
                } else {
                    $ret .= '<td style="padding-right:3px;text-align:center;"><a href="/operador/os_DadosOS.php?ID_SOLICITA_VISTO=' . $pRS['id_solicita_visto'] . '" target="_blank">' . $pRS['nu_solicitacao'] . '</a></td>';
                }
                $ret .= '<td>' . $pRS['NO_RAZAO_SOCIAL'] . '</td>';
                $ret .= '<td>' . $pRS['NO_EMBARCACAO_PROJETO'] . '</td>';
                if ($pexcel == '1') {
                    $ret .= '<td>' . $pRS['NOME_COMPLETO'] . '</td>';
                } else {
                    $ret .= '<td><a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $pRS['NU_CANDIDATO'] . '" target="_blank">' . $pRS['NOME_COMPLETO'] . ' (' . $pRS['NU_CANDIDATO'] . ')</a></td>';
                }
                $ret .= '<td style="text-align:center;">&nbsp;' . $pRS['NU_PASSAPORTE'] . '</td>';
                if ($pRS['nu_protocolo'] == '') {
                    $processo = '???';
                } else {
                    $processo = "&nbsp;" . htmlentities($pRS['nu_protocolo']);
                }
                $ret .= '<td style="text-align:center;">' . $processo . '</td>';
                $ret .= '<td style="text-align:center;">' . $pRS['dt_requerimento'] . '</td>';
                $ret .= '<td>&nbsp;</td>';
                $ret .= '<td>&nbsp;</td>';
                $ret .= '<td>' . $pRS['depf_no_nome'] . '</td>';
                $ret .= '<td>&nbsp;</td>';

                $sql = "select a.*, no_tipo_arquivo, co_tipo_arquivo
					     from arquivos a
						 join tipo_arquivo ta on ta.id_tipo_arquivo = a.tp_arquivo
						where id_solicita_visto = " . $pRS['id_solicita_visto'] . "
						  and nu_candidato=" . $pRS['NU_CANDIDATO'] . "
						  and TP_ARQUIVO in (3,4,9)";

                $xres = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
                $br = '';
//				$arqs = $sql;
                $arqs = '';
                while ($xrs = $xres->fetch(PDO::FETCH_ASSOC)) {
                    $arq = new cARQUIVO();
                    $arqs .= $br . '<a href="' . cAMBIENTE::$m_dominio . '/operador/download.php?arq=' . $arq->CaminhoReal($xrs['NU_CANDIDATO'], $xrs['NO_ARQUIVO'], $xrs['NU_EMPRESA'], $xrs['ID_SOLICITA_VISTO']) . '" target="_blank">' . $xrs['co_tipo_arquivo'] . '</a>';
//					$arqs .= $br.'xx';
                    $br = '<br/>';
                }

                $ret .= '<td>' . $arqs . '</td>';
                $ret .= '</tr>' . "\n";
            }
        }

        $cab = '<table border=0 width=100% class="grid">';
        $cab .= '<thead>';
        $titulo = 'Protocolo de Registro de ' . $pData_ini . ' até ' . $pData_fim;
        if ($pNU_EMPRESA != '') {
            $titulo .= ' (' . $empresa . ')';
        }
        $cab .= '<tr><td colspan="13" style="font-size:12pt;font-weight:bold;">' . $titulo . '</td></tr>
			 <tr align="center" height=20>
			  <th style="text-align:center;">#</th>
			  <th style="text-align:center;">OS</th>
			  <th style="text-align:left">Empresa (da OS)</th>
			  <th style="text-align:left">Embarcação (do candidato)</th>
			  <th style="text-align:left">Nome</th>
			  <th style="text-align:center">Passaporte</th>
			  <th style="text-align:center">Protocolo</th>
			  <th style="text-align:center">Data</th>
			  <th style="text-align:left">Valor</th>
			  <th style="text-align:left">Ref</th>
			  <th style="text-align:left">Local</th>
			  <th style="text-align:left">Comprovante</th>
			  <th style="text-align:center">Anexos</th>
			</tr>
			</thead>
			<tbody>
		';
        $ret = $cab . $ret;
        $ret .= "</tbody></table>";
        $ret .= $i . " processos selecionados.";
        return $ret;
    }

    public static function ProtocoloRegistro_Rodape($pQtd) {
        $ret = '';
        $ret .= "</tbody></table>";
        $ret .= $pQtd . " processos selecionados.";
        return $ret;
    }

    public static function Abas_ProtocoloBSB($pAtiva) {
        $ret = '';
        $abas = array(
            self::abPROT_NAO_ENVIADOS => array("url" => '/paginas/carregueComMenu.php?controller=cINTERFACE_OS&metodo=ProtocoloBsb_NaoEnviados', "label" => 'Alterar protocolo')
            , self::abPROT_ENVIADOS => array("url" => '/paginas/carregueComMenu.php?controller=cINTERFACE_OS&metodo=ProtocoloBsb_Enviados', "label" => 'Protocolos enviados')
            , self::abPROT_PROTOCOLO => array("url" => '/paginas/carregueComMenu.php?controller=cINTERFACE_OS&metodo=ProtocoloBsb_Protocolo', "label" => 'Informar andamento')
        );
        $ret .= '<ul>';
        for ($i = 1; $i <= count($abas); $i++) {
            if ($i == $pAtiva) {
                $ret .= '<li><a href="#abaAtiva" class="ativo">' . $abas[$i]['label'] . '</a></li>';
            } else {
                $ret .= '<li><a href="' . $abas[$i]['url'] . '">' . $abas[$i]['label'] . '</a></li>';
            }
        }

        $ret.= '</ul>';
        return $ret;
    }

    public static function ProtocoloBsb_NaoEnviados() {
        $msg = '';
        $ret = '';
        cHTTP::$comMenu = true;
        cLAYOUT::AdicionarScriptsPadrao();
        $tmpl = new cTEMPLATE_OS_PROTOCOLO_BSB_NAO_ENVIADOS();
        $tmpl->CarregueDoHTTP();
        //var_dump($tmpl->mCampo);
        if ($tmpl->mFiltro['dt_envio_bsb']->mValor == '') {
            $tmpl->mFiltro['dt_envio_bsb']->mValor = date('d/m/Y');
        }

        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                    try {
                        foreach (cHTTP::$POST['CMP_SELETOR'] as $i => $valor) {
                            $os = new cORDEMSERVICO();
                            $os->mID_SOLICITA_VISTO = $valor;
                            //$os->
                        }
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                case 'Filtrar':
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        } else {
            $hoje = new DateTime();
            $hoje->sub(new DateInterval('P3M'));
            $tmpl->mFiltro['dt_cadastro_ate']->mValor = $hoje->format('d/m/Y');
        }
        $res = cORDEMSERVICO::RecupereOsProtocoloTodos($tmpl->mFiltro['dt_envio_bsb']->mValor, $tmpl->mFiltro['dt_cadastro_ini']->mValor, $tmpl->mFiltro['dt_cadastro_ate']->mValor);
        $ret .= cINTERFACE::formTemplate($tmpl);
        $ret.= cINTERFACE::RenderizeTitulo($tmpl, true, cINTERFACE::titPAGINA);
        $ret.= '<div class="conteudoInterno">';
        $ret.= '<div id="tabsOS" class="aba">';
        $ret.= self::Abas_ProtocoloBSB(self::abPROT_NAO_ENVIADOS);
        $ret.= '<div id="abaAtiva" style="padding-left:5px;padding-right:5px;">';
        $ret.= cINTERFACE::RenderizeListagem_Filtros($tmpl);
        $ret.= cINTERFACE::RenderizeListagem_Cabecalho($tmpl);
        $dtCadastro = '';
        if (is_object($res)) {
            while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
                if ($dtCadastro != $rs['dt_cadastro']) {
                    $dtCadastro = $rs['dt_cadastro'];
                    $ret .= '<tr class="sep"><th colspan="10" style="text-align:left;">Cadastradas em ' . $rs['dt_cadastro_fmt'] . '</th></tr>';
                }
                $tmpl->CarregueDoRecordset($rs);
                if ($tmpl->mCampo['dt_envio_bsb_fmt_full']->mValor == $tmpl->mFiltro['dt_envio_bsb']->mValor) {
                    $tmpl->mCampo['SELETOR']->mValor = true;
                } else {
                    $tmpl->mCampo['SELETOR']->mValor = false;
                }
                $tmpl->mCampo['SELETOR']->mValorChave = $rs['id_solicita_visto'];
                $tmpl->mCampo['NU_SOLICITACAO']->mParametros = 'id_solicita_visto=' . $tmpl->mCampo['id_solicita_visto']->mValor;
                $ret.= cINTERFACE::RenderizeListagem_Linha($tmpl);
            }
        } else {
            $ret.= '</table>Nenhuma ordem de serviço encontrada para os filtros informados';
        }
        $ret.= '</form>';
        $ret.= '</div>';
        $ret.= '</div>';
        $ret.= '</div>';
        cHTTP::$SESSION['msg'] = $msg;
        cLAYOUT::AdicionarScriptExibicaoMensagens();
        return $ret;
    }

    public static function ProtocoloBsb_Enviados() {
        $msg = '';
        $ret = '';
        cHTTP::$comMenu = true;
        cLAYOUT::AdicionarScriptsPadrao();
        $tmpl = new cTEMPLATE_OS_PROTOCOLO_BSB_ENVIADOS();
        $tmpl->CarregueDoHTTP();
        if ($tmpl->mFiltro['dt_envio_bsb']->mValor == '') {
            $tmpl->mFiltro['dt_envio_bsb']->mValor = date('d/m/Y');
        }
        //var_dump($tmpl->mCampo);

        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                case 'SALVAR_E_FECHAR':
                    try {
                        cINTERFACE::MapeiaEdicaoClasse($tmpl, $cand);
                        $cand->AtualizarExterno();
                        if ($tmpl->mAcaoPostada == 'SALVAR_E_FECHAR') {

                        }
                        $msg = "Data updated sucessfully!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                case 'Filtrar':
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        }

        $res = cORDEMSERVICO::RecupereOsProtocoloEnviados($tmpl->mFiltro['dt_envio_bsb']->mValor);
        $ret.= cINTERFACE::RenderizeTitulo($tmpl, true, cINTERFACE::titPAGINA);
        $ret.= '<div class="conteudoInterno">';
        $ret.= '<div id="tabsOS" class="aba">';
        $ret.= self::Abas_ProtocoloBSB(self::abPROT_ENVIADOS);
        $ret.= '<div style="padding-left:5px;padding-right:5px;">';
        $ret .= cINTERFACE::formTemplate($tmpl);
        $ret.= cINTERFACE::RenderizeListagem_Filtros($tmpl);
        $ret.= cINTERFACE::RenderizeListagem_Cabecalho($tmpl);
        $dtCadastro = '';
        while ($rs = mysql_fetch_array($res)) {
            if ($dtCadastro != $rs['dt_cadastro']) {
                $dtCadastro = $rs['dt_cadastro'];
                $ret .= '<tr class="sep"><th colspan="10" style="text-align:left;">Cadastradas em ' . $rs['dt_cadastro_fmt'] . '</th></tr>';
            }
            $tmpl->CarregueDoRecordset($rs);
            $tmpl->mCampo['NU_SOLICITACAO']->mParametros = 'id_solicita_visto=' . $tmpl->mCampo['id_solicita_visto']->mValor;
            $ret.= cINTERFACE::RenderizeListagem_Linha($tmpl);
        }
        $ret.= '</form>';
        $ret.= '</div>';
        $ret.= '</div>';
        $ret.= '</div>';
        cHTTP::$SESSION['msg'] = $msg;
        cLAYOUT::AdicionarScriptExibicaoMensagens();
        return $ret;
    }

    public static function ProtocoloBsb_Protocolo() {
        $msg = '';
        $ret = '';
        cHTTP::$comMenu = true;
        cLAYOUT::AdicionarScriptsPadrao();
        $tmpl = new cTEMPLATE_OS_PROTOCOLO_BSB_PROTOCOLO();
        $tmpl->CarregueDoHTTP();
        if ($tmpl->mFiltro['dt_envio_bsb']->mValor == '') {
            $tmpl->mFiltro['dt_envio_bsb']->mValor = date('d/m/Y');
        }
        //var_dump($tmpl->mCampo);

        if ($tmpl->mFoiPostado) {
            // Limpa o combo por default
            switch ($tmpl->mAcaoPostada) {
                case 'SALVAR':
                case 'SALVAR_E_FECHAR':
                    try {
                        cINTERFACE::MapeiaEdicaoClasse($tmpl, $cand);
                        $cand->AtualizarExterno();
                        if ($tmpl->mAcaoPostada == 'SALVAR_E_FECHAR') {

                        }
                        $msg = "Data updated sucessfully!";
                    } catch (Exception $e) {
                        $msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
                    }
                    break;
                case 'Filtrar':
                    break;
                default:
                    $msg = "Operação não encontrada (" . $tmpl->mAcaoPostada . ")";
                    break;
            }
        }

        $res = cORDEMSERVICO::RecupereOsProtocoloProtocolo($tmpl->mFiltro['dt_envio_bsb']->mValor);
        $ret.= cINTERFACE::RenderizeTitulo($tmpl, true, cINTERFACE::titPAGINA);
        $ret.= '<div class="conteudoInterno">';
        $ret.= '<div id="tabsOS" class="aba">';
        $ret.= self::Abas_ProtocoloBSB(self::abPROT_PROTOCOLO);
        $ret.= '<div style="padding-left:5px;padding-right:5px;">';
        $ret .= cINTERFACE::formTemplate($tmpl);
        $ret.= cINTERFACE::RenderizeListagem_Cabecalho($tmpl);
        $dt_envio_bsb = '';
        while ($rs = mysql_fetch_array($res)) {
            if ($dt_envio_bsb != $rs['dt_envio_bsb']) {
                $dt_envio_bsb = $rs['dt_envio_bsb'];
                $ret .= '<tr class="sep"><th colspan="11" style="text-align:left;">Protocolo de ' . $rs['dt_envio_bsb_fmt'] . '</th></tr>';
            }
            $tmpl->CarregueDoRecordset($rs);
            $tmpl->mCampo['NU_SOLICITACAO']->mParametros = 'id_solicita_visto=' . $tmpl->mCampo['id_solicita_visto']->mValor;
            $ret.= cINTERFACE::RenderizeListagem_Linha($tmpl);
        }
        $ret.= '</form>';
        $ret.= '</div>';
        $ret.= '</div>';
        $ret.= '</div>';
        cHTTP::$SESSION['msg'] = $msg;
        cLAYOUT::AdicionarScriptExibicaoMensagens();
        return $ret;
    }

}
