<?php 
class cCTRL_PROCESSO_EDIT_OS_TIMESHEET extends cCTRL_MODELO_LIST{
	public function InicializeModelo()
	{
		$this->modelo = new cdespesa();
	}
	
	public function InicializeTemplate()
	{
		$this->template = new cTMPL_PROCESSO_EDIT_OS_TIMESHEET();
	}
	
	public function Redirecione()
	{
		// não redireciona!
	}
	public function Edite()
	{
		return $this->Liste();
	}
	
	public function ProcessePost() 
	{
		if($this->template->get_acaoPostada() == 'NOVA'){
			$desp = new cdespesa();
			$desp->mid_solicita_visto = $this->template->getValorFiltro('id_solicita_visto');
			$desp->Salvar();
		}
		if($this->template->get_acaoPostada() == 'EXCLUIR'){
			$parametros = cHTTP::ParametroValido('CMP___parametros');
			$parametros = explode("&", $parametros);
			foreach($parametros as $parametro){
				$atributo = explode("=", $parametro);
				if($atributo[0] == 'desp_id'){
					$desp = new cdespesa();
					$desp->mdesp_id = $atributo[1];
					$desp->Excluir();					
				}
			}
		}
	}
	
}
