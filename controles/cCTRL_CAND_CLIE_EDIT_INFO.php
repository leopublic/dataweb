<?php

class cCTRL_CAND_CLIE_EDIT_INFO extends cCTRL_CAND_CLIE_EDIT {

    public function __construct() {
        parent::__construct();
        $this->nomeTemplate = 'cliente/formulario_cand.twig';
        $this->nomeAbaAtiva = 'INFO';
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_CAND_CLIE_EDIT_INFO();
        parent::InicializeTemplate();
    }

    public function ProcessePost() {
        $this->modelo->Recuperar($this->template->getValorCampo('NU_CANDIDATO'));
        parent::ProcessePost();
    }

    public function ProcesseSalvar() {
        $rep = new cREPOSITORIO_CANDIDATO;
        $rep->salvaAlteracoesCliente($this->template->getValorCampo('NU_CANDIDATO'), $_POST);
    }

}
