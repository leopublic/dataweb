<?php

class cCTRL_ORDEMSERVICO extends cCTRL_MODELO {

    public function getEditar() {
        $id_solicita_visto = $_GET['id_solicita_visto'];

        $repOs = new cREPOSITORIO_OS;

        $menu = cLAYOUT::Menu();
        $abas = cABAS::novaAbaOS($id_solicita_visto);
        $abas->abas['DadosOs']->ativa = true;
        $os = new cORDEMSERVICO();
        if ($id_solicita_visto > 0) {
            $os->Recuperar($id_solicita_visto);
            $candidatos = $repOs->candidatos($id_solicita_visto);
            $servicos = cSERVICO::ativos();
        } else {
            $servicos = array(array('chave' => '', 'descricao' => '(selecione a empresa)'));
            $candidatos = array();
        }
        $abas->configuraPelaOs($os);
        $empresas = cEMPRESA::ativasComPreco();
        $empresas_cobranca = cEMPRESA::deCobranca();
        $responsaveis = cusuarios::responsaveis();

        if ($os->mid_solicita_visto_pacote > 0) {
            $pacote = new cORDEMSERVICO;
            $pacote->Recuperar($os->mid_solicita_visto_pacote);
        }

        if ($os->mNU_EMPRESA > 0) {
            $emb_projetos = cEMBARCACAO_PROJETO::daEmpresa($os->mNU_EMPRESA);
        } else {
            $emb_projetos = array(array('chave'=> '', 'descricao'=>'(selecione a empresa)'));
        }
        $data = array(
            'menu' => $menu,
            'abas' => $abas,
            'subtitulo' => ' - Nova',
            'os' => $os,
            'empresas' => $empresas,
            'empresas_cobranca' => $empresas_cobranca,
            'servicos' => $servicos,
            'responsaveis' => $responsaveis,
            'emb_projetos' => $emb_projetos,
            'candidatos' => $candidatos,
            'pacote' => $pacote
        );
        return cTWIG::Render('intranet/os/editar.twig', $data);
    }


    public function getEmpresaServico() {
        $id_solicita_visto = $_GET['id_solicita_visto'];
        $menu = cLAYOUT::Menu();
        $abas = cABAS::novaAbaOS($id_solicita_visto);
        $abas->abas['DadosOs']->ativa = true;
        $os = new cORDEMSERVICO();
        $abas->configuraPelaOs($os);
        $empresas = cEMPRESA::ativasComPreco();
        $empresas_cobranca = cEMPRESA::deCobranca();
        $responsaveis = cusuarios::responsaveis();
        $emb_proj = array();
        $servicos = array(array('chave' => '', 'descricao' => '(selecione a empresa)'));
        $data = array(
            'menu' => $menu,
            'abas' => $abas,
            'subtitulo' => ' - Nova',
            'os' => $os,
            'empresas' => $empresas,
            'empresas_cobranca' => $empresas_cobranca,
            'servicos' => $servicos,
            'responsaveis' => $responsaveis,
            'emb_proj' => $emb_proj,
        );
        return cTWIG::Render('intranet/os/abrir.twig', $data);
    }

    public function postEmpresaServico() {

    }

    public function getPreenchimento() {

    }

    public function postPreenchimento() {

    }

    public function ComplementarLinhaListagem() {

    }

    public function getCandidatos() {
        $ret = array();
        $id_solicita_visto = $_GET['id_solicita_visto'];
        $lista = $_GET['lista'];
        $rep = new cREPOSITORIO_OS;
        $candidatos = $rep->candidatos($id_solicita_visto, $lista);
        $data = array(
            'candidatos' => $candidatos,
            'id_solicita_visto' => $id_solicita_visto
        );
        $html = cTWIG::Render('intranet/os/candidatos.twig', $data);
        $ret['codret'] = 0;
        $ret['msg'] = "Candidato adicionado com sucesso";
        $ret['html'] = utf8_encode($html);
        return json_encode($ret);

    }

    public function ListeFinanceiro() {
        cHTTP::$comCabecalhoFixo = true;
        $tela = new cTMPL_OS_LISTAR_PENDENTES_COBRANCA();
        $tela->CarregueDoHTTP();
        if (!$tela->mFoiPostado) {
            $tela->mFiltro['stco_id']->mValor = 1;
        	$dt_ini = new DateTime('NOW');
        	$dt_ini->sub(new DateInterval('P1D'));
        	$tela->mFiltro['soli_dt_liberacao_cobranca_ini']->mValor = $dt_ini->format('d/m/Y');
            $tela->mFiltro['soli_dt_liberacao_cobranca_fim']->mValor = date('d/m/Y');
        }

        if (cHTTP::HouvePost()) {
            $titulo_msg = "Mensagem";

            try {
                switch (cHTTP::$POST['CMP___acao_GRID']) {
                    case "LIBERAR_SELECIONADOS":
                    case "COBRAR_CONCEITO_SELECIONADOS":
                    case "COBRAR_PACOTE_SELECIONADOS":
                    case "COBRAR_NAO_SELECIONADOS":
                    case "PENDENTES":
                        if (isset(cHTTP::$POST["CMP_SELETOR"])) {
                            $lote = '';
                            $virg = '';
                            foreach (cHTTP::$POST["CMP_SELETOR"] as $id_solicita_visto) {
                                $lote .= $virg . intval($id_solicita_visto);
                                $virg = ',';
                            }
                            if ($lote != '') {
                                if (cHTTP::$POST['CMP___acao_GRID'] == "LIBERAR_SELECIONADOS") {
                                    cORDEMSERVICO::LibereParaCobrancaEmLote(cSESSAO::$mcd_usuario, $lote);
                                } elseif (cHTTP::$POST['CMP___acao_GRID'] == "COBRAR_CONCEITO_SELECIONADOS") {
                                    cORDEMSERVICO::CobrancaConceitoEmLote(cSESSAO::$mcd_usuario, $lote);
                                } elseif (cHTTP::$POST['CMP___acao_GRID'] == "COBRAR_PACOTE_SELECIONADOS") {
                                    cORDEMSERVICO::CobrancaPacoteEmLote(cSESSAO::$mcd_usuario, $lote);
                                } elseif (cHTTP::$POST['CMP___acao_GRID'] == "COBRAR_NAO_SELECIONADOS") {
                                    cORDEMSERVICO::NaoCobreEmLote(cSESSAO::$mcd_usuario, $lote);
                                } elseif (cHTTP::$POST['CMP___acao_GRID'] == "PENDENTES") {
                                    error_log('Entou no pendentes');
                                    cORDEMSERVICO::PendentesEmLote(cSESSAO::$mcd_usuario, $lote);
                                }
                                $msg = "Ordens de serviço alteradas com sucesso!";
                            } else {
                                throw new cERRO_CONSISTENCIA('- nenhuma ordem de serviço foi selecionada.');
                            }
                        } else {
                            throw new cERRO_CONSISTENCIA('- nenhum ordem de serviço foi selecionada.');
                        }
                        break;
                }
            } catch (cERRO_CONSISTENCIA $e) {
                $retorno = '-1';
                $msg = "Não foi possível executar a solicitação pois:" . $e->mMsg;
                $data['titulo_mensagem'] = 'warning';
            } catch (cERRO_SQL $e) {
                $retorno = '-1';
                $msg = "Não foi possível executar a solicitação. Encaminhe essas informações ao suporte para agilizar a resolução do problema:<br/>" . htmlentities($e->mMsg);
                $data['titulo_mensagem'] = 'error';
            } catch (Exception $e) {
                $retorno = '-1';
                $msg = "Não foi possível executar a solicitação. Encaminhe essas informações ao suporte para agilizar a resolução do problema:<br>" . htmlentities($e->getMessage(), ENT_QUOTES);
                $data['titulo_mensagem'] = 'error';
            }
        }
        cHTTP::$SESSION['msg'] = $msg;
        $x = new cORDEMSERVICO();
        $ordenacao = '';
        $virgula = '';
        $ret = '';
        if (intval($tela->mFiltro['NU_EMPRESA']->mValor) > 0) {
            if (intval($tela->mFiltro['NU_EMBARCACAO_PROJETO']->mValor) > 0) {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = false;
            } else {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = true;
            }
        }
        if ($tela->mFiltro['dt_solicitacao_ini']->mValor != '') {
            $tela->mFiltro['dt_solicitacao_ini']->mWhere = " dt_solicitacao >= " . cBANCO::DataOk($tela->mFiltro['dt_solicitacao_ini']->mValor);
        }
        if ($tela->mFiltro['dt_solicitacao_fim']->mValor != '') {
            $tela->mFiltro['dt_solicitacao_fim']->mWhere = " dt_solicitacao <= " . cBANCO::DataOk($tela->mFiltro['dt_solicitacao_fim']->mValor);
        }

        if ($tela->mFiltro['resc_dt_faturamento_ini']->mValor != '') {
            $tela->mFiltro['resc_dt_faturamento_ini']->mWhere = " resc_dt_faturamento >= " . cBANCO::DataOk($tela->mFiltro['resc_dt_faturamento_ini']->mValor);
        }
        if ($tela->mFiltro['resc_dt_faturamento_fim']->mValor != '') {
            $tela->mFiltro['resc_dt_faturamento_fim']->mWhere = " resc_dt_faturamento <= " . cBANCO::DataOk($tela->mFiltro['resc_dt_faturamento_fim']->mValor);
        }

        if (cBANCO::WhereFiltros($tela->mFiltro) == ''){
        	$dt_ini = new DateTime('NOW');
        	$dt_ini->sub(new DateInterval('P1D'));
        	$tela->mFiltro['soli_dt_liberacao_cobranca_ini']->mValor = $dt_ini->format('d/m/Y');
            $tela->mFiltro['soli_dt_liberacao_cobranca_fim']->mValor = date('d/m/Y');
        }

        if ($tela->mFiltro['soli_dt_liberacao_cobranca_ini']->mValor != '') {
            $tela->mFiltro['soli_dt_liberacao_cobranca_ini']->mWhere = " soli_dt_liberacao_cobranca >= " . cBANCO::DataOk($tela->mFiltro['soli_dt_liberacao_cobranca_ini']->mValor." 00:00:00");
        }
        if ($tela->mFiltro['soli_dt_liberacao_cobranca_fim']->mValor != '') {
            $tela->mFiltro['soli_dt_liberacao_cobranca_fim']->mWhere = " soli_dt_liberacao_cobranca <= " . cBANCO::DataOk($tela->mFiltro['soli_dt_liberacao_cobranca_fim']->mValor." 23:59:59");
        }
        $cursor = $x->ListeCobranca('', $tela->mFiltro, $tela->ordenacao());

        $tela->Renderize($cursor);
        $ret .= $tela->outputBuffer;
        $ret .= $tela->TratamentoPadraoDeMensagens();
        return $this->EnveloparParaTela($ret, $tela);
    }

    public function ListeFinanceiroExcel(){
        cHTTP::$MIME = cHTTP::mmXLS;

        $tela = new cTMPL_OS_LISTAR_PENDENTES_COBRANCA();
        $tela->CarregueDoHTTP();
        $x = new cORDEMSERVICO();
        $ordenacao = '';
        $virgula = '';
        $ret = '';
        if (intval($tela->mFiltro['NU_EMPRESA']->mValor) > 0) {
            if (intval($tela->mFiltro['NU_EMBARCACAO_PROJETO']->mValor) > 0) {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = false;
            } else {
                $tela->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = true;
            }
        }
        if ($tela->mFiltro['dt_solicitacao_ini']->mValor != '') {
            $tela->mFiltro['dt_solicitacao_ini']->mWhere = " dt_solicitacao >= " . cBANCO::DataOk($tela->mFiltro['dt_solicitacao_ini']->mValor);
        }
        if ($tela->mFiltro['dt_solicitacao_fim']->mValor != '') {
            $tela->mFiltro['dt_solicitacao_fim']->mWhere = " dt_solicitacao <= " . cBANCO::DataOk($tela->mFiltro['dt_solicitacao_fim']->mValor);
        }

        if ($tela->mFiltro['resc_dt_faturamento_ini']->mValor != '') {
            $tela->mFiltro['resc_dt_faturamento_ini']->mWhere = " resc_dt_faturamento >= " . cBANCO::DataOk($tela->mFiltro['resc_dt_faturamento_ini']->mValor);
        }
        if ($tela->mFiltro['resc_dt_faturamento_fim']->mValor != '') {
            $tela->mFiltro['resc_dt_faturamento_fim']->mWhere = " resc_dt_faturamento <= " . cBANCO::DataOk($tela->mFiltro['resc_dt_faturamento_fim']->mValor);
        }

        if (cBANCO::WhereFiltros($tela->mFiltro) == ''){
        	$dt_ini = new DateTime('NOW');
        	$dt_ini->sub(new DateInterval('P1D'));
        	$tela->mFiltro['soli_dt_liberacao_cobranca_ini']->mValor = $dt_ini->format('d/m/Y');
            $tela->mFiltro['soli_dt_liberacao_cobranca_fim']->mValor = date('d/m/Y');
        }

        if ($tela->mFiltro['soli_dt_liberacao_cobranca_ini']->mValor != '') {
            $tela->mFiltro['soli_dt_liberacao_cobranca_ini']->mWhere = " soli_dt_liberacao_cobranca >= " . cBANCO::DataOk($tela->mFiltro['soli_dt_liberacao_cobranca_ini']->mValor." 00:00:00");
        }
        if ($tela->mFiltro['soli_dt_liberacao_cobranca_fim']->mValor != '') {
            $tela->mFiltro['soli_dt_liberacao_cobranca_fim']->mWhere = " soli_dt_liberacao_cobranca <= " . cBANCO::DataOk($tela->mFiltro['soli_dt_liberacao_cobranca_fim']->mValor." 23:59:59");
        }
        $cursor = $x->ListeCobranca('', $tela->mFiltro, $tela->ordenacao());

        $tela->Renderize($cursor);
        $ret .= $tela->outputBuffer;
        return $ret;
    }

    public function ListeNaoFaturadasPorSolicitante() {
        cHTTP::$comCabecalhoFixo = true;
        $tela = new cTMPL_OS_FATURAR_POR_SOLICITANTE();
        $tela->CarregueDoHTTP();

        if ($tela->mFoiPostado) {
            if ($tela->mAcaoPostada == 'GERAR_RESUMO') {
                $size = (int) $_SERVER['CONTENT_LENGTH'];
                $selecionados = $_POST[cINTERFACE::PREFIXO_CAMPOS . 'SELETOR'];
                $qtd_linhas = $tela->getQtdLinhasPostadas();
                $virgula = '';
                //Montar uma lista com os id_solicita_visto selecionados
                $nu_empresa_requerente = '';
                $NU_EMBARCACAO_PROJETO_COBRANCA = '';
                $msg = '';
                $array_ids = array();
                foreach (cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'SELETOR'] as $linhaSelecionada) {
//					$linhaSelecionada--;
                    if ($nu_empresa_requerente == '') {
                        $nu_empresa_requerente = $tela->mCampo['nu_empresa_requerente']->mValor[$linhaSelecionada];
                    }
                    if ($nu_empresa_requerente != $tela->mCampo['nu_empresa_requerente']->mValor[$linhaSelecionada]) {
                        $msg = 'Para criar um Resumo de Cobranças todas as OS devem ser da mesma empresa. Verifique.';
                        break;
                    }

                    if ($NU_EMBARCACAO_PROJETO_COBRANCA == '') {
                        $NU_EMBARCACAO_PROJETO_COBRANCA = $tela->mCampo['NU_EMBARCACAO_PROJETO_COBRANCA']->mValor[$linhaSelecionada];
                    }

                    $array_ids[] = $tela->mCampo['id_solicita_visto']->mValor[$linhaSelecionada];
                }

                if (count($array_ids) == 0){
                   $msg = "Selecione pelo menos uma OS para criar o resumo ";
                }
                if ($msg != '') {
                    cHTTP::$SESSION['msg'] = $msg;
                } else {

                    $rep = new cREPOSITORIO_FATURAMENTO;
                    $rep->criarFaturamentoTemporario(cSESSAO::$mcd_usuario, $array_ids);
                    $parametros = 'resc_id=0';
                    $parametros.= '&nu_empresa_requerente=' . $nu_empresa_requerente;
                    $parametros.= '&NU_EMBARCACAO_PROJETO_COBRANCA=' . $NU_EMBARCACAO_PROJETO_COBRANCA;
                    cHTTP::$SESSION['msg'] = "Confira os dados para a geração do resumo";
                    cHTTP::RedirecionePara('cCTRL_RESUMO_COBRANCA', 'Edite', $parametros);
                }
            }
        } else {
            // Inicializa a empresa prestadora na Mundivisas
            $tela->setValorFiltro('nu_empresa_prestadora', 1);
        }

        $x = new cORDEMSERVICO();
        if ($tela->getValorFiltro('nu_empresa_requerente') != '') {
            $tela->mAcoes['GERAR_RESUMO']->mVisivel = true;
            $cursor = $x->ListeLiberadasParaFatPorSolicitante('', $tela->mFiltro, $tela->ordenacao());
        } else {
            $tela->mAcoes['GERAR_RESUMO']->mVisivel = false;
            $msg.= "<br/>Informe qual empresa prestadora você deseja faturar";
            cHTTP::$SESSION['msg'] = $msg;
        }
        $ret = '';
        $tela->Renderize($cursor);

        $ret .= $tela->outputBuffer;
        $ret .= $tela->TratamentoPadraoDeMensagens();
        return $this->EnveloparParaTela($ret, $tela);
    }

    public function Edite() {
        $tmpl = new cTMPL_ORDEMSERVICO_EDITE();
        return parent::Edite($tmpl, new cORDEMSERVICO(), "", "");
    }

    public function ListeArquivos() {
        $tmpl = new cTMPL_ORDEMSERVICO_ARQUIVOS_LISTE();
        $tmpl->mEstiloTitulo = cINTERFACE::titSUB;
        $tmpl->mAcoesLinha['EXCLUIR_ARQUIVO']->mModo = cACAO_LINK_CONTROLER_METODO::modoAJAX;
        $tmpl->mAcoes['SALVAR']->mModo = cACAO_LINK_CONTROLER_METODO::modoAJAX;
        $tmpl->mAcoes['SALVAR']->mParametros .= "&ID_SOLICITA_VISTO=" . cHTTP::ParametroValido("ID_SOLICITA_VISTO");
        $tmpl->CarregueDoHTTP();
        // Carrega o usuario correspondente aa tela exibida
        $os = new cORDEMSERVICO();
        $linha = cHTTP::ParametroValido('linha');
        switch ($tmpl->mAcaoPostada) {
            case "EXCLUIR_ARQUIVO":
                try {
                    $NU_SEQUENCIAL = cHTTP::ParametroValido('NU_SEQUENCIAL');
                    $os->ExcluaArquivo($NU_SEQUENCIAL);
                    cHTTP::$SESSION['msg'] = 'Arquivo excluído com sucesso!';
                } catch (cERRO_CONSISTENCIA $e) {
                    cHTTP::$SESSION['msg'] = $e->getMessage();
                    cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
                    cHTTP::$SESSION['msgTitulo'] = "Aten&ccedil;&atilde;o";
                } catch (Exception $e) {
                    cHTTP::$SESSION['msg'] = 'N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o pois:<br/>' . htmlentities($e->getMessage());
                    cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ERROR;
                }
                break;
            case 'SALVAR':
                try {
                    for ($index = 1; $index <= $tmpl->mCampo[cTEMPLATE::CMP_TOTAL_LINHAS]->mValor; $index++) {
                        $NU_SEQUENCIAL = $tmpl->CampoLinha('NU_SEQUENCIAL', $index);
                        $NU_CANDIDATO = $tmpl->CampoLinha('NU_CANDIDATO', $index);
                        $os->AtualizarCandidatoDeArquivo($NU_SEQUENCIAL, $NU_CANDIDATO);
                    }
                    cHTTP::$SESSION['msg'] = 'Informações atualizadas com sucesso!';
                } catch (cERRO_CONSISTENCIA $e) {
                    cHTTP::$SESSION['msg'] = $e->getMessage();
                    cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ALERT;
                    cHTTP::$SESSION['msgTitulo'] = "Aten&ccedil;&atilde;o";
                } catch (Exception $e) {
                    cHTTP::$SESSION['msg'] = 'N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o pois:<br/>' . htmlentities($e->getMessage());
                    cHTTP::$SESSION['tipo_msg'] = cNOTIFICACAO::TM_ERROR;
                }
                break;
        }
        return $this->MonteTabelaArquivos($tmpl, new cORDEMSERVICO(), "ListeArquivos", "ConfigureLinhaArquivo") . '</div>';
    }

    public function MonteTabelaArquivos(cTEMPLATE $pTemplateListar, cMODELO $pModelo, $pFonteDados = "Listar", $pCustomizarLinha = "ComplementarLinhaListagem") {
        $ret = '';
        // Carrega o ID informado no get ou o form postado anteriormente, se houver
        $pTemplateListar->CarregueDoHTTP();
        $pTemplateListar->AtribuaAoOBJETO($pModelo);

        // Configura tela de acordo com as caracteristicas da instancia recebido.
        $ret .= cINTERFACE::RenderizeTitulo($pTemplateListar, true, $pTemplateListar->mEstiloTitulo);
        $ret .= '<div class="conteudoInterno">';
        $ret .= cINTERFACE::RenderizeListagem_Filtros($pTemplateListar);
        $qtdLinhas = 0;
        $conteudo = $this->MontaLinhaArquivos($pTemplateListar, $pModelo, $pFonteDados, $pCustomizarLinha, $qtdLinhas);
        $ret .= $conteudo;
        $pTemplateListar->mCampo[cTEMPLATE::CMP_TOTAL_LINHAS]->mValor = $qtdLinhas;
        $ret .= '</div>';
        $ret = cINTERFACE::formTemplate($pTemplateListar) . $ret . "</form>";
        return $this->EnveloparParaTela($ret, $pTemplateListar);
    }

    public function MontaLinhaArquivos(cTEMPLATE $pTemplateListar, $pModelo, $pFonteDados = "Listar", $pCustomizarLinha = "ComplementarLinhaListagem", &$pIndLinha = 0) {
        $conteudo = '';
        $cursor = $pModelo->$pFonteDados($pTemplateListar->mFiltro, $pTemplateListar->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor);
        $conteudo.= cINTERFACE::RenderizeListagem_Cabecalho($pTemplateListar);
        $pIndLinha = 0;
        while ($rs = mysql_fetch_array($cursor)) {
            $pIndLinha++;
            $pTemplateListar->CarregueDoRecordset($rs, true);
            // Fazer configuracoes quando necessario
            $this->$pCustomizarLinha($pTemplateListar);
            $conteudo .= cINTERFACE::RenderizeListagem_Linha($pTemplateListar, "", $pIndLinha);
        }
        $pIndLinha++;
        $pTemplateListar->MostreCampo('NU_TIPO_ARQUIVO');
        $pTemplateListar->EscondaCampo('CO_TIPO_ARQUIVO');
        $pTemplateListar->MostreCampo('arquivo');
        $pTemplateListar->EscondaCampo('NO_ARQ_ORIGINAL');
        $pTemplateListar->mCampo['NU_CANDIDATO']->mValor = '';
        $pTemplateListar->mAcoesLinha['ENVIAR_ARQUIVO']->mVisivel = true;
        $pTemplateListar->mAcoesLinha['DOWNLOAD']->mVisivel = false;
        $pTemplateListar->mAcoesLinha['EXCLUIR_ARQUIVO']->mVisivel = false;

        $conteudo .= cINTERFACE::RenderizeListagem_Linha($pTemplateListar, "", $pIndLinha);

        $conteudo .= cINTERFACE::RenderizeListagem_Rodape($pTemplateListar);
        $conteudo .= cINTERFACE::RenderizeListagem_RodapePagina($pTemplateListar);
        return $conteudo;
    }

    public function ConfigureLinhaArquivo(cTEMPLATE &$ptmpl) {
        $ptmpl->mCampo['NU_CANDIDATO']->mExtra .= " and ID_SOLICITA_VISTO = " . cHTTP::ParametroValido("ID_SOLICITA_VISTO");
        $msgConf = 'Confirma a exclusão do arquivo \\\'' . str_replace('"', "\'", str_replace("'", "\'", $ptmpl->mCampo['NO_ARQ_ORIGINAL']->mValor)) . '\\\' da OS ' . $ptmpl->mCampo['nu_solicitacao']->mValor . '?';
        $ptmpl->mAcoesLinha['EXCLUIR_ARQUIVO']->mMsgConfirmacao = $msgConf;
        $ptmpl->mAcoesLinha['DOWNLOAD']->mTarget = "_blank";
    }

    public function DisponibilizarArquivo() {
        $NU_SEQUENCIAL = cHTTP::ParametroValido('NU_SEQUENCIAL');
        readfile($file);
    }

    public function ComplementarConsultaDescricoes(&$pTmpl) {
        $valor = $pTmpl->mCampo['TE_DESCRICAO_ATIVIDADES']->mValor;
        $pTmpl->mAcoesLinha ['COPY']->mOnClick = 'copyToClipboard(\'' . $valor . '\')';
    }

    public function ConsultarDescricoesAtividade() {
        return parent::Liste(new cTMPL_ATIVIDADES_LISTE(), new cORDEMSERVICO(), 'CursorDescricoesAtividade', 'ComplementarConsultaDescricoes');
    }

    public function EditeProcesso() {
        $id_solicita_visto = cHTTP::ParametroValido('id_solicita_visto');
        $tela = new cEDICAO("operacoes_os");
        $tela->CarregarConfiguracao();
        $tela->mCampoNome['id_solicita_visto']->mValor = $id_solicita_visto;
        cINTERFACE_OS::PostOPERACOES_OS($tela, cHTTP::$POST, cHTTP::$FILES, cHTTP::$SESSION);
        $OS = new cORDEMSERVICO();
        $OS->mID_SOLICITA_VISTO = $id_solicita_visto;
        $OS->Recuperar();
        $OS->RecuperarCandidatos();
        $ctrl_processo = cCTRL_PROCESSO::FabricaControllerProcessoDaOS($OS);

        $OS_i = new cINTERFACE_OS();
        $OS_i->mID_SOLICITA_VISTO = $id_solicita_visto;
        $OS_i->Recuperar();
        $OS_i->RecuperarCandidatos(); // Precisa para saber se deve apresentar os dados de cadastro ou nao.
        $html = $OS_i->FormCabecalhoOS();

        $html .= '
		<div class="conteudoInterno">
			<div id="tabsOS" class="aba">' . $OS_i->aba("Processo", '/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso');

//			$msg = cINTERFACE::ProcessePost($_POST, $_GET, $_FILES, $_SESSION);
//            $ctrl_processo->id_solicita_visto = $id_solicita_visto;
        $formProcesso = $ctrl_processo->Edite();

        $html.= '<div id="Processo" style="padding-right:10px;padding-left:10px;"><!--inicio processo-->';
        $html.= $formProcesso;
        $html.= '</div><!--fim processo-->';
        if ($OS_i->mID_TIPO_ACOMPANHAMENTO == 3 || $OS_i->mID_TIPO_ACOMPANHAMENTO == 4 || $OS_i->mID_TIPO_ACOMPANHAMENTO == 5 || $OS_i->mID_TIPO_ACOMPANHAMENTO == 6 || $OS_i->mID_TIPO_ACOMPANHAMENTO == 8
//			 || $OS_i->mID_TIPO_ACOMPANHAMENTO == 12
        ) {
            $html .= '<br />
                            <div id="historico" class="ui-widget-content" style="padding:2px;">
                                    <div id="historicoHeader" class="ui-widget ui-widget-header" style="padding:9px;">Histórico do visto</div>
                                    <div style="padding:15px;">' . cINTERFACE_CANDIDATO::FormHistoricoVistoDeOs($OS_i->mID_SOLICITA_VISTO) . '</div>
                            </div>';
        }
        $html .= '</div>';

        $html .= '<div style="padding-top:20px; padding-left:10px;padding-right:10px;">';
        $x = new cCTRL_OS_HIST_LIST();
        $x->id_solicita_visto = $id_solicita_visto;
        $html .= $x->Liste();
        $html .= '</div>';

        if (isset(cHTTP::$SESSION['msg']) && cHTTP::$SESSION['msg'] != '') {
            $mensagem.="\njAlert('" . cHTTP::$SESSION['msg'] . "');";
            $_SESSION['msg'] = '';
        }
        $html .= '
			<script language="javascript">
				$(document).ready(function() {
					' . $mensagem . '
					$("#historico").addClass("ui-corner-all");
					$("#historicoHeader").addClass("ui-corner-all");
				});
			</script>';
        return $html;
    }

    public function OsAbertasPorMes() {
        cHTTP::setMetodoOperacaoDOWNLOAD();
        cHTTP::$MIME = cHTTP::mmXLS;
        $cursor = cAMBIENTE::$db_pdo->query("select * from v_os_abertas_por_mes");
        $ret = "<table>";
        $ret .= '<tr>';
        $ret .= '<td>Ano</td>';
        $ret .= '<td>Mes</td>';
        $ret .= '<td>Empresa</td>';
        $ret .= '<td>Embarcacao/projeto</td>';
        $ret .= '<td>Nome servico</td>';
        $ret .= '<td>Codigo servico</td>';
        $ret .= '<td>Tipo servico</td>';
        $ret .= '<td>Qtd OS</td>';
        $ret .= '<td>Qtd candidatos</td>';
        $ret .= '</tr>';
        while ($rs = $cursor->fetch(PDO::FETCH_ASSOC)) {
            $ret .= '<tr>';
            $ret .= '<td>' . $rs['ano'] . '</td>';
            $ret .= '<td>' . $rs['mes'] . '</td>';
            $ret .= '<td>' . $rs['no_razao_social'] . '</td>';
            $ret .= '<td>' . $rs['no_embarcacao_projeto'] . '</td>';
            $ret .= '<td>' . $rs['no_servico_resumido'] . '</td>';
            $ret .= '<td>' . $rs['nu_servico'] . '</td>';
            $ret .= '<td>' . $rs['id_tipo_acompanhamento'] . '</td>';
            $ret .= '<td>' . $rs['qtd'] . '</td>';
            $ret .= '<td>' . $rs['qtd_cand'] . '</td>';
            $ret .= '</tr>';
        }
        $ret .= "</table>";
        return $ret;
    }

    public static function LinkProcessoOs($pid_solicita_visto, $pnu_solicitacao, $ptarget = "_parent") {
        return '<a href="/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso&id_solicita_visto=' . $pid_solicita_visto . '" target="' . $ptarget . '">' . $pnu_solicitacao . '</a>';
    }

    public static function LinkDadosOs($pid_solicita_visto, $pnu_solicitacao, $ptarget = "_parent") {
        return '<a href="/operador/os_DadosOS.php?id_solicita_visto=' . $pid_solicita_visto . '&ID_SOLICITA_VISTO=' . $pid_solicita_visto . '" target="' . $ptarget . '">' . $pnu_solicitacao . '</a>';
    }

    public function getAdicioneCandidato() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$GET['id_solicita_visto'];
            $nu_candidato = cHTTP::$GET['nu_candidato'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->Recuperar();
            $os->mNU_USUARIO_CAD = cSESSAO::$mcd_usuario;
            $os->AdicionarCandidatos($nu_candidato);
            $ret['html'] = $this->getCandidatos();
            $ret['codret'] = 0;
            $ret['msg'] = 'Operação realizada com sucesso!';
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
            $ret['codret'] = -1;
            $ret['msg'] = $e->getMessage();
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
            $ret['codret'] = -1;
            $ret['msg'] =  $e->getMessage();
        }

        return json_encode($ret);
    }

    public static function ArquivosDaOS($pID_SOLICITA_VISTO) {
        $sql = "select nu_empresa, coalesce(FL_ENCERRADA,0) FL_ENCERRADA"
                . " from solicita_visto "
                . " left join status_solicitacao ss on ss.id_status_sol = solicita_visto.id_status_sol "
                . " where id_solicita_visto = " . $pID_SOLICITA_VISTO;
        $res = cAMBIENTE::ConectaQuery($sql, __FILE__);
        if ($rw = $res->fetch(PDO::FETCH_BOTH)) {
            $NU_EMPRESA = $rw['nu_empresa'];
            $FL_ENCERRADA = $rw['FL_ENCERRADA'];
        }

        $sql = "select count(*) qtd from autorizacao_candidato where id_solicita_visto = " . $pID_SOLICITA_VISTO;
        $res = cAMBIENTE::ConectaQuery($sql, __FILE__);
        $rw = $res->fetch(PDO::FETCH_BOTH);
        $qtdCand = $rw['qtd'];

        $sql = "SELECT NU_SEQUENCIAL, NO_ARQ_ORIGINAL, NO_ARQUIVO, ARQUIVOS.NU_EMPRESA, NO_TIPO_ARQUIVO, CO_TIPO_ARQUIVO, NOME_COMPLETO";
        $sql.= "  FROM ARQUIVOS ";
        $sql.= "  left join TIPO_ARQUIVO TA on TA.ID_TIPO_ARQUIVO = ARQUIVOS.TP_ARQUIVO";
        $sql.= "  left join CANDIDATO C on C.NU_CANDIDATO = ARQUIVOS.NU_CANDIDATO";
        $sql.= " where ARQUIVOS.ID_SOLICITA_VISTO = " . $pID_SOLICITA_VISTO;
        $sql.= " ORDER BY CO_TIPO_ARQUIVO, NOME_COMPLETO asc ";
        $rs = cAMBIENTE::ConectaQuery($sql, __FILE__);
        $qtdArqs = 0;
        $arquivos = "";
        while ($rw = $rs->fetch(PDO::FETCH_BOTH)) {
            $nmDir = cAMBIENTE::getUrlSol() . '/' . $rw['NU_EMPRESA'] . '/';
            $qtdArqs = $qtdArqs + 1;
            $seq = $rw['NU_SEQUENCIAL'];
            $nmArq = $rw['NO_ARQUIVO'];
            $tpArq = $rw['TP_ARQUIVO'];
            $CO_TIPO_ARQ = $rw['CO_TIPO_ARQUIVO'];
            $nmArqOrg = $rw['NO_ARQ_ORIGINAL'];
            $dtInc = $rw['DT_INCLUSAO'];
            $numAdmin = $rw['NU_ADMIN'];
            $docpagina = $nmDir . $nmArq;
            if ($rw['NOME_COMPLETO'] == '') {
                $NOME_COMPLETO = "(geral)";
            } else {
                $NOME_COMPLETO = $rw['NOME_COMPLETO'];
            }
            //('Docpagina='.$docpagina);

            $src = cAMBIENTE::getDirSol() . '/' . $rw['NU_EMPRESA'] . '/' . $rw['NO_ARQUIVO'];
            $acoes = '';
            if (file_exists($src)) {
                $NO_ARQ_ORIGINAL = '<a class="img" href="download.php?arq=' . $src . '&nmf=' . $rw['NO_ARQ_ORIGINAL'] . '">' . $rw['NO_ARQ_ORIGINAL'] . '</a>';
                $acoes .='<a class="img" href="download.php?arq=' . $src . '&nmf=' . $rw['NO_ARQ_ORIGINAL'] . '"><img src="/imagens/icons/downld.gif" title="clique para baixar esse arquivo" style="border:none" /></a>';
            } else {
                $NO_ARQ_ORIGINAL = '<span style="text-decoration:line-through;color:#cccccc;">' . $rw['NO_ARQ_ORIGINAL'] . "</span>";
            }
            if (!$FL_ENCERRADA) {
                $del = '<img';
                $del.= ' style="cursor:pointer"';
                $del.= ' title="clique para remover esse arquivo"';
                $del.= ' src="/imagens/icons/delete.png"';
                $del.= ' onclick="ajaxRemoverArquivoSol(\'' . $rw['NU_SEQUENCIAL'] . '\', \'' . str_replace('"', "", str_replace("'", "", $rw['NO_ARQ_ORIGINAL'])) . '\');"';
                $del.= ' />';
                $acoes.=$del;
            }
            $arquivos.= '<tr>';
            $arquivos.= '<td style="text-align:center;">' . $acoes . '</td>';
            $arquivos.= '<td>' . $rw['CO_TIPO_ARQUIVO'] . '</td>';
            $arquivos.= '<td>' . $NO_ARQ_ORIGINAL . '</td>';
            $arquivos.= '<td>' . $NOME_COMPLETO . '</td>';
            $arquivos.= "</tr>";
        }
        $tabela = '<form id="novoArquivoOS" method="post" action="/LIB/ARQUIVO_SOL_INCLUIR.php" enctype="multipart/form-data" target="_blank">';
        $tabela .= cINTERFACE::inputHidden('NU_EMPRESA', $NU_EMPRESA);
        $tabela .= cINTERFACE::inputHidden('ID_SOLICITA_VISTO', $pID_SOLICITA_VISTO);
        $tabela .= '<table class="edicao dupla" style="width:auto">';
        $tabela .= '<col width="80px"></col><col width="120px"></col><col width="auto"></col><col width="auto"></col>';
        $tabela .= '<tr><th style="text-align:center;">Ações</th><th>Tipo</th><th>Arquivo</th><th>Candidato</th></tr>';
        $tabela .= $arquivos;
        if (!$FL_ENCERRADA) {
            //	$tabela .= '<tr><td><img src="../imagens/icons/add.png" onclick="validaFormArquivo();" style="cursor:pointer" title="Clique para adicionar o arquivo" style="vertical-align:middle"/><input type="file" id="ARQUIVOOS" name="ARQUIVOOS" style="width:auto"/></td>';
            //	$tabela .= '<td>'.cINTERFACE::RenderizeCombo(new cCAMPO("ID_TIPO_ARQUIVO_OS", "Tipo", 3,"","", "", "", 17 )).'</td></tr>';
            //	$tabela .= '<tr class="limpa"><td colspan="2" style="text-align:center"><input type="button" value="Refresh" onclick="carregarArquivos();" style="width:auto"/></td></tr>';
            $tabela .= '<tr>';
            $tabela .= '<td style="text-align:center;"><img src="../imagens/icons/add.png" onclick="validaFormArquivo();" style="cursor:pointer" title="Clique para adicionar o arquivo" style="vertical-align:middle"/></td>';
            $combo = cINTERFACE::RenderizeCombo(new cCAMPO("ID_TIPO_ARQUIVO_OS", "Tipo", 3, "", "", "", "auto", 17));
            $tabela .= '<td>' . $combo . '</td>';
            $tabela .= '<td><input type="file" id="ARQUIVOOS" name="ARQUIVOOS" style="width:auto"/></td>';
            $comboCandidatos = cFABRICA_CAMPO::NovoCombo("NU_CANDIDATO", "", cCOMBO::cmbCANDIDATO_OS);
            if ($qtdCand == 1) {
                $comboCandidatos->mPermiteDefault = false;
            }
            $comboCandidatos->mExtra .= " and ID_SOLICITA_VISTO = " . $pID_SOLICITA_VISTO;
            $tabela .= '<td>' . cINTERFACE::RenderizeCampo($comboCandidatos) . '</td>';
            $tabela .= '</tr>';
        }
        $tabela .= '</table>';
        if (!$FL_ENCERRADA) {
            $tabela .= '<input type="button" value="Refresh" onclick="carregarArquivos();" style="width:auto"/>';
        }
        $tabela .= '</form>';
        $tabela .= "<script>";
        $tabela .= "function validaFormArquivo(pNomeArquivo){";
        $tabela .= "	if($('#ARQUIVOOS').val()==''){";
        $tabela .= "		jAlert('Informe o arquivo a ser carregado');";
        $tabela .= "	}";
        $tabela .= "	else{";
        $tabela .= "		if($('#CMP_ID_TIPO_ARQUIVO_OS').val()==''){";
        $tabela .= "			jAlert('Informe o tipo do arquivo');}";
        $tabela .= "		else{";
        $tabela .= "			document.forms['novoArquivoOS'].submit();";
        $tabela .= "		}";
        $tabela .= "	}";
        $tabela .= "}";
        $tabela .= "</script>";
        return $tabela;
    }

    public static function ArquivosDaOSResumido($pID_SOLICITA_VISTO) {
        $sql = "select nu_empresa, coalesce(FL_ENCERRADA,0) FL_ENCERRADA"
                . " from solicita_visto "
                . " left join status_solicitacao ss on ss.id_status_sol = solicita_visto.id_status_sol "
                . " where id_solicita_visto = " . $pID_SOLICITA_VISTO;
        $res = cAMBIENTE::ConectaQuery($sql, __FILE__);
        if ($rw = $res->fetch(PDO::FETCH_BOTH)) {
            $NU_EMPRESA = $rw['nu_empresa'];
            $FL_ENCERRADA = $rw['FL_ENCERRADA'];
        }

        $sql = "SELECT NU_SEQUENCIAL, NO_ARQ_ORIGINAL, NO_ARQUIVO, ARQUIVOS.NU_EMPRESA, NO_TIPO_ARQUIVO, CO_TIPO_ARQUIVO, NOME_COMPLETO";
        $sql.= "  FROM ARQUIVOS ";
        $sql.= "  left join TIPO_ARQUIVO TA on TA.ID_TIPO_ARQUIVO = ARQUIVOS.TP_ARQUIVO";
        $sql.= "  left join CANDIDATO C on C.NU_CANDIDATO = ARQUIVOS.NU_CANDIDATO";
        $sql.= " where ARQUIVOS.ID_SOLICITA_VISTO = " . $pID_SOLICITA_VISTO;
        $sql.= " ORDER BY CO_TIPO_ARQUIVO, NOME_COMPLETO asc ";
        $rs = conectaQuery($sql, __FILE__);
        $qtdArqs = 0;
        $arquivos = "";
        while ($rw = mysql_fetch_array($rs)) {
            $nmDir = cAMBIENTE::getUrlSol() . '/' . $rw['NU_EMPRESA'] . '/';
            $qtdArqs = $qtdArqs + 1;
            $seq = $rw['NU_SEQUENCIAL'];
            $nmArq = $rw['NO_ARQUIVO'];
            $tpArq = $rw['TP_ARQUIVO'];
            $CO_TIPO_ARQ = $rw['CO_TIPO_ARQUIVO'];
            $nmArqOrg = $rw['NO_ARQ_ORIGINAL'];
            $dtInc = $rw['DT_INCLUSAO'];
            $numAdmin = $rw['NU_ADMIN'];
            $docpagina = $nmDir . $nmArq;
            if ($rw['NOME_COMPLETO'] == '') {
                $NOME_COMPLETO = "(geral)";
            } else {
                $NOME_COMPLETO = $rw['NOME_COMPLETO'];
            }
            //error_log('Docpagina='.$docpagina);

            $src = cAMBIENTE::getDirSol() . '/' . $rw['NU_EMPRESA'] . '/' . $rw['NO_ARQUIVO'];
            $acoes = '';
            if (file_exists($src)) {
                $NO_ARQ_ORIGINAL = '<a class="img" href="download.php?arq=' . $src . '&nmf=' . $rw['NO_ARQ_ORIGINAL'] . '">' . $rw['NO_ARQ_ORIGINAL'] . '</a>';
                $acoes .='<a class="img" href="download.php?arq=' . $src . '&nmf=' . $rw['NO_ARQ_ORIGINAL'] . '"><img src="/imagens/icons/downld.gif" title="clique para baixar esse arquivo" style="border:none" /></a>';
            } else {
                $NO_ARQ_ORIGINAL = '<span style="text-decoration:line-through;color:#cccccc;">' . $rw['NO_ARQ_ORIGINAL'] . "</span>";
            }
            if (!$FL_ENCERRADA) {
                $del = '<img';
                $del.= ' style="cursor:pointer"';
                $del.= ' title="clique para remover esse arquivo"';
                $del.= ' src="/imagens/icons/delete.png"';
                $del.= ' onclick="ajaxRemoverArquivoSol(\'' . $rw['NU_SEQUENCIAL'] . '\', \'' . str_replace('"', "", str_replace("'", "", $rw['NO_ARQ_ORIGINAL'])) . '\');"';
                $del.= ' />';
                $acoes.=$del;
            }
            $arquivos.= '<tr>';
            $arquivos.= '<td style="text-align:center;">' . $acoes . '</td>';
            $arquivos.= '<td>' . $rw['CO_TIPO_ARQUIVO'] . '</td>';
            $arquivos.= '<td>' . $NO_ARQ_ORIGINAL . '</td>';
//			$arquivos.= '<td>'.$NOME_COMPLETO.'</td>';
            $arquivos.= "</tr>";
        }
        $tabela = '<form id="novoArquivoOS" method="post" action="/LIB/ARQUIVO_SOL_INCLUIR.php" enctype="multipart/form-data" target="_blank">';
        $tabela .= cINTERFACE::inputHidden('NU_EMPRESA', $NU_EMPRESA);
        $tabela .= cINTERFACE::inputHidden('ID_SOLICITA_VISTO', $pID_SOLICITA_VISTO);
        $tabela .= '<table class="edicao dupla" style="width:auto">';
        $tabela .= '<col width="80px"></col><col width="120px"></col><col width="auto"></col><col width="auto"></col>';
//		$tabela .= '<tr><th style="text-align:center;">Ações</th><th>Tipo</th><th>Arquivo</th><th>Candidato</th></tr>';
        $tabela .= $arquivos;
        if (!$FL_ENCERRADA) {
            //	$tabela .= '<tr><td><img src="../imagens/icons/add.png" onclick="validaFormArquivo();" style="cursor:pointer" title="Clique para adicionar o arquivo" style="vertical-align:middle"/><input type="file" id="ARQUIVOOS" name="ARQUIVOOS" style="width:auto"/></td>';
            //	$tabela .= '<td>'.cINTERFACE::RenderizeCombo(new cCAMPO("ID_TIPO_ARQUIVO_OS", "Tipo", 3,"","", "", "", 17 )).'</td></tr>';
            //	$tabela .= '<tr class="limpa"><td colspan="2" style="text-align:center"><input type="button" value="Refresh" onclick="carregarArquivos();" style="width:auto"/></td></tr>';
            $tabela .= '<tr>';
            $tabela .= '<td style="text-align:center;"><img src="../imagens/icons/add.png" onclick="validaFormArquivo();" style="cursor:pointer" title="Clique para adicionar o arquivo" style="vertical-align:middle"/></td>';
            $combo = cINTERFACE::RenderizeCombo(new cCAMPO("ID_TIPO_ARQUIVO_OS", "Tipo", 3, "", "", "", "auto", 17));
            $tabela .= '<td>' . $combo . '</td>';
            $tabela .= '<td><input type="file" id="ARQUIVOOS" name="ARQUIVOOS" style="width:auto"/></td>';
            $comboCandidatos = cFABRICA_CAMPO::NovoCombo("NU_CANDIDATO", "", cCOMBO::cmbCANDIDATO_OS);

            $comboCandidatos->mExtra .= " and ID_SOLICITA_VISTO = " . $pID_SOLICITA_VISTO;
            $tabela .= '<td>' . cINTERFACE::RenderizeCampo($comboCandidatos) . '</td>';
            $tabela .= '</tr>';
        }
        $tabela .= '</table>';
        if (!$FL_ENCERRADA) {
            $tabela .= '<input type="button" value="Refresh" onclick="carregarArquivos();" style="width:auto"/>';
        }
        $tabela .= '</form>';
        $tabela .= "<script>";
        $tabela .= "function validaFormArquivo(pNomeArquivo){";
        $tabela .= "	if($('#ARQUIVOOS').val()==''){";
        $tabela .= "		jAlert('Informe o arquivo a ser carregado');";
        $tabela .= "	}";
        $tabela .= "	else{";
        $tabela .= "		if($('#CMP_ID_TIPO_ARQUIVO_OS').val()==''){";
        $tabela .= "			jAlert('Informe o tipo do arquivo');}";
        $tabela .= "		else{";
        $tabela .= "			document.forms['novoArquivoOS'].submit();";
        $tabela .= "		}";
        $tabela .= "	}";
        $tabela .= "}";
        $tabela .= "</script>";
        return $tabela;
    }

    public function CorrigeProcessosOs() {
        $i = 0;
        $sql = "select sv.id_solicita_visto, sv.nu_solicitacao, sv.dt_solicitacao, c.nome_completo, e.no_razao_social, s.no_servico_resumido, p.codigo, p.nu_servico
                from solicita_visto sv
                join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto
                join servico s on s.nu_servico = sv.nu_servico
                join candidato c on c.nu_candidato = ac.nu_candidato
                left join empresa e on e.nu_empresa = c.nu_empresa
                left join processo_mte p on p.id_solicita_visto = sv.id_solicita_visto and p.cd_candidato = ac.nu_candidato
                where s.id_tipo_acompanhamento = 1
                and p.codigo is null
                and coalesce(sv.fl_sistema_antigo , 0 ) = 0
                order by nu_solicitacao desc
                ";
        $res = cAMBIENTE::ConectaQuery($sql, __FILE__);
        print "<br/><br/>==============AUTORIZAÇÕES";
        while ($rw = $res->fetch(PDO::FETCH_ASSOC)) {
            $os = new cORDEMSERVICO();
            $os->Recuperar($rw['id_solicita_visto']);
            $i++;
            print "<br/>" . $i . "- Adicionando processos na OS " . $rw['nu_solicitacao'];
            $os->AdicionaProcessoATodosCandidatos();
            print " ... OK";
        }
        $i = 0;
        $sql = "select sv.id_solicita_visto, sv.nu_solicitacao, sv.dt_solicitacao, c.nome_completo, e.no_razao_social, s.no_servico_resumido, p.codigo, p.nu_servico
                from solicita_visto sv
                join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto
                join servico s on s.nu_servico = sv.nu_servico
                join candidato c on c.nu_candidato = ac.nu_candidato
                left join empresa e on e.nu_empresa = c.nu_empresa
                left join processo_prorrog p on p.id_solicita_visto = sv.id_solicita_visto and p.cd_candidato = ac.nu_candidato
                where s.id_tipo_acompanhamento = 3
                and p.codigo is null
                and coalesce(sv.fl_sistema_antigo , 0 ) = 0
                order by nu_solicitacao desc
                ";
        $res = cAMBIENTE::ConectaQuery($sql, __FILE__);
        print "<br/><br/>==============PRORROGACOES";
        while ($rw = $res->fetch(PDO::FETCH_ASSOC)) {
            $os = new cORDEMSERVICO();
            $os->Recuperar($rw['id_solicita_visto']);
            $i++;
            print "<br/>" . $i . "-Adicionando processos na OS " . $rw['nu_solicitacao'];
            $os->AdicionaProcessoATodosCandidatos();
            print " ... OK";
        }
        $i = 0;
        $sql = "select sv.id_solicita_visto, sv.nu_solicitacao, sv.dt_solicitacao, c.nome_completo, e.no_razao_social, s.no_servico_resumido, p.codigo, p.nu_servico
                from solicita_visto sv
                join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto
                join servico s on s.nu_servico = sv.nu_servico
                join candidato c on c.nu_candidato = ac.nu_candidato
                left join empresa e on e.nu_empresa = c.nu_empresa
                left join processo_regcie p on p.id_solicita_visto = sv.id_solicita_visto and p.cd_candidato = ac.nu_candidato
                where s.id_tipo_acompanhamento = 4
                and p.codigo is null
                and coalesce(sv.fl_sistema_antigo , 0 ) = 0
                order by nu_solicitacao desc
                ";
        $res = cAMBIENTE::ConectaQuery($sql, __FILE__);
        print "<br/><br/>==============REGISTROS";
        while ($rw = $res->fetch(PDO::FETCH_ASSOC)) {
            $os = new cORDEMSERVICO();
            $os->Recuperar($rw['id_solicita_visto']);
            $i++;
            print "<br/>" . $i . "-Adicionando processos na OS " . $rw['nu_solicitacao'];
            $os->AdicionaProcessoATodosCandidatos();
            print " ... OK";
        }
        $i = 0;
        $sql = "select sv.id_solicita_visto, sv.nu_solicitacao, sv.dt_solicitacao, c.nome_completo, e.no_razao_social, s.no_servico_resumido, p.codigo, p.nu_servico
                from solicita_visto sv
                join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto
                join servico s on s.nu_servico = sv.nu_servico
                join candidato c on c.nu_candidato = ac.nu_candidato
                left join empresa e on e.nu_empresa = c.nu_empresa
                left join processo_emiscie p on p.id_solicita_visto = sv.id_solicita_visto and p.cd_candidato = ac.nu_candidato
                where s.id_tipo_acompanhamento = 5
                and p.codigo is null
                and coalesce(sv.fl_sistema_antigo , 0 ) = 0
                order by nu_solicitacao desc
                ";
        $res = cAMBIENTE::ConectaQuery($sql, __FILE__);
        print "<br/><br/>==============COLETA";
        while ($rw = $res->fetch(PDO::FETCH_ASSOC)) {
            $os = new cORDEMSERVICO();
            $os->Recuperar($rw['id_solicita_visto']);
            $i++;
            print "<br/>" . $i . "-Adicionando processos na OS " . $rw['nu_solicitacao'];
            $os->AdicionaProcessoATodosCandidatos();
            print " ... OK";
        }
        $i = 0;
        $sql = "select sv.id_solicita_visto, sv.nu_solicitacao, sv.dt_solicitacao, c.nome_completo, e.no_razao_social, s.no_servico_resumido, p.codigo, p.nu_servico
                from solicita_visto sv
                join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto
                join servico s on s.nu_servico = sv.nu_servico
                join candidato c on c.nu_candidato = ac.nu_candidato
                left join empresa e on e.nu_empresa = c.nu_empresa
                left join processo_cancel p on p.id_solicita_visto = sv.id_solicita_visto and p.cd_candidato = ac.nu_candidato
                where s.id_tipo_acompanhamento = 6
                and p.codigo is null
                and coalesce(sv.fl_sistema_antigo , 0 ) = 0
                order by nu_solicitacao desc
                ";
        $res = cAMBIENTE::ConectaQuery($sql, __FILE__);
        print "<br/><br/>==============CANCELAMENTOS";
        while ($rw = $res->fetch(PDO::FETCH_ASSOC)) {
            $os = new cORDEMSERVICO();
            $os->Recuperar($rw['id_solicita_visto']);
            $i++;
            print "<br/>" . $i . "-Adicionando processos na OS " . $rw['nu_solicitacao'];
            $os->AdicionaProcessoATodosCandidatos();
            print " ... OK";
        }
    }

    /**
     * Renderiza tabela de candidatos da OS, pelo que está na tabela ou a partir dos IDs (para as novas)
     */
    public function tabelaDeCandidatos() {
        $rep = new cREPOSITORIO_OS;

        $candidatos = "";
    }

    public function excluaCandidato() {
        $id_solicita_visto = $_GET['id_solicita_visto'];
        $nu_candidato = $_GET['nu_candidato'];
        $rep = new cREPOSITORIO_OS;
        try {
            $rep->excluaCandidato($id_solicita_visto, $nu_candidato);
            $msg = "Candidato excluído com sucesso!";
        } catch (Exception $ex) {
            $msg = "Não foi possível excluir o candidato: " . $ex->getMessage();
        }
        $html = utf8_encode($rep->candidatos($id_solicita_visto));
        return json_encode(array('msg' => $msg, 'html' => $html));
    }


    public function certidao_tramite(){
        $qtd = cHTTP::ParametroValido('qtd');
        $inicio = cHTTP::ParametroValido('inicio');

        $sql = "select os.id_solicita_visto, os.nu_solicitacao"
                . ", s.no_servico_resumido"
                . ", pp.nu_protocolo"
                . ", e.no_razao_social"
                . " from solicita_visto os"
                . " join servico s on s.nu_servico = os.nu_servico"
                . " join processo_prorrog pp on pp.id_solicita_visto = os.id_solicita_visto"
                . " join empresa e on e.nu_empresa = os.nu_empresa"
                . " where pp.nu_protocolo is not null"
                . " and s.fl_certidao_tramite = 1 "
                . " order by os.id_solicita_visto desc"
                . " limit ".$inicio.",".$qtd;

        $res = cAMBIENTE::$db_pdo->query($sql);
        $regs = $res->fetchAll(PDO::FETCH_ASSOC);

        foreach($regs as &$reg){
            if (!strstr($reg['nu_protocolo'], '.') ){
                $nu = trim($reg['nu_protocolo']);
                $reg['nu_protocolo'] = substr($nu, 0,5).'.'.substr($nu,5,6).'/'.substr($nu,11,4).'-'.substr($nu,15,2);
            }
        }
        $data['menu'] = cLAYOUT::soMenu();

        $data['regs'] = $regs;
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        return cTWIG::Render('intranet/os/os_certidao_tramite_list.html', $data);
    }

    public function excluir(){
        $rep = new cREPOSITORIO_OS;
        $id_solicita_visto = $_GET['id_solicita_visto'];
        $os = new cORDEMSERVICO;
        $os->Recuperar($id_solicita_visto);
        $os->RecuperarCandidatos();
        try{
            cAMBIENTE::$db_pdo->beginTransaction();
                $rep->excluir($os);
            cAMBIENTE::$db_pdo->commit();
            $_SESSION['msg'] = 'Ordem de serviço excluída com sucesso';
            cHTTP::RedirecionaLinkInterno('/intranet/geral/os_listar.php');
        }
        catch(Exception $ex){
            cAMBIENTE::$db_pdo->rollBack();
            $_SESSION['msg'] = "Não foi possível excluir a OS: ".$ex->getMessage();
            cHTTP::RedirecionaLinkInterno('/operador/os_DadosOS.php?&ID_SOLICITA_VISTO='.$id_solicita_visto.'&id_solicita_visto='.$id_solicita_visto);
        }
    }

}
