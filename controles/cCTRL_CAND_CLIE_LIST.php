<?php

class cCTRL_CAND_CLIE_LIST extends cCTRL_MODELO_TWIG_LIST {

    public function __construct() {
        $this->nomeTemplate = 'cliente/listagem.html';
    }

    public function InicializeModelo() {
        $this->modelo = new cCANDIDATO();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_CAND_CLIE_LIST();
        $menu = cmenu::NovoMenuCliente();
        $this->template->set_menuEsquerdo($menu);
        $menu = cmenu::NovoMenuClienteSystem();
        $this->template->set_menuDireito($menu);
    }

    public function CarregueTemplatePeloHTTP() {
        parent::CarregueTemplatePeloHTTP();
        $this->template->mFiltro['NU_EMPRESA']->mValor = cSESSAO::$mcd_empresa;
        cHTTP::setLang('en_us');
    }

    public function complementaData() {
        parent::complementaData();
        $empresa = new cEMPRESA();
        $empresa->mNU_EMPRESA = cSESSAO::$mcd_empresa;
        $empresa->RecuperePeloId();
        if ($empresa->mnu_empresa_prestadora == ''){
            $prestadora = 1;
        } else {
            $prestadora = $empresa->mnu_empresa_prestadora;
        }
        $_SESSION['nu_empresa_prestadora'] = $prestadora;
    }
    
    public function get_cursorListagem(cTEMPLATE_LISTAGEM_TWIG $ptmpl) {
//        if (intval($this->template->mFiltro['NU_EMPRESA']->mValor) > 0 && (intval($this->template->mFiltro['NU_EMBARCACAO_PROJETO']->mValor) > 0 || $this->template->mFiltro['NOME_COMPLETO']->mValor != '' )) {
        if (intval($this->template->mFiltro['NU_EMPRESA']->mValor) > 0  ) {
            if (intval($this->template->mFiltro['NU_EMBARCACAO_PROJETO']->mValor) > 0) {
                $this->template->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = false;
                $this->template->mCols = array('70px', 'auto', 'auto', '90px', '70px', '80px', 'auto', '80px', '80px');
            } else {
                $this->template->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = true;
                $this->template->mCols = array('70px', 'auto', 'auto', 'auto', '90px', '70px', '100px', 'auto', '100px', '100px');
            }
            $modelo = $this->modelo;
            $cursor = $modelo->Cursor_CAND_CLIE_LIST($this->template->mFiltro, $this->template->get_ordenacao());
            return $cursor;
        } else {
            cNOTIFICACAO::singleton('Please select one vessel/project name', cNOTIFICACAO::TM_SUCCESS);
        }
    }
    
}
