<?php 
class cCTRL_PACT_LIST extends cCTRL_MODELO_LIST{
	public function InicializeModelo(){
		$this->modelo = new cPACOTE();
	}
	
	public function InicializeTemplate(){
		$this->template = new cTMPL_PACT_LIST();
	}

	public function AcessoLiberado()
	{
		if(Acesso::permitido(Acesso::tp_Sistema_Tabelas_Servico)){
			return true;
		}
		else{
			cHTTP::$SESSION['msg'] = "Você não tem acesso a essa função.";
			cHTTP::RedirecioneParaExterno('/intranet/geral/principal.php');
		}		
	}

}
