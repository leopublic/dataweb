<?php

/**
 * Description of cCTRL_PROCESSO_PRORROG
 *
 * @author leonardo
 */
class cCTRL_PROCESSO_COLETA_CIE extends cCTRL_PROCESSO{
	public function FormEdicaoOS(cORDEMSERVICO $pOS){
		$processo = new cprocesso_coleta_cie();
		$processo->RecupereSePelaOs($pOS->mID_SOLICITA_VISTO);
		$tela = new cTEMPLATE_PROCESSO_COLETACIE_EDIT();
		if(cHTTP::HouvePost() && cHTTP::get_telaPostada() == get_class($tela)){
			$tela->CarregueDoHTTP();
			try{
				switch($tela->get_acaoPostada()){
					case "SALVAR":
						$processo->SalvarPelaOs($tela->getValorCampo('nu_rne')
											  , $tela->getValorCampo('dt_atendimento')
											  , $tela->getValorCampo('dt_expedicao')
											  , $tela->getValorCampo('dt_validade')
											  , $tela->getValorCampo('observacao')
									);
						cHTTP::$SESSION['msg'] = 'Operação realizada com sucesso';
						break;
					case "SALVAR_FECHAR":
						$processo->SalvarPelaOs($tela->getValorCampo('nu_rne')
											  , $tela->getValorCampo('dt_atendimento')
											  , $tela->getValorCampo('dt_expedicao')
											  , $tela->getValorCampo('dt_validade')
											  , $tela->getValorCampo('observacao')
									);
						$pOS->Fechar();
						cHTTP::$SESSION['msg'] = 'Operação realizada com sucesso';
						break;
					default:
						throw new Exception('Operação "'.$tela->get_acaoPostada().'" não esperada.');
				}
			}
			catch(cERRO_CONSISTENCIA $e){
				cHTTP::$SESSION['msg'] = $e->mMsg;				
			}
			catch(Exception $e){
				cHTTP::$SESSION['msg'] = $e->getMessage();
			}
		}
		if($pOS->mReadonly){
			$tela->ProtejaTodosCampos();
			$tela->mAcoes['SALVAR']->mVisivel = false;
			$tela->mAcoes['SALVAR_FECHAR']->mVisivel = false;
		}
		$tela->CarregueDoOBJETO($processo);
		return $tela->HTML_form();
	}
}
