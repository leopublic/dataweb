<?php
class cCTRL_FORM_TVS extends cCTRL_MODELO{
	public function ComplementarLinhaListagem(&$pTmpl) {

	}
	public function Liste(){
		return parent::Liste(new cTMPL_FORM_TVS_LISTAR(), new cFORM_TVS());
	}

	public function Edite() {
		$tmpl = new cTMPL_CANDIDATO_TVS();
		$tvs = new cform_tvs();
		// Obtem os parametros
		$id_solicita_visto = cHTTP::ParametroValido('id_solicita_visto');
		$NU_CANDIDATO = cHTTP::ParametroValido('nu_candidato');
		if ($NU_CANDIDATO == '' ){
			$NU_CANDIDATO = cHTTP::ParametroValido('CMP_nu_candidato');
		}
		$OS = new cORDEMSERVICO();
		$OS->mID_SOLICITA_VISTO = $id_solicita_visto;
		$OS->Recuperar();
		$cursor = $OS->CursorDeCandidatos();
		$tmplAbaCands = new cTMPL_ABA_CANDIDATOS();
		$tmplAbaCands->CarregueDoHTTP();
		while ($rs = $cursor->fetch(PDO::FETCH_BOTH)){
			if ($NU_CANDIDATO == '' ){
				$NU_CANDIDATO = $rs['NU_CANDIDATO'];
				cHTTP::$GET['nu_candidato'] =  $NU_CANDIDATO;
			}
			$tmplAbaCands->AdicionaAbaCandidato($rs['NU_CANDIDATO'], $rs['NOME_COMPLETO']);
		}
		$tmplAbaCands->RepliqueChavesParaAbasAjax();

		$cursor = $tvs->RecuperePeloCandidatoOs($NU_CANDIDATO, $id_solicita_visto);
		// Monta o conteudo da tela
		if (! cHTTP::HouvePost()){
			// Verifica se já existe um formulario para essa OS candidato
			$tmpl->CarregueDoRecordset($cursor);
			$tmpl->CarregueDoOBJETO($tvs);
		}
		else{
			if(cHTTP::ParametroValido('CMP___acao') == "SALVAR_E_GERAR" || cHTTP::ParametroValido('CMP___acao') == "GERAR"){
				$xScript = "window.open('/paginas/carregueSemMenu.php?controller=cCTRL_FORM_TVS&metodo=GerePDF&ftvs_id=".$tvs->ftvs_id."');";
				cHTTP::AdicionarScript($xScript);
				cHTTP::$POST['CMP___acao'] = 'SALVAR'; //Gambiarra para provocar o tratamento do salvar pelo método padrão
			}
			if(cHTTP::ParametroValido('CMP___acao') == "SALVAR_E_GERAR"){
				cHTTP::$POST['CMP___acao'] = 'SALVAR'; //Gambiarra para provocar o tratamento do salvar pelo método padrão
			}
			$cand = new cCANDIDATO();
			$cand->Recuperar($NU_CANDIDATO);
			$tmpl->CarregueDoHTTP();
			$tmpl->AtribuaAoOBJETO($cand);
			$cand->Atualizar();
		}
		// Renderiza a tela
		$html = parent::Edite($tmpl, new cFORM_TVS(), "", "", "id_solicita_visto=".$id_solicita_visto."&nu_candidato=".$NU_CANDIDATO);

		$tmplAba = new cTMPL_ABA_OS;
		$tmplAba->mCampo['ID_SOLICITA_VISTO']->mValor = cHTTP::ParametroValido('id_solicita_visto');
		$tmplAba->mCampo['id_solicita_visto']->mValor = cHTTP::ParametroValido('id_solicita_visto');
		$tmplAba->mCampo['NU_CANDIDATO']->mValor = cHTTP::ParametroValido('nu_candidato');
		$tmplAba->RepliqueChavesParaAbasAjax();

		// Adiciona a chave se houver como parametro nas abas
		$tmplAba->mTitulo = 'Ordem de serviço '.$OS->mNU_SOLICITACAO;
		//$ret .= cINTERFACE::formTemplate($tmplAba);
		$ret .= cINTERFACE::RenderizeTitulo($tmplAba, false, cINTERFACE::titPAGINA);
		$ret .= '<div class="conteudoInterno">';
		$ret .= '<div id="tabs">';
		$ret .= cINTERFACE::RenderizeIndiceDeAbas($tmplAba, 'TVS');
		$ret .= '<div id="abaAtiva">';
		$ret .= '<div id="tabsCands">';
		$ret .= cINTERFACE::RenderizeIndiceDeAbas($tmplAbaCands, $NU_CANDIDATO, 'tabsCands');
		$ret .= '<div id="abaAtiva">';
		$ret .= $html;
		$ret .= '</div>';
		$ret .= '</div>';
		$ret .= '</div>';
		$ret .= '</form>';
		$ret .= $tmplAba->TratamentoPadraoDeMensagens();
		return $ret;
	}

	public function GerePDF() {
		$tmpl = new cTMPL_CANDIDATO_TVS();
		$tvs = new cform_tvs();
		// Monta o conteudo da tela
		$cursor = $tvs->RecuperePeloId(cHTTP::ParametroValido('ftvs_id'));
		$tmpl->CarregueDoRecordset($cursor);
		$html = $tmpl->RenderizeFormularioPdf();
		$html = cBANCO::SemCaracteresEspeciaisMS($html);

		$mpdf = new mPDF("pt_br", "A4", "", "" , 10,10,7,10 );
		$mpdf->img_dpi = 100;
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
}
