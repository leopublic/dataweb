<?php

/**
 * Metodos para atualização de atributos de ORDENS DE SERVICO
 */
class cCTRL_UPDT_SOLV {

    public function de_observacao() {
        $ret = array();
        try {
            $valor = cHTTP::$POST['valor'];
            ;
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->mde_observacao = $valor;
            $os->SalvarObservacao();

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function de_observacao_bsb() {
        $ret = array();
        try {
            $valor = cHTTP::$POST['valor'];
            ;
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->mde_observacao_bsb = $valor;
            $os->SalvarObservacaoBsb($cd_usuario);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function dt_pre_cadastro() {
        $ret = array();
        try {
            $dt_pre_cadastro = cHTTP::$POST['valor'];
            $codigo = cHTTP::$POST['codigo'];
            $cd_usuario_pre_cadastro = cHTTP::$POST['cd_usuario_pre_cadastro'];
            cprocesso_prorrog::Atualiza_dt_pre_cadastro($codigo, $dt_pre_cadastro, $cd_usuario_pre_cadastro);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function resc_nu_numero_mv() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $resc_nu_numero_mv = cHTTP::$POST['valor'];

            $os = cORDEMSERVICO::FabriquePeloId($id_solicita_visto);

            if (trim($resc_nu_numero_mv) != '') {
                cFINANCEIRO::CorrigirResumoPara(cresumo_cobranca::FabriquePeloNumero($resc_nu_numero_mv, $os->get_nu_empresa_requerente()), $os);
                cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
            }
        } catch (cERRO_CONSISTENCIA $e) {
            $valor_orig = cHTTP::$POST['valor_orig'];
            if ($valor_orig == '') {
                $valor_orig = ' ';
            }
            $ret['novoConteudo'] = $valor_orig;
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function observacao() {
        $ret = array();
        try {
            $valor = cHTTP::$POST['valor'];
            ;
            $codigo = cHTTP::$POST['codigo'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            cprocesso_prorrog::Atualiza_observacao($codigo, $valor, $cd_usuario);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function IndicaPendenciaBSB() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->IndicaPendenciaBSB($cd_usuario);
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        $links = 'Pendente';
        $ret['novoConteudo'] = $links;
        return json_encode($ret);
    }

    public function IndicaAceitaBSB() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->IndicaAceitaBSB($cd_usuario);
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        $links = 'Aceita';
        $ret['novoConteudo'] = $links;
        return json_encode($ret);
    }

    public function LiberaPendenciaBSB() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->LiberaPendenciaBSB($cd_usuario);
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        $links = 'Não cadastrada';
        $ret['novoConteudo'] = $links;
        return json_encode($ret);
    }

    public function Devolver() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->Devolver($cd_usuario);
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        $links = 'devolvida';
        $ret['novoConteudo'] = $links;
        return json_encode($ret);
    }

    public function EnviadoDigital() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->EnviadoDigital($cd_usuario);
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        $links = 'enviada digital';
        $ret['novoConteudo'] = $links;
        return json_encode($ret);
    }

    public function Pendente() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->AtualizarStatus('2');
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        $links = 'pendente';
        $ret['novoConteudo'] = $links;
        return json_encode($ret);
    }

    public function Desfaturar() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->Desfaturar();
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        $links = 'Liberada';
        $ret['novoConteudo'] = $links;
        return json_encode($ret);
    }

    public function LibereParaCobranca() {
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->LibereParaCobranca(cSESSAO::$mcd_usuario);
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        $links = 'Liberada para cobrança';
        $ret['novoConteudo'] = $links;
        return json_encode($ret);
    }

    public function AtualizeCampoPadrao() {
        $ret = array();
        try {
            $valor = cHTTP::$POST['valor'];
            ;
            $chave = cHTTP::$POST['chave'];
            $nome_campo = cHTTP::$POST['xnome_campo'];
            $tipo_campo = cHTTP::$POST['tipo_campo'];
            cORDEMSERVICO::AtualizeCampoPadrao($chave, $nome_campo, $valor, $tipo_campo);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function AlterarStatus() {
        $ret = array();
        $novoConteudo = '';
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $id_status_sol = cHTTP::$POST['id_status_sol'];
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->AtualizarStatus($id_status_sol, $cd_usuario);
            $status = new cSTATUS_SOLICITACAO();
            $status->mID_STATUS_SOL = $id_status_sol;
            $status->RecuperePeloId();
            $novoConteudo = utf8_encode($status->mNO_STATUS_SOL_RES);
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $ret['novoConteudo'] = $novoConteudo;
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function AtualizaNumeroProcesso(){
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $numero = cHTTP::$POST['valor'];

            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->Recuperar();
            $os->AtualizaNumeroProcesso($numero, $cd_usuario);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
            $ret['novoConteudo'] = ' ';
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
            $ret['novoConteudo'] = ' ';
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];

        return json_encode($ret);

    }

    public function AtualizaNumeroProcessoDigital(){
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $numero = cHTTP::$POST['valor'];

            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->Recuperar();
            $os->AtualizaNumeroProcessoDigital($numero, $cd_usuario);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
            $ret['novoConteudo'] = ' ';
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
            $ret['novoConteudo'] = ' ';
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];

        return json_encode($ret);

    }

    public function AtualizaDtRequerimento(){
        $ret = array();
        try {
            $id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $data = cHTTP::$POST['valor'];

            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os->Recuperar();
            $os->AtualizaDtRequerimento($data, $cd_usuario);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);

    }
}
