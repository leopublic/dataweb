<?php 
class cCTRL_CAND_PIT_LIST extends cCTRL_MODELO_LIST{
	public function InicializeModelo()
	{
		$this->modelo = new cCANDIDATO();
	}
	
	public function InicializeTemplate()
	{
		$this->template = new cTMPL_CAND_PIT_LIST();
		if(isset(cHTTP::$SESSION['chaves'])){
			$this->template->chavesSelecionadas = cHTTP::$SESSION['chaves'];
		}
	}
	
	public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) 
	{	
		if($this->template->getValorFiltro('NU_EMPRESA') == '' && $this->template->getValorFiltro('NU_EMBARCACAO_PROJETO') == '' && $this->template->getValorFiltro('nome_completo') == ''){
			$this->template->setValorFiltro('situacao', '1');
		}
		
		if($this->template->getValorFiltro('situacao') != ''){
			$this->template->mFiltro['situacao']->mWhere = " cand_fl_pit = ".$this->template->getValorFiltro('situacao');
		}
		
		$this->template->mFiltro['mes']->mWhere = '1=1';
		$this->template->mFiltro['ano']->mWhere = '1=1';
		
		return cCANDIDATO::CursorPit($this->template->mFiltro, $this->template->get_ordenacao());
	}
	
	
	public function ProcessePost() 
	{
		if($this->template->get_acaoPostada() == 'GERAR_OS_MENSAL' || $this->template->get_acaoPostada() == 'GERAR_OS_ANUAL'){
			cHTTP::$SESSION['chaves'] = $this->template->ChavesSelecionadas('nu_candidato');
			if($this->template->getValorFiltro('mes') == '' || $this->template->getValorFiltro('ano') == ''){
				throw new Exception('Preencha o mês e o ano das OS que deverão ser criadas');
			}
			else{
				if($this->template->get_acaoPostada() == 'GERAR_OS_MENSAL'){
					cORDEMSERVICO::CriarServicosPIT(71, cHTTP::$SESSION['chaves'], $this->template->getValorFiltro('mes'), $this->template->getValorFiltro('ano'));				
				}
				elseif($this->template->get_acaoPostada() == 'GERAR_OS_ANUAL'){
					cORDEMSERVICO::CriarServicosPIT(73, cHTTP::$SESSION['chaves'], $this->template->getValorFiltro('mes'), $this->template->getValorFiltro('ano'));				
				}
				cNOTIFICACAO::singleton('OS criadas com sucesso!');
			}
			
		}
	}
	
}
