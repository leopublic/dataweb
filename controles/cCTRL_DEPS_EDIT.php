<?php
/**
 * Description of cCTRL_DEPS_EDIT
 * @author leonardo
 */
class cCTRL_DEPS_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo()
	{
		$this->modelo = new cCANDIDATO();
	}
	
	public function InicializeTemplate()
	{
		$this->template = new cTMPL_DEPS_EDIT();
	}
	
	public function CarregueTemplatePeloModelo(&$ptmpl, &$pmodelo)
	{
//		$this->modelo->set_depe_id($this->template->getValorCampo('NU_CANDIDATO'));
		
		if (intval($this->modelo->getid()) > 0){
			$this->modelo->RecuperePeloId();
			$ptmpl->CarregueDoOBJETO($this->modelo);
		}
	}

	
	public function ProcesseSalvar()
	{
		$this->template->AtribuaAoOBJETO($this->modelo);
		try{
			$this->modelo->mNU_CANDIDATO = 0;
			$this->modelo->mnu_candidato_parente = $this->template->getValorCampo('NU_CANDIDATO');
			$this->modelo->CriarDependente(cSESSAO::$mcd_usuario);
			$this->template->setValorCampo('NO_PRIMEIRO_NOME', '');
			$this->template->setValorCampo('NO_NOME_MEIO', '');
			$this->template->setValorCampo('NO_ULTIMO_NOME', '');
			$this->template->setValorCampo('NU_PASSAPORTE', '');
			$this->template->setValorCampo('CO_NACIONALIDADE', '');
			$this->template->setValorCampo('CO_PAIS_EMISSOR_PASSAPORTE', '');
			$this->template->setValorCampo('DT_NASCIMENTO', '');
			$this->template->setValorCampo('DT_VALIDADE_PASSAPORTE', '');
			$this->template->setValorCampo('co_grau_parentesco', '');
			cHTTP::$SESSION['msg'] = 'Operação realizada com sucesso!';			
		}
		catch (cERRO_CONSISTENCIA $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
			cHTTP::$SESSION['msg'] = $e->getMessage();
		}
		catch (Exception $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
			cHTTP::$SESSION['msg'] = $e->getMessage();
		}
	}
}
