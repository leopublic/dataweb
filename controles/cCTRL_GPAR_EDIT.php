<?php
/**
 * Description of cCTRL_EMBP_EDIT
 *
 * @author leonardo
 */
class cCTRL_GPAR_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new cGRAU_PARENTESCO();
	}
	
	public function InicializeTemplate() {
		$this->template = new cTMPL_GPAR_EDIT();
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_GPAR_LIST', 'Liste');		
	}

}
