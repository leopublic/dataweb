<?php

/**
 * Metodos para atualização de atributos de ORDENS DE SERVICO
 */
class cCTRL_UPDT_CAND {

    public function IncluirPit() {
        $ret = array();
        try {
            $nu_candidato = cHTTP::$POST['nu_candidato'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $cand = new cCANDIDATO();
            $cand->mNU_CANDIDATO = $nu_candidato;
            $cand->IncluirPit($cd_usuario);
            $links = utf8_encode('incluído');
            $ret['novoConteudo'] = $links;
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function RemoverPit() {
        $ret = array();
        try {
            $nu_candidato = cHTTP::$POST['nu_candidato'];
            $cd_usuario = cHTTP::$POST['cd_usuario'];
            $cand = new cCANDIDATO();
            $cand->mNU_CANDIDATO = $nu_candidato;
            $cand->RemoverPit($cd_usuario);
            $links = 'retirado';
            $ret['novoConteudo'] = $links;
            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

    public function AtualizeCampoPadrao() {
        $ret = array();
        try {
            $valor = cHTTP::$POST['valor'];
            ;
            $chave = cHTTP::$POST['chave'];
            $nome_campo = cHTTP::$POST['xnome_campo'];
            $tipo_campo = cHTTP::$POST['tipo_campo'];
            cESCOLARIDADE::AtualizeCampoPadrao($chave, $nome_campo, $valor, $tipo_campo);

            cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
        } catch (cERRO_CONSISTENCIA $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
        } catch (Exception $e) {
            cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
        }
        $noti = cNOTIFICACAO::ConteudoEmJson();
        $ret['msg'] = $noti['msg'];
        return json_encode($ret);
    }

}
