<?php
class cCTRL_MODELO_EDIT extends cCTRL_MODELO{
	protected $houvePost;
	
	public function __construct() {
	}
	
	public function get_houvePost(){
		return $this->houvePost;
	}

	public function get_AcessoNecessario()
	{
		return '';
	}
	
	public function Edite(){
		if($this->AcessoLiberado($this->get_AcessoNecessario)){
			$this->InicializeModelo();
			$this->InicializeTemplate();
			// Carrega o ID informado no get ou o form postado anteriormente, se houver
			$this->template->CarregueDoHTTP();
			// Carrega o usuário correspondente à tela exibida
			if ($this->template->mFoiPostado){
				$this->houvePost = true;
				$this->ProcessePost();
			}
			else{
				$this->houvePost = false;
				// Se não houve o post, então é a primeira vez que está exibindo
				$this->CarregueTemplatePeloModelo($this->template, $this->modelo);
			}
			$conteudo = $this->template->HTML();
			return $conteudo;	
		}
	}
	public function AcessoLiberado($pacesso = '')
	{
		if($pacesso != ''){
			if(Acesso::permitido($pacesso)){
				return true;
			}
			else{
				cHTTP::$SESSION['msg'] = "Você não tem acesso a essa função.";
				cHTTP::RedirecioneParaExterno('/intranet/geral/principal.php');
			}					
		}
		else{
			return true;
		}
	}

	public function ProcessePost(){
		if ($this->template->get_acaoPostada() == 'SALVAR'){
			$this->ProcesseSalvar();
		}
	}

	public function ProcesseSalvar(){
		$this->template->AtribuaAoOBJETO($this->modelo);
		try{
			$this->modelo->Salvar();
			cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
			$this->RedirecioneEmSucesso();
		}
		catch (cERRO_CONSISTENCIA $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch (Exception $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
	}

	public function CarregueTemplatePeloModelo(&$ptmpl, &$pmodelo){
		$ptmpl->AtribuaAoOBJETO($pmodelo);
		if (intval($pmodelo->getid()) > 0){
			$pmodelo->RecuperePeloId();
			$ptmpl->CarregueDoOBJETO($pmodelo);
		}
	}
	
	public function get_msgErroPadrao(){
		return 'Não foi possível realizar a operação pois ';
	}
	
	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara($this->get_controllerDestinoSucesso(), $this->get_metodoDestinoSucesso(), $this->get_parametrosAdicionaisListagem());		
	}
	
	public function get_controllerDestinoSucesso(){
		return __CLASS__;
	}
	
	public function get_metodoDestinoSucesso(){
		return 'Liste';
	}
	
	public function get_parametrosAdicionaisListagem(){
		return "";
	}

}
