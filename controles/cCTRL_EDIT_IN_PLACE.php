<?php

/**
 * Description of cCTRL_EDIT_IN_PLACE
 *
 * @author leonardo
 */
class cCTRL_EDIT_IN_PLACE {
	//put your code here
	public function Salvar($pid, $pvalor){
		$tokens = explode(':', $pid);
		$classe = 'c'.$tokens[1];
		$atributo = $tokens[2];
		$id = $tokens[3];
		$obj = new $classe();
		$obj->setId($id);
		$ret = new cRET_AJAX_PADRAO();
		try{
			$obj->AtualizeAtributo($atributo, $pvalor);
			$ret->FoiSucesso('Atualização realizada com sucesso.');
		}
		catch (cERRO_CONSISTENCIA $e) {
			$ret->FoiAlerta('Não foi possível realizar a operação pois:<br/>'.$e->getMessage());
		}
		catch (Exception $e) {
			$ret->FoiErro('Não foi possível realizar a operação pois:<br/>'.$e->getMessage());
		}
	}
	
	public function AcionarAtualizacao(){
		$msg = 'teste';
		$ret = new cRET_AJAX_PADRAO();
		try{
			// Tem que decodificar porque as chamadas são todas em UTF 8...
			$valor = utf8_decode(cHTTP::$POST['value']);
			$parametros = cHTTP::$POST['id'];
			$tokens = explode(':', $parametros);
			$classe = 'c'.$tokens[1];
			$metodo = $tokens[2];
			$id = $tokens[3];
			$obj = new $classe();
			$obj->$metodo($id,$valor);
			$ret->FoiSucesso('Atualização realizada com sucesso.');
		}
		catch (cERRO_CONSISTENCIA $e) {
			$ret->FoiAlerta("Não foi possível realizar a operação pois:\n".$e->mMsg);
		}
		catch (Exception $e) {
			$ret->FoiErro("Não foi possível realizar a operação pois:\n".$e->getMessage());
		}
		return $ret->EmJSON();
	}

	public function AtualizeAtributo(){
		$msg = 'teste';
		$ret = new cRET_AJAX_PADRAO();
		try{
			// Tem que decodificar porque as chamadas são todas em UTF 8...
			$valorAtributo = utf8_decode(cHTTP::$POST['valorAtributo']);
			$classe = cHTTP::$POST['entidade'];
			$obj = new $classe();
			$sql = $obj->AtualizeAtributo(cHTTP::$POST['campoAtributo'], $valorAtributo, cHTTP::$POST['tipoAtributo'], json_encode(cHTTP::$POST['chave']),cSESSAO::$mcd_usuario);
			$ret->FoiSucesso('Atualização realizada com sucesso.');
		}
		catch (cERRO_CONSISTENCIA $e) {
			$ret->FoiAlerta("Não foi possível realizar a operação pois:\n".$e->mMsg);
		}
		catch (Exception $e) {
			$ret->FoiErro("Não foi possível realizar a operação pois:\n".$e->getMessage());
		}
		return $ret->EmJSON();
	}
	
	public function Atualize(){
		$msg = 'teste';
		$ret = new cRET_AJAX_PADRAO();
		try{
			// Tem que decodificar porque as chamadas são todas em UTF 8...
			$valor = utf8_decode(cHTTP::$POST['value']);
			$parametros = cHTTP::$POST['id'];
			$tokens = explode(':', $parametros);
			$classe = 'c'.$tokens[1];
			$metodo = $tokens[2];
			$id = $tokens[3];
			$obj = new $classe();
			$obj->$metodo($id,$valor);
			$ret->FoiSucesso('Atualização realizada com sucesso.');
		}
		catch (cERRO_CONSISTENCIA $e) {
			$ret->FoiAlerta("Não foi possível realizar a operação pois:\n".$e->mMsg);
		}
		catch (Exception $e) {
			$ret->FoiErro("Não foi possível realizar a operação pois:\n".$e->getMessage());
		}
		return $ret->EmJSON();
	}
}
