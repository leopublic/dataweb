<?php
/**
 * @author leonardo
 */
class cCTRL_TAXA_EDIT extends cCTRL_MODELO_EDIT{
	public function InicializeModelo() {
		$this->modelo = new ctaxa();
	}

	public function InicializeTemplate() {
		$this->template = new cTMPL_TAXA_EDIT();
	}

	public function RedirecioneEmSucesso(){
		cHTTP::RedirecionePara('cCTRL_TAXA_LIST', 'Liste', 'id_perfiltaxa='.$this->template->getValorCampo('id_perfiltaxa'));
	}

}
