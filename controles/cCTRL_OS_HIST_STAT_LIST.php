<?php

class cCTRL_OS_HIST_STAT_LIST extends cCTRL_MODELO_LIST {

    public function InicializeModelo() {
        $this->modelo = new cORDEMSERVICO();
    }

    public function InicializeTemplate() {
        $this->template = new cTMPL_OS_HIST_STAT_LIST();
    }

    public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) {
        if(!cHTTP::HouvePost()){
            if($this->template->getValorFiltro('dt_cadastro_fim') == ''){
                $this->template->setValorFiltro('dt_cadastro_fim', date('d/m/Y'));
            }
            if($this->template->getValorFiltro('dt_cadastro_ini') == ''){
                $date = new DateTime();
                $date->sub(new DateInterval('P1M'));
                $this->template->setValorFiltro('dt_cadastro_ini', date_format($date, 'd/m/Y'));
            }
        }

        if($this->template->getValorFiltro('dt_cadastro_ini') != ''){
            $this->template->mFiltro['dt_cadastro_ini']->mWhere = " dt_cadastro >=".cBANCO::DataOk($this->template->getValorFiltro('dt_cadastro_ini'));
        }
        if($this->template->getValorFiltro('dt_cadastro_fim') != ''){
            $this->template->mFiltro['dt_cadastro_fim']->mWhere = " dt_cadastro <=".cBANCO::DataOk($this->template->getValorFiltro('dt_cadastro_fim'));
        }

        return cREPOSITORIO_OS::CursorHistoricoOs($this->template->mFiltro);
    }
}
