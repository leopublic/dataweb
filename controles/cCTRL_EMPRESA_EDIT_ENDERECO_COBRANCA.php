<?php
/**
 * Description of cCTRL_EMPRESA_EDIT_ENDERECO_COBRANCA
 *
 * @author leonardo
 */
class cCTRL_EMPRESA_EDIT_ENDERECO_COBRANCA extends cCTRL_MODELO_EDIT{
	public function InicializeModelo(){
		$this->modelo = new cEMPRESA();
	}
	
	public function InicializeTemplate(){
		$this->template = new cTMPL_EMPRESA_EDIT_END_COB();
	}
		
	public function ProcessePost(){
		print 'postou '.$this->template->get_acaoPostada();
		if ($this->template->get_acaoPostada() == 'SALVAR'){
			$this->ProcesseSalvar();
		}
	}

	public function ProcesseSalvar() {
		$this->template->AtribuaAoOBJETO($this->modelo);
		try{
			$this->modelo->AtualizarEnderecoCobranca();
			cNOTIFICACAO::singleton('Operação realizada com sucesso!', cNOTIFICACAO::TM_SUCCESS);
			$this->RedirecioneEmSucesso();
		}
		catch (cERRO_CONSISTENCIA $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch (Exception $e) {
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$this->CarregueTemplatePeloModelo($this->template, $this->modelo);
	}
	
	public function RedirecioneEmSucesso() {
		
	}
}
