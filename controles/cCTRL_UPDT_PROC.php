<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class cCTRL_UPDT_PROC{

	public function dt_envio_bsb()
	{
		$ret = array();
		try{
			$tabela = cHTTP::$POST['tabela'];
			$dt_envio_bsb = cHTTP::$POST['dt_envio_bsb'];
			$id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			if ($tabela == 'mte'){
				cprocesso_mte::Atualiza_dt_envio_bsb($id_solicita_visto, $dt_envio_bsb, $cd_usuario);
			}
			elseif($tabela == 'prorrog'){
				cprocesso_prorrog::Atualiza_dt_envio_bsb($id_solicita_visto, $dt_envio_bsb, $cd_usuario);
			}
			elseif($tabela == 'cancel'){
				cprocesso_cancel::Atualiza_dt_envio_bsb($id_solicita_visto, $dt_envio_bsb, $cd_usuario);
			}
			elseif($tabela == 'precadastro'){
				cprocesso_prorrog::Atualiza_dt_envio_bsb_pre_cadastro($id_solicita_visto, $dt_envio_bsb, $cd_usuario);
			}
			elseif($tabela == 'mudemb'){
                            cprocesso_mudanca_embarcacao::Atualiza_dt_envio_bsb($id_solicita_visto, $dt_envio_bsb, $cd_usuario);
			}
			$ret['novoConteudo'] = 'Adicionada';
			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}	

	public function RetirarDoProtocoloBsb()
	{
		$ret = array();
		try{
			$tabela = cHTTP::$POST['tabela'];
			$id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			if ($tabela == 'mte'){
				cprocesso_mte::RetirarDoProtocoloBsb($id_solicita_visto, $cd_usuario);
			}
			elseif($tabela == 'prorrog'){
				cprocesso_prorrog::RetirarDoProtocoloBsb($id_solicita_visto, $cd_usuario);
			}
			elseif($tabela == 'cancel'){
				cprocesso_cancel::RetirarDoProtocoloBsb($id_solicita_visto, $cd_usuario);
			}
			elseif($tabela == 'precadastro'){
				cprocesso_prorrog::RetirarDoProtocoloBsbPrecadastro($id_solicita_visto, $cd_usuario);
			}
			elseif($tabela == 'mudemb'){
                            cprocesso_mudanca_embarcacao::RetirarDoProtocoloBsb($id_solicita_visto, $cd_usuario);
			}
			$ret['novoConteudo'] = 'Retirada';
			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}	
	
	public function dt_requerimento()
	{
		$ret = array();
		try{
			$dt_requerimento = cHTTP::$POST['valor'];
			$tabela = cHTTP::$POST['tabela'];
			$id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			if ($tabela == 'mte'){
				cprocesso_mte::Atualiza_dt_requerimento($dt_requerimento, $id_solicita_visto, $cd_usuario);
			}
			elseif($tabela == 'prorrog'){
				cprocesso_prorrog::Atualiza_dt_requerimento($dt_requerimento, $id_solicita_visto, $cd_usuario);
			}
			elseif($tabela == 'cancel'){
				cprocesso_cancel::Atualiza_dt_processo($dt_requerimento, $id_solicita_visto, $cd_usuario);
			}
			$ret['novoConteudo'] = $dt_requerimento;
			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}
	
	public function nu_protocolo()
	{
		$ret = array();
		try{
			$nu_protocolo = cHTTP::$POST['valor'];
			$tabela = cHTTP::$POST['tabela'];
			$id_solicita_visto = cHTTP::$POST['id_solicita_visto'];
			$cd_usuario = cHTTP::$POST['cd_usuario'];
			if ($tabela == 'mte'){
				cprocesso_mte::Atualiza_nu_processo($nu_protocolo, $id_solicita_visto, $cd_usuario);
			}
			elseif($tabela == 'prorrog'){
				cprocesso_prorrog::AtualizaPelaOs_nu_protocolo($nu_protocolo, $id_solicita_visto, $cd_usuario);
			}
			elseif($tabela == 'cancel'){
				cprocesso_cancel::AtualizaPelaOs_nu_processo($nu_protocolo, $id_solicita_visto, $cd_usuario);
			}
			
			cNOTIFICACAO::singleton('Ok', cNOTIFICACAO::TM_SUCCESS);
		}
		catch(cERRO_CONSISTENCIA $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ALERT);
		}
		catch(Exception $e){
			cNOTIFICACAO::singleton($e->getMessage(), cNOTIFICACAO::TM_ERROR);
		}
		$noti = cNOTIFICACAO::ConteudoEmJson();
		$ret['msg'] = $noti['msg'];
		return json_encode($ret);
	}
}
