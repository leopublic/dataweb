<?php 
class cCTRL_OS_PREG_LIST extends cCTRL_MODELO_LIST{
	public function InicializeModelo(){
		$this->modelo = new cORDEMSERVICO();
	}
	
	public function InicializeTemplate(){
		$this->template = new cTMPL_OS_PREG_LIST();
	}

	public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) {
		
		
		if($this->template->getValorFiltro('dt_requerimento_ini') != ''){
			$this->template->mFiltro['dt_requerimento_ini']->mWhere = ' dt_requerimento >= '.cBANCO::DataOk($this->template->getValorFiltro('dt_requerimento_ini'));
		}
		if($this->template->getValorFiltro('dt_requerimento_fim') != ''){
			$this->template->mFiltro['dt_requerimento_fim']->mWhere = ' dt_requerimento <= '.cBANCO::DataOk($this->template->getValorFiltro('dt_requerimento_fim'));
		}
		
		if($this->template->getValorFiltro('dt_requerimento_fim') != '' && $this->template->getValorFiltro('dt_requerimento_ini') != ''){
			return $this->modelo->CursorOsRegistro($this->template->mFiltro, $this->template->get_ordenacao());
		}
	}
}
