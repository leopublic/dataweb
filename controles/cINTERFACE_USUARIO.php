<?php
class cINTERFACE_USUARIO{

	public static function TrocarSenha(){
		$msg = '' ;
		$retorno= '' ;
		$tmpl = new cTEMPLATE_USUARIO_TROCAR_SENHA();
		$tmpl->CarregueDoHTTP();
		$usuario = new cusuarios();
		$usuario->cd_usuario = $tmpl->mCampo['cd_usuario']->mValor;
		//var_dump($tmpl->mCampo);
		if ($tmpl->mFoiPostado){
			// Limpa o combo por default
			switch ($tmpl->mAcaoPostada){
				case 'SALVAR':
					try{
						$usuario->AltereSenha($tmpl->mCampo['nm_senha']->mValor);
						$msg = "Senha alterada com sucesso";
					}
					catch(Exception $e){
						$msg = str_replace("'", '"', str_replace("\n", '\n', htmlentities($e->getMessage())));
					}
					break;
				default:
					$msg = "Operação não encontrada (".$tmpl->mAcaoPostada.")";
					break;
			}
		}
		else{
			$tmpl->CarregueDoOBJETO($usuario);
		}
		cHTTP::$SESSION['msg'] = $msg;
		$conteudo = cINTERFACE::RenderizeTemplate($tmpl);
		return $conteudo;
	}

}
