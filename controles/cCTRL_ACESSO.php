<?php
class cCTRL_ACESSO{
	public function permitido($tpElemento){
       return in_array($tpElemento, $_SESSION['ACESSO']);
    }

	public static function Liste(){
		cHTTP::$comCabecalhoFixo = true;
		$tela = new cTEMPLATE_PERFIL_LISTE();
		$tela->CarregueDoHTTP();

		$x = new cusuario_perfil();
		$ordenacao = '';
		$virgula = '';
		$ret = '';
		$ret .= cINTERFACE::RenderizeTitulo($tela, true, cINTERFACE::titPAGINA);
		$ret .= '<div class="conteudoInterno">';
		$ret .= cINTERFACE::formTemplate($tela);
		$ret .= cINTERFACE::RenderizeListagem_Filtros($tela);

		$cursor = $x->ListeTodos($tela->mFiltro, $tela->ordenacao());
		$ret.= cINTERFACE::RenderizeListagem_Cabecalho($tela);
		$estilo = '';
		while ($rs = mysql_fetch_array($cursor)) {
			$tela->CarregueDoRecordset($rs);
			//var_dump($tela->mCampo);
			$ret .= cINTERFACE::RenderizeListagem_Linha($tela, $estilo);
		}
		$ret .= cINTERFACE::RenderizeListagem_Rodape($tela);
		//
		//
		if (!$tela->mFoiPostado) {
			cLAYOUT::AdicionarScriptExibicaoMensagens();
		}
		$ret.= "</form>";
		$ret.= '</div>';
		return $ret;

	}
}
