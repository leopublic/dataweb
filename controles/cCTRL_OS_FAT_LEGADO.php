<?php 
class cCTRL_OS_FAT_LEGADO extends cCTRL_MODELO_LIST{
	public function InicializeModelo(){
		$this->modelo = new cORDEMSERVICO();
	}
	
	public function InicializeTemplate(){
		$this->template = new cTMPL_OS_FAT_LEGADO();
	}

	public function get_cursorListagem(\cTEMPLATE_LISTAGEM $ptmpl) 
	{
		$qtdReg = $this->modelo->QtdFaturadasLegado($this->template->mFiltro, $this->template->ordenacao());
		$offset = $this->template->get_offset();
		if($offset > 0){
			$qtdPag =  floor($qtdReg / $this->template->get_offset());			
		}
		else{
			$qtdPag = 1;
		}
		$this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS, $qtdPag);
		return $this->modelo->CursorFaturadasLegado($this->template->mFiltro, $this->template->ordenacao(), $this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL), $this->template->get_offset());
	}

	public function ProcessePost()
	{
		if($this->template->get_acaoPostada() == 'PRIMEIRA'){
			$this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, 1);
		}
		if($this->template->get_acaoPostada() == 'PROXIMA'){
			$this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) + 1);
		}
		if($this->template->get_acaoPostada() == 'ANTERIOR'){
			$this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) - 1);
		}
		if($this->template->get_acaoPostada() == 'ULTIMA'){
			$this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS)));
		}
	}

	public function Redirecione()
	{

	}
}
