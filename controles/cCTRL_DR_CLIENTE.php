<?php

class cCTRL_CANDIDATO_CLIENTE extends cCTRL_MODELO {

    public function dataInicial($candidato = null) {
        $data = array();
        $data['pagina']['titulo'] = 'Mundivisas::Dataweb';
        $menu = cmenu::NovoMenuCliente();
        cHTTP::set_menu_esquerdo($menu);
        $data['pagina']['menu']['esquerdo'] = $menu->JSON();
        $menu = cmenu::NovoMenuClienteSystem();
        cHTTP::set_menu_Direito($menu);
        $data['pagina']['menu']['direito'] = $menu->JSON();
        $data['nu_empresa_prestadora'] = 1;

        $emp = new cEMPRESA;
        $emp->RecuperePeloId(cSESSAO::$mcd_empresa);
        $data['empresa'] = $emp;

        $rep = new cREPOSITORIO_CANDIDATO;
        if (isset($candidato)) {
            if ($candidato->mNU_EMBARCACAO_PROJETO > 0){
                $emb = new cEMBARCACAO_PROJETO;
                $emb->mNU_EMPRESA = cSESSAO::$mcd_empresa;
                $emb->mNU_EMBARCACAO_PROJETO = $candidato->mNU_EMBARCACAO_PROJETO;
                $emb->RecuperePeloId();
            } else {
                $emb = new cEMBARCACAO_PROJETO;
                $emb->mNO_EMBARCACAO_PROJETO = '(not informed)';
            }

            $data['empresa'] = $emp;
            if ($rep->edicaoHabilitada($candidato, cSESSAO::$cd_usuario)) {
                $disabled = '';
                $readonly = '';
            } else {
                $disabled = ' disabled="disabled" ';
                $readonly = true;
            }
            $data['nu_candidato'] = $candidato->mNU_CANDIDATO;
            if (get_class($candidato) == 'cCANDIDATO'){
                $data['nu_candidato_tmp'] = '';
            } else {
                $data['nu_candidato_tmp'] = $candidato->mnu_candidato_tmp;
            }
            $data['candidato'] = $candidato;
            if ($candidato->mNU_CANDIDATO > 0){
                if ($candidato->mcodigo_processo_mte_atual > 0) {
                    $autorizacao = new cprocesso_mte();
                    $autorizacao->mcodigo = $candidato->mcodigo_processo_mte_atual;
                    $autorizacao->RecupereSe();
                    $data['prazo_estada'] = $autorizacao->get_situacao_prazo_estada();
                    $data['status'] = $autorizacao->get_situacao_texto();
                } else {
                    $data['prazo_estada'] = '--';
                    $data['status'] = 'no visa';
                }

            }
        } else {
            $emb = new cEMBARCACAO_PROJETO;
            $emb->mNO_EMBARCACAO_PROJETO = '(not informed)';
        }
        $data['embarcacao']  =$emb;
        $data['disabled'] = $disabled;
        $data['readonly'] = $readonly;
        $data['sexos'] = array(array("valor" => "F", "descricao" => "Feminine"), array("valor" => "M", "descricao" => "Masculine"));
        return $data;
    }

    public function redirecionaPara($metodo, $nu_candidato = '', $nu_candidato_tmp = '') {
        $link = '/paginas/carregue.php?controller=cCTRL_CANDIDATO_CLIENTE&metodo=' . $metodo . '&NU_CANDIDATO=' . $nu_candidato . '&nu_candidato_tmp=' . $nu_candidato_tmp;
        cHTTP::RedirecionaLinkInterno($link);
    }

    public function editarInfo() {
        $data = array();
//        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            $cand = $rep->candidatoParaCliente($_GET['NU_CANDIDATO'], $_GET['nu_candidato_tmp']);
            $data = $this->dataInicial($cand);
            $data['servicos'] = cSERVICO::comboDisponiveisClientes('', '(select...)');
            $data['nacionalidades'] = cPAIS_NACIONALIDADE::comboMustacheIngles('', '(select...)');
            $data['paises'] = cPAIS_NACIONALIDADE::comboMustachePaisIngles('', '(select...)');
            $data['sexos'] = array(array("valor" => "F", "descricao" => "Feminine"), array("valor" => "M", "descricao" => "Masculine"));
        // } catch (Exception $ex) {
        //     cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
        // }
        $data['ativa'] = 'INFO';
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        return cTWIG::Render('cliente/candidato/dados_pessoais_cliente.twig', $data);
    }

    public function postEditarInfo() {
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            $nu_candidato = $_POST['nu_candidato'];
            $cand = $rep->salvaAlteracoesCliente($nu_candidato, $_POST['nu_candidato_tmp'], $_POST);
            $_SESSION['msg'] = 'Changes saved!';
            cNOTIFICACAO::singleton('Changes updated succesfully.', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
        }
        $this->redirecionaPara('editarInfo', $cand->mNU_CANDIDATO, $cand->mnu_candidato_tmp);
    }

    public function getEditarDocumentos() {
        $data = array();
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            $cand = $rep->candidatoParaCliente($_GET['NU_CANDIDATO'], $_GET['nu_candidato_tmp']);
            $data = $this->dataInicial($cand);
            $data['paises'] = cPAIS_NACIONALIDADE::comboMustachePaisIngles('', '(select...)');
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
        }
        $data['ativa'] = 'DOCS';
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        return cTWIG::Render('cliente/candidato/documentos_cliente.twig', $data);
    }

    public function postEditarDocumentos() {
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            $nu_candidato = $_POST['nu_candidato'];
            $cand = $rep->salvaAlteracoesCliente($nu_candidato, $_POST['nu_candidato_tmp'], $_POST);
            $_SESSION['msg'] = 'Changes saved!';
            cNOTIFICACAO::singleton('Changes updated succesfully.', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
        }
        $this->redirecionaPara('getEditarDocumentos', $cand->mNU_CANDIDATO, $cand->mnu_candidato_tmp);
    }

    public function getEditarExperiencia() {
        $data = array();
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            $cand = $rep->candidatoParaCliente($_GET['NU_CANDIDATO'], $_GET['nu_candidato_tmp']);
            $data = $this->dataInicial($cand);

            $data['nacionalidades'] = cPAIS_NACIONALIDADE::comboMustacheIngles('', '(select...)');
            $data['paises'] = cPAIS_NACIONALIDADE::comboMustachePaisIngles('', '(select...)');
            $data['locais'] = clocal_projeto::comboMustache('', '(select...)');
            $data['embarcacoes'] = cEMBARCACAO_PROJETO::comboMustache(cSESSAO::$mcd_empresa, '', '(select...)');
            $data['experiencias'] = cexperiencia_profissional::cursorDoCandidatoTmp($data['nu_candidato_tmp']);
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
        }
        $data['ativa'] = 'EXPE';
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        return cTWIG::Render('cliente/candidato/experiencia_cliente.twig', $data);
    }

    public function postEditarExperiencia() {
        $rep = new cREPOSITORIO_CANDIDATO;
        try {
            $nu_candidato = $_POST['nu_candidato'];
            $cand = $rep->salvaAlteracoesCliente($nu_candidato, $_POST['nu_candidato_tmp'], $_POST);
            cNOTIFICACAO::singleton('Changes updated succesfully.', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
        }
        $this->redirecionaPara('getEditarExperiencia', $cand->mNU_CANDIDATO, $cand->mnu_candidato_tmp);
    }

    public function getEditarArquivos() {
        $data = array();
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
            $cand = $rep->candidatoParaCliente($_GET['NU_CANDIDATO'], $_GET['nu_candidato_tmp']);
            $data = $this->dataInicial($cand);
            $data['tipos_arquivo'] = cTIPO_ARQUIVO::comboMustacheIngles('', '(select...)');
            $data['ativa'] = 'FILES';
            $data['arquivos'] = $rep->cursorArquivos($cand);
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Attention");
        }
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        return cTWIG::Render('cliente/candidato/arquivos_cliente.twig', $data);
    }

    public function postEditarArquivos() {
        try {
            $nu_candidato = $_POST['nu_candidato'];
            $nu_candidato_tmp = $_POST['CMP_nu_candidato_tmp'];
            $rep = new cREPOSITORIO_ARQUIVO;
            $rep->novoArquivoDoCandidatoPeloCandidato($nu_candidato, $_FILES['CMP_arquivo'], $nu_candidato_tmp);
            cNOTIFICACAO::singleton('File sent succesfully.', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Attention");
        }
        $this->redirecionaPara('getEditarArquivos', $nu_candidato, $nu_candidato_tmp);
    }

    public function visaDetails() {
        $rep = new cREPOSITORIO_CANDIDATO;
        $cand = $rep->candidatoParaCliente($_GET['NU_CANDIDATO'], $_GET['nu_candidato_tmp']);
        $data = $this->dataInicial($cand);
        $cand_real = new cCANDIDATO();
        $cand_real->Recuperar($_GET['NU_CANDIDATO']);
        $processos = $cand_real->VistoAtual();
        foreach ($processos as &$processo) {
            if ($processo->mnome_tabela == 'prorrog') {
                $nu = preg_replace('/\D/', '', $processo->mnu_protocolo);
                if ($nu != '') {
                    $processo->mnu_protocolo = substr($nu, 0, 5) . '.' . substr($nu, 5, 6) . '/' . substr($nu, 11, 4) . '-' . substr($nu, 15, 2);
                } else {
                    $processo->mnu_protocolo = '--';
                }
            }
        }
        $data['nacionalidades'] = cPAIS_NACIONALIDADE::comboMustacheIngles('', '(select...)');
        $data['ativa'] = 'VISA';
        $data['autorizacao'] = $processos['autorizacao'];
        $data['processos'] = $processos['processos'];
        return cTWIG::Render('cliente/candidato/visa_details_cliente.twig', $data);
    }

    public function getEnviar() {
        $rep = new cREPOSITORIO_CANDIDATO;
        $cand = $rep->candidatoParaCliente($_GET['NU_CANDIDATO'], $_GET['nu_candidato_tmp']);
        $data = $this->dataInicial($cand);
        $data['ativa'] = 'ENVIAR';
        return cTWIG::Render('cliente/candidato/enviar_cliente.twig', $data);
    }

    public function postEnviar() {
        try {
            $rep = new cREPOSITORIO_DR;
            $nu_candidato = $_POST['nu_candidato'];
            $nu_candidato_tmp = $_POST['CMP_nu_candidato_tmp'];
            $rep->enviaAlteracoes($nu_candidato, $nu_candidato_tmp);
            cNOTIFICACAO::singleton('Changes updated succesfully.', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_SUCCESS, "Attention");
        }
        $this->redirecionaPara('editarInfo', $nu_candidato, $nu_candidato_tmp);
    }

    public function download() {
        $NU_SEQUENCIAL = $_GET['NU_SEQUENCIAL'];
        $rep = new cREPOSITORIO_ARQUIVO();
        $rep->downloadParaCliente($NU_SEQUENCIAL);
    }

    public function getListarPendentes() {
        $rep = new cREPOSITORIO_CONVIDADO;
        $data = $this->dataInicial();

        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        $data['linhas'] = $rep->preenchimentosPendentes(cSESSAO::$mcd_empresa);
        error_log($data['linhas']);
        return cTWIG::Render('cliente/pendentes.twig', $data);
    }

    public function getEnviarconvite() {
        $data = $this->dataInicial();
        try {
            $rep = new cREPOSITORIO_CANDIDATO;
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
        }
        $data['embarcacoes'] = cEMBARCACAO_PROJETO::comboMustache(cSESSAO::$mcd_empresa, '', '(select...)');
        $data['servicos'] = cSERVICO::comboDisponiveisClientes('', '(please select one)');
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        $conv = new cconvidado;
        $conv->mnome = $this->valorInputSalvo('CMP_nome');
        $conv->mnu_servico = $this->valorInputSalvo('CMP_nu_servico');
        $conv->mnu_embarcacao_projeto = $this->valorInputSalvo('CMP_nu_embarcacao_projeto');
        $conv->memail = $this->valorInputSalvo('CMP_email');
        return cTWIG::Render('cliente/candidato/criar_convidado.twig', $data);
    }

    public function postEnviarconvite() {
        try {
            $rep = new cREPOSITORIO_CONVIDADO;
            $email = $this->valorInputSalvo('CMP_email');
            $nome = $this->valorInputSalvo('CMP_nome');
            $nu_embarcacao_projeto = $this->valorInputSalvo('CMP_nu_embarcacao_projeto');
            $nu_servico = $this->valorInputSalvo('CMP_nu_servico');
            $conv = $rep->criar($_POST['CMP_email'], $_POST['CMP_nome'], cSESSAO::$mcd_empresa, $_POST['CMP_nu_embarcacao_projeto'], $_POST['CMP_nu_servico']);
            $cands = $rep->adicionarPreenchimentos($conv, $_POST['nome_completo']);
            $repDR = new cREPOSITORIO_DR;
            $repDR->enviaConvitePreenchimento($conv->mnome, $conv, cSESSAO::$mcd_usuario);
            $this->destroiInpuSalvo('CMP_email');
            $this->destroiInpuSalvo('CMP_nome');
            $this->destroiInpuSalvo('CMP_nu_embarcacao_projeto');
            $this->destroiInpuSalvo('CMP_nu_servico');
            cNOTIFICACAO::singleton('Invitation sent to ' . $_POST['CMP_email'] . '!', cNOTIFICACAO::TM_SUCCESS, "Attention");
        } catch (Exception $ex) {
            cNOTIFICACAO::singleton($ex->getMessage(), cNOTIFICACAO::TM_ERROR, "Attention");
        }
        $this->redirecionaPara('getEnviarconvite');
    }

    public function postAdicionarExperiencia(){
        $rep = new cREPOSITORIO_DR;
        $post = $_POST;
        $exp = $rep->adicionarExperiencia($post['nu_candidato_tmp'], $post['no_companhia'], $post['no_funcao'], $post['dt_inicio'], $post['dt_fim'], $post['tx_atribuicoes']) ;
        $data = array(
            'experiencias' => cexperiencia_profissional::cursorDoCandidatoTmp($post['nu_candidato_tmp']),
            );
        $ret = cTWIG::Render('cliente/candidato/experiencia_list.twig', $data);
        return $ret;
    }

    public function postExcluirExperiencia(){
        $rep = new cREPOSITORIO_DR;
        $post = $_POST;
        $exp = $rep->excluirExperiencia($post['epro_id']);
        $data = array(
            'experiencias' => cexperiencia_profissional::cursorDoCandidatoTmp($post['nu_candidato_tmp']),
        );
        $ret = cTWIG::Render('cliente/candidato/experiencia_list.twig', $data);
        return $ret;
    }

    public function getListarCandidatos() {
        $data = $this->dataInicial();
        $rep = new cREPOSITORIO_CANDIDATO;

        $nu_embarcacao_projeto = $this->valorInputSalvo('CMP_nu_embarcacao_projeto');
        $nome_completo = $this->valorInputSalvo('CMP_nome_completo');

        $emp = new cEMPRESA;
        $emp->RecuperePeloId(cSESSAO::$mcd_empresa);
        if (intval($emp->mcfgr_id) > 0){
            $conf = new cconfiguracao_relatorio;
            $conf->RecuperePeloId($emp->mcfgr_id);
            $colunas = $conf->colunas();
        } else {
            $colunas = array();
        }

        $where = "";
        $where = ' and C.nu_empresa = '.cSESSAO::$mcd_empresa;
        if ($nu_embarcacao_projeto != ''){
            $where .= ' and C.nu_embarcacao_projeto = '.$nu_embarcacao_projeto;
        }
        if ($nome_completo != ''){
            $where .= " and C.nome_completo like '%".str_replace(' ', '%', $nome_completo)."%'";
        }
        $cand = new cCANDIDATO;
        $sql = $cand->sqlCursorRelatorio($where, 'nome_completo');
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);

        $repRel = new cREPOSITORIO_RELATORIO;
        $paineis = $repRel->campos();
        foreach($paineis as &$painel){
            $nomePainel = $painel['titulo'];
            $campos = $painel['campos'];
            $visiveis = 0;
            foreach($campos as &$campo){
                if ($campo['obrigatorio'] || in_array($campo['campo'], $colunas)){
                    $campo['visivel'] = true;
                    $visiveis++;
                } else {
                    $campo['visivel'] = false;
                }
            }
            $painel['campos'] = $campos;
            $painel['visiveis'] = $visiveis;
        }

        $data['usuario']                   = cSESSAO::$mnome;
        $data['cadastros_pendentes']       = $cadastros_pendentes;
        $data['nu_embarcacao_projeto']     = $nu_embarcacao_projeto;
        $data['nome_completo']             = $nome_completo;
        $data['candidatos']                = $rs;
        $data['embarcacoes']               = cEMBARCACAO_PROJETO::daEmpresa(cSESSAO::$mcd_empresa);
        $data['paineis']                   = $paineis;
        $data['msg']                       = $_SESSION['msg'];
        $_SESSION['msg'] = '';
        return cTWIG::Render('cliente/candidato/listar.twig', $data);
    }

    public function getListarCandidatosExcel() {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-disposition: attachment; filename="report.xls"');
        $rep = new cREPOSITORIO_CANDIDATO;

        $emp = new cEMPRESA;
        $emp->RecuperePeloId(cSESSAO::$mcd_empresa);
        if (intval($emp->mcfgr_id) > 0){
            $conf = new cconfiguracao_relatorio;
            $conf->RecuperePeloId($emp->mcfgr_id);
            $colunas = $conf->colunas();
        } else {
            $colunas = array();
        }

        $where = "";
        $where = ' and C.nu_empresa = '.cSESSAO::$mcd_empresa;
        $cand = new cCANDIDATO;
        $sql = $cand->sqlCursorRelatorio($where, 'nome_completo');
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);

        $repRel = new cREPOSITORIO_RELATORIO;
        $paineis = $repRel->campos();
        $totalVisivel=0;
        foreach($paineis as &$painel){
            $nomePainel = $painel['titulo'];
            $campos = $painel['campos'];
            $visiveis = 0;
            foreach($campos as &$campo){
                if ($campo['obrigatorio'] || in_array($campo['campo'], $colunas)){
                    $campo['visivel'] = true;
                    $visiveis++;
                    $totalVisivel++;
                } else {
                    $campo['visivel'] = false;
                }
            }
            $painel['campos'] = $campos;
            $painel['visiveis'] = $visiveis;
        }
        $data = array();
        $data['empresa'] = $emp;
        $data['candidatos']                = $rs;
        $data['paineis']                   = $paineis;
        $data['totalVisivel']              = $totalVisivel;
        return cTWIG::Render('cliente/candidato/listarexcel.twig', $data);
    }

    public function getProrrogacoes(){
        $rep = new cREPOSITORIO_CONVIDADO;
        $data = $this->dataInicial();

        $nu_embarcacao_projeto = $this->valorInputSalvo('CMP_nu_embarcacao_projeto');
        $nome_completo = $this->valorInputSalvo('CMP_nome_completo');
        $where = "";
        if ($nu_embarcacao_projeto != ''){
            $where .= ' and c.nu_embarcacao_projeto = '.$nu_embarcacao_projeto;
        }
        if ($nome_completo != ''){
            $where .= " and c.nome_completo like '%".str_replace(' ', '%', $nome_completo)."%'";
        }
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        $data['linhas'] = $rep->prorrogacoes(cSESSAO::$mcd_empresa, $where);
        $data['nu_embarcacao_projeto']     = $nu_embarcacao_projeto;
        $data['nome_completo']             = $nome_completo;
        $data['embarcacoes']               = cEMBARCACAO_PROJETO::daEmpresa(cSESSAO::$mcd_empresa, '(all)');

        return cTWIG::Render('cliente/prorrogacoes.twig', $data);

    }

    public function getCancelamentos(){
        $rep = new cREPOSITORIO_CONVIDADO;
        $data = $this->dataInicial();

        $nu_embarcacao_projeto = $this->valorInputSalvo('CMP_nu_embarcacao_projeto');
        $nome_completo = $this->valorInputSalvo('CMP_nome_completo');
        $where = "";
        if ($nu_embarcacao_projeto != ''){
            $where .= ' and c.nu_embarcacao_projeto = '.$nu_embarcacao_projeto;
        }
        if ($nome_completo != ''){
            $where .= " and c.nome_completo like '%".str_replace(' ', '%', $nome_completo)."%'";
        }
        error_log('where='.$where);
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        $data['linhas'] = $rep->cancelamentos(cSESSAO::$mcd_empresa, $where);
        $data['nu_embarcacao_projeto']     = $nu_embarcacao_projeto;
        $data['nome_completo']             = $nome_completo;
        $data['embarcacoes']               = cEMBARCACAO_PROJETO::daEmpresa(cSESSAO::$mcd_empresa, '(all)');

        return cTWIG::Render('cliente/cancelamentos.twig', $data);
    }
}
