
<?php

class cCTRL_OS_TAXA_LIST extends cCTRL_MODELO_LIST {

    public function __construct() {
        
    }

    public function InicializeModelo() {
        $this->modelo = new cREPOSITORIO_OS();
    }

    public function InicializeTemplate() {
        cHTTP::$comCabecalhoFixo = true;
        $this->template = new cTMPL_OS_TAXA_LIST();
    }

    public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl) {
        if ($this->template->getValorFiltro('dt_solicitacao_ini') != '') {
            $this->template->mFiltro['dt_solicitacao_ini']->mWhere = ' dt_solicitacao >= ' . cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_ini'));
        }
        if ($this->template->getValorFiltro('dt_solicitacao_fim') != '') {
            $this->template->mFiltro['dt_solicitacao_fim']->mWhere = ' dt_solicitacao <= ' . cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_fim'));
        }

        $qtdReg = $this->modelo->QtdCursorTaxas($this->template->mFiltro, $this->template->ordenacao());
        $offset = $this->template->get_offset();
        if ($offset > 0) {
            $qtdPag = floor($qtdReg / $this->template->get_offset());
        } else {
            $qtdPag = 1;
        }
        $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS, $qtdPag);

        $cursor = $this->modelo->cursorTaxas($this->template->mFiltro, $this->template->get_ordenacao(), $this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL), $this->template->get_offset());
        return $cursor;
    }

    public function ProcessePost() {
        if ($this->template->get_acaoPostada() == 'PRIMEIRA') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, 1);
            $this->template->setValorCampo('__acao', '');
        }
        if ($this->template->get_acaoPostada() == 'PROXIMA') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) + 1);
            $this->template->setValorCampo('__acao', '');
        }
        if ($this->template->get_acaoPostada() == 'ANTERIOR') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) - 1);
            $this->template->setValorCampo('__acao', '');
        }
        if ($this->template->get_acaoPostada() == 'ULTIMA') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS)));
            $this->template->setValorCampo('__acao', '');
        }
        
        if (count($_FILES) > 0){
            foreach($_FILES as $input => $file){
                if ($file['error'] == 0 && $file['size'] > 0){
                    $nomes = explode("_", $input);
                    $nu_candidato = $nomes[2];
                    $id_solicita_visto = $nomes[3];
                    $id_taxa = $nomes[4];
                    
                    $cand= new cCANDIDATO;
                    $cand->RecuperePeloId($nu_candidato);
                    
                    $arquivo = cARQUIVO_OK::novoPeloFile($file);
                    $arquivo->mNU_CANDIDATO = $nu_candidato;
                    $arquivo->mid_taxa = $id_taxa;
                    $arquivo->mTP_ARQUIVO = 34;
                    $arquivo->mID_SOLICITA_VISTO = $id_solicita_visto;
                    $arquivo->mNU_EMPRESA = $cand->mNU_EMPRESA;
                    $arquivo->salvaEArmazena();
                }
            }
        }
    }

    public function Redirecione() {
        
    }

}
