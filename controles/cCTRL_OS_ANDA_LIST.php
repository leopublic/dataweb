<?php

/**
 * Controller de montagem do protocolo de BSB
 */
class cCTRL_OS_ANDA_LIST extends cCTRL_MODELO_LIST {

    public function __construct() {

    }

    public function InicializeModelo() {
        $this->modelo = new cORDEMSERVICO();
    }

    public function InicializeTemplate() {
        cHTTP::$comCabecalhoFixo = true;
        $this->template = new cTMPL_OS_ANDA_LIST();
    }

    public function get_cursorListagem(cTEMPLATE_LISTAGEM $ptmpl) {
        if (!$this->template->mFoiPostado) {
            $this->template->mFiltro['id_status_sol']->mValor = cSTATUS_SOLICITACAO::ssENVIADA_BSB;
        }

        if (intval($this->template->mFiltro['id_status_sol']->mValor) == 0) {
            $this->template->mFiltro['id_status_sol']->mValor = 'x';
            $this->template->mFiltro['id_status_sol']->mWhere = " s.id_status_sol in (".cSTATUS_SOLICITACAO::statusDoAndamento().") ";
        }

        if ($this->template->mFiltro['dt_solicitacao_ini']->mValor != '') {
            $this->template->mFiltro['dt_solicitacao_ini']->mWhere = " s.dt_solicitacao > ".cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_ini'));
        }

        if ($this->template->mFiltro['dt_solicitacao_fim']->mValor != '') {
            $this->template->mFiltro['dt_solicitacao_fim']->mWhere = " s.dt_solicitacao > ".cBANCO::DataOk($this->template->getValorFiltro('dt_solicitacao_fim'));
        }


        $qtdReg = $this->modelo->qtdProtocoloDoDia($this->template->mFiltro);

        $offset = $this->template->get_offset();
        if ($offset > 0) {
            $qtdPag = floor($qtdReg / $this->template->get_offset());
        } else {
            $qtdPag = 1;
        }
        $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS, $qtdPag);

        $cursor = $this->modelo->CursorProtocoloDoDia($this->template->mFiltro, $this->template->get_ordenacao(), $this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL), $this->template->get_offset());
        return $cursor;
    }

    public function ProcessePost() {
        if ($this->template->get_acaoPostada() == 'PRIMEIRA') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, 1);
        }
        if ($this->template->get_acaoPostada() == 'PROXIMA') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) + 1);
        }
        if ($this->template->get_acaoPostada() == 'ANTERIOR') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL)) - 1);
        }
        if ($this->template->get_acaoPostada() == 'ULTIMA') {
            $this->template->setValorCampo(cTEMPLATE_LISTAGEM::CMP_PAGINAATUAL, intval($this->template->getValorCampo(cTEMPLATE_LISTAGEM::CMP_QTDPAGINAS)));
        }
    }

    public function Redirecione() {

    }

}
