<?php

set_time_limit(0);

$dsn = 'mysql:host=127.0.0.1;dbname=dataweb;default-character-set:latin1';
$db_pdo = new PDO($dsn, 'dataweb', 'gg0rezqn');

$sql = "select * from arquivos_faltando ";
$res = $db_pdo->query($sql);
$total = 0;
$erro = 0;

$raiz = 'C:\\dataweb_antigo\\arquivos\\';
$raiz_nova = 'C:\\_arquivos_encontrados\\';
$encontrados = array();
while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
    $total++;
    if ($rs['ID_SOLICITA_VISTO'] > 0) {
        $dir = $raiz . 'solicitacoes\\' . $rs['NU_EMPRESA'] . '\\' . $rs['NO_ARQUIVO'];
    } else {
        if ($rs['NU_EMPRESA'] == '') {
            $rs["NU_EMPRESA"] = 0;
        }
        $dir = $raiz . "documentos\\" . $rs['NU_EMPRESA'] . "\\" . $rs['NU_CANDIDATO'] . "\\" . $rs['NO_ARQUIVO'];
    }
    if (!file_exists($dir)) {
        $erro++;
        print "\n" . $dir . '    --->Nao encontrado';
    } else {
        $encontrados[] = $rs;
//                print "\nArquivo " . $rs['NU_SEQUENCIAL'] . ' - ' . $dir . ' (' . $rs['DT_INCLUSAO'] . ') OK.';                
    }
}

foreach ($encontrados as $rs) {
    if ($rs['ID_SOLICITA_VISTO'] > 0) {
        $dir = $raiz . 'solicitacoes\\' . $rs['NU_EMPRESA'] . '\\' . $rs['NO_ARQUIVO'];
        $dir_novo = $raiz_nova . 'solicitacoes\\' . $rs['NU_EMPRESA'] . '\\' . $rs['NO_ARQUIVO'];
        $caminho_novo = $raiz_nova . 'solicitacoes\\' . $rs['NU_EMPRESA'];
    } else {
        if ($rs['NU_EMPRESA'] == '') {
            $rs["NU_EMPRESA"] = 0;
        }
        $dir = $raiz . "documentos\\" . $rs['NU_EMPRESA'] . "\\" . $rs['NU_CANDIDATO'] . "\\" . $rs['NO_ARQUIVO'];
        $dir_novo = $raiz_nova . "documentos\\" . $rs['NU_EMPRESA'] . "\\" . $rs['NU_CANDIDATO'] . "\\" . $rs['NO_ARQUIVO'];
        $caminho_novo = $raiz_nova . "documentos\\" . $rs['NU_EMPRESA'] . "\\" . $rs['NU_CANDIDATO'];
    }
    mkdir($caminho_novo, 0777, true);
    copy($dir, $dir_novo);
    print "\n" . $dir . '    --->Encontrado';
}

print "\n\nTotal de arquivos verificados: " . $total;
print "\nTotal nao encontrados: " . $erro;
