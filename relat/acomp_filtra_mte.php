<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
if($ehCliente=="S") {
  $idEmpresa = cSESSAO::$mcd_empresa;
}

$idEmbarcacaoProjeto = $_POST['idEmbarcacaoProjeto'];
$fase = 0+$_POST['fase'];
$cd_solicitacao = $_POST['cd_solicitacao'];

$empCmb="";
if(strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $projCmb = montaComboProjetos("",$idEmpresa,"ORDER BY NO_EMBARCACAO_PROJETO");
} else {
  $empCmb = montaComboEmpresas($codigo,"ORDER BY NO_RAZAO_SOCIAL");
}


if($lang=="E") {
  $titulo = "Process per Client";
  $titEmpresa = "Client";
  $titProjeto = "Project/Vessel";
  $titFiltroEmpresa = "Company Filter";
  $titGerarRelatorio = "List Report";
  $titTodos = "All";
  $titAlerta = "Changed";
  $titFim = "Finally";
  $titCadastrado = "Cadastrado";
  $titHoje = "Today";
  $tit3dias = "Last 3 days";
  $titSemana = "One Week";
  $tit2Semana = "Two Week";
  $titmes = "One Month";
} else {
  $titulo = "Acompanhamento de Processos por Empresa";
  $titEmpresa = "Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titGerarRelatorio = "Listar Relatório";
  $titFiltroEmpresa = "Filtro Empresa";
  $titTodos = "Todos";
  $titAlerta = "Alterados";
  $titFim = "Finalizados";
  $titCadastrado = "Cadastrados";
  $titHoje = "Hoje";
  $tit3dias = "Últimos 3 dias";
  $titSemana = "1 Semana";
  $tit2Semana = "2 Semanas";
  $titmes = "1 Mes";
}

echo Topo($opcao);
echo Menu($opcao);

?>

<form name=relatorio method=post action="acomp_lista_mte.php">
<input type=hidden name=fase value='1'>
<input type=hidden name=totCand value='<?=$totCand?>'>
<br><center>
<table border=0 width="680" class='textoazul'>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="3">
	<p align="center" class="textoazul"><strong>:: <?=$titulo?> ::</strong></p>				
  </td>
 </tr>
 <tr><td colspan="3"><br></td></tr>
<?php
  if(strlen($idEmpresa)>0){
    echo "<input type=hidden name=idEmpresa value='$idEmpresa'>\n";
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "    <td>&#160;$nomeEmpresa</td>\n";
    echo "<td>&#160;</td>";
    echo "</tr>";
    echo "<tr><td class='textoazul'>".$titProjeto.":</td>\n";
    echo "     <td><select name=idEmbarcacaoProjeto><option value=''>$titTodos ... $projCmb</select></td>\n";
    echo "<td>&#160;</td>";
    echo "</tr>";
  } else {
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "     <td><select name=idEmpresa><option value=''>$titTodos ... $empCmb</select></td>\n";
    echo "<td><input type=button style=\"<?=$estilo?>\" value='$titFiltroEmpresa' onclick='javascript:Filtrar();'></td>";
    echo "<td>&#160;</td>";
    echo "</tr>";
  }
  echo "<tr><td class='textoazul'>Solicita&ccedil;&atilde;o:</td>\n";
  echo "    <td>&#160;<input type=text class=textoazulpeq siz=10 maxlength=5 name='cd_solicitacao' value='$cd_solicitacao'></td>\n";
  echo "<td>&#160;</td>";
?>
</table>
<br>
<table width=680 border=0 cellspacing=0 cellpadding=0 class='textoazul'>
<tr>
 <td><input type=radio name=status value='A' checked> <?=$titAlerta?></td>
 <td><input type=radio name=status value='C' > <?=$titCadastrado?></td>
 <td><input type=radio name=status value='AC' > <?=$titAlerta?>/<?=$titCadastrado?></td>
 <td><input type=radio name=status value='F' > <?=$titFim?></td>
 <td><input type=radio name=status value='' > <?=$titTodos?></td>
</tr>
<tr>
  <td><input type=radio name=prazo value='1' checked> <?=$titHoje?></td>
  <!-- td><input type=radio name=prazo value='3'> <?=$tit3dias?></td -->
  <td><input type=radio name=prazo value='7'> <?=$titSemana?></td>
  <td><input type=radio name=prazo value='15'> <?=$tit2Semana?></td>
  <td><input type=radio name=prazo value='30'> <?=$titmes?></td>
  <td><input type=radio name=prazo value=''> <?=$titTodos?></td>
</tr>
</table>
<br>
<input type="button" class="textformtopo" style="<?=$estilo?>" value="<?=$titGerarRelatorio?>" onclick="javascript:Enviar();">
</form>

<?php
echo Rodape($opcao);
?>

<script language="javascript">
function Filtrar() {
  var emp = document.relatorio.idEmpresa[document.relatorio.idEmpresa.selectedIndex].value;
  var sol = document.relatorio.cd_solicitacao.value;
  if( (emp.length == 0) && (sol.length == 0) ) {
    alert("Escolha a empresa ou o numero da solicitacao para filtrar.");
  } else {
    document.relatorio.action = "acomp_filtra_mte.php";
    document.relatorio.submit();
  }
}
function Enviar() {
  document.relatorio.action = "acomp_lista_mte.php";
<?php if(strlen($idEmpresa)>0){ ?>
  var emp = document.relatorio.idEmpresa.value;
<?php } else { ?>
  var emp = document.relatorio.idEmpresa[document.relatorio.idEmpresa.selectedIndex].value;
<?php } ?>
  if(emp.length == 0) { 
     document.relatorio.submit();
//     alert("Escolha a empresa");
  } else {
     document.relatorio.submit();
  }
}
</script>
