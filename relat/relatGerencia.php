<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
$idEmbarcacaoProjeto = "";
$fase = 0+$_POST['fase'];

if($ehCliente=="S") {
  $idEmpresa = cSESSAO::$mcd_empresa;
}

$empCmb="";
if(strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $embCmb = montaComboProjetos("",$idEmpresa," ORDER BY NO_EMBARCACAO_PROJETO");
} else {
  $empCmb = montaComboEmpresas($codigo,"ORDER BY NO_RAZAO_SOCIAL");
}

echo Topo($opcao);
echo Menu("REL");

if($lang=="E") {
  $titulo = "Management of Period";
  $titEmpresa = "Company";
  $titProjeto = "Project/Vessel";
  $titOrdenar = "Classify"; 
  $titFiltro = "Filter";
   $titTodos = "All";
} else {
  $titulo = "Gerencia de Prazo por Empresa/Projeto/Embarcação";
  $titEmpresa = "Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titOrdenar = "Ordenar"; 
  $titFiltro = "Filtro";
   $titTodos = "Todos";
}

?>
<div class="conteudo">
	<div class="titulo"><div style="float:left"><?=$titulo?></div>&nbsp;</div>
	<div class="conteudoInterno">				

<form name=relatorio method=post>
<input type=hidden name=fase value='1'>
  <table border=0 width="650" class='textoazul'>
   <tr><td width=200>&#160;</td><td width=250>&#160;</td><td width=200>&#160;</td></tr>
		
<?php
  if(strlen($idEmpresa)>0){
    echo "<input type=hidden name=idEmpresa value='$idEmpresa'>\n";
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "    <td colspan=2>&#160;$nomeEmpresa</td>\n";
    echo "</tr>\n";	
    echo "<tr><td class='textoazul'><td>&#160;</td></tr>\n";
    echo "<tr><td class='textoazul'>".$titProjeto.":</td>\n";
    echo "    <td colspan=2><select name=idProjeto><option value=''>$titTodos ... $embCmb</select></td>\n";
    echo "</tr>";
  } else {
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "     <td><select name=idEmpresa><option value=''>$titTodos ... $empCmb</select></td>\n";
   echo "     <td>\n";
    echo "<input type='button' class='textformtopo' style=\"$estilo\" value='Filtro Empresa' onclick=\"javascript:Empresa();\">";
    echo "</td></tr>";
  }
?>
 <tr><td class='textoazul'><td>&#160;</td></tr>
 <tr>
  <td class='textoazul'><?=$titOrdenar?>:</td>
  <td colspan=2><select name='orderby'>

<?php
  if($lang=="E") {
     print "            <option value=''>Choose ...</option>";
     print "            <option value='AC.DT_PRAZO_ESTADA asc'>Visa Validity</option>";
     print "            <option value='NOME_CANDIDATO asc'>Name</option>";
} else {
     print "            <option value=''>Selecione ...</option>";
     print "            <option value='AC.DT_PRAZO_ESTADA asc'>Prazo de Estada</option>";
     print "            <option value='NOME_CANDIDATO asc'>Nome do Candidato</option>";
}
?>
          </select>
  </td>
 </tr>
 <tr><td class='textoazul'><td>&#160;</td></tr>
 <tr>
  <td class='textoazul'><?=$titFiltro?>:</td>
  <td colspan=2><select name='filtro'>

<?php
  if($lang=="E") {
    print "            <option value='all'>All</option>";
    print "            <option value='alerta'>With alert</option>";
    print "            <option value='estada60'>Visa Validity less than 60 days</option>";
    print "            <option value='estada'>Visa Validity less than 180 days</option>";
    print "            <option value='pass'>Passaport Validity less than 180 days</option>";
    print "            <option value='prot'>Validity of the Extension less than 45 days</option>";
    print "            <option value='cie'>Registration ID Card Protocol Validity less than 45 days</option>";
  } else {
    print "            <option value='all'>Todos os candidatos</option>";
    print "            <option value='alerta'>Candidatos com alerta</option>";
    print "            <option value='estada60'>Prazo de estada menor que 60 dias</option>";
    print "            <option value='estada'>Prazo de estada menor que 180 dias</option>";
    print "            <option value='pass'>Vencimento do passaporte menor que 180 dias</option>";
    print "            <option value='prot'>Prorrogação vencendo a menos de 45 dias</option>";
    print "            <option value='cie'>Protocolo CIE vencendo a menos de 45 dias</option>";
  }
?>

          </select>
  </td>
 </tr>
</table>
<br>
<input type="button" class="textformtopo" style="<?=$estilo?>" value="Gerar Relatório" onclick="javascript:Relatorio();">
</form>

<script language="JavaScript">
function Empresa() {
  document.relatorio.action='relatGerencia.php';
  document.relatorio.target="_top";
  document.relatorio.submit();
}
function Relatorio() {
  document.relatorio.action='relatGerenciaGerar.php';
  document.relatorio.target='newwinger<?=idEmpresa?>';
  document.relatorio.submit();
}
</script>

<?php
echo Rodape($opcao);
?>

