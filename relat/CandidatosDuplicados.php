<?php

    require_once '../LIB/ControleEventos.php';
    
    $app = new Generic;
    $app->filtro = $_POST['relat'];  // Filtro é os campos POST que será o filtro das colunas da Grid
    
    $html = new html;
    $html->cache_submit = true;      // Quando dar o Submit no formulários ele quarda o e exibe seu valor "post" novamente
  
  

    if($excel):   // se excel estiver ok
       $nome_arquivo = 'relatorio_('.date('d-m-Y').')_'.$app->str_basename($razaosocial).'-'.$app->str_basename($projeto); 
       header('Content-type: application/vnd.ms-excel'); 
       header('Content-type: application/force-download');
       header('Content-Disposition: attachment; filename='.$nome_arquivo.'.xls');
       header('Pragma: no-cache'); 
       
       echo '
       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
       <html><head>
       <style rel="stylesheet" type="text/css"/>'.file_get_contents('../estilos.css').'
           #TRelat th       {background:#003366;color:#fff}
           #TRelat          {font:10px verdana;color:#000}
           #TRelat          {border:1px solid #c0c0c0}
           #TRelat tbody td {border:1px solid #D8D8D8}
       </style></head><body>';
       
    else:  
?>
<script>
$(document).ready(function(){ 

   $('#NU_EMPRESA').change(function(){
      $.post('../ajax/AjaxUtils.php?name=embarcacao&id='+$(this).val(),function(d){
        $('#NU_EMBARCACAO').html(d);
      });
   }); 
   
   if($('.todos').attr('checked')=='true'){
        $('#TFiltro input').attr('checked', true);
   }
   
    $('.todos').on('click',function(event){
        if($('.todos').attr('checked')){
           $('#TFiltro input').attr('checked', true);
           $('#TRelat tr td').show();
           $('#TRelat tr th').show();
        }else{
           
           $('#TFiltro input').attr('checked', false);
           $('#TRelat td').hide();   
        }
    });
    $('#TFiltro input[type="checkbox"]').on('click',function(event){
       $('.todos').attr('checked', false); 
    })

   filtroDinamico('#TFrmRelat', '#TRelat');
   $('#getEXCEL').on('click',function(event){
        $('#excel').val('true');
        $('#TFrmRelat').submit();
   })
});
</script>
<div class="titulo"> 
    Relatório / Candidatos duplicados 
    <?if($isPost): ?><button id="getEXCEL" style="float: right;"> <img src="/imagens/icons/page_excel.png" /> Gerar Excel</button><?endif;?>
</div>
    <form method="post" id="TFrmRelat" name="TFrmRelat" class="boxer edicao" style="width: 98%;">
        <input type="hidden" id="excel" name="excel" value="" />
        Empresa :<?=$app->combo_empresa($app->p('NU_EMPRESA'))?> | Embarcação/Projeto : <?=$combo_EmbarcacaoEmpresa?> |  
        <button onclick="forms['TFrmRelat'].submit()"><img src="/imagens/icons/zoom.png" /> Gerar</button><br /> <br />
			<?=$html->input('hidden', array('name'=>'relat[NU_CANDIDATO]', 'value'=>'Id'))?> Id
			<?=$html->input('hidden', array('name'=>'relat[NOME_COMPLETO]', 'value'=>'Nome'))?> Nome
			<?=$html->input('hidden', array('name'=>'relat[NO_RAZAO_SOCIAL]', 'value'=>'Nome'))?> Empresa
			<?=$html->input('hidden', array('name'=>'relat[NO_EMBARCACAO_PROJETO]', 'value'=>'Embarcação/Projeto'))?> Embarcação/Projeto
    </form>
    <?endif; ?>
    
    
    
    <!-- inicio relatório -->
    <?if($isPost): ?>
    <p align="center">:: Relatório de Candidatos duplicados ::</p>
    <div align="center" class="formtextoitalico"> <?='Empresa: '.$razaosocial;?> - <?=$projeto;?> </div>
    <table id="TRelat" class="orderby destaque relatorio grade" cellspacing="1" style="text-align: left;">
    <thead>
        <tr>
           <th width="30">Seq</th>
           <?=$app->TitulosDinamicos()?>           
        </tr>
    </thead>
    <tbody>
    <?foreach($candidatos as $c): $SEQ++?>
        <tr>
           <td><?=$SEQ?></td>
           <?=$app->ColunasDinamicas($c)?>
        </tr>
    <?endforeach;?>
    </tbody>
    </table>
    <?endif?>
    <!-- Fim relatorio -->

 </body>
</html>
