<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}

if($ehCliente=="S") {
  $idEmpresa = cSESSAO::$mcd_empresa;
}

$idEmbarcacaoProjeto = "";

$empCmb="";
if(strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $embCmb = montaComboProjetos("",$idEmpresa," ORDER BY NO_EMBARCACAO_PROJETO");
} else {
  $empCmb = montaComboEmpresas($codigo,"ORDER BY NO_RAZAO_SOCIAL");
}

if($lang=="E") {
  $titulo = "Synthetic Report";
  $titEmpresa = "Company";
  $titFiltroEmpresa = "Company Filter";
  $titProjeto = "Project/Vessel";
  $titRelatSint = "Synthetic Report"; 
  $titRelatDetalhe = "List Candidates, showing";
  $titNome = "Name";
  $titNacionalidade = "Nationality";
  $titFuncao = "Function";
  $titPassaport = "Passport Number";
  $titDatPassap = "Passport Date";
  $titRNE = "Registration Number";
  $titEstada = "Visa Validity";
  $titProtocoloCIE = "Registration ID Card Protocol";
  $titValidadeCIE = "ID Card Validity";
  $titNumProrr = "Extension Number";
  $titValProrr = "Validity of the Extension";
  $titPretProrr = "Expected Extension Validity Date";
  $titGerarRelatorio = "Generate Report";
  $titMTEnum = "MTE Process Number";
  $titMTEdata = "MTE Process Date";
  $titObservacao = "Observation";
  $titConsul = "Consul";
  $titRemMensal = "Salary";
  $titRemBrasil = "Salary in Brazil";
  $titPrazoSol = "Term Requested";
  $titTodos = "All";
} else {
  $titulo = "Relatório Sintético da Relação Estrangeiro";
  $titEmpresa = "Empresa";
  $titFiltroEmpresa = "Filtro Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titRelatSint = "Relatório Sintético"; 
  $titRelatDetalhe = "Relaciona Candidatos, mostrando";
  $titNome = "Nome";
  $titNacionalidade = "Nacionalidade";
  $titFuncao = "Função";
  $titPassaport = "Num. Passaporte";
  $titDatPassap = "Data Val Passaporte";
  $titRNE = "RNE";
  $titEstada = "Prazo Estada";
  $titProtocoloCIE = "Num Protocolo CIE";
  $titValidadeCIE = "Data Validade CIE";
  $titNumProrr = "Num Protocolo Prorrog";
  $titValProrr = "Data Validade da Prorrog";
  $titPretProrr = "Data Pretendida Prorrog";
  $titGerarRelatorio = "Gerar Relatório";
  $titMTEnum = "Numero do Processo MTE";
  $titMTEdata = "Data no MTE";
  $titConsul = "Consulado";
  $titRemMensal = "Salario Mensal";
  $titRemBrasil = "Salario Brasil";
  $titPrazoSol = "Prazo Solicitado";
  $titObservacao = "Observa&ccedil;&atilde;o";
  $titTodos = "Todos";
}

echo Topo($opcao);
echo Menu("REL");

?>
<div class="conteudo">
	<div class="titulo"><div style="float:left"><?=$titulo?></div>&nbsp;</div>
	<div class="conteudoInterno">				

<form name=relatorio method=post>
  	<table border=0 width="80%">
			
<?php
  if(strlen($idEmpresa)>0){
    echo "<input type=hidden name=idEmpresa value='$idEmpresa'>\n";
    echo "<tr><td class='textoazul'>$titEmpresa :</td>\n";
    echo "    <td class='textoazul'>&#160;$nomeEmpresa</td>\n";
    echo "    <td>&#160;</tr>\n";	
    echo "<tr><td class='textoazul'>$titProjeto :</td>\n";
    echo "    <td><select name=idProjeto><option value=''>$titTodos ... $embCmb</select></td>\n";
    echo "    <td>&#160;</tr></tr></table><br>\n";
    echo "<input type='button' class='textformtopo' style=\"$estilo\" value='$titGerarRelatorio' onclick='javascript:Relatorio();'>\n";
  } else {
    echo "<tr><td class='textoazul'>Empresa:</td>\n";
    echo "     <td><select name=idEmpresa><option value=''>$titTodos ... $empCmb</select></td>\n";
    echo "     <td>\n";
    echo "<input type='button' class='textformtopo' style=\"$estilo\" value='<?=$titFiltroEmpresa?>' onclick=\"javascript:Empresa();\">";
    echo "</td></tr></table>";
  }
?>
</form>

<script language="JavaScript">
function Empresa() {
  document.relatorio.action='relatSintetico.php';
  document.relatorio.submit();
}
function Relatorio() {
  document.relatorio.action='relatSinteticoGerar.php';
  document.relatorio.target='newsin<?=idEmpresa?>';
  document.relatorio.submit();
}</script>

<?php
echo Rodape($opcao);
?>

