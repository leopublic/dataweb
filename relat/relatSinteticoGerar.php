<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/geral.php");

$idEmpresa = 0+$_POST['idEmpresa'];
$idProjeto = 0+$_POST['idProjeto'];

$listaVistos = listaGeral("TIPO_AUTORIZACAO","");
$totVist = mysql_num_rows($listaVistos);
if($totVist==0) {
  $msg = "Nenhum tipo de visto foi encontrado.";
} else {
  $x=0;
  while($rw1=mysql_fetch_array($listaVistos)) {
    $x++;
    $cod = $rw1['CO_TIPO_AUTORIZACAO'];
    $nom = $rw1['NO_TIPO_AUTORIZACAO'];
    $red = $rw1['NO_REDUZIDO_TIPO_AUTORIZACAO'];
    $cdvist[$x] = $cod;
    $nmvist[$x] = $nom;
    $reduz[$x] = $red;
    $qtdvistos[$cod] = 0;
  }
# print_r($vistos);
}

if($idProjeto>0) {
   $listaProjs = listaGeral("EMBARCACAO_PROJETO"," WHERE NU_EMPRESA=$idEmpresa AND NU_EMBARCACAO_PROJETO=$idProjeto");
} else {
   $listaProjs = listaGeral("EMBARCACAO_PROJETO"," WHERE NU_EMPRESA=$idEmpresa");
}
$totProjs = mysql_num_rows($listaProjs);
if($totProjs==0) {
  $msg = "Nenhum projeto para a empresa '".$idEmpresa."' foi encontrado.";
} else {
  $totProjs=0;
  $x = 0;
  while($rw=mysql_fetch_array($listaProjs)) {
     $x++;
     $codsproj[$x]=$rw['NU_EMBARCACAO_PROJETO'];
     $nomeproj[$x]=$rw['NO_EMBARCACAO_PROJETO'];
     $qtdproj[$x]=$qtdvistos;
  }
  $totProjs=$x;
}


$sql = "select count(NU_CANDIDATO) as QTD,NU_EMPRESA,NU_EMBARCACAO_PROJETO,CO_TIPO_AUTORIZACAO from AUTORIZACAO_CANDIDATO ";
$sql = $sql." where NU_EMPRESA=$idEmpresa ";

if(strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
}
if( (strlen($idProjeto)>0) ) {
  if($idProjeto>0) {
    $nomeProjeto = pegaNomeProjeto($idProjeto,$idEmpresa);
    $sql = $sql." AND NU_EMBARCACAO_PROJETO=$idProjeto ";
  }
}
$sql = $sql." GROUP BY NU_EMPRESA,NU_EMBARCACAO_PROJETO,CO_TIPO_AUTORIZACAO ";
$lista = mysql_query($sql);
if(mysql_errno()>0) {
  $msg = mysql_error();
}

$x = 0;
while($rw1=mysql_fetch_array($lista)) {
  $NU_EMPRESA = 0+$rw1['NU_EMPRESA'];
  $idProjeto = 0+$rw1['NU_EMBARCACAO_PROJETO'];
  $CO_TIPO_AUTORIZACAO = $rw1['CO_TIPO_AUTORIZACAO'];
  $QTD = $rw1['QTD'];
  $indice = array_search($idProjeto,$codsproj);
  if($indice != false) {
    $auxqtd = $qtdproj[$indice];
    $auxqtd[$CO_TIPO_AUTORIZACAO]=$QTD;
    $qtdproj[$indice] = $auxqtd;
  }
}

if($lang=="E") {
  $titulo = "Synthetic Report";
  $titEmpresa = "Company";
  $titFiltroEmpresa = "Company Filter";
  $titProjeto = "Project/Vessel";
  $titRelatSint = "Synthetic Report"; 
  $titRelatDetalhe = "List Candidates, showing";
  $titNome = "Name";
  $titNacionalidade = "Nationality";
  $titFuncao = "Function";
  $titPassaport = "Passport Number";
  $titDatPassap = "Passport Date";
  $titRNE = "Registration Number";
  $titEstada = "Visa Validity";
  $titProtocoloCIE = "Registration ID Card Protocol";
  $titValidadeCIE = "ID Card Validity";
  $titNumProrr = "Extension Number";
  $titValProrr = "Validity of the Extension";
  $titPretProrr = "Expected Extension Validity Date";
  $titGerarRelatorio = "Generate Report";
  $titMTEnum = "MTE Process Number";
  $titMTEdata = "MTE Process Date";
  $titObservacao = "Observation";
  $titConsul = "Consul";
  $titRemMensal = "Salary";
  $titRemBrasil = "Salary in Brazil";
  $titPrazoSol = "Term Requested";
  $titTodos = "All";
  $titErr = "<font color=red>An error occur while consulting the report.\n<br><br>Please, try again. If the problem occur again, call the administrator.<br>";
} else {
  $titulo = "Relatório Sintético da Relação Estrangeiro";
  $titEmpresa = "Empresa";
  $titFiltroEmpresa = "Filtro Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titRelatSint = "Relatório Sintético"; 
  $titRelatDetalhe = "Relaciona Candidatos, mostrando";
  $titNome = "Nome";
  $titNacionalidade = "Nacionalidade";
  $titFuncao = "Função";
  $titPassaport = "Num. Passaporte";
  $titDatPassap = "Data Val Passaporte";
  $titRNE = "RNE";
  $titEstada = "Prazo Estada";
  $titProtocoloCIE = "Num Protocolo CIE";
  $titValidadeCIE = "Data Validade CIE";
  $titNumProrr = "Num Protocolo Prorrog";
  $titValProrr = "Data Validade da Prorrog";
  $titPretProrr = "Data Pretendida Prorrog";
  $titGerarRelatorio = "Gerar Relatório";
  $titMTEnum = "Numero do Processo MTE";
  $titMTEdata = "Data no MTE";
  $titConsul = "Consulado";
  $titRemMensal = "Salario Mensal";
  $titRemBrasil = "Salario Brasil";
  $titPrazoSol = "Prazo Solicitado";
  $titObservacao = "Observa&ccedil;&atilde;o";
  $titTodos = "Todos";
  $titErr = "<font color=red>Ocorreu um erro ao consultar Relatório de Estrangeiros.\n<br><br>Favor, tente novamente. Se o problema persistir, contate o administrador do site.<br>";
}

?>
<html><head><title>Mundivisas - Intranet</title>
<link href='/estilos.css' rel='stylesheet' type='text/css'></head>
<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>
<form name=relatorio method=post>
<br><center>
<table border=0 width="90%">
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="3">
   <p align="center" class="textoazul"><strong>:: <?=$titulo?> ::</strong></p>				
  </td>
 </tr>
 <tr><td>&#160;</td></tr>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="3">
   <p align="center" class="textoazul"><?=pegaNomeEmpresa($idEmpresa)?></p>				
  </td>
 </tr>
</table>
<br>
<?php
if(strlen($msg)>0) {
  echo "<font color=red>Ocorreu um erro ao consultar Relatório de Estrangeiros.\n<br><br>Favor, tente novamente. Se o problema persistir, contate o administrador do site.<br>";
  echo "<br><br>Err.: $msg<br>";
  echo "\n\n<!-- ERRO: $msg -->";
  echo "\n\n<!-- SQL: $sql -->";
  exit;
} else {
  echo "\n\n<!-- SQL: $sql -->";
  echo "\n<!-- lista = ".mysql_num_rows($lista)." registros. -->\n";
  echo "\n\n<!-- SQL: $sql -->\n\n";

  echo "<table border=1 cellpadding=0 cellspacing=0 class='textoazulPeq'>\n";
  echo "<tr><td align=center width=140><b>$titProjeto</td>\n";
  for($y1=0;$y1<$totVist;$y1++) {
     $y2=$y1+1;
#     echo "<td align=center><b>".$nmvist[$y2]."</td>\n";
     echo "<td align=center width=80><b>".$reduz[$y2]."</td>\n";
  }  
  echo "</tr>\n";

  $qtdcli = $qtdvistos;
  for($x1=0;$x1<$totProjs;$x1++) {
    $x2=$x1+1;
    $cdproj = $codsproj[$x2];
    $nmproj = $nomeproj[$x2];
    $projqtd = $qtdproj[$x2];
    echo "<tr><td width=200><b>$nmproj</td>\n";
    for($y1=0;$y1<$totVist;$y1++) {
       $y2=$y1+1;
       $cdvis = $cdvist[$y2];
       $qtdemb = $projqtd[$cdvis];
       $qtdcli[$cdvis] = $qtdcli[$cdvis] + $qtdemb;
       echo "<td align=center>".$qtdemb."</td>\n";
    }  
    echo "</tr>\n";
  }
  echo "<tr><td colspan=".($totVist+1).">&#160;</td></tr>";
  echo "<tr><td><b>Total</td>\n";
    for($y1=0;$y1<$totVist;$y1++) {
       $y2=$y1+1;
       $cdvis = $cdvist[$y2];
       $qtdemb = $qtdcli[$cdvis];
       echo "<td align=center>".$qtdemb."</td>\n";
    }  
  echo "</tr>\n";
  echo "</table>";
}
?>			
