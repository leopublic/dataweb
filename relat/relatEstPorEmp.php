<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/relat/libEstPorEmpGerar.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
if($ehCliente=="S") {
  $idEmpresa = cSESSAO::$mcd_empresa;
}

$idEmbarcacaoProjeto = "";
$fase = 0+$_POST['fase'];
if( ($_POST['relac']=='y') || ($fase==0) )  { $relac="checked"; }
if( ($_POST['relsin']=='y') || ($fase==0) ) { $relsin="checked"; }
if( ($_POST['nome']=='y') || ($fase==0) ) { $nome="checked"; }
if( ($_POST['nacional']=='y') || ($fase==0) ) { $nacional="checked"; }
if( ($_POST['funcao']=='y') || ($fase==0) ) { $funcao="checked"; }
if( ($_POST['passnum']=='y') || ($fase==0) ) { $passnum="checked"; }
if( ($_POST['passdata']=='y') || ($fase==0) ) { $passdata="checked"; }
if( ($_POST['rne']=='y') || ($fase==0) ) { $rne="checked"; }
if( ($_POST['estada']=='y') || ($fase==0) ) { $estada="checked"; }
if( ($_POST['cieprot']=='y') || ($fase==0) ) { $cieprot="checked"; }
if( ($_POST['cieval']=='y') || ($fase==0) ) { $cieval="checked"; }
if( ($_POST['prorrnum']=='y') || ($fase==0) ) { $prorrnum="checked"; }
if( ($_POST['prorrval']=='y') || ($fase==0) ) { $prorrval="checked"; }
if( ($_POST['prorrpre']=='y') || ($fase==0) ) { $prorrpre="checked"; }

$empCmb="";
if(strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $lstCand = ListaCandidatos($idEmpresa,"");
  $totCand = count($lstCand);
  if($totCand>0) {
     for($x=0;$x<$totCand;$x++) {
         $aux1 = "cand$x";
         $aux2 = $lstCand[$x];
#print_r($aux2);
         $cod = $aux2->getNU_CANDIDATO();
         $nm = $aux2->getNOME_CANDIDATO();
         $empCmb = $empCmb."<br><input type=checkbox name='$aux1' value='$cod' > $nm";
     }
# print $empCmb;
  }
} else {
  $empCmb = montaComboEmpresas($codigo,"ORDER BY NO_RAZAO_SOCIAL");
}

echo Topo($opcao);
echo Menu("REL");

if($lang=="E") {
  $titulo = "Foreigners per Client";
  $titEmpresa = "Company";
  $titFiltroEmpresa = "Company Filter";
  $titProjeto = "Project/Vessel";
  $titCandidatos = "Candidates";
  $titRelatSint = "Synthetic Report"; 
  $titRelatDetalhe = "List Candidates, showing";
  $titNome = "Name";
  $titNacionalidade = "Nationality";
  $titFuncao = "Function";
  $titPassaport = "Passport Number";
  $titDatPassap = "Passport Date";
  $titRNE = "Registration Number";
  $titEstada = "Visa Validity";
  $titProtocoloCIE = "Registration ID Card Protocol";
  $titValidadeCIE = "ID Card Validity";
  $titNumProrr = "Extension Number";
  $titValProrr = "Validity of the Extension";
  $titPretProrr = "Expected Extension Validity Date";
  $titGerarRelatorio = "Generate Report";
  $titTodos = "All";
} else {
  $titulo = "Relatório de Estrageiros por Empresa";
  $titEmpresa = "Empresa";
  $titFiltroEmpresa = "Filtro Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titCandidatos = "Candidatos";
  $titRelatSint = "Relatório Sintético"; 
  $titRelatDetalhe = "Relaciona Candidatos, mostrando";
  $titNome = "Nome";
  $titNacionalidade = "Nacionalidade";
  $titFuncao = "Função";
  $titPassaport = "Num. Passaporte";
  $titDatPassap = "Data Val Passaporte";
  $titRNE = "RNE";
  $titEstada = "Prazo Estada";
  $titProtocoloCIE = "Num Protocolo CIE";
  $titValidadeCIE = "Data Validade CIE";
  $titNumProrr = "Num Protocolo Prorrog";
  $titValProrr = "Data Validade da Prorrog";
  $titPretProrr = "Data Pretendida Prorrog";
  $titGerarRelatorio = "Gerar Relatório";
  $titTodos = "Todos";
}

?>
<div class="conteudo">
	<div class="titulo"><div style="float:left"><?=$titulo?></div>&nbsp;</div>
	<div class="conteudoInterno">				

<form name=relatorio method=post>
<input type=hidden name=fase value='1'>
<input type=hidden name=totCand value='<?=$totCand?>'>
<?php
  if(strlen($idEmpresa)>0){
    echo "<input type=hidden name=idEmpresa value='$idEmpresa'>\n";
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "    <td>&#160;$nomeEmpresa</td>\n";
    echo "    <td>&#160;</tr>\n";	
    echo "<tr><td class='textoazul' valign='top'>".$titCandidatos.":</td>\n";
    echo "    <td>$empCmb</td>\n";
    echo "    <td>&#160;</tr></tr></table>";
  } else {
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "     <td><select name=idEmpresa><option value=''>$titTodos ... $empCmb</select></td>\n";
    echo "     <td>\n";
    echo "<input type='button' class='textformtopo' style=\"$estilo\" value='Filtro Empresa' onclick=\"javascript:Empresa();\">";
    echo "</td></tr></table>";
  }
?>
<table width=680 border=0 cellspacing=0 cellpadding=0 class='textoazul'>
<tr><td><input type=checkbox name=relsin value='y' <?=$relsin?>> <?=$titRelatSint?></td></tr>
<tr><td><input type=checkbox name=relac value='y' <?=$relac?> onclick='javascript:detalhes();'> <?=$titRelatDetalhe?>:</td></tr>
</table>
<table width=680 border=0 cellspacing=0 cellpadding=0 class='textoazul' id=tb_detalhes>
 <tr>
  <td width=50>&#160;</td>
  <td width=150><input type=checkbox name=nome value='y' <?=$nome?>> <?=$titNome?></td>
  <td width=250><input type=checkbox name=nacional value='y' <?=$nacional?>> <?=$titNacionalidade?></td>
  <td width=200><input type=checkbox name=funcao value='y' <?=$funcao?>> <?=$titFuncao?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=passnum value='y' <?=$passnum?>> <?=$titPassaport?></td>
  <td><input type=checkbox name=passdata value='y' <?=$passdata?>> <?=$titDatPassap?></td>
  <td><input type=checkbox name=rne value='y' <?=$rne?>> <?=$titRNE?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=estada value='y' <?=$estada?>> <?=$titEstada?></td>
  <td><input type=checkbox name=cieprot value='y' <?=$cieprot?>> <?=$titProtocoloCIE?></td>
  <td><input type=checkbox name=cieval value='y'<?=$cieval?>> <?=$titValidadeCIE?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=prorrnum value='y' <?=$prorrnum?>> <?=$titNumProrr?></td>
  <td><input type=checkbox name=prorrval value='y' <?=$prorrval?>> <?=$titValProrr?></td>
  <td><input type=checkbox name=prorrpre value='y' <?=$prorrpre?>> <?=$titPretProrr?></td>
 </tr>
</table>
<br>
<input type="button" class="textformtopo" style="<?=$estilo?>" value="<?=$titGerarRelatorio?>" onclick="javascript:Relatorio();">
</form>

<script language="JavaScript">
function Empresa() {
  document.relatorio.action='relatEstPorEmp.php';
  document.relatorio.target="_top";
  document.relatorio.submit();
}
function Relatorio() {
  document.relatorio.action='relatEstPorEmpGerar.php';
  document.relatorio.target='newwinproj<?=idEmpresa?>';
  document.relatorio.submit();
}
function detalhes() {
  if(document.relatorio.relac.checked == true) {
    tb_detalhes.style.display = 'block';
  } else {
    tb_detalhes.style.display = 'none';
  }
}
detalhes();
</script>

<?php
echo Rodape($opcao);
?>
