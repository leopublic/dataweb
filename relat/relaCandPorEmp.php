<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}

if($ehCliente=="S") {
  $idEmpresa = cSESSAO::$mcd_empresa;
}

echo Topo($opcao);
echo Menu($opcao);

?>
<form name="candidatos" action="#" method="post" style="margin: 0px auto; width: 90%;">
    <label>Empresa : </label><select name="NU_EMPRESA" id="NU_EMPRESA" onchange="navios(this.options[this.selectedIndex].value)">
                                <option value=""> - Selecione - </option>
                                <?=montaComboEmpresas($_POST['NU_EMPRESA'],"ORDER BY NO_RAZAO_SOCIAL");?>
                                </select>
    <label>Embarcação : </label><select name="NU_EMBARCACAO_PROJETO" id="NU_EMBARCACAO_PROJETO">
                                <?
                                if($NU_EMPRESA!=''){
                                    montaComboProjetos("",$idEmpresa," ORDER BY NO_EMBARCACAO_PROJETO");   
                                }else{
                                    print "<option value=''> - Selecione a empresa - </option>";
                                }
                                ?>
                                </select>
    <input type="submit" value=" Filtrar " />
</form><br /><br />
<script type="text/javascript" language="JavaScript">
    function navios(emprId){
        $.post('projetos_aux.php',{empresa:emprId},function(retorno){
            $("#NU_EMBARCACAO_PROJETO").html(retorno);
        });
    }
</script>
<?php
if($_POST['NU_EMPRESA']!=''){
    $emprId = $_POST['NU_EMPRESA'];

    if($_POST['NU_EMBARCACAO_PROJETO']!=''){
        $projeto = "AND B.NU_EMBARCACAO_PROJETO = ".$_POST['NU_EMBARCACAO_PROJETO'];
    }
    
    $query = "SELECT E.NO_RAZAO_SOCIAL,
           B.NO_EMBARCACAO_PROJETO,
           C.NU_CANDIDATO,
           C.NOME_COMPLETO,
           P.NO_NACIONALIDADE,
           F.NO_FUNCAO,
           C.NU_CPF,
           C.NU_PASSAPORTE,
           R.NO_REPARTICAO_CONSULAR,
           CASE WHEN PR.codigo IS NOT NULL THEN
              'Registro'
           ELSE
             CASE WHEN PE.codigo IS NOT NULL THEN
                'Emissao'
             ELSE
                 CASE WHEN PP.codigo IS NOT NULL THEN
                    'Prorrogacao'
                 ELSE
                     CASE WHEN PC.codigo IS NOT NULL THEN
                        'Coleta'
                     ELSE
                        CASE WHEN PX.codigo IS NOT NULL THEN
                            'Cancelamento'
                        ELSE
                            'MTE'
                        END
                    END
                 END
              END
           END processo,
           CASE WHEN PX.codigo IS NOT NULL THEN
                PX.nu_processo
           ELSE
                M.nu_processo
           END numero,
           CASE WHEN PR.codigo IS NOT NULL THEN
              PR.dt_validade
           ELSE
             CASE WHEN PE.codigo IS NOT NULL THEN
                PE.dt_validade
             ELSE
                 CASE WHEN PP.codigo IS NOT NULL THEN
                    PP.dt_validade
                 ELSE
                     CASE WHEN PC.codigo IS NOT NULL THEN
                        PC.no_validade
                     ELSE
                        CASE WHEN PX.codigo IS NOT NULL THEN
                            PX.dt_cancel
                        ELSE
                            M.dt_deferimento
                        END
                    END
                 END
              END
           END Validade,
           M.prazo_solicitado,
           T.NO_TIPO_AUTORIZACAO
    FROM processo_mte M
      LEFT JOIN CANDIDATO C ON C.NU_CANDIDATO = M.CD_CANDIDATO
      LEFT JOIN PAIS_NACIONALIDADE P ON P.CO_PAIS = C.CO_NACIONALIDADE
      LEFT JOIN EMPRESA E ON E.NU_EMPRESA = M.NU_EMPRESA
      LEFT JOIN EMBARCACAO_PROJETO B ON B.NU_EMBARCACAO_PROJETO = M.NU_EMBARCACAO_PROJETO
           AND B.NU_EMPRESA = E.NU_EMPRESA
      LEFT JOIN REPARTICAO_CONSULAR R ON R.CO_REPARTICAO_CONSULAR = M.CD_REPARTICAO
      LEFT JOIN FUNCAO_CARGO F ON F.CO_FUNCAO = M.CD_FUNCAO
      LEFT JOIN processo_regcie PR ON PR.codigo_processo_mte = M.codigo
           AND PR.fl_processo_atual = 1
      LEFT JOIN processo_emiscie PE ON PE.codigo_processo_mte = M.codigo
           AND PE.fl_processo_atual = 1
      LEFT JOIN processo_prorrog PP ON PP.codigo_processo_mte = M.codigo
           AND PP.fl_processo_atual = 1
      LEFT JOIN processo_coleta PC ON PC.codigo_processo_mte = M.codigo
           AND PC.fl_processo_atual = 1
      LEFT JOIN processo_cancel PX ON PX.codigo_processo_mte = M.codigo
           AND PX.fl_processo_atual = 1
      LEFT JOIN SERVICO S ON S.NU_SERVICO = M.NU_SERVICO
      LEFT JOIN TIPO_AUTORIZACAO T ON T.CO_TIPO_AUTORIZACAO = S.CO_TIPO_AUTORIZACAO
    WHERE E.NU_EMPRESA = $emprId
     $projeto 
    AND M.fl_visto_atual = 1";
    
    $res = mysql_query($query);
    print "<table cellpadding='0' cellspacing='0' class='grid'>
        <thead>
        <tr>
            <th>Empresa do último visto</th>
            <th>Embarcação</th>
            <th>Nome</th>
            <th>Nacionalidade</th>
            <th>Função</th>
            <th>CPF</th>
            <th>Número do passaporte</th>
            <th>Repartição consular</th>
            <th>Tipo do processo</th>
            <th>Validade do documento</th>
            <th>Validade do visto</th>
            <th>Tipo de autorização</th>     
        </tr>
        </thead>
        <tboby>";
        while($row=mysql_fetch_object($res)){
            print " <tr>
                        <td>".$row->NO_RAZAO_SOCIAL."</td>
                        <td>".$row->NO_EMBARCACAO_PROJETO."</td>
                        <td>".$row->NOME_COMPLETO."</td>
                        <td>".$row->NO_NACIONALIDADE."</td>
                        <td>".$row->NO_FUNCAO."</td>
                        <td>".$row->NU_CPF."</td>
                        <td>".$row->NU_PASSAPORTE."</td>
                        <td>".$row->NO_REPARTICAO_CONSULAR."</td>
                        <td>".$row->processo."</td>
                        <td>".$row->Validade."</td>
                        <td>".$row->prazo_solicitado."</td>
                        <td>".$row->NO_TIPO_AUTORIZACAO."</td>
                    </tr>";
        }
        print "</tboby>
        </table>";
    
}
echo Rodape($opcao);
?>
