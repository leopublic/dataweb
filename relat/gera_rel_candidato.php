<?php
include ("../LIB/conecta.php");
$sql = 'SELECT
          E.NO_RAZAO_SOCIAL,
          B.NO_EMBARCACAO_PROJETO,
          C.NO_PRIMEIRO_NOME,
          C.NO_NOME_MEIO,
          C.NO_ULTIMO_NOME,
          C.NO_MAE,
          C.NO_PAI,
          N.NO_PAIS,
          C.CO_SEXO,
          DATE_FORMAT(C.DT_NASCIMENTO,"%d/%m/%Y") as DT_NASCIMENTO,
          C.NO_LOCAL_NASCIMENTO,
          C.NU_PASSAPORTE,
          DATE_FORMAT(C.DT_EMISSAO_PASSAPORTE,"%d/%m/%Y") as DT_EMISSAO_PASSAPORTE,
          DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,"%d/%m/%Y") as DT_VALIDADE_PASSAPORTE,
          Ne.NO_PAIS as PAIS_EMISSOR_PASSAPORTE,
          PC.no_validade,
          PC.no_local_emissao,
          PC.dt_emissao_visto,
          TA.NO_REDUZIDO_TIPO_AUTORIZACAO as Visto_Class,
          DATE_FORMAT(PC.dt_entrada,"%d/%m/%Y") as dt_entrada,
          concat( " ", PM.nu_processo) nu_processo,
          PM.nu_oficio,
          DATE_FORMAT(PM.dt_deferimento,"%d/%m/%Y") as dt_deferimento,
          PM.prazo_solicitado,
          PR.nu_protocolo,
          DATE_FORMAT(PR.dt_requerimento,"%d/%m/%Y") as dt_requerimento,
          DATE_FORMAT(PR.dt_validade,"%d/%m/%Y") as dt_validade,
          DATE_FORMAT(PR.dt_prazo_estada,"%d/%m/%Y") as dt_prazo_estada,
          PP.nu_protocolo,
          DATE_FORMAT(PP.dt_requerimento,"%d/%m/%Y") as dt_requerimento,
          DATE_FORMAT(PP.dt_prazo_pret,"%d/%m/%Y") as dt_prazo_pret,
          DATE_FORMAT(PP.dt_validade,"%d/%m/%Y") as dt_validade
        FROM CANDIDATO C
          LEFT JOIN EMPRESA E ON C.NU_EMPRESA = E.NU_EMPRESA
          LEFT JOIN EMBARCACAO_PROJETO B ON B.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO
                                         AND B.NU_EMPRESA = E.NU_EMPRESA
          LEFT JOIN PAIS_NACIONALIDADE N ON N.CO_PAIS = C.CO_NACIONALIDADE
          LEFT JOIN PAIS_NACIONALIDADE Ne ON Ne.CO_PAIS = C.CO_PAIS_EMISSOR_PASSAPORTE
          LEFT JOIN processo_mte PM ON PM.CD_CANDIDATO = C.NU_CANDIDATO
                                    AND PM.codigo = C.codigo_processo_mte_atual
          LEFT JOIN processo_coleta PC ON PC.codigo_processo_mte = PM.codigo
                                       AND PC.CD_CANDIDATO = C.NU_CANDIDATO
                                       AND PC.fl_processo_atual = 1
          LEFT JOIN processo_regcie PR ON PR.codigo_processo_mte = PM.codigo
                                       AND PR.cd_candidato = C.NU_CANDIDATO
                                       AND PR.fl_processo_atual = 1
          LEFT JOIN processo_prorrog PP ON PP.codigo_processo_mte = PM.codigo
                                       AND PP.cd_candidato = C.NU_CANDIDATO
                                       AND PP.fl_processo_atual = 1
          LEFT JOIN SERVICO S ON S.NU_SERVICO = PM.NU_SERVICO
          LEFT JOIN TIPO_AUTORIZACAO TA ON TA.CO_TIPO_AUTORIZACAO = S.CO_TIPO_AUTORIZACAO
        WHERE C.NU_EMPRESA = '.$_POST['idEmpresa'].($_POST['idProjeto']!=''?' AND C.NU_EMBARCACAO_PROJETO = '.$_POST['idProjeto']:'')
        .' ORDER BY C.NO_PRIMEIRO_NOME ASC';
        
    $res = mysql_query($sql)or die(mysql_error());

$xls = '<table cellpadding="5" cellspacing="0" style="font-family:Arial;font-size: 11px; width: 4000px; border:1px solid #CCC;" >
            <tr>
                <th colspan="15" style="background-color: #CFC;">Identificação do Candidato</th>
                <th colspan="5"  style="background-color: #F6F;">Visa details</th>
                <th colspan="4"  style="background-color: #CFF;">Processo MTE</th>
                <th colspan="4"  style="background-color: #FC9;">Registro</th>
                <th colspan="4"  style="background-color: #FFC;">Processo MJ</th>
            </tr>
            <tr>
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Empresa</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Embarcação / Projeto</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Primeiro nome</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Nome do meio</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Último nome</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Nome da Mãe</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Nome do Pai</th>
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Nacionalidade</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Sexo</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Nascimento</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Naturalidade</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Nº Passaporte</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">Validade</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">País emissor</th> 
                <th style="border-bottom:1px solid #CCC; background-color: #CFC;">RNE</th>
                <th style="border-bottom:1px solid #CCC; background-color: #F6F;">Validade</th>
                <th style="border-bottom:1px solid #CCC; background-color: #F6F;">Local de Emissão</th>
                <th style="border-bottom:1px solid #CCC; background-color: #F6F;">Data de Emissão</th>
                <th style="border-bottom:1px solid #CCC; background-color: #F6F;">Classificação do Visto</th>
                <th style="border-bottom:1px solid #CCC; background-color: #F6F;">Data de Entrada</th>
                <th style="border-bottom:1px solid #CCC; background-color: #CFF;">Nº processo MTE</th>
                <th style="border-bottom:1px solid #CCC; background-color: #CFF;">Nº ofício MRE</th>
                <th style="border-bottom:1px solid #CCC; background-color: #CFF;">Data deferimento</th>
                <th style="border-bottom:1px solid #CCC; background-color: #CFF;">Prazo solicitado</th>
                <th style="border-bottom:1px solid #CCC; background-color: #FC9;">Nº protocolo CIE</th>
                <th style="border-bottom:1px solid #CCC; background-color: #FC9;">Data requerimento</th>
                <th style="border-bottom:1px solid #CCC; background-color: #FC9;">Validade protocolo</th>
                <th style="border-bottom:1px solid #CCC; background-color: #FC9;">Prazo de estada atual</th>
                <th style="border-bottom:1px solid #CCC; background-color: #FFC;">Nº protocolo MJ </th>
                <th style="border-bottom:1px solid #CCC; background-color: #FFC;">Data requerimento</th>
                <th style="border-bottom:1px solid #CCC; background-color: #FFC;">Prazo pretendido</th>
                <th style="border-bottom:1px solid #CCC; background-color: #FFC;">Data deferimento</th>
            </tr>';     
    while($row=mysql_fetch_array($res)){
        $xls .= "<tr style='background-color:#".($c%2==0?'FFF':'DDD')."'>
                    <td>{$row[0]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[1]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[2]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[3]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[4]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[5]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[6]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[7]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[8]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[9]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[10]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[11]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[12]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[13]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[14]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[15]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[16]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[17]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[18]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[19]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[20]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[21]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[22]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[23]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[24]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[25]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[26]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[27]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[28]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[29]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[30]}</td> 
                    <td style='border-left:1px solid #CCC'>{$row[31]}</td> 
                </tr>"; 
        $c++;
    }

$xls .= "</table>";

header('Content-type: application/ms-excel');
header("Content-Disposition: attachment; filename=Rel_".date("dmYhis").".xls");
header("Pragma: no-cache");
header("Expires: 0");

print $xls;
?>
