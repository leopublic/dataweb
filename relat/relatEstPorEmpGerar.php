<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/geral.php");
include ("../LIB/relat/libEstPorEmpGerar.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
$totCand = 0+$_POST['totCand'];
if($totCand>0) {
   for($x=0;$x<$totCand;$x++) {
      $aux1 = "cand$x";
      $aux2 = 0+$_POST[$aux1];
      if($aux2>0) {
          if(strlen($lstCand)>0) { $lstCand=$lstCand.","; }
          $lstCand = $lstCand.$aux2;
      }
   }
}

$relac = $_POST['relac'];
$relsin = $_POST['relsin'];
if($relac=='y') {
  $ehRelacao  = true;
  $mostracand = true;
  $mostrarelproj = false;
  $mostrarelemp = false;
  if($_POST['relac']=='y') { $relac=true; }
  if($_POST['relsin']=='y') { $relsin=true; }
  if($_POST['nome']=='y') { $nome=true; }
  if($_POST['nacional']=='y') { $nacional=true; }
  if($_POST['funcao']=='y') { $funcao=true; }
  if($_POST['passnum']=='y') { $passnum=true; }
  if($_POST['passdata']=='y') { $passdata=true; }
  if($_POST['rne']=='y') { $rne=true; }
  if($_POST['estada']=='y') { $estada=true; }
  if($_POST['cieprot']=='y') { $cieprot=true; }
  if($_POST['cieval']=='y') { $cieval=true; }
  if($_POST['prorrnum']=='y') { $prorrnum=true; }
  if($_POST['prorrval']=='y') { $prorrval=true; }
  if($_POST['prorrpre']=='y') { $prorrpre=true; }
}
if($relsin=='y') {
  $ehSintetico = true;
}

$empresas = ListaEmpresas($idEmpresa,$lstCand);
$totempresas = count($empresas);
if($totempresas==0) {
  if($lang=="E") {
    $msg = "Não há empresa cadastrada. <!-- $empresas -->";
  } else {
    $msg = "No client in Database. <!-- $empresas -->";
  }
}

if($lang=="E") {
  $titulo = "Foreigners per Company Report";
  $titulo2 = "Foreigners per Project Synthetic Report";
  $titEmpresa = "Company";
  $titProjeto = "Project/Vessel";
  $titRelatSint = "Synthetic Report"; 
  $titRelatDetalhe = "List Candidates, showing";
  $titNome = "Name";
  $titNacionalidade = "Nationality";
  $titFuncao = "Function";
  $titPassaport = "Passport Number";
  $titDatPassap = "Passport Date";
  $titRNE = "Registration Number";
  $titEstada = "Visa Validity";
  $titProtocoloCIE = "Registration ID Card Protocol";
  $titValidadeCIE = "ID Card Validity";
  $titNumProrr = "Extension Number";
  $titValProrr = "Validity of the Extension";
  $titPretProrr = "Expected Extension Validity Date";
  $titGerarRelatorio = "Generate Report";
  $titTodos = "All";
  $txtProjeto = "Project";
  $txtSemProjeto = "No projets for the client.";
  $txtSemCandidato = "No one in the project.";
  $txtTotEmpresa = "Total of the client";
  $txtTotProjeto = "Total of the project";
} else {
  $titulo = "Relatório de Estrageiros por Empresa";
  $titulo2 = "Relatório Sintético de Estrageiros por Empresa";
  $titEmpresa = "Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titRelatSint = "Relatório Sintético"; 
  $titRelatDetalhe = "Relaciona Candidatos, mostrando";
  $titNome = "Nome";
  $titNacionalidade = "Nacionalidade";
  $titFuncao = "Função";
  $titPassaport = "Num. Passaporte";
  $titDatPassap = "Validade Passaporte";
  $titRNE = "RNE";
  $titEstada = "Prazo Estada";
  $titProtocoloCIE = "Num Protocolo CIE";
  $titValidadeCIE = "Validade Proto CIE";
  $titNumProrr = "Num Protocolo Prorrog";
  $titValProrr = "Validade Proto Prorrog";
  $titPretProrr = "Prazo de Estada Pretendido";
  $titGerarRelatorio = "Gerar Relatório";
  $titTodos = "Todos";
  $txtProjeto = "Project";
  $txtSemProjeto = "Não há projeto nesta empresa.";
  $txtSemCandidato = "Não há candidato neste projeto.";
  $txtTotEmpresa = "Total da empresa";
  $txtTotProjeto = "Total do projeto";
}

?>
<html><head><title>Mundivisas</title>
<link href='/estilos.css' rel='stylesheet' type='text/css'></head>
<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>
<form name=relatorio method=post>


<?php
if(strlen($msg)>0) {
  echo "<br>";
  echo MostraTitulo(":: $titulo ::");
  if($lang=="E") {
    echo "<font color=red>An error happens when trying to get information necessary to generate the report.\n";
    echo "<br><br>Please, try again. If the problem persist, call the web administrator.<br>";
  } else {
    echo "<font color=red>Ocorreu um erro ao consultar relatório de prazos de Estrangeiros por Projeto/Embarcação.\n";
    echo "<br><br>Favor, tente novamente. Se o problema persistir, contate o administrador do site.<br>";
  }
  echo "\n\n<br><br>$msg";
  exit;
} else {
  $x=0;
  $qtd_cand_tot = 0;
  $qtd_estada_tot = 0;
  $qtd_prorr_tot = 0;
  $qtd_cie_tot = 0;
  $txtrelac = "<br>".MostraTitulo(":: $titulo ::");
  while($x<$totempresas) {
    $qtd_cand_emp = 0;
    $qtd_estada_emp = 0;
    $qtd_prorr_emp = 0;
    $qtd_cie_emp = 0;
    $projetos = null;
    $axempresa = $empresas[$x];
    $num_empresa = $axempresa->getNU_EMPRESA();
    $nome_empresa = $axempresa->getNO_EMPRESA();
    $candidatos = $axempresa->getCANDIDATOS();
    //$tamcand= strlen($candidatos);
    //$totcandidatos = count($candidatos);
    $txtrelac = $txtrelac."<br><table border=0 width=100?>\n";
    $txtrelac = $txtrelac."<tr class='textoazul' align=center bgcolor='#FFFFA0'><td><b>$titEmpresa : </b>$nome_empresa</td><tr>\n";
    //if( ($totcandidatos==0) || ($tamcand==0) ) {
    if(!is_array($candidatos) || count($candidatos)==0) {
    	$txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#eeeeee><td><b>$txtSemCandidato</td></tr>\n";
    } else {
      $totcandidatos = count($candidatos);
      $txtrelac = $txtrelac."<tr><td><table border=1 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
      $txtrelac = $txtrelac.MostraCabecaCand();
      $z=0;
      while($z<$totcandidatos) {
         $seq = $z+1;
         $axcand = $candidatos[$z];
         $DIAS_ESTADA = $axcand->getDIAS_ESTADA();
         $NU_PROTOCOLO_PRORROGACAO = $axcand->getNU_PROTOCOLO_PRORROGACAO();
         $DIAS_CIE = $axcand->getDIAS_CIE();

         $txtrelac = $txtrelac.MostraCandidato($axcand,$seq);

         if( ($DIAS_ESTADA < 180) && (strlen($DIAS_ESTADA)>0) ) { $qtd_estada_proj++; }
         if(strlen($NU_PROTOCOLO_PRORROGACAO)>0) { $qtd_prorr_proj++; }
         if( ($DIAS_CIE < 60) && (strlen($DIAS_CIE)>0) ) { $qtd_cie_proj++; }
         $qtd_cand_proj++;
         $z++;
         $qtd_cand_emp = $qtd_cand_emp+$qtd_cand_proj;
         $qtd_estada_emp = $qtd_estada_emp+$qtd_estada_proj;
         $qtd_prorr_emp = $qtd_prorr_emp+$qtd_prorr_proj;
         $qtd_cie_emp = $qtd_cie_emp+$qtd_cie_proj;
      }
    }

    $axempresa->setQTD_ESTR($qtd_cand_emp);
    $axempresa->setQTD_ESTA($qtd_estada_emp);
    $axempresa->setQTD_PRR($qtd_prorr_emp);
    $axempresa->setQTD_CIE($qtd_cie_emp);
    $empresas[$x]=$axempresa;

#    $txtrelac = $txtrelac."<br><br><table border=1 width=750 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
#    $txtrelac = $txtrelac."<tr bgcolor=#eeeeee><td colspan=4><b>$txtTotEmpresa : $nome_empresa</td></tr>\n";
#    $txtrelac = $txtrelac.MostraCabecaRel();
#    $txtrelac = $txtrelac.MostraRelatorio($axempresa);
#    $txtrelac = $txtrelac."</table>";

    $x++;
    $txtrelac = $txtrelac."</table>\n";
  }
  if($ehRelacao == true) {
    echo $txtrelac;
  }
  echo "<center>";
}

function MostraTitulo($texto) {
  $ret = "<center>\n";
  $ret = $ret."<table border=0>\n";
  $ret = $ret." <tr>\n";
  $ret = $ret."  <td class='textobasico' valign='top'>\n";
  $ret = $ret."   <p class='textoazul'><strong>$texto</strong></p>\n";				
  $ret = $ret."  </td>\n";
  $ret = $ret." </tr>\n";
  $ret = $ret."</table>\n";
  $ret = $ret."</center>\n";
  return $ret;
}

?>
