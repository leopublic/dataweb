<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
$idEmbarcacaoProjeto = "";
$fase = 0+$_POST['fase'];

if($ehCliente=="S") {
  $idEmpresa = cSESSAO::$mcd_empresa;
}

$empCmb="";
if(strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $embCmb = montaComboProjetos("",$idEmpresa," ORDER BY NO_EMBARCACAO_PROJETO");
} else {
  $empCmb = montaComboEmpresas($codigo,"ORDER BY NO_RAZAO_SOCIAL");
}

echo Topo($opcao);
echo Menu("REL");

if($lang=="E") {
  $titulo = "Management of People";
  $titEmpresa = "Company";
  $titProjeto = "Project/Vessel";
  $titOrdenar = "Classify"; 
  $titFiltro = "Filter";
   $titTodos = "All";
} else {
  $titulo = "Gerencia de pessoal por Empresa/Projeto/Embarcação";
  $titEmpresa = "Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titOrdenar = "Ordenar"; 
  $titFiltro = "Filtro";
   $titTodos = "Todos";
}

?>
<div class="conteudo">
	<div class="titulo"><div style="float:left"><?=$titulo?></div>&nbsp;</div>
	<div class="conteudoInterno">				

<form name=relatorio method=post action="gera_rel_candidato.php">
<input type=hidden name=fase value='1'>
  <table border=0 width="650" class='textoazul'>
   <tr><td width=200>&#160;</td><td width=250>&#160;</td><td width=200>&#160;</td></tr>
		
<?php
  if(strlen($idEmpresa)>0){
    echo "<input type=hidden name=idEmpresa value='$idEmpresa'>\n";
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "    <td colspan=2>&#160;$nomeEmpresa</td>\n";
    echo "</tr>\n";	
    echo "<tr><td class='textoazul'><td>&#160;</td></tr>\n";
    echo "<tr><td class='textoazul'>".$titProjeto.":</td>\n";
    echo "    <td colspan=2><select name=idProjeto><option value=''>$titTodos ... $embCmb</select></td>\n";
    echo "</tr>";
  } else {
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "     <td><select name=idEmpresa><option value=''>$titTodos ... $empCmb</select></td>\n";
   echo "     <td>\n";
    echo "<input type='button' class='textformtopo' style=\"$estilo\" value='Filtro Empresa' onclick=\"javascript:Empresa();\">";
    echo "</td></tr>";
  }
?>
</table>
<br />
<input type="submit" class="textformtopo" style="<?=$estilo?>" value="Gerar Relatório" />
</form>

<script language="JavaScript">
function Empresa() {
  document.relatorio.action='relatCandidato.php';
  document.relatorio.target="_top";
  document.relatorio.submit();
}
</script>

<?php
echo Rodape($opcao);
?>

