<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
if($ehCliente=="S") {
  $idEmpresa = cSESSAO::$mcd_empresa;
}

$idEmbarcacaoProjeto = "";
$fase = 0+$_POST['fase'];
if( ($_POST['relac']=='y') || ($fase==0) )  { $relac="checked"; }
if( ($_POST['relsin']=='y') || ($fase==0) ) { $relsin="checked"; }
if( ($_POST['nome']=='y') || ($fase==0) ) { $nome="checked"; }
if( ($_POST['nacional']=='y') || ($fase==0) ) { $nacional="checked"; }
if( ($_POST['funcao']=='y') || ($fase==0) ) { $funcao="checked"; }
if( ($_POST['passnum']=='y') || ($fase==0) ) { $passnum="checked"; }
if( ($_POST['passdata']=='y') || ($fase==0) ) { $passdata="checked"; }
if( ($_POST['rne']=='y') || ($fase==0) ) { $rne="checked"; }
if( ($_POST['estada']=='y') || ($fase==0) ) { $estada="checked"; }
if( ($_POST['dtEntrada']=='y') || ($fase==0) ) { $dtEntrada="checked"; }
if( ($_POST['dtRequerimento']=='y') || ($fase==0) ) { $dtRequerimento="checked"; }
if( ($_POST['cieprot']=='y') || ($fase==0) ) { $cieprot="checked"; }
if( ($_POST['cieval']=='y') || ($fase==0) ) { $cieval="checked"; }
if( ($_POST['dtDeferimento']=='y') || ($fase==0) ) { $dtDeferimento="checked"; }
if( ($_POST['prorrnum']=='y') || ($fase==0) ) { $prorrnum="checked"; }
if( ($_POST['prorrval']=='y') || ($fase==0) ) { $prorrval="checked"; }
if( ($_POST['prorrpre']=='y') || ($fase==0) ) { $prorrpre="checked"; }
if( ($_POST['mtenum']=='y') || ($fase==0) ) { $mtenum="checked"; }
if( ($_POST['mtedat']=='y') || ($fase==0) ) { $mtedat="checked"; }
if( ($_POST['consul']=='y') ) { $consul="checked"; }
if( ($_POST['remtot']=='y') ) { $remtot="checked"; }
if( ($_POST['rembr']=='y') ) { $rembr="checked"; }
if( ($_POST['prazosol']=='y') ) { $prazosol="checked"; }
if( ($_POST['tipovisto']=='y') ) { $tipovisto="checked"; }


$empCmb="";
if(strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $embCmb = montaComboProjetos("",$idEmpresa," ORDER BY NO_EMBARCACAO_PROJETO");
} else {
  $empCmb = montaComboEmpresas($codigo,"ORDER BY NO_RAZAO_SOCIAL");
}

echo Topo($opcao);
echo Menu("REL");

if($lang=="E") {
  $titulo = "Foreigners per Project";
  $titEmpresa = "Current Company";
  $titFiltroEmpresa = "Company Filter";
  $titProjeto = "Current Project/Vessel";
  $titRelatSint = "Synthetic Report"; 
  $titRelatDetalhe = "List Candidates, showing";
  $titNome = "Name";
  $titNacionalidade = "Nationality";
  $titFuncao = "Function";
  $titPassaport = "Passport Number";
  $titDatPassap = "Passport Date";
  $titRNE = "Registration Number";
  $titEstada = "Visa Validity";
  $titProtocoloCIE = "Registration ID Card Protocol";
  $titValidadeCIE = "ID Card Validity";
  $titNumProrr = "Extension Number";
  $titValProrr = "Validity of the Extension";
  $titPretProrr = "Expected Extension Validity Date";
  $titGerarRelatorio = "Generate Report";
  $titMTEnum = "MTE Process Number";
  $titMTEdata = "MTE Process Date";
  $titObservacao = "Observation";
  $titConsul = "Consul";
  $titRemMensal = "Salary";
  $titRemBrasil = "Salary in Brazil";
  $titPrazoSol = "Term Requested";
  $titTipoVisto = "Type of Visa";
  $titDtEntrada = "Entrance date";
  $titDtRequerimento = "Requirement date";
  $titDtDeferimento = "Deffered date";
  $titTodos = "All";
} else {
  $titulo = "Relatório de Estrageiros por Projeto/Embarcação";
  $titEmpresa = "Empresa atual";
  $titFiltroEmpresa = "Filtro Empresa";
  $titProjeto = "Projeto/Embarcação atual";
  $titRelatSint = "Relatório Sintético"; 
  $titRelatDetalhe = "Relaciona Candidatos, mostrando";
  $titNome = "Nome";
  $titNacionalidade = "Nacionalidade";
  $titFuncao = "Função";
  $titPassaport = "Num. Passaporte";
  $titDatPassap = "Data Val Passaporte";
  $titRNE = "RNE";
  $titEstada = "Prazo Estada";
  $titProtocoloCIE = "Num Protocolo CIE";
  $titValidadeCIE = "Data Validade CIE";
  $titNumProrr = "Num Protocolo Prorrog";
  $titValProrr = "Data Validade da Prorrog";
  $titPretProrr = "Data Pretendida Prorrog";
  $titGerarRelatorio = "Gerar Relatório";
  $titMTEnum = "Num do Processo MTE";
  $titMTEdata = "Data no MTE";
  $titConsul = "Consulado";
  $titRemMensal = "Salario Mensal";
  $titRemBrasil = "Salario Brasil";
  $titPrazoSol = "Prazo Solicitado";
  $titTipoVisto = "Tipo de Visto";
  $titObservacao = "Observa&ccedil;&atilde;o";
  $titDtEntrada = "Data de entrada";
  $titDtRequerimento = "Data de requerimento";
  $titDtDeferimento = "Data de deferimento";
  $titTodos = "Todos";
}

?>
<div class="conteudo">
	<div class="titulo"><div style="float:left"><?=$titulo?></div>&nbsp;</div>
	<div class="conteudoInterno">				

<form name=relatorio method=post>
<input type=hidden name=fase value='1'>
  	<table border=0 width="680" class='textoazul'>
			
<?php
  if(strlen($idEmpresa)>0){
    echo "<input type=hidden name=idEmpresa value='$idEmpresa'>\n";
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "    <td>&#160;$nomeEmpresa</td>\n";
    echo "    <td>&#160;</tr>\n";	
	echo "<tr><td class='textoazul'>".$titProjeto.":</td>\n";
	echo "    <td><select name=idProjeto><option value=''>$titTodos ... $embCmb</select></td>\n";
	echo "    <td>&#160;</tr></tr></table>";
  } else {
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
	echo "     <td><select name=idEmpresa><option value=''>$titTodos ... $empCmb</select></td>\n";
	echo "     <td>\n";
    echo "<input type='button' class='textformtopo' style=\"$estilo\" value='$titFiltroEmpresa' onclick=\"javascript:Empresa();\">";
    echo "</td></tr></table>";
  }
?>
<table width=680 border=0 cellspacing=0 cellpadding=0 class='textoazul'>
<tr><td><input type=checkbox name=relsin value='y' <?=$relsin?>> <?=$titRelatSint?></td></tr>
<tr><td><input type=checkbox name=relac value='y' <?=$relac?> onclick='javascript:detalhes();'> <?=$titRelatDetalhe?>:</td></tr>
</table>
<table width=680 border=0 cellspacing=0 cellpadding=0 class='textoazul' id=tb_detalhes>
 <tr>
  <td width=50>&#160;</td>
  <td width=150><input type=checkbox name=nome value='y' <?=$nome?>> <?=$titNome?></td>
  <td width=250><input type=checkbox name=nacional value='y' <?=$nacional?>> <?=$titNacionalidade?></td>
  <td width=200><input type=checkbox name=funcao value='y' <?=$funcao?>> <?=$titFuncao?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=passnum value='y' <?=$passnum?>> <?=$titPassaport?></td>
  <td><input type=checkbox name=passdata value='y' <?=$passdata?>> <?=$titDatPassap?></td>
  <td><input type=checkbox name=rne value='y' <?=$rne?>> <?=$titRNE?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=estada value='y' <?=$estada?>> <?=$titEstada?></td>
  <td><input type=checkbox name=dtDeferimento value='y' <?=$dtDeferimento?>> <?=$titDtDeferimento?></td>
  <td><input type=checkbox name=dtEntrada value='y' <?=$dtEntrada?>> <?=$titDtEntrada?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=cieprot value='y' <?=$cieprot?>> <?=$titProtocoloCIE?></td>
  <td><input type=checkbox name=cieval value='y'<?=$cieval?>> <?=$titValidadeCIE?></td>
  <td><input type=checkbox name=dtRequerimento value='y'<?=$dtRequerimento?>> <?=$titDtRequerimento?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=prorrnum value='y' <?=$prorrnum?>> <?=$titNumProrr?></td>
  <td><input type=checkbox name=prorrval value='y' <?=$prorrval?>> <?=$titValProrr?></td>
  <td><input type=checkbox name=prorrpre value='y' <?=$prorrpre?>> <?=$titPretProrr?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=mtenum value='y' <?=$mtenum?>> <?=$titMTEnum?></td>
  <td><input type=checkbox name=mtedat value='y' <?=$mtedat?>> <?=$titMTEdata?></td>
  <td><input type=checkbox name=consul value='y' <?=$consul?>> <?=$titConsul?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=remtot value='y' <?=$remtot?>> <?=$titRemMensal?></td>
  <td><input type=checkbox name=rembr value='y' <?=$rembr?>> <?=$titRemBrasil?></td>
  <td><input type=checkbox name=prazosol value='y' <?=$prazosol?>> <?=$titPrazoSol?></td>
 </tr>
 <tr>
  <td>&#160;</td>
  <td><input type=checkbox name=tipovisto value='y' <?=$tipovisto?>> <?=$titTipoVisto?></td>
  <td><input type=checkbox name=observacao value='y' <?=$observacao?>> <?=$titObservacao?></td>
  <td>&#160;</td>
 </tr>
</table>
<br>
<input type="button" class="textformtopo" style="<?=$estilo?>" value="<?=$titGerarRelatorio?>" onclick="javascript:Relatorio();">
</form>

<script language="JavaScript">
function Empresa() {
  document.relatorio.action='relatEstPorProj.php';
  document.relatorio.target="_top";
  document.relatorio.submit();
}
function Relatorio() {
  document.relatorio.action='relatEstPorProjGerar.php';
  document.relatorio.target='newwinproj<?=idEmpresa?>';
  document.relatorio.submit();
}
function detalhes() {
  if(document.relatorio.relac.checked == true) {
    tb_detalhes.style.display = 'block';
  } else {
    tb_detalhes.style.display = 'none';
  }
}
detalhes();
</script>

<?php
echo Rodape($opcao);
?>

