<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/geral.php");
include ("../LIB/cabecalho.php");
include ("../LIB/relat/libEstPorProjGerar.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
$idProjeto = $_POST['idProjeto'];

$relac = $_POST['relac'];
$relsin = $_POST['relsin'];
if($relac=='y') {
  $ehRelacao  = true;
  $mostracand = true;
  $mostrarelproj = false;
  $mostrarelemp = false;
  if($_POST['relac']=='y') { $relac=true; }
  if($_POST['relsin']=='y') { $relsin=true; }
  if($_POST['nome']=='y') { $nome=true; }
  if($_POST['nacional']=='y') { $nacional=true; }
  if($_POST['funcao']=='y') { $funcao=true; }
  if($_POST['passnum']=='y') { $passnum=true; }
  if($_POST['passdata']=='y') { $passdata=true; }
  if($_POST['rne']=='y') { $rne=true; }
  if($_POST['estada']=='y') { $estada=true; }
  if($_POST['cieprot']=='y') { $cieprot=true; }
  if($_POST['cieval']=='y') { $cieval=true; }
  if($_POST['prorrnum']=='y') { $prorrnum=true; }
  if($_POST['prorrval']=='y') { $prorrval=true; }
  if($_POST['prorrpre']=='y') { $prorrpre=true; }
  if($_POST['mtenum']=='y') { $mtenum=true; }
  if($_POST['mtedat']=='y') { $mtedat=true; }
  if($_POST['observacao']=='y') { $observacao=true; }
  if($_POST['consul']=='y') { $consul=true; }
  if($_POST['remtot']=='y') { $remtot=true; }
  if($_POST['rembr']=='y')  { $rembr=true; }
  if($_POST['prazosol']=='y') { $prazosol=true; }
  if($_POST['tipovisto']=='y') { $tipovisto=true; }
}
if($relsin=='y') {
  $ehSintetico = true;
}

$empresas = ListaEmpresas($idEmpresa,$idProjeto);
$totempresas = count($empresas);
if($totempresas==0) {
  if($lang=="E") {
    $msg = "Não há empresa cadastrada. <!-- $empresas -->";
  } else {
    $msg = "No client in Database. <!-- $empresas -->";
  }
}

if($lang=="E") {
  $titulo = "Foreigners per Project Report";
  $titulo2 = "Foreigners per Project Synthetic Report";
  $titEmpresa = "Company";
  $titProjeto = "Project/Vessel";
  $titRelatSint = "Synthetic Report"; 
  $titRelatDetalhe = "List Candidates, showing";
  $titNome = "Name";
  $titNacionalidade = "Nationality";
  $titFuncao = "Function";
  $titPassaport = "Passport Number";
  $titDatPassap = "Passport Date";
  $titRNE = "Registration Number";
  $titEstada = "Visa Validity";
  $titProtocoloCIE = "Registration ID Card Protocol";
  $titValidadeCIE = "ID Card Validity";
  $titNumProrr = "Extension Number";
  $titValProrr = "Validity of the Extension";
  $titPretProrr = "Expected Extension Validity Date";
  $titMTEnum = "MTE Process Number";
  $titMTEdata = "MTE Process Date";
  $titObservacao = "Observation";
  $titGerarRelatorio = "Generate Report";
  $titRemMensal = "Salary";
  $titRemBrasil = "Salary in Brazil";
  $titPrazoSol = "Term Requested";
  $titTodos = "All";
  $titConsul = "Consul";
  $titTipoVisto = "Type of Visa";
  $txtProjeto = "Project";
  $txtSemProjeto = "No projets for the client.";
  $txtSemCandidato = "No one in the project.";
  $txtTotEmpresa = "Total of the client";
  $txtTotProjeto = "Total of the project";
  $titDtEntrada = "Data de entrada";
  $titDtRequerimento = "Data de requerimento";
  $titDtDeferimento = "Data de deferimento";
} else {
  $titulo = "Relatório de Estrageiros por Projeto/Embarcação";
  $titulo2 = "Relatório Sintético de Estrageiros por Projeto/Embarcação";
  $titEmpresa = "Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titRelatSint = "Relatório Sintético"; 
  $titRelatDetalhe = "Relaciona Candidatos, mostrando";
  $titNome = "Nome";
  $titNacionalidade = "Nacionalidade";
  $titFuncao = "Função";
  $titPassaport = "Passaporte";
  $titDatPassap = "Val Passaporte";
  $titRNE = "RNE";
  $titEstada = "Prazo Estada";
  $titProtocoloCIE = "Protocolo CIE";
  $titValidadeCIE = "Val. Proto CIE";
  $titNumProrr = "Protocolo Prorrog";
  $titValProrr = "Val. Proto Prorrog";
  $titPretProrr = "Prazo Pret. Prorrog";
  $titMTEnum = "Processo MTE";
  $titMTEdata = "Data no MTE";
  $titObservacao = "Observa&ccedil;&atilde;o";
  $titGerarRelatorio = "Gerar Relatório";
  $titRemMensal = "Salario Mensal";
  $titRemBrasil = "Salario Brasil";
  $titPrazoSol = "Prazo Solicitado";
  $titTodos = "Todos";
  $titConsul = "Consulado";
  $titTipoVisto = "Tipo de Visto";
  $txtProjeto = "Project";
  $txtSemProjeto = "Não há projeto nesta empresa.";
  $txtSemCandidato = "Não há candidato neste projeto.";
  $txtTotEmpresa = "Total da empresa";
  $txtTotProjeto = "Total do projeto";
  $titDtEntrada = "Data de entrada";
  $titDtRequerimento = "Data de requerimento";
  $titDtDeferimento = "Data de deferimento";
}

//<html><head><title>Mundivisas</title>
//<link href='/estilos.css' rel='stylesheet' type='text/css'></head>
//<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>
//<form name=relatorio method=post>

print IniPag(false);

?>

<?php
if(strlen($msg)>0) {
  echo "<br>";
  echo MostraTitulo(":: $titulo ::");
  if($lang=="E") {
    echo "<font color=red>An error happens when trying to get information necessary to generate the report.\n";
    echo "<br><br>Please, try again. If the problem persist, call the web administrator.<br>";
  } else {
    echo "<font color=red>Ocorreu um erro ao consultar relatório de prazos de Estrangeiros por Projeto/Embarcação.\n";
    echo "<br><br>Favor, tente novamente. Se o problema persistir, contate o administrador do site.<br>";
  }
  echo "\n\n<br><br>$msg";
  exit;
} else {
  $x=0;
  $qtd_cand_tot = 0;
  $qtd_estada_tot = 0;
  $qtd_prorr_tot = 0;
  $qtd_cie_tot = 0;
  $txtrelac = "<br>".MostraTitulo(":: $titulo ::");
  while($x<$totempresas) {
    $qtd_cand_emp = 0;
    $qtd_estada_emp = 0;
    $qtd_prorr_emp = 0;
    $qtd_cie_emp = 0;
    $projetos = null;
    $axempresa = $empresas[$x];
    $num_empresa = $axempresa->getNU_EMPRESA();
    $nome_empresa = $axempresa->getNO_EMPRESA();
    $projetos = $axempresa->getPROJETOS();
    //$tamproj = strlen($projetos);
    //$totprojetos = count($projetos);
    $txtrelac = $txtrelac."<br><table border=0 width=100%>\n";
    $txtrelac = $txtrelac."<tr class='textoazul' align=center bgcolor='#FFFFA0'><td><b>$titEmpresa : </b>$nome_empresa</td><tr>\n";
    //if( ($totprojetos==0) || ($tamproj==0) ) {
    if(!is_array($projetos) || count($projetos)==0) {
      $txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#eeeeee><td><b>$txtProjeto : </b>$txtSemProjeto</td></tr>\n";
    } else {
      $y=0;
      $totprojetos = count($projetos);
      while($y<$totprojetos) {
        $totcandidatos = 0;
        $candidatos = null;
        $qtd_cand_proj = 0;
        $qtd_estada_proj = 0;
        $qtd_prorr_proj = 0;
        $qtd_cie_proj = 0;
        $z=0;
        $axprojeto = $projetos[$y];
        $num_projeto = $axprojeto->getNU_EMBARCACAO_PROJETO();
        $nome_projeto = $axprojeto->getNO_EMBARCACAO_PROJETO();
        $candidatos = $axprojeto->getCANDIDATOS();
        //$tamcand = strlen($candidatos);
        //$totcandidatos = count($candidatos);
        if($y>0) { $txtrelac = $txtrelac."<tr class='textoazulPeq'><td>&#160;</td></tr>\n"; }
        $txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#dddddd><td><b>$txtProjeto : </b>$nome_projeto</td><tr>\n";
        //if( ($totcandidatos==0) || ($tamcand==0) ) {
        if (!is_array($candidatos) || count($candidatos)==0){
          $txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#eeeeee><td align=center>$txtSemCandidato</td></tr>\n";
        } else {
          $totcandidatos = count($candidatos);
          if ( ($mostracand==true)  && ($totcandidatos>0) ) {
            $txtrelac = $txtrelac."<tr><td><table border=1 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
            $txtrelac = $txtrelac.MostraCabecaCand();
          }
          while($z<$totcandidatos) {
            $seq = $z+1;
            $axcand = $candidatos[$z];
            $DIAS_ESTADA = $axcand->getDIAS_ESTADA();
            $NU_PROTOCOLO_PRORROGACAO = $axcand->getNU_PROTOCOLO_PRORROGACAO();
            $DIAS_CIE = $axcand->getDIAS_CIE();
            if($mostracand==true) {
              $txtrelac = $txtrelac.MostraCandidato($axcand,$seq);
            }
            if( ($DIAS_ESTADA < 180) && (strlen($DIAS_ESTADA)>0) ) { $qtd_estada_proj++; }
            if(strlen($NU_PROTOCOLO_PRORROGACAO)>0) { $qtd_prorr_proj++; }
            if( ($DIAS_CIE < 60) && (strlen($DIAS_CIE)>0) ) { $qtd_cie_proj++; }
            $qtd_cand_proj++;
            $z++;
          }
          if ( ($mostracand==true)  && ($totcandidatos>0) ) {
            $txtrelac = $txtrelac."</table></td></tr>";
          }
        }
        $axprojeto->setQTD_ESTR($qtd_cand_proj);
        $axprojeto->setQTD_ESTA($qtd_estada_proj);
        $axprojeto->setQTD_PRR($qtd_prorr_proj);
        $axprojeto->setQTD_CIE($qtd_cie_proj);
        $projetos[$y]=$axprojeto;
        if($mostrarelproj == true) {
            $txtrelac = $txtrelac."<table border=1 width=750 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
            $txtrelac = $txtrelac."<tr bgcolor=#eeeeee><td colspan=4><b>$txtTotProjeto : $nome_projeto</td></tr>\n";
            $txtrelac = $txtrelac.MostraCabecaRel();
            $txtrelac = $txtrelac.MostraRelatorio($axprojeto);
            $txtrelac = $txtrelac."</table>";
        }
        $y++;
        $qtd_cand_emp = $qtd_cand_emp+$qtd_cand_proj;
        $qtd_estada_emp = $qtd_estada_emp+$qtd_estada_proj;
        $qtd_prorr_emp = $qtd_prorr_emp+$qtd_prorr_proj;
        $qtd_cie_emp = $qtd_cie_emp+$qtd_cie_proj;
      }
      $axempresa->setPROJETOS($projetos);
    }
    $axempresa->setQTD_ESTR($qtd_cand_emp);
    $axempresa->setQTD_ESTA($qtd_estada_emp);
    $axempresa->setQTD_PRR($qtd_prorr_emp);
    $axempresa->setQTD_CIE($qtd_cie_emp);
    $empresas[$x]=$axempresa;
    if($mostrarelemp == true) {
        $txtrelac = $txtrelac."<br><table border=1 width=750 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
        $txtrelac = $txtrelac."<tr bgcolor=#eeeeee><td colspan=4><b>$txtTotEmpresa : $nome_empresa</td></tr>\n";
        $txtrelac = $txtrelac.MostraCabecaRel();
        $txtrelac = $txtrelac.MostraRelatorio($axempresa);
        $txtrelac = $txtrelac."</table>";
    }
    $x++;
    $txtrelac = $txtrelac."</table>\n";
    $qtd_cand_tot = $qtd_cand_tot + $qtd_cand_emp;
    $qtd_estada_tot = $qtd_estada_tot + $qtd_estada_emp;
    $qtd_prorr_tot = $qtd_prorr_tot + $qtd_prorr_emp;
    $qtd_cie_tot = $qtd_cie_tot + $qtd_cie_emp;
  }
  if($ehRelacao == true) {
    echo $txtrelac;
  }
  echo "<center>";
  if($ehSintetico == true) {
    echo "<br>";
    echo MostraTitulo(":: $titulo2 ::");
    echo RelatorioCompleto($empresas);
  }
}

function RelatorioCompleto($empresas) {
  global $txtProjeto,$txtSemProjeto,$titEmpresa;
  $totempresas = count($empresas);
  $x=0;
  echo  "<br>";
  while($x<$totempresas) {
    $axempresa = $empresas[$x];
    $nome_empresa = $axempresa->getNO_EMPRESA();
    $projetos = $axempresa->getPROJETOS();
    //$tamproj = strlen($projetos);
    //$totprojetos = count($projetos);
    echo "<table border=0 width=750 cellpadding=0 cellspacing=0 class='textoazul'>\n";
    echo "<tr align=center bgcolor='#FFFFA0'><td><b>$titEmpresa : </b>$nome_empresa</td><tr>\n";
    echo "<tr><td><table border=1 width=750 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
    //if( ($totprojetos==0) || ($tamproj==0) ) {
    if (!is_array($projetos) || count($projetos)==0){	
      echo "<tr class='textoazul' bgcolor=#eeeeee><td colspan=5><b>$txtProjeto : $txtSemProjeto</td><tr>\n";
    } else {
      $totprojetos = count($projetos);
      echo MostraCabecaRelOpt();
      $y=0;
      
      while($y<$totprojetos) {
        $axprojeto = $projetos[$y];
        $nome_projeto = $axprojeto->getNO_EMBARCACAO_PROJETO();
        echo MostraRelatorioOpt($axprojeto,$nome_projeto);
        $y++;
      }
      echo MostraRelatorioOpt($axempresa,"Total");
    }
    echo "</td></tr></table><br>\n";
    $x++;
  }
}

function MostraTitulo($texto) {
  $ret = "<center>\n";
  $ret = $ret."<table border=0>\n";
  $ret = $ret." <tr>\n";
  $ret = $ret."  <td class='textobasico' valign='top'>\n";
  $ret = $ret."   <p class='textoazul'><strong>$texto</strong></p>\n";				
  $ret = $ret."  </td>\n";
  $ret = $ret." </tr>\n";
  $ret = $ret."</table>\n";
  $ret = $ret."</center>\n";
  return $ret;
}

?>
<script>
$(
		 function()
		 {
		  $(".textoazulPeq tr").hover(
		   function()
		   {
		    $(this).addClass("highlight");
		   },
		   function()
		   {
		    $(this).removeClass("highlight");
		   }
		  )
		 }
		)
</script>
