<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/geral.php");
include ("../LIB/relat/libGerenciaGerar.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
$idProjeto = $_POST['idProjeto'];

$ehRelacao = true;
$mostracand = true;
$mostrarelproj = false;
$mostrarelemp = false;
$nome=true;
$nacional=true; 
$passdata=true; 
$protval=true; 
$estada=true; 
$empresas = ListaEmpresas($idEmpresa,$idProjeto);
$totempresas = count($empresas);
if($totempresas==0) {
  $msg = "Não há empresa cadastrada. <!-- $empresas -->";
}

if($lang=="E") {
  $titulo = "Management of Period";
  $titEmpresa = "Company";
  $titProjeto = "Project/Vessel";
  $titOrdenar = "Classify"; 
  $titFiltro = "Filter";
  $titTodos = "All";
  $txtProjeto = "Project";
  $txtSemProjeto = "No projets for the client.";
  $txtSemCandidato = "No one in the project.";
} else {
  $titulo = "Gerencia de Prazo por Empresa/Projeto/Embarcação";
  $titEmpresa = "Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titOrdenar = "Ordenar"; 
  $titFiltro = "Filtro";
  $titTodos = "Todos";
  $txtProjeto = "Project";
  $txtSemProjeto = "Não há projeto nesta empresa.";
  $txtSemCandidato = "Não há candidato neste projeto.";
}

?>
<html><head><title>Mundivisas</title>
<link href='/estilos.css' rel='stylesheet' type='text/css'></head>
<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>
<form name=relatorio method=post>


<?php
if(strlen($msg)>0) {
  echo "<br>";
  echo MostraTitulo(":: $titulo ::");
  if($lang=="E") {
    echo "<font color=red>An error happens when trying to get information necessary to generate the report.\n";
    echo "<br><br>Please, try again. If the problem persist, call the web administrator.<br>";
  } else {
    echo "<font color=red>Ocorreu um erro ao consultar relatório de prazos de Estrangeiros por Projeto/Embarcação.\n";
    echo "<br><br>Favor, tente novamente. Se o problema persistir, contate o administrador do site.<br>";
  }
  echo "\n\n<br><br>$msg";
  exit;
} else {
  $x=0;
  $qtd_cand_tot = 0;
  $qtd_estada_tot = 0;
  $qtd_prorr_tot = 0;
  $qtd_cie_tot = 0;
  $qtd_pass_tot = 0;
  $txtrelac = "<br>".MostraTitulo(":: $titulo ::");
  while($x<$totempresas) {
    $qtd_cand_emp = 0;
    $qtd_estada_emp = 0;
    $qtd_prorr_emp = 0;
    $qtd_cie_emp = 0;
    $qtd_pass_emp = 0;
    $projetos = null;
    $axempresa = $empresas[$x];
    $num_empresa = $axempresa->getNU_EMPRESA();
    $nome_empresa = $axempresa->getNO_EMPRESA();
    $projetos = $axempresa->getPROJETOS();
    //$tamproj = strlen($projetos);
    //$totprojetos = count($projetos);
    $txtrelac = $txtrelac."<br><table border=0 width=100?>\n";
    $txtrelac = $txtrelac."<tr class='textoazul' align=center bgcolor='#FFFFA0'><td><b>$titEmpresa : </b>$nome_empresa</td><tr>\n";
    //if( ($totprojetos==0) || ($tamproj==0) ) {
    if (! is_array($projetos) || count($projetos)==0){	
      $txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#dddddd><td><b>Projeto: </b>$txtSemProjeto</td><tr>\n";
    } else {
      $totprojetos = count($projetos);
      $y=0;
      while($y<$totprojetos) {
        $totcandidatos = 0;
        $candidatos = null;
        $qtd_cand_proj = 0;
        $qtd_estada_proj = 0;
        $qtd_prorr_proj = 0;
        $qtd_cie_proj = 0;
        $qtd_pass_proj = 0;
        $z=0;
        $axprojeto = $projetos[$y];
        $num_projeto = $axprojeto->getNU_EMBARCACAO_PROJETO();
        $nome_projeto = $axprojeto->getNO_EMBARCACAO_PROJETO();
        $candidatos = $axprojeto->getCANDIDATOS();
        //$tamcand = strlen($candidatos);
        //$totcandidatos = count($candidatos);
        if($y>0) { $txtrelac = $txtrelac."<tr class='textoazulPeq'><td>&#160;</td></tr>\n"; }
        $txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#dddddd><td><b>Projeto: </b>$nome_projeto</td></tr>\n";
        //if( ($totcandidatos==0) || ($tamcand==0) ) {
        if (!is_array($candidatos) || count($candidatos)==0){
          $txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#eeeeee><td align=center>$txtSemCandidato<br/></td></tr>\n";
        } else {
          $totcandidatos = count($candidatos);
          if( ($mostracand==true) && ($totcandidatos>0) ) {
            $txtrelac = $txtrelac."<tr><td><table border=1 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
            $txtrelac = $txtrelac.MostraCabecaCand();
          }
          $seq = 0;
          while($z<$totcandidatos) {
            $axcand = $candidatos[$z];
            $soma = 0;
            $DIAS_ESTADA = $axcand->getDIAS_ESTADA();
            $DIAS_CIE = $axcand->getDIAS_CIE();
            $DIAS_PROR = $axcand->getDIAS_PROR();
            $DIAS_PASS = $axcand->getDIAS_PASS();
            if( ($DIAS_ESTADA < 180) && (strlen($DIAS_ESTADA)>0) ) { $qtd_estada_proj++; $soma = 1; }
            if( ($DIAS_CIE < 45) && (strlen($DIAS_CIE)>0) ) { $qtd_cie_proj++; $soma = 1; }
            if( ($DIAS_PROR < 45) && (strlen($DIAS_PROR)>0) ) { $qtd_prorr_proj++; $soma = 1; }
            if( ($DIAS_PASS < 180) && (strlen($DIAS_PASS)>0) ) { $qtd_pass_proj++; $soma = 1; }
#            if($soma == 1) { # Para motrar apenas o candidato que tem problema no prazo
            if($soma > -1) { # Para mostrar todos
               $qtd_cand_proj++;
               $seq++;
               if($mostracand==true) {
                  $txtrelac = $txtrelac.MostraCandidato($axcand,$seq);
               }
            }
            $z++;
          }
          if( ($mostracand==true)  && ($totcandidatos>0) ) {
            $txtrelac = $txtrelac."</table></td></tr>";
          }
        }
        $axprojeto->setQTD_PASS($qtd_pass_proj);
        $axprojeto->setQTD_ESTA($qtd_estada_proj);
        $axprojeto->setQTD_PRR($qtd_prorr_proj);
        $axprojeto->setQTD_CIE($qtd_cie_proj);
        $axprojeto->setQTD_CAND($qtd_cand_proj);
        $projetos[$y]=$axprojeto;
        if($mostrarelproj == true) {
# Mostra total por projeto ????
        }
        $y++;
        $qtd_cand_emp = $qtd_cand_emp+$qtd_cand_proj;
        $qtd_estada_emp = $qtd_estada_emp+$qtd_estada_proj;
        $qtd_prorr_emp = $qtd_prorr_emp+$qtd_prorr_proj;
        $qtd_cie_emp = $qtd_cie_emp+$qtd_cie_proj;
        $qtd_pass_emp = $qtd_pass_emp+$qtd_pass_proj;
      }
      $axempresa->setPROJETOS($projetos);
    }
    $axempresa->setQTD_CAND($qtd_cand_emp);
    $axempresa->setQTD_PASS($qtd_pass_emp);
    $axempresa->setQTD_ESTA($qtd_estada_emp);
    $axempresa->setQTD_PRR($qtd_prorr_emp);
    $axempresa->setQTD_CIE($qtd_cie_emp);
    $empresas[$x]=$axempresa;
    if($mostrarelemp == true) {
# Mostra total por empresa ????
    }
    $x++;
    $txtrelac = $txtrelac."</table>\n";
    $qtd_cand_tot = $qtd_cand_tot + $qtd_cand_emp;
    $qtd_pass_tot = $qtd_pass_tot + $qtd_pass_emp;
    $qtd_estada_tot = $qtd_estada_tot + $qtd_estada_emp;
    $qtd_prorr_tot = $qtd_prorr_tot + $qtd_prorr_emp;
    $qtd_cie_tot = $qtd_cie_tot + $qtd_cie_emp;
  }
  if($ehRelacao == true) {
    echo $txtrelac;
  }
  echo "<center>";
  if($ehSintetico == true) {
    echo "<br>";
    echo MostraTitulo(":: $titulo ::");
    echo RelatorioCompleto($empresas);
  }
  echo "<br><br>";
}

function RelatorioCompleto($empresas) {
   global $txtProjeto;
  $totempresas = count($empresas);
  $x=0;
  while($x<$totempresas) {
    $axempresa = $empresas[$x];
    $nome_empresa = $axempresa->getNO_EMPRESA();
    $projetos = $axempresa->getPROJETOS();
    //$tamproj = strlen($projetos);
    //$totprojetos = count($projetos);
    echo "<table border=0 width=750 cellpadding=0 cellspacing=0 class='textoazul'>\n";
    echo "<tr align=center height=30><td><b>Empresa: </b>$nome_empresa</td><tr>\n";
    echo "<tr><td><table border=1 width=750 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
    //if( ($totprojetos==0) || ($tamproj==0) ) {
    if (!is_array($projetos) || count($projetos)==0){	
    	echo "<tr class='textoazul' bgcolor=#eeeeee><td colspan=5><b>$txtProjeto: $txtSemProjeto</td><tr>\n";
    } else {
      echo MostraCabecaRelOpt();
      $totprojetos = count($projetos);
      $y=0;
      while($y<$totprojetos) {
        $axprojeto = $projetos[$y];
        $nome_projeto = $axprojeto->getNO_EMBARCACAO_PROJETO();
        echo MostraRelatorioOpt($axprojeto,$nome_projeto);
        $y++;
      }
      echo MostraRelatorioOpt($axempresa,"Total");
    }
    echo "</td></tr></table><br>\n";
    $x++;
  }
}

function MostraTitulo($texto) {
  $ret = "<center>\n";
  $ret = $ret."<table border=0>\n";
  $ret = $ret." <tr>\n";
  $ret = $ret."  <td class='textobasico' valign='top'>\n";
  $ret = $ret."   <p class='textoazul'><strong>$texto</strong></p>\n";				
  $ret = $ret."  </td>\n";
  $ret = $ret." </tr>\n";
  $ret = $ret."</table>\n";
  $ret = $ret."</center>\n";
  return $ret;
}

?>
