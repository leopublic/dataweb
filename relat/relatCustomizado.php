<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/relat/libEstPorEmpGerar.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
if($ehCliente=="S") {
  $idEmpresa = cSESSAO::$mcd_empresa;
}

$idEmbarcacaoProjeto = "";
$fase = 0+$_POST['fase'];
if( ($_POST['relac']=='y') || ($fase==0) )  { $relac="checked"; }
if( ($_POST['relsin']=='y') || ($fase==0) ) { $relsin="checked"; }
if( ($_POST['nome']=='y') || ($fase==0) ) { $nome="checked"; }
if( ($_POST['nacional']=='y') || ($fase==0) ) { $nacional="checked"; }
if( ($_POST['funcao']=='y') || ($fase==0) ) { $funcao="checked"; }
if( ($_POST['passnum']=='y') || ($fase==0) ) { $passnum="checked"; }
if( ($_POST['passdata']=='y') || ($fase==0) ) { $passdata="checked"; }
if( ($_POST['rne']=='y') || ($fase==0) ) { $rne="checked"; }
if( ($_POST['estada']=='y') || ($fase==0) ) { $estada="checked"; }
if( ($_POST['cieprot']=='y') || ($fase==0) ) { $cieprot="checked"; }
if( ($_POST['cieval']=='y') || ($fase==0) ) { $cieval="checked"; }
if( ($_POST['prorrnum']=='y') || ($fase==0) ) { $prorrnum="checked"; }
if( ($_POST['prorrval']=='y') || ($fase==0) ) { $prorrval="checked"; }
if( ($_POST['prorrpre']=='y') || ($fase==0) ) { $prorrpre="checked"; }

$empCmb="";
if(strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $lstCand = ListaCandidatos($idEmpresa,"");
  $totCand = count($lstCand);
  if($totCand>0) {
     for($x=0;$x<$totCand;$x++) {
         $aux1 = "cand$x";
         $aux2 = $lstCand[$x];
#print_r($aux2);
         $cod = $aux2->getNU_CANDIDATO();
         $nm = $aux2->getNOME_CANDIDATO();
         $empCmb = $empCmb."<br><input type=checkbox name='$aux1' value='$cod' > $nm";
     }
# print $empCmb;
  }
} else {
  $empCmb = montaComboEmpresas($codigo,"ORDER BY NO_RAZAO_SOCIAL");
}

echo Topo($opcao);
echo Menu("REL");

if($lang=="E") {
  $titulo = "Foreigners per Client";
  $titEmpresa = "Company";
  $titFiltroEmpresa = "Company Filter";
  $titProjeto = "Project/Vessel";
  $titCandidatos = "Candidates";
  $titRelatSint = "Synthetic Report"; 
  $titRelatDetalhe = "List Candidates, showing";
  $titNome = "Name";
  $titNacionalidade = "Nationality";
  $titFuncao = "Function";
  $titPassaport = "Passport Number";
  $titDatPassap = "Passport Date";
  $titRNE = "Registration Number";
  $titEstada = "Visa Validity";
  $titProtocoloCIE = "Registration ID Card Protocol";
  $titValidadeCIE = "ID Card Validity";
  $titNumProrr = "Extension Number";
  $titValProrr = "Validity of the Extension";
  $titPretProrr = "Expected Extension Validity Date";
  $titGerarRelatorio = "Generate Report";
  $titTodos = "All";
} else {
  $titulo = "Relatório de Estrageiros por Empresa";
  $titEmpresa = "Empresa";
  $titFiltroEmpresa = "Filtro Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titCandidatos = "Candidatos";
  $titRelatSint = "Relatório Sintético"; 
  $titRelatDetalhe = "Relaciona Candidatos, mostrando";
  $titNome = "Nome";
  $titNacionalidade = "Nacionalidade";
  $titFuncao = "Função";
  $titPassaport = "Num. Passaporte";
  $titDatPassap = "Data Val Passaporte";
  $titRNE = "RNE";
  $titEstada = "Prazo Estada";
  $titProtocoloCIE = "Num Protocolo CIE";
  $titValidadeCIE = "Data Validade CIE";
  $titNumProrr = "Num Protocolo Prorrog";
  $titValProrr = "Data Validade da Prorrog";
  $titPretProrr = "Data Pretendida Prorrog";
  $titGerarRelatorio = "Gerar Relatório";
  $titTodos = "Todos";
}

?>
<div class="conteudo">
	<div class="titulo"><div style="float:left">Relatório Customizado</div>&nbsp;</div>
	<div class="conteudoInterno">				

<form name=relatorio method=post>
<input type=hidden name=fase value='1'>
<input type=hidden name=totCand value='<?=$totCand?>'>
<div class="subTitulo">Relatório</div>
<div class="conteudoCercado" >
	Recuperar relatório <select>
		<option>(selecione)</option><option>Exemplo 1</option><option>Exemplo 2</option><option>Exemplo 3</option>
	</select> 
	<table width=680 border=0 cellspacing=0 cellpadding=0 class='textoazul' id=tb_detalhes>
	 <tr>
	  <td width=300>
	  	Campos disponíveis<br/>
	  	<select id="camposTodos" name="camposTodos" size="20" style="font-size:8pt;width:100%">
			<option value="Candidato.nome">Candidato::Nome</option>  	
			<option value="Candidato.sobreNome">Candidato::Sobrenome</option>  	
			<option value="Candidato.endereco">Candidato::Endereco</option>  	
			<option value="Candidato.profissao">Candidato::Profissão</option>  	
	  		<option value="Candidato.dataNascimento">Candidato::Data de nascimento</option>  	
			<option value="Candidato.dataValPassaporte">Candidato::Data de validade do passaporte</option>  	
	  	</select>
	  </td>
	  <td width=80 style="text-align:center"><a href="javascript:TrocaList('camposTodos', 'camposSelecionados');"><img src="../images/seta_direita.png" style="border:none;"/></a></td>
	  <td width=300>
	  	Campos selecionados<br/>
	  	<select id="camposSelecionados" name="camposSelecionados" size="20" style="font-size:8pt;width:100%">
			<option value="--">--</option>
	  	</select>
	  </td>
	 </tr>
	</table>
	<br/><input type="checkbox" name="salvar" value="1"/>Salvar esse relatório como :<input type="text" name="nomeRelatorio"/>
</div>
<div class="subTitulo">Filtros</div>
<div class="conteudoCercado" >
<?php
  if(strlen($idEmpresa)>0){
    echo "<input type=hidden name=idEmpresa value='$idEmpresa'>\n";
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "    <td>&#160;$nomeEmpresa</td>\n";
    echo "    <td>&#160;</tr>\n";	
    echo "<tr><td class='textoazul' valign='top'>".$titCandidatos.":</td>\n";
    echo "    <td>$empCmb</td>\n";
    echo "    <td>&#160;</tr></tr></table>";
  } else {
    echo "<tr><td class='textoazul'>".$titEmpresa.":</td>\n";
    echo "     <td><select name=idEmpresa><option value=''>$titTodos ... $empCmb</select></td>\n";
    echo "     <td>\n";
    echo "<input type='button' class='textformtopo' style=\"$estilo\" value='Filtro Empresa' onclick=\"javascript:Empresa();\">";
    echo "</td></tr></table>";
  }
?>
</div>

<br/><input type="button" class="textformtopo" style="<?=$estilo?>" value="<?=$titGerarRelatorio?>" onclick="javascript:Relatorio();">
</form>

<script language="JavaScript">
function Empresa() {
  document.relatorio.action='relatEstPorEmp.php';
  document.relatorio.target="_top";
  document.relatorio.submit();
}
function Relatorio() {
  document.relatorio.action='relatEstPorEmpGerar.php';
  document.relatorio.target='newwinproj<?=idEmpresa?>';
  document.relatorio.submit();
}
function detalhes() {
  if(document.relatorio.relac.checked == true) {
    tb_detalhes.style.display = 'block';
  } else {
    tb_detalhes.style.display = 'none';
  }
}
function TrocaList(nomeListOrigem,nomeListDestino)
{
	var ListOrigem = document.getElementById(nomeListOrigem);
	var ListDestino = document.getElementById(nomeListDestino);
	var moveu = false	
	var i;
	for (i = 0; i < ListOrigem.options.length ; i++)
	{
		if (ListOrigem.options[i].selected == true)
		{
			// Remove dummy
			if (ListDestino.options[0].text=='--')
			{	ListDestino.remove(0);	}

			moveu=true
			var Op = document.createElement("OPTION");
			Op.text = ListOrigem.options[i].text;
			Op.value = ListOrigem.options[i].value;
			ListDestino.options.add(Op);
			ListOrigem.remove(i);
			i--;
		}
	}
	if (moveu==false)
	{	alert('Selecione um campo para incluir no relatório.');	} 
}


detalhes();


</script>

<?php
echo Rodape($opcao);
?>
