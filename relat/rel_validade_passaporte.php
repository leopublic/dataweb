<?php
    require_once '../LIB/autenticacao.php';

    require_once '../LIB/ControleEventos.php';
    
    $app = new Generic;
    $app->filtro = $_POST['relat'];  // Filtro é os campos POST que será o filtro das colunas da Grid
    
    $html = new html;
    $html->cache_submit = true;      // Quando dar o Submit no formulários ele quarda o e exibe seu valor "post" novamente
  
    if($excel):   // se excel estiver ok
       $nome_arquivo = 'relatorio_('.date('d-m-Y').')_'.$app->str_basename($razaosocial).'-'.$app->str_basename($projeto); 
       header('Content-type: application/vnd.ms-excel'); 
       header('Content-type: application/force-download');
       header('Content-Disposition: attachment; filename='.$nome_arquivo.'.xls');
       header('Pragma: no-cache'); 
       
       echo '
       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
       <html><head>
       <style rel="stylesheet" type="text/css"/>'.file_get_contents('../estilos.css').'
           #TRelat th       {background:#003366;color:#fff}
           #TRelat          {font:10px verdana;color:#000}
           #TRelat          {border:1px solid #c0c0c0}
           #TRelat tbody td {border:1px solid #D8D8D8}
       </style></head><body>';
       
    else:  
?>
<script>
$(document).ready(function(){ 

   $('#NU_EMPRESA').change(function(){
      $.post('../ajax/AjaxUtils.php?name=embarcacao&id='+$(this).val(),function(d){
        $('#NU_EMBARCACAO').html(d);
      });
   }); 
   
   if($('.todos').attr('checked')=='true'){
        $('#TFiltro input').attr('checked', true);
   }
   
    $('.todos').on('click',function(event){
        if($('.todos').attr('checked')){
           $('#TFiltro input').attr('checked', true);
           $('#TRelat tr td').show();
           $('#TRelat tr th').show();
        }else{
           
           $('#TFiltro input').attr('checked', false);
           $('#TRelat td').hide();   
        }
    });
    $('#TFiltro input[type="checkbox"]').on('click',function(event){
       $('.todos').attr('checked', false); 
    })

   filtroDinamico('#TFrmRelat', '#TRelat');
   $('#getEXCEL').on('click',function(event){
        $('#excel').val('true');
        $('#TFrmRelat').submit();
        $('#excel').val('');
   })
});
</script>
<div class="titulo"> 
    Prazo de Validade de Passaportes
    <?if($isPost): ?><button id="getEXCEL" style="float: right;"> <img src="/imagens/icons/page_excel.png" /> Gerar Excel</button><?endif;?>
</div>
    <form method="post" id="TFrmRelat" name="TFrmRelat" class="boxer edicao" style="width: 98%;background-color: #fff;">
        <input type="hidden" id="excel" name="excel" value="" />
		<table class="filtro">
			  <col style="width:220px">
			  <col style="width:10px;">
			  <col style="width:320px;">
			  <col style="width:auto;">

			<tr><td>Empresa</td><td>:</td><td><?=$app->combo_empresa($app->p('NU_EMPRESA'))?></td>
				<td rowspan="3" style="padding-left:10px;"><button onclick="forms['TFrmRelat'].submit()"><img src="/imagens/icons/zoom.png" /> Gerar</button></td>
			</tr>
			<tr><td>Embarcação/Projeto</td><td>:</td><td><?=$combo_EmbarcacaoEmpresa?></td></tr>
			<tr><td>Com passaporte vencendo em menos de</td><td>:</td>
				<td><select id="DIAS_VENCIMENTO"  name="DIAS_VENCIMENTO" style="width:auto;">
						<option value=""    <?if($app->p('DIAS_VENCIMENTO')=='')   {print 'selected';}?>>(todos)
						<option value="30"  <?if($app->p('DIAS_VENCIMENTO')=='30') {print 'selected';}?>>30 dias
						<option value="60"  <?if($app->p('DIAS_VENCIMENTO')=='60') {print 'selected';}?>>60 dias
						<option value="90"  <?if($app->p('DIAS_VENCIMENTO')=='90') {print 'selected';}?>>90 dias
						<option value="180" <?if($app->p('DIAS_VENCIMENTO')=='180'){print 'selected';}?>>180 dias
					</select>
				</td></tr>
			</tr>
			<tr><td>Situação cadastral:</td><td>:</td>
				<td><select id="FL_INATIVO"  name="FL_INATIVO" style="width:auto;">
						<option value="1" <?if(!($app->p('FL_INATIVO')) || $app->p('FL_INATIVO')=='1'){print 'selected';}?>>somente ativos
						<option value="2" <?if($app->p('FL_INATIVO') && $app->p('FL_INATIVO')=='2'){print 'selected';}?>>somente inativos
						<option value="-1"  <?if($app->p('FL_INATIVO') && $app->p('FL_INATIVO')=='-1') {print 'selected';}?>>(todos)
					</select>
				</td></tr>
			</tr>
		</table>
		<br/>
        <?=$html->input('checkbox', array('name'=>'todos', 'class'=>'todos' ,'value'=>'on'/*, 'checked'=>'true'*/))?> 
        Campos à Exibir:
            <table id="TFiltro" class="colunas">
            <tr>
                <td width="150">Candidato:</td>
                <td>
                    <?=$html->input('checkbox', array('name'=>'relat[NOME_COMPLETO_LINK]', 'value'=>'Nome'))?> Nome
                    <?=$html->input('checkbox', array('name'=>'relat[NO_NACIONALIDADE]', 'value'=>'Nacionalidade'))?> Nacionalidade
                    <?=$html->input('checkbox', array('name'=>'relat[NU_CPF]', 'value'=>'CPF'))?> CPF
                    <?=$html->input('checkbox', array('name'=>'relat[NO_FUNCAO]', 'value'=>'Função'))?> Função 
                    <?=$html->input('checkbox', array('name'=>'relat[NU_PASSAPORTE]', 'value'=>'Nº Passaporte'))?> Nº Passaporte 
                    <?=$html->input('checkbox', array('name'=>'relat[DT_EMISSAO_PASSAPORTE]', 'value'=>'Emissão Passaporte'))?> Dt emissão Passaporte
                    <?=$html->input('checkbox', array('name'=>'relat[DT_VALIDADE_PASSAPORTE]', 'value'=>'Val. Passaporte'))?> Val. Passaporte
                    <?=$html->input('checkbox', array('name'=>'relat[DIAS_VENCIMENTO_PASSAPORTE]', 'value'=>'Venc. Pass. (dias)'))?> Dias para o venc. do passaporte
                    <?=$html->input('checkbox', array('name'=>'relat[TIPO_VISTO]', 'value'=>'Tipo de visto'))?> Tipo de visto
                    <?=$html->input('checkbox', array('name'=>'relat[QTD_PRORROG]', 'value'=>'Qtd prorrog.'))?> Qtd. prorrogações
                </td>
            </tr>
            <tr>
                <td>Autorização: </td>
                <td>
                    <?=$html->input('checkbox', array('name'=>'relat[NU_PROCESSO_MTE]', 'value'=>'Nº Processo MTE.'))?> Nº processo MTE
                    <?=$html->input('checkbox', array('name'=>'relat[DT_REQUERIMENTO]', 'value'=>'Data Req. MTE'))?> Data Req. MTE
                    <?=$html->input('checkbox', array('name'=>'relat[CONSULADO]', 'value'=>'Consulado'))?> Consulado
                    <?=$html->input('checkbox', array('name'=>'relat[PRAZO_SOLICITADO]', 'value'=>'Prazo Solicitado'))?> Prazo Solicitado 
                    <?=$html->input('checkbox', array('name'=>'relat[DT_DEFERIMENTO]', 'value'=>'Deferimento'))?> Deferimento
                    <?=$html->input('checkbox', array('name'=>'relat[DT_ENTRADA]', 'value'=>'Entrada'))?> Entrada
                </td>
            </tr>
            <tr>
                <td>Registro: </td>
                <td> 
                    <?=$html->input('checkbox', array('name'=>'relat[NU_PROTOCOLO_REG]', 'value'=>'Nº Protocolo reg.'))?> Nº protocolo reg.
                    <?=$html->input('checkbox', array('name'=>'relat[DT_REQUERIMENTO_CIE]', 'value'=>'Data Req Reg'))?> Data Req. Reg.
                    <?=$html->input('checkbox', array('name'=>'relat[DT_PRAZO_ESTADA]', 'value'=>'Prazo Estada'))?> Prazo Estada
                    <?=$html->input('checkbox', array('name'=>'relat[DIAS_VENCIMENTO]', 'value'=>'Vencimento (dias)'))?> Dias para o vencimento do visto
                </td>
            </tr>
            <tr>
               <td>Prorrogação:</td> 
               <td>
                    <?=$html->input('checkbox', array('name'=>'relat[NU_PROTOCOLO_PRO]', 'value'=>'Nº Protocolo Prorrog.'))?> Nº Protocolo Prorrog.
                    <?=$html->input('checkbox', array('name'=>'relat[DT_REQUERIMENTO_PRO]', 'value'=>'Req Prorrog.'))?> Data Req. Prorrog
                    <?=$html->input('checkbox', array('name'=>'relat[DT_PRAZO_PRET_PRO]', 'value'=>'Prazo Prorrog.'))?> Prazo Prorrog
                    <?=$html->input('checkbox', array('name'=>'relat[NU_SOLICITACAO_PRO]', 'value'=>'OS'))?> Número da OS
                    <?=$html->input('checkbox', array('name'=>'relat[NO_STATUS_SOL]', 'value'=>'Status OS'))?> Status da OS
               </td>
            </tr>
            </table>
    </form>
    <?endif; ?>
    
    
    
    <!-- inicio relatório -->
    <?
	if(isset ($candidatos) && is_array($candidatos)){
		// Primeiro lista todos que estão sem OS aberta, ou com OS aberta mas sem ter dado entrada
		if ($app->p('DIAS_VENCIMENTO') != '' ){
			$diasvencimento = ' (com passaporte vencendo em menos de '.$app->p('DIAS_VENCIMENTO').' dias)';
		}
		$Emb= '' ;
		$SEQ= 0;
		foreach($candidatos as $c){
			if ($Emb != $c['NO_EMBARCACAO_PROJETO']){
				if ($Emb != ''){
					print '</tbody></table>'."\n";
				}
				$Emb = $c['NO_EMBARCACAO_PROJETO'];
				print '
				<p align="center">:: Relatório de prazo de expiração de passaportes '.$diasvencimento.' ::</p>
				<div align="center" class="formtextoitalico"> Empresa: '.$razaosocial.' - '.$c['NO_EMBARCACAO_PROJETO'].'</div>
				<table id="TRelat" class="orderby destaque relatorio grade" cellspacing="1" style="text-align: left;">
				<thead>
					<tr><th width="30">Seq</th>'.$app->TitulosDinamicos().'</tr>
				</thead>
				<tbody>';
			}
			$SEQ++;
			// Testar condições de exibição...
			if ($c['DIAS_VENCIMENTO_PASSAPORTE'] < 180){
				$class = ' class="alerta"';
			}
			else{
				$class = '';
			}

			print '<tr'.$class.'><td>'.$SEQ.'</td>'.$app->ColunasDinamicas($c).'</tr>';
			
		}
		print '</tbody></table>'."\n";
	}
?>
    <!-- Fim relatorio -->

 </body>
</html>
