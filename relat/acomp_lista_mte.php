<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
$idEmbarcacaoProjeto = $_POST['idEmbarcacaoProjeto'];
if(strlen($idEmbarcacaoProjeto)>0) {
  $nomeProjeto = pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa);
}
$cd_solicitacao = $_POST['cd_solicitacao'];

$numcampos = 8;
$total = 0;
$textoprocura = "";
$msg = "";
if(strlen($idEmpresa)>0){
  $numcampos++;
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
} else {
  $nomeEmpresa = "Todas as empresas.";
}

if($lang=="E") {
  $titulo = "Process per Client";
  $titEmpresa = "Client";
  $titProjeto = "Project/Vessel";
  $titGerarRelatorio = "List Report";
  $titTodos = "All";
  $titAlerta = "Changed";
  $titFim = "Finally";
  $titCadastrado = "Cadastrados";
  $titHoje = "Today";
  $tit3dias = "Last 3 days";
  $titSemana = "One Week";
  $tit2Semana = "Two Week";
  $titmes = "One Month";
  $titProcura = "Search";
} else {
  $titulo = "Acompanhamento de Processos por Empresa";
  $titEmpresa = "Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titGerarRelatorio = "Listar Relatório";
  $titTodos = "Todos";
  $titAlerta = "Alterados";
  $titFim = "Finalizados";
  $titCadastrado = "Cadastrados";
  $titHoje = "Hoje";
  $tit3dias = "Últimos 3 dias";
  $titSemana = "1 Semana";
  $tit2Semana = "2 Semanas";
  $titmes = "1 Mes";
  $titProcura = "Pesquisa";
}

$status = $_POST['status'];
$prazo = $_POST['prazo'];

if($status=='A') {
   $textoprocura = $textoprocura.$titAlerta;
} elseif($status=='F') {
   $textoprocura = $textoprocura.$titFim;
} elseif($status=='C') {
   $textoprocura = $textoprocura.$titCadastrado;
} elseif($status=='AC') {
   $textoprocura = $textoprocura.$titAlerta."/".$titCadastrado;
} elseif($status=='') {
   $textoprocura = $textoprocura.$titTodos;
}

if($prazo==1) {
   $textoprocura = $textoprocura." - ".$titHoje;
} elseif($prazo==3) {
   $textoprocura = $textoprocura." - ".$tit3dias;
} elseif($prazo==7) {
   $textoprocura = $textoprocura." - ".$titSemana;
} elseif($prazo==15) {
   $textoprocura = $textoprocura." - ".$tit2Semana;
} elseif($prazo==30) {
   $textoprocura = $textoprocura." - ".$titmes;
} else {
   $textoprocura = $textoprocura." - ".$titTodos;
}

echo Topo($opcao);
echo Menu($opcao);

$sql = "SELECT a.codigo,a.empresa,a.candidato,a.solicitacao,a.tipo,a.processo,a.status,a.data_alteracao,a.data_pesquisa,";
$sql = $sql." b.NO_RAZAO_SOCIAL,c.NO_PRIMEIRO_NOME,c.NO_NOME_MEIO,c.NO_ULTIMO_NOME,d.NU_EMBARCACAO_PROJETO ";
$sql = $sql." FROM acompanhamento_processo a , EMPRESA b , CANDIDATO c , AUTORIZACAO_CANDIDATO d ";
$sql = $sql." WHERE b.NU_EMPRESA=a.empresa AND c.NU_CANDIDATO=a.candidato AND d.NU_CANDIDATO=a.candidato AND d.NU_SOLICITACAO=a.solicitacao ";
if(strlen($idEmpresa)>0){
  $sql = $sql."AND a.empresa=$idEmpresa ";
}
if(strlen($cd_solicitacao)>0) {
  $sql = $sql."AND a.solicitacao=$cd_solicitacao ";
}
if($prazo > 0) {
  if($status=="A") {
    $sql = $sql." AND a.data_alteracao >= DATE_SUB(CURDATE(), INTERVAL $prazo DAY)";
  } else {
    $sql = $sql." AND a.data_pesquisa >= DATE_SUB(CURDATE(), INTERVAL $prazo DAY)";
  }
} 
if(strlen($status)==1) {
  $sql = $sql." AND a.status='$status' ";
} else if(strlen($status)>1) {
  $status1 = substr($status,0,1);
  $status2 = substr($status,1,1);
  $sql = $sql." AND a.status in ('$status1','$status2') ";
}
if(strlen($idEmbarcacaoProjeto)>0) {
  $sql = $sql." AND d.NU_EMBARCACAO_PROJETO=$idEmbarcacaoProjeto ";
}
$sql = $sql." order by a.status , b.NO_RAZAO_SOCIAL , c.NO_PRIMEIRO_NOME , c.NO_NOME_MEIO , c.NO_ULTIMO_NOME ";
$lista = mysql_query($sql);
#$msgerr = "TESTE: ".mysql_error()."\n<!--\n sql = $sql \n-->\n";
if(mysql_errno()  != 0) {
  $msgerr = "Erro: ".mysql_error()."\n<!--\n sql = $sql \n-->\n";
  $total = 0;
} else {
  $total = mysql_num_rows($lista);
}

?>

<form name=relatorio method=post>
<input type=hidden name=fase value='1'>
<br><center>
<table border=0 width="680" class='textoazul'>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="2">
	<p align="center" class="textoazul"><strong>:: <?=$titulo?> ::</strong></p>				
  </td>
 </tr>
 <tr><td colspan="3"><br></td></tr>
			
<?php
echo "<input type=hidden name=idEmpresa value='$idEmpresa'>\n";
echo "<tr><td class='textoazul' width=100>".$titEmpresa.":</td>\n";
echo "    <td width=500>&#160;$nomeEmpresa</td>\n";
echo "</tr>";
if(strlen($idEmbarcacaoProjeto)>0) {
  echo "<tr><td class='textoazul' width=100>".$titProjeto.":</td>\n";
  echo "    <td width=500>&#160;$nomeProjeto</td>\n";
  echo "</tr>";
}
echo "<tr><td class='textoazul'>".$titProcura.":</td>\n";
echo "    <td>&#160;$textoprocura</td>\n";
echo "</tr>";
echo "</table>";
echo "<br>";
echo "<table width=800 border=1 cellspacing=0 cellpadding=0 class='textoazulpeq'>";
if(strlen($idEmbarcacaoProjeto)==0) { $numcampos++; }
if(strlen($msgerr)>0) {
   echo "<tr><td align=center colspan='$numcampos'><font color=red>$msgerr</td></tr>\n";
}
echo "<tr><td align=center colspan='$numcampos'>Total: $total</td></tr>\n";

if( (strlen($idEmpresa)>0) && ($total > 0) ){
  if(strlen($idEmbarcacaoProjeto)==0) {
    echo "<tr><td align=center><b>Processo</td><td align=center><b>Tipo</td><td align=center><b>Candidato</td><td align=center><b>Projeto/Embarcacao</td><td align=center><b>Solicitacao</td><td align=center><b>Status</td><td align=center><b>Pesquisa</td><td align=center><b>Alteracao</td></tr>\n";
  } else {
    echo "<tr><td align=center><b>Processo</td><td align=center><b>Tipo</td><td align=center><b>Candidato</td><td align=center><b>Solicitacao</td><td align=center><b>Status</td><td align=center><b>Pesquisa</td><td align=center><b>Alteracao</td></tr>\n";
  }
  while($rw = mysql_fetch_array($lista)) {
     $codigo = $rw['codigo'];
     $empresa = $rw['empresa'];
     $candidato = $rw['candidato'];
     $solicitacao = $rw['solicitacao'];
     $tipo  = $rw['tipo'];
     $processo  = $rw['processo'];
     $status  = $rw['status'];
     $data_alteracao = $rw['data_alteracao'];
     $data_pesquisa = $rw['data_pesquisa'];
     $dt_alt = dataMy2BR($data_alteracao);
     $dt_pes = dataMy2BR($data_pesquisa);
     $nomeempresa = $rw['NO_RAZAO_SOCIAL'];
     $nomecandidato = $rw['NO_PRIMEIRO_NOME']." ".$rw['NO_NOME_MEIO']." ".$rw['NO_ULTIMO_NOME'];
     if(strlen($idEmbarcacaoProjeto)==0) {
        $projeto = pegaNomeProjeto($rw['NU_EMBARCACAO_PROJETO'],$empresa);
     }
     if($status=='A') {
        $status = "<font color=red><b>A</b></font>";
        $dt_alt = "<font color=red><b>$dt_alt</b></font>";
     }
     $lnkAcomp = "<a href=\"javascript:veracomp($codigo,'$tipo','$processo');\" class='textoazulpeq'>$processo</a>";
     $lnkVisa = "<a href=\"javascript:visadetail($candidato,$empresa,'A');\" class='textoazulpeq'>$nomecandidato</a>";
     if(strlen($idEmbarcacaoProjeto)==0) {
       echo "<tr><td align=center>$lnkAcomp</td><td align=center>$tipo</td><td>$lnkVisa</td><td>$projeto</td><td align=center>$solicitacao</td>";
       echo "<td align=center>$status</td><td align=center>$dt_pes</td><td align=center>$dt_alt</td></tr>\n";
     } else {
       echo "<tr><td align=center>$lnkAcomp</td><td align=center>$tipo</td><td>$lnkVisa</td><td align=center>$solicitacao</td>";
       echo "<td align=center>$status</td><td align=center>$dt_pes</td><td align=center>$dt_alt</td></tr>\n";
     }
  }
} else {
  echo "<tr><td align=center><b>Processo</td><td align=center><b>Empresa</td><td align=center><b>Candidato</td><td align=center><b>Projeto/Embarcacao</td><td align=center><b>Solicitacao</td><td align=center><b>Status</td><td align=center><b>Pesquisa</td><td align=center><b>Alteracao</td></tr>\n";
  while($rw = mysql_fetch_array($lista)) {
     $codigo = $rw['codigo'];
     $empresa = $rw['empresa'];
     $candidato = $rw['candidato'];
     $solicitacao = $rw['solicitacao'];
     $tipo  = $rw['tipo'];
     $processo  = $rw['processo'];
     $status  = $rw['status'];
     $data_alteracao = $rw['data_alteracao'];
     $data_pesquisa = $rw['data_pesquisa'];
     $dt_alt = dataMy2BR($data_alteracao);
     $dt_pes = dataMy2BR($data_pesquisa);
     $projeto = pegaNomeProjeto($rw['NU_EMBARCACAO_PROJETO'],$empresa);
     $nomeempresa = $rw['NO_RAZAO_SOCIAL'];
     $nomecandidato = $rw['NO_PRIMEIRO_NOME']." ".$rw['NO_NOME_MEIO']." ".$rw['NO_ULTIMO_NOME'];
     if($status=='A') {
        $status = "<font color=red><b>A</b></font>";
        $dt_alt = "<font color=red><b>$dt_alt</b></font>";
     }
     $lnkAcomp = "<a href=\"javascript:veracomp($codigo,'$tipo','$processo');\" class='textoazulpeq'>$processo</a>";
     $lnkVisa = "<a href=\"javascript:visadetail($candidato,$empresa,'A');\" class='textoazulpeq'>$nomecandidato</a>";
     echo "<tr><td align=center>$lnkAcomp</td><td align=center>$nomeempresa</td><td>$lnkVisa</td><td>$projeto</td><td align=center>$solicitacao</td>";
     echo "<td align=center>$status</td><td align=center>$dt_pes</td><td align=center>$dt_alt</td></tr>\n";
  }
}

echo "</table>";

?>
<br>
</form>

<?php
echo Rodape($opcao);
?>

<script language="javascript">

function visadetail(codigo,emp,acao) {
//  var pagina = "/intranet/geral/novoVisaDetail.php?idCandidato=" + codigo + "&acao="+acao+"&idEmpresa=" + emp;
  var pagina = "/operador/operacao3.php?idCandidato=" + codigo + "&opvisa=PROCMTE&idEmpresa=" + emp;
//  var ret = AbrePagina(acao,pagina,emp,codigo);
  AbrePagina("Visa",pagina);
}

function veracomp(codigo,tipo,processo) {
  if(tipo=="MTE") {
    var pagina = "http://www.mte.gov.br/Empregador/TrabEstrang/Pesquisa/ConsultaProcesso.asp?Parametro="+processo+"&Opcao=1";
//    var pagina = "acomp_mostra_processo_mte.php?codigo=" + codigo;
//    var ret = AbrePagina("",pagina,"","");
      AbrePagina("Acomp",pagina);
  } else {
    alert("Tipo invalido");
  }
}

//function AbrePagina(tipo,pagina,emp,cand) {
function AbrePagina(nome,pagina) {
    var ret = '';
//    var MyArgs = new Array(emp,cand,tipo,"");
//    var WinSettings = "center:yes;resizable:yes;dialogHeight:580px;dialogWidth=700px";
//    var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
//    if (MyArgsRet != null) {
//        var msg = MyArgsRet[0].toString();
//        var erro = MyArgsRet[1].toString();
//    }
    var WinSettings = "status=0,menubar=0,resizeble=1,location=0,scrollbars=1,height=580,width=700";
    window.open(pagina,nome,WinSettings);
    return msg;
}

</script>
