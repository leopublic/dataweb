<?php
include ("../LIB/relat/libCrewList.php");
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$acao = $_POST["acao"];
$idEmpresa = 0 + $_POST['idEmpresa'];
$idEmbarcacaoProjeto = 0 + $_POST['idEmbarcacaoProjeto'];
$idCandidato = 0 + $_POST['idCandidato'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = 0 + $_GET['idEmpresa'];
}
if(strlen($idEmbarcacaoProjeto)==0) {
  $idEmbarcacaoProjeto = 0 + $_GET['idEmbarcacaoProjeto'];
}
if(strlen($idCandidato)==0) {
  $idCandidato = 0 + $_GET['idCandidato'];
}

if( ($acao=="PDF") && ($idEmpresa>0) ) {


} elseif($idEmpresa==0) {
  print geraTitulo();
  print geraEmpresa();
  print Rodape("");
} elseif($idEmbarcacaoProjeto==0) {
  print geraTitulo();
  print geraEmbarcacoes($idEmpresa);
  print Rodape("");
} else {
  print geraTitulo();
  print mostraEmbarcacoes($idEmpresa,$idEmbarcacaoProjeto);
  print crewList($idEmpresa,$idEmbarcacaoProjeto);
  print Rodape("");
}

exit;

function crewList($idEmpresa,$idEmbarcacao) {
  global $totallista,$NU_CANDIDATO,$NO_PRIMEIRO_NOME,$NO_NOME_MEIO,$NO_ULTIMO_NOME,$NU_PASSAPORTE,$DT_VALIDADE_PASSAPORTE;
  global $DT_ABERTURA_PROCESSO_MTE,$NU_PROCESSO_MTE,$NU_AUTORIZACAO_MTE,$DT_AUTORIZACAO_MTE,$DT_PRAZO_AUTORIZACAO_MTE;
  global $NU_PROTOCOLO_CIE,$DT_PROTOCOLO_CIE,$DT_VALIDADE_PROTOCOLO_CIE,$DT_PRAZO_ESTADA,$DT_VALIDADE_CIE,$DT_EMISSAO_CIE;
  global $NU_PROTOCOLO_PRORROGACAO,$DT_PROTOCOLO_PRORROGACAO,$DT_VALIDADE_PROTOCOLO_PROR,$DT_PRETENDIDA_PRORROGACAO;
  global $DT_CANCELAMENTO,$NU_PROCESSO_CANCELAMENTO,$DT_PROCESSO_CANCELAMENTO,$DT_PRAZO_ESTADA_SOLICITADO;
  $idCandidato = 0;
  listaCandidatos($idEmpresa,$idEmbarcacao,$idCandidato);
  $ret = "<br><br>";
  if($totallista==0) {
    $ret = $ret."<p align=center>Nenhum candidato na embarca&ccedil;&atilde;o</p>";
  } else {
    $ret = $ret."<table width=700 cellspacing=0 cellpadding=0 border=1 class=textoazulPeq >";
    $ret = $ret."<tr>";
    $ret = $ret."<td align=center nowrap>Nome do Candidato</td>";
    $ret = $ret."<td align=center nowrap>Passaporte</td>";
    $ret = $ret."<td align=center nowrap>Prazo de Estada</td>";
    $ret = $ret."<td align=center nowrap>Registro CIE</td>";
    $ret = $ret."<td align=center nowrap>Data Reg. CIE</td>";
    $ret = $ret."<td align=center nowrap>Num Protocolo Prorrog</td>";
    $ret = $ret."<td align=center nowrap>Data Pretendida</td>";
    $ret = $ret."<td align=center nowrap>Data do Registro</td>";
    $ret = $ret."</tr>";
    for($x=1;$x<=$totallista;$x++) {
       $nome = $NO_PRIMEIRO_NOME[$x]." ".$NO_NOME_MEIO[$x]." ".$NO_ULTIMO_NOME[$x];
       $ret = $ret."<tr>";
       $ret = $ret."<td nowrap>$nome</td>";
       $ret = $ret."<td align=center nowrap>&#160;".$NU_PASSAPORTE[$x]."</td>";
       $ret = $ret."<td align=center nowrap>&#160;".$DT_PRAZO_ESTADA[$x]."</td>";
       $ret = $ret."<td align=center nowrap>&#160;".$NU_PROTOCOLO_CIE[$x]."</td>";
       $ret = $ret."<td align=center nowrap>&#160;".$DT_PROTOCOLO_CIE[$x]."</td>";
       $ret = $ret."<td align=center nowrap>&#160;".$NU_PROTOCOLO_PRORROGACAO[$x]."</td>";
       $ret = $ret."<td align=center nowrap>&#160;".$DT_PRETENDIDA_PRORROGACAO[$x]."</td>";
       $ret = $ret."<td align=center nowrap>&#160;".$DT_PROTOCOLO_PRORROGACAO[$x]."</td>";
       $ret = $ret."</tr>";
    }
    $ret = $ret."</table>";
  }
  $ret = $ret."\n<!-- finalizando a lista -->\n";
  return $ret;
}

function geraTitulo() {
  $ret = Topo("CON");
  $ret = $ret.Menu("CON");
  $ret = $ret."<br><center><table border=0 width=500 class='textoazul'>";
  $ret = $ret."<tr><td class=textobasico align=center valign=top colspan=3>";
  $ret = $ret."<p align=center class=textoazul><strong>:: Crew List ::</strong></p>";
  $ret = $ret."</td></tr></table>";
  return $ret;
}

function geraEmpresa() {
  global $estilo;
  $empCmb = montaComboEmpresas("","ORDER BY NO_RAZAO_SOCIAL");
  $ret = "<form action='relatCrewList.php' method=post name=emp>";
  $ret = $ret."<br><center><table border=0 width=500 class='textoazul'>";
  $ret = $ret."<tr><td class='textoazul'>Empresa:</td>\n";
  $ret = $ret."     <td><select name=idEmpresa><option value=''>Todos ... $empCmb</select></td>\n";
  $ret = $ret."<td>&#160;<input type='submit' class='textformtopo' style=\"$estilo\" value='Filtro Empresa'>";
  $ret = $ret."</td></tr></table></form>";
  return $ret;
}

function geraEmbarcacoes($idEmpresa) {
  global $estilo;
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $embCmb = montaComboProjetos("",$idEmpresa," ORDER BY NO_EMBARCACAO_PROJETO");
  $ret = "<form action='relatCrewList.php' method=post name=emb>";
  $ret = $ret."<input type=hidden name=idEmpresa value='$idEmpresa'>";
  $ret = $ret."<br><center><table border=0 width=500 class='textoazul'>";
  $ret = $ret."<tr><td class='textoazul'>Empresa:</td>\n";
  $ret = $ret."    <td>&#160;$nomeEmpresa</td>\n";
  $ret = $ret."    <td>&#160;</tr>\n";	
  $ret = $ret."<tr><td class='textoazul'>Projeto/Embarcação:</td>\n";
  $ret = $ret."    <td><select name=idEmbarcacaoProjeto><option value=''>Todos ... $embCmb</select></td>\n";
  $ret = $ret."<td>&#160;<input type='submit' class='textformtopo' style=\"$estilo\" value='Filtro Embarcacao'>";
  $ret = $ret."</td></tr></table></form>";
  return $ret;
}

function mostraEmbarcacoes($idEmpresa,$idEmbarcacaoProjeto) {
  global $estilo;
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $nomeEmbarcacao = pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa);
  $ret = "<form action='relatCrewList.php' method=post name=emb>";
  $ret = $ret."<input type=hidden name=idEmpresa value='$idEmpresa'>";
  $ret = $ret."<br><center><table border=0 width=500 class='textoazul'>";
  $ret = $ret."<tr><td class='textoazul'>Empresa:</td>\n";
  $ret = $ret."    <td>&#160;$nomeEmpresa</td>\n";
  $ret = $ret."    <td>&#160;</tr>\n";	
  $ret = $ret."<tr><td class='textoazul'>Projeto/Embarcação:</td>\n";
  $ret = $ret."    <td>$nomeEmbarcacao</td>\n";
  $ret = $ret."<td>&#160</td></tr></table>";
  return $ret;
}


?>

