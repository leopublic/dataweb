<?php
$opcao = "CON";
include ("../LIB/autenticacao.php");
include ("../LIB/geral.php");
include ("../LIB/relat/libEstPorPerGerar.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
$idProjeto = $_POST['idProjeto'];
$perini = $_POST['perini'];
$perfim = $_POST['perfim'];

$relac = $_POST['relac'];
$relsin = $_POST['relsin'];
if($relac=='y') {
  $ehRelacao  = true;
  $mostracand = true;
  $mostrarelproj = false;
  $mostrarelemp = false;
  if($_POST['relac']=='y') { $relac=true; }
  if($_POST['relsin']=='y') { $relsin=true; }
  if($_POST['nome']=='y') { $nome=true; }
  if($_POST['nacional']=='y') { $nacional=true; }
  if($_POST['funcao']=='y') { $funcao=true; }
  if($_POST['passnum']=='y') { $passnum=true; }
  if($_POST['passdata']=='y') { $passdata=true; }
  if($_POST['rne']=='y') { $rne=true; }
  if($_POST['estada']=='y') { $estada=true; }
  if($_POST['cieprot']=='y') { $cieprot=true; }
  if($_POST['cieval']=='y') { $cieval=true; }
  if($_POST['prorrnum']=='y') { $prorrnum=true; }
  if($_POST['prorrval']=='y') { $prorrval=true; }
  if($_POST['prorrpre']=='y') { $prorrpre=true; }
}
if($relsin=='y') {
  $ehSintetico = true;
}

$perini = dataBR2My($perini);
$perfim = dataBR2My($perfim);
$empresas = ListaEmpresas($idEmpresa,$idProjeto,$perini,$perfim);
$totempresas = count($empresas);
if($totempresas==0) {
  $msg = "Não há empresa cadastrada. <!-- $empresas -->";
}
$perini = limpaValor($perini);
$perfim = limpaValor($perfim);

if($lang=="E") {
  $titulo = "Procces per Period";
  $titEmpresa = "Company";
  $titFiltroEmpresa = "Company Filter";
  $titProjeto = "Project/Vessel";
  $titRelatSint = "Synthetic Report"; 
  $titRelatDetalhe = "List Candidates, showing";
  $titNome = "Name";
  $titNacionalidade = "Nationality";
  $titFuncao = "Function";
  $titPassaport = "Passport Number";
  $titDatPassap = "Passport Date";
  $titRNE = "Registration Number";
  $titEstada = "Visa Validity";
  $titProtocoloCIE = "Registration ID Card Protocol";
  $titValidadeCIE = "ID Card Validity";
  $titNumProrr = "Extension Number";
  $titValProrr = "Validity of the Extension";
  $titPretProrr = "Expected Extension Validity Date";
  $titGerarRelatorio = "Generate Report";
  $titMTEnum = "MTE Process Number";
  $titMTEdata = "MTE Process Date";
  $titObservacao = "Observation";
  $titConsul = "Consul";
  $titRemMensal = "Salary";
  $titRemBrasil = "Salary in Brazil";
  $titPrazoSol = "Term Requested";
  $titTodos = "All";
  $titPeriodo = "Period";
  $titRelacionaCandidatos = "Relaciona Candidatos, mostrando";
  $titErr = "<font color=red>An error occur while consulting the report.\n<br><br>Please, try again. If the problem occur again, call the administrator.<br>";
  $titNoProject = "No projects for the client";
  $titTotalEmpresa = "Total for the Client";
  $titTotalProjeto = "Total for the Project";
  $titSemCandidatoProjeto = "Não há candidato neste projeto";
} else {
  $titulo = "Relatório de Processos por Período";
  $titEmpresa = "Empresa";
  $titFiltroEmpresa = "Filtro Empresa";
  $titProjeto = "Projeto/Embarcação";
  $titRelatSint = "Relatório Sintético"; 
  $titRelatDetalhe = "Relaciona Candidatos, mostrando";
  $titNome = "Nome";
  $titNacionalidade = "Nacionalidade";
  $titFuncao = "Função";
  $titPassaport = "Num. Passaporte";
  $titDatPassap = "Data Val Passaporte";
  $titRNE = "RNE";
  $titEstada = "Prazo Estada";
  $titProtocoloCIE = "Num Protocolo CIE";
  $titValidadeCIE = "Data Validade CIE";
  $titNumProrr = "Num Protocolo Prorrog";
  $titValProrr = "Data Validade da Prorrog";
  $titPretProrr = "Data Pretendida Prorrog";
  $titGerarRelatorio = "Gerar Relatório";
  $titMTEnum = "Numero do Processo MTE";
  $titMTEdata = "Data no MTE";
  $titConsul = "Consulado";
  $titRemMensal = "Salario Mensal";
  $titRemBrasil = "Salario Brasil";
  $titPrazoSol = "Prazo Solicitado";
  $titObservacao = "Observa&ccedil;&atilde;o";
  $titTodos = "Todos";
  $titPeriodo = "Período";
  $titRelacionaCandidatos = "Relaciona Candidatos, mostrando";
  $titErr = "<font color=red>Ocorreu um erro ao consultar Relatório de Estrangeiros.\n<br><br>Favor, tente novamente. Se o problema persistir, contate o administrador do site.<br>";
  $titNoProject = "Não há projeto nesta empresa";
  $titTotalEmpresa = "Total da empresa";
  $titTotalProjeto = "Total do Projeto";
  $titSemCandidatoProjeto = "Não há candidato neste projeto";
}

?>
<html><head><title>Mundivisas - Intranet</title>
<link href='/estilos.css' rel='stylesheet' type='text/css'></head>
<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>
<form name=relatorio method=post>


<?php
if(strlen($msg)>0) {
  echo "<br>";
  echo MostraTitulo(":: $titulo ::");
  echo $titErr;
  echo "\n\n<br><br>$msg";
  exit;
} else {
  $x=0;
  $QTD_ABERTO_t = 0;
  $QTD_MUNDIV_t = 0;
  $QTD_NOMTE_t = 0;
  $QTD_DIFMTE_t = 0;
  $QTD_REGPF_t = 0;
  $QTD_PRORROG_t = 0;
  $QTD_CANCEL_t = 0;
  $txtrelac = "<br>".MostraTitulo(":: $titulo ::");
  $txtrelac = $txtrelac."<br>".MostraPeriodo("$titPeriodo : ".$_POST['perini']." - ".$_POST['perfim']);
  while($x<$totempresas) {
    $QTD_ABERTO_e = 0;
    $QTD_MUNDIV_e = 0;
    $QTD_NOMTE_e = 0;
    $QTD_DIFMTE_e = 0;
    $QTD_REGPF_e = 0;
    $QTD_PRORROG_e = 0;
    $QTD_CANCEL_e = 0;
    $axempresa = $empresas[$x];
    $num_empresa = $axempresa->getNU_EMPRESA();
    $nome_empresa = $axempresa->getNO_EMPRESA();
    $projetos = $axempresa->getPROJETOS();
    //$tamproj = strlen($projetos);
    //$totprojetos = count($projetos);
    $txtrelac = $txtrelac."<br><table border=0 width=100?>\n";
    $txtrelac = $txtrelac."<tr class='textoazul' align=center bgcolor='#FFFFA0'><td><b>$titEmpresa : </b>$nome_empresa</td></tr>\n";
    //if( ($totprojetos==0) || ($tamproj==0) ) {
    if(!is_array($projetos) || count($projetos)==0) {
      $txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#eeeeee><td><b>Projeto: </b>$titNoProject .</td></tr>\n";
    } else {
      $totprojetos = count($projetos);
      $y=0;
      while($y<$totprojetos) {
        $QTD_ABERTO_p = 0;
        $QTD_MUNDIV_p = 0;
        $QTD_NOMTE_p = 0;
        $QTD_DIFMTE_p = 0;
        $QTD_REGPF_p = 0;
        $QTD_PRORROG_p = 0;
        $QTD_CANCEL_p = 0;
        $axprojeto = $projetos[$y];
        $num_projeto = $axprojeto->getNU_EMBARCACAO_PROJETO();
        $nome_projeto = $axprojeto->getNO_EMBARCACAO_PROJETO();
        $candidatos = $axprojeto->getCANDIDATOS();
        //$tamcand = strlen($candidatos);
        //$totcandidatos = count($candidatos);
        if($y>0) { $txtrelac = $txtrelac."<tr class='textoazulPeq'><td>&#160;</td></tr>\n"; }
        $txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#dddddd><td><b>$titProjeto : </b>$nome_projeto</td></tr>\n";
        //if( ($totcandidatos==0) || ($tamcand==0) ) {
        if(!is_array($candidatos) || count($candidatos)==0) {
          $txtrelac = $txtrelac."<tr class='textoazul' bgcolor=#eeeeee><td align=center>$titSemCandidatoProjeto .</td></tr>\n";
        } else {
          $z=0;
          $totcandidatos = count($candidatos);
          if( ($mostracand==true)  && ($totcandidatos>0) ) {
            $txtrelac = $txtrelac."<tr><td><table border=1 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
            $txtrelac = $txtrelac.MostraCabecaCand();
          }
          while($z<$totcandidatos) {
            $seq = $z+1;
            $axcand = $candidatos[$z];
            if($mostracand==true) {
              $txtrelac = $txtrelac.MostraCandidato($axcand,$seq);
            }
            $DT_ABERTURA = 0+limpaValor($axcand->getDT_ABERTURA());
            $DT_AUTORIZACAO = 0+limpaValor($axcand->getDT_AUTORIZACAO());
            $DT_PROTOCOLO = 0+limpaValor($axcand->getDT_PROTOCOLO());
            $DT_PRORROGACAO = 0+limpaValor($axcand->getDT_PRORROGACAO());
            $DT_CANCELA = 0+limpaValor($axcand->getDT_CANCELA());
            if( ($perini<=$DT_ABERTURA) && ($DT_ABERTURA<=$perfim) ) { $QTD_ABERTO_p++; }
            if( $DT_ABERTURA==0) { $QTD_MUNDIV_p++; }
            if( ($DT_AUTORIZACAO==0) && ($DT_ABERTURA>0) ) { $QTD_NOMTE_p++; }
            if( ($perini<=$DT_AUTORIZACAO) && ($DT_AUTORIZACAO<=$perfim) ) { $QTD_DIFMTE_p++; }
            if( ($perini<=$DT_PROTOCOLO) && ($DT_PROTOCOLO<=$perfim) ) { $QTD_REGPF_p++; }
            if( ($perini<=$DT_PRORROGACAO) && ($DT_PRORROGACAO<=$perfim) ) { $QTD_PRORROG_p++; }
            if( ($perini<=$DT_CANCELA) && ($DT_CANCELA<=$perfim) ) { $QTD_CANCEL_p++; }
            $z++;
          }
          if( ($mostracand==true)  && ($totcandidatos>0) ) {
            $txtrelac = $txtrelac."</table></td></tr>";
          }
        }
        $qtd_cand_proj = $z;
        $axprojeto->setQTD_ABERTO($QTD_ABERTO_p);
        $axprojeto->setQTD_MUNDIV($QTD_MUNDIV_p);
        $axprojeto->setQTD_NOMTE($QTD_NOMTE_p);
        $axprojeto->setQTD_DIFMTE($QTD_DIFMTE_p);
        $axprojeto->setQTD_REGPF($QTD_REGPF_p);
        $axprojeto->setQTD_PRORROG($QTD_PRORROG_p);
        $axprojeto->setQTD_CANCEL($QTD_CANCEL_p);
        $projetos[$y]=$axprojeto;
        if($mostrarelproj == true) {
            $txtrelac = $txtrelac."<table border=1 width=750 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
            $txtrelac = $txtrelac."<tr bgcolor=#eeeeee><td colspan=4><b>$titTotalProjeto : $nome_projeto</td></tr>\n";
            $txtrelac = $txtrelac.MostraCabecaRel();
            $txtrelac = $txtrelac.MostraRelatorio($axprojeto);
            $txtrelac = $txtrelac."</table>";
        }
        $y++;
        $QTD_ABERTO_e = $QTD_ABERTO_e+$QTD_ABERTO_p;
        $QTD_MUNDIV_e = $QTD_MUNDIV_e+$QTD_MUNDIV_p;
        $QTD_NOMTE_e = $QTD_NOMTE_e+$QTD_NOMTE_p;
        $QTD_DIFMTE_e = $QTD_DIFMTE_e+$QTD_DIFMTE_p;
        $QTD_REGPF_e = $QTD_REGPF_e+$QTD_REGPF_p;
        $QTD_PRORROG_e = $QTD_PRORROG_e+$QTD_PRORROG_p;
        $QTD_CANCEL_e = $QTD_CANCEL_e+$QTD_CANCEL_p;
      }
      $axempresa->setPROJETOS($projetos);
    }
    $axempresa->setQTD_ABERTO($QTD_ABERTO_e);
    $axempresa->setQTD_MUNDIV($QTD_MUNDIV_e);
    $axempresa->setQTD_NOMTE($QTD_NOMTE_e);
    $axempresa->setQTD_DIFMTE($QTD_DIFMTE_e);
    $axempresa->setQTD_REGPF($QTD_REGPF_e);
    $axempresa->setQTD_PRORROG($QTD_PRORROG_e);
    $axempresa->setQTD_CANCEL($QTD_CANCEL_e);
    $empresas[$x]=$axempresa;
    if($mostrarelemp == true) {
        $txtrelac = $txtrelac."<br><table border=1 width=750 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
        $txtrelac = $txtrelac."<tr bgcolor=#eeeeee><td colspan=4><b>$titTotalEmpresa : $nome_empresa</td></tr>\n";
        $txtrelac = $txtrelac.MostraCabecaRel();
        $txtrelac = $txtrelac.MostraRelatorio($axempresa);
        $txtrelac = $txtrelac."</table>";
    }
    $x++;
    $txtrelac = $txtrelac."</table>\n";
    $QTD_ABERTO_t = $QTD_ABERTO_t + $QTD_ABERTO_e;
    $QTD_MUNDIV_t = $QTD_MUNDIV_t + $QTD_MUNDIV_e;
    $QTD_NOMTE_t = $QTD_NOMTE_t + $QTD_NOMTE_e;
    $QTD_DIFMTE_t = $QTD_DIFMTE_t + $QTD_DIFMTE_e;
    $QTD_REGPF_t = $QTD_REGPF_t + $QTD_REGPF_e;
    $QTD_PRORROG_t = $QTD_PRORROG_t + $QTD_PRORROG_e;
    $QTD_CANCEL_t = $QTD_CANCEL_t + $QTD_CANCEL_e;
  }
  if($ehRelacao == true) {
    echo $txtrelac;
  }
  echo "<center>";
  if($ehSintetico == true) {
    echo "<br>";
    echo MostraTitulo(":: $titulo ::");
    echo RelatorioCompleto($empresas);
  }
}

function RelatorioCompleto($empresas) {
  global $titEmpresa,$titProjeto,$titNoProject;
  $totempresas = count($empresas);
  $x=0;
  echo "<br>";
  while($x<$totempresas) {
    $axempresa = $empresas[$x];
    $nome_empresa = $axempresa->getNO_EMPRESA();
    $projetos = $axempresa->getPROJETOS();
    //$tamproj = strlen($projetos);
    //$totprojetos = count($projetos);
    echo "<table border=0 width=750 cellpadding=0 cellspacing=0 class='textoazul'>\n";
    echo "<tr align=center bgcolor='#FFFFA0'><td><b>$titEmpresa : </b>$nome_empresa</td><tr>\n";
    echo "<tr><td><table border=1 width=750 class='textoazulPeq' cellpadding=0 cellspacing=0>\n";
    //if( ($totprojetos==0) || ($tamproj==0) ) {
    if(!is_array($projetos) || count($projetos)==0) {
      echo "<tr class='textoazul' bgcolor=#eeeeee><td colspan=5><b>$titProjeto : $titNoProject .</td><tr>\n";
    } else {
      $totprojetos = count($projetos);
      echo MostraCabecaRelOpt();
      $y=0;
      while($y<$totprojetos) {
        $axprojeto = $projetos[$y];
        $nome_projeto = $axprojeto->getNO_EMBARCACAO_PROJETO();
        echo MostraRelatorioOpt($axprojeto,$nome_projeto);
        $y++;
      }
      echo MostraRelatorioOpt($axempresa,"Total");
    }
    echo "</td></tr></table><br>\n";
    $x++;
  }
}

function MostraTitulo($texto) {
  $ret = "<center>\n";
  $ret = $ret."<table border=0>\n";
  $ret = $ret." <tr>\n";
  $ret = $ret."  <td class='textobasico' valign='top'>\n";
  $ret = $ret."   <p class='textoazul'><strong>$texto</strong></p>\n";				
  $ret = $ret."  </td>\n";
  $ret = $ret." </tr>\n";
  $ret = $ret."</table>\n";
  $ret = $ret."</center>\n";
  return $ret;
}

function MostraPeriodo($texto) {
  $ret = "<center>\n";
  $ret = $ret."<table border=0>\n";
  $ret = $ret." <tr>\n";
  $ret = $ret."  <td class='textobasico' valign='top'>\n";
  $ret = $ret."   <p class='textoazulpeq'><strong>$texto</strong></p>\n";
  $ret = $ret."  </td>\n";
  $ret = $ret." </tr>\n";
  $ret = $ret."</table>\n";
  $ret = $ret."</center>\n";
  return $ret;
}
?>
