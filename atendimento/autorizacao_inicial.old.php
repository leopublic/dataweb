<?php
$opcao = "EMP";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
$idEmbarcacaoProjeto = $_POST['idEmbarcacaoProjeto'];
$nmEmbarcacaoProjeto = pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa);
$idCandidato = $_POST['idCandidato'];
$nomeCandidato = $_POST['nomeCandidato'];
$cdSolicitacao = $_POST['cdSolicitacao'];
$idStatusAcomp = $_POST['idStatusAcomp'];
$cdPaisResidencia = $_POST['cdPaisResidencia'];
$acao = $_POST['acao'];
$totcand = 0 + $_POST['totcand'];

if($ehCliente=="S") {
  $idEmpresa = $MyAuth->GETcd_empresa();
}

if($acao=="A") {
  $adm = $usulogado;
  $per = $usuperfil;
  $totcand = 0+$_POST['totcand'];
  if($totcand>0) {
    for($z=1;$z<=$totcand;$z++) {
      $sql = "";
      $seq = 0+$_POST["seq$z"];
      $chk = $_POST["chk$z"];
      if( ($seq>0) && ($chk=="y") ) {
        if($per==5) {
          $sql = "UPDATE solicita_visto_acomp_cand SET cd_tecnico=$adm,dt_tecnico=now() WHERE cd_sequencial=$seq";
        } else if($per==4) {
          $sql = "UPDATE solicita_visto_acomp_cand SET cd_coordenador=$adm,dt_coordenador=now() WHERE cd_sequencial=$seq";
        } else if($ehCliente=="S") {
          $sql = "UPDATE solicita_visto_acomp_cand SET cd_empresa=$adm,dt_empresa=now() WHERE cd_sequencial=$seq";
        } else if($per < 4) {
# fazer update para os 3
        }
        if(strlen($sql)>0) {
          mysql_query($sql);
          if(mysql_errno()!=0) {
            print "<br>ERRO:".mysql_error();
          }
        }
      }
    }
  }
}

echo Topo("");
echo Menu("ATE");

print "  <br><center><table border=0 width=800  class='textoazul'>\n";
print "  <tr><td align=center valign=top colspan=4 class='textoazul'>\n";
print "   <strong>:: Controle de Autoriza&ccedil;&atilde;es ::</strong></td></tr>\n";
print "  <tr><td colspan=4>&#160;</td></tr>\n";

$query = MontaQueryAutorizacao();  # -------- Monta Query

if(strlen($msg)>0) {
  print "  <tr><td colspan=4>$msg&#160;</td></tr>\n";
  print "  <tr><td colspan=4>&#160;</td></tr>\n";
}

if( strlen($idEmpresa)>0) {

  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);

  print MontaTelaPesquisaAutorizacao();  # -------- Monta Tela de Pesquisa

  print "  <form action='autorizacao_inicial.php' name=lista method=post>\n";
  print "  <input type=hidden name='idEmpresa' value='$idEmpresa'>\n";
  print "  <input type=hidden name='idEmbarcacaoProjeto' value='$idEmbarcacaoProjeto'>\n";
  print "  <input type=hidden name='nomeCandidato' value='$nomeCandidato'>\n";
  print "  <input type=hidden name='cdSolicitacao' value='$cdSolicitacao'>\n";
  print "  <input type=hidden name='cdPaisResidencia' value='$cdPaisResidencia'>\n";
  print "  <input type=hidden name='acao' value='A'>\n";
  print "  <br>\n";
  print "  <table border=1 width=790 class='textoazulpeq' cellspacing=0>\n";
  print "   <tr align='center' height=20>\n";
  print "    <td bgcolor=#DADADA ><b>Candidato</td>\n";
  print "    <td bgcolor=#DADADA width=60><b>Solicit.</td>\n";
  print "    <td bgcolor=#DADADA ><b>Projeto / Embarca&ccedil;&atilde;o</td>\n";
  print "    <td bgcolor=#DADADA ><b>Pa&iacute;s de Residencia</td>\n";
  print "    <td bgcolor=#DADADA ><b>Situa&ccedil;&atilde;o</td>\n";
  print "    <td bgcolor=#DADADA width=40><b>A&ccedil;&atilde;o</td>\n";
  print "  </tr>\n";
  
  $linhas = mysql_query($query);
  if(mysql_errno()>0) {
     echo "\n<!-- \nSQL=$sql\n -->\n".mysql_error();
  }

  $x=0;
  $y=0;
  $lstSit = ListaSituacoesAutorizacao();
  while($rw1=mysql_fetch_array($linhas)) {
    $x++;
    $nomeProjetoEmbarcacao = "";
    $idCandidato = $rw1['NU_CANDIDATO'];
    $primeiroNomeCandidato = $rw1['NO_PRIMEIRO_NOME'];
    $meioNomeCandidato = $rw1['NO_NOME_MEIO'];
    $ultimoNomeCandidato = $rw1['NO_ULTIMO_NOME'];
    $numProjeto = $rw1['NU_EMBARCACAO_PROJETO'];
    $cdPaisRes = $rw1['CO_PAIS_RESIDENCIA'];
    $cdSol = $rw1['NU_SOLICITACAO'];
    $cdSeq = $rw1['cd_sequencial'];
    $cd_tecnico = $rw1['cd_tecnico'];
    $dt_tecnico = $rw1['dt_tecnico'];
    $cd_coordenador = $rw1['cd_coordenador'];
    $dt_coordenador = $rw1['dt_coordenador'];
    $cd_empresa = $rw1['cd_empresa'];
    $dt_empresa = $rw1['dt_empresa'];
    $nomeCopleto = "$primeiroNomeCandidato $meioNomeCandidato $ultimoNomeCandidato";

    if(strlen($numProjeto)>0) { $nomeProjetoEmbarcacao=pegaNomeProjeto($numProjeto,$idEmpresa); }

    if(strlen($cdPaisRes)>0) { $nmPaisResidencia = ""; }

    $solicit = $cdSol;
    if($cdSol>0) {
      $solicit = "<a href='javascript:mostraSolicitacao($cdSol)' class='textoazulpeq'>$cdSol</a>";
    }
    if(strlen($dt_tecnico)==0) { 
       $idSit = 1; 
    } else if(strlen($dt_coordenador)==0) { 
       $idSit = 2; 
    } else if(strlen($dt_empresa)==0) { 
       $idSit = 3; 
    } else {
       $idSit = 4; 
    }
    $nmSituacao = $lstSit[$idSit];

    if( ($idSit==1) && ( ($usuperfil==5) || ($usuperfil<4) ) ) {
       $y++;
       $mostra = "<input type=checkbox name='chk$x' value='y'>";
    } else if( ($idSit==2) && ($usuperfil<=4) ) {
       $y++;
       $mostra = "<input type=checkbox name='chk$x' value='y'>";
    } else if( ($idSit==3) && ( ($ehCliente=="S") || ($usuperfil<4) ) ) {
       $y++;
       $mostra = "<input type=checkbox name='chk$x' value='y'>";
    } else {
       $mostra = "-";
    }
    
    echo "<input type=hidden name='cand$x' value='$idCandidato'>";	
    echo "<input type=hidden name='sol$x' value='$cdSol'>";	
    echo "<input type=hidden name='seq$x' value='$cdSeq'>";	
    echo "<tr height=20>\n";
    echo "<td>&#160;<a href='javascript:mostra($idCandidato);' class='textoazulpeq'>$nomeCopleto</a></td>";
    echo "<td align=center>$solicit</td>\n";
    echo "<td>&#160;$nomeProjetoEmbarcacao</td>\n";
    echo "<td>&#160;$nmPaisResidencia</td>\n";
    echo "<td>&#160;$nmSituacao</td>\n";
    echo "<td align=center>$mostra</td>";
    echo "</tr>\n";
  }
  echo "<input type=hidden name=totcand value='$x'>";
  print "</table>\n";




  if($y>0) {
    print "<p align=center><input type=submit value='Autorizar' style=\"$estilo\"></p>\n";
  }

  print "</form>";

} else {

  $cmbEmpresas = montaComboEmpresas($codigo,"ORDER BY NO_RAZAO_SOCIAL");
  print "<form action='autorizacao_inicial.php' method=post name=emp>\n";
  print " <tr class='textoazul'>\n";
  print "  <td NOWRAP>Empresa:</td>\n";
  print "  <td><b><select name=idEmpresa class='textoazul'><option value=''>Selecione ... $cmbEmpresas</select></td>\n";
  print "  <td align=center><input type='button' class='textoazul' value='Escolher Empresa' onclick='javascript:escolheempresa();'></td>\n";
  print " </tr></form></table>\n";

}

echo Rodape("");

MostraJavascript();

exit;

function MontaQueryAutorizacao() {
  global $usuarmad,$usulogado,$idEmpresa,$cdSolicitacao,$nomeCandidato,$idEmbarcacaoProjeto,$idStatusAcomp,$cdPaisResidencia,$msg;
  if($usuarmad == "S") {
    $extraproj = " AND NU_ARMADOR=$usulogado";
  } 
  $sql1 = "select a.NU_CANDIDATO,NO_PRIMEIRO_NOME,NO_NOME_MEIO,NO_ULTIMO_NOME,NU_EMBARCACAO_PROJETO,b.NU_SOLICITACAO,a.CO_PAIS_RESIDENCIA,c.cd_sequencial, ";
  $sql1 = $sql1." c.cd_tecnico,date(c.dt_tecnico) as dt_tecnico,c.cd_coordenador,date(c.dt_coordenador) as dt_coordenador,c.cd_empresa,date(c.dt_empresa) dt_empresa ";
  $sql1 = $sql1." from CANDIDATO a,AUTORIZACAO_CANDIDATO b,solicita_visto_acomp_cand c ";
  $sql1 = $sql1." where b.NU_EMPRESA=$idEmpresa and c.nu_empresa=b.NU_EMPRESA ";
  $sql1 = $sql1." and b.NU_CANDIDATO=a.NU_CANDIDATO and c.cd_candidato=a.NU_CANDIDATO and c.cd_solicitacao=b.NU_SOLICITACAO ";
  $sql1 = $sql1.$extraproj;
  $sql2 = "";
  if(strlen($cdSolicitacao)>0){
     $sql2 = $sql2." and b.NU_SOLICITACAO=$cdSolicitacao ";
  } else if(strlen($nomeCandidato)==0) {
     $sql1 = $sql1." and b.NU_SOLICITACAO in (SELECT max(NU_SOLICITACAO) FROM AUTORIZACAO_CANDIDATO d where d.NU_CANDIDATO = b.NU_CANDIDATO)"; 
  }
  if(strlen($idEmbarcacaoProjeto)>0){
     $sql2 = $sql2." and b.NU_EMBARCACAO_PROJETO=$idEmbarcacaoProjeto";
  }
  if(strlen($nomeCandidato)>0){
     $aux = split(" ",$nomeCandidato);
     $nomeTeste = "";
     for($x=0;$x<count($aux);$x++) {
        $nomeTeste = "+".$aux[$x]." ".$nomeTeste;
     }
     $sql2 = $sql2." and MATCH (a.NO_PRIMEIRO_NOME,a.NO_NOME_MEIO,a.NO_ULTIMO_NOME) AGAINST ('$nomeTeste' IN BOOLEAN MODE)";
  }
  if(strlen($cdPaisResidencia)>0) {
     $sql2 = $sql2." and a.CO_PAIS_RESIDENCIA=$cdPaisResidencia";
  }
  if(strlen($idStatusAcomp)>0) {
    if($idStatusAcomp==1) {
       $sql2 = $sql2." and c.dt_tecnico is NULL ";
    } else if($idStatusAcomp==2) {
       $sql2 = $sql2." and c.dt_tecnico is not NULL and c.dt_coordenador is NULL ";
    } else if($idStatusAcomp==3) {
       $sql2 = $sql2." and c.dt_tecnico is not NULL and c.dt_coordenador is not NULL and c.dt_empresa is NULL";
    } else if($idStatusAcomp==4) {
       $sql2 = $sql2." and c.dt_tecnico is not NULL and c.dt_coordenador is not NULL and c.dt_empresa is not NULL";
    } else if($idStatusAcomp==5) {
       $sql2 = $sql2." and b.DT_ABERTURA_PROCESSO_MTE is not NULL and b.DT_AUTORIZACAO_MTE is NULL ";
    } else if($idStatusAcomp==6) {
       $sql2 = $sql2." and b.DT_ABERTURA_PROCESSO_MTE is not NULL and b.DT_AUTORIZACAO_MTE is NULL ";
    }
  }
  if(strlen($sql2)==0) {
     $msg = "Entre com pelo menos 1 filtro.";
     $sql2 = $sql2." and a.NU_CANDIDATO=0";
  }
  $sql3 = " ORDER BY a.NO_PRIMEIRO_NOME,a.NO_NOME_MEIO,a.NO_ULTIMO_NOME ";
  $sql = $sql1.$sql2.$sql3;
  return $sql;
}

function MontaTelaPesquisaAutorizacao() {
  global $usuarmad,$usulogado,$idEmpresa,$nomeEmpresa,$estilo,$cdSolicitacao,$nomeCandidato,$idEmbarcacaoProjeto,$ehCliente,$idStatusAcomp,$cdPaisResidencia;
  if($usuarmad == "S") {
    $extraproj = " AND NU_ARMADOR=$usulogado";
  } 
  $cmbProjetos = montaComboProjetos($idEmbarcacaoProjeto,$idEmpresa,$extraproj." ORDER BY NO_EMBARCACAO_PROJETO");
  $cmbPaisRes = montaComboPais($cdPaisResidencia,"");
  $ret = "";
  $ret = $ret."  <form action='autorizacao_inicial.php' method='post' name='filtro'>\n";
  $ret = $ret."  <input type=hidden name=idEmpresa value='$idEmpresa'>\n";
  $ret = $ret."  <tr class='textoazul'><td NOWRAP>Empresa:</td><td colspan=2><b>$nomeEmpresa</b></a></td>\n";
  if($ehCliente=="S") {
    $ret = $ret."   <td align=center>&#160;</td>\n";
    $cmbSituacoes = "";
  } else {
    $ret = $ret."   <td align=center><input type='button' class='textformtopo' style=\"$estilo\" value='Voltar Empresa' onclick='javascript:voltarempresa();'></td>\n";
    $cmbSituacoes = ComboSituacoesAutorizacao($sit);
  }
  $ret = $ret."  </tr><tr><td colspan=4>&#160;</td></tr><tr class='textoazul'>\n";
  $ret = $ret."   <td>Situa&ccedil;&atilde;o:</td><td><select name='idStatusAcomp' class='textoazul'><option value=''>Todos $cmbSituacoes</select></td>\n";
  $ret = $ret."   <td>Num. Solicita&ccedil;&atilde;o:</td><td><input type='text' name='cdSolicitacao' value='$cdSolicitacao' size='10'></td>\n";
  $ret = $ret."  </tr><tr class='textoazul'>\n";
  $ret = $ret."   <td nowrap>Projeto/Embarca&ccedil;&atilde;o:</td>\n";
  $ret = $ret."   <td><select name='idEmbarcacaoProjeto' class='textoazul'><option value=''>Selecione...</option>$cmbProjetos</select></td>\n";
  $ret = $ret."   <td nowrap>Nome do candidato:</td><td><input type='text' name='nomeCandidato' value='$nomeCandidato' size=30 class='textoazul'></td>\n";
  $ret = $ret."   </tr>\n";
  $ret = $ret."  </tr><tr class='textoazul'>\n";
  $ret = $ret."   <td nowrap>Pa&iacute;s Resid&ecirc;ncia:</td>\n";
  $ret = $ret."   <td><select name='cdPaisResidencia' class='textoazul'><option value=''>Selecione...</option>$cmbPaisRes</select></td>\n";
  $ret = $ret."   <td align=center></td><td align=center></td></tr>\n";
  $ret = $ret."  <tr><td colspan=4 align=center><br><input type='submit' style=\"$estilo\" value='Listar'></td></tr>\n";
  $ret = $ret."</form></table>\n";
  return $ret;
}

function ComboSituacoesAutorizacao($sit) {
  global $ehCliente;
  $lst = ListaSituacoesAutorizacao();
  $ret = "";
  $y = 1;
  if($ehCliente=="S") { $y = 3; }
  for($x=$y;$x<=6;$x++) {
    if( (strlen($sit)>0) && ($sit==$x) ) { $sel = " selected "; } else { $sel = ""; }
    $ret = $ret."<option value='$x' $sel>".$lst[$x];
  }
  return $ret;
}

function ListaSituacoesAutorizacao() {
  $ret[1] = "Pendente do Tecnico";
  $ret[2] = "Pendente do Coordenador";
  $ret[3] = "Pendente de Autorizacao";
  $ret[4] = "Autorizado";
  $ret[5] = "Enviado para o MTE";
  $ret[6] = "Deferido no MTE";
  return $ret;
}

function MostraJavascript() {
  global $idEmpresa,$idEmbarcacaoProjeto;
?>

<script language="javascript">

var idEmpresa = "<?=$idEmpresa?>";
var idEmbarcacaoProjeto = "<?=$idEmbarcacaoProjeto?>";

function mostra(codigo) {
  var pagina = "/intranet/geral/novoCandidato.php?idCandidato=" + codigo + "&acao=V&idEmpresa=" + idEmpresa;
  var ret = AbrePagina("V",pagina,idEmpresa,codigo,idEmbarcacaoProjeto);
}

function voltarempresa() {
  history.go(-1);
}

function OpenChild(tipo,pagina,emp,cand,proj) {
    var ret = '';
    var MyArgs = new Array(emp,cand,tipo,proj);
    var WinSettings = "center:yes;resizable:yes;dialogHeight:580px;dialogWidth=800px";
    var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
    if (MyArgsRet != null) {
        var msg = MyArgsRet[0].toString();
        var erro = MyArgsRet[1].toString();
        if(erro.length > 0) {
            document.all.aguarde.innerHTML = "<!--"+erro+"-->";
            document.all.aguardeerr.innerHTML = "<!--"+msg+"-->";
        }
    } else {
        document.all.aguardeerr.innerHTML = "<!-- retorno nulo, provavelmente cancelou a operação -- --> ";
        msg = "Houve um erro ao executar o pedido, favor tentar mais tarde.";
    }
    return msg;
}

function AbrePagina(tipo,pagina,emp,cand,proj) {
    var ret = '';
    var MyArgs = new Array(emp,cand,tipo,proj);
    var WinSettings = "center:yes;resizable:yes;dialogHeight:580px;dialogWidth=800px";
    var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
    if (MyArgsRet != null) {
        var msg = MyArgsRet[0].toString();
        var erro = MyArgsRet[1].toString();
    }
    return msg;
}

function mostraSolicitacao(cdSol) {
    var MyArgs = new Array();
    var pagina = "/solicitacoes/mostraDadosSolicitacao.php?acao=obs&idEmpresa=<?=$idEmpresa?>&solicitacao="+cdSol;
    var WinSettings = "center:yes;resizable:yes;dialogHeight:600px;dialogWidth=780px"
    var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
}

function escolheempresa(obj) {
  if(document.emp.idEmpresa[document.emp.idEmpresa.selectedIndex].value=="") {
    alert('Escolha a empresa.');
  } else {
    document.emp.submit();
  }
}

</script>

<form name=candidato method=post>
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
<input type=hidden name=idEmbarcacaoProjeto value='<?=$idEmbarcacaoProjeto?>'>
<input type=hidden name=idCandidato>
<input type=hidden name=acao>
<input type=hidden name=nomeCandidato>
</form>

<form name=empresa method=post action="empresas.php">
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
</form>

<?php

}

?>
