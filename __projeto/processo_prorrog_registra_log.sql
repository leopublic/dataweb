delimiter ; 
drop trigger if exists processo_prorrog_registra_log ;
delimiter $$
CREATE TRIGGER processo_prorrog_registra_log BEFORE UPDATE ON processo_prorrog
  FOR EACH ROW
  BEGIN
    DECLARE alterou tinyint;
	DECLARE msg varchar(2000);
	DECLARE virgula varchar(2);
	DECLARE x_no_servico_resumido varchar(500);
	DECLARE x_old_no_servico_resumido varchar(500);
	DECLARE x_no_status_conclusao varchar(500);
	DECLARE x_old_no_status_conclusao varchar(500);
	DECLARE x_nome varchar(500);
	DECLARE x_old_nome varchar(500);
    
    SET alterou = 0;
	SET msg = '';
	SET virgula = '';

    IF ifnull(NEW.cd_candidato, '') <> ifnull(OLD.cd_candidato, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' migrou do candidato ', ifnull(OLD.cd_candidato, '(nulo)'), ' para o candidato ', ifnull(NEW.cd_candidato, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_protocolo, '') <> ifnull(OLD.nu_protocolo, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' n�mero do protocolo de ', ifnull(OLD.nu_protocolo, '(nulo)'), ' para ', ifnull(NEW.nu_protocolo, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_servico, 0) <> ifnull(OLD.nu_servico, 0) THEN 
      SET alterou = 1;
	  select no_servico_resumido into x_no_servico_resumido from servico where nu_servico = NEW.nu_servico;
	  select no_servico_resumido into x_old_no_servico_resumido  from servico where nu_servico = OLD.nu_servico;

 	  SET msg = concat(msg, virgula, ' do servi�o ', ifnull(x_old_no_servico_resumido, '(nulo)'), ' para ', ifnull(x_no_servico_resumido, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.ID_STATUS_CONCLUSAO, '') <> ifnull(OLD.ID_STATUS_CONCLUSAO, '') THEN 
      SET alterou = 1;
	  select no_status_conclusao into x_no_status_conclusao from status_conclusao where ID_STATUS_CONCLUSAO = NEW.ID_STATUS_CONCLUSAO;
	  select no_status_conclusao into x_old_no_status_conclusao from status_conclusao where ID_STATUS_CONCLUSAO = OLD.ID_STATUS_CONCLUSAO;

	  SET msg = concat(msg, virgula, ' do status ', ifnull(x_old_no_status_conclusao, '(nulo)'), ' para ', ifnull(x_no_status_conclusao, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_requerimento, '') <> ifnull(OLD.dt_requerimento, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' data requerimento de ', ifnull(OLD.dt_requerimento, '(nulo)'), ' para ', ifnull(NEW.dt_requerimento, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_validade, '') <> ifnull(OLD.dt_validade, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' data de validade de ', ifnull(OLD.dt_validade, '(nulo)'), ' para ', ifnull(NEW.dt_validade, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_prazo_pret, '') <> ifnull(OLD.dt_prazo_pret, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' prazo pretendido de ', ifnull(OLD.dt_prazo_pret, '(nulo)'), ' para ', ifnull(NEW.dt_prazo_pret, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_envio_bsb, '') <> ifnull(OLD.dt_envio_bsb, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' envio BSB de ', ifnull(OLD.dt_envio_bsb, '(nulo)'), ' para ', ifnull(NEW.dt_envio_bsb, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.id_solicita_visto, '') <> ifnull(OLD.id_solicita_visto, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' OS de ', ifnull(OLD.id_solicita_visto, '(nulo)'), ' para ', ifnull(NEW.id_solicita_visto, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.codigo_processo_mte, '') <> ifnull(OLD.codigo_processo_mte, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' migrou do visto ', ifnull(OLD.codigo_processo_mte, '(nulo)'), ' para o visto ', ifnull(NEW.codigo_processo_mte, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_publicacao_dou, '') <> ifnull(OLD.dt_publicacao_dou, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' data de publica��o no DOU de ', ifnull(OLD.dt_publicacao_dou, '(nulo)'), ' para ', ifnull(NEW.dt_publicacao_dou, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_pagina_dou, '') <> ifnull(OLD.nu_pagina_dou, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' p�gina onde foi publicada no DOU de ', ifnull(OLD.nu_pagina_dou, '(nulo)'), ' para ', ifnull(NEW.nu_pagina_dou, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_pre_cadastro, '') <> ifnull(OLD.nu_pre_cadastro, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' pr�-cadastro de', ifnull(OLD.nu_pre_cadastro, '(nulo)'), ' para ', ifnull(NEW.nu_pre_cadastro, '(nulo)'));
	  SET virgula = ', ';
	  IF NEW.cd_usuario_pre_cadastro is null THEN
		SET NEW.cd_usuario_pre_cadastro = NEW.cd_usuario;
	  END IF;
    END IF;

    IF ifnull(NEW.dt_pre_cadastro, '') <> ifnull(OLD.dt_pre_cadastro, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' data do pr�-cadastro de', ifnull(OLD.dt_pre_cadastro, '(nulo)'), ' para ', ifnull(NEW.dt_pre_cadastro, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.cd_usuario_pre_cadastro, '') <> ifnull(OLD.cd_usuario_pre_cadastro, '') THEN 
      SET alterou = 1;
	  select nome into x_nome from usuarios where cd_usuario = NEW.cd_usuario_pre_cadastro;
	  select nome into x_old_nome from usuarios  where cd_usuario = OLD.cd_usuario_pre_cadastro;

 	  SET msg = concat(msg, virgula, ' de pr�-cadastrado por ', ifnull(x_old_nome, '(nulo)'), ' para ', ifnull(x_nome, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.observacao, '') <> ifnull(OLD.observacao, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' observa��o de "', ifnull(OLD.observacao, '(nulo)'), '" para "', ifnull(NEW.observacao, '(nulo)'), '"');
	  SET virgula = ', ';
    END IF;

    IF msg <> '' THEN
        SET msg = concat('Altera��es: ', msg);
    END IF;

    IF alterou = 1 THEN
		insert into historico_processo_prorrog(codigo, cd_usuario, hpro_tx_observacao) values (NEW.codigo, NEW.cd_usuario, msg);
    END IF;
  END;
$$
