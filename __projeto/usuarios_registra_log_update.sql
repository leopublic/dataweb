use dataweb;
-- Trigger DDL Statements
DELIMITER $$
drop trigger if exists usuarios_registra_log_update; $$
CREATE
TRIGGER usuarios_registra_log_update
	BEFORE UPDATE ON usuarios
	FOR EACH ROW
	BEGIN
		DECLARE alterou tinyint;
		IF NEW.cd_usuario <> OLD.cd_usuario THEN
			SET alterou = 1;
		END IF;
		IF NEW.cd_perfil <> OLD.cd_perfil THEN
			SET alterou = 1;
		END IF;
		IF NEW.cd_empresa <> OLD.cd_empresa THEN
			SET alterou = 1;
		END IF;
		IF NEW.login <> OLD.login THEN
			SET alterou = 1;
		END IF;
		IF NEW.nm_senha <> OLD.nm_senha THEN
			SET alterou = 1;
		END IF;
		IF NEW.nome <> OLD.nome THEN
			SET alterou = 1;
		END IF;
		IF NEW.nm_email <> OLD.nm_email THEN
			SET alterou = 1;
		END IF;
		IF NEW.cd_superior <> OLD.cd_superior THEN
			SET alterou = 1;
		END IF;
		IF NEW.dt_cadastro <> OLD.dt_cadastro THEN
			SET alterou = 1;
		END IF;
		IF NEW.dt_ult <> OLD.dt_ult THEN
			SET alterou = 1;
		END IF;
		IF NEW.flagativo <> OLD.flagativo THEN
			SET alterou = 1;
		END IF;
		IF NEW.usua_tx_ip_ult_acesso <> OLD.usua_tx_ip_ult_acesso THEN
			SET alterou = 1;
		END IF;
		IF NEW.usua_dt_ult_acesso <> OLD.usua_dt_ult_acesso THEN
			SET alterou = 1;
		END IF;


		IF alterou = 1 THEN
			insert into dataweb_log.usuarios_log (
				cd_usuario
				,cd_perfil
				,cd_empresa
				,login
				,nm_senha
				,nome
				,nm_email
				,cd_superior
				,dt_cadastro
				,dt_ult
				,flagativo
				,usua_tx_ip_ult_acesso
				,usua_dt_ult_acesso
				,cd_usuario_log
				,log_tx_controller
				,log_tx_metodo
				,log_dt_evento
				,log_tx_evento
			)
			values (
				OLD.cd_usuario
				,OLD.cd_perfil
				,OLD.cd_empresa
				,OLD.login
				,OLD.nm_senha
				,OLD.nome
				,OLD.nm_email
				,OLD.cd_superior
				,OLD.dt_cadastro
				,OLD.dt_ult
				,OLD.flagativo
				,OLD.usua_tx_ip_ult_acesso
				,OLD.usua_dt_ult_acesso
				,OLD.cd_usuario_log
				,OLD.log_tx_controller
				,OLD.log_tx_metodo
				,now()
				,'update'
			);
		END IF;
	END$$


