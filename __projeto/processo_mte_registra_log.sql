delimiter ; 
drop trigger if exists processo_mte_registra_log ;
delimiter $$
CREATE TRIGGER processo_mte_registra_log BEFORE UPDATE ON processo_mte
  FOR EACH ROW
  BEGIN
    DECLARE alterou tinyint;
  	DECLARE msg varchar(2000);
  	DECLARE virgula varchar(2);
  	DECLARE x_no_servico_resumido varchar(500);
  	DECLARE x_old_no_servico_resumido varchar(500);
  	DECLARE x_no_status_conclusao varchar(500);
  	DECLARE x_old_no_status_conclusao varchar(500);
  	DECLARE x_nome varchar(500);
  	DECLARE x_old_nome varchar(500);
    DECLARE x_desc varchar(1000);
    DECLARE x_old_desc varchar(1000);
    
    SET alterou = 0;
  	SET msg = '';
  	SET virgula = '';

    IF ifnull(NEW.cd_candidato, '') <> ifnull(OLD.cd_candidato, '') THEN 
      SET alterou = 1;
  	  SET msg = concat(msg, virgula, ' migrou do candidato ', ifnull(OLD.cd_candidato, '(nulo)'), ' para o candidato ', ifnull(NEW.cd_candidato, '(nulo)'));
  	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_processo, '') <> ifnull(OLD.nu_processo, '') THEN 
      SET alterou = 1;
  	  SET msg = concat(msg, virgula, ' n�mero do protocolo de ', ifnull(OLD.nu_processo, '(nulo)'), ' para ', ifnull(NEW.nu_processo, '(nulo)'));
  	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_requerimento, '') <> ifnull(OLD.dt_requerimento, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data requerimento de ', ifnull(OLD.dt_requerimento, '(nulo)'), ' para ', ifnull(NEW.dt_requerimento, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_oficio, '') <> ifnull(OLD.nu_oficio, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' n�mero do of�cio de', ifnull(OLD.nu_oficio, '(nulo)'), ' para ', ifnull(NEW.nu_oficio, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_deferimento, '') <> ifnull(OLD.dt_deferimento, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data deferimento de ', ifnull(OLD.dt_deferimento, '(nulo)'), ' para ', ifnull(NEW.dt_deferimento, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.prazo_solicitado, '') <> ifnull(OLD.prazo_solicitado, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' prazo de estada solicitado (os) de ', ifnull(OLD.prazo_solicitado, '(nulo)'), ' para ', ifnull(NEW.prazo_solicitado, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.observacao, '') <> ifnull(OLD.observacao, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' observa��o de ', ifnull(OLD.observacao, '(nulo)'), ' para ', ifnull(NEW.observacao, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.id_solicita_visto, '') <> ifnull(OLD.id_solicita_visto, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' OS de ', ifnull(OLD.id_solicita_visto, '(nulo)'), ' para ', ifnull(NEW.id_solicita_visto, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_ult_atu_andamento, '') <> ifnull(OLD.dt_ult_atu_andamento, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data �ltima atualiza��o do andamento de ', ifnull(OLD.dt_ult_atu_andamento, '(nulo)'), ' para ', ifnull(NEW.dt_ult_atu_andamento, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_publicacao_dou, '') <> ifnull(OLD.dt_publicacao_dou, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data publica��o DOU de ', ifnull(OLD.dt_publicacao_dou, '(nulo)'), ' para ', ifnull(NEW.dt_publicacao_dou, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_envio_bsb, '') <> ifnull(OLD.dt_envio_bsb, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' envio BSB de ', ifnull(OLD.dt_envio_bsb, '(nulo)'), ' para ', ifnull(NEW.dt_envio_bsb, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_recebimento_bsb, '') <> ifnull(OLD.dt_recebimento_bsb, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' recebimento em BSB de ', ifnull(OLD.dt_recebimento_bsb, '(nulo)'), ' para ', ifnull(NEW.dt_recebimento_bsb, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.ID_STATUS_CONCLUSAO, '') <> ifnull(OLD.ID_STATUS_CONCLUSAO, '') THEN 
      SET alterou = 1;
      select no_status_conclusao into x_no_status_conclusao from status_conclusao where ID_STATUS_CONCLUSAO = NEW.ID_STATUS_CONCLUSAO;
      select no_status_conclusao into x_old_no_status_conclusao from status_conclusao where ID_STATUS_CONCLUSAO = OLD.ID_STATUS_CONCLUSAO;

      SET msg = concat(msg, virgula, ' da conclus�o ', ifnull(x_old_no_status_conclusao, '(nulo)'), ' para ', ifnull(x_no_status_conclusao, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.ID_STATUS_ANDAMENTO, '') <> ifnull(OLD.ID_STATUS_ANDAMENTO, '') THEN 
      SET alterou = 1;
      select no_status_andamento into x_desc from status_andamento where ID_STATUS_ANDAMENTO = NEW.ID_STATUS_ANDAMENTO;
      select no_status_andamento into x_old_desc from status_andamento where ID_STATUS_ANDAMENTO = OLD.ID_STATUS_ANDAMENTO;

      SET msg = concat(msg, virgula, ' do andamento ', ifnull(x_old_desc, '(nulo)'), ' para ', ifnull(x_desc, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_servico, 0) <> ifnull(OLD.nu_servico, 0) THEN 
      SET alterou = 1;
  	  select no_servico_resumido into x_no_servico_resumido from servico where nu_servico = NEW.nu_servico;
  	  select no_servico_resumido into x_old_no_servico_resumido  from servico where nu_servico = OLD.nu_servico;

   	  SET msg = concat(msg, virgula, ' do servi�o ', ifnull(x_old_no_servico_resumido, '(nulo)'), ' para ', ifnull(x_no_servico_resumido, '(nulo)'));
  	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.observacao_visto, '') <> ifnull(OLD.observacao_visto, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' observa��o do visto de ', ifnull(OLD.observacao_visto, '(nulo)'), ' para ', ifnull(NEW.observacao_visto, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF msg <> '' THEN
      SET msg = concat('Altera��es:', msg);
    END IF;

    IF alterou = 1 THEN
		  insert into historico_processo_mte(codigo, cd_usuario, hmte_tx_observacao) values (NEW.codigo, NEW.cd_usuario, msg);
    END IF;
  END;
$$
