-- alter table reparticao_consular engine=innodb;
-- alter table candidato engine=innodb;
-- alter table processo_mte engine=innodb;

drop table if exists processo_troca_rep_consular;

/*==============================================================*/
/* Table: processo_troca_rep_consular                           */
/*==============================================================*/
create table processo_troca_rep_consular
(
   ptrp_id              int not null auto_increment,
   nu_candidato         int(11),
   co_reparticao_consular_atual int(11) default 0,
   co_reparticao_consular_nova int(11) default 0,
   ptrp_tx_observacao text,
   codigo_processo_mte  int(11),
   primary key (ptrp_id)
)
Engine=InnoDB;

alter table processo_troca_rep_consular add constraint fk_processo_troca_rep_consular_atual foreign key (co_reparticao_consular_atual)
      references reparticao_consular (co_reparticao_consular) on delete restrict on update restrict;

alter table processo_troca_rep_consular add constraint fk_processo_troca_rep_consular_candidato foreign key (nu_candidato)
      references candidato (nu_candidato) on delete restrict on update restrict;

alter table processo_troca_rep_consular add constraint fk_processo_troca_rep_consular_nova foreign key (co_reparticao_consular_nova)
      references reparticao_consular (co_reparticao_consular) on delete restrict on update restrict;

alter table processo_troca_rep_consular add constraint fk_processo_troca_rep_consular_processo_mte foreign key (codigo_processo_mte)
      references processo_mte (codigo) on delete restrict on update restrict;
