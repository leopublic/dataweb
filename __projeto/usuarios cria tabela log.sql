use dataweb;
alter table usuarios add cd_usuario_log int null
	,add log_tx_controller varchar(200)
	,add log_tx_metodo varchar(200)
	,add log_dt_evento datetime
	,add log_tx_evento varchar(50)
;

drop table if exists dataweb.usuarios_log;
drop table if exists dataweb_log.usuarios_log;

/*==============================================================*/
/* Table: usuarios_log                                          */
/*==============================================================*/
create table dataweb_log.usuarios_log
(
   usul_id              int not null auto_increment,
   cd_usuario           smallint(6) ,
   cd_perfil            smallint(6) ,
   cd_empresa           smallint(6) ,
   login                varchar(20) ,
   nm_senha             varchar(10) ,
   nome                 varchar(50) ,
   nm_email             varchar(100),
   cd_superior          smallint(6) ,
   dt_cadastro          date default NULL,
   dt_ult               timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   flagativo            char(1) ,
   usua_tx_ip_ult_acesso varchar(20),
   usua_dt_ult_acesso   datetime,
   log_tx_controller    varchar(200),
   log_tx_metodo        varchar(200),
   cd_usuario_log       int,
   log_dt_evento        datetime,
   log_tx_evento        varchar(50),
   primary key (usul_id)
)
Engine = InnoDB;
