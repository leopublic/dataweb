use dataweb;
-- Trigger DDL Statements
DELIMITER $$
drop procedure if exists os_gera_numero; $$
CREATE procedure os_gera_numero (IN p_id_solicita_visto int (11) )
	BEGIN
    START TRANSACTION;
    select max(nu_solicitacao) + 1 into @nu_solicitacao from solicita_visto for update;
    select max(nu_solicitacao) + 1 into @nu_solicitacao_excluido from dataweb_log.solicita_visto for update;
    if (@nu_solicitacao_excluido > @nu_solicitacao) then
      set @nu_solicitacao = @nu_solicitacao_excluido;
    end if;
    update solicita_visto set nu_solicitacao = @nu_solicitacao where id_solicita_visto = p_id_solicita_visto;
    COMMIT ;
    select @nu_solicitacao nu_solicitacao;
  END;
  $$
  