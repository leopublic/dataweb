drop view if exists vsolicita_visto;
create view vsolicita_visto as 
select
sv.nu_empresa AS NU_EMPRESA,
sv.NU_EMBARCACAO_PROJETO AS NU_EMBARCACAO_PROJETO,
sv.id_solicita_visto AS id_solicita_visto,
sv.NU_SERVICO AS NU_SERVICO,
sv.nu_solicitacao AS NU_SOLICITACAO,
sv.cd_admin_cad AS cd_admin_cad,
sv.ID_STATUS_SOL AS ID_STATUS_SOL,
sv.no_solicitador AS no_solicitador,
sv.de_observacao AS de_observacao,
sv.fl_nao_conformidade AS fl_nao_conformidade,
sv.nu_empresa_requerente AS nu_empresa_requerente,
sv.fl_cobrado AS fl_cobrado,
sv.fl_sistema_antigo AS fl_sistema_antigo,
sv.NU_EMBARCACAO_PROJETO_COBRANCA AS NU_EMBARCACAO_PROJETO_COBRANCA,
sv.tppr_id AS tppr_id,
date_format(sv.dt_solicitacao,'%d/%m/%Y') AS dt_solicitacao,
date_format(sv.dt_cadastro,'%d/%m/%Y') AS dt_cadastro,
ac.DT_SITUACAO_SOL AS DT_SITUACAO_SOL,
ac.CD_EMBARCADO AS CD_EMBARCADO,
date_format(ac.DT_PRAZO_ESTADA,'%d/%m/%Y') AS DT_PRAZO_ESTADA,
ac.DT_PRAZO_ESTADA_SOLICITADO AS DT_PRAZO_ESTADA_SOLICITADO,
sv.cd_tecnico AS cd_tecnico,
ep.NO_EMBARCACAO_PROJETO AS NO_EMBARCACAO_PROJETO,
epc.NO_EMBARCACAO_PROJETO AS NO_EMBARCACAO_PROJETO_COBRANCA,
ut.nome AS NO_USUARIO_TECNICO,
uc.nome AS NO_USUARIO_CADASTRO,
e.NO_RAZAO_SOCIAL AS NO_RAZAO_SOCIAL,
er.NO_RAZAO_SOCIAL AS NO_RAZAO_SOCIAL_REQUERENTE,
s.CO_SERVICO AS CO_SERVICO,
s.NO_SERVICO AS NO_SERVICO,
s.NO_SERVICO_RESUMIDO AS NO_SERVICO_RESUMIDO,
s.NU_TIPO_SERVICO AS NU_TIPO_SERVICO,
s.ID_TIPO_ACOMPANHAMENTO AS ID_TIPO_ACOMPANHAMENTO,
s.CO_TIPO_AUTORIZACAO AS CO_TIPO_AUTORIZACAO,
s.CO_CLASSIFICACAO_VISTO AS CO_CLASSIFICACAO_VISTO,
s.NU_FORMULARIOS AS NU_FORMULARIOS,
tp.tppr_tx_nome AS tppr_tx_nome,
tbp.tbpc_tx_nome AS tbpc_tx_nome
from solicita_visto sv
left join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto
left join embarcacao_projeto ep on ep.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO and ep.NU_EMPRESA = sv.nu_empresa
left join embarcacao_projeto epc on epc.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO_COBRANCA and epc.NU_EMPRESA = sv.nu_empresa_requerente
left join vservico s on s.NU_SERVICO = sv.NU_SERVICO
left join usuarios ut on ut.cd_usuario = sv.cd_tecnico
left join usuarios uc on uc.cd_usuario = sv.cd_admin_cad
left join empresa e on e.NU_EMPRESA = sv.nu_empresa
left join empresa er on er.NU_EMPRESA = sv.nu_empresa_requerente
left join tipo_preco tp on tp.tppr_id = sv.tppr_id
left join tabela_precos tbp on tbp.tbpc_id = er.tbpc_id
;
