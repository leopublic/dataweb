drop view if exists vprocessos_candidato;
create view vprocessos_candidato as
select cd_candidato, 'processo_mte' tabela, 1 prioridade, codigo, dt_cad, dt_ult, id_solicita_visto, null codigo_processo_mte from processo_mte
union select cd_candidato, 'processo_regcie' tabela, 2 prioridade, codigo,  dt_cad, dt_ult, id_solicita_visto, codigo_processo_mte from processo_regcie
union select cd_candidato, 'processo_emiscie' tabela, 3 prioridade, codigo,  dt_cad, dt_ult, id_solicita_visto, codigo_processo_mte from processo_emiscie
union select cd_candidato, 'processo_prorrog' tabela, 4 prioridade, codigo,  dt_cad, dt_ult, id_solicita_visto, codigo_processo_mte from processo_prorrog
union select cd_candidato, 'processo_cancel' tabela, 5 prioridade, codigo,  dt_cad, dt_ult, id_solicita_visto, codigo_processo_mte from processo_cancel
;

select * from vprocessos_candidato where codigo_processo_mte = 18545 and cd_candidato = 15836
