drop view if exists vORDEM_SERVICO_PROTOCOLO_COLETA;
create view vORDEM_SERVICO_PROTOCOLO_COLETA as
select E.NO_RAZAO_SOCIAL
  , C.NOME_COMPLETO
  , C.NU_PASSAPORTE
  , EP.NO_EMBARCACAO_PROJETO
  , S.NO_SERVICO_RESUMIDO
  , S.ID_TIPO_ACOMPANHAMENTO
  , TA.NO_REDUZIDO_TIPO_AUTORIZACAO
  , RC.NO_REPARTICAO_CONSULAR
  , AC.DT_PRAZO_ESTADA_SOLICITADO
  , AC.NU_EMBARCACAO_PROJETO
  , sv.id_solicita_visto
  , sv.nu_empresa
  , sv.dt_cadastro
  , sv.dt_solicitacao
  , sv.nu_solicitacao
  , date_format(sv.dt_cadastro, '%d/%m/%y') dt_cadastro_fmt
  , date_format(sv.dt_solicitacao, '%d/%m/%y') dt_solicitacao_fmt
  , pm.nu_oficio
  , REPLACE(pm.nu_processo, '-', '') nu_processo
  , pm.dt_deferimento
  , date_format(pm.dt_deferimento, '%d/%m/%y') dt_deferimento_fmt
  , pm.prazo_solicitado
  , ACp.DT_PRAZO_ESTADA_SOLICITADO DT_PRAZO_ESTADA_SOLICITADO_P
  , u.nome
from solicita_visto sv
join AUTORIZACAO_CANDIDATO AC on AC.id_solicita_visto = sv.id_solicita_visto
join EMPRESA E on E.NU_EMPRESA = sv.nu_empresa
join CANDIDATO C on C.NU_CANDIDATO = AC.NU_CANDIDATO
join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO
left join processo_mte pm on pm.codigo = (select max(codigo) from processo_mte where cd_candidato = C.NU_CANDIDATO)
left join solicita_visto svp on svp.id_solicita_visto = pm.id_solicita_visto
left join SERVICO Sp on Sp.NU_SERVICO = svp.NU_SERVICO
left join AUTORIZACAO_CANDIDATO ACp on ACp.id_solicita_visto = svp.id_solicita_visto and ACp.NU_CANDIDATO = C.NU_CANDIDATO
left join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = c.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = c.NU_EMBARCACAO_PROJETO
left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = ACp.CO_REPARTICAO_CONSULAR
left join TIPO_AUTORIZACAO TA on TA.CO_TIPO_AUTORIZACAO = Sp.CO_TIPO_AUTORIZACAO
left join usuarios u on u.cd_usuario = sv.cd_admin_cad
where S.serv_fl_coleta = 1
;
