drop view if exists vprocesso_coleta;
create view vprocesso_coleta as
select 
    C.codigo
		,C.id_solicita_visto
		,C.cd_candidato
		,C.no_validade
		,C.nu_visto
    ,date_format(C.dt_emissao_visto,_latin1'%d/%m/%Y') AS dt_emissao_visto
		,dt_emissao_visto as dt_emissao_visto_orig
		,C.no_local_emissao
		,C.co_pais_nacionalidade_visto
		,C.co_classificacao
		,C.no_local_entrada
		,C.co_uf
    ,date_format(C.dt_entrada,_latin1'%d/%m/%Y') AS dt_entrada
		,C.dt_entrada as dt_entrada_orig
		,C.nu_transporte_entrada
		,C.fl_processo_atual
		,C.codigo_processo_mte
    ,C.nu_servico
    ,C.no_classe
    ,C.no_metodo
    ,P.NO_NACIONALIDADE
    ,P.NO_PAIS
    ,T.NO_TRANSPORTE_ENTRADA
    ,S.NO_SERVICO
    ,S.NO_SERVICO_RESUMIDO
    ,sv.ID_STATUS_SOL
    ,sv.nu_solicitacao
    ,CV.NO_CLASSIFICACAO_VISTO
    ,uf.no_nome
	,sv.de_observacao
from processo_coleta C
left join PAIS_NACIONALIDADE P on P.CO_PAIS = C.co_pais_nacionalidade_visto
left join TRANSPORTE_ENTRADA T on T.NU_TRANSPORTE_ENTRADA = C.nu_transporte_entrada
left join SERVICO S on S.NU_SERVICO = C.nu_servico
left join solicita_visto sv ON sv.id_solicita_visto = C.id_solicita_visto
left join processo_mte pm on pm.codigo = C.codigo_processo_mte
left join SERVICO Spm on Spm.NU_SERVICO = pm.NU_SERVICO
left join CLASSIFICACAO_VISTO CV ON CV.CO_CLASSIFICACAO_VISTO = Spm.CO_CLASSIFICACAO_VISTO
left join estado uf on uf.nu_estado = C.co_uf 
;
