drop view if exists vprocesso_prorrog;
CREATE VIEW vprocesso_prorrog AS 
select processo_prorrog.codigo 
,processo_prorrog.cd_candidato 
,processo_prorrog.cd_solicitacao 
,processo_prorrog.nu_protocolo 
,date_format(processo_prorrog.dt_requerimento,'%d/%m/%Y') AS dt_requerimento
,date_format(processo_prorrog.dt_validade,'%d/%m/%Y') AS dt_validade
,date_format(processo_prorrog.dt_prazo_pret,'%d/%m/%Y') AS dt_prazo_pret
,date_format(processo_prorrog.dt_envio_bsb_pre_cadastro,'%d/%m/%Y') AS dt_envio_bsb_pre_cadastro
,processo_prorrog.observacao 
,processo_prorrog.dt_cad 
,processo_prorrog.dt_ult 
,processo_prorrog.id_solicita_visto 
,processo_prorrog.fl_vazio
,processo_prorrog.fl_processo_atual
,processo_prorrog.codigo_processo_mte
,processo_prorrog.codigo_regcie
,date_format(processo_prorrog.dt_publicacao_dou,'%d/%m/%Y') AS dt_publicacao_dou
,processo_prorrog.ID_STATUS_CONCLUSAO
,processo_prorrog.nu_servico
,processo_prorrog.no_classe
,processo_prorrog.no_metodo
,processo_prorrog.nu_pre_cadastro
,processo_prorrog.dt_pre_cadastro as dt_pre_cadastro_orig
,processo_prorrog.dt_requerimento_pre_cadastro as dt_requerimento_pre_cadastro_orig
,date_format(processo_prorrog.dt_requerimento_pre_cadastro,'%d/%m/%Y') AS dt_requerimento_pre_cadastro
,date_format(processo_prorrog.dt_pre_cadastro,'%d/%m/%Y') AS dt_pre_cadastro
,date_format(processo_prorrog.dt_envio_bsb,'%d/%m/%Y') AS dt_envio_bsb
,processo_prorrog.nu_pagina_dou
,SC.NO_STATUS_CONCLUSAO
,S.NO_SERVICO
,S.NO_SERVICO_RESUMIDO
,sv.ID_STATUS_SOL
,sv.nu_solicitacao
,ss.no_status_sol_res
,u.nome nome_pre_cadastro
,sv.de_observacao
, concat(coalesce(concat(substr(replace(replace(replace(nu_protocolo, '-', ''), '.', ''), '/', '') , 1, 5), '.', substr(replace(replace(replace(nu_protocolo, '-', ''), '.', ''), '/', ''), 6, 6), '/', substr(replace(replace(replace(nu_protocolo, '-', ''), '.', ''), '/', ''), 12, 4), '-', substr(replace(replace(replace(nu_protocolo, '-', ''), '.', ''), '/', ''), 16)), '?????.??????/????-??'), ' - OS ', coalesce(sv.nu_solicitacao, '?????'), ' - prazo pretendido ', coalesce(processo_prorrog.dt_prazo_pret, '??')) as nu_protocolo_visto
from processo_prorrog
left join STATUS_CONCLUSAO SC on SC.ID_STATUS_CONCLUSAO = processo_prorrog.ID_STATUS_CONCLUSAO
left join SERVICO S on S.NU_SERVICO = processo_prorrog.nu_servico
left join solicita_visto sv ON sv.id_solicita_visto = processo_prorrog.id_solicita_visto
left join status_solicitacao ss ON ss.id_status_sol = sv.id_status_sol
left join usuarios u on u.cd_usuario = processo_prorrog.cd_usuario_pre_cadastro
;

