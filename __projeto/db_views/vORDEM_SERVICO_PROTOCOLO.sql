use dataweb;
drop view if exists vORDEM_SERVICO_PROTOCOLO;
create view vORDEM_SERVICO_PROTOCOLO as
select E.NO_RAZAO_SOCIAL
  , C.NOME_COMPLETO
  , C.NU_PASSAPORTE
  , EP.NO_EMBARCACAO_PROJETO
  , S.NO_SERVICO_RESUMIDO
  , S.ID_TIPO_ACOMPANHAMENTO
  , TA.NO_REDUZIDO_TIPO_AUTORIZACAO
  , RC.NO_REPARTICAO_CONSULAR
  , AC.DT_PRAZO_ESTADA_SOLICITADO
  , P.NO_NACIONALIDADE
  , sv.id_solicita_visto
  , sv.dt_cadastro
  , sv.dt_solicitacao
  , sv.nu_solicitacao
  , date_format(sv.dt_cadastro, '%d/%m/%y') dt_cadastro_fmt
  , date_format(sv.dt_solicitacao, '%d/%m/%y') dt_solicitacao_fmt
  , pm.codigo
  , pm.dt_envio_bsb 
  , date_format(pm.dt_envio_bsb, '%d/%m/%y') dt_envio_bsb_fmt
  , date_format(pm.dt_envio_bsb, '%d/%m/%Y') dt_envio_bsb_fmt_full
  , pm.dt_requerimento
  , date_format(pm.dt_requerimento, '%d/%m/%y') dt_requerimento_fmt
  , pm.nu_processo
from solicita_visto sv
join EMPRESA E on E.NU_EMPRESA = sv.nu_empresa
join AUTORIZACAO_CANDIDATO AC on AC.id_solicita_visto = sv.id_solicita_visto
join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = sv.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO
join CANDIDATO C on C.NU_CANDIDATO = AC.NU_CANDIDATO
join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO 
left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = AC.CO_REPARTICAO_CONSULAR
left join TIPO_AUTORIZACAO TA on TA.CO_TIPO_AUTORIZACAO = S.CO_TIPO_AUTORIZACAO
left join PAIS_NACIONALIDADE P on P.CO_PAIS = C.CO_NACIONALIDADE
left join processo_mte pm on pm.id_solicita_visto = sv.id_solicita_visto and pm.cd_candidato = AC.NU_CANDIDATO 
where S.ID_TIPO_ACOMPANHAMENTO =1
union
select E.NO_RAZAO_SOCIAL
  , C.NOME_COMPLETO
  , C.NU_PASSAPORTE
  , EP.NO_EMBARCACAO_PROJETO
  , S.NO_SERVICO_RESUMIDO
  , S.ID_TIPO_ACOMPANHAMENTO
  , TA.NO_REDUZIDO_TIPO_AUTORIZACAO
  , RC.NO_REPARTICAO_CONSULAR
  , AC.DT_PRAZO_ESTADA_SOLICITADO
  , P.NO_NACIONALIDADE
  , sv.id_solicita_visto
  , sv.dt_cadastro
  , sv.dt_solicitacao
  , sv.nu_solicitacao
  , date_format(sv.dt_cadastro, '%d/%m/%y') dt_cadastro_fmt
  , date_format(sv.dt_solicitacao, '%d/%m/%y') dt_solicitacao_fmt
  , pp.codigo
  , pp.dt_envio_bsb 
  , date_format(pp.dt_envio_bsb, '%d/%m/%y') dt_envio_bsb_fmt
  , date_format(pp.dt_envio_bsb, '%d/%m/%Y') dt_envio_bsb_fmt_full
  , pp.dt_requerimento
  , date_format(pp.dt_requerimento, '%d/%m/%y') dt_requerimento_fmt
  , pp.nu_protocolo nu_processo
from solicita_visto sv
join EMPRESA E on E.NU_EMPRESA = sv.nu_empresa
join AUTORIZACAO_CANDIDATO AC on AC.id_solicita_visto = sv.id_solicita_visto
join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = sv.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO
join CANDIDATO C on C.NU_CANDIDATO = AC.NU_CANDIDATO
join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO 
left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = AC.CO_REPARTICAO_CONSULAR
left join TIPO_AUTORIZACAO TA on TA.CO_TIPO_AUTORIZACAO = S.CO_TIPO_AUTORIZACAO
left join PAIS_NACIONALIDADE P on P.CO_PAIS = C.CO_NACIONALIDADE
left join processo_prorrog pp on pp.id_solicita_visto = sv.id_solicita_visto and pp.cd_candidato = AC.NU_CANDIDATO 
where S.ID_TIPO_ACOMPANHAMENTO =3
union
select E.NO_RAZAO_SOCIAL
  , C.NOME_COMPLETO
  , C.NU_PASSAPORTE
  , EP.NO_EMBARCACAO_PROJETO
  , S.NO_SERVICO_RESUMIDO
  , S.ID_TIPO_ACOMPANHAMENTO
  , TA.NO_REDUZIDO_TIPO_AUTORIZACAO
  , RC.NO_REPARTICAO_CONSULAR
  , AC.DT_PRAZO_ESTADA_SOLICITADO
  , P.NO_NACIONALIDADE
  , sv.id_solicita_visto
  , sv.dt_cadastro
  , sv.dt_solicitacao
  , sv.nu_solicitacao
  , date_format(sv.dt_cadastro, '%d/%m/%y') dt_cadastro_fmt
  , date_format(sv.dt_solicitacao, '%d/%m/%y') dt_solicitacao_fmt
  , pc.codigo
  , pc.dt_envio_bsb 
  , date_format(pc.dt_envio_bsb, '%d/%m/%y') dt_envio_bsb_fmt
  , date_format(pc.dt_envio_bsb, '%d/%m/%Y') dt_envio_bsb_fmt_full
  , pc.dt_processo dt_requerimento
  , date_format(pc.dt_processo, '%d/%m/%y') dt_requerimento_fmt
  , pc.nu_processo
from solicita_visto sv
join EMPRESA E on E.NU_EMPRESA = sv.nu_empresa
join AUTORIZACAO_CANDIDATO AC on AC.id_solicita_visto = sv.id_solicita_visto
join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = sv.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO
join CANDIDATO C on C.NU_CANDIDATO = AC.NU_CANDIDATO
join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO 
left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = AC.CO_REPARTICAO_CONSULAR
left join TIPO_AUTORIZACAO TA on TA.CO_TIPO_AUTORIZACAO = S.CO_TIPO_AUTORIZACAO
left join PAIS_NACIONALIDADE P on P.CO_PAIS = C.CO_NACIONALIDADE
left join processo_cancel pc on pc.id_solicita_visto = sv.id_solicita_visto and pc.cd_candidato = AC.NU_CANDIDATO and S.ID_TIPO_ACOMPANHAMENTO = 6
where S.ID_TIPO_ACOMPANHAMENTO =6
;


select * from vORDEM_SERVICO_PROTOCOLO where dt_envio_bsb_ger is null