drop view if exists vprocesso_mte;
CREATE VIEW vprocesso_mte AS 
select processo_mte.codigo 
,processo_mte.cd_candidato 
,processo_mte.cd_solicitacao 
,processo_mte.nu_processo 
,date_format(processo_mte.dt_requerimento,_latin1'%d/%m/%Y') AS dt_requerimento
,processo_mte.nu_oficio 
,date_format(processo_mte.dt_deferimento,_latin1'%d/%m/%Y') AS dt_deferimento
,processo_mte.prazo_solicitado 
,processo_mte.cd_funcao 
,processo_mte.cd_reparticao 
,processo_mte.observacao 
,processo_mte.dt_cad 
,processo_mte.dt_ult 
,processo_mte.id_solicita_visto 
,processo_mte.NU_EMPRESA
,processo_mte.NU_EMBARCACAO_PROJETO
,processo_mte.observacao_visto
,processo_mte.fl_visto_atual
,processo_mte.fl_vazio
,processo_mte.nu_servico
,date_format(processo_mte.dt_envio_bsb,_latin1'%d/%m/%Y') AS dt_envio_bsb
,date_format(processo_mte.dt_recebimento_bsb,_latin1'%d/%m/%Y') AS dt_recebimento_bsb
,processo_mte.ID_STATUS_ANDAMENTO 
,processo_mte.ID_STATUS_CONCLUSAO 
,date_format(processo_mte.dt_ult_atu_andamento,_latin1'%d/%m/%Y') AS dt_ult_atu_andamento
,date_format(processo_mte.dt_publicacao_dou,_latin1'%d/%m/%Y') AS dt_publicacao_dou
,processo_mte.fl_ignorar_vencimento
, S.NO_SERVICO_RESUMIDO
, TA.NO_REDUZIDO_TIPO_AUTORIZACAO
, CONCAT(S.NO_SERVICO_RESUMIDO, ' (', ifnull(TA.NO_REDUZIDO_TIPO_AUTORIZACAO, 'n/a'), ')') AS NO_SERVICO_AUTORIZACAO
, E.NO_RAZAO_SOCIAL
, EP.NO_EMBARCACAO_PROJETO
, F.NO_FUNCAO
, RC.NO_REPARTICAO_CONSULAR
, concat(coalesce(concat(substr(replace(replace(replace(nu_processo, '-', ''), '.', ''), '/', '') , 1, 5), '.', substr(replace(replace(replace(nu_processo, '-', ''), '.', ''), '/', ''), 6, 6), '/', substr(replace(replace(replace(nu_processo, '-', ''), '.', ''), '/', ''), 12, 4), '-', substr(replace(replace(replace(nu_processo, '-', ''), '.', ''), '/', ''), 16)), '?????.??????/????-??'), ' - OS ', coalesce(sv.nu_solicitacao, '?????'), ' - prazo solicitado ', coalesce(processo_mte.prazo_solicitado, '??')) as nu_processo_visto
, sv.NU_SERVICO as NU_SERVICO_SV
, sv.nu_solicitacao
, sv.ID_STATUS_SOL
, ss.fl_encerrada
, ss.no_status_sol
, sv.de_observacao
from processo_mte 
left join solicita_visto sv on sv.id_solicita_visto = processo_mte.id_solicita_visto
left join autorizacao_candidato ac on ac.id_solicita_visto = processo_mte.id_solicita_visto and ac.nu_candidato = processo_mte.cd_candidato
left join status_solicitacao ss on ss.id_status_sol = sv.id_status_sol
left join SERVICO S on S.NU_SERVICO = processo_mte.nu_servico
left join TIPO_AUTORIZACAO TA on TA.CO_TIPO_AUTORIZACAO = S.CO_TIPO_AUTORIZACAO
left join EMPRESA E on E.NU_EMPRESA = processo_mte.NU_EMPRESA
left join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = processo_mte.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = processo_mte.NU_EMBARCACAO_PROJETO
left join FUNCAO_CARGO  F on F.CO_FUNCAO = coalesce(processo_mte.cd_funcao, ac.CO_FUNCAO_CANDIDATO)
left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = coalesce(processo_mte.cd_reparticao, ac.CO_REPARTICAO_CONSULAR)
;
