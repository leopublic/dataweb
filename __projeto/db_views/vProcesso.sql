drop view if exists vProcesso;
CREATE VIEW vProcesso AS 
  select 'mte' AS tipo
    ,mte.cd_candidato AS cd_candidato
    ,mte.codigo AS codigo
    ,null as codigo_processo_mte
    ,NULL AS codigo_processo_prorrog
    ,mte.id_solicita_visto AS id_solicita_visto
    ,mte.fl_vazio AS fl_vazio
    ,ordem 
    ,1 AS ordem_temp 
    , dt_cad
    , nu_servico
    , coalesce(dt_requerimento, dt_cad) as dt_ordem
  from processo_mte mte 
 union 
 select 'coleta' AS tipo
    ,col.cd_candidato AS cd_candidato
    ,col.codigo AS codigo
    ,col.codigo_processo_mte AS codigo_processo_mte
    ,NULL AS codigo_processo_prorrog
    ,col.id_solicita_visto AS id_solicita_visto
    ,0 AS fl_vazio
    ,ordem 
    ,2 AS ordem_temp 
    , dt_cad
    , nu_servico
    , null as dt_ordem
  from processo_coleta col 
union 
  select 'regcie' AS tipo
    ,reg.cd_candidato AS cd_candidato
    ,reg.codigo AS codigo
    ,reg.codigo_processo_mte AS codigo_processo_mte
    ,reg.codigo_processo_prorrog AS codigo_processo_prorrog
    ,reg.id_solicita_visto AS id_solicita_visto
    ,reg.fl_vazio AS fl_vazio
    ,ordem 
    ,(case reg.nu_servico when 58 then 6 else 3 end) AS ordem_temp
    , dt_cad
    , nu_servico
    , coalesce(dt_requerimento, dt_cad) as dt_ordem
  from processo_regcie reg 
union 
  select 'emiscie' AS tipo
    ,emi.cd_candidato AS cd_candidato
    ,emi.codigo AS codigo
    ,emi.codigo_processo_mte AS codigo_processo_mte
    ,NULL AS codigo_processo_prorrog
    ,emi.id_solicita_visto AS id_solicita_visto
    ,emi.fl_vazio AS fl_vazio
    ,ordem 
    ,4 AS ordem_temp 
    , dt_cad
    , nu_servico
    , coalesce(dt_emissao, dt_cad) as dt_ordem
  from processo_emiscie emi 
union 
  select 'prorrog' AS tipo
    ,pro.cd_candidato AS cd_candidato
    ,pro.codigo AS codigo
    ,pro.codigo_processo_mte AS codigo_processo_mte
    ,NULL AS codigo_processo_prorrog
    ,pro.id_solicita_visto AS id_solicita_visto
    ,pro.fl_vazio AS fl_vazio
    ,ordem 
    ,5 AS ordem_temp 
    , dt_cad
    , nu_servico
    , coalesce(dt_requerimento, dt_cad) as dt_ordem
  from processo_prorrog pro 
union 
  select 'cancel' AS tipo
    ,can.cd_candidato AS cd_candidato
    ,can.codigo AS codigo
    ,can.codigo_processo_mte AS codigo_processo_mte
    ,NULL AS codigo_processo_prorrog
    ,can.id_solicita_visto AS id_solicita_visto
    ,can.fl_vazio AS fl_vazio
    ,ordem 
    ,7 AS ordem_temp 
    , dt_cad
    , nu_servico
    , coalesce(dt_processo, dt_cad) as dt_ordem
from processo_cancel can;

