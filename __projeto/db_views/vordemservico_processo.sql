drop view if exists vsolicita_visto_processo;
CREATE VIEW vsolicita_visto_processo AS 
select 
 sv.id_solicita_visto 
,sv.NU_SERVICO 
,sv.nu_solicitacao 
,sv.cd_admin_cad 
,sv.ID_STATUS_SOL 
,sv.de_observacao
,S.NO_SERVICO
,S.NO_SERVICO_RESUMIDO 
,S.ID_TIPO_ACOMPANHAMENTO
from solicita_visto sv 
left join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO 

