drop view if exists vprocesso_cancel ;
CREATE VIEW vprocesso_cancel AS 
select processo_cancel.codigo 
,processo_cancel.cd_candidato 
,processo_cancel.cd_solicitacao 
,processo_cancel.nu_processo 
,date_format(processo_cancel.dt_processo,_latin1'%d/%m/%Y') AS dt_processo
,date_format(processo_cancel.dt_cancel,_latin1'%d/%m/%Y') AS dt_cancel
,processo_cancel.observacao 
,processo_cancel.dt_cad 
,processo_cancel.dt_ult 
,processo_cancel.id_solicita_visto 
,processo_cancel.fl_vazio
,processo_cancel.fl_processo_atual
,processo_cancel.codigo_processo_mte
,processo_cancel.nu_servico
,processo_cancel.no_classe
,processo_cancel.no_metodo
,date_format(processo_cancel.dt_envio_bsb,_latin1'%d/%m/%Y') AS dt_envio_bsb
,S.NO_SERVICO
,S.NO_SERVICO_RESUMIDO
,sv.ID_STATUS_SOL
,sv.nu_solicitacao
,sv.de_observacao
from processo_cancel
left join SERVICO S on S.NU_SERVICO = processo_cancel.nu_servico
left join solicita_visto sv ON sv.id_solicita_visto = processo_cancel.id_solicita_visto
;
