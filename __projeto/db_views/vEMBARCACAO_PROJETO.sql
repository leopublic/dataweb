drop view if exists vEMBARCACAO_PROJETO ;
create view vEMBARCACAO_PROJETO as
select 
  EP.NU_EMPRESA
, EP.NU_EMBARCACAO_PROJETO
, EP.NO_EMBARCACAO_PROJETO
, EP.IN_EMBARCACAO_PROJETO
, EP.NO_MUNICIPIO
, EP.CO_UF
, date_format(EP.DT_PRAZO_CONTRATO, '%d/%m/%Y') DT_PRAZO_CONTRATO_FMT
, EP.DT_PRAZO_CONTRATO
, EP.NO_CONTATO
, EP.NO_EMAIL_CONTATO
, EP.NU_TELEFONE_CONTATO
, EP.QT_TRIPULANTES
, EP.QT_TRIPULANTES_ESTRANGEIROS
, EP.DT_CADASTRAMENTO
, date_format(EP.DT_CADASTRAMENTO, '%d/%m/%Y') DT_CADASTRAMENTO_FMT
, EP.CO_USUARIO_CADASTRAMENTO
, EP.NO_CONTRATO
, EP.CO_PAIS
, EP.NU_ARMADOR
, EP.DS_JUSTIFICATIVA
, EP.NO_PREFIXO
, EP.cd_usuario
, EP.ID_TIPO_EMBARCACAO_PROJETO
, EP.TX_OBSERVACAO
, EP.nu_estado
, EP.embp_fl_ativo
, EP.embp_dt_ult_revisao
, date_format(EP.embp_dt_ult_revisao, '%d/%m/%Y') embp_dt_ult_revisao_fmt
, u.nome
, ur.nome nome_ult_revisao
, uc.nome nome_cadastramento
, P.NO_PAIS
, (select count(*) from CANDIDATO C where C.NU_EMPRESA = EP.NU_EMPRESA and C.NU_EMBARCACAO_PROJETO = EP.NU_EMBARCACAO_PROJETO) as QT_CANDIDATOS
, (select count(*) from CANDIDATO C where C.NU_EMPRESA = EP.NU_EMPRESA and C.NU_EMBARCACAO_PROJETO = EP.NU_EMBARCACAO_PROJETO and ifnull(C.FL_INATIVO,0) = 0 ) as QT_CANDIDATOS_ATIVOS
, (select count(*) from CANDIDATO C where C.NU_EMPRESA = EP.NU_EMPRESA and C.NU_EMBARCACAO_PROJETO = EP.NU_EMBARCACAO_PROJETO and C.cand_fl_revisado = 1 and C.DT_CADASTRAMENTO < '2013-01-01' ) as QT_CANDIDATOS_REVISADOS_N2013
from EMBARCACAO_PROJETO EP
left join usuarios u on u.cd_usuario = EP.cd_usuario
left join usuarios ur on ur.cd_usuario = EP.cd_usuario_ult_revisao
left join usuarios uc on uc.cd_usuario = EP.CO_USUARIO_CADASTRAMENTO
left join PAIS_NACIONALIDADE P on P.CO_PAIS = EP.CO_PAIS
;