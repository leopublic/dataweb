drop view if exists vordemservico
;
CREATE VIEW vordemservico AS 
select candidato.NU_CANDIDATO 
	,candidato.NOME_COMPLETO 
	,candidato.NU_CPF 
	,candidato.NU_CTPS 
	,candidato.NU_CNH 
	,candidato.bo_cadastro_minimo_ok
	,date_format(candidato.DT_EXPIRACAO_CNH,'%d/%m/%Y') AS DT_EXPIRACAO_CNH
	,date_format(candidato.DT_EXPIRACAO_CTPS,'%d/%m/%Y') AS DT_EXPIRACAO_CTPS
	,b.ID_AUTORIZACAO_CANDIDATO 
	,b.NU_EMBARCACAO_INICIAL 
	,b.DT_SITUACAO_SOL 
	,b.CD_EMBARCADO 
	,b.autc_fl_revisado
	,b.cd_usuario_revisao
	,date_format(b.DT_CADASTRAMENTO,'%d/%m/%y') AS DT_CADASTRAMENTO
	,sv.NU_EMPRESA 
	,sv.NU_EMBARCACAO_PROJETO 
	,sv.id_solicita_visto 
	,sv.NU_SERVICO 
	,sv.nu_solicitacao 
	,sv.cd_admin_cad 
	,sv.ID_STATUS_SOL 
	,sv.de_observacao 
	,sv.no_solicitador 
	,sv.NU_EMBARCACAO_PROJETO_COBRANCA 
	,sv.tppr_id 
	,sv.soli_dt_devolucao soli_dt_devolucao_orig
	,date_format(sv.soli_dt_devolucao,'%d/%m/%Y') AS soli_dt_devolucao
	,date_format(sv.dt_solicitacao,'%d/%m/%Y') AS dt_solicitacao
	,dt_solicitacao as dt_solicitacao_orig
	,sv.cd_tecnico 
	,ep.NO_EMBARCACAO_PROJETO 
	,ts.CO_TIPO_SERVICO 
	,s.CO_SERVICO 
	,s.NO_SERVICO 
	,s.NO_SERVICO_RESUMIDO 
	,s.ID_TIPO_ACOMPANHAMENTO 
	,s.serv_fl_envio_bsb
	,s.serv_fl_revisao_analistas
	,u.nome AS NO_USUARIO
	,ut.nome AS NO_USUARIO_TECNICO
	,e.NO_RAZAO_SOCIAL 
	,ss.NO_STATUS_SOL_RES 
	,ss.NO_STATUS_SOL 
	,epc.NO_EMBARCACAO_PROJETO AS NO_EMBARCACAO_PROJETO_CAND
	,epcob.NO_EMBARCACAO_PROJETO AS NO_EMBARCACAO_PROJETO_COBRANCA 
	,rc.no_reparticao_consular
	,ur.usua_tx_apelido as nome_revisao
	,ud.usua_tx_apelido as nome_devolucao
	,sc.stco_tx_nome
from solicita_visto sv 
left join autorizacao_candidato b on b.id_solicita_visto = sv.id_solicita_visto
left join candidato on candidato.NU_CANDIDATO = b.NU_CANDIDATO
left join embarcacao_projeto ep on ep.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO and ep.NU_EMPRESA = sv.nu_empresa
left join servico s on s.NU_SERVICO = sv.NU_SERVICO
left join tipo_servico ts on ts.NU_TIPO_SERVICO = s.NU_TIPO_SERVICO
left join usuarios u on u.cd_usuario = sv.cd_admin_cad
left join usuarios ut on ut.cd_usuario = sv.cd_tecnico
left join usuarios ur on ur.cd_usuario = b.cd_usuario_revisao
left join usuarios ud on ud.cd_usuario = sv.cd_usuario_devolucao
left join empresa e on e.NU_EMPRESA = sv.nu_empresa
left join embarcacao_projeto epc on epc.NU_EMBARCACAO_PROJETO = candidato.NU_EMBARCACAO_PROJETO and epc.NU_EMPRESA = candidato.NU_EMPRESA
left join embarcacao_projeto epcob on epcob.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO_COBRANCA and epcob.NU_EMPRESA = sv.nu_empresa
left join status_solicitacao ss on ss.ID_STATUS_SOL = sv.ID_STATUS_SOL
left join status_cobranca_os sc on sc.stco_id = sv.stco_id
left join reparticao_consular rc on rc.co_reparticao_consular = b.co_reparticao_consular
;