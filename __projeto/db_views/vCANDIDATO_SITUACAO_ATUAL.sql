--
-- Retorna os processos mais recentes do candidato, mesmo os orf�os.
drop view if exists vCANDIDATO_SITUACAO_ATUAL ;
create view vCANDIDATO_SITUACAO_ATUAL as
select distinct
  CMTE.NOME_COMPLETO
  ,CMTE.NU_EMPRESA
  ,CMTE.NU_EMBARCACAO_PROJETO
  ,CMTE.NU_CANDIDATO
  ,CMTE.NU_PASSAPORTE
  ,CMTE.NU_RNE
  ,CMTE.nu_processo NU_PROCESSO_MTE
  ,CMTE.prazo_solicitado PRAZO_SOLICITADO
  ,DATE_FORMAT(CMTE.DT_VALIDADE_PASSAPORTE,"%d/%m/%Y") DT_VALIDADE_PASSAPORTE 
  ,DATE_FORMAT(CMTE.dt_deferimento,"%d/%m/%Y") DT_DEFERIMENTO
  ,DATE_FORMAT(CMTE.dt_requerimento  ,"%d/%m/%Y") DT_REQUERIMENTO

  ,EMIS.nu_cie  NU_PROTOCOLO_CIE
  ,DATE_FORMAT(EMIS.dt_emissao,"%d/%m/%Y")  DT_EMISSAO_CIE
  ,datediff(EMIS.dt_validade,now()) DT_VALIDADE_CIE

  ,REG.nu_protocolo  NU_PROTOCOLO_REG
  ,DATE_FORMAT(REG.dt_prazo_estada,"%d/%m/%Y")  DT_PRAZO_ESTADA
  ,datediff(REG.dt_validade,now()) DT_VALIDADE_REG
  ,DATE_FORMAT(REG.dt_requerimento ,"%d/%m/%Y") DT_REQUERIMENTO_CIE

  ,PRO.nu_protocolo NU_PROTOCOLO_PRO
  ,datediff(PRO.dt_validade,now()) DT_VALIDADE_PRO
  ,DATE_FORMAT(PRO.dt_requerimento ,"%d/%m/%Y") DT_REQUERIMENTO_PRO
  ,DATE_FORMAT(PRO.dt_prazo_pret  ,"%d/%m/%Y") DT_PRAZO_PRET_PRO

  ,DATE_FORMAT(AC.DT_ENTRADA ,"%d/%m/%Y") DT_ENTRADA
  ,AC.VA_RENUMERACAO_MENSAL SALARIO_MENSAL
  ,AC.VA_REMUNERACAO_MENSAL_BRASIL SALARIO_BRASIL

  ,TA.NO_REDUZIDO_TIPO_AUTORIZACAO TIPO_VISTO
  ,PN.NO_NACIONALIDADE
  ,RC.NO_CIDADE CONSULADO
  ,F.NO_FUNCAO
          
  from vCANDIDATO_MTE CMTE
  left join processo_prorrog PRO  on PRO.cd_candidato   = CMTE.NU_CANDIDATO and ((PRO.codigo_processo_mte  = CMTE.codigo) or (PRO.codigo_processo_mte  is null and PRO.id_solicita_visto is not null))
  left join processo_regcie REG   on REG.cd_candidato   = CMTE.NU_CANDIDATO and ((REG.codigo_processo_mte  = CMTE.codigo) or (REG.codigo_processo_mte  is null and REG.id_solicita_visto is not null))
  left join processo_emiscie EMIS on EMIS.cd_candidato  = CMTE.NU_CANDIDATO and ((EMIS.codigo_processo_mte = CMTE.codigo) or (EMIS.codigo_processo_mte is null and EMIS.id_solicita_visto is not null))
  left join processo_cancel CANC  on CANC.cd_candidato  = CMTE.NU_CANDIDATO and ((CANC.codigo_processo_mte = CMTE.codigo) or (CANC.codigo_processo_mte is null and CANC.id_solicita_visto is not null))
  left join PAIS_NACIONALIDADE PN on CMTE.CO_NACIONALIDADE = PN.CO_PAIS 
  left join FUNCAO_CARGO F on F.CO_FUNCAO = CMTE.cd_funcao
  left join AUTORIZACAO_CANDIDATO AC 
         ON AC.NU_CANDIDATO = CMTE.NU_CANDIDATO and AC.NU_SOLICITACAO = (select max(NU_SOLICITACAO) from AUTORIZACAO_CANDIDATO where NU_CANDIDATO = CMTE.NU_CANDIDATO)
  LEFT JOIN SERVICO S
         ON AC.CO_TIPO_AUTORIZACAO = S.CO_TIPO_AUTORIZACAO
  LEFT JOIN TIPO_AUTORIZACAO TA
         ON S.CO_TIPO_AUTORIZACAO = TA.CO_TIPO_AUTORIZACAO
  LEFT JOIN REPARTICAO_CONSULAR RC
         ON CMTE.cd_reparticao = RC.CO_REPARTICAO_CONSULAR 
;
