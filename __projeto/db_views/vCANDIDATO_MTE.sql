-- ======================================================================
-- View que relaciona o candidato ao seu processo MTE mais atual
-- ======================================================================
drop view if exists vCANDIDATO_MTE;
create  view vCANDIDATO_MTE as
select * 
  from CANDIDATO C    
  left join processo_mte MTE on 
      MTE.cd_candidato = C.NU_CANDIDATO 
      and MTE.codigo = (select max(codigo) 
                        from processo_mte mteI
                        left join solicita_visto sv on sv.NU_SOLICITACAO = mteI.cd_solicitacao
                        left join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO
                        where mteI.cd_candidato = MTE.cd_candidato
                        and ((mteI.cd_solicitacao < 5698 and S.ID_TIPO_ACOMPANHAMENTO is null) 
                              or (mteI.cd_solicitacao <  5698 and S.ID_TIPO_ACOMPANHAMENTO = 1) 
                              or (mteI.cd_solicitacao >= 5698 and S.ID_TIPO_ACOMPANHAMENTO = 1) )
                        and mteI.cd_solicitacao >=0
                      )
;
