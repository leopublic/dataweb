drop view if exists vusuarios_produtividade
;
create view vusuarios_produtividade as 
select distinct cd_usuario_devolucao cd_usuario 
	from solicita_visto , servico
	where cd_usuario_devolucao is not null 
	and servico.nu_servico = solicita_visto.nu_servico
	and id_tipo_acompanhamento in (1,3)
	and soli_dt_devolucao is not null 
	and soli_dt_devolucao  > '2013-01-01'
union distinct 
select distinct cd_usuario_pre_cadastro cd_usuario 
	from processo_prorrog
	where cd_usuario_pre_cadastro is not null 
and processo_prorrog.nu_pre_cadastro is not null
and convert(nu_pre_cadastro USING latin1) <> CONVERT('(já realizado)' using latin1)
and dt_pre_cadastro  > '2013-01-01'
and dt_pre_cadastro is not null
;
