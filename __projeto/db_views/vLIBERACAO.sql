drop view if exists vLiberacao;
create view vLiberacao as 
select id_solicita_visto
  , date_format(dt_liberacao, '%d/%m/%Y' ) dt_liberacao
  , cd_usuario_qualidade
  , cd_usuario_liberacao
  , de_comentarios_liberacao
  , fl_liberado
  , fl_conclusao_conforme
  , uq.nome nm_usuario_qualidade
  , ul.nome nm_usuario_liberacao
  , solicita_visto.fl_liberacao_fechada
  , solicita_visto.fl_qualidade_fechada
from solicita_visto
left join usuarios uq on uq.cd_usuario = solicita_visto.cd_usuario_qualidade
left join usuarios ul on ul.cd_usuario = solicita_visto.cd_usuario_liberacao
;