drop view if exists vprocesso_regcie;
CREATE VIEW vprocesso_regcie AS 
select processo_regcie.codigo 
,processo_regcie.cd_candidato 
,processo_regcie.cd_solicitacao 
,processo_regcie.nu_protocolo 
,date_format(processo_regcie.dt_requerimento,'%d/%m/%Y') AS dt_requerimento
,date_format(processo_regcie.dt_validade,'%d/%m/%Y') AS dt_validade
,date_format(processo_regcie.dt_prazo_estada,'%d/%m/%Y') AS dt_prazo_estada
,processo_regcie.observacao 
,processo_regcie.dt_cad 
,processo_regcie.dt_ult 
,processo_regcie.id_solicita_visto 
,processo_regcie.fl_vazio
,processo_regcie.fl_processo_atual
,processo_regcie.codigo_processo_mte
,processo_regcie.codigo_processo_prorrog
,processo_regcie.nu_servico
,processo_regcie.no_classe
,processo_regcie.no_metodo
,S.NO_SERVICO
,S.NO_SERVICO_RESUMIDO
,S.ID_TIPO_ACOMPANHAMENTO
,sv.ID_STATUS_SOL
,sv.NU_SERVICO NU_SERVICO_SV
,sv.nu_solicitacao
,sv.de_observacao
,concat(depf_no_nome, ' (', depf_cd_uf, ')') depf_no_nome
from processo_regcie
left join SERVICO S on S.NU_SERVICO = processo_regcie.nu_servico
left join solicita_visto sv ON sv.id_solicita_visto = processo_regcie.id_solicita_visto
LEFT JOIN delegacia_pf d on d.depf_cd_codigo = substr(trim(replace(processo_regcie.nu_protocolo, '''', '')),1,5)
;

