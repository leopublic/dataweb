drop view if exists vprocesso_emiscie ;
CREATE VIEW vprocesso_emiscie AS 
select processo_emiscie.codigo 
,processo_emiscie.cd_candidato 
,processo_emiscie.cd_solicitacao 
,processo_emiscie.nu_cie 
,date_format(processo_emiscie.dt_emissao,_latin1'%d/%m/%Y') AS dt_emissao
,date_format(processo_emiscie.dt_validade,_latin1'%d/%m/%Y') AS dt_validade
,processo_emiscie.observacao
,processo_emiscie.dt_cad
,processo_emiscie.dt_ult
,processo_emiscie.id_solicita_visto 
,processo_emiscie.codigo_processo_mte
,processo_emiscie.fl_vazio
,processo_emiscie.fl_processo_atual
,processo_emiscie.nu_servico
,processo_emiscie.no_classe
,processo_emiscie.no_metodo
,processo_emiscie.CO_CLASSIFICACAO_VISTO
,S.NO_SERVICO
,S.NO_SERVICO_RESUMIDO
,sv.ID_STATUS_SOL
,sv.nu_solicitacao
,cv.NO_CLASSIFICACAO_VISTO
,sv.de_observacao
from processo_emiscie
left join SERVICO S on S.NU_SERVICO = processo_emiscie.nu_servico
left join solicita_visto sv ON sv.id_solicita_visto = processo_emiscie.id_solicita_visto
left join CLASSIFICACAO_VISTO CV ON CV.CO_CLASSIFICACAO_VISTO = processo_emiscie.CO_CLASSIFICACAO_VISTO
;

