drop view if exists vcandidato_processo ;
create view vcandidato_processo as 
   select 
    C.NU_CANDIDATO
	  C.NOME_COMPLETO,
	  C.NU_PASSAPORTE,
	  C.NU_RNE,
	  C.DT_VALIDADE_PASSAPORTE DT_VALIDADE_PASSAPORTE_ORIG ,
	  DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE ,
	  DATE_FORMAT(MAX_PRAZO_ESTADA.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA,
	  datediff(MAX_PRAZO_ESTADA.dt_prazo_estada, now()) DIAS_VENCIMENTO,
	  ifnull(QTD_PRORROG.qtd_prorrog, 0) QTD_PRORROG,

	  DATE_FORMAT(REG.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_CIE,
	  REG.nu_protocolo NU_PROTOCOLO_REG,

	  DATE_FORMAT(PRO.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_PRO,
	  PRO.nu_protocolo NU_PROTOCOLO_PRO,
	  DATE_FORMAT(PRO.dt_prazo_pret,'%d/%m/%Y')  DT_PRAZO_PRET_PRO,
	  sv.ID_STATUS_SOL,
	  sv.nu_solicitacao NU_SOLICITACAO_PRO,
	  SS.NO_STATUS_SOL,

	  MTE.nu_processo NU_PROCESSO_MTE,
	  MTE.prazo_solicitado PRAZO_SOLICITADO,
	  DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO ,
	  DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO,

	  DATE_FORMAT(COLETA.dt_entrada,'%d/%m/%Y')  DT_ENTRADA,
	  PN.NO_NACIONALIDADE,
	  F.NO_FUNCAO,
	  S.NO_SERVICO_RESUMIDO TIPO_VISTO,
	  RC.NO_REPARTICAO_CONSULAR CONSULADO
	from CANDIDATO C
	  left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual
		  and MTE.cd_candidato = C.NU_CANDIDATO
		  and MTE.fl_vazio = 0
	  left join (select cd_candidato, codigo_processo_mte, count(*) qtd_prorrog from processo_prorrog where codigo_processo_mte is not null group by cd_candidato, codigo_processo_mte) as QTD_PRORROG
		  on QTD_PRORROG.codigo_processo_mte = MTE.codigo
		  and QTD_PRORROG.cd_candidato = MTE.cd_candidato
	  left join (select cd_candidato, codigo_processo_mte, max(dt_prazo_estada) dt_prazo_estada from processo_regcie
				  where codigo_processo_mte is not null
				  group by cd_candidato, codigo_processo_mte) as MAX_PRAZO_ESTADA
		  on MAX_PRAZO_ESTADA.codigo_processo_mte = MTE.codigo
		  and MAX_PRAZO_ESTADA.cd_candidato = MTE.cd_candidato
	  left join processo_regcie REG
		  on REG.codigo_processo_mte = MTE.codigo
		  and REG.cd_candidato = MTE.cd_candidato
		  and REG.dt_prazo_estada = MAX_PRAZO_ESTADA.dt_prazo_estada
	  left join (select cd_candidato, codigo_processo_mte, max(dt_requerimento) dt_requerimento from processo_prorrog where codigo_processo_mte is not null group by cd_candidato, codigo_processo_mte) as MAX_PRORROG
		  on MAX_PRORROG.codigo_processo_mte = MTE.codigo
		  and MAX_PRORROG.cd_candidato = MTE.cd_candidato
		  and MAX_PRORROG.dt_requerimento > REG.dt_requerimento
	  left join processo_prorrog PRO
		  on PRO.codigo_processo_mte = MTE.codigo
		  and PRO.cd_candidato = MTE.cd_candidato
		  and PRO.dt_requerimento = MAX_PRORROG.dt_requerimento
	  left join solicita_visto sv on sv.id_solicita_visto = PRO.id_solicita_visto
	  left join processo_coleta COLETA  on COLETA.cd_candidato = C.NU_CANDIDATO
		  and COLETA.codigo_processo_mte = MTE.codigo
	  left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
	  left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
	  left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
	  left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = MTE.cd_reparticao
	  left join STATUS_SOLICITACAO SS ON SS.ID_STATUS_SOL = sv.ID_STATUS_SOL
;