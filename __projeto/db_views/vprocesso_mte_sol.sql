drop view if exists vprocesso_mte_sol;
CREATE VIEW vprocesso_mte_sol AS 
select pm.codigo
,pm.cd_candidato 
,pm.cd_solicitacao 
,pm.nu_processo 
,date_format(pm.dt_requerimento,_latin1'%d/%m/%Y') AS dt_requerimento
,pm.nu_oficio 
,date_format(pm.dt_deferimento,_latin1'%d/%m/%Y') AS dt_deferimento
,pm.prazo_solicitado
,pm.cd_funcao
,pm.cd_reparticao 
,pm.observacao 
,pm.dt_cad 
,pm.dt_ult 
,pm.id_solicita_visto
,date_format(pm.dt_envio_bsb,_latin1'%d/%m/%Y') AS dt_envio_bsb
,date_format(pm.dt_recebimento_bsb,_latin1'%d/%m/%Y') AS dt_recebimento_bsb
,pm.ID_STATUS_ANDAMENTO 
,pm.ID_STATUS_CONCLUSAO
,date_format(pm.dt_ult_atu_andamento,_latin1'%d/%m/%Y') AS dt_ult_atu_andamento
,date_format(pm.dt_publicacao_dou,_latin1'%d/%m/%Y') AS dt_publicacao_dou
,pm.no_classe
,pm.no_metodo
,fc.NO_FUNCAO 
,fc.NO_FUNCAO_EM_INGLES 
,rc.NO_REPARTICAO_CONSULAR 
,epa.NO_EMBARCACAO_PROJETO 
,e.NO_RAZAO_SOCIAL 
, S.NO_SERVICO_RESUMIDO
, TA.NO_REDUZIDO_TIPO_AUTORIZACAO
, sv.NU_SERVICO
from processo_mte pm 
left join FUNCAO_CARGO fc on fc.CO_FUNCAO = pm.cd_funcao
left join solicita_visto sv on sv.id_solicita_visto = pm.id_solicita_visto
left join AUTORIZACAO_CANDIDATO ac on ac.id_solicita_visto = pm.id_solicita_visto and ac.NU_CANDIDATO = pm.cd_candidato 
left join EMBARCACAO_PROJETO epa on epa.NU_EMBARCACAO_PROJETO = pm.NU_EMBARCACAO_PROJETO and epa.NU_EMPRESA = pm.NU_EMPRESA 
left join EMPRESA e on e.NU_EMPRESA = pm.nu_empresa 
left join REPARTICAO_CONSULAR rc on rc.CO_REPARTICAO_CONSULAR = ac.CO_REPARTICAO_CONSULAR
left join TIPO_AUTORIZACAO TA on TA.CO_TIPO_AUTORIZACAO = ac.CO_TIPO_AUTORIZACAO
left join SERVICO S on S.NU_SERVICO = pm.nu_servico;
