delimiter ; 
drop trigger if exists autorizacao_candidato_registra_log ;
delimiter $$
CREATE TRIGGER autorizacao_candidato_registra_log BEFORE UPDATE ON autorizacao_candidato
  FOR EACH ROW
  BEGIN
    DECLARE alterou tinyint;
	DECLARE msg varchar(2000);
	DECLARE virgula varchar(2);
	DECLARE x_desc varchar(1000);
	DECLARE x_old_desc varchar(1000);

	SET msg = '';
	SET virgula = '';

	call alterou_campo_string(msg, OLD.NO_JUSTIFICATIVA_REP_CONS, NEW.NO_JUSTIFICATIVA_REP_CONS, 'justificativa da reparti��o de', 'para');
	call alterou_campo_string(msg, OLD.VA_RENUMERACAO_MENSAL, NEW.VA_RENUMERACAO_MENSAL, 'sal�rio no exterior de', 'para');
	call alterou_campo_string(msg, OLD.VA_REMUNERACAO_MENSAL_BRASIL, NEW.VA_REMUNERACAO_MENSAL_BRASIL, 'sal�rio no Brasil de', 'para');
	call alterou_campo_string(msg, OLD.DS_LOCAL_TRABALHO, NEW.DS_LOCAL_TRABALHO, 'local de trabalho de', 'para');
	call alterou_campo_string(msg, OLD.TE_DESCRICAO_ATIVIDADES, NEW.TE_DESCRICAO_ATIVIDADES, 'descri��o de atividades de', 'para');
	call alterou_campo_string(msg, OLD.DS_JUSTIFICATIVA, NEW.DS_JUSTIFICATIVA, 'justificativa de', 'para');
	call alterou_campo_string(msg, OLD.DS_SALARIO_EXTERIOR, NEW.DS_SALARIO_EXTERIOR, 'declara��o de remunera��o de', 'para');
	call alterou_campo_string(msg, OLD.autc_fl_revisado, NEW.autc_fl_revisado, 'situa��o de revis�o de', 'para');

	IF msg<> '' THEN
		SET virgula = ', ';
	END IF;

    IF ifnull(NEW.CO_REPARTICAO_CONSULAR, 0) <> ifnull(OLD.CO_REPARTICAO_CONSULAR, 0) THEN 
      SET alterou = 1;
	  select NO_REPARTICAO_CONSULAR into x_desc from reparticao_consular where co_reparticao_consular = NEW.CO_REPARTICAO_CONSULAR;
	  select NO_REPARTICAO_CONSULAR into x_old_desc from reparticao_consular where co_reparticao_consular  = OLD.CO_REPARTICAO_CONSULAR;

 	  SET msg = concat(msg, virgula, ' reparti��o consular de "', ifnull(x_old_desc, '(nulo)'), '" para "', ifnull(x_desc, '(nulo)'), '"');
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.CO_FUNCAO_CANDIDATO, 0) <> ifnull(OLD.CO_FUNCAO_CANDIDATO, 0) THEN 
      SET alterou = 1;
	  select NO_FUNCAO into x_desc from funcao_cargo where CO_FUNCAO = NEW.CO_FUNCAO_CANDIDATO;
	  select NO_FUNCAO into x_old_desc from funcao_cargo where CO_FUNCAO = OLD.CO_FUNCAO_CANDIDATO;

 	  SET msg = concat(msg, virgula, ' fun��o de "', ifnull(x_old_desc, '(nulo)'), '" para "', ifnull(x_desc, '(nulo)'), '"');
	  SET virgula = ', ';
    END IF;

    IF msg <> '' THEN
		select nome_completo into x_desc from candidato where nu_candidato = NEW.nu_candidato;

        SET msg = concat('Altera��es no cadastro do candidato "', x_desc, '": ', msg);
		insert into historico_os(id_solicita_visto, nu_candidato, hios_dt_evento, cd_usuario, hios_tx_observacoes) values (NEW.id_solicita_visto, NEW.nu_candidato, now(), NEW.NU_USUARIO_CAD, msg);
    END IF;
  END;
$$
