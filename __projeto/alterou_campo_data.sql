delimiter ; 
drop procedure if exists alterou_campo_data ;
delimiter $$
CREATE procedure alterou_campo_data (msg_ant varchar(5000), new_campo date, old_campo date, coment_antes varchar(1000), coment_depois varchar(1000) )
BEGIN
    IF ifnull(new_campo, '') <> ifnull(old_campo, '') THEN 
	  SET msg_ant = concat(coment_antes , ifnull(old_campo, '(nulo)'), coment_depois, ifnull(new_campo, '(nulo)'));
    END IF;
END
