drop trigger if exists processo_regcie_registra_log ;
CREATE TRIGGER processo_regcie_registra_log BEFORE UPDATE ON processo_regcie
  FOR EACH ROW
  BEGIN
    DECLARE alterou tinyint;
	DECLARE msg varchar(2000);
	DECLARE virgula varchar(2);
	DECLARE no_servico_resumido varchar(500);
	DECLARE old_no_servico_resumido varchar(500);
    SET alterou = 0;
	SET msg = '';
	SET virgula = '';
    IF NEW.cd_candidato <> OLD.cd_candidato THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' migrou do candidato ', OLD.cd_candidato, ' para o candidato ', cd_candidato);
	  SET virgula = ', ';
    END IF;
    IF NEW.nu_protocolo <> OLD.nu_protocolo THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' n�mero do protocolo de ', OLD.nu_protocolo, ' para ', nu_protocolo);
	  SET virgula = ', ';
    END IF;
    IF NEW.dt_requerimento <> OLD.dt_requerimento THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' data requerimento de ', OLD.dt_requerimento, ' para ', dt_requerimento);
	  SET virgula = ', ';
    END IF;
    IF NEW.dt_validade <> OLD.dt_validade THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' data de validade de ', OLD.dt_validade, ' para ', dt_validade);
	  SET virgula = ', ';
    END IF;
    IF NEW.dt_prazo_pret <> OLD.dt_prazo_pret THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' prazo pretendido de ', OLD.dt_prazo_pret, ' para ', dt_prazo_pret);
	  SET virgula = ', ';
    END IF;
    IF NEW.observacao <> OLD.observacao THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' observacao de ', OLD.observacao, ' para ', observacao);
	  SET virgula = ', ';
    END IF;
    IF NEW.id_solicita_visto <> OLD.id_solicita_visto THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' OS de ', OLD.id_solicita_visto, ' para ', id_solicita_visto);
	  SET virgula = ', ';
    END IF;
    IF NEW.codigo_processo_mte <> OLD.codigo_processo_mte THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' migrou do visto ', OLD.codigo_processo_mte, ' para o visto ', codigo_processo_mte);
	  SET virgula = ', ';
    END IF;
    IF NEW.fl_vazio <> OLD.fl_vazio THEN 
      SET alterou = 1;
    END IF;
    IF NEW.codigo_processo_prorrog <> OLD.codigo_processo_prorrog THEN 
      SET alterou = 1;
    END IF;
    IF NEW.nu_servico <> OLD.nu_servico THEN 
      SET alterou = 1;
	  select no_servico_resumido = no_servico_resumido from servico where nu_servico = nu_servico;
	  select old_no_servico_resumido = no_servico_resumido from servico where nu_servico = OLD.nu_servico;

	  SET msg = concat(msg, virgula, ' alterou do servi�o ', old_nu_servico_resumido, ' para ', no_servico_resumido);
	  SET virgula = ', ';
    END IF;
    
    IF alterou = 1 THEN
		insert into historico_processo_prorrog(codigo, cd_usuario, hpro_tx_observacao) values (codigo, cd_usuario, msg);
    END IF;
  END;
