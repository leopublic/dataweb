	use dataweb;
	-- Trigger DDL Statements
	DELIMITER $$
	drop trigger if exists usuarios_registra_log_insert; $$
	CREATE
	TRIGGER usuarios_registra_log_insert
		after INSERT ON usuarios
		FOR EACH ROW
		BEGIN
			insert into dataweb_log.usuarios_log (
				cd_usuario
				,cd_perfil
				,cd_empresa
				,login
				,nm_senha
				,nome
				,nm_email
				,cd_superior
				,dt_cadastro
				,dt_ult
				,flagativo
				,usua_tx_ip_ult_acesso
				,usua_dt_ult_acesso
				,cd_usuario_log
				,log_tx_controller
				,log_tx_metodo
				,log_dt_evento
				,log_tx_evento
			)
			values (
				NEW.cd_usuario
				,NEW.cd_perfil
				,NEW.cd_empresa
				,NEW.login
				,NEW.nm_senha
				,NEW.nome
				,NEW.nm_email
				,NEW.cd_superior
				,NEW.dt_cadastro
				,NEW.dt_ult
				,NEW.flagativo
				,NEW.usua_tx_ip_ult_acesso
				,NEW.usua_dt_ult_acesso
				,NEW.cd_usuario_log
				,NEW.log_tx_controller
				,NEW.log_tx_metodo
				,now()
				,'insert'
			);
		END$$


