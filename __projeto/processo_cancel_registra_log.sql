delimiter ; 
drop trigger if exists processo_cancel_registra_log ;
delimiter $$
CREATE TRIGGER processo_cancel_registra_log BEFORE UPDATE ON processo_cancel
  FOR EACH ROW
  BEGIN
    DECLARE alterou tinyint;
  	DECLARE msg varchar(2000);
  	DECLARE virgula varchar(2);
    DECLARE x_desc varchar(1000);
    DECLARE x_old_desc varchar(1000);
    
    SET alterou = 0;
  	SET msg = '';
  	SET virgula = '';

    IF ifnull(NEW.cd_candidato, '') <> ifnull(OLD.cd_candidato, '') THEN 
      SET alterou = 1;
  	  SET msg = concat(msg, virgula, ' migrou do candidato ', ifnull(OLD.cd_candidato, '(nulo)'), ' para o candidato ', ifnull(NEW.cd_candidato, '(nulo)'));
  	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_processo, '') <> ifnull(OLD.nu_processo, '') THEN 
      SET alterou = 1;
  	  SET msg = concat(msg, virgula, ' n�mero do protocolo de ', ifnull(OLD.nu_processo, '(nulo)'), ' para ', ifnull(NEW.nu_processo, '(nulo)'));
  	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_processo, '') <> ifnull(OLD.dt_processo, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data requerimento de ', ifnull(OLD.dt_processo, '(nulo)'), ' para ', ifnull(NEW.dt_processo, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_cancel, '') <> ifnull(OLD.dt_cancel, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data de cancelamento ', ifnull(OLD.dt_cancel, '(nulo)'), ' para ', ifnull(NEW.dt_cancel, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.observacao, '') <> ifnull(OLD.observacao, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' observa��o de ', ifnull(OLD.observacao, '(nulo)'), ' para ', ifnull(NEW.observacao, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.id_solicita_visto, '') <> ifnull(OLD.id_solicita_visto, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' OS de ', ifnull(OLD.id_solicita_visto, '(nulo)'), ' para ', ifnull(NEW.id_solicita_visto, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.codigo_processo_mte, '') <> ifnull(OLD.codigo_processo_mte, '') THEN 
      SET alterou = 1;
    SET msg = concat(msg, virgula, ' migrou do visto ', ifnull(OLD.codigo_processo_mte, '(nulo)'), ' para o visto ', ifnull(NEW.codigo_processo_mte, '(nulo)'));
    SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_envio_bsb, '') <> ifnull(OLD.dt_envio_bsb, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' envio BSB de ', ifnull(OLD.dt_envio_bsb, '(nulo)'), ' para ', ifnull(NEW.dt_envio_bsb, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_servico, 0) <> ifnull(OLD.nu_servico, 0) THEN 
      SET alterou = 1;
  	  select no_servico_resumido into x_desc from servico where nu_servico = NEW.nu_servico;
  	  select no_servico_resumido into x_old_desc  from servico where nu_servico = OLD.nu_servico;

   	  SET msg = concat(msg, virgula, ' do servi�o ', ifnull(x_old_desc, '(nulo)'), ' para ', ifnull(x_desc, '(nulo)'));
  	  SET virgula = ', ';
    END IF;

    IF msg <> '' THEN
      SET msg = concat('Altera��es:', msg);
    END IF;

    IF alterou = 1 THEN
		  insert into historico_processo_cancel(codigo, cd_usuario, hcan_tx_observacao) values (NEW.codigo, NEW.cd_usuario, msg);
    END IF;
  END;
$$
