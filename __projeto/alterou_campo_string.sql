delimiter ; 
drop procedure if exists alterou_campo_string ;
delimiter $$
CREATE procedure alterou_campo_string (
    INOUT msg text
    , old_campo varchar(5000)
	, new_campo varchar(5000)
    , coment_antes varchar(1000)
    , coment_depois varchar(1000) 
)
BEGIN
	DECLARE virgula varchar(2);
	IF trim(msg) = '' THEN
		SET virgula = '';
	ELSE
		SET virgula = ', ';
	END IF;

    IF ifnull(new_campo, '') <> ifnull(old_campo, '') THEN 
	  SET msg = concat(msg, virgula, ' ', coment_antes , ' "', ifnull(old_campo, '(nulo)'), '" ', coment_depois, ' "', ifnull(new_campo, '(nulo)'), '"');
    END IF;
END
$$
delimiter ;