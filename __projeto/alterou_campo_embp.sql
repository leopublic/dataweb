delimiter ; 
drop procedure if exists alterou_campo_embp ;
delimiter $$
CREATE procedure alterou_campo_embp (msg_ant varchar(5000), new_campo int, old_campo int, coment_antes varchar(1000), coment_depois varchar(1000) )
BEGIN
	DECLARE x_desc		varchar(1000);
	DECLARE x_old_desc	varchar(1000);

    IF ifnull(new_campo, '') <> ifnull(old_campo, '') THEN 
	  select no_embarcaca into x_desc from empresa where nu_empresa = NEW.nu_empresa;
	  select no_razao_social into x_old_desc from empresa where nu_empresa = OLD.nu_empresa;

	  SET msg_ant = concat(coment_antes , ifnull(old_campo, '(nulo)'), coment_depois, ifnull(new_campo, '(nulo)'));
    END IF;
END
