DROP PROCEDURE IF EXISTS ordem_servico_juntar;
CREATE PROCEDURE ordem_servico_juntar (pnu_solicitacao_origem int, pnu_solicitacao_destino int)
begin
    declare xid_solicita_visto_origem int;
    declare xid_solicita_visto_destino int;
    
    declare xnu_empresa_origem int;
    declare xnu_empresa_destino int;
    declare xnu_embarcacao_projeto_origem int;
    declare xnu_embarcacao_destino_origem int;

    declare xdt_prazo_estada_solicitado  varchar(20);
    --
    -- Obtem os ids da origem e do destino
    select id_solicita_visto into xid_solicita_visto_origem 
        from solicita_visto 
        where NU_SOLICITACAO = pnu_solicitacao_origem;
        
    select id_solicita_visto into xid_solicita_visto_destino 
        from solicita_visto 
        where NU_SOLICITACAO = pnu_solicitacao_destino;
    
    select max(DT_PRAZO_ESTADA_SOLICITADO) into xdt_prazo_estada_solicitado  
        from AUTORIZACAO_CANDIDATO
        where id_solicita_visto = xid_solicita_visto_destino;
    --
    -- Migra os dados
    update AUTORIZACAO_CANDIDATO 
    set id_solicita_visto = xid_solicita_visto_destino 
        , NU_SOLICITACAO = pnu_solicitacao_destino 
        , DT_PRAZO_ESTADA_SOLICITADO = xdt_prazo_estada_solicitado
    where id_solicita_visto = xid_solicita_visto_origem;
       
    update processo_mte 
    set id_solicita_visto = xid_solicita_visto_destino 
        , cd_solicitacao = pnu_solicitacao_destino
    where id_solicita_visto = xid_solicita_visto_origem;

    update processo_cancel 
    set id_solicita_visto = xid_solicita_visto_destino 
        , cd_solicitacao = pnu_solicitacao_destino
    where id_solicita_visto = xid_solicita_visto_origem;

    update processo_emiscie
    set id_solicita_visto = xid_solicita_visto_destino 
        , cd_solicitacao = pnu_solicitacao_destino
    where id_solicita_visto = xid_solicita_visto_origem;

    update processo_prorrog
    set id_solicita_visto = xid_solicita_visto_destino 
        , cd_solicitacao = pnu_solicitacao_destino
    where id_solicita_visto = xid_solicita_visto_origem;

    update processo_regcie
    set id_solicita_visto = xid_solicita_visto_destino 
        , cd_solicitacao = pnu_solicitacao_destino
    where id_solicita_visto = xid_solicita_visto_origem;

    update ARQUIVOS
    set ID_SOLICITA_VISTO = xid_solicita_visto_destino 
    where ID_SOLICITA_VISTO = xid_solicita_visto_origem;

    update ARQUIVOS_FORMULARIOS
    set id_solicita_visto = xid_solicita_visto_destino 
    where id_solicita_visto = xid_solicita_visto_origem;
    
    delete from solicita_visto where id_solicita_visto = xid_solicita_visto_origem;
end;
