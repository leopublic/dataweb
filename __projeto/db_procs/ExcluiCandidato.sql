delimiter //
CREATE PROCEDURE ExcluiCandidato (xnu_candidato INT)
BEGIN
	delete from processo_regcie where cd_candidato = xnu_candidato;
	delete from processo_prorrog where cd_candidato = xnu_candidato;
	delete from processo_emiscie where cd_candidato = xnu_candidato;
	delete from processo_cancel where cd_candidato = xnu_candidato;
	delete from processo_mte where cd_candidato = xnu_candidato;
	delete from autorizacao_candidato where nu_candidato = xnu_candidato;
	delete from arquivos where nu_candidato = xnu_candidato;
	delete from taxapaga where nu_candidato = xnu_candidato;
	delete from documento where nu_candidato = xnu_candidato;
	delete from form_tvs where nu_candidato = xnu_candidato;
	delete from candidato where nu_candidato = xnu_candidato;
END//
