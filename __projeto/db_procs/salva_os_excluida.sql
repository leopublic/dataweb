delimiter $
DROP PROCEDURE IF EXISTS dataweb.salva_os_excluida$
CREATE PROCEDURE dataweb.salva_os_excluida(IN p_id_solicita_visto int (11), p_cd_usuario int(11) )
BEGIN
  delete from dataweb_log.solicita_visto where id_solicita_visto = p_id_solicita_visto;
  delete from dataweb_log.autorizacao_candidato where id_solicita_visto = p_id_solicita_visto;
  insert into dataweb_log.solicita_visto (
                    nu_solicitacao,
                    cd_libs_finan,
                    nu_empresa,
                    no_solicitador,
                    dt_solicitacao,
                    cd_admin_cad,
                    dt_cadastro,
                    cd_admin_fim,
                    dt_finalizacao,
                    nm_arquivo_sol,
                    de_observacao,
                    id_solicita_visto,
                    NU_SERVICO,
                    nu_empresa_requerente,
                    dt_envio_bsb,
                    dt_recebimento_bsb,
                    fl_cobrado,
                    fl_nao_conformidade,
                    ID_STATUS_SOL,
                    fl_sistema_antigo,
                    dt_liberacao,
                    cd_usuario_qualidade,
                    fl_conclusao_conforme,
                    fl_liberado,
                    de_comentarios_liberacao,
                    cd_usuario_liberacao,
                    fl_liberacao_fechada,
                    fl_qualidade_fechada,
                    NU_EMBARCACAO_PROJETO,
                    stco_id,
                    resc_id,
                    soli_vl_cobrado,
                    soli_tx_obs_cobranca,
                    soli_dt_liberacao_cobranca,
                    soli_dt_cobranca,
                    cd_usuario_liberacao_cobranca,
                    cd_usuario_cobranca,
                    NU_EMPRESA_COBRANCA,
                    NU_EMBARCACAO_PROJETO_COBRANCA,
                    tbpc_id,
                    tppr_id,
                    cd_usuario_analista,
                    soli_tx_codigo_servico,
                    soli_tx_solicitante_cobranca,
                    cd_usuario_alteracao,
                    soli_dt_alteracao,
                    soli_tx_descricao_servico,
                    hios_id_atual,
                    soli_dt_devolucao,
                    cd_usuario_devolucao,
                    de_observacao_bsb,
                    soli_qt_autenticacoes,
                    id_solicita_visto_pacote,
                    nu_servico_pacote,
                    soli_tx_justificativa,
                    id_tipo_envio,
                    dt_exclusao,
                    cd_usuario_exclusao,
                    cd_tecnico)
                SELECT 
                    nu_solicitacao,
                    cd_libs_finan,
                    nu_empresa,
                    no_solicitador,
                    dt_solicitacao,
                    cd_admin_cad,
                    dt_cadastro,
                    cd_admin_fim,
                    dt_finalizacao,
                    nm_arquivo_sol,
                    de_observacao,
                    id_solicita_visto,
                    NU_SERVICO,
                    nu_empresa_requerente,
                    dt_envio_bsb,
                    dt_recebimento_bsb,
                    fl_cobrado,
                    fl_nao_conformidade,
                    ID_STATUS_SOL,
                    fl_sistema_antigo,
                    dt_liberacao,
                    cd_usuario_qualidade,
                    fl_conclusao_conforme,
                    fl_liberado,
                    de_comentarios_liberacao,
                    cd_usuario_liberacao,
                    fl_liberacao_fechada,
                    fl_qualidade_fechada,
                    NU_EMBARCACAO_PROJETO,
                    stco_id,
                    resc_id,
                    soli_vl_cobrado,
                    soli_tx_obs_cobranca,
                    soli_dt_liberacao_cobranca,
                    soli_dt_cobranca,
                    cd_usuario_liberacao_cobranca,
                    cd_usuario_cobranca,
                    NU_EMPRESA_COBRANCA,
                    NU_EMBARCACAO_PROJETO_COBRANCA,
                    tbpc_id,
                    tppr_id,
                    cd_usuario_analista,
                    soli_tx_codigo_servico,
                    soli_tx_solicitante_cobranca,
                    cd_usuario_alteracao,
                    soli_dt_alteracao,
                    soli_tx_descricao_servico,
                    hios_id_atual,
                    soli_dt_devolucao,
                    cd_usuario_devolucao,
                    de_observacao_bsb,
                    soli_qt_autenticacoes,
                    id_solicita_visto_pacote,
                    nu_servico_pacote,
                    soli_tx_justificativa,
                    id_tipo_envio,
                    now(),
                    p_cd_usuario,
                    cd_tecnico
                FROM solicita_visto 
                where id_solicita_visto = p_id_solicita_visto;
                insert into dataweb_log.autorizacao_candidato (
                        NU_EMPRESA,
                        NU_CANDIDATO,
                        NU_SOLICITACAO,
                        NU_EMBARCACAO_PROJETO,
                        CO_TIPO_AUTORIZACAO,
                        VA_RENUMERACAO_MENSAL,
                        NO_MOEDA_REMUNERACAO_MENSAL,
                        VA_REMUNERACAO_MENSAL_BRASIL,
                        CO_REPARTICAO_CONSULAR,
                        CO_FUNCAO_CANDIDATO,
                        TE_DESCRICAO_ATIVIDADES,
                        DT_ABERTURA_PROCESSO_MTE,
                        NU_PROCESSO_MTE,
                        NU_AUTORIZACAO_MTE,
                        DT_AUTORIZACAO_MTE,
                        DT_PRAZO_AUTORIZACAO_MTE,
                        DT_VALIDADE_VISTO,
                        NU_VISTO,
                        DT_EMISSAO_VISTO,
                        NO_LOCAL_EMISSAO_VISTO,
                        CO_NACIONALIDADE_VISTO,
                        NU_PROTOCOLO_CIE,
                        DT_PROTOCOLO_CIE,
                        DT_VALIDADE_PROTOCOLO_CIE,
                        DT_PRAZO_ESTADA,
                        DT_VALIDADE_CIE,
                        DT_EMISSAO_CIE,
                        NU_PROTOCOLO_PRORROGACAO,
                        DT_PROTOCOLO_PRORROGACAO,
                        DT_VALIDADE_PROTOCOLO_PROR,
                        DT_PRETENDIDA_PRORROGACAO,
                        DT_CANCELAMENTO,
                        NU_PROCESSO_CANCELAMENTO,
                        DT_PROCESSO_CANCELAMENTO,
                        TE_OBSERVACOES_PROCESSOS,
                        DT_PRAZO_ESTADA_SOLICITADO,
                        NU_ARMADOR,
                        NU_EMISSAO_CIE,
                        NO_LOCAL_ENTRADA,
                        NO_UF_ENTRADA,
                        DT_ENTRADA,
                        NU_TRANSPORTE_ENTRADA,
                        CO_CLASSIFICACAO_VISTO,
                        DS_JUSTIFICATIVA,
                        DS_LOCAL_TRABALHO,
                        DS_SALARIO_EXTERIOR,
                        DS_BENEFICIOS_SALARIO,
                        NU_EMBARCACAO_INICIAL,
                        CD_EMBARCADO,
                        DT_SITUACAO_SOL,
                        DT_CADASTRAMENTO,
                        DT_ULT_ALTERACAO,
                        NU_USUARIO_CAD,
                        NO_JUSTIFICATIVA_REP_CONS,
                        ID_AUTORIZACAO_CANDIDATO,
                        id_solicita_visto,
                        NU_SERVICO,
                        SALARIO_DOLAR,
                        autc_fl_revisado,
                        cd_usuario_revisao)
                    SELECT 
                        NU_EMPRESA,
                        NU_CANDIDATO,
                        NU_SOLICITACAO,
                        NU_EMBARCACAO_PROJETO,
                        CO_TIPO_AUTORIZACAO,
                        VA_RENUMERACAO_MENSAL,
                        NO_MOEDA_REMUNERACAO_MENSAL,
                        VA_REMUNERACAO_MENSAL_BRASIL,
                        CO_REPARTICAO_CONSULAR,
                        CO_FUNCAO_CANDIDATO,
                        TE_DESCRICAO_ATIVIDADES,
                        DT_ABERTURA_PROCESSO_MTE,
                        NU_PROCESSO_MTE,
                        NU_AUTORIZACAO_MTE,
                        DT_AUTORIZACAO_MTE,
                        DT_PRAZO_AUTORIZACAO_MTE,
                        DT_VALIDADE_VISTO,
                        NU_VISTO,
                        DT_EMISSAO_VISTO,
                        NO_LOCAL_EMISSAO_VISTO,
                        CO_NACIONALIDADE_VISTO,
                        NU_PROTOCOLO_CIE,
                        DT_PROTOCOLO_CIE,
                        DT_VALIDADE_PROTOCOLO_CIE,
                        DT_PRAZO_ESTADA,
                        DT_VALIDADE_CIE,
                        DT_EMISSAO_CIE,
                        NU_PROTOCOLO_PRORROGACAO,
                        DT_PROTOCOLO_PRORROGACAO,
                        DT_VALIDADE_PROTOCOLO_PROR,
                        DT_PRETENDIDA_PRORROGACAO,
                        DT_CANCELAMENTO,
                        NU_PROCESSO_CANCELAMENTO,
                        DT_PROCESSO_CANCELAMENTO,
                        TE_OBSERVACOES_PROCESSOS,
                        DT_PRAZO_ESTADA_SOLICITADO,
                        NU_ARMADOR,
                        NU_EMISSAO_CIE,
                        NO_LOCAL_ENTRADA,
                        NO_UF_ENTRADA,
                        DT_ENTRADA,
                        NU_TRANSPORTE_ENTRADA,
                        CO_CLASSIFICACAO_VISTO,
                        DS_JUSTIFICATIVA,
                        DS_LOCAL_TRABALHO,
                        DS_SALARIO_EXTERIOR,
                        DS_BENEFICIOS_SALARIO,
                        NU_EMBARCACAO_INICIAL,
                        CD_EMBARCADO,
                        DT_SITUACAO_SOL,
                        DT_CADASTRAMENTO,
                        DT_ULT_ALTERACAO,
                        NU_USUARIO_CAD,
                        NO_JUSTIFICATIVA_REP_CONS,
                        ID_AUTORIZACAO_CANDIDATO,
                        id_solicita_visto,
                        NU_SERVICO,
                        SALARIO_DOLAR,
                        autc_fl_revisado,
                        cd_usuario_revisao
                    FROM dataweb.autorizacao_candidato
                    WHERE id_solicita_visto =p_id_solicita_visto;
END$
delimiter ;