DROP PROCEDURE IF EXISTS CorrigeVistoAtual;
CREATE PROCEDURE CorrigeVistoAtual()
BEGIN
    DECLARE fim INT DEFAULT 0;
    DECLARE xNU_CANDIDATO, xcodigo int;
    DECLARE log text;
    DECLARE qtd int;
    DECLARE candidatos CURSOR FOR 
        SELECT NU_CANDIDATO
        from CANDIDATO C 
        where codigo_processo_mte_atual not in (select codigo from processo_mte)
        and codigo_processo_mte_atual is not null;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET fim = 1;

    OPEN candidatos;
    set qtd = 0;
    set log = '' ;
    read_loop: LOOP
        FETCH candidatos INTO xNU_CANDIDATO;
        IF fim THEN
            LEAVE read_loop;
        END IF;
        select ifnull(max(codigo), 0) into xcodigo from processo_mte where cd_candidato = xNU_CANDIDATO;
        if xcodigo > 0 then
          UPDATE processo_mte set fl_visto_atual = 0 where cd_candidato = xNU_CANDIDATO;
          UPDATE processo_mte set fl_visto_atual = 1 where codigo = xcodigo;
          UPDATE CANDIDATO set codigo_processo_mte_atual = xcodigo where NU_CANDIDATO = xNU_CANDIDATO;
          set log = concat(log, "*** Candidato ", xNU_CANDIDATO, ' associado ao processo ', xcodigo);
          set qtd = qtd + 1;
        END IF;
    END LOOP;
    
    CLOSE candidatos;    
    select qtd, log;
END;
