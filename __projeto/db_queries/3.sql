select * , CONCAT('<a target=\"_blank\" href=/operador/detalheCandidatoAuto.php?NU_CANDIDATO=',
                                      C.NU_CANDIDATO, '>', C.NOME_COMPLETO ,' (', C.NU_CANDIDATO, ')</a>')
										NOME_COMPLETO_LINK,
                                C.NOME_COMPLETO,
                                C.NU_PASSAPORTE,
                                C.NU_RNE,
                                C.NU_CPF,
                                DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE ,
                                C.codigo_processo_mte_atual,

								MTE.codigo codigo_mte,
								MTE.nu_processo NU_PROCESSO_MTE,
								MTE.prazo_solicitado PRAZO_SOLICITADO,
								DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO ,
                                DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO,

								DATE_FORMAT(COLETA.dt_entrada,'%d/%m/%Y')  DT_ENTRADA,
								DATE_FORMAT(COLETA.dt_emissao_visto,'%d/%m/%Y')  DT_EMISSAO,
								COLETA.*,

								DATE_FORMAT(REG.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA,
                                DATE_FORMAT(REG.dt_validade,'%d/%m/%Y')  DT_VALIDADE_REG,
                                DATE_FORMAT(REG.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_CIE,
								REG.nu_protocolo NU_PROTOCOLO_REG,
                                datediff(REG.dt_validade,now()) DT_VALIDADE_REG,
                                datediff(REG.dt_validade,now()) DT_VALIDADE_REG,


								PRO.NU_PROTOCOLO NU_PROTOCOLO_PRO,
                                DATE_FORMAT(PRO.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_PRO,
                                DATE_FORMAT(PRO.dt_prazo_pret,'%d/%m/%Y')  DT_PRAZO_PRET_PRO,
                                datediff(PRO.dt_validade,now()) DT_VALIDADE_PRO,

								PN.NO_NACIONALIDADE,
                                F.NO_FUNCAO,
								S.NO_SERVICO_RESUMIDO TIPO_VISTO,
								RC.NO_REPARTICAO_CONSULAR CONSULADO
                        from CANDIDATO C
                        left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual and MTE.fl_vazio = 0
                        left join processo_prorrog PRO on PRO.dt_prazo_pret = (select max(dt_prazo_pret) from processo_prorrog  where fl_vazio = 0 and codigo_processo_mte = MTE.codigo and cd_candidato = C.NU_CANDIDATO and  dt_prazo_pret is not null)
                                  and PRO.codigo_processo_mte = MTE.codigo
                                  and PRO.cd_candidato = C.NU_CANDIDATO
                                  and PRO.fl_vazio = 0
                        left join processo_regcie REG on REG.dt_requerimento = (select max(dt_requerimento) from processo_regcie where codigo_processo_mte = MTE.codigo and cd_candidato = C.NU_CANDIDATO and fl_vazio =0 and dt_requerimento is not null)
                                  and REG.codigo_processo_mte = MTE.codigo
                                  and REG.cd_candidato = C.NU_CANDIDATO
                                  and REG.fl_vazio = 0
                        left join processo_emiscie EMIS on EMIS.dt_emissao = (select max(dt_emissao) from processo_emiscie where codigo_processo_mte = MTE.codigo and cd_candidato = C.NU_CANDIDATO and fl_vazio =0 and dt_emissao is not null)
                                  and EMIS.cd_candidato = C.NU_CANDIDATO
                                  and EMIS.codigo_processo_mte = MTE.codigo
                                  and EMIS.fl_vazio = 0
                        left join processo_coleta COLETA  on COLETA.cd_candidato = C.NU_CANDIDATO
                                  and COLETA.codigo_processo_mte = MTE.codigo
                        left join processo_cancel CANC on CANC.dt_processo = (select max(dt_processo) from processo_cancel where codigo_processo_mte = MTE.codigo and cd_candidato = C.NU_CANDIDATO and fl_vazio =0)
                                  and CANC.cd_candidato = C.NU_CANDIDATO
                                  and CANC.codigo_processo_mte = MTE.codigo
                                  and CANC.fl_vazio = 0
                        left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
                        left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
                        left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
                        left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = MTE.cd_reparticao
                         WHERE C.NU_EMPRESA = 164 
                         ORDER BY C.NOME_COMPLETO ASC