
update processo_mte 
set prazo_solicitado = concat(dt_prazo_unid, case dt_prazo_tipo when 'y' then ' ano(s)' when 'm' then ' mes(es)' when 'd' then ' dia(s)' end)
where cd_candidato in (select NU_CANDIDATO from CANDIDATO where NU_EMPRESA = 90)
and dt_prazo_tipo is not null
;
update processo_mte 
set prazo_solicitado = date_format(dt_prazo_data, 'd/m/Y')
where cd_candidato in (select NU_CANDIDATO from CANDIDATO where NU_EMPRESA = 90 and CANDIDATO.DT_CADASTRAMENTO > '2011-06-09')
and dt_prazo_tipo is  null
and dt_prazo_data is  not null
;


update processo_coleta 
set no_validade =  concat(dt_validade_unid, case dt_validade_tipo when 'y' then ' ano(s)' when 'm' then ' mes(es)' when 'd' then ' dia(s)' end)
where cd_candidato in (select NU_CANDIDATO from CANDIDATO where NU_EMPRESA = 90 and CANDIDATO.DT_CADASTRAMENTO > '2011-06-09')
and dt_validade_unid is not null
;

update processo_coleta 
set no_validade =  date_format(dt_validade_data, 'd/m/Y')
where cd_candidato in (select NU_CANDIDATO from CANDIDATO where NU_EMPRESA = 90 and CANDIDATO.DT_CADASTRAMENTO > '2011-06-09')
and dt_validade_unid is null
and dt_validade_data is not null
;

select * from Edicao;
select * from CANDIDATO where NOME_COMPLETO like '%Terrel%Michael%Watts%'