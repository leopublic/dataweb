delimiter ; 
drop trigger if exists processo_emiscie_registra_log ;
delimiter $$
CREATE TRIGGER processo_emiscie_registra_log BEFORE UPDATE ON processo_emiscie
  FOR EACH ROW
  BEGIN
    DECLARE alterou tinyint;
  	DECLARE msg varchar(2000);
  	DECLARE virgula varchar(2);
    DECLARE x_desc varchar(1000);
    DECLARE x_old_desc varchar(1000);
    
    SET alterou = 0;
  	SET msg = '';
  	SET virgula = '';

    IF ifnull(NEW.cd_candidato, '') <> ifnull(OLD.cd_candidato, '') THEN 
      SET alterou = 1;
  	  SET msg = concat(msg, virgula, ' migrou do candidato ', ifnull(OLD.cd_candidato, '(nulo)'), ' para o candidato ', ifnull(NEW.cd_candidato, '(nulo)'));
  	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_cie, '') <> ifnull(OLD.nu_cie, '') THEN 
      SET alterou = 1;
  	  SET msg = concat(msg, virgula, ' n�mero do RNE de ', ifnull(OLD.nu_cie, '(nulo)'), ' para ', ifnull(NEW.nu_cie, '(nulo)'));
  	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_emissao, '') <> ifnull(OLD.dt_emissao, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data de expedi��o de ', ifnull(OLD.dt_emissao, '(nulo)'), ' para ', ifnull(NEW.dt_emissao, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_validade, '') <> ifnull(OLD.dt_validade, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' validade da CIE de ', ifnull(OLD.dt_validade, '(nulo)'), ' para ', ifnull(NEW.dt_validade, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.co_classificacao_visto, '') <> ifnull(OLD.co_classificacao_visto, '') THEN 
      SET alterou = 1;
      select no_classificacao_visto into x_desc from classificacao_visto where co_classificacao_visto = NEW.co_classificacao_visto;
      select no_classificacao_visto into x_old_desc  from classificacao_visto where co_classificacao_visto = OLD.co_classificacao_visto;

      SET msg = concat(msg, virgula, ' classifica��o do visto de ', ifnull(x_old_desc, '(nulo)'), ' para ', ifnull(x_desc, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_atendimento, '') <> ifnull(OLD.dt_atendimento, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data do atendimento de ', ifnull(OLD.dt_atendimento, '(nulo)'), ' para ', ifnull(NEW.dt_atendimento, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.observacao, '') <> ifnull(OLD.observacao, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' observa��o de ', ifnull(OLD.observacao, '(nulo)'), ' para ', ifnull(NEW.observacao, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.id_solicita_visto, '') <> ifnull(OLD.id_solicita_visto, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' OS de ', ifnull(OLD.id_solicita_visto, '(nulo)'), ' para ', ifnull(NEW.id_solicita_visto, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.codigo_processo_mte, '') <> ifnull(OLD.codigo_processo_mte, '') THEN 
      SET alterou = 1;
    SET msg = concat(msg, virgula, ' migrou do visto ', ifnull(OLD.codigo_processo_mte, '(nulo)'), ' para o visto ', ifnull(NEW.codigo_processo_mte, '(nulo)'));
    SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_servico, 0) <> ifnull(OLD.nu_servico, 0) THEN 
      SET alterou = 1;
  	  select no_servico_resumido into x_desc from servico where nu_servico = NEW.nu_servico;
  	  select no_servico_resumido into x_old_desc  from servico where nu_servico = OLD.nu_servico;

   	  SET msg = concat(msg, virgula, ' do servi�o ', ifnull(x_old_desc, '(nulo)'), ' para ', ifnull(x_desc, '(nulo)'));
  	  SET virgula = ', ';
    END IF;

    IF msg <> '' THEN
      SET msg = concat('Altera��es:', msg);
    END IF;

    IF alterou = 1 THEN
		  insert into historico_processo_emiscie(codigo, cd_usuario, hemi_tx_observacao) values (NEW.codigo, NEW.cd_usuario, msg);
    END IF;
  END;
$$
