use dataweb;
-- Trigger DDL Statements
DELIMITER $$
drop trigger if exists usuarios_registra_log_delete; $$
CREATE
TRIGGER usuarios_registra_log_delete
	BEFORE DELETE ON usuarios
	FOR EACH ROW
	BEGIN
			insert into dataweb_log.usuarios_log (
				cd_usuario
				,cd_perfil
				,cd_empresa
				,login
				,nm_senha
				,nome
				,nm_email
				,cd_superior
				,dt_cadastro
				,dt_ult
				,flagativo
				,usua_tx_ip_ult_acesso
				,usua_dt_ult_acesso
				,cd_usuario_log
				,log_tx_controller
				,log_tx_metodo
				,log_dt_evento
				,log_tx_evento
			)
			values (
				OLD.cd_usuario
				,OLD.cd_perfil
				,OLD.cd_empresa
				,OLD.login
				,OLD.nm_senha
				,OLD.nome
				,OLD.nm_email
				,OLD.cd_superior
				,OLD.dt_cadastro
				,OLD.dt_ult
				,OLD.flagativo
				,OLD.usua_tx_ip_ult_acesso
				,OLD.usua_dt_ult_acesso
				,OLD.cd_usuario_log
				,OLD.log_tx_controller
				,OLD.log_tx_metodo
				,now()
				,'delete'
			);
	END$$


