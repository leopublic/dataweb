﻿delimiter ; 
drop trigger if exists solicita_visto_registra_log ;
delimiter $$
CREATE TRIGGER solicita_visto_registra_log BEFORE UPDATE ON solicita_visto
  FOR EACH ROW
  BEGIN
    DECLARE alterou tinyint;
	DECLARE msg varchar(2000);
	DECLARE virgula varchar(2);
	DECLARE x_no_servico_resumido varchar(500);
	DECLARE x_old_no_servico_resumido varchar(500);
	DECLARE x_no_status_conclusao varchar(500);
	DECLARE x_old_no_status_conclusao varchar(500);
	DECLARE x_no_razao_social varchar(500);
	DECLARE x_old_no_razao_social varchar(500);
	DECLARE x_desc varchar(1000);
	DECLARE x_old_desc varchar(1000);
	DECLARE x_nome varchar(500);
	DECLARE x_old_nome varchar(500);
    
    SET alterou = 0;
	SET msg = '';
	SET virgula = '';

    IF ifnull(NEW.nu_empresa, '') <> ifnull(OLD.nu_empresa, '') THEN 
      SET alterou = 1;
	  select no_razao_social into x_desc from empresa where nu_empresa = NEW.nu_empresa;
	  select no_razao_social into x_desc from empresa where nu_empresa = OLD.nu_empresa;
	  SET msg = concat(msg, virgula, ' alterou da empresa do processo de "', ifnull(x_old_desc, '(nulo)'), '" para "', ifnull(x_desc, '(nulo)'), '"');
	  SET virgula = ', ';

	  select no_embarcacao_projeto into x_desc from embarcacao_projeto where nu_empresa = NEW.nu_empresa and nu_embarcacao_projeto = NEW.NU_EMBARCACAO_PROJETO;
	  select no_embarcacao_projeto into x_old_desc from embarcacao_projeto where nu_empresa = OLD.nu_empresa and nu_embarcacao_projeto = OLD.NU_EMBARCACAO_PROJETO;
	  SET msg = concat(msg, virgula, ' alterou da embarcação do processo de "', ifnull(x_old_desc, '(nulo)'), '" para "', ifnull(x_desc, '(nulo)'), '"');
	  SET virgula = ', ';
    else
	    IF ifnull(NEW.NU_EMBARCACAO_PROJETO, '') <> ifnull(OLD.NU_EMBARCACAO_PROJETO, '') THEN 
			select no_embarcacao_projeto into x_desc from embarcacao_projeto where nu_empresa = NEW.nu_empresa and nu_embarcacao_projeto = NEW.NU_EMBARCACAO_PROJETO;
			select no_embarcacao_projeto into x_old_desc from embarcacao_projeto where nu_empresa = OLD.nu_empresa and nu_embarcacao_projeto = OLD.NU_EMBARCACAO_PROJETO;
			SET msg = concat(msg, virgula, ' alterou da embarcação do processo "', ifnull(x_old_desc, '(nulo)'), ' para "', ifnull(x_desc, '(nulo)'), '"');
			SET virgula = ', ';
		END IF;
    END IF;

    IF ifnull(NEW.NU_EMPRESA_COBRANCA, '') <> ifnull(OLD.NU_EMPRESA_COBRANCA, '') THEN 
      SET alterou = 1;
	  select no_razao_social into x_no_razao_social from empresa where nu_empresa = NEW.NU_EMPRESA_COBRANCA;
	  select no_razao_social into x_old_no_razao_social from empresa where nu_empresa = OLD.NU_EMPRESA_COBRANCA;
	  SET msg = concat(msg, virgula, ' alterou da empresa de cobrança de "', ifnull(x_old_no_razao_social, '(nulo)'), '" para "', ifnull(x_no_razao_social, '(nulo)'));
	  SET virgula = ', ';

	  select no_embarcacao_projeto into x_desc from embarcacao_projeto where nu_empresa = NEW.NU_EMPRESA_COBRANCA and nu_embarcacao_projeto = NEW.NU_EMBARCACAO_PROJETO_COBRANCA;
	  select no_embarcacao_projeto into x_old_desc from embarcacao_projeto where nu_empresa = OLD.NU_EMPRESA_COBRANCA and nu_embarcacao_projeto = OLD.NU_EMBARCACAO_PROJETO_COBRANCA;
	  SET msg = concat(msg, virgula, ' alterou da embarcação de cobrança de "', ifnull(x_old_desc, '(nulo)'), '" para "', ifnull(x_desc, '(nulo)'), '"');
	  SET virgula = ', ';
    else
	    IF ifnull(NEW.NU_EMBARCACAO_PROJETO_COBRANCA, '') <> ifnull(OLD.NU_EMBARCACAO_PROJETO_COBRANCA, '') THEN 
			select no_embarcacao_projeto into x_desc from embarcacao_projeto where nu_empresa = NEW.NU_EMPRESA_COBRANCA and nu_embarcacao_projeto = NEW.NU_EMBARCACAO_PROJETO_COBRANCA;
			select no_embarcacao_projeto into x_old_desc from embarcacao_projeto where nu_empresa = OLD.NU_EMPRESA_COBRANCA and nu_embarcacao_projeto = OLD.NU_EMBARCACAO_PROJETO_COBRANCA;
			SET msg = concat(msg, virgula, ' alterou da embarcação de cobrança "', ifnull(x_old_desc, '(nulo)'), '" para "', ifnull(x_desc, '(nulo)'), '"');
			SET virgula = ', ';
		END IF;
    END IF;

    IF ifnull(NEW.no_solicitador, '') <> ifnull(OLD.no_solicitador, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' solicitante de "', ifnull(OLD.no_solicitador, '(nulo)'), '" para "', ifnull(NEW.no_solicitador, '(nulo)'), '"');
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.dt_solicitacao, '') <> ifnull(OLD.dt_solicitacao, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' data da solicitação de ', ifnull(OLD.dt_solicitacao, '(nulo)'), ' para ', ifnull(NEW.dt_solicitacao, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.nu_servico, 0) <> ifnull(OLD.nu_servico, 0) THEN 
      SET alterou = 1;
	  select no_servico_resumido into x_no_servico_resumido from servico where nu_servico = NEW.nu_servico;
	  select no_servico_resumido into x_old_no_servico_resumido  from servico where nu_servico = OLD.nu_servico;

 	  SET msg = concat(msg, virgula, ' do serviço "', ifnull(x_old_no_servico_resumido, '(nulo)'), '" para "', ifnull(x_no_servico_resumido, '(nulo)'), '"');
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.tppr_id, '') <> ifnull(OLD.tppr_id, '') THEN 
      SET alterou = 1;
	  select tppr_tx_nome into x_desc from tipo_preco where tppr_id= NEW.tppr_id;
	  select tppr_tx_nome into x_old_desc from tipo_preco where tppr_id= OLD.tppr_id;
	  SET msg = concat(msg, virgula, ' do tipo de cobrança ', ifnull(x_old_desc, '(nulo)'), ' para ', ifnull(x_desc, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.soli_vl_cobrado, '') <> ifnull(OLD.soli_vl_cobrado, '') THEN 
      SET alterou = 1;
	  SET msg = concat(msg, virgula, ' valor cobrado de ', ifnull(OLD.soli_vl_cobrado, '(nulo)'), ' para ', ifnull(NEW.soli_vl_cobrado, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.soli_dt_liberacao_cobranca, '') <> ifnull(OLD.soli_dt_liberacao_cobranca, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data de liberação para cobrança de "', ifnull(OLD.soli_dt_liberacao_cobranca, '(nulo)'), '" para "', ifnull(NEW.soli_dt_liberacao_cobranca, '(nulo)'), '"');
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.soli_tx_codigo_servico, '') <> ifnull(OLD.soli_tx_codigo_servico, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' código do serviço "', ifnull(OLD.soli_tx_codigo_servico, '(nulo)'), '" para "', ifnull(NEW.soli_tx_codigo_servico, '(nulo)'), '"');
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.soli_tx_descricao_servico, '') <> ifnull(OLD.soli_tx_descricao_servico, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' descrição do serviço para cobrança de "', ifnull(OLD.soli_tx_descricao_servico, '(nulo)'), '" para "', ifnull(NEW.soli_tx_descricao_servico, '(nulo)'), '"');
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.soli_tx_obs_cobranca, '') <> ifnull(OLD.soli_tx_obs_cobranca, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' observação de cobrança de "', ifnull(OLD.soli_tx_obs_cobranca, '(nulo)'), '" para "', ifnull(NEW.soli_tx_obs_cobranca, '(nulo)'), '"');
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.de_observacao, '') <> ifnull(OLD.de_observacao, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' observação de "', ifnull(OLD.de_observacao, '(nulo)'), '" para "', ifnull(NEW.de_observacao, '(nulo)'), '"');
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.soli_dt_devolucao, '') <> ifnull(OLD.soli_dt_devolucao, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' data devolução de "', ifnull(OLD.soli_dt_devolucao, '(nulo)'), '" para "', ifnull(NEW.soli_dt_devolucao, '(nulo)'), '"');
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.soli_tx_justificativa, '') <> '' THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, '<br/>Justificativa informada: ', ifnull(NEW.soli_tx_justificativa, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF ifnull(NEW.id_status_sol, '') <> ifnull(OLD.id_status_sol, '') THEN 
      SET alterou = 1;
	  select no_status_sol into x_desc from status_solicitacao where id_status_sol= NEW.id_status_sol;
	  select no_status_sol into x_old_desc from status_solicitacao where id_status_sol= OLD.id_status_sol;
	  SET msg = concat(msg, virgula, ' do status ', ifnull(x_old_desc, '(nulo)'), ' para ', ifnull(x_desc, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.id_tipo_envio, '') <> ifnull(OLD.id_tipo_envio, '') THEN 
      SET alterou = 1;
	  select descricao into x_desc from tipo_envio where id_tipo_envio= NEW.id_tipo_envio;
	  select descricao into x_old_desc from tipo_envio where id_tipo_envio= OLD.id_tipo_envio;
	  SET msg = concat(msg, virgula, ' do tipo de envio ', ifnull(x_old_desc, '(nulo)'), ' para ', ifnull(x_desc, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.stco_id, '') <> ifnull(OLD.stco_id, '') THEN 
      SET alterou = 1;
	  select stco_tx_nome into x_desc from status_cobranca_os where stco_id= NEW.stco_id;
	  select stco_tx_nome into x_old_desc from status_cobranca_os where stco_id= OLD.stco_id;
	  SET msg = concat(msg, virgula, ' do status de cobrança ', ifnull(x_old_desc, '(nulo)'), ' para ', ifnull(x_desc, '(nulo)'));
	  SET virgula = ', ';
    END IF;

    IF ifnull(NEW.resc_id, '') <> ifnull(OLD.resc_id, '') THEN 
      SET alterou = 1;
      SET msg = concat(msg, virgula, ' do resumo de cobrança ', ifnull(OLD.resc_id, '(nulo)'), ' para ', ifnull(NEW.resc_id, '(nulo)'));
      SET virgula = ', ';
    END IF;

    IF msg <> '' THEN
        SET msg = concat('Alterações: ', msg);
    END IF;

    IF alterou = 1 THEN
		insert into historico_os(id_solicita_visto, id_status_sol, hios_dt_evento, cd_usuario, hios_tx_observacoes, controller, metodo) values (NEW.id_solicita_visto, NEW.id_status_sol, now(), NEW.cd_usuario_alteracao, msg, NEW.controller, NEW.metodo);
    END IF;
  END;
$$
