drop table if exists visa_entry_type;

/*==============================================================*/
/* Table: visa_entry_type                                       */
/*==============================================================*/
create table visa_entry_type
(
   entt_id              int not null auto_increment,
   entt_tx_nome         varchar(200),
   primary key (entt_id)
);
drop table if exists travel_reason;

/*==============================================================*/
/* Table: travel_reason                                         */
/*==============================================================*/
create table travel_reason
(
   trrs_id              int not null auto_increment,
   trrs_tx_nome         varchar(200),
   primary key (trrs_id)
);
drop table if exists passport_request_type;

/*==============================================================*/
/* Table: passport_request_type                                 */
/*==============================================================*/
create table passport_request_type
(
   past_id              int not null auto_increment,
   past_tx_nome         varchar(200),
   primary key (past_id)
);
drop table if exists accounting_category;

/*==============================================================*/
/* Table: accounting_category                                   */
/*==============================================================*/
create table accounting_category
(
   accc_id              int not null auto_increment,
   accc_tx_nome         varchar(200),
   primary key (accc_id)
);
insert into travel_reason (trrs_tx_nome) VALUES ('Tourist'), ('Business'), ('Work'), ('Resident');
insert into visa_entry_type (entt_tx_nome) values ('Single'), ('Multiple');
insert into passport_request_type(past_tx_nome) values('1st time applicant'), ('Lost/Damage or stolen'), ('Renewal'), ('2nd passport'), ('Name change'), ('Add pages');
insert into accounting_category(accc_tx_nome) values ('Employee'), ('Family/Relative');

drop table if exists form_tvs;

/*==============================================================*/
/* Table: form_tvs                                              */
/*==============================================================*/
create table form_tvs
(
   ftvs_id              int not null auto_increment,
   entt_id              int,
   past_id              int,
   accc_id              int,
   trrs_id              int,
   nu_candidato         int(11),
   id_solicita_visto    int(10),
   ftvs_tx_job_title    varchar(300),
   ftvs_tx_employee_id  varchar(300),
   ftvs_fl_us_citizen   tinyint,
   ftvs_tx_us_status    varchar(300),
   ftvs_fl_offshore     tinyint,
   ftvs_tx_visatype     varchar(300),
   ftvs_tx_focalpoint   varchar(300),
   ftvs_tx_focalpoint_tel varchar(300),
   ftvs_tx_focalpoint_cel varchar(300),
   ftvs_tx_focalpoint_email varchar(300),
   ftvs_tx_foreign_employer varchar(300),
   ftvs_tx_foreign_address varchar(300),
   ftvs_tx_foreign_tel  varchar(300),
   ftvs_tx_brazil_employer varchar(300),
   ftvs_tx_brazil_address varchar(300),
   ftvs_tx_brazil_tel   varchar(300),
   ftvs_fl_request      tinyint,
   ftvs_tx_countries    varchar(300),
   ftvs_dt_travel       date,
   ftvs_dt_passport_needed date,
   cd_usuario_ult_atu   int,
   ftvs_dt_ult_atu      datetime,
   ftvs_tx_other_nationalities varchar(500),
   ftvs_fl_fedex        tinyint,
   ftvs_tx_return_address varchar(500),
   ftvs_tx_return_city  varchar(500),
   ftvs_tx_return_country varchar(500),
   ftvs_tx_return_zip   varchar(50),
   ftvs_tx_other_request text,
   primary key (ftvs_id)
);

alter table form_tvs add constraint FK_form_tvs_accounting_category foreign key (accc_id)
      references accounting_category (accc_id) on delete restrict on update restrict;

alter table form_tvs add constraint FK_form_tvs_candidato foreign key (nu_candidato)
      references candidato (nu_candidato) on delete restrict on update restrict;

alter table form_tvs add constraint FK_form_tvs_passport_request_type foreign key (past_id)
      references passport_request_type (past_id) on delete restrict on update restrict;

alter table form_tvs add constraint FK_form_tvs_solicita_visto foreign key (id_solicita_visto)
      references solicita_visto (id_solicita_visto) on delete restrict on update restrict;

alter table form_tvs add constraint FK_form_tvs_travel_reason foreign key (trrs_id)
      references travel_reason (trrs_id) on delete restrict on update restrict;

alter table form_tvs add constraint FK_form_tvs_visa_entry_type foreign key (entt_id)
      references visa_entry_type (entt_id) on delete restrict on update restrict;
