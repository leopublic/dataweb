<?php
session_start();
?>
<html>
<head>
<title>Mundivisas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="/estilos.css" rel="stylesheet" type="text/css" />
<link href="/css/jquery.grid.css" rel="stylesheet" type="text/css"/>

<style>
body{
    margin:0;
    padding: 0;
    text-align: center;
    font:10px verdana;
    background:url("../imagens/topfundo.png") repeat-x 5px
}
#header{
    background: url('images/fundo_barra.gif');height:23px;
    background: #1C5A80;
}
#conteudo{
    padding-top: 40px;
    padding-bottom:40px;
    text-align:center;
    height:auto;
}
#footer{
    /*background: url('images/fundo_barra.gif'); height:23px;
    background: #1C5A80;*/
    text-align:center;
    padding-top:3px;

    color: #000;
}
#boxlogin{
    margin:0 auto;
    padding:0;
    margin-top:50px;
}
</style>
</head>

<body>
<div id="global">

    <div id="header">

    </div>
        <div id="topo">
        <img src="images/logo.gif" />
    </div>
    <div id="conteudo">
      <p class="textoazul">:: <strong>Bem vindo a Intranet da Mundivisas</strong> ::</p>
      <?php
            require_once 'LIB/conecta.php';
            $app = new AppRequest;

            if($app->g('bloqueado')){

                echo '
                <div class="error" style="font:11px arial">
                    <img src="/imagens/close.png" /> '.Acesso::GetBloqueio($app->g('bloqueado')).'
                </div><br /><br />';
            }

      ?>
    <!-- box login -->
      <table id="boxlogin">
          <tr>
            <td class="textoazul" style="text-align:center;"><strong>O sistema está temporariamente suspenso para a realização de manutenções preventivas<br/><br/>Estimativa de retorno : 28/05/2012</strong></td>
        </table>
    </form>
    <!-- box login -->
    </div>
	<div style="alig:center;margin:auto;color:red;font-weight:bold;font-size:12px;">
		<?php
		if (isset($_SESSION['msgErro'])){
			print $_SESSION['msgErro'];
			unset($_SESSION['msgErro']);
		}
		?>
	</div>
    <div id="footer">
        © Mundivisas 2010 - Produção
    </div>
</div>
</body>
</html>
