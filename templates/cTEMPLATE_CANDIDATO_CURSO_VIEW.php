<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_CANDIDATO_CURSO_VIEW extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('curs_id'));
		if (cHTTP::getLang()== "en_us"){
			$this->mTitulo = "Saved Courses";
			$this->AdicioneColuna(new cCAMPO_TEXTO('tpen_no_nome_en_us', 'Tipo curso', 'Course type'));
			$this->mCampo['tpen_no_nome_en_us']->mClasse = 'esq';
		}
		else{
			$this->mTitulo = "Cursos informados";
			$this->AdicioneColuna(new cCAMPO_TEXTO('tpen_no_nome', 'Tipo curso', 'Course type'));
			$this->mCampo['tpen_no_nome']->mClasse = 'esq';
		}
		$this->AdicioneColuna(new cCAMPO_TEXTO('curs_no_instituicao', 'Instituição', 'Institution'));
		$this->mCampo['curs_no_instituicao']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('curs_no_grau', 'Grau recebido', 'Degree'));
		$this->mCampo['curs_no_grau']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('curs_no_cidade', 'Cidade/País', 'City/Country'));
		$this->mCampo['curs_no_cidade']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('curs_no_periodo', 'Período', 'Period'));
		$this->mCampo['curs_no_periodo']->mClasse = 'esq';
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcaoLinha(new cACAO_SUBMIT_BUTTON("Remove", "EXCLUIR", "Hit here to remove this item."));
	}
}
