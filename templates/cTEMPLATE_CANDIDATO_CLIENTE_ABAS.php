<?php
class cTEMPLATE_CANDIDATO_CLIENTE_ABAS extends cTEMPLATE_ABA{

	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "CADASTRO DE CLIENTE";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_CANDIDATO'));
		$this->AdicioneAbaAjax('DADOS_PESSOAIS', 'Personal data', 'cCTRL_CANDIDATO', 'DadosPessoais_cliente', 'Personal info' );
		$this->AdicioneAbaAjax('DADOS_VISTO', 'Visa details', 'cCTRL_CANDIDATO', 'VisaDetails_cliente', 'Visa details' );
		$this->AdicioneAbaAjax('ANEXOS', 'Files & Documentation', 'cCTRL_CANDIDATO', 'ListeArquivos', 'Documentation');
	}
}
