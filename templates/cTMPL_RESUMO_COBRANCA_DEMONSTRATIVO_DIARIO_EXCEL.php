<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_RESUMO_COBRANCA_DEMONSTRATIVO_DIARIO_EXCEL extends cTEMPLATE_LISTAGEM_EXCEL{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Movimentação diária";
		cHTTP::$comCabecalhoFixo = false;
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneColuna(new cCAMPO_HIDDEN('ID_TIPO_ACOMPANHAMENTO'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empf_tx_descricao', 'Mundivisas'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa RC'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO', 'Emb./proj. RC'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_tx_solicitante_cobranca', 'Solicitante'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Candidato(s)'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_processo', 'Processo'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_responsavel', 'Responsável'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Serviço'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_tx_codigo_servico', 'Cód.'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao', 'Data serviço'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento', 'Data RC'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_nu_numero_mv', 'Núm. RC'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'soli_vl_cobrado', 'Valor (R$)'));
		$this->EstilizeUltimoCampo('dir');

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'pre2012', "Mostrar pré 01/12/2012?"));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento_ini', 'Data RC desde'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento_fim', 'Data RC até'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_requerente', "Empresa RC", cCOMBO::cmbEMPRESA));
		$this->mFiltro['nu_empresa_requerente']->mQualificadorFiltro = 'rc';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj. RC', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
		$this->mFiltro['NU_EMBARCACAO_PROJETO_COBRANCA']->mQualificadorFiltro = 'rc';
	}

	public function CustomizarCabecalho() {
		parent::CustomizarCabecalho();
		$this->mTitulo = "Movimentação diária de ".$this->getValorFiltro('resc_dt_faturamento_ini');
		if($this->getValorFiltro('resc_dt_faturamento_fim') != ''){
			$this->mTitulo .= " até ".$this->getValorFiltro('resc_dt_faturamento_fim');
		}
	}

	public function CustomizarLinha() {

		$processo = cprocesso::FabricaProcesso($this->getValorCampo('ID_TIPO_ACOMPANHAMENTO'));
		if(method_exists($processo, 'RecupereSePelaOsSemCand') && method_exists($processo, 'get_nu_processo')){
			$processo->RecupereSePelaOsSemCand($this->getValorCampo('id_solicita_visto'));
			$this->setValorCampo('nu_processo', "'".$processo->get_nu_processo());
		}
		else{
			$this->setValorCampo('nu_processo', '(n/a)');
		}
	}

	public function RenderizeCabecalhoListagem() {
		parent::RenderizeCabecalhoListagem();

	}

}
