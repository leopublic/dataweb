<?php
class cTMPL_PROCESSO_EDIT_OS_MTE extends cTMPL_PROCESSO_EDIT_OS{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicionePainel('Protocolo');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_envio_bsb', 'Data de envio do processo'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('id_tipo_envio', 'Método de envio do processo', cCOMBO::cmbTIPO_ENVIO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_recebimento_bsb', 'Data de recebimento BSB'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_processo', 'Processo MTE'));
		$this->EstilizeUltimoCampo('processo');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_requerimento', 'Data de Requerimento no MTE'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'observacao', 'Observação'));
		$this->mCampo['observacao']->mLarguraDupla = true;

		$this->AdicionePainel('Andamento');
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('id_status_sol', 'Situação atual', cCOMBO::cmbSTATUS_SOLICITACAO_PROCESSOS));
//		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'NO_STATUS_ANDAMENTO', 'Situação atual'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_ult_atu_andamento', 'Data última atual.'));

		$this->AdicionePainel('Conclusão');
//		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('ID_STATUS_CONCLUSAO', 'Resultado', cCOMBO::cmbSTATUS_CONCLUSAO));
//		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'NO_STATUS_CONCLUSAO', 'Resultado'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_deferimento', 'Data de conclusão'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_publicacao_dou', 'Data publicação DOU'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_oficio', 'Número ofício do MRE'));

		$this->AdicionarCamposCobranca();
	}

	public function CustomizeCabecalho() {
		parent::CustomizeCabecalho();
		unset($this->mAcoes['SALVAR_E_FECHAR']);

        $this->setValorCampo('id_tipo_envio', $this->os->mid_tipo_envio);

        if($this->os->mReadonly){
 			$this->mCampo['id_status_sol']->mReadonly = false;
			$this->mCampo['id_status_sol']->mDisabled = true;

 			$this->mCampo['id_tipo_envio']->mReadonly = false;
			$this->mCampo['id_tipo_envio']->mDisabled = true;
        }
	}
}
