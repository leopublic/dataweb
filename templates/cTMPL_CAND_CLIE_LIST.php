<?php

class cTMPL_CAND_CLIE_LIST extends cTEMPLATE_LISTAGEM_TWIG {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "List Candidates";
        $this->mOrdenacaoDefault = 'NOME_COMPLETO';
        $this->mCols = array('70px', 'auto', 'auto', '100px', '100px', '80px', 'auto', '80px', '80px');
        // Adiciona campos da tela
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('nu_candidato_tmp'));
        $this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('FL_ATUALIZACAO_HABILITADA'));
        $this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('codigo_processo_mte_atual'));
        $this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Nome', 'Name'));
        $this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', "Embarcação/projeto", "Vessel/project name"));

        if (cHTTP::getLang() == 'pt_br') {
            $this->AdicioneColuna(new cCAMPO_TEXTO('NO_NACIONALIDADE', "Nacionalidade", "Nationality"));
        } else {
            $this->AdicioneColuna(new cCAMPO_TEXTO('NO_NACIONALIDADE_EM_INGLES', "Nacionalidade", "Nationality"));
        }
        $this->AdicioneColuna(new cCAMPO_TEXTO('NU_CPF', "CPF", "CPF"));
        $this->mCampo['NU_CPF']->mClasse = 'cen';
        $this->AdicioneColuna(new cCAMPO_TEXTO('NU_PASSAPORTE', "N&ordm; passaporte", "Passport #"));
        $this->mCampo['NU_PASSAPORTE']->mClasse = 'cen';
        $this->AdicioneColuna(new cCAMPO_TEXTO('DT_VALIDADE_PASSAPORTE', "Data val.", "Passport expiry date"));
        $this->mCampo['DT_VALIDADE_PASSAPORTE']->mClasse = 'cen';
        $this->AdicioneColuna(new cCAMPO_TEXTO('TIPO_VISTO', "Tipo de visto", "Visa type"));
        $this->AdicioneColuna(new cCAMPO_TEXTO('DT_DEFERIMENTO', "Deferimento", "Process granted"));
        $this->mCampo['DT_DEFERIMENTO']->mClasse = 'cen';
        $this->AdicioneColuna(new cCAMPO_TEXTO('DT_PRAZO_ESTADA', "Prazo de estada", "Period of stay"));
        $this->mCampo['DT_PRAZO_ESTADA']->mClasse = 'cen';
        // Filtros
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', 'Company', cCOMBO::cmbEMPRESA));
        $this->mFiltro['NU_EMPRESA']->mVisivel = false;
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Embarcação/projeto', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS, 'Vessel/project name'));
        $this->mFiltro['NU_EMBARCACAO_PROJETO']->mValorDefault = '(any)';
        $this->VinculeFiltros('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Name'));
//		$this->mFiltro['NOME_COMPLETO']->mTipoComparacaoFiltro = cFILTRO::tpSEL_LIKE_INI;
    }

    public function CustomizarLinha() {
        if ($this->getValorCampo('NU_CANDIDATO') > 0){
            $link = '/paginas/carregue.php?controller=cCTRL_CANDIDATO_CLIENTE&metodo=editarInfo&NU_CANDIDATO=' . $this->mCampo['NU_CANDIDATO']->mValor;
        } else {
            $link = '/paginas/carregue.php?controller=cCTRL_CANDIDATO_CLIENTE&metodo=editarInfo&nu_candidato_tmp=' . $this->mCampo['nu_candidato_tmp']->mValor;
            
        }
        $this->mCampo['NOME_COMPLETO']->mValor = '<a href="'.$link.'">' . $this->mCampo['NOME_COMPLETO']->mValor . '</a>';
        if (intval($this->getValorCampo('codigo_processo_mte_atual')) > 0) {
            $proc = new cprocesso_mte();
            $proc->mcodigo = $this->getValorCampo('codigo_processo_mte_atual');
            $this->setValorCampo('DT_PRAZO_ESTADA', $proc->get_situacao_prazo_estada());
        } else {
            $this->setValorCampo('DT_PRAZO_ESTADA', '--');
        }
    }

}
