<?php

/**
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_OS_ABA_COBR_EDIT extends cTMPL_OS_ABA {

    public function __construct() {
        parent::__construct(__CLASS__);

        $this->nomeAba = "Cobrança";
        $this->linkAba = "/paginas/carregueComMenu.php?controller=cCTRL_OS_ABA_COBR_EDIT&metodo=Edite";
        $this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
        $this->mMsgInicial = "";
        // Adiciona campos da tela
        $this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));

        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_EMPRESA'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'stco_id'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'stco_tx_nome'));

        $this->AdicionePainel('Empresa');
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa PROCESSO'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO', 'Embarcação/projeto PROCESSO'));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('nu_empresa_requerente', 'Empresa COBRANÇA', cCOMBO::cmbEMPRESA));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Embarcação/projeto REAL', cCOMBO::cmbEMBARCACAO_PROJETO));
        $this->AdicionePainel('Solicitação');
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_solicitacao', 'Data da solicitação'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Serviço da OS'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_solicitador', 'Solicitante OS'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_tx_solicitante_cobranca', 'Solicitante cobrança'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_responsavel', 'Responsável'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'de_observacao', 'Observações da OS'));
        $this->AdicionePainel('Faturamento');
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('tppr_id', 'Tipo cobrança', cCOMBO::cmbTIPO_PRECO));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_tx_codigo_servico', 'Código serviço'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_tx_descricao_servico', 'Descrição do serviço'));
//		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('nome_criada_por', 'Criada por', 'Name'));
//		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMPRESA_COBRANCA', 'Empresa a ser faturada', cCOMBO::cmbEMPRESA));
//		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('tbpc_id', 'Tabela de preço', cCOMBO::cmbTABELA_PRECO));
//		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'prec_vl_real', 'Valor do serviço na tabela'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'soli_vl_cobrado', 'Valor cobrado'));
//		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'stco_tx_nome', 'Situação'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'soli_tx_obs_cobranca', 'Observações da cobrança'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'justificativa', 'Justificativa para alteração'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_id', 'Situação'));
        // Adiciona as ações que estarão disponíveis na tela
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar as alterações", "", "", "Submit"));
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar e liberar para cobrança", "SALVAR_E_LIBERAR", "Clique para salvar as alterações e liberar a OS para cobran&ccedil;a", "", "", "Submit"));
    }

    public function CustomizeCabecalho() {
        parent::CustomizeCabecalho();
        $this->mCampo['NU_EMBARCACAO_PROJETO_COBRANCA']->mExtra = " and NU_EMPRESA = " . $this->getValorCampo('NU_EMPRESA');

        $this->mCampo['tppr_id']->mDisabled = true;
        $xresc_id = $this->getValorCampo('stco_tx_nome');
        switch ($this->getValorCampo('stco_id')) {
            case 1:  // Pendente
                $this->mCampo['tppr_id']->mDisabled = false;
                break;
            case 2:  // Liberada para cobranca
                $this->mCampo['tppr_id']->mDisabled = false;
                break;
            case 3:  // Faturada
                break;
            case 4:  // Não gera cobranca
                $this->mCampo['tppr_id']->mDisabled = false;
                break;
            case 5:  // Antiga
                break;
            default:
                break;
        }
        if ($this->getValorCampo('stco_id') == 1) {
            $this->mAcoes['SALVAR_E_LIBERAR']->mVisivel = true;
        } else {
            $this->mAcoes['SALVAR_E_LIBERAR']->mVisivel = false;            
        }
        
        if ($this->getValorCampo('resc_id') > 0) {
            $resumo = new cresumo_cobranca();
            $resumo->mresc_id = $this->getValorCampo('resc_id');
            $resumo->RecuperePeloId();
            if ($resumo->mresc_nu_numero_mv == '') {
                $numero = '(sem número)';
            } else {
                $numero = $resumo->mresc_nu_numero_mv;
            }
            $link = ' <a href="/paginas/carregueComMenu.php?controller=cCTRL_RESUMO_COBRANCA&metodo=ListeOs&resc_id=' . $resumo->mresc_id . '" title="Clique para abrir o resumo" target="_blank">em ' . $resumo->mresc_dt_faturamento . ' no resumo ' . $numero . '</a>';
            $xresc_id .= $link;
        } 
        
        $this->setValorCampo('resc_id', $xresc_id);

        if ($this->getValorCampo('stco_id') == '4' || $this->getValorCampo('stco_id') == '5') {
            
        }
    }

}
