<?php
class cTMPL_SERVICO_EDITE extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Incluir/alterar Servi&ccedil;o";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
		$this->qtd_colunas = 1;

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_SERVICO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_SERVICO', 'C&oacute;digo'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO', 'Nome completo'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Nome resumido'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_em_ingles', 'Nome completo em inglês'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_resumido_em_ingles', 'Nome resumido em inglês'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_TIPO_SERVICO', 'Grupo de servi&ccedil;os', cCOMBO::cmbTIPO_SERVICO));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_TIPO_AUTORIZACAO', 'Autoriza&ccedil;&atilde;o', cCOMBO::cmbTIPO_AUTORIZACAO));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_CLASSIFICACAO_VISTO', 'Classifica&ccedil;&atilde;o', cCOMBO::cmbCLASSIFICA_VISTO));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('ID_TIPO_ACOMPANHAMENTO', 'Tela de acompanhamento', cCOMBO::cmbTIPO_ACOMPANHAMENTO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_candidato_obrigatorio', 'É obrigado a ter candidatos?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_form_tvs', 'Gera formulário TVS?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_ativo', 'Ativo?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_revisao_analistas', 'É revisado pelas analistas?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_envio_bsb', 'É enviado para BSB?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_timesheet', 'Habilitado para o timesheet?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_despesas_adicionais', 'Habilitado para despesas adicionais?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_cria_visto', 'Esse serviço cria um visto novo no candidato?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_coleta', 'Esse serviço vai para o protocolo de coleta?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'fl_disponivel_clientes', 'Esse serviço está disponível para os clientes abrirem pré-OS?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo( 'id_perfiltaxa', 'Perfil de taxas aplicável', cCOMBO::cmbPERFILTAXA));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar"));
	}
}
