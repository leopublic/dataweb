<?php
class cTMPL_PROCESSO_REGCIE_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('nu_servico'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('codigo'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('cd_candidato'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('cd_solicitacao'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('nu_protocolo', 'N&uacute;mero do Protocolo CIE'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_requerimento', 'Data de Requerimento'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('depf_no_nome', 'Delegacia PF'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_validade', 'Validade do Protocolo CIE'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Serviço'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_prazo_estada', 'Prazo Estada Atual'));
		$this->AdicioneCampoDuplo(new cCAMPO_MEMO('observacao', 'Observa&ccedil;&atilde;o'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('dt_cad', 'Data de cadastro'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('ID_TIPO_ACOMPANHAMENTO', 'Tipo de acompanhamento'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NU_SERVICO_SV', 'Serviço da OS'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('dt_ult', 'dt_ult'));
	}
}
