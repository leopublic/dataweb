<?php

/**
 *
 * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada. á
 */
class cTMPL_OS_ANDA_LIST extends cTEMPLATE_LISTAGEM {

    public $acum;
    public $os_ant;
    public $linhaPendente;

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "Andamento de processos";
        $this->mCols = array("auto", "auto", "120px","auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "120px", "170px",  "200px");
        $this->mOrdenacaoDefault = 'dt_envio_bsb desc, nu_solicitacao desc, nome_completo asc';
        $this->exibirPaginacao = true;
        // Adiciona campos da tela
        $this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
        $this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato'));

        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'tabela'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'codigo'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_status_sol'));

        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
        $this->EstilizeUltimoCampo('cen');

        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_status_sol_res', 'Status atual'));
        if (cHTTP::$MIME != cHTTP::mmXLS) {
            $this->EstilizeUltimoCampo('cen');
            $this->EstilizeUltimoCampo('button');
        }

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'acao', 'Alterar para '));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('button');

        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_envio_bsb', 'Protocolo BSB'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_solicitacao', 'Dt. solic.'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_razao_social', 'Empresa PROCESSO'));

        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato(s)'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_passaporte', '# passaporte'));
//        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_reparticao_consular', 'Repartição consular'));

        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_resumido', 'Serviço'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_embarcacao_projeto', 'Embarcação/projeto PROCESSO'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_prazo_estada_solicitado', 'Prazo pretendido'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento', 'Dt. requerimento'));
        if (cHTTP::$MIME != cHTTP::mmXLS) {
            $this->EstilizeUltimoCampo('editavel');
            $this->EstilizeUltimoCampo('cen');
        }

        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_protocolo', 'Protocolo'));
        if (cHTTP::$MIME != cHTTP::mmXLS) {
            $this->EstilizeUltimoCampo('editavel');
            $this->EstilizeUltimoCampo('processo');
            $this->EstilizeUltimoCampo('cen');
        }

        $this->AdicioneCampo(new cCAMPO_MEMO('de_observacao_bsb', 'Observações'));
        $this->EstilizeUltimoCampo('editavel');

        // $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('dt_envio_bsb', 'Protocolo do dia', cCOMBO::cmbULTIMOS_PROTOCOLOS_BSB));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_ini', 'Cadastradas desde'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_fim', 'até'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
        $this->mFiltro['nu_empresa']->mQualificadorFiltro = 's';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
        $this->mFiltro['nu_solicitacao']->mQualificadorFiltro = 's';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('id_status_sol', 'Status', cCOMBO::cmbSTATUS_SOLICITACAO));
        $this->mFiltro['id_status_sol']->mQualificadorFiltro = 's';
        $this->AdicioneFiltroInvisivel('orgao_protocolo', 'MENOS_PRE');
        $this->mFiltro['orgao_protocolo']->mWhere = ' 1=1';

        $onclick = "window.open('carregueComMenu.php?controller=cCTRL_OS_ANDA_LIST_EXCEL&metodo=Liste', '_blank');";
        $this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel", "EXP_EXCEL", "Clique para exportar em Excel", $onclick));
    }

    public function CustomizarCabecalho() {
        $nomeArq = 'protocolo_bsb';
        // if($this->getValorFiltro('dt_envio_bsb') != ''){
        // 	$nomeArq .= "_".str_replace("/", "_", $this->getValorFiltro('dt_envio_bsb'));
        // }
        cHTTP::set_nomeArquivo($nomeArq . '.xls');
        $this->mFiltro['id_status_sol']->mExtra = " and id_status_sol in (".cSTATUS_SOLICITACAO::statusDoAndamento().")";
    }

    public function RenderizeCabecalhoListagem() {
        parent::RenderizeCabecalhoListagem();
        $this->linhaPendente = false;
    }

    public function CustomizarLinha() {
        $this->FormateCampoData('dt_solicitacao');
        $this->FormateCampoData('dt_envio_bsb');
        $this->FormateCampoData('dt_requerimento');

        $link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $this->getValorCampo('nu_candidato') . '" target="_blank">' . $this->getValorCampo('nome_completo') . '</a>';
        $this->setValorCampo('nome_completo', $link);

        $link = cCTRL_ORDEMSERVICO::LinkProcessoOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), '_blank');
        $this->setValorCampo('nu_solicitacao', $link);

        if (cHTTP::$MIME != cHTTP::mmXLS) {
            switch ($this->getValorCampo('tabela')) {
                case 'prorrog':
                case 'cancel':
                case 'mte':
                    $parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "AtualizaNumeroProcesso"';
                    $parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
                    $parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
                    $parametros.= '}';
                    $this->mCampo['nu_protocolo']->set_parametros($parametros);

                    $parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "AtualizadtRequerimento"';
                    $parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
                    $parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
                    $parametros.= '}';
                    $this->mCampo['dt_requerimento']->set_parametros($parametros);
                    break;

//                    $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_PROCPROR', 'dt_requerimento', 'id_solicita_visto');
//                    $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_PROCPROR', 'nu_protocolo', 'id_solicita_visto');
//                    $this->DesprotejaCampo('dt_requerimento');
//                    $this->DesprotejaCampo('nu_protocolo');
//                    $this->DesprotejaCampo('de_observacao_bsb');
//                    break;
//                    $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_PROCCANC', 'dt_processo', 'id_solicita_visto', 'dt_requerimento');
//                    $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_PROCCANC', 'nu_processo', 'id_solicita_visto', 'nu_protocolo');
//                    $this->DesprotejaCampo('dt_requerimento');
//                    $this->DesprotejaCampo('nu_protocolo');
//                    $this->DesprotejaCampo('de_observacao_bsb');
//                    break;
//                    $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_PROCMTE', 'dt_requerimento', 'id_solicita_visto');
//                    $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_PROCMTE', 'nu_processo', 'id_solicita_visto', 'nu_protocolo');
//                    $this->DesprotejaCampo('dt_requerimento');
//                    $this->DesprotejaCampo('nu_protocolo');
//                    $this->DesprotejaCampo('de_observacao_bsb');
//                    break;
                default:
                    $this->ProtejaCampo('dt_requerimento');
                    $this->ProtejaCampo('nu_protocolo');
                    $this->ProtejaCampo('de_observacao_bsb');
            }

            $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'de_observacao_bsb', 'id_solicita_visto');


            $links = '';
            $os = new cORDEMSERVICO();
            $os->mID_STATUS_SOL = $this->getValorCampo('id_status_sol');
            $os->mID_SOLICITA_VISTO = $this->getValorCampo('id_solicita_visto');
            foreach (cciclo_vida_os::acoesDisponiveis($os) as $acao) {
                $links .= $acao->emHtml();
            }

//			if($this->getValorCampo('id_status_sol') == 4 && $this->getValorCampo('nu_protocolo') == ''){
//				$parametros = '{"controller": "cCTRL_UPDT_SOLV"';
//				$parametros.= ', "metodo": "IndicaPendenciaBSB"';
//				$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
//				$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
//				$parametros.= '}';
//				$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Pendência</a>';
//				$parametros = '{"controller": "cCTRL_UPDT_SOLV"';
//				$parametros.= ', "metodo": "IndicaAceitaBSB"';
//				$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
//				$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
//				$parametros.= '}';
//				$links .= '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Aceita</a>';
//			}
//			elseif($this->getValorCampo('id_status_sol') == 11 && $this->getValorCampo('nu_protocolo') == ''){
//				$parametros = '{"controller": "cCTRL_UPDT_SOLV"';
//				$parametros.= ', "metodo": "LiberaPendenciaBSB"';
//				$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
//				$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
//				$parametros.= '}';
//				$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Autorizar</a>';
//			}
            $this->setValorCampo('acao', $links);
        }
    }

    public function RenderizeLinha() {
        // Se mudou a OS e não é o primeiro registro, então descarrega o que está acumulado
//		error_log('Nova linha '.$this->getValorCampo('nu_solicitacao').' '.$this->getValorCampo('nome_completo'));
        if ($this->os_ant != '' && $this->os_ant != $this->getValorCampo('id_solicita_visto')) {
            // Salva a linha atual, que deve depois ser adicionada ao buffer
            $xlinha = array();
            foreach ($this->mCampo as $campo) {
                $xlinha[$campo->mCampoBD] = clone $campo;
            }
            // Descarrega o buffer
            $this->RenderizeBuffer();

            //Inicializa o buffer com a linha atual salva
            unset($this->bufferLinhas);
//			error_log('Zerou buffer e restaurou ultima linha salva '.$xlinha['nu_solicitacao']->mValor.' '.$xlinha['nome_completo']->mValor);
            $this->mCampo = $xlinha;
        }
        // Clona o conteúdo dos campos
        $xCampos = array();
        foreach ($this->mCampo as $campo) {
            $xCampos[$campo->mCampoBD] = clone $campo;
        }
        // Adiciona o registro no buffer.
//		error_log('-->Linha que esta indo para o buffer '.$xCampos['nu_solicitacao']->mValor.' '.$xCampos['nome_completo']->mValor);
        $this->bufferLinhas[] = $xCampos;
//		error_log('-->Armazenada no buffer. Tam buffer:'.count($this->bufferLinhas));
        $this->os_ant = $this->getValorCampo('id_solicita_visto');
        $this->linhaPendente = true;
    }

    /**
     * Descarrega o buffer para a tela
     */
    public function RenderizeBuffer() {
        // Identifica quantas linhas foram armazenadas
        $qtd = count($this->bufferLinhas);
        if ($qtd == 1) {
            $this->mEstiloLinha = "bloco";
            $this->mCampo = $this->bufferLinhas[0];
            $this->VisibilidadeCamposFixos(true);
            $this->ConfigureQtdLinhasDetalhe('');
            parent::RenderizeLinha();
        } else {
            $i = 0;
            foreach ($this->bufferLinhas as $linha) {
                $this->mCampo = $linha;
                // Imprime as colunas fixas na primeira linha
                if ($i == 0) {
                    $this->VisibilidadeCamposFixos(true);
//					$this->mCampo = $this->bufferLinhas[0];
                    $this->ConfigureQtdLinhasDetalhe($qtd);
                }
                // Esconde as colunas fixas e imprime os detalhes
                else {
                    $this->mEstiloLinha = "";
                    $this->VisibilidadeCamposFixos(false);
                }
                parent::RenderizeLinha();
                $i++;
            }
        }
    }

    public function VisibilidadeCamposFixos($pvisibilidade) {
//		if(cHTTP::$MIME != cHTTP::mmXLS){
//			$this->mCampo['acao']->mVisivel = $pvisibilidade;
//		}
        $this->mCampo['dt_envio_bsb']->mVisivel = $pvisibilidade;
        $this->mCampo['dt_requerimento']->mVisivel = $pvisibilidade;
        $this->mCampo['nu_protocolo']->mVisivel = $pvisibilidade;
        $this->mCampo['nu_solicitacao']->mVisivel = $pvisibilidade;
        $this->mCampo['dt_solicitacao']->mVisivel = $pvisibilidade;
        $this->mCampo['dt_prazo_estada_solicitado']->mVisivel = $pvisibilidade;
        $this->mCampo['no_razao_social']->mVisivel = $pvisibilidade;
        $this->mCampo['no_embarcacao_projeto']->mVisivel = $pvisibilidade;
        $this->mCampo['no_servico_resumido']->mVisivel = $pvisibilidade;
        $this->mCampo['no_status_sol_res']->mVisivel = $pvisibilidade;
        $this->mCampo['de_observacao_bsb']->mVisivel = $pvisibilidade;
    }

    public function ConfigureQtdLinhasDetalhe($pqtd) {
//		$this->mCampo['acao']->mRowspan = $pqtd;
        $this->mCampo['dt_envio_bsb']->mRowspan = $pqtd;
        $this->mCampo['dt_requerimento']->mRowspan = $pqtd;
        $this->mCampo['nu_protocolo']->mRowspan = $pqtd;
        $this->mCampo['nu_solicitacao']->mRowspan = $pqtd;
        $this->mCampo['dt_solicitacao']->mRowspan = $pqtd;
        $this->mCampo['dt_prazo_estada_solicitado']->mRowspan = $pqtd;
        $this->mCampo['no_razao_social']->mRowspan = $pqtd;
        $this->mCampo['no_embarcacao_projeto']->mRowspan = $pqtd;
        $this->mCampo['no_servico_resumido']->mRowspan = $pqtd;
        $this->mCampo['no_status_sol_res']->mRowspan = $pqtd;
        $this->mCampo['de_observacao_bsb']->mRowspan = $pqtd;
    }

    public function RenderizeListagemRodape() {
        $this->RenderizeBuffer();
        parent::RenderizeListagemRodape();
    }

}
