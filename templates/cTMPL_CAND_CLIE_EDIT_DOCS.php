<?php
class cTMPL_CAND_CLIE_EDIT_DOCS extends cTEMPLATE_EDICAO_TWIG{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->set_titulo('New Applicant');
		$this->set_titulo_painel('Personal Document');
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('NOME_COMPLETO'));

		$this->AdicionePainel("Passaporte", "Passport information");
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_PASSAPORTE', "Número do passaporte", null ,"Number"));
		$this->AdicionePainel("", "");
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('CO_PAIS_EMISSOR_PASSAPORTE', "País emissor", cCOMBO::cmbPAIS ,"Issuing government"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_EMISSAO_PASSAPORTE', "Data de emissão", null ,"Issue date"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_VALIDADE_PASSAPORTE', "Data de expiração", null ,"Expiry date"));

		$this->AdicionePainel("Seaman's book");
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SEAMAN', "Número do Seaman's book", null ,"Seaman's book number"));
		$this->AdicionePainel("", "");
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_EMISSAO_SEAMAN', "Data de emissão", null ,"Issue date"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_VALIDADE_SEAMAN', "Data de expiração", null ,"Expiry date"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('CO_PAIS_SEAMAN', "País emissor", cCOMBO::cmbPAIS, "Issuing government"));
		$this->mCampo['CO_PAIS_SEAMAN']->mPermiteDefault = true;

		$this->AdicionePainel("RNE");
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_RNE', "Number"));
	}
}
