<?php

class cTMPL_PROCESSO_EDIT_OS_PRORROG extends cTMPL_PROCESSO_EDIT_OS {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'cd_usuario_pre_cadastro'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_protocolo', 'Número do Protocolo'));
        $this->EstilizeUltimoCampo('processo');
        $this->mCampo['nu_protocolo']->mlinkCertidao = true;
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_envio_bsb', 'Data de envio BSB'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento', 'Data de Requerimento'));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('id_tipo_envio', 'Método de envio do processo', cCOMBO::cmbTIPO_ENVIO));
//		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_validade', 'Validade do Protocolo'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_prazo_pret', 'Prazo Pretendido'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_publicacao_dou', 'Data publicação DOU'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCHAVE_ESTR, 'id_status_sol', 'Status', cCOMBO::cmbSTATUS_SOLICITACAO_PROCESSOS));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_pagina_dou', 'Número página DOU'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'observacao', 'Observação'));
        $this->AdicionePainel('Pré-cadastro');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_pre_cadastro', 'Número do pré-cadastro'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_pre_cadastro', 'Data do pré-cadastro'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_pre_cadastro', 'Pré-cadastro por'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_envio_bsb_pre_cadastro', 'Data de envio BSB'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento_pre_cadastro', 'Data de requerimento'));
        $this->mCampo['observacao']->mLarguraDupla = true;

        $this->AdicionarCamposCobranca();
    }

    public function CustomizeCabecalho() {
        unset($this->mAcoes['SALVAR_E_FECHAR']);
        if ($this->getValorCampo('NO_SERVICO_RESUMIDO')) {
            $titulo = $this->getValorCampo('NO_SERVICO_RESUMIDO');
        } else {
            $titulo = "Prorrogação";
        }
        $this->mTitulo = $titulo;
        parent::CustomizeCabecalho();
        $this->FormateCampoData('dt_pre_cadastro');
        $this->FormateCampoData('dt_requerimento_pre_cadastro');

        $this->setValorCampo('id_tipo_envio', $this->os->mid_tipo_envio);

        if ($this->getValorCampo('cd_usuario_pre_cadastro') != '') {
            $usua = new cusuarios();
            $usua->RecupereSe($this->getValorCampo('cd_usuario_pre_cadastro'));
            $this->setValorCampo('nome_pre_cadastro', $usua->nome);
        }
        if ($this->os->mReadonly) {
            $this->mCampo['id_status_sol']->mReadonly = false;
            $this->mCampo['id_status_sol']->mDisabled = true;
            $this->mCampo['id_tipo_envio']->mReadonly = false;
            $this->mCampo['id_tipo_envio']->mDisabled = true;
        }
    }

}
