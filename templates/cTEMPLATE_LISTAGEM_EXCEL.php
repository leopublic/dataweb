<?php
/**
 * Template especializado para a geração de grids
 * 
 * TODO: Incompleto
 */
class cTEMPLATE_LISTAGEM_EXCEL extends cTEMPLATE_LISTAGEM{
	/**
	 * Métodos
	 */	
	public function __construct($pNomeTemplate) {
		parent::__construct($pNomeTemplate);
	}
	/**
	 * Gera o html do template
	 * @param type $pcursor
	 */
	public function Renderize($pcursor){
		$this->OutputInicializar();
		$this->GerarOutput('<head><style>td{border: thin solid #000;}</style></head><body><table>');
		$this->CustomizarCabecalho();
		
		$this->RenderizeTitulo();
		$this->RenderizeCabecalhoListagem();
		// Gera linha em branco para adição de registros, se for o caso
	
		$this->RenderizeLinhaInclusao();
		if (is_object($pcursor)){
			if($pcursor->rowCount() > 0){
				$this->LoopProcessamento($pcursor);
			}
		}
		
		$this->RenderizeListagemRodape();
		if ($this->get_qtdLinhas() == 0){
			$this->GerarOutput('<br/>Nenhum registro encontrado');
		}
		$this->GerarOutput('</table></body>');
	}	
	
	public function RenderizeTitulo() {		
		$ret = '<tr><td style="font-size:14pt; font-weight:bold;" colspan="'.$this->get_qtdColunasVisiveis().'">'.$this->mTitulo.'</td></tr>';
		$this->GerarOutput($ret);
	}
	
	public function CustomizarCabecalho() {
		parent::CustomizarCabecalho();
		foreach($this->mCampo as &$campo){
			$campo->mOrdenavel = false;
		}
	}
	
	public function get_qtdColunasVisiveis(){
		$qtd = 0;
		foreach($this->mCampo as $campo){
			if ($campo->mTipo != cCAMPO::cpHIDDEN && $campo->mVisivel && $campo->mCampoBD != 'SELETOR'){
				$qtd++;
			}
		}
		return $qtd;
	}
	
	public function FiltrosEmUso(){
		$ret = '';
		$virg = '';
		foreach($this->mFiltro as $filtro){
			if($filtro->mValor != ''){
				$ret .= $virg.$filtro->mLabel.': '.$filtro->mValor;
				$virg = ', ';
			}
		}
		return $ret;
	}
}
