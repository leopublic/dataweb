<?php
class cTMPL_PROCESSO_EDIT_OS_GENERICO extends cTMPL_PROCESSO_EDIT_OS{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'de_observacao', 'Observação'));
		$this->mCampo['de_observacao']->mLarguraDupla = true;
		
		$this->AdicionarCamposCobranca();
	}
	
	public function CustomizeCabecalho() {
		parent::CustomizeCabecalho();
		$this->mTitulo = $this->os->mNO_SERVICO_RESUMIDO;
	}
}
