<?php
class cTMPL_OS_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "";
		cHTTP::$comCabecalhoFixo = false;
		$this->mCols = array("130px", "100px", "auto");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'qtd', '#'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_status_sol_res', 'Status'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_razao_social', 'Empresa'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_embarcacao_projeto', 'Embarcação/Projeto'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato(s)'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_embarcacao_projeto_atual', 'Embarcação/Projeto candidato'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_resumido', 'Serviço'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'data_solicitacao', 'Criada em'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_responsavel', 'responsavel'));
	}

}
