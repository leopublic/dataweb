<?php
class cTMPL_RESUMO_COBRANCA_LISTAR extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->set_exibirPaginacao(true);
		$this->mTitulo = "Resumos de cobrança";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->mCols = array("80px", "90px", "60px", "70px", "auto", "auto", "80px", "80px", "80px", "auto");
		$this->mOrdenacaoDefault = "resc_dt_faturamento desc";
		cHTTP::$comCabecalhoFixo = true;
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'resc_id'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_prestadora', 'Prestadora'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empf_tx_descricao', 'Mundivisas'));
		$this->AdicioneColunaEditavel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_nu_numero_mv', 'N&ordm; do RC (MV)'), 'cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento', 'Dt. fat.'), 'cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa COBRANÇA'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO', 'Emb./projeto REAL'));
		$this->AdicioneColunaEditavel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_tx_solicitante', 'Solicitante'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'strc_tx_nome', 'Situação'), 'cen');
		$this->AdicioneColunaEditavel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_tx_nota_fiscal', 'N&ordm; da NF'), 'cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_recebimento', 'Recebida em'), 'cen');
//		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'soli_vl_cobrado', 'Total (R$)'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'soli_vl_cobrado', 'Valor total (R$)'), 'dir');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'resc_tx_observacoes', 'Observações'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_prestadora', "Prestadora", cCOMBO::cmbEMPRESA_PRESTADORA));
		$this->mFiltro['nu_empresa_prestadora']->mQualificadorFiltro = 'r';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_requerente', "Empresa", cCOMBO::cmbEMPRESA_DA_PRESTADORA));
		$this->mFiltro['nu_empresa_requerente']->mQualificadorFiltro = 'r';
		$this->VinculeFiltros('nu_empresa_requerente', 'nu_empresa_prestadora');
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj.', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
		$this->mFiltro['NU_EMBARCACAO_PROJETO_COBRANCA']->mQualificadorFiltro = 'r';
		$this->VinculeFiltros('NU_EMBARCACAO_PROJETO_COBRANCA', 'nu_empresa_requerente');
		cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieFiltros($this, $this->mFiltro['nu_empresa_requerente'], $this->mFiltro['NU_EMBARCACAO_PROJETO_COBRANCA']));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_nu_numero_mv', "Número RC"));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento_de', 'Data de faturamento de'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento_ate', 'até'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'pre2012', "Mostrar pré 01/12/2012?"));

		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR",  "Clique para alterar esse item", "cCTRL_RESUMO_COBRANCA", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Tree.png", "LISTE_OS",  "Listar as OS desse resumo", "cCTRL_RESUMO_COBRANCA", "ListeOs"));
		//$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Mail.png", "EMITIR_FATURA",  "Indicar o número da nota emitida", "cCTRL_RESUMO_COBRANCA", "IndiqueNotaFiscal"));
		//$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Ok.png", "RECEBER",  "Indicar recebimento dessa fatura", "cCTRL_RESUMO_COBRANCA", "Receber"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Trash.png", "EXCLUIR",  "Clique para excluir esse item", "cCTRL_RESUMO_COBRANCA", "Exclua", "Confirma a exclusão do resumo? As OS que estavam associadas a esse resumo voltarão ao status liberadas para cobrança. Essa operação não poderá ser desfeita.", "Atenção"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Download.png", "RELATORIO",  "Clique para gerar uma cópia em excel desse item", "cCTRL_RESUMO_COBRANCA_IMPRESSO", "Gerar"));
		$this->mAcoesLinha['RELATORIO']->mTarget = "_blank"	;
		$this->mAcoesLinha['RELATORIO']->mModo = cACAO_LINK_CONTROLER_METODO::modoREDIRECT_SEM_MENU ;
	}
        
	public function CustomizarLinha() {
		$this->mAcoes['RELATORIO']->mParametros = '&resc_id='.$this->getValorCampo('resc_id');
		$this->FormateCampoData('resc_dt_faturamento');
		$this->FormateCampoVazioComTracos('resc_dt_recebimento');
		$this->FormateCampoVazioComTracos('resc_tx_nota_fiscal');
		
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_RESC', 'resc_nu_numero_mv', 'resc_id');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_RESC', 'resc_tx_solicitante', 'resc_id');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_RESC', 'resc_tx_nota_fiscal', 'resc_id');
	}
}
