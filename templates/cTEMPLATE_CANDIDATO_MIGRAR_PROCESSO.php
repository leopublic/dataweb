<?php

/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_CANDIDATO_MIGRAR_PROCESSO extends cTEMPLATE {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mCampo[cTEMPLATE::CMP_ALTURA]->mValor = '300';
        $this->mCampo[cTEMPLATE::CMP_LARGURA]->mValor = '850';
        $this->mCampo[cTEMPLATE::CMP_TITULO]->mValor = "Migrar este processo para outro candidato";
        $this->mMsgInicial = "Selecione um candidato e clique em migrar";
        // Adiciona campos da tela
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO_ORIGEM'));
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('tabela'));
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('codigo'));

        $this->AdicioneCampo(new cCAMPO_TEXTO('NOME_COMPLETO_ORIG', 'Candidato origem'));
        $this->mCampo['NOME_COMPLETO_ORIG']->mReadonly = 1;
        $this->AdicioneCampo(new cCAMPO_TEXTO('xxx', ''));
        $this->mCampo['xxx']->mReadonly = 1;
        $this->AdicioneCampo(new cCAMPO_TEXTO('NOME_COMPLETO_DESTINO', 'Candidato destino'));
        $this->AdicioneCampo(new cCAMPO_TEXTO('yyyy', ''));
        $this->mCampo['yyyy']->mReadonly = 1;
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('NU_CANDIDATO_DESTINO', '&nbsp;', cCOMBO::cmbCANDIDATO_PROCESSO));
        $this->AdicioneCampo(new cCAMPO_TEXTO('zzz', ''));
        $this->mCampo['zzz']->mReadonly = 1;
        $this->AdicioneCampo(new cCAMPO_SIMNAO('MIGRAR_ARQUIVOS', 'Migrar também os arquivos?'));
        $this->AdicioneCampo(new cCAMPO_TEXTO('aaa', ''));
        $this->mCampo['aaa']->mReadonly = 1;
        $this->AdicioneCampo(new cCAMPO_SIMNAO('MIGRAR_ASSOCIADOS', 'Migrar também os processos associados a esse visto?'));
        // Adiciona as ações que estarão disponíveis na tela
        $this->AdicioneAcao(new cACAO_METODO_POPUP_POST("Pesquisar candidatos", "PESQUISAR", "Clique para pesquisar pelo candidato destino"));
        $this->AdicioneAcao(new cACAO_METODO_POPUP_POST("Migrar", "MIGRAR", "Clique para migrar esse processo do candidato origem para o candidato destino"));
    }

}
