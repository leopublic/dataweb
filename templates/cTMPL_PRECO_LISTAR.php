<?php
class cTMPL_PRECO_LISTAR extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Tabelas de pre&ccedil;o - Pre&ccedil;os por servi&ccedil;o";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->mCols = array("50px", "80px", "auto", "auto", "100px", "100px");
		cHTTP::$comCabecalhoFixo = true;
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'prec_id'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'tbpc_id'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_SERVICO'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'prec_tx_item', 'Item'));
		$this->mCampo['prec_tx_item']->mClasse = "cen";
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'prec_tx_descricao_tabela_precos', 'Descrição'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Servi&ccedil;o'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'prec_vl_pacote', 'Valor pacote(R$)'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'prec_vl_conceito', 'Valor conceito(R$)'));
//		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Criar nova", "ADICIONAR", "Clique para adicionar um novo", "cCTRL_TABELA_PRECOS", "Edite"));

		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para alterar esse item", "cCTRL_PRECO", "Edite"));
	}
}
