<?php
class cTMPL_OS_PIT_LIST extends cTEMPLATE_LISTAGEM{
	protected $QT_CANDIDATOS_TOTAL;
	protected $QT_CANDIDATOS_ATIVOS_TOTAL;
	protected $QT_CANDIDATOS_REVISADOS_TOTAL;
	public $chavesSelecionadas;
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "PIT - Ordens de serviço";
		$this->mOrdenacaoDefault = 'dt_solicitacao desc, nome_completo asc';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "60px", "300px", "300px", "300px", "auto");
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato'));

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_solicitacao', 'Mês/ano'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Estrangeiro'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_razao_social', 'Empresa'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_embarcacao_projeto', 'Emb./proj.'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'anexos', 'Anexos'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', "Estrangeiro"));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa', "Empresa", cCOMBO::cmbEMPRESA_ATIVA));
		$this->mFiltro['nu_empresa']->mQualificadorFiltro = 'sv';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_embarcacao_projeto', "Embarcação/projeto", cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
		$this->mFiltro['nu_embarcacao_projeto']->mQualificadorFiltro = 'c';
		$this->VinculeFiltros('nu_embarcacao_projeto', 'nu_empresa');
		
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('mes', "Mês", cCOMBO::cmbMES));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ano', "Ano"));
		
//		$onclick = "window.open('carregueComMenu.php?controller=cCTRL_EMBP_LIST_EXCEL&metodo=Liste', '_blank');";
//		$this->AdicioneAcao(new cACAO_JAVASCRIPT ("Excel", "EXP_EXCEL",  "Clique para exportar em Excel", $onclick));		
	}

	public function CustomizarCabecalho() {
		$this->QT_CANDIDATOS_TOTAL =0;
	}
	
	public function CustomizarLinha()
	{
		$this->QT_CANDIDATOS_TOTAL++;

		$nomeComLink = $this->getValorCampo('nome_completo').' ('.$this->getValorCampo('nu_candidato').')';
		$link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$this->getValorCampo('nu_candidato').'" target="_blank">'.$nomeComLink.'</a>';
		$this->setValorCampo('nome_completo', $link);
		
		$this->FormateCampoData('dt_solicitacao');

		$this->setValorCampo('nu_solicitacao', cCTRL_ORDEMSERVICO::LinkProcessoOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), '_blank'));
//		$this->setValorCampo('anexos', cCTRL_ORDEMSERVICO::ArquivosDaOSResumido($this->getValorCampo('id_solicita_visto')));
		
	}

}
