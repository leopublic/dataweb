<?php
class cTMPL_TPRC_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Tabelas de preço";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->mCols = array("100px", "auto", "80px", "80px", auto);
		cHTTP::$comCabecalhoFixo = true;

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'tbpc_id'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tbpc_tx_nome', 'Nome'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tbpc_nu_ano', 'Ano'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'qtdEmpresas', '#clientes'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tbpc_tx_observacao', 'Observação'));
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Adicionar", "ADICIONAR", "Clique para adicionar um novo", "cCTRL_TPRC_EDIT", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para alterar a descrição dessa tabela", "cCTRL_TPRC_EDIT", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Currency Dollar.png", "LISTAR_VALORES", "Clique para listar os preços dessa tabela", "cCTRL_PREC_LIST", "Liste"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Applications.png", "CLONAR", "Clique para clonar essa tabela", "cCTRL_TABELA_PRECOS", "Clonar"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Trash.png", "EXCLUIR", "Clique para excluir esse item", "cCTRL_TABELA_PRECOS", "Exclua", "Deseja mesmo excluir essa tabela de preços? Essa ação não poderá ser desfeita."));
	}
}
