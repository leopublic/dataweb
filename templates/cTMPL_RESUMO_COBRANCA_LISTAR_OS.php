<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_RESUMO_COBRANCA_LISTAR_OS extends cTEMPLATE_LISTAGEM{
	public $cabecalhoExternas ;
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Controle de cobran&ccedil;as - Ordens de Servi&ccedil;o";
		$this->mOrdenacaoDefault = "s.nu_solicitacao";
		cHTTP::$comCabecalhoFixo = true;
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('resc_id'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('resc_id_real'));

		$this->AdicioneColuna(new cCAMPO_HIDDEN('resc_id_solicitacao'));
		$this->AdicioneColuna(new cCAMPO_LINK_EXTERNO('nu_solicitacao', 'OS', '/operador/os_DadosOS.php'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL', 'Empresa COBRANÇA'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarcação/projeto REAL'));
		$this->AdicioneColunaEditavel(new cCAMPO_TEXTO('soli_tx_solicitante_cobranca', 'Solicitante'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Candidato(s)'));
		$this->mCampo['NOME_COMPLETO']->mOrdenavel = false;
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Serviço'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'item', 'Item'), 'cen');
		$this->AdicioneColunaEditavel(new cCAMPO_VALOR_REAL('soli_vl_cobrado', 'Valor (R$)'));

		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Gerar Excel", "GERAR_EXCEL", "Gerar uma versão em Excel", "cCTRL_RESUMO_COBRANCA", "ListeOsExcel"));
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Resumo", "RESUMO", "Gerar o resumo de cobrança", "cCTRL_RESUMO_COBRANCA_IMPRESSO", "Gerar"));
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Atualizar preços", "ATUALIZA_PRECOS", "Atualizar os valores com o valor de tabela", "cCTRL_RESUMO_COBRANCA", "atualize_precos"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Plus.png", "ADICIONAR", "Adicionar essa OS ao resumo de cobranca", "cCTRL_RESUMO_COBRANCA", "AdicioneOs"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Minus.png", "REMOVER", "Retirar essa OS do resumo de cobranca", "cCTRL_RESUMO_COBRANCA", "RemovaOs"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "EDITAR_FINANCEIRO", "Editar informações financeiras", "cCTRL_OS_ABA_COBR_EDIT", "Edite"));
	}
	public function CustomizarCabecalho() {
		$this->mAcoes['GERAR_EXCEL']->mParametros = '&resc_id='.$this->getValorCampo('resc_id');
		$this->mAcoes['GERAR_EXCEL']->mpagina = 'carregueComoExcel.php';
		$this->mAcoes['RESUMO']->mParametros = '&resc_id='.$this->getValorCampo('resc_id');
		$this->mAcoes['RESUMO']->mpagina = 'carregueSemMenu.php';
		$this->mAcoes['RESUMO']->mTarget = '_blank';
		$this->mAcoes['ATUALIZA_PRECOS']->mParametros = '&resc_id='.$this->getValorCampo('resc_id');

		$this->cabecalhoExternas = false;
	}

	public function CustomizarLinha() {
		$this->FormateCampoValor('soli_vl_cobrado', 2);
		if ($this->mCampo['resc_id_solicitacao']->mValor == '' ){
			$this->mAcoesLinha['ADICIONAR']->mVisivel = true;
			$this->mAcoesLinha['REMOVER']->mVisivel = false;
			$this->mAcoesLinha['EDITAR_FINANCEIRO']->mVisivel = false;
			$this->mEstiloLinha = "fora";
		}
		else{
			$this->mAcoesLinha['ADICIONAR']->mVisivel = false;
			$this->mAcoesLinha['REMOVER']->mVisivel = true;
			$this->mAcoesLinha['EDITAR_FINANCEIRO']->mVisivel = true;
			$this->mEstiloLinha = "";
		}

		$this->setValorCampo('nu_solicitacao', cCTRL_ORDEMSERVICO::LinkProcessoOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao')));
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO = $this->getValorCampo('id_solicita_visto');
		$candidatos = $os->ObtenhaNomesCandidatos();
		$br = '' ;
		$nomes = '';
		foreach ($candidatos as $key => $value) {
			$nomes .= $br.$value;
			$br = '<br/>';
		}
		$this->setValorCampo('NOME_COMPLETO', $nomes);

		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'soli_vl_cobrado', 'id_solicita_visto');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'soli_tx_solicitante_cobranca', 'id_solicita_visto');

		if($this->getValorCampo('resc_id_real') == '' && !$this->cabecalhoExternas){
			$cab = '<tr><th colspan="9" style="text-align:left; padding: 5px; padding-top: 15px;background-color: #fff; font-weight: bold; font-size:12px;">OS liberadas para cobrança da mesma empresa que podem ser incluídas no resumo:</th></tr>';
			$this->GerarOutput($cab);
			$this->cabecalhoExternas = true;
		}
	}
}
