<?php
// à
class cTMPL_REPC_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mlocalAcoes = self::lca_RODAPE;
		$this->qtd_colunas = 1;
		// Adiciona campos da tela
		$this->set_titulo('Repartição consular');
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_REPARTICAO_CONSULAR'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_REPARTICAO_CONSULAR', 'Descrição'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO', 'Endereço'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE', 'Cidade'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_REPARTICAO_CONSULAR_ALTERNATIVO', 'Descrição alternativa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS', 'País', cCOMBO::cmbPAIS));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar","SALVAR", "Clique para editar" ));
	}
}
