<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_OS_FATURAR_POR_CC extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Faturar OS por centro de custo";
		cHTTP::$comCabecalhoFixo = true;
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('nu_empresa_requerente'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('NU_EMBARCACAO_PROJETO_COBRANCA'));
		$this->AdicioneCampo(new cCAMPO_SIMNAO('SELECIONADA', 'Sel'));
		$this->mCampo['SELECIONADA']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_LINK_EXTERNO('nu_solicitacao', 'OS', '/operador/os_DadosOS.php'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL_REQUERENTE', 'Empresa'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj.'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('no_solicitador', 'Solicitante'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Candidato(s)'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Serviço'));
		$this->AdicioneColuna(new cCAMPO_VALOR_REAL('soli_vl_cobrado', 'Valor (R$)'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_requerente', 'Empresa', cCOMBO::cmbEMPRESA));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj.', cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->mFiltro['no_solicitador']->mTipoComparacaoFiltro = cFILTRO::tpSEL_LIKE_INI;
		$this->VinculeFiltros('NU_EMBARCACAO_PROJETO_COBRANCA', 'nu_empresa_requerente');
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Gerar resumo", "GERAR_RESUMO", "Clique aqui para criar um resumo com as OS selecionadas") );
	}
}
