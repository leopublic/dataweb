<?php

/**
 *
 * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada.
 */
class cTMPL_OS_TAXA_NPAG_LIST extends cTEMPLATE_LISTAGEM {

    public $os_ant;
    public $linhaPendente;
    public $total;

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "OS encerradas sem taxa paga";
        $this->mCols = array("auto", "auto", "auto", "auto", "auto", "auto", "200px");
        cHTTP::$comCabecalhoFixo = true;
        // Adiciona campos da tela
        $this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'no_razao_social'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'no_embarcacao_projeto'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'dt_solicitacao_fmt'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nome_cad'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato'));

        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_status_sol', 'Status'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'abertura', 'Aberta por/em'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'emp_emb', 'Empresa/emb. PROCESSO'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_resumido', 'Serviço'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato(s)'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'de_observacao', 'Observações (OS)'));

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
        $this->mFiltro['nu_empresa']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('ID_STATUS_SOL', 'Situação', cCOMBO::cmbSTATUS_SOLICITACAO));
        $this->mFiltro['ID_STATUS_SOL']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_servico', 'Serviço', cCOMBO::cmbSERVICO));
        $this->mFiltro['nu_servico']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
        $this->mFiltro['nu_solicitacao']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_ini', 'Cadastradas desde'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_fim', 'até'));
    }

    public function CustomizarCabecalho() {
        parent::CustomizarCabecalho();
        $this->total = 0;
    }

    public function CustomizarLinha() {
        $this->setValorCampo('emp_emb', '<strong>' . $this->getValorCampo('no_razao_social') . '</strong><br>' . $this->getValorCampo('no_embarcacao_projeto'));
        $this->setValorCampo('abertura', $this->getValorCampo('nome_cad') . '<br>' . $this->getValorCampo('dt_solicitacao_fmt'));
        $this->setValorCampo('nu_solicitacao', cCTRL_ORDEMSERVICO::LinkDadosOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), '_blank'));

        $link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $this->getValorCampo('nu_candidato') . '" target="_blank">' . $this->getValorCampo('nome_completo') . '</a>';
        $this->setValorCampo('nome_completo', $link);
    }

}
