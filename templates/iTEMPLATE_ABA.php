<?php

interface iTEMPLATE_ABA{

	public function AdicioneAbaAjax($pNome, $pLabel, $pController, $pMetodo, $pTitulo, $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "");

	public function AdicioneAbaLink($pNome, $pLabel, $pLink, $pTitulo, $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "");

	public function Renderize();

	public function CustomizeAbas();

}
