<?php
class cTMPL_OS_ACOMP_REV_CAND extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->enveloparEmConteudoInterno  = true;
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_CANDIDATO'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NOME_COMPLETO'));

		$this->AdicionePainel('Situação cadastral');
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', 'Empresa', cCOMBO::cmbEMPRESA));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Embarca&ccedil;&atilde;o/Projeto', cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('empt_tx_descricao', 'Empresa terceirizada', cCOMBO::cmbEMPRESA_TERCEIRIZADA, "", true));
		$this->VinculeCombos('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'FL_INATIVO', 'Inativo'));
		$this->AdicionePainel('Dados pessoais');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PRIMEIRO_NOME', 'Primeiro nome'));
		$this->mCampo['NO_PRIMEIRO_NOME']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NOME_MEIO', 'Nome do meio'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ULTIMO_NOME', 'Último nome'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CPF', 'CPF'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMAIL_CANDIDATO', 'Email'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PROFISSAO_CANDIDATO', 'Profissão', cCOMBO::cmbPROFISSAO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PAI', 'Nome do pai'));
		$this->mCampo['NO_PAI']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('co_pais_nacionalidade_pai', 'Nacionalidade pai', cCOMBO::cmbNACIONALIDADE));
		$this->mCampo['co_pais_nacionalidade_pai']->mPermiteDefault = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_MAE', 'Nome da mãe'));
		$this->mCampo['NO_MAE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('co_pais_nacionalidade_mae', 'Nacionalidade mãe', cCOMBO::cmbNACIONALIDADE));
		$this->mCampo['co_pais_nacionalidade_mae']->mPermiteDefault = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_ESTADO_CIVIL', 'Estado Civil', cCOMBO::cmbESTADO_CIVIL));
		$this->mCampo['CO_ESTADO_CIVIL']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_SEXO', 'Sexo', cCOMBO::cmbSEXO));
		$this->mCampo['Sexo']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'DT_NASCIMENTO', 'Data de nascimento'));
		$this->mCampo['DT_NASCIMENTO']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_LOCAL_NASCIMENTO', 'Local de nascimento'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_NACIONALIDADE', 'Nacionalidade', cCOMBO::cmbNACIONALIDADE));
		$this->mCampo['CO_NACIONALIDADE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_NIVEL_ESCOLARIDADE', 'Escolaridade', cCOMBO::cmbESCOLARIDADE));
		$this->mCampo['CO_NIVEL_ESCOLARIDADE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TE_TRABALHO_ANTERIOR_BRASIL', 'Experiência profissional'));
		$this->AdicionePainel('Endereço no Brasil');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO_RESIDENCIA', 'Endereço'));
		$this->mCampo['NO_ENDERECO_RESIDENCIA']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE_RESIDENCIA', 'Cidade'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_CANDIDATO', 'Telefone'));
		$this->AdicionePainel('Endereço no exterior');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMPRESA_ESTRANGEIRA', 'Nome da empresa no exterior'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO_EMPRESA', 'Endereço da empresa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE_EMPRESA', 'Cidade da empresa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_EMPRESA', 'País da empresa', cCOMBO::cmbPAIS));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_EMPRESA', 'Telefone da empresa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO_ESTRANGEIRO', 'Endereço do candidato'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE_ESTRANGEIRO', 'Cidade do candidato'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_ESTRANGEIRO', 'País do candidato', cCOMBO::cmbPAIS));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_ESTRANGEIRO', 'Telefone do candidato'));
		$this->AdicionePainel('Dados do passaporte');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_PASSAPORTE', 'Número do passaporte'));
		$this->mCampo['NU_PASSAPORTE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'DT_EMISSAO_PASSAPORTE', 'Data de emissão'));
		$this->mCampo['DT_EMISSAO_PASSAPORTE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'DT_VALIDADE_PASSAPORTE', 'Data de validade'));
		$this->mCampo['DT_VALIDADE_PASSAPORTE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_EMISSOR_PASSAPORTE', 'País emissor', cCOMBO::cmbPAIS));
		$this->mCampo['CO_PAIS_EMISSOR_PASSAPORTE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_RNE', 'RNE'));
		$this->AdicionePainel('Acompanhamento');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'de_observacoes', 'Observações'));

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Revisado", "SALVAR", "Clique para indicar que o cadastro foi revisado"));
	}

	public function CustomizeCabecalho(){
		$os = new cORDEMSERVICO();
		$os->Recuperar($this->getValorCampo('id_solicita_visto'));
		$this->set_titulo("Revisar candidato ".$this->getValorCampo('NOME_COMPLETO').' na OS '.$os->mNU_SOLICITACAO);
	}
}
