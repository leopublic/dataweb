<?php
class cTEMPLATE_API{
	protected $tipo;
	protected $nome;
	protected $titulo;
	protected $titulo_painel;
	protected $menu;
	protected $menu_lateral;
	protected $campos_controle;
	protected $filtros_invisiveis;
	protected $filtros_visiveis;
	protected $campos;
	protected $linhas;
	protected $notificacao;
	protected $acoes;
	protected $complemento;

	public function __construct(){
		$this->complemento = array();
	}

	public function CarregaVisto($pvisto){
				
	}


	public function CarregaEdicao($ptmpl){
		$this->tipo = 'edit';
		$this->nome = $ptmpl->mnome;
		$this->titulo = $ptmpl->get_titulo();
		foreach($ptmpl->mCampo as $campo){
			if($campo->mVisivel && $campo->mTipo != 1){
				$this->campos[$campo->mCampoBD] = clone $campo;
			}
			else{
				$this->campos_controle[$campo->mCampoBD] = clone $campo;
			}
		}
		foreach($ptmpl->mAcoes as $acao){
			if($acao->mVisivel){
				$this->acoes[] = clone $acao;
			}
		}
	}

	public function CarregaEdicaoTwig($ptmpl){
		$this->CarregaEdicao($ptmpl);
		$this->titulo_painel = $ptmpl->get_titulo_painel();
	}

	public function AdicionaComplemento($pArray){
		$this->complemento = array_merge($this->complemento, $pArray);
	}

	public function CarregaListagem($ptmpl){

	}

	public function CarregaAbaLateral($ptmpl){
		$this->menu_lateral = $ptmpl->JSON_twig();
	}

	public function AdicionaLinhaListagem($ptmpl){

	}

	public function get_tipo(){
		return $this->tipo;
	}

	public function get_nome(){
		return $this->nome;
	}

	public function get_campos(){
		return $this->campos;
	}

	public function get_data(){
		$this->notificacao = cNOTIFICACAO::NotificacaoPendente();
		$data['pagina']['titulo'] = cHTTP::get_titulo_pagina();

		$menu = cHTTP::get_menu_esquerdo();
		if(is_object($menu)){
			$data['pagina']['menu']['esquerdo'] = $menu->JSON();
		}

		$menu = cHTTP::get_menu_direito();
		if(is_object($menu)){
			$data['pagina']['menu']['direito'] = $menu->JSON();
		}

		$data['template'] = array(
			 "tipo" 				=> $this->tipo
			,"nome" 				=> $this->nome
			,"titulo" 				=> $this->titulo
			,"titulo_painel" 		=> $this->titulo_painel
			,"menu" 				=> $this->menu
			,"campos_controle"		=> $this->campos_controle
			,"filtros_invisiveis"	=> $this->filtros_invisiveis
			,"filtros_visiveis"		=> $this->filtros_visiveis
			,"campos"				=> $this->campos
			,"linhas"				=> $this->linhas
			,"notificacao"			=> $this->notificacao
			,"acoes"				=> $this->acoes
			,"menu_lateral"			=> $this->menu_lateral
			,"complemento"			=> $this->complemento
		);
		return $data;
	}
}
