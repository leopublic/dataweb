<?php
class cTMPL_TSRV_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Tipos de serviços";
		$this->mOrdenacaoDefault = 'NO_TIPO_SERVICO';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "auto", "auto");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_TIPO_SERVICO'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_SERVICO', 'Descrição'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'CO_TIPO_SERVICO', 'Mnemônico'));
		$this->EstilizeUltimoCampo('cen');
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar um novo tipo de serviço", "cCTRL_TSRV_EDIT", "Edite" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_TSRV_EDIT", "Edite" ));
	}
}
