<?php
class cTMPL_TSRV_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mlocalAcoes = self::lca_RODAPE;
		$this->qtd_colunas = 1;
		// Adiciona campos da tela
		$this->set_titulo('Inclusão/Alteração de Tipo de serviço');
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_TIPO_SERVICO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_SERVICO', 'Descrição'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'CO_TIPO_SERVICO', 'Mnemônico'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar","SALVAR", "Clique para editar" ));
	}
}
