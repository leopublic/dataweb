<?php

/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_RESUMO_COBRANCA_LISTAR_OS_EXCEL extends cTEMPLATE_LISTAGEM_EXCEL {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
        $this->AdicioneColuna(new cCAMPO_HIDDEN('ID_TIPO_ACOMPANHAMENTO'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_solicitador', 'Solicitante'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Candidato(s)'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_processo', 'Processo'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_responsavel', 'Responsável'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Serviço'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao', 'Data serviço'));
        $this->EstilizeUltimoCampo('cen');

        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_id', 'Resumo'));
        $this->mFiltro['resc_id']->mQualificadorFiltro = 'rc';
    }

    public function CustomizarCabecalho() {
        $resumo = new cresumo_cobranca();
        $resumo->mresc_id = $this->getValorFiltro('resc_id');
        $resumo->RecuperePeloId();
        $emp = new cEMPRESA();
        $emp->mNU_EMPRESA = $resumo->mnu_empresa_requerente;
        $emp->RecuperePeloId();
        $emb = new cEMBARCACAO_PROJETO();
        if ($resumo->mNU_EMBARCACAO_PROJETO_COBRANCA > 0){
            $emb->mNU_EMPRESA = $resumo->mnu_empresa_requerente;
            $emb->mNU_EMBARCACAO_PROJETO = $resumo->mNU_EMBARCACAO_PROJETO_COBRANCA;
            $emb->RecupereSe();
        } else {
            $emb->mNU_EMBARCACAO_PROJETO = '--';
        }
        if ($emp->mnu_empresa_prestadora == 2) {
            $nome_prestadora = 'BCS Visas';
        } else {
            $nome_prestadora = 'Mundivisas';
        }
        $this->mTitulo = 'Resumo de cobrança No' . $resumo->mresc_nu_numero . ' - ' . $resumo->mresc_dt_faturamento . ' (' . $emp->mNO_RAZAO_SOCIAL . ' - ' . $emb->mNO_EMBARCACAO_PROJETO . ') - Prestadora: ' . $nome_prestadora;
        parent::CustomizarCabecalho();
    }

    public function CustomizarLinha() {
        $os = new cORDEMSERVICO();
        $os->mID_SOLICITA_VISTO = $this->getValorCampo('id_solicita_visto');
        $candidatos = $os->ObtenhaNomesCandidatos();
        $br = '';
        $nomes = '';
        foreach ($candidatos as $key => $value) {
            $nomes .= $br . $value;
            $br = '<br/>';
        }
        $this->setValorCampo('NOME_COMPLETO', $nomes);
        $processo = cprocesso::FabricaProcesso($this->getValorCampo('ID_TIPO_ACOMPANHAMENTO'));
        if (method_exists($processo, 'RecupereSePelaOsSemCand')) {
            $processo->RecupereSePelaOsSemCand($this->getValorCampo('id_solicita_visto'));
            $this->setValorCampo('nu_processo', $processo->get_nu_processo());
        } else {
            $this->setValorCampo('nu_processo', '(n/a)');
        }
    }

}
