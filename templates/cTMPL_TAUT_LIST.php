<?php
class cTMPL_TAUT_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Tipos de autorização";
		$this->mOrdenacaoDefault = 'NO_TIPO_AUTORIZACAO';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "auto", "auto", "auto");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_TIPO_AUTORIZACAO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_AUTORIZACAO', 'Descrição'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_REDUZIDO_TIPO_AUTORIZACAO', 'Abreviada'));
		$this->EstilizeUltimoCampo('editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_AUTORIZACAO', 'Descrição'));
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar um novo estado civil", "cCTRL_TAUT_EDIT", "Edite" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_TAUT_EDIT", "Edite" ));
	}

	public function CustomizarLinha()
	{
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_TAUT", "NO_TIPO_AUTORIZACAO", "CO_TIPO_AUTORIZACAO");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_TAUT", "NO_REDUZIDO_TIPO_AUTORIZACAO", "CO_TIPO_AUTORIZACAO");
	}

}
