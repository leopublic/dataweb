<?php

class cTEMPLATE_EDICAO extends cTEMPLATE {

    private $buffer;

    public function get_gerarAcoesNoTopo() {
        if ($this->mlocalAcoes == cTEMPLATE::lca_TOPO) {
            return true;
        } else {
            return false;
        }
    }

    public function set_estiloTituloPagina() {
        $this->mEstiloTitulo = cINTERFACE::titPAGINA;
    }

    public function set_estiloTituloInterno() {
        $this->mEstiloTitulo = cINTERFACE::titSUB;
    }

    public function __construct($pClass = __CLASS__) {
        parent::__construct($pClass);
        $this->mlocalAcoes = self::lca_RODAPE;
        $this->set_estiloTituloPagina();
        $this->enveloparEmConteudoInterno = true;
    }

    public function HTML($pEnveloparEmConteudoInterno = "") {
        if ($pEnveloparEmConteudoInterno != '') {
            $this->enveloparEmConteudoInterno = $pEnveloparEmConteudoInterno;
        }
        $this->buffer = '';
        $this->buffer .= $this->RenderizeTitulo();
        if ($this->enveloparEmConteudoInterno) {
            $this->buffer .= '<div class="conteudoInterno">' . $this->RenderizeFormulario() . '</div>';
        } else {
            $this->buffer .= $this->RenderizeFormulario();
        }
        $this->buffer .= $this->RenderizeRodape();

        $this->EnveloparEmForm();
        $this->EnveloparEmDivIndividual();
        return $this->buffer;
    }

    public function HTML_form() {
        return $this->HTML(false);
    }

    public function RenderizeTitulo() {
        $this->CustomizeCabecalho();
        return cINTERFACE::RenderizeTitulo($this, $this->get_gerarAcoesNoTopo(), $this->mEstiloTitulo);
    }

    public function RenderizeFormulario() {
        return cINTERFACE::RenderizeTemplateEdicao($this) . $this->FechaTabela();
    }
    
    public function FechaTabela(){
        return '</table>';
    }

    public function RenderizeRodape() {
        if ($this->mlocalAcoes == cTEMPLATE::lca_RODAPE) {
            return cINTERFACE::RenderizeBarraAcoesRodape($this->mAcoes);
        } else {
            return "";
        }
    }

    public function EnveloparEmForm() {
        if ($this->enveloparEmForm) {
            $this->buffer = cINTERFACE::formTemplate($this) . $this->buffer . '</form>';
        }
    }

    public function EnveloparEmDivIndividual() {
        if ($this->enveloparEmDiv) {
            $this->buffer = '<div id="' . $this->mnome . '">' . $this->buffer;
            $this->buffer.= $this->TratamentoPadraoDeMensagens();
            $this->buffer.= '</div>';
        }
    }

    public function CustomizeCabecalho() {
        
    }
}
