<?php

/**
 * Template especializado para a geração de grids
 *
 * TODO: Incompleto
 */
class cTEMPLATE_LISTAGEM extends cTEMPLATE {

    public $outputBuffer;
    public $outputBufferAtivo = true;
    public $bufferLinhas;
    protected $qtdMaxLinhas;
    protected $estilosAdicionaisGrid;
    protected $linhaPendente;
    protected $mExibirLinhaInclusao;
    protected $msgVazio;
    public $exibirPaginacao;

    public $id_linha;
    /**
     * Constantes
     */
    const CMP_ORDENACAO = '__ordenacao';
    const CMP_ORDENACAO_SENTIDO = '__ordenacaoSentido';
    const CMP_QTDPAGINAS = '__qtdpaginas';
    const CMP_PAGINAATUAL = '__paginaatual';
    const CMP_OFFSET = '__offset';

    /*
     * Manipuladores de propriedades
     */

    public function InicializaQtdLinhas() {
        $this->setValorCampo(self::CMP_TOTAL_LINHAS, 0);
    }

    public function IncrementaQtdLinhas() {
        $novo = $this->getValorCampo(self::CMP_TOTAL_LINHAS) + 1;
        $this->setValorCampo(self::CMP_TOTAL_LINHAS, $novo);
    }

    public function get_qtdAcoesLinhaVisiveis() {
        $i = 0;
        foreach ($this->mAcoesLinha as $acao) {
            $i++;
        }
        return $i;
    }

    public function get_qtdFiltrosVisiveis() {
        $i = 0;
        foreach ($this->mFiltro as $filtro) {
            if ($filtro->mVisivel) {
                $i++;
            }
        }
        return $i;
    }

    public function get_qtdLinhas() {
        return $this->getValorCampo(self::CMP_TOTAL_LINHAS);
    }

    public function set_exibirLinhaInclusao($pValor) {
        $this->mExibirLinhaInclusao = $pValor;
    }

    public function get_exibirLinhaInclusao() {
        return $this->mExibirLinhaInclusao;
    }

    public function get_offset() {
        $x = cHTTP::$POST['CMP__offset'];
        if ($x == '') {
            $x = 30;
        }
        return $x;
    }
    public function set_ordenacaoDefault($pValor) {
        $this->mOrdenacaoDefault = $pValor;
    }

    public function get_ordenacao() {
        $virgula = '';
        $ordenacao = '';
        if ($this->mCampo[self::CMP_ORDENACAO]->mValor != '') {
            $ordenacao .= $virgula . $this->mCampo[self::CMP_ORDENACAO]->mValor . ' ' . $this->mCampo[self::CMP_ORDENACAO_SENTIDO]->mValor;
            $virgula = ', ';
        }
        if ($this->mOrdenacaoDefault != '') {
            $ordenacao .= $virgula . $this->mOrdenacaoDefault;
        }
        return $ordenacao;
    }

    public function AdicioneCampo($pCampo) {
        parent::AdicioneCampo($pCampo);
        $this->mCampo[$pCampo->mNome]->mOrdenavel = true;
    }

    public function AdicioneColunaEditavel($pCampo, $pClasse = 'esq') {
        parent::AdicioneColuna($pCampo, $pClasse);
        $this->mCampo[$pCampo->mNome]->mClasse .= ' editavel';
        $this->mCampo[$pCampo->mNome]->mReadonly = false;
        $this->mCampo[$pCampo->mNome]->mOrdenavel = true;
    }

    public function AdicionarGridHighlight() {
        $this->estilosAdicionaisGrid = "tablesorter relatorio grid";
    }

    public function get_estilosAdicionaisGrid() {
        return $this->estilosAdicionaisGrid;
    }

    public function adicionar_estilosAdicionaisGrid($estilo) {
        $this->estilosAdicionaisGrid.= " ".$estilo;
    }

    public function set_msgVazio($pval) {
        $this->msgVazio = $pval;
    }

    public function set_exibirPaginacao($pval) {
        $this->exibirPaginacao = $pval;
    }

    public function get_exibirPaginacao() {
        return $this->exibirPaginacao;
    }

    /**
     * Métodos
     */
    public function __construct($pNomeTemplate) {
        parent::__construct($pNomeTemplate);
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_ORDENACAO_SENTIDO));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_ORDENACAO));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_PAGINAATUAL));
        $this->setValorCampo(self::CMP_PAGINAATUAL, 1);
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_QTDPAGINAS));
        $this->estilosAdicionaisGrid = "grid";
        $this->mExibirLinhaInclusao = false;
        $this->msgVazio = 'Nenhum registro encontrado';
        $this->exibirPaginacao = false;
    }

    /**
     * Gera o html do template
     * @param type $pcursor
     */
    public function Renderize($pcursor) {
        $this->OutputInicializar();
        $this->CustomizarCabecalho();
        if ($this->enveloparEmForm) {
            $this->GerarOutput(cINTERFACE::formTemplate($this));
        }
        $this->RenderizeTitulo();
        if ($this->enveloparEmConteudoInterno) {
            $this->GerarOutput('<div class="conteudoInterno">');
        }

        $this->RenderizeFiltros();
        $this->RenderizeCabecalhoListagem();
        // Gera linha em branco para adição de registros, se for o caso
        $this->InicializaQtdLinhas();

        $this->RenderizeLinhaInclusao();
        if (is_object($pcursor)) {
            if ($pcursor->rowCount() > 0) {
                $this->LoopProcessamento($pcursor);
            }
        }
        $this->CustomizarAcoes();

        $this->RenderizeListagemRodape();
        if ($this->get_qtdLinhas() == 0) {
            $this->GerarOutput('<br/>' . $this->msgVazio);
        }
        if ($this->enveloparEmConteudoInterno) {
            $this->GerarOutput('</div>');
        }
        if ($this->enveloparEmForm) {
            $this->GerarOutput('</form>');
        }
    }

    public function CustomizarCabecalho() {
        // Implementar override se houver necessidade
    }

    public function CustomizarLinha() {
        // Implementar override se houver necessidade
    }

    public function CustomizarAcoes() {
        // Implementar override se houver necessidade
    }

    public function RenderizeTitulo() {
        if ($this->mTitulo != '') {
            $this->GerarOutput(cINTERFACE::RenderizeTitulo($this, true, cINTERFACE::titPAGINA));
        }
    }

    public function BarraNavegacao() {
        $cab = '';
        $cab = '<div class="paginacao">';
        $acao = new cACAO_SUBMIT_BUTTON('/imagens/grey16/play_prev_f.png', 'PRIMEIRA', 'Ir para a primeira página');
        $cab .= cINTERFACE::RenderizeAcao($acao);
        $acao = new cACAO_SUBMIT_BUTTON('/imagens/grey16/play_prev.png', 'ANTERIOR', 'Ir para a página anterior');
        $cab .= cINTERFACE::RenderizeAcao($acao);

        $cab .= ' ' . $this->getValorCampo(self::CMP_PAGINAATUAL) . ' de ' . $this->getValorCampo(self::CMP_QTDPAGINAS);

        $acao = new cACAO_SUBMIT_BUTTON('/imagens/grey16/play_next.png', 'PROXIMA', 'Ir para a próxima página');
        $cab .= cINTERFACE::RenderizeAcao($acao);
        $acao = new cACAO_SUBMIT_BUTTON('/imagens/grey16/play_next_f.png', 'ULTIMA', 'Ir para a última página');
        $cab .= cINTERFACE::RenderizeAcao($acao);

        $cab .= '&nbsp;Qtd registros por página:<input type="text" name="CMP__offset" style="padding:0;margin:0;font-size:8pt;width:30px;" value="' . $this->get_offset() . '"/>';
        $cab .= '</div>';
        return $cab;
    }

    public function RenderizeCabecalhoListagem() {
        if ($this->get_exibirPaginacao()) {
            $this->GerarOutput($this->BarraNavegacao());
        }
        $this->GerarOutput(cINTERFACE::RenderizeListagem_Cabecalho($this));
    }

    public function RenderizeLinhaInclusao() {
        if ($this->mExibirLinhaInclusao) {
            $this->ResseteValoresCampos();
            $this->GerarOutput(cINTERFACE::RenderizeListagem_Linha($this, $this->get_qtdLinhas()));
            $this->IncrementaQtdLinhas();
        }
    }

    public function CustomizarFiltros() {
        // Usar override para adicionar customizaçoes nos filtros.
    }

    public function RenderizeFiltros() {
        if (cHTTP::$MIME != cHTTP::mmXLS) {
            $this->CustomizarFiltros();
            $this->GerarOutput(cINTERFACE::RenderizeListagem_Filtros($this));
        }
    }

    public function RenderizeListagemRodape() {
        $this->GerarOutput(cINTERFACE::RenderizeListagem_Rodape($this));
        if ($this->get_exibirPaginacao()) {
            $this->GerarOutput($this->BarraNavegacao());
        }
    }

    public function SalveLinhaAnterior() {
        unset($this->mCampoLinhaAnterior);
        foreach ($this->mCampo as $campo) {
            $x = clone $campo;
            $this->mCampoLinhaAnterior[$x->mNome] = $x;
        }
    }

    public function RestaureLinhaAnterior() {
        foreach ($this->mCampo as &$campo) {
            $campo->mValor = $this->mCampoLinhaAnterior[$campo->mNome]->mValor;
        }
    }

    public function LoopProcessamento($pcursor) {
        while ($rs = $pcursor->fetch(PDO::FETCH_BOTH)) {
            $this->ProcesseRegistro($rs);
        }
    }

    public function ProcesseRegistro($prs) {
        $this->mClasse = '';
        $this->CarregueDoRecordset($prs);
        $this->RenderizeLinha();
        $this->SalveLinhaAnterior();
    }

    public function RenderizeLinha() {
        $this->IncrementaQtdLinhas();
        $this->CustomizarLinha();
        $x = '';
        $this->GerarOutput(cINTERFACE::RenderizeListagem_Linha($this, trim($this->mEstiloLinha) . ' ' . $this->get_qtdLinhas(),$x ,  $this->id_linha));
    }

    public function RenderizeLinhaBuffer() {
        $this->bufferLinhas[] = clone $this->mCampo;
    }

    public function RenderizeBuffer() {
        foreach ($this->bufferLinhas as $linha) {
            $this->mCampo = $linha;
            $this->GerarOutput(cINTERFACE::RenderizeListagem_Linha($this, trim($this->mEstiloLinha) . ' ' . $this->get_qtdLinhas(), "",  $this->id_linha));
        }
    }

    public function OutputInicializar() {
        $this->outputBuffer = '';
    }

    public function GerarOutput($pconteudo) {
        if ($this->outputBufferAtivo) {
            $this->outputBuffer .= $pconteudo;
        } else {
            print $pconteudo;
        }
    }

    public function getValorCampoLinhaAnterior($pCampo) {
        return $this->mCampoLinhaAnterior[$pCampo]->mValor;
    }

    public function appendValorCampoLinhaAnterior($pCampo, $pValor) {
        $this->mCampoLinhaAnterior[$pCampo]->mValor = $this->mCampoLinhaAnterior[$pCampo]->mValor . $pValor;
    }

    public function JSON_Data($pcursor) {
        $data = array();
        $data['lang'] = cHTTP::getLang();
        $data['head_title'] = "Mundivisas::Dataweb";
        $data['titulo'] = "";
        $data['titulo_tela'] = "";
        $data['titulo_painel'] = "";
        $data['campos'] = $this->mCampo;
        $data['template'] = array('nome' => $this->mnome, 'multipart' => $this->mMultipart, 'controles' => $this->controles);
        $data['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        $data['acoes'] = $this->mAcoes;
        $data['filtros'] = $this->mFiltro;

        while ($rs = $pcursor->fetch(PDO::FETCH_BOTH)) {
            $this->ProcesseRegistro($rs);
        }
    }

    public function HabiliteEditInPlacePadrao($pnomeController, $pnomeCampo, $pnomeCampoChave, $pnomeCampoForm = '') {
        if ($pnomeCampoForm == '') {
            $pnomeCampoForm = $pnomeCampo;
        }
        $parm = self::chamadaEdicaoPadrao($pnomeController
                        , $this->getValorCampo($pnomeCampoChave)
                        , $pnomeCampoChave
                        , $pnomeCampo
                        , $this->mCampo[$pnomeCampoForm]->mTipo
                        , cSESSAO::$mcd_usuario);
        $this->mCampo[$pnomeCampoForm]->set_parametros($parm);
    }

    public static function chamadaEdicaoPadrao($nomeController, $valorChave, $campoChave, $campoAlterado, $tipoCampoAlterado, $cd_usuario) {
        $parm = '{';
        $parm.= '  "controller": "' . $nomeController . '"';
        $parm.= ', "metodo": "AtualizeCampoPadrao"';
        $parm.= ', "chave": "' . $valorChave . '"';
        $parm.= ', "xnome_chave": "' . $campoChave . '"';
        $parm.= ', "xnome_campo": "' . $campoAlterado . '"';
        $parm.= ', "tipo_campo": "' . $tipoCampoAlterado . '"';
        $parm.= ', "cd_usuario": "' . $cd_usuario . '"';
        $parm.= '}';
        return $parm;
    }

    public function ChavesSelecionadas($pnomeCampoChave) {
        $ret = '';
        foreach (cHTTP::$POST['CMP_SELETOR'] as $ind => $valor) {
            $ret .= ',' . $valor;
        }
        if ($ret != '') {
            $ret .= ',';
        }
        return $ret;
    }

}
