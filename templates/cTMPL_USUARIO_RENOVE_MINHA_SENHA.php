<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_USUARIO_RENOVE_MINHA_SENHA extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		if(cHTTP::getLang() == 'pt_br'){
			$this->mTitulo = "Alterar senha";		
		}
		else{
			$this->mTitulo = "Change password";
		}
		$this->mMsgInicial = "";
		$this->qtd_colunas = 1;
		$this->mlocalAcoes = self::lca_RODAPE;
		// Adiciona campos da tela
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSENHA, 'nm_senha_atual', 'Senha atual', '', 'Current password' ));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSENHA, 'nm_senha', 'Nova senha', '', 'New password'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSENHA, 'nm_senha_rep', '(repita)', '', '(repeat new password)'));
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar as alterações", "", "", "Submit"));
	}
}
