<?php

/**
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_OS_FATURAR_POR_SOLICITANTE extends cTEMPLATE_LISTAGEM {

    public $i;

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "Faturar OS liberadas";
        cHTTP::$comCabecalhoFixo = true;
        // Adiciona campos da tela
        $this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
        $this->AdicioneColunaChave(new cCAMPO_HIDDEN('nu_empresa_requerente'));
        $this->AdicioneColunaChave(new cCAMPO_HIDDEN('NU_EMBARCACAO_PROJETO_COBRANCA'));
        $this->AdicioneCampo(new cCAMPO_SELETOR());
        $this->mCampo['SELETOR']->mEhArray = true;
        $this->mCampo['id_solicita_visto']->mEhArray = true;
        $this->mCampo['nu_empresa_requerente']->mEhArray = true;
        $this->mCampo['NU_EMBARCACAO_PROJETO_COBRANCA']->mEhArray = true;
        $this->mCampo['SELETOR']->mClasse = 'cen';
        $this->AdicioneColuna(new cCAMPO_TEXTO('indice', '#'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(new cCAMPO_TEXTO('nu_solicitacao', 'OS'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL_REQUERENTE', 'Empresa COBRANÇA'));
        $this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj. REAL'));
        $this->AdicioneColuna(new cCAMPO_TEXTO('soli_tx_solicitante_cobranca', 'Solicitante'));

        $this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Candidato(s)'));
        $this->mCampo['NOME_COMPLETO']->mOrdenavel = false;
        $this->AdicioneColuna(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Serviço'));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('tppr_id', 'Cobrança', cCOMBO::cmbTIPO_PRECO));
        $this->EstilizeUltimoCampo('editavel');
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneCampo(new cCAMPO_VALOR_REAL('soli_vl_cobrado_tabela', 'Valor tabela(R$)'));
        $this->AdicioneCampo(new cCAMPO_VALOR_REAL('soli_vl_cobrado', 'Valor (R$)'));
        $this->EstilizeUltimoCampo('editavel');

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_prestadora', 'Empresa PRESTADORA', cCOMBO::cmbEMPRESA_PRESTADORA));
        $this->mFiltro['nu_empresa_prestadora']->mValorDefault = '(selecione)';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_requerente', 'Empresa COBRANÇA', cCOMBO::cmbEMPRESA_DA_PRESTADORA));
        $this->VinculeFiltros('nu_empresa_requerente', 'nu_empresa_prestadora');
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj. REAL', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
        $this->VinculeFiltros('NU_EMBARCACAO_PROJETO_COBRANCA', 'nu_empresa_requerente');
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_servico', 'Serviço', cCOMBO::cmbSERVICO));
        $this->mFiltro['nu_servico']->mQualificadorFiltro = 's';

        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_solicitador', 'Solicitante'));
        $this->mFiltro['no_solicitador']->mTipoComparacaoFiltro = cFILTRO::tpSEL_LIKE_INI;
        // Adiciona as ações que estarão disponíveis na tela
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Gerar resumo", "GERAR_RESUMO", "Clique aqui para criar um resumo com as OS selecionadas"));
        cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieFiltros($this, $this->mFiltro['nu_empresa_prestadora'], $this->mFiltro['nu_empresa_requerente']));
        cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieFiltros($this, $this->mFiltro['nu_empresa_requerente'], $this->mFiltro['NU_EMBARCACAO_PROJETO_COBRANCA']));
    }

    public function CustomizarCabecalho() {
        $js = "
			$('#SELETOR_ALL').on('click',
				function(event) {
					if($('#SELETOR_ALL').prop('checked')){
						$('.seletor').prop('checked', true);				
					}
					else{
						$('.seletor').prop('checked', false);			
					}
				}
			);

		";

        cHTTP::AdicionarScript($js);
        $this->i = 0;
    }

    public function CustomizarLinha() {
        $this->i++;
        $this->setValorCampo('indice', $this->i);
        $this->mCampo['nu_solicitacao']->mValor = '<a href="../paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso&id_solicita_visto=' . $this->getValorCampo('id_solicita_visto') . '">' . $this->getValorCampo('nu_solicitacao') . '</a>';
        $os = new cORDEMSERVICO();
        $os->mID_SOLICITA_VISTO = $this->getValorCampo('id_solicita_visto');
        $candidatos = $os->ObtenhaNomesCandidatos();
        $br = '';
        $nomes = '';
        foreach ($candidatos as $key => $value) {
            $nomes .= $br . $value;
            $br = '<br/>';
        }
        $this->mCampo['NOME_COMPLETO']->mValor = $nomes;
        if (cHTTP::HouvePost() && is_array(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'SELETOR'])) {
            $linha = $this->get_qtdLinhas() - 1;
            if (array_search($linha, cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'SELETOR'])) {
                $this->mCampo['SELETOR']->mValor = "1";
            } else {
                $this->mCampo['SELETOR']->mValor = "0";
            }
        }
        $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'soli_vl_cobrado', 'id_solicita_visto');
        $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'tppr_id', 'id_solicita_visto');

        $this->FormateCampoValor('soli_vl_cobrado_tabela', 2);
        $this->FormateCampoValor('soli_vl_cobrado', 2);

        $this->mCampo['SELETOR']->set_valorChave($this->get_qtdLinhas() - 1);
    }

}
