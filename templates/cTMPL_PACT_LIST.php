<?php
class cTMPL_PACT_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Pacotes de serviços";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->enveloparEmForm = true;
		$this->mCols = array("50px", "60px", "50px", "50px", "auto", "auto", "300px", "120px", "50px");
		cHTTP::$comCabecalhoFixo = true;

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_SERVICO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'serv_fl_pacote'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('serv_fl_ativo', 'Ativo?', cCOMBO::cmbSIM_NAO ));
		$this->mCampo['serv_fl_ativo']->mPermiteDefault = false;
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');
		$this->mCampo['serv_fl_ativo']->mOrdenavel = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_SERVICO_TIPO_SERVICO', 'Código'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');
		$this->mCampo['CO_SERVICO_TIPO_SERVICO']->mOrdenavel = true;
		
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'serv_tx_codigo_sg', 'Código SG'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');
		$this->mCampo['serv_tx_codigo_sg']->mOrdenavel = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Nome resumido'));
		$this->EstilizeUltimoCampo('editavel');
		$this->mCampo['NO_SERVICO_RESUMIDO']->mOrdenavel = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO', 'Nome'));
		$this->EstilizeUltimoCampo('editavel');
		$this->mCampo['NO_SERVICO']->mOrdenavel = true;
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_INTEIRO, 'qtd_os', '#OS'));

		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Excel", "EXCEL", "Gerar essa consulta em Excel", "cCTRL_SERV_LIST_EXCEL", "Liste"));
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Novo pacote", "ADICIONAR_PACOTE", "Clique para adicionar um novo", "cCTRL_SERVICO", "NovoPacote"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR_PACOTE", "Clique para alterar os itens do pacote", "cCTRL_PACS_LIST", "Liste"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Trash.png", "EXCLUIR", "Clique para excluir esse item", "cCTRL_SERVICO", "Exclua", "Confirma a exclusão desse serviço? Essa operação não poderá ser desfeita.", "Atenção!"));
	}
	
	public function CustomizarLinha(){
		if ($this->getValorCampo('qtd_os') == 0 && $this->getValorCampo('NU_SERVICO') != 53){
			$this->mAcoesLinha['EXCLUIR']->mVisivel = true;
		}
		else{
			$this->mAcoesLinha['EXCLUIR']->mVisivel = false;			
		}
		
		$this->setValorCampo('qtd_os', number_format($this->getValorCampo('qtd_os'), 0, ",", "."));
		
		if ($this->getValorCampo('serv_fl_ativo') != 1){
			$this->mEstiloLinha = 'inativo';
		}
		else{
			$this->mEstiloLinha = '';
		}		

		parent::CustomizarLinha();
		$parametros = '{"controller": "cCTRL_UPDT_SERV", "metodo": "serv_fl_ativo"';
		$parametros.= ', "nu_servico": "'.$this->getValorCampo('NU_SERVICO').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['serv_fl_ativo']->set_parametros($parametros);

		$parametros = '{"controller": "cCTRL_UPDT_SERV", "metodo": "no_servico_resumido"';
		$parametros.= ', "nu_servico": "'.$this->getValorCampo('NU_SERVICO').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['NO_SERVICO_RESUMIDO']->set_parametros($parametros);
		
		$parametros = '{"controller": "cCTRL_UPDT_SERV", "metodo": "no_servico"';
		$parametros.= ', "nu_servico": "'.$this->getValorCampo('NU_SERVICO').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['NO_SERVICO']->set_parametros($parametros);

		$parametros = '{"controller": "cCTRL_UPDT_SERV", "metodo": "serv_tx_codigo_sg"';
		$parametros.= ', "nu_servico": "'.$this->getValorCampo('NU_SERVICO').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['serv_tx_codigo_sg']->set_parametros($parametros);

	}
}
