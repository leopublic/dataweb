<?php

class cTMPL_CAND_CLIE_EDIT_VISA extends cTEMPLATE_EDICAO_TWIG {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->set_titulo('New Applicant');
        $this->set_titulo_painel('Visa Details');
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
        $this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('NOME_COMPLETO'));
    }

}
