<?php
class cTMPL_TARQ_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Tipos de arquivos";
		$this->mOrdenacaoDefault = 'NO_TIPO_ARQUIVO';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "auto", "auto", "100px");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'ID_TIPO_ARQUIVO'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_ARQUIVO', 'Descrição'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_ARQUIVO_EM_INGLES', 'Descrição em inglês'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'CO_TIPO_ARQUIVO', 'Mnemônico'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'fl_disponivel_clientes', 'Disponível para os clientes?'));
		$this->EstilizeUltimoCampo('cen');

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_ARQUIVO', 'Nome da função'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'CO_TIPO_ARQUIVO', 'Mnemônico'));
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar um novo tipo de arquivo", "cCTRL_TARQ_EDIT", "Edite" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_TARQ_EDIT", "Edite" ));
	}

}
