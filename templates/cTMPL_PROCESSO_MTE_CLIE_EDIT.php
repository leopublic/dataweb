<?php

class cTMPL_PROCESSO_MTE_CLIE_EDIT extends cTEMPLATE_EDICAO {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('codigo'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', 'Empresa', cCOMBO::cmbEMPRESA, 'Company'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Embarcação/Projeto', cCOMBO::cmbEMBARCACAO_PROJETO, 'Vessel/project name'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_processo', 'Número do processo', null, 'Process #'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento', 'Data requerimento'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('cd_funcao', 'Função', cCOMBO::cmbFUNCAO));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('cd_reparticao', 'Repartição', cCOMBO::cmbREPARTICAO_CONSULAR));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_oficio', 'Número MRE'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('nu_servico', 'Serviço', cCOMBO::cmbSERVICO_TIPO_1));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_deferimento', 'Data deferimento MTE'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'prazo_solicitado', 'Prazo solicitado'));
    }

}
