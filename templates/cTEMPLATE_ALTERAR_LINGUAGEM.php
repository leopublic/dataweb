<?php
class cTEMPLATE_ALTERAR_LINGUAGEM extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("/images/pt_br.png", "ALTERARLANG_PT_BR", "Clique para português"));
		$this->mAcoes['ALTERARLANG_PT_BR']->mno_classe = "botao";
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("/images/en_us.png", "ALTERARLANG_EN_US", "Click here for english"));
		$this->mAcoes['ALTERARLANG_EN_US']->mno_classe = "botao";

	}
}
