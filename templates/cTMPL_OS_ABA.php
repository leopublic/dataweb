<?php
class cTMPL_OS_ABA extends cTEMPLATE_EDICAO{
	/**
	 *
	 * @var cORDEMSERVICO 
	 */
	public $os;
	public $nomeAba;
	public $linkAba;
	public function __construct($pClass = __CLASS__) {
		parent::__construct($pClass);
		$this->enveloparEmConteudoInterno = false;
	}
	
	public function HTML()
	{
		$OS_i = new cINTERFACE_OS();
		$OS_i->mID_SOLICITA_VISTO = $this->os->mID_SOLICITA_VISTO;
		$OS_i->Recuperar();
		$OS_i->RecuperarCandidatos();	// Precisa para saber se deve apresentar os dados de cadastro ou nao.
		$html = $OS_i->FormCabecalhoOS();
		
		$html .= '
		<div class="conteudoInterno">
			<div id="tabsOS" class="aba">'.$OS_i->aba($this->nomeAba, $this->linkAba);
		$html .= '<div id="Processo" style="padding:10px;">'.parent::HTML();
		$html .= '</div></div>';

		return $html;
	}
}
