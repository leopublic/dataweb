<?php
class cTEMPLATE_EDICAO_TWIG extends cTEMPLATE{
	protected $titulo_painel;


	public function __construct($pClass = __CLASS__){
		parent::__construct($pClass);
	}
	
    /**
     * Get e set para a propriedade titulo_painel
     */
    public function get_titulo_painel(){
        return $this->titulo_painel;
    }
    public function set_titulo_painel($pVal){
        $this->titulo_painel = $pVal;
    }			

	public function CustomizeCabecalho(){
		
	}
}
