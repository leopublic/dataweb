<?php
class cTMPL_PACS_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Serviços do pacote ";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->enveloparEmForm = true;
		$this->mCols = array("50px", "auto", "auto");
		cHTTP::$comCabecalhoFixo = true;

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_servico_pacote'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_servico_incluido'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'pacs_fl_principal'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_SERVICO_NOVO', 'Nome', cCOMBO::cmbSERVICO_ATIVO));
		$this->mCampo['NU_SERVICO_NOVO']->mVisivel = false;
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Nome resumido'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO', 'Nome completo'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_SERVICO'));
		$this->AdicioneAcaoLinha(new cACAO_SUBMIT_BUTTON("/imagens/grey16/Plus.png", "ADICIONAR", "Clique para adiconar o serviço selecionado"));
		$this->mAcoesLinha['ADICIONAR']->mVisivel = false;
		$this->AdicioneAcaoLinha(new cACAO_SUBMIT_BUTTON("/imagens/grey16/Exclamation.png", "ATIVO", "Clique para tornar esse o serviço principal do pacote"));
		$this->AdicioneAcaoLinha(new cACAO_SUBMIT_BUTTON("/imagens/grey16/Trash.png", "EXCLUIR", "Clique para excluir esse item", "Confirma a exclusão desse serviço do pacote? Essa operação não poderá ser desfeita.", "Atenção!"));
	}
	public function CustomizarCabecalho() {
		$this->mFiltro['NU_SERVICO']->mVisivel = false;
		$servico = new cSERVICO();
		$servico->RecuperePeloId($this->getValorFiltro('NU_SERVICO'));
		$this->mTitulo .= '"'.$servico->mNO_SERVICO_RESUMIDO.'"';
	}

	public function CustomizarLinha(){
		if ($this->getValorCampo('pacs_fl_principal') == 0 ){
			$this->mAcoesLinha['EXCLUIR']->mVisivel = true;
			$this->mAcoesLinha['ATIVO']->mVisivel = true;
		}
		else{
			$this->mAcoesLinha['EXCLUIR']->mVisivel = false;			
			$this->mAcoesLinha['ATIVO']->mVisivel = false;
			$this->setValorCampo('NO_SERVICO', '<b>'.$this->getValorCampo('NO_SERVICO').'</b>');
			$this->setValorCampo('NO_SERVICO_RESUMIDO', '<b>'.$this->getValorCampo('NO_SERVICO_RESUMIDO').'</b>');
		}

	}
	
	public function RenderizeListagemRodape() {
		$this->MostreCampo('NU_SERVICO_NOVO');
		$this->EscondaCampo('NO_SERVICO_RESUMIDO');
		$this->setValorCampo('NO_SERVICO', '');
		$this->mAcoesLinha['EXCLUIR']->mVisivel = false;
		$this->mAcoesLinha['ATIVO']->mVisivel = false;
		$this->mAcoesLinha['ADICIONAR']->mVisivel = true;
		$this->GerarOutput(cINTERFACE::RenderizeListagem_Linha($this, trim($this->mEstiloLinha).' '.$this->get_qtdLinhas()));
		parent::RenderizeListagemRodape();
	}
	
}
