<?php
class cTMPL_EMBP_LIST extends cTEMPLATE_LISTAGEM{
	protected $QT_CANDIDATOS_TOTAL;
	protected $QT_CANDIDATOS_ATIVOS_TOTAL;
	protected $QT_CANDIDATOS_REVISADOS_TOTAL;

	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Embarcações/Projetos";
		$this->mOrdenacaoDefault = 'NO_EMBARCACAO_PROJETO';
		cHTTP::$comCabecalhoFixo = true;
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_EMPRESA'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_EMBARCACAO_PROJETO'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('embp_dt_ult_revisao_fmt'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('nome_ult_revisao'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('nome_cadastramento'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('DT_CADASTRAMENTO_FMT'));

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO,'embp_fl_ativo', 'Ativa?'));
		$this->mCampo['embp_fl_ativo']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Nome'));
		$this->mCampo['NO_EMBARCACAO_PROJETO']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('embp_tx_revisao', 'Status revisão'));
		$this->mCampo['embp_tx_revisao']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_PAIS', 'Bandeira'));
		$this->mCampo['NO_PAIS']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_PRAZO_CONTRATO_FMT', 'Prazo contrato'));
		$this->mCampo['DT_PRAZO_CONTRATO_FMT']->mClasse = 'cen';
		$this->mCampo['DT_PRAZO_CONTRATO_FMT']->mCampoOrdenacao = 'DT_PRAZO_CONTRATO';
		$this->AdicioneColuna(new cCAMPO_TEXTO('QT_TRIPULANTES', 'Tripulantes'));
		$this->mCampo['QT_TRIPULANTES']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('QT_TRIPULANTES_ESTRANGEIROS', 'Tripulantes estrangeiros'));
		$this->mCampo['QT_TRIPULANTES_ESTRANGEIROS']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('QT_CANDIDATOS', 'Candidatos no sistema'));
		$this->mCampo['QT_CANDIDATOS']->mClasse = 'dir';
		$this->AdicioneColuna(new cCAMPO_TEXTO('QT_CANDIDATOS_ATIVOS', 'Candidatos ativos'));
		$this->mCampo['QT_CANDIDATOS_ATIVOS']->mClasse = 'dir';
		$this->AdicioneColuna(new cCAMPO_TEXTO('QT_CANDIDATOS_REVISADOS_N2013', 'Candidatos revisados<br/>(cad. antes de 2013)'));
		$this->mCampo['QT_CANDIDATOS_REVISADOS_N2013']->mClasse = 'dir';
		$this->AdicioneColuna(new cCAMPO_TEXTO('nome', 'Ponto focal'));
		$this->mCampo['nome']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('embp_tx_cadastro', 'Cadastro'));
		$this->mCampo['embp_tx_cadastro']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('TX_OBSERVACAO', 'Observações'));
		$this->mCampo['TX_OBSERVACAO']->mClasse = 'esq';

		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMPRESA', "Empresa", cCOMBO::cmbEMPRESA));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("embp_fl_ativo", "Situação cadastral", cCOMBO::cmbSITUACAO) );
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, "ATIVAS", "Somente com contrato ativo") );
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar uma nova embarcação/projeto", "cCTRL_EMBP_EDIT", "Edite" ));
		$onclick = "window.open('carregueComMenu.php?controller=cCTRL_EMBP_LIST_EXCEL&metodo=Liste', '_blank');";
		$this->AdicioneAcao(new cACAO_JAVASCRIPT ("Excel", "EXP_EXCEL",  "Clique para exportar em Excel", $onclick));
		
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_EMBP_EDIT", "Edite" ));
	}

	public function CustomizarCabecalho(){
		if (intval($this->mFiltro['NU_EMPRESA']->mValor) > 0 ) {
			$this->mAcao['INCLUIR']->mParametros = '&NU_EMPRESA='.$this->mFiltro['NU_EMPRESA']->mValor;
		}

	}
	public function CustomizarLinha(){
		$this->QT_CANDIDATOS_TOTAL =+ $this->getValorCampo('QT_CANDIDATOS');
		$this->QT_CANDIDATOS_ATIVOS_TOTAL =+ $this->getValorCampo('QT_CANDIDATOS_ATIVOS');
		$this->QT_CANDIDATOS_REVISADOS_TOTAL =+ $this->getValorCampo('QT_CANDIDATOS_REVISADOS_N2013');

		if($this->getValorCampo('nome_ult_revisao') != ''){
			$revisao = $this->getValorCampo('nome_ult_revisao').' em '.$this->getValorCampo('embp_dt_ult_revisao_fmt');			
		}
		else{
			$revisao = '--';
		}
		$this->setValorCampo('embp_tx_revisao', $revisao);

		if($this->getValorCampo('nome_cadastramento') != ''){
			$adastro = $this->getValorCampo('nome_cadastramento').' em '.$this->getValorCampo('DT_CADASTRAMENTO_FMT');			
		}
		else{
			$adastro = '(n/d)';
		}
		$this->setValorCampo('embp_tx_cadastro', $adastro);
	}

}
