<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_ORDEMSERVICO_LISTAR_ACOMP extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "";
		$this->mMsgInicial = "";
		$this->enveloparEmConteudoInterno = false;
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('id_solicita_visto'));

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa do processo'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO', 'Emb./proj. do processo'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_solicitador', 'Solicitante'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_solicitacao', 'Dt. da solicitação'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'DT_PRAZO_ESTADA_SOLICITADO', 'Prazo solicitado'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Tipo de serviço'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_responsavel', 'Responsável'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_ult', 'Dt. últ. atu.'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'nome', 'Por'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO,'de_observacao', 'Observações'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Emb./Proj.', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
		
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_solicitacao_ini', 'Solicitada desde'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_solicitacao_fim', 'Solicitada até'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_ult_ini', 'Últ. atu. desde'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_ult_ate', 'Últ. atu. até'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('cd_usuario', 'Atu. por', cCOMBO::cmbRESPONSAVEL));
	}
	
	public function CustomizarLinha() {
		parent::CustomizarLinha();
		$link = cCTRL_ORDEMSERVICO::LinkProcessoOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'));
		$this->setValorCampo('nu_solicitacao', $link);
	}
}
