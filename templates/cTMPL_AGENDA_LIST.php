<?php
/**
 *
 * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada.
 */
class cTMPL_AGENDA_LIST extends cTEMPLATE_LISTAGEM_TWIG {

    public $acum;
    public $os_ant;
    public $linhaPendente;

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "";
        $this->mCols = array("auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "200px");
        $this->mOrdenacaoDefault = 'nu_solicitacao desc, nome_completo asc';
        // Adiciona campos da tela
        $this->AdicioneColuna(new cCAMPO_HIDDEN('codigo_processo_mte_atual'));
        $this->AdicioneColuna(new cCAMPO_TEXTO('no_razao_social', "Empresa" ));
        $this->EstilizeUltimoCampo('esq');
        $this->AdicioneColuna(new cCAMPO_TEXTO('no_embarcacao_projeto', "Embarcação/proj." ));
        $this->EstilizeUltimoCampo('esq');

        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_envio_bsb', 'Incluir no protocolo de'));
        $this->mFiltro['dt_envio_bsb']->mQualificadorFiltro = 'p';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_empresa', 'Empresa'));
        $this->mFiltro['nu_empresa']->mQualificadorFiltro = 's';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'orgao_protocolo', 'Protocolo para'));
    }

    public function JSON_Data($pcursor) {
        $this->data = array();

        if ($this->getValorFiltro('nu_empresa') == 'xx') {
            $this->mFiltro['nu_empresa']->mWhere = ' s.nu_empresa <> 21';
            $this->data['escopo'] = 'Geral';
            cHTTP::set_nomeArquivo('protocoloBsb_Geral_' . str_replace('/', "_", $this->getValorFiltro('dt_envio_bsb')) . '.xls');
        } else {
            $this->data['escopo'] = 'Petrobrás';
            cHTTP::set_nomeArquivo('protocoloBsb_Petrobras_' . str_replace('/', "_", $this->getValorFiltro('dt_envio_bsb')) . '.xls');
        }
        $os = new cORDEMSERVICO();
        $this->mFiltro['orgao_protocolo']->mWhere = ' 1=1 ';
        $this->setValorFiltro('orgao_protocolo', 'MTE');
        $cursor = $os->CursorProtocoloDoDia($this->mFiltro, $this->get_ordenacao(), 1, 0);
        $this->data['data'] = $this->getValorFiltro('dt_envio_bsb');
        $this->data['mte'] = array();
        $i = 0;
        if (is_object($cursor)) {
            while ($rs = $cursor->fetch(PDO::FETCH_BOTH)) {
                $processo = array();
                $processo['num'] = $i;
                $processo['no_razao_social'] = $rs['no_razao_social'];
                $processo['nome_completo'] = $rs['nome_completo'];
                $processo['nu_passaporte'] = $rs['nu_passaporte'];
                $processo['no_nacionalidade'] = $rs['no_nacionalidade'];
                $processo['no_servico_resumido'] = $rs['no_servico_resumido'];
                $processo['nu_protocolo'] = $rs['nu_protocolo'];
                $processo['dt_requerimento'] = $rs['dt_requerimento'];
                $processo['no_embarcacao_projeto'] = $rs['no_embarcacao_projeto'];
                $processo['no_embarcacao_projeto_cobranca'] = $rs['no_embarcacao_projeto_cobranca'];
                $processo['no_reduzido_tipo_autorizacao'] = $rs['no_reduzido_tipo_autorizacao'];
                $processo['no_reparticao_consular'] = $rs['no_reparticao_consular'];
                $processo['dt_prazo_estada_solicitado'] = $rs['dt_prazo_estada_solicitado'];
                $processo['soli_qt_autenticacoes'] = $rs['soli_qt_autenticacoes'];
                $processo['soli_qt_autenticacoes_bsb'] = $rs['soli_qt_autenticacoes_bsb'];
                $this->data['mte'][] = $processo;
                $i++;
            }
        }

        $i = 0;
        $this->setValorFiltro('orgao_protocolo', 'MJ');
        $cursor = $os->CursorProtocoloDoDia($this->mFiltro, $this->get_ordenacao(), 1, 0);
        $this->data['mj'] = array();
        if (is_object($cursor)) {
            while ($rs = $cursor->fetch(PDO::FETCH_BOTH)) {
                $processo = array();
                $processo['num'] = $i;
                $processo['no_razao_social'] = $rs['no_razao_social'];
                $processo['nome_completo'] = $rs['nome_completo'];
                $processo['nu_passaporte'] = $rs['nu_passaporte'];
                $processo['no_nacionalidade'] = $rs['no_nacionalidade'];
                $processo['no_servico_resumido'] = $rs['no_servico_resumido'];
                $processo['nu_protocolo'] = $rs['nu_protocolo'];
                $processo['dt_requerimento'] = $rs['dt_requerimento'];
                $processo['no_embarcacao_projeto'] = $rs['no_embarcacao_projeto'];
                $processo['no_embarcacao_projeto_cobranca'] = $rs['no_embarcacao_projeto_cobranca'];
                $processo['no_reduzido_tipo_autorizacao'] = $rs['no_reduzido_tipo_autorizacao'];
                $processo['no_reparticao_consular'] = $rs['no_reparticao_consular'];
                $processo['dt_prazo_estada_solicitado'] = $rs['dt_prazo_estada_solicitado'];
                $processo['soli_qt_autenticacoes'] = $rs['soli_qt_autenticacoes'];
                $processo['soli_qt_autenticacoes_bsb'] = $rs['soli_qt_autenticacoes_bsb'];
                $this->data['mj'][] = $processo;
                $i++;
            }
        }
        return $this->data;
    }

}
