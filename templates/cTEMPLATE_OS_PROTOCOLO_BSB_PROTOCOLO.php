<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_OS_PROTOCOLO_BSB_PROTOCOLO extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Protocolo de Brasília";
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneColuna(new cCAMPO_LINK_EXTERNO('nu_solicitacao', 'OS', '/operador/os_DadosOS.php'));
		$this->mCampo['nu_solicitacao']->mClasse = "cen";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL', 'Empresa', 'Company'));
		$this->mCampo['NO_RAZAO_SOCIAL']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Candidato', 'Candidate'));
		$this->mCampo['NOME_COMPLETO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PASSAPORTE', 'N&ordm; Passaporte', 'Passport #'));
		$this->mCampo['NU_PASSAPORTE']->mClasse = "cen";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Tipo serviço', 'Visa type'));
		$this->mCampo['NO_SERVICO_RESUMIDO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarcação', 'Ship'));
		$this->mCampo['NO_EMBARCACAO_PROJETO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_REDUZIDO_TIPO_AUTORIZACAO', 'Resolução normativa', 'Normative'));
		$this->mCampo['NO_REDUZIDO_TIPO_AUTORIZACAO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_PRAZO_ESTADA_SOLICITADO', 'Prazo pretendido', 'Required period'));
		$this->mCampo['DT_PRAZO_ESTADA_SOLICITADO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PROTOCOLO', 'Protocolo'));
		$this->mCampo['NU_PROTOCOLO']->mReadonly = false;
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_REQUERIMENTO', 'Dt Req.'));
		$this->mCampo['DT_REQUERIMENTO']->mReadonly = false;
		$this->AdicioneColuna(new cCAMPO_TEXTO('CO_TIPO_EXIGENCIA', 'Exigência'));
		$this->mCampo['CO_TIPO_EXIGENCIA']->mReadonly = false;
		$this->AdicioneColuna(new cCAMPO_TEXTO('TX_OBS_EXIGENCIA', 'Observações'));
		$this->mCampo['TX_OBS_EXIGENCIA']->mReadonly = false;
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO(cACAO::ACAO_FORM, "Salvar", 'SALVAR', 'Clique para salvar as alterações'));
	}
}
