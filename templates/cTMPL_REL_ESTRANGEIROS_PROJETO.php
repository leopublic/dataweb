<?php

class cTMPL_REL_ESTRANGEIROS_PROJETO extends cTEMPLATE_LISTAGEM {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->AdicionePainel("Candidato");
        $this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_CANDIDATO'));

        $this->set_ordenacaoDefault('NU_ORDEM_CREWLIST, NOME_COMPLETO asc');

        $this->mCampo['NU_CANDIDATO']->mColunaSelecionavel = false;

        $rep = new cREPOSITORIO_RELATORIO;
        $paineis = $rep->campos();
        foreach($paineis as $painel){
            $nomePainel = $painel['titulo'];
            $campos = $painel['campos'];
            $this->AdicionePainel($painel['painel']);
            foreach($campos as $campo){
                $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, $campo['campo'], $campo['label']));
                $estilo = $painel['painel'];
                if ($campo['obrigatorio']){
                    $this->mCampo[$campo['campo']]->mColunaSelecionavel = false;
                }
                if ($campo['class'] != ''){
                    $estilo .= $campo['class'];
                }
                $this->EstilizeUltimoCampo($estilo);
            }
        }

        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NO_RAZAO_SOCIAL', 'Empresa'));
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NO_EMBARCACAO_PROJETO', 'Embarcação/Projeto'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'empt_tx_descricao', 'Empresa terceirizada'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_ORDEM_CREWLIST', 'Ordem crewlist'));
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'FL_EMBARCADO', 'Embarcado'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NOME_COMPLETO', 'Nome'));
        // $this->mCampo['NOME_COMPLETO']->mColunaSelecionavel = false;
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'situacao_cadastral', 'Situação'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'revisado', 'Revisado?'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->EstilizeUltimoCampo("cen");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NO_NACIONALIDADE', 'Nacionalidade'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_CPF', 'CPF'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_RNE', 'RNE'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_CTPS', 'CTPS'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_EXPIRACAO_CTPS', 'Expiração CTPS'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_CNH', 'CNH'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_EXPIRACAO_CNH', 'Expiração CNH'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NO_ENDERECO_ESTRANGEIRO_COMPLETO', 'Endereço'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NO_FUNCAO', 'Função'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_PASSAPORTE', 'No passaporte'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_VALIDADE_PASSAPORTE', 'Val. Passaporte'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'TIPO_VISTO', 'Tipo de visto'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'VA_RENUMERACAO_MENSAL', 'Salário'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NO_EMAIL_CANDIDATO', 'Email'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_TELEFONE_ESTRANGEIRO', 'Telefone'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_NASCIMENTO', 'Dt. nasc.'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NO_ESCOLARIDADE', 'Escolaridade'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'TE_OBSERVACAO_CREWLIST', 'Observação Crewlist'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'fl_tem_copia_passaporte', 'Tem cópia passaporte?'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'fl_tem_form1344', 'Tem form. 1344?'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'situacao_prazo_estada', 'Prazo de estada calculado'));
        // $this->EstilizeUltimoCampo("cand");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'situacao_texto', 'Situação visto'));
        // $this->EstilizeUltimoCampo("");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'situacao_origem', 'Porque?'));
        // $this->EstilizeUltimoCampo("");

        // $this->AdicionePainel("Autorização");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_PROCESSO_MTE', 'Nº Processo MTE.'));
        // $this->EstilizeUltimoCampo("mte numero");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'dt_solicitacao', 'Data solicitação'));
        // $this->EstilizeUltimoCampo("mte");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_REQUERIMENTO', 'Data Req. MTE'));
        // $this->EstilizeUltimoCampo("mte");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'CONSULADO', 'Consulado'));
        // $this->EstilizeUltimoCampo("mte");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'PRAZO_SOLICITADO', 'Prazo Solicitado'));
        // $this->EstilizeUltimoCampo("mte");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_DEFERIMENTO', 'Deferimento'));
        // $this->EstilizeUltimoCampo("mte");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DIAS_EXPIRACAO_DEFERIMENTO', 'Exp. do deferimento (180-dias)'));
        // $this->EstilizeUltimoCampo("mte");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'OBSERVACAO_VISTO', 'Obs'));
        // $this->EstilizeUltimoCampo("mte");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NO_RAZAO_SOCIAL_COBRANCA', 'Empresa Cobr.'));
        // $this->EstilizeUltimoCampo("mte");

        // $this->AdicionePainel("Coleta");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_EMISSAO', 'Dt emissão'));
        // $this->EstilizeUltimoCampo("coleta");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_ENTRADA', 'Dt entrada'));
        // $this->EstilizeUltimoCampo("coleta");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'no_validade', 'Validade'));
        // $this->EstilizeUltimoCampo("coleta");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'no_local_emissao', 'Local emissão'));
        // $this->EstilizeUltimoCampo("coleta");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'nu_visto', 'Núm. visto'));
        // $this->EstilizeUltimoCampo("coleta");

        // $this->AdicionePainel("Registro");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_PROTOCOLO_REG', 'Nº Protocolo reg.'));
        // $this->EstilizeUltimoCampo("registro");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_PRAZO_ESTADA', 'Prazo Estada'));
        // $this->EstilizeUltimoCampo("destaque");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_REQUERIMENTO_CIE', 'Dt Req. Reg.'));
        // $this->EstilizeUltimoCampo("registro");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'dt_validade_proreg', 'Validade prot.'));
        // $this->EstilizeUltimoCampo("registro");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'depf_no_nome', 'Delegacia PF'));
        // $this->EstilizeUltimoCampo("registro");

        // $this->AdicionePainel("Prorrogação");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NU_PROTOCOLO_PRO', 'Nº Prot. Prorrog.'));
        // $this->EstilizeUltimoCampo("prorrog");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_REQUERIMENTO_PRO', 'Dt. Req. Prorrog.'));
        // $this->EstilizeUltimoCampo("prorrog");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'DT_PRAZO_PRET_PRO', 'Prazo Prorrog.'));
        // $this->EstilizeUltimoCampo("destaque");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'dt_publicacao_dou', 'Dt. Def. Prorrog.'));
        // $this->EstilizeUltimoCampo("prorrog");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'NO_STATUS_CONCLUSAO_PRO', 'Status'));
        // $this->EstilizeUltimoCampo("prorrog");

        // $this->AdicionePainel("Restabelecimento");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'nu_protocolo_rest', 'Nº Prot. Restab.'));
        // $this->EstilizeUltimoCampo("restab");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'dt_requerimento_rest_fmt', 'Dt. Req. Restab.'));
        // $this->EstilizeUltimoCampo("restab");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'dt_prazo_estada_rest_fmt', 'Prazo estada Restab.'));
        // $this->EstilizeUltimoCampo("destaque");
        // $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'dt_validade_rest', 'Dt. val. prot. CIE.'));
        // $this->EstilizeUltimoCampo("restab");

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("NU_EMPRESA", 'Empresa', cCOMBO::cmbEMPRESA));
        $this->mFiltro['NU_EMPRESA']->mQualificadorFiltro = 'C';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("NU_EMBARCACAO_PROJETO", 'Embarcação/Projeto', cCOMBO::cmbEMBARCACAO_PROJETO_COM_REVISAO));
        $this->mFiltro['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = 'C';
        $this->VinculeFiltros("NU_EMBARCACAO_PROJETO", "NU_EMPRESA");

        cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieFiltros($this, $this->mFiltro['NU_EMPRESA'], $this->mFiltro['NU_EMBARCACAO_PROJETO']));


        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("empt_id", 'Empresa terceirizada', cCOMBO::cmbEMPRESA_TERCEIRIZADA));
        $this->mFiltro['empt_id']->mPermiteDefault = true;
        $this->mFiltro['empt_id']->mQualificadorFiltro = 'C';

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("CO_NACIONALIDADE", "Nacionalidade", cCOMBO::cmbNACIONALIDADE));
        $this->mFiltro['CO_NACIONALIDADE']->mPermiteDefault = true;
        $this->mFiltro['CO_NACIONALIDADE']->mQualificadorFiltro = 'C';

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("CO_NACIONALIDADE_MENOS", "Menos nacionalidade", cCOMBO::cmbNACIONALIDADE));
        $this->mFiltro['CO_NACIONALIDADE_MENOS']->mPermiteDefault = true;
        $this->mFiltro['CO_NACIONALIDADE_MENOS']->mQualificadorFiltro = 'C';
        $this->mFiltro['CO_NACIONALIDADE_MENOS']->mTipoComparacaoFiltro = cFILTRO::tpSEL_DIFERENTE;
        $this->mFiltro['CO_NACIONALIDADE_MENOS']->mCampoBD = "CO_NACIONALIDADE";

        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, "excluir_brasileiros", "Excluir brasileiros?"));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, "excluir_sem_nac", "Excluir sem nacionalidade?"));

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("FL_INATIVO", "Situação cadastral", cCOMBO::cmbSITUACAO_CADASTRAL_CANDIDATO));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("tipo_candidato", "Tipo candidato", cCOMBO::cmbTIPO_CANDIDATO));
        $this->mFiltro['tipo_candidato']->mPermiteDefault = true;
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("cand_fl_revisado", "Situação de revisão", cCOMBO::cmbSITUACAO_REVISAO_CANDIDATO));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("FL_EMBARCADO", "Embarcado", cCOMBO::cmbSIM_NAO));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, "NOME_COMPLETO", "OU nome do candidato"));
        $this->mFiltro['NOME_COMPLETO']->mOperadorFiltro = ' or ';

        $this->AdicioneAcaoLinha(new cACAO_JAVASCRIPT("/imagens/grey16/Ok.png", "REVISAR", "Marcar esse candidato como revisado"));
        $this->AdicioneAcaoLinha(new cACAO_JAVASCRIPT("/imagens/grey16/Cancel.png", "DESREVISAR", "Limpar a revisão desse candidato"));
        $this->AdicioneAcaoLinha(new cACAO_JAVASCRIPT("/imagens/grey16/Exclamation.png", "PENDENTE", "Marcar esse candidato como pendente"));
        $this->AdicioneAcaoLinha(new cACAO_JAVASCRIPT("/imagens/grey16/Minus.png", "INATIVAR", "Tornar esse candidato inativo", ""));
        $this->AdicioneAcaoLinha(new cACAO_JAVASCRIPT("/imagens/grey16/Plus.png", "ATIVAR", "Tornar esse candidato ativo"));
        $this->AdicioneAcaoLinha(new cACAO_JAVASCRIPT("/imagens/grey16/Trash.png", "EXCLUIR", "Excluir esse candidato", "Confirma solicitação de exclusão desse candidato?", "Aten&ccedil;&atilde;o!"));
    }
    
    public function CustomizeCabecalho(){
        $this->mFiltro['NU_EMBARCACAO_PROJETO']->AdicionarValor(-1, '(todos)');
    }
    
    public function recuperarConfiguracao($nome){
        
    }
    
    public function criarConfiguracao($nome){
        $sql = "insert into configuracao_relatorio (cfgr_nome) value ('".$nome."')";
        cAMBIENTE::$db_pdo->exec($sql);
        $cfgr_id = cAMBIENTE::$db_pdo->lastInsertId();
        foreach($this->mCampo as $campo){
            if ($campo->mColunaAtiva ){
                $sql = "insert into configuracao_relatorio_coluna(cfgr_id, cfgc_nome_coluna) values(".$cfgr_id.", '".$campo->mNome."')";
                cAMBIENTE::$db_pdo->exec($sql);
            }
        }        
        return $cfgr_id;
    }
    
    public function salvarConfiguracao($cfgr_id, $nome){
        $sql = "delete from configuracao_relatorio_coluna where cfgr_id = ".$cfgr_id;
        cAMBIENTE::$db_pdo->exec($sql);
        foreach($this->mCampo as $campo){
            if ($campo->mColunaAtiva ){
                $sql = "insert into configuracao_relatorio_coluna(cfgr_id, cfgc_nome_coluna) values(".$cfgr_id.", '".$campo->mNome."')";
                cAMBIENTE::$db_pdo->exec($sql);
            }
        }
    }
}

