<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_OS_LISTAR_NAO_FATURADAS extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Faturar Ordens de Servi&ccedil;o liberadas para cobran&ccedil;a";
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('NU_EMPRESA_COBRANCA'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('NU_EMBARCACAO_PROJETO_COBRANCA'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL', 'Empresa'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarca&ccedil;&atilde;o/projeto'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('qtd', 'Qtd OS'));
		$this->mCampo['qtd']->mClasse = "cen";
		$this->AdicioneColuna(new cCAMPO_VALOR_REAL('soli_vl_cobrado', 'Valor total (R$)'));

		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA("NU_EMPRESA_COBRANCA", "Empresa", cCOMBO::cmbEMPRESA));
		$this->mFiltro['NU_EMPRESA_COBRANCA']->mQualificadorFiltro = 's';
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "GERAR_RESUMO_COBRANCA", "Gerar o resumo de cobrança para esse centro de custos", "cCTRL_RESUMO_COBRANCA", "Edite"));

	}
}
