<?php

/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_USUARIO_LISTE extends cTEMPLATE_LISTAGEM {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "Usuários do sistema";
        $this->mMsgInicial = "";
        $this->mOrdenacaoDefault = 'nome';
        // Adiciona campos da tela
        $this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'cd_usuario'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'usua_qtd_acesso_negado'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome', 'Nome'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'login', 'Login'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nm_email', 'Email'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nm_perfil', 'Perfil'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa (cliente)'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'usua_dt_ult_acesso', 'Último acesso'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'flagativo', 'Ativo?'));
        $this->EstilizeUltimoCampo('cen');

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('flagativo', 'Ativo?', cCOMBO::cmbSIM_NAO));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome', 'Nome'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('cd_perfil', 'Perfil', cCOMBO::cmbPERFIL));
        $this->mFiltro['cd_perfil']->mQualificadorFiltro = 'u';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('cd_empresa', 'Empresa (cliente)', cCOMBO::cmbEMPRESA));

        // Adiciona as ações que estarão disponíveis na tela
        $this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Adicionar novo", "EDITAR", "Clique para adicionar um novo usuário", "cCTRL_USUARIOS", "Edite"));
        $this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "EDITAR", "Clique para editar o usu&aacute;rio", "cCTRL_USUARIOS", "Edite"));
        $this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Key.png", "RENOVAR_SENHA", "Clique para gerar uma nova senha para esse usuário", "cCTRL_USUARIOS", "RenoveSenha"));
        $this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Mail.png", "ENVIAR SENHA", "Clique para enviar uma nova senha aleatória para esse usuário", "cCTRL_USUARIOS", "enviarNovaSenha"));
        $this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/lock_open.png", "DESBLOQUEAR", "Clique para desbloquear esse usuário", "cCTRL_USUARIOS", "desbloquear"));
    }

    public function CustomizarLinha() {
        parent::CustomizarLinha();
        if ($this->getValorCampo('flagativo') != 1) {
            $this->mEstiloLinha = 'inativo';
        } else {
            $this->mEstiloLinha = '';
        }
        
        if ($this->getValorCampo('usua_qtd_acesso_negado')>= 5){
            $this->mAcoesLinha['DESBLOQUEAR']->mVisivel = true;
        } else {
            $this->mAcoesLinha['DESBLOQUEAR']->mVisivel = false;            
        }
    }

}
