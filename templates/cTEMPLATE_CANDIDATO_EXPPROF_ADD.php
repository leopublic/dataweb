<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_CANDIDATO_EXPPROF_ADD extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('epro_id'));
		$this->AdicionePainel('Adicione todas alocações anteriores', "Add all your previous allocation");
		$this->AdicioneCampo(new cCAMPO_TEXTO('epro_no_companhia', 'Empresa', 'Company'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('epro_no_funcao', 'Função', 'Function'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('epro_no_periodo', 'Período', 'Period'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('epro_tx_atribuicoes', 'Atribuições/responsabilidades', 'Atributed responsabilities'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Adicionar", "SALVAR", "Click to add a new previous experience", "", "", "Add" ));
	}
}
