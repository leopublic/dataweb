<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_OS_PROTOCOLO_BSB_ENVIADOS extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Protocolo de Brasília";
		// Adiciona campos da tela
//		$this->AdicioneFiltro(new cCAMPO_DATA('dt_envio_bsb', 'Protocolo do dia', 'Protocol'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('dt_envio_bsb', 'Últimos protocolos', cCOMBO::cmbULTIMOS_PROTOCOLOS_BSB));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneColuna(new cCAMPO_LINK_EXTERNO('nu_solicitacao', 'OS', '/operador/os_DadosOS.php'));
		$this->mCampo['nu_solicitacao']->mClasse = "cen";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL', 'Empresa', 'Company'));
		$this->mCampo['NO_RAZAO_SOCIAL']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Candidato', 'Candidate'));
		$this->mCampo['NOME_COMPLETO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PASSAPORTE', 'N&ordm; Passaporte', 'Passport #'));
		$this->mCampo['NU_PASSAPORTE']->mClasse = "cen";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Tipo serviço', 'Visa type'));
		$this->mCampo['NO_SERVICO_RESUMIDO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarcação', 'Ship'));
		$this->mCampo['NO_EMBARCACAO_PROJETO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_REDUZIDO_TIPO_AUTORIZACAO', 'Resolução normativa', 'Normative'));
		$this->mCampo['NO_REDUZIDO_TIPO_AUTORIZACAO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_REPARTICAO_CONSULAR', 'Repartição consular', 'Consulate'));
		$this->mCampo['NO_REPARTICAO_CONSULAR']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_PRAZO_ESTADA_SOLICITADO', 'Prazo pretendido', 'Required period'));
		$this->mCampo['DT_PRAZO_ESTADA_SOLICITADO']->mClasse = "esq";
		// Adiciona as ações que estarão disponíveis na tela
	}
}
