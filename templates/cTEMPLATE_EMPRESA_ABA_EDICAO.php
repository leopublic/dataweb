<?php
class cTEMPLATE_EMPRESA_ABA_EDICAO extends cTEMPLATE_ABA{

	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Empresa";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_EMPRESA'));
		$this->AdicioneAbaAjax('EDITAR', 'Principal', 'cCTRL_EMPRESA', 'Edite', 'Editar dados cadastrais' );
		$this->AdicioneAbaAjax('PROCURADORES', 'Procuradores', 'cCTRL_PROCURADORES', 'ListeProcuradores', 'Informações complementares' );
		$this->AdicioneAbaAjax('EMBARCACOES', 'Embarcações/Projetos', 'cCTRL_EMBARCACAO_PROJETO', 'ListeDaEmpresa', '');
		$this->AdicioneAbaAjax('DIRETORIA', 'Diretoria', 'cCTRL_DIRETORIA', 'ListeDocumentos', 'Cadastro dos documentos associados ao cliente');
		$this->AdicioneAbaAjax('ASSOCIADOS', 'Associados', 'cCTRL_ASSOCIADAS', 'ListeDocumentos', 'Cadastro dos documentos associados ao cliente');
	}
}
