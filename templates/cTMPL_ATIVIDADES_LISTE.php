<?php
class cTMPL_ATIVIDADES_LISTE extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Descrições de atividade";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->mCols = array("60px","70px", "200px", "70px", "200px", "120px", "auto");
		cHTTP::$comCabecalhoFixo = true;

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_SOLICITACAO', 'OS'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_solicitacao', 'Data da OS'));
		$this->mCampo['dt_solicitacao']->mClasse = "cen";
		
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Serviço'));
		$this->mCampo['NO_SERVICO_RESUMIDO']->mClasse = "cen";
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_FUNCAO', 'Função'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'TE_DESCRICAO_ATIVIDADES', 'Descrição de atividades'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
		$this->mFiltro['NU_EMPRESA']->mQualificadorFiltro = 'sv';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('CO_FUNCAO_CANDIDATO', 'Função', cCOMBO::cmbFUNCAO));
		
		
		$this->AdicioneAcaoLinha(new cACAO_JAVASCRIPT("/imagens/grey16/copy.png", "COPY", "Clique para copiar o texto para a área de transferência", ""));
	}
}
