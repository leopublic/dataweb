<?php
class cTMPL_PROCESSO_EDIT_OS_EMISCIE extends cTMPL_PROCESSO_EDIT_OS{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_cie', 'Número RNE'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_emissao', 'Data de expedição'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_validade', 'Validade da CIE'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCHAVE_ESTR,'CO_CLASSIFICACAO_VISTO', 'Classificação', cCOMBO::cmbCLASSIFICA_VISTO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CLASSIFICACAO_VISTO', 'Classificação texto'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'observacao', 'Observação'));
		$this->mCampo['observacao']->mLarguraDupla = true;
		
		$this->AdicionarCamposCobranca();
	}
	
	public function CustomizeCabecalho() {
		if ($this->getValorCampo('NO_SERVICO_RESUMIDO')){
			$titulo = $this->getValorCampo('NO_SERVICO_RESUMIDO');
		}
		else{
			$titulo = "Coleta de CIE";
		}
		$this->mTitulo = $titulo;
		parent::CustomizeCabecalho();
		if ($this->os->mReadonly){
			$this->EscondaCampo('CO_CLASSIFICACAO_VISTO');
			$this->MostreCampo('NO_CLASSIFICACAO_VISTO');
		}
		else{
			$this->MostreCampo('CO_CLASSIFICACAO_VISTO');
			$this->EscondaCampo('NO_CLASSIFICACAO_VISTO');
		}
	}
}
