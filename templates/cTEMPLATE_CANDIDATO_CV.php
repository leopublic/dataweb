<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_CANDIDATO_CV extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Acesso externo do candidato";
		$this->mMsgInicial = "Please fill out this form carefully, it is very important that the date given be complete and correct to avoid delays with the Visa authorization. Please fill in all the blak spaces.";
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NOME_COMPLETO', 'Full name'));
		$this->mCampo['NOME_COMPLETO']->mReadonly = true;

		$this->AdicionePainel("Education background");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_HIGH_SCHOOL', 'High school attended'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CIDADE', "Father's full name"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_MAE', "Mother's full name"));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_NACIONALIDADE', "Nationality", cCOMBO::cmbNACIONALIDADE_EN));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_NIVEL_ESCOLARIDADE', "Highest level of education", cCOMBO::cmbESCOLARIDADE_EN));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_ESTADO_CIVIL', "Marital status", cCOMBO::cmbESTADO_CIVIL_EN));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_SEXO', "Gender", cCOMBO::cmbSEXO_EN));
		$this->AdicioneCampo(new cCAMPO_DATA('DT_NASCIMENTO', "Date of birth"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_LOCAL_NASCIMENTO', "Place of birth"));

		$this->AdicionePainel("Salary information (NOTE: This does NOT mean you will be subject to tax. Please state your GLOBAL monthly salary)");
		$this->AdicioneCampo(new cCAMPO_TEXTO('VA_REMUNERACAO_MENSAL', "Monthly income"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('VA_REMUNERACAO_MENSAL_BRASIL', "Monthly salary in Brazil"));

		$this->AdicionePainel("Home address");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ENDERECO_RESIDENCIA', 'Address'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CIDADE_RESIDENCIA', 'City'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_PAIS_RESIDENCIA', "Contry", cCOMBO::cmbPAIS_EN));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_TELEFONE_CANDIDATO', 'Phone #'));

		$this->AdicionePainel("Business address");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ENDERECO_EMPRESA', 'Address'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CIDADE_EMPRESA', 'City'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_PAIS_EMPRESA', "Contry", cCOMBO::cmbPAIS_EN));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_TELEFONE_EMPRESA', 'Phone #'));

		$this->AdicionePainel("Passport and documentation");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_PASSAPORTE', "Passport number"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('DT_EMISSAO_PASSAPORTE', "Issue date"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('DT_VALIDADE_PASSAPORTE', "Expiry date"));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_PAIS_EMISSOR_PASSAPORTE', "Issuing government", cCOMBO::cmbPAIS_EN));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_REPARTICAO_CONSULAR', "Consulate where the visa will be collected"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_SEAMAN_BOOK', "Seaman's book number"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_RNE', "RNE"));

		$this->AdicionePainel("Passport");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_FUNCAO', "Occupation"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_POSICAO', "Position"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', "Vessel / project name"));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_LOCAL_EMBARCACAO_PROJETO', "Vessel / project location", cCOMBO::cmbLOCAL_PROJETO));
		$this->AdicioneCampo(new cCAMPO_MEMO('TE_DESCRICAO_ATIVIDADE', "Provide a description of the activities you expect to carry out in Brazil"));
		$this->mCampo['TE_DESCRICAO_ATIVIDADE']->mLarguraDupla = 1;
		$this->AdicioneCampo(new cCAMPO_MEMO('TE_TRABALHO_ANTERIOR_BRASIL', "Have you worked in Brazil before? If so, state briefly when / place / length of last stay"));
		$this->mCampo['TE_TRABALHO_ANTERIOR_BRASIL']->mLarguraDupla = 1;
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar as alterações"));
	}
}
