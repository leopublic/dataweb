<?php

class cTMPL_PROCESSO_EDIT_OS_MUDEMB extends cTMPL_PROCESSO_EDIT_OS {

    public function __construct($comCobranca = true) {
        parent::__construct(__CLASS__);

        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_servico'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'codigo'));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('nu_empresa', 'Empresa', cCOMBO::cmbEMPRESA));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento', 'Data requerimento'));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('nu_embarcacao_projeto_anterior', 'Embarcação anterior', cCOMBO::cmbEMBARCACAO_PROJETO));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_envio_bsb', 'Data de envio BSB'));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('nu_embarcacao_projeto_novo', 'Embarcação nova', cCOMBO::cmbEMBARCACAO_PROJETO));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'observacao', 'Observação'));
        $this->VinculeCombos('nu_embarcacao_projeto_anterior', 'nu_empresa');
        $this->VinculeCombos('nu_embarcacao_projeto_novo', 'nu_empresa');
        $this->mCampo['observacao']->mLarguraDupla = true;
        if ($comCobranca) {
            $this->AdicionarCamposCobranca();
        }

//        $acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Alterar", "ALTERAR", "Alterar os dados do visto", "");
//        $this->AdicioneAcao($acao);
    }

    public function CustomizeCabecalho() {

        $this->mAcoes['ALTERAR']->mOnClick = "carregaFormPopup('codigo=" . $this->getValorCampo('codigo') . "&tipo=mte&modo=edit');";
        //parent::CustomizeCabecalho();
        $servico = new cSERVICO();
        $servico->mNU_SERVICO = $this->getValorCampo('nu_servico');
        $servico->RecuperePeloId();
        $titulo = $servico->mNO_SERVICO_RESUMIDO;
        if ($this->getValorCampo('id_solicita_visto') > 0) {
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $this->getValorCampo('id_solicita_visto');
            $os->Recuperar();
            $titulo .= ' <a href="os_DadosOS.php?ID_SOLICITA_VISTO=' . $this->getValorCampo('id_solicita_visto') . '&id_solicita_visto=' . $this->getValorCampo('id_solicita_visto') . '" target="_blank">(OS ' . $os->mNU_SOLICITACAO . ')</a>';
        } else {
            $titulo .= ' (processo avulso)';
        }

        $this->set_titulo($titulo);

        $this->FormateCampoData('dt_requerimento');
        $this->FormateCampoData('dt_envio_bsb');

        unset($this->mAcoes['SALVAR_E_FECHAR']);
    }

}
