<?php

/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_OS_LISTAR_PENDENTES_COBRANCA extends cTEMPLATE_LISTAGEM {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "Controle de cobranças - Ordens de Serviço";
        $this->mOrdenacaoDefault = 'dt_solicitacao desc';
        // Adiciona campos da tela
        //
        $this->AdicioneColuna(new cCAMPO_SELETOR());
        $this->mCampo['SELETOR']->mReadonly = false;
        $this->mCampo['SELETOR']->mEhArray = true;
        $this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
        $this->mCampo['id_solicita_visto']->mEhArray = true;
//		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('ID_SOLICITA_VISTO'));
//		$this->mCampo['ID_SOLICITA_VISTO']->mValor = &$this->mCampo['id_solicita_visto']->mValor;

        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'stco_id'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'prec_vl_conceito'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'prec_vl_pacote'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'observacao_visto'));


        $this->AdicioneColuna(new cCAMPO_LINK_EXTERNO('nu_solicitacao', 'OS', '/operador/os_DadosOS.php'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Candidato(s)'));
        $this->mCampo['NOME_COMPLETO']->mOrdenavel = false;
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_status_sol', 'Status OS'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'stco_tx_nome', 'Status cobr.'));
        $this->EstilizeUltimoCampo('cen');
        if (cHTTP::$MIME == cHTTP::mmXLS){
            $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'tppr_id'));
            $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tppr_tx_nome', "Cobrança"));
        } else {
            $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('tppr_id', 'Cobrança', cCOMBO::cmbTIPO_PRECO));
            $this->EstilizeUltimoCampo('editavel');
        }
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL_REQUERENTE', 'Empresa COBRANÇA'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO_COBRANCA', 'Embarcação/projeto REAL'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO', 'Embarcação/projeto PROCESSO'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Serviço'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao', 'Dt. solic.'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'soli_dt_liberacao_cobranca', 'Dt. lib. cobr.'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento', 'Dt. fat.'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_tx_solicitante_cobranca', 'Solicitante'));
        if (cHTTP::$MIME == cHTTP::mmXLS){
            $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_vl_cobrado', "Valor R$"));
        } else {
            $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'soli_vl_cobrado', 'Valor R$'));
            $this->EstilizeUltimoCampo('editavel');
            $this->EstilizeUltimoCampo('dir');
        }

        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', "Número OS"));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', "Emp. PROCESSO", cCOMBO::cmbEMPRESA));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Emb./proj. PROCESSO', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
        $this->mFiltro['NU_EMPRESA']->mQualificadorFiltro = 's';
        $this->mFiltro['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = 's';
        $this->VinculeFiltros('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_requerente', "Emp. COBRANÇA", cCOMBO::cmbEMPRESA));
        $this->mFiltro['nu_empresa_requerente']->mQualificadorFiltro = 's';

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj. REAL', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
        $this->mFiltro['NU_EMBARCACAO_PROJETO_COBRANCA']->mQualificadorFiltro = 's';

        $this->VinculeFiltros('NU_EMBARCACAO_PROJETO_COBRANCA', 'NU_EMPRESA');
//		cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieFiltros($this, $this->mFiltro['NU_EMPRESA'], $this->mFiltro['NU_EMBARCACAO_PROJETO_COBRANCA']));

        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, "dt_solicitacao_ini", "Aberta desde"));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, "dt_solicitacao_fim", "Aberta até"));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, "soli_dt_liberacao_cobranca_ini", "Lib. p/ cobrança desde"));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, "soli_dt_liberacao_cobranca_fim", "Lib. p/ cobrança até"));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, "resc_dt_faturamento_ini", "Faturamento desde"));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, "resc_dt_faturamento_fim", "Faturamento até"));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('ID_STATUS_SOL', 'Status da OS', cCOMBO::cmbSTATUS_SOLICITACAO));
        $this->mFiltro['ID_STATUS_SOL']->mQualificadorFiltro = 's';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("stco_id", "Status cobr.", cCOMBO::cmbSITUACAO_COBRANCA));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("tppr_id", "Tipo cobrança", cCOMBO::cmbTIPO_PRECO));
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo("nu_servico", "Serviço", cCOMBO::cmbSERVICO));
        $this->mFiltro['nu_servico']->mQualificadorFiltro = 's';
        $this->mFiltro['stco_id']->mCombo->mPermiteDefault = true;
        $this->mFiltro['stco_id']->mQualificadorFiltro = 's';
        $this->mFiltro['tppr_id']->mQualificadorFiltro = 's';
        // Adiciona as ações que estarão disponíveis na tela
        $this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "EDITAR_FINANCEIRO", "Editar informações financeiras", "cCTRL_OS_ABA_COBR_EDIT", "Edite"));
        $onclick = "window.open('carregueComoExcel.php?controller=cCTRL_ORDEMSERVICO&metodo=ListeFinanceiroExcel', '_blank');";
		$this->AdicioneAcao(new cACAO_JAVASCRIPT ("Excel", "EXP_EXCEL",  "Clique para exportar em Excel", $onclick));
    }

    public function RenderizeCabecalhoListagem() {
            $acoes = new cCOMBO_ACOES_GRID();
            $acoes->AdicionarValor('NADA', '(nenhuma ação)');
            $acoes->AdicionarValor('LIBERAR_SELECIONADOS', 'liberados para cobrança');
            $acoes->AdicionarValor('COBRAR_CONCEITO_SELECIONADOS', 'cobrança por conceito');
            $acoes->AdicionarValor('COBRAR_PACOTE_SELECIONADOS', 'cobrança por pacote');
            $acoes->AdicionarValor('COBRAR_NAO_SELECIONADOS', 'não cobráveis');
            $acoes->AdicionarValor('PENDENTES', 'pendente');

            parent::RenderizeCabecalhoListagem();
            $x = '<div style="background-color:#DADADA;padding:5px;background-image:url(/imagens/icons/seta_esquerda_baixo.png);background-repeat: no-repeat;background-position: center left;padding-left:25px;"> ';
            $x .= cINTERFACE::RenderizeCampo($acoes);
            $x .= '<button  onclick="ConfirmaAcao(this,\'LIBERAR_SELECIONADOS\',\'\',\'\',\'\');">Enviar</button>
    		</div>';
            $js = "
    			$('#SELETOR_ALL').on('click',
    				function(event) {
    					if($('#SELETOR_ALL').is(':checked')){
    						$('.seletor').prop('checked', true);
    					}
    					else{
    						$('.seletor').prop('checked', false);
    					}
    				}
    			);
    		";

        if (cHTTP::$MIME != cHTTP::mmXLS){
            cHTTP::AdicionarScript($js);
            $this->GerarOutput($x);
            $this->linhaPendente = false;
        }
    }

    public function CustomizarLinha() {
        $this->mEstiloLinha = '';
        parent::CustomizarLinha();
        $this->FormateCampoData('dt_solicitacao');
        $this->FormateCampoData('resc_dt_faturamento');
        $this->FormateCampoData('soli_dt_liberacao_cobranca');
//		$this->FormateCampoVazioComTracos('tppr_tx_nome');
        $this->mCampo['nu_protocolo_pro']->mValor = str_replace('.', "", str_replace(".", "", str_replace('/', "", str_replace('-', "", $tela->mCampo['nu_protocolo_pro']->mValor))));
        $os = new cORDEMSERVICO();
        $os->mID_SOLICITA_VISTO = $this->getValorCampo('id_solicita_visto');
        $candidatos = $os->ObtenhaNomesCandidatos();
        $qtd_candidatos = count($candidatos);
        $br = '';
        $this->mCampo['SELETOR']->set_valorChave($this->getValorCampo('id_solicita_visto'));
        $this->mCampo['SELETOR']->mValor = "";
        $nomes = '';
        foreach ($candidatos as $key => $value) {
            $nomes .= $br . $value;
            $br = '<br/>';
        }

        // Monta valor cobrado
        if (intval($this->getValorCampo('soli_vl_cobrado')) == 0) {
            switch ($this->getValorCampo('tppr_id')) {
                case 1:
                    $this->setValorCampo('soli_vl_cobrado', 0);
                    break;
                case 2:
                    $this->setValorCampo('soli_vl_cobrado', $this->getValorCampo('prec_vl_conceito'));
                    break;
                case 3:
                    $this->setValorCampo('soli_vl_cobrado', $this->getValorCampo('prec_vl_pacote'));
                    break;
                default:
                    $this->setValorCampo('soli_vl_cobrado', $this->getValorCampo('prec_vl_conceito'));
                    break;
            }
        }
        if (intval($this->getValorCampo('soli_vl_cobrado')) == 0) {
            $this->mEstiloLinha = 'alerta';
        } else {
            $valor = $this->getValorCampo('soli_vl_cobrado') * $qtd_candidatos;
            $this->setValorCampo('soli_vl_cobrado', $valor);
        }

        $this->FormateCampoValor('soli_vl_cobrado', 2);

        $this->mAcoesLinha['EDITAR_FINANCEIRO']->mParametros = '';
        $this->mCampo['NOME_COMPLETO']->mValor = $nomes;
        $this->mCampo['nu_solicitacao']->mValor = '<a href="../paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso&id_solicita_visto=' . $this->getValorCampo('id_solicita_visto') . '">' . $this->getValorCampo('nu_solicitacao') . '</a>';
        if (cHTTP::$MIME != cHTTP::mmXLS){
            $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'soli_vl_cobrado', 'id_solicita_visto');
            $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'tppr_id', 'id_solicita_visto');
        }
    }

    public function RenderizeLinha() {
        parent::RenderizeLinha();
    }

}
