<?php

class cTMPL_PROCESSO_PRORROG_CLIE_EDIT extends cTEMPLATE {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_protocolo', 'Número do Protocolo'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento', 'Data de Requerimento'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_validade', 'Validade do Protocolo'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_prazo_pret', 'Prazo Pretendido'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_publicacao_dou', 'Data publicação DOU'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('ID_STATUS_CONCLUSAO', 'Status', cCOMBO::cmbSTATUS_CONCLUSAO));
    }

}
