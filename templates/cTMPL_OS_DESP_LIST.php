<?php

class cTMPL_OS_DESP_LIST extends cTMPL_OS_ABA_LISTAGEM {

    protected $VALOR_TOTAL;
    protected $HORAS_TOTAL;

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->nomeAba = "Despesas";
        $this->linkAba = "/paginas/carregueComMenu.php?controller=cCTRL_OS_DESP_LIST&metodo=Liste";
        $this->mTitulo = "OS - Timesheet";
        $this->mOrdenacaoDefault = '';
        $this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
//		cHTTP::$comCabecalhoFixo = true;
        $this->mcols = array("60px", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto");
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
        // Adiciona campos da tela
        $this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'desp_id'));
        $this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));

        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'acao', 'Ação'));
        $this->EstilizeUltimoCampo('cen');
        $this->EstilizeUltimoCampo('button');

        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATAHORA, 'desp_dt_inicio', 'Início'));
        $this->EstilizeUltimoCampo('cen');
        $this->EstilizeUltimoCampo('editavel');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATAHORA, 'desp_dt_fim', 'Fim'));
        $this->EstilizeUltimoCampo('cen');
        $this->EstilizeUltimoCampo('editavel');
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('nu_servico', 'Serviço', cCOMBO::cmbSERVICO_TIMESHEET));
        $this->EstilizeUltimoCampo('editavel');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'desp_vl_qtd', 'Quantidade'));
        $this->EstilizeUltimoCampo('editavel');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'desp_vl_valor', 'Valor'));
        $this->EstilizeUltimoCampo('editavel');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'desp_tx_tarefa', 'Tarefa'));
        $this->EstilizeUltimoCampo('editavel');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'desp_tx_observacao', 'Observação'));
        $this->EstilizeUltimoCampo('editavel');

        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
        $this->mFiltro['id_solicita_visto']->mVisivel = false;

        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON('Adicionar nova', 'NOVA', 'Clique para abrir uma nova despesa'));
    }

    public function CustomizarCabecalho() {
        
    }

    public function CustomizarLinha() {
        
    }

    public function RenderizeListagemRodape() {
        parent::RenderizeListagemRodape();
        $this->GerarOutput('</div>');
    }

}
