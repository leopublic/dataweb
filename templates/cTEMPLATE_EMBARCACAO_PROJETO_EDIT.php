<?php
class cTEMPLATE_EMBARCACAO_PROJETO_EDIT extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_EMBARCACAO_PROJETO', 'Empresa', cCOMBO::cmbEMPRESA));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMPRESA', 'Empresa', cCOMBO::cmbEMPRESA));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Nome da embarcação/projeto'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('ID_TIPO_EMBARCACAO_PROJETO', 'Tipo', cCOMBO::cmbTIPO_EMBARCACAO_PROJETO));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CONTRATO', 'Número do contrato'));
		$this->AdicioneCampo(new cCAMPO_DATA('DT_PRAZO_CONTRATO', 'Prazo do contrato'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_MUNICIPIO', 'Local'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('nu_estado', 'UF', cCOMBO::cmbUF));
		$this->AdicioneCampo(new cCAMPO_TEXTO('QT_TRIPULANTES', 'Qtd de tripulantes'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('QT_TRIPULANTES_ESTRANGEIROS', 'Qtd de estrangeiros'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CONTATO', 'Nome do contato'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_TELEFONE_CONTATO', 'Telefone'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_EMAIL_CONTATO', 'Email'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_PAIS', 'Bandeira', cCOMBO::cmbPAIS));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('cd_usuario', 'Ponto focal', cCOMBO::cmbPONTO_FOCAL));
		$this->AdicioneCampo(new cCAMPO_SIMNAO('embp_fl_ativo', 'Ativa?'));
		$this->AdicioneCampo(new cCAMPO_MEMO('DS_JUSTIFICATIVA', 'Justificativa'));
		$this->mCampo['DS_JUSTIFICATIVA']->mLarguraDupla = true;
		$this->AdicioneCampo(new cCAMPO_MEMO('TX_OBSERVACAO', 'Observações'));
		$this->mCampo['TX_OBSERVACAO']->mLarguraDupla = true;
		$this->mCampo[cTEMPLATE::CMP_ALTURA]->mValor = 450;
		$this->mCampo[cTEMPLATE::CMP_TITULO]->mValor = 'Alterar Embarcação/Projeto';
		$this->AdicioneAcao(new cACAO_METODO_POPUP_POST("Salvar","SALVAR", "Clique para editar" ));
	}
}
