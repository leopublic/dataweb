<?php
// á
class cTMPL_PAIS_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Países/nacionalidades";
		$this->mOrdenacaoDefault = 'NO_PAIS';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "auto", "auto", "100px");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_PAIS'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PAIS', 'Descrição'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PAIS_EM_INGLES', 'Descrição em inglês'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NACIONALIDADE', 'Nacionalidade'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NACIONALIDADE_EM_INGLES', 'Nacionalidade em inglês'));
		$this->EstilizeUltimoCampo('editavel');
		// $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_PAIS_OFICIAL', 'País oficial'));
		// $this->EstilizeUltimoCampo('cen');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_PAIS_PF', 'Código PF'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PAIS', 'Descrição'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NACIONALIDADE', 'Nacionalidade'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_PAIS_PF', 'Código PF'));
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar um novo pais", "cCTRL_PAIS_EDIT", "Edite" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_PAIS_EDIT", "Edite" ));
	}

	public function CustomizarLinha()
	{
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PAIS", "NO_PAIS", "CO_PAIS");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PAIS", "NO_PAIS_EM_INGLES", "CO_PAIS");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PAIS", "NO_NACIONALIDADE", "CO_PAIS");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PAIS", "NO_NACIONALIDADE_EM_INGLES", "CO_PAIS");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PAIS", "CO_PAIS_PF", "CO_PAIS");
	}
}
