<?php
class cTMPL_PROCESSO_EDIT_OS extends cTEMPLATE_EDICAO{
    /**
     *
     * @var cORDEMSERVICO
     */
    protected $os;
    public function __construct() {
        parent::__construct(__CLASS__);
        $this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'codigo'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'cd_candidato'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'cd_solicitacao'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'codigo_processo_mte'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Serviço'));

        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON('Salvar', 'SALVAR', "Clique para salvar as alterações"));
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON('Salvar e fechar a OS', 'SALVAR_E_FECHAR', "Clique para salvar as alterações e fechar a OS"));
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON('Salvar e liberar para cobrança', 'SALVAR_E_LIBERAR', "Clique para salvar as alterações e liberar a OS para cobrança"));
    }

    public function AdicionarCamposCobranca(){
        $this->AdicionePainel('Cobrança');
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('tppr_id', 'Tipo cobrança', cCOMBO::cmbTIPO_PRECO));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tppr_tx_nome', 'Tipo cobrança'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'soli_tx_obs_cobranca', 'Observação cobrança'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'soli_dt_liberacao_cobranca', 'Liberado para cobrança em:'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_liberacao_cobranca', 'Liberado para cobrança por:'));
        $this->AdicioneCampoprotegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'stco_tx_nome', 'Status cobrança:'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_nu_numero_mv', 'Resumo de cobrança:'));
    }

    public function CustomizeCabecalho() {
        // Obtem as informações que vem da OS
        $this->os = new cORDEMSERVICO();
        $this->os->mID_SOLICITA_VISTO = $this->getValorCampo('id_solicita_visto');
        $this->os->RecuperarSolicitaVistoCobranca();
        if(intval($this->os->mresc_id) > 0){
            $this->setValorCampo('resc_nu_numero_mv', cCTRL_RESUMO_COBRANCA::LinkParaOsDoResumo($this->os->mresc_id));
        }
        $this->setValorCampo('tppr_id', $this->os->mtppr_id);
        $this->setValorCampo('soli_dt_liberacao_cobranca', $this->os->msoli_dt_liberacao_cobranca);
        $this->setValorCampo('soli_tx_obs_cobranca', $this->os->msoli_tx_obs_cobranca);
        $this->setValorCampo('stco_tx_nome', $this->os->mstco_tx_nome);
        $this->setValorCampo('tppr_tx_nome', $this->os->mtppr_tx_nome);
        $this->setValorCampo('nome_liberacao_cobranca', $this->os->mnome_liberacao_cobranca);
        if ($this->os->mReadonly){
            $this->ProtejaTodosCampos();
            $this->mAcoes['SALVAR']->mVisivel = false;
            $this->mAcoes['SALVAR_E_FECHAR']->mVisivel = false;
        }

        if ($this->os->mstco_id > 1){
            $this->mAcoes['SALVAR_E_LIBERAR']->mVisivel = false;
            $this->EscondaCampo('tppr_id');
            $this->MostreCampo('tppr_tx_nome');
            $this->MostreCampo('stco_tx_nome');
            $this->ProtejaCampo('soli_tx_obs_cobranca');
        }
        else{
            $this->DesprotejaCampo('tppr_id');
            $this->DesprotejaCampo('soli_tx_obs_cobranca');
            $this->EscondaCampo('tppr_tx_nome');

            $this->EscondaCampo('soli_dt_liberacao_cobranca');
            $this->EscondaCampo('nome_liberacao_cobranca');
            $this->EscondaCampo('stco_tx_nome');
            $this->EscondaCampo('resc_nu_numero');
        }

    }
}
