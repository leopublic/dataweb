<?php
class cTMPL_SERVICO_LISTE extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Serviços";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->enveloparEmForm = true;
		$this->mCols = array("50px", "60px", "50px", "50px", "200px", "200px", "200px", "auto", "auto", "120px", "50px", "80px", "60px", "40px");
		cHTTP::$comCabecalhoFixo = true;

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_SERVICO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'serv_fl_pacote'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('serv_fl_ativo', 'Ativo?', cCOMBO::cmbSIM_NAO ));
		$this->mCampo['serv_fl_ativo']->mPermiteDefault = false;
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');
		$this->mCampo['serv_fl_ativo']->mOrdenavel = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_SERVICO_TIPO_SERVICO', 'Código'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');
		$this->mCampo['CO_SERVICO_TIPO_SERVICO']->mOrdenavel = true;
		
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'serv_tx_codigo_sg', 'Código SG'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');
		$this->mCampo['serv_tx_codigo_sg']->mOrdenavel = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_TIPO_SERVICO', 'Tipo', cCOMBO::cmbTIPO_SERVICO));
		$this->EstilizeUltimoCampo('editavel');
		$this->mCampo['NU_TIPO_SERVICO']->mOrdenavel = true;
		$this->mCampo['NU_TIPO_SERVICO']->mCampoOrdenacao = 'NO_TIPO_SERVICO';
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Nome resumido'));
		$this->EstilizeUltimoCampo('editavel');
		$this->mCampo['NO_SERVICO_RESUMIDO']->mOrdenavel = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO', 'Nome'));
		$this->EstilizeUltimoCampo('editavel');
		$this->mCampo['NO_SERVICO']->mOrdenavel = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_em_ingles', 'Nome resumido (em inglês)'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_ACOMPANHAMENTO', 'Acompanhamento'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_form_tvs', 'Form TVS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_envio_bsb', 'BSB?'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_revisao_analistas', 'Revisão<br/>analistas?'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'fl_disponivel_clientes', 'Disp. clientes?'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_INTEIRO, 'qtd_os', '#OS'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('id_tipo_acompanhamento', 'Acompanhamento', cCOMBO::cmbTIPO_ACOMPANHAMENTO));
		$this->mFiltro['id_tipo_acompanhamento']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_tipo_servico', 'Tipo de serviço', cCOMBO::cmbTIPO_SERVICO));
		$this->mFiltro['nu_tipo_servico']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('ignorar_inativos', 'Ignorar inativos', cCOMBO::cmbSIM_NAO));
		$this->mFiltro['ignorar_inativos']->mPermiteDefault = false;

		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Excel", "EXCEL", "Gerar essa consulta em Excel", "cCTRL_SERV_LIST_EXCEL", "Liste"));
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Novo serviço", "ADICIONAR", "Clique para adicionar um novo", "cCTRL_SERVICO", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para alterar esse item", "cCTRL_SERVICO", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Trash.png", "EXCLUIR", "Clique para excluir esse item", "cCTRL_SERVICO", "Exclua", "Confirma a exclusão desse serviço? Essa operação não poderá ser desfeita.", "Atenção!"));
	}
	
	public function CustomizarLinha(){
		if ($this->getValorCampo('qtd_os') == 0 && $this->getValorCampo('NU_SERVICO') != 53){
			$this->mAcoesLinha['EXCLUIR']->mVisivel = true;
		}
		else{
			$this->mAcoesLinha['EXCLUIR']->mVisivel = false;			
		}
		
		$this->setValorCampo('qtd_os', number_format($this->getValorCampo('qtd_os'), 0, ",", "."));
		
		if ($this->getValorCampo('serv_fl_ativo') != 1){
			$this->mEstiloLinha = 'inativo';
		}
		else{
			$this->mEstiloLinha = '';
		}		

		parent::CustomizarLinha();
		$parametros = '{"controller": "cCTRL_UPDT_SERV", "metodo": "serv_fl_ativo"';
		$parametros.= ', "nu_servico": "'.$this->getValorCampo('NU_SERVICO').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['serv_fl_ativo']->set_parametros($parametros);

		$parametros = '{"controller": "cCTRL_UPDT_SERV", "metodo": "nu_tipo_servico"';
		$parametros.= ', "nu_servico": "'.$this->getValorCampo('NU_SERVICO').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['NU_TIPO_SERVICO']->set_parametros($parametros);
		
		$parametros = '{"controller": "cCTRL_UPDT_SERV", "metodo": "no_servico_resumido"';
		$parametros.= ', "nu_servico": "'.$this->getValorCampo('NU_SERVICO').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['NO_SERVICO_RESUMIDO']->set_parametros($parametros);
		
		$parametros = '{"controller": "cCTRL_UPDT_SERV", "metodo": "no_servico"';
		$parametros.= ', "nu_servico": "'.$this->getValorCampo('NU_SERVICO').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['NO_SERVICO']->set_parametros($parametros);

		$parametros = '{"controller": "cCTRL_UPDT_SERV", "metodo": "serv_tx_codigo_sg"';
		$parametros.= ', "nu_servico": "'.$this->getValorCampo('NU_SERVICO').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['serv_tx_codigo_sg']->set_parametros($parametros);
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_SERV", "no_servico_em_ingles", "NU_SERVICO");
//		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_SERV", "no_servico_resumido_em_ingles", "NU_SERVICO");

	}
}
