<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_CANDIDATO_NOVO_CLIENTE extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		if (cHTTP::getLang() =='pt_br'){
			$this->mTitulo = "Novo candidato";
			$this->mMsgInicial = "Por favor, preencha inicialmente esses campos, para que verifiquemos se o candidato já está cadastrado em nossa base de dados:<br/><br/>";
		}
		else{
			$this->mTitulo = "Add new candidate";
			$this->mMsgInicial = "First of all let's check if the one you are trying to add is not already in our database:<br/><br/>";
		}
		// Adiciona campos da tela
		$this->AdicioneCampo(new cCAMPO_TEXTO('NOME_COMPLETO', 'Nome completo', 'Full name'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_MAE', "Nome da mãe", "Mother's full name"));
		$this->AdicioneCampo(new cCAMPO_DATA('DT_NASCIMENTO', "Data de nascimento  (dd/mm/aaaa)", "Birth date(dd/mm/yyyy)"));
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Continuar", "VERIFICAR", "Hit here to proceed with the verification.", "", "", "Proceed" ));
	}
}
