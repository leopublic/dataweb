<?php

class cTMPL_OS_ABA_LISTAGEM extends cTEMPLATE_LISTAGEM {

    /**
     *
     * @var cORDEMSERVICO
     */
    public $os;
    public $nomeAba;
    public $linkAba;

    public function __construct($pClass = __CLASS__) {
        parent::__construct($pClass);
        $this->enveloparEmConteudoInterno = false;
    }

    public function Renderize($pcursor) {
        $OS_i = new cINTERFACE_OS();
        $OS_i->mID_SOLICITA_VISTO = $this->os->mID_SOLICITA_VISTO;
        $OS_i->Recuperar();
        $OS_i->RecuperarCandidatos(); // Precisa para saber se deve apresentar os dados de cadastro ou nao.
        $html = $OS_i->FormCabecalhoOS();

        $html .= '
		<div class="conteudoInterno">
			<div id="tabsOS" class="aba">' . $OS_i->aba($this->nomeAba, $this->linkAba);
        $html .= '<div id="'.$this->nomeAba.'" style="padding:10px;">' . parent::Renderize($pcursor);
        $html .= '</div></div>';

        return $html;
    }

}
