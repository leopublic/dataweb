<?php
class cTMPL_PREC_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Tabelas de pre&ccedil;o - Pre&ccedil;os por servi&ccedil;o";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->mCols = array("50px", "200px", "70px", "70px", "auto", "auto",  "80px");
		$this->set_ordenacaoDefault('NO_SERVICO_RESUMIDO');
		cHTTP::$comCabecalhoFixo = true;

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'prec_id'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'tbpc_id'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_SERVICO'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'serv_fl_ativo'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'serv_fl_pacote'));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_SERVICO_NOVO', 'Descrição OS', cCOMBO::cmbSERVICO_ATIVO));
		$this->mCampo['NU_SERVICO_NOVO']->mVisivel = false;
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Descrição OS'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'serv_tx_codigo_sg', 'Código SG'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'prec_tx_item', 'Item'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'prec_tx_descricao_tabela_precos', 'Descrição cobrança'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'prec_tx_descricao_tabela_precos_em_ingles', 'Descrição cobrança (inglês)'));
		$this->EstilizeUltimoCampo('editavel');
//		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'prec_vl_pacote', 'Valor pacote(R$)'));
//		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'prec_vl_conceito', 'Valor conceito(R$)'));
//		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Criar nova", "ADICIONAR", "Clique para adicionar um novo", "cCTRL_TABELA_PRECOS", "Edite"));
		$this->EstilizeUltimoCampo('editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('tbpc_id', 'Tabela', cCOMBO::cmbTABELA_PRECO));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_tipo_servico', 'Tipo serviço', cCOMBO::cmbTIPO_SERVICO));

		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para alterar esse item", "cCTRL_PRECO", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Trash.png", "EXCLUIR", "Clique para alterar esse item", "cCTRL_PRECO", "Exclua", "Confirma a exclusão desse serviço da tabela", "Atenção"));
		$this->AdicioneAcaoLinha(new cACAO_SUBMIT_BUTTON("/imagens/grey16/Plus.png", "ADICIONAR", "Clique para adicionar esse item"));
		$this->mAcoesLinha['ADICIONAR']->mVisivel = false;
	}

	public function CustomizarLinha() {
//		$this->FormateCampoValor("prec_vl_pacote", 2);
		$this->id_linha = $this->getValorCampo('prec_id');
		if (intval($this->getValorCampo('prec_id')) > 0){
			if (intval($this->getValorCampo('serv_fl_ativo')) == '0'){
				$this->mEstiloLinha = 'alerta';
			} else {
				if ($this->getValorCampo('serv_fl_pacote') == '1'){
					$this->mEstiloLinha = 'destaque';
				} else {
					$this->mEstiloLinha = '';
				}
			}
		}

		$this->FormateCampoValor("prec_vl_conceito", 2);

		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PREC", "prec_vl_conceito", "prec_id");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PREC", "prec_tx_descricao_tabela_precos", "prec_id");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PREC", "prec_tx_descricao_tabela_precos_em_ingles", "prec_id");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PREC", "prec_tx_item", "prec_id");

		$this->mAcoesLinha['EXCLUIR']->mMsgConfirmacao = "Confirma a exclusão do serviço ".$this->getValorCampo('NO_SERVICO_RESUMIDO')." dessa tabela de preços?";
	}

	public function RenderizeListagemRodape() {
		$this->mEstiloLinha = '';
		$this->EscondaCampo('NO_SERVICO_RESUMIDO');
		$this->mCampo['NU_SERVICO_NOVO']->mExtra = " and serv_fl_ativo = 1 and nu_servico not in (select nu_servico from preco where tbpc_id=".$this->getValorFiltro('tbpc_id').")";
		$this->MostreCampo('NU_SERVICO_NOVO');
		$this->setValorCampo('serv_tx_codigo_sg', '');
		$this->setValorCampo('prec_tx_item', '');
		$this->setValorCampo('prec_tx_descricao_tabela_precos', '');
		$this->setValorCampo('prec_vl_conceito', '');
		$this->mAcoesLinha['ALTERAR']->mVisivel = false;
		$this->mAcoesLinha['ADICIONAR']->mVisivel = true;
		$this->RenderizeLinha();
		parent::RenderizeListagemRodape();
	}
}
