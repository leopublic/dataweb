<?php
class cTMPL_CAND_CLIE_EDIT_ABA_LATERAL extends cTEMPLATE_ABA{
	public function __construct(){
		parent::__construct(__CLASS__);

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_CANDIDATO'));

		$this->AdicioneAbaLink('INFO', 'Information', "/paginas/carregueCliente.php?controller=cCTRL_CAND_CLIE_EDIT_INFO&metodo=Edite");
		$this->AdicioneAbaLink('DOCS', 'Documents', "/paginas/carregueCliente.php?controller=cCTRL_CAND_CLIE_EDIT_DOCS&metodo=Edite");
		$this->AdicioneAbaLink('EXPE', 'Experience', "/paginas/carregueCliente.php?controller=cCTRL_CAND_CLIE_EDIT_EXPE&metodo=Edite");
		$this->AdicioneAbaLink('VISA', 'Visa Details', "/paginas/carregueCliente.php?controller=cCTRL_CAND_CLIE_EDIT_VISA&metodo=Edite");
		$this->AdicioneAbaLink('FILE', 'Files', "/paginas/carregueCliente.php?controller=cCTRL_CAND_CLIE_EDIT_FILE&metodo=Edite");
	}
}
