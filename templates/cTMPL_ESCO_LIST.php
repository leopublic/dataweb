<?php
// á
class cTMPL_ESCO_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Escolaridade";
		$this->mOrdenacaoDefault = 'NO_ESCOLARIDADE';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "auto", "auto", "100px");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_ESCOLARIDADE'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESCOLARIDADE', 'Descrição'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESCOLARIDADE_EM_INGLES', 'Descrição em inglês'));
		$this->EstilizeUltimoCampo('editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESCOLARIDADE', 'Descrição'));
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar uma nova escolaridade", "cCTRL_ESCO_EDIT", "Edite" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_ESCO_EDIT", "Edite" ));
	}

	public function CustomizarLinha()
	{
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_ESCO", "NO_ESCOLARIDADE", "CO_ESCOLARIDADE");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_ESCO", "NO_ESCOLARIDADE_EM_INGLES", "CO_ESCOLARIDADE");
	}

}
