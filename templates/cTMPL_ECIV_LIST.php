<?php
// á
class cTMPL_ECIV_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Estados civis";
		$this->mOrdenacaoDefault = 'NO_ESTADO_CIVIL';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "auto", "auto", "100px");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_ESTADO_CIVIL'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESTADO_CIVIL', 'Descrição'));
		$this->EstilizeUltimoCampo('editavel');		
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESTADO_CIVIL_EM_INGLES', 'Descrição em inglês'));
		$this->EstilizeUltimoCampo('editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESTADO_CIVIL', 'Descrição'));
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar um novo estado civil", "cCTRL_ECIV_EDIT", "Edite" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_ECIV_EDIT", "Edite" ));
	}

	public function CustomizarLinha()
	{
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_ECIV", "NO_ESTADO_CIVIL", "CO_ESTADO_CIVIL");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_ECIV", "NO_ESTADO_CIVIL_EM_INGLES", "CO_ESTADO_CIVIL");
	}

}
