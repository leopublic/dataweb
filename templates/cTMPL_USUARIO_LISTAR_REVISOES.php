<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_USUARIO_LISTAR_REVISOES extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Evolução das revisões por usuário";
		$this->mMsgInicial = "";
		$this->mOrdenacaoDefault = 'nome';
		$this->mCols = array("200px", "200px", "50px", "auto" );
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'cd_usuario'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nm_perfil', 'Perfil'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome', 'Usuário'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_INTEIRO , 'total', 'Qtd revisões'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_INTEIRO , 'vazio', ''));

	}
}
