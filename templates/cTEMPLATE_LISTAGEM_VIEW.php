<?php
/**
 * Template especializado para listagem, especializado para geração do código através do Mustache.
 * 
 * TODO: Incompleto
 */
class cTEMPLATE_LISTAGEM_VIEW extends cTEMPLATE{
	public $outputBuffer;
	public $outputBufferAtivo = true;

	protected $view;
	
	public function setView($pvalue){
		$this->view = $pvalue;
	}
	public function getView(){
		return $this->view;
	}
	
	public function Renderize($pcursor){
	
		$this->OutputInicializar();
		$this->CustomizarCabecalho();
		if ($this->mTitulo != '' ){
			$this->GerarOutput(cLAYOUT::RenderizeTitulo($this));
		}
		$this->GerarOutput('<div id="margem">');
		$this->GerarOutput(cLAYOUT::RenderizeListagem_Filtros($this));

		$this->GerarOutput(cLAYOUT::RenderizeListagem_Cabecalho($this));
		// Gera linha em branco para adição de registros, se for o caso
		$this->mCampo[cTEMPLATE::CMP_QTDLINHAS]->mValor = 0;
		$this->GerarOutput($this->RenderizeLinhaInclusao());
		if (is_object($pcursor)){
			if($pcursor->rowCount() > 0){
				while ($rs = $pcursor->fetch(PDO::FETCH_BOTH)){
					$this->mClasse = '';
					$this->CarregueDoRecordset($rs);
					$this->IncrementaQtdLinhas();
					$this->CustomizarLinha();
					$this->GerarOutput(cLAYOUT::RenderizeListagem_Linha($this, $this->qtdLinhas()));
					$this->SalveLinhaAnterior();
				}
			}
		}
		$this->CustomizarAcoes();
		$this->GerarOutput(cLAYOUT::RenderizeListagem_Rodape($this));
		if ($this->qtdLinhas() == 0){
			$this->GerarOutput('<br/>Nenhum registro encontrado');
		}
		$this->GerarOutput('</div>');
	}

	public function CustomizarCabecalho(){
		// Implementar override se houver necessidade
	}
	public function CustomizarLinha(){
		// Implementar override se houver necessidade
	}
	public function CustomizarAcoes(){
		// Implementar override se houver necessidade
	}

	public function RenderizeLinhaInclusao(){
		if($this->mExibirLinhaInclusao){
			$this->ResseteValoresCampos();
			$this->GerarOutput(cINTERFACE::RenderizeListagem_Linha($this, $this->qtdLinhas()));
			$this->IncrementaQtdLinhas();
		}
	}

	public function OutputInicializar(){
		$this->outputBuffer = '';
	}
	
	public function GerarOutput($pconteudo){
		if ($this->outputBufferAtivo){
			$this->outputBuffer .= $pconteudo;
		}
		else{
			print $pconteudo;
		}
	}
	
	public function view_Filtros(){
		$filtros = array();
		foreach($this->mFiltro as $filtro){
			$filtros[] = $filtro->FormatoMustache();
		}
		return $filtros;
		
	}
}
