<?php
class cTMPL_PROCESSO_EDIT_OS_CANCEL extends cTMPL_PROCESSO_EDIT_OS{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('codigo_processo_mte', 'Visto', cCOMBO::cmbPROCESSO_MTE));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('codigo_processo_prorrog', 'Prorrogação', cCOMBO::cmbPROCESSO_PRORROG));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_processo', 'Número do Processo'));
		$this->EstilizeUltimoCampo('processo');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_envio_bsb', 'Data de envio BSB'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('id_tipo_envio', 'Método de envio do processo', cCOMBO::cmbTIPO_ENVIO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_processo', 'Data de Processo'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_cancel', 'Data de Cancelamento'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'observacao', 'Observação'));
		$this->mCampo['observacao']->mLarguraDupla = true;

		$this->AdicionarCamposCobranca();
	}

	public function CustomizeCabecalho() {
		if ($this->getValorCampo('NO_SERVICO_RESUMIDO')){
			$titulo = $this->getValorCampo('NO_SERVICO_RESUMIDO');
		}
		else{
			$titulo = "Registro";
		}
		$this->mTitulo = $titulo;
		parent::CustomizeCabecalho();

        $this->setValorCampo('id_tipo_envio', $this->os->mid_tipo_envio);

		if($this->mCampo['codigo_processo_mte']->mReadonly == true){
			$this->mCampo['codigo_processo_mte']->mReadonly = false;
			$this->mCampo['codigo_processo_mte']->mDisabled = true;
		}

        if ($this->os->mReadonly){
            $this->mCampo['id_status_sol']->mReadonly = false;
            $this->mCampo['id_status_sol']->mDisabled = true;
 			$this->mCampo['id_tipo_envio']->mReadonly = false;
			$this->mCampo['id_tipo_envio']->mDisabled = true;
        }


		$this->mCampo['codigo_processo_mte']->mExtra = ' and cd_candidato='.$this->getValorCampo('cd_candidato');
		$this->mCampo['codigo_processo_prorrog']->mExtra = " and codigo_processo_mte = ".$this->getValorCampo('codigo_processo_mte');

	}
}
