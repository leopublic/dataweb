<?php
class cTMPL_OS_HIST_STAT_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Histórico de mudanças de status de solicitações";
		$this->mOrdenacaoDefault = 'hios_dt_evento';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_resumido', 'Serviço'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_cadastro', 'Criada'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'hios_dt_evento1', ''));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'hios_dt_evento2', ''));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'hios_dt_evento3', ''));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'hios_dt_evento4', ''));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'hios_dt_evento5', ''));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'hios_dt_evento6', ''));
		$this->EstilizeUltimoCampo('cen');

        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_cadastro_ini', 'Cadastrada desde'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_cadastro_fim', 'Cadastrada até'));
//		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar uma nova função", "cCTRL_FUNC_EDIT", "Edite" ));
	}

    public function CustomizarCabecalho() {
        parent::CustomizarCabecalho();
        $status = new cSTATUS_SOLICITACAO();
        for($i=1;$i<=6;$i++){
            $status->RecuperePeloId($i);
            $this->mCampo['hios_dt_evento'.$i]->mLabel = $status->mNO_STATUS_SOL_RES;
        }
    }

    public function CustomizarLinha() {
        parent::CustomizarLinha();
        $link = cCTRL_ORDEMSERVICO::LinkProcessoOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), "_blank");
        $this->setValorCampo('nu_solicitacao', $link);
    }
}
