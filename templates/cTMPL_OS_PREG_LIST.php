<?php
/**
 *
 * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada.
 */
class cTMPL_OS_PREG_LIST extends cTEMPLATE_LISTAGEM{
	public $qtd;
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Protocolo de Registro";
		$this->mCols = array("auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "200px");
		$this->mOrdenacaoDefault = 'nu_solicitacao desc';
		cHTTP::comCabecalhoFixo();
		// Adiciona campos da tela
		$this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		$this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'codigo'));

		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato'));

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'qtd', '#'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_razao_social', 'Empresa (da OS)'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_embarcacao_projeto', 'Embarcação/projeto PROCESSO'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato(s)'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_passaporte', 'Passaporte'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_protocolo', 'Protocolo'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento', 'Data'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'valor', 'Valor'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ref', 'Ref'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'depf_no_nome', 'Local'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'comprovante', 'Comprovante'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'anexos', 'Anexos'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento_ini', 'Requeridas desde'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento_fim', 'até'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
		$this->mFiltro['nu_empresa']->mQualificadorFiltro = 'sv';

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('depf_id', 'Local', cCOMBO::cmbDELEGACIA_PF));

		$onclick = "window.open('carregueComoExcel.php?controller=cCTRL_OS_PREG_LIST_EXCEL&metodo=Liste', '_blank');";
		$this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel", "EXP_EXCEL",  "Clique para exportar em Excel", $onclick));

	}

	public function RenderizeCabecalhoListagem()
	{
		parent::RenderizeCabecalhoListagem();
		cHTTP::set_nomeArquivo('protocolo_registro_'.date('Y_m_d').'.xls');
		$this->qtd = 0;
	}

	public function CustomizarLinha()
	{
		$this->qtd++;
		$this->setValorCampo('qtd', $this->qtd);
		if(cHTTP::$MIME != cHTTP::mmXLS){
			$link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$this->getValorCampo('nu_candidato').'" target="_blank">'.$this->getValorCampo('nome_completo').' ('.$this->getValorCampo('nu_candidato').')</a>';
			$this->setValorCampo('nome_completo', $link);

			$link = cCTRL_ORDEMSERVICO::LinkProcessoOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), '_blank');
			$this->setValorCampo('nu_solicitacao', $link);
		}

		$sql = "select a.*, no_tipo_arquivo, co_tipo_arquivo
				 from arquivos a
				 join tipo_arquivo ta on ta.id_tipo_arquivo = a.tp_arquivo
				where id_solicita_visto = ".$this->getValorCampo('id_solicita_visto')."
				  and TP_ARQUIVO in (3,4,9,50)";

		$xres = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		$br  = '';
		$arqs = '';
		while($xrs = $xres->fetch(PDO::FETCH_ASSOC)){
			$arq = new cARQUIVO();
			if(cHTTP::$MIME != cHTTP::mmXLS){
				$arqs .= $br.'<a href="'.cAMBIENTE::$m_dominio.'/operador/download.php?arq='.$arq->CaminhoReal($xrs['NU_CANDIDATO'], $xrs['NO_ARQUIVO'], $xrs['NU_EMPRESA'], $xrs['ID_SOLICITA_VISTO']).'" target="_blank">'.$xrs['co_tipo_arquivo']. '</a>';
			}
			else{
				$arqs .= $br.$xrs['co_tipo_arquivo'].' OK';
			}
			$br = '<br/>';
		}
		$this->setValorCampo('anexos', $arqs);

	}

}
