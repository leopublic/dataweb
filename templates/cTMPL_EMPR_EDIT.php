<?php
class cTMPL_EMPR_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Empresa";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_EMPRESA'));
		$this->AdicionePainel('Dados da empresa');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Razão social'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_ATIVIDADE_ECONOMICA', 'Código da ativ. econômica'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CNPJ', 'CNPJ'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_DDD', 'DDD'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE', 'Telefone'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO', 'Endereço'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_COMPLEMENTO_ENDERECO', 'Complemento'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_BAIRRO', 'Bairro'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_MUNICIPIO', 'Município'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_UF', 'UF', cCOMBO::cmbUF_PELO_CODIGO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CEP', 'CEP'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMAIL_EMPRESA', 'Email'));
		$this->AdicionePainel('Dados do administrador');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ADMINISTRADOR', 'Nome'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_CARGO_ADMIN', 'Cargo', cCOMBO::cmbFUNCAO));
		$this->AdicionePainel('Dados do contato principal');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CONTATO_PRINCIPAL', 'Nome'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMAIL_CONTATO_PRINCIPAL', 'Email'));
		$this->AdicionePainel('Dados do contrato social');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TE_OBJETO_SOCIAL', 'Objeto social'));
		$this->mCampo['TE_OBJETO_SOCIAL']->mLarguraDupla = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_CAPITAL_INICIAL', 'Capital inicial'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_CONSTITUICAO_EMPRESA', 'Constituição da empresa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_CAPITAL_ATUAL', 'Capital atual'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_ALTERACAO_CONTRATUAL', 'Data de alteração contratual'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_CADASTRO_BC', 'Data de cadastro no BC'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMPRESA_ESTRANGEIRA', 'Empresa estrangeira'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_INVESTIMENTO_ESTRANGEIRO', 'Investimento estrangeiro (R$)'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_INVESTIMENTO_ESTRANGEIRO', 'Data do investimento'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'QT_EMPREGADOS', 'Qtd de empregados'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'QT_EMPREGADOS_ESTRANGEIROS', 'Qtd de empregados estrangeiros'));
		$this->AdicionePainel('Controles no Dataweb');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'FL_ATIVA', 'Ativa?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('usua_id_responsavel', 'Responsável 1', cCOMBO::cmbRESPONSAVEL));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('tbpc_id', 'Tabela de preços', cCOMBO::cmbTABELA_PRECO));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('usua_id_responsavel2', 'Responsável 2', cCOMBO::cmbRESPONSAVEL));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'fl_exclusiva_cobranca', 'Exclusivamente para cobrança?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('usua_id_responsavel3', 'Responsável 3', cCOMBO::cmbRESPONSAVEL));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'empr_fl_faturavel', 'Empresa pode ser faturada?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('usua_id_responsavel4', 'Responsável 4', cCOMBO::cmbRESPONSAVEL));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'empr_fl_fatura_em_ingles', 'Descrição dos serviços em inglês?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('usua_id_responsavel5', 'Responsável 5', cCOMBO::cmbRESPONSAVEL));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('nu_empresa_prestadora', 'Empresa prestadora', cCOMBO::cmbEMPRESA_PRESTADORA));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('cfgr_id', 'Layout de relatório', cCOMBO::cmbCONFIGURACAO_RELATORIO));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar"));
	}

}
