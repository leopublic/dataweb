<?php
class cTMPL_PROCESSO_REGCIE_CLIE_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_protocolo', 'Número do Protocolo CIE'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_requerimento', 'Data de Requerimento'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'depf_no_nome', 'Delegacia PF'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_validade', 'Validade do Protocolo CIE'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_prazo_estada', 'Prazo Estada Atual'));
	}
}
