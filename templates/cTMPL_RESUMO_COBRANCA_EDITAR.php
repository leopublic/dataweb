<?php

class cTMPL_RESUMO_COBRANCA_EDITAR extends cTEMPLATE_EDICAO {

    public function __construct() {
        parent::__construct(__CLASS__);
        // Configura aspectos de apresentacao do template
        $this->mTitulo = "Criar/alterar Resumo de Cobrança";
        $this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
        $this->enveloparEmConteudoInterno = true;
        $this->qtd_colunas = 1;
        //
        // Carrega os campos
        $this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'resc_id'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_empresa_requerente'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_empresa_prestadora'));
	$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empresa_prestadora', 'Prestadora'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'resc_nu_numero_mv', 'N&ordm; RC (Mundivisas)'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'resc_nu_numero', 'N&ordm; RC (Empresa)'));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('empf_id', 'Mundivisas', cCOMBO::cmbEMPRESA_FATURAMENTO));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa COBRANÇA'));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Emb./projeto REAL', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
        // $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO', ''));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_tx_solicitante', 'Solicitante'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento', 'Data de Faturamento'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_tx_nota_fiscal', 'N&ordm; da nota fiscal'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_recebimento', 'Data de Recebimento'));
        $this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_criacao', 'Criado em'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'resc_tx_observacoes', 'Observações'));
        //
        // Carrega as acoes
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar"));
    }

    public function CustomizeCabecalho() {

        $this->mCampo['NU_EMBARCACAO_PROJETO_COBRANCA']->mExtra .= " and NU_EMPRESA = " . $this->getValorCampo('nu_empresa_requerente');
        if (intval($this->getValorCampo('resc_id')) == 0) {
            $this->EscondaCampo('resc_dt_criacao');
            $this->EscondaCampo('resc_dt_recebimento');
            $this->EscondaCampo('resc_tx_nota_fiscal');
            $this->mCampo['resc_dt_faturamento']->mValor = date('d/m/Y');
            if ($this->getValorCampo('nu_empresa_requerente') > 0){
                $emp = new cEMPRESA();
                $emp->mNU_EMPRESA = $this->getValorCampo('nu_empresa_requerente');
                $emp->RecuperePeloId();
                $this->setValorCampo('NO_RAZAO_SOCIAL', $emp->mNO_RAZAO_SOCIAL);
                $this->setValorCampo('nu_empresa_prestadora', $emp->mnu_empresa_prestadora);
                if ($emp->mnu_empresa_prestadora == 2){
                    $this->setValorCampo('empresa_prestadora', 'BCS Visas');
                } else {
                    $this->setValorCampo('empresa_prestadora', 'Mundivisas');
                }
                
            }
            // $emb = new cEMBARCACAO_PROJETO();
            // $emb->mNU_EMPRESA = $this->getValorCampo('nu_empresa_requerente');
            // $emb->mNU_EMBARCACAO_PROJETO = $this->getValorCampo('NU_EMBARCACAO_PROJETO_COBRANCA');
            // $emb->RecupereSe();
            // $this->setValorCampo('NO_EMBARCACAO_PROJETO', $emb->mNO_EMBARCACAO_PROJETO);
        } else {
            if ($this->getValorCampo('nu_empresa_prestadora') == 2){
                $this->setValorCampo('empresa_prestadora', 'BCS Visas');
            } else {
                $this->setValorCampo('empresa_prestadora', 'Mundivisas');
            }
        }
    }

}
