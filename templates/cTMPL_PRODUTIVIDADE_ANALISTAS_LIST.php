<?php
class cTMPL_PRODUTIVIDADE_ANALISTAS_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Produtividade das analistas";
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("auto", "auto", "auto", "auto");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'cd_usuario'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome', 'Analista'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'mesano', 'Mês/Ano'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'qtdProcessosAut', 'Qtd autorizações'));
		$this->EstilizeUltimoCampo('dir');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'qtdProcessosPro', 'Qtd prorrogações'));
		$this->EstilizeUltimoCampo('dir');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'qtdProcessosPre', 'Qtd pré-cadastros'));
		$this->EstilizeUltimoCampo('dir');

		$onclick = "window.open('carregueComMenu.php?controller=cCTRL_PRODUTIVIDADE_ANALISTAS_EXCEL_LIST&metodo=Liste', '_blank');";
		$this->AdicioneAcao(new cACAO_JAVASCRIPT ("Excel", "EXP_EXCEL",  "Clique para exportar em Excel", $onclick));
	}

    public function CustomizarLinha() {
        $this->setValorCampo('qtdProcessosAut', number_format($this->getValorCampo('qtdProcessosAut'), 0, ',', '.' ));
        $this->setValorCampo('qtdProcessosPro', number_format($this->getValorCampo('qtdProcessosPro'), 0, ',', '.' ));
        $this->setValorCampo('qtdProcessosPre', number_format($this->getValorCampo('qtdProcessosPre'), 0, ',', '.' ));

        $this->setValorCampo('nome', $this->getValorCampo('nome').' ('.$this->getValorCampo('cd_usuario').')');
    }
}
