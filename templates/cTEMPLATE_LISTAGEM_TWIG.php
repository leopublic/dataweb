<?php

/**
 * Template especializado para a geração de grids
 */
class cTEMPLATE_LISTAGEM_TWIG extends cTEMPLATE_LISTAGEM {

    protected $data;
    protected $menu_esquerdo;
    protected $menu_direito;

    public function set_menuEsquerdo($pmenu) {
        $this->menu_esquerdo = $pmenu;
    }

    public function set_menuDireito($pmenu) {
        $this->menu_direito = $pmenu;
    }

    public function LoopProcessamento($pcursor) {
        while ($rs = $pcursor->fetch(PDO::FETCH_BOTH)) {
            $this->ProcesseRegistro($rs);
        }
    }

    public function JSON_Data($pcursor) {
        $this->data = array();
        $this->data['lang'] = cHTTP::getLang();
        $this->data['pagina']['titulo'] = "Mundivisas::Dataweb";
        $this->data['titulo_tela'] = $this->get_titulo();
        $this->data['titulo_painel'] = "";
        $this->data['campos'] = $this->mCampo;
        $this->data['template'] = array('nome' => $this->mnome, 'multipart' => $this->mMultipart, 'controles' => $this->controles);
        $this->data['template']['notificacao'] = cNOTIFICACAO::NotificacaoPendente();
        $this->data['acoes'] = $this->mAcoes;
        $this->data['filtros'] = $this->mFiltro;
        $this->data['linhas'] = array();
        $this->data['pagina']['menu']['esquerdo'] = $this->menu_esquerdo->JSON();
        $this->data['pagina']['menu']['direito'] = $this->menu_direito->JSON();
        $this->data['qtdFiltrosVisiveis'] = $this->get_qtdFiltrosVisiveis();
        $this->data['qtdAcoesLinhaVisiveis'] = $this->get_qtdAcoesLinhaVisiveis();
        $this->data['linhas'] = array();
        if (is_object($pcursor)) {
            while ($rs = $pcursor->fetch(PDO::FETCH_BOTH)) {
                $this->ProcesseRegistro($rs);
            }
        }
        return $this->data;
    }

    public function ProcesseRegistro($prs) {
        $this->mClasse = '';
        $this->CarregueDoRecordset($prs);
        $this->RenderizeLinha();
    }

    public function RenderizeLinha() {
        $this->IncrementaQtdLinhas();
        $this->CustomizarLinha();
        $this->GerarLinha();
    }

    public function CustomizarLinha() {
        $parametros = '';
        foreach ($this->mCampo as $campo) {
            if ($campo->mChave) {
                $parametros .= '&' . $campo->mCampoBD . '=' . $campo->mValor;
            }
        }

        foreach ($this->mAcoesLinha as &$acao) {
            $acao->mParametros = $parametros;
        }
    }

    public function GerarLinha() {
        $campos = array();
        foreach ($this->mCampo as $campo) {
            if ($campo->get_ehVisivel()) {
                $campos[] = clone $campo;
            }
        }

        $acoes = array();
        foreach ($this->mAcoesLinha as $acao) {
            if ($acao->mVisivel) {
                $acoes[] = clone $acao;
            }
        }
        $linha['campos'] = $campos;
        $linha['acoesLinha'] = $acoes;
        $this->data['linhas'][] = $linha;
    }

}
