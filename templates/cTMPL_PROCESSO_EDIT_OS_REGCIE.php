<?php
class cTMPL_PROCESSO_EDIT_OS_REGCIE extends cTMPL_PROCESSO_EDIT_OS{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_protocolo', 'Número do Protocolo CIE'));
		$this->EstilizeUltimoCampo('processo');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_requerimento', 'Data de Requerimento'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'depf_no_nome', 'Delegacia PF'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_validade', 'Validade do Protocolo CIE'));
//		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCHAVE_ESTR,'nu_servico', 'Serviço', cCOMBO::cmbSERVICO_TIPO_4));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_prazo_estada', 'Prazo Estada Atual'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'nu_rne', 'Número RNE (atualizado no cand.)'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'observacao', 'Observação'));
		$this->mCampo['observacao']->mLarguraDupla = true;
		
		$this->AdicionarCamposCobranca();
	}
	
	public function CustomizeCabecalho() {
		if ($this->getValorCampo('NO_SERVICO_RESUMIDO')){
			$titulo = $this->getValorCampo('NO_SERVICO_RESUMIDO');
		}
		else{
			$titulo = "Registro";
		}
		$this->mTitulo = $titulo;
		parent::CustomizeCabecalho();
	}
}
