<?php
class cTMPL_ECIV_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mlocalAcoes = self::lca_RODAPE;
		$this->qtd_colunas = 1;
		// Adiciona campos da tela
		$this->set_titulo('Inclusão/Alteração de estado civl');
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_ESTADO_CIVIL'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESTADO_CIVIL', 'Descrição'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESTADO_CIVIL_EM_INGLES', 'Descrição em inglês'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar","SALVAR", "Clique para editar" ));
	}
}
