<?php
class cTMPL_PROCESSO_EDIT_OS_CTPS extends cTMPL_PROCESSO_EDIT_OS{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Candidato'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CTPS', 'Número CTPS'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_EXPIRACAO_CTPS', 'Expiração CTPS'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'de_observacao', 'Observação'));
		$this->mCampo['de_observacao']->mLarguraDupla = true;
		
		$this->AdicionarCamposCobranca();
	}
	
	public function CustomizeCabecalho() {
		parent::CustomizeCabecalho();
		$this->mTitulo = $this->os->mNO_SERVICO_RESUMIDO;
	}
}
