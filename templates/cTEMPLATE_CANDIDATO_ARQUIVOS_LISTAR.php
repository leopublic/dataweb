<?php
class cTEMPLATE_CANDIDATO_ARQUIVOS_LISTAR extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mCols = array('70px', 'auto', 'auto');
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_SEQUENCIAL'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_TIPO_ARQUIVO', 'Tipo', 'Type'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_ARQ_ORIGINAL', "Nome", "File name"));

		$this->AdicioneFiltroInvisivel('NU_CANDIDATO');
		$this->AdicioneAcaoLinha(new cACAO(cACAO::ACAO_LINK, '/imagens/grey16/Download.png', 'DOWNLOAD', 'Click here to download the file'));
	}
}
