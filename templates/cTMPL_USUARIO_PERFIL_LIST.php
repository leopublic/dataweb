<?php

// á
class cTMPL_USUARIO_PERFIL_LIST extends cTEMPLATE_LISTAGEM {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "";
        $this->mOrdenacaoDefault = 'nm_perfil';
        $this->mCols = array("50px", "auto");
        $this->adicionar_estilosAdicionaisGrid("semfloatheader");
        // Adiciona campos da tela
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'cd_perfil'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'cd_perfil_concedido'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'cd_perfil_sel', 'Acesso'));
        $this->EstilizeUltimoCampo('cen');
        $this->mCampo['cd_perfil_sel']->valorCustomizado = true;
        $this->mCampo['cd_perfil_sel']->mEhArray = true;
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nm_perfil', 'Perfil'));
    }

    public function CustomizarLinha() {
        $this->setValorCampo('cd_perfil_sel', $this->getValorCampo('cd_perfil'));
        if ($this->getValorCampo('cd_perfil_concedido') != '') {
            $this->mCampo['cd_perfil_sel']->checked = true;
        } else {
            $this->mCampo['cd_perfil_sel']->checked = false;
        }
    }

    public function RenderizeTitulo(){
        
    }
}
