<?php

class cTMPL_PROCESSO_EDIT_OS_COLETA extends cTMPL_PROCESSO_EDIT_OS {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_validade', 'Validade do visto'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_visto', 'Número do visto'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_emissao_visto', 'Data de emissão'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_local_emissao', 'Local de emissão'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCHAVE_ESTR, 'co_pais_nacionalidade_visto', 'País de emissão do visto', cCOMBO::cmbPAIS));
//        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NACIONALIDADE', 'País de emissão do visto'));
        $this->mCampo['NO_NACIONALIDADE']->mVisivel = false;
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCHAVE_ESTR, 'co_classificacao', 'Classificação do visto', cCOMBO::cmbCLASSIFICA_VISTO));
//        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CLASSIFICACAO_VISTO', 'Classificação do visto'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_local_entrada', 'Local de entrada (cidade)'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCHAVE_ESTR, 'co_uf', 'UF de entrada', cCOMBO::cmbUF));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_entrada', 'Data de entrada'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCHAVE_ESTR, 'nu_transporte_entrada', 'Meio de transporte', cCOMBO::cmbTRANSPORTE_ENTRADA));
//        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TRANSPORTE_ENTRADA', 'Meio de transporte'));

        $this->AdicionarCamposCobranca();
    }

    public function CustomizeCabecalho() {
        if ($this->getValorCampo('NO_SERVICO_RESUMIDO')) {
            $titulo = $this->getValorCampo('NO_SERVICO_RESUMIDO');
        } else {
            $titulo = "Coleta de visto";
        }
        $this->mTitulo = $titulo;
        parent::CustomizeCabecalho();
        if ($this->os->mReadonly) {
            $this->mCampo['co_uf']->mReadonly = false;
            $this->mCampo['co_uf']->mDisabled = true;
            
            $this->mCampo['nu_transporte_entrada']->mReadonly = false;
            $this->mCampo['nu_transporte_entrada']->mDisabled = true;
            
            $this->mCampo['co_pais_nacionalidade_visto']->mReadonly = false;
            $this->mCampo['co_pais_nacionalidade_visto']->mDisabled = true;
 
            $this->mCampo['co_classificacao']->mReadonly = false;
            $this->mCampo['co_classificacao']->mDisabled = true;
        }
    }

}
