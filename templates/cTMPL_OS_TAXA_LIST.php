<?php

/**
 *
 * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada.
 */
class cTMPL_OS_TAXA_LIST extends cTEMPLATE_LISTAGEM {

    public $os_ant;
    public $linhaPendente;
    public $total;

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "Controle de taxas";
        $this->mCols = array("auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "50px", "200px");
        $this->set_ordenacaoDefault('sv.id_solicita_visto desc, nome_completo, t.descricao');
		$this->set_exibirPaginacao(true);
		cHTTP::$comCabecalhoFixo = true;
        // Adiciona campos da tela
        $this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_perfiltaxa'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_taxa'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_taxapaga'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'no_razao_social'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'no_embarcacao_projeto'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'dt_solicitacao_fmt'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nome_cad'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato'));

        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_status_sol', 'Status'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'abertura', 'Aberta por/em'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'emp_emb', 'Empresa/emb. PROCESSO'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_resumido', 'Serviço'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato(s)'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'descricao_taxa', 'Taxa'));
        $this->EstilizeUltimoCampo('button');
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'data_taxa', 'Data'));
        $this->EstilizeUltimoCampo('cen');
        $this->EstilizeUltimoCampo('editavel');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'valor', 'Valor'));
        $this->EstilizeUltimoCampo('dir');
        $this->EstilizeUltimoCampo('editavel');
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'observacao', 'Observações'));
        $this->EstilizeUltimoCampo('editavel');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'anexos', 'GRUs'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpFILE, 'anexo', 'Nova GRU'));
        $this->EstilizeUltimoCampo('editavel');

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
        $this->mFiltro['nu_empresa']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('ID_STATUS_SOL', 'Situação', cCOMBO::cmbSTATUS_SOLICITACAO));
        $this->mFiltro['ID_STATUS_SOL']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_servico', 'Serviço', cCOMBO::cmbSERVICO));
        $this->mFiltro['nu_servico']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
        $this->mFiltro['nu_solicitacao']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_ini', 'Cadastradas desde'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_fim', 'até'));
    }

    public function CustomizarCabecalho() {
        parent::CustomizarCabecalho();
        $this->total = 0;        
    }

    public function CustomizarLinha() {
        $this->total += $this->getValorCampo('valor');
        $this->mCampo['anexo']->mNome = 'anexo_'.$this->getValorCampo('nu_candidato')."_".$this->getValorCampo('id_solicita_visto')."_".$this->getValorCampo('id_taxa');
        $this->FormateCampoValor('valor', 2);
        $this->setValorCampo('emp_emb', '<strong>' . $this->getValorCampo('no_razao_social') . '</strong><br>' . $this->getValorCampo('no_embarcacao_projeto'));
        $this->setValorCampo('abertura', $this->getValorCampo('nome_cad') . '<br>' . $this->getValorCampo('dt_solicitacao_fmt'));
        $this->setValorCampo('nu_solicitacao', cCTRL_ORDEMSERVICO::LinkDadosOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), '_blank')  );
        $links = '';
        
        $link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$this->getValorCampo('nu_candidato').'" target="_blank">'.$this->getValorCampo('nome_completo').'</a>';
        $this->setValorCampo('nome_completo', $link);
        $br = '';
        if ($this->getValorCampo('id_taxapaga') == '') {
            $parametros = '{"controller": "cCTRL_UPDT_TAXA_PAGA", "metodo": "Pagar"';
            $parametros.= ', "id_solicita_visto": "' . $this->getValorCampo('id_solicita_visto') . '"';
            $parametros.= ', "id_taxa": "' . $this->getValorCampo('id_taxa') . '"';
            $parametros.= ', "nu_candidato": "' . $this->getValorCampo('nu_candidato') . '"';
            $parametros.= ', "cd_usuario": "' . cSESSAO::$mcd_usuario . '"';
            $parametros.= '}';
            $links = '<a href="#" onclick="AcionaProcessoAtualizandoColunas(this); return false;" data-parameters=\'' . $parametros . '\'>' . $this->getValorCampo('descricao_taxa') . '</a>';
            $this->setValorCampo('descricao_taxa', $links);
            $this->mCampo['valor']->mValor = '--';
        } else {
            $parametros = '{"controller": "cCTRL_UPDT_TAXA_PAGA", "metodo": "excluir"';
            $parametros.= ', "id_taxapaga": "' . $this->getValorCampo('id_taxapaga') . '"';
            $parametros.= ', "cd_usuario": "' . cSESSAO::$mcd_usuario . '"';
            $parametros.= '}';
            $links = $this->getValorCampo('descricao_taxa').'<br/><a href="#" onclick="AcionaProcessoAtualizandoColunas(this); return false;" data-parameters=\'' . $parametros . '\'><img src="/imagens/grey16/Trash.png"/></a>';
            $this->setValorCampo('descricao_taxa', $links);
        }
        $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_TAXA_PAGA', 'data_taxa', 'id_taxapaga');
        $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_TAXA_PAGA', 'valor', 'id_taxapaga');
        $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_TAXA_PAGA', 'observacao', 'id_taxapaga');
        
        $sql = "select * from arquivos where id_taxa =".$this->getValorCampo('id_taxa')." and nu_candidato=".$this->getValorCampo('nu_candidato');
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__.'.'.__FUNCTION__);
        $grus = '';
        $br = '';
        while($rw = $res->fetch(PDO::FETCH_ASSOC)) {
            $arq = new cARQUIVO_OK;
            cBANCO::CarreguePropriedades($rw, $arq);
            $grus .= $br.'<a href="'.$arq->url().'" target="_blank">GRU</a><img src="/imagens/icons/delete.png" title="clique para remover esse arquivo" onclick="ajaxRemoverArquivo(\'' . $rw['NU_SEQUENCIAL'] . '\', \'' . $rw['NU_CANDIDATO'] . '\');" style="cursor:pointer"/>';
            $br = '<br/>';
        }
        $this->setValorCampo('anexos', $grus);
        
    }

    public function RenderizeLinha() {
        // Se mudou a OS e não é o primeiro registro, então descarrega o que está acumulado
        if ($this->getValorCampo('id_solicita_visto')> 0){
            if ($this->os_ant != '' && $this->os_ant != $this->getValorCampo('id_solicita_visto')) {
                // Salva a linha atual, que deve depois ser adicionada ao buffer
                $xlinha = array();
                foreach ($this->mCampo as $campo) {
                    $xlinha[$campo->mCampoBD] = clone $campo;
                }
                // Descarrega o buffer
                $this->RenderizeBuffer();

                //Inicializa o buffer com a linha atual salva
                unset($this->bufferLinhas);
    //			$xCampos = array();
    //			foreach($xlinha as $campo){
    //				$xCampos[$campo->mCampoBD] = clone $campo;
    //			}
    //			error_log('Zerou buffer e adicionou ultima OS '.$xCampos['nu_solicitacao']->mValor.' '.$xCampos['NOME_COMPLETO']->mValor);
    //			$this->bufferLinhas[] = $xlinha;
                $this->mCampo = $xlinha;
    //			$this->os_ant = $xlinha['id_solicita_visto']->mValor;
            }
            // Caso contrário acumula o candidato no buffer.
    //		else{
            // Clona o conteúdo dos campos
            $xCampos = array();
            foreach ($this->mCampo as $campo) {
                $xCampos[$campo->mCampoBD] = clone $campo;
            }
            // Adiciona o registro no buffer.
            $this->bufferLinhas[] = $xCampos;

            $this->os_ant = $this->getValorCampo('id_solicita_visto');
            $this->linhaPendente = true;
//		}
        }
    }

    /**
     * Descarrega o buffer para a tela
     */
    public function RenderizeBuffer() {
        // Identifica quantas linhas foram armazenadas
        $qtd = count($this->bufferLinhas);
        if ($qtd == 1) {
            $this->mEstiloLinha = "bloco";
            $this->mCampo = $this->bufferLinhas[0];
            $this->VisibilidadeCamposFixos(true);
            $this->ConfigureQtdLinhasDetalhe('');
            parent::RenderizeLinha();
        } else {
            $i = 0;
            foreach ($this->bufferLinhas as $linha) {
                $this->mCampo = $linha;
                // Imprime as colunas fixas na primeira linha
                if ($i == 0) {
                    $this->VisibilidadeCamposFixos(true);
//					$this->mCampo = $this->bufferLinhas[0];
                    $this->ConfigureQtdLinhasDetalhe($qtd);
                }
                // Esconde as colunas fixas e imprime os detalhes
                else {
                    $this->mEstiloLinha = "";
                    $this->VisibilidadeCamposFixos(false);
                }
                parent::RenderizeLinha();
                $i++;
            }
        }
    }

    public function VisibilidadeCamposFixos($pvisibilidade) {
        $this->mCampo['no_status_sol']->mVisivel = $pvisibilidade;
        $this->mCampo['abertura']->mVisivel = $pvisibilidade;
        $this->mCampo['nu_solicitacao']->mVisivel = $pvisibilidade;
        $this->mCampo['emp_emb']->mVisivel = $pvisibilidade;
        $this->mCampo['no_servico_resumido']->mVisivel = $pvisibilidade;
    }

    public function ConfigureQtdLinhasDetalhe($pqtd) {
        $this->mCampo['no_status_sol']->mRowspan = $pqtd;
        $this->mCampo['abertura']->mRowspan = $pqtd;
        $this->mCampo['nu_solicitacao']->mRowspan = $pqtd;
        $this->mCampo['emp_emb']->mRowspan = $pqtd;
        $this->mCampo['no_servico_resumido']->mRowspan = $pqtd;
    }

    public function RenderizeListagemRodape() {
        $this->RenderizeBuffer();
        $this->VisibilidadeCamposFixos(true);
        $this->ConfigureQtdLinhasDetalhe(1);
        $this->setValorCampo('no_status_sol', '');
        $this->setValorCampo('abertura', '');
        $this->setValorCampo('nu_solicitacao', '');
        $this->setValorCampo('emp_emb', '');
        $this->setValorCampo('no_servico_resumido', '');
        $this->setValorCampo('abertura', '');
        
        $this->setValorCampo('nome_completo', 'TOTAL');
        $this->setValorCampo('descricao_taxa', '');
        $this->setValorCampo('data_taxa', '');
        $this->mCampo['valor']->mReadonly = true;
        $this->setValorCampo('valor', $this->total);
        $this->mCampo['data_taxa']->mClasse = '';
        $this->mCampo['data_taxa']->mReadonly = true;
        $this->mCampo['observacao']->mReadonly = true;
        $this->mCampo['anexo']->mReadonly = true;
        $this->setValorCampo('id_solicita_visto', 0);
        $this->setValorCampo('anexos', '');
        $this->mEstiloLinha = 'total';
        $this->GerarOutput(cINTERFACE::RenderizeListagem_Linha($this, trim($this->mEstiloLinha) . ' ' . $this->get_qtdLinhas()));
        parent::RenderizeListagemRodape();
    }
    
    public function Renderize($pcursor) {
        $this->OutputInicializar();
        $this->CustomizarCabecalho();
        $this->RenderizeTitulo();
        $this->GerarOutput('<div class="conteudoInterno">');
    
        $this->GerarOutput(cINTERFACE::formTemplate($this, true));
        $this->RenderizeFiltros();
//        $this->GerarOutput('</form>');
//        $this->GerarOutput(cINTERFACE::formTemplate($this, true));
//        $this->GerarOutput('<form name="'.$this->novoFormLinhas.'" id="'.$this->novoFormLinhas.'" method="post" enctype="multipart/form-data">');
        $this->GerarOutput('<div style="width:100%; text-align:right;"><input type="submit" value="Enviar arquivos"/></div>');
        $this->RenderizeCabecalhoListagem();
        // Gera linha em branco para adição de registros, se for o caso
        $this->InicializaQtdLinhas();

        $this->RenderizeLinhaInclusao();
        if (is_object($pcursor)) {
            if ($pcursor->rowCount() > 0) {
                $this->LoopProcessamento($pcursor);
            }
        }
        $this->CustomizarAcoes();

        $this->RenderizeListagemRodape();
        if ($this->get_qtdLinhas() == 0) {
            $this->GerarOutput('<br/>' . $this->msgVazio);
        }
        $this->GerarOutput('</div>');
        $this->GerarOutput('</form>');
    }

}
