<?php
class cTMPL_PTAX_LIST extends cTEMPLATE_LISTAGEM {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "Perfis de taxa";
        $this->mOrdenacaoDefault = 'descricao';
        cHTTP::$comCabecalhoFixo = true;
        $this->mCols = array("50px", "auto", "auto", "100px");
        // Adiciona campos da tela
        $this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_perfiltaxa'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'descricao', 'Descrição'));
        $this->EstilizeUltimoCampo('editavel');

        $this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR", "Clique para cadastrar um novo pais", "cCTRL_PTAX_EDIT", "Edite"));
        $this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_PTAX_EDIT", "Edite"));
        $this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Tree.png", "TAXAS", "Clique para ver as taxas desse perfil", "cCTRL_TAXA_LIST", "Liste"));
    }

    public function CustomizarLinha() {
        $this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PTAX", "descricao", "id_perfiltaxa");
    }

}
