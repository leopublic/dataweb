<?php
/**
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_CANDIDATO_ACESSO_EXTERNO extends cTEMPLATE_EDICAO {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "";
        $this->mMsgInicial = "";
        // Adiciona campos da tela
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));

        $this->AdicioneCampo(new cCAMPO_TEXTO('email_pessoal', 'Email pessoal (login)'));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_SENHA', 'Senha'));
        $this->AdicioneCampo(new cCAMPO_SIMNAO('FL_ATUALIZACAO_HABILITADA', 'Habilitar acesso ao cadastro'));
        $this->AdicioneCampo(new cCOMBO_STATUS_EDICAO('id_status_edicao', "Situação da edição"));
        $this->mCampo['xxx']->mReadonly = true;
        $this->AdicioneCampo(new cCAMPO_SIMNAO('FL_HABILITAR_CV', 'Habilitar preenchimento de CV'));
        // Adiciona as ações que estarão disponíveis na tela
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar as alterações"));
        //	$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Enviar acesso para candidato", "ENVIAR_EMAIL", "Clique para enviar os dados de acesso ao candidato"));
    }

}
