<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_RESUMO_COBRANCA_DEMONSTRATIVO_DIARIO extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Movimentação diária";
		cHTTP::$comCabecalhoFixo = false;
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneColuna(new cCAMPO_HIDDEN('ID_TIPO_ACOMPANHAMENTO'));
		$this->AdicioneColuna(new cCAMPO_HIDDEN('resc_id'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_prestadora', 'Prestadora'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empf_tx_descricao', 'Mundivisas'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa RC'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO', 'Emb./proj. do RC'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_tx_solicitante_cobranca', 'Solicitante'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Candidato(s)'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_processo', 'Processo'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_responsavel', 'Responsável'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Serviço'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_tx_codigo_servico', 'Cód.'));
		$this->EstilizeUltimoCampo('cen editavel');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao', 'Data serviço'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento', 'Data RC'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_nu_numero_mv', 'Núm. RC'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'soli_vl_cobrado', 'Valor (R$)'));
		$this->EstilizeUltimoCampo('dir editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_prestadora', 'Empresa PRESTADORA', cCOMBO::cmbEMPRESA_PRESTADORA));
		$this->mFiltro['nu_empresa_prestadora']->mQualificadorFiltro = 'rc';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_requerente', "Empresa RC", cCOMBO::cmbEMPRESA_DA_PRESTADORA));
		$this->mFiltro['nu_empresa_requerente']->mQualificadorFiltro = 'rc';
		$this->VinculeFiltros('nu_empresa_requerente', 'nu_empresa_prestadora');
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj. RC', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
		$this->mFiltro['NU_EMBARCACAO_PROJETO_COBRANCA']->mQualificadorFiltro = 'rc';
		$this->VinculeFiltros('NU_EMBARCACAO_PROJETO_COBRANCA', 'nu_empresa_requerente');
		cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieFiltros($this, $this->mFiltro['nu_empresa_requerente'], $this->mFiltro['NU_EMBARCACAO_PROJETO_COBRANCA']));
		cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieFiltros($this, $this->mFiltro['nu_empresa_prestadora']), $this->mFiltro['nu_empresa_requerente']);

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento_ini', 'Data RC desde'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento_fim', 'Data RC até'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'pre2012', "Mostrar pré 01/12/2012?"));

		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO('Exportar para Excel', 'EXPORTAR_EXCEL', 'Clique para gerar uma cópia da consulta atual em excel', 'cCTRL_RESUMO_COBRANCA', 'DemonstrativoDiarioExcel'));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "EDITAR_FINANCEIRO", "Editar informações financeiras", "cCTRL_OS_ABA_COBR_EDIT", "Edite"));
	}

	public function CustomizarCabecalho() {
		$parametros = "";
		$parametros .= "&nu_empresa_requerente=".$this->getValorFiltro('nu_empresa_requerente');
		$parametros .= "&NU_EMBARCACAO_PROJETO_COBRANCA=".$this->getValorFiltro('NU_EMBARCACAO_PROJETO_COBRANCA');
		$parametros .= "&resc_dt_faturamento_ini=".$this->getValorFiltro('resc_dt_faturamento_ini');
		$parametros .= "&resc_dt_faturamento_fim=".$this->getValorFiltro('resc_dt_faturamento_fim');
		$parametros .= "&pre2012=".$this->getValorFiltro('pre2012');
		$this->mAcoes['EXPORTAR_EXCEL']->mParametros = $parametros;
		$this->mAcoes['EXPORTAR_EXCEL']->mpagina = 'carregueComoExcel.php';
	}

	public function CustomizarLinha() {
		$link = '<a href="../operador/os_DadosOS.php?ID_SOLICITA_VISTO='.$this->getValorCampo('id_solicita_visto').'&id_solicita_visto='.$this->getValorCampo('id_solicita_visto').'">'.$this->getValorCampo('nu_solicitacao').'</a>';
		$this->setValorCampo('nu_solicitacao', $link);
		$this->setValorCampo('resc_nu_numero_mv', cCTRL_RESUMO_COBRANCA::LinkParaOsDoResumo($this->getValorCampo('resc_id')));

		$processo = cprocesso::FabricaProcesso($this->getValorCampo('ID_TIPO_ACOMPANHAMENTO'));
		if(method_exists($processo, 'RecupereSePelaOsSemCand') && method_exists($processo, 'get_nu_processo')){
			$processo->RecupereSePelaOsSemCand($this->getValorCampo('id_solicita_visto'));
			$this->setValorCampo('nu_processo', $processo->get_nu_processo());
		}
		else{
			$this->setValorCampo('nu_processo', '(n/a)');
		}
        $this->FormateCampoValor('soli_vl_cobrado', 2);
        $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'soli_tx_codigo_servico', 'id_solicita_visto');
        $this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'soli_vl_cobrado', 'id_solicita_visto');
	}
}
