<?php
class cTEMPLATE_PROCESSO_MTE_EDIT_VISTO extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('codigo'));
		$this->AdicioneCampo(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneCampo(new cCAMPO_HIDDEN('cd_candidato'));
		$this->AdicioneCampo(new cCAMPO_HIDDEN('NU_SOLICITACAO'));
		$this->AdicioneCampo(new cCAMPO_HIDDEN('NU_SERVICO_SV'));
		$this->AdicioneCampo(new cCAMPO_HIDDEN('ID_STATUS_SOL'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMPRESA', 'Empresa', cCOMBO::cmbEMPRESA));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMBARCACAO_PROJETO', 'Embarca&ccedil;&atilde;o/Projeto', cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->AdicioneCampo(new cCAMPO_TEXTO('nu_processo', 'N&uacute;mero do processo'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_requerimento', 'Data requerimento'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('cd_funcao', 'Fun&ccedil;&atilde;o', cCOMBO::cmbFUNCAO));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('cd_reparticao', 'Reparti&ccedil;&atilde;o', cCOMBO::cmbREPARTICAO_CONSULAR));
		$this->AdicioneCampo(new cCAMPO_TEXTO('nu_oficio', 'N&uacute;mero MRE'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('nu_servico', 'Serviço', cCOMBO::cmbSERVICO_TIPO_1));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Serviço (da OS)'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_deferimento', 'Data deferimento MTE'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('prazo_solicitado', 'Prazo solicitado'));
		$this->AdicioneCampoDuplo(new cCAMPO_MEMO('observacao', 'Observação'));
		$this->mCampo[cTEMPLATE::CMP_ALTURA]->mValor = 300;

		$this->AdicioneAcao(new cACAO_METODO_POPUP_POST("Salvar", "SALVAR", "Clique para salvar as alterações"));
		$this->AdicioneAcao(new cACAO_METODO_POPUP_POST("Salvar e fechar a OS", "SALVAR_E_FECHAR", "Clique para salvar as alterações e fechar a OS correspondente"));
	}
}
