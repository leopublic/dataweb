<?php

class cTEMPLATE_CANDIDATO_EDIT_CLIENTE extends cTEMPLATE{

	public function __construct() {

		parent::__construct(__CLASS__);

		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));

		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('nu_candidato_tmp'));

		$this->AdicionePainel("Dados pessoais", "Personal data");

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PRIMEIRO_NOME', 'Primeiro nome', 'First name'));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_NOME_MEIO', 'Nome do meio', 'Middle name'));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ULTIMO_NOME', 'Último nome', 'Last name'));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_EMAIL_CANDIDATO', 'E-mail de contato', 'Contact email'));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAI', "Nome do pai", "Father's full name"));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_MAE', "Nome da mãe", "Mother's full name"));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_NACIONALIDADE', "Nacionalidade", cCOMBO::cmbNACIONALIDADE, "Nationality"));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_NIVEL_ESCOLARIDADE', "Escolaridade", cCOMBO::cmbESCOLARIDADE, "Highest level of education"));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_ESTADO_CIVIL', "Estado civil", cCOMBO::cmbESTADO_CIVIL, "Marital status"));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_SEXO', "Sexo", cCOMBO::cmbSEXO, "Gender"));

		$this->AdicioneCampo(new cCAMPO_DATA('DT_NASCIMENTO', "Data de nascimento", "Date of birth"));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_LOCAL_NASCIMENTO', "Local de nascimento", "Place of birth"));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_RNE', "RNE"));



		$this->AdicionePainel("Dados de salário (Atenção: Isso não tem objetivos fiscais. Por favor informe o seu rendimento global real.)", "Salary information (NOTE: This does NOT mean you will be subject to tax. Please state your GLOBAL monthly salary)");

		$this->AdicioneCampo(new cCAMPO_TEXTO('VA_REMUNERACAO_MENSAL', "Salário mensal", "Monthly income"));

		$this->AdicioneCampo(new cCAMPO_TEXTO('VA_REMUNERACAO_MENSAL_BRASIL', "Salário mensal no Brasil", "Monthly salary in Brazil"));



		$this->AdicionePainel("Endereço residencial", "Home address");

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ENDERECO_RESIDENCIA', 'Endereço', 'Address'));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CIDADE_RESIDENCIA', 'Cidade', 'City'));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_RESIDENCIA', "País", cCOMBO::cmbPAIS, "Country"));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_TELEFONE_CANDIDATO', 'Telefone', 'Phone #'));



		$this->AdicionePainel("Endereço comercial", "Business address");

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ENDERECO_EMPRESA', 'Endereço', 'Address'));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CIDADE_EMPRESA', 'Cidade', 'City'));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_EMPRESA', "País", cCOMBO::cmbPAIS, "Country"));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_TELEFONE_EMPRESA', 'Telefone', 'Phone #'));



		$this->AdicionePainel("Passporte", "Passport");

		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_PASSAPORTE', "Número do passaporte", "Passport number"));

		$this->AdicioneCampo(new cCAMPO_DATA('DT_EMISSAO_PASSAPORTE', "Data de emissão", "Issue date"));

		$this->AdicioneCampo(new cCAMPO_DATA('DT_VALIDADE_PASSAPORTE', "Data de expiração", "Expiry date"));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_EMISSOR_PASSAPORTE', "País emissor", cCOMBO::cmbPAIS, "Issuing government"));

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_REPARTICAO_CONSULAR', "Consulado onde o visto será coletado", "Consulate where the visa will be collected"));



		$this->AdicionePainel("Seaman's book");

		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_SEAMAN', "Número do Seaman's book", "Seaman's book number"));

		$this->AdicioneCampo(new cCAMPO_DATA('DT_EMISSAO_SEAMAN', "Data de emissão", "Issue date"));

		$this->AdicioneCampo(new cCAMPO_DATA('DT_VALIDADE_SEAMAN', "Data de expiração", "Expiry date"));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_SEAMAN', "País emissor", cCOMBO::cmbPAIS, "Issuing government"));



		$this->AdicionePainel("Descrição das atividades", "Occupation and job description");

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_FUNCAO', "Função", cCOMBO::cmbFUNCAO, "Occupation"));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PROFISSAO_CANDIDATO', "Profissão", cCOMBO::cmbPROFISSAO,  "Position"));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', "Embarcação/projeto", cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS, "Vessel/project name"));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_LOCAL_EMBARCACAO_PROJETO', "Local da embarcação/projeto", cCOMBO::cmbLOCAL_PROJETO, "Vessel / project location"));

		$this->AdicioneCampo(new cCAMPO_MEMO('TE_DESCRICAO_ATIVIDADES', "Descreva as atividades que você pretende desempenhar no Brasil", "Provide a description of the activities you expect to carry out in Brazil"));

		$this->mCampo['TE_DESCRICAO_ATIVIDADES']->mLarguraDupla = 1;

		$this->AdicioneCampo(new cCAMPO_MEMO('TE_TRABALHO_ANTERIOR_BRASIL', "Você já trabalhou no Brasil anteriormente? Em caso afirmativo, descreva brevemente onde, quando e a duração de sua estada anterior", "Have you worked in Brazil before? If so, state briefly when / place / length of last stay"));

		$this->mCampo['TE_TRABALHO_ANTERIOR_BRASIL']->mLarguraDupla = 1;

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar & Continuar editando", "SALVAR", "Hit here to save the changes but keep the form open so you can continue later.", "", "", "Save & continue editing" ));

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar & Enviar os dados para a Mundivisas", "SALVAR_E_FECHAR", "Hit here to submit the changes and notify Mundivisas you finished", "", "", "Save & submit data to Mundivisas"));

	}

}
