<?php
class cTMPL_TAXA_LIST extends cTEMPLATE_LISTAGEM {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "Perfis de taxa";
        $this->mOrdenacaoDefault = 'descricao';
        cHTTP::$comCabecalhoFixo = true;
        $this->mCols = array("50px", "auto", "auto", "100px");
        // Adiciona campos da tela
        $this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_taxa'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'descricao', 'Descrição'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'valor_fixo', 'Valor fixo'));
        $this->EstilizeUltimoCampo('editavel');

        $this->AdicioneFiltroInvisivel('id_perfiltaxa');
        $this->mFiltro['id_perfiltaxa']->mQualificadorFiltro = 't';

        $this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR", "Clique para cadastrar um novo pais", "cCTRL_TAXA_EDIT", "Edite"));
        $this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_TAXA_EDIT", "Edite"));
    }

    public function CustomizarCabecalho() {
        $this->mAcoes['INCLUIR']->AdicionaParametro('&id_perfiltaxa='.$this->getValorFiltro('id_perfiltaxa'));
    }

    public function CustomizarLinha() {
        $this->HabiliteEditInPlacePadrao("cCTRL_UPDT_TAXA", "descricao", "id_taxa");
        $this->HabiliteEditInPlacePadrao("cCTRL_UPDT_TAXA", "valor_fixo", "id_taxa");
        $this->FormateCampoValor('valor_fixo', 2);
    }

}
