<?php
class cTEMPLATE_CANDIDATO_LISTAR_CLIENTE extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Candidates";
		$this->mOrdenacaoDefault = 'NOME_COMPLETO';
		$this->mCols = array('70px', 'auto', 'auto', '100px', '100px', '80px', 'auto', '80px', '80px');
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('FL_ATUALIZACAO_HABILITADA'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Nome', 'Name'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', "Embarcação/projeto", "Vessel/project name"));

		if (cHTTP::getLang() == 'pt_br'){
			$this->AdicioneColuna(new cCAMPO_TEXTO('NO_NACIONALIDADE', "Nacionalidade", "Nationality"));
		}
		else{
			$this->AdicioneColuna(new cCAMPO_TEXTO('NO_NACIONALIDADE_EM_INGLES', "Nacionalidade", "Nationality"));
		}
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_CPF', "CPF", "CPF"));
		$this->mCampo['NU_CPF']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PASSAPORTE', "N&ordm; passaporte", "Passport #"));
		$this->mCampo['NU_PASSAPORTE']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_VALIDADE_PASSAPORTE', "Data val.", "Passport expiry date"));
		$this->mCampo['DT_VALIDADE_PASSAPORTE']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('TIPO_VISTO', "Tipo de visto", "Visa type"));
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_DEFERIMENTO', "Deferimento", "Process granted"));
		$this->mCampo['DT_DEFERIMENTO']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_PRAZO_ESTADA', "Prazo de estada", "Period of stay"));
		$this->mCampo['DT_PRAZO_ESTADA']->mClasse = 'cen';
		// Filtros
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', 'Company', cCOMBO::cmbEMPRESA));
		$this->mFiltro['NU_EMPRESA']->mVisivel = false;
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Embarcação/projeto', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS, 'Vessel/project name'));
		$this->VinculeFiltros('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Name'));
		$this->mFiltro['NOME_COMPLETO']->mTipoComparacaoFiltro = cFILTRO::tpSEL_LIKE_INI;
		// Adicionar as ações da linha
		$this->AdicioneAcaoLinha( new cACAO_LINK_CONTROLER_METODO('/imagens/man.png', 'DADOS_PESSOAIS', 'Personal data', 'cCTRL_CANDIDATO', 'DadosPessoaisEdit_cliente'));
		$this->AdicioneAcaoLinha( new cACAO_LINK_CONTROLER_METODO('/imagens/travel.png', 'DADOS_VISTO', 'Visa information', 'cCTRL_CANDIDATO', 'VisaDetails_cliente'));
		$this->AdicioneAcaoLinha( new cACAO_LINK_CONTROLER_METODO('/imagens/attach.png', 'DOCUMENTOS', 'Documentation', 'cCTRL_CANDIDATO', 'ListeArquivos'));
	}

}
