<?php
class cTMPL_PROCESSO_EDIT_OS_TIMESHEET_EXCEL extends cTEMPLATE_LISTAGEM_TWIG{
	protected $VALOR_TOTAL;
	protected $HORAS_TOTAL;

	public function __construct()
	{
		parent::__construct(__CLASS__);
		
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
	}
	
	public function JSON_Data($pcursor){
		$this->data = array();
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO = $this->getValorFiltro('id_solicita_visto');
		$os->Recuperar();
		
		cHTTP::set_nomeArquivo('OS_'.$os->mNU_SOLICITACAO.'_timesheet.xls');
		$desp = new cdespesa();
		$cursor = $desp->Listar($this->mFiltro, $this->get_ordenacao());
		$this->data['timesheet'] = array();
		if(is_object($cursor)){
			while ($rs = $cursor->fetch(PDO::FETCH_BOTH)){
				$processo = array();
				$processo['desp_dt_inicio'] = $rs['desp_dt_inicio'];
				$processo['desp_dt_fim'] = $rs['desp_dt_fim'];
				$processo['desp_tx_funcionario'] = $rs['nu_passaporte'];
				$processo['no_servico_resumido'] = $rs['no_servico_resumido'];
				$processo['desp_vl_qtd'] = $rs['desp_vl_qtd'];
				$processo['desp_vl_valor'] = $rs['desp_vl_valor'];
				$processo['desp_tx_tarefa'] = $rs['desp_tx_tarefa'];
				$this->data['timesheet'][] = $processo;
			}			
		}

		return $this->data;
	}

}
