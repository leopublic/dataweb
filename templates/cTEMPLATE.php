<?php

/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 * @property boolean $enveloparEmForm Indica se o template deve ser envelopado em um form. Default = true
 * @property boolean $enveloparEmDiv Indica se o template deve ser envelopado em um div identificado com o nome do template. Default = true
 * @property boolean $enveloparEmConteudoInterno Indica se o template deve ser envelopado em um div de conteudo interno. Default = true
 */
class cTEMPLATE {

    public $outputBuffer;
    public $outputBufferAtivo = true;
    public $enveloparEmForm;
    public $enveloparEmDiv;
    public $enveloparEmConteudoInterno;
    public $mfonteDados;
    public $mnome;
    public $mno_edicao;
    public $mid;
    public $mTabela;
    public $atd_colunas_ativas;
    /**
     *
     * @var type Obsoleto. Utilize substitua a classe do template pela classe cTEMPLATE_LISTAGEM e use o metodo get_ordenacao();
     */
    public $mOrdenacaoDefault;
    public $qtd_colunas;

    /**
     * Indica como em que parte da tela devem ser renderizadas as acoes: cTEMPLATE::lca_TOPO ou cTEMPLATE::lca_RODAPE
     * @var type
     */
    public $mlocalAcoes;

    const lca_TOPO = "1";
    const lca_RODAPE = "2";

    /**
     * Apresenta qual ação foi postada na tela.
     * @var boolean
     */
    public $mAcaoPostada;
    public $mclasseProcessadora;
    public $mTitulo;
    public $mMsgInicial;
    public $mController;
    public $mMetodo;
    private $mPainelAtual;
    private $mPainelAtual_en_us;
    protected $mSeletorDeCampos;

    const MAX_WIDTH_DIALOG = 500;

    /**
     * Classe css adicional do grid gerado.
     * @var type
     */
    public $mClasse;

    /**
     * Conjunto de campos da tela.
     * Chave do array: nome do campo
     * @var array()
     */
    public $mCampo = array();
    public $mCampoChave = array();
    public $mCampoLinhaAnterior = array();

    /**
     * Conjunto de ações disponível para a tela.
     * Chave do array: comando da ação
     * @var array()
     */
    public $mAcoes = array();

    /**
     * Conjunto de ações disponível nas linhas
     * Chave do array: comando da ação
     * @var array()
     */
    public $mAcoesLinha = array();

    /**
     * Conjunto de filtros, no caso de uma tela de consulta
     * @var $mFiltro contains cCAMPO information
     */
    public $mFiltro = array();

    /**
     * Indica se o template foi postado ou não
     * @var boolean
     */
    public $mFoiPostado;
    public $mIndLinhas;
    public $mEstiloLinha;

    /**
     * Especifica a largura das colunas
     */
    public $mCols;
    public $mEstiloTitulo;

    /**
     * Indica se o form vai enviar anexos ou não
     * @var bool
     */
    public $mMultipart;
    public $controles;
    public $mAction;

    const CMP_EDICAO = '__nomeTemplate';
    const CMP_ACAO = '__acao';
    const CMP_ORDENACAO = '__ordenacao';
    const CMP_ORDENACAO_SENTIDO = '__ordenacaoSentido';
    const CMP_ALTURA = "__altura";
    const CMP_LARGURA = "__largura";
    const CMP_TITULO = "__titulo";
    const CMP_MSG = "__msg";
    const CMP_FINALIZOU_COM_SUCESSO = "__sucesso";
    const CMP_ACAO_SUCESSO = "__acao_sucesso";
    // Migrar essa propriedade para o template listagem
    const CMP_TOTAL_LINHAS = "__total_linhas";
    const CMP_PARAMETROS = "__parametros";
    const CMP_ACAO_SUCESSO_FECHAR = "1";
    const CMP_ACAO_SUCESSO_PERMANECER = "2";

    public function __construct($pNomeTemplate) {
        $this->mnome = $pNomeTemplate;
        $this->mno_edicao = $this->mnome;
        $x = new cCAMPO_HIDDEN(self::CMP_EDICAO);
        $x->mValor = $this->mnome;
        $this->mEstiloTitulo = cINTERFACE::titPAGINA;
        $this->AdicioneCampo($x);
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_ACAO));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_ORDENACAO_SENTIDO));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_ORDENACAO));
        $this->AdicioneCampo(new cCAMPO_HIDDEN('controller'));
        $this->AdicioneCampo(new cCAMPO_HIDDEN('metodo'));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_TITULO));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_ALTURA));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_LARGURA));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_MSG));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_FINALIZOU_COM_SUCESSO));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_ACAO_SUCESSO));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_TOTAL_LINHAS));
        $this->AdicioneCampo(new cCAMPO_HIDDEN(self::CMP_PARAMETROS));
        $this->controles = $this->mCampo;
        $this->qtd_colunas = 2;
        $this->outputBufferAtivo = true;
        $this->mMultipart = false;
        $this->mSeletorDeCampos = false;
        $this->enveloparEmDiv = true;
        $this->enveloparEmForm = true;
        $this->enveloparEmConteudoInterno = true;
    }

    /**
     * Metodo criado para manter a compatibilidade dos templates de listagem que não são TEMPLATE_LISTAGEM
     * @return string
     */
    public function get_estilosAdicionaisGrid() {
        return 'grid';
    }

    public function getSeletorDeCampos() {
        return $this->mSeletorDeCampos;
    }

    public function setSeletorDeCampos($pValor) {
        $this->mSeletorDeCampos = $pValor;
    }

    public function getQtdLinhasPostadas() {
        return $this->mCampo[self::CMP_TOTAL_LINHAS]->mValor;
    }

    public function setQtdLinhasPostadas($pValor) {
        $this->mCampo[self::CMP_TOTAL_LINHAS]->mValor = $pValor;
    }

    public function getValorCampo($pNomeCampo) {
        if (isset($this->mCampo[$pNomeCampo])) {
            return $this->mCampo[$pNomeCampo]->mValor;
        } else {
            throw new Exception("Campo " . $pNomeCampo . " não existe nesse formulário");
        }
    }

    public function setValorCampo($pNomeCampo, $pValor) {
        if (isset($this->mCampo[$pNomeCampo])) {
            $this->mCampo[$pNomeCampo]->mValor = $pValor;
        } else {
            throw new Exception("Campo " . $pNomeCampo . " não existe nesse formulário");
        }
    }

    public function getValorFiltro($pNomeFiltro) {
        if (isset($this->mFiltro[$pNomeFiltro])) {
            return $this->mFiltro[$pNomeFiltro]->mValor;
        } else {
            throw new Exception("Filtro " . $pNomeFiltro . " não existe nesse formulário");
        }
    }

    public function setValorFiltro($pNomeFiltro, $pValor) {
        if (isset($this->mFiltro[$pNomeFiltro])) {
            $this->mFiltro[$pNomeFiltro]->mValor = $pValor;
        } else {
            throw new Exception("Filtro " . $pNomeFiltro . " não existe nesse formulário");
        }
    }

    public function set_titulo($pValor) {
        $this->mTitulo = $pValor;
    }

    public function get_titulo() {
        return $this->mTitulo;
    }

    public function get_acaoPostada() {
        return $this->mAcaoPostada;
    }

    public function set_acaoPostada($pValor) {
        $this->mAcaoPostada = $pValor;
    }

    public function set_msgInicial($pValor) {
        $this->mMsgInicial = $pValor;
    }

    public function get_msgInicial() {
        return $this->mMsgInicial;
    }

    public function OutputInicializar() {
        $this->outputBuffer = '';
    }

    public function GerarOutput($pconteudo) {
        if ($this->outputBufferAtivo) {
            $this->outputBuffer .= $pconteudo;
        } else {
            print $pconteudo;
        }
    }

    public function IdentificadorCampos() {
        $virg = '';
        foreach ($this->mCampo as $xCampo) {
            if ($xCampo->mChave) {
                $id.= $virg . $xCampo->mValor;
            }
        }
        return $id;
    }

    /**
     * Adiciona um campo a tela
     * @param cCAMPO $pCampo
     */
    public function AdicioneCampo($pCampo) {
        $this->mCampo[$pCampo->mNome] = $pCampo;
        $this->mCampo[$pCampo->mNome]->mPainel = $this->mPainelAtual;
    }

    /**
     * Adiciona um campo protegido a tela
     * @param cCAMPO $pCampo
     */
    public function AdicioneCampoProtegido($pCampo) {
        $this->mCampo[$pCampo->mNome] = $pCampo;
        $this->mCampo[$pCampo->mNome]->mPainel = $this->mPainelAtual;
        $this->mCampo[$pCampo->mNome]->mReadonly = true;
    }

    /**
     * Adiciona um campo de largura dupla a tela
     * @param cCAMPO $pCampo
     */
    public function AdicioneCampoDuplo($pCampo) {
        $this->mCampo[$pCampo->mNome] = $pCampo;
        $this->mCampo[$pCampo->mNome]->mPainel = $this->mPainelAtual;
        $this->mCampo[$pCampo->mNome]->mLarguraDupla = true;
    }

    /**
     * Adiciona um campo invisível a tela
     * @param cCAMPO $pCampo
     */
    public function AdicioneCampoInvisivel($pCampo) {
        $this->mCampo[$pCampo->mNome] = $pCampo;
        $this->mCampo[$pCampo->mNome]->mPainel = $this->mPainelAtual;
        $this->mCampo[$pCampo->mNome]->mVisivel = false;
    }

    /**
     * Adiciona um campo tipo coluna a tela
     * @param cCAMPO $pCampo
     */
    public function AdicioneColuna($pCampo, $pClasse = 'esq') {
        if ($pCampo->mClasse == '') {
            $pCampo->mClasse = $pClasse;
            if ($pCampo->mTipo == cCAMPO::cpVALOR_DIN_REAL || $pCampo->mTipo == cCAMPO::cpVALOR_INTEIRO) {
                $this->mCampo[$pCampo->mNome]->mClasse = "dir";
            }
        }
        $pCampo->mOrdenavel = true;
        $this->mCampo[$pCampo->mNome] = $pCampo;
        $this->mCampo[$pCampo->mNome]->mReadonly = true;
    }

    /**
     * Adiciona um campo tipo coluna chave
     * @param cCAMPO $pCampo
     */
    public function AdicioneColunaChave($pCampo) {
        $this->mCampo[$pCampo->mNome] = $pCampo;
        $this->mCampo[$pCampo->mNome]->mReadonly = true;
        $this->mCampo[$pCampo->mNome]->mChave = true;
    }

    /**
     * Adiciona um campo chave a tela
     * @param cCAMPO $pCampo
     */
    public function AdicioneCampoChave($pCampo) {
        $this->mCampo[$pCampo->mNome] = $pCampo;
        $this->mCampo[$pCampo->mNome]->mChave = true;
        $this->mCampoChave[$pCampo->mNome] = &$this->mCampo[$pCampo->mNome];
    }

    public function AdicionePainel($pPainel, $pPainel_en_us = '') {
        if (cHTTP::getLang() == 'pt_br') {
            $this->mPainelAtual = $pPainel;
        } else {
            if ($pPainel_en_us == '') {
                $pPainel_en_us = $pPainel;
            }
            $this->mPainelAtual = $pPainel_en_us;
        }
    }

    /**
     * Adiciona acoes a tela
     * @param cACAO $pAcao
     */
    public function AdicioneAcao($pAcao) {
        if (get_class($pAcao) == "cACAO_LINK_CONTROLER_METODO") {
            $pAcao->mContainerDestino = $this->mnome;
        }
        $this->mAcoes[$pAcao->mCodigo] = $pAcao;
    }

    /**
     * Adiciona acoes a tela
     * @param cACAO $pAcao
     */
    public function AdicioneAcaoLinha($pAcao) {
        if (get_class($pAcao) == "cACAO_LINK_CONTROLER_METODO") {
            $pAcao->mContainerDestino = $this->mnome;
        }
        $this->mAcoesLinha[$pAcao->mCodigo] = $pAcao;
    }

    /**
     * Adiciona acoes a tela
     * @param cCAMPO $pFiltro
     */
    public function AdicioneFiltro($pFiltro, $pOperadorFiltro = ' and ') {
//		$pFiltro->mNome = 'FILTRO_'.$pFiltro->mNome ;
//		$pFiltro->mCampoBD = $pFiltro->mNome ;
        if ($pFiltro->mTipo == cCAMPO::cpCHAVE_ESTR) {
            $pFiltro->mCombo->mValorDefault = "(todos)";
        }
        if ($pFiltro->mTipo == cCAMPO::cpCOMBO_GERAL) {
            $pFiltro->mValorDefault = "(todos)";
        }

        if ($pFiltro->mTipo == cCAMPO::cpTEXTO) {
            $pFiltro->mTipoComparacaoFiltro = cFILTRO::tpSEL_LIKE;
        }
        $pFiltro->mOperadorFiltro = $pOperadorFiltro;
        $this->mFiltro[$pFiltro->mNome] = $pFiltro;
    }

    public function AdicioneFiltroInvisivel($pNomeCampo, $pValorDefault = '', $pOperadorFiltro = ' and ') {
        $xFiltro = cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, $pNomeCampo);
        $xFiltro->mVisivel = false;
        $xFiltro->mValor = $pValorDefault;
        $xFiltro->mOperadorFiltro = $pOperadorFiltro;
        $this->mFiltro[$xFiltro->mNome] = $xFiltro;
    }

    public function utf_decodeCampos() {
        foreach ($this->mCampo as &$xCampo) {
            if ($xCampo->mTipo == cCAMPO::cpTEXTO || $xCampo->mTipo == cCAMPO::cpMEMO || $xCampo->mTipo == cCAMPO::cpHIDDEN
            )
                $xCampo->mValor = utf8_decode($xCampo->mValor);
        }
    }

    public function RecupereRegistro() {
        $where = ' where 1=1 ';
        foreach ($this->mCampo as $xCampo) {
            if ($xCampo->mChave) {
                $where.= " and " . $xCampo->mCampoBD . " = " . cBANCO::valorParaBanco($xCampo);
            }
        }

        $sql = "select * from " . $this->mfonteDados . $where;
        //error_log("sql=".$sql);
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                foreach ($this->mCampo as &$xCampo) {
                    //error_log ('testando campo '.$xCampo->mCampoBD.'='.$xCampo->mValor);
                    if (isset($rs[$xCampo->mCampoBD])) {
                        $xCampo->mValor = $rs[$xCampo->mCampoBD];
                        //error_log ($xCampo->mCampoBD.'='.$xCampo->mValor);
                    } else {
                        if (substr($xCampo->mCampoBD, 0, 2) != '__') {
                            $xCampo->mValor = '';
                        }
                    }
                }
            }
        }
    }

    public function BloqueieCamposVisiveis() {
        foreach ($this->mCampo as &$xCampo) {
            if ($xCampo->mVisivel == 1 && !$xCampo->mReadonly) {
                $xCampo->mReadonly = 1;
            }
        }
    }

    public function EscondaCampo($pNome) {
        if (isset($this->mCampo[$pNome])) {
            $this->mCampo[$pNome]->mVisivel = 0;
        }
    }

    public function MostreCampo($pNome) {
        if (isset($this->mCampo[$pNome])) {
            $this->mCampo[$pNome]->mVisivel = 1;
        }
    }

    public function ProtejaCampo($pNome) {
        if (isset($this->mCampo[$pNome])) {
            $this->mCampo[$pNome]->mReadonly = 1;
        }
    }

    public function DesprotejaCampo($pNome) {
        if (isset($this->mCampo[$pNome])) {
            $this->mCampo[$pNome]->mReadonly = 0;
        }
    }

    public function ProtejaTodosCampos() {
        foreach ($this->mCampo as &$campo) {
            if ($campo->mTipo != cCAMPO::cpCHAVE && $campo->mTipo != cCAMPO::cpCHAVE) {
                $campo->mReadonly = 1;
            }
        }
    }

    public function DisableTodosCampos() {
        foreach ($this->mCampo as &$campo) {
            if ($campo->mTipo != cCAMPO::cpCHAVE) {
                $campo->mDisabled = true;
            }
        }
    }

    public function ResseteChavePara($pNomeCampo) {
        foreach ($this->mCampo as &$campo) {
            $campo->mChave = 0;
        }
        $this->mCampo[$pNomeCampo]->mChave = 1;
    }

    /**
     * Carrega os campos da tela com os valores disponíveis no HTTP (POST, GET, etc)
     */
    public function CarregueDoHTTP($pDif = '') {
        if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . self::CMP_EDICAO]) && ( cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . self::CMP_EDICAO] == $this->mnome || $this->mnome == '')) {
            $this->mFoiPostado = true;
            $this->mAcaoPostada = cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . self::CMP_ACAO];

            // Captura ordenacao
            if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . self::CMP_ORDENACAO])) {
                $this->mCampo[self::CMP_ORDENACAO]->mValor = cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . self::CMP_ORDENACAO];
                cHTTP::$SESSION[$this->mnome . '->ORDENACAO'] = cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . self::CMP_ORDENACAO];
            }

            $this->qtd_colunas_ativas = 1;
            foreach ($this->mCampo as &$xCampo) {
                if ($xCampo->mTipo == cCAMPO::cpSIMNAO) {
                    if ($xCampo->mEhArray) {
                        $xCampo->mValor = cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif];
                    } else {
                        if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif]) && cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif] == "1") {
                            $xCampo->mValor = "1";
                        } else {
                            $xCampo->mValor = "0";
                        }
                    }
                } elseif ($xCampo->mTipo == cCAMPO::cpSIMNAO_LITERAL) {
                    if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif])) {
                        $xCampo->mValor = cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif];
                    }
                } else {
                    if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif])) {
                        if ($xCampo->mEhArray) {
                            $xCampo->mValor = cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif];
                        } else {
                            $xCampo->mValor = stripslashes(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif]);
                        }
                    } elseif (isset(cHTTP::$GET[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif])) {
                        if ($xCampo->mEhArray) {
                            $xCampo->mValor = cHTTP::$GET[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif];
                        } else {
                            $xCampo->mValor = stripslashes(cHTTP::$GET[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif]);
                        }
                    } elseif (isset(cHTTP::$GET[$xCampo->mNome])) {
                        if ($xCampo->mEhArray) {
                            $xCampo->mValor = cHTTP::$GET[$xCampo->mNome];
                        } else {
                            $xCampo->mValor = stripslashes(cHTTP::$GET[$xCampo->mNome]);
                        }
                    }
                }
                //
                // Carrega seletores marcados
                if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'SEL_' . $xCampo->mNome . $pDif])) {
                    $xCampo->mColunaAtiva = true;
                    $this->qtd_colunas_ativas++;
                } else {
                    $xCampo->mColunaAtiva = false;
                }
                cHTTP::$SESSION[$this->mnome . '->SEL_' . $xCampo->mNome] = $xCampo->mColunaAtiva;
            }

            foreach ($this->mFiltro as &$xCampo) {
                if ($xCampo->mTipo == cCAMPO::cpSIMNAO) {
                    if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'FILTRO_' . $xCampo->mNome]) && cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'FILTRO_' . $xCampo->mNome] == "1") {
                        $xCampo->mValor = "1";
                    } else {
                        $xCampo->mValor = "0";
                    }
                } elseif ($xCampo->mTipo == cCAMPO::cpSIMNAO_LITERAL) {
                    if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'FILTRO_' . $xCampo->mNome])) {
                        $xCampo->mValor = cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'FILTRO_' . $xCampo->mNome];
                    }
                } else {
                    if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'FILTRO_' . $xCampo->mNome . $pDif])) {
//						var_dump('entrou pelo post');
                        $xCampo->mValor = stripslashes(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . 'FILTRO_' . $xCampo->mNome . $pDif]);
                    } elseif (isset(cHTTP::$GET[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif])) {
//						var_dump('entrou pelo get CMP');
                        $xCampo->mValor = stripslashes(cHTTP::$GET[cINTERFACE::PREFIXO_CAMPOS . $xCampo->mNome . $pDif]);
                    } elseif (isset(cHTTP::$GET[$xCampo->mNome])) {
//						var_dump('entrou pelo get puro');
                        $xCampo->mValor = stripslashes(cHTTP::$GET[$xCampo->mNome]);
                    }
                }
                cHTTP::$SESSION[$this->mnome . '->FILTRO_' . $xCampo->mNome] = $xCampo->mValor;
            }
        } else {
//			var_dump('entrou pelo nao foi postado '.cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS.self::CMP_EDICAO]);
            $this->mFoiPostado = false;

            $this->mCampo[self::CMP_ORDENACAO]->mValor = cHTTP::$SESSION[$this->mnome . '->ORDENACAO'];

            foreach ($this->mCampo as &$xCampo) {
                if (isset(cHTTP::$GET[$xCampo->mNome])) {
                    $xCampo->mValor = stripslashes(cHTTP::$GET[$xCampo->mNome]);
                    cHTTP::$SESSION[$this->mnome . '->' . $xCampo->mNome] = stripslashes(cHTTP::$GET[$xCampo->mNome]);
                }
                // Carrega seletores de colunas salvos na session
                $xCampo->mColunaAtiva = cHTTP::$SESSION[$this->mnome . '->SEL_' . $xCampo->mNome];
            }
            foreach ($this->mFiltro as &$xCampo) {
                if (isset(cHTTP::$GET[$xCampo->mNome])) {
                    $xCampo->mValor = cHTTP::$GET[$xCampo->mNome];
                    cHTTP::$SESSION[$this->mnome . '->FILTRO_' . $xCampo->mNome] = $xCampo->mValor;
                } elseif (isset(cHTTP::$SESSION[$this->mnome . '->FILTRO_' . $xCampo->mNome])) {
                    $xCampo->mValor = stripslashes(cHTTP::$SESSION[$this->mnome . '->FILTRO_' . $xCampo->mNome]);
                }
            }
        }
    }

    /**
     * Carrega os campos da tela com os valores disponíveis em uma classe
     * Os nomes dos campos da tela devem ser os mesmos do nome da classe
     */
    public function CarregueDoOBJETO($pObjeto) {
        foreach ($this->mCampo as &$xCampo) {
            $prop = '';
            if (property_exists($pObjeto, $xCampo->mNome)) {
                $prop = $xCampo->mNome;
            } elseif (property_exists($pObjeto, 'm' . $xCampo->mNome)) {
                $prop = 'm' . $xCampo->mNome;
            }
            if ($prop != '') {
                switch ($xCampo->mTipo) {
                    case cCAMPO::cpVALOR:
                    case cCAMPO::cpVALOR_DIN_REAL:
                    case cCAMPO::cpVALOR_DIN_DOLAR:
                        $xCampo->mValor = number_format($pObjeto->$prop, 2, ',', '.');
                        break;
                    case cCAMPO::cpDATA:
                        if (strstr($pObjeto->$prop, "-")) {
                            $valor = cBANCO::DataFmt($pObjeto->$prop);
                        } else {
                            $valor = $pObjeto->$prop;
                        }
                        $xCampo->mValor = $valor;
                        break;
                    default:
                        $xCampo->mValor = $pObjeto->$prop;
                        break;
                }
            }
        }
    }

    /**
     * Carrega os campos da tela com os valores disponíveis em uma classe
     * Os nomes dos campos da tela devem ser os mesmos do nome da classe
     */
    public function CarregueDoRecordset($pRs) {
        if (is_array($pRs)) {
            foreach ($this->mCampo as &$xCampo) {
                $prop = $xCampo->mNome;
                if (array_key_exists($prop, $pRs)) {
                    $xCampo->mValor = $pRs[$prop];
                }
            }
        } else {
            throw new exception("Fonte de dados apresentada não é um recordset");
        }
    }

    /**
     * Carrega os atributos de uma classe com os valores dos campos de um template
     * Os nomes dos campos da tela devem ser os mesmos do nome da classe
     */
    public function AtribuaAoOBJETO($pObjeto, $pextensao = 'm') {
        foreach ($this->mCampo as &$xCampo) {
            $prop = $xCampo->mNome;
            $mprop = $pextensao . $xCampo->mNome;
            if (property_exists($pObjeto, $prop)) {
                $pObjeto->$prop = $xCampo->mValor;
            } elseif (property_exists($pObjeto, $mprop)) {
                $pObjeto->$mprop = $xCampo->mValor;
            }
        }
    }

    public function TratamentoPadraoDeMensagens() {
        $ret = '';
        if (cHTTP::$SESSION['msg'] != '') {
            $tipo_msg = '';
            if (cHTTP::$SESSION['tipo_msg'] != '') {
                $tipo_msg = cHTTP::$SESSION['tipo_msg'];
            } else {
                $tipo_msg = cNOTIFICACAO::TM_ALERT;
            }

            $ret .= '<div id="dialog" style="display: none;" title="' . cHTTP::TituloMsg() . '"><div class="ui-state-' . $tipo_msg . '">' . cHTTP::$SESSION['msg'] . '</div></div>';
            cHTTP::AdicionarScript("$('#dialog').dialog({minWidth:500}) ;");
        } else {
            if (cNOTIFICACAO::TemNotificacaoPendente()) {
                $ret .= cNOTIFICACAO::HTML_NotificacaoPendente();
            }
        }
        unset(cHTTP::$SESSION['msg']);
        unset(cHTTP::$SESSION['tipo_msg']);
        unset(cHTTP::$SESSION['msgTitulo']);
        return $ret;
    }

    /**
     * Mantido para garantir compatibilidade com versões anteriores. Deve ser substituido pelo get_ordenacao do TEMPLATE_LISTAGEM
     * @return string
     */
    public function ordenacao() {
        $virgula = '';
        $ordenacao = '';
        if ($this->mCampo[self::CMP_ORDENACAO]->mValor != '') {
            $ordenacao .= $virgula . $this->mCampo[self::CMP_ORDENACAO]->mValor . ' ' . $this->mCampo[self::CMP_ORDENACAO_SENTIDO]->mValor;
            $virgula = ', ';
        }
        if ($this->mOrdenacaoDefault != '') {
            $ordenacao .= $virgula . $this->mOrdenacaoDefault;
        }
        return $ordenacao;
    }

    public function VinculeCombos($pNomeComboFilho, $pNomeComboPai) {
        $this->mCampo[$pNomeComboFilho]->campoPai = &$this->mCampo[$pNomeComboPai];
    }

    public function VinculeFiltros($pNomeComboFilho, $pNomeComboPai) {
        $this->mFiltro[$pNomeComboFilho]->campoPai = &$this->mFiltro[$pNomeComboPai];
    }

    public function CampoLinha($pNomeCampo, $linha) {
        if (isset(cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $pNomeCampo . $linha])) {
            return cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . $pNomeCampo . $linha];
        }
    }

    public function FiltrosMustache() {
        $filtros = array();
        foreach ($this->mFiltro as $filtro) {
            $filtros[] = $filtro->FormatoMustache();
        }
        return $filtros;
    }

    public function MustacheSeletorDeColunas() {
        $painelAnt = '';
        $seletorDeColunas = array();
        $campos = array();
        $paineis = array();
        $camposAtivos = array();
        foreach ($this->mCampo as &$campo) {
            if ($campo->mPainel != $painelAnt && $painelAnt != '') {
                $paineis[] = array("nome" => $painelAnt, "campos" => $campos);
                $painelAnt = $campo->mPainel;
                $campos = array();
            } else {
                $painelAnt = $campo->mPainel;
            }

            if (!$campo->mColunaSelecionavel && $campo->mVisivel) {
                $campo->mColunaAtiva = true;
            }
    
            if ($campo->mVisivel && $campo->mTipo <> cCAMPO::cpHIDDEN) {
                $campos[] = $campo;
            }


//			if ($campo->mValor == true ){
//				$camposAtivos[$campo->mNome] = true;
//				$campo->mColunaAtiva = true;
//			}
//			else{
//				$campo->mColunaAtiva = false;
//			}
            $camposAtivos[$campo->mNome] = $campo->mColunaAtiva;
        }
        $paineis[] = array("nome" => $painelAnt, "campos" => $campos);
        $seletorDeColunas = array("paineis" => $paineis, "camposAtivos" => $camposAtivos);
        return $seletorDeColunas;
    }

    public function EstilizeUltimoCampo($pEstilo) {
        end($this->mCampo);
        $this->mCampo[key($this->mCampo)]->mClasse .= " " . $pEstilo;
    }

    public function MustacheColunas() {
        $saida = array();
    }

    public function FormateCampoData($pNomeCampo, $pcomSeculo = true, $pcomHora = false) {
        $this->setValorCampo($pNomeCampo, cBANCO::DataFmt($this->getValorCampo($pNomeCampo), $pcomSeculo, $pcomHora));
    }

    public function FormateCampoValor($pNomeCampo, $pQtdCasas) {
        $this->setValorCampo($pNomeCampo, number_format(($this->getValorCampo($pNomeCampo)), $pQtdCasas, ",", "."));
    }

    public function FormateCampoVazioComTracos($pNomeCampo) {
        if ($this->getValorCampo($pNomeCampo) == '') {
            $this->setValorCampo($pNomeCampo, '--');
        }
    }

}
