<?php
class cTMPL_PROCESSO_COLETA_CLIE_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'no_validade', 'Validade do visto'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'nu_visto', 'Número do visto'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_emissao_visto', 'Data de emissão'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'no_local_emissao', 'Local de emissão'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('co_pais_nacionalidade_visto', 'País de emissão do visto', cCOMBO::cmbPAIS));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('co_classificacao', 'Classificação do visto', cCOMBO::cmbCLASSIFICA_VISTO));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'no_local_entrada', 'Local de entrada (cidade)'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('co_uf', 'UF de entrada', cCOMBO::cmbUF));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_entrada', 'Data de entrada'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('nu_transporte_entrada', 'Meio de transporte', cCOMBO::cmbTRANSPORTE_ENTRADA));
	}
}
