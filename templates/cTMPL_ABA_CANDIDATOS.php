<?php

class cTMPL_ABA_CANDIDATOS extends cTEMPLATE_ABA implements iTEMPLATE_ABA{

	public function __construct() {

		parent::__construct(__CLASS__);

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));

	}



	public function AdicionaAbaCandidato($pNU_CANDIDATO, $pNOME_COMPLETO){

		$this->AdicioneAbaAjax($pNU_CANDIDATO, $pNOME_COMPLETO, "cCTRL_FORM_TVS" , "Edite", "Clique para acessar a aba do formulário TVS da OS" );

		$this->mAbas[$pNU_CANDIDATO]->mParametros .= '&nu_candidato='.$pNU_CANDIDATO;

	}

}
