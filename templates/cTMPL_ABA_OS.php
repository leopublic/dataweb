<?php
class cTMPL_ABA_OS extends cTEMPLATE_ABA implements iTEMPLATE_ABA{
	public function __construct() {
		parent::__construct(__CLASS__);

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_CANDIDATO'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'ID_SOLICITA_VISTO'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));

		$this->AdicioneAbaLink('DADOS_OS', 'Dados OS', "../operador/os_DadosOS.php", "Clique para acessar a aba de dados da OS");
		$this->AdicioneAbaLink('CADASTRO', 'Cadastro', "../operador/os_Cadastro.php", "Clique para acessar a aba de cadastro da OS");
		$this->AdicioneAbaLink('FORMULARIOS', 'Formulários', "../operador/os_Formularios.php", "Clique para acessar a aba de formulários da OS");
		$this->AdicioneAbaLink('PROCESSO', 'Processo', "../paginas/carregueComMenu.php", "Clique para acessar a aba de processo da OS");
		$this->mAbas['PROCESSO']->mParametros = 'controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso';
		$this->AdicioneAbaLink('QUALIDADE', 'Qualidade & Liberação', "../operador/os_Liberacao.php", "Clique para acessar a aba de liberação da OS" );
		$this->AdicioneAbaAjax('TVS', 'Formulário TVS', "cCTRL_FORM_TVS" , "Edite", "Clique para acessar a aba do formulário TVS da OS" );
	}
}
