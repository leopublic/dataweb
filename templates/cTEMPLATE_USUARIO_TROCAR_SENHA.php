<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_USUARIO_TROCAR_SENHA extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Alterar senha";
		$this->mMsgInicial = "";
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('cd_usuario'));

		$this->AdicioneCampo(new cCAMPO_TEXTO('nome', 'Usuário', 'Name'));
		$this->AdicioneCampo(new cCAMPO_('nm_senha', 'Senha', 'Password'));
		$this->AdicioneCampo(new cCAMPO_('nm_senha_conf', 'Confirme', 'Repeat'));
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar as alterações", "", "", "Submit"));
	}
}
