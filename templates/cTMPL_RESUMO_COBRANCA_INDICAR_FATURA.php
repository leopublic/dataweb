<?php
class cTMPL_RESUMO_COBRANCA_INDICAR_FATURA extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		// Configura aspectos de apresentacao do template
		$this->mTitulo = "Resumo de Cobran&ccedil;a - Indicar fatura emitida";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
		$this->qtd_colunas = 1;
		//
		// Carrega os campos
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'resc_id'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_nu_numero', 'Resumo n&ordm;'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento', 'Data de Faturamento'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa', cCOMBO::cmbEMPRESA));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO', 'Centro de custos', cCOMBO::cmbEMBARCACAO_PROJETO));

//		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA(cCAMPO::cpTEXTO, 'strc_id', 'Situa&ccedil;&atilde;o', cCOMBO::cmbSITUACAO_RESUMO_COBRANCA));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_tx_nota_fiscal', 'N&ordm; da nota fiscal'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_criacao', 'Criada em'));
		//
		// Carrega as acoes
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar"));
	}
}
