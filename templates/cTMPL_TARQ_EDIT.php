<?php
class cTMPL_TARQ_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mlocalAcoes = self::lca_RODAPE;
		$this->qtd_colunas = 1;
		// Adiciona campos da tela
		$this->set_titulo('Inclusão/Alteração de Tipo de arquivo');
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'ID_TIPO_ARQUIVO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_ARQUIVO', 'Descrição'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_ARQUIVO_EM_INGLES', 'Descrição em inglês'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'CO_TIPO_ARQUIVO', 'Mnemônico'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'fl_disponivel_clientes', 'Disponível para clientes?'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar","SALVAR", "Clique para editar" ));
	}
}
