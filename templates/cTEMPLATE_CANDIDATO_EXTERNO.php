<?php

/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_CANDIDATO_EXTERNO extends cTEMPLATE_EDICAO {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "";
//        if (cHTTP::getLang() == 'pt_br') {
//            $this->mMsgInicial = "Por favor preencha com atenção, é muito importante que os dados estejam completos e corretamente preenchidos para evitar atrasos na obtenção do Visto. Please fill in all the blank spaces.<br/><br/><b>Você necessitará dos seguintes documentos no consulado quando estiver coletando seu visto daqui a algumas semanas:</b><br/>1) Police letter - necessária para <b>todos</b> vistos (exceto para visto emergencial de 30 dias e visto técnico de 90 dias);<br/>2) Passaporte com validade equivalente ao período do visto e o mínimo de 4 páginas em branco.<br/><br/>";
//        } else {
//            $this->mMsgInicial = "Please fill out this form carefully, it is very important that the data given be complete and correct to avoid delays with the Visa authorization. Please fill in all the blank spaces.<br/><br/><b>You will need the following paperwork to submit to the Consulate when collecting your visa in a few weeks from now:</b><br/>1) Police letter - required for <b>all</b> visas (except 30 day emergencial and 90 day technical visas);<br/>2) Passport with validity equal to the visa period and with a minimun of 4 clean pages.<br/><br/>";
//        }
        // Adiciona campos da tela
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('nu_candidato_tmp'));
        $this->AdicionePainel("Dados pessoais", "Personal data");
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_PRIMEIRO_NOME', 'Primeiro nome', 'First name'));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_NOME_MEIO', 'Nome do meio', 'Middle name'));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_ULTIMO_NOME', 'Último nome', 'Last name'));
//        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_EMAIL_CANDIDATO', 'E-mail de contato', 'Contact email'));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAI', "Nome do pai", "Father's full name"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('co_pais_nacionalidade_pai', "Nacionalidade Pai", cCOMBO::cmbNACIONALIDADE, "Father's Nationality"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_MAE', "Nome da mãe", "Mother's full name"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('co_pais_nacionalidade_mae', "Nacionalidade Mãe", cCOMBO::cmbNACIONALIDADE, "Mother's Nationality"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_NACIONALIDADE', "Nacionalidade", cCOMBO::cmbNACIONALIDADE, "Nationality"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_NIVEL_ESCOLARIDADE', "Escolaridade", cCOMBO::cmbESCOLARIDADE, "Highest level of education"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_ESCOLARIDADE_EM_INGLES', "Escolaridade", "Highest level of education"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_ESTADO_CIVIL', "Estado civil", cCOMBO::cmbESTADO_CIVIL, "Marital status"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_ESTADO_CIVIL_EM_INGLES', "Escolaridade", "Highest level of education"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_SEXO', "Sexo", cCOMBO::cmbSEXO, "Gender"));
        $this->AdicioneCampo(new cCAMPO_DATA('DT_NASCIMENTO', "Data de nascimento", "Date of birth"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_LOCAL_NASCIMENTO', "Local de nascimento", "Place of birth"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NU_RNE', "RNE"));

        $this->AdicionePainel("Dados de salário");
        $this->AdicioneCampo(new cCAMPO_TEXTO('VA_REMUNERACAO_MENSAL', "Salário mensal", "Monthly income"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('VA_REMUNERACAO_MENSAL_BRASIL', "Salário mensal no Brasil", "Monthly salary in Brazil"));

        $this->AdicionePainel("Endereço residencial", "Home address");
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_ENDERECO_RESIDENCIA', 'Endereço', 'Address'));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_CIDADE_RESIDENCIA', 'Cidade', 'City'));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_PAIS_RESIDENCIA', "País", cCOMBO::cmbPAIS, "Country"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NU_TELEFONE_CANDIDATO', 'Telefone', 'Phone #'));

        $this->AdicionePainel("Endereço comercial", "Business address");
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_ENDERECO_EMPRESA', 'Endereço', 'Address'));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_CIDADE_EMPRESA', 'Cidade', 'City'));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_PAIS_EMPRESA', "País", cCOMBO::cmbPAIS, "Country"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NU_TELEFONE_EMPRESA', 'Telefone', 'Phone #'));

        $this->AdicionePainel("Passporte", "Passport");
        $this->AdicioneCampo(new cCAMPO_TEXTO('NU_PASSAPORTE', "Número do passaporte", "Passport number"));
        $this->AdicioneCampo(new cCAMPO_DATA('DT_EMISSAO_PASSAPORTE', "Data de emissão", "Issue date"));
        $this->AdicioneCampo(new cCAMPO_DATA('DT_VALIDADE_PASSAPORTE', "Data de expiração", "Expiry date"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_PAIS_EMISSOR_PASSAPORTE', "País emissor", cCOMBO::cmbPAIS, "Issuing government"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_REPARTICAO_CONSULAR', "Consulado onde o visto será coletado", cCOMBO::cmbREPARTICAO_CONSULAR, "Consulate where the visa will be collected"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_REPARTICAO_CONSULAR', "Consulado onde o visto será coletado", "Consulate where the visa will be collected"));

        $this->AdicionePainel("Seaman's book");
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_SEAMAN', "Número do Seaman's book", "Seaman's book number"));
        $this->AdicioneCampo(new cCAMPO_DATA('DT_EMISSAO_SEAMAN', "Data de emissão", "Issue date"));
        $this->AdicioneCampo(new cCAMPO_DATA('DT_VALIDADE_SEAMAN', "Data de expiração", "Expiry date"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_PAIS_SEAMAN', "País emissor", cCOMBO::cmbPAIS, "Issuing government"));

        $this->AdicionePainel("Descrição das atividades", "Occupation and job description");
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', "Empresa", cCOMBO::cmbEMPRESA));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_EMPRESA_ESTRANGEIRA', "Empresa", "Employer"));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Embarcação/projeto', cCOMBO::cmbEMBARCACAO_PROJETO));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', "Embarcação/projeto", "Vessel / project name"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_FUNCAO', "Função", cCOMBO::cmbFUNCAO, "Occupation"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_FUNCAO_EM_INGLES', "Função", "Occupation"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('CO_PROFISSAO_CANDIDATO', "Profissão", cCOMBO::cmbPROFISSAO, "Position"));
        $this->AdicioneCampo(new cCAMPO_TEXTO('NO_PROFISSAO_EM_INGLES', "Função", "Occupation"));
        $this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('loca_id', "Local da embarcação/projeto", cCOMBO::cmbLOCAL_PROJETO, "Vessel / project location"));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TE_DESCRICAO_ATIVIDADES', "Descreva as atividades que você pretende desempenhar no Brasil"));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TE_TRABALHO_ANTERIOR_BRASIL', "Você já trabalhou no Brasil anteriormente? Em caso afirmativo, descreva brevemente onde, quando e a duração de sua estada anterior"));
//        $this->AdicioneCampo(new cCAMPO_MEMO('TE_TRABALHO_ANTERIOR_BRASIL', "Você já trabalhou no Brasil anteriormente? Em caso afirmativo, descreva brevemente onde, quando e a duração de sua estada anterior"));

        // Adiciona as ações que estarão disponíveis na tela
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar & Continuar editando", "SALVAR", "Hit here to save the changes but keep the form open so you can continue later.", "", "", "Save & continue editing"));
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar & Enviar os dados para a Mundivisas", "SALVAR_E_FECHAR", "Hit here to submit the changes and notify Mundivisas you finished", "", "", "Save & submit data to Mundivisas"));
        $this->VinculeCombos('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');
        cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieCombos($this, $this->mCampo['NU_EMPRESA'], $this->mCampo['NU_EMBARCACAO_PROJETO'], ""));

        }

}
