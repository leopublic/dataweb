<?php
class cTMPL_FUNC_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Funções/Cargos";
		$this->mOrdenacaoDefault = 'NO_FUNCAO';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "250px", "250px", "80px", "auto");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_FUNCAO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_FUNCAO', 'Nome da função'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_FUNCAO_EM_INGLES', 'Nome da função em inglês'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_FUNCAO_ALTERNATIVO', 'Nome alternativo (imp.)'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'NU_CBO', 'CBO'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TX_DESCRICAO_ATIVIDADES', 'Descrição atividades'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_FUNCAO', 'Nome da função'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'NU_CBO', 'CBO'));

		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar uma nova função", "cCTRL_FUNC_EDIT", "Edite" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_FUNC_EDIT", "Edite" ));
	}

	public function CustomizarLinha(){
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_FUNC", "NO_FUNCAO", "CO_FUNCAO");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_FUNC", "NO_FUNCAO_EM_INGLES", "CO_FUNCAO");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_FUNC", "NO_FUNCAO_ALTERNATIVO", "CO_FUNCAO");

	}
}
