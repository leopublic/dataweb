<?php
class cTMPL_PROCESSO_EDIT_OS_TIMESHEET extends cTEMPLATE_LISTAGEM{
	protected $VALOR_TOTAL;
	protected $HORAS_TOTAL;

	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Timesheet";
		$this->mOrdenacaoDefault = '';
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
//		$this->enveloparEmForm = false;
//		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("40px", "auto", "auto", "auto", "auto", "80px", "200px", "auto", "auto");
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'desp_id'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'acao', 'Ação'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('button');
		
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATAHORA, 'desp_dt_inicio', 'Início'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATAHORA, 'desp_dt_fim', 'Fim'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATAHORA, 'desp_dt_diff', 'Intervalo'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'desp_tx_funcionario', 'Funcionário'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('nu_servico', 'Serviço', cCOMBO::cmbSERVICO_TIMESHEET));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'desp_vl_qtd', 'Quantidade'));
		$this->EstilizeUltimoCampo('dir');
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'desp_vl_valor', 'Valor'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'desp_tx_tarefa', 'Tarefa'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'desp_tx_observacao', 'Observação'));
		$this->EstilizeUltimoCampo('editavel');
		
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		$this->mFiltro['id_solicita_visto']->mVisivel= false;

		$this->AdicioneAcaoLinha(new cACAO_SUBMIT_BUTTON('Excluir', 'EXCLUIR', 'Clique para excluir a despesa'));

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON('Adicionar nova', 'NOVA', 'Clique para abrir uma nova despesa'));
//		$this->AdicioneAcao(new cACAO_JAVASCRIPT ("Gerar Excel", "EXP_EXCEL",  "Clique para exportar em Excel", $onclick));
	}

	public function CustomizarCabecalho() {
		$onclick = "window.open('carregueComMenu.php?controller=cCTRL_PROCESSO_EDIT_OS_TIMESHEET_EXCEL&metodo=Liste&id_solicita_visto=".$this->getValorFiltro('id_solicita_visto')."', '_blank');";
		$this->mAcoes['EXP_EXCEL']->mOnClick =$onclick;
	}
	
//	public function RenderizeTitulo() {
//		parent::RenderizeTitulo();
//		$os = new cINTERFACE_OS();
//		$os->mID_SOLICITA_VISTO = $this->getValorFiltro('id_solicita_visto');
//		$html = $os->FormCabecalhoOS();
//		$this->GerarOutput($html);
//		$this->GerarOutput('<div id="tabsOS" class="aba">');
//		$html = $os->aba("Processo", '/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso');
//		$this->GerarOutput($html);
//		
//	}
	
	public function CustomizarLinha()
	{
		$parametros = '{"controller": "cCTRL_UPDT_DESP", "metodo": "Excluir"';
		$parametros.= ', "chave": "'.$this->getValorCampo('desp_id').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';			
		$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'><i class="icon-trash"></i></a>';
		
		$this->FormateCampoValor('desp_vl_qtd', 2);
		$this->FormateCampoValor('desp_vl_valor', 2);
		$this->FormateCampoData('desp_dt_inicio', true, true);
		$this->FormateCampoData('desp_dt_fim', true, true);
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_DESP', 'desp_dt_inicio', 'desp_id');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_DESP', 'desp_dt_fim', 'desp_id');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_DESP', 'nu_servico', 'desp_id');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_DESP', 'desp_vl_qtd', 'desp_id');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_DESP', 'desp_vl_valor', 'desp_id');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_DESP', 'desp_tx_funcionario', 'desp_id');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_DESP', 'desp_tx_tarefa', 'desp_id');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_DESP', 'desp_tx_observacao', 'desp_id');
	}
	
	public function RenderizeListagemRodape() {
		parent::RenderizeListagemRodape();
		$this->GerarOutput('</div>');
		
	}

}
