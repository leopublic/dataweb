<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_CAND_DOCS_EDIT extends cTEMPLATE_EDICAO{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "";
		$this->mMsgInicial = "";
		$this->mEstiloTitulo = cINTERFACE::titSUB;
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;

		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_CANDIDATO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'cand_fl_doc_1', 'Tem cópia do passaporte?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'cand_fl_doc_2', 'Tem formulário 1344 assinado?'));

		// Adiciona as ações que estarão disponíveis na tela		
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar","SALVAR", "Clique para salvar as alterações" ));
	}
}
