<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_DEPS_EDIT extends cTEMPLATE_EDICAO{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "";
		$this->mMsgInicial = "";
		$this->mEstiloTitulo = cINTERFACE::titSUB;
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
		$this->set_msgInicial('');

		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_CANDIDATO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PRIMEIRO_NOME', 'Primeiro Nome'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NOME_MEIO', 'Nome meio'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ULTIMO_NOME', 'Último nome'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('co_grau_parentesco', 'Parentesco', cCOMBO::cmbGRAU_PARENTESCO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_NASCIMENTO', 'Data de nascimento'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_NACIONALIDADE', 'Nacionalidade', cCOMBO::cmbNACIONALIDADE));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_PASSAPORTE', 'No. passaporte'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_VALIDADE_PASSAPORTE', 'Validade passaporte'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_EMISSOR_PASSAPORTE', 'País emissor', cCOMBO::cmbPAIS));

		// Adiciona as ações que estarão disponíveis na tela		
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Adicionar","SALVAR", "Clique para criar esse dependente" ));
	}
	
	public function CustomizeCabecalho() {
		$this->mCampo['CO_PAIS_EMISSOR_PASSAPORTE']->mPermiteDefault = true;
		$this->mCampo['CO_NACIONALIDADE']->mPermiteDefault = true;
	}
}
