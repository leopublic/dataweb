<?php
class cTMPL_EMPRESA_EDIT_END_COB extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Alterar endereço de cobrança";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_EMPRESA'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_razao_social_cobranca', 'Razão social'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_cnpj_cobranca', 'CNPJ'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_endereco_cobranca', 'Endereço'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_complemento_endereco_cobranca', 'Complemento'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_bairro_cobranca', 'Bairro'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_municipio_cobranca', 'Município'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_cep_cobranca', 'CEP'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('co_uf_cobranca', 'UF', cCOMBO::cmbUF));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_ddd_cobranca', 'Telefone (DDD)'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_telefone_cobranca', 'Telefone'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_email_cobranca', 'E-mail'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_contato_cobranca', 'Contato'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('tbpc_id', 'Tabela de preços', cCOMBO::cmbTABELA_PRECO));

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar"));
	}
}
