<?php
class cTMPL_REPC_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Repartições consulares";
		$this->mOrdenacaoDefault = 'NO_REPARTICAO_CONSULAR';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "300px", "auto", "auto");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_REPARTICAO_CONSULAR'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_REPARTICAO_CONSULAR', 'Nome'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO', 'Endereço'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE', 'Cidade'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PAIS', 'País'));
		// $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS', 'País', cCOMBO::cmbPAIS));
		// $this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE', 'Telefone'));
		$this->EstilizeUltimoCampo('editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_REPARTICAO_CONSULAR', 'Nome'));
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar um novo estado civil", "cCTRL_REPC_EDIT", "Edite" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_REPC_EDIT", "Edite" ));
	}

	public function CustomizarLinha()
	{
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_REPC", "NO_REPARTICAO_CONSULAR", "CO_REPARTICAO_CONSULAR");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_REPC", "NO_ENDERECO", "CO_REPARTICAO_CONSULAR");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_REPC", "NO_CIDADE", "CO_REPARTICAO_CONSULAR");
		// $this->HabiliteEditInPlacePadrao("cCTRL_UPDT_REPC", "CO_PAIS", "CO_REPARTICAO_CONSULAR");
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_REPC", "NU_TELEFONE", "CO_REPARTICAO_CONSULAR");
	}

}
