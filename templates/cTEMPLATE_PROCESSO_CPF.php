<?php
class cTEMPLATE_PROCESSO_CPF extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NOME_COMPLETO', 'Candidato'));
		$this->AdicioneCampo(new cCAMPO_DATA('NU_CPF', 'CPF'));
		$this->mCampo['NU_CPF']->mClasse = 'cpf';
		$this->AdicioneCampoDuplo(new cCAMPO_TEXTO('de_observacao', 'Observação'));
	}
}
