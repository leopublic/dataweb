<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_OS_LISTAR_COBRANCA extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Controle de cobran&ccedil;as - Ordens de Servi&ccedil;o";
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('ID_SOLICITA_VISTO'));
		$this->mCampo['ID_SOLICITA_VISTO']->mValor = &$this->mCampo['id_solicita_visto']->mValor;
		$this->AdicioneColuna(new cCAMPO_LINK_EXTERNO('nu_solicitacao', 'OS', '/operador/os_DadosOS.php'));
		$this->AdicioneColuna(new cCAMPO_DATA('dt_solicitacao', 'Dt. solic.'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL', 'Empresa (da OS)'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarca&ccedil;&atilde;o/projeto (da OS)'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('stco_tx_nome', 'Situa&ccedil;&atilde;o'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Candidato(s)'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Serviço'));

		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA("NU_EMPRESA", "Empresa", cCOMBO::cmbEMPRESA));
		$this->mFiltro['NU_EMPRESA']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA("NU_EMBARCACAO_PROJETO", "Emb./projeto", cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->mFiltro['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(new cCAMPO_DATA("dt_solicitacao_ini", "Aberta desde"));
		$this->AdicioneFiltro(new cCAMPO_DATA("dt_solicitacao_fim", "Aberta at&eacute;"));
		$this->AdicioneFiltro(new cCAMPO_DATA("soli_dt_liberacao_cobranca", "Lib. p/ cobran&ccedil;a em"));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA("stco_id", "Situa&ccedil;&atilde;o", cCOMBO::cmbSITUACAO_COBRANCA));
		$this->mFiltro['stco_id']->mCombo->mPermiteDefault = true;
		$this->mFiltro['stco_id']->mQualificadorFiltro = 's';
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "EDITAR_FINANCEIRO", "Editar informa&ccedil;&otilde;es financeiras", "cCTRL_OS_ABA_COBR_EDIT", "Edite"));

	}
}
