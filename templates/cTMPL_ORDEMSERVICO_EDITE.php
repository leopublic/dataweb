<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_ORDEMSERVICO_EDITE extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "";
		$this->mMsgInicial = "";
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('ID_SOLICITA_VISTO'));

		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', 'Empresa do processo', cCOMBO::cmbEMPRESA));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Emb./proj. do processo', cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->VinculeCombos('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('nu_empresa_requisitante', 'Empresa solicitante', cCOMBO::cmbEMPRESA));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj. solicitante', cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->VinculeCombos('NU_EMBARCACAO_PROJETO_COBRANCA', 'nu_empresa_requisitante');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_solicitador', 'Solicitante'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_solicitacao', 'Data da solicita&ccedil;&atilde;o'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'DT_PRAZO_ESTADA_SOLICITADO', 'Prazo solicitado'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_SERVICO', 'Tipo de servi&ccedil;o', cCOMBO::cmbSERVICO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO,'de_observacao', 'Observa&ccedil;&otilde;es'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('nome_responsavel', 'Respons&aacute;vel', cCOMBO::cmbRESPONSAVEL));
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar as alterações", "", "", "Submit"));
	}
}
