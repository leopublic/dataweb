<?php

/**
 *
 * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada.
 */
class cTMPL_OS_TAXA_ACOMP_LIST extends cTEMPLATE_LISTAGEM {

    public $os_ant;
    public $linhaPendente;
    public $total;

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "Acompanhamento de taxas";
        $this->mCols = array("auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto");
        cHTTP::$comCabecalhoFixo = true;
        // Adiciona campos da tela
        $this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_perfiltaxa'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_taxa'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_taxapaga'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'no_razao_social'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'no_embarcacao_projeto'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'dt_solicitacao_fmt'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nome_cad'));
        $this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato'));

        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_status_sol', 'Status OS'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'stco_tx_nome', 'Status cobr.'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'abertura', 'Aberta por/em'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'emp_emb', 'Empresa/emb. PROCESSO'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_resumido', 'Serviço'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato(s)'));
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'descricao_taxa', 'Taxa'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'data_taxa_fmt', 'Data'));
        $this->EstilizeUltimoCampo('cen');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'valor', 'Valor'));
        $this->EstilizeUltimoCampo('dir');
        $this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'observacao', 'Observações'));

        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
        $this->mFiltro['nu_empresa']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('ID_STATUS_SOL', 'Status da OS', cCOMBO::cmbSTATUS_SOLICITACAO));
        $this->mFiltro['ID_STATUS_SOL']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('stco_id', 'Status de cobr.', cCOMBO::cmbSITUACAO_COBRANCA));
        $this->mFiltro['stco_id']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_servico', 'Serviço', cCOMBO::cmbSERVICO));
        $this->mFiltro['nu_servico']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
        $this->mFiltro['nu_solicitacao']->mQualificadorFiltro = 'sv';
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'data_taxa_ini', 'Lançadas desde'));
        $this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'data_taxa_fim', 'até'));
    }

    public function CustomizarCabecalho() {
        parent::CustomizarCabecalho();
        $this->total = 0;
    }

    public function CustomizarLinha() {
        $this->total += $this->getValorCampo('valor');
        $this->FormateCampoValor('valor', 2);
        $this->setValorCampo('emp_emb', '<strong>' . $this->getValorCampo('no_razao_social') . '</strong><br>' . $this->getValorCampo('no_embarcacao_projeto'));
        $this->setValorCampo('abertura', $this->getValorCampo('nome_cad') . '<br>' . $this->getValorCampo('dt_solicitacao_fmt'));
        $this->setValorCampo('nu_solicitacao', cCTRL_ORDEMSERVICO::LinkDadosOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), '_blank'));
        $links = '';

        $link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $this->getValorCampo('nu_candidato') . '" target="_blank">' . $this->getValorCampo('nome_completo') . '</a>';
        $this->setValorCampo('nome_completo', $link);


//        $sql = "select * from arquivos where id_taxa =".$this->getValorCampo('id_taxa')." and nu_candidato=".$this->getValorCampo('nu_candidato');
//        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__.'.'.__FUNCTION__);
//        $grus = '';
//        $br = '';
//        while($rw = $res->fetch(PDO::FETCH_ASSOC)) {
//            $arq = new cARQUIVO_OK;
//            cBANCO::CarreguePropriedades($rw, $arq);
//            $grus .= $br.'<a href="'.$arq->url().'" target="_blank">GRU</a><img src="/imagens/icons/delete.png" title="clique para remover esse arquivo" onclick="ajaxRemoverArquivo(\'' . $rw['NU_SEQUENCIAL'] . '\', \'' . $rw['NU_CANDIDATO'] . '\');" style="cursor:pointer"/>';
//            $br = '<br/>';
//        }
//        $this->setValorCampo('anexos', $grus);
    }

}
