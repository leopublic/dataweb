<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_OS_PROTOCOLO_BSB_NAO_ENVIADOS extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		// Adiciona campos da tela
		
		$this->AdicioneFiltro(new cCAMPO_DATA('dt_envio_bsb', 'Protocolo do dia'));
		$this->AdicioneFiltro(new cCAMPO_DATA('dt_cadastro_ini', 'Exibir OS desde'));
		$this->AdicioneFiltro(new cCAMPO_DATA('dt_cadastro_ate', 'até'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('codigo'));
		
		$this->AdicioneColuna(new cCAMPO_SELETOR());
		$this->mCampo['SELETOR']->mReadonly = false;
		$this->mCampo['SELETOR']->mEhArray = true;
		$this->mCampo['id_solicita_visto']->mEhArray = true;
		$this->AdicioneColuna(new cCAMPO_HIDDEN('dt_cadastro'));
		$this->AdicioneColuna(new cCAMPO_HIDDEN('dt_cadastro_fmt'));
		$this->AdicioneColuna(new cCAMPO_HIDDEN('dt_envio_bsb_fmt_full'));
		$this->AdicioneColuna(new cCAMPO_LINK_EXTERNO('nu_solicitacao', 'OS', '/operador/os_DadosOS.php'));
		$this->mCampo['nu_solicitacao']->mClasse = "cen";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL', 'Empresa', 'Company'));
		$this->mCampo['NO_RAZAO_SOCIAL']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Candidato', 'Candidate'));
		$this->mCampo['NOME_COMPLETO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PASSAPORTE', 'N&ordm; Passaporte', 'Passport #'));
		$this->mCampo['NU_PASSAPORTE']->mClasse = "cen";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Tipo serviço', 'Visa type'));
		$this->mCampo['NO_SERVICO_RESUMIDO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarcação', 'Ship'));
		$this->mCampo['NO_EMBARCACAO_PROJETO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_REDUZIDO_TIPO_AUTORIZACAO', 'Resolução normativa', 'Normative'));
		$this->mCampo['NO_REDUZIDO_TIPO_AUTORIZACAO']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_REPARTICAO_CONSULAR', 'Repartição consular', 'Consulate'));
		$this->mCampo['NO_REPARTICAO_CONSULAR']->mClasse = "esq";
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_PRAZO_ESTADA_SOLICITADO', 'Prazo pretendido', 'Required period'));
		$this->mCampo['DT_PRAZO_ESTADA_SOLICITADO']->mClasse = "esq";
	}

	public function RenderizeCabecalhoListagem() {
		parent::RenderizeCabecalhoListagem();
		$x = '<div style="background-color:#DADADA;padding:5px;background-image:url(/imagens/icons/seta_esquerda_baixo.png);background-repeat: no-repeat;background-position: center left 5px;padding-left:25px;">
			Salvar marcados no protocolo <button  onclick="ConfirmaAcao(this,\'INCLUIR_NO_PROTOCOLO\',\'\',\'\',\'\');">Salvar</button>
		</div>';

		$this->GerarOutput($x);
		$this->linhaPendente = false;
	}
	
	public function LoopProcessamento($pcursor) {
		while ($rs = $pcursor->fetch(PDO::FETCH_BOTH)){
			$this->ProcesseRegistro($rs);
		}
		if ($this->linhaPendente){
			$this->RestaureLinhaAnterior();
			$this->RenderizeLinha();
		}
	}
	
	public function ProcesseRegistro($prs) {
		$this->mClasse = '';
		$this->CarregueDoRecordset($prs);
//		print '<br>'.$this->mCampo['dt_cadastro']->mValor.'-'.$this->mCampoLinhaAnterior['dt_cadastro']->mValor;
		if ($this->getValorCampoLinhaAnterior('id_solicita_visto') == ''){
			$this->GerarOutput('<tr class="sep"><th colspan="10" style="text-align:left;">Cadastradas em '.$this->getValorCampo('dt_cadastro_fmt').'</th></tr>');
			$this->SalveLinhaAnterior();								
		}
		else{
			if($this->getValorCampo('id_solicita_visto') == $this->getValorCampoLinhaAnterior('id_solicita_visto')){
				$this->appendValorCampoLinhaAnterior('NOME_COMPLETO', '<br/>'.$this->getValorCampo('NOME_COMPLETO'));
				$this->linhaPendente = true;
			}
			else{
				$this->linhaPendente = false;
				$this->RestaureLinhaAnterior();
				$this->RenderizeLinha();
				$this->CarregueDoRecordset($prs);				
				if($this->getValorCampo('dt_cadastro') != $this->getValorCampoLinhaAnterior('dt_cadastro') ){
					$this->GerarOutput('<tr class="sep"><th colspan="10" style="text-align:left;">Cadastradas em '.$this->getValorCampo('dt_cadastro_fmt').'</th></tr>');
				}
				$this->SalveLinhaAnterior();					
			}
			
		}
	}
	
	public function CustomizarLinha() {
		$this->setValorCampo('SELETOR', false) ;
		if ($this->getValorCampo('dt_envio_bsb_fmt_full') != ''){
			$this->setValorCampo('SELETOR', true);			
		}
		$this->mCampo['SELETOR']->set_valorChave($this->getValorCampo('id_solicita_visto'));
		$linkOs = '<a href="/operador/os_DadosOS.php?&ID_SOLICITA_VISTO='.$this->getValorCampo('id_solicita_visto').'&id_solicita_visto='.$this->getValorCampo('id_solicita_visto'). '" target="_blank">'.  $this->getValorCampo('nu_solicitacao').'</a>';
		$this->setValorCampo('nu_solicitacao', $linkOs);
	}
	
	
}
