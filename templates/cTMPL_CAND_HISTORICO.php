<?php
/**
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_CAND_HISTORICO extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "";
		$this->mOrdenacaoDefault = 'hica_dt_evento desc';
		$this->mCols = array('120px', '250px', 'auto');
		// Adiciona campos da tela
		$this->AdicioneColuna(new cCAMPO_TEXTO('hica_dt_evento_fmt', 'Data'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('nome', 'Usuário'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_TEXTO('hica_tx_observacoes', 'Evento'));

		$this->AdicioneFiltroInvisivel('NU_CANDIDATO');
	}
}
