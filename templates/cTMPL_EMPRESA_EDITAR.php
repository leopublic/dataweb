<?php
class cTMPL_EMPRESA_EDITAR extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "INCLUIR/ALTERAR EMPRESA";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_EMPRESA'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'SOCIAL'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_ATIVIDADE_ECONOMICA', 'ECONOMICA'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CNPJ', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_COMPLEMENTO_ENDERECO', 'ENDERECO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_BAIRRO', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_MUNICIPIO', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_UF', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CEP', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_DDD', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMAIL_EMPRESA', 'EMPRESA'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CONTATO_PRINCIPAL', 'PRINCIPAL'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMAIL_CONTATO_PRINCIPAL', 'CONTATO PRINCIPAL'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'TE_OBJETO_SOCIAL', 'SOCIAL'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_CAPITAL_ATUAL', 'ATUAL'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_CONSTITUICAO_EMPRESA', 'EMPRESA'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_ALTERACAO_CONTRATUAL', 'CONTRATUAL'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMPRESA_ESTRANGEIRA', 'ESTRANGEIRA'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_INVESTIMENTO_ESTRANGEIRO', 'ESTRANGEIRO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_INVESTIMENTO_ESTRANGEIRO', 'ESTRANGEIRO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'QT_EMPREGADOS', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'QT_EMPREGADOS_ESTRANGEIROS', 'ESTRANGEIROS'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_CADASTRAMENTO', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_USUARIO_CADASTRAMENTO', 'CADASTRAMENTO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_CAPITAL_INICIAL', 'INICIAL'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ADMINISTRADOR', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_CARGO_ADMIN', 'ADMIN'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'EH_PETROBRAS', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NM_LOGO_EMPRESA', 'EMPRESA'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_CADASTRO_BC', 'BC'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NM_SENHA', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'FL_ATIVA', ''));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tbpc_id', ''));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("SALVAR", "Salvar", "Clique para salvar"));
	}
}
