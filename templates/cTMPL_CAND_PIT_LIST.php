<?php
class cTMPL_CAND_PIT_LIST extends cTEMPLATE_LISTAGEM{
	protected $QT_CANDIDATOS_TOTAL;
	protected $QT_CANDIDATOS_ATIVOS_TOTAL;
	protected $QT_CANDIDATOS_REVISADOS_TOTAL;
	public $chavesSelecionadas;
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "PIT - Controle de participantes";
		$this->mOrdenacaoDefault = 'nome_completo asc';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("40px", "70px", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto");
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato'));
		$this->mCampo['nu_candidato']->mEhArray = true;
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_empresa'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_embarcacao_projeto'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'cand_fl_pit'));

		$this->AdicioneCampo(new cCAMPO_SELETOR());
		$this->mCampo['SELETOR']->mEhArray = true;

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'acao', 'Ação'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('button');
		
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Estrangeiro'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_razao_social', 'Empresa'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_embarcacao_projeto', 'Embarcação/projeto'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('situacao', "Somente inscritos?", cCOMBO::cmbSIM_NAO));
		$this->mCampo['situacao']->mValorDefault = '(todos)';

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('mes', "Mês", cCOMBO::cmbMES));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ano', "Ano"));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', "Estrangeiro"));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', "Empresa", cCOMBO::cmbEMPRESA_ATIVA));
		$this->mFiltro['NU_EMPRESA']->mQualificadorFiltro = 'c';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', "Embarcação/projeto", cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
		$this->mFiltro['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = 'c';
		$this->VinculeFiltros('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');
		
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Gerar OS cálculo mensal", "GERAR_OS_MENSAL",  "Clique para gerar as OS de cálculo mensal"));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Gerar OS declaração anual", "GERAR_OS_ANUAL",  "Clique para gerar as OS de declaração anual"));
//		$onclick = "window.open('carregueComMenu.php?controller=cCTRL_EMBP_LIST_EXCEL&metodo=Liste', '_blank');";
//		$this->AdicioneAcao(new cACAO_JAVASCRIPT ("Excel", "EXP_EXCEL",  "Clique para exportar em Excel", $onclick));		
	}

	public function CustomizarCabecalho() {
		$this->QT_CANDIDATOS_TOTAL =0;
		$js = "
			$('#SELETOR_ALL').on('click',
				function(event) {
					if($('#SELETOR_ALL').prop('checked')){
						$('.seletor').prop('checked', true);				
					}
					else{
						$('.seletor').prop('checked', false);			
					}
				}
			);

		";
		
		cHTTP::AdicionarScript($js);
	}
	
	public function CustomizarLinha()
	{
		$this->QT_CANDIDATOS_TOTAL++;

		$nomeComLink = $this->getValorCampo('nome_completo').' ('.$this->getValorCampo('nu_candidato').')';
		$link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$this->getValorCampo('nu_candidato').'" target="_blank">'.$nomeComLink.'</a>';
		$this->setValorCampo('nome_completo', $link);
		
		if($this->getValorCampo('cand_fl_pit') == 0){
			$parametros = '{"controller": "cCTRL_UPDT_CAND", "metodo": "IncluirPit"';
			$parametros.= ', "nu_candidato": "'.$this->getValorCampo('nu_candidato').'"';
			$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
			$parametros.= '}';			
			$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Incluir</a>';			
		}
		else{
			$parametros = '{"controller": "cCTRL_UPDT_CAND", "metodo": "RemoverPit"';
			$parametros.= ', "nu_candidato": "'.$this->getValorCampo('nu_candidato').'"';
			$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
			$parametros.= '}';			
			$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Retirar</a>';
		}
		$this->mCampo['SELETOR']->set_valorChave($this->getValorCampo('nu_candidato'));
		
		if($this->chavesSelecionadas != ''){
			if(strstr($this->chavesSelecionadas, ','.$this->getValorCampo('nu_candidato').',')){
				$this->mCampo['SELETOR']->mValor = 1;
			}
			else{
				$this->mCampo['SELETOR']->mValor = 0;				
			}
		}
		
		$this->setValorCampo('acao', $links);

	}

}
