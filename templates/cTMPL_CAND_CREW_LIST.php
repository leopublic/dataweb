<?php
class cTMPL_CAND_CREW_LIST extends cTEMPLATE_LISTAGEM
{
	public function __construct() 
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Crew List";
		$this->mCols = array("70px", "30px", "200px", "50px", "50px", "60px", "170px", "100px", "170px", "80px", "auto", "auto" );
		$this->mOrdenacaoDefault = 'NOME_COMPLETO';
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneColuna(new cCAMPO_HIDDEN('codigo_processo_mte_atual'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_ORDEM_CREWLIST', '#', "#" ));
		$this->mCampo['NU_ORDEM_CREWLIST']->mClasse = 'cen';
//		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarcação/Projeto', "Project/Ship" ));
//		$this->mCampo['NO_EMBARCACAO_PROJETO']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Nome', "Crew name" ));
		$this->mCampo['NOME_COMPLETO']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_SIMNAO('FL_INATIVO', 'Inativo', 'Inactive'));
		$this->mCampo['FL_INATIVO']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_SIMNAO_LITERAL('FL_EMBARCADO', 'Embarc.', 'On board'));
		$this->mCampo['FL_EMBARCADO']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PASSAPORTE', 'Passaporte', 'Pspt NR'));
		$this->mCampo['NU_PASSAPORTE']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('nu_processo_mte', 'Processo MTE', 'MTE Process'));
		$this->mCampo['nu_processo_mte']->mClasse = 'cen';
		$this->mCampo['nu_processo_mte']->mlinkMte = true;
		$this->AdicioneColuna(new cCAMPO_TEXTO('validade_visto', 'Validade visto', 'Expiry date'));
		$this->mCampo['validade_visto']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('nu_protocolo_reg', 'Registro', 'Registration'));
		$this->mCampo['nu_processo_regcie']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('dt_requerimento_reg_fmt', 'Data do registro', 'Date of register'));
		$this->mCampo['dt_requerimento_reg_fmt']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('TE_OBSERVACAO_CREWLIST', 'Observações (crew list)', 'Observations'));
		$this->mCampo['TE_OBSERVACAO_CREWLIST']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('observacao_visto', 'Observações (visto)', 'Observations'));
		$this->mCampo['observacao_visto']->mClasse = 'esq';

		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMPRESA', "Empresa", cCOMBO::cmbEMPRESA));
		$this->mFiltro['NU_EMPRESA']->mQualificadorFiltro = 'C';
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMBARCACAO_PROJETO', "Embarc./proj.", cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->mFiltro['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = 'C';
		$this->AdicioneFiltro(new cCAMPO_TEXTO('NOME_COMPLETO', "Nome cand."));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('TIPO_CANDIDATO', "Tipo cand.", cCOMBO::cmbTIPO_CANDIDATO));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('FL_EMBARCADO', "Embarcado", cCOMBO::cmbEMBARCADO));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('FL_INATIVO', "Cadastro", cCOMBO::cmbSITUACAO_CADASTRAL_CANDIDATO));
		$this->mFiltro['NOME_COMPLETO']->mTipoComparacaoFiltro = cFILTRO::tpSEL_LIKE_INI;
		
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Renumerar ordem", "RENUMERAR", "Clique para renumerar a ordem dos candidatos"));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Exportar para excel", "GERAR_EXCEL", "Clique para gerar essa consulta em Excel"));
		$this->AdicioneAcaoLinha(new cACAO_METODO_POPUP("/imagens/grey16/Write2.png","ALTERAR", "cCTRL_CANDIDATO", "EditarCrewList",  "Clique para editar os dados" ));
		$this->AdicioneAcaoLinha(new cACAO_METODO_POPUP("/imagens/grey16/Clock.png","EXIBIR_VISTO", "cCTRL_CANDIDATO", "ExibirVisto",  "Clique para ver o histórico detalhado do visto deste candidato" ));
//		$this->AdicioneAcaoLinha(new cACAO_METODO_POPUP("/imagens/grey16/Discuss.png","ADICIONAR_OBS", "cCTRL_CANDIDATO", "ComplementarObservacao",  "Clique para complementar as observações do visto do candidato" ));
	}
	
	public function CustomizarFiltros() {
		if (intval($this->getValorFiltro('TIPO_CANDIDATO')) == 1){
			$this->mFiltro['TIPO_CANDIDATO']->mWhere = " codigo_processo_mte_atual is not null";
		}
		elseif (intval($this->getValorFiltro('TIPO_CANDIDATO')) == 2){
			$this->mFiltro['TIPO_CANDIDATO']->mWhere = " codigo_processo_mte_atual is null";
		}
		
	}
	
	public function CustomizarLinha() {
		$nomecomcodigo = $this->getValorCampo('NOME_COMPLETO').'('.$this->getValorCampo('NU_CANDIDATO').')';
		$link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$this->getValorCampo('NU_CANDIDATO').'" target="_blank">'.$nomecomcodigo.'</a>';
		$this->setValorCampo('NOME_COMPLETO', $link );
		
		$cand = new cCANDIDATO();
		$cand->mNU_CANDIDATO = $this->getValorCampo('NU_CANDIDATO');
		$cand->mcodigo_processo_mte_atual = $this->getValorCampo('codigo_processo_mte_atual');
		$visto = $cand->VistoAtual();
//		var_dump($visto);
		
		$nu_processo_mte = '';
		if(isset($visto['autorizacao'])){
			if(trim($visto['autorizacao']->mnu_processo) != ''){
				$nu_processo_mte = $visto['autorizacao']->mnu_processo;
				$this->mCampo['nu_processo_mte']->mlinkMte = true;
				if($visto['autorizacao']->mdt_deferimento != ''){
					$nu_processo_mte .= '<br/>(deferido em '.$visto['autorizacao']->mdt_deferimento.')';
				}
				if($visto['autorizacao']->mid_solicita_visto > 6678){
					if ($visto['autorizacao']->mNO_STATUS_CONCLUSAO == 'n/a'){
						$visto['autorizacao']->mNO_STATUS_CONCLUSAO = 'em andamento';
					}
					elseif ($visto['autorizacao']->mNO_STATUS_CONCLUSAO == '' ){
						$visto['autorizacao']->mNO_STATUS_CONCLUSAO = 'status desconhecido';
					}
					$nu_processo_mte .= '<br/>'.$visto['autorizacao']->mNO_STATUS_CONCLUSAO;
				}
				else{
					$nu_processo_mte .= '<br/>(processo antigo)';					
				}
			}
			else{
				$nu_processo_mte = '<span style="color:red;">(processo sem número!)</span>';
				$this->mCampo['nu_processo_mte']->mlinkMte = false;
			}
			$this->setValorCampo('validade_visto', $visto['autorizacao']->get_situacao_prazo_estada());
			$this->setValorCampo('observacao_visto', $visto['autorizacao']->get_situacao_texto());
		}
		else{
			$nu_processo_mte = '(sem visto atual)';
			$this->mCampo['nu_processo_mte']->mlinkMte = false;
			$this->setValorCampo('validade_visto', '--');
			$this->setValorCampo('observacao_visto', '--');
		}
		$this->setValorCampo('nu_processo_mte', $nu_processo_mte);
		
		if(isset($visto['processos'])){
			foreach($visto['processos'] as $processo){
				if($processo->mnu_servico == 20){
					$this->setValorCampo('nu_protocolo_reg', $processo->mnu_protocolo);
					$this->setValorCampo('dt_requerimento_reg_fmt', $processo->mdt_requerimento);
					break;
				}
			}
		}
		
	}
}
