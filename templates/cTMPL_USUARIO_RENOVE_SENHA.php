<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_USUARIO_RENOVE_SENHA extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Renovar senha de usuário";
		$this->mMsgInicial = "";
		$this->qtd_colunas = 1;
		$this->mlocalAcoes = self::lca_RODAPE;
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('cd_usuario'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome', 'Nome'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSENHA, 'nm_senha', 'Senha'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSENHA, 'nm_senha_rep', '(repita)'));
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar as alterações", "", "", "Submit"));
	}
}
