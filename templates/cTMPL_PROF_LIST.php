<?php
// á
class cTMPL_PROF_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Profissões";
		$this->mOrdenacaoDefault = 'NO_PROFISSAO';
		cHTTP::$comCabecalhoFixo = true;
		$this->mCols = array("50px", "auto", "auto", "100px");
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_PROFISSAO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PROFISSAO', 'Descrição'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PROFISSAO_EM_INGLES', 'Descrição em inglês'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CBO', 'CBO'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PROFISSAO', 'Descrição'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CBO', 'CBO'));
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Cadastrar nova", "INCLUIR",  "Clique para cadastrar um novo pais", "cCTRL_PROF_EDIT", "Edite" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para editar", "cCTRL_PROF_EDIT", "Edite" ));
	}

	public function CustomizarLinha()
	{
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PROF", "NO_PROFISSAO", "CO_PROFISSAO");		
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PROF", "NO_PROFISSAO_EM_INGLES", "CO_PROFISSAO");		
		$this->HabiliteEditInPlacePadrao("cCTRL_UPDT_PROF", "NU_CBO", "CO_PROFISSAO");		
	}

}
