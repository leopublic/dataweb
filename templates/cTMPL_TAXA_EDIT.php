<?php
// à
class cTMPL_TAXA_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mlocalAcoes = self::lca_RODAPE;
		$this->qtd_colunas = 1;
		// Adiciona campos da tela
		$this->set_titulo('Taxa');
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_taxa'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_perfiltaxa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'descricao', 'Descrição'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'valor_fixo', 'Valor fixo'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar","SALVAR", "Clique para editar" ));
	}

    public function CustomizeCabecalho() {
        if (intval($this->getValorCampo('id_taxa')) ==0 ){
            $this->setValorCampo('id_perfiltaxa', cHTTP::ParametroValido('id_perfiltaxa'));
        }
    }
}
