<?php
class cTMPL_CAND_CLIE_EDIT_FILE extends cTEMPLATE_EDICAO_TWIG{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->set_titulo_painel('Files');
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('NOME_COMPLETO'));

		$this->AdicioneCampo(new cCAMPO('arquivo', "File", cCAMPO::cpFILE));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo("ID_TIPO_ARQUIVO", "Referente Ã ", cCOMBO::cmbTIPO_ARQUIVO, "Refers to"));

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Hit here to send the file.", "", "", "Save" ));
		$this->mAcoes['SALVAR']->mno_classe = "save";
	}
}
