<?php
class cTMPL_PRECO_EDITAR extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Informar pre&ccedil;o";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
		$this->qtd_colunas = 1;
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'prec_id'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'tbpc_id'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_SERVICO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Servi&ccedil;o (OS)'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'prec_tx_item', 'Item'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'prec_tx_descricao_tabela_precos', 'Descrição do servi&ccedil;o para cobran&ccedil;a'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'prec_tx_descricao_tabela_precos_em_ingles', 'Descrição do servi&ccedil;o para cobran&ccedil;a em inglês'));
		$this->mCampo['NO_SERVICO_RESUMIDO']->mReadonly = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'prec_vl_conceito', 'Valor conceito R$'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'prec_vl_pacote', 'Valor pacote R$'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar"));
	}
}
