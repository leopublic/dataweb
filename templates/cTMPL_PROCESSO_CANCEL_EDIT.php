<?php
class cTMPL_PROCESSO_CANCEL_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO(cCAMPO::cpTEXTO, 'nu_processo', 'NÃºmero do Processo'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO(cCAMPO::cpDATA, 'dt_processo', 'Data de Processo'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO(cCAMPO::cpDATA, 'dt_cancel', 'Data de Cancelamento'));
	}
}
