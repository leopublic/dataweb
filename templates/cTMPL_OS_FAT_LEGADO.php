<?php
/**
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_OS_FAT_LEGADO extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Conferência e ajuste de faturamento";
		$this->set_exibirPaginacao(true);
		cHTTP::$comCabecalhoFixo = true;
		$this->set_ordenacaoDefault('s.nu_solicitacao desc');
		// Adiciona campos da tela
		$this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		$this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_empresa_requerente'));
		$this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_EMBARCACAO_PROJETO_COBRANCA'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'acao', 'Ação'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('button');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_nu_numero_mv', 'Resumo n&ordm;'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL_REQUERENTE', 'Empresa COBRANÇA'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj. REAL'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'soli_tx_solicitante_cobranca', 'Solicitante'));
		
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Candidato(s)'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Serviço'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'tppr_tx_nome', 'Cobrança'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'soli_vl_cobrado_tabela', 'Valor tabela(R$)'));
		$this->EstilizeUltimoCampo('dir');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR_DIN_REAL, 'soli_vl_cobrado', 'Valor (R$)'));
		
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa_requerente', 'Empresa COBRANÇA', cCOMBO::cmbEMPRESA));
		$this->mFiltro['nu_empresa_requerente']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', 'Empresa processo', cCOMBO::cmbEMPRESA));
		$this->mFiltro['NU_EMPRESA']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO_COBRANCA', 'Emb./proj. REAL', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
		$this->VinculeFiltros('NU_EMBARCACAO_PROJETO_COBRANCA', 'NU_EMPRESA');
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_tipo_servico', 'Tipo de serviço', cCOMBO::cmbTIPO_SERVICO));
//		$this->mFiltro['nu_tipo_servico']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('stco_id', 'Situação de cobrança', cCOMBO::cmbSITUACAO_COBRANCA));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato'));
	}

	public function CustomizarCabecalho()
	{
		cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieFiltros($this, $this->mFiltro['NU_EMPRESA'], $this->mFiltro['NU_EMBARCACAO_PROJETO_COBRANCA']));
	}
	
	public function CustomizarLinha() {
		$this->mCampo['nu_solicitacao']->mValor = '<a href="../paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso&id_solicita_visto='.$this->getValorCampo('id_solicita_visto').'" target="_blank">'.$this->getValorCampo('nu_solicitacao').'</a>';
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO = $this->getValorCampo('id_solicita_visto');
		$candidatos = $os->ObtenhaNomesCandidatos();
		$br = '' ;
		$nomes = '';
		foreach ($candidatos as $key => $value) {
				$nomes .= $br.$value;
			$br = '<br/>';
		}
		$this->mCampo['NOME_COMPLETO']->mValor = $nomes;

		$parametros = '{"controller": "cCTRL_UPDT_SOLV"';
		$parametros.= ', "metodo": "Desfaturar"';
		$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Desfaturar</a>';
		$this->setValorCampo('acao', $links);

		$parametros = '{"controller": "cCTRL_UPDT_SOLV"';
		$parametros.= ', "metodo": "resc_nu_numero_mv"';
		$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['resc_nu_numero_mv']->set_parametros($parametros);
	}
}
