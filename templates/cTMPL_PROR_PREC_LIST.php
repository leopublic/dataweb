<?php
/**
 * Lista as OS de prorrogacao para controle do pré-cadastro
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_PROR_PREC_LIST extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Controle de pré-cadastro de prorrogações";

		$this->mCols = array("50px", "50px", "auto", "85px", "auto", "auto", "auto", "auto","auto", "150px", "120px","auto", "230px");

		$this->mOrdenacaoDefault = 'dt_solicitacao desc';
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('nu_candidato'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('codigo'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('dt_pre_cadastro'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('SI_STATUS_SOL'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_STATUS_SOL', 'Status'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_TEXTO('nu_solicitacao', 'OS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_TEXTO('nome_completo', 'Candidato'));
		$this->AdicioneColuna(new cCAMPO_DATA('dt_solicitacao', 'Dt. solic.'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_TEXTO('usua_tx_apelido', 'Aberta por'));
		$this->AdicioneColuna(new cCAMPO_DATA('dt_prazo_pret', 'Prazo pretendido'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_DATA('dt_requerimento', 'Dt protocolo'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneCampo(new cCAMPO_TEXTO('nu_protocolo', '# protocolo'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('processo');
		$this->AdicioneCampo(new cCAMPO_TEXTO('nu_pre_cadastro', '# pré-cadastro'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('editavel');
//		$this->AdicioneCampo(new cCAMPO_DATA('dt_pre_cadastro', 'Data pré-cadastro'));
//		$this->EstilizeUltimoCampo('cen');
//		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneColuna(new cCAMPO_TEXTO('usua_tx_apelido_pre_cadastro', 'Pré-cadastrado por'));
		$this->AdicioneCampo(new cCAMPO_DATA_ONLY('dt_publicacao_dou', 'Data DOU'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(new cCAMPO_TEXTO('nu_pagina_dou', 'Pág. DOU'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneCampo(new cCAMPO_MEMO('observacao', 'Observações (processo)'));
		$this->EstilizeUltimoCampo('editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', "Emp. PROCESSO", cCOMBO::cmbEMPRESA));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Emb./proj. PROCESSO', cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS));
		$this->mFiltro['NU_EMPRESA']->mQualificadorFiltro = 's';
		$this->mFiltro['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = 's';
		$this->VinculeFiltros('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');
        $this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('ID_STATUS_SOL', 'Status da OS', cCOMBO::cmbSTATUS_SOLICITACAO));
        $this->mFiltro['ID_STATUS_SOL']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('id_status_conclusao', 'Status processo', cCOMBO::cmbSTATUS_CONCLUSAO));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_pre_cadastro_preenchido', 'Pré-cadastro preenchido?', cCOMBO::cmbSIM_NAO));

		$this->AdicioneFiltro(new cCAMPO_TEXTO("NOME_COMPLETO", "Candidato"));
		$this->AdicioneFiltro(new cCAMPO_TEXTO("nu_solicitacao", "OS"));
		$this->AdicioneFiltro(new cCAMPO_DATA("dt_solicitacao_ini", "Aberta desde"));
		$this->AdicioneFiltro(new cCAMPO_DATA("dt_solicitacao_fim", "Aberta até"));
		$this->AdicioneFiltro(new cCAMPO_DATA("dt_requerimento_ini", "Protocolo desde"));
		$this->AdicioneFiltro(new cCAMPO_DATA("dt_requerimento_fim", "Protocolo até"));

		$onclick = "window.open('carregueComoExcel.php?controller=cCTRL_PROR_PREC_LIST_EXCEL&metodo=Liste', '_blank');";
		$this->AdicioneAcao(new cACAO_JAVASCRIPT ("Excel", "EXP_EXCEL",  "Clique para exportar em Excel", $onclick));
	}

	public function CustomizarLinha() {
		$parametros = '{"controller": "cCTRL_UPDT_PROCPROR", "metodo": "nu_pre_cadastro"';
		$parametros.= ', "codigo": "'.$this->getValorCampo('codigo').'"';
		$parametros.= ', "cd_usuario_pre_cadastro": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['nu_pre_cadastro']->set_parametros($parametros);

		$parametros = '{"controller": "cCTRL_UPDT_PROCPROR", "metodo": "nu_protocolo"';
		$parametros.= ', "codigo": "'.$this->getValorCampo('codigo').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['nu_protocolo']->set_parametros($parametros);

		$parametros = '{"controller": "cCTRL_UPDT_PROCPROR", "metodo": "dt_publicacao_dou"';
		$parametros.= ', "codigo": "'.$this->getValorCampo('codigo').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['dt_publicacao_dou']->set_parametros($parametros);

		$parametros = '{"controller": "cCTRL_UPDT_PROCPROR", "metodo": "nu_pagina_dou"';
		$parametros.= ', "codigo": "'.$this->getValorCampo('codigo').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['nu_pagina_dou']->set_parametros($parametros);

		$parametros = '{"controller": "cCTRL_UPDT_PROCPROR", "metodo": "observacao"';
		$parametros.= ', "codigo": "'.$this->getValorCampo('codigo').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['observacao']->set_parametros($parametros);

		$this->FormateCampoData('dt_solicitacao');
		$this->FormateCampoData('dt_requerimento');
		$this->FormateCampoData('dt_publicacao_dou');
		$this->FormateCampoData('dt_prazo_pret');

		$this->FormateCampoData('dt_pre_cadastro');
		if($this->getValorCampo('nu_pre_cadastro') != ''){
			$this->setValorCampo('usua_tx_apelido_pre_cadastro', $this->getValorCampo('usua_tx_apelido_pre_cadastro').'<br/>em '.$this->getValorCampo('dt_pre_cadastro'));
		}

		$valor = '<span class='.$this->getValorCampo('SI_STATUS_SOL').' title="'.$this->getValorCampo('NO_STATUS_SOL').'">'.$this->getValorCampo('SI_STATUS_SOL').'</span>';
		$this->setValorCampo('NO_STATUS_SOL', $valor);

		$link = cCTRL_ORDEMSERVICO::LinkProcessoOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), "_blank");
		$this->setValorCampo('nu_solicitacao', $link);

		$link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$this->getValorCampo('nu_candidato').'" target="_blank">'.$this->getValorCampo('nome_completo').'</a>';
		$this->setValorCampo('nome_completo', $link);
	}
}
