<?php
class cTMPL_PROCESSO_COLETA_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->set_titulo('');
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('ID_STATUS_SOL'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('codigo'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('cd_candidato'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('codigo_processo_mte'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('nu_servico'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('no_validade', 'Validade do visto'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('nu_visto', 'Número do visto'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_emissao_visto', 'Data de emissão'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('no_local_emissao', 'Local de emissão'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('co_pais_nacionalidade_visto', 'País de emissão do visto', cCOMBO::cmbPAIS));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_NACIONALIDADE', 'País de emissão do visto'));
		$this->mCampo['NO_NACIONALIDADE']->mVisivel = false;
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('co_classificacao', 'Classificação do visto', cCOMBO::cmbCLASSIFICA_VISTO));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CLASSIFICACAO_VISTO', 'Classificação do visto'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('no_local_entrada', 'Local de entrada (cidade)'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('co_uf', 'UF de entrada', cCOMBO::cmbUF));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_entrada', 'Data de entrada'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('nu_transporte_entrada', 'Meio de transporte', cCOMBO::cmbTRANSPORTE_ENTRADA));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_TRANSPORTE_ENTRADA', 'Meio de transporte'));
		$this->mCampo['NO_TRANSPORTE_ENTRADA']->mVisivel = false;
	}
}
