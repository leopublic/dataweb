<?php
class cTMPL_EMPRESA_LIST_END_COB extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Endereços de cobrança";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->set_ordenacaoDefault('NO_RAZAO_SOCIAL');
		$this->mCols = array("60px", "auto","auto", "120px", "auto",  "30px", "100px", "auto", "auto");

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_EMPRESA'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Razão social'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_razao_social_cobranca', 'Razão social cobrança'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_cnpj_cobranca', 'CNPJ'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_endereco_cobranca', 'Endereço'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_complemento_endereco_cobranca', 'Compl.'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_bairro_cobranca', 'Bairro'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_municipio_cobranca', 'Município'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_cep_cobranca', 'CEP'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'co_uf_cobranca', 'UF'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_ddd_cobranca', 'DDD'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_telefone_cobranca', 'Tel'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_email_cobranca', 'Email'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'empr_tx_contato_cobranca', 'Contato'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Razão social'));
		
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para alterar esse item", "cCTRL_EMPRESA_EDIT_ENDERECO_COBRANCA", "Edite"));
	}

	public function CustomizarLinha(){
		$end = $this->getValorCampo('empr_tx_endereco_cobranca'). ' ' . $this->getValorCampo('empr_tx_complemento_endereco_cobranca');
		$end .= '<br/>'.$this->getValorCampo('empr_tx_bairro_cobranca'). ' - ' . $this->getValorCampo('empr_tx_municipio_cobranca'). ' - ' . $this->getValorCampo('co_uf_cobranca');
		$end .= '<br/>CEP '.$this->getValorCampo('empr_tx_cep_cobranca');
		$this->setValorCampo('empr_tx_endereco_cobranca', $end);
	}
}
