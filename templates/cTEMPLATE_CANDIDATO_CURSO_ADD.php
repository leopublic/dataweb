<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_CANDIDATO_CURSO_ADD extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		if (cHTTP::getLang() =='pt_br'){
			$this->mTitulo = "Adicionar curso";
		}
		else{
			$this->mTitulo = "Add course";
		}
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('curs_id'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('tpen_id', 'Tipo de curso', cCOMBO::cmbTipoEnsino, 'Course type'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('curs_no_nome', 'Nome do curso', 'Course name'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('curs_no_instituicao', 'Instituição', 'Instituition'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('curs_no_grau', 'Grau', 'Degree'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('curs_no_periodo', 'Período', 'Period'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('curs_no_cidade', 'Cidade/País', 'City/Country'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Adicionar", "SALVAR", "Hit here to add this course to your curriculum", "", "", "Add" ));
	}
}
