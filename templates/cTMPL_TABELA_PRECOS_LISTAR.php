<?php
class cTMPL_TABELA_PRECOS_LISTAR extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Tabelas de pre&ccedil;o";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->mCols = array("100px", "auto", "200px", auto);
		cHTTP::$comCabecalhoFixo = true;
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'tbpc_id'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tbpc_tx_nome', 'Nome'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tbpc_nu_ano', 'Ano'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tbpc_tx_observacao', 'Observação'));
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Adicionar", "ADICIONAR", "Clique para adicionar um novo", "cCTRL_TABELA_PRECOS", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Write2.png", "ALTERAR", "Clique para alterar esse item", "cCTRL_TABELA_PRECOS", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Currency Dollar.png", "LISTAR_VALORES", "Clique para listar os pre&ccedil;os", "cCTRL_PRECO", "Liste"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Applications.png", "CLONAR", "Clique para clonar essa tabela", "cCTRL_TABELA_PRECOS", "Clonar"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Trash.png", "EXCLUIR", "Clique para excluir esse item", "cCTRL_TABELA_PRECOS", "Exclua", "Deseja mesmo excluir essa tabela de preços? Essa ação não poderá ser desfeita."));
	}
}
