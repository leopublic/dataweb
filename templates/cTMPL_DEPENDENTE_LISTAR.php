<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_DEPENDENTE_LISTAR extends cTEMPLATE_LISTAGEM{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "";
		$this->mMsgInicial = "";
		$this->mOrdenacaoDefault = 'd.co_grau_parentesco';
		$this->enveloparEmForm = true;
		$this->mCols = array("60px", "auto", "auto", "140px", "auto", "auto", "auto");
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;

		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_CANDIDATO'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato_parente'));
//		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_dependente'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Nome'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_grau_parentesco', 'Parentesco'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_NASCIMENTO', 'Data de nascimento'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NACIONALIDADE', 'Nacionalidade'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_PASSAPORTE', 'No. passaporte'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PAIS_EMISSOR_PASSAPORTE', 'País emissor'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_VALIDADE_PASSAPORTE_ORIG', 'Validade passaporte'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpVALOR, 'NU_CANDIDATO', ''));
		$this->mFiltro['NU_CANDIDATO']->mVisivel = false;

		// Adiciona as ações que estarão disponíveis na tela
		
		$this->AdicioneAcaoLinha(new cACAO_LINK("/imagens/grey16/Write2.png", "ATUALIZAR",  "Clique para editar esse dependente", "/operador/detalheCandidatoAuto.php" ));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Trash.png", "EXCLUIR", "Excluir esse dependente", "cCTRL_DEPENDENTE_LIST", "Exclua",  "O dependente é um candidato independente, e ao excluí-lo você estará excluindo todo o seu histórico e OS relacionadas. Deseja mesmo excluir esse dependente? ", "Atenção") );
		}
	
	public function CustomizarCabecalho() {
		parent::CustomizarCabecalho();
		$this->mAcoesLinha['ADICIONAR']->mVisivel = false;
	}
	
	public function CustomizarLinha() {
		$this->FormateCampoData('DT_NASCIMENTO');
		$this->FormateCampoData('DT_VALIDADE_PASSAPORTE_ORIG');
		$this->mAcoesLinha['ATUALIZAR']->mLink = '/operador/detalheCandidatoAuto.php?&painelAtivo=3';
		$this->mAcoesLinha['ATUALIZAR']->mTarget = '_blank'; 
	
	}
	
}
