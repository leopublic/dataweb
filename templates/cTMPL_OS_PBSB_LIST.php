<?php
/**
 *
 * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada.
 */
class cTMPL_OS_PBSB_LIST extends cTEMPLATE_LISTAGEM{
	public $acum;
	public $os_ant;
	public $linhaPendente;
	public $gerouSeparador;
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Protocolo de Brasília - MONTAR PROTOCOLO";
		$this->mCols = array("auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto","auto", "200px");
		$this->mOrdenacaoDefault = 'dt_envio_bsb desc, nu_solicitacao desc, nome_completo asc';
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('tabela'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('codigo'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('ID_STATUS_SOL'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('dt_envio_bsb'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('nu_candidato'));

		$this->AdicioneColuna(new cCAMPO_TEXTO('acao', 'Ação'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('button');

		$this->AdicioneColuna(new cCAMPO_TEXTO('nu_solicitacao', 'OS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_TEXTO('dt_solicitacao', 'Dt. solic.'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_TEXTO('no_razao_social', 'Empresa PROCESSO'));

		$this->AdicioneColuna(new cCAMPO_TEXTO('nome_completo', 'Candidato(s)'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('nu_passaporte', '# passaporte'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('no_reparticao_consular', 'Repartição consular'));

		$this->AdicioneColuna(new cCAMPO_TEXTO('no_servico_resumido', 'Serviço'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('no_embarcacao_projeto', 'Embarcação/projeto PROCESSO'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('dt_prazo_estada_solicitado', 'Prazo pretendido'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneCampo(new cCAMPO_TEXTO('soli_qt_autenticacoes', 'Qtd autent. RJ'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneCampo(new cCAMPO_TEXTO('soli_qt_autenticacoes_bsb', 'Qtd autent. BSB'));
		$this->EstilizeUltimoCampo('editavel');
		$this->EstilizeUltimoCampo('cen');

		$this->AdicioneCampo(new cCAMPO_MEMO('de_observacao_bsb', 'Observações'));
		$this->EstilizeUltimoCampo('editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_envio_bsb', 'Incluir no protocolo de'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('dt_envio_bsb_select', '', cCOMBO::cmbULTIMOS_PROTOCOLOS_BSB));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('orgao_protocolo', 'Protocolo para', cCOMBO::cmbORGAO_PROTOCOLO));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_ini', 'Cadastradas desde'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_fim', 'até'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
		$this->mFiltro['nu_empresa']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->mFiltro['nu_solicitacao']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato'));

		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO('Consultar protocolo', 'LISTAR_ENVIADAS', "Ver as OS enviadas", 'cCTRL_OS_PBSB_EDIT', 'Liste'));
		// $onclick = "window.open('carregueComMenu.php?controller=cCTRL_PBSB_PLAN_LIST&metodo=Liste&nu_empresa=xx&dt_envio_bsb=".$this->getValorFiltro('dt_envio_bsb')."', '_blank');";
		// $this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel Geral", "EXP_EXCEL_GERAL",  "Clique para exportar em Excel", $onclick));

		// $onclick = "window.open('carregueComMenu.php?controller=cCTRL_PBSB_PLAN_LIST&metodo=Liste&nu_empresa=21&dt_envio_bsb=".$this->getValorFiltro('dt_envio_bsb')."', '_blank');";
		// $this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel Petrobrás", "EXP_EXCEL_PETR",  "Clique para exportar em Excel", $onclick));

	}

	public function CustomizarCabecalho()
	{
		$data = $this->getValorFiltro('dt_envio_bsb');
		if ($data != ''){
			$qtd = cORDEMSERVICO::TotalAutenticacoesProtocolo($this->getValorFiltro('dt_envio_bsb'));
			$this->mCampo['soli_qt_autenticacoes']->mLabel = 'Qtd autentic. RJ<br/>tot.:'.$qtd;
			cHTTP::set_nomeArquivo('protocolo_bsb_'.$this->getValorFiltro('dt_envio_bsb').'.xls');

                        $qtd = cORDEMSERVICO::TotalAutenticacoesBsbProtocolo($this->getValorFiltro('dt_envio_bsb'));
			$this->mCampo['soli_qt_autenticacoes_bsb']->mLabel = 'Qtd autentic. BSB<br/>tot.:'.$qtd;
			cHTTP::set_nomeArquivo('protocolo_bsb_'.$this->getValorFiltro('dt_envio_bsb').'.xls');
		}
		// $script = "
		// 	$('#CMP_FILTRO_dt_envio_bsb').mask('99/99/9999');
		// 	$('#CMP_FILTRO_dt_envio_bsb').blur(function(){
		// 		$('td.acao a').attr('value', $('#CMP_FILTRO_dt_envio_bsb').val());
		// 		$('td.acao a').text('Incluir '+$('#CMP_FILTRO_dt_envio_bsb').val());
		// 		});

		// ";



		$script = "
			$('#CMP_FILTRO_dt_envio_bsb').datepicker({
				beforeShowDay: function (data){
						var dataOk = $.datepicker.formatDate('dd/mm/yy', data);
						var qtd;
						$.ajax({
						  type: 'POST',
						  async: false,
						  url: '/paginas/carregueSemMenu.php?controller=cCTRL_PROCESSO&metodo=QtdProtocolosDoDia',
						  data: { dt_envio_bsb: dataOk },
						  success: function(data) {
								qtd = data;
							},
						  dataType: 'text'
						});
						var ret;
						var css = '';
						var title = '';
						if($(this).val() != '0'){
							ret = [ true, 'bold', $(this).val()+' processos nesse dia' ];
							css = 'bold';
							title = $(this).val()+' processos';
						}
						else{
							ret = [ true, '', $(this).val()+'nenhum processo'];
						}
						return ret;
				}
			}
				).mask('99/99/9999');
		";

		$script = "
			$('#CMP_FILTRO_dt_envio_bsb_select').change(function(){
				$('#CMP_FILTRO_dt_envio_bsb').val($('#CMP_FILTRO_dt_envio_bsb_select').val());
			});";
		 cHTTP::AdicionarScript($script);

		if($this->mFiltro['dt_envio_bsb']->mValor != ''){
			$onclick = "window.open('carregueComoExcel.php?controller=cCTRL_PBSB_PLAN_LIST&metodo=Liste&nu_empresa=xx&dt_envio_bsb=".$this->getValorFiltro('dt_envio_bsb')."', '_blank');";
			$this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel Geral", "EXP_EXCEL_GERAL",  "Clique para exportar em Excel", $onclick));

			$onclick = "window.open('carregueComoExcel.php?controller=cCTRL_PBSB_PLAN_LIST&metodo=Liste&nu_empresa=21&dt_envio_bsb=".$this->getValorFiltro('dt_envio_bsb')."', '_blank');";
			$this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel Petrobrás", "EXP_EXCEL_PETR",  "Clique para exportar em Excel", $onclick));
		}
		$this->gerouSeparador = false;

	}

	public function RenderizeCabecalhoListagem()
	{
		parent::RenderizeCabecalhoListagem();
		$this->linhaPendente = false;
	}

	public function CustomizarLinha()
	{
		// Disponibiliza botão de inclusão no protocolo
		if($this->getValorCampo('dt_envio_bsb') == ''){
			$parametros = '{"controller": "cCTRL_UPDT_PROC"';
			$parametros.= ', "metodo": "dt_envio_bsb"';
			$parametros.= ', "tabela": "'.$this->getValorCampo('tabela').'"';
			$parametros.= ', "dt_envio_bsb": "'.$this->getValorFiltro('dt_envio_bsb').'"';
			$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
			$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
			$parametros.= '}';
			$xdata = cBANCO::DataFmt($this->getValorFiltro('dt_envio_bsb'), false);
			if($xdata == ''){
				$xdata = '??/??/??';
				$links = '<a href="#" onclick="alert(\'Informe a data do protocolo que a OS deve ser incluída.\');">Incluir<br/>'.$xdata.'</a>';
				$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Incluir<br/>'.$xdata.'</a>';
			}
			else{
				$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Incluir<br/>'.$xdata.'</a>';
			}
		}
		else{
			$parametros = '{"controller": "cCTRL_UPDT_PROC"';
			$parametros.= ', "metodo": "RetirarDoProtocoloBsb"';
			$parametros.= ', "tabela": "'.$this->getValorCampo('tabela').'"';
			$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
			$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
			$parametros.= '}';
			$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Retirar</a>';
		}

		$this->setValorCampo('acao', $links);

 		$link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$this->getValorCampo('nu_candidato').'" target="_blank">'.$this->getValorCampo('nome_completo').'</a>';
		$this->setValorCampo('nome_completo', $link);

		$link = cCTRL_ORDEMSERVICO::LinkProcessoOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), '_blank');
		$this->setValorCampo('nu_solicitacao', $link);

		$parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "de_observacao_bsb"';
		$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['de_observacao_bsb']->set_parametros($parametros);

		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'soli_qt_autenticacoes', 'id_solicita_visto');
		$this->HabiliteEditInPlacePadrao('cCTRL_UPDT_SOLV', 'soli_qt_autenticacoes_bsb', 'id_solicita_visto');

	}

	public function RenderizeLinha()
	{
		// Se mudou a OS e não é o primeiro registro, então descarrega o que está acumulado
//		error_log('Nova linha '.$this->getValorCampo('nu_solicitacao').' '.$this->getValorCampo('nome_completo'));
		$this->FormateCampoData('dt_solicitacao');
		if($this->os_ant != '' && $this->os_ant != $this->getValorCampo('id_solicita_visto').$this->getValorCampo('tabela')){
			// Salva a linha atual, que deve depois ser adicionada ao buffer
			$xlinha = array();
			foreach($this->mCampo as $campo){
				$xlinha[$campo->mCampoBD] = clone $campo;
			}
			// Descarrega o buffer
			$this->RenderizeBuffer();


			//Inicializa o buffer com a linha atual salva
			unset($this->bufferLinhas);
//			error_log('Zerou buffer e restaurou ultima linha salva '.$xlinha['nu_solicitacao']->mValor.' '.$xlinha['nome_completo']->mValor);
			$this->mCampo = $xlinha;
		}
			if($this->getValorCampo('dt_envio_bsb') == '' and !$this->gerouSeparador){
				$this->gerouSeparador = true;
				$linha = '<tr><td colspan="11" style="font-size:14px; padding-top: 15px; font-weight:bold;">OS que podem ser incluídas nesse protocolo:</td></tr>';
				$this->GerarOutput($linha);
			}
		// Clona o conteúdo dos campos
		$xCampos = array();
		foreach($this->mCampo as $campo){
			$xCampos[$campo->mCampoBD] = clone $campo;
		}
		// Adiciona o registro no buffer.
//		error_log('-->Linha que esta indo para o buffer '.$xCampos['nu_solicitacao']->mValor.' '.$xCampos['nome_completo']->mValor);
		$this->bufferLinhas[] = $xCampos;
//		error_log('-->Armazenada no buffer. Tam buffer:'.count($this->bufferLinhas));
		$this->os_ant = $this->getValorCampo('id_solicita_visto').$this->getValorCampo('tabela');
		$this->linhaPendente = true;
	}

	/**
	 * Descarrega o buffer para a tela
	 */
	public function RenderizeBuffer()
	{
		// Identifica quantas linhas foram armazenadas
		$qtd = count($this->bufferLinhas);
		if($qtd == 1){
			$this->mEstiloLinha = "bloco";
			$this->mCampo = $this->bufferLinhas[0];
			$this->VisibilidadeCamposFixos(true);
			$this->ConfigureQtdLinhasDetalhe('');
			parent::RenderizeLinha();
		}
		else{
			$i = 0;
			foreach($this->bufferLinhas as $linha){
				$this->mCampo = $linha;
				// Imprime as colunas fixas na primeira linha
				if($i == 0){
					$this->VisibilidadeCamposFixos(true);
//					$this->mCampo = $this->bufferLinhas[0];
					$this->ConfigureQtdLinhasDetalhe($qtd);
				}
				// Esconde as colunas fixas e imprime os detalhes
				else{
					$this->mEstiloLinha = "";
					$this->VisibilidadeCamposFixos(false);
				}
				parent::RenderizeLinha();
				$i++;
			}
		}
	}

	public function VisibilidadeCamposFixos($pvisibilidade)
	{
		if(cHTTP::$MIME != cHTTP::mmXLS){
			$this->mCampo['acao']->mVisivel = $pvisibilidade;
		}
		$this->mCampo['nu_solicitacao']->mVisivel = $pvisibilidade;
		$this->mCampo['dt_solicitacao']->mVisivel = $pvisibilidade;
		$this->mCampo['dt_prazo_estada_solicitado']->mVisivel = $pvisibilidade;
		$this->mCampo['no_razao_social']->mVisivel = $pvisibilidade;
		$this->mCampo['no_embarcacao_projeto']->mVisivel = $pvisibilidade;
		$this->mCampo['no_servico_resumido']->mVisivel = $pvisibilidade;
		$this->mCampo['de_observacao_bsb']->mVisivel = $pvisibilidade;
		$this->mCampo['soli_qt_autenticacoes']->mVisivel = $pvisibilidade;
	}

	public function ConfigureQtdLinhasDetalhe($pqtd)
	{
		$this->mCampo['acao']->mRowspan = $pqtd;
		$this->mCampo['nu_solicitacao']->mRowspan = $pqtd;
		$this->mCampo['dt_solicitacao']->mRowspan = $pqtd;
		$this->mCampo['dt_prazo_estada_solicitado']->mRowspan = $pqtd;
		$this->mCampo['no_razao_social']->mRowspan = $pqtd;
		$this->mCampo['no_embarcacao_projeto']->mRowspan = $pqtd;
		$this->mCampo['no_servico_resumido']->mRowspan = $pqtd;
		$this->mCampo['de_observacao_bsb']->mRowspan = $pqtd;
		$this->mCampo['soli_qt_autenticacoes']->mRowspan = $pqtd;
	}


	public function RenderizeListagemRodape(){
		$this->RenderizeBuffer();
		parent::RenderizeListagemRodape();
	}
}
