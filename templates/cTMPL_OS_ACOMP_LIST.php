<?php
/**
 *
 * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada.
 */
class cTMPL_OS_ACOMP_LIST extends cTEMPLATE_LISTAGEM{
	public $acum;
	public $os_ant;
	public $linhaPendente;
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Ordens de Serviço - Acompanhamento";
		$this->mCols = array("auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "200px");
		$this->mOrdenacaoDefault = 'dt_solicitacao desc';
		// Adiciona campos da tela
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('bo_cadastro_minimo_ok'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('ID_STATUS_SOL'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('SI_STATUS_SOL'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('autc_fl_revisado'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('soli_dt_devolucao'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('ID_TIPO_ACOMPANHAMENTO'));

        $this->AdicioneColunaChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));

        $this->AdicioneColuna(new cCAMPO_TEXTO('NO_STATUS_SOL', 'Status'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_TEXTO('acao', 'Ação'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('button');
		$this->AdicioneColuna(new cCAMPO_TEXTO('dt_solicitacao', 'Dt. solic.'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(new cCAMPO_TEXTO('nu_solicitacao', 'OS'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Candidato(s)'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('no_reparticao_consular', 'Consulado'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('situacao_cadastro', 'Cadastro Ok?'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('button');
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL', 'Empresa PROCESSO'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarcação/projeto PROCESSO'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_SERVICO_RESUMIDO', 'Serviço'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_USUARIO', 'Aberta por'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('nu_protocolo', '# processo'));
		$this->EstilizeUltimoCampo('editavel');
		$this->AdicioneColuna(new cCAMPO_TEXTO('nome_devolucao', 'Cadastro por'));
		$this->AdicioneCampo(new cCAMPO_MEMO('de_observacao', 'Observações'));
		$this->EstilizeUltimoCampo('editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('ID_STATUS_SOL', 'Situação', cCOMBO::cmbSTATUS_SOLICITACAO));
		$this->mFiltro['ID_STATUS_SOL']->mQualificadorFiltro = 'v';
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_ini', 'Cadastradas desde'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_fim', 'até'));
	}

	public function CustomizarCabecalho()
	{
		parent::CustomizarCabecalho();
		$this->mFiltro['ID_STATUS_SOL']->mExtra = ' and FL_ENCERRADA = 0';
	}

	public function RenderizeCabecalhoListagem()
	{
		parent::RenderizeCabecalhoListagem();
		$x = '<div style="background-color:#DADADA;padding:5px;background-image:url(/imagens/icons/seta_esquerda_baixo.png);background-repeat: no-repeat;background-position: center left;padding-left:25px;"> ';
//		$x .= cINTERFACE::RenderizeCampo($acoes);
		$x .= '<button  onclick="ConfirmaAcao(this,\'LIBERAR_SELECIONADOS\',\'\',\'\',\'\');">Enviar</button>
		</div>';
		$js = "
			$('#SELETOR_ALL').on('click',
				function(event) {
					if($('#SELETOR_ALL').is(':checked')){
						$('.seletor').prop('checked', true);
					}
					else{
						$('.seletor').prop('checked', false);
					}
				}
			);
		";

		cHTTP::AdicionarScript($js);
		$this->linhaPendente = false;
	}

	public function CustomizarLinha()
	{
		if($this->getValorCampo('autc_fl_revisado') == 1){
			$link = '<i class="icon-ok"></i>';
		}
		else{
			$link = '<a href="/paginas/carregueComMenu.php?controller=cCTRL_CAND_REVISAR_OS&metodo=Edite&NU_CANDIDATO='.$this->getValorCampo('NU_CANDIDATO').'&id_solicita_visto='.$this->getValorCampo('id_solicita_visto').'&painelAtivo=3" target="_blank">Revisar</a>';
		}
		$this->setValorCampo('situacao_cadastro', $link);

		$valor = '<span class='.$this->getValorCampo('SI_STATUS_SOL').' title="'.$this->getValorCampo('NO_STATUS_SOL').'">'.$this->getValorCampo('SI_STATUS_SOL').'</span>';
		$this->setValorCampo('NO_STATUS_SOL', $valor);

        $links = '';
        $br = '';
		if($this->getValorCampo('ID_STATUS_SOL')== '1'){
			$parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "Devolver"';
			$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
			$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
			$parametros.= '}';
			$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>devolver</a>';

			$parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "Pendente"';
			$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
			$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
			$parametros.= '}';
			$links .= '<br/><a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>pendente</a>';
            $br = '<br/>';
		}
		elseif($this->getValorCampo('ID_STATUS_SOL')== '2'){
			$parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "Devolver"';
			$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
			$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
			$parametros.= '}';
			$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>devolver</a>';
            $br = '<br/>';
		}
		if($this->getValorCampo('ID_STATUS_SOL')== '1' || $this->getValorCampo('ID_STATUS_SOL')== '2'){
            if($this->getValorCampo('ID_TIPO_ACOMPANHAMENTO') == '1' || $this->getValorCampo('ID_TIPO_ACOMPANHAMENTO') == '3'){
                $parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "EnviadoDigital"';
                $parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
                $parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
                $parametros.= '}';
                $links .= $br.'<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>enviado digital</a>';

            }
        }

        $this->setValorCampo('acao', $links);

        if (cHTTP::$MIME != cHTTP::mmXLS) {
            if($this->getValorCampo('ID_TIPO_ACOMPANHAMENTO') == '1' || $this->getValorCampo('ID_TIPO_ACOMPANHAMENTO') == '3' || $this->getValorCampo('ID_TIPO_ACOMPANHAMENTO') == '6'){
                $parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "AtualizaNumeroProcessoDigital"';
                $parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
                $parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
                $parametros.= '}';
                $this->mCampo['nu_protocolo']->set_parametros($parametros);
            }
        }



 		$link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$this->getValorCampo('NU_CANDIDATO').'" target="_blank">'.$this->getValorCampo('NOME_COMPLETO').'</a>';
		$this->setValorCampo('NOME_COMPLETO', $link);

		$link = '<a href="/operador/os_Formularios.php?&ID_SOLICITA_VISTO='.$this->getValorCampo('id_solicita_visto').'&id_solicita_visto='.$this->getValorCampo('id_solicita_visto').'" target="_blank">'.$this->getValorCampo('nu_solicitacao').'</a>';
		$this->setValorCampo('nu_solicitacao', $link);

		$parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "de_observacao"';
		$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['de_observacao']->set_parametros($parametros);

		if($this->getValorCampo('nome_devolucao')!= ''){
			$this->setValorCampo('nome_devolucao', $this->getValorCampo('nome_devolucao').'<br/>em '.$this->getValorCampo('soli_dt_devolucao'));
		}
	}

	public function RenderizeLinha()
	{
		// Se mudou a OS e não é o primeiro registro, então descarrega o que está acumulado
		if($this->os_ant != '' && $this->os_ant != $this->getValorCampo('id_solicita_visto')){
			// Salva a linha atual, que deve depois ser adicionada ao buffer
			$xlinha = array();
			foreach($this->mCampo as $campo){
				$xlinha[$campo->mCampoBD] = clone $campo;
			}
			// Descarrega o buffer
			$this->RenderizeBuffer();

			//Inicializa o buffer com a linha atual salva
			unset($this->bufferLinhas);
//			$xCampos = array();
//			foreach($xlinha as $campo){
//				$xCampos[$campo->mCampoBD] = clone $campo;
//			}
//			error_log('Zerou buffer e adicionou ultima OS '.$xCampos['nu_solicitacao']->mValor.' '.$xCampos['NOME_COMPLETO']->mValor);
//			$this->bufferLinhas[] = $xlinha;
			$this->mCampo = $xlinha;
//			$this->os_ant = $xlinha['id_solicita_visto']->mValor;
		}
		// Caso contrário acumula o candidato no buffer.
//		else{
			// Clona o conteúdo dos campos
			$xCampos = array();
			foreach($this->mCampo as $campo){
				$xCampos[$campo->mCampoBD] = clone $campo;
			}
			// Adiciona o registro no buffer.
			$this->bufferLinhas[] = $xCampos;

			$this->os_ant = $this->getValorCampo('id_solicita_visto');
			$this->linhaPendente = true;
//		}
	}

	/**
	 * Descarrega o buffer para a tela
	 */
	public function RenderizeBuffer()
	{
		// Identifica quantas linhas foram armazenadas
		$qtd = count($this->bufferLinhas);
		if($qtd == 1){
			$this->mEstiloLinha = "bloco";
			$this->mCampo = $this->bufferLinhas[0];
			$this->VisibilidadeCamposFixos(true);
			$this->ConfigureQtdLinhasDetalhe('');
			parent::RenderizeLinha();
		}
		else{
			$i = 0;
			foreach($this->bufferLinhas as $linha){
				$this->mCampo = $linha;
				// Imprime as colunas fixas na primeira linha
				if($i == 0){
					$this->VisibilidadeCamposFixos(true);
//					$this->mCampo = $this->bufferLinhas[0];
					$this->ConfigureQtdLinhasDetalhe($qtd);
				}
				// Esconde as colunas fixas e imprime os detalhes
				else{
					$this->mEstiloLinha = "";
					$this->VisibilidadeCamposFixos(false);
				}
				parent::RenderizeLinha();
				$i++;
			}
		}
	}

	public function VisibilidadeCamposFixos($pvisibilidade)
	{
		$this->mCampo['acao']->mVisivel = $pvisibilidade;
		$this->mCampo['NO_STATUS_SOL']->mVisivel = $pvisibilidade;
		$this->mCampo['dt_solicitacao']->mVisivel = $pvisibilidade;
		$this->mCampo['nu_solicitacao']->mVisivel = $pvisibilidade;
		$this->mCampo['NO_RAZAO_SOCIAL']->mVisivel = $pvisibilidade;
		$this->mCampo['NO_EMBARCACAO_PROJETO']->mVisivel = $pvisibilidade;
		$this->mCampo['NO_SERVICO_RESUMIDO']->mVisivel = $pvisibilidade;
		$this->mCampo['NO_USUARIO']->mVisivel = $pvisibilidade;
		$this->mCampo['nome_devolucao']->mVisivel = $pvisibilidade;
		$this->mCampo['nu_protocolo']->mVisivel = $pvisibilidade;
		$this->mCampo['de_observacao']->mVisivel = $pvisibilidade;
	}

	public function ConfigureQtdLinhasDetalhe($pqtd)
	{
		$this->mCampo['acao']->mRowspan = $pqtd;
		$this->mCampo['NO_STATUS_SOL']->mRowspan = $pqtd;
		$this->mCampo['dt_solicitacao']->mRowspan = $pqtd;
		$this->mCampo['nu_solicitacao']->mRowspan = $pqtd;
		$this->mCampo['NO_RAZAO_SOCIAL']->mRowspan = $pqtd;
		$this->mCampo['NO_EMBARCACAO_PROJETO']->mRowspan = $pqtd;
		$this->mCampo['NO_SERVICO_RESUMIDO']->mRowspan = $pqtd;
		$this->mCampo['NO_USUARIO']->mRowspan = $pqtd;
		$this->mCampo['nome_devolucao']->mRowspan = $pqtd;
		$this->mCampo['nu_protocolo']->mRowspan = $pqtd;
		$this->mCampo['de_observacao']->mRowspan = $pqtd;
	}


	public function RenderizeListagemRodape(){
		$this->RenderizeBuffer();
		parent::RenderizeListagemRodape();
	}
}
