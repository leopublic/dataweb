<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTEMPLATE_CANDIDATO_EXPPROF_VIEW extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		if (cHTTP::getLang() =='pt_br'){
			$this->mTitulo = "Experiência prévia informada";
		}
		else{
			$this->mTitulo = "Saved previous experience";
		}
		$this->mMsgInicial = "Please fill out this form carefully, it is very important that the data given be complete and correct to avoid delays with the Visa authorization. Please fill in all the blank spaces.<br/><br/><b>You will need the following paperwork to submit to the Consulate when collecting your visa in a few weeks from now:</b><br/>1) Police letter - required for <b>all</b> visas (except 30 day emergencial and 90 day technical visas);<br/>2) Passport with validity equal to the visa period and with a minimun of 4 clean pages.<br/><br/>";
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneColunaChave(new cCAMPO_HIDDEN('epro_id'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('epro_no_companhia', 'Empresa', 'Company'));
		$this->mCampo['epro_no_companhia']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('epro_no_funcao', 'Função', 'Function'));
		$this->mCampo['epro_no_funcao']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('epro_no_periodo', 'Período', 'Period'));
		$this->mCampo['epro_no_periodo']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('epro_tx_atribuicoes', 'Atribuições/responsabilidades', 'Atributed responsabilities'));
		$this->mCampo['epro_tx_atribuicoes']->mClasse = 'esq';
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcaoLinha(new cACAO_SUBMIT_BUTTON("Remove", "EXCLUIR", "Hit here to remove this item."));
	}
}
