<?php
class cTMPL_TABELA_PRECOS_EDITAR extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Incluir/alterar Tabela de pre&ccedil;os";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
		$this->qtd_colunas = 1;
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'tbpc_id'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tbpc_tx_nome', 'Nome da tabela'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'tbpc_nu_ano', 'Ano de refer&ecirc;ncia'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'tbpc_fl_tem_pacote', 'Tem pacote?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'tbpc_tx_observacao', 'Observa&ccedil;&otilde;es'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar"));
	}
}
