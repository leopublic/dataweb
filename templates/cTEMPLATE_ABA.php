<?php
class cTEMPLATE_ABA extends cTEMPLATE implements iTEMPLATE_ABA{
	public $mAbas = array();
	public $indiceDasAbas = array();
	public $nomeAbaAtiva;

	public function AdicioneAbaAjax($pNome, $pLabel, $pController, $pMetodo, $pTitulo, $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = ""){
		$acao = new cACAO_LINK_ABA($pNome, $pLabel, $pTitulo, $pController, $pMetodo, $pMsgConfirmacao, $pMsgConfirmacaoTitulo );
		$this->indiceDasAbas[$pNome] = count($this->mAbas);
		$this->mAbas[$pNome] = $acao;
	}

	public function AdicioneAbaLink($pNome, $pLabel, $pLink, $pTitulo, $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = ""){
		$acao = new cACAO_LINK($pLabel, $pNome, $pTitulo, $pLink, $pMsgConfirmacao, $pMsgConfirmacaoTitulo );
		$this->indiceDasAbas[$pNome] = count($this->mAbas);
		$this->mAbas[$pNome] = $acao;
	}

	public function Renderize(){
		$this->OutputInicializar();
		$this->CarregueDoHTTP();
		$this->RepliqueChavesParaAbasAjax();

		$this->RenderizeCabecalho();
		$this->RenderizeRodape();
	}

	public function RenderizeCabecalho(){
		$this->GerarOutput(cLAYOUT::RenderizeTitulo($this));
		$this->GerarOutput('<div id="tabs">');
		$this->RenderizeIndiceDeAbas();
		$this->GerarOutput('<div id="abaAtiva">');		
	}

	public function RenderizeRodape(){
		$ret .= '</div>';
		$ret .= '</div>';
		$ret = cLAYOUT::RenderizeForm($pTemplateAbas, $ret);
		$ret .= $pTemplateAbas->TratamentoPadraoDeMensagens();
	}

	public function RenderizeIndiceAbas(){
		$this->GerarOutput('<ul>');
		foreach($this->mAbas as $aba){
			if ($aba->mCodigo == $this->nomeAbaAtiva){
				$this->GerarOutput('<li><a href="#abaAtiva">'. $aba->mLabel .'</a></li>');
			}
			else{
				$this->GerarOutput('<li>'. cINTERFACE::RenderizeAcao($aba) .'</li>');
			}
		}
		$this->GerarOutput('</ul>');
	}

	public function GerarScriptDeInicializacao(){
		$script = '$(document).ready(function(){
						$("#tabs").tabs({
							active: '.$this->indiceDasAbas[$this->nomeAbaAtiva].',
							heightStyle: "content",
							beforeActivate: function(event, ui) {
								if(ui.newTab.context.href.substr(0,4) == "http" ){
									window.location.href = ui.newTab.context.href;
									return false;
								}
								else{
									return true;
								}
							}
						})
					});
				';
		cHTTP::AdicionarScript($script);
	}

	/**
	 * Replica o valor recebido nos campos marcados como "chave" como parametro nos links das abas
	 */
	public function RepliqueChavesParaAbasAjax(){
		if(count($this->mCampoChave) > 0){
			$parametros = '' ;
			foreach($this->mCampoChave as $chave){
				$parametros .= '&'. $chave->mCampoBD .'='. $chave->mValor;
			}
			foreach($this->mAbas as $aba){
				$aba->mParametros .= $parametros;
			}
		}

	}

	public function CustomizeAbas() {
	}

	public function JSON_twig(){
		$ret = array();
		$ret['abas'] = $this->mAbas;		
		return $ret;
	}
}
