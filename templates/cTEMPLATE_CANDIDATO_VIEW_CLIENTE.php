<?php
class cTEMPLATE_CANDIDATO_VIEW_CLIENTE extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicionePainel("Dados pessoais", "Personal data");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NOME_COMPLETO', 'Nome completo', 'Full name'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_EMAIL_CANDIDATO', 'E-mail de contato', 'Contact email'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAI', "Nome do pai", "Father's full name"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_MAE', "Nome da mãe", "Mother's full name"));
		if (cHTTP::getLang() == 'pt_br'){
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_NACIONALIDADE', "Nacionalidade", "Nationality"));
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ESCOLARIDADE', "Escolaridade", "Highest level of education"));
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ESTADO_CIVIL', "Estado civil", "Marital status"));
		}
		else{
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_NACIONALIDADE_EM_INGLES', "Nacionalidade", "Nationality"));
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ESCOLARIDADE_EM_INGLES', "Escolaridade", "Highest level of education"));
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ESTADO_CIVIL_EM_INGLES', "Estado civil", "Marital status"));
		}
		$this->AdicioneCampo(new cCAMPO_TEXTO('CO_SEXO', "Sexo", "Gender"));
		$this->AdicioneCampo(new cCAMPO_DATA('DT_NASCIMENTO', "Data de nascimento", "Date of birth"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_LOCAL_NASCIMENTO', "Local de nascimento", "Place of birth"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_RNE', "RNE"));

		$this->AdicionePainel("Dados de salário (Atenção: Isso não tem objetivos fiscais. Por favor informe o seu rendimento global real.)", "Salary information (NOTE: This does NOT mean you will be subject to tax. Please state your GLOBAL monthly salary)");
		$this->AdicioneCampo(new cCAMPO_TEXTO('VA_REMUNERACAO_MENSAL', "Salário mensal", "Monthly income"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('VA_REMUNERACAO_MENSAL_BRASIL', "Salário mensal no Brasil", "Monthly salary in Brazil"));

		$this->AdicionePainel("Endereço residencial", "Home address");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ENDERECO_RESIDENCIA', 'Endereço', 'Address'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CIDADE_RESIDENCIA', 'Cidade', 'City'));
		if (cHTTP::getLang() == 'pt_br'){
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAIS_RESIDENCIA', "País", "Country"));
		}
		else{
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAIS_RESIDENCIA_EM_INGLES', "País", "Country"));
		}
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_TELEFONE_CANDIDATO', 'Telefone', 'Phone #'));

		$this->AdicionePainel("Endereço comercial", "Business address");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_ENDERECO_EMPRESA', 'Endereço', 'Address'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_CIDADE_EMPRESA', 'Cidade', 'City'));
		if (cHTTP::getLang() == 'pt_br'){
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAIS_EMPRESA', "País", "Country"));
		}
		else{
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAIS_EMPRESA_EM_INGLES', "País", "Country"));
		}
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_TELEFONE_EMPRESA', 'Telefone', 'Phone #'));

		$this->AdicionePainel("Passporte", "Passport");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_PASSAPORTE', "Número do passaporte", "Passport number"));
		$this->AdicioneCampo(new cCAMPO_DATA('DT_EMISSAO_PASSAPORTE', "Data de emissão", "Issue date"));
		$this->AdicioneCampo(new cCAMPO_DATA('DT_VALIDADE_PASSAPORTE', "Data de expiração", "Expiry date"));
		if (cHTTP::getLang() == 'pt_br'){
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAIS_EMISSOR_PASSAPORTE', "País emissor", "Issuing government"));
		}
		else{
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAIS_EMISSOR_PASSAPORTE_EM_INGLES', "País emissor", "Issuing government"));
		}
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_REPARTICAO_CONSULAR', "Consulado onde o visto será coletado", "Consulate where the visa will be collected"));

		$this->AdicionePainel("Seaman's book");
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_SEAMAN', "Número do Seaman's book", "Seaman's book number"));
		$this->AdicioneCampo(new cCAMPO_DATA('DT_EMISSAO_SEAMAN', "Data de emissão", "Issue date"));
		$this->AdicioneCampo(new cCAMPO_DATA('DT_VALIDADE_SEAMAN', "Data de expiração", "Expiry date"));
		if (cHTTP::getLang() == 'pt_br'){
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAIS_SEAMAN', "País emissor", "Issuing government"));
		}
		else{
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PAIS_SEAMAN_EM_INGLES', "País emissor", "Issuing government"));
		}

		$this->AdicionePainel("Descrição das atividades", "Occupation and job description");
		if (cHTTP::getLang() == 'pt_br'){
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_FUNCAO', "Função", "Occupation"));
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PROFISSAO', "Profissão", "Position"));
		}
		else{
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_FUNCAO_EM_INGLES', "Função", "Occupation"));
			$this->AdicioneCampo(new cCAMPO_TEXTO('NO_PROFISSAO_EM_INGLES', "Profissão", "Position"));
		}
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', "Embarcação/projeto", "Vessel / project name"));
		$this->AdicioneCampo(new cCAMPO_TEXTO('loca_no_nome', "Local da embarcação/projeto", "Vessel / project location"));
		$this->AdicioneCampo(new cCAMPO_MEMO('TE_DESCRICAO_ATIVIDADES', "Descreva as atividades que você pretende desempenhar no Brasil", "Provide a description of the activities you expect to carry out in Brazil"));
		$this->mCampo['TE_DESCRICAO_ATIVIDADES']->mLarguraDupla = 1;
		$this->AdicioneCampo(new cCAMPO_MEMO('TE_TRABALHO_ANTERIOR_BRASIL', "Você já trabalhou no Brasil anteriormente? Em caso afirmativo, descreva brevemente onde, quando e a duração de sua estada anterior", "Have you worked in Brazil before? If so, state briefly when / place / length of last stay"));
		$this->mCampo['TE_TRABALHO_ANTERIOR_BRASIL']->mLarguraDupla = 1;
		$this->ProtejaTodosCampos();
	}
}
