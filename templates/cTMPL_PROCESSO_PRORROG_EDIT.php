<?php
class cTMPL_PROCESSO_PRORROG_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('nu_servico'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('codigo'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('cd_candidato'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('cd_solicitacao'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('nu_protocolo', 'Número do Protocolo'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_requerimento', 'Data de Requerimento'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_validade', 'Validade do Protocolo'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_prazo_pret', 'Prazo Pretendido'));
		$this->AdicioneCampo(new cCAMPO_DATA('dt_publicacao_dou', 'Data publicação DOU'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NO_STATUS_CONCLUSAO', 'Status'));
		$this->AdicioneCampo(new cCAMPO_MEMO('observacao', 'Observação'));
	}
}
