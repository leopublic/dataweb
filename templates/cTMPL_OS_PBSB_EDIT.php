<?php
/**
 *
 * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada.
 */
class cTMPL_OS_PBSB_EDIT extends cTEMPLATE_LISTAGEM{
	public $acum;
	public $os_ant;
	public $linhaPendente;
	public function __construct()
	{
		parent::__construct(__CLASS__);
		$this->mTitulo = "Protocolo de Brasília - CONSULTAR PROTOCOLO";
		$this->mCols = array("auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "auto", "200px");
		$this->mOrdenacaoDefault = 'nu_solicitacao desc';
		// Adiciona campos da tela
		$this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN,'id_solicita_visto'));
		$this->AdicioneColunaChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato'));

		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN,'tabela'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN,'codigo'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN,'ID_STATUS_SOL'));

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'acao', 'Ação'));
		$this->EstilizeUltimoCampo('cen');
		$this->EstilizeUltimoCampo('button');

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_envio_bsb', 'Protocolo BSB'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_solicitacao', 'Dt. solic.'));
		$this->EstilizeUltimoCampo('cen');
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_razao_social', 'Empresa PROCESSO'));

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato(s)'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_passaporte', '# passaporte'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_reparticao_consular', 'Repartição consular'));

		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_servico_resumido', 'Serviço'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'no_embarcacao_projeto', 'Embarcação/projeto PROCESSO'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'dt_prazo_estada_solicitado', 'Prazo pretendido'));
		$this->EstilizeUltimoCampo('cen');

		$this->AdicioneCampo(new cCAMPO_MEMO('de_observacao_bsb', 'Observações'));
		$this->EstilizeUltimoCampo('editavel');

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('dt_envio_bsb', 'Protocolo do dia', cCOMBO::cmbULTIMOS_PROTOCOLOS_BSB));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_ini', 'Cadastradas desde'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'dt_solicitacao_fim', 'até'));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('nu_empresa', 'Empresa', cCOMBO::cmbEMPRESA_ATIVA));
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_solicitacao', 'OS'));
		$this->mFiltro['nu_solicitacao']->mQualificadorFiltro = 's';
		$this->AdicioneFiltro(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome_completo', 'Candidato'));

		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO('Montar protocolo', 'LISTAR_ENVIADAS', "Ver as OS pendentes", 'cCTRL_OS_PBSB_LIST', 'Liste'));
		$onclick = "window.open('carregueComoExcel.php?controller=cCTRL_PBSB_PLAN_LIST&metodo=Liste&nu_empresa=xx', '_blank');";
		$this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel Geral", "EXP_EXCEL_GERAL",  "Clique para exportar em Excel", $onclick));
		$onclick = "window.open('carregueComoExcel.php?controller=cCTRL_PBSB_PLAN_LIST&metodo=Liste&nu_empresa=21', '_blank');";
		$this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel Petrobrás", "EXP_EXCEL_PETR",  "Clique para exportar em Excel", $onclick));
	}

	public function CustomizarCabecalho() {
		parent::CustomizarCabecalho();
		$nomeArq = 'protocolo_bsb';
		if($this->getValorFiltro('dt_envio_bsb') != ''){
			$nomeArq .= "_".str_replace("/", "_", $this->getValorFiltro('dt_envio_bsb'));
		}

		$onclick = "window.open('carregueComoExcel.php?controller=cCTRL_PBSB_PLAN_LIST&metodo=Liste&nu_empresa=xx&dt_envio_bsb=".$this->getValorFiltro('dt_envio_bsb')."', '_blank');";
		$this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel Geral", "EXP_EXCEL_GERAL",  "Clique para exportar em Excel", $onclick));

		$onclick = "window.open('carregueComoExcel.php?controller=cCTRL_PBSB_PLAN_LIST&metodo=Liste&nu_empresa=21&dt_envio_bsb=".$this->getValorFiltro('dt_envio_bsb')."', '_blank');";
		$this->AdicioneAcao(new cACAO_JAVASCRIPT("Excel Petrobrás", "EXP_EXCEL_PETR",  "Clique para exportar em Excel", $onclick));

		cHTTP::set_nomeArquivo($nomeArq.'.xls');
	}


	public function RenderizeCabecalhoListagem()
	{
		parent::RenderizeCabecalhoListagem();
		$this->linhaPendente = false;
	}

	public function CustomizarLinha()
	{
		$this->FormateCampoData('dt_solicitacao');
		$this->FormateCampoData('dt_envio_bsb');
		// Disponibiliza botão de inclusão no protocolo
		$parametros = '{"controller": "cCTRL_UPDT_PROC"';
		$parametros.= ', "metodo": "RetirarDoProtocoloBsb"';
		$parametros.= ', "tabela": "'.$this->getValorCampo('tabela').'"';
		$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$links = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>Retirar</a>';

		$this->setValorCampo('acao', $links);

 		$link = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO='.$this->getValorCampo('nu_candidato').'" target="_blank">'.$this->getValorCampo('nome_completo').'</a>';
		$this->setValorCampo('nome_completo', $link);

		$link = cCTRL_ORDEMSERVICO::LinkProcessoOs($this->getValorCampo('id_solicita_visto'), $this->getValorCampo('nu_solicitacao'), '_blank');
		$this->setValorCampo('nu_solicitacao', $link);


		$parametros = '{"controller": "cCTRL_UPDT_SOLV", "metodo": "de_observacao_bsb"';
		$parametros.= ', "id_solicita_visto": "'.$this->getValorCampo('id_solicita_visto').'"';
		$parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
		$parametros.= '}';
		$this->mCampo['de_observacao_bsb']->set_parametros($parametros);
	}

	public function RenderizeLinha()
	{
		// Se mudou a OS e não é o primeiro registro, então descarrega o que está acumulado
//		error_log('Nova linha '.$this->getValorCampo('nu_solicitacao').' '.$this->getValorCampo('nome_completo'));
		if($this->os_ant != '' && $this->os_ant != $this->getValorCampo('id_solicita_visto')){
			// Salva a linha atual, que deve depois ser adicionada ao buffer
			$xlinha = array();
			foreach($this->mCampo as $campo){
				$xlinha[$campo->mCampoBD] = clone $campo;
			}
			// Descarrega o buffer
			$this->RenderizeBuffer();

			//Inicializa o buffer com a linha atual salva
			unset($this->bufferLinhas);
//			error_log('Zerou buffer e restaurou ultima linha salva '.$xlinha['nu_solicitacao']->mValor.' '.$xlinha['nome_completo']->mValor);
			$this->mCampo = $xlinha;
		}
		// Clona o conteúdo dos campos
		$xCampos = array();
		foreach($this->mCampo as $campo){
			$xCampos[$campo->mCampoBD] = clone $campo;
		}
		// Adiciona o registro no buffer.
//		error_log('-->Linha que esta indo para o buffer '.$xCampos['nu_solicitacao']->mValor.' '.$xCampos['nome_completo']->mValor);
		$this->bufferLinhas[] = $xCampos;
//		error_log('-->Armazenada no buffer. Tam buffer:'.count($this->bufferLinhas));
		$this->os_ant = $this->getValorCampo('id_solicita_visto');
		$this->linhaPendente = true;
	}

	/**
	 * Descarrega o buffer para a tela
	 */
	public function RenderizeBuffer()
	{
		// Identifica quantas linhas foram armazenadas
		$qtd = count($this->bufferLinhas);
		if($qtd == 1){
			$this->mEstiloLinha = "bloco";
			$this->mCampo = $this->bufferLinhas[0];
			$this->VisibilidadeCamposFixos(true);
			$this->ConfigureQtdLinhasDetalhe('');
			parent::RenderizeLinha();
		}
		else{
			$i = 0;
			foreach($this->bufferLinhas as $linha){
				$this->mCampo = $linha;
				// Imprime as colunas fixas na primeira linha
				if($i == 0){
					$this->VisibilidadeCamposFixos(true);
//					$this->mCampo = $this->bufferLinhas[0];
					$this->ConfigureQtdLinhasDetalhe($qtd);
				}
				// Esconde as colunas fixas e imprime os detalhes
				else{
					$this->mEstiloLinha = "";
					$this->VisibilidadeCamposFixos(false);
				}
				parent::RenderizeLinha();
				$i++;
			}
		}
	}

	public function VisibilidadeCamposFixos($pvisibilidade)
	{
		if(cHTTP::$MIME != cHTTP::mmXLS){
			$this->mCampo['acao']->mVisivel = $pvisibilidade;
		}
		$this->mCampo['dt_envio_bsb']->mVisivel = $pvisibilidade;
		$this->mCampo['nu_solicitacao']->mVisivel = $pvisibilidade;
		$this->mCampo['dt_solicitacao']->mVisivel = $pvisibilidade;
		$this->mCampo['dt_prazo_estada_solicitado']->mVisivel = $pvisibilidade;
		$this->mCampo['no_razao_social']->mVisivel = $pvisibilidade;
		$this->mCampo['no_embarcacao_projeto']->mVisivel = $pvisibilidade;
		$this->mCampo['no_servico_resumido']->mVisivel = $pvisibilidade;
		$this->mCampo['de_observacao_bsb']->mVisivel = $pvisibilidade;
	}

	public function ConfigureQtdLinhasDetalhe($pqtd)
	{
		$this->mCampo['acao']->mRowspan = $pqtd;
		$this->mCampo['dt_envio_bsb']->mRowspan = $pqtd;
		$this->mCampo['nu_solicitacao']->mRowspan = $pqtd;
		$this->mCampo['dt_solicitacao']->mRowspan = $pqtd;
		$this->mCampo['dt_prazo_estada_solicitado']->mRowspan = $pqtd;
		$this->mCampo['no_razao_social']->mRowspan = $pqtd;
		$this->mCampo['no_embarcacao_projeto']->mRowspan = $pqtd;
		$this->mCampo['no_servico_resumido']->mRowspan = $pqtd;
		$this->mCampo['de_observacao_bsb']->mRowspan = $pqtd;
	}


	public function RenderizeListagemRodape(){
		$this->RenderizeBuffer();
		parent::RenderizeListagemRodape();
	}
}
