<?php
class cTMPL_CANDIDATO_TVS extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "";
		$this->mMsgInicial = "";
		// Adiciona campos da tela
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'ftvs_id'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_candidato'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_solicitacao'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NO_NACIONALIDADE_EM_INGLES'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'imprimir'));

		$this->AdicionePainel('Traveler Information');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PRIMEIRO_NOME', 'First name/Prenom'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NOME_MEIO', 'Middle name'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ULTIMO_NOME', 'Last name/Surname'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_CANDIDATO', 'Telephone number'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_CANDIDATO_CEL', 'Cell phone number'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMAIL_CANDIDATO', 'E-mail'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_SEXO', 'Gender', cCOMBO::cmbSEXO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_job_title', 'Traveler job title'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_employee_id', 'Employee ID#'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'ftvs_fl_us_citizen', 'US Citizen'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_NACIONALIDADE', 'Nationality', cCOMBO::cmbNACIONALIDADE));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO,'ftvs_tx_other_nationalities', 'Other nationality'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_us_status', 'US status (type of US visa)'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('trrs_id', 'Reason of travel', cCOMBO::cmbTRAVEL_REASON));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'ftvs_fl_offshore', 'Are you going offshore'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_visatype', 'Type of visa'));

		$this->AdicionePainel('Traveler Arranger / Focal Point Requestor');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_focalpoint', 'Name'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_focalpoint_tel', 'Telephone number'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_focalpoint_cel', 'CellPhone number'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_focalpoint_email', 'E-mail'));

		$this->AdicionePainel('Foreign Business Information');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_foreign_employer', 'Employer'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_foreign_address', 'Business address'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_foreign_tel', 'Telephone number'));

		$this->AdicionePainel('Brazil Business Information');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_brazil_employer', 'Name'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_brazil_address', 'Address'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_brazil_tel', 'Telephone number'));

		$this->AdicionePainel('Visa Request');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'ftvs_fl_request', 'Request visa?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_countries', 'Country(ies)'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('entt_id', 'Entries', cCOMBO::cmbVISA_ENTRIES));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'ftvs_dt_travel', 'Travel date (dd/mm/yy)'));

		$this->AdicionePainel('Passport Request');
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('past_id', 'Type', cCOMBO::cmbPASSPORT_REQUEST));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'ftvs_dt_passport_needed', 'Date needed (dd/mm/yy)'));

		$this->AdicionePainel('Other Request');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'ftvs_tx_other_request', 'Other request'));

		$this->AdicionePainel('Accounting Information');
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('accc_id', 'Category', cCOMBO::cmbACCOUNTING_CATEGORY));

		$this->AdicionePainel('Document Return Information');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'ftvs_fl_fedex', 'Fedex?'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_return_address', 'Street address'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_return_city', 'City/provence'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_return_country', 'State/Country'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'ftvs_tx_return_zip', 'Postal code'));

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique aqui para salvar os dados"));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar e Gerar PDF", "SALVAR_E_GERAR", "Clique aqui para salvar os dados e gerar o PDF"));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Gerar PDF", "GERAR", "Clique aqui para gerar o PDF dos dados salvos"));
	}

	public function RenderizeFormularioPdf(){
		cHTTP::setLang("en_us");
		$html = '<style>
				*{font-family:tahoma;}
				h1{font-size:18pt;}
				table{margin-top:10px;border-collapse:collapse;width:100%;}
				table tr td{border: solid 1px #000;vertical-align:top;color:#000;padding:2px;font-size:12px;}
				table tr.titulo td{background-color:#ccc;font-weight:bold;font-size:16px;text-align:center;}
				span.titulo{font-weight:bold;}
				div.valorEmLista{float:left;min-width:80px;margin-bottom:5px;}
				</style>
				<body style="font-family: Tahoma; font-size: 12pt;">';
		$html.='<div style="width:100%">
			<div style="width:100%;text-align:center;">
						<img  src="/imagens/tvs_cab1.png"/><br/>
						<h1>VISA & PASSPORT REQUEST</h1>
					</div>
					<div style="width:100%;text-align:right;">
						<img  src="/imagens/tvs_cab2.png"/><br/>
					</div>
					';
		$html.='<div style="float:right;"></div>';
		$html.='<table>';
		$html.='<tr class="titulo"><td colspan="3">Traveler Information</td></tr>';
		$html.='<tr>';
			$html.='<td><span class="titulo">First Name/Prenom:</span><br/>'.htmlentities($this->getValorCampo('NO_PRIMEIRO_NOME')).'</td>';
			$html.='<td><span class="titulo">Middle Name:</span><br/>'.htmlentities($this->getValorCampo('NO_NOME_MEIO')).'</td>';
			$html.='<td><span class="titulo">Last Name/Surname:</span><br/>'.htmlentities($this->getValorCampo('NO_ULTIMO_NOME')).'</td>';
		$html.='</tr>';
		$html.='<tr>';
			$html.='<td><span class="titulo">Telephone number:</span><br/>'.htmlentities($this->getValorCampo('NU_TELEFONE_CANDIDATO')).'</td>';
			$html.='<td><span class="titulo">Cell phone number:</span><br/>'.htmlentities($this->getValorCampo('NU_TELEFONE_CANDIDATO_CEL')).'</td>';
			$html.='<td><span class="titulo">E-mail:</span><br/>'.htmlentities($this->getValorCampo('NO_EMAIL_CANDIDATO')).'</td>';
		$html.='</tr>';
		$html.='<tr>';
			$html.='<td><span class="titulo">Gender:</span><br/>'.$this->mCampo['CO_SEXO']->HTMLEmLista().'</td>';
			$html.='<td><span class="titulo">Traveler job title:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_job_title')).'</td>';
			$html.='<td><span class="titulo">Employee ID#:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_employee_id')).'</td>';
		$html.='</tr>';
		$html.='<tr>';
			$html.='<td><span class="titulo">US Citizen:</span><br/>'.$this->mCampo['ftvs_fl_us_citizen']->HTMLEmLista().'</td>';
			$html.='<td><span class="titulo">Nationality:</span><br/>'.$this->getValorCampo('NO_NACIONALIDADE_EM_INGLES').'</td>';
			$html.='<td><span class="titulo">Other nationality:</span><br/>'.$this->getValorCampo('ftvs_tx_other_nationalities').'</td>';
		$html.='</tr>';
		$html.='<tr><td colspan="3"><span class="titulo">US Status:</span></td></tr>';
		$this->mCampo['trrs_id']->mPermiteDefault = false;
		$html.='<tr><td colspan="3"><span class="titulo">Reason of travel:</span>'.$this->mCampo['trrs_id']->HTMLEmLista().'</td></tr>';
		$html.='<tr>';
			$html.='<td colspan="2"><span class="titulo">Type of visa:</span><br/>'.$this->getValorCampo('ftvs_tx_visatype').'</td>';
			$html.='<td><span class="titulo">Are you going offshore:</span><br/>'.$this->mCampo['ftvs_fl_offshore']->HTMLEmLista().'</td>';
		$html.='</tr>';
		$html.='</table>';

		$html.='<table style="">';
		$html.='<tr class="titulo"><td colspan="3">Traveler Arranger / Focal Point Requestor</td></tr>';
		$html.='<tr>';
			$html.='<td colspan="3"><span class="titulo">Name:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_focalpoint')).'</td>';
		$html.='</tr>';
		$html.='<tr>';
			$html.='<td style="width:30%"><span class="titulo">Telephone number:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_focalpoint_tel')).'</td>';
			$html.='<td style="width:30%"><span class="titulo">Cell phone number:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_focalpoint_cel')).'</td>';
			$html.='<td style="width:40%"><span class="titulo">E-mail:</span><br/>'.$this->getValorCampo('ftvs_tx_focalpoint_email').'</td>';
		$html.='</tr>';
		$html.='</table>';

		$html.='<table style="">';
		$html.='<tr class="titulo"><td colspan="3">Foreign Business Information</td></tr>';
		$html.='<tr>';
			$html.='<td style="width:30%"><span class="titulo">Employer:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_foreign_employer')).'</td>';
			$html.='<td style="width:40%"><span class="titulo">Business address:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_foreign_address')).'</td>';
			$html.='<td style="width:30%"><span class="titulo">Telephone number:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_foreign_tel')).'</td>';
		$html.='</tr>';
		$html.='</table>';

		$html.='<table style="">';
		$html.='<tr class="titulo"><td colspan="3">Brazil Business Information</td></tr>';
		$html.='<tr>';
			$html.='<td style="width:30%"><span class="titulo">Name:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_brazil_employer')).'</td>';
			$html.='<td style="width:40%"><span class="titulo">Address:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_brazil_address')).'</td>';
			$html.='<td style="width:30%"><span class="titulo">Telephone number:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_brazil_tel')).'</td>';
		$html.='</tr>';
		$html.='</table>';

		$html.='<table style="">';
		$html.='<tr class="titulo"><td style="width:50%">Visa Request</td><td style="width:50%">Passport Request</td></tr>';
		$html.='<tr>';
		$this->mCampo['entt_id']->mPermiteDefault = false;
		$this->mCampo['past_id']->mPermiteDefault = false;
			$html.='<td style="text-align:center;">'.$this->mCampo['ftvs_fl_request']->HTMLEmLista().'</td>';
			$html.='<td rowspan="3">';

			$combo = $this->mCampo['past_id'];
			$combo->CarregarValores();
			$valores = $combo->valores();
			for ($index = 0; $index < count($valores); $index++) {
				$html.= '<div class="valorEmLista">'.$combo->OpcaoEmLista($combo->mValor, $valores[$index][0], $valores[$index][1]).'</div>';
			}

			$html.='</td>';
		$html.='</tr>';
		$html.='<tr>';
			$html.='<td><span class="titulo">Countries:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_countries')).'</td>';
		$html.='</tr>';
		$html.='<tr>';
			$html.='<td><span class="titulo">Entries:</span><br/>'.$this->mCampo['entt_id']->HTMLEmLista().'</td>';
		$html.='</tr>';
		$html.='<tr>';
			if($this->getValorCampo('ftvs_dt_travel') != '' && $this->getValorCampo('ftvs_dt_travel') != '-'){
				$data = new DateTime($this->getValorCampo('ftvs_dt_travel'));
				$valor = $data->format('m/d/y');
			}
			else{
				$valor = '-';
			}
			$html.='<td><span class="titulo">Travel date (mm/dd/yy):</span>'.$valor.'</td>';
			
			if($this->getValorCampo('ftvs_dt_passport_needed') != '' && $this->getValorCampo('ftvs_dt_passport_needed') != '-'){
				$data = new DateTime($this->getValorCampo('ftvs_dt_passport_needed'));
				$valor = $data->format('m/d/y');
			}
			else{
				$valor = '-';
			}
			$html.='<td><span class="titulo">Date passaport needed (mm/dd/yy):</span>'.$valor.'</td>';
		$html.='</tr>';
		$html.='<tr class="titulo"><td colspan="2">Other Request</td></tr>';
		$html.='<tr>';
			$html.='<td colspan="2">'.htmlentities($this->getValorCampo('ftvs_tx_other_request')).'</td>';
		$html.='</tr>';
		$html.='</table>';

		$html.='<table>';
		$html.='<tr class="titulo"><td>Accounting Information</td></tr>';
		$html.='<tr>';
			$html.='<td><span class="titulo">Category:</span>'.$this->mCampo['accc_id']->HTMLEmLista().'</td>';
		$html.='</tr>';
		$html.='<tr>';
			$html.='<td><span class="titulo">MVOS #:</span>'.$this->getValorCampo('nu_solicitacao').'</td>';
		$html.='</tr>';
		$html.='</table>';

		$html.='<table style="">';
		$html.='<tr class="titulo"><td colspan="3">Document Return Information</td></tr>';
		$html.='<tr>';
			$html.='<td colspan="3"><span class="titulo">Fedex:</span>'.$this->mCampo['ftvs_fl_fedex']->HTMLEmLista().'</td>';
		$html.='</tr>';
		$html.='<tr>';
			$html.='<td colspan="3"><span class="titulo">Street/Address:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_return_address')).'</td>';
		$html.='</tr>';
		$html.='<tr>';
			$html.='<td style="width:40%"><span class="titulo">City/Provence:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_return_city')).'</td>';
			$html.='<td style="width:30%"><span class="titulo">State/Country:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_return_country')).'</td>';
			$html.='<td style="width:30%"><span class="titulo">Postal code:</span><br/>'.htmlentities($this->getValorCampo('ftvs_tx_return_zip')).'</td>';
		$html.='</tr>';
		$html.='</table>';

		$html.='</div></body>';

		return $html;
	}

	public function getValorCampo($pNomeCampo){
		$ret = parent::getValorCampo($pNomeCampo);
		if ($ret == ''){
			$ret = '-';
		}
		return $ret;
	}
}
