<?php
class cTMPL_RESUMO_COBRANCA_INDICAR_RECEBIMENTO extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		// Configura aspectos de apresentacao do template
		$this->mTitulo = "Criar/alterar Resumo de Cobran&ccedil;a";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
		$this->qtd_colunas = 1;
		//
		// Carrega os campos
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'resc_id'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_nu_numero', 'Resumo n&ordm;'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_faturamento', 'Data de Faturamento'));
		$this->mCampo['resc_dt_faturamento']->mReadonly = true;
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Empresa', cCOMBO::cmbEMPRESA));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMBARCACAO_PROJETO', 'Centro de custos', cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'resc_tx_nota_fiscal', 'N&ordm; da nota fiscal'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_recebimento', 'Data de Recebimento'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'resc_dt_criacao', 'Criada em'));
		$this->mCampo['resc_dt_criacao']->mDisabled = true;
		//
		// Carrega as acoes
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar"));
	}
}
