<?php
class cTMPL_CAND_CLIE_EDIT_EXPE extends cTEMPLATE_EDICAO_TWIG{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->set_titulo('New Applicant');
		$this->set_titulo_painel('Actual Work Information');
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('NOME_COMPLETO'));

		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMPRESA_ESTRANGEIRA', 'Empresa no exterior', null ,"Employer's Name"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO_EMPRESA', 'Endereço', null , "Employer's Address"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE_EMPRESA', 'Cidade', null ,"Employer's Address - City"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('CO_PAIS_EMPRESA', 'País', cCOMBO::cmbPAIS ,"Employer's Address - Country"));
		$this->mCampo['CO_PAIS_EMPRESA']->mPermiteDefault = true;
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_EMPRESA', 'Telefone', null ,'Phone #'));

		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_REMUNERACAO_MENSAL', "Salário mensal", null ,"Monthly income"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_REMUNERACAO_MENSAL_BRASIL', "Salário mensal no Brasil", null ,"Monthly salary in Brazil"));

		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('CO_FUNCAO', "Função", cCOMBO::cmbFUNCAO, "Occupation"));
		$this->mCampo['CO_FUNCAO']->mPermiteDefault = true;
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('loca_id', "Local da embarcação/projeto", cCOMBO::cmbLOCAL_PROJETO, "Vessel/ project location"));
		$this->mCampo['loca_id']->mPermiteDefault = true;
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('CO_PROFISSAO_CANDIDATO', 'Profissão', cCOMBO::cmbPROFISSAO ,"Profession"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TE_DESCRICAO_ATIVIDADES', "Descreva as atividades que você pretende desempenhar no Brasil", null, "Provide a description of the activities you expect to carry out in Brazil"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TE_TRABALHO_ANTERIOR_BRASIL', "Você já trabalhou no Brasil anteriormente? Em caso afirmativo, descreva brevemente onde, quando e a duração de sua estada anterior", null ,"Have you worked in Brazil before? If so, state briefly when / place / length of last stay"));

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar & Continuar editando", "SALVAR", "Hit here to save the changes but keep the form open so you can continue later.", "", "", "Save & continue editing" ));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar & Enviar os dados para a Mundivisas", "SALVAR_E_FECHAR", "Hit here to submit the changes and notify Mundivisas you finished", "", "", "Save & submit data to Mundivisas"));
	}
}
