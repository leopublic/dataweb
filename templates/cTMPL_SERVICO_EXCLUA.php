<?php
class cTMPL_SERVICO_EXCLUA extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Exclus&atilde;o de Servi&ccedil;o";
		$this->mMsgInicial = "Confirma a exclusão do serviço abaixo?";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
		$this->qtd_colunas = 1;

		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_SERVICO'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_SERVICO', 'C&oacute;digo'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO', 'Nome completo'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_SERVICO_RESUMIDO', 'Nome resumido'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_AUTORIZACAO', 'Autoriza&ccedil;&atilde;o'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CLASSIFICACAO_VISTO', 'Classifica&ccedil;&atilde;o'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_TIPO_ACOMPANHAMENTO', 'Tela de acompanhamento'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'serv_fl_candidato_obrigatorio', 'É obrigado a ter candidatos?'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Sim", "SIM", "Clique para excluir"));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Não", "NAO", "Clique para cancelar essa exclusão"));
	}
}
