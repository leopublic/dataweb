<?php
class cTMPL_FUNC_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mlocalAcoes = self::lca_RODAPE;
		$this->qtd_colunas = 1;
		// Adiciona campos da tela
		$this->set_titulo('Inclusão/Alteração de Função/Cargo');
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'CO_FUNCAO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_FUNCAO', 'Nome da função'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_FUNCAO_EM_INGLES', 'Nome da função em inglês'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCODIGO, 'NU_CBO', 'CBO'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TX_DESCRICAO_ATIVIDADES', 'Descrição atividades'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_FUNCAO_ALTERNATIVO', 'Nome alternativo para a função'));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar","SALVAR", "Clique para editar" ));
	}
}
