<?php
class cTMPL_CAND_CLIE_EDIT_INFO extends cTEMPLATE_EDICAO_TWIG{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->set_titulo('New Applicant');
		$this->set_titulo_painel('Personal Information');
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('nu_candidato_tmp'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('NOME_COMPLETO'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PRIMEIRO_NOME', 'Primeiro nome', null, 'First name'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NOME_MEIO', 'Nome do meio', null ,'Middle name'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ULTIMO_NOME', 'Último nome', null ,'Last name'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMAIL_CANDIDATO', 'E-mail de contato', null ,'Contact email'));

		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO_RESIDENCIA', 'Endereço', null ,'Home Address'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE_RESIDENCIA', 'Cidade', null ,'Home Address - City'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('cand_tx_residencia_estado', 'Estado residência', 'Home address - State'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('NO_PAIS_RESIDENCIA_EM_INGLES', 'País residência', 'Home address - Country'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_CANDIDATO', 'Telefone', null ,'Phone #'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NACIONALIDADE_EM_INGLES', "Nacionalidade da mãe", null ,"Nationality"));

		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PAI', "Nome do pai", null ,"Father's full name"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('co_pais_nacionalidade_pai', "Nacionalidade do pai", cCOMBO::cmbPAIS ,"Father's nationality"));
		$this->mCampo['co_pais_nacionalidade_pai']->mPermiteDefault = true;
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_MAE', "Nome da mãe", null ,"Mother's full name"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('co_pais_nacionalidade_mae', "Nacionalidade da mãe", cCOMBO::cmbPAIS ,"Mother's nationality"));
		$this->mCampo['co_pais_nacionalidade_mae']->mPermiteDefault = true;

		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESTADO_CIVIL_EM_INGLES', "Estado civil", null ,"Marital status"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('CO_SEXO', "Sexo", cCOMBO::cmbSEXO ,"Sex"));

		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_NASCIMENTO_CLIE', "Data de nascimento", null ,"Date of birth"));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_LOCAL_NASCIMENTO', "Local de nascimento", null ,"Place of birth"));

		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ESCOLARIDADE_EM_INGLES', "Escolaridade", null ,"Highest level of education"));

		$this->AdicionePainel("Consulado", "Consulate Information");
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('NO_REPARTICAO_CONSULAR', "Consulado onde o visto será coletado", "Consulate where the visa will be collected"));

	}
}
