<?php
class cTMPL_PROCESSO_EMISCIE_CLIE_EDIT extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nu_cie', 'Número RNE'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_emissao', 'Data de expedição'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'dt_validade', 'Validade da CIE'));
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::NovoCombo('CO_CLASSIFICACAO_VISTO', 'Classificação', cCOMBO::cmbCLASSIFICA_VISTO));		
	}
}
