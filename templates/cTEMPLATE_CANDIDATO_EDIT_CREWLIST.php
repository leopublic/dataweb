<?php
class cTEMPLATE_CANDIDATO_EDIT_CREWLIST extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneCampo(new cCAMPO_HIDDEN('NOME_COMPLETO'));
		$this->AdicioneCampo(new cCAMPO_HIDDEN('NU_EMPRESA'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('NU_ORDEM_CREWLIST', 'Ordem'));
		$this->AdicioneCampo(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMBARCACAO_PROJETO', 'Embarcação/projeto', cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->AdicioneCampo(new cCAMPO_SIMNAO('FL_EMBARCADO', 'Embarcado', cCOMBO::cmbEMBARCADO));
		$this->AdicioneCampo(new cCAMPO_SIMNAO('FL_INATIVO', 'Inativo'));
		$this->AdicioneCampo(new cCAMPO_MEMO('TE_OBSERVACAO_CREWLIST', 'Observações (Crew List)'));
		$this->mCampo['TE_OBSERVACAO_CREWLIST']->mLarguraDupla = true;
		$this->AdicioneCampo(new cCAMPO_MEMO('observacao_visto', 'Observações (Visto)'));
		$this->mCampo['observacao_visto']->mLarguraDupla = true;
		$this->mCampo[cTEMPLATE::CMP_ALTURA]->mValor = 320;
		$this->mCampo[cTEMPLATE::CMP_TITULO]->mValor = 'Alterar dados do Candidato';
		$this->AdicioneAcao(new cACAO_METODO_POPUP_POST("Salvar","SALVAR", "Clique para salvar" ));
	}
}
