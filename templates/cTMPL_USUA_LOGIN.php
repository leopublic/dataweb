<?php
class cTMPL_USUA_LOGIN extends cTEMPLATE_EDICAO_TWIG{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampo(new cCAMPO_TEXTO('login', 'Usuário', 'Username'));
		$this->AdicioneCampo(new cCAMPO_TEXTO('nm_senha', 'Senha', 'Password'));

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Login", "SALVAR", "", "", "", "" ));
	}
}
