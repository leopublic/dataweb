<?php
class cTMPL_PROCESSO_EDIT_OS_CNH extends cTMPL_PROCESSO_EDIT_OS{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoProtegido(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NOME_COMPLETO', 'Candidato'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CNH', 'Número CNH'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'DT_EXPIRACAO_CNH', 'Data expiração CNH'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'de_observacao', 'Observação'));
		$this->mCampo['de_observacao']->mLarguraDupla = true;
		
		$this->AdicionarCamposCobranca();
	}
	
	public function CustomizeCabecalho() {
		parent::CustomizeCabecalho();
		$this->mTitulo = $this->os->mNO_SERVICO_RESUMIDO;
	}
}
