<?php

/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_USUARIO_EDITE extends cTEMPLATE_EDICAO {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->mTitulo = "Alterar usu&aacute;rio";
        $this->mMsgInicial = "Atenção: A senha só será alterada se uma nova senha for informada.";
        $this->qtd_colunas = 1;
        // Adiciona campos da tela
        $this->AdicioneCampoChave(new cCAMPO_HIDDEN('cd_usuario'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nome', 'Nome'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'usua_tx_apelido', 'Apelido'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'login', 'Login'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'nm_email', 'Email'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'flagativo', 'Ativo?'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSENHA, 'nm_senha', 'Senha'));
        $this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSENHA, 'nm_senha_rep', '(repita a senha)'));
//        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('cd_perfil', 'Perfil', cCOMBO::cmbPERFIL));
        $this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('cd_empresa', 'Empresa (cliente)', cCOMBO::cmbEMPRESA));
        // Adiciona as ações que estarão disponíveis na tela
        $this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Salvar", "SALVAR", "Clique para salvar as alterações", "", "", "Submit"));
    }

    public function CustomizeCabecalho() {
        if (intval($this->getValorCampo('cd_usuario')) == '0') {
            $this->set_titulo('Criar novo usuário');
            $this->EscondaCampo('flagativo');
        } else {
            $this->set_titulo('Alterar usuário');
            $this->MostreCampo('flagativo');
        }
    }
    public function RenderizeRodape(){
        // Zerado para não gerar o salvar antes dos perfis
    }
    
    public function FechaTabela(){
        return '';
    }

}
