<?php
class cTMPL_CAND_REVISAR_OS extends cTEMPLATE_EDICAO{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->enveloparEmConteudoInterno  = false;
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_CANDIDATO'));
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'id_solicita_visto'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_EMBARCACAO_PROJETO_COBRANCA'));

		$this->AdicionePainel('Cadastro da OS');
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_REPARTICAO_CONSULAR', 'Repartição consular', cCOMBO::cmbREPARTICAO_CONSULAR));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_JUSTIFICATIVA_REP_CONS', 'Justificativa da repartição consular'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCHAVE_ESTR,'CO_FUNCAO_CANDIDATO', 'Função', cCOMBO::cmbFUNCAO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_RENUMERACAO_MENSAL', 'Salário no exterior em R$'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'VA_REMUNERACAO_MENSAL_BRASIL', 'Salário no Brasil em R$'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'DS_LOCAL_TRABALHO', 'Local de trabalho'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TE_DESCRICAO_ATIVIDADES', 'Descrição das atividades'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'DS_JUSTIFICATIVA', 'Justificativa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'DS_SALARIO_EXTERIOR', 'Declaração de remunera&ccedil;&atilde;o'));

		$this->AdicionePainel('Situação cadastral');
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', 'Empresa', cCOMBO::cmbEMPRESA));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', 'Embarca&ccedil;&atilde;o/Projeto', cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('empt_tx_descricao', 'Empresa terceirizada', cCOMBO::cmbEMPRESA_TERCEIRIZADA, "", true));
		$this->VinculeCombos('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'FL_INATIVO', 'Inativo'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'FL_EMBARCADO', 'Embarcado'));
		$this->AdicionePainel('Dados pessoais');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PRIMEIRO_NOME', 'Primeiro nome'));
		$this->mCampo['NO_PRIMEIRO_NOME']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_NOME_MEIO', 'Nome do meio'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ULTIMO_NOME', 'Último nome'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CPF', 'CPF'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMAIL_CANDIDATO', 'Email'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PROFISSAO_CANDIDATO', 'Profissão', cCOMBO::cmbPROFISSAO));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_PAI', 'Nome do pai'));
		$this->mCampo['NO_PAI']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('co_pais_nacionalidade_pai', 'Nacionalidade pai', cCOMBO::cmbNACIONALIDADE));
		$this->mCampo['co_pais_nacionalidade_pai']->mPermiteDefault = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_MAE', 'Nome da mãe'));
		$this->mCampo['NO_MAE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('co_pais_nacionalidade_mae', 'Nacionalidade mãe', cCOMBO::cmbNACIONALIDADE));
		$this->mCampo['co_pais_nacionalidade_mae']->mPermiteDefault = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_ESTADO_CIVIL', 'Estado Civil', cCOMBO::cmbESTADO_CIVIL));
		$this->mCampo['CO_ESTADO_CIVIL']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_SEXO', 'Sexo', cCOMBO::cmbSEXO));
		$this->mCampo['Sexo']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'DT_NASCIMENTO', 'Data de nascimento'));
		$this->mCampo['DT_NASCIMENTO']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_LOCAL_NASCIMENTO', 'Local de nascimento'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_NACIONALIDADE', 'Nacionalidade', cCOMBO::cmbNACIONALIDADE));
		$this->mCampo['CO_NACIONALIDADE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_NIVEL_ESCOLARIDADE', 'Escolaridade', cCOMBO::cmbESCOLARIDADE));
		$this->mCampo['CO_NIVEL_ESCOLARIDADE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, 'TE_TRABALHO_ANTERIOR_BRASIL', 'Experiência profissional'));
		$this->AdicionePainel('Endereço no Brasil');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO_RESIDENCIA', 'Endereço'));
		$this->mCampo['NO_ENDERECO_RESIDENCIA']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE_RESIDENCIA', 'Cidade'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_CANDIDATO', 'Telefone'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_CANDIDATO_CEL', 'Celular'));
		$this->AdicionePainel('Endereço no exterior');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_EMPRESA_ESTRANGEIRA', 'Nome da empresa no exterior'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO_EMPRESA', 'Endereço da empresa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE_EMPRESA', 'Cidade da empresa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_EMPRESA', 'País da empresa', cCOMBO::cmbPAIS));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_EMPRESA', 'Telefone da empresa'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ENDERECO_ESTRANGEIRO', 'Endereço do candidato'));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_CIDADE_ESTRANGEIRO', 'Cidade do candidato'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_ESTRANGEIRO', 'País do candidato', cCOMBO::cmbPAIS));
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_TELEFONE_ESTRANGEIRO', 'Telefone do candidato'));
		$this->AdicionePainel('Dados do passaporte');
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_PASSAPORTE', 'Número do passaporte'));
		$this->mCampo['NU_PASSAPORTE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'DT_EMISSAO_PASSAPORTE', 'Data de emissão'));
		$this->mCampo['DT_EMISSAO_PASSAPORTE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,'DT_VALIDADE_PASSAPORTE', 'Data de validade'));
		$this->mCampo['DT_VALIDADE_PASSAPORTE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('CO_PAIS_EMISSOR_PASSAPORTE', 'País emissor', cCOMBO::cmbPAIS));
		$this->mCampo['CO_PAIS_EMISSOR_PASSAPORTE']->mObrigatorio = true;
		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_RNE', 'RNE'));
		$this->AdicionePainel('Cadastro');

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Revisar", "REVISAR", "Clique para salvar"));
		$this->AdicioneAcao(new cACAO_LINK("Formulários", "FORMS", "Clique para acessar os formulários da OS", ""));
	}

	public function CustomizeCabecalho() {
		$this->mAcoes['FORMS']->mLink = '/operador/os_Formularios.php?&ID_SOLICITA_VISTO='.$this->getValorCampo('id_solicita_visto').'&id_solicita_visto='.$this->getValorCampo('id_solicita_visto').'&NU_CANDIDATO='.$this->getValorCampo('NU_CANDIDATO');

		$cand = new cCANDIDATO();
		$cand->mNU_CANDIDATO = $this->getValorCampo('NU_CANDIDATO');
		$os = new cORDEMSERVICO();
		$os->Recuperar($this->getValorCampo('id_solicita_visto'));

		if($os->mNU_EMBARCACAO_PROJETO_COBRANCA > 0){
			$this->setValorCampo('NU_EMBARCACAO_PROJETO', $os->NU_EMBARCACAO_PROJETO_COBRANCA);
		}

		if(!$cand->get_RevisadoNaOs($this->getValorCampo('id_solicita_visto'))){
			$this->mAcoes['REVISAR']->mLabel = 'Revisar OS '.$os->mNU_SOLICITACAO;
		}
		else{
			$this->mAcoes['REVISAR']->mLabel = 'Salvar';
			if($os->mID_STATUS_SOL < 6){
			}
			else{
				$this->ProtejaTodosCampos();
				$this->DesprotejaCampo('CO_FUNCAO_CANDIDATO');
				$this->mCampo['CO_FUNCAO_CANDIDATO']->mDisabled = true;
				$this->DesprotejaCampo('CO_REPARTICAO_CONSULAR');
				$this->mCampo['CO_REPARTICAO_CONSULAR']->mDisabled = true;

				$this->DesprotejaCampo('NU_EMPRESA');
				$this->mCampo['NU_EMPRESA']->mDisabled = true;
				$this->DesprotejaCampo('NU_EMBARCACAO_PROJETO');
				$this->mCampo['NU_EMBARCACAO_PROJETO']->mDisabled = true;
				$this->DesprotejaCampo('CO_PROFISSAO_CANDIDATO');
				$this->mCampo['CO_PROFISSAO_CANDIDATO']->mDisabled = true;
				$this->DesprotejaCampo('co_pais_nacionalidade_pai');
				$this->mCampo['co_pais_nacionalidade_pai']->mDisabled = true;
				$this->DesprotejaCampo('co_pais_nacionalidade_mae');
				$this->mCampo['co_pais_nacionalidade_mae']->mDisabled = true;
				$this->DesprotejaCampo('CO_ESTADO_CIVIL');
				$this->mCampo['CO_ESTADO_CIVIL']->mDisabled = true;
				$this->DesprotejaCampo('CO_NACIONALIDADE');
				$this->mCampo['CO_NACIONALIDADE']->mDisabled = true;
				$this->DesprotejaCampo('CO_NIVEL_ESCOLARIDADE');
				$this->mCampo['CO_NIVEL_ESCOLARIDADE']->mDisabled = true;
				$this->DesprotejaCampo('CO_PAIS_EMPRESA');
				$this->mCampo['CO_PAIS_EMPRESA']->mDisabled = true;
				$this->DesprotejaCampo('CO_PAIS_ESTRANGEIRO');
				$this->mCampo['CO_PAIS_ESTRANGEIRO']->mDisabled = true;
				$this->DesprotejaCampo('CO_PAIS_EMISSOR_PASSAPORTE');
				$this->mCampo['CO_PAIS_EMISSOR_PASSAPORTE']->mDisabled = true;
			}
		}
	}
}
