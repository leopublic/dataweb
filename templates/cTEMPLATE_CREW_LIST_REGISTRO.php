<?php
class cTEMPLATE_CREW_LIST_REGISTRO extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Crew List";
		$this->mCols = array("70px", "30px", "200px", "50px", "50px", "60px", "170px", "100px", "170px", "80px", "auto", "auto" );
		$this->mOrdenacaoDefault = 'NOME_COMPLETO';
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneColuna(new cCAMPO_HIDDEN('codigo_mte'));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_ORDEM_CREWLIST', '#', "#" ));
		$this->mCampo['NU_ORDEM_CREWLIST']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarcação/Projeto', "Project/Ship" ));
		$this->mCampo['NO_EMBARCACAO_PROJETO']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Nome', "Crew name" ));
		$this->mCampo['NOME_COMPLETO']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_SIMNAO('FL_INATIVO', 'Inativo', 'Inactive'));
		$this->mCampo['FL_INATIVO']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_SIMNAO_LITERAL('FL_EMBARCADO', 'Embarc.', 'On board'));
		$this->mCampo['FL_EMBARCADO']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PASSAPORTE', 'Passaporte', 'Pspt NR'));
		$this->mCampo['NU_PASSAPORTE']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('nu_processo_mte', 'Processo MTE', 'MTE Process'));
		$this->mCampo['nu_processo_mte']->mClasse = 'cen';
		$this->mCampo['nu_processo_mte']->mlinkMte = true;
		$this->AdicioneColuna(new cCAMPO_TEXTO('validade_visto', 'Validade visto', 'Expiry date'));
		$this->mCampo['validade_visto']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('nu_protocolo_reg', 'Registro', 'Registration'));
		$this->mCampo['nu_processo_regcie']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('dt_requerimento_reg_fmt', 'Data do registro', 'Date of register'));
		$this->mCampo['dt_requerimento_reg_fmt']->mClasse = 'cen';
		$this->AdicioneColuna(new cCAMPO_TEXTO('TE_OBSERVACAO_CREWLIST', 'Observações (crew list)', 'Observations'));
		$this->mCampo['TE_OBSERVACAO_CREWLIST']->mClasse = 'esq';
		$this->AdicioneColuna(new cCAMPO_TEXTO('observacao_visto', 'Observações (visto)', 'Observations'));
		$this->mCampo['observacao_visto']->mClasse = 'esq';

		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMPRESA', "Empresa", cCOMBO::cmbEMPRESA));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('NU_EMBARCACAO_PROJETO', "Embarc./proj.", cCOMBO::cmbEMBARCACAO_PROJETO));
		$this->AdicioneFiltro(new cCAMPO_TEXTO('NOME_COMPLETO', "Nome cand."));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('TIPO_CANDIDATO', "Tipo cand.", cCOMBO::cmbTIPO_CANDIDATO));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('FL_EMBARCADO', "Embarcado", cCOMBO::cmbEMBARCADO));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('FL_INATIVO', "Cadastro", cCOMBO::cmbSITUACAO_CADASTRAL_CANDIDATO));
		$this->mFiltro['NOME_COMPLETO']->mTipoComparacaoFiltro = cFILTRO::tpSEL_LIKE_INI;
		
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Renumerar ordem", "RENUMERAR", "Clique para renumerar a ordem dos candidatos"));
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Exportar para excel", "GERAR_EXCEL", "Clique para gerar essa consulta em Excel"));
		$this->AdicioneAcaoLinha(new cACAO_METODO_POPUP("/imagens/grey16/Write2.png","ALTERAR", "cCTRL_CANDIDATO", "EditarCrewList",  "Clique para editar os dados" ));
		$this->AdicioneAcaoLinha(new cACAO_METODO_POPUP("/imagens/grey16/Clock.png","EXIBIR_VISTO", "cCTRL_CANDIDATO", "ExibirVisto",  "Clique para ver o histórico detalhado do visto deste candidato" ));
//		$this->AdicioneAcaoLinha(new cACAO_METODO_POPUP("/imagens/grey16/Discuss.png","ADICIONAR_OBS", "cCTRL_CANDIDATO", "ComplementarObservacao",  "Clique para complementar as observações do visto do candidato" ));
	}
}
