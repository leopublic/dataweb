<?php
/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cTMPL_ORDEMSERVICO_ARQUIVOS_LISTE extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Arquivos";
		$this->mMsgInicial = "";
	
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('ID_SOLICITA_VISTO'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_SEQUENCIAL'));

		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'nu_solicitacao'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::NovoCombo( 'NU_TIPO_ARQUIVO', 'Tipo', cCOMBO::cmbTIPO_ARQUIVO));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'CO_TIPO_ARQUIVO', 'Tipo'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_ARQ_ORIGINAL', 'Arquivo'));
		$this->AdicioneCampoInvisivel(cFABRICA_CAMPO::Novo(cCAMPO::cpFILE, 'arquivo', 'Arquivo'));
		$this->AdicioneCampo(cFABRICA_CAMPO::NovoCombo('NU_CANDIDATO', 'Candidato', cCOMBO::cmbCANDIDATO_OS));
		// Adiciona as ações que estarão disponíveis na tela
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Salvar alterações", "SALVAR", "Clique para salvar as alterações nos candidatos", "cCTRL_ORDEMSERVICO", "ListeArquivos"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("ADICIONAR", "ENVIAR_ARQUIVO", "Clique para adicionar esse arquivo", "cCTRL_ORDEMSERVICO", "AdicionarArquivo"));
		$this->mAcoesLinha['ENVIAR_ARQUIVO']->mVisivel = false;
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Download.png", "DOWNLOAD", "Clique para baixar esse arquivo", "cCTRL_ORDEMSERVICO", "BaixarArquivo"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Trash.png", "EXCLUIR_ARQUIVO", "Clique para excluir esse arquivo", "cCTRL_ORDEMSERVICO", "ListeArquivos", "Confirma a exclusão desse arquivo?", "Confirmar exclusão"));
	}
}
