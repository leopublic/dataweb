<?php
class cTEMPLATE_PROCESSO_MTE_VIEW extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('codigo'));
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('id_solicita_visto'));
		$this->AdicioneCampoInvisivel(new cCAMPO_HIDDEN('cd_candidato'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NU_SOLICITACAO', 'Número'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('NO_RAZAO_SOCIAL', 'Empresa'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarca&ccedil;&atilde;o/Projeto'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('NO_SERVICO_AUTORIZACAO', 'Tipo de visto'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('nu_processo', 'N&uacute;mero do processo'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('dt_requerimento', 'Data requerimento'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('NO_FUNCAO', 'Fun&ccedil;&atilde;o'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('NO_REPARTICAO_CONSULAR', 'Reparti&ccedil;&atilde;o'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('nu_oficio', 'N&uacute;mero MRE'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('dt_deferimento', 'Data deferimento MTE'));
		$this->AdicioneCampoProtegido(new cCAMPO_TEXTO('prazo_solicitado', 'Prazo solicitado'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NO_USUARIO_TECNICO', 'Responsável'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NO_USUARIO_CADASTRO', 'Usuário cadastro'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('dt_cadastro', 'Data cadastro'));
		$this->AdicioneCampoInvisivel(new cCAMPO_SIMNAO('fl_visto_atual', 'É o processo atual?'));
		$this->AdicioneCampoDuplo(new cCAMPO_TEXTO('observacao', 'Observação'));
		$this->mCampo['observacao']->mReadonly = true; 
		$this->AdicioneCampoInvisivel(new cCAMPO_SIMNAO('fl_nao_conformidade', 'Não conformidade?'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NU_SERVICO_SV', ''));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NU_EMPRESA', 'NU_EMPRESA'));
		$this->AdicioneCampoInvisivel(new cCAMPO_DATA('DT_SITUACAO_SOL', 'DT_SITUACAO_SOL'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('cd_admin_cad', 'cd_admin_cad'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('ID_STATUS_SOL', 'ID_STATUS_SOL'));
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NU_FORMULARIOS', 'Quantidade de formularios'));
		$this->m = '300';


		$acao = new cACAO_METODO_POPUP("Alterar", "ALTERAR", "cINTERFACE_PROCESSO_MTE", "Editar",  "Alterar os dados do visto");
		$this->AdicioneAcao($acao) ;
		$acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Coleta", "ADICIONAR_COLETA", "Adicionar uma coleta a esse visto", "");
		$acao->mOnClick = "carregaFormPopup('codigo=0&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=coleta&modo=edit');";
		$this->AdicioneAcao($acao) ;
		$acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Registro", "ADICIONAR_REGISTRO", "Adicionar um registro", "");
		$acao->mOnClick = "carregaFormPopup('codigo=0&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=regcie&modo=edit');";
		$this->AdicioneAcao($acao) ;
		$acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Emissão", "ADICIONAR_EMISSAO", "Adicionar uma emissão de CIE", "");
		$acao->mOnClick = "carregaFormPopup('codigo=0&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=emiscie&modo=edit');";
		$this->AdicioneAcao($acao) ;
		$acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Prorrogação", "ADICIONAR_PRORROGACAO", "Adicionar uma prorrogação", "");
		$acao->mOnClick = "carregaFormPopup('codigo=0&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=prorrog&modo=edit');";
		$this->AdicioneAcao($acao) ;
		$acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "+ Cancelamento", "ADICIONAR_CANCELAMENTO", "Adicionar um cancelamento", "");
		$acao->mOnClick = "carregaFormPopup('codigo=0&codigo_processo_mte=" . $pTela->mCampoNome['codigo']->mValor . "&tipo=cancel&modo=edit');";
		$this->AdicioneAcao($acao) ;
		$acao = new cACAO(cACAO::ACAO_FORM, "Tornar ATUAL", "TORNAR_ATUAL", "Tornar esse visto o visto atual do candidato", "Deseja mesmo tornar esse processo o visto mais atual do candidato?", "Modificação de visto atual");
		$this->AdicioneAcao($acao) ;
		$acao = new cACAO(cACAO::ACAO_JAVASCRIPT, "Migrar", "MIGRAR_CAND", "Migrar esse processo para outro candidato", "");
		$acao->mOnClick = "CarregueAjax('cINTERFACE_CANDIDATO', 'MigrarProcessoAjax', 'NU_CANDIDATO_ORIGEM=".$pTela->mCampoNome['cd_candidato']->mValor ."&codigo=" . $pTela->mCampoNome['codigo']->mValor . "&tabela=processo_mte', 'formPopup');";
		$this->AdicioneAcao($acao) ;
		$acao = new cACAO(cACAO::ACAO_FORM, "Excluir", "EXCLUIR", "Excluir o visto", "Deseja mesmo excluir esse processo? Os processos associados ficarão órfãos. Essa operação não pode ser desfeita", "Exclusão de processo MTE");
		$this->AdicioneAcao($acao) ;

	}
}
