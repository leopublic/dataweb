<?php
class cTEMPLATE_CANDIDATO_LIST_NECESSITA_PRORROGACAO extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "Controle de necessidades de prorrogação";
		$this->mCols = array("55px", "45px", "50px", "auto", "60px", "160px", "55px", "120px", "55px",  "160px","55px", "55px", "auto" );
		$this->mOrdenacaoDefault = 'NOME_COMPLETO';
//		$this->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor = 'NO_EMBARCACAO_PROJETO';
		// Adiciona campos da tela
		$this->AdicioneCampoChave(new cCAMPO_HIDDEN('NU_CANDIDATO'));
		$this->AdicioneColuna(new cCAMPO_HIDDEN('codigo_mte'));
		$this->AdicioneColuna(new cCAMPO_SIMNAO('FL_INATIVO', 'Inativo', 'Inactive'), "cen");
		$this->AdicioneColuna(new cCAMPO_SIMNAO_LITERAL('FL_EMBARCADO', 'Embarc.', 'On board'), "cen");
		$this->AdicioneColuna(new cCAMPO_TEXTO('NO_EMBARCACAO_PROJETO', 'Embarcação/Projeto', "Project/Ship" ));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NOME_COMPLETO', 'Nome', "Crew name" ));
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PASSAPORTE', 'Passaporte', 'Pspt NR'), "cen");
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PROCESSO_MTE', 'Processo MTE', 'MTE Process'), "cen");
		$this->mCampo['nu_processo_mte']->mlinkMte = true;
		$this->AdicioneColuna(new cCAMPO_TEXTO('validade_visto', 'Validade visto', 'Expiry date'), "cen");
		$this->mCampo['validade_visto']->mOrdenavel = false;
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PROTOCOLO_REG', 'Registro', 'Registration'), "cen");
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_REQUERIMENTO_CIE', 'Data do registro', 'Date of register'), "cen");
		$this->AdicioneColuna(new cCAMPO_TEXTO('NU_PROTOCOLO_PRO', 'Prorrogação', 'Prorrogation'), "cen");
		$this->mCampo['nu_protocolo_pro']->mlinkMJ = true;
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_REQUERIMENTO_PRO', 'Req. prorr.', 'Req. prorr.'), "cen");
		$this->AdicioneColuna(new cCAMPO_TEXTO('DT_PRAZO_PRET_PRO', 'Prazo pret.', 'Prazo pret.'), "cen");
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('DT_DEFERIMENTO', 'DT_DEFERIMENTO', 'DT_DEFERIMENTO'), "cen");
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NO_STATUS_CONCLUSAO_MTE', 'NO_STATUS_CONCLUSAO_MTE', 'NO_STATUS_CONCLUSAO_MTE'), "cen");
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('ID_STATUS_CONCLUSAO_MTE', 'ID_STATUS_CONCLUSAO_MTE', 'ID_STATUS_CONCLUSAO_MTE'), "cen");
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('NO_STATUS_CONCLUSAO_PRO', 'NO_STATUS_CONCLUSAO_PRO', 'NO_STATUS_CONCLUSAO_PRO'), "cen");
		$this->AdicioneCampoInvisivel(new cCAMPO_TEXTO('ID_STATUS_CONCLUSAO_PRO', 'ID_STATUS_CONCLUSAO_PRO', 'ID_STATUS_CONCLUSAO_PRO'), "cen");
//		$this->AdicioneColuna(new cCAMPO_TEXTO('dt_prazo_pret_diff', 'dias prorrog'), "cen");
//		$this->AdicioneColuna(new cCAMPO_TEXTO('dt_prazo_estada_diff', 'dias reg.'), "cen");
//		$this->AdicioneColuna(new cCAMPO_TEXTO('dias_limite', 'dias limite'), "cen");
		$this->AdicioneColuna(new cCAMPO_TEXTO('observacao_visto', 'Observações (visto)', 'Observations'));

		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMPRESA', "Empresa", cCOMBO::cmbEMPRESA));
		$this->AdicioneFiltro(cFABRICA_CAMPO::NovoCombo('NU_EMBARCACAO_PROJETO', "Embarc./proj.", cCOMBO::cmbEMBARCACAO_PROJETO));

        $this->VinculeFiltros('NU_EMBARCACAO_PROJETO', 'NU_EMPRESA');


        $this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('cd_usuario', "Ponto focal", cCOMBO::cmbPONTO_FOCAL_ATIVO));
		$this->AdicioneFiltro(new cCAMPO_TEXTO('NOME_COMPLETO', "Nome cand."));
		$this->mFiltro['NOME_COMPLETO']->mTipoComparacaoFiltro = cFILTRO::tpSEL_LIKE_INI;
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('FL_EMBARCADO', "Embarcado", cCOMBO::cmbEMBARCADO));
		$this->mFiltro['FL_EMBARCADO']->mCombo->mValorDefault = "(todos)";
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('FL_INATIVO', "Cadastro", cCOMBO::cmbSITUACAO_CADASTRAL_CANDIDATO));
		$this->mFiltro['FL_INATIVO']->mCombo->mValorDefault = "(todos)";
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('MESES', "Expirado até", cCOMBO::cmbPROXIMOS_18_MESES));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('TIPO_CANDIDATO', "Tipo cand.", cCOMBO::cmbTIPO_CANDIDATO));
		$this->AdicioneFiltro(new cCAMPO_CHAVE_ESTRANGEIRA('SITUACAO_VISTO', "Sit. do visto", cCOMBO::cmbSITUACAO_VISTO));

		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("Exportar para excel","GERAR_EXCEL", "Clique para gerar essa consulta em Excel" ));
		$this->AdicioneAcaoLinha(new cACAO_METODO_POPUP("/imagens/grey16/Write2.png","ALTERAR", "cCTRL_CANDIDATO", "EditarCrewList",  "Clique para editar os dados" ));
		$this->AdicioneAcaoLinha(new cACAO_METODO_POPUP("/imagens/grey16/Clock.png","EXIBIR_VISTO", "cCTRL_CANDIDATO", "ExibirVisto",  "Clique para ver o histórico detalhado do visto deste candidato" ));
	}

}
