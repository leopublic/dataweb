<?php
class cTMPL_EMPRESA_LISTAR extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "EMPRESA";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->mCols = array("40px", "auto", "180px", "100px", "100px");
			
		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, 'NU_EMPRESA'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NO_RAZAO_SOCIAL', 'Razão Social'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, 'NU_CNPJ', 'CNPJ'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, 'DT_CADASTRAMENTO', 'Cadastrada em'));
		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, 'FL_ATIVA', 'Ativa?'));
		
		$this->AdicioneAcao(new cACAO_LINK_CONTROLER_METODO("Adicionar nova", "ADICIONAR", "Clique para adicionar um novo", "cCTRL_EMPRESA", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Edit.png", "ALTERAR", "Clique para alterar esse item", "cCTRL_EMPRESA", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_CONTROLER_METODO("/imagens/grey16/Trash.png", "EXCLUIR", "Clique para excluir esse item", "cCTRL_EMPRESA", "Exclua"));
	}
}
