<?php
$opcao="CON";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
echo Topo($opcao);
echo Menu($opcao);
?>
<br>
 <table border=0 width="600">
  <tr>
   <td class="textobasico" align="center" valign="top">
<?php
if($lang=="E") {
?>
  <p align="center" class="textoazul"><strong>:: Welcome Mundivisas ::</strong></p>
  <p align="justify" class="textoazul">
  Mundivisas is focused on the provision and administration of consultancy services for Immigration in Brazil, 
  developing a personalized service to each of its clients.
  <br>
  Our main activities are aimed to serve the foreigners (individuals or company employees), 
  guiding  and accompanying them throughout the various processes, from obtaining a visa  
  to entering and remaining in the country.
<?php
} else {
?>
  <p align="center" class="textoazul"><strong>:: Bem vindo a Extranet da Mundivisas ::</strong></p>
  <p align="justify" class="textoazul">
  A Mundivisas tem como foco prestar serviços administrativos e consultoria na área de imigração no Brasil, 
  desenvolvendo um atendimento personalizado a todos os seus clientes . 
  <br>
  Nossas atividades estão voltadas 
  para atender os estrangeiros, pessoas físicas e jurídicas, orientando e acompanhando todo tramite necessário 
  (procedimentos e obrigações), desde a obtenção do visto consular à entrada e permanência no país.
<?php
}
?>


    </td>
   </tr>
  </table>

<?php
echo Rodape($opcao);
?>
