<?
session_start();
header('Content-Type: text/html; charset=ISO-8859-1');
include ("../LIB/autenticacao.php");
include ("../LIB/geral.php");

cHTTP::$comMenu = false;
$controller = new cCTRL_EDIT_IN_PLACE();
$controller->Salvar($_POST['id'], $_POST['value']);
if(cHTTP::$SESSION['msg'] != ''){
	$tipo_msg = '' ;
	if(cHTTP::$SESSION['tipo_msg'] != ''){
		$tipo_msg = cHTTP::$SESSION['tipo_msg'];
	}
	else{
		$tipo_msg = cNOTIFICACAO::TM_ALERT;
	}

	print '<div id="msg" class="ui-state-'.$tipo_msg.'">'.cHTTP::$SESSION['msg'].'</div>';
}
unset(cHTTP::$SESSION['msg']);
unset(cHTTP::$SESSION['tipo_msg']);
unset(cHTTP::$SESSION['msgTitulo']);
