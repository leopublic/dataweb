<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

cHTTP::$comMenu = true;

$nomeController = cHTTP::$controller;
$metodo = cHTTP::$metodo;
if(method_exists($nomeController, "ExtendeModelo"))
{
	$controller = new $nomeController();
	$conteudo = $controller->$metodo();
}
else
{
	$conteudo = $nomeController::$metodo();
}

//eval ('$conteudo = '.cHTTP::$controller.'::'.cHTTP::$metodo.'();');

if(cHTTP::RedirecionarSeSolicitado())
{
	exit();
}
else
{
	if (cHTTP::$MIME == cHTTP::mmXLS)
	{
		cLAYOUT::Head_ContentType('excel');
		echo '
		   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
			"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		   <html><head>
		   <style rel="stylesheet" type="text/css"/>'.file_get_contents('../estilos.css').'
				table.grid {font:10px verdana;color:#000}
				table.grid thead tr th {border:solid 1px #000;font:10px verdana;color:#000;background-color:#c0c0c0;font-weight:bold;}
				table.grid tbody tr td {border:solid 1px #000;font:10px verdana;color:#000;text-align:left;}
		   </style></head><body>';

		echo $conteudo;
	}
	else
	{
		echo cLAYOUT::CabecalhoPadrao();
		cLAYOUT::AdicionarScriptsPadrao();
		echo cLAYOUT::Logo();
		echo cLAYOUT::Menu();

		echo cLAYOUT::boxConteudo_inicio();
		echo cLAYOUT::boxConteudoInterno_inicio();
		echo $conteudo;
		echo cLAYOUT::boxConteudoInterno_fim();
		echo cLAYOUT::boxConteudo_fim();
		echo "\n<!-- Scripts -->";
		//if (cHTTP::$flAssociarEmpresaEmbProjAutomaticamente){
		//	cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieCombosEmpresaEmbProj());
	//	}
		echo cHTTP::ScriptsOnLoad();
	//$time = microtime();
	//$time = explode(' ', $time);
	//$time = $time[1] + $time[0];
	//$finish = $time;
	//$total_time = round(($finish - $start), 4);
	//echo '<br/><span style="font-size:9px;color:#cccccc;text-decoration:italic">Página gerada em '.$total_time.' segundos.</span>';
	}
	
}

