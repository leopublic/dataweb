<?php
// Carrega página com view totalmente gerada pelo controller.
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

$nomeController = cHTTP::$controller;
$metodo = cHTTP::$metodo;

if (method_exists($nomeController, "ExtendeModelo")) {
    $controller = new $nomeController();
    $conteudo = $controller->$metodo();
} else {
    $conteudo = $nomeController::$metodo();
}
if (cHTTP::RedirecionarSeSolicitado()){
    exit();
}

echo $conteudo;
