<?php
//while (@ob_end_clean());
//ob_start;
// TODO: Verificar diferencas em relacao a versao de producao
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

cHTTP::$comMenu = false;
cHTTP::setMetodoOperacaoDOWNLOAD();
$nomeController = cHTTP::$controller;
$metodo = cHTTP::$metodo;
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header('Content-Description: File Transfer');
header('Content-Type: application/force-download');
if(cHTTP::$tamanhoArq > 0)
{
	header('Content-Length: ' . cHTTP::$tamanhoArq);
}

header('Content-Disposition: attachment; filename="' . cHTTP::$nomeArq . '"');
if(method_exists($nomeController, "ExtendeModelo")) {
	$controller = new $nomeController();
	$conteudo = $controller->$metodo();
}
else{
	$conteudo = $nomeController::$metodo();
}
