<?php
ini_set('session.gc_maxlifetime', 3600);
// each client should remember their session id for EXACTLY 1 hour
session_set_cookie_params(3600);
session_start();
include ("../LIB/conecta.php");
include ("../LIB/cabecalho.php");

cHTTP::$comMenu = true;

$nomeController = cHTTP::$controller;
$metodo = cHTTP::$metodo;

if (substr($nomeController,0,15) != 'cCTRL_CAND_DRE_'
&&  substr($nomeController,0,15) != 'cCTRL_USUA_LOGI'
&&  substr($nomeController,0,15) != 'cCTRL_USUARIO_E'){
	header('location:/index.php');
}

if(method_exists($nomeController, "ExtendeModelo")) {
	$controller = new $nomeController();
	$conteudo = $controller->$metodo();
}
else{
	$conteudo = $nomeController::$metodo();
}
cHTTP::RedirecionarSeSolicitado();
echo $conteudo;
