<?php

// TODO: Verificar diferencas em relacao a versao de producao

ini_set('session.gc_maxlifetime', 14400);

// each client should remember their session id for EXACTLY 1 hour
session_set_cookie_params(14400);

session_start();
include ("../LIB/conecta.php");
$pag = $_SERVER['PHP_SELF'];

cHTTP::$comMenu = true;

$metodo = cHTTP::$metodo;
$controller = new cCTRL_CONVIDADO_DR();
if ($metodo == 'getLogin'){
    $conteudo = $controller->$metodo();  
} else {
  if ($controller->ativo()){
      $conteudo = $controller->$metodo();
  }  
}
//eval ('$conteudo = '.cHTTP::$controller.'::'.cHTTP::$metodo.'();');

cHTTP::RedirecionarSeSolicitado();

if (cHTTP::$MIME == cHTTP::mmXLS) {
    cLAYOUT::Head_ContentType('excel');
    echo '
       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
       <html><head>
       <style rel="stylesheet" type="text/css"/>' . file_get_contents('../estilos.css') . '
            table.grid {font:10px verdana;color:#000}
            table.grid thead tr th {border:solid 1px #000;font:10px verdana;color:#000;background-color:#c0c0c0;font-weight:bold;}
            table.grid tbody tr td {border:solid 1px #000;font:10px verdana;color:#000;text-align:left;}
       </style></head><body>';

    echo $conteudo;
} else {
    cHTTP::RedirecionarSeSolicitado();
    echo $conteudo;
}
