<?php
ini_set('session.gc_maxlifetime', 14400);
session_set_cookie_params(14400);

session_start();
include ("../LIB/conecta.php");
cHTTP::$comMenu = true;

//
// Valida hash
$hash = str_replace("'", "", $_GET['hash']);
$nu_candidato_tmp = intval($_GET['nu_candidato_tmp']);
$cd_usuario = intval($_GET['usuario']);

$ctrl = new cCTRL_CANDIDATO_CLIENTE;
// Verifica se o hash do candidato Ã© valido.
if ($ctrl->hashValido($hash, $cd_usuario, $nu_candidato_tmp)){
    // Sendo o hash vÃ¡lido, autentica o usuÃ¡rio
    $usuario = new cusuarios;
    $usuario->cd_usuario = $cd_usuario;
    $usuario->RecuperePeloId();
    cSESSAO::RegistraSessao($cd_usuario, $usuario->nome, $usuario->cd_perfil);
    $metodos = array('getCopiaDr' =>1, 'getConfirmaEnvio'=>2, 'getCancelaEnvio'=>3);
    $metodo = $_GET['metodo'];
    if (array_key_exists($metodo, $metodos)){
        $conteudo = $ctrl->$metodo();
        cHTTP::RedirecionarSeSolicitado();
        print $conteudo;
    } else {
        cNOTIFICACAO::singleton("Link incorrect or dated - metodo", cNOTIFICACAO::TM_ERROR);
        session_write_close();
        header('Location:/index.php');
    }
} else {
    cNOTIFICACAO::singleton("Link incorrect or dated", cNOTIFICACAO::TM_ERROR);
    session_write_close();
    header('Location:/index.php');
}
