<?php
include ("../LIB/autenticacao.php");
$nomeController = cHTTP::$controller;
$metodo = cHTTP::$metodo;
if(method_exists($nomeController, "ExtendeModelo")) {
	$controller = new $nomeController();
	$conteudo = $controller->$metodo();
}
else{
	$conteudo = $nomeController::$metodo();
}
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment;filename=" . cHTTP::get_nomeArquivo());
header("Content-Transfer-Encoding: binary ");
echo iconv("UTF-8", "UTF-16LE", $conteudo);
