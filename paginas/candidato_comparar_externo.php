<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");

$pNU_CANDIDATO = $_GET['NU_CANDIDATO'];
$cand = new cINTERFACE_CANDIDATO();
$cand2 = new cCANDIDATO_TMP();
$cand2->RecuperarPeloTmp($_GET['nu_candidato_tmp']);

if ($pNU_CANDIDATO > 0) {
    $cand->Recuperar($pNU_CANDIDATO);
    $titulo = $cand->mNOME_COMPLETO;
}

$processo = cCTRL_DR::ComparadorCadastros();

cHTTP::RedirecionarSeSolicitado();

$conteudo = "";

$conteudo .= Topo($titulo, "<style>form{display:inline;}</style>", true);
$conteudo .= Menu("SOL");
print $conteudo;
?>
<div class="conteudo">
<?
print '<div class="titulo" style="">DATA REQUEST para '.$cand2->mNOME_COMPLETO.'</div>';
?>
<? if ($pNU_CANDIDATO > 0) { ?>
    <div class="conteudoInterno">
        <div id="tabsOS">
            <ul>
                <li><a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=<?= $pNU_CANDIDATO; ?>">Perfil do candidato</a></li>
                <li><a href="#DadosOS"  class="ativo">Verificar atualização de cadastro</a></li>
            </ul>
            <div id="DadosOS">                
<? } ?>
                <div style="padding-left:10px;padding-right:10px;padding-top:10px;">
                    <? print $processo; ?>
                </div>
<? if ($pNU_CANDIDATO > 0) { ?>
            </div>
        </div>
    </div>
<? } ?>
</form>
</div>
<?
echo cHTTP::ScriptsOnLoad();
?>
<script language="javascript">
    $(document).ready(function () {
        var x = $("#tabsOS").tabs({
            active: 1,
            heightStyle: "content",
            beforeActivate: function (event, ui) {
                if (ui.newTab.context.href.substr(0, 4) == 'http') {
                    window.location.href = ui.newTab.context.href;
                    return false;
                }
                else {
                    return true;
                }
            }
        });

<? if ($_SESSION['msg'] != '') { ?>
            jAlert('<?= $_SESSION['msg']; ?>');
    <?
    $_SESSION['msg'] = '';
}
?>
    });

</script>
<?
echo Rodape("");
