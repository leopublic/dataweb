<?php

include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

cHTTP::$comMenu = true;

$nomeController = cHTTP::$controller;
$metodo = cHTTP::$metodo;
if (method_exists($nomeController, "ExtendeModelo")) {
    $controller = new $nomeController();
    $conteudo = $controller->$metodo();
} else {
    $conteudo = $nomeController::$metodo();
}

if (cHTTP::RedirecionarSeSolicitado()) {
    exit();
} else {
    echo cLAYOUT::CabecalhoPadrao();
    cLAYOUT::AdicionarScriptsPadrao();
    echo $conteudo;
    echo "\n<!-- Scripts -->";
    echo cHTTP::ScriptsOnLoad();
}

