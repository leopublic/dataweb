
<tr class="textoazul" height="20" style="background-color:#DADADA;">
  <td colspan=5><b>Dados da Empresa</td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Raz&atilde;o Social:</td>
  <td><?=montaCampos("razaoSocial",$razaoSocial,$razaoSocial,"T",$acao,"maxlength=150 size=30")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>C&oacute;d. Atividade Econ&ocirc;mica:</td>
  <td><?=montaCampos("atividadeEconomica",$atividadeEconomica,$atividadeEconomica,"T",$acao,"maxlength=15 size=30")?></td>
 </tr>

<tr height=20>
  <td><b><font color="Red">*</font>CNPJ:</td>
  <td><?=montaCampos("cnpj",$cnpj,$cnpj,"T",$acao,"maxlength=18 size=30 onkeyup=\"javascript:criaMascara(this,'##.###.###/####-##');\"")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Telefone:</td>
  <td><?=montaCampos("ddd",$ddd,$ddd,"T",$acao,"maxlength=2 size=3")?>-<?=montaCampos("telefone",$telefone,$telefone,"T",$acao,"maxlength=30 size=15")?></td>
</tr>

<tr height=20>
  <td><b><font color="Red">*</font>Endere&ccedil;o:</td>
  <td><?=montaCampos("endereco",$endereco,$endereco,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Complemento:</td>
  <td><?=montaCampos("complemento",$complemento,$complemento,"T",$acao,"maxlength=20 size=30")?></td>
</tr>

<tr height=20>
  <td><b><font color="Red">*</font>Bairro:</td>
  <td><?=montaCampos("bairro",$bairro,$bairro,"T",$acao,"maxlength=40 size=30")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Munic&iacute;pio:</td>
  <td><?=montaCampos("municipio",$municipio,$municipio,"T",$acao,"maxlength=40 size=30")?></td>
</tr>

<tr height=20>
  <td><b><font color="Red">*</font>UF:</td>
  <td><?=montaCampos("uf",$cmbEstados,$uf,"S",$acao,$selecione)?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>CEP:</td>
  <td><?=montaCampos("cep",$cep,$cep,"T",$acao,"maxlength=9 size=20  onkeyup=\"javascript:criaMascara(this,'#####-###');\"")?></td>
</tr>

<tr height=20>
  <td><b><font color="Red">*</font>Email:</td>
  <td colspan=4><?=montaCampos("email",$email,$email,"T",$acao,"")?></td>
</tr>

<tr><td colspan=5>&#160;</td></tr>
<tr class="textoazul" height="20" style="background-color:#DADADA;">
  <td colspan=5><b>Dados do Administrador:</td>		  
</tr>

<tr height=20>
  <td><b>Nome:</td>
  <td><?=montaCampos("nomeAdmin",$nomeAdmin,$nomeAdmin,"T",$acao,"maxlength=100 size=30")?></td>
  <td>&#160;</td>
  <td><b>Cargo:</td>
  <td><?=montaCampos("cargoAdmin",$cmbCargoAdmin,$nmCargoAdmin,"S",$acao,$selecione)?></td>
</tr>

<tr><td colspan=5>&#160;</td></tr>
<tr class="textoazul" height="20" style="background-color:#DADADA;">
  <td colspan=5><b>Dados do contato:</td>		  
</tr>

<tr height=20>
  <td><b><font color="Red">*</font>Nome:</td>
  <td><?=montaCampos("nomeContato",$nomeContato,$nomeContato,"T",$acao,"maxlength=50 size=30")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Email:</td>
  <td><?=montaCampos("emailContato",$emailContato,$emailContato,"T",$acao,"maxlength=50 size=30")?></td>
</tr>

<tr><td colspan=5>&#160;</td></tr>
<tr class="textoazul" height="20" style="background-color:#DADADA;">
  <td colspan=5><b>Dados do contrato social:</td>		  
</tr>

<tr>
  <td><b><font color="Red">*</font>Objetivo Social:</td>
  <td colspan=4><?=montaCampos("objetoSocial",$objetoSocial,$objetoSocial,"A",$acao,"rows=5 cols=50")?></td>
</tr>

<tr height=20>
  <td><b>Capital Inicial (R$):</td>
  <td><?=montaCampos("capitalInicial",$capitalInicial,$capitalInicial,"T",$acao,"size=20")?></td>
  <td>&#160;</td>
  <td><b>Constitui&ccedil;&atilde;o da Empresa:</td>
  <td><?=montaCampos("dataConstituicaoEmpresa",$dataConstituicaoEmpresa,$dataConstituicaoEmpresa,"D",$acao,"size=20")?></td>
</tr>

<tr height=20>
  <td><b>Capital Atual (R$):</td>
  <td><?=montaCampos("capitalAtual",$capitalAtual,$capitalAtual,"T",$acao,"size=20")?></td>
  <td>&#160;</td>
  <td><b>Data de altera&ccedil;&atilde;o contratual:</td>
  <td><?=montaCampos("dataAlteracaoContratual",$dataAlteracaoContratual,$dataAlteracaoContratual,"D",$acao,"size=20")?></td>
</tr>

<tr height=20>
  <td><b>Data de Cadastro no BC:</td>
  <td><?=montaCampos("DT_CADASTRO_BC",$DT_CADASTRO_BC,$DT_CADASTRO_BC,"D",$acao,"")?></td>
  <td>&#160;</td>
  <td>&#160;</td>
  <td>&#160;</td>
</tr>

<tr height=20>
  <td><b>Empresa estrangeira:</td>
  <td colspan=4><?=montaCampos("nomeEmpresaEstrangeira",$nomeEmpresaEstrangeira,$nomeEmpresaEstrangeira,"T",$acao,"maxlength=250 size=30")?></td>
</tr>

<tr height=20>
  <td><b>Investimento estrangeiro (R$):</td>
  <td><?=montaCampos("investimentoEstrangeiro",$investimentoEstrangeiro,$investimentoEstrangeiro,"T",$acao,"maxlength=20 size=20")?></td>
  <td>&#160;</td>
  <td><b>Data do investimento:</td>
  <td>
<?php
if( (strlen($dataInvestimentoEstrangeiro)>0) || ($acao != "V") ) {
  print montaCampos("dataInvestimentoEstrangeiro",$dataInvestimentoEstrangeiro,$dataInvestimentoEstrangeiro,"D",$acao,"");
} else {
  print "Nao ha.";
}
?></td>
</tr>

<tr height=20>
  <td><b>Quantidade de empregados:</td>
<?php
if( (strlen($qtdEmpregados)>0) && ($qtdEmpregados>0) ) {
  echo "<td>".montaCampos("qtdEmpregados",$qtdEmpregados,$qtdEmpregados,"T",$acao,"maxlength=10 size=20")."</td>";
} else {
  echo "<td>".montaCampos("qtdEmpregados",$qtdEmpregados,"Nao ha","T",$acao,"maxlength=10 size=20")."</td>";
}
echo "<td>&#160;</td>\n";
echo "<td><b>Empregados estrangeiros:</td>\n";
if( (strlen($qtdEmpregadosEstrangeiro)>0) && ($qtdEmpregadosEstrangeiro>0) ) {
  echo "<td>".montaCampos("qtdEmpregadosEstrangeiro",$qtdEmpregadosEstrangeiro,$qtdEmpregadosEstrangeiro,"T",$acao,"maxlength=10 size=20")."</td>";
} else {
  echo "<td>".montaCampos("qtdEmpregadosEstrangeiro",$qtdEmpregadosEstrangeiro,"Nao ha","T",$acao,"maxlength=10 size=20")."</td>";
}
?>
</tr>
<?php 
if(Acesso::permitido(Acesso::tp_Cadastro_EmpresaAtivar)){
//if ($usuperfil==1 || $usuperfil==4){
	$acaoAtivo = $acao;
}
else{
	$acaoAtivo = "V";
}
if ($acao == "V"){
	$cmp_tbpc_id = cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, "tbpc_id", "");
	$cmp_tbpc_id->mValor = $tbpc_tx_nome;
	$cmp_tbpc_id->mReadonly = true;
	$cmp_usua_id_responsavel = cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, "usua_id_responsavel", "");
	$cmp_usua_id_responsavel->mValor = $usua_tx_nome_responsavel;
	$cmp_usua_id_responsavel->mReadonly = true;
	$cmp_usua_id_responsavel2 = cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, "usua_id_responsavel2", "");
	$cmp_usua_id_responsavel2->mValor = $usua_tx_nome_responsavel2;
	$cmp_usua_id_responsavel2->mReadonly = true;
	$cmp_usua_id_responsavel3 = cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, "usua_id_responsavel3", "");
	$cmp_usua_id_responsavel3->mValor = $usua_tx_nome_responsavel3;
	$cmp_usua_id_responsavel3->mReadonly = true;
	$cmp_usua_id_responsavel4 = cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, "usua_id_responsavel4", "");
	$cmp_usua_id_responsavel4->mValor = $usua_tx_nome_responsavel4;
	$cmp_usua_id_responsavel4->mReadonly = true;
	$cmp_usua_id_responsavel5 = cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, "usua_id_responsavel5", "");
	$cmp_usua_id_responsavel5->mValor = $usua_tx_nome_responsavel5;
	$cmp_usua_id_responsavel5->mReadonly = true;
}
else{
	$cmp_tbpc_id = cFABRICA_CAMPO::NovoCombo("tbpc_id", "" , cCOMBO::cmbTABELA_PRECO);
	$cmp_tbpc_id->mValor = $tbpc_id;
	$cmp_usua_id_responsavel = cFABRICA_CAMPO::NovoCombo("usua_id_responsavel", "" , cCOMBO::cmbRESPONSAVEL);
	$cmp_usua_id_responsavel->mValor = $usua_id_responsavel;
	$cmp_usua_id_responsavel2 = cFABRICA_CAMPO::NovoCombo("usua_id_responsavel2", "" , cCOMBO::cmbRESPONSAVEL);
	$cmp_usua_id_responsavel2->mValor = $usua_id_responsavel2;
	$cmp_usua_id_responsavel3 = cFABRICA_CAMPO::NovoCombo("usua_id_responsavel3", "" , cCOMBO::cmbRESPONSAVEL);
	$cmp_usua_id_responsavel3->mValor = $usua_id_responsavel3;
	$cmp_usua_id_responsavel4 = cFABRICA_CAMPO::NovoCombo("usua_id_responsavel4", "" , cCOMBO::cmbRESPONSAVEL);
	$cmp_usua_id_responsavel4->mValor = $usua_id_responsavel4;
	$cmp_usua_id_responsavel5 = cFABRICA_CAMPO::NovoCombo("usua_id_responsavel5", "" , cCOMBO::cmbRESPONSAVEL);
	$cmp_usua_id_responsavel5->mValor = $usua_id_responsavel5;
}
?>
<tr class="textoazul" height="20" style="background-color:#DADADA;">
  <td colspan=5><b>Comportamento no Dataweb:</td>
</tr>
<tr height=20>
  <td><b>Ativa?:</td>
  <td><?=montaCampos("FL_ATIVA",$FL_ATIVA,'',"C",$acaoAtivo,"size=30")?></td>
  <input type="hidden" name="FL_ATIVA_VALOR" value="<?=$FL_ATIVA;?>"/>
  <td>&#160;</td>
  <td><b>Usuário responsável 1:</td>
  <td><?=cINTERFACE::RenderizeCampo($cmp_usua_id_responsavel);?></td>
</tr>
<tr height=20>
  <td><b>Usuário responsável 2:</td>
  <td><?=cINTERFACE::RenderizeCampo($cmp_usua_id_responsavel2);?></td>
  <td>&#160;</td>
  <td><b>Usuário responsável 3:</td>
  <td><?=cINTERFACE::RenderizeCampo($cmp_usua_id_responsavel3);?></td>
</tr>
<tr height=20>
  <td><b>Usuário responsável 4:</td>
  <td><?=cINTERFACE::RenderizeCampo($cmp_usua_id_responsavel4);?></td>
  <td>&#160;</td>
  <td><b>Usuário responsável 5:</td>
  <td><?=cINTERFACE::RenderizeCampo($cmp_usua_id_responsavel5);?></td>
</tr>
<tr height=20>
  <td><b>Tabela de preços:</td>
  <td colspan="4"><?=cINTERFACE::RenderizeCampo($cmp_tbpc_id);?></td>
<tr>
