

 <tr>
  <td class="textoazul" bgcolor="#DADADA" height="25" colspan=5>Empresa: <?=$nomeEmpresa?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Nome:</td>
  <td><?=montaCampos("nomeProcurador",$nomeProcurador,$nomeProcurador,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Cargo / Fun&ccedil;&atilde;o:</td>
  <td><?=montaCampos("idCargo",$cmbCargo,$nmCargo,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>CPF:</td>
  <td><?=montaCampos("cpfProcurador",formatarCPF_CNPJ($cpfProcurador, true),formatarCPF_CNPJ($cpfProcurador, true),"T",$acao,"maxlength=14 size=30 onkeyup=\"javascript:criaMascara(this,'###.###.###-##');\"")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Identidade:</td>
  <td><?=montaCampos("identidadeProcurador",$identidadeProcurador,$identidadeProcurador,"T",$acao,"maxlength=10 size=20")?></td>
 </tr>


 <tr height=20>
  <td><b><font color="Red">*</font>Org&atilde;o emissor:</td>
  <td><?=montaCampos("orgaoEmissorIdentProcurador",$orgaoEmissorIdentProcurador,$orgaoEmissorIdentProcurador,"T",$acao,"maxlength=20 size=20")?></td>
  <td>&#160;</td>
  <td><b>Data de emiss&atilde;o:</td>
  <td><?=montaCampos("dataEmissaoIdentProcurador",$dataEmissaoIdentProcurador,$dataEmissaoIdentProcurador,"T",$acao,"maxlength=10 size=20")?></td>
 </tr>

 <tr height=20>
  <td><b>Nacionalidade:</td>
  <td><?=montaCampos("idNacionalidade",$cmbNacionalidade,$nmNacionalidade,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Estado Civil:</td>
  <td><?=montaCampos("idEstadoCivil",$cmbEstadoCivil,$nmEstadoCivil,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Email:</td>
  <td colspan=4><?=montaCampos("emailProcurador",$emailProcurador,$emailProcurador,"T",$acao,"maxlength=50 size=50")?></td>
 </tr>		

