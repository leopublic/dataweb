
 <tr>
  <td class="textoazul" bgcolor="#DADADA" height="25" colspan=5>Empresa: <?=$nomeEmpresa?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Nome do projeto:</td>
  <td colspan=4><?=montaCampos("nomeEmbarcacaoProjeto",$nomeEmbarcacaoProjeto,$nomeEmbarcacaoProjeto,"T",$acao,"maxlength=60 size=50")?></td>
 </tr>
 
 <tr height=20>
  <td><b><font color="Red">*</font>Tipo:</td>
  <td colspan=4>
<?php
if($acao=="V") {
  if($tipo=="E") { $nmTipo="Afretamento"; } 
  else if($tipo=="P") { $nmTipo="Prestação de Serviços"; }
  else if($tipo=="T") { $nmTipo="Assistência Técnica"; }
  echo montaCampos("tipo",$tipo,$nmTipo,"V",$acao,"");
} else {
  if($tipo=="E") { $nmTipoE="checked"; } else { $nmTipoE=""; }
  if($tipo=="P") { $nmTipoP="checked"; } else { $nmTipoP=""; }
  if($tipo=="T") { $nmTipoT="checked"; } else { $nmTipoT=""; }
  echo montaCampos("tipo","E","Afretamento","R",$acao,$nmTipoE);
  echo "&nbsp;&nbsp;".montaCampos("tipo","P","Prestação de Serviços","R",$acao,$nmTipoP);
  echo "&nbsp;&nbsp;".montaCampos("tipo","T","Assistência Técnica","R",$acao,$nmTipoT);
}
?>
  </td>
 </tr>

 <tr height=20>
  <td><b>N&uacute;mero do contrato:</td>
  <td><?=montaCampos("numContrato",$numContrato,$numContrato,"T",$acao,"maxlength=20 size=20")?></td>
  <td>&#160;</td>
  <td><b>Prazo do contrato:</td>
  <td><?=montaCampos("dataContrato",$dataContrato,$dataContrato,"T",$acao,"maxlength=10 size=20")?></td>
 </tr>

<tr height=20>
  <td><b><font color="Red">*</font>Local:</td>
  <td><?=montaCampos("municipio",$municipio,$municipio,"T",$acao,"maxlength=40 size=30")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>UF:</td>
  <td><?=montaCampos("uf",$cmbEstados,$uf,"S",$acao,"")?></td>
</tr>

<tr height=20>
  <td><b>Qtd de tripulantes:</td>
  <td><?=montaCampos("qtdTripulantes",$qtdTripulantes,$qtdTripulantes,"T",$acao,"maxlength=5 size=10")?></td>
  <td>&#160;</td>
  <td><b>Qtd de estrangeiros:</td>
  <td><?=montaCampos("qtdTripulantesEstrangeiros",$qtdTripulantesEstrangeiros,$qtdTripulantesEstrangeiros,"T",$acao,"maxlength=5 size=10")?></td>
</tr>

<tr height=20>
  <td><b>Nome do contato:</td>
  <td colspan=4><?=montaCampos("nomeContato",$nomeContato,$nomeContato,"T",$acao,"maxlength=60 size=50")?></td>
</tr>

<tr height=20>
  <td><b>Telefone:</td>
  <td><?=montaCampos("telefoneContato",$telefoneContato,$telefoneContato,"T",$acao,"maxlength=30 size=30")?></td>
  <td>&#160;</td>
  <td><b>Email:</td>
  <td><?=montaCampos("emailContato",$emailContato,$emailContato,"T",$acao,"maxlength=50 size=30")?></td>
</tr>
<?php
if($usupetro == "S") {
  echo "<tr height=20>\n";
  echo "<td><b>Armador:</td>\n";
  echo "<td colspan=4>".montaCampos("nuArmador",$cmbArmadores,$nmAmador,"S",$acao,"")."</td>\n";
  echo "</tr>\n";
}
  echo "<tr height=20>\n";
  echo "<td><b>Justificativa:</td>\n";
  echo "<td colspan=4>".montaCampos("justificativa",$justificativa,$justificativa,"A",$acao,"rows=4 cols=50")."</td>\n";
  echo "</tr>\n";
?>

<?php 
  if ( ( ($acao=="V") && ($tipo=="E") ) || ($acao != "V") ) {
?>
 <tr height=20>
  <td><b>Bandeira:</td>
  <td colspan=4><?=montaCampos("coPais",$cmbPais,$nmPais,"S",$acao,"")?> 
      <?php 
       if ($acao != "V") { 
           echo "( * Em caso de Afretamentos )"; 
      } 
    ?>
 </td>
 </tr>
<?php } ?>

