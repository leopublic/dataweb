<?php
$acaoorg = $acao;
$acao = "V";
?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") ) { ?>
 <tr height="25">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Candidato: <?=$nomeCandidato?></td>
 </tr>

 <tr height=20>
  <td><b>Embarca&ccedil;&atilde;o/Projeto:</td>
  <td><?=montaCampos("NU_EMBARCACAO_PROJETO",$cmbProjetos,$nmProjetos,"H","V","")?></td>
  <td>&#160;</td>
  <td><b>Armador:</td>
  <td><?=montaCampos("NU_ARMADOR",$cmbArmador,$nmArmador,"S","V","")?></td>
 </tr>

<?php  }  ?> 

<?php  if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "REMU") ) { ?>
 <tr height=20>     <!-- esses 2 itens apenas usuario Mundivisas pode manipular -->
  <td><b>N&uacute;mero da Solicita&ccedil;&atilde;o:</td>
  <td><input type=hidden name="NU_SOLICITACAO" value="<?=$NU_SOLICITACAO?>"><?=$NU_SOLICITACAO?></td>
  <td>&#160;</td>
  <td><b>Tipo de Visto:</td>
  <td><?=montaCampos("CO_TIPO_AUTORIZACAO",$cmbAutorizacao,$nmAutorizacao,"S","V","")?></td>
 </tr>

<?php  }  ?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "PROCMTE") ) { ?>
 <tr height="20">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Processo MTE</td>
 </tr>
 <tr height=20>
  <td><b>N&uacute;mero Processo MTE:</td>
  <td><?=montaCampos("NU_PROCESSO_MTE",$NU_PROCESSO_MTE,$NU_PROCESSO_MTE,"T",$acao,"maxlength=20 size=30")?></td>
  <td>&#160;</td>
  <td><b>Data de Requerimento no MTE:</td>
  <td><?=montaCampos("DT_ABERTURA_PROCESSO_MTE",$DT_ABERTURA_PROCESSO_MTE,$DT_ABERTURA_PROCESSO_MTE,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Fun&ccedil;&atilde;o do Candidato:</td>
  <td colspan=4><?=montaCampos("CO_FUNCAO_CANDIDATO",$cmbFuncao,$nmFuncao,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><b>Reparti&ccedil;&atilde;o Consular:</td>
  <td><?=montaCampos("CO_REPARTICAO_CONSULAR",$cmbReparticao,$nmReparticao,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>N&uacute;mero  Oficio MRE:</td>
  <td><?=montaCampos("NU_AUTORIZACAO_MTE",$NU_AUTORIZACAO_MTE,$NU_AUTORIZACAO_MTE,"T",$acao,"maxlength=20 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Data Deferimento MTE:</td>
  <td><?=montaCampos("DT_AUTORIZACAO_MTE",$DT_AUTORIZACAO_MTE,$DT_AUTORIZACAO_MTE,"T",$acao,"maxlength=10 size=30")?></td>
  <td>&#160;</td>
  <td><b>Prazo Solicitado no MTE:</td>
  <td><?=montaCampos("DT_PRAZO_AUTORIZACAO_MTE",$DT_PRAZO_AUTORIZACAO_MTE,$DT_PRAZO_AUTORIZACAO_MTE,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>
<?php   }  ?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "VISTO") ) { ?>
 <tr height="20">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Dados do Visto</td>
 </tr>
 <tr height=20>
  <td><b>Valido Por:</td>
  <td><?=montaCampos("DT_VALIDADE_VISTO",$DT_VALIDADE_VISTO,$DT_VALIDADE_VISTO,"T",$acao,"maxlength=10 size=30")?></td>
  <td>&#160;</td>
  <td><b>N&uacute;mero do Visto:</td>
  <td><?=montaCampos("NU_VISTO",$NU_VISTO,$NU_VISTO,"T",$acao,"maxlength=20 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Data de Emiss&atilde;o:</td>
  <td><?=montaCampos("DT_EMISSAO_VISTO",$DT_EMISSAO_VISTO,$DT_EMISSAO_VISTO,"T",$acao,"maxlength=10 size=30")?></td>
  <td>&#160;</td>
  <td><b>Local de Emiss&atilde;o:</td>
  <td><?=montaCampos("NO_LOCAL_EMISSAO_VISTO",$NO_LOCAL_EMISSAO_VISTO,$NO_LOCAL_EMISSAO_VISTO,"T",$acao,"maxlength=60 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Pais de Emiss&atilde;o do Visto:</td>
  <td><?=montaCampos("CO_NACIONALIDADE_VISTO",$cmbNacVisto,$nmNacVisto,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Classifica&ccedil;&atilde;o Visto:</td>
  <td><?=montaCampos("CO_CLASSIFICACAO_VISTO",$cmbClassVisto,$nmClassVisto,"S",$acao,"")?></td>
 </tr>
 <tr height=20>
  <td><b>Local de Entrada: (cidade)</td>
  <td><?=montaCampos("NO_LOCAL_ENTRADA",$NO_LOCAL_ENTRADA,$NO_LOCAL_ENTRADA,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <td><b>UF de Entrada:</td>
  <td><?=montaCampos("NO_UF_ENTRADA",$cmbUFEntrada,$nmUFEntrada,"S",$acao,"")?></td>
 </tr>
 <tr height=20>
  <td><b>Data de Entrada:</td>
  <td><?=montaCampos("DT_ENTRADA",$DT_ENTRADA,$DT_ENTRADA,"T",$acao,"maxlength=10 size=20")?></td>
  <td>&#160;</td>
  <td><b>Meio de Transporte:</td>
  <td><?=montaCampos("NU_TRANSPORTE_ENTRADA",$cmbTransEntrada,$nmTransEntrada,"S",$acao,"")?></td>
 </tr>
<?php   }  ?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "REGCIE") ) { ?>
 <tr height="20">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Registro CIE</td>
 </tr>
 <tr height=20>
  <td><b>N&uacute;mero do Protocolo CIE:</td>
  <td><?=montaCampos("NU_PROTOCOLO_CIE",$NU_PROTOCOLO_CIE,$NU_PROTOCOLO_CIE,"T",$acao,"maxlength=20 size=30")?></td>
  <td>&#160;</td>
  <td><b>Data do Requerimento:</td>
  <td><?=montaCampos("DT_PROTOCOLO_CIE",$DT_PROTOCOLO_CIE,$DT_PROTOCOLO_CIE,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Validade do Protocolo CIE:</td>
  <td><?=montaCampos("DT_VALIDADE_PROTOCOLO_CIE",$DT_VALIDADE_PROTOCOLO_CIE,$DT_VALIDADE_PROTOCOLO_CIE,"T",$acao,"maxlength=10 size=30")?></td>
  <td>&#160;</td>
  <td><b>Prazo Estada Atual:</td>
  <td><?=montaCampos("DT_PRAZO_ESTADA",$DT_PRAZO_ESTADA,$DT_PRAZO_ESTADA,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>
  <input type=hidden name="DT_PRAZO_ESTADA_SOLICITADO" value="<?=$DT_PRAZO_ESTADA_SOLICITADO?>">
<?php   }  ?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "EMISCIE") ) { ?>
 <tr height="20">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Emissao CIE</td>
 </tr>
 <tr height=20>
  <td><b>N&uacute;mero da CIE:</td>
  <td><?=montaCampos("NU_EMISSAO_CIE",$NU_EMISSAO_CIE,$NU_EMISSAO_CIE,"T",$acao,"maxlength=20 size=30")?></td>
  <td>&#160;</td>
  <td>&#160;</td>
  <td>&#160;</td>
 </tr>
 <tr height=20>
  <td><b>Data Emiss&atilde;o CIE:</td>
  <td><?=montaCampos("DT_EMISSAO_CIE",$DT_EMISSAO_CIE,$DT_EMISSAO_CIE,"T",$acao,"maxlength=10 size=30")?></td>
  <td>&#160;</td>
  <td><b>Data de Validade CIE:</td>
  <td><?=montaCampos("DT_VALIDADE_CIE",$DT_VALIDADE_CIE,$DT_VALIDADE_CIE,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>
<?php   }  ?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "PRORR") ) { ?>
 <tr height="20">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Prorrogação</td>
 </tr>
 <tr height=20>
  <td><b>N&uacute;mero do Protocolo:</td>
  <td><?=montaCampos("NU_PROTOCOLO_PRORROGACAO",$NU_PROTOCOLO_PRORROGACAO,$NU_PROTOCOLO_PRORROGACAO,"T",$acao,"maxlength=20 size=30")?></td>
  <td>&#160;</td>
  <td><b>Data do Requerimento:</td>
  <td><?=montaCampos("DT_PROTOCOLO_PRORROGACAO",$DT_PROTOCOLO_PRORROGACAO,$DT_PROTOCOLO_PRORROGACAO,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Validade Protocolo:</td>
  <td><?=montaCampos("DT_VALIDADE_PROTOCOLO_PROR",$DT_VALIDADE_PROTOCOLO_PROR,$DT_VALIDADE_PROTOCOLO_PROR,"T",$acao,"maxlength=10 size=30")?></td>
  <td>&#160;</td>
  <td><b>Prazo Pretendido:</td>
  <td><?=montaCampos("DT_PRETENDIDA_PRORROGACAO",$DT_PRETENDIDA_PRORROGACAO,$DT_PRETENDIDA_PRORROGACAO,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>
<?php   }  ?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "CANC") ) { ?>
 <tr height="20">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Cancelamento</td>
 </tr>
 <tr height=20>
  <td><b>Data de Cancelamento:</td>
  <td><?=montaCampos("DT_CANCELAMENTO",$DT_CANCELAMENTO,$DT_CANCELAMENTO,"T",$acao,"maxlength=10 size=30")?></td>
  <td>&#160;</td>
  <td><b>N&uacute;mero do Processo:</td>
  <td><?=montaCampos("NU_PROCESSO_CANCELAMENTO",$NU_PROCESSO_CANCELAMENTO,$NU_PROCESSO_CANCELAMENTO,"T",$acao,"maxlength=20 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Data de Processo:</td>
  <td colspan=4><?=montaCampos("DT_PROCESSO_CANCELAMENTO",$DT_PROCESSO_CANCELAMENTO,$DT_PROCESSO_CANCELAMENTO,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>
<?php  }  ?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") ) { ?>
 <tr><td colspan=5>&#160;</td></tr>
 <tr>
  <td><b>Observa&ccedil;&otilde;es dos Processos:</td>
  <td colspan=4><?=montaCampos("TE_OBSERVACOES_PROCESSOS",$TE_OBSERVACOES_PROCESSOS,$TE_OBSERVACOES_PROCESSOS,"A",$acaoorg,"cols=70 rows=3")?></td>
 </tr>
<?php  }  ?>

<!-- Por compatibilidade com as tabelas -->

<input type=hidden name=TE_DESCRICAO_ATIVIDADES value='<?=$TE_DESCRICAO_ATIVIDADES?>'>
<input type="hidden" name="NO_MOEDA_REMUNERACAO_MENSAL" value="Reais" >
<input type="hidden" name="VA_RENUMERACAO_MENSAL" value="<?=$VA_RENUMERACAO_MENSAL?>" >
<input type="hidden" name="VA_REMUNERACAO_MENSAL_BRASIL" value="<?=$VA_REMUNERACAO_MENSAL_BRASIL?>" >

<?php
$acao = $acaoorg;
?>
