<?php
include ("../../LIB/classes/classe_proc_cancel.php");
include ("../../LIB/classes/classe_proc_emiscie.php");
include ("../../LIB/classes/classe_proc_mte.php");
include ("../../LIB/classes/classe_proc_prorrog.php");
include ("../../LIB/classes/classe_proc_regcie.php");
$acaoorg = $acao;
$acao = "V";
?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") ) { ?>
 <tr height="25">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Candidato: <?=$nomeCandidato?></td>
 </tr>

 <tr height=20>
  <td><b>Embarca&ccedil;&atilde;o/Projeto:</td>
  <td><?=montaCampos("NU_EMBARCACAO_PROJETO",$cmbProjetos,$nmProjetos,"S","V","")?></td>
  <td>&#160;</td>
  <td><b>Armador:</td>
  <td><?=montaCampos("NU_ARMADOR",$cmbArmador,$nmArmador,"S","V","")?></td>
 </tr>

<?php  }  ?> 

<?php  if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "REMU") ) { ?>
 <tr height=20>     <!-- esses 2 itens apenas usuario Mundivisas pode manipular -->
  <td><b>N&uacute;mero da Solicita&ccedil;&atilde;o:</td>
  <td><input type=hidden name="NU_SOLICITACAO" value="<?=$NU_SOLICITACAO?>"><?=$NU_SOLICITACAO?></td>
  <td>&#160;</td>
  <td><b>Tipo de Visto:</td>
  <td><?=montaCampos("CO_TIPO_AUTORIZACAO",$cmbAutorizacao,$nmAutorizacao,"S","V","")?></td>
 </tr>

<?php  }  ?>

<?php

if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "PROCMTE") ) {
  $proc = new proc_mte();
  $proc->BuscaPorCandidato($idCandidato,$NU_SOLICITACAO);
  print $proc->MontaTextoProcessoMTE("V");
}

if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "VISTO") ) {

?>

 <tr height="20">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Dados do Visto</td>
 </tr>
 <tr height=20>
  <td><b>Valido Por:</td>
  <td><?=montaCampos("DT_VALIDADE_VISTO",$DT_VALIDADE_VISTO,$DT_VALIDADE_VISTO,"T",$acao,"maxlength=10 size=30")?></td>
  <td>&#160;</td>
  <td><b>N&uacute;mero do Visto:</td>
  <td><?=montaCampos("NU_VISTO",$NU_VISTO,$NU_VISTO,"T",$acao,"maxlength=20 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Data de Emiss&atilde;o:</td>
  <td><?=montaCampos("DT_EMISSAO_VISTO",$DT_EMISSAO_VISTO,$DT_EMISSAO_VISTO,"T",$acao,"maxlength=10 size=30")?></td>
  <td>&#160;</td>
  <td><b>Local de Emiss&atilde;o:</td>
  <td><?=montaCampos("NO_LOCAL_EMISSAO_VISTO",$NO_LOCAL_EMISSAO_VISTO,$NO_LOCAL_EMISSAO_VISTO,"T",$acao,"maxlength=60 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Pais de Emiss&atilde;o do Visto:</td>
  <td><?=montaCampos("CO_NACIONALIDADE_VISTO",$cmbNacVisto,$nmNacVisto,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Classifica&ccedil;&atilde;o Visto:</td>
  <td><?=montaCampos("CO_CLASSIFICACAO_VISTO",$cmbClassVisto,$nmClassVisto,"S",$acao,"")?></td>
 </tr>
 <tr height=20>
  <td><b>Local de Entrada: (cidade)</td>
  <td><?=montaCampos("NO_LOCAL_ENTRADA",$NO_LOCAL_ENTRADA,$NO_LOCAL_ENTRADA,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <td><b>UF de Entrada:</td>
  <td><?=montaCampos("NO_UF_ENTRADA",$cmbUFEntrada,$nmUFEntrada,"S",$acao,"")?></td>
 </tr>
 <tr height=20>
  <td><b>Data de Entrada:</td>
  <td><?=montaCampos("DT_ENTRADA",$DT_ENTRADA,$DT_ENTRADA,"T",$acao,"maxlength=10 size=20")?></td>
  <td>&#160;</td>
  <td><b>Meio de Transporte:</td>
  <td><?=montaCampos("NU_TRANSPORTE_ENTRADA",$cmbTransEntrada,$nmTransEntrada,"S",$acao,"")?></td>
 </tr>

<?php

}

if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "REGCIE") ) {
  $regcie = new proc_regcie();
  $regcie->BuscaPorCandidato($idCandidato,$NU_SOLICITACAO);
  print $regcie->MontaTextoRegistroCIE("V");
}

if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "EMISCIE") ) {
  $emicie = new proc_emiscie();
  $emicie->BuscaPorCandidato($idCandidato,$NU_SOLICITACAO);
  print $emicie->MontaTextoEmissaoCIE("V");
}

if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "PRORR") ) {
  $prorr = new proc_prorrog();
  $prorr->BuscaPorCandidato($idCandidato,$NU_SOLICITACAO);
  print $prorr->MontaTextoProrrogacao("V");
}

if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "CANC") ) {
  $canc = new proc_cancel();
  $canc->BuscaPorCandidato($idCandidato,$NU_SOLICITACAO);
  print $canc->MontaTextoCancel("V");
}

?>

<?php  if( ($opvisa == "") || ($opvisa == "COMP") ) { ?>
 <tr><td colspan=5>&#160;</td></tr>
 <tr>
  <td><b>Obs. dos Processos:</td>
  <td colspan=4><?=montaCampos("TE_OBSERVACOES_PROCESSOS",$TE_OBSERVACOES_PROCESSOS,$TE_OBSERVACOES_PROCESSOS,"A",$acaoorg,"cols=70 rows=3")?></td>
 </tr>
<?php  }  ?>

<!-- Por compatibilidade com as tabelas -->

<input type=hidden name=TE_DESCRICAO_ATIVIDADES value='<?=$TE_DESCRICAO_ATIVIDADES?>'>
<input type="hidden" name="NO_MOEDA_REMUNERACAO_MENSAL" value="Reais" >
<input type="hidden" name="VA_RENUMERACAO_MENSAL" value="<?=$VA_RENUMERACAO_MENSAL?>" >
<input type="hidden" name="VA_REMUNERACAO_MENSAL_BRASIL" value="<?=$VA_REMUNERACAO_MENSAL_BRASIL?>" >

<?php
$acao = $acaoorg;
?>
