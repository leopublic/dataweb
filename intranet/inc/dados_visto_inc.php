<?php  if( ($opvisa == "") || ($opvisa == "COMP") || ($opvisa == "VISTO") ) { ?>
 <tr height="20">
  <td class="textoazul" bgcolor="#DADADA" colspan=5>Dados do Visto</td>
 </tr>
 <tr height=20>
  <td><b>Valido Por:</td>
  <td><?=montaCampos("DT_VALIDADE_VISTO",$DT_VALIDADE_VISTO,$DT_VALIDADE_VISTO,"T",$acao,"maxlength=15 size=10")?></td>
  <td>&#160;</td>
  <td><b>N&uacute;mero do Visto:</td>
  <td><?=montaCampos("NU_VISTO",$NU_VISTO,$NU_VISTO,"T",$acao,"maxlength=20 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Data de Emiss&atilde;o:</td>
  <td><?=montaCampos("DT_EMISSAO_VISTO",$DT_EMISSAO_VISTO,$DT_EMISSAO_VISTO,"D",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Local de Emiss&atilde;o:</td>
  <td><?=montaCampos("NO_LOCAL_EMISSAO_VISTO",$NO_LOCAL_EMISSAO_VISTO,$NO_LOCAL_EMISSAO_VISTO,"T",$acao,"maxlength=60 size=30")?></td>
 </tr>
 <tr height=20>
  <td><b>Pais de Emiss&atilde;o do Visto:</td>
  <td><?=montaCampos("CO_NACIONALIDADE_VISTO",$cmbNacVisto,$nmNacVisto,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Classifica&ccedil;&atilde;o Visto:</td>
  <td><?=montaCampos("CO_CLASSIFICACAO_VISTO",$cmbClassVisto,$nmClassVisto,"S",$acao,"")?></td>
 </tr>
 <tr height=20>
  <td><b>Local de Entrada: (cidade)</td>
  <td><?=montaCampos("NO_LOCAL_ENTRADA",$NO_LOCAL_ENTRADA,$NO_LOCAL_ENTRADA,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <td><b>UF de Entrada:</td>
  <td><?=montaCampos("NO_UF_ENTRADA",$cmbUFEntrada,$nmUFEntrada,"S",$acao,"")?></td>
 </tr>
 <tr height=20>
  <td><b>Data de Entrada:</td>
  <td><?=montaCampos("DT_ENTRADA",$DT_ENTRADA,$DT_ENTRADA,"D",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Meio de Transporte:</td>
  <td><?=montaCampos("NU_TRANSPORTE_ENTRADA",$cmbTransEntrada,$nmTransEntrada,"S",$acao,"")?></td>
 </tr>
<?php   }  ?>
