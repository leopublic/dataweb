
 <tr height="20">
  <td bgcolor="#DADADA" colspan=5>Dados da Solicitação</td>
 </tr>

 <tr height="20">
  <td><b>N&uacute;mero da Solicita&ccedil;&atilde;o:</b></td>
  <td><input type=hidden name="NU_SOLICITACAO" value="<?=$NU_SOLICITACAO;?>"/><?=$NU_SOLICITACAO?></td>
  <td>&#160;</td>
  <td><font color="Red">*</font><b>Prazo Solicitado:</b></td>
  <td><?=montaCampos("DT_PRAZO_ESTADA_SOLICITADO",$DT_PRAZO_ESTADA_SOLICITADO,$DT_PRAZO_ESTADA_SOLICITADO,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>

 <tr height=20>    
  <td><font color="Red">*</font><b>Tipo de Visto:</b></td>
  <td><?=montaCampos("CO_TIPO_AUTORIZACAO",$cmbAutorizacao,$nmAutorizacao,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>RNE:</td>
  <td><?=montaCampos("rne",$rne,$rne,"T",$acao,"maxlength=10 size=30")?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Projeto/Embarca&ccedil;&atilde;o Atual:</td>
  <td><?=montaCampos("idEmbarcacaoProjeto",$cmbProjetos,$nmProjeto,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Embarcado:</td>
  <td>
<?php
if($acao=="V") {
  if($CD_EMBARCADO>0) { echo "Sim"; } else { echo "Não"; }
} else {
  echo " <input type=radio name=CD_EMBARCADO value=1 ";
  if($CD_EMBARCADO>0) { echo "checked"; } 
  echo " > Sim";
  echo " <input type=radio name=CD_EMBARCADO value=0 ";
  if($CD_EMBARCADO==0) { echo "checked"; } 
  echo " > Não";
}
?>
  </td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Projeto/Embarca&ccedil;&atilde;o Inical:</td>
<?php
if($ehAdmin=="S") {
  echo "<td>".montaCampos("idEmbarcacaoInicial",$cmbProjetosIni,$nmProjetoIni,"S",$acao,"")."</td>";
} else {
  echo "<td><input type=hidden name='idEmbarcacaoInicial' value='$idEmbarcacaoInicial'>$nmProjetoIni</td>";
}
?>
  <td>&#160;</td>
  <td><b>Armador:</td>
  <td><?=montaCampos("NU_ARMADOR",$cmbArmador,$nmArmador,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Reparti&ccedil;&atilde;o Consular:</td>
  <td colspan=4><?=montaCampos("CO_REPARTICAO_CONSULAR",$cmbReparticao,$nmReparticao,"S",$acao,"")?></td>
 </tr>

 <tr height="20">
  <td bgcolor="#DADADA" colspan=5>Identificação do Candidato</td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Primeiro nome:</td>
  <td><?=montaCampos("primeiroNome",$primeiroNome,$primeiroNome,"T",$acao,"maxlength=200 size=32")?></td>
  <td>&#160;</td>
  <td><b>Nome do meio:</td>
  <td><?=montaCampos("nomeMeio",$nomeMeio,$nomeMeio,"T",$acao,"maxlength=50 size=52")?></td>
 </tr>

 <tr height=20>
  <td><b>&Uacute;ltimo nome:</td>
  <td><?=montaCampos("ultimoNome",$ultimoNome,$ultimoNome,"T",$acao,"maxlength=200 size=32")?></td>
  <td>&#160;</td>
  <td><b>CPF:</td>
  <td><?=montaCampos("cpf",formatarCPF_CNPJ($cpf, true),$cpf,"T",$acao,"maxlength=14 size=30  onkeyup=\"javascript:criaMascara(this,'###.###.###-##');\"")?></td>
 </tr>

 <tr>
  <td><font color="Red">*</font><b>Email:</td>
  <td colspan=5><?=montaCampos("email",$email,$email,"T",$acao,"maxlength=200 size=30")?></td>
 </tr>

 <tr height="20">
  <td bgcolor="#DADADA" colspan=5>Dados do Candidato</td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Nome do Pai:</td>
  <td><?=montaCampos("nomePai",$nomePai,$nomePai,"T",$acao,"maxlength=200 size=30")?></td>
  <td>&#160;</td>
  <td><font color="Red">*</font><b>Nome da M&atilde;e:</td>
  <td><?=montaCampos("nomeMae",$nomeMae,$nomeMae,"T",$acao,"maxlength=200 size=30")?></td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Estado civil:</td>
  <td><?=montaCampos("idEstadoCivil",$cmbCivil,$nmCivil,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><font color="Red">*</font><b>Sexo:</td>
  <td>
<?php
if($acao=="V") {
  if($sexo=="F") { $nmSexo="Feminino"; } else { $nmSexo="Masculino"; }
  echo montaCampos("sexo",$sexo,$nmSexo,"V",$acao,"");
} else {
  if($sexo=="F") { $nmSexoF="checked"; } else { $nmSexoF=""; }
  if($sexo=="M") { $nmSexoM="checked"; } else { $nmSexoM=""; }
  echo montaCampos("sexo","F","Feminino","R",$acao,$nmSexoF);
  echo "&nbsp;&nbsp;".montaCampos("sexo","M","Masculino","R",$acao,$nmSexoM);
}
?>
  </td>
 </tr>

 <tr height=20>
  <td><b>Data de nascimento:</td>
  <td><?=montaCampos("dataNascimento",$dataNascimento,$dataNascimento,"D",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Local de nascimento:</td>
  <td><?=montaCampos("localNascimento",$localNascimento,$localNascimento,"T",$acao,"maxlength=40 size=30")?></td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Nacionalidade:</td>
  <td><?=montaCampos("idNacionalidade",$cmbNacional,$nmNacional,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><font color="Red">*</font><b>N&iacute;vel de escolaridade:</td>
  <td><?=montaCampos("idEscolaridade",$cmbEscola,$nmEscola,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Profiss&atilde;o:</td>
  <td><?=montaCampos("idProfissao",$cmbProfissao,$nmProfissao,"S",$acao,"")?></td>
  <td>&#160;</td>
  <td><font color="Red">*</font><b>Fun&ccedil;&atilde;o:</td>
  <td><?=montaCampos("idFuncao",$cmbFuncao,$nmFuncao,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Sal&aacute;rio Total em Reais:</td>
  <td><?=montaCampos("salTotal",$salTotal,$salTotal,"T",$acao,"maxlength=30 size=30")?></td>
  <td>&#160;</td>
  <td><font color="Red">*</font><b>Sal&aacute;rio no Brasil:</td>
  <td><?=montaCampos("salBrasil",$salBrasil,$salBrasil,"T",$acao,"maxlength=30 size=30")?></td>
 </tr>

 <tr height=20>
  <td><b>Benef&iacute;cios Sal&aacute;rio:</td>
  <td colspan="5"><?=montaCampos("DS_BENEFICIOS_SALARIO",$DS_BENEFICIOS_SALARIO,$DS_BENEFICIOS_SALARIO,"A",$acao,"cols=60 rows=3")?></td>
 </tr>

 <tr height="20">
  <td bgcolor="#DADADA" colspan=5>Dados do Passaporte</td>
 </tr>

 <tr height=20>
  <td><b>N&uacute;mero do passaporte:</td>
  <td><?=montaCampos("numeroPassaporte",$numeroPassaporte,$numeroPassaporte,"T",$acao,"maxlength=30 size=30")?></td>
  <td>&#160;</td>
  <td><font color="Red">*</font><b>Pa&iacute;s emissor do passaporte:</td>
  <td><?=montaCampos("idPaisEmissorPassaporte",$cmbPaisPass,$nmPaisPass,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><b>Data de emiss&atilde;o do passaporte:</td>
  <td><?=montaCampos("dataEmissaoPassaporte",$dataEmissaoPassaporte,$dataEmissaoPassaporte,"D",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Data validade passaporte:</td>
  <td><?=montaCampos("dataValidadePassaporte",$dataValidadePassaporte,$dataValidadePassaporte,"D",$acao,"")?></td>
 </tr>

 <tr height="20">
  <td bgcolor="#DADADA" colspan=5>Endereço do Candidato no Brasil</td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Endere&ccedil;o:</td>
  <td colspan=5><?=montaCampos("endereco",$endereco,$endereco,"T",$acao,"maxlength=60 size=30")?></td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Cidade</td>
  <td><?=montaCampos("cidade",$cidade,$cidade,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <td><font color="Red">*</font><b>Pa&iacute;s:</td>
  <td><?=montaCampos("idPais",$cmbPaisRes,$nmPaisRes,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><font color="Red">*</font><b>Telefone:</td>
  <td><?=montaCampos("telefone",$telefone,$telefone,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <td>&#160;</td>
  <td>&#160;</td>
 </tr>

 <tr height="20">
  <td bgcolor="#DADADA" colspan=5>Endereços no Exterior</td>
 </tr>

 <tr height=20>
  <td><b>Nome da Empresa no Exterior:</td>
  <td><?=montaCampos("nomeEmpresa",$nomeEmpresa,$nomeEmpresa,"T",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Tel. da empresa no exterior:</td>
  <td><?=montaCampos("telefoneEmpresa",$telefoneEmpresa,$telefoneEmpresa,"T",$acao,"maxlength=30 size=30")?></td>
 </tr>

 <tr height=20>
  <td><b>Endere&ccedil;o da Empresa:</td>
  <td colspan=5><?=montaCampos("enderecoEmpresa",$enderecoEmpresa,$enderecoEmpresa,"T",$acao,"maxlength=60 size=30")?></td>
 </tr>

 <tr height=20>
  <td><b>Cidade da Empresa:</td>
  <td><?=montaCampos("cidadeEmpresa",$cidadeEmpresa,$cidadeEmpresa,"T",$acao,"")?></td>
  <td>&#160;</td>
  <td><b>Pa&iacute;s da Empresa:</td>
  <td><?=montaCampos("idPaisEmpresa",$cmbPaisEmp,$nmPaisEmp,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><b>Endere&ccedil;o do Candidato:</td>
  <td colspan=5><?=montaCampos("enderecoext",$enderecoext,$enderecoext,"T",$acao,"maxlength=60 size=30")?></td>
 </tr>

 <tr height=20>
  <td><b>Cidade do Candidato:</td>
  <td><?=montaCampos("cidadeext",$cidadeext,$cidadeext,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <td><font color="Red">*</font><b>Pa&iacute;s de Resid&ecirc;ncia:</td>
  <td><?=montaCampos("idPaisExt",$cmbPaisExt,$nmPaisExt,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><b>Telefone do Candidato:</td>
  <td><?=montaCampos("telefoneext",$telefoneext,$telefoneext,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <td>&#160;</td>
  <td>&#160;</td>
 </tr>

 <tr height="20">
  <td bgcolor="#DADADA" colspan=5>Outras Informações do Candidato</td>
 </tr>

 <tr height=20>
  <td><b>Experi&ecirc;ncia Profissional:</td>
  <td colspan=5><?=montaCampos("expProfissional",$expProfissional,$expProfissional,"A",$acao,"rows=3 cols=60")?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Descri&ccedil;&atilde;o das Atividades:</td>
  <td colspan=5><?=montaCampos("TE_DESCRICAO_ATIVIDADES",$TE_DESCRICAO_ATIVIDADES,$TE_DESCRICAO_ATIVIDADES,"A",$acao,"cols=60 rows=5")?></td>
 </tr>

 <tr height=20>
  <td><b>Justificativa:</td>
  <td colspan=5><?=montaCampos("DS_JUSTIFICATIVA",$DS_JUSTIFICATIVA,$DS_JUSTIFICATIVA,"A",$acao,"rows=4 cols=60")?></td>
 </tr>

<?php   if($idEmpresa != 21)  {  ?>

 <tr height=20>
  <td><b>Local de Trabalho:</td>
  <td colspan=5><?=montaCampos("DS_LOCAL_TRABALHO",$DS_LOCAL_TRABALHO,$DS_LOCAL_TRABALHO,"A",$acao,"rows=2 cols=60")?></td>
 </tr>

 <tr height=20>
  <td><b>Declara&ccedil;&atilde;o de remunera&ccedil;&atilde;o:</td>
  <td colspan=5><?=montaCampos("DS_SALARIO_EXTERIOR",$DS_SALARIO_EXTERIOR,$DS_SALARIO_EXTERIOR,"A",$acao,"rows=4 cols=60")?></td>
 </tr>

<?php   }    ?>

<?php if(strlen($idCandidato)>0) { ?>

 <tr height="20">
  <td bgcolor="#DADADA" colspan=5>Alterações nos Dados do Candidato</td>
 </tr>

 <tr height=20>
  <td><b>Data do Cadastro:</td>
  <td><?=montaCampos("DT_CADASTAMENTO",$dtUsuCad,$dtUsuCad,"H","","")?><?=$dtUsuCad?></td>
  <td>&#160;</td>
  <td><b>Admin Cadastro:</td>
  <td><?=montaCampos("CO_USU_CADASTAMENTO",$idUsuCad,$nmUsuCad,"H","","")?><?=$nmUsuCad?></td>
 </tr>
 <tr height=20>
  <td><b>&Uacute;ltima Altera&ccedil;&atilde;o:</td>
  <td><?=$dtUsuAlt?></td>
  <td>&#160;</td>
  <td><b>Admin Altera&ccedil;&atilde;o:</td>
  <td><?=$nmUsuAlt?></td>
 </tr>


<?php } ?>
