<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");

$acao = $_POST['acao'];
$idEmpresa = $_POST['idEmpresa'];
$idAssociada = $_POST['idAssociada'];

if( ($acao=="R") && (strlen($idAssociada)>0) ) {
  $sql = "delete from ASSOCIADAS where NU_ASSOCIADA=$idAssociada";
  mysql_query($sql);
  if(mysql_errno()>0) {
    gravaLogErro($sql,mysql_error(),"REMOCAO","ASSOCIADAS");
  } else {
    gravaLog($sql,mysql_error(),"REMOCAO","ASSOCIADAS");
  }
  $idAssociada = "";
}

if( strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $sql = "SELECT * FROM ASSOCIADAS where NU_EMPRESA=$idEmpresa";
  $linhas = mysql_query($sql);
} else {
  $msg = "Não foi informado a empresa.";
}

echo Topo("");
echo Menu("");

?>
<div class="conteudo">
<br><center>
<table border=0 width=700  cellspacing=0>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="3">
   <p align="center" class="textoazul"><strong>:: Cadastro de Associadas SA ::</strong></p>				
  </td>
 </tr>
 <tr><td colspan="3"><br><?=$msg?></td></tr>
 <tr>
  <td class="textoazul" NOWRAP>Empresa: <b><?=$nomeEmpresa?></b></a></td>
  <td width=200 align=center>
   <input type="button" class="textformtopo" style="<?=$estilo?>" value="Nova Associada" onclick="javascript:inclui();">
  </td>
  <td width=200 align=center>
   <input type="button" class="textformtopo" style="<?=$estilo?>" value="Voltar Empresa" onclick="javascript:empresas();">
  </td>
 </tr>
</table>
<br>
<table border=1 width=700 class="textoazulpeq">
 <tr align="center" height=20>
  <td width=100 bgcolor="#DADADA">A&ccedil;&atilde;o</td>
  <td width=250 bgcolor="#DADADA">Nome</td>
  <td width=150 bgcolor="#DADADA">CNPJ</td>
  <td width=200 bgcolor="#DADADA">Nacionalidade</td>
</tr>

<?php		   
while($rw1=mysql_fetch_array($linhas)) {
  $idAssociada = $rw1['NU_ASSOCIADA'];
  $nomeAssociada = $rw1['NO_ASSOCIADA'];			
  $cnpj = $rw1['NU_CNPJ'];
  $cdPais = 0+$rw1['CO_NACIONALIDADE'];
  if($cdPais > 0) {
     $nmNacionalidade = pegaNomeNacionalidade($cdPais);
  }
  echo "<tr height=20>";
  echo "<td align=center><a href='javascript:mostra($idAssociada);'>VER</a>";
  echo "&#160;&#160;<a href='javascript:altera($idAssociada);'>ALT</a>";
  if($ehAdmin=="S") {
    echo "&#160;&#160;<a href='javascript:remove($idAssociada,\"$nomeAssociada\");'>REM</a></td>";
  }
  echo "<td>&#160;$nomeAssociada</td>";
  echo "<td>&#160;$cnpj</td>";
  echo "<td>&#160;$nmNacionalidade</td>";
  echo "</tr>";
}
?>
		
</table>

<form name=tabela method=post>
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
<input type=hidden name=idAssociada>
<input type=hidden name=acao>
</form>

<form name=empresa method=post action="empresas.php">
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
</form>
</center>
</div>
<script language="javascript">
function mostra(codigo) {
  document.tabela.idAssociada.value=codigo;
  document.tabela.acao.value="V";
  document.tabela.target="assoc_"+codigo;
  document.tabela.action="novaAssociada.php";
  document.tabela.submit();
}
function inclui() {
  document.tabela.idAssociada.value='';
  document.tabela.acao.value="I";
  document.tabela.target="_top";
  document.tabela.action="novaAssociada.php";
  document.tabela.submit();
}
function altera(codigo) {
  document.tabela.idAssociada.value=codigo;
  document.tabela.acao.value="A";
  document.tabela.target="_top";
  document.tabela.action="novaAssociada.php";
  document.tabela.submit();
}
function remove(codigo,nome) {
  if(confirm("Voce deseja remover a associada "+nome+" ? ")) {
    document.tabela.idAssociada.value=codigo;
    document.tabela.acao.value="R";
    document.tabela.target="_top";
    document.tabela.action="associadas.php";
    document.tabela.submit();
  } else {
    alert("Operacao nao realizada.");
  }
}
function empresas() {
  document.empresa.submit();
}
</script>

<?php
echo Rodape($opcao);
?>
