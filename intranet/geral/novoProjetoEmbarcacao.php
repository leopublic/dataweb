<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");
include ("../javascript/validacoes_js.php"); 
include ("../javascript/novoProjetoEmbarcacao_js.php"); 

$idEmpresa = $_POST['idEmpresa'];
$idEmbarcacaoProjeto = $_POST['idEmbarcacaoProjeto'];
$acao = $_POST['acao'];

if( $idEmpresa != null && $idEmpresa != ""){
  $nomeEmpresa=pegaNomeEmpresa($idEmpresa);
  $condicao = " and b.NU_EMPRESA = $idEmpresa";
}
if( $idEmbarcacaoProjeto != null && $idEmbarcacaoProjeto != ""){
  $condicao = $condicao." and a.NU_EMBARCACAO_PROJETO = $idEmbarcacaoProjeto ";
}

if( $idEmpresa != null && $idEmbarcacaoProjeto != null){
  $sql = "select a.NU_EMPRESA as idEmpresa, a.NU_EMBARCACAO_PROJETO as idEmbarcacaoProjeto, a.NO_EMBARCACAO_PROJETO as nomeEmbarcacaoProjeto, ";
  $sql = $sql." a.IN_EMBARCACAO_PROJETO as tipo, a.NO_MUNICIPIO as municipio, a.CO_UF as uf, a.DT_PRAZO_CONTRATO as dataContrato, ";
  $sql = $sql." a.NO_CONTATO as nomeContato, a.NO_EMAIL_CONTATO as emailContato, a.NU_TELEFONE_CONTATO as telefoneContato, ";
  $sql = $sql." a.QT_TRIPULANTES as qtdTripulantes, a.QT_TRIPULANTES_ESTRANGEIROS as qtdTripulantesEstrangeiros, a.NO_CONTRATO, a.CO_PAIS, ";
  $sql = $sql." a.NU_ARMADOR,a.DS_JUSTIFICATIVA from EMBARCACAO_PROJETO a, EMPRESA b where a.NU_EMPRESA = b.NU_EMPRESA $condicao ";
  $resultado = mysql_query($sql);
  $dados = mysql_fetch_array($resultado);
  $idEmpresa = $dados['idEmpresa'];
  $idEmbarcacaoProjeto = $dados['idEmbarcacaoProjeto'];
  $nomeEmbarcacaoProjeto = $dados['nomeEmbarcacaoProjeto'];
  $tipo = $dados['tipo'];
  $municipio = $dados['municipio'];
  $uf = $dados['uf'];
  $dataContrato = dataMy2BR($dados['dataContrato']);
  $nomeContato = $dados['nomeContato'];
  $telefoneContato = $dados['telefoneContato'];
  $emailContato = $dados['emailContato'];
  $qtdTripulantes = $dados['qtdTripulantes'];
  $qtdTripulantesEstrangeiros = $dados['qtdTripulantesEstrangeiros'];
  $numContrato = $dados['NO_CONTRATO'];
  $coPais = 0+$dados['CO_PAIS'];
  $nuArmador = $dados['NU_ARMADOR'];
  $justificativa = $dados['DS_JUSTIFICATIVA'];
}

if($usuarmad == "S") {
   $nuArmador = $usulogado;
   $extra = " NU_USUARIO=$nuArmador";
}

$nmAmador = "";
$cmbArmadores = "<option value=''>Escolha um armador".montaComboArmador($codigo,$extra);
if(strlen($nuArmador)>0) { 
   $nmAmador = pegaNomeUsuario($nuArmador);
}

$cmbEstados = "<option value=''>Escolha um estado".montaComboEstados($uf);
$cmbPais = "<option value=''>Escolha um país".montaComboPaisNacionalidade($coPais,"");
$nmPais = pegaNomeNacionalidade($coPais);

echo Topo("");
echo Menu("");

?>

<br><center>
<table border=0 width=800>
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Cadastro de Projetos e Embarcações ::</strong></p>
  </td>
 </tr>
</table>
<br>
<table border=0 align="center" width=800 class="textoazulPeq">
 <tr>
  <td colspan="5" class="textoazul">Preencha os dados abaixo para o cadastro de Projeto / Embarcação</font></td>
 </tr>
 <tr>
  <td colspan="5"><font color="Red">*</font>&nbsp;<font class="textoazulPeq">Campos obrigatórios</font><br><br></td>
 </tr>

<form action="insereProjetoEmbarcacao.php" method="post" onSubmit="return validarDados(this)">
<input type="hidden" name="idEmbarcacaoProjeto" value="<?=$idEmbarcacaoProjeto?>">
<input type="Hidden" name="idEmpresa" value='<?=$idEmpresa?>'>

<?php
include("../inc/projetos_inc.php");
?>

</table>
<br>

<?php
if($acao=="V") {
  echo "<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();'>\n";
} else {
  echo "<input name='salvar' type='Submit' class='textformtopo' style=\"$estilo\" value='Salvar'>\n";
}
echo Rodape("");
?>
