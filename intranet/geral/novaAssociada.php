<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
$idAssociada = $_POST['idAssociada'];
$acao = $_POST['acao'];

if( $idEmpresa != null && $idEmpresa != ""){
  $nomeEmpresa=pegaNomeEmpresa($idEmpresa);
  $condicao = " and b.NU_EMPRESA = $idEmpresa";
}
if( $idAssociada != null && $idAssociada != ""){
  $condicao = $condicao." and a.NU_ASSOCIADA = $idAssociada ";
}

if( $idEmpresa != null && $idAssociada != null){
  $query = "select a.NU_ASSOCIADA as idAssociada, a.NU_EMPRESA as idEmpresa, a.NO_ASSOCIADA as nomeAssoc, a.CO_NACIONALIDADE as cdPais, ";
  $query = $query." a.NO_CAPITAL_VOTANTE as votante, a.NO_CAPITAL_TOTAL as capital, a.NU_CNPJ as cnpj ";
  $query = $query." from ASSOCIADAS a, EMPRESA b where a.NU_EMPRESA = b.NU_EMPRESA $condicao ";
  $resultado = mysql_query($query);
  $dados = mysql_fetch_array($resultado);
  $idAssociada = $dados['idAssociada'];
  $nomeAssoc = $dados['nomeAssoc'];
  $cdPais = $dados['cdPais'];
  $votante = $dados['votante'];
  $capital = $dados['capital'];
  $cnpj = $dados['cnpj'];
  $nmNacionalidade = pegaNomeNacionalidade($cdPais);
} 
$cmbNacionalidade = "<option value=''>Selecione a nacionalidade".montaComboNacionalidade($cdPais,"");

echo Topo($opcao);
echo Menu("");

?>

<div class="conteudo">
<br><center>
<table border=0 width=700 class="textoazulPeq">
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Cadastro de Associadas S.A. ::</strong></p>				
  </td>
 </tr>
</table>
<br>
<form action="insereAssociada.php" method="post" name=admin>
<table border=0 align="center" width="800" class="textoazulPeq">
 <tr>
  <td colspan="5" class="textoazul">Preencha os dados abaixo para o cadastro de Associadas</font></td>
 </tr>
 <tr>
  <td colspan="5"><font color="Red">*</font>&nbsp;<font class="textoazulPeq">Campos obrigatórios</font><br><br></td>
 </tr>

<input type="Hidden" name="idAssociada" value="<?=$idAssociada?>">
<input type="Hidden" name="idEmpresa" value='<?=$idEmpresa?>'>

<?php
include("../inc/associadas_inc.php");
?>

</table>
<br>

<?php
if($acao=="V") {
  echo "<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();'>\n";
} else {
  echo "<input name='salvar' type='button' class='textformtopo' style=\"$estilo\" value='Salvar' onClick='javascript:Enviar();'>\n";
}

echo Rodape("");
?>
</form>
</center>
</div>

<script language="javascript">
function Enviar() {
  var ret = "";
  if(document.admin.nomeAssoc.value.length < 10) {
     ret = ret+"Nome da associada invalida.\n";
  }
  if(document.admin.cdNacionalidade.selectedIndex <= 0) {
     ret = ret+"Nacionalidade da associada invalida.\n";
  }
  if(document.admin.cnpj.value.length < 10) {
     ret = ret+"CNPJ da associada invalido.\n";
  }
  if(document.admin.votante.value.length < 5) {
     ret = ret+"Capital Votante da associada invalido.\n";
  }
  if(document.admin.capital.value.length < 5) {
     ret = ret+"Capital Total da associada invalido.\n";
  }
  if(ret.length == 0) {
     document.admin.submit();
  } else {
     alert(ret);
  }
}
</script>
