<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");
include ("../javascript/criamascara_js.php");
include ("../javascript/validacoes_js.php"); 
include ("../javascript/novoProcurador_js.php"); 

$idEmpresa = $_POST['idEmpresa'];
$idProcurador = $_POST['idProcurador'];
$acao = $_POST['acao'];

if( $idEmpresa != null && $idEmpresa != ""){
  $nomeEmpresa=pegaNomeEmpresa($idEmpresa);
  $condicao = " and b.NU_EMPRESA = $idEmpresa";
}
if( $idProcurador != null && $idProcurador != ""){
  $condicao = $condicao." and a.NU_PROCURADOR = $idProcurador ";
}

if( $idEmpresa != null && $idProcurador != null){
  $query = "select a.NU_PROCURADOR as idProcurador, a.NU_EMPRESA as idEmpresa, a.NO_PROCURADOR as nomeProcurador, ";
  $query = $query."a.CO_CARGO_FUNCAO as cargoFuncao, a.NU_CPF as cpf, a.NU_IDENTIDADE as identidade, a.NO_EMISSOR_IDENTIDADE,";
  $query = $query." a.DT_EMISSAO_IDENTIDADE, a.NO_EMAIL as email,a.CO_NACIONALIDADE, a.CO_ESTADO_CIVIL  ";
  $query = $query."from PROCURADOR_EMPRESA a, EMPRESA b where a.NU_EMPRESA = b.NU_EMPRESA $condicao ";
  $resultado = mysql_query($query);
  $dados = mysql_fetch_array($resultado);
  $idProcurador = $dados['idProcurador'];
  $nomeProcurador = $dados['nomeProcurador'];
  $idCargo = $dados['cargoFuncao'];
  $cpfProcurador = $dados['cpf'];
  $identidadeProcurador = $dados['identidade'];
  $orgaoEmissorIdentProcurador = $dados['NO_EMISSOR_IDENTIDADE'];
  $dataEmissaoIdentProcurador = dataMy2BR($dados['DT_EMISSAO_IDENTIDADE']);
  $emailProcurador = $dados['email'];
  $idNacionalidade = $dados['CO_NACIONALIDADE'];
  $idEstadoCivil = $dados['CO_ESTADO_CIVIL'];
} 
if(strlen($idCargo)>0) {
  $nmCargo = pegaNomeFuncao($idCargo);
}
if(strlen($idNacionalidade)>0) {
  $nmNacionalidade = pegaNomeNacionalidade($idNacionalidade);
}
if(strlen($idEstadoCivil)>0) {
  $nmEstadoCivil = pegaNomeEstadoCivil($idEstadoCivil);
}

$cmbCargo = montaComboFuncoes($idCargo," ORDER BY NO_FUNCAO");
$cmbNacionalidade = montaComboPaisNacionalidade($idNacionalidade,"");
$cmbEstadoCivil = montaComboEstadoCivil($idEstadoCivil,"");

echo Topo($opcao);
echo Menu("");

?>
<div class="conteudo">
<br><center>
<table border=0 width=700 class="textoazulPeq">
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Cadastro de Procurador ::</strong></p>				
  </td>
 </tr>
</table>
<br>
<table border=0 align="center" width="800" class="textoazulPeq">
 <tr>
  <td colspan="5" class="textoazul">Preencha os dados abaixo para o cadastro de procurador</font></td>
 </tr>
 <tr>
  <td colspan="5"><font color="Red">*</font>&nbsp;<font class="textoazulPeq">Campos obrigatórios</font><br><br></td>
 </tr>

<form action="insereProcurador.php" method="post" onSubmit="return validarDados(this)">
<input type="Hidden" name="idProcurador" value="<?=$idProcurador?>">
<input type="Hidden" name="idEmpresa" value='<?=$idEmpresa?>'>

<?php
include("../inc/procuradores_inc.php");
?>

</table>
<br>

<?php
if($acao=="V") {
  echo "<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();'>\n";
} else {
  echo "<input name='salvar' type='Submit' class='textformtopo' style=\"$estilo\" value='Salvar'>\n";
}
echo Rodape("");
?>
</form>
</div>

