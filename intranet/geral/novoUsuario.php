<?php
$opcao = "USU";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");
include ("../javascript/novoUsuario_js.php");

$acao = $_POST['acao'];
echo Topo($opcao);
if($acao!="V") {
  echo Menu($opcao);
}

$id = 0+$_POST['usuario'];
$idEmpresa = $_POST['idEmpresa'];
$funcS = "";
$funcN = "";
$petroS = "";
$petroN = "";
$armaS = "";
$armaN = "";
$wilS="";
$wilN="";
$opRestN="";
$opRestB=""; # Brasilia

if( $id > 0){
  $sql = "SELECT a.NU_USUARIO as id, a.NU_EMPRESA as idEmpresa, a.NO_EMAIL_USUARIO as email, a.CO_SENHA as senha, ";
  $sql = $sql." a.CO_USUARIO as login, a.NO_USUARIO as nome, a.SE_PETROBRAS, a.SE_ARMADOR, a.SE_MUNDIVISAS, ";
  $sql = $sql." a.SE_OPERADOR, a.OPER_RESTRITO , b.NU_PERFIL as idPerfil ";
  $sql = $sql." from USUARIO as a, PERFIL as b where a.NU_PERFIL = b.NU_PERFIL and a.NU_USUARIO = $id ";
  $resultado = mysql_query($sql);
  if(mysql_errno() != 0) {
    print "Erro ao buscar dados do usuario: ".mysql_error()."\n<!-- SQL=".$sql." -->\n";
  } else {
    $dados = mysql_fetch_array($resultado);    
    $id = $dados['id'];
    $idEmpresa = $dados['idEmpresa'];
    $login = $dados['login'];
    $nome = $dados['nome'];
    $email = $dados['email'];
    $senha = $dados['senha'];
    $se_arm = $dados['SE_ARMADOR'];
    $se_pet = $dados['SE_PETROBRAS'];
    $se_fun = $dados['SE_MUNDIVISAS'];
    $se_wil = $dados['SE_OPERADOR'];
    $oper_rest = $dados['OPER_RESTRITO'];
    $idPerfil = 0+$dados['idPerfil'];
    $nmPerfil = pegaNomePerfil($idPerfil);
  }
  if($se_arm=="S") { $armaS="checked"; } else { $armaN="checked"; }
  if($se_pet=="S") { $petroS="checked"; } else { $petroN="checked"; }
  if($se_fun=="S") { $funcS="checked"; } else { $funcN="checked"; }
  if($se_wil=="S") { $wilS="checked"; } else { $wilN="checked"; }
  if($oper_rest=="N") { $opRestN="checked"; } elseif($oper_rest=="B") { $opRestB="checked"; }
 
} else {
  $id = "";
}
$cmbPerfil = montaComboPerfis($idPerfil,"ORDER BY NO_PERFIL");
$cmbEmpresa = montaComboEmpresas($idEmpresa,"ORDER BY NO_RAZAO_SOCIAL");

if($acao=="V") {
  $senha = "*******";
  $confsenha = "*******";
  $nmEmpresa = pegaNomeEmpresa($idEmpresa);
} else {
  $nmEmpresa = pegaNomeEmpresa($idEmpresa);
  $confsenha = $senha;
}
?>

<br><center>
<table border=0 width="700">
 <tr>
  <td class="textobasico" align="center" valign="top">
	<p align="center" class="textoazul"><strong>:: Cadastro de Usuários ::</strong></p>
  </td>
 </tr>
 <tr><td><br></td></tr>
</table>
<br>
<table border=0 width="700" class="textoazulpeq">
 <tr>
  <td colspan="5"><font color="Red">*</font>&nbsp;<font class="textoazulPeq">Campos obrigatórios</font></td>
 </tr>
 <form action="insereUsuario.php" method="post" onSubmit="return validarDados(this)">
 <input type="hidden" name="id" value='<?=$id?>'>

 <tr height=20>
  <td><b><font color="Red">*</font>Login:</td>
  <td><?=montaCampos("login",$login,$login,"T",$acao,"maxlength=20 size=30")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Nome:</td>
  <td><?=montaCampos("nome",$nome,$nome,"T",$acao,"maxlength=50 size=30")?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Senha:</td>
  <td><?=montaCampos("senha",$senha,$senha,"P",$acao,"maxlength=20 size=30")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Confirmação:</td>
  <td><?=montaCampos("confsenha",$confsenha,$confsenha,"P",$acao,"maxlength=20 size=30")?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Email:</td>
  <td><?=montaCampos("email",$email,$email,"T",$acao,"maxlength=50 size=30")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Perfil de acesso:</td>
  <td><?=montaCampos("idPerfil",$cmbPerfil,$nmPerfil,"S",$acao,"")?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Empresa:</td>
  <td colspan=4>
<?php
if($acao=="V") {
  echo $nmEmpresa;
} else {
  echo "<select name=idEmpresa><option value=''>Nenhuma</option>$cmbEmpresa</select>\n";
}
?>
  </td>
 </tr>

 <tr height=20>
  <td colspan=5><b><font color="Red">*</font>O usuário é funcionario Mundivisas ? 
    &#160;&#160;&#160;	
<?php 
   if($acao!="V") { 
?>
    <input type=radio name=eh_func value="S" <?=$funcS?>> Sim
    &#160;&#160;&#160;
    <input type=radio name=eh_func value="N" <?=$funcN?>> Não
<?php 
   } else {
     if($se_fun=="S") { echo "Sim"; } else { echo "Não"; }
   }
?>
  </td>
 </tr>


 <tr height=20>
  <td colspan=5><b><font color="Red">*</font>O usuário atende apenas Petrobras ? 
    &#160;&#160;&#160;	
<?php 
   if($acao!="V") { 
?>
    <input type=radio name=eh_petro value="S" <?=$petroS?>> Sim
    &#160;&#160;&#160;
    <input type=radio name=eh_petro value="N" <?=$petroN?>> Não
<?php 
   } else {
     if($se_pet=="S") { echo "Sim"; } else { echo "Não"; }
   }
?>
  </td>
 </tr>

 <tr height=20>
  <td colspan=5><b><font color="Red">*</font>O usuário é armador (Petrobras) ? 
    &#160;&#160;&#160;	
<?php 
   if($acao!="V") { 
?>
    <input type=radio name=eh_arma value="S" <?=$armaS?>> Sim
    &#160;&#160;&#160;
    <input type=radio name=eh_arma value="N" <?=$armaN?>> Não
<?php 
   } else {
     if($se_arm=="S") { echo "Sim"; } else { echo "Não"; }
   }
?>
  </td>
 </tr>

 <tr height=20>
  <td colspan=5><b><font color="Red">*</font>O usuário é operador apenas ? 
    &#160;&#160;&#160;	
<?php 
   if($acao!="V") { 
?>
    <input type=radio name=eh_wil value="S" <?=$wilS?> onclick="javascript:restrito(1);"> Sim
    &#160;&#160;&#160;
    <input type=radio name=eh_wil value="N" <?=$wilN?> onclick="javascript:restrito(0);"> Não
<?php 
   } else {
     if($se_wil=="S") { echo "Sim"; } else { echo "Não"; }
   }
?>
  </td>
 </tr>

 <tr height=20 id="opRest" style="display:none;">
  <td colspan=5><b><font color="Red">*</font>Se o usuário é operador, é restrito ? 
    &#160;&#160;&#160;	
<?php 
   if($acao!="V") { 
?>
    <input type=radio name=eh_opRest value="N" <?=$opRestN?>> Não
    &#160;&#160;&#160;
    <input type=radio name=eh_opRest value="B" <?=$opRestB?>> Brasilia
<?php 
   } else {
     if($oper_rest=="N") { echo "Não"; } else if($oper_rest=="B") { echo "Brasilia"; }
   }
?>
  </td>
 </tr>


 <tr><td><br></td></tr>
 <tr>
  <td colspan=5 align="center">
<?php
if($acao=="V") {
  echo "<input name='fechar' type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();'>";
} else {
  echo "<input name='salvar' type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Salvar' onclick='javascript:if(validarDados()) { document.forms[0].submit(); }'>";
}
?>
  </td>
 </tr>
 </form>
</table>

<?php
echo Rodape($opcao);
?>

<script language="javascript">
function restrito(cond) {
   if(cond>0) {
      document.all.opRest.style.display = "block";
   } else {
      document.all.opRest.style.display = "none";
   }
}
</script>
