<?php

$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");

//ini_set('error_reporting', E_ALL); 
//ini_set('display_errors', 1);
$_SESSION['voltar'] = "os_listar.php";

$pData = $_POST['dt_cadastro'];
if($pData ==''){
	$pData = date('d/m/Y');
}
$pNU_EMPRESA = $_POST['cFILTRO_NU_EMPRESA'];
$pNU_EMBARCACAO_PROJETO = $_POST['NU_EMBARCACAO_PROJETO'];

$nome_arquivo = 'checklist_protocolo_'.str_replace('/','_',$pData); 
header('Content-type: application/vnd.ms-excel'); 
header('Content-type: application/force-download');
header('Content-Disposition: attachment; filename='.$nome_arquivo.'.xls');
header('Pragma: no-cache'); 
       
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
       <html>
       	<head>
       <style rel="stylesheet" type="text/css"/>'.file_get_contents('../estilos.css').'
			@page rotated{margin:.98in .79in .98in .79in;
					mso-header-margin:.49in;
					mso-footer-margin:.49in;
				size : landscape;
				}
       		table.grid { page : rotated; border:solid thin #000;font:10pt verdana;color:#000;border-collapse:collapse;}
			table.grid thead tr td {border:solid thin #000;font:8pt verdana;color:#000;background-color:#fff;font-weight:normal;}
			table.grid thead tr th {border:solid thin #000;font:8pt verdana;color:#000;background-color:#c0c0c0;font-weight:bold;}
			table.grid thead tr.titulo th {text-align:left;border:solid thin #000;font:8pt verdana;color:#000;background-color:#ffffff;font-weight:bold;}
			table.grid thead tr.gabarito td {border:none;height:2px;}
			table.grid tbody tr td {border:solid thin #000;font:8pt verdana;color:#000;text-align:left;}
			br { mso-data-placement:same-cell; }
       </style></head><body>';

$OS_I = new cINTERFACE_OS();
$OS_I->ListarChecklistProtocoloColeta($pData, $pNU_EMPRESA, $pNU_EMBARCACAO_PROJETO );
?>
</body>
