<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");
include ("../javascript/validacoes_js.php"); 
include ("../javascript/criamascara_js.php"); 
include ("../javascript/novaEmpresa_js.php"); 

$id = $_POST['idEmpresa'];
$idEmpresa = $_POST['idEmpresa'];
$acao = $_POST['acao'];

$selecione="<option value=''>Selecione ...</option>";

if( $id != null){
	$sql = "SELECT E.*, NO_FUNCAO , tbpc_tx_nome, nome nome_responsavel
			from EMPRESA E
			left join FUNCAO_CARGO F on F.CO_FUNCAO = CO_CARGO_ADMIN
			left join tabela_precos tp on tp.tbpc_id = E.tbpc_id
			left join usuarios u on u.cd_usuario = E.usua_id_responsavel
			where E.NU_EMPRESA = $id ";

  $resultado = mysql_query($sql);
  $dados = mysql_fetch_array($resultado);    
  $id = $dados['NU_EMPRESA'];
  $razaoSocial = $dados['NO_RAZAO_SOCIAL'];
  $atividadeEconomica = $dados['CO_ATIVIDADE_ECONOMICA'];
  $cnpj = $dados['NU_CNPJ'];
  $endereco = $dados['NO_ENDERECO'];
  $complemento = $dados['NO_COMPLEMENTO_ENDERECO'];
  $bairro = $dados['NO_BAIRRO'];
  $municipio = $dados['NO_MUNICIPIO'];
  $uf = $dados['CO_UF'];
  $cep = $dados['NU_CEP'];
  $ddd = $dados['NU_DDD'];
  $telefone = $dados['NU_TELEFONE'];
  $email = $dados['NO_EMAIL_EMPRESA'];
  $nomeContato = $dados['NO_CONTATO_PRINCIPAL'];
  $emailContato = $dados['NO_EMAIL_CONTATO_PRINCIPAL'];
  $nomeAdmin = $dados['NO_ADMINISTRADOR'];
  $cargoAdmin = $dados['CO_CARGO_ADMIN'];
  $objetoSocial = $dados['TE_OBJETO_SOCIAL'];
  $capitalAtual = $dados['VA_CAPITAL_ATUAL'];
  $capitalInicial = $dados['VA_CAPITAL_INICIAL'];
  $dataConstituicaoEmpresa = $dados['DT_CONSTITUICAO_EMPRESA'];	
  $dataAlteracaoContratual = $dados['DT_ALTERACAO_CONTRATUAL'];
  $nomeEmpresaEstrangeira = $dados['NO_EMPRESA_ESTRANGEIRA'];
  $investimentoEstrangeiro = $dados['VA_INVESTIMENTO_ESTRANGEIRO'];
  $dataInvestimentoEstrangeiro = $dados['DT_INVESTIMENTO_ESTRANGEIRO'];
  $qtdEmpregados = $dados['QT_EMPREGADOS'];
  $qtdEmpregadosEstrangeiro = $dados['QT_EMPREGADOS_ESTRANGEIROS'];
  $DT_CADASTRO_BC = $dados['DT_CADASTRO_BC'];
  $nmCargoAdmin = $dados['NO_FUNCAO'];
  $FL_ATIVA = $dados['FL_ATIVA'];
  $tbpc_id = $dados['tbpc_id'];
  $usua_id_responsavel = $dados['usua_id_responsavel'];
  $usua_id_responsavel2 = $dados['usua_id_responsavel2'];
  $usua_id_responsavel3 = $dados['usua_id_responsavel3'];
  $usua_id_responsavel4 = $dados['usua_id_responsavel4'];
  $usua_id_responsavel5 = $dados['usua_id_responsavel5'];

  $dataConstituicaoEmpresa = dataMy2BR($dataConstituicaoEmpresa);
  $dataAlteracaoContratual = dataMy2BR($dataAlteracaoContratual);
  $dataInvestimentoEstrangeiro = dataMy2BR($dataInvestimentoEstrangeiro);
  $DT_CADASTRO_BC = dataMy2BR($DT_CADASTRO_BC);

   if( (strlen($uf)>0) ) {  $seluf = "";  } else { $seluf=$selecione; }
   if( (strlen($cargoAdmin)>0) && ($cargoAdmin>0) ) {  $selcargo = "";  }  else { $selcargo=$selecione; }
}

$cmbEstados = $seluf.montaComboEstados($uf);
$cmbCargoAdmin = $selcargo.montaComboFuncoes($cargoAdmin,"");

echo Topo("");
echo Menu("EMP");

if(! Acesso::permitido(Acesso::tp_Cadastro_EmpresaAlterar)){
	$acao = "V";
}


?>
<br><center>
<table id="TEmpresa" border=0 width="800">
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Dados da Empresa ::</strong></p>				
  </td>
 </tr>
</table>
<br>
<table border=0 align="center" width="800" class="textoazulPeq">
<?php
if($acao!="V") {
  echo "<tr><td colspan=5 class='textoazul'>Preencha os dados abaixo para o cadastro da empresa</font></td></tr>\n";
}
?>
 <tr><td colspan="5"><font color="Red">*</font>&nbsp;Campos obrigat&oacute;rios</font></td></tr>
 <tr><td width=190>&#160;</td><td width=210>&#160;</td><td width=10>&#160;</td><td width=180>&#160;</td><td width=210>&#160;</td></tr>

 <form action="insereEmpresa.php" method="post" onSubmit="return validarDados(this)">
 <input type="hidden" name="id" value="<?=$id?>">

<?php
include("../inc/empresa_inc.php");
?>

<tr><td colspan=5>&#160;</td></tr>

<tr>
 <td colspan=5 align=center>
<?php
if($acao == "V") {
   echo "<input type='button' class='textformtopo' style=\"$estilo\" value='Fechar' onclick='javascript:window.close();'>\n";
} else {
   echo "<input name='salvar' type='Submit' class='textformtopo' style=\"$estilo\" value='Salvar'>\n";
}
?>				
 </td>
</tr>
</table>
<form action="empresas.php" method=post>
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
</form>

<?php
echo Rodape($opcao);
?>

