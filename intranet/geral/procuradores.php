<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");

$acao = $_POST['acao'];
$idEmpresa = $_POST['idEmpresa'];
$idProcurador = $_POST['idProcurador'];

if( ($acao=="R") && (strlen($idProcurador)>0) ) {
  $sql = "delete from PROCURADOR_EMPRESA where NU_PROCURADOR=$idProcurador and NU_EMPRESA = $idEmpresa";
  mysql_query($sql);
  if(mysql_errno()>0) {
    gravaLogErro($sql,mysql_error(),"REMOCAO","PROCURADOR");
  } else {
    gravaLog($sql,mysql_error(),"REMOCAO","PROCURADOR");
  }
  $idProcurador = "";
}

if( strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $sql = "SELECT * FROM PROCURADOR_EMPRESA where NU_EMPRESA=$idEmpresa";
  $linhas = mysql_query($sql);
} else {
  $msg = "Não foi informado a empresa.";
}

echo Topo("");
echo Menu("");

?>
<div class="conteudo">
<br><center>
<table border=0 width=700  cellspacing=0>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="3">
   <p align="center" class="textoazul"><strong>:: Cadastro de Procuradores ::</strong></p>				
  </td>
 </tr>
 <tr><td colspan="3"><br><?=$msg?></td></tr>
 <tr>
  <td class="textoazul" NOWRAP>Empresa: <b><?=$nomeEmpresa?></b></a></td>
  <td width=200 align=center>

<input type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Novo procurador' onclick='javascript:inclui();'>

  </td>
  <td width=200 align=center>
   <input type="button" class="textformtopo" style="<?=$estilo?>" value="Voltar Empresa" onclick="javascript:empresas();">
  </td>
 </tr>
</table>
<br>
<table border=1 width=700 class="textoazulpeq">
 <tr align="center" height=20>
  <td width=150 bgcolor="#DADADA">A&ccedil;&atilde;o</td>
  <td width=250 bgcolor="#DADADA">Nome</td>
  <td width=250 bgcolor="#DADADA">Cargo / Fun&ccedil;&atilde;o</td>
  <!-- td width=150 bgcolor="#DADADA">Empresa</td -->
</tr>

<?php		   
while($rw1=mysql_fetch_array($linhas)) {
  $cargoFuncao="";
  $idProcurador = $rw1["NU_PROCURADOR"];
  $nomeProcurador = $rw1["NO_PROCURADOR"];			
  $cargo = $rw1["CO_CARGO_FUNCAO"];
  if(strlen($cargo)>0) { $cargoFuncao = pegaNomeFuncao($cargo); } else { $cargoFuncao = "Não informado"; }
  echo "<tr height=20>";
  echo "<td align=center NOWRAP><a href='javascript:mostra($idProcurador,\"$nomeProcurador\");'>VER</a>";
  if($ehCliente!="S") {
    echo "&#160;&#160;<a href='javascript:altera($idProcurador);'>ALT</a>";
    if($ehAdmin=="S") {
      echo "&#160;&#160;<a href='javascript:remove($idProcurador,\"$nomeProcurador\");'>REM</a>";
    }
  }
  echo "</td><td NOWRAP>&#160;$nomeProcurador</td>";
  echo "<td NOWRAP>&#160;$cargoFuncao</td>";
#  echo "<td>&#160;$nomeEmpresa</td>";
  echo "</tr>";
}
?>
		
</table>
</center>
</div>
<form name=procurador method=post>
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
<input type=hidden name=idProcurador>
<input type=hidden name=acao>
</form>

<form name=empresa method=post action="empresas.php">
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
</form>

<script language="javascript">
function mostra(codigo) {
  document.procurador.idProcurador.value=codigo;
  document.procurador.acao.value="V";
  document.procurador.target="proc_"+codigo;
  document.procurador.action="novoProcurador.php";
  document.procurador.submit();
}
function inclui() {
  document.procurador.idProcurador.value='';
  document.procurador.acao.value="I";
  document.procurador.target="_top";
  document.procurador.action="novoProcurador.php";
  document.procurador.submit();
}
function altera(codigo) {
  document.procurador.idProcurador.value=codigo;
  document.procurador.acao.value="A";
  document.procurador.target="_top";
  document.procurador.action="novoProcurador.php";
  document.procurador.submit();
}
function remove(codigo,nome) {
  if(confirm("Voce deseja remover o procurador "+nome+" ? ")) {
    document.procurador.idProcurador.value=codigo;
    document.procurador.acao.value="R";
    document.procurador.target="_top";
    document.procurador.action="procuradores.php";
    document.procurador.submit();
  } else {
    alert("Operacao nao realizada.");
  }
}
function empresas() {
  document.empresa.submit();
}
</script>

<?php
echo Rodape($opcao);
?>
