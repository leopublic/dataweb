<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");

//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
$_SESSION['voltar'] = "os_listar.php";

$primeiraVez = $_GET['primeiravez'];

ValorParametro($_SESSION, $_POST, 'NU_EMBARCACAO_PROJETO', $pNU_EMBARCACAO_PROJETO);

$fNome = new cFILTRO_TEXTO('Nome do candidato', 'NOME_COMPLETO');

$fEmpresa = new cFILTRO_CHAVE('Empresa (processo)', cFILTRO_CHAVE::enEMPRESA, '', 'onChange="javascript:handleOnChange(this);" class="formulario"');
$matrizEmpresa = new cFILTRO_CHAVE('Empresa (processo)', cFILTRO_CHAVE::enEMPRESA, '', 'class="formulario"', '', '999');


$fStatus = new cFILTRO_CHAVE('Status OS', cFILTRO_CHAVE::enSTATUS_SOLICITACAO, '', '', '', '', 'sv');
$fStatusCob = new cFILTRO_CHAVE('Status cobrança', cFILTRO_CHAVE::enSTATUS_COBRANCA, '', '', '', '', 'sv');
$fResp = new cFILTRO_CHAVE('Responsável', cFILTRO_CHAVE::enRESPONSAVEL, 'cd_tecnico');
$fServico = new cFILTRO_CHAVE('Tipo de serviço', cFILTRO_CHAVE::enSERVICO, '', '', '', '', 'sv');
$fCadastradaPor = new cFILTRO_CHAVE('Cadastrada por', cFILTRO_CHAVE::enCADASTRADOPOR, 'cd_admin_cad');
$fNumSol = new cFILTRO_VALOR('Número OS', 'NU_SOLICITACAO', cFILTRO_VALOR::tpSEL_IGUAL, '', '', 'sv');
$fno_solicitador = new cFILTRO_TEXTO('Solicitada por', 'no_solicitador');
//$fDataCad = new cFILTRO_DATA('Data de cadastro', 'DT_CADASTRAMENTO');


$fNome->ObterValorInformado($_POST, $_SESSION);
$fEmpresa->ObterValorInformado($_POST, $_SESSION);
$fStatus->ObterValorInformado($_POST, $_SESSION);
$fStatusCob->ObterValorInformado($_POST, $_SESSION);
$fResp->ObterValorInformado($_POST, $_SESSION);
$fServico->ObterValorInformado($_POST, $_SESSION);
$fCadastradaPor->ObterValorInformado($_POST, $_SESSION);
$fNumSol->ObterValorInformado($_POST, $_SESSION);
$fno_solicitador->ObterValorInformado($_POST, $_SESSION);

$virgula = '';
if (intval($fEmpresa->valorInformado()) > 0) {
    $valores_NU_EMPRESA = $fEmpresa->valorInformado();
    $virgula = ',';
}


$empresas = array();
$valores = "";

if (isset($_POST['enviado'])) {
    $_SESSION['qtdEmpresas'] = $_POST['qtdEmpresas'];
    // Zera campos se foram retirados
    for ($index1 = 1; $index1 <= 10; $index1++) {
        if (!isset($_POST['cFILTRO_NU_EMPRESA' . $index1])) {
            unset($_SESSION['cFILTROcFILTRO_NU_EMPRESA' . $index1]);
        }
    }
}

for ($x = 1; $x <= intval($_SESSION['qtdEmpresas']); $x++) {
    if (isset($_POST['cFILTRO_NU_EMPRESA' . $x]) || isset($_SESSION['cFILTROcFILTRO_NU_EMPRESA' . $x])) {
        $xEmpresa = new cFILTRO_CHAVE('Empresa (processo)', cFILTRO_CHAVE::enEMPRESA, '', 'class="formulario" ', '', $x);
        $xEmpresa->ObterValorInformado($_POST, $_SESSION);
        if (intval($xEmpresa->valorInformado()) > 0 && !strstr($valores, '#' . $xEmpresa->valorInformado() . '#')) {
            if ($xEmpresa->getInd() != count($empresas) + 1) {
                unset($_SESSION['cFILTROcFILTRO_NU_EMPRESA' . $xEmpresa->getInd()]);
                $xEmpresa->setInd(count($empresas) + 1);
                $_SESSION['cFILTROcFILTRO_NU_EMPRESA' . $xEmpresa->getInd()] = $xEmpresa->valorInformado();
            }

            $empresas[] = $xEmpresa;
            $valores_NU_EMPRESA .= $virgula . $xEmpresa->valorInformado();
            $valores.= "#" . $xEmpresa->valorInformado() . "#";
            $virgula = ',';
        }
    }
}

//$fDataCad->ObterValorInformado($_POST, $_SESSION);
$msg = '';


if ($fEmpresa->valorInformado() > 0) {
    $cmbProjetos = '<option value="">Selecione...</option>' . montaComboProjetos($pNU_EMBARCACAO_PROJETO, $fEmpresa->valorInformado(), " ORDER BY embp_fl_ativo desc, NO_EMBARCACAO_PROJETO");
} else {
    $cmbProjetos = '<option value="">(selecione a empresa)</option>';
}
if ($fNumSol->valorInformado() != '' && !is_numeric($fNumSol->valorInformado())) {
    $msg = 'Não é possível pesquisar por nome no número da OS. Revise o preenchimento dos filtros!';
}

if ($msg == '') {
    $where = "";
    if ($valores_NU_EMPRESA != '') {
        $where.= ' and sv.NU_EMPRESA in (' . $valores_NU_EMPRESA . ')';
    }
    $where.=$fStatus->Where();
    $where.=$fStatusCob->Where();
    $where.=$fResp->Where();
    $where.=$fServico->Where();
    $where.=$fNome->Where();
    $where.=$fCadastradaPor->Where();
    $where.=$fNumSol->Where();

    $where.=$fno_solicitador->Where();
    //$where.=$fDataCad->Where();

    $data_cad = '';
    if (isset($_POST['cFILTRO_DATA_CADASTRO'])) {
        $data_cad = $_POST['cFILTRO_DATA_CADASTRO'];
        $_SESSION['cFILTRO_DATA_CADASTRO'] = $data_cad;
    } else {
        if (isset($_SESSION['cFILTRO_DATA_CADASTRO'])) {
            $data_cad = $_SESSION['cFILTRO_DATA_CADASTRO'];
        }
    }
    $data_cad_fim = '';
    if (isset($_POST['cFILTRO_DATA_CADASTRO_FIM'])) {
        $data_cad_fim = $_POST['cFILTRO_DATA_CADASTRO_FIM'];
        $_SESSION['cFILTRO_DATA_CADASTRO_FIM'] = $data_cad_fim;
    } else {
        if (isset($_SESSION['cFILTRO_DATA_CADASTRO_FIM'])) {
            $data_cad_fim = $_SESSION['cFILTRO_DATA_CADASTRO_FIM'];
        }
    }
    if ($data_cad != '') {
        $data = DateTime::createFromFormat('d/m/Y', $data_cad);
        $xdata_cad = $data->format('Y-m-d');
        if ($data_cad_fim != '') {
            $data_fim = DateTime::createFromFormat('d/m/Y', $data_cad_fim);
            $xdata_cad_fim = $data_fim->format('Y-m-d');
            $where .= " and sv.DT_CADASTRO >= '$xdata_cad' and sv.DT_CADASTRO <= '$xdata_cad_fim'";
        } else {
            $data = DateTime::createFromFormat('d/m/Y', $data_cad);
            $xdata_cad = $data->format('Y-m-d');
            $where .= " and sv.DT_CADASTRO = '$xdata_cad'";
        }
    }

    if (strlen($pNU_EMBARCACAO_PROJETO) > 0) {
        $where .= " and sv.NU_EMBARCACAO_PROJETO=$pNU_EMBARCACAO_PROJETO";
    }

    $sql1 = "select candidato.NU_CANDIDATO
	,candidato.NOME_COMPLETO
	,candidato.NU_CPF
	,candidato.NU_CTPS
	,candidato.NU_CNH
	,candidato.bo_cadastro_minimo_ok
	,date_format(candidato.DT_EXPIRACAO_CNH,'%d/%m/%Y') AS DT_EXPIRACAO_CNH
	,date_format(candidato.DT_EXPIRACAO_CTPS,'%d/%m/%Y') AS DT_EXPIRACAO_CTPS
	,b.ID_AUTORIZACAO_CANDIDATO
	,b.NU_EMBARCACAO_INICIAL
	,b.DT_SITUACAO_SOL
	,b.CD_EMBARCADO
	,b.autc_fl_revisado
	,b.cd_usuario_revisao
	,date_format(sv.DT_CADASTRO,'%d/%m/%y') AS DT_CADASTRO
	,sv.NU_EMPRESA
	,sv.NU_EMBARCACAO_PROJETO
	,sv.id_solicita_visto
	,sv.NU_SERVICO
	,sv.nu_solicitacao
	,sv.cd_admin_cad
	,sv.ID_STATUS_SOL
	,sv.de_observacao
	,sv.no_solicitador
	,sv.NU_EMBARCACAO_PROJETO_COBRANCA
	,sv.tppr_id
	,sv.soli_dt_devolucao soli_dt_devolucao_orig
	,date_format(sv.soli_dt_devolucao,'%d/%m/%Y') AS soli_dt_devolucao
	,date_format(sv.dt_solicitacao,'%d/%m/%Y') AS dt_solicitacao
	,dt_solicitacao as dt_solicitacao_orig
	,sv.cd_tecnico
	,ep.NO_EMBARCACAO_PROJETO
	,ts.CO_TIPO_SERVICO
	,s.CO_SERVICO
	,s.NO_SERVICO
	,s.NO_SERVICO_RESUMIDO
	,s.ID_TIPO_ACOMPANHAMENTO
	,s.serv_fl_envio_bsb
	,s.serv_fl_revisao_analistas
	,u.nome AS NO_USUARIO
	,ut.nome AS NO_USUARIO_TECNICO
	,e.NO_RAZAO_SOCIAL
	,ss.NO_STATUS_SOL_RES
	,ss.NO_STATUS_SOL
	,epc.NO_EMBARCACAO_PROJETO AS NO_EMBARCACAO_PROJETO_CAND
	,epcob.NO_EMBARCACAO_PROJETO AS NO_EMBARCACAO_PROJETO_COBRANCA
	,rc.no_reparticao_consular
	,ur.usua_tx_apelido as nome_revisao
	,ud.usua_tx_apelido as nome_devolucao
	,sc.stco_tx_nome
        , 0 excluida
from solicita_visto sv
left join autorizacao_candidato b on b.id_solicita_visto = sv.id_solicita_visto
left join candidato on candidato.NU_CANDIDATO = b.NU_CANDIDATO
left join embarcacao_projeto ep on ep.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO and ep.NU_EMPRESA = sv.nu_empresa
left join servico s on s.NU_SERVICO = sv.NU_SERVICO
left join tipo_servico ts on ts.NU_TIPO_SERVICO = s.NU_TIPO_SERVICO
left join usuarios u on u.cd_usuario = sv.cd_admin_cad
left join usuarios ut on ut.cd_usuario = sv.cd_tecnico
left join usuarios ur on ur.cd_usuario = b.cd_usuario_revisao
left join usuarios ud on ud.cd_usuario = sv.cd_usuario_devolucao
left join empresa e on e.NU_EMPRESA = sv.nu_empresa
left join embarcacao_projeto epc on epc.NU_EMBARCACAO_PROJETO = candidato.NU_EMBARCACAO_PROJETO and epc.NU_EMPRESA = candidato.NU_EMPRESA
left join embarcacao_projeto epcob on epcob.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO_COBRANCA and epcob.NU_EMPRESA = sv.nu_empresa
left join status_solicitacao ss on ss.ID_STATUS_SOL = sv.ID_STATUS_SOL
left join status_cobranca_os sc on sc.stco_id = sv.stco_id
left join reparticao_consular rc on rc.co_reparticao_consular = b.co_reparticao_consular
where 1=1
    and coalesce(soli_fl_disponivel, 0) = 0
" . $where . "
union all
select candidato.NU_CANDIDATO
	,candidato.NOME_COMPLETO
	,candidato.NU_CPF
	,candidato.NU_CTPS
	,candidato.NU_CNH
	,candidato.bo_cadastro_minimo_ok
	,date_format(candidato.DT_EXPIRACAO_CNH,'%d/%m/%Y') AS DT_EXPIRACAO_CNH
	,date_format(candidato.DT_EXPIRACAO_CTPS,'%d/%m/%Y') AS DT_EXPIRACAO_CTPS
	,b.ID_AUTORIZACAO_CANDIDATO
	,b.NU_EMBARCACAO_INICIAL
	,b.DT_SITUACAO_SOL
	,b.CD_EMBARCADO
	,b.autc_fl_revisado
	,b.cd_usuario_revisao
	,date_format(b.DT_CADASTRAMENTO,'%d/%m/%y') AS DT_CADASTRAMENTO
	,sv.NU_EMPRESA
	,sv.NU_EMBARCACAO_PROJETO
	,sv.id_solicita_visto
	,sv.NU_SERVICO
	,sv.nu_solicitacao
	,sv.cd_admin_cad
	,sv.ID_STATUS_SOL
	,sv.de_observacao
	,sv.no_solicitador
	,sv.NU_EMBARCACAO_PROJETO_COBRANCA
	,sv.tppr_id
	,sv.soli_dt_devolucao soli_dt_devolucao_orig
	,date_format(sv.soli_dt_devolucao,'%d/%m/%Y') AS soli_dt_devolucao
	,date_format(sv.dt_solicitacao,'%d/%m/%Y') AS dt_solicitacao
	,dt_solicitacao as dt_solicitacao_orig
	,sv.cd_tecnico
	,ep.NO_EMBARCACAO_PROJETO
	,ts.CO_TIPO_SERVICO
	,s.CO_SERVICO
	,s.NO_SERVICO
	,s.NO_SERVICO_RESUMIDO
	,s.ID_TIPO_ACOMPANHAMENTO
	,s.serv_fl_envio_bsb
	,s.serv_fl_revisao_analistas
	,u.nome AS NO_USUARIO
	,ut.nome AS NO_USUARIO_TECNICO
	,e.NO_RAZAO_SOCIAL
	,ss.NO_STATUS_SOL_RES
	,ss.NO_STATUS_SOL
	,epc.NO_EMBARCACAO_PROJETO AS NO_EMBARCACAO_PROJETO_CAND
	,epcob.NO_EMBARCACAO_PROJETO AS NO_EMBARCACAO_PROJETO_COBRANCA
	,rc.no_reparticao_consular
	,ur.usua_tx_apelido as nome_revisao
	,ud.usua_tx_apelido as nome_devolucao
	,sc.stco_tx_nome
        , 1 excluida
from dataweb_log.solicita_visto sv
left join dataweb_log.autorizacao_candidato b on b.id_solicita_visto = sv.id_solicita_visto
left join candidato on candidato.NU_CANDIDATO = b.NU_CANDIDATO
left join embarcacao_projeto ep on ep.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO and ep.NU_EMPRESA = sv.nu_empresa
left join servico s on s.NU_SERVICO = sv.NU_SERVICO
left join tipo_servico ts on ts.NU_TIPO_SERVICO = s.NU_TIPO_SERVICO
left join usuarios u on u.cd_usuario = sv.cd_admin_cad
left join usuarios ut on ut.cd_usuario = sv.cd_tecnico
left join usuarios ur on ur.cd_usuario = b.cd_usuario_revisao
left join usuarios ud on ud.cd_usuario = sv.cd_usuario_devolucao
left join empresa e on e.NU_EMPRESA = sv.nu_empresa
left join embarcacao_projeto epc on epc.NU_EMBARCACAO_PROJETO = candidato.NU_EMBARCACAO_PROJETO and epc.NU_EMPRESA = candidato.NU_EMPRESA
left join embarcacao_projeto epcob on epcob.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO_COBRANCA and epcob.NU_EMPRESA = sv.nu_empresa
left join status_solicitacao ss on ss.ID_STATUS_SOL = sv.ID_STATUS_SOL
left join status_cobranca_os sc on sc.stco_id = sv.stco_id
left join reparticao_consular rc on rc.co_reparticao_consular = b.co_reparticao_consular
where 1=1 " . $where;

    $sql3 = " ORDER BY nu_solicitacao desc, NOME_COMPLETO asc ";
    $sql = $sql1 . $sql3;


    if ($where != '') {
        if ($linhas = mysql_query($sql)) {

        } else {
            print "<br><br><br><br>SQL=" . $sql . " Erro " . mysql_error();
        }
        if (mysql_errno() > 0) {
            $msg = mysql_error() . ' SQL=' . $sql;
        }
    } else {
        if(!$primeiraVez){
            $msg = "Selecione ao menos um filtro para exibir as OS.";
        }
    }
}
if ($msg != ''){
    $_SESSION['msg'] = $msg;
}

if (isset($_POST['enviado'])) {
    header("Location:os_listar.php");
    exit;
}

echo Topo("", '', true, true);
echo Menu();
?>
<style>
    table.grid tr.excluida td{background-color: #D3D3D3; color:#FFFFFF;}
</style>
<div class="conteudo">
    <div class="titulo">
        <div class="texto">Ordens de serviço</div>
        <button onclick="forms['filtro'].submit();" style="float: right;"><img src="/imagens/icons/zoom.png" /> Buscar</button>
    </div>
    <div class="conteudoInterno">

        <div style="background-color:#f3f3f3;padding:10px;margin:0;">
            <form action="os_listar.php" method="post" name="filtro">
                <input type="hidden" name="enviado" id="enviado" value="1">
                <input type="hidden" name="qtdEmpresas" id="qtdEmpresas" value="<?= count($empresas); ?>">
                <table  class="edicao" style="margin:0px;padding:0;width:100%;border:solid 0px black;text-align:left">
                    <tr>
                        <th style="width:13%;"><?= $fEmpresa->label(); ?></th>
                        <td style="width:37%;" id="tdEmpresa">
                            <div>
                                <img src="/imagens/grey16/Plus.png" style="width:auto;margin-top:0;margin-bottom:0;padding:0;margin-left:3px;margin-top:2px;float:right;cursor:pointer;" onClick="javascript:AdicionarEmpresa();"/>
                                <div style="width:auto;margin-right:20px;"><? print $fEmpresa->campoHTML(); ?></div>
                            </div>
                            <?
                            if (count($empresas) > 0) {
                                for ($index = 0; $index < count($empresas); $index++) {
                                    ?>
                                    <div id="div<?= $index + 1; ?>">
                                        <img src="/imagens/grey16/Trash.png" id="remove<?= $index + 1; ?>" style="width:auto;margin-top:0;margin-bottom:0;padding:0;margin-left:3px;margin-top:2px;float:right;" onclick="javascript:Remove(this);"/>
                                        <div style="width:auto;margin-right:20px;"><? print $empresas[$index]->campoHTML(); ?></div>
                                    </div>
                                    <?
                                }
                            }
                            ?>
                            <div id="matrizDiv" style="visibility:hidden;display:none;">
                                <img src="/imagens/grey16/Trash.png" id="remove" style="width:auto;margin-top:0;margin-bottom:0;padding:0;margin-left:3px;margin-top:2px;float:right;" onclick="javascript:Remove(this);"/>
                                <div style="width:auto;margin-right:20px;"><? print $matrizEmpresa->campoHTML(); ?></div>
                            </div>

                        </td>
                        <td  style="width:1%;">&nbsp;</td>
                        <th  style="width:13%;">Embarca&ccedil;&atilde;o (processo):</th>
                        <td style="width:36%;"><div id="tdEmbarcacaoProjeto"><select name="NU_EMBARCACAO_PROJETO" class="formulario"><?= $cmbProjetos ?></select></div></td>
                    </tr>
                    <tr>
                        <th width="220" nowrap><?= $fNumSol->label(); ?>:</th><td><?= $fNumSol->campoHTML(); ?></td>
                        <td >&nbsp;</td>
                        <th nowrap><?= $fStatus->label(); ?>:</td><td><?= $fStatus->campoHTML(); ?></td>
                    </tr>
                    <tr>
                        <th nowrap><?= $fNome->label(); ?>:</th><td><?= $fNome->campoHTML(); ?></td>
                        <td >&nbsp;</td>
                        <th nowrap><?= $fResp->label(); ?>:</th><td><?= $fResp->campoHTML(); ?></td>
                    </tr>
                    <tr>
                        <th nowrap><?= $fCadastradaPor->label(); ?>:</th><td><?= $fCadastradaPor->campoHTML(); ?></td>
                        <td >&nbsp;</td>
                        <th nowrap>Data de Cadastro:</th><td><input type="text" name="cFILTRO_DATA_CADASTRO" class="data" style="width: 100px;" value="<?= $data_cad; ?>" /> até <input type="text" name="cFILTRO_DATA_CADASTRO_FIM" class="data" style="width: 100px;" value="<?= $data_cad_fim; ?>" /></td>
                    </tr>
                    <tr>
                        <th nowrap><?= $fServico->label(); ?>:</th><td><?= $fServico->campoHTML(); ?></td>
                        <td >&nbsp;</td>
                        <th nowrap><?= $fno_solicitador->label(); ?>:</th><td><?= $fno_solicitador->campoHTML(); ?></td>
                    </tr>
                    <tr>
                        <th nowrap><?= $fStatusCob->label(); ?>:</th><td><?= $fStatusCob->campoHTML(); ?></td>
                        <td >&nbsp;</td>
                        <th nowrap>&nbsp;</th><td>&nbsp;</td>
                    </tr>
                </table>
            </form>
        </div>

        <br>


        <?php
        $x = 0;
        $cdSolAnt = '';
        $candidato = '';
        if ($msg == '') {
            ?>
            <table border=0 class="grid">
                <colgroup>
                    <col width="10px"/>
                    <col width="80px"/>
                    <col width="80px"/>
                    <col width="50px"/>
                    <col width="200px"/>
                    <col width="auto"/>
                    <col width="200px"/>
                    <col width="auto"/>
                    <col width="200px"/>
                    <col width="60px"/>
                    <col width="200px"/>
                </colgroup>
                <thead>
                    <tr align="center" height=20>
                        <th class="esq">#</th>
                        <th>St.</th>
                        <th>St. Cobr</th>
                        <th>OS #</th>
                        <th style="text-align:left">Empresa (processo)</th>
                        <th width="160" class="esq">Projeto / Embarca&ccedil;&atilde;o (processo)</th>
                        <th class="esq">Candidato(s)</th>
                        <th width="160" class="esq">Projeto / Embarca&ccedil;&atilde;o (Candidato)</th>
                        <th class="esq">Serviço</th>
                        <th>Criada em</th>
                        <th style="text-align:left">Responsável</th>
                        <th style="text-align:left">Arquivos</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $status = '';
                    $candidato = '';
                    $solicit = '';
                    $NO_USUARIO = '';
                    $dataCad = '';
                    $servico = '';
                    $nomeProjetoEmbarcacao = '';
                    $nomeProjetoEmbarcacaoCand = '';
                    $NO_USUARIO_TECNICO = '';
                    $empresa = '';
                    $cdSolAnt = '';
                    $excluida = '';
                    $id_solicita_visto = '';
                    $i = 0;
                    while ($rw1 = mysql_fetch_array($linhas)) {

                        if ($cdSolAnt == '') {
                            $cdSolAnt = $rw1['nu_solicitacao'];
                        }
                        if ($rw1['nu_solicitacao'] != $cdSolAnt || $rw1['nu_solicitacao'] == 0) {
                            $i = $i + 1;
                            // Imprime o registro anterior
                            if ($excluida > 0) {
                                echo '<tr width="220" height="20" class="excluida" >' . "\n";
                            } else {
                                echo '<tr width="220" height="20" >' . "\n";
                            }
//	echo '<tr width="220" height="20" onmouseover="this.style.backgroundColor=\'#dfe4ff\';" onmouseout="this.style.backgroundColor=\'#ffffff\';">'."\n";
                            echo "<td align=left>$i</td>\n";
                            echo "<td align=center>$status</td>\n";
                            echo "<td align=center>$status_cobr</td>\n";
                            echo "<td align=center>$solicit</td>\n";
                            echo "<td>$empresa</td>\n";
                            echo "<td>$nomeProjetoEmbarcacao</td>\n";
                            echo "<td>$candidato</td>";
                            echo "<td>$nomeProjetoEmbarcacaoCand</td>\n";
                            echo "<td>$servico</td>\n";
                            echo "<td align=center>$dataCad</td>\n";
                            echo "<td>$NO_USUARIO_TECNICO</td>\n";
                            echo '<td><input type="button" value="Adicionar novo" onclick="window.open(\'/paginas/carregueComEstilos.php?controller=cCTRL_ARQUIVO&metodo=getAdicionarArquivoOs&id_solicita_visto='.$id_solicita_visto.'&nu_solicitacao='.$nu_solicitacao.'\', \'novoArquivo\', \'width=450,height=500\');" style="width:auto; padding: 4px; margin-right:10px;"/></td>';
                            echo "</tr>\n";
                            $candidato = '';
                            $contCand = 0;
                            $cdSolAnt = $rw1['nu_solicitacao'];
                        }
                        // Salva o registro lido
                        $id_solicita_visto = $rw1['id_solicita_visto'];
                        $nu_solicitacao = $rw1['nu_solicitacao'];
                        $candidato .= "<a href=\"/operador/detalheCandidatoAuto.php?NU_CANDIDATO=" . $rw1['NU_CANDIDATO'] . "\" class='textoazulpeq'>" . $rw1['NOME_COMPLETO'] . " (" . $rw1['NU_CANDIDATO'] . ")</a><br/>";
                        if ($rw1['ID_STATUS_SOL'] == 1) {
                            $icone = 'action_pendente';
                            $title = "Pendente";
                        }
                        if ($rw1['ID_STATUS_SOL'] == 1) {
                            $icone = 'action_finalizado';
                            $title = "Finalizado";
                        }
                        if ($rw1['ID_STATUS_SOL'] == 3) {
                            $icone = 'action_encerrado';
                            $title = "Encerrado";
                        }
                        $status = '';
                        /*
                          if ($rw1['ID_STATUS_SOL']==1){
                          $status = cINTERFACE::iconPendente();
                          } elseif ($rw1['ID_STATUS_SOL']==2){
                          $status = cINTERFACE::iconLiberada();
                          } elseif ($rw1['ID_STATUS_SOL']==3){
                          $status = cINTERFACE::iconDisponivel();
                          } elseif ($rw1['ID_STATUS_SOL']==4){
                          $status = cINTERFACE::iconFechada();
                          }
                         */
                        if ($rw1['excluida'] > 0) {
                            $status = '<strong>Excluída</strong>';
                        } else {
                            $status = '<strong>' . $rw1['NO_STATUS_SOL_RES'] . '</strong>';
                        }
                        $status_cobr = $rw1['stco_tx_nome'];
                        $dataCad = $rw1['DT_CADASTRO'];
                        $NO_USUARIO = $rw1['NO_USUARIO'];
                        $NO_USUARIO_TECNICO = $rw1['NO_USUARIO_TECNICO'];
                        $empresa = $rw1['NO_RAZAO_SOCIAL'];
                        if ($rw1['NU_SERVICO'] == '') {
                            $servico = '-';
                        } else {
                            $servico = $rw1['NO_SERVICO_RESUMIDO'];
                        }
                        $excluida = $rw1['excluida'];
                        $nomeProjetoEmbarcacao = $rw1['NO_EMBARCACAO_PROJETO'];
                        $nomeProjetoEmbarcacaoCand = $rw1['NO_EMBARCACAO_PROJETO_CAND'];
                        if ($rw1['excluida'] > 0) {
                            $solicit = '<a href="#" onclick="javascript:alert(\'Essa OS foi excluída e não pode mais ser aberta.\');" class="textoazulpeq">' . $rw1['nu_solicitacao'] . '</a>';
                        } else {
                            if ($rw1['NU_SOLICITACAO'] > 0) {
                                $solicit = '<a href="/operador/os_DadosOS.php?ID_SOLICITA_VISTO=' . $rw1['id_solicita_visto'] . '" class="textoazulpeq">' . $rw1['nu_solicitacao'] . '</a>';
                            } else {
                                //			$solicit = '<a href="/operador/os_Processo.php?ID_SOLICITA_VISTO='.$rw1['id_solicita_visto'].'&NU_CANDIDATO='.$rw1['NU_CANDIDATO'].'" class="textoazulpeq">'.$rw1['nu_solicitacao'].'</a>';
                                $solicit = '<a href="/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso&id_solicita_visto=' . $rw1['id_solicita_visto'] . '&NU_CANDIDATO=' . $rw1['NU_CANDIDATO'] . '" class="textoazulpeq">' . $rw1['nu_solicitacao'] . '</a>';
                            }
                        }
                    }
                    // Mudou!
                    // Imprime o registro anterior
                    $i = $i + 1;
                    echo '<tr height=20 style="height:20px" onmouseover="this.style.backgroundColor=\'#dfe4ff\';" onmouseout="this.style.backgroundColor=\'#ffffff\';">' . "\n";
                    echo "<td align=left>$i</td>\n";
                    echo "<td height=20  align=center>$status</td>\n";
                    echo "<td height=20  align=center>$status_cobr</td>\n";
                    echo "<td height=20 align=center>$solicit</td>\n";
                    echo "<td height=20>$empresa</td>\n";
                    echo "<td height=20>$nomeProjetoEmbarcacao</td>\n";
                    echo "<td height=20>$candidato</td>";
                    echo "<td height=20>$nomeProjetoEmbarcacaoCand</td>\n";
                    echo "<td height=20 align=left>$servico</td>\n";
                    echo "<td height=20 align=center>$dataCad</td>\n";
                    echo "<td height=20 align=left>$NO_USUARIO_TECNICO</td>\n";
                    echo '<td><input type="button" value="Adicionar novo" onclick="window.open(\'/paginas/carregueComEstilos.php?controller=cCTRL_ARQUIVO&metodo=getAdicionarArquivoOs&id_solicita_visto='.$id_solicita_visto.'&nu_solicitacao='.$nu_solicitacao.'\', \'novoArquivo\', \'width=450,height=300\');" style="width:auto; padding: 4px; margin-right:10px;"/></td>';
                    echo "</tr>\n";
                    $candidato = '';
                    $contCand = 0;
                    $cdSolAnt = $rw1['nu_solicitacao'];

                    echo "<input type=hidden name=totcand value='$x'>";
                    echo "</tbody></table>";
                } else {
                    echo "<h2>" . $msg . "</h2>";
                }


                if (isset($_SESSION['msg']) && $_SESSION['msg'] != '') {
                    $mensagem.="\njAlert('" . $_SESSION['msg'] . "');";
                    $_SESSION['msg'] = '';
                }
                ?>
            <p class=textoazul><b>
                    <span id=aguarde class='textoazul'></span><br><span id=aguardeerr class='textoazul'></span>
            </p>
    </div>
</div>

<script language="javascript">
    var iEmpresa = <?= count($empresas); ?>;
    $(document).ready(function () {
<?= $mensagem; ?>

        $("input").bind("keydown", function (event) {
            // track enter key
            var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
            if (keycode == 13) { // keycode for enter key
                // force the 'Enter Key' to implicitly click the Update button
                $(this).closest('form').submit();
                return false;
            } else {
                return true;
            }
        }); // end of function

    });

    function EmpresaAlterada(campo) {
        meuNome = $(campo).attr('name');
        ind = meuNome.charAt(meuNome.length - 1);
        if (iEmpresa == 0 || ind == iEmpresa) {
            if ($(this).value != '0') {
                if (iEmpresa > 0) {
                    $('#remove' + iEmpresa).remove();
                }
                else {
                    $('#NU_EMBARCACAO_PROJETO').val('');
                    $('#NU_EMBARCACAO_PROJETO').attr('disabled', 'disabled');
                }
                iEmpresa++;
                novo = $('#cFILTRO_NU_EMPRESA').clone(true);
                $(novo).attr('id', 'cFILTRO_NU_EMPRESA' + iEmpresa);
                $(novo).attr('name', 'cFILTRO_NU_EMPRESA' + iEmpresa);
                $(novo).css('width', '80%');
                $(novo).val(0);
                novo.appendTo('#tdEmpresa');
                $('#removeEmpresa').css('visibility', 'visible');
            }
        }
    }

    function AdicionarEmpresa() {
        if (iEmpresa > 0) {
            $('#NU_EMBARCACAO_PROJETO').val('');
            $('#NU_EMBARCACAO_PROJETO').attr('disabled', 'disabled');
        }
        iEmpresa++;
        div = $('#matrizDiv').clone(true);

        $(div).attr('id', 'div' + iEmpresa);
        $(div).css('visibility', 'visible');
        $(div).css('display', 'block');
        $(div).find('#cFILTRO_NU_EMPRESA999').attr('id', 'cFILTRO_NU_EMPRESA' + iEmpresa);
        $(div).find('#cFILTRO_NU_EMPRESA' + iEmpresa).attr('name', 'cFILTRO_NU_EMPRESA' + iEmpresa);
        $(div).find('#cFILTRO_NU_EMPRESA' + iEmpresa).val(0);
        $(div).children('#remove').attr('id', 'remove' + iEmpresa);
        div.appendTo('#tdEmpresa');
        $('#qtdEmpresas').val(iEmpresa);
    }

    function Remove(campo) {
        meuNome = $(campo).attr('id');
        ind = meuNome.charAt(meuNome.length - 1);
        $('#div' + ind).remove();
    }



    var idEmpresa = "<?= $fEmpresa->valorInformado(); ?>";
    var NU_EMPRESA = "<?= $fEmpresa->valorInformado(); ?>";

    var idEmbarcacaoProjeto = "<?= $pNU_EMBARCACAO_PROJETO; ?>";
    var NU_EMBARCACAO_PROJETO = "<?= $pNU_EMBARCACAO_PROJETO; ?>";


    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        try {
            xmlhttp = new XMLHttpRequest();
        }
        catch (e) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && window.createRequest) {
        try {
            xmlhttp = window.createRequest();
        } catch (e) {
            xmlhttp = false;
        }
    }

    function handleOnChange(dd1)
    {
        var idx = dd1.selectedIndex;
        var val = dd1[idx].value;
        //alert ('valor='+val);
        var url = "/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA=" + val;
        //alert("url="+url);
        xmlhttp.open("GET", url, true);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                document.getElementById('tdEmbarcacaoProjeto').innerHTML = '<select name="NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>' + xmlhttp.responseText + '</select>'
            }
        }
        xmlhttp.send(null)
    }
</script>

<?php
echo Rodape("");
?>
