<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");

$id = $_POST['id'];
$razaoSocial = $_POST['razaoSocial'];
$atividadeEconomica = $_POST['atividadeEconomica'];
$cnpj = $_POST['cnpj'];
$endereco = $_POST['endereco'];
$complemento = $_POST['complemento'];
$bairro = $_POST['bairro'];
$municipio = $_POST['municipio'];
$uf = $_POST['uf'];
$cep = $_POST['cep'];
$ddd = $_POST['ddd'];
$telefone = $_POST['telefone'];
$email = $_POST['email'];
$nomeContato = $_POST['nomeContato'];
$emailContato = $_POST['emailContato'];
$nomeAdmin = $_POST['nomeAdmin'];
$cargoAdmin = $_POST['cargoAdmin'];
$objetoSocial = $_POST['objetoSocial'];
$capitalAtual = $_POST['capitalAtual'];
$capitalInicial = $_POST['capitalInicial'];
$dataConstituicaoEmpresa = $_POST['dataConstituicaoEmpresa'];
$dataAlteracaoContratual = $_POST['dataAlteracaoContratual'];
$nomeEmpresaEstrangeira = $_POST['nomeEmpresaEstrangeira'];
$investimentoEstrangeiro = $_POST['investimentoEstrangeiro'];
$dataInvestimentoEstrangeiro = $_POST['dataInvestimentoEstrangeiro'];
$qtdEmpregados = $_POST['qtdEmpregados'];
$qtdEmpregadosEstrangeiro = $_POST['qtdEmpregadosEstrangeiro'];
$DT_CADASTRO_BC = $_POST['DT_CADASTRO_BC'];
$tbpc_id = $_POST['CMP_tbpc_id'];
$usua_id_responsavel = $_POST['CMP_usua_id_responsavel'];
$usua_id_responsavel2 = $_POST['CMP_usua_id_responsavel2'];
$usua_id_responsavel3 = $_POST['CMP_usua_id_responsavel3'];
$usua_id_responsavel4 = $_POST['CMP_usua_id_responsavel4'];
$usua_id_responsavel5 = $_POST['CMP_usua_id_responsavel5'];
if (intval($usua_id_responsavel) > 0){
	$sql = "select nome from usuarios where cd_usuario = ".$usua_id_responsavel;
	$res = cAMBIENTE::$db_pdo->query($sql);
	$rw = $res->fetch(PDO::FETCH_BOTH);
	$usua_tx_nome_responsavel = $rw['nome'];
}
if (intval($usua_id_responsavel2) > 0){
	$sql = "select nome from usuarios where cd_usuario = ".$usua_id_responsavel2;
	$res = cAMBIENTE::$db_pdo->query($sql);
	$rw = $res->fetch(PDO::FETCH_BOTH);
	$usua_tx_nome_responsavel2 = $rw['nome'];
}
if (intval($usua_id_responsavel3) > 0){
	$sql = "select nome from usuarios where cd_usuario = ".$usua_id_responsavel3;
	$res = cAMBIENTE::$db_pdo->query($sql);
	$rw = $res->fetch(PDO::FETCH_BOTH);
	$usua_tx_nome_responsavel3 = $rw['nome'];
}
if (intval($usua_id_responsavel4) > 0){
	$sql = "select nome from usuarios where cd_usuario = ".$usua_id_responsavel4;
	$res = cAMBIENTE::$db_pdo->query($sql);
	$rw = $res->fetch(PDO::FETCH_BOTH);
	$usua_tx_nome_responsavel4 = $rw['nome'];
}
if (intval($usua_id_responsavel5) > 0){
	$sql = "select nome from usuarios where cd_usuario = ".$usua_id_responsavel5;
	$res = cAMBIENTE::$db_pdo->query($sql);
	$rw = $res->fetch(PDO::FETCH_BOTH);
	$usua_tx_nome_responsavel5 = $rw['nome'];
}

if (intval($tbpc_id) > 0){
	$sql = "select tbpc_tx_nome from tabela_precos where tbpc_id= ".$tbpc_id;
	$res = cAMBIENTE::$db_pdo->query($sql);
	$rw = $res->fetch(PDO::FETCH_BOTH);
	$tbpc_tx_nome = $rw['tbpc_tx_nome'];
}

if(Acesso::permitido(Acesso::tp_Cadastro_EmpresaAtivar)){
	if (isset($_POST['FL_ATIVA'])) {
		$FL_ATIVA = 1;
	} else {
		$FL_ATIVA = 0;
	}
}
else{
	$FL_ATIVA = $_POST['FL_ATIVA_VALOR'];
}
//$dataConstituicaoEmpresa = dataBR2My($dataConstituicaoEmpresa);
//$dataAlteracaoContratual = dataBR2My($dataAlteracaoContratual);
//$dataInvestimentoEstrangeiro = dataBR2My($dataInvestimentoEstrangeiro);
//$DT_CADASTRO_BC = dataBR2My($DT_CADASTRO_BC);

if ($id == null || $id == "") {
	$acao = "I";
	$consultaRazaoSocial = "select * from EMPRESA where NO_RAZAO_SOCIAL = '$razaoSocial' ";
	$resultado = mysql_query($consultaRazaoSocial);
	if (mysql_affected_rows() > 0) {
		$ret = "<script>window.alert('* Já existe uma Empresa cadastrada com esta Razão Social'); history.go(-1);</script>";
	}
	if($cnpj != ''){
		$consultaCNPJ = "select * from EMPRESA where NU_CNPJ = '$cnpj' ";
		$resultado = mysql_query($consultaCNPJ);
		if (mysql_affected_rows() > 0) {
			$ret = "<script>window.alert('* Já existe uma Empresa cadastrada com este CNPJ'); history.go(-1);</script>";
		}
		
	}
	if (strlen($ret) == 0) {
		$insert = "insert into EMPRESA (NO_RAZAO_SOCIAL, CO_ATIVIDADE_ECONOMICA, NU_CNPJ,
			NO_ENDERECO, NO_COMPLEMENTO_ENDERECO, NO_BAIRRO, NO_MUNICIPIO,CO_UF, NU_CEP , NU_DDD , NU_TELEFONE , NO_EMAIL_EMPRESA ,
			NO_CONTATO_PRINCIPAL , NO_EMAIL_CONTATO_PRINCIPAL , TE_OBJETO_SOCIAL ,VA_CAPITAL_ATUAL , DT_CONSTITUICAO_EMPRESA,
			DT_ALTERACAO_CONTRATUAL, NO_EMPRESA_ESTRANGEIRA, VA_INVESTIMENTO_ESTRANGEIRO, DT_INVESTIMENTO_ESTRANGEIRO,
			QT_EMPREGADOS , QT_EMPREGADOS_ESTRANGEIROS , DT_CADASTRAMENTO , CO_USUARIO_CADASTRAMENTO,VA_CAPITAL_INICIAL,
			DT_CADASTRO_BC, NO_ADMINISTRADOR, CO_CARGO_ADMIN, tbpc_id, usua_id_responsavel, usua_id_responsavel2, usua_id_responsavel3, usua_id_responsavel4, usua_id_responsavel5)
		values ('" . addslashes($razaoSocial) . "', '" . addslashes($atividadeEconomica) . "','$cnpj',
			'" . addslashes($endereco) . "','" . addslashes($complemento) . "','" . addslashes($bairro) . "','" . addslashes($municipio) . "','$uf','$cep','$ddd','$telefone','$email',
			'" . addslashes($nomeContato) . "','$emailContato','" . addslashes($objetoSocial) . "','$capitalAtual',".cBANCO::DataOk($dataConstituicaoEmpresa).",
			".cBANCO::DataOk($dataAlteracaoContratual).",'" . addslashes($nomeEmpresaEstrangeira) . "','$investimentoEstrangeiro',".cBANCO::DataOk($dataInvestimentoEstrangeiro).",
			".cBANCO::InteiroOk($qtdEmpregados).",".cBANCO::InteiroOk($qtdEmpregadosEstrangeiro).", sysdate(), '$usulogado','$capitalInicial',".cBANCO::DataOk($DT_CADASTRO_BC).",
			'" . addslashes($nomeAdmin) . "','$cargoAdmin', ". cBANCO::ChaveOk($tbpc_id).", ". cBANCO::ChaveOk($usua_id_responsavel).", ". cBANCO::ChaveOk($usua_id_responsavel2).",
			". cBANCO::ChaveOk($usua_id_responsavel3).", ". cBANCO::ChaveOk($usua_id_responsavel4).", ". cBANCO::ChaveOk($usua_id_responsavel5).") ";

		$resultado = mysql_query($insert);
		if (mysql_errno() > 0) {
			gravaLogErro($insert, mysql_error(), "INCLUSAO", "EMPRESA");
			$ret = "<script>window.alert('Ocorreu um erro durante a operação. Tente novamnte.');history.go(-1)</script>";
		} else {
			gravaLog($insert, mysql_error(), "INCLUSAO", "EMPRESA");
		}
	}
} else {
	$acao = "A";
	$update = "update EMPRESA set NO_RAZAO_SOCIAL ='" . addslashes($razaoSocial) . "',
								  CO_ATIVIDADE_ECONOMICA ='" . addslashes($atividadeEconomica) . "',
								  NU_CNPJ ='$cnpj', 
								  NO_ENDERECO ='" . addslashes($endereco) . "',
								  NO_COMPLEMENTO_ENDERECO ='" . addslashes($complemento) . "',
								  NO_BAIRRO = '" . addslashes($bairro) . "',
								  NO_MUNICIPIO ='" . addslashes($municipio) . "',
								  CO_UF ='$uf', 
								  NU_CEP = '$cep', 
								  NU_DDD = '$ddd', 
								  NU_TELEFONE = '$telefone', 
								  NO_EMAIL_EMPRESA ='$email', 
								  NO_CONTATO_PRINCIPAL = '" . addslashes($nomeContato) . "',
								  NO_EMAIL_CONTATO_PRINCIPAL = '$emailContato', 
								  NO_ADMINISTRADOR = '" . addslashes($nomeAdmin) . "',
								  CO_CARGO_ADMIN = '$cargoAdmin', 
								  TE_OBJETO_SOCIAL ='" . addslashes($objetoSocial) . "',
								  VA_CAPITAL_ATUAL = '$capitalAtual', 
								  DT_CONSTITUICAO_EMPRESA = ".cBANCO::DataOk($dataConstituicaoEmpresa).",
								  DT_ALTERACAO_CONTRATUAL = ".cBANCO::DataOk($dataAlteracaoContratual)." ,
								  NO_EMPRESA_ESTRANGEIRA = '" . addslashes($nomeEmpresaEstrangeira) . "',
								  VA_INVESTIMENTO_ESTRANGEIRO ='$investimentoEstrangeiro', 
								  DT_INVESTIMENTO_ESTRANGEIRO = ".cBANCO::DataOk($dataInvestimentoEstrangeiro).",
								  QT_EMPREGADOS = ".cBANCO::InteiroOk($qtdEmpregados).",
								  QT_EMPREGADOS_ESTRANGEIROS = ".cBANCO::InteiroOk($qtdEmpregadosEstrangeiro).",
								  DT_CADASTRAMENTO = sysdate(), 
								  CO_USUARIO_CADASTRAMENTO= '$usulogado',
								  DT_CADASTRO_BC=".cBANCO::DataOk($DT_CADASTRO_BC).",
								  VA_CAPITAL_INICIAL='$capitalInicial',
								  tbpc_id=".cBANCO::ChaveOk($tbpc_id).",
								  usua_id_responsavel=".cBANCO::ChaveOk($usua_id_responsavel).",
								  usua_id_responsavel2=".cBANCO::ChaveOk($usua_id_responsavel2).",
								  usua_id_responsavel3=".cBANCO::ChaveOk($usua_id_responsavel3).",
								  usua_id_responsavel4=".cBANCO::ChaveOk($usua_id_responsavel4).",
								  usua_id_responsavel5=".cBANCO::ChaveOk($usua_id_responsavel5)."
			";
	if (Acesso::permitido(Acesso::tp_Cadastro_EmpresaAtivar)) {
		$update .= " , FL_ATIVA=$FL_ATIVA";
	}
	$update .= " where NU_EMPRESA = $id";

	$resultado = mysql_query($update);
	if (mysql_errno() > 0) {
		gravaLogErro($update, mysql_error(), "ALTERACAO", "EMPRESA");
		$ret = "<script>window.alert('Ocorreu um erro durante a operação. Tente novamente.'); history.go(-1);</script>";
		$resultado = mysql_query($consultaRazaoSocial);
		if (mysql_affected_rows() > 0) {
			
		} else {
			gravaLog($update, mysql_error(), "ALTERACAO", "EMPRESA");
		}
	}
}
$dataConstituicaoEmpresa = dataMy2BR($dataConstituicaoEmpresa);
$dataAlteracaoContratual = dataMy2BR($dataAlteracaoContratual);
$dataInvestimentoEstrangeiro = dataMy2BR($dataInvestimentoEstrangeiro);
$DT_CADASTRO_BC = dataMy2BR($DT_CADASTRO_BC);
$nmCargoAdmin = pegaNomeFuncao($cargoAdmin);

if (strlen($ret) > 0) {
	echo $ret;
} else {
	echo Topo("");
	echo Menu("");
	$acao = "V";
	?>

	<br><center>
		<table border=0 width="800">
			<tr>
				<td class="textobasico" align="center" valign="top">
					<p align="center" class="textoazul"><strong>:: Dados da Empresa ::</strong></p>
				</td>
			</tr>
		</table>
		<br>
		<table border=0 align="center" width="800" class="textoazulPeq">
			<tr><td colspan="5" class="textoazul">Efetuado o cadastro da empresa</font></td></tr>
			<tr><td colspan="5"><font color="Red">*</font>&nbsp;Campos obrigatórios</font></td></tr>
			<tr><td width=190>&#160;</td><td width=210>&#160;</td><td width=10>&#160;</td><td width=180>&#160;</td><td width=210>&#160;</td></tr>

	<?php
	include("../inc/empresa_inc.php");
	?>

			<tr><td colspan=5>&#160;</td></tr>

			<form action="empresas.php" method="post" name="voltarempresa">
				<input type=hidden name=idEmpresa value='<?= $idEmpresa ?>'>
			</form>
			<tr>
				<td colspan=5 align=center>
					<input type='button' class='textformtopo' style="<?= $estilo ?>" value='Voltar' onclick='javascript:document.voltarempresa.submit();'>
				</td>
			</tr>
		</table>

	<?php
	echo Rodape("");
}
