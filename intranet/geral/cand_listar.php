<?php

$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");

//ini_set('error_reporting', E_ALL); 
//ini_set('display_errors', 1);
/*
function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler("exception_error_handler");
*/

$_SESSION['voltar'] = "cand_listar.php";
$x = '';
ValorParametro($_SESSION, $_POST, 'NU_EMPRESA', $pNU_EMPRESA, "PESQ_CAND");
ValorParametro($_SESSION, $_POST, 'NU_EMBARCACAO_PROJETO', $pNU_EMBARCACAO_PROJETO, "PESQ_CAND");

$fNome = new cFILTRO_TEXTO('Nome do candidato', 'NOME_COMPLETO');
$fEmpresa = new cFILTRO_CHAVE('Empresa', cFILTRO_CHAVE::enEMPRESA, '','onChange="javascript:handleOnChange(this);" class="formulario"');

if (isset($_POST['FL_INATIVO'])){
	$FL_INATIVO = trim($_POST['FL_INATIVO']);
	$_SESSION['cFILTROFL_INATIVO']=$FL_INATIVO;
}
else{
	$FL_INATIVO = 1;
	if (isset($_SESSION['cFILTROFL_INATIVO'])){
		$FL_INATIVO = $_SESSION['cFILTROFL_INATIVO'];
	}
}


$fNome->ObterValorInformado($_POST, $_SESSION, "PESQ_CAND");
$fEmpresa->ObterValorInformado($_POST, $_SESSION, "PESQ_CAND");
$qtdInformados = 0;
if($fEmpresa->valorInformado() > 0) {
  $cmbProjetos = '<option value="">Selecione...</option>'.montaComboProjetos($pNU_EMBARCACAO_PROJETO,$fEmpresa->valorInformado()," ORDER BY NO_EMBARCACAO_PROJETO");
	$qtdInformados++;
}
else{
	$cmbProjetos = '<option value="">(selecione a empresa)</option>';
}

if($fNome->valorInformado() != ''){
	$qtdInformados++;
} 
if($pNU_EMBARCACAO_PROJETO > 0){
	$qtdInformados++;
}

if (isset($_POST['ordenacao']) && trim($_POST['ordenacao'])!='' ){
	$ordenacao = $_POST['ordenacao'];
}
else{
	$ordenacao = 'NOME_COMPLETO';
}

$ativoNOME_COMPLETO='';
$ativoNO_RAZAO_SOCIAL='';
$ativoNO_EMBARCACAO_PROJETO='';
$ativoNO_MAE='';
$ativoNO_NACIONALIDADE='';
$ativoNU_PASSAPORTE='';
$ativoNU_CPF='';
$ativoDT_VALIDADE_PASSAPORTE_ORIG='';
$ativoDDT_CADASTRAMENTO_ORIG='';

eval('$ativo'.$ordenacao." = '_ativo';");



$sql1 = "SELECT * from vCANDIDATO_CONS where 1=1 ";
$where = "";
$where.=$fNome->Where();

if($fEmpresa->valorInformado() > 0){
	$where .= " and NU_EMPRESA = ".$fEmpresa->valorInformado();
}
if(trim($fNome->valorInformado()) != ''){
	$where .= " and NOME_COMPLETO LIKE '%".$fNome->valorInformado()."%'";
}
if($pNU_EMBARCACAO_PROJETO > 0){
	$where .= " and NU_EMBARCACAO_PROJETO=$pNU_EMBARCACAO_PROJETO";
}

if (isset ($_POST['enviado'])){
	header("Location:cand_listar.php");
	exit;
}



if (trim($where) != ''){
	if ($FL_INATIVO > 0){
		$where .= " and FL_INATIVO = ".($FL_INATIVO - 1);
	}
	$sql3 = " ORDER BY ".$ordenacao." asc, NOME_COMPLETO asc";
	$sql = $sql1.$where.$sql3;
	$msg = '';
	if ($linhas = mysql_query($sql)){  
		
	}
	else{
	  	print "<br><br><br><br>SQL=".$sql." Erro ".mysql_error();
	}
	if(mysql_errno()>0) {
	  	$msg = mysql_error().' SQL='.$sql;
	}	
}


echo Topo("", "", true, true);
echo Menu("SOL");
?>
<div class="conteudo">
	<div class="titulo"><div style="float:left">Candidatos</div>&nbsp;</div>
	<div class="conteudoInterno">				
		<div style="background-color:#dfe4ff;padding:0px;margin:0;">
			<form action="cand_listar.php" method="post" name="filtro">
				<input type="hidden" name="enviado" id="enviado" value="1">

				<table style="margin:0px;padding:0;width:100%;border:solid 1px black;">
			 		<tr>
						<td NOWRAP><?=$fEmpresa->label();?></td>
						<td><?=$fEmpresa->campoHTML();?></td>
						<td width="100px" rowspan="3">
							<input type="submit" class="textformtopo" value="Buscar">
						</td>
					</tr>
			 		<tr>
						<td nowrap>Projeto/Embarca&ccedil;&atilde;o:</td>
						<td ><div id="tdEmbarcacaoProjeto"><select name="NU_EMBARCACAO_PROJETO" class="formulario"><?=$cmbProjetos?></select></div></td>
					</tr>
			 		<tr><td nowrap><?=$fNome->label();?>:</td>
						<td><?=$fNome->campoHTML();?></td></tr>
					<tr><td>Situação cadastral:</td>
						<td><select id="FL_INATIVO"  name="FL_INATIVO" style="width:auto;">
								<option value="1" <?if($FL_INATIVO=='1'){print 'selected';}?>>somente ativos
								<option value="2" <?if($FL_INATIVO=='2'){print 'selected';}?>>somente inativos
								<option value="-1"  <?if($FL_INATIVO=='-1') {print 'selected';}?>>(todos)
							</select>
						</td>
					</tr>
				</table>
				<input type="hidden" name="ordenacao" id="ordenacao" value="<?=$ordenacao;?>"/>
			</form>
		</div>
		<br>
<?php
$x=0;
$cdSolAnt = '';
$candidato = '';
if ($msg=='')
{
?>	
<table width=100% class="grid">
	<thead>
		<tr align="center" height=20>
		  <th class="esq">Nome (id)<img src="/imagens/icons/bullet_arrow_down<?=$ativoNOME_COMPLETO;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NOME_COMPLETO';document.forms[0].submit();"/></th>
		  <th class="esq">Empresa<img src="/imagens/icons/bullet_arrow_down<?=$ativoNO_RAZAO_SOCIAL;?>.png" style="border:0;margin:0;padding:0"  onclick="javascript:document.forms[0].ordenacao.value='NO_RAZAO_SOCIAL';document.forms[0].submit();"/></th>
		  <th class="esq">Embarcação/Projeto<img src="/imagens/icons/bullet_arrow_down<?=$ativoNO_EMBARCACAO_PROJETO;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NO_EMBARCACAO_PROJETO';document.forms[0].submit();"/></th>
		  <th class="esq">Nome mãe<img src="/imagens/icons/bullet_arrow_down<?=$ativoNO_MAE;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NO_MAE';document.forms[0].submit();"/></th>
		  <th class="esq">Nacionalidade<img src="/imagens/icons/bullet_arrow_down<?=$ativoNO_NACIONALIDADE;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NO_NACIONALIDADE';document.forms[0].submit();"/></th>
		  <th class="esq">CPF<img src="/imagens/icons/bullet_arrow_down<?=$ativoNU_CPF;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NU_CPF';document.forms[0].submit();"/></th>
		  <th>Passaporte<img src="/imagens/icons/bullet_arrow_down<?=$ativoNU_PASSAPORTE;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NU_PASSAPORTE';document.forms[0].submit();"/></th>
		  <th>Validade<img src="/imagens/icons/bullet_arrow_down<?=$ativoDT_VALIDADE_PASSAPORTE_ORIG;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='DT_VALIDADE_PASSAPORTE_ORIG';document.forms[0].submit();"/></th>
		  <th>Cadastrado em<img src="/imagens/icons/bullet_arrow_down<?=$ativoDT_CADASTRAMENTO_ORIG;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='DT_CADASTRAMENTO_ORIG';document.forms[0].submit();"/></th>
		</tr>
	</thead>
<?
	$status = '';
	$candidato = '';
	$solicit = '';
	$NO_USUARIO = '';
	$dataCad = '';
	$servico = '';
	$nomeProjetoEmbarcacao = '';
	$NO_USUARIO_TECNICO = '';
	if (isset($linhas)){
		while($rw1=mysql_fetch_array($linhas)) {
			$candidato = "<a href=\"/operador/detalheCandidatoAuto.php?NU_CANDIDATO=".$rw1['NU_CANDIDATO']."\" class='textoazulpeq'>".$rw1['NOME_COMPLETO']." (".$rw1['NU_CANDIDATO'].")</a>";
			
			echo '<tr height="20" onmouseover="this.style.backgroundColor=\'#dfe4ff\';" onmouseout="this.style.backgroundColor=\'#ffffff\';">'."\n";
			echo "<td align=left>".$candidato."</td>\n";
			echo "<td align=left>".$rw1['NO_RAZAO_SOCIAL']."</td>\n";
			echo "<td align=left>".$rw1['NO_EMBARCACAO_PROJETO']."</td>\n";
			echo "<td align=left>".$rw1['NO_MAE']."</td>\n";
			echo "<td align=left>".$rw1['NO_NACIONALIDADE']."</td>\n";
			echo "<td align=left>".$rw1['NU_CPF']."</td>\n";
			echo "<td align=center>".$rw1['NU_PASSAPORTE']."</td>\n";
			echo "<td align=center>".$rw1['DT_VALIDADE_PASSAPORTE']."</td>\n";
			echo "<td align=center>".$rw1['DT_CADASTRAMENTO']."</td>\n";
			echo "</tr>\n";
		}
		echo "<input type=hidden name=totcand value='$x'>";
	}
	echo "</table>";
}
?>
<p class=textoazul><b>
   <span id=aguarde class='textoazul'></span><br><span id=aguardeerr class='textoazul'></span>
</p>
</div>
</div>
<script language="javascript">
<?php 
if ($msg!=''){
?>
jAlert('<?=$msg;?>');
<?php 
}
?>


var idEmpresa = "<?=$fEmpresa->valorInformado();?>";
var NU_EMPRESA = "<?=$fEmpresa->valorInformado();?>";

var idEmbarcacaoProjeto = "<?=$pNU_EMBARCACAO_PROJETO;?>";
var NU_EMBARCACAO_PROJETO = "<?=$pNU_EMBARCACAO_PROJETO;?>";


try {
	  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
} 
catch (e) {
	try {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
	catch (E) {
		xmlhttp = false;
		}
}
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	try {
		xmlhttp = new XMLHttpRequest();
	} 
	catch (e) {
		xmlhttp=false;
	}
}
if (!xmlhttp && window.createRequest) {
	try {
		xmlhttp = window.createRequest();
	} catch (e) {
		xmlhttp=false;
	}
}

function handleOnChange(dd1)
{
  var idx = dd1.selectedIndex;
  var val = dd1[idx].value;
  //alert ('valor='+val);
  var url = "/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA="+val;
  //alert("url="+url);
  xmlhttp.open("GET", url ,true);
  xmlhttp.onreadystatechange=function() {
	if (xmlhttp.readyState==4) {
		document.getElementById('tdEmbarcacaoProjeto').innerHTML = '<select name="NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>'+xmlhttp.responseText+'</select>'
	}
  }
  xmlhttp.send(null)
}
</script>

<?php
echo Rodape("");
?>
