<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");
include ("../javascript/validacoes_js.php");
include ("../javascript/novoDependente_js.php"); 

$idEmpresa = $_POST['idEmpresa'];
$idCandidato = $_POST['idCandidato'];
$idDependente = $_POST['idDependente'];
$nomeCandidato= pegaNomeCandidato($idCandidato);

if( $idDependente != null){
  $query = "SELECT a.NU_CANDIDATO, a.NU_DEPENDENTE, a.NO_DEPENDENTE, a.CO_GRAU_PARENTESCO, a.DT_NASCIMENTO, a.CO_NACIONALIDADE, a.NU_PASSAPORTE, a.CO_PAIS_EMISSOR_PASSAPORTE FROM DEPENDENTES a, GRAU_PARENTESCO b where a.CO_GRAU_PARENTESCO = b.CO_GRAU_PARENTESCO and a.NU_CANDIDATO = $idCandidato and a.NU_DEPENDENTE = $idDependente ";
  $resultado = mysql_query($query);
  $dados = mysql_fetch_array($resultado);
  $idCandidato = $dados['NU_CANDIDATO'];
  $idDependente = $dados['NU_DEPENDENTE'];
  $nome = $dados['NO_DEPENDENTE'];
  $idGrauParentesco = $dados['CO_GRAU_PARENTESCO'];
  $dataNascimento = dataMy2BR($dados['DT_NASCIMENTO']);
  $idNacionalidade = $dados['CO_NACIONALIDADE'];
  $passaporte = $dados['NU_PASSAPORTE'];
  $idPaisEmissorPassaporte = $dados['CO_PAIS_EMISSOR_PASSAPORTE'];

  $nmParentesco = pegaNomeGrauParentesco($idGrauParentesco);
  $nmNacional = pegaNomeNacionalidade($idNacionalidade);
  $nmEmissor = pegaNomePais($idPaisEmissorPassaporte);
}

$cmbParentesco = montaComboGrauParentesco($idGrauParentesco,"");
$cmbNacional = montaComboNacionalidade($idNacionalidade,"");
$cmbEmissor = montaComboPais($idPaisEmissorPassaporte,"");

echo Topo("");
echo Menu("");
?>

<br><center>
<table border=0 width=800>
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Cadastro de Dependente ::</strong></p>
  </td>
 </tr>
</table>
<br>

<table border=0 align="center" width=700 class='textoazulPeq'>
 <tr>
  <td colspan="5" class="textoazul">Preencha os dados abaixo para cadastro de dependente</font></td>
 </tr>
 <tr>
  <td colspan="5"><font color="Red">*</font>&nbsp;<font class="textoazulPeq">Campos obrigatórios</font></td>
 </tr>
 <tr><td width=160>&#160;</td><td width=210>&#160;</td><td width=10>&#160;</td><td width=140>&#160;</td><td width=180>&#160;</td></tr>

<form action="insereDependente.php" method="post" onSubmit="return validarDados(this)">
<input type="Hidden" name="idEmpresa" value='<?=$idEmpresa?>'>
<input type="Hidden" name="idCandidato" value='<?=$idCandidato?>'>
<input type="Hidden" name="idDependente" value="<?=$idDependente?>">

<?php
include("../inc/dependentes_inc.php");
?>

</table>
<br>

<?php
if($acao=="V") {
  echo "<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();'>\n";
} else {
  echo "<input name='salvar' type='Submit' class='textformtopo' style=\"$estilo\" value='Salvar'>\n";
}
echo Rodape("");
?>
