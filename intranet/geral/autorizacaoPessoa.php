<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");

echo Topo($opcao);
echo Menu($opcao);

$idEmpresa = 0+$HTTP_GET_VARS['idEmpresa'];
if($idEmpresa==0) { $idEmpresa = 0+$POST['idEmpresa']; }
if($idEmpresa>0) {
  $ehempresa = "true";
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $txtempresa = "<input type=hidden name=idEmpresa value='$idEmpresa'>$nomeEmpresa";
} else {
  $ehempresa = "false";
  $empresas = montaComboEmpresas("","ORDER BY NO_RAZAO_SOCIAL");
  $txtempresa = "<select name=idEmpresa><option value=''>Selecione ...".$empresas."</select>";
}
# montaComboCandidatos($codigo,$empresa,$extra)  # VERIFICAR QUE NAO TEM COLUNA NU_EMPRESA
$candidatos = montaComboCandidatos("","ORDER BY NO_PRIMEIRO_NOME,NO_NOME_MEIO,NO_ULTIMO_NOME");
$txtcandidato = "<select name='idCandidato'><option value=''>Selecione ...".$candidatos."</select>";
?>

    <br><center>
  	<table border=0 width="80%">
		<tr>
			<td class="textobasico" align="center" valign="top" colspan="2">
				<p align="center" class="textoazul"><strong>:: Cadastro de Autorizações ::</strong></p>				
			</td>
		</tr>
		<tr><td colspan="2"><br></td></tr>
		<form action="novoAutorizacao.php" method="post" name='forms'>
		<tr>
			<td class="textoazul">Empresa:</td>
			<td class="textoazul"><?=$txtempresa?></td>
		</tr>
		<tr>
			<td class="textoazul">Candidato: </td>
			<td class="textoazul"><?=$txtcandidato?></td>
		</tr>

		<tr><td><br></td></tr>
		<tr>
			<td></td>
			<td><input type="Button" class="textformtopo" style="<?=$estilo?>" value="Buscar" onclick="javascript:Continuar();"></td>
		</tr>		
	 </form>
	</table>

<script language="JavaScript">
function Continuar() {
  var ret="";
  if(<?=$ehempresa?> == true) {
    if(document.forms.idEmpresa.value == '') {
      ret += "* Para continuar é necessário haver uma empresa.";
    }
  } else {
    if(document.forms.idEmpresa[document.forms.idEmpresa.selectedIndex].value == '') {
      ret += "* Para continuar é necessário escolher uma empresa.";
    }
  }
  if(document.forms.idCandidato[document.forms.idCandidato.selectedIndex].value == '') {
     ret += "* Para continuar é necessário escolher o candidato.";
  }
  if(ret.length == 0) {
    document.forms.submit();
  } else {
    alert(ret);
  }
}
</script>

<?php
echo Rodape($opcao);
?>
