<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");
include ("../javascript/criamascara_js.php");
include ("../javascript/validacoes_js.php");
include ("../javascript/novoCandidato_js.php"); 

$target = "target = 'WILSONS'";
echo "<script language='javascript'>window.name='WILSONS';</script>";

$idCandidato = $_REQUEST['idCandidato'];
$NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
$idEmpresa = $_REQUEST['idEmpresa'];
$nmEmpresa=pegaNomeEmpresa($idEmpresa);
$acao = $_REQUEST['acao'];
$selecione="<option value=''>Selecione ...</option>";
$extraproj = "";
if($usuarmad == "S") {
   $extraproj = " AND NU_ARMADOR=$usulogado";
}

if(strlen($idCandidato)>0){
	$query = " SELECT NU_CANDIDATO,NO_PRIMEIRO_NOME,NO_NOME_MEIO,NO_ULTIMO_NOME,NO_EMAIL_CANDIDATO,
                 NO_ENDERECO_RESIDENCIA,NO_CIDADE_RESIDENCIA,CO_PAIS_RESIDENCIA,NU_TELEFONE_CANDIDATO,
                 NO_ENDERECO_ESTRANGEIRO,NO_CIDADE_ESTRANGEIRO,CO_PAIS_ESTRANGEIRO,NU_TELEFONE_ESTRANGEIRO,
                 NO_EMPRESA_ESTRANGEIRA,NO_ENDERECO_EMPRESA,NO_CIDADE_EMPRESA,CO_PAIS_EMPRESA,NU_TELEFONE_EMPRESA,
                 NO_PAI,NO_MAE,CO_NACIONALIDADE,CO_NIVEL_ESCOLARIDADE,CO_ESTADO_CIVIL,CO_SEXO,DT_NASCIMENTO,
                 NO_LOCAL_NASCIMENTO,NU_PASSAPORTE,DT_EMISSAO_PASSAPORTE,DT_VALIDADE_PASSAPORTE,CO_PAIS_EMISSOR_PASSAPORTE,
                 NU_RNE,CO_PROFISSAO_CANDIDATO,TE_TRABALHO_ANTERIOR_BRASIL,NU_CPF,CO_USU_CADASTAMENTO,
                 DT_CADASTRAMENTO,CO_USU_ULT_ALTERACAO,date(DT_ULT_ALTERACAO) as dcad,time(DT_ULT_ALTERACAO) as hcad
                 FROM CANDIDATO where NU_CANDIDATO=$idCandidato ";
	$resultado = mysql_query($query);
	$dados = mysql_fetch_array($resultado);	
	$idCandidato = $dados['NU_CANDIDATO'];
	$primeiroNome = $dados['NO_PRIMEIRO_NOME'];
	$nomeMeio = $dados['NO_NOME_MEIO'];	
	$ultimoNome = $dados['NO_ULTIMO_NOME'];
	$email = $dados['NO_EMAIL_CANDIDATO'];
	$endereco = $dados['NO_ENDERECO_RESIDENCIA'];
	$cidade = $dados['NO_CIDADE_RESIDENCIA'];
	$idPais = $dados['CO_PAIS_RESIDENCIA'];
	$telefone = $dados['NU_TELEFONE_CANDIDATO'];
	$enderecoext = $dados['NO_ENDERECO_ESTRANGEIRO'];
	$cidadeext = $dados['NO_CIDADE_ESTRANGEIRO'];
	$idPaisExt = $dados['CO_PAIS_ESTRANGEIRO'];
	$telefoneext = $dados['NU_TELEFONE_ESTRANGEIRO'];
	$nomeEmpresa = $dados['NO_EMPRESA_ESTRANGEIRA'];
	$enderecoEmpresa = $dados['NO_ENDERECO_EMPRESA'];
	$cidadeEmpresa = $dados['NO_CIDADE_EMPRESA'];
	$idPaisEmpresa = $dados['CO_PAIS_EMPRESA'];
	$telefoneEmpresa = $dados['NU_TELEFONE_EMPRESA'];
	$nomePai = $dados['NO_PAI'];
	$nomeMae = $dados['NO_MAE'];
	$idNacionalidade = $dados['CO_NACIONALIDADE'];
	$idEscolaridade = $dados['CO_NIVEL_ESCOLARIDADE'];
	$idEstadoCivil = $dados['CO_ESTADO_CIVIL'];
	$sexo = $dados['CO_SEXO'];
	$dataNascimento = dataMy2BR($dados['DT_NASCIMENTO']);
	$localNascimento = $dados['NO_LOCAL_NASCIMENTO'];
	$numeroPassaporte = $dados['NU_PASSAPORTE'];
	$dataEmissaoPassaporte = dataMy2BR($dados['DT_EMISSAO_PASSAPORTE']);
	$dataValidadePassaporte = dataMy2BR($dados['DT_VALIDADE_PASSAPORTE']);
	$idPaisEmissorPassaporte = $dados['CO_PAIS_EMISSOR_PASSAPORTE'];
	$rne = $dados['NU_RNE'];
	$idProfissao = $dados['CO_PROFISSAO_CANDIDATO'];
	$expProfissional = $dados['TE_TRABALHO_ANTERIOR_BRASIL'];
	$cpf = $dados['NU_CPF'];
    $idUsuCad = $dados['CO_USU_CADASTAMENTO'];
    if($idUsuCad>0) {
      $nmUsuCad = pegaNomeUsuario($idUsuCad);
    } else {
      $nmUsuCad = "Sem informacao";
    }
    $dtUsuCad = dataMy2BR($dados['DT_CADASTRAMENTO']);
    $idUsuAlt = $dados['CO_USU_ULT_ALTERACAO'];
    if($idUsuAlt>0) {
      $nmUsuAlt = pegaNomeUsuario($idUsuAlt);
    } else {
      $nmUsuAlt = "Sem informacao";
    }
    $dtUsuAlt = dataMy2BR($dados['dcad'])." ".$dados['hcad'];
    if(dataMy2BR($dados['dcad'])=="") { $dtUsuAlt=""; }
  	$query = "SELECT * FROM AUTORIZACAO_CANDIDATO where NU_CANDIDATO=$idCandidato ";
  	if(strlen($NU_SOLICITACAO)>0) {
       $query = $query."AND NU_SOLICITACAO = $NU_SOLICITACAO ";
  	} else {
       $query = $query."AND NU_SOLICITACAO in (SELECT max(NU_SOLICITACAO) FROM AUTORIZACAO_CANDIDATO where NU_CANDIDATO=$idCandidato)";
  	}
	$resultado = mysql_query($query);
	if($dados = mysql_fetch_array($resultado)) {
      $NU_SOLICITACAO = $dados['NU_SOLICITACAO'];
      if($acao!="S") {
  	    $idEmbarcacaoProjeto = $dados['NU_EMBARCACAO_PROJETO'];
  	    $idEmbarcacaoInicial = $dados['NU_EMBARCACAO_INICIAL'];
	    $idFuncao = $dados['CO_FUNCAO_CANDIDATO'];
	    $salTotal = $dados['VA_RENUMERACAO_MENSAL'];
	    $salBrasil = $dados['VA_REMUNERACAO_MENSAL_BRASIL'];
	    $CO_REPARTICAO_CONSULAR = $dados['CO_REPARTICAO_CONSULAR'];
	    $NU_ARMADOR = $dados['NU_ARMADOR'];
	    $TE_DESCRICAO_ATIVIDADES = $dados['TE_DESCRICAO_ATIVIDADES'];
        $CO_TIPO_AUTORIZACAO = $dados['CO_TIPO_AUTORIZACAO'];
        $DT_PRAZO_ESTADA_SOLICITADO = $dados['DT_PRAZO_ESTADA_SOLICITADO'];
        $DT_PRAZO_AUTORIZACAO_MTE = $dados['DT_PRAZO_AUTORIZACAO_MTE'];
        if(strlen($DT_PRAZO_ESTADA_SOLICITADO)==0) { $DT_PRAZO_ESTADA_SOLICITADO=$DT_PRAZO_AUTORIZACAO_MTE; }
        $DS_JUSTIFICATIVA = $dados['DS_JUSTIFICATIVA'];
        $DS_LOCAL_TRABALHO = $dados['DS_LOCAL_TRABALHO'];
        $DS_SALARIO_EXTERIOR = $dados['DS_SALARIO_EXTERIOR'];
        $DS_BENEFICIOS_SALARIO = $dados['DS_BENEFICIOS_SALARIO']; 
        $CD_EMBARCADO = $dados['CD_EMBARCADO']; 
      }
    }
# Nao sei o que e isso, mas como somar solicitacao = solicitacao + 1 ??????????????
#    if($acao=="S") {
#      $NU_SOLICITACAO = $NU_SOLICITACAO + 1;
#      $sql1 = "INSERT INTO AUTORIZACAO_CANDIDATO (NU_CANDIDATO,NU_EMPRESA,NU_SOLICITACAO,CD_EMBARCADO) values ($idCandidato,$idEmpresa,$NU_SOLICITACAO,1)";
#      print "sql=$sql1";
#      mysql_query($sql1);
#      if(mysql_errno()!=0) {
#        print "Erro: ".mysql_error();
#        exit;
#      }
#    }
}
if($acao=="V") {
  $nmProjeto=pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa);
  $nmProjetoIni=pegaNomeProjeto($idEmbarcacaoInicial,$idEmpresa);
  $nmEscola= pegaNomeEscola($idEscolaridade);
  $nmCivil = pegaNomeEstadoCivil($idEstadoCivil);
  $nmPaisRes=pegaNomePais($idPais);
  $nmPaisExt=pegaNomePais($idPaisExt);
  $nmPaisPass= pegaNomePais($idPaisEmissorPassaporte);
  $nmPaisEmp = pegaNomePais($idPaisEmpresa);
  $nmNacional= pegaNomeNacionalidade($idNacionalidade);
  $nmProfissao= pegaNomeProfissao($idProfissao);
  $nmFuncao= pegaNomeFuncao($idFuncao);
  $nmReparticao = pegaNomeReparticao($CO_REPARTICAO_CONSULAR);
  $nmArmador = pegaNomeUsuario($NU_ARMADOR);
  $nmAutorizacao = pegaNomeTipoAutorizacao2($CO_TIPO_AUTORIZACAO);
} else {
  if( (strlen($idEmbarcacaoProjeto)>0) && ($idEmbarcacaoProjeto>0) ) {  $selproj = "";  }  else { $selproj=$selecione; }
  if( (strlen($idEmbarcacaoInicial)>0) && ($idEmbarcacaoInicial>0) ) {  $selprojini = "";  }  else { $selprojini=$selecione; }
  if( (strlen($idEscolaridade)>0) && ($idEscolaridade>0) ) {  $selescol = "";  }  else { $selescol=$selecione; }
  if( (strlen($idEstadoCivil)>0) && ($idEstadoCivil>0) ) {  $selcivil = "";  }  else { $selcivil=$selecione; }
  if( (strlen($idPais)>0) && ($idPais>0) ) {  $selpais = "";  }  else { $selpais=$selecione; }
  if( (strlen($idPaisExt)>0) && ($idPaisExt>0) ) {  $selpaisExt = "";  }  else { $selpaisExt=$selecione; }
  if( (strlen($idPaisEmissorPassaporte)>0) && ($idPaisEmissorPassaporte>0) ) {  $selpass = "";  }  else { $selpass=$selecione; }
  if( (strlen($idPaisEmpresa)>0) && ($idPaisEmpresa>0) ) {  $selpaise = "";  }  else { $selpaise=$selecione; }
  if( (strlen($idNacionalidade)>0) && ($idNacionalidade>0) ) {  $selnac = "";  }  else { $selnac=$selecione; }
  if( (strlen($idProfissao)>0) && ($idProfissao>0) ) {  $selprof = "";  }  else { $selprof=$selecione; }
  if( (strlen($idFuncao)>0) && ($idFuncao>0) ) {  $selfunc = "";  }  else { $selfunc=$selecione; }
  $cmbProjetos=$selproj.montaComboProjetos($idEmbarcacaoProjeto,$idEmpresa,$extraproj." ORDER BY NO_EMBARCACAO_PROJETO");
  $cmbProjetosIni=$selprojini.montaComboProjetos($idEmbarcacaoInicial,$idEmpresa,$extraproj." ORDER BY NO_EMBARCACAO_PROJETO");
  $cmbEscola= $selescol.montaComboEscolaridade($idEscolaridade," ORDER BY NO_ESCOLARIDADE");
  $cmbCivil = $selcivil.montaComboEstadoCivil($idEstadoCivil," ORDER BY NO_ESTADO_CIVIL");
  $cmbPaisRes=$selpais.montaComboPais($idPais,"");
  $cmbPaisExt=$selpaisExt.montaComboPais($idPaisExt,"");
  $cmbPaisPass= $selpass.montaComboPais($idPaisEmissorPassaporte,"");
  $cmbPaisEmp = $selpaise.montaComboPais($idPaisEmpresa,"");
  $cmbNacional = $selnac.montaComboPais($idNacionalidade,"");
  $cmbProfissao= $selprof.montaComboProfissoes($idProfissao," ORDER BY NO_PROFISSAO");
  $cmbFuncao= $selfunc. montaComboFuncoes($idFuncao," ORDER BY NO_FUNCAO");
  $cmbReparticao = $selecione.montaComboReparticoes($CO_REPARTICAO_CONSULAR,"");
  if($usuarmad=="S") {
    $cmbArmador = "$aux_esc um armador".montaComboArmador(""," AND NU_USUARIO=$usulogado");
  } else {
    $cmbArmador = "$aux_esc um armador".montaComboArmador($usulogado," ORDER BY NO_USUARIO");
  }
  $cmbAutorizacao = "$aux_esc uma autorizacao".montaComboTipoAutorizacao2($CO_TIPO_AUTORIZACAO,"ORDER BY NO_TIPO_AUTORIZACAO");
}

echo IniPag();

?>

<br><center>
<table border=0 width=800 class="textoazulPeq">
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Cadastro de Candidato ::</strong></p>
  </td>
 </tr>
</table>
<br>

<table border=0 align="center" width=800 class="textoazulPeq">
 <tr>
  <td colspan="5" class="textoazul">Preencha os dados abaixo para o cadastro de um novo candidato</font></td>
 </tr>
 <tr>
  <td colspan="5"><font color="Red">*</font>&nbsp;<font class="textoazulPeq">Campos obrigatórios</font><br><br></td>
 </tr>

<form action="insereCandidato.php" method="post" id="candidato" name="candidato" onSubmit="return validarDados(this);" <?=$target?> >
<input type="Hidden" name="idCandidato" value="<?=$idCandidato?>">
<input type="Hidden" name="idEmpresa" value="<?=$idEmpresa?>">
<input type="Hidden" name="nuSolicitacao" value="<?=$NU_SOLICITACAO?>">

<?php
include("../inc/candidatos_inc.php");
?>

</table>
<br>

<?php
if($acao=="V") {
  echo "<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();' >\n";
} else {
  echo "<input name='salvar' type='Submit' class='textformtopo' style=\"$estilo\" value='Salvar' $target>\n";
  echo "&#160;&#160;&#160;&#160;";
  echo "<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();' >\n";
}
echo Rodape("");
?>
