<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");

$target = "target = 'WILSONS'";
echo "<script language='javascript'>window.name='WILSONS';</script>";

$idEmpresa = $_POST['idEmpresa'];
$idEmbarcacaoProjeto = 0+$_POST['idEmbarcacaoProjeto'];
$idEmbarcacaoInicial = 0+$_POST['idEmbarcacaoInicial'];
if($idEmbarcacaoInicial==0) {
  $idEmbarcacaoInicial = $idEmbarcacaoProjeto;
}
$idCandidato = $_POST['idCandidato'];
$primeiroNome = trim($_POST['primeiroNome']);
$nomeMeio = trim($_POST['nomeMeio']);
$ultimoNome = trim($_POST['ultimoNome']);
if($nomeMeio==".") { $nomeMeio=""; }
if($ultimoNome==".") { $ultimoNome=""; }
$nome_completo = $primeiroNome;
if(strlen($nomeMeio)>0) { $nome_completo = $nome_completo." ".$nomeMeio; }
if(strlen($ultimoNome)>0) { $nome_completo = $nome_completo." ".$ultimoNome; }
$email = $_POST['email'];
$endereco = $_POST['endereco'];
$cidade = $_POST['cidade'];
$idPais = $_POST['idPais'];
$telefone = $_POST['telefone'];
$enderecoext = $_POST['enderecoext'];
$cidadeext = $_POST['cidadeext'];
$idPaisExt = $_POST['idPaisExt'];
$telefoneext = $_POST['telefoneext'];
$nomeEmpresa = $_POST['nomeEmpresa'];
$enderecoEmpresa = $_POST['enderecoEmpresa'];
$cidadeEmpresa = $_POST['cidadeEmpresa'];
$idPaisEmpresa = $_POST['idPaisEmpresa'];
$telefoneEmpresa = $_POST['telefoneEmpresa'];
$nomePai = $_POST['nomePai'];
$nomeMae = $_POST['nomeMae'];
$idNacionalidade = $_POST['idNacionalidade'];
$idEscolaridade = $_POST['idEscolaridade'];
$idEstadoCivil = $_POST['idEstadoCivil'];
$sexo = $_POST['sexo'];
$dataNascimento = $_POST['dataNascimento'];
$localNascimento = $_POST['localNascimento'];
$numeroPassaporte = $_POST['numeroPassaporte'];
$dataEmissaoPassaporte = $_POST['dataEmissaoPassaporte'];
$dataValidadePassaporte = $_POST['dataValidadePassaporte'];
$idPaisEmissorPassaporte = $_POST['idPaisEmissorPassaporte'];
$rne = $_POST['rne'];
$idProfissao = $_POST['idProfissao'];
$expProfissional = $_POST['expProfissional'];
$cpf = formataCPF($_POST['cpf']);
$idFuncao = 0+$_POST['idFuncao'];
$salTotal = $_POST['salTotal'];
$salBrasil = $_POST['salBrasil'];
$CO_REPARTICAO_CONSULAR = $_POST['CO_REPARTICAO_CONSULAR'];
$TE_DESCRICAO_ATIVIDADES = $_POST['TE_DESCRICAO_ATIVIDADES'];
$NU_ARMADOR = $_POST['NU_ARMADOR'];
$CO_TIPO_AUTORIZACAO = $_POST['CO_TIPO_AUTORIZACAO'];
$NU_SOLICITACAO = $_POST['NU_SOLICITACAO'];
$DT_PRAZO_ESTADA_SOLICITADO = $_POST['DT_PRAZO_ESTADA_SOLICITADO'];
$DT_PRAZO_AUTORIZACAO_MTE = $_POST['DT_PRAZO_AUTORIZACAO_MTE'];
$DS_JUSTIFICATIVA = $_POST['DS_JUSTIFICATIVA'];
$DS_LOCAL_TRABALHO = $_POST['DS_LOCAL_TRABALHO'];
$DS_SALARIO_EXTERIOR = $_POST['DS_SALARIO_EXTERIOR'];
$DS_BENEFICIOS_SALARIO = $_POST['DS_BENEFICIOS_SALARIO'];
$CD_EMBARCADO = 0+$_POST['CD_EMBARCADO'];

if(strlen($CO_REPARTICAO_CONSULAR)=='') { $CO_REPARTICAO_CONSULAR="NULL"; }
if(strlen($idPaisExt)==0) { $idPaisExt = "NULL"; }
if(strlen($idPais)==0) { $idPais = "NULL"; }
if(strlen($idEscolaridade)==0) { $idEscolaridade="NULL"; }
if(strlen($idPaisEmpresa)==0) { $idPaisEmpresa="NULL"; }
if(strlen($idNacionalidade)==0) { $idNacionalidade="NULL"; }
if(strlen($idEstadoCivil)==0) { $idEstadoCivil="NULL"; }
if(strlen($NU_ARMADOR)==0) { $NU_ARMADOR="NULL"; }

$aux_dt_prazo="'".$DT_PRAZO_ESTADA_SOLICITADO."'";
$dataNascimento = dataBR2My($dataNascimento);
$dataEmissaoPassaporte = dataBR2My($dataEmissaoPassaporte);
$dataValidadePassaporte = dataBR2My($dataValidadePassaporte);
if($usuarmad == "S") {
   $NU_ARMADOR=$usulogado;
} 
if(strlen($dataNascimento)==0) { $dataNascimento="NULL"; } else { $dataNascimento="'$dataNascimento'"; }
if(strlen($dataValidadePassaporte)==0) { $dataValidadePassaporte="NULL"; } else { $dataValidadePassaporte="'$dataValidadePassaporte'"; }
if(strlen($dataEmissaoPassaporte)==0) { $dataEmissaoPassaporte="NULL"; } else { $dataEmissaoPassaporte="'$dataEmissaoPassaporte'"; }

$ret1 = "";
if( $idCandidato == null || $idCandidato == ""){	
	
  $nmUsuCad = pegaNomeUsuario($usulogado);
  $dtUsuCad = date("d/m/Y");
  $nmUsuAlt = pegaNomeUsuario($usulogado);
  $dtUsuAlt = date("d/m/Y H:i:s");

  //Insere candidato
  $sql1 = "insert into CANDIDATO (NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME, NOME_COMPLETO, NO_EMAIL_CANDIDATO,
	  NO_ENDERECO_RESIDENCIA, NO_CIDADE_RESIDENCIA, CO_PAIS_RESIDENCIA, NU_TELEFONE_CANDIDATO,
      NO_ENDERECO_ESTRANGEIRO,NO_CIDADE_ESTRANGEIRO,CO_PAIS_ESTRANGEIRO,NU_TELEFONE_ESTRANGEIRO,
	  NO_EMPRESA_ESTRANGEIRA,NO_ENDERECO_EMPRESA, NO_CIDADE_EMPRESA, CO_PAIS_EMPRESA, NU_TELEFONE_EMPRESA, 
      NO_PAI, NO_MAE, CO_NACIONALIDADE,
	  CO_NIVEL_ESCOLARIDADE, CO_ESTADO_CIVIL,CO_SEXO, DT_NASCIMENTO,NO_LOCAL_NASCIMENTO, NU_PASSAPORTE,
	  DT_EMISSAO_PASSAPORTE, DT_VALIDADE_PASSAPORTE,CO_PAIS_EMISSOR_PASSAPORTE,NU_RNE, CO_PROFISSAO_CANDIDATO,
	  TE_TRABALHO_ANTERIOR_BRASIL, NU_CPF, DT_CADASTRAMENTO,CO_USU_CADASTAMENTO,CO_USU_ULT_ALTERACAO) 
      values ('".addslashes($primeiroNome)."', '".addslashes($nomeMeio)."', '".addslashes($ultimoNome)."','".addslashes($nome_completo)."', '$email',
      '".addslashes($endereco)."','".addslashes($cidade)."',$idPais,'$telefone','".addslashes($enderecoext)."',
      '".addslashes($cidadeext)."',$idPaisExt,'$telefoneext','".addslashes($nomeEmpresa)."','".addslashes($enderecoEmpresa)."',
      '$cidadeEmpresa',$idPaisEmpresa,'$telefoneEmpresa','".addslashes($nomePai)."','".addslashes($nomeMae)."',
      $idNacionalidade,$idEscolaridade,$idEstadoCivil,'$sexo',$dataNascimento,
	  '$localNascimento','$numeroPassaporte',$dataEmissaoPassaporte,$dataValidadePassaporte,
	  '$idPaisEmissorPassaporte','$rne','$idProfissao','".addslashes($expProfissional)."','$cpf',now(),$usulogado,$usulogado)";

  $resultadoCandidato = mysql_query($sql1);
#print "\n\n<!-- 1 SQL = $sql -->\n\n";
#print "\n\n<!-- 1 Erro:".mysql_error()." -->\n\n";
  if (mysql_errno() == 0){
    gravaLog($sql1,mysql_error(),"INCLUSAO","CANDIDATO");
    $selectUltCandidato = "SELECT max(nu_candidato) as idCandidato FROM CANDIDATO";
    $resultadoConsulta = mysql_query($selectUltCandidato);
    $dados = mysql_fetch_array($resultadoConsulta);	
    $idCandidato = $dados['idCandidato'];
    if (mysql_errno() == 0){
      $sql2 = " insert into AUTORIZACAO_CANDIDATO (NU_EMPRESA, NU_CANDIDATO, NU_EMBARCACAO_PROJETO , CO_FUNCAO_CANDIDATO,
         VA_RENUMERACAO_MENSAL,VA_REMUNERACAO_MENSAL_BRASIL,NU_ARMADOR,CO_REPARTICAO_CONSULAR,TE_DESCRICAO_ATIVIDADES,
         CO_TIPO_AUTORIZACAO,NU_SOLICITACAO,DT_PRAZO_ESTADA_SOLICITADO,DT_PRAZO_AUTORIZACAO_MTE,DS_JUSTIFICATIVA,
         DS_LOCAL_TRABALHO,DS_SALARIO_EXTERIOR,DS_BENEFICIOS_SALARIO,NU_EMBARCACAO_INICIAL,CD_EMBARCADO) 
         values ($idEmpresa, $idCandidato, $idEmbarcacaoProjeto,$idFuncao,'$salTotal','$salBrasil',$NU_ARMADOR,
         $CO_REPARTICAO_CONSULAR,'".addslashes($TE_DESCRICAO_ATIVIDADES)."','$CO_TIPO_AUTORIZACAO','$NU_SOLICITACAO',$aux_dt_prazo,
         $aux_dt_prazo, '".addslashes($DS_JUSTIFICATIVA)."','".addslashes($DS_LOCAL_TRABALHO)."','".addslashes($DS_SALARIO_EXTERIOR)."',
         '".addslashes($DS_BENEFICIOS_SALARIO)."',$idEmbarcacaoInicial,$CD_EMBARCADO) ";

      mysql_query($sql2);
#print "\n\n<!-- 2 SQL = $sql2 -->\n\n";
#print "\n\n<!-- 2 Erro:".mysql_error()." -->\n\n";
      if (mysql_errno() > 0){
        gravaLogErro($sql2,mysql_error(),"INCLUSAO","AUTORIZACAO_CANDIDATO");
        $ret1 = "Ocorreu um erro durante a operação de inclusão do visa detail. Tente novamente.";
      } else {
        gravaLog($sql2,mysql_error(),"INCLUSAO","AUTORIZACAO_CANDIDATO");
      }
    }
    $nome = $primeiroNome." ".$nomeMeio." ".$ultimoNome;
    $nomeEmp = pegaNomeEmpresa($idEmpresa);
    insereEvento($idEmpresa,$idCandidato,"","Inclusão do candidato $nome no cadastro da empresa $nomeEmp.");
  } else {
    gravaLogErro($sql1,mysql_error(),"INCLUSAO","CANDIDATO");
    $ret1 = "Ocorreu um erro durante a operação de inclusão do candidato. Tente novamente.";
  }
} else {

  $nmUsuCad = pegaNomeUsuario($_POST['CO_USU_CADASTAMENTO']);
  $dtUsuCad = $_POST['DT_CADASTRAMENTO'];
  $nmUsuAlt = pegaNomeUsuario($usulogado);
  $dtUsuAlt = date("Y-m-d H:i:s");

  $sql = " update CANDIDATO set NOME_COMPLETO='".addslashes($nome_completo)."'
  	, NO_PRIMEIRO_NOME='".addslashes($primeiroNome)."'
  	, NO_NOME_MEIO='".addslashes($nomeMeio)."'
  	, NO_ULTIMO_NOME = '".addslashes($ultimoNome)."'
	, NO_EMAIL_CANDIDATO = '$email'
	, NO_ENDERECO_RESIDENCIA = '".addslashes($endereco)."'
	, NO_CIDADE_RESIDENCIA = '".addslashes($cidade)."'
	, CO_PAIS_RESIDENCIA = $idPais
	, NU_TELEFONE_CANDIDATO = '$telefone'
	, NO_ENDERECO_ESTRANGEIRO = '".addslashes($enderecoext)."'
	, NO_CIDADE_ESTRANGEIRO = '".addslashes($cidadeext)."'
	, CO_PAIS_ESTRANGEIRO = $idPaisExt
    , NU_TELEFONE_ESTRANGEIRO = '$telefoneext'
    , NO_EMPRESA_ESTRANGEIRA = '".addslashes($nomeEmpresa)."'
    , NO_ENDERECO_EMPRESA = '".addslashes($enderecoEmpresa)."'
    , NO_CIDADE_EMPRESA = '".addslashes($cidadeEmpresa)."'
    , CO_PAIS_EMPRESA = $idPaisEmpresa
    , NU_TELEFONE_EMPRESA = '$telefoneEmpresa'
    , NO_PAI = '".addslashes($nomePai)."'
	, NO_MAE = '".addslashes($nomeMae)."'
	, CO_NACIONALIDADE = $idNacionalidade
	, CO_NIVEL_ESCOLARIDADE = $idEscolaridade
	, CO_ESTADO_CIVIL = $idEstadoCivil
	, CO_SEXO = '$sexo'
	, DT_NASCIMENTO = $dataNascimento
	, NO_LOCAL_NASCIMENTO = '".addslashes($localNascimento)."'
	, NU_PASSAPORTE = '$numeroPassaporte'
	, DT_EMISSAO_PASSAPORTE = $dataEmissaoPassaporte
	, DT_VALIDADE_PASSAPORTE = $dataValidadePassaporte
	, CO_PAIS_EMISSOR_PASSAPORTE = '$idPaisEmissorPassaporte'
	, NU_RNE = '$rne'
	, CO_PROFISSAO_CANDIDATO = '$idProfissao'
	, TE_TRABALHO_ANTERIOR_BRASIL='".addslashes($expProfissional)."'
	, NU_CPF = '$cpf'
	, CO_USU_ULT_ALTERACAO=$usulogado
	, DT_ULT_ALTERACAO = '$dtUsuAlt' 
	where NU_CANDIDATO = $idCandidato ";
  mysql_query($sql);
#print "\n\n<!-- 3 SQL = $sql -->\n\n";
#print "\n\n<!-- 3 Erro:".mysql_error()." -->\n\n";
  if (mysql_errno() > 0){
    gravaLogErro($sql,mysql_error(),"ALTERACAO","CANDIDATO");
    $ret1 = "Ocorreu um erro durante a operação de alteração do candidato. Tente novamente.";
  } else {
    gravaLog($sql,mysql_error(),"ALTERACAO","CANDIDATO");
    $sql2 = " update AUTORIZACAO_CANDIDATO set NU_EMBARCACAO_PROJETO=$idEmbarcacaoProjeto, CO_FUNCAO_CANDIDATO=$idFuncao,
        VA_RENUMERACAO_MENSAL='$salTotal', VA_REMUNERACAO_MENSAL_BRASIL='$salBrasil',CO_REPARTICAO_CONSULAR=$CO_REPARTICAO_CONSULAR,
        TE_DESCRICAO_ATIVIDADES='".addslashes($TE_DESCRICAO_ATIVIDADES)."',NU_ARMADOR=$NU_ARMADOR,CO_TIPO_AUTORIZACAO='$CO_TIPO_AUTORIZACAO',
 		DT_PRAZO_ESTADA_SOLICITADO=$aux_dt_prazo,DT_PRAZO_AUTORIZACAO_MTE=$aux_dt_prazo,DS_JUSTIFICATIVA='".addslashes($DS_JUSTIFICATIVA)."',
        DS_LOCAL_TRABALHO='".addslashes($DS_LOCAL_TRABALHO)."',DS_SALARIO_EXTERIOR='$DS_SALARIO_EXTERIOR',DS_BENEFICIOS_SALARIO='".addslashes($DS_BENEFICIOS_SALARIO)."',
        NU_EMBARCACAO_INICIAL=$idEmbarcacaoInicial,CD_EMBARCADO=$CD_EMBARCADO
        where NU_EMPRESA=$idEmpresa and NU_CANDIDATO=$idCandidato and NU_SOLICITACAO=$NU_SOLICITACAO";
    mysql_query($sql2);
    
# print "SQL=$sql2";
#print "\n\n<!-- 4 SQL = $sql2 -->\n\n";
#print "\n\n<!-- 4 Erro:".mysql_error()." -->\n\n";
    if (mysql_errno() > 0){
      gravaLogErro($sql2,mysql_error(),"ALTERACAO","AUTORIZACAO_CANDIDATO");
      $ret1 = "Ocorreu um erro durante a operação de alteração do visa detail. Tente novamente.";
    } else {
      gravaLog($sql2,mysql_error(),"ALTERACAO","AUTORIZACAO_CANDIDATO");
      $nome = $primeiroNome." ".$nomeMeio." ".$ultimoNome;
      $nomeEmp = pegaNomeEmpresa($idEmpresa);
      insereEvento($idEmpresa,$idCandidato,"","Alteração do candidato $nome no cadastro da empresa $nomeEmp.");
    }
    
    $sql3 = "update processo_mte set prazo_solicitado = $aux_dt_prazo where cd_candidato=$idCandidato and cd_solicitacao=$NU_SOLICITACAO";
	//print $sql3;
    mysql_query($sql3);
    
    
    
    
  }
}

$dataNascimento = dataMy2BR($dataNascimento);
$dataEmissaoPassaporte = dataMy2BR($dataEmissaoPassaporte);
$dataValidadePassaporte = dataMy2BR($dataValidadePassaporte);

$nmProjeto=pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa);
$nmEscola= pegaNomeEscola($idEscolaridade);
$nmCivil = pegaNomeEstadoCivil($idEstadoCivil);
$nmPaisRes=pegaNomePais($idPais);
$nmPaisExt=pegaNomePais($idPaisExt);
$nmPaisPass= pegaNomePais($idPaisEmissorPassaporte);
$nmPaisEmp = pegaNomePais($idPaisEmpresa);
$nmNacional= pegaNomeNacionalidade($idNacionalidade);
$nmProfissao= pegaNomeProfissao($idProfissao);
$nmFuncao= pegaNomeFuncao($idFuncao);
$nmReparticao = pegaNomeReparticao($CO_REPARTICAO_CONSULAR);
$nmArmador = pegaNomeUsuario($NU_ARMADOR);
$nmAutorizacao = pegaNomeTipoAutorizacao2($CO_TIPO_AUTORIZACAO);

IniPag();

?>

<br><center>
<table border=0 width=700 class="textoazulPeq">
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Cadastro de Candidato ::</strong></p>
  </td>
 </tr>
</table>
<br>
<table border=0 align="center" width="800" class="textoazulPeq">
<tr><td colspan=5 align=center>
<?php
if(strlen($ret1)>0) {
  echo $ret1;
} else {
  echo "Operacao executada com sucesso.";
}
?>
</td></tr>

<tr><td colspan=5>&#160;</td></tr>

  <tr>
   <td colspan=5 align=center>
    <input type='button' class='textformtopo' style="<?=$estilo?>" value='Fechar' onclick='javascript:window.close();'>
   </td>
  </tr>

</table>

<?php
  echo Rodape("");
?>

<script language="javascript">
function voltaroper() {
    var msg = "<?=$ret1?>";
    var msgErro = "";
    var MyArgs = new Array(msg,msgErro);
    window.returnValue = MyArgs;
    window.close();
}
</script>

