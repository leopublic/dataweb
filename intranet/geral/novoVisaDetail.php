<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");
include ("../../LIB/libVisaDetail.php");
#include ("../javascript/novoCandAutorizacao_js.php");

$target = "target = 'WILSONS'";
echo "<script language='javascript'>window.name='WILSONS';</script>";

$idCandidato = $_POST['idCandidato'];
if(strlen($idCandidato)==0) { $idCandidato = $_GET['idCandidato']; }
$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) { $idEmpresa = $_GET['idEmpresa']; }
$NU_SOLICITACAO = $_POST['NU_SOLICITACAO'];
if(strlen($NU_SOLICITACAO)==0) { $NU_SOLICITACAO = $_GET['NU_SOLICITACAO']; }
$acao = $_POST['acao'];
if(strlen($acao)==0) { $acao = $_GET['acao']; }

$nomeEmpresa = pegaNomeEmpresa($idEmpresa);
$nomeCandidato = pegaNomeCandidato($idCandidato);
if(strlen($NU_SOLICITACAO)==0) {
  $NU_SOLICITACAO = pegaUltimaSolicitacao($idCandidato);
}
$autorizacao = lerVisaDetail($idEmpresa,$idCandidato,$NU_SOLICITACAO);

if($autorizacao == null){
   $acao = "I";
   $autorizacao = new objVisaDetail();
} else {
   $NU_SOLICITACAO=$autorizacao->getNU_SOLICITACAO();
   $NU_EMBARCACAO_PROJETO=$autorizacao->getNU_EMBARCACAO_PROJETO();
   $CO_TIPO_AUTORIZACAO=$autorizacao->getCO_TIPO_AUTORIZACAO();
   $VA_RENUMERACAO_MENSAL=$autorizacao->getVA_RENUMERACAO_MENSAL();
   $NO_MOEDA_REMUNERACAO_MENSAL=$autorizacao->getNO_MOEDA_REMUNERACAO_MENSAL();
   $VA_REMUNERACAO_MENSAL_BRASIL=$autorizacao->getVA_REMUNERACAO_MENSAL_BRASIL();
   $CO_REPARTICAO_CONSULAR=$autorizacao->getCO_REPARTICAO_CONSULAR();
   $CO_FUNCAO_CANDIDATO=$autorizacao->getCO_FUNCAO_CANDIDATO();
   $TE_DESCRICAO_ATIVIDADES=$autorizacao->getTE_DESCRICAO_ATIVIDADES();
   $DT_ABERTURA_PROCESSO_MTE=$autorizacao->getDT_ABERTURA_PROCESSO_MTE();
   $NU_PROCESSO_MTE=$autorizacao->getNU_PROCESSO_MTE();
   $NU_AUTORIZACAO_MTE=$autorizacao->getNU_AUTORIZACAO_MTE();
   $DT_AUTORIZACAO_MTE=$autorizacao->getDT_AUTORIZACAO_MTE();
   $DT_PRAZO_AUTORIZACAO_MTE=$autorizacao->getDT_PRAZO_AUTORIZACAO_MTE();
   $DT_VALIDADE_VISTO=$autorizacao->getDT_VALIDADE_VISTO();
   $NU_VISTO=$autorizacao->getNU_VISTO();
   $DT_EMISSAO_VISTO=$autorizacao->getDT_EMISSAO_VISTO();
   $NO_LOCAL_EMISSAO_VISTO=$autorizacao->getNO_LOCAL_EMISSAO_VISTO();
   $CO_NACIONALIDADE_VISTO=$autorizacao->getCO_NACIONALIDADE_VISTO();
   $NU_PROTOCOLO_CIE=$autorizacao->getNU_PROTOCOLO_CIE();
   $DT_PROTOCOLO_CIE=$autorizacao->getDT_PROTOCOLO_CIE();
   $DT_VALIDADE_PROTOCOLO_CIE=$autorizacao->getDT_VALIDADE_PROTOCOLO_CIE();
   $DT_PRAZO_ESTADA=$autorizacao->getDT_PRAZO_ESTADA();
   $DT_VALIDADE_CIE=$autorizacao->getDT_VALIDADE_CIE();
   $DT_EMISSAO_CIE=$autorizacao->getDT_EMISSAO_CIE();
   $NU_PROTOCOLO_PRORROGACAO=$autorizacao->getNU_PROTOCOLO_PRORROGACAO();
   $DT_PROTOCOLO_PRORROGACAO=$autorizacao->getDT_PROTOCOLO_PRORROGACAO();
   $DT_VALIDADE_PROTOCOLO_PROR=$autorizacao->getDT_VALIDADE_PROTOCOLO_PROR();
   $DT_PRETENDIDA_PRORROGACAO=$autorizacao->getDT_PRETENDIDA_PRORROGACAO();
   $DT_CANCELAMENTO=$autorizacao->getDT_CANCELAMENTO();
   $NU_PROCESSO_CANCELAMENTO=$autorizacao->getNU_PROCESSO_CANCELAMENTO();
   $DT_PROCESSO_CANCELAMENTO=$autorizacao->getDT_PROCESSO_CANCELAMENTO();
   $TE_OBSERVACOES_PROCESSOS=$autorizacao->getTE_OBSERVACOES_PROCESSOS();
   $DT_PRAZO_ESTADA_SOLICITADO=$autorizacao->getDT_PRAZO_ESTADA_SOLICITADO();
   $NU_EMISSAO_CIE=$autorizacao->getNU_EMISSAO_CIE();
   $NU_ARMADOR=$autorizacao->getNU_ARMADOR();
   $NO_LOCAL_ENTRADA=$autorizacao->getNO_LOCAL_ENTRADA();
   $NO_UF_ENTRADA=$autorizacao->getNO_UF_ENTRADA();
   $DT_ENTRADA=$autorizacao->getDT_ENTRADA();
   $NU_TRANSPORTE_ENTRADA=$autorizacao->getNU_TRANSPORTE_ENTRADA();
   $CO_CLASSIFICACAO_VISTO=$autorizacao->getCO_CLASSIFICACAO_VISTO();
   $TE_DESCRICAO_ATIVIDADES=$autorizacao->getTE_DESCRICAO_ATIVIDADES();
}

if( (strlen($CO_TIPO_AUTORIZACAO)==0) && ($idEmpresa==21) ) {
  $CO_TIPO_AUTORIZACAO = "RM";
}

$nmProjetos = pegaNomeProjeto($NU_EMBARCACAO_PROJETO,$idEmpresa);
$nmAutorizacao = pegaNomeTipoAutorizacao2($CO_TIPO_AUTORIZACAO);
$nmReparticao = pegaNomeReparticao($CO_REPARTICAO_CONSULAR);
$nmFuncao = pegaNomeFuncao($CO_FUNCAO_CANDIDATO);
$nmNacVisto = pegaNomePais($CO_NACIONALIDADE_VISTO);
$nmArmador = pegaNomeUsuario($NU_ARMADOR);
$nmClassVisto = pegaNomeClassVisto($CO_CLASSIFICACAO_VISTO);
$nmUFEntrada = $NO_UF_ENTRADA;
$nmTransEntrada = pegaNomeTransEntrada($NU_TRANSPORTE_ENTRADA);

$aux_esc = "<option value=''>Escolha ";
$cmbProjetos = "$aux_esc um projeto".montaComboProjetos($NU_EMBARCACAO_PROJETO,$idEmpresa,"ORDER BY NO_EMBARCACAO_PROJETO");
$cmbAutorizacao = "$aux_esc uma autorizacao".montaComboTipoAutorizacao2($CO_TIPO_AUTORIZACAO,"ORDER BY NO_TIPO_AUTORIZACAO");
$cmbReparticao = "$aux_esc uma reparticao".montaComboReparticoes($CO_REPARTICAO_CONSULAR,"");
$cmbFuncao = "$aux_esc uma funcao".montaComboFuncoes($CO_FUNCAO_CANDIDATO,"ORDER BY NO_FUNCAO");
$cmbNacVisto = "$aux_esc um pais".montaComboPais($CO_NACIONALIDADE_VISTO,"");
if($usuarmad=="S") {
  $cmbArmador = "$aux_esc um armador".montaComboArmador(""," AND NU_USUARIO=$usulogado");
} else {
  $cmbArmador = "$aux_esc um armador".montaComboArmador($usulogado," ORDER BY NO_USUARIO");
}
$cmbClassVisto = "$aux_esc um tipo de visto".montaComboClassVisto($CO_CLASSIFICACAO_VISTO);
$cmbUFEntrada = "$aux_esc um estado".montaComboEstados($NO_UF_ENTRADA);
$cmbTransEntrada = "$aux_esc um transporte".montaComboTransEntrada($NU_TRANSPORTE_ENTRADA);

echo IniPag();

?>

<br><center>
<table border=0 width=800>
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Visa Detail ::</strong></p>				
  </td>
</tr>
</table>
<br>

<table border=0 width="800"  class="textoazulPeq">
 <tr>
  <td colspan="5" class="textoazul" align="center">Preencha os dados abaixo para o cadastro do Visa Detail</font></td>
 </tr>		
 <tr>
  <td colspan="5"><font color="Red">*</font>&nbsp;Campos obrigatórios</td>
 </tr>		
 <tr><td width=160>&#160;</td><td width=240>&#160;</td><td width=10>&#160;</td><td width=200>&#160;</td><td width=200>&#160;</td></tr>
		
<!-- form action="insereVisaDetail.php" method="post" onSubmit="return validarDados(this)" <?=$target?> -->
<form action="insereVisaDetail.php" method="post" <?=$target?> >
<input type="hidden" name="acao" value="<?=$acao?>">
<input type="hidden" name="NU_CANDIDATO" value="<?=$idCandidato?>">
<input type="hidden" name="NU_EMPRESA" value="<?=$idEmpresa?>">

<tr height="25">
 <td class="textoazul" bgcolor="#DADADA" colspan=5>Empresa: <?=$nomeEmpresa?></td>
</tr>

<?php
include("../inc/visadetail_inc.php");
?>


</table>
<br>


<?php
if($acao=="V") {
  echo "<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();'>\n";
} else {
  echo "<input name='salvar' type='Submit' class='textformtopo' style=\"$estilo\" value='Salvar' $target>\n";
  echo "&#160;&#160;&#160;&#160;";
  echo "<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();'>\n";
}
echo Rodape("");
?>
