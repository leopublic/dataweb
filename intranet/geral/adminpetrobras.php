<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");

$acao = $_POST['acao'];
$idEmpresa = $_POST['idEmpresa'];
$idAdminPetro = $_POST['idAdminPetro'];

if( ($acao=="R") && (strlen($idAdminPetro)>0) ) {
  $sql = "delete from ADMIN_PETROBRAS where CO_ADMIN=$idAdminPetro";
  mysql_query($sql);
  gravaLog($sql,mysql_error(),"REMOCAO","ADMINISTRADORES");
  $idAdminPetro = "";
}

if( strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $sql = "SELECT * FROM ADMIN_PETROBRAS where CO_EMPRESA=$idEmpresa";
  $linhas = mysql_query($sql);
} else {
  $msg = "Não foi informado a empresa.";
}

echo Topo("");
echo Menu("");

?>
<div class="conteudo">
<br><center>
<table border=0 width=700  cellspacing=0>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="3">
   <p align="center" class="textoazul"><strong>:: Cadastro de Administradores e Diretoria ::</strong></p>				
  </td>
 </tr>
 <tr><td colspan="3"><br><?=$msg?></td></tr>
 <tr>
  <td class="textoazul" NOWRAP>Empresa: <b><?=$nomeEmpresa?></b></a></td>
  <td width=200 align=center>
   <input type="button" class="textformtopo" style="<?=$estilo?>" value="Novo Admin" onclick="javascript:inclui();">
  </td>
  <td width=200 align=center>
   <input type="button" class="textformtopo" style="<?=$estilo?>" value="Voltar Empresa" onclick="javascript:empresas();">
  </td>
 </tr>
</table>
<br>
<div style="margin:auto; padding: 10px;">
<table class="grid" style="margin:auto;">
 <tr align="center" height=20>
  <th width=100 bgcolor="#DADADA">A&ccedil;&atilde;o</th>
  <th width=200 bgcolor="#DADADA">Nome</th>
  <th width=150 bgcolor="#DADADA">Cargo / Fun&ccedil;&atilde;o</th>
  <th width=100 bgcolor="#DADADA">Admin/Diretor</th>
  <th width=200 bgcolor="#DADADA">Empresa</th>
</tr>

<?php		   
while($rw1=mysql_fetch_array($linhas)) {
  $cargo=$rw1['NM_CARGO'];
  $idAdminPetro = $rw1['CO_ADMIN'];
  $nomeAdminPetro = $rw1['NM_ADMIN'];			
  $tpcargo = $rw1['TP_CARGO'];
  if($tpcargo == "A") { $tipoCargo = "Administrador"; }
  if($tpcargo == "D") { $tipoCargo = "Diretor"; }
  if($tpcargo == "R") { $tipoCargo = "Representante dos funcionários"; }
  if($tpcargo == "C") { $tipoCargo = "Conselho de administração"; }
  echo "<tr height=20>";
  echo "<td align=center><a href='javascript:mostra($idAdminPetro);'>VER</a>";
  echo "&#160;&#160;<a href='javascript:altera($idAdminPetro);'>ALT</a>";
  if($ehAdmin=="S") {
    echo "&#160;&#160;<a href='javascript:remove($idAdminPetro,\"$nomeAdminPetro\");'>REM</a></td>";
  }
  echo "<td>&#160;$nomeAdminPetro</td>";
  echo "<td>&#160;$cargo</td>";
  echo "<td>&#160;$tipoCargo</td>";
  echo "<td>&#160;$nomeEmpresa</td>";
  echo "</tr>";
}
?>
		
</table>
</div>
<form name=procurador method=post>
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
<input type=hidden name=idAdminPetro>
<input type=hidden name=acao>
</form>

<form name=empresa method=post action="empresas.php">
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
</form>
</center>
</div>
<script language="javascript">
function mostra(codigo) {
  document.procurador.idAdminPetro.value=codigo;
  document.procurador.acao.value="V";
  document.procurador.target="proc_"+codigo;
  document.procurador.action="novoAdminPetro.php";
  document.procurador.submit();
}
function inclui() {
  document.procurador.idAdminPetro.value='';
  document.procurador.acao.value="I";
  document.procurador.target="_top";
  document.procurador.action="novoAdminPetro.php";
  document.procurador.submit();
}
function altera(codigo) {
  document.procurador.idAdminPetro.value=codigo;
  document.procurador.acao.value="A";
  document.procurador.target="_top";
  document.procurador.action="novoAdminPetro.php";
  document.procurador.submit();
}
function remove(codigo,nome) {
  if(confirm("Voce deseja remover o administrador "+nome+" ? ")) {
    document.procurador.idAdminPetro.value=codigo;
    document.procurador.acao.value="R";
    document.procurador.target="_top";
    document.procurador.action="adminpetrobras.php";
    document.procurador.submit();
  } else {
    alert("Operacao nao realizada.");
  }
}
function empresas() {
  document.empresa.submit();
}
</script>

<?php
echo Rodape($opcao);
?>
