<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");

$idCandidato = $_POST['idCandidato'];
$idEmpresa = $_POST['idEmpresa'];
$idDependente = $_POST['idDependente'];
$acao = $_POST['acao'];

if( ($acao=="R") && (strlen($idDependente)>0) ) {
  $sql = "delete from DEPENDENTES where NU_DEPENDENTE=$idDependente";
  mysql_query($sql);
  if(mysql_errno()>0) {
    gravaLogErro($sql,mysql_error(),"REMOCAO","DEPENDENTES");
  } else {
    gravaLog($sql,mysql_error(),"REMOCAO","DEPENDENTES");
  }
  if(mysql_errno()>0) {
    $msg = "Houve um erro ao remover dependente. Tente Novamente.";
  }
}

$nomeCandidato = pegaNomeCandidato($idCandidato);
$nomeEmpresa = pegaNomeEmpresa($idEmpresa);

$sql = "SELECT * FROM DEPENDENTES where NU_CANDIDATO=$idCandidato";
$linhas = mysql_query($sql);

echo Topo("");
echo Menu("");

?>

<br><center>
<table border=0 width=600>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan=3>
	<p align="center" class="textoazul"><strong>:: Cadastro de Dependentes ::</strong></p>
  </td>
 </tr>
 <tr><td><br></td></tr>
 <tr><td colspan="4"><br><?=$msg?></td></tr>
 <tr class="textoazul">
  <td width=100>Empresa:</td><td><b><?=$nomeEmpresa?></b></a></td>
  <td width=200 align=center>
   <input type="button" class="textformtopo" style="<?=$estilo?>" value="Voltar Empresa" onclick="javascript:empresas();">
  </td>
 </tr>
 <tr class="textoazul">
  <td>Candidato:</td><td><b><?=$nomeCandidato?></b></a></td>
  <td align=center>
   <input type="button" class="textformtopo" style="<?=$estilo?>" value="Voltar Candidato" onclick="javascript:candidatos();">
  </td>
 </tr>

<?php
if($ehCliente!="S") {
  echo " <tr><td><br></td></tr>\n";
  echo " <tr><td align=center class='textoazul' colspan=3>\n";
  echo " 	<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Novo dependente' onclick='javascript:inclui();'>\n";
  echo "  </td></tr>\n";
}
?>

</table>

<br>
<table border=1 width=600 class="textoazulpeq"  cellspacing=0>
 <tr align="center">
  <td bgcolor="#DADADA" width=100><b>Ação</td>
  <td bgcolor="#DADADA" width=300><b>Nome</td>
  <td bgcolor="#DADADA" width=200><b>Grau de parentesco</td>
 </tr>
 
<?php
while($rw1=mysql_fetch_array($linhas)) {
  $idDependente = $rw1['NU_DEPENDENTE'];
  $nome = $rw1['NO_DEPENDENTE'];
  $grauParentesco = pegaNomeGrauParentesco($rw1['CO_GRAU_PARENTESCO']);
		
  echo "<tr>";
  echo "<td align=center><a href='javascript:mostra($idDependente);'>VER</a>";
  if($ehCliente!="S") {
    echo "&#160;&#160;<a href='javascript:altera($idDependente);'>ALT</a>";
    if($ehAdmin=="S") {
      echo "&#160;&#160;<a href='javascript:remove($idDependente);'>REM</a></td>";
    }
  }
  echo "<td>&#160;$nome</td>";
  echo "<td align='center'>$grauParentesco</td>";
  echo "</tr>";
}
?>
</table>

<form name=dependente method=post>
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
<input type=hidden name=idCandidato value='<?=$idCandidato?>'>
<input type=hidden name=idDependente>
<input type=hidden name=acao>
</form>

<form name=empresa method=post action="empresas.php">
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
</form>

<form name=candidato method=post action="candidatos.php">
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
<input type=hidden name=idCandidato value='<?=$idCandidato?>'>
</form>

<script language="javascript">
function mostra(codigo) {
  document.dependente.idDependente.value=codigo;
  document.dependente.acao.value="V";
  document.dependente.target="dep_"+codigo;
  document.dependente.action="novoDependente.php";
  document.dependente.submit();
}
function inclui() {
  document.dependente.idDependente.value='';
  document.dependente.acao.value="I";
  document.dependente.target="_top";
  document.dependente.action="novoDependente.php";
  document.dependente.submit();
}
function altera(codigo) {
  document.dependente.idDependente.value=codigo;
  document.dependente.acao.value="A";
  document.dependente.target="_top";
  document.dependente.action="novoDependente.php";
  document.dependente.submit();
}
function remove(codigo) {
  document.dependente.idDependente.value=codigo;
  document.dependente.acao.value="R";
  document.dependente.target="_top";
  document.dependente.action="dependentes.php";
  document.dependente.submit();
}
function candidatos() {
  document.candidato.submit();
}
function empresas() {
  document.empresa.submit();
}
</script>

<?php
echo Rodape($opcao);
?>
