<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
$idAdminPetro = $_POST['idAdminPetro'];
$acao = $_POST['acao'];

if ($idEmpresa != null && $idEmpresa != "") {
    $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
    $condicao = " and b.NU_EMPRESA = $idEmpresa";
}
if ($idAdminPetro != null && $idAdminPetro != "") {
    $condicao = $condicao . " and a.CO_ADMIN = $idAdminPetro ";
}

if ($idEmpresa != null && $idAdminPetro != null) {
    $query = "select a.CO_ADMIN as idAdminPetro, a.CO_EMPRESA as idEmpresa, a.NM_ADMIN as nomeProcurador, a.NM_CARGO as cargo, a.TP_CARGO as tpcargo";
    $query = $query . " from ADMIN_PETROBRAS a, EMPRESA b where a.CO_EMPRESA = b.NU_EMPRESA $condicao ";
    $resultado = mysql_query($query);
    $dados = mysql_fetch_array($resultado);
    $idAdminPetro = $dados['idAdminPetro'];
    $nomeAdminPetro = $dados['nomeProcurador'];
    $nomeCargo = $dados['cargo'];
    $tpcargo = $dados['tpcargo'];
}
$cmbTipoCargo = "<option value=''>Selecione o tipo de cargo";
if ($tpcargo == "A") {
    $nmTipoCargo = "Administrador";
    $cmbTipoCargo = "$cmbTipoCargo<option value='A' selected>Administradores<option value='C'>Conselho de Administração<option value='D'>Diretor<option value='R'>Representante dos empregados";
} elseif ($tpcargo == "C") {
    $nmTipoCargo = "Conselho de Administraçao";
    $cmbTipoCargo = "$cmbTipoCargo<option value='A'>Administradores<option value='C' selected>Conselho de Administração<option value='D' selected>Diretor<option value='R'>Representante dos empregados";
} elseif ($tpcargo == "D") {
    $nmTipoCargo = "Diretor";
    $cmbTipoCargo = "$cmbTipoCargo<option value='A'>Administradores<option value='C'>Conselho de Administração<option value='D' selected>Diretor<option value='R'>Representante dos empregados";
} elseif ($tpcargo == "R") {
    $nmTipoCargo = "Representante dos empregados";
    $cmbTipoCargo = "$cmbTipoCargo<option value='A'>Administradores<option value='C'>Conselho de Administração<option value='D' selected>Diretor<option value='R' selected>Representante dos empregados";
} else {
    $nmTipoCargo = "";
    $cmbTipoCargo = "$cmbTipoCargo<option value='A'>Administradores<option value='C'>Conselho de Administração<option value='D'>Diretor<option value='R'>Representante dos empregados";
}

echo Topo($opcao);
echo Menu("");
?>
<div class="conteudo">

<br><center>
    <table border=0 width=700 class="textoazulPeq">
        <tr>
            <td class="textobasico" align="center" valign="top">
                <p align="center" class="textoazul"><strong>:: Cadastro de Administradores e Diretoria ::</strong></p>				
            </td>
        </tr>
    </table>
    <br>
    <form action="insereAdminPetro.php" method="post" name=admin>
    <table border=0 align="center" width="800" class="textoazulPeq">
        <tr>
            <td colspan="5" class="textoazul">Preencha os dados abaixo para o cadastro de Administradores e Diretoria</font></td>
        </tr>
        <tr>
            <td colspan="5"><font color="Red">*</font>&nbsp;<font class="textoazulPeq">Campos obrigatórios</font><br><br></td>
        </tr>

            <input type="Hidden" name="idAdminPetro" value="<?= $idAdminPetro ?>">
            <input type="Hidden" name="idEmpresa" value='<?= $idEmpresa ?>'>

<?php
include("../inc/adminpetrobras_inc.php");
?>

            </table>
            <br>

            <?php
            if ($acao == "V") {
                echo "<input type='Button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();'>\n";
            } else {
                echo "<input name='salvar' type='button' class='textformtopo' style=\"$estilo\" value='Salvar' onClick='javascript:Enviar();'>\n";
            }
            echo Rodape("");
            ?>
</form>
</center>
</div>
            <script language="javascript">
                function Enviar() {
                    var ret = "";
                    if (document.admin.nomeAdmin.value.length < 5) {
                        ret = ret + "Nome do administrador/diretor invalido.\n";
                    }
                    if (document.admin.nomeCargo.value.length < 5) {
                        ret = ret + "Nome do cargo invalido.\n";
                    }
                    if (document.admin.tipoCargo.selectedIndex <= 0) {
                        ret = ret + "Tipo do cargo invalido.\n";
                    }
                    if (ret.length == 0) {
                        document.admin.submit();
                    } else {
                        alert(ret);
                    }
                }
            </script>
