<?php
$opcao = "EMP";
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/combos.php");

$acao = $_POST['acao'];
$idEmpresa = 0 + $_POST['idEmpresa'];
$idEmbarcacaoProjeto = $_POST['idEmbarcacaoProjeto'];

if( ($acao=="R") && (strlen($idEmbarcacaoProjeto)>0) && ($idEmpresa>0)  ) {
  $sql = "delete from EMBARCACAO_PROJETO  where NU_EMBARCACAO_PROJETO=$idEmbarcacaoProjeto and NU_EMPRESA=$idEmpresa";
  mysql_query($sql);
  if(mysql_errno()>0) {
    gravaLogErro($sql,mysql_error(),"REMOCAO","PROJETO/EMBARCACAO");
  } else {
    gravaLog($sql,mysql_error(),"REMOCAO","PROJETO/EMBARCACAO");
  }
  $idEmbarcacaoProjeto = "";
}

$extraproj = "";
if($usuarmad == "S") {
   $extraproj = " AND NU_ARMADOR=$usulogado";
}

if(strlen($idEmpresa)>0){
  $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
  $sql = "SELECT * FROM EMBARCACAO_PROJETO where NU_EMPRESA=$idEmpresa $extraproj ORDER BY NO_EMBARCACAO_PROJETO";
  $linhas = mysql_query($sql);
} else {
  $msg = "Não foi informado a empresa.";
}

echo Topo("");
echo Menu("");

?>

<br><center>
<table border=0 width=700>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="3">
   <p align="center" class="textoazul"><strong>:: Cadastro de Projetos e Embarca&ccedil;&otilde;es ::</strong></p>				
  </td>
 </tr>
 <tr><td colspan="2"><br><?=$msg?></td></tr>
 <tr>
  <td class="textoazul">Empresa: <b><?=$nomeEmpresa?></b></td>
  <td width=200 align=center>

<?php

if($ehCliente!="S") {
   echo "<input type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Novo Projeto' onclick='javascript:inclui();'>\n";
}

?>

  </td>
  <td width=200 align=center>
   <input type="button" class="textformtopo" style="<?=$estilo?>" value="Voltar Empresa" onclick="javascript:empresas();">
  </td>
 </tr>
</table>
<br>	
<table border=1 width=700 class="textoazulpeq"  cellspacing=0>
 <tr align="center" height=20>
  <td width=150 bgcolor="#DADADA">A&ccedil;&atilde;o</td>
  <td width=350 bgcolor="#DADADA">Projeto / Embarca&ccedil;&atilde;o</td>
  <td width=100 bgcolor="#DADADA">UF</td>
  <td width=100 bgcolor="#DADADA">Prazo do contrato</td>
 </tr>
<?php		   
while($rw1=mysql_fetch_array($linhas)) {
  $idEmbarcacaoProjeto = $rw1['NU_EMBARCACAO_PROJETO'];
  $nome = $rw1['NO_EMBARCACAO_PROJETO'];
  $uf = $rw1['CO_UF'];
  $dataContrato = dataMy2BR($rw1['DT_PRAZO_CONTRATO']);

  echo "<tr height=20>";
  echo "<td align=center><a href='javascript:mostra($idEmbarcacaoProjeto);'>VER</a>";
  if($ehCliente!="S") {
    echo "&#160;&#160;<a href='javascript:altera($idEmbarcacaoProjeto);'>ALT</a>";
    if($ehAdmin=="S") {
      echo "&#160;&#160;<a href='javascript:remove($idEmbarcacaoProjeto,\"$nome\");'>REM</a></td>";
    }
  }
  echo "<td>&#160;$nome</td>";
  echo "<td align=center>$uf</td>";
  echo "<td align=center>$dataContrato</td>";
  echo "</tr>";
}
?>			
		
</table>

<form name=projeto method=post>
<input type=hidden name="idEmpresa" value='<?=$idEmpresa?>'>
<input type=hidden name="idEmbarcacaoProjeto">
<input type=hidden name="acao">
</form>

<form name=empresa method=post action="empresas.php">
<input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
</form>

<script language="javascript">
function mostra(codigo) {
  document.projeto.acao.value="V";
  document.projeto.idEmbarcacaoProjeto.value=codigo;
  document.projeto.target="win_"+codigo;
  document.projeto.action="novoProjetoEmbarcacao.php";
  document.projeto.submit();
}
function inclui() {
  document.projeto.idEmbarcacaoProjeto.value='';
  document.projeto.acao.value="I";
  document.projeto.target="_top";
  document.projeto.action="novoProjetoEmbarcacao.php";
  document.projeto.submit();
}
function altera(codigo) {
  document.projeto.idEmbarcacaoProjeto.value=codigo;
  document.projeto.acao.value="A";
  document.projeto.target="_top";
  document.projeto.action="novoProjetoEmbarcacao.php";
  document.projeto.submit();
}
function remove(codigo,nome) {
  if(confirm("Voce deseja remover o projeto "+nome+" ? ")) {
    document.projeto.idEmbarcacaoProjeto.value=codigo;
    document.projeto.acao.value="R";
    document.projeto.target="_top";
    document.projeto.action="projetosEmbarcacoes.php";
    document.projeto.submit();
  } else {
    alert("Operacao nao realizada.");
  }
}
function empresas() {
  document.empresa.submit();
}
</script>

<?php
echo Rodape($opcao);
?>

