<?php
include ("../../LIB/autenticacao.php");
include ("../../LIB/cabecalho.php");
include ("../../LIB/geral.php");
include ("../../LIB/agenda/agenda_inc.php");
echo Topo($opcao);
echo Menu();
$dia = mktime();
?>
<div class="conteudo" style="text-align:center">
  <div class="boxer" style=" width:500px; margin:0 auto;color: #000;">
    <h4>:: Bem vindo a Intranet da Mundivisas ::</h4>
    <p align="justify" style="padding: 10px ;">
    A Mundivisas tem como foco prestar serviços administrativos e consultoria na área de imigração no Brasil,
    desenvolvendo um atendimento personalizado a todos os seus clientes . Nossas atividades estão voltadas
    para atender os estrangeiros, pessoas físicas e jurídicas, orientando e acompanhando todo tramite necessário
    (procedimentos e obrigações), desde a obtenção do visto consular à entrada e permanência no país.
    </p>
  </div>
	<br />
  <div class="boxer" style="margin:0 auto;width: 500px;">
    <?=AgendaDoDia($dia,$usulogado)?>
  </div>
  <div class="boxer" style="margin:0 auto;width: 800px; margin-top:15px;">
    <?print cCTRL_CONTROLE::ModificacoesRecentes();?>
  </div>
</div>
<?php
if($_SESSION['msg'] != ''){
?>
<script>
  jAlert('<?= htmlentities($_SESSION['msg'], ENT_COMPAT , 'UTF-8'); ?>');
</script>
<?php
//  unset($_SESSION['msg']);
}
echo Rodape($opcao);
?>
