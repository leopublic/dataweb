<script>

/*
Validação de datas:
A explicação a seguir representa os níveis de validação para campos de data. 
Cada variável (reDate + numero) está explicada abaixo.

Estas expressões regulares vão do mais simples ao mais completo, da seguinte forma:

1- Simples — valida apenas o uso de dígitos, nas posições e quantidade certas: 1 a 2 dígitos para dia e para mês, 1 a 4 dígitos para ano. 
2- Média — testa os dígitos possíveis em cada posição: o primeiro dígito do dia, se houver, deve ser de 0 a 3 ([0-3]?\d); o primeiro dígito do mês, se houver, deve ser 0 ou 1 ([01]?\d); passamos a aceitar apenas 2 ou 4 dígitos para o ano. 
3- Avançada — garante as faixas de valores corretas para dias 1 a 31 ((0?[1-9]|[12]\d|3[01])) e meses 1 a 12 ((0?[1-9]|1[0-2])). E aqui optamos por forçar os 2 primeiros dígitos do ano (correspondentes ao século), quando fornecidos, a serem 19 ou 20 ((19|20)?\d{2}). 
4- Completa — valida os dias permitidos de acordo com o mês. Para este último, foram criados três grupos alternativos de pares dia/mês: 
Os dias 1 a 29 ((0?[1-9]|[12]\d)) são aceitos em todos os meses (1 a 12): (0?[1-9]|1[0-2]) 
Dia 30 é válido em todos os meses, exceto fevereiro (02): (0?[13-9]|1[0-2]) 
Dia 31 é permitido em janeiro (01), março (03), maio (05), julho (07), agosto (08), outubro (10) e dezembro (12): (0?[13578]|1[02]). 
5- Tradicional — data no formato DD/MM/AAAA, basicamente é a data Completa, porém sem a opcionalidade do zero à esquerda no dia ou mês menor que 10 e sem a opcionalidade e verificação de século no ano, aceitando qualquer seqüência de 4 dígitos (\d{4}) como ano. 

Para utiliza-las, basta passar como parametro o campo a ser testado + o numero correspondente da validaçao que se deseje fazer.
*/

var reDate1 = /^\d{1,2}\/\d{1,2}\/\d{1,4}$/;
var reDate2 = /^[0-3]?\d\/[01]?\d\/(\d{2}|\d{4})$/;
var reDate3 = /^(0?[1-9]|[12]\d|3[01])\/(0?[1-9]|1[0-2])\/(19|20)?\d{2}$/;
var reDate4 = /^((0?[1-9]|[12]\d)\/(0?[1-9]|1[0-2])|30\/(0?[13-9]|1[0-2])|31\/(0?[13578]|1[02]))\/(19|20)?\d{2}$/;
var reDate5 = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
var reDate = reDate4;

function doDate(pStr, pFmt) {
	eval("reDate = reDate" + pFmt);
	if (reDate.test(pStr)) {
		return true; // Data válida
	} else if (pStr != null && pStr != "") {
		return false; // Data inválida
	}
} // doDate



function validarCNPJ(campo) {
  var CNPJ = campo.value;
  var erro = new String;

  if (CNPJ.length < 14) {
    return false; //Verifico 14 caracteres pois não deve ser digitado '.' ou '/'
  }
//if ((CNPJ.charAt(2) != ".") || (CNPJ.charAt(6) != ".") || (CNPJ.charAt(10) != "/") || (CNPJ.charAt(15) != "-")){
//	if (erro.length == 0) return false;
//}

//substituir os caracteres que nao sao numeros
  if(document.layers && parseInt(navigator.appVersion) == 4){
    x = CNPJ.substring(0,2);
    x += CNPJ.substring(3,6);
    x += CNPJ.substring(7,10);
    x += CNPJ.substring(11,15);
    x += CNPJ.substring(16,18);
    CNPJ = x; 
  } else {
    CNPJ = CNPJ.replace(".","");
    CNPJ = CNPJ.replace(".","");
    CNPJ = CNPJ.replace("-","");
    CNPJ = CNPJ.replace("/","");
  }
  var nonNumbers = /\D/;
  if (nonNumbers.test(CNPJ)) {
    return false; 
  }
  var a = [];
  var b = new Number;
  var c = [6,5,4,3,2,9,8,7,6,5,4,3,2];
  for (i=0; i<12; i++){
    a[i] = CNPJ.charAt(i);
    b += a[i] * c[i+1];
  }
  if ((x = b % 11) < 2) { a[12] = 0 } else { a[12] = 11-x }
  b = 0;
  for (y=0; y<13; y++) {
    b += (a[y] * c[y]); 
  }
  if ((x = b % 11) < 2) { 
    a[13] = 0; 
  } else { 
    a[13] = 11-x; 
  }
  if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])) {
    return false;
  }
  return true;
} // validarCNPJ()


function validarCPF(campo) {
  var CPF = campo.value; // Recebe o valor digitado no campo

// Aqui começa a checagem do CPF
  var POSICAO, I, SOMA, DV, DV_INFORMADO;
  var DIGITO = new Array(10);
  DV_INFORMADO = CPF.substr(9, 2); // Retira os dois últimos dígitos do número informado

// Desemembra o número do CPF na array DIGITO
  for (I=0; I<=8; I++) {
    DIGITO[I] = CPF.substr( I, 1);
  }

// Calcula o valor do 10º dígito da verificação
  POSICAO = 10;
  SOMA = 0;
  for (I=0; I<=8; I++) {
    SOMA = SOMA + DIGITO[I] * POSICAO;
    POSICAO = POSICAO - 1;
  }
  DIGITO[9] = SOMA % 11;
  if (DIGITO[9] < 2) {
    DIGITO[9] = 0;
  } else{
    DIGITO[9] = 11 - DIGITO[9];
  }

// Calcula o valor do 11º dígito da verificação
  POSICAO = 11;
  SOMA = 0;
  for (I=0; I<=9; I++) {
    SOMA = SOMA + DIGITO[I] * POSICAO;
    POSICAO = POSICAO - 1;
  }
  DIGITO[10] = SOMA % 11;
  if (DIGITO[10] < 2) {
    DIGITO[10] = 0;
  } else {
    DIGITO[10] = 11 - DIGITO[10];
  }

// Verifica se os valores dos dígitos verificadores conferem
  DV = DIGITO[9] * 10 + DIGITO[10];
  if (DV != DV_INFORMADO) {      
    campo.value = '';
    campo.focus();
    return false;
  } 
  return true;
} // validarCPF()


</script>
