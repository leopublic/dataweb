<script>

/*
Validação de datas:
A explicação a seguir representa os níveis de validação para campos de data. 
Cada variável (reDate + numero) está explicada abaixo.

Estas expressões regulares vão do mais simples ao mais completo, da seguinte forma:

1- Simples — valida apenas o uso de dígitos, nas posições e quantidade certas: 1 a 2 dígitos para dia e para mês, 1 a 4 dígitos para ano. 
2- Média — testa os dígitos possíveis em cada posição: o primeiro dígito do dia, se houver, deve ser de 0 a 3 ([0-3]?\d); o primeiro dígito do mês, se houver, deve ser 0 ou 1 ([01]?\d); passamos a aceitar apenas 2 ou 4 dígitos para o ano. 
3- Avançada — garante as faixas de valores corretas para dias 1 a 31 ((0?[1-9]|[12]\d|3[01])) e meses 1 a 12 ((0?[1-9]|1[0-2])). E aqui optamos por forçar os 2 primeiros dígitos do ano (correspondentes ao século), quando fornecidos, a serem 19 ou 20 ((19|20)?\d{2}). 
4- Completa — valida os dias permitidos de acordo com o mês. Para este último, foram criados três grupos alternativos de pares dia/mês: 
Os dias 1 a 29 ((0?[1-9]|[12]\d)) são aceitos em todos os meses (1 a 12): (0?[1-9]|1[0-2]) 
Dia 30 é válido em todos os meses, exceto fevereiro (02): (0?[13-9]|1[0-2]) 
Dia 31 é permitido em janeiro (01), março (03), maio (05), julho (07), agosto (08), outubro (10) e dezembro (12): (0?[13578]|1[02]). 
5- Tradicional — data no formato DD/MM/AAAA, basicamente é a data Completa, porém sem a opcionalidade do zero à esquerda no dia ou mês menor que 10 e sem a opcionalidade e verificação de século no ano, aceitando qualquer seqüência de 4 dígitos (\d{4}) como ano. 

Para utiliza-las, basta passar como parametro o campo a ser testado + o numero correspondente da validaçao que se deseje fazer.
*/

var reDate1 = /^\d{1,2}\/\d{1,2}\/\d{1,4}$/;
var reDate2 = /^[0-3]?\d\/[01]?\d\/(\d{2}|\d{4})$/;
var reDate3 = /^(0?[1-9]|[12]\d|3[01])\/(0?[1-9]|1[0-2])\/(19|20)?\d{2}$/;
var reDate4 = /^((0?[1-9]|[12]\d)\/(0?[1-9]|1[0-2])|30\/(0?[13-9]|1[0-2])|31\/(0?[13578]|1[02]))\/(19|20)?\d{2}$/;
var reDate5 = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
var reDate = reDate4;

function doDate(pStr, pFmt) {
	eval("reDate = reDate" + pFmt);
	if (reDate.test(pStr)) {
		return true; // Data válida
	} else if (pStr != null && pStr != "") {
		return false; // Data inválida
	}
} // doDate

function initArray() {
  this.length = initArray.arguments.length;
  for (var i = 0 ; i < 14 ; i++)  {
    this[i] = " ";
  }
}

function mod(ini, fim){
  t = ini % fim;
  return t;
}

function validarCNPJ(campo) {
  var CNPJ = SoNumeros(campo);
  return testaCgc(CNPJ);
} 


function validarCPF(campo) {
  var CPF = SoNumeros(campo); 
  return testaCPF(CPF);
} // validarCPF()


function testaCPF(CPF) {
  if ( CPF == 11111111111 || CPF == 22222222222 || CPF == 33333333333 || CPF == 44444444444 || CPF == 55555555555 || CPF == 66666666666 || CPF == 77777777777 || CPF == 88888888888 || CPF == 99999999999 || CPF == 00000000000 ) {
    return false;
  }
  var POSICAO, I, SOMA, DV, DV_INFORMADO;
  var DIGITO = new Array(10);
  DV_INFORMADO = CPF.substr(9, 2); // Retira os dois últimos dígitos do número informado
  for (I=0; I<=8; I++) {
    DIGITO[I] = CPF.substr( I, 1);
  }
  POSICAO = 10;
  SOMA = 0;
  for (I=0; I<=8; I++) {
     SOMA = SOMA + DIGITO[I] * POSICAO;
     POSICAO = POSICAO - 1;
  }
  DIGITO[9] = SOMA % 11;
  if (DIGITO[9] < 2) {
    DIGITO[9] = 0;
  } else {
    DIGITO[9] = 11 - DIGITO[9];
  }
  POSICAO = 11; 
  SOMA = 0;
  for (I=0; I<=9; I++) {
    SOMA = SOMA + DIGITO[I] * POSICAO;
    POSICAO = POSICAO - 1;
  }
  DIGITO[10] = SOMA % 11;
  if (DIGITO[10] < 2) {
     DIGITO[10] = 0;
  } else {
     DIGITO[10] = 11 - DIGITO[10];
  }
  DV = DIGITO[9] * 10 + DIGITO[10];
  if (DV != DV_INFORMADO) {
    return false;
  } 
  return true;
}

function testaCgc(txtCnpj) {
  if (txtCnpj == 11111111111111 || txtCnpj == 22222222222222 || txtCnpj == 33333333333333 || txtCnpj == 44444444444444 || txtCnpj == 55555555555555 || txtCnpj == 66666666666666 || txtCnpj == 77777777777777 || txtCnpj == 888888888888888 || txtCnpj == 999999999999999 || txtCnpj == 00000000000000){
    return false;
  }
  var num1 = new initArray(14);
  if(txtCnpj == null) {
    return false;
  }
  if(txtCnpj.length != 14) {
    return false;
  }
  for (var i = 0 ; i < 14 ; i++) {
    num1[i] = txtCnpj.substring(i, i+1);
  }
  digito13 = calculaDigito(13, num1);
  digito14 = calculaDigito(14, num1);
  if (num1[12]==(digito13) && num1[13]==(digito14)){
    return true;  
  } else {
    return false;  
  }
}

function calculaDigito( cgc_limite,  num) {
  cgc_soma = 0;
  cgc_ind = 1;
  cgc_peso = cgc_limite - 7 - cgc_ind;
  while(cgc_ind < cgc_limite) {
    cgc_soma += num[cgc_ind - 1] * cgc_peso;
    cgc_ind++;
    if(cgc_peso == 2) {
         cgc_peso = 9;
    } else {
         cgc_peso--;
    }
  }
  cgc_resto = mod(cgc_soma, 11);
  if(cgc_resto == 0 || cgc_resto == 1) {
    cgc_digito = 0;
  } else {
    cgc_digito = 11 - cgc_resto;
  }
  return cgc_digito;
}

function SoNumeros(campo) {
  //retorna so numeros
  var valor = campo.value;
  var tam = valor.length;
  var naux = "0123456789";
  var aux = "";
  for (i=0;i<tam;i++) {
     comp = false;
     for (j=0;j<naux.length;j++) {
	if (valor.charAt(i) == naux.charAt(j)) { comp = true; }
     }
     if(comp == true) { 
        aux = aux+''+valor.charAt(i);
     }
  }
  return aux;
}

</script>
