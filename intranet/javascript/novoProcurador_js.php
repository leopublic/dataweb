<script language="javascript">
function validarDados(){
 msg = "";
 if (document.forms[0].nomeProcurador.value == "") { 
   	msg += "* Preencha o campo Nome do procurador\n";
 }  
 if (document.forms[0].idCargo.value == "") {
   	msg += "* Preencha o campo Cargo do procurador\n";
 } 
 if (document.forms[0].cpfProcurador.value == "") { 
   	msg += "* Preencha o CPF do procurador\n";
 } else {
 	if ( !validarCPF(document.forms[0].cpfProcurador) ){
		msg += "* Campo CPF inválido\n";
	}
 }
//  if (document.forms[0].dataEmissaoIdentProcurador.value != "") { 
//    if( !doDate(document.forms[0].dataEmissaoIdentProcurador.value,5) )
//		msg += "* Campo Data de emissão inválida. Verifique o formato dd/mm/yyyy\n";
// }  
 if ( msg != ""){
	alert(msg);
	return false;
 } else {
	return true;
 }	
}
</script>
