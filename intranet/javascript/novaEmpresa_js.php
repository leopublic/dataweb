<script language="javascript">
function validarDados(){
 msg = "";
 if (document.forms[0].razaoSocial.value == "") { 
   	msg += "* Preencha o campo Razão social\n";
 }  
 if (document.forms[0].atividadeEconomica.value == "") { 
   	msg += "* Preencha o campo Atividade econômica\n";
 } 	
 if (document.forms[0].cnpj.value == "") { 
   	msg += "* Preencha o campo CNPJ\n";
 } else {
    if ( !validarCNPJ(document.forms[0].cnpj) ) {
	msg += "* Campo CNPJ inválido\n";
    }
 }
 if (document.forms[0].endereco.value == "") { 
   	msg += "* Preencha o campo Endereço\n";
 } 
  if (document.forms[0].bairro.value == "") { 
   	msg += "* Preencha o campo Bairro\n";
 } 
  if (document.forms[0].municipio.value == "") { 
   	msg += "* Preencha o campo Município\n";
 } 
  if (document.forms[0].uf.value == "") { 
   	msg += "* Preencha o campo UF\n";
 } 
  if (document.forms[0].ddd.value == "") { 
   	msg += "* Preencha o campo DDD\n";
    if (isNaN(document.forms[0].ddd.value)){
 	  msg += "* Campo DDD deve conter somente números\n";
    }
 }
 if (document.forms[0].telefone.value == "") { 
   	msg += "* Preencha o campo Telefone\n";
 } 
 if (document.forms[0].cep.value == "") { 
   	msg += "* Campo CEP deve ser preenchidos\n";
 } 
 if (document.forms[0].qtdEmpregados.value != "" && isNaN(document.forms[0].qtdEmpregados.value)) { 
   	msg += "* Campo Quantidade de Empregados deve conter somente números\n";
 } 
 if (document.forms[0].qtdEmpregadosEstrangeiro.value != "" && isNaN(document.forms[0].qtdEmpregadosEstrangeiro.value)) { 
   	msg += "* Campo Quantidade de Empregados Estrangeiros deve conter somente números\n";
 } 
 if (document.forms[0].dataConstituicaoEmpresa.value != "" ) { 
    if( !doDate(document.forms[0].dataConstituicaoEmpresa.value,5) ) {
		msg += "* Campo Data de constituição da empresa inválida. Verifique o formato dd/mm/yyyy\n";
	}
 } 
 if (document.forms[0].dataAlteracaoContratual.value != "") { 
    if( !doDate(document.forms[0].dataAlteracaoContratual.value,5) ) {
		msg += "* Campo Data de alteração contratual inválida. Verifique o  formato dd/mm/yyyy\n";
	}
 } 
 if (document.forms[0].dataInvestimentoEstrangeiro.value != "") { 
    if( !doDate(document.forms[0].dataInvestimentoEstrangeiro.value,5) ) {
		msg += "* Campo Data de investimento estrangeiro inválida. Verifique o formato dd/mm/yyyy\n";
	}
 }  
 if ( msg != ""){
	alert(msg);
	return false;
 } else {
	return true;
 }	
}
</script>
