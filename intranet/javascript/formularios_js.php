<script language="javascript">

function escolher() {
  document.form1.action = "formindex.php";
  document.form1.target = "_top";
  document.form1.submit();
}

function pedVisto(pagina) {
  if(podefazer()==true) {
    document.form1.pedido.value = "pedidodevisto_p"+pagina;
    document.form1.action = "formulariosoutros.php";
    document.form1.target = "pedVisto"+pagina;
    document.form1.submit();
  }
}

// ----------------  TODOS  --------------------------------

function formDadosCadastrais() {
  var ret = podefazer();
  if(ret == '') {
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if(proc == '') {
      alert("Escolha o procurador.");
    } else {
      document.form1.action = "/intranet/formularios/all_formDadosCadastrais.php";
      document.form1.target = "formDadosCadastrais";
      document.form1.submit();
    }
  } else {
    alert(ret);
  }
}

function formAutorizacaoTrabalho() {
  var ret = podefazer();
  if(ret == '') {
    var prazo = document.form1.prazo_contrato.value;
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if( (prazo = '') || (proc == '') ) { 
      alert("Entre com o procurador.");
    } else {
      document.form1.action = "/intranet/formularios/all_formAutorizacaoTrabalho.php";
      document.form1.target = "formAutorizacaoTrabalho";
      document.form1.submit();
    }
  } else {
    alert(ret);
  }
}

function termoResponsabilidadeEmbarcacao() {
  var ret = podefazer();
  if(ret == '') {
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if(proc == '') {
      alert("Escolha o procurador.");
    } else {
      document.form1.action = "/intranet/formularios/all_termoResponsabilidadeEmbarcacao.php";
      document.form1.target = "termoResponsabilidadeEmbarcacao";
      document.form1.submit();
    }
  } else {
    alert(ret);
  } 
}

function termoResponsabilidadeRepatriamento() {
  var ret = podefazer();
  if(ret == '') {
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if(proc == '') {
      alert("Escolha o procurador.");
    } else {
      document.form1.action = "/intranet/formularios/all_termoResponsabilidadeRepatriamento.php";
      document.form1.target = "termoResponsabilidadeRepatriamento";
      document.form1.submit();
    }
  } else {
    alert(ret);
  } 
}

function formRequerimentoProrrogacao() {
  var ret = podefazer();
  if(ret == '') {
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if(proc == '') {
      alert("Escolha o procurador.");
    } else {
      document.form1.action = "/intranet/formularios/all_formRequerimentoProrrogacao.php";
      document.form1.target = "pedModelo";
      document.form1.submit();
    }
  } else {
    alert(ret);
  }
}

function formRegistroPF() {
  var ret = podefazer();
  if(ret == '') {
    document.form1.action = "/intranet/formularios/all_registroPF.php";
    document.form1.target = "pedRegistroPF";
    document.form1.submit();
  }
}


// --------------  PETROBRAS  ----------------------------------------

function petroformDadosCadastrais() {
  var ret = podefazer();
  if(ret == '') {
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if(proc == '') {
      alert("Escolha o procurador.");
    } else {
      document.form1.action = "/intranet/formularios/apetro_formDadosCadastrais.php";
      document.form1.target = "petroformDadosCadastrais";
      document.form1.submit();
    }
  } else {
    alert(ret);
  }
}

function petroDeclaracaoEmbarcacoes() {
  var ret = podefazer();
  if(ret == '') {
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if(proc == '') {
      alert("Escolha o procurador.");
    } else {
      document.form1.pedido.value = "listaembarcacoes";
      document.form1.action = "/intranet/formularios/apetro_DeclaracaoEmbarcacoes.php";
      document.form1.target = "petroDeclaracaoEmbarcacoes";
      document.form1.submit();
    }
  } else {
    alert(ret);
  }
}

// --------------------  OUTRAS  --------------------------------------------

function outrosDeclaracaoEmbarcacoes() {
  var ret = podefazer();
  if(ret == '') {
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if(proc == '') {
      alert("Escolha o procurador.");
    } else {
      document.form1.pedido.value = "listaembarcacoes";
      document.form1.action = "/intranet/formularios/aoutros_DeclaracaoEmbarcacoes.php";
      document.form1.target = "outrosDeclaracaoEmbarcacoes";
      document.form1.submit();
    }
  } else {
    alert(ret);
  }
}

function outrosContratoTrabalhoDependentes() {
  var ret = podefazer();
  if(ret == '') {
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if(proc == '') {
      alert("Escolha o procurador.");
    } else {
      document.form1.action = "/intranet/formularios/aoutros_ContratoTrabalhoDep.php";
      document.form1.target = "outrosContratoTrabalhoDependentes";
      document.form1.submit();
    }
  } else {
    alert(ret);
  } 
}


// --------------------  VISA FORM  --------------------------------------------

function VisaForm() {
  return podefazer();
}

function visaform(visa) {
  var ret = VisaForm();
  if(ret == '') {
    document.form1.pedido.value=visa;
    var proc = document.form1.idProcurador[document.form1.idProcurador.selectedIndex].value;
    if(proc == '') {
      alert("Escolha o procurador.");
    } else {
      if(visa == "HOUSTON") {
        document.form1.action = "/intranet/formularios/visa_form_1.php";
      } else if(visa == "CINGAPURA") {
        document.form1.action = "/intranet/formularios/visa_form_1.php";
      }
      document.form1.target = "visaformWindow";
      document.form1.submit();
    }
  } else {
    alert(ret);
  } 

}

// ----------------------------------------------------------//

function podefazer() {
  var ret = "";
  if(document.form1.montaform.value > 0) { 
    if(!(document.form1.tpSolicitacao[0].checked || document.form1.tpSolicitacao[1].checked)) {
      ret = "Voce deve escolher o tipo de solicitação para continuar.";
    }
  } else {
    ret = "Voce deve escolher Empresa/Candidato para continuar.";
  }
  return ret;
}

function recupera() {
  alert("Funcao em desenvolvimento");
}

function modelo() {
  if(podefazer()==true) {
    document.form1.pedido.value = "registropf";
//    document.form1.action = "formularios/modelo_pdf.php";
    document.form1.action = "formularios/outros_respdependentes_new.php";
    document.form1.target = "modelo";
    document.form1.submit();
  }
}

</script>

