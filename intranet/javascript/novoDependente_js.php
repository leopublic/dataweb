<script language="javascript">
function validarDados(){
 msg = "";
 if (document.forms[0].nome.value == "") { 
   	msg += "* Preencha o campo Nome\n";
 }  
 if (document.forms[0].idGrauParentesco.value == "") {
   	msg += "* Preencha o campo Grau de parentesco\n";
 } 
 if (document.forms[0].idNacionalidade.value == "") { 
   	msg += "* Preencha o campo Nacionalidade\n";
 }
 if (document.forms[0].passaporte.value == "") { 
   	msg += "* Preencha o campo Número do passaporte\n";
 }
 if (document.forms[0].idPaisEmissorPassaporte.value == "") { 
   	msg += "* Preencha o campo País emissor do passaporte\n";
 }
 if (document.forms[0].dataNascimento.value != "") { 
    if( !doDate(document.forms[0].dataNascimento.value,5) ) {
		msg += "* Campo Data de nascimento inválida. Verifique o formato dd/mm/yyyy\n";
	}
 }  
 if ( msg != ""){
	alert(msg);
	return false;
 } else {
	return true;
 }	
}
</script>
