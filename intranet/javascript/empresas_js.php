<script language="javascript">
function mostra(codigo) {
  document.empresa.idEmpresa.value = codigo;
  document.empresa.acao.value = "V";
  document.empresa.action = "novaEmpresa.php";
  document.empresa.target = "emp"+codigo;
  document.empresa.submit();
}
function altera(codigo) {
  document.empresa.idEmpresa.value = codigo;
  document.empresa.acao.value = "A";
  document.empresa.action = "novaEmpresa.php";
  document.empresa.target = "_top";
  document.empresa.submit();
}
function inclui() {
  document.empresa.idEmpresa.value = '';
  document.empresa.acao.value = "I";
  document.empresa.action = "novaEmpresa.php";
  document.empresa.target = "_top";
  document.empresa.submit();
}
function remove(codigo,nome) {
  if(confirm("Voce deseja remover a empresa "+nome+" ? ")) {
    document.empresa.idEmpresa.value = codigo;
    document.empresa.acao.value = "R";
    document.empresa.action = "empresas.php";
    document.empresa.target = "_top";
    document.empresa.submit();
  } else {
    alert("Operacao nao realizada.");
  }
}
function procuradores(codigo) {
  document.empresa.target = "_top";
  document.empresa.idEmpresa.value = codigo;
  document.empresa.action = "procuradores.php";
  document.empresa.submit();
}
function candidatos(codigo) {
  document.empresa.target = "_top";
  document.empresa.idEmpresa.value = codigo;
  document.empresa.action = "candidatos.php";
  document.empresa.submit();
}
function projetos(codigo) {
  document.empresa.target = "_top";
  document.empresa.idEmpresa.value = codigo;
  document.empresa.action = "projetosEmbarcacoes.php";
  document.empresa.submit();
}
function diretoria(codigo) {
  document.empresa.target = "_top";
  document.empresa.idEmpresa.value = codigo;
  document.empresa.action = "adminpetrobras.php";
  document.empresa.submit();
}
function associadas(codigo) {
  document.empresa.target = "_top";
  document.empresa.idEmpresa.value = codigo;
  document.empresa.action = "associadas.php";
  document.empresa.submit();
}
function lista() {
  document.empresa.idEmpresa.value = "";
  document.empresa.acao.value = "T";
  document.empresa.action = "empresas.php";
  document.empresa.target = "_top";
  document.empresa.submit();
}
</script>
