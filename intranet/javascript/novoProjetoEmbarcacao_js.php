<script language="javascript">
function validarDados(){
 msg = "";
 if (document.forms[0].idEmpresa.value == "") { 
   	msg += "* Preencha o campo Empresa\n";
 }  
 if (document.forms[0].nomeEmbarcacaoProjeto.value == "") {
   	msg += "* Preencha o campo Nome do Projeto\n";
 } 
 if( (document.forms[0].tipo[0].checked == false) && (document.forms[0].tipo[1].checked == false) && (document.forms[0].tipo[2].checked == false) ) { 
   	msg += "* Escolha o Tipo\n";
 }
 if (document.forms[0].municipio.value == "") { 
   	msg += "* Preencha o campo Município\n";
 }
 if (document.forms[0].uf.value == "") { 
   	msg += "* Preencha o campo UF\n";
 }
 if (document.forms[0].qtdTripulantes.value != "") { 
   if (isNaN(document.forms[0].qtdTripulantes.value)) { 
	  msg += "* Campo Quantidade de tripulantes deve conter somente números\n";   		
   } 	
 }
 if (document.forms[0].qtdTripulantesEstrangeiros.value != "") { 
   if (isNaN(document.forms[0].qtdTripulantesEstrangeiros.value)) { 
	  msg += "* Campo Quantidade de tripulantes estrangeiros deve conter somente números\n";   		
   } 	
 }
 if (document.forms[0].dataContrato.value != "") { 
    if( !doDate(document.forms[0].dataContrato.value,5) ) {
		msg += "* Campo Prazo contrato inválida. Verifique o formato dd/mm/yyyy\n";
	}
 }  
 if ( msg != ""){
	alert(msg);
	return false;
 } else {
	return true;
 }	
}
</script>
