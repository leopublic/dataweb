<script language="javascript">
function validarDados(){
 msg = "";
 if (document.forms[0].CO_TIPO_AUTORIZACAO.value == "") { 
   	msg += "* Escolha o tipo de Visto\n";
 }
 if (document.forms[0].primeiroNome.value == "") { 
   	msg += "* Preencha o campo Primeiro nome\n";
 }  
 if (document.forms[0].email.value == "") {
   	msg += "* Preencha o campo Email\n";
 }   
 if (document.forms[0].endereco.value == "") {
   	msg += "* Preencha o campo Endereço\n";
 } 
 if (document.forms[0].cidade.value == "") {
   	msg += "* Preencha o campo Cidade\n";
 }  
 if (document.forms[0].idPais.value == "") {
   	msg += "* Preencha o campo País\n";
 } 
 if (document.forms[0].telefone.value == "") {
   	msg += "* Preencha o campo Telefone\n";
 }  
 if (document.forms[0].nomePai.value == "") {
   	msg += "* Preencha o campo Nome do pai\n";
 }  
 if (document.forms[0].nomeMae.value == "") {
   	msg += "* Preencha o campo Nome da mãe\n";
 }  
 if (document.forms[0].idNacionalidade.value == "") {
   	msg += "* Preencha o campo Nacionalidade\n";
 }  
 if (document.forms[0].idEscolaridade.value == "") {
   	msg += "* Preencha o campo Escolaridade\n";
 }  
 if (document.forms[0].idEstadoCivil.value == "") {
   	msg += "* Preencha o campo Estado civil\n";
 }  
 if ( !  (  (document.forms[0].sexo[0].checked == true) || (document.forms[0].sexo[1].checked == true) ) ) {
   	msg += "* Preencha o campo Sexo\n";
 }  
 if (document.forms[0].idPaisEmissorPassaporte.value == "") {
   	msg += "* Preencha o campo País emissor do passaporte\n";
 }  
 if (document.forms[0].idProfissao.value == "") {
   	msg += "* Preencha o campo Profissão\n";
 }  
 if (document.forms[0].idFuncao.value == "") {
   	msg += "* Preencha o campo Função\n";
 }  
 if (document.forms[0].salTotal.value == "") {
   	msg += "* Preencha o campo Salário Total\n";
 }  
 if (document.forms[0].salBrasil.value == "") {
   	msg += "* Preencha o campo Salário no Brasil\n";
 }  
 if (document.forms[0].dataNascimento.value != "") { 
    if( !doDate(document.forms[0].dataNascimento.value,5) ) {
		msg += "* Campo Data de nascimento inválida. Verifique o formato dd/mm/yyyy\n";
	}
 }  
 if (document.forms[0].dataEmissaoPassaporte.value != "") { 
    if( !doDate(document.forms[0].dataEmissaoPassaporte.value,5) ) {
		msg += "* Campo Data de emissão do passaporte inválida. Verifique o formato dd/mm/yyyy\n";
	}
 }  
 if (document.forms[0].dataValidadePassaporte.value != "") { 
    if( !doDate(document.forms[0].dataValidadePassaporte.value,5) ) {
		msg += "* Campo Data de validade do passaporte inválida. Verifique o formato dd/mm/yyyy\n";
	}
 }
 if (document.forms[0].cpf.value != "") { 
 	if ( !validarCPF(document.forms[0].cpf) ){
		msg += "* Campo CPF inválido\n";
	}
 }
 if (document.forms[0].idPaisExt.value == "") {
   	msg += "* Preencha o campo País de Residência\n";
 } 
 if (document.forms[0].TE_DESCRICAO_ATIVIDADES.value == "") {
   	msg += "* Preencha o campo Descrição das Atividades\n";
 }  
 if ( msg != ""){
	alert(msg);
	return false;
 } else {
	 if (confirm('Voce deseja mesmo salvar as alteracoes?'))
	 {
			return true;		 
	 }
	 else
	 {
		alert ('Operacao cancelada');
		return false;
	 }
			 
 }	
}

</script>
