drop procedure if exists RenumerarProcessosCandidato;
create procedure RenumerarProcessosCandidato(pcd_candidato int) 
BEGIN
  DECLARE eof INT DEFAULT FALSE;
  DECLARE xordem int ;
  DECLARE xtipo varchar(7);
  DECLARE xcd_candidato, xcodigo, xcodigo_mte int;
  DECLARE mtes CURSOR FOR 
      SELECT codigo  
      FROM processo_mte
      WHERE cd_candidato = xcd_candidato ;

  DECLARE processos CURSOR FOR 
      SELECT tipo, codigo  
      FROM vProcesso 
      WHERE cd_candidato = xcd_candidato 
      AND tipo <> 'mte' 
      AND codigo_processo_mte = xcodigo_mte 
      ORDER BY dt_cad ;
      
      
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET eof = TRUE;

  set xcd_candidato = pcd_candidato;
    
    OPEN mtes;
    loop_mtes: LOOP
      FETCH mtes INTO xcodigo_mte;
      IF eof THEN
        LEAVE loop_mtes;
      END IF;
      set xordem = 0;

      OPEN processos;
      loop_processos: LOOP
        FETCH processos INTO xtipo, xcodigo;
        IF eof THEN
          LEAVE loop_processos;
        END IF;
        set xordem = xordem + 10;
        CASE xtipo
          WHEN 'cancel'  THEN update processo_cancel  set nu_ordem = xordem where codigo = xcodigo;
          WHEN 'coleta'  THEN update processo_coleta  set nu_ordem = xordem where codigo = xcodigo;
          WHEN 'emiscie' THEN update processo_emiscie set nu_ordem = xordem where codigo = xcodigo;
          WHEN 'prorrog' THEN update processo_prorrog set nu_ordem = xordem where codigo = xcodigo;
          WHEN 'regcie'  THEN update processo_regcie  set nu_ordem = xordem where codigo = xcodigo;
        END CASE;
        
      END LOOP;
      CLOSE processos;
      set eof = false;
      
    END LOOP;
    CLOSE mtes; 
    set eof = false;
END;
;
