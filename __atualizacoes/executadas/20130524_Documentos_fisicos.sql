CREATE  TABLE IF NOT EXISTS tipo_documento (
  tdoc_id INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  tdoc_tx_descricao VARCHAR(200) NULL ,
  tdoc_tx_sigla VARCHAR(10) NULL ,
  PRIMARY KEY (tdoc_id) )
ENGINE = InnoDB
;
truncate tipo_documento
;
insert into tipo_documento(tdoc_tx_descricao, tdoc_tx_sigla) 
	values ('C�pia do passaporte', 'Passporte')
	, ('Formul�rio 1344 assinado', 'Form 144')
;
CREATE  TABLE IF NOT EXISTS tipo_acompanhamento_tipo_documento (
  tdoc_id INT UNSIGNED NOT NULL ,
  ID_TIPO_ACOMPANHAMENTO INT NOT NULL ,
  tatd_dt_inclusao DATETIME NULL ,
  cd_usuario_inclusao INT NULL ,
  PRIMARY KEY (tdoc_id, ID_TIPO_ACOMPANHAMENTO) ,
  INDEX fk_tipo_acompanhamento_tipo_documento_tipo_acompanhamento (ID_TIPO_ACOMPANHAMENTO ASC) ,
  INDEX fk_tipo_acompanhamento_tipo_documento_tipo_documento (tdoc_id ASC) ,
  CONSTRAINT fk_tipo_acompanhamento_tipo_documento_tipo_acompanhamento
    FOREIGN KEY (ID_TIPO_ACOMPANHAMENTO )
    REFERENCES dataweb.tipo_acompanhamento (ID_TIPO_ACOMPANHAMENTO )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_tipo_acompanhamento_tipo_documento_tipo_documento
    FOREIGN KEY (tdoc_id )
    REFERENCES dataweb.tipo_documento (tdoc_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
;
CREATE  TABLE IF NOT EXISTS documento (
  docu_id INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  cd_usuario_inclusao INT UNSIGNED NULL ,
  docu_dt_inclusao DATETIME NULL ,
  docu_tx_localizacao VARCHAR(200) NULL ,
  tdoc_id INT UNSIGNED NULL ,
  nu_candidato INT NULL ,
  codigo int NULL ,
  docu_dt_utilizado DATETIME NULL,
  cd_usuario_utilizou INT NULL ,
  docu_tx_tipo_processo VARCHAR(30) NULL ,
  PRIMARY KEY (docu_id) ,
  INDEX fk_documento_tipo_documento (tdoc_id ASC) ,
  INDEX fk_documento_candidato (nu_candidato ASC) ,
  CONSTRAINT fk_documento_tipo_documento
    FOREIGN KEY (tdoc_id )
    REFERENCES dataweb.tipo_documento (tdoc_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_documento_candidato
    FOREIGN KEY (nu_candidato )
    REFERENCES dataweb.candidato (NU_CANDIDATO )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
;