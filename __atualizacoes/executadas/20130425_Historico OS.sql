-- Executado 04/05/2013
alter table usuarios 
	engine = InnoDB
;
alter table usuarios 
	modify cd_usuario int(11) unsigned not null auto_increment
;
drop table if exists historico_os
;
CREATE  TABLE historico_os (
  hios_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  id_solicita_visto INT(10) UNSIGNED NULL ,
  id_status_sol INT(11) NULL ,
  hios_dt_evento DATETIME NULL ,
  cd_usuario INT(11) UNSIGNED NULL ,
  hios_tx_observacoes VARCHAR(1000) NULL ,
  PRIMARY KEY (hios_id) ,
  INDEX fk_solicita_visto (id_solicita_visto ASC) ,
  INDEX fk_status_sol (id_status_sol ASC) ,
  INDEX fk_usuarios (cd_usuario ASC) ,
  CONSTRAINT fk_historico_os_solicita_visto
    FOREIGN KEY (id_solicita_visto )
    REFERENCES solicita_visto (id_solicita_visto )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT fk_historico_os_status_solicitacao
    FOREIGN KEY (id_status_sol )
    REFERENCES status_solicitacao (ID_STATUS_SOL )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_historico_os_usuarios
    FOREIGN KEY (cd_usuario )
    REFERENCES usuarios (cd_usuario )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
;
alter table solicita_visto add 
	hios_id_atual INT(11) UNSIGNED NULL
;
alter table solicita_visto add
  CONSTRAINT fk_solicita_visto_historico_os_atual
    FOREIGN KEY (hios_id_atual )
    REFERENCES historico_os (hios_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
;