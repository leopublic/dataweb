<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
OK
Criar formulario para estrangeiros
- Adicionar login e senha no cadastro dos candidatos
- Inicializar login e senha a partir dos dados dos candidatos
- Adicionar login e senha nos formulários de identificação do candidato
- Alterar login para testar usuário e senha dos candidatos e nesse caso direcioná-lo para a nova tela
- Implementar geração automática do login/senha na inclusão de candidatos
- Corrigir bug com plic no cadastro de candidatos
- Montar nova tela para alimentação pelo Candidato
	- Verificar se atualização está habilitada



Pendente
- Alterar login para texto universal
- Montar nova tela para alimentação pelo Candidato
	- 3 abas: dados pessoais, arquivos, curriculum
	- Salvar no candidato
	- Enviar email para ponto focal
	- Alterar tela de empresa para indicar o ponto focal da empresa
- Alterar consulta de candidato para exibir se houve alteração pelo CANDIDATO
- Alterar visualização de detalhes do candidato para exibir dados alterados lado a lado com opção de aproveitar
- Implementae envio de e-mails
- Documentar funcionamento
- Implementar alterações solicitadas
- Apontar para tabela nova
- Criar campos texto
- Modelar CV
- Implementar CV
- Implementar carregamento de arquivos

