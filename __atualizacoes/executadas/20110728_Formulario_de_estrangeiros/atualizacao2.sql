drop table if exists tipo_ensino;
/*==============================================================*/
/* Table: tipo_ensino                                           */
/*==============================================================*/
create table tipo_ensino
(
   tpen_id              int not null auto_increment,
   tpen__no_nome        varchar(200),
   primary key (tpen_id)
);
drop table if exists curso;
/*==============================================================*/
/* Table: curso                                                 */
/*==============================================================*/
create table curso
(
   curs_id              int not null auto_increment,
   tpen_id              int,
   NU_CANDIDATO         int(11),
   curs_no_cidade       varchar(200),
   curs_no_periodo      varchar(200),
   curs_no_instituicao  varchar(200),
   curs_no_grau         varchar(200),
   primary key (curs_id)
);
drop table if exists experiencia_profissional;
/*==============================================================*/
/* Table: experiencia_profissional                              */
/*==============================================================*/
create table experiencia_profissional
(
   epro_id              int not null auto_increment,
   NU_CANDIDATO         int(11),
   epro_no_companhia    varchar(200),
   epro_no_funcao       varchar(200),
   epro_no_periodo      varchar(200),
   epro_tx_atribuicoes  text,
   primary key (epro_id)
);
alter table experiencia_profissional add constraint fk_experiencia_candidato foreign key (NU_CANDIDATO)
      references CANDIDATO (NU_CANDIDATO) on delete restrict on update restrict;
alter table curso add constraint fk_curso_candidato foreign key (NU_CANDIDATO)
      references CANDIDATO (NU_CANDIDATO) on delete restrict on update restrict;
alter table curso add constraint fk_curso_tipo_ensino foreign key (tpen_id)
      references tipo_ensino (tpen_id) on delete restrict on update restrict;
      
      drop table if exists sexo;

/*==============================================================*/
/* Table: sexo                                                  */
/*==============================================================*/
create table sexo
(
   CO_SEXO              char(1) not null,
   sexo_no_nome         varchar(100),
   sexo_no_nome_ingles  varchar(100),
   primary key (CO_SEXO)
);
insert into sexo(CO_SEXO, sexo_no_nome,sexo_no_nome_ingles) values ('M', 'Masculino', 'Masculine'), ('F', 'Feminino', 'Feminine');

alter table CANDIDATO add CO_FUNCAO int ;
alter table CANDIDATO add TE_DESCRICAO_ATIVIDADES text;
alter table CANDIDATO add VA_REMUNERACAO_MENSAL varchar(50);
alter table CANDIDATO add VA_REMUNERACAO_MENSAL_BRASIL varchar(50);
alter table CANDIDATO add NO_SEAMAN varchar(50);
alter table CANDIDATO add DT_VALIDADE_SEAMAN datetime;
alter table CANDIDATO add DT_EMISSAO_SEAMAN datetime;
alter table CANDIDATO add CO_PAIS_SEAMAN int;
alter table CANDIDATO add loca_id int;

drop table if exists local_projeto;

/*==============================================================*/
/* Table: local_projeto                                         */
/*==============================================================*/
create table local_projeto
(
   local_id             int not null auto_increment,
   loca_no_nome         varchar(200),
   primary key (local_id)
);
insert into local_projeto (loca_no_nome) values ('Maca�'), ('Vit�ria'), ('Rio de Janeiro');
drop table if exists candidato_tmp;

/*==============================================================*/
/* Table: candidato_tmp                                         */
/*==============================================================*/
create table candidato_tmp
(
   nu_candidato_tmp     int not null auto_increment,
   NU_CANDIDATO         int(11),
   NO_PRIMEIRO_NOME     varchar(200) default NULL,
   NO_NOME_MEIO         varchar(200) default NULL,
   NO_ULTIMO_NOME       varchar(200) default NULL,
   NOME_COMPLETO        varchar(600) default NULL,
   NO_EMAIL_CANDIDATO   varchar(200) default NULL,
   NO_ENDERECO_RESIDENCIA varchar(200) default NULL,
   NO_CIDADE_RESIDENCIA varchar(50) default NULL,
   CO_PAIS_RESIDENCIA   int(11) default NULL,
   NU_TELEFONE_CANDIDATO varchar(50) default NULL,
   NO_EMPRESA_ESTRANGEIRA varchar(80) default NULL,
   NO_ENDERECO_EMPRESA  varchar(50) default NULL,
   NO_CIDADE_EMPRESA    varchar(50) default NULL,
   CO_PAIS_EMPRESA      int(11) default NULL,
   NU_TELEFONE_EMPRESA  varchar(30) default NULL,
   NO_PAI               varchar(200) default NULL,
   NO_MAE               varchar(200) default NULL,
   CO_NACIONALIDADE     int(11) default NULL,
   CO_NIVEL_ESCOLARIDADE char(2) default NULL,
   CO_ESTADO_CIVIL      char(1) default NULL,
   CO_SEXO              char(1) default NULL,
   DT_NASCIMENTO        date default NULL,
   NO_LOCAL_NASCIMENTO  varchar(100) default NULL,
   NU_PASSAPORTE        varchar(20) default NULL,
   DT_EMISSAO_PASSAPORTE date default NULL,
   DT_VALIDADE_PASSAPORTE date default NULL,
   CO_PAIS_EMISSOR_PASSAPORTE int(11) default NULL,
   NU_RNE               varchar(255) default NULL,
   CO_PROFISSAO_CANDIDATO int(11) default NULL,
   TE_TRABALHO_ANTERIOR_BRASIL longtext,
   NU_CPF               varchar(14) default NULL,
   DT_CADASTRAMENTO     date not null,
   CO_USU_CADASTAMENTO  int(10) unsigned not null default 0,
   DT_ULT_ALTERACAO     timestamp not null default CURRENT_TIMESTAMP,
   CO_USU_ULT_ALTERACAO int(10) unsigned not null,
   NO_ENDERECO_ESTRANGEIRO varchar(200) default NULL,
   NO_CIDADE_ESTRANGEIRO varchar(50) default NULL,
   CO_PAIS_ESTRANGEIRO  int(11) default NULL,
   NU_TELEFONE_ESTRANGEIRO varchar(50) default NULL,
   BO_CADASTRO_MINIMO_OK tinyint(4) not null default 0,
   NU_EMPRESA           int,
   NU_EMBARCACAO_PROJETO int(11) default NULL,
   NU_CTPS              varchar(50),
   NU_CNH               varchar(50),
   DT_EXPIRACAO_CNH     datetime,
   DT_EXPIRACAO_CTPS    datetime,
   FL_CADASTRO_REVISADO tinyint,
   CO_LOCAL_EMBARCACAO_PROJETO varchar(200),
   TE_DESCRICAO_ATIVIDADES text,
   CO_FUNCAO            int,
   VA_REMUNERACAO_MENSAL varchar(50),
   VA_REMUNERACAO_MENSAL_BRASIL varchar(50),
   NO_SEAMAN            varchar(50),
   DT_EMISSAO_SEAMAN    datetime,
   DT_VALIDADE_SEAMAN   datetime,
   CO_PAIS_SEAMAN       int,
   loca_id int,
   primary key (nu_candidato_tmp),
   key NO_PRIMEIRO_NOME (NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME),
   key NOME_COMPLETO (NOME_COMPLETO),
   key IX_CANDIDATO_PROFISSAO (CO_PROFISSAO_CANDIDATO),
   key IX_CANDIDATO_ESTADO_CIVIL (CO_ESTADO_CIVIL),
   key IX_CANDIDATO_ESCOLARIDADE (CO_NIVEL_ESCOLARIDADE),
   key IX_CANDIDATO_NACIONALIDADE (CO_NACIONALIDADE)
);

alter table candidato_tmp add constraint FK_candidato_tmp_CANDIDATO foreign key (NU_CANDIDATO)
      references CANDIDATO (NU_CANDIDATO) on delete restrict on update restrict;
