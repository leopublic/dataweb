CREATE TABLE `candidato_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `NU_CANDIDATO` int(11) DEFAULT NULL,
  `NO_PRIMEIRO_NOME` varchar(200) DEFAULT NULL,
  `NO_NOME_MEIO` varchar(200) DEFAULT NULL,
  `NO_ULTIMO_NOME` varchar(200) DEFAULT NULL,
  `NOME_COMPLETO` varchar(600) DEFAULT NULL,
  `NO_EMAIL_CANDIDATO` varchar(200) DEFAULT NULL,
  `NO_ENDERECO_RESIDENCIA` varchar(200) DEFAULT NULL,
  `NO_CIDADE_RESIDENCIA` varchar(200) DEFAULT NULL,
  `CO_PAIS_RESIDENCIA` int(11) DEFAULT NULL,
  `NU_TELEFONE_CANDIDATO` varchar(50) DEFAULT NULL,
  `NO_EMPRESA_ESTRANGEIRA` varchar(200) DEFAULT NULL,
  `NO_ENDERECO_EMPRESA` varchar(200) DEFAULT NULL,
  `NO_CIDADE_EMPRESA` varchar(200) DEFAULT NULL,
  `CO_PAIS_EMPRESA` int(11) DEFAULT NULL,
  `NU_TELEFONE_EMPRESA` varchar(100) DEFAULT NULL,
  `NO_PAI` varchar(200) DEFAULT NULL,
  `NO_MAE` varchar(200) DEFAULT NULL,
  `CO_NACIONALIDADE` int(11) DEFAULT NULL,
  `CO_NIVEL_ESCOLARIDADE` char(2) DEFAULT NULL,
  `CO_ESTADO_CIVIL` char(1) DEFAULT NULL,
  `CO_SEXO` char(1) DEFAULT NULL,
  `DT_NASCIMENTO` date DEFAULT NULL,
  `NO_LOCAL_NASCIMENTO` varchar(200) DEFAULT NULL,
  `NU_PASSAPORTE` varchar(20) DEFAULT NULL,
  `DT_EMISSAO_PASSAPORTE` date DEFAULT NULL,
  `DT_VALIDADE_PASSAPORTE` date DEFAULT NULL,
  `CO_PAIS_EMISSOR_PASSAPORTE` int(11) DEFAULT NULL,
  `NU_RNE` varchar(255) DEFAULT NULL,
  `CO_PROFISSAO_CANDIDATO` int(11) DEFAULT NULL,
  `TE_TRABALHO_ANTERIOR_BRASIL` longtext,
  `NU_CPF` varchar(14) DEFAULT NULL,
  `DT_CADASTRAMENTO` date DEFAULT NULL,
  `CO_USU_CADASTAMENTO` int(10) unsigned DEFAULT NULL,
  `DT_ULT_ALTERACAO` timestamp NULL DEFAULT NULL,
  `CO_USU_ULT_ALTERACAO` int(10) unsigned DEFAULT NULL,
  `NO_ENDERECO_ESTRANGEIRO` varchar(200) DEFAULT NULL,
  `NO_CIDADE_ESTRANGEIRO` varchar(200) DEFAULT NULL,
  `CO_PAIS_ESTRANGEIRO` int(11) DEFAULT NULL,
  `NU_TELEFONE_ESTRANGEIRO` varchar(100) DEFAULT NULL,
  `BO_CADASTRO_MINIMO_OK` tinyint(4) DEFAULT NULL,
  `NU_EMPRESA` int(11) DEFAULT NULL,
  `NU_EMBARCACAO_PROJETO` int(11) DEFAULT NULL,
  `codigo_processo_mte_atual` int(11) DEFAULT NULL,
  `NU_CTPS` varchar(50) DEFAULT NULL,
  `NU_CNH` varchar(50) DEFAULT NULL,
  `DT_EXPIRACAO_CNH` datetime DEFAULT NULL,
  `DT_EXPIRACAO_CTPS` datetime DEFAULT NULL,
  `FL_INATIVO` tinyint(4) DEFAULT NULL,
  `NO_LOGIN` varchar(20) DEFAULT NULL,
  `NO_SENHA` varchar(10) DEFAULT NULL,
  `FL_ATUALIZACAO_HABILITADA` tinyint(4) DEFAULT NULL,
  `FL_HABILITAR_CV` tinyint(4) DEFAULT NULL,
  `CO_FUNCAO` int(11) DEFAULT NULL,
  `TE_DESCRICAO_ATIVIDADES` text,
  `VA_REMUNERACAO_MENSAL` varchar(50) DEFAULT NULL,
  `VA_REMUNERACAO_MENSAL_BRASIL` varchar(50) DEFAULT NULL,
  `NO_SEAMAN` varchar(50) DEFAULT NULL,
  `DT_VALIDADE_SEAMAN` datetime DEFAULT NULL,
  `DT_EMISSAO_SEAMAN` datetime DEFAULT NULL,
  `CO_PAIS_SEAMAN` int(11) DEFAULT NULL,
  `loca_id` int(11) DEFAULT NULL,
  `FL_EMBARCADO` tinyint(4) DEFAULT NULL,
  `TE_OBSERVACAO_CREWLIST` varchar(500) DEFAULT NULL,
  `NU_ORDEM_CREWLIST` int(11) DEFAULT NULL,
  `cd_usuario_inativacao` int(11) DEFAULT NULL,
  `cd_usuario_sol_exclusao` int(11) DEFAULT NULL,
  `cd_usuario_revisao` int(11) DEFAULT NULL,
  `cd_usuario_conferencia` int(11) DEFAULT NULL,
  `cand_dt_inativacao` datetime DEFAULT NULL,
  `cand_dt_sol_exclusao` datetime DEFAULT NULL,
  `cand_dt_revisao` datetime DEFAULT NULL,
  `cand_dt_conferencia` datetime DEFAULT NULL,
  `cd_usuario_ativacao` int(11) DEFAULT NULL,
  `cand_dt_ativacao` datetime DEFAULT NULL,
  `cand_fl_excluir` tinyint(4) DEFAULT NULL,
  `cand_fl_revisado` tinyint(4) DEFAULT NULL,
  `cand_fl_conferido` tinyint(4) DEFAULT NULL,
  `log_tx_evento` varchar(20) DEFAULT NULL,
  `cd_usuario_log` int(11) DEFAULT NULL,
  `log_tx_controller` varchar(200) DEFAULT NULL,
  `log_tx_metodo` varchar(200) DEFAULT NULL,
  `log_dt_evento` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `NO_PRIMEIRO_NOME` (`NO_PRIMEIRO_NOME`,`NO_NOME_MEIO`,`NO_ULTIMO_NOME`),
  KEY `NOME_COMPLETO` (`NOME_COMPLETO`),
  KEY `IX_CANDIDATO_PROFISSAO` (`CO_PROFISSAO_CANDIDATO`),
  KEY `IX_CANDIDATO_ESTADO_CIVIL` (`CO_ESTADO_CIVIL`),
  KEY `IX_CANDIDATO_ESCOLARIDADE` (`CO_NIVEL_ESCOLARIDADE`),
  KEY `IX_CANDIDATO_NACIONALIDADE` (`CO_NACIONALIDADE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
