CREATE TABLE historico_processo_emiscie (
  hemi_id int(10) unsigned NOT NULL AUTO_INCREMENT,
  hemi_dt_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  codigo int(11) DEFAULT NULL,
  teve_id int(11) DEFAULT NULL,
  cd_usuario int(10) unsigned DEFAULT NULL,
  hemi_tx_observacao text,
  PRIMARY KEY (hemi_id)
) ENGINE=InnoDB