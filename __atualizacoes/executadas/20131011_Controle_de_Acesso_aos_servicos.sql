-- Cria controle de acesso para a tabela de serviços
insert into acesso(const_acesso, tipo_acesso, nome_acesso) values 
('tp_Sistema_Tabelas_Servico', '001.040', 'Sistema - Tabelas Auxiliares - Serviço: Consultar lista')
,('tp_Sistema_Tabelas_Servico_Cadastrar', '001.041', 'Sistema - Tabelas Auxiliares - Serviço: Criar novo')
,('tp_Sistema_Tabelas_Servico_Editar', '001.042', 'Sistema - Tabelas Auxiliares - Serviço: Alterar')
,('tp_Sistema_Tabelas_Servico_Excluir', '001.043', 'Sistema - Tabelas Auxiliares - Serviço: Excluir')
;

insert into acesso_perfil(cd_perfil, cd_acesso) values (1, 35), (1,36), (1,37), (1, 34)
;
