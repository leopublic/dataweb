drop view if exists vProcesso;
create view vProcesso as
select  'mte' tipo, cd_candidato, codigo, codigo 'codigo_processo_mte', null 'codigo_processo_prorrog', id_solicita_visto, fl_vazio, 1 'ordem'
from processo_mte mte
union
select 'coleta' tipo, cd_candidato, codigo, codigo_processo_mte, null 'codigo_processo_prorrog', id_solicita_visto, 0 'fl_vazio', 2 'ordem'
from   processo_coleta col
union
select 'regcie' tipo, cd_candidato, codigo, codigo_processo_mte, codigo_processo_prorrog, id_solicita_visto, fl_vazio
, case nu_servico when 58 then 6 else 3 end 'ordem'
from  processo_regcie reg
union
select 'emiscie' tipo, cd_candidato, codigo, codigo_processo_mte, null 'codigo_processo_prorrog', id_solicita_visto, fl_vazio, 4 'ordem'
from   processo_emiscie emi
union
select 'prorrog' tipo, cd_candidato, codigo, codigo_processo_mte, null 'codigo_processo_prorrog', id_solicita_visto, fl_vazio, 5 'ordem'
from processo_prorrog pro
union
select 'cancel' tipo, cd_candidato, codigo, codigo_processo_mte, null 'codigo_processo_prorrog', id_solicita_visto, fl_vazio, 7 'ordem'
from   processo_cancel can
;
