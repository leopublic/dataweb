 drop table if exists processo_coleta;
/*==============================================================*/
/* Table: processo_coleta                                       */
/*==============================================================*/
create table processo_coleta
(
   codigo               int not null auto_increment,
   id_solicita_visto    int(10),
   cd_candidato         int,
   no_validade          varchar(100),
   nu_visto             varchar(100),
   dt_emissao_visto     datetime,
   no_local_emissao     varchar(200),
   co_pais_nacionalidade_visto int,
   co_classificacao     varchar(1),
   no_local_entrada     varchar(200),
   co_uf                int,
   dt_entrada           datetime,
   nu_transporte_entrada int,
   codigo_processo_mte  int,
   dt_ult               datetime,
   dt_cad               datetime,
   cd_usuario_cad       int,
   cd_usuario_ult       int,
   primary key (codigo)
);

alter table processo_coleta add constraint fk_processo_coleta_candidato foreign key (cd_candidato)
      references candidato (NU_CANDIDATO) on delete restrict on update restrict;

alter table processo_coleta add constraint fk_processo_coleta_solicita_visto foreign key (id_solicita_visto)
      references solicita_visto (id_solicita_visto) on delete restrict on update restrict;

delete from processo_coleta;
ALTER TABLE AUTORIZACAO_CANDIDATO
  ADD INDEX IX_ID_SOLICITA_VISTO (id_solicita_visto);
ALTER TABLE AUTORIZACAO_CANDIDATO
  ADD INDEX IX_DATA_VALIDADE_VISTO (DT_VALIDADE_VISTO);  
ALTER TABLE processo_mte
ADD INDEX IX_ID_SOLICITA_VISTO (id_solicita_visto);
  
insert into processo_coleta (id_solicita_visto, cd_candidato, no_validade, nu_visto, dt_emissao_visto, no_local_emissao, co_pais_nacionalidade_visto, no_local_entrada, co_uf, dt_entrada, nu_transporte_entrada, codigo_processo_mte)
select id_solicita_visto, NU_CANDIDATO, DT_VALIDADE_VISTO, NU_VISTO, DT_EMISSAO_VISTO, NO_LOCAL_EMISSAO_VISTO, CO_NACIONALIDADE_VISTO,  NO_LOCAL_ENTRADA, null, DT_ENTRADA, NU_TRANSPORTE_ENTRADA, (select max(codigo) from processo_mte where id_solicita_visto = AUTORIZACAO_CANDIDATO.id_solicita_visto)
from AUTORIZACAO_CANDIDATO where DT_VALIDADE_VISTO is not null
;

update SERVICO set ID_TIPO_ACOMPANHAMENTO = 8 where NO_SERVICO_RESUMIDO like 'Coleta%';

alter table processo_cancel add fl_vazio tinyint default 0;
alter table processo_emiscie add fl_vazio tinyint default 0;
alter table processo_regcie add fl_vazio tinyint default 0;
alter table processo_mte add fl_vazio tinyint default 0;
alter table processo_prorrog add fl_vazio tinyint default 0;

update processo_prorrog set fl_vazio = 1
where  nu_protocolo = '' and
( dt_requerimento is null or date_format(dt_requerimento, '%y-%m-%d') = '00-00-00')
and (dt_validade is null or date_format(dt_validade, '%y-%m-%d') = '00-00-00')
and (dt_prazo_pret is null or date_format(dt_prazo_pret, '%y-%m-%d') = '00-00-00')
;

update processo_emiscie set fl_vazio = 1
where  nu_cie = '' and
( dt_emissao is null or date_format(dt_emissao, '%y-%m-%d') = '00-00-00')
and ( dt_validade is null or date_format(dt_validade, '%y-%m-%d') = '00-00-00')
;

update processo_regcie set fl_vazio = 1
where  nu_protocolo = '' and
    (dt_requerimento is null or date_format(dt_requerimento, '%y-%m-%d') = '00-00-00')
and (dt_validade is null or date_format(dt_validade, '%y-%m-%d') = '00-00-00')
and dt_prazo_estada is null
;

update processo_cancel set fl_vazio = 1
where  (nu_processo = '' or nu_processo is null)
and (dt_processo is null or date_format(dt_processo, '%y-%m-%d') = '00-00-00')
and (dt_cancel is null or date_format(dt_cancel, '%y-%m-%d') = '00-00-00')

;
update processo_mte set fl_vazio = 1 where 
 dt_requerimento is null 
and nu_processo is null
and dt_deferimento is null
and prazo_solicitado is null
;

alter table processo_mte add nu_servico int;
update processo_mte, solicita_visto set processo_mte.nu_servico = solicita_visto.NU_SERVICO
where processo_mte.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_mte.id_solicita_visto is not null
and processo_mte.id_solicita_visto <> 0
;
update processo_mte, solicita_visto, AUTORIZACAO_CANDIDATO
set processo_mte.nu_servico = 1
where processo_mte.id_solicita_visto = solicita_visto.id_solicita_visto
and solicita_visto.nu_servico is null
and AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
and AUTORIZACAO_CANDIDATO.CO_TIPO_AUTORIZACAO = 'RM'
;
update processo_mte, solicita_visto, AUTORIZACAO_CANDIDATO
set processo_mte.nu_servico = 2
where processo_mte.id_solicita_visto = solicita_visto.id_solicita_visto
and solicita_visto.nu_servico is null
and AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
and AUTORIZACAO_CANDIDATO.CO_TIPO_AUTORIZACAO = 'RN'
;
update processo_mte, solicita_visto, AUTORIZACAO_CANDIDATO
set processo_mte.nu_servico = 3
where processo_mte.id_solicita_visto = solicita_visto.id_solicita_visto
and solicita_visto.nu_servico is null
and AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
and AUTORIZACAO_CANDIDATO.CO_TIPO_AUTORIZACAO = 'RT'
;
update processo_mte, solicita_visto, AUTORIZACAO_CANDIDATO
set processo_mte.nu_servico = 5
where processo_mte.id_solicita_visto = solicita_visto.id_solicita_visto
and solicita_visto.nu_servico is null
and AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
and AUTORIZACAO_CANDIDATO.CO_TIPO_AUTORIZACAO = 'RC'
;

update processo_mte, solicita_visto, AUTORIZACAO_CANDIDATO
set processo_mte.NU_SERVICO = 8
where processo_mte.id_solicita_visto = solicita_visto.id_solicita_visto
and solicita_visto.nu_servico is null
and AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
and AUTORIZACAO_CANDIDATO.CO_TIPO_AUTORIZACAO = '02'
;
alter table solicita_visto add NU_EMBARCACAO_PROJETO int null;
update solicita_visto, AUTORIZACAO_CANDIDATO 
  set solicita_visto.NU_EMBARCACAO_PROJETO = AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO
  where AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
  and solicita_visto.nu_solicitacao > 0
;
  
alter table processo_mte add fl_visto_atual tinyint default 0;
alter table processo_mte add fl_ignorar_vencimento tinyint default 0;
alter table processo_mte add NU_EMPRESA int null;
alter table processo_mte add NU_EMBARCACAO_PROJETO int null;


alter table processo_mte add constraint fk_processo_mte_empresa foreign key (NU_EMPRESA)
      references EMPRESA (NU_EMPRESA) on delete restrict on update restrict;
alter table processo_mte add constraint fk_processo_mte_servico foreign key (nu_servico)
      references SERVICO (NU_SERVICO) on delete restrict on update restrict;
alter table processo_mte add constraint fk_processo_mte_embarcacao_projeto foreign key (NU_EMPRESA, NU_EMBARCACAO_PROJETO)
      references EMBARCACAO_PROJETO (NU_EMPRESA, NU_EMBARCACAO_PROJETO) on delete restrict on update restrict;

-- Atualiza a solicita��o dos processos zerados
update processo_mte set id_solicita_visto = 5834 where cd_solicitacao = 0;
update processo_prorrog set id_solicita_visto = 5834 where cd_solicitacao = 0;
update processo_regcie set id_solicita_visto = 5834 where cd_solicitacao = 0;
update processo_emiscie set id_solicita_visto = 5834 where cd_solicitacao = 0;
update processo_cancel set id_solicita_visto = 5834 where cd_solicitacao = 0;

update processo_mte, solicita_visto 
  set processo_mte.nu_servico = solicita_visto.NU_SERVICO
  , processo_mte.NU_EMPRESA = solicita_visto.NU_EMPRESA
  , processo_mte.NU_EMBARCACAO_PROJETO = solicita_visto.NU_EMBARCACAO_PROJETO
where processo_mte.id_solicita_visto = solicita_visto.id_solicita_visto
and solicita_visto.nu_solicitacao > 0;
;
update processo_mte, AUTORIZACAO_CANDIDATO
  set processo_mte.nu_servico = (select case trim(CO_TIPO_AUTORIZACAO)
              WHEN '02' THEN 8
              WHEN '37' THEN 59
              WHEN '87' THEN 62
              WHEN 'P2' THEN 57
              WHEN 'P4' THEN 13
              WHEN 'PF' THEN 9
              WHEN 'PJ' THEN 56
              WHEN 'RC' THEN 5
              WHEN 'RM' THEN 1
              WHEN 'RT' THEN 3
              WHEN 'TR' THEN 12
              END)
  , processo_mte.NU_EMPRESA = AUTORIZACAO_CANDIDATO.NU_EMPRESA
  , processo_mte.NU_EMBARCACAO_PROJETO = AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO
where processo_mte.id_solicita_visto = AUTORIZACAO_CANDIDATO.id_solicita_visto
and processo_mte.cd_candidato = AUTORIZACAO_CANDIDATO.NU_CANDIDATO
and AUTORIZACAO_CANDIDATO.NU_SOLICITACAO = 0;
;

alter table CANDIDATO add codigo_processo_mte_atual int null;

drop table if exists acao;

/*==============================================================*/
/* Table: acao                                                  */
/*==============================================================*/
create table acao
(
   id_acao              int not null,
   id_edicao            int(11),
   no_acao              varchar(200),
   no_classe            varchar(200),
   no_funcao            varchar(200),
   no_title             varchar(500),
primary key (id_acao)
);

alter table acao add constraint fk_acao_edicao foreign key (id_edicao)
      references edicao (id_edicao) on delete restrict on update restrict;


alter table processo_mte add codigo_ultimo_processo int null;
alter table processo_mte add id_tipo_acompanhamento_ultimo_processo int null;

alter table processo_prorrog add fl_processo_atual tinyint default 0;
alter table processo_regcie add fl_processo_atual tinyint default 0;
alter table processo_emiscie add fl_processo_atual tinyint default 0;
alter table processo_cancel add fl_processo_atual tinyint default 0;
alter table processo_coleta add fl_processo_atual tinyint default 0;

alter table processo_prorrog add codigo_regcie int(11) null;

alter table CANDIDATO add NU_CTPS varchar(50) null;
alter table CANDIDATO add NU_CNH varchar(50) null;
alter table CANDIDATO add DT_EXPIRACAO_CNH datetime null;
alter table CANDIDATO add DT_EXPIRACAO_CTPS datetime null;


insert into TIPO_ACOMPANHAMENTO (NO_TIPO_ACOMPANHAMENTO, CO_TIPO_ACOMPANHAMENTO, FL_MULTI_CANDIDATOS)
values ('Obten��o de CPF', 'CPF', 0);
insert into TIPO_ACOMPANHAMENTO (NO_TIPO_ACOMPANHAMENTO, CO_TIPO_ACOMPANHAMENTO, FL_MULTI_CANDIDATOS)
values ('Obten��o de CNH', 'CNH', 0);
insert into TIPO_ACOMPANHAMENTO (NO_TIPO_ACOMPANHAMENTO, CO_TIPO_ACOMPANHAMENTO, FL_MULTI_CANDIDATOS)
values ('Obten��o de CTPS', 'CTPS', 0);

alter table processo_mte add prazo_solicitado_qtd smallint;
alter table processo_mte add prazo_solicitado_dt datetime;
alter table processo_mte add prazo_solicitado_periodo char(1);

alter table processo_regcie modify column cd_solicitacao int(11) null;
alter table processo_emiscie modify column cd_solicitacao int(11) null;
alter table processo_cancel modify column cd_solicitacao int(11) null;
alter table processo_prorrog modify column cd_solicitacao int(11) null;

CREATE TABLE estado (
  nu_estado int(11) NOT NULL DEFAULT '0',
  no_sigla char(2) DEFAULT NULL,
  no_nome varchar(20) DEFAULT NULL,
  PRIMARY KEY (nu_estado)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO estado (nu_estado, no_sigla, no_nome) VALUES 
 (0,'--','(n�o definido)'),
 (1,'AM','Amaz�nia'),
 (2,'AP','Amap�'),
 (3,'BA','Bahia'),
 (4,'CE','Cear�'),
 (5,'MG','Minas Gerais'),
 (6,'RJ','Rio de Janeiro'),
 (7,'RS','Rio Grande do Sul'),
 (8,'PR','Paran�'),
 (9,'SC','Santa Catarina'),
 (10,'SP','S�o Paulo'),
 (11,'ES','Esp�rito Santo'),
 (12,'AL','Alagoas'),
 (13,'SE','Sergipe'),
 (14,'PE','Pernambuco'),
 (15,'PB','Para�ba'),
 (16,'RN','Rio Grande do Norte'),
 (17,'PI','Piau�'),
 (18,'MA','Maranh�o'),
 (19,'AC','Acre'),
 (20,'PA','Par�'),
 (21,'RR','Roraima'),
 (22,'RD','Rond�nia'),
 (23,'GO','Goi�s'),
 (24,'MT','Mato Grosso'),
 (25,'MS','Mato Grosso do Sul'),
 (26,'TO','Tocantins');
 
ALTER TABLE processo_coleta
ADD INDEX IX_CODIGO_PROCESSO_MTE (codigo_processo_mte);
ALTER TABLE processo_prorrog
ADD INDEX IX_CODIGO_PROCESSO_MTE (codigo_processo_mte);
ALTER TABLE processo_regcie
ADD INDEX IX_CODIGO_PROCESSO_MTE (codigo_processo_mte);
ALTER TABLE processo_emiscie
ADD INDEX IX_CODIGO_PROCESSO_MTE (codigo_processo_mte);

alter table processo_prorrog add dt_publicacao_dou datetime null;
alter table processo_prorrog add ID_STATUS_CONCLUSAO int null;

alter table processo_prorrog add constraint fk_processo_prorrog_status_conclusao foreign key (ID_STATUS_CONCLUSAO)
      references STATUS_CONCLUSAO (ID_STATUS_CONCLUSAO) on delete restrict on update restrict;
      
insert into ACESSO (TIPO_ACESSO, NOME_ACESSO, CONST_ACESSO) values ('004.007', 'Cadastro - Candidatos - Incluir', 'tp_Cadastro_Candidato_Incluir');
insert into ACESSO (TIPO_ACESSO, NOME_ACESSO, CONST_ACESSO) values ('004.008', 'Cadastro - Candidatos - Alterar processos', 'tp_Cadastro_Candidato_Alterar_Processos');

alter table processo_regcie add codigo_processo_prorrog int (11) null;
alter table processo_prorrog add nu_servico int(11) null;
alter table processo_regcie add nu_servico int(11) null;
alter table processo_emiscie add nu_servico int(11) null;
alter table processo_coleta add nu_servico int(11) null;
alter table processo_cancel add nu_servico int(11) null;
update processo_prorrog, solicita_visto 
set processo_prorrog.nu_servico = solicita_visto.nu_servico
where processo_prorrog.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_prorrog.id_solicita_visto <> 0 
and processo_prorrog.id_solicita_visto is not null
;
update processo_cancel, solicita_visto 
set processo_cancel.nu_servico = solicita_visto.nu_servico
where processo_cancel.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_cancel.id_solicita_visto <> 0 
and processo_cancel.id_solicita_visto is not null
;
update processo_coleta, solicita_visto 
set processo_coleta.nu_servico = solicita_visto.nu_servico
where processo_coleta.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_coleta.id_solicita_visto <> 0 
and processo_coleta.id_solicita_visto is not null
;
update processo_regcie, solicita_visto 
set processo_regcie.nu_servico = solicita_visto.nu_servico
where processo_regcie.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_regcie.id_solicita_visto <> 0 
and processo_regcie.id_solicita_visto is not null
;
update processo_emiscie, solicita_visto 
set processo_emiscie.nu_servico = solicita_visto.nu_servico
where processo_emiscie.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_emiscie.id_solicita_visto <> 0 
and processo_emiscie.id_solicita_visto is not null
;

alter table processo_mte modify column cd_solicitacao int(11) null;

alter table processo_mte add observacao_visto text null;
update processo_mte, AUTORIZACAO_CANDIDATO 
set observacao_visto = AUTORIZACAO_CANDIDATO.TE_OBSERVACOES_PROCESSOS
where processo_mte.id_solicita_visto = AUTORIZACAO_CANDIDATO.id_solicita_visto
and processo_mte.cd_candidato = AUTORIZACAO_CANDIDATO.NU_CANDIDATO
;

alter table processo_mte add cd_usuario int(11) null;
alter table processo_prorrog add cd_usuario int(11) null;
alter table processo_emiscie add cd_usuario int(11) null;
alter table processo_regcie add cd_usuario int(11) null;
alter table processo_coleta add cd_usuario int(11) null;