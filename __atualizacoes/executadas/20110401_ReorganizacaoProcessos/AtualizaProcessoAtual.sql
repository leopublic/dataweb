DROP PROCEDURE if exists CarregaVistoAtual;
delimiter //
CREATE PROCEDURE CarregaVistoAtual()
BEGIN
    DECLARE fim INT DEFAULT 0;
    DECLARE xNU_CANDIDATO, xcodigo int;
    DECLARE processo_ok CURSOR FOR 
        SELECT NU_CANDIDATO, max(codigo) codigo 
        from CANDIDATO C 
        left join processo_mte MTE on MTE.cd_candidato = C.NU_CANDIDATO and fl_vazio = 0 
        group by NU_CANDIDATO;

    DECLARE processo_vazio CURSOR FOR 
        SELECT NU_CANDIDATO, max(codigo) codigo 
        from CANDIDATO C 
        left join processo_mte MTE on MTE.cd_candidato = C.NU_CANDIDATO and fl_vazio = 1 
        where codigo_processo_mte_atual = 0
        group by NU_CANDIDATO;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET fim = 1;

    update processo_mte set fl_visto_atual = 0;
    update CANDIDATO set codigo_processo_mte_atual = 0;
    
    OPEN processo_ok;
      
    read_loop: LOOP
        FETCH processo_ok INTO xNU_CANDIDATO, xcodigo;
        IF fim THEN
            LEAVE read_loop;
        END IF;
        UPDATE processo_mte set fl_visto_atual = 1 where codigo = xcodigo;
        UPDATE CANDIDATO set codigo_processo_mte_atual = xcodigo where NU_CANDIDATO = xNU_CANDIDATO;
    END LOOP;

    CLOSE processo_ok;
    
    OPEN processo_vazio;
      
    read_loop: LOOP
        FETCH processo_vazio INTO xNU_CANDIDATO, xcodigo;
        IF fim THEN
            LEAVE read_loop;
        END IF;
        UPDATE processo_mte set fl_visto_atual = 1, fl_vazio = 0 where codigo = xcodigo;
        UPDATE CANDIDATO set codigo_processo_mte_atual = xcodigo where NU_CANDIDATO = xNU_CANDIDATO;
    END LOOP;

    CLOSE processo_vazio;
    
    
END;    
//
delimiter ;

call CarregaVistoAtual();
