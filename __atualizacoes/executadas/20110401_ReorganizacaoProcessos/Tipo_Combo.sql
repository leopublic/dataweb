drop table if exists Tipo_Combo;

/*==============================================================*/
/* Table: Tipo_Combo                                            */
/*==============================================================*/
create table Tipo_Combo
(
   id_tipo_combo        int not null auto_increment,
   no_nome              varchar(200),
   no_tabela       varchar(200),
   no_campo_chave       varchar(200),
   no_campo_descricao   varchar(200),
   no_extra             varchar(200),
   no_campo_ordem       varchar(200),
   primary key (id_tipo_combo)
);
insert into Tipo_Combo (no_tabela, no_campo_chave, no_campo_descricao, no_extra, no_campo_ordem) 
values  ("TIPO_AUTORIZACAO","CO_TIPO_AUTORIZACAO","NO_REDUZIDO_TIPO_AUTORIZACAO", "", "")
		, ("ESTADO_CIVIL","CO_ESTADO_CIVIL","NO_ESTADO_CIVIL", "", "")
		, ("TIPO_SERVICO TS JOIN SERVICO S ON S.NU_TIPO_SERVICO = TS.NU_TIPO_SERVICO","NU_SERVICO","concat(CO_TIPO_SERVICO , concat('.', concat(lpad(CO_SERVICO,2,'0' ), concat(' ', NO_SERVICO_RESUMIDO))))", "", "ORDER BY NO_SERVICO_RESUMIDO")
		, ("STATUS_SOLICITACAO","ID_STATUS_SOL","NO_STATUS_SOL", "", "")
		, ("PAIS_NACIONALIDADE","CO_PAIS","CONCAT(NO_NACIONALIDADE,' (',NO_PAIS,')')", "where CO_PAIS_PF <> ''", " ORDER BY NO_NACIONALIDADE ")
		, ("PAIS_NACIONALIDADE","CO_PAIS","NO_PAIS", "", "")
		, ("ESCOLARIDADE","CO_ESCOLARIDADE","NO_ESCOLARIDADE", "", "")
		, ("REPARTICAO_CONSULAR","CO_REPARTICAO_CONSULAR","NO_REPARTICAO_CONSULAR", "WHERE CO_REPARTICAO_CONSULAR>999", "")
		, ("PROFISSAO","CO_PROFISSAO","NO_PROFISSAO", "", "")
		, ("EMPRESA","NU_EMPRESA","NO_RAZAO_SOCIAL", "", "NO_RAZAO_SOCIAL")
		, ("EMBARCACAO_PROJETO","NU_EMBARCACAO_PROJETO","NO_EMBARCACAO_PROJETO", "", "")
		, ("FUNCAO_CARGO","CO_FUNCAO","NO_FUNCAO", "", "")		
		, ("CLASSIFICACAO_VISTO","CO_CLASSIFICACAO_VISTO","NO_CLASSIFICACAO_VISTO", "", "")		
		, ("TRANSPORTE_ENTRADA","NU_TRANSPORTE_ENTRADA","NO_TRANSPORTE_ENTRADA", "", "")		
		, ("TIPO_SERVICO","NU_TIPO_SERVICO","NO_TIPO_SERVICO", "", "")		
		, ("--","--","--", "", "")		
		, ("TIPO_ARQUIVO","ID_TIPO_ARQUIVO","NO_TIPO_ARQUIVO", "", "")		
		, ("estados","id_estado","nm_estado", "", "")		
		, ("usuarios","cd_usuario","nome", " WHERE cd_perfil < 10 ", "")
		, ("STATUS_ANDAMENTO","ID_STATUS_ANDAMENTO","NO_STATUS_ANDAMENTO", "", "")
		, ("STATUS_CONCLUSAO","ID_STATUS_CONCLUSAO","NO_STATUS_CONCLUSAO", "", "")
		, ("EMPRESA","NU_EMPRESA","NO_RAZAO_SOCIAL", "WHERE FL_ATIVA = 1", "")
		, ("Tipo_Combo","id_tipo_combo","no_nome", "", "")
		, ("Tipo_Campo","id_tipo_campo","no_tipo_campo", "", "")
;
update Tipo_Combo set no_nome = no_tabela;