drop table if exists acao;

/*==============================================================*/
/* Table: acao                                                  */
/*==============================================================*/
create table acao
(
   id_acao              int not null auto_increment,
   id_edicao            int(11),
   no_acao              varchar(200),
   no_classe            varchar(200),
   no_funcao            varchar(200),
   no_label             varchar(200),
   primary key (id_acao)
);

alter table acao add constraint fk_acao_edicao foreign key (id_edicao)
      references edicao (id_edicao) on delete restrict on update restrict;

insert into Edicao (no_edicao, no_view, no_tabela) values ('Campo_edit', 'Campo', 'Campo');
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 12, 'id_campo', 'id_campo', '', 0, 1, 1, 0, 0, 10,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 3, 'id_tipo_campo', 'id_tipo_campo', '', 0, 0, 1, 0, 0, 20,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'no_label', 'no_label', '', 0, 0, 1, 0, 0, 30,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'no_campo_bd', 'no_campo_bd', '', 0, 0, 1, 0, 0, 40,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'no_classe', 'no_classe', '', 0, 0, 1, 0, 0, 50,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'bo_obrigatorio', 'bo_obrigatorio', '', 0, 0, 1, 0, 0, 60,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'bo_chave', 'bo_chave', '', 0, 0, 1, 0, 0, 70,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'id_tipo_combo', 'id_tipo_combo', '', 0, 0, 1, 0, 0, 80,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'nu_ordem', 'nu_ordem', '', 0, 0, 1, 0, 0, 90,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'id_edicao', 'id_edicao', '', 0, 0, 1, 0, 0, 100,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'no_painel', 'no_painel', '', 0, 0, 1, 0, 0, 110,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'nu_ordem_painel', 'nu_ordem_painel', '', 0, 0, 1, 0, 0, 120,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'bo_readonly', 'bo_readonly', '', 0, 0, 1, 0, 0, 130,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'bo_exibir', 'bo_exibir', '', 0, 0, 1, 0, 0, 140,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'fl_link_mte', 'fl_link_mte', '', 0, 0, 1, 0, 0, 150,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'fl_link_mj', 'fl_link_mj', '', 0, 0, 1, 0, 0, 160,27);
 insert into Campo(id_tipo_campo, no_label, no_campo_bd, no_classe, bo_obrigatorio, bo_chave, bo_exibir, bo_readonly,id_tipo_combo, nu_ordem, id_edicao) values ( 6, 'fl_largura_dupla', 'fl_largura_dupla', '', 0, 0, 1, 0, 0, 170,27);
 insert into acao (id_edicao, no_acao, no_classe, no_funcao, no_label) values (27,'Salvar', 'cCAMPO', 'Salve', 'Salvar');
 update Campo set id_tipo_campo = 3, id_tipo_combo = 24 where id_edicao  = 27 and no_campo_bd = 'id_tipo_campo';
  update Campo set id_tipo_campo = 3, id_tipo_combo = 23 where id_edicao  = 27 and no_campo_bd = 'id_tipo_combo'