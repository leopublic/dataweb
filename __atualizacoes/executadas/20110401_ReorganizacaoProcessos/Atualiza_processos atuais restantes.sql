select p.id_solicita_visto, p.cd_candidato, DT_PRAZO_AUTORIZACAO_MTE, p.prazo_solicitado
from processo_mte p, AUTORIZACAO_CANDIDATO AC
where AC.NU_CANDIDATO = p.cd_candidato
and AC.id_solicita_visto = p.id_solicita_visto
and p.prazo_solicitado is null and AC.DT_PRAZO_AUTORIZACAO_MTE is not null and AC.DT_PRAZO_AUTORIZACAO_MTE <> '' ;
;

update processo_mte, AUTORIZACAO_CANDIDATO
set processo_mte.prazo_solicitado = AUTORIZACAO_CANDIDATO.DT_PRAZO_AUTORIZACAO_MTE
where AUTORIZACAO_CANDIDATO.NU_CANDIDATO = processo_mte.cd_candidato
and AUTORIZACAO_CANDIDATO.id_solicita_visto = processo_mte.id_solicita_visto
and processo_mte.prazo_solicitado is null 
and AUTORIZACAO_CANDIDATO.DT_PRAZO_AUTORIZACAO_MTE is not null 
and AUTORIZACAO_CANDIDATO.DT_PRAZO_AUTORIZACAO_MTE <> '' ;

update processo_mte , CANDIDATO
set fl_visto_atual = 1 
where fl_visto_atual = 0 
and processo_mte.cd_candidato = CANDIDATO.NU_CANDIDATO
and CANDIDATO.codigo_processo_mte_atual is null
and processo_mte.prazo_solicitado is not null
 and processo_mte.prazo_solicitado <> '' 
;
update CANDIDATO , processo_mte
set CANDIDATO.codigo_processo_mte_atual = processo_mte.codigo
where fl_visto_atual = 1 
and processo_mte.cd_candidato = CANDIDATO.NU_CANDIDATO
and CANDIDATO.codigo_processo_mte_atual is null
;