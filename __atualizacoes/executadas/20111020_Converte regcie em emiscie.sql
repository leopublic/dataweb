-- Converte os processos de registro dos servi�os de coleta de cie para processos de emis_cie.
alter table processo_emiscie add dt_execucao datetime null; 
alter table processo_emiscie add nu_protocolo varchar(20) null; 
insert into processo_emiscie (cd_candidato,cd_solicitacao,nu_protocolo, dt_execucao,observacao,dt_cad,dt_ult,id_solicita_visto,codigo_processo_mte,fl_vazio,nu_servico,cd_usuario)
select cd_candidato, cd_solicitacao, nu_protocolo, dt_cad, concat(observacao, '\n\n(processo convertido em 20/10/2011)'), dt_cad, dt_ult, id_solicita_visto, codigo_processo_mte, fl_vazio,nu_servico,cd_usuario
from processo_regcie 
where nu_servico in (22,23) and (fl_vazio = 0 or fl_vazio is null)
;
update SERVICO set ID_TIPO_ACOMPANHAMENTO = 5 where NU_SERVICO in (22,23);
;
delete from processo_regcie where nu_servico in (22,23) and (fl_vazio = 0 or fl_vazio is null);
select * from vprocesso;