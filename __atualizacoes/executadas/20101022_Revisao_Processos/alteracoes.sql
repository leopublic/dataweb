--
-- Cria o relacionamento entre os processos e as autorizações dos candidatos.
alter table processo_prorrog add codigo_processo_mte int null;
alter table processo_cancel add codigo_processo_mte int null;
alter table processo_emiscie add codigo_processo_mte int null;
alter table processo_regcie add codigo_processo_mte int null;

update processo_prorrog set codigo_processo_mte = null;
update processo_cancel set codigo_processo_mte = null;
update processo_regcie set codigo_processo_mte = null;
update processo_emiscie set codigo_processo_mte = null;

--
-- Linka as satelites antigas com seus pr�prios MTE (era tudo junto..)
update processo_prorrog, processo_mte
set processo_prorrog.codigo_processo_mte = processo_mte.codigo
where processo_mte.cd_solicitacao = processo_prorrog.cd_solicitacao
and processo_prorrog.cd_candidato = processo_mte.cd_candidato
and processo_prorrog.cd_solicitacao < 5698
;
update processo_prorrog, processo_mte
set processo_prorrog.codigo_processo_mte = processo_mte.codigo
where processo_mte.cd_solicitacao = processo_prorrog.cd_solicitacao
and processo_prorrog.cd_candidato = processo_mte.cd_candidato
and processo_prorrog.id_solicita_visto = processo_mte.id_solicita_visto 
and processo_prorrog.cd_solicitacao < 5698
;

update processo_regcie, processo_mte
set processo_regcie.codigo_processo_mte = processo_mte.codigo
where processo_mte.cd_solicitacao = processo_regcie.cd_solicitacao
and processo_regcie.cd_candidato = processo_mte.cd_candidato
and processo_regcie.cd_solicitacao < 5698
;
update processo_emiscie, processo_mte
set processo_emiscie.codigo_processo_mte = processo_mte.codigo
where processo_mte.cd_solicitacao = processo_emiscie.cd_solicitacao
and processo_emiscie.cd_candidato = processo_mte.cd_candidato
and processo_emiscie.cd_solicitacao < 5698
;
update processo_cancel, processo_mte
set processo_cancel.codigo_processo_mte = processo_mte.codigo
where processo_mte.cd_solicitacao = processo_cancel.cd_solicitacao
and processo_cancel.cd_candidato = processo_mte.cd_candidato
and processo_cancel.cd_solicitacao < 5698

;
--
-- Linka os processos novos
update processo_prorrog
set processo_prorrog.codigo_processo_mte = 
                  (select max(codigo) 
                        from processo_mte mteI
                        left join solicita_visto sv on sv.NU_SOLICITACAO = mteI.cd_solicitacao
                        left join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO
                        where mteI.cd_candidato = processo_prorrog.cd_candidato
                        and ((mteI.cd_solicitacao < 5698) 
                              or (mteI.cd_solicitacao >= 5698 and S.ID_TIPO_ACOMPANHAMENTO = 1) ))
where processo_prorrog.id_solicita_visto is not null
and processo_prorrog.cd_solicitacao >= 5698
;
update processo_regcie
set processo_regcie.codigo_processo_mte = 
                  (select max(codigo) 
                        from processo_mte mteI
                        left join solicita_visto sv on sv.NU_SOLICITACAO = mteI.cd_solicitacao
                        left join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO
                        where mteI.cd_candidato = processo_regcie.cd_candidato
                        and ((mteI.cd_solicitacao < 5698) 
                              or (mteI.cd_solicitacao >= 5698 and S.ID_TIPO_ACOMPANHAMENTO = 1) ))
where processo_regcie.id_solicita_visto is not null
and processo_regcie.cd_solicitacao >= 5698
;
update processo_emiscie
set processo_emiscie.codigo_processo_mte = 
                  (select max(codigo) 
                        from processo_mte mteI
                        left join solicita_visto sv on sv.NU_SOLICITACAO = mteI.cd_solicitacao
                        left join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO
                        where mteI.cd_candidato = processo_emiscie.cd_candidato
                        and ((mteI.cd_solicitacao < 5698) 
                              or (mteI.cd_solicitacao >= 5698 and S.ID_TIPO_ACOMPANHAMENTO = 1) ))
where processo_emiscie.id_solicita_visto is not null
and processo_emiscie.cd_solicitacao >= 5698
;
update processo_cancel
set processo_cancel.codigo_processo_mte = 
                  (select max(codigo) 
                        from processo_mte mteI
                        left join solicita_visto sv on sv.NU_SOLICITACAO = mteI.cd_solicitacao
                        left join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO
                        where mteI.cd_candidato = processo_cancel.cd_candidato
                        and ((mteI.cd_solicitacao < 5698) 
                              or (mteI.cd_solicitacao >= 5698 and S.ID_TIPO_ACOMPANHAMENTO = 1) ))
where processo_cancel.id_solicita_visto is not null
and processo_cancel.cd_solicitacao >= 5698
;
