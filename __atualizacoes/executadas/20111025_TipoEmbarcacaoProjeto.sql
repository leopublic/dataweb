drop table if exists TIPO_EMBARCACAO_PROJETO;

/*==============================================================*/
/* Table: TIPO_EMBARCACAO_PROJETO                               */
/*==============================================================*/
create table TIPO_EMBARCACAO_PROJETO
(
   ID_TIPO_EMBARCACAO_PROJETO int not null auto_increment,
   NO_TIPO_EMBARCACAO_PROJETO varchar(100),
   primary key (ID_TIPO_EMBARCACAO_PROJETO)
);
alter table EMBARCACAO_PROJETO add ID_TIPO_EMBARCACAO_PROJETO int null;
alter table EMBARCACAO_PROJETO add constraint FK_EMBARCACAO_PROJETO_TIPO_EMBARCACAO_PROJETO foreign key (ID_TIPO_EMBARCACAO_PROJETO)
      references TIPO_EMBARCACAO_PROJETO (ID_TIPO_EMBARCACAO_PROJETO) on delete restrict on update restrict;
insert into TIPO_EMBARCACAO_PROJETO (NO_TIPO_EMBARCACAO_PROJETO) values ('Afretamento');
insert into TIPO_EMBARCACAO_PROJETO (NO_TIPO_EMBARCACAO_PROJETO) values ('Presta��o de Servi�os');
insert into TIPO_EMBARCACAO_PROJETO (NO_TIPO_EMBARCACAO_PROJETO) values ('Assit�ncia t�cnica');

update EMBARCACAO_PROJETO set ID_TIPO_EMBARCACAO_PROJETO = 1 where IN_EMBARCACAO_PROJETO = 'E';
update EMBARCACAO_PROJETO set ID_TIPO_EMBARCACAO_PROJETO = 2 where IN_EMBARCACAO_PROJETO = 'P';
update EMBARCACAO_PROJETO set ID_TIPO_EMBARCACAO_PROJETO = 3 where IN_EMBARCACAO_PROJETO = 'T';

alter table EMBARCACAO_PROJETO add nu_estado int;
update EMBARCACAO_PROJETO set nu_estado = (select nu_estado from estado where estado.no_sigla = EMBARCACAO_PROJETO.CO_UF);

