alter table autorizacao_candidato 
	  add autc_fl_revisado tinyint null
	, add cd_usuario_revisao int null
;
alter table solicita_visto 
	  add soli_dt_devolucao datetime null
    , add cd_usuario_devolucao int
;
update autorizacao_candidato, solicita_visto  
set autc_fl_revisado = 1 
where solicita_visto.id_solicita_visto = autorizacao_candidato.id_solicita_visto 
and id_status_sol = 6
;
update status_solicitacao set no_status_sol = 'nova',  no_status_sol_res = 'nova' where id_status_sol = 1;
update status_solicitacao set no_status_sol = 'pendente',  no_status_sol_res = 'pendente' where id_status_sol = 2;
update status_solicitacao set no_status_sol = 'devolvida',  no_status_sol_res = 'devolvida' where id_status_sol = 3;
update status_solicitacao set no_status_sol = 'enviada bsb',  no_status_sol_res = 'enviada bsb' where id_status_sol = 4;

