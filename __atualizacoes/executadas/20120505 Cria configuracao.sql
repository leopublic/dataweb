drop table if exists grupo_configuracoes;
/*==============================================================*/
/* Table: grupo_configuracoes                                   */
/*==============================================================*/
create table grupo_configuracoes
(
   cfgg_id              int not null auto_increment,
   cfgg_tx_nome         varchar(500),
   primary key (cfgg_id)
);
drop table if exists configuracoes;
/*==============================================================*/
/* Table: configuracoes                                         */
/*==============================================================*/
create table configuracoes
(
   conf_id              int not null auto_increment,
   cfgg_id              int,
   conf_tx_codigo       varchar(300),
   conf_tx_nome         varchar(300),
   conf_tx_valor        varchar(1000),
   primary key (conf_id)
);

alter table configuracoes add constraint fk_configuracoes_grupo_configuracoes foreign key (cfgg_id)
      references grupo_configuracoes (cfgg_id) on delete restrict on update restrict;
insert into grupo_configuracoes (cfgg_tx_nome) values ('Endere�o da MV para faturamento');
select * from grupo_configuracoes;
insert into configuracoes (cfgg_id, conf_tx_codigo, conf_tx_nome, conf_tx_valor) 
values (1, 'RAZAO_SOCIAL', 'Raz�o social', 'Mundivisas')
, (1, 'CNPJ', 'CNPJ', '02.414.595-0001/57')
, (1, 'ENDERECO', 'Endere�o', 'Rua S�o Pedro')
, (1, 'INSC_ESTADUAL', 'Inscri��o estadual', '23445454')
, (1, 'INSC_MUNICIPAL', 'Inscri��o municipal', '22932938')
, (1, 'CIDADE', 'Cidade', 'Niter�i')
, (1, 'ESTADO', 'Estado', 'RJ')
, (1, 'INSC_MUNICIPAL', 'Inscri��o municipal', '22932938')

;
alter table solicita_visto drop foreign key  fk_solicita_visto_resumo_cobranca
;
drop table if exists resumo_cobranca;

/*==============================================================*/
/* Table: resumo_cobranca                                       */
/*==============================================================*/
create table resumo_cobranca
(
   resc_id              int not null auto_increment,
   nu_empresa_requerente int(10),
   strc_id              int,
   NU_EMBARCACAO_PROJETO_COBRANCA int(10),
   resc_dt_criacao      date,
   resc_dt_faturamento  date,
   resc_dt_recebimento  date,
   resc_tx_nota_fiscal  varchar(10),
   resc_nu_numero       int,
   primary key (resc_id)
);

alter table resumo_cobranca add constraint fk_resumo_cobranca_embarcacao_projeto foreign key (nu_empresa_requerente, NU_EMBARCACAO_PROJETO_COBRANCA)
      references EMBARCACAO_PROJETO (NU_EMPRESA, NU_EMBARCACAO_PROJETO) on delete restrict on update restrict;

alter table resumo_cobranca add constraint fk_resumo_cobranca_empresa foreign key (nu_empresa_requerente)
      references EMPRESA (NU_EMPRESA) on delete restrict on update restrict;

alter table resumo_cobranca add constraint fk_resumo_cobranca_status_resumo_cobranca foreign key (strc_id)
      references status_resumo_cobranca (strc_id) on delete restrict on update restrict;
