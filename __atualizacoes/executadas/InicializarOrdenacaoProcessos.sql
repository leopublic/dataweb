drop procedure if exists InicializaOrdemProcessos;
create procedure InicializaOrdemProcessos() 
BEGIN
  DECLARE eof INT DEFAULT FALSE;
  DECLARE xordem int ;
  DECLARE xcd_candidato int;
  DECLARE xnome_completo varchar(500);
  DECLARE xtipo varchar(7);
  DECLARE xcodigo, xcodigo_mte int;
  DECLARE candidatos CURSOR FOR SELECT NU_CANDIDATO, NOME_COMPLETO FROM CANDIDATO order by NU_CANDIDATO desc;
  DECLARE mtes CURSOR FOR 
      SELECT codigo  
      FROM processo_mte
      WHERE cd_candidato = xcd_candidato ;

  DECLARE processos CURSOR FOR 
      SELECT tipo, codigo  
      FROM vProcesso 
      WHERE cd_candidato = xcd_candidato 
      AND tipo <> 'coleta' 
      AND tipo <> 'mte' 
      AND codigo_processo_mte = xcodigo_mte 
      ORDER BY dt_cad ;
      
  DECLARE coletas CURSOR FOR 
      SELECT codigo  
      FROM processo_coleta 
      WHERE cd_candidato = xcd_candidato 
      AND codigo_processo_mte = xcodigo_mte 
      ORDER BY dt_cad ;
      
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET eof = TRUE;

  OPEN candidatos;
  set eof = false;
  loop_candidatos: LOOP
    FETCH candidatos INTO xcd_candidato, xnome_completo;
    IF eof THEN
      LEAVE loop_candidatos;
    END IF;
--     select xcd_candidato, xnome_completo;

    update processo_cancel set nu_ordem = 0 where cd_candidato = xcd_candidato;
    update processo_emiscie set nu_ordem = 0 where cd_candidato = xcd_candidato;
    update processo_prorrog set nu_ordem = 0 where cd_candidato = xcd_candidato;
    update processo_regcie set nu_ordem = 0 where cd_candidato = xcd_candidato;
    
    OPEN mtes;
    loop_mtes: LOOP
      FETCH mtes INTO xcodigo_mte;
      IF eof THEN
        LEAVE loop_mtes;
      END IF;
      set xordem = 0;

      OPEN coletas;
      loop_coletas: LOOP
        FETCH coletas INTO xcodigo;
        IF eof THEN
          LEAVE loop_coletas;
        END IF;
        set xordem = xordem + 10;
        update processo_coleta set nu_ordem = xordem where codigo = xcodigo;
      END LOOP;
      CLOSE coletas; 
      set eof = false;
  
      OPEN processos;
      loop_processos: LOOP
        FETCH processos INTO xtipo, xcodigo;
        IF eof THEN
          LEAVE loop_processos;
        END IF;
        set xordem = xordem + 10;
        CASE xtipo
          WHEN 'cancel'  THEN update processo_cancel  set nu_ordem = xordem where codigo = xcodigo;
          WHEN 'emiscie' THEN update processo_emiscie set nu_ordem = xordem where codigo = xcodigo;
          WHEN 'prorrog' THEN update processo_prorrog set nu_ordem = xordem where codigo = xcodigo;
          WHEN 'regcie'  THEN update processo_regcie  set nu_ordem = xordem where codigo = xcodigo;
        END CASE;
        
      END LOOP;
      CLOSE processos;
      set eof = false;
      
    END LOOP;
    CLOSE mtes; 
    set eof = false;
    
  END LOOP;

  CLOSE candidatos;
END;
;
