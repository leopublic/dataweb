
CREATE  TABLE IF NOT EXISTS historico_candidato (
  hica_id INT(11) UNSIGNED NOT NULL auto_increment,
  nu_candidato INT(11) NULL ,
  hica_dt_evento DATETIME NULL ,
  cd_usuario INT(11) UNSIGNED NULL ,
  hica_tx_observacoes VARCHAR(1500) NULL ,
  PRIMARY KEY (hica_id) ,
  INDEX ix_candidato (nu_candidato ASC, hica_dt_evento ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
;
