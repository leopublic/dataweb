drop procedure if exists ClonarCadastroCandidatoOs;
create procedure ClonarCadastroCandidatoOs(pnu_solicitacao_origem int,  pnu_solicitacao_destino int) 
BEGIN
  DECLARE xid_solicita_visto_origem int;
  DECLARE xid_solicita_visto_destino  int;
  DECLARE xNU_EMPRESA int;
  DECLARE xNU_EMBARCACAO_PROJETO int;
  DECLARE xNU_SERVICO int;
  
    select id_solicita_visto into xid_solicita_visto_origem 
        from solicita_visto 
        where NU_SOLICITACAO = pnu_solicitacao_origem;
        
    select id_solicita_visto , NU_EMPRESA ,  NU_SERVICO 
        into xid_solicita_visto_destino , xNU_EMPRESA,  xNU_SERVICO
        from solicita_visto 
        where NU_SOLICITACAO = pnu_solicitacao_destino;
    select NU_EMBARCACAO_PROJETO 
        into xNU_EMBARCACAO_PROJETO 
        from AUTORIZACAO_CANDIDATO
        where id_solicita_visto = xid_solicita_visto_destino limit 1;
        
        
    delete from AUTORIZACAO_CANDIDATO where id_solicita_visto = xid_solicita_visto_destino  ;
    insert into AUTORIZACAO_CANDIDATO (
       NU_EMPRESA
      ,NU_CANDIDATO
      ,NU_SOLICITACAO
      ,NU_EMBARCACAO_PROJETO
      ,CO_TIPO_AUTORIZACAO
      ,VA_RENUMERACAO_MENSAL
      ,NO_MOEDA_REMUNERACAO_MENSAL
      ,VA_REMUNERACAO_MENSAL_BRASIL
      ,CO_REPARTICAO_CONSULAR
      ,CO_FUNCAO_CANDIDATO
      ,TE_DESCRICAO_ATIVIDADES
      ,DT_PRAZO_ESTADA_SOLICITADO
      ,DS_JUSTIFICATIVA
      ,DS_LOCAL_TRABALHO
      ,DS_SALARIO_EXTERIOR
      ,DS_BENEFICIOS_SALARIO
      ,NU_EMBARCACAO_INICIAL
      ,CD_EMBARCADO
      ,DT_SITUACAO_SOL
      ,DT_CADASTRAMENTO
      ,DT_ULT_ALTERACAO
      ,NU_USUARIO_CAD
      ,NO_JUSTIFICATIVA_REP_CONS
      ,id_solicita_visto
      ,NU_SERVICO
      ,SALARIO_DOLAR
      ,DT_PRAZO_AUTORIZACAO_MTE
    ) 
    select        
      xNU_EMPRESA
      ,NU_CANDIDATO
      ,pnu_solicitacao_destino
      ,xNU_EMBARCACAO_PROJETO
      ,CO_TIPO_AUTORIZACAO
      ,VA_RENUMERACAO_MENSAL
      ,NO_MOEDA_REMUNERACAO_MENSAL
      ,VA_REMUNERACAO_MENSAL_BRASIL
      ,CO_REPARTICAO_CONSULAR
      ,CO_FUNCAO_CANDIDATO
      ,TE_DESCRICAO_ATIVIDADES
      ,DT_PRAZO_ESTADA_SOLICITADO
      ,DS_JUSTIFICATIVA
      ,DS_LOCAL_TRABALHO
      ,DS_SALARIO_EXTERIOR
      ,DS_BENEFICIOS_SALARIO
      ,NU_EMBARCACAO_INICIAL
      ,CD_EMBARCADO
      ,DT_SITUACAO_SOL
      ,now()
      ,now()
      ,NU_USUARIO_CAD
      ,NO_JUSTIFICATIVA_REP_CONS
      ,xid_solicita_visto_destino
      ,xNU_SERVICO
      ,SALARIO_DOLAR
      ,DT_PRAZO_AUTORIZACAO_MTE
 from AUTORIZACAO_CANDIDATO where id_solicita_visto = xid_solicita_visto_origem;
    
END;