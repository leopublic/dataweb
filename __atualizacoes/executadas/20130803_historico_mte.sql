CREATE TABLE historico_processo_mte (
  hmte_id int(10) unsigned NOT NULL AUTO_INCREMENT,
  hmte_dt_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  codigo int(11) DEFAULT NULL,
  teve_id int(11) DEFAULT NULL,
  cd_usuario int(10) unsigned DEFAULT NULL,
  hmte_tx_observacao text,
  PRIMARY KEY (hmte_id)
) ENGINE=InnoDB