alter table processo_mte add fl_cp_passaporte smallint default 0;
alter table processo_mte add fl_form_144 smallint default 0;
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_chave, nu_ordem, bo_readonly, bo_exibir) values (36,9,'Cópia do passaporte?', 'fl_cp_passaporte', 0, 70, 0, 1);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_chave, nu_ordem, bo_readonly, bo_exibir) values (36,9,'Form. 144 assinado?', 'fl_form_144', 0, 80, 0, 1);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_chave, nu_ordem, bo_readonly, bo_exibir, nu_ordem_painel) values (19,9,'Cópia do passaporte?', 'fl_cp_passaporte', 0, 70, 1, 1, 0);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_chave, nu_ordem, bo_readonly, bo_exibir, nu_ordem_painel) values (19,9,'Form. 144 assinado?', 'fl_form_144', 0, 80, 1, 1, 0);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_chave, nu_ordem, bo_readonly, bo_exibir, nu_ordem_painel) values (7,9,'Cópia do passaporte?', 'fl_cp_passaporte', 0, 78, 0, 1, 0);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_chave, nu_ordem, bo_readonly, bo_exibir, nu_ordem_painel) values (7,9,'Form. 144 assinado?', 'fl_form_144', 0, 79, 1, 0, 0);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_chave, nu_ordem, bo_readonly, bo_exibir, nu_ordem_painel) values (20,9,'Cópia do passaporte?', 'fl_cp_passaporte', 0, 78, 1, 1, 0);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_chave, nu_ordem, bo_readonly, bo_exibir, nu_ordem_painel) values (20,9,'Form. 144 assinado?', 'fl_form_144', 0, 79, 1, 1, 0);

