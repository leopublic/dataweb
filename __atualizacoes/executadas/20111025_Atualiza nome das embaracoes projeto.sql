alter table EMBARCACAO_PROJETO add NO_PREFIXO varchar(20) null;
alter table EMBARCACAO_PROJETO add cd_usuario int null;
alter table EMBARCACAO_PROJETO add TX_OBSERVACAO varchar(500) null;
-- select * from EMBARCACAO_PROJETO Where NU_EMPRESA = 21 order by NO_EMBARCACAO_PROJETO;
update EMBARCACAO_PROJETO set NO_EMBARCACAO_PROJETO = REPLACE(NO_EMBARCACAO_PROJETO, 'M/T', '')   , NO_PREFIXO = 'M/T'   where NU_EMPRESA = 21 and NO_EMBARCACAO_PROJETO like 'M/T%';
update EMBARCACAO_PROJETO set NO_EMBARCACAO_PROJETO = REPLACE(NO_EMBARCACAO_PROJETO, 'LPG/C', '') , NO_PREFIXO = 'LPG/C' where NU_EMPRESA = 21 and NO_EMBARCACAO_PROJETO like 'LPG/C%';
update EMBARCACAO_PROJETO set NO_EMBARCACAO_PROJETO = REPLACE(NO_EMBARCACAO_PROJETO, 'LPGC', '')  , NO_PREFIXO = 'LPGC'  where NU_EMPRESA = 21 and NO_EMBARCACAO_PROJETO like 'LPGC%';
update EMBARCACAO_PROJETO set NO_EMBARCACAO_PROJETO = REPLACE(NO_EMBARCACAO_PROJETO, 'LPG', '')   , NO_PREFIXO = 'LPG'   where NU_EMPRESA = 21 and NO_EMBARCACAO_PROJETO like 'LPG%';
update EMBARCACAO_PROJETO set NO_EMBARCACAO_PROJETO = REPLACE(NO_EMBARCACAO_PROJETO, 'M/V', '')   , NO_PREFIXO = 'M/V'   where NU_EMPRESA = 21 and NO_EMBARCACAO_PROJETO like 'M/V%';
update EMBARCACAO_PROJETO set NO_EMBARCACAO_PROJETO = REPLACE(NO_EMBARCACAO_PROJETO, 'LNGC', '')  , NO_PREFIXO = 'LNGC'  where NU_EMPRESA = 21 and NO_EMBARCACAO_PROJETO like 'LNGC%';
update EMBARCACAO_PROJETO set NO_EMBARCACAO_PROJETO = REPLACE(NO_EMBARCACAO_PROJETO, 'LNG', '')   , NO_PREFIXO = 'LNG'   where NU_EMPRESA = 21 and NO_EMBARCACAO_PROJETO like 'LNG%';
update EMBARCACAO_PROJETO set NO_EMBARCACAO_PROJETO = trim(NO_EMBARCACAO_PROJETO) where NU_EMPRESA = 21 and NO_EMBARCACAO_PROJETO not like '%Sem em%';


