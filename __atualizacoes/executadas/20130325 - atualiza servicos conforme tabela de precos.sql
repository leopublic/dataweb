alter table solicita_visto add soli_tx_descricao_servico varchar(500);
-- Atualiza a descrição dos serviços conforme a tabela de serviços
update solicita_visto, servico
set soli_tx_descricao_servico = no_servico_resumido
where servico.nu_servico = solicita_visto.nu_servico
and solicita_visto.nu_servico is not null;
-- Atualiza descrição dos serviços conforme a tabela de preços.
update solicita_visto, empresa, tabela_precos, preco
set soli_tx_descricao_servico = prec_tx_descricao_tabela_precos
, soli_tx_codigo_servico = prec_tx_item
where empresa.nu_empresa = solicita_visto.nu_empresa
and tabela_precos.tbpc_id = empresa.tbpc_id
and preco.tbpc_id = tabela_precos.tbpc_id
and preco.nu_servico = solicita_visto.nu_servico;
-- Atualiza descrição dos serviços conforme a tabela de preços.
update solicita_visto, empresa, tabela_precos, preco
set soli_vl_cobrado = prec_vl_pacote
where empresa.nu_empresa = solicita_visto.nu_empresa
and tabela_precos.tbpc_id = empresa.tbpc_id
and preco.tbpc_id = tabela_precos.tbpc_id
and preco.nu_servico = solicita_visto.nu_servico
and solicita_visto.tppr_id = 3
and solicita_visto.stco_id in (1,2)
;
-- Atualiza descrição dos serviços conforme a tabela de preços.
update solicita_visto, empresa, tabela_precos, preco
set soli_vl_cobrado = prec_vl_conceito
where empresa.nu_empresa = solicita_visto.nu_empresa
and tabela_precos.tbpc_id = empresa.tbpc_id
and preco.tbpc_id = tabela_precos.tbpc_id
and preco.nu_servico = solicita_visto.nu_servico
and solicita_visto.tppr_id = 2
and solicita_visto.stco_id in (1,2)
;

-- Atualiza descrição dos serviços conforme a tabela de preços.
update solicita_visto, empresa, tabela_precos, preco
set soli_tx_descricao_servico = prec_tx_descricao_tabela_precos
where empresa.nu_empresa = solicita_visto.nu_empresa
and tabela_precos.tbpc_id = empresa.tbpc_id
and preco.tbpc_id = tabela_precos.tbpc_id
and preco.nu_servico = solicita_visto.nu_servico
and solicita_visto.stco_id = 1;
