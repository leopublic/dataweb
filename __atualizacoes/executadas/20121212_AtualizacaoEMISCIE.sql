alter table processo_prorrog add dt_envio_bsb date default null;
update processo_prorrog set dt_envio_bsb = dt_requerimento where dt_requerimento is not null;
alter table processo_cancel add dt_envio_bsb date default null;
update processo_cancel set dt_envio_bsb = dt_processo where dt_processo is not null;


update campo set no_label = 'N&uacute;mero RNE' where id_campo in (272,282);
update campo set no_label = 'Validade da CIE' where id_campo in (274,284);
update campo set no_label = 'Data de expedi&ccedil;&atilde;o' where id_campo in(273,283);
alter table processo_emiscie add dt_atendimento date null;

