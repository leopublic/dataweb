select VA_RENUMERACAO_MENSAL
--  , substring(VA_RENUMERACAO_MENSAL, 1, instr(VA_RENUMERACAO_MENSAL, '.') - 1) 
-- , substring(VA_RENUMERACAO_MENSAL, instr(VA_RENUMERACAO_MENSAL, '.') + 1) 
  , concat(substring(VA_RENUMERACAO_MENSAL, 1, instr(VA_RENUMERACAO_MENSAL, '.')) 
        , replace(substring(VA_RENUMERACAO_MENSAL, instr(VA_RENUMERACAO_MENSAL, '.') + 1) , '.', ','))
  from AUTORIZACAO_CANDIDATO
where VA_RENUMERACAO_MENSAL like '%.%.%'
;


select VA_RENUMERACAO_MENSAL
--  , substring(VA_RENUMERACAO_MENSAL, 1, instr(VA_RENUMERACAO_MENSAL, '.') - 1) 
-- , substring(VA_RENUMERACAO_MENSAL, instr(VA_RENUMERACAO_MENSAL, '.') + 1) 
  , concat(replace(substring(VA_RENUMERACAO_MENSAL, 1, instr(VA_RENUMERACAO_MENSAL, '.')-1), ',', '.')
          , replace(substring(VA_RENUMERACAO_MENSAL, instr(VA_RENUMERACAO_MENSAL, '.') ) , '.', ','),'0')
       
  from AUTORIZACAO_CANDIDATO
where VA_RENUMERACAO_MENSAL like '%n%'
;



-- Conversao
update AUTORIZACAO_CANDIDATO 
    set VA_RENUMERACAO_MENSAL = '0,00'
    where trim(VA_RENUMERACAO_MENSAL) = '.' or trim(VA_RENUMERACAO_MENSAL) = '..' or trim(VA_RENUMERACAO_MENSAL) = '...';

update AUTORIZACAO_CANDIDATO 
    set VA_RENUMERACAO_MENSAL =   concat(substring(VA_RENUMERACAO_MENSAL, 1, instr(VA_RENUMERACAO_MENSAL, ',') - 1) 
        ,'.'
        , substring(VA_RENUMERACAO_MENSAL, instr(VA_RENUMERACAO_MENSAL, ',') + 1) 
       )
    where VA_RENUMERACAO_MENSAL like '%,%,%';

update AUTORIZACAO_CANDIDATO
    set VA_RENUMERACAO_MENSAL = substr(VA_RENUMERACAO_MENSAL, 1, length(VA_RENUMERACAO_MENSAL)-1)
    where VA_RENUMERACAO_MENSAL like '%.'
;
update AUTORIZACAO_CANDIDATO 
    set VA_RENUMERACAO_MENSAL =   concat(substring(VA_RENUMERACAO_MENSAL, 1, instr(VA_RENUMERACAO_MENSAL, ',')) 
        , replace(substring(VA_RENUMERACAO_MENSAL, instr(VA_RENUMERACAO_MENSAL, ',') + 1) , '.', ',')
       )
    where VA_RENUMERACAO_MENSAL like '%.%.%'
;
update AUTORIZACAO_CANDIDATO 
    set VA_RENUMERACAO_MENSAL =   concat(replace(substring(VA_RENUMERACAO_MENSAL, 1, instr(VA_RENUMERACAO_MENSAL, '.')-1), ',', '.')
          , replace(substring(VA_RENUMERACAO_MENSAL, instr(VA_RENUMERACAO_MENSAL, '.') ) , '.', ','))
    where VA_RENUMERACAO_MENSAL like '%.__'
;
update AUTORIZACAO_CANDIDATO 
    set VA_RENUMERACAO_MENSAL =   concat(replace(substring(VA_RENUMERACAO_MENSAL, 1, instr(VA_RENUMERACAO_MENSAL, '.')-1), ',', '.')
          , replace(substring(VA_RENUMERACAO_MENSAL, instr(VA_RENUMERACAO_MENSAL, '.') ) , '.', ','),'0')
    where VA_RENUMERACAO_MENSAL like '%._'
    