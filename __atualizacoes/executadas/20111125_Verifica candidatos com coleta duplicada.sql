select * from EMPRESA where NO_RAZAO_SOCIAL like '%Ensco%';
select  E.NO_RAZAO_SOCIAL, EP.NO_EMBARCACAO_PROJETO, c.NOME_COMPLETO, cd_candidato, pc.codigo_processo_mte, count(*) 
from processo_coleta pc, EMPRESA E, CANDIDATO c left join EMBARCACAO_PROJETO EP on EP.NU_EMBARCACAO_PROJETO = c.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = c.NU_EMPRESA
where c.NU_CANDIDATO = pc.cd_candidato
and (c.FL_INATIVO is null or c.FL_INATIVO = 0)
-- and c.NU_EMPRESA = 164
and E.NU_EMPRESA = c.NU_EMPRESA
and E.FL_ATIVA = 1 
group by E.NO_RAZAO_SOCIAL, cd_candidato, pc.codigo_processo_mte
having count(*) > 1
order by NO_RAZAO_SOCIAL, EP.NO_EMBARCACAO_PROJETO, NOME_COMPLETO
;
