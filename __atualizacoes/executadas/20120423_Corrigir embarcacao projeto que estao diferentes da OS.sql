update solicita_visto, AUTORIZACAO_CANDIDATO
set solicita_visto.NU_EMBARCACAO_PROJETO = AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO
where AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
and solicita_visto.NU_EMBARCACAO_PROJETO is null
and AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO is not null
;
update solicita_visto, AUTORIZACAO_CANDIDATO, SERVICO
set solicita_visto.NU_EMBARCACAO_PROJETO = AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO
where AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
and SERVICO.NU_SERVICO = solicita_visto.NU_SERVICO
and solicita_visto.NU_EMBARCACAO_PROJETO is not null
and AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO is not null
and solicita_visto.NU_EMBARCACAO_PROJETO <> AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO
and (SERVICO.ID_TIPO_ACOMPANHAMENTO <> 1 or SERVICO.ID_TIPO_ACOMPANHAMENTO is null) 
;
update solicita_visto, AUTORIZACAO_CANDIDATO
set solicita_visto.NU_EMBARCACAO_PROJETO = AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO
where AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
and solicita_visto.NU_EMBARCACAO_PROJETO is not null
and AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO is not null
and solicita_visto.NU_EMBARCACAO_PROJETO <> AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO
and solicita_visto.NU_SERVICO is null
;
select solicita_visto.id_solicita_visto, solicita_visto.nu_solicitacao, solicita_visto.NU_SERVICO, solicita_visto.NU_EMBARCACAO_PROJETO, AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO
, concat('update solicita_visto set NU_EMBARCACAO_PROJETO = ', AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO, ' where id_solicita_visto = ', solicita_visto.id_solicita_visto)
from solicita_visto, AUTORIZACAO_CANDIDATO
where AUTORIZACAO_CANDIDATO.id_solicita_visto = solicita_visto.id_solicita_visto
and solicita_visto.NU_EMBARCACAO_PROJETO is not null
and AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO is not null
and solicita_visto.NU_EMBARCACAO_PROJETO <> AUTORIZACAO_CANDIDATO.NU_EMBARCACAO_PROJETO
and solicita_visto.NU_SERVICO is null

;
select sv.NU_EMPRESA, nu_empresa_requerente, sva.cd_tecnico from solicita_visto sv, solicita_visto_acomp sva
where ID_SOLICITA_VISTO=34648
and sva.cd_solicitacao = sv.nu_solicitacao
;
select * from usuarios where cd_usuario = 330;

SELECT sv.nu_solicitacao, sv.nu_empresa, sv.NU_SERVICO , date_format(sv.dt_cadastro, '%d/%m/%Y') dt_cadastro , sv.ID_STATUS_SOL, sv.fl_nao_conformidade , ac.NU_CANDIDATO, sv.NU_EMBARCACAO_PROJETO , sv.nu_empresa_requerente , sv.fl_liberacao_fechada , sv.fl_qualidade_fechada , no_solicitador 
, de_observacao 
, date_format(dt_solicitacao, '%d/%m/%Y') dt_solicitacao 
, sva.cd_tecnico 
, u.nome nome_criada_por 
, ut.nome nome_responsavel 
, S.ID_TIPO_ACOMPANHAMENTO , SS.NO_STATUS_SOL , ac.DT_PRAZO_ESTADA_SOLICITADO , fl_cobrado , NO_SERVICO_RESUMIDO , NO_SERVICO , E.NO_RAZAO_SOCIAL , NO_EMBARCACAO_PROJETO , EREQ.NO_RAZAO_SOCIAL NO_RAZAO_SOCIAL_REQUERENTE 
FROM solicita_visto sv 
JOIN EMPRESA E on E.NU_EMPRESA = sv.NU_EMPRESA JOIN EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = sv.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO 
LEFT JOIN EMPRESA EREQ on EREQ.NU_EMPRESA = sv.nu_empresa_requerente 
LEFT JOIN AUTORIZACAO_CANDIDATO ac on ac.id_solicita_visto = sv.id_solicita_visto 
LEFT JOIN solicita_visto_acomp sva on sva.cd_solicitacao = sv.nu_solicitacao 
LEFT JOIN SERVICO S on S.NU_SERVICO = sv.NU_SERVICO 
LEFT JOIN usuarios u on u.cd_usuario = sv.cd_admin_cad 
LEFT JOIN usuarios ut on u.cd_usuario = sva.cd_tecnico 
LEFT JOIN STATUS_SOLICITACAO SS on SS.ID_STATUS_SOL = sv.ID_STATUS_SOL 
WHERE sv.id_solicita_visto =34648 