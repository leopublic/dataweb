-- Exeuctado 04/05/2013
alter table empresa engine = InnoDB
;
alter table empresa
    add cd_usuario_ponto_focal int(11) unsigned null
;
alter table empresa 
  ADD CONSTRAINT fk_empresa_usuario_ponto_focal
    FOREIGN KEY (cd_usuario_ponto_focal )
    REFERENCES usuarios (cd_usuario )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
;
