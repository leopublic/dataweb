select codigo, pr.nu_servico, sv.NU_SERVICO, S.NO_SERVICO_RESUMIDO
FROM processo_regcie pr, solicita_visto sv , SERVICO S
where pr.id_solicita_visto = sv.id_solicita_visto
and pr.nu_servico is null
and sv.NU_SERVICO is not null
and S.NU_SERVICO = sv.NU_SERVICO
;
update processo_regcie, solicita_visto
set processo_regcie.nu_servico = solicita_visto.NU_SERVICO
where processo_regcie.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_regcie.nu_servico is null
and solicita_visto.NU_SERVICO is not null
;
insert into log_modificacoes(logm_data, logm_titulo, logm_descricao) values (now(), 'Corrigido o tipo de servi�o dos processos de registro', 'Foi corrigida a apresenta��o do tipo de servi�o dos processos de registros que estavam sendo apresentados como "tipo n�o informado", devido a um erro no sistema.');
