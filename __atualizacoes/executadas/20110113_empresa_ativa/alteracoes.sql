alter table EMPRESA add FL_ATIVA tinyint default 0;
update EMPRESA set FL_ATIVA = 1 where NU_EMPRESA in (select distinct nu_empresa  from solicita_visto where ID_STATUS_SOL < 6);
update Campo set id_tipo_combo = 22 where id_edicao = (select id_edicao from Edicao where no_edicao = 'SOLICITA_VISTO_edit') 
and no_campo_bd in ('nu_empresa_requerente', 'NU_EMPRESA') and id_tipo_campo = 3;

select * from Edicao;
select * from Campo where id_edicao in (7, 20) and no_label like 'Validade%';
update Campo set bo_exibir = 0 where id_campo in (210, 491);

 INSERT INTO SERVICO (CO_SERVICO, NO_SERVICO_RESUMIDO,NO_SERVICO, NU_TIPO_SERVICO, ID_TIPO_ACOMPANHAMENTO, CO_TIPO_AUTORIZACAO) VALUES( 37, 'Retificação de visto', 'Retificação dos dados do visto', 4, null, null )