alter table processo_prorrog add dt_envio_bsb_pre_cadastro date null;
update processo_prorrog, solicita_visto 
	set dt_envio_bsb_pre_cadastro = dt_pre_cadastro 
	where solicita_visto.id_solicita_visto = processo_prorrog.id_solicita_visto 
	and id_status_sol = 6;

update processo_prorrog set dt_envio_bsb_pre_cadastro = dt_pre_cadastro
where nu_pre_cadastro is not null
;
insert into campo (id_edicao, no_label, no_campo_bd, id_tipo_campo, bo_obrigatorio, bo_chave, nu_ordem, 
no_painel, nu_ordem_painel, bo_readonly, bo_exibir, fl_link_mte, fl_link_mj, fl_largura_dupla)
values (20, 'Data de envio BSB', 'dt_envio_bsb_pre_cadastro', 2, 0, 0, 15, 'Pr�-cadastro'
, 10, 1, 1, 0, 0, 0);
insert into campo (id_edicao, no_label, no_campo_bd, id_tipo_campo, bo_obrigatorio, bo_chave, nu_ordem, 
no_painel, nu_ordem_painel, bo_readonly, bo_exibir, fl_link_mte, fl_link_mj, fl_largura_dupla)
values (7, 'Data de envio BSB', 'dt_envio_bsb_pre_cadastro', 2, 0, 0, 15, 'Pr�-cadastro'
, 10, 0, 1, 0, 0, 0);

drop view if exists vprocesso_prorrog;
CREATE VIEW vprocesso_prorrog AS 
select processo_prorrog.codigo 
,processo_prorrog.cd_candidato 
,processo_prorrog.cd_solicitacao 
,processo_prorrog.nu_protocolo 
,date_format(processo_prorrog.dt_requerimento,'%d/%m/%Y') AS dt_requerimento
,date_format(processo_prorrog.dt_validade,'%d/%m/%Y') AS dt_validade
,date_format(processo_prorrog.dt_prazo_pret,'%d/%m/%Y') AS dt_prazo_pret
,date_format(processo_prorrog.dt_envio_bsb_pre_cadastro,'%d/%m/%Y') AS dt_envio_bsb_pre_cadastro
,processo_prorrog.observacao 
,processo_prorrog.dt_cad 
,processo_prorrog.dt_ult 
,processo_prorrog.id_solicita_visto 
,processo_prorrog.fl_vazio
,processo_prorrog.fl_processo_atual
,processo_prorrog.codigo_processo_mte
,processo_prorrog.codigo_regcie
,date_format(processo_prorrog.dt_publicacao_dou,'%d/%m/%Y') AS dt_publicacao_dou
,processo_prorrog.ID_STATUS_CONCLUSAO
,processo_prorrog.nu_servico
,processo_prorrog.no_classe
,processo_prorrog.no_metodo
,processo_prorrog.nu_pre_cadastro
,processo_prorrog.dt_pre_cadastro as dt_pre_cadastro_orig
,date_format(processo_prorrog.dt_pre_cadastro,'%d/%m/%Y') AS dt_pre_cadastro
,date_format(processo_prorrog.dt_envio_bsb,'%d/%m/%Y') AS dt_envio_bsb
,processo_prorrog.nu_pagina_dou
,SC.NO_STATUS_CONCLUSAO
,S.NO_SERVICO
,S.NO_SERVICO_RESUMIDO
,sv.ID_STATUS_SOL
,sv.nu_solicitacao
,u.nome nome_pre_cadastro
from processo_prorrog
left join STATUS_CONCLUSAO SC on SC.ID_STATUS_CONCLUSAO = processo_prorrog.ID_STATUS_CONCLUSAO
left join SERVICO S on S.NU_SERVICO = processo_prorrog.nu_servico
left join solicita_visto sv ON sv.id_solicita_visto = processo_prorrog.id_solicita_visto
left join usuarios u on u.cd_usuario = processo_prorrog.cd_usuario_pre_cadastro
;


;