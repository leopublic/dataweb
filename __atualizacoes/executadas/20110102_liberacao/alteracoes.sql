alter table Campo add fl_largura_dupla tinyint default 0;

insert into ACESSO(TIPO_ACESSO, NOME_ACESSO, CONST_ACESSO) values ('003.020', 'Ordem de Servi�o - ABA - Libera��o', 'tp_Os_Aba_Liberacao');

alter table solicita_visto add dt_liberacao  date null;
alter table solicita_visto add cd_usuario_qualidade int(11) null;
alter table solicita_visto add fl_conclusao_conforme tinyint null;
alter table solicita_visto add fl_liberado tinyint null;
alter table solicita_visto add de_comentarios_liberacao text null;
alter table solicita_visto add cd_usuario_liberacao int(11) null;
--
insert into Tipo_Campo(no_tipo_campo) values('SIMNAO_LITERAL');
--

delete from Campo where id_edicao = (select id_edicao from Edicao where no_edicao = 'liberacao');
delete from Campo where id_edicao = (select id_edicao from Edicao where no_edicao = 'qualidade');

insert into Edicao (no_edicao, no_view, no_tabela) values( 'liberacao', 'vLiberacao', 'solicita_visto');
insert into Edicao (no_edicao, no_view, no_tabela) values( 'qualidade', 'vLiberacao', 'solicita_visto');
delete from Campo where id_edicao = (select id_edicao from Edicao where no_edicao = 'liberacao');
delete from Campo where id_edicao = (select id_edicao from Edicao where no_edicao = 'qualidade');

-- Qualidade
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel)
values (6, 'id_solicita_visto', 'id_solicita_visto',0, 1,0,10,(select id_edicao from Edicao where no_edicao = 'qualidade'),0,0,'Controle de qualidade', 10);
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel)
values (13, 'Os servi�os constantes na OS foram satisfatoriamente conclu�dos?', 'fl_conclusao_conforme',0, 0,0,20,(select id_edicao from Edicao where no_edicao = 'qualidade'),1,0,'Controle de qualidade', 10);
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel)
values (6, 'Respons�vel', 'cd_usuario_qualidade',0, 0,0,30,(select id_edicao from Edicao where no_edicao = 'qualidade'),0,1,'Controle de qualidade', 10);
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel)
values (6, 'Respons�vel', 'nm_usuario_qualidade',0, 0,0,30,(select id_edicao from Edicao where no_edicao = 'qualidade'),1,1,'Controle de qualidade', 10);

-- Liberacao
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel)
values (6, 'id_solicita_visto', 'id_solicita_visto',0, 1,0,10,(select id_edicao from Edicao where no_edicao = 'liberacao'),0,0,'Libera��o de servi�o', 10);
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel)
values (2, 'Data libera��o', 'dt_liberacao',0, 0,0,30,(select id_edicao from Edicao where no_edicao = 'liberacao'),1,0,'Libera��o de servi�o', 10);
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel)
values (13, 'Servi�o liberado?', 'fl_liberado',0, 0,0,20,(select id_edicao from Edicao where no_edicao = 'liberacao'),1,0,'Libera��o de servi�o', 10);
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel, fl_largura_dupla)
values (7, 'Coment�rios', 'de_comentarios_liberacao',0, 0,19,60,(select id_edicao from Edicao where no_edicao = 'liberacao'),1,0,'Libera��o de servi�o', 10, 1);
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel)
values (6, 'Respons�vel', 'cd_usuario_liberacao',0, 0,0,70,(select id_edicao from Edicao where no_edicao = 'liberacao'),0,1,'Libera��o de servi�o', 10);
insert into Campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_exibir, bo_readonly, no_painel, nu_ordem_painel)
values (6, 'Respons�vel', 'nm_usuario_liberacao',0, 0,0,80,(select id_edicao from Edicao where no_edicao = 'liberacao'),1,1,'Libera��o de servi�o', 10);
