drop table if exists status_cobranca_os;

/*==============================================================*/
/* Table: status_cobranca_os                                    */
/*==============================================================*/
create table status_cobranca_os
(
   stco_id              int not null auto_increment,
   stco_tx_sigla        char(2),
   stco_tx_nome         varchar(50),
   primary key (stco_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
drop table if exists status_resumo_cobranca;

/*==============================================================*/
/* Table: status_resumo_cobranca                                */
/*==============================================================*/
create table status_resumo_cobranca
(
   strc_id              int not null auto_increment,
   strc_tx_sigla        char(2),
   strc_tx_nome         varchar(50),
   primary key (strc_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
drop table if exists resumo_cobranca;

/*==============================================================*/
/* Table: resumo_cobranca                                       */
/*==============================================================*/
create table resumo_cobranca
(
   resc_id              int not null auto_increment,
   NU_EMPRESA           int(10),
   strc_id              int,
   NU_EMBARCACAO_PROJETO int(10),
   resc_dt_criacao      date,
   resc_dt_faturamento  date,
   resc_dt_recebimento  date,
   resc_tx_nota_fiscal  varchar(10),
   resc_nu_numero       int,
   primary key (resc_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter table resumo_cobranca add constraint fk_resumo_cobranca_status_resumo_cobranca foreign key (strc_id)
      references status_resumo_cobranca (strc_id) on delete restrict on update restrict;
      
alter table solicita_visto add stco_id int null ;
alter table solicita_visto add resc_id int null ;
alter table solicita_visto add constraint fk_solicita_visto_status_cobranca_os foreign key (stco_id)
      references status_cobranca_os (stco_id) on delete restrict on update restrict;
alter table solicita_visto add constraint fk_solicita_visto_resumo_cobranca foreign key (resc_id)
      references resumo_cobranca (resc_id) on delete restrict on update restrict;
      
truncate status_cobranca_os;
insert into status_cobranca_os (stco_tx_sigla  ,stco_tx_nome) VALUES ('P'  ,'Pendente'), ('L'  ,'Liberada para cobran&ccedil;a'), ('F', 'Faturada');

alter table SERVICO add tbpc_id int null;
drop table if exists tabela_precos;

/*==============================================================*/
/* Table: tabela_precos                                         */
/*==============================================================*/
create table tabela_precos
(
   tbpc_id              int not null auto_increment,
   tbpc_tx_nome         varchar(200),
   tbpc_nu_ano          int,
   primary key (tbpc_id)
)
ENGINE = InnoDB;
drop table if exists preco;

/*==============================================================*/
/* Table: preco                                                 */
/*==============================================================*/
create table preco
(
   prec_id              int not null auto_increment,
   NU_SERVICO           int(11),
   tbpc_id              int,
   prec_vl_real   decimal(10,2),
   prec_vl_dolar        decimal(10,2),
   prec_tx_descricao_tabela_precos varchar(200),
primary key (prec_id)
)
ENGINE = InnoDB;
alter table SERVICO ENGINE=InnoDB;
alter table preco add constraint fk_preco_servico foreign key (NU_SERVICO)
      references SERVICO (NU_SERVICO) on delete restrict on update restrict;

alter table preco add constraint fk_preco_tabela_preco foreign key (tbpc_id)
      references tabela_precos (tbpc_id) on delete restrict on update restrict;
      
alter table SERVICO add serv_tx_descricao_tabela_precos varchar(200) null;

insert into ACESSO (TIPO_ACESSO  ,NOME_ACESSO  ,CONST_ACESSO) VALUES ( '006.001' ,'Cobran&ccedil;a'  ,'tp_Cobranca' );
insert into ACESSO_PERFIL (CD_PERFIL,CD_ACESSO) VALUES (1, (select max(CD_ACESSO) from ACESSO));

alter table tabela_precos add tbpc_tx_observacao text;
alter table solicita_visto 
  add   soli_vl_cobrado      decimal(10,2),
  add   soli_tx_obs_cobranca text,
  add   soli_dt_liberacao_cobranca datetime,
  add   soli_dt_cobranca     datetime,
  add   cd_usuario_liberacao_cobranca int,
  add   cd_usuario_cobranca  int,
  add   NU_EMPRESA_COBRANCA int ,
  add   NU_EMBARCACAO_PROJETO_COBRANCA int,
  add   tbpc_id int
  
  ;
alter table EMPRESA add tbpc_id int;
alter table EMPRESA add constraint fk_empresa_tabela_precos foreign key (tbpc_id)
      references tabela_precos (tbpc_id) on delete restrict on update restrict;
alter table solicita_visto add constraint fk_solicita_visto_tabela_precos foreign key (tbpc_id)
      references tabela_precos (tbpc_id) on delete restrict on update restrict;
ALTER TABLE solicita_visto
  ADD INDEX IX_COBRANCA (NU_EMPRESA_COBRANCA, NU_EMBARCACAO_PROJETO_COBRANCA, stco_id);

