-- *===========================================
-- * Empresas
-- *===========================================
CREATE TABLE empresa (
    NU_EMPRESA int(10) unsigned NOT NULL comment 'Chave unica da empresa',
    NO_RAZAO_SOCIAL varchar(150) DEFAULT NULL comment 'Razao social',
    NU_CNPJ varchar(20) DEFAULT NULL comment 'CNPJ',
    PRIMARY KEY (NU_EMPRESA)
)  ENGINE=InnoDB DEFAULT CHARSET=latin1
;
-- *===========================================
-- * Embarcacoes/projetos
-- *===========================================
CREATE TABLE embarcacao_projeto (
    ID_EMBARCACAO_PROJETO int(10) unsigned NOT NULL comment 'Chave unica da embarcacao/projeto',
    NU_EMPRESA int(11) NOT NULL comment 'Empresa do projeto',
    NU_EMBARCACAO_PROJETO int(10) unsigned NOT NULL comment 'Numero interno do projeto',
    NO_EMBARCACAO_PROJETO varchar(50) DEFAULT NULL comment 'Nome do projeto',
    PRIMARY KEY (ID_EMBARCACAO_PROJETO)
)  ENGINE=InnoDB DEFAULT CHARSET=latin1
;

-- *===========================================
-- * Servicos
-- *===========================================
CREATE TABLE servico (
    NU_SERVICO int(11) NOT NULL comment 'Chave unica do servico',
    NO_SERVICO varchar(200) DEFAULT NULL comment 'Nome do servico',
    PRIMARY KEY (NU_SERVICO)
)  
ENGINE=InnoDB 
DEFAULT CHARSET=latin1 
comment 'Todos os servicos que podem ser prestados atraves de ordens de servico'
;

-- *===========================================
-- * Candidatos
-- *===========================================
CREATE TABLE candidato (
    NU_CANDIDATO int(11) NOT NULL comment 'Chave unica do candidato',
    NOME_COMPLETO varchar(600) DEFAULT NULL comment 'Nome do candidato',
    NO_PAI varchar(200) DEFAULT NULL comment 'Nome do pai',
    NO_MAE varchar(200) DEFAULT NULL comment 'Nome da mae',
    CO_SEXO char(1) DEFAULT NULL comment 'Sexo: m ou f',
    DT_NASCIMENTO date DEFAULT NULL comment 'Data de nascimento ',
    DT_CADASTRAMENTO date NOT NULL comment 'Data em que foi cadastrado',
    DT_ULT_ALTERACAO timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP comment 'Data ultima alteracao',
    CO_USU_ULT_ALTERACAO int(10) unsigned NOT NULL comment 'Usuario resp pela ultima alteracao',
    NU_EMPRESA int(11) DEFAULT NULL comment 'Chave da empresa',
    NU_EMBARCACAO_PROJETO int(11) DEFAULT NULL comment 'Chave da embarcacao/projeto',
    PRIMARY KEY (NU_CANDIDATO)
)  ENGINE=InnoDB DEFAULT CHARSET=latin1
;

-- *===========================================
-- * Ordem de servico (solicitacoes de visto e outros servicos)
-- *===========================================
CREATE TABLE solicita_visto (
    id_solicita_visto int(10) unsigned NOT NULL comment 'Chave unica das ordens de servico',
    nu_solicitacao int(11) NOT NULL comment 'Numero da ordem de servico',
    nu_empresa int(11) NOT NULL comment 'Empresa da ordem de servico',
    id_embarcacao_projeto int(11) DEFAULT NULL comment 'Embarcacao projeto da ordem de servico',
    dt_cadastro date DEFAULT NULL comment 'Data em que a ordem de servico foi cadastrada',
    NU_SERVICO int(11) DEFAULT NULL comment 'Servico executado pela ordem de servico',
    de_observacao text comment 'Observações adicionais',
    PRIMARY KEY (id_solicita_visto)
) 
ENGINE=InnoDB 
DEFAULT CHARSET=latin1
;

