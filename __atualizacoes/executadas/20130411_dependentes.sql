ALTER TABLE dependentes MODIFY NU_DEPENDENTE INT NULL;
ALTER TABLE dependentes DROP PRIMARY KEY;
ALTER TABLE dependentes ADD depe_id int(10) unsigned not null primary key auto_increment;

-- Adicionar chave na embarcacao projeto

-- Adicionar pai e mae 
ALTER TABLE candidato ADD co_pais_nacionalidade_pai int null;
ALTER TABLE candidato ADD co_pais_nacionalidade_mae int null;
ALTER TABLE candidato ADD cand_tx_residencia_estado varchar(300);

ALTER TABLE candidato_tmp ADD co_pais_nacionalidade_pai int null;
ALTER TABLE candidato_tmp ADD co_pais_nacionalidade_mae int null;
ALTER TABLE candidato_tmp ADD cand_tx_residencia_estado varchar(300);

ALTER TABLE dr.candidato ADD co_pais_nacionalidade_pai int null;
ALTER TABLE dr.candidato ADD co_pais_nacionalidade_mae int null;
ALTER TABLE dr.candidato ADD cand_tx_residencia_estado varchar(300);

ALTER TABLE dr.candidato ADD NO_RAZAO_SOCIAL varchar(500);
ALTER TABLE dr.candidato ADD NO_RAZAO_SOCIAL varchar(500);

ALTER TABLE dr.candidato ADD CO_LOCAL_EMBARCACAO_PROJETO int(11);

CREATE TABLE dr.arquivos (
  NU_SEQUENCIAL int(11) NOT NULL AUTO_INCREMENT,
  NU_CANDIDATO int(11) DEFAULT NULL,
  NO_ARQUIVO varchar(255)  NULL,
  NO_ARQ_ORIGINAL varchar(255)  NULL,
  TP_ARQUIVO tinyint(3) NOT NULL DEFAULT '0',
  DT_INCLUSAO date NOT NULL,
  PRIMARY KEY (NU_SEQUENCIAL)
) ENGINE=InnoDB DEFAULT CHARSET=latin1