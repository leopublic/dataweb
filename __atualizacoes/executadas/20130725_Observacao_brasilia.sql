alter table solicita_visto add de_observacao_bsb text null;
alter table servico add serv_fl_envio_bsb tinyint null default 0;
update servico set serv_fl_envio_bsb = 1 where id_tipo_acompanhamento = 1;
update servico set serv_fl_envio_bsb = 1 where id_tipo_acompanhamento = 3;
update servico set serv_fl_envio_bsb = 1 where nu_servico = 27;
