insert into TIPO_ACOMPANHAMENTO (
  NO_TIPO_ACOMPANHAMENTO
  ,CO_TIPO_ACOMPANHAMENTO
  ,FL_MULTI_CANDIDATOS
) VALUES (
   'Coleta CIE' -- NO_TIPO_ACOMPANHAMENTO
  ,'COLC' -- CO_TIPO_ACOMPANHAMENTO
  ,0 -- FL_MULTI_CANDIDATOS
)
;
update servico set id_tipo_acompanhamento = 12 where nu_servico = 22;

drop table if exists processo_coleta_cie;

/*==============================================================*/
/* Table: processo_coleta_cie                                   */
/*==============================================================*/
create table processo_coleta_cie
(
   codigo               int not null auto_increment,
   codigo_processo_mte  int(11),
   cd_candidato         int,
   id_solicita_visto    int(10),
   nu_rne               varchar(50),
   dt_expedicao         datetime,
   dt_validade          datetime,
   dt_atendimento       datetime,
   dt_cad               datetime,
   dt_ult               datetime,
   cd_usuario           int,
   observacao           varchar(255),
   nu_servico           int,
   cd_usuario_ult       int,
   log_tx_controler     varchar(200),
   log_tx_metodo        varchar(200),
   primary key (codigo)
) engine=InnoDB;

alter table processo_coleta_cie add constraint fk_processo_coleta_cie_candidato foreign key (cd_candidato)
      references CANDIDATO (NU_CANDIDATO) on delete restrict on update restrict;

alter table processo_coleta_cie add constraint fk_processo_coleta_cie_solicita_visto foreign key (id_solicita_visto)
      references solicita_visto (id_solicita_visto) on delete restrict on update restrict;

alter table processo_coleta_cie add constraint FK_REFERENCE_53 foreign key (codigo_processo_mte)
      references processo_mte (codigo) on delete restrict on update restrict;

insert into processo_coleta_cie (codigo_processo_mte, id_solicita_visto, nu_rne, dt_atendimento, dt_validade, nu_servico, observacao) select codigo_processo_mte, p.id_solicita_visto, nu_protocolo, dt_requerimento, dt_prazo_estada, p.nu_servico, sv.de_observacao) from processo_regcie p , solicita_visto sv where p.nu_servico = 22 and sv.nu_servico=22 and sv.id_solicita_visto = p.id_solicita_visto;