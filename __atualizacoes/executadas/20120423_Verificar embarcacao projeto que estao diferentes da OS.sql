select s.ID_SOLICITA_VISTO, s.nu_solicitacao, s.dt_cadastro, s.dt_solicitacao, s.nu_empresa, s.NU_EMBARCACAO_PROJETO, AC.NU_EMPRESA, AC.NU_EMBARCACAO_PROJETO
from solicita_visto s, AUTORIZACAO_CANDIDATO AC
where AC.id_solicita_visto = s.id_solicita_visto
and s.NU_EMBARCACAO_PROJETO is null
and AC.NU_EMBARCACAO_PROJETO is not null;
;
update solicita_visto set NU_SERVICO = null where NU_SERVICO ;

select * from SERVICO;

select E.NO_RAZAO_SOCIAL, s.ID_SOLICITA_VISTO, s.nu_solicitacao, s.dt_cadastro, s.dt_solicitacao, s.NU_SERVICO, SVC.NO_SERVICO_RESUMIDO, s.nu_empresa, s.NU_EMBARCACAO_PROJETO, EP1.NO_EMBARCACAO_PROJETO, AC.NU_EMPRESA, AC.NU_EMBARCACAO_PROJETO, EP2.NO_EMBARCACAO_PROJETO
from solicita_visto s 
  join AUTORIZACAO_CANDIDATO AC on AC.id_solicita_visto = s.id_solicita_visto
  join EMPRESA E on E.NU_EMPRESA = s.nu_empresa
  left join EMBARCACAO_PROJETO EP1 on EP1.NU_EMPRESA = s.nu_empresa and EP1.NU_EMBARCACAO_PROJETO = s.NU_EMBARCACAO_PROJETO
  left join EMBARCACAO_PROJETO EP2 on EP2.NU_EMPRESA = AC.NU_EMPRESA and EP2.NU_EMBARCACAO_PROJETO = AC.NU_EMBARCACAO_PROJETO
  left join SERVICO SVC on SVC.NU_SERVICO = s.NU_SERVICO
where 
 s.NU_EMBARCACAO_PROJETO <> AC.NU_EMBARCACAO_PROJETO
-- and SVC.ID_TIPO_ACOMPANHAMENTO is not null
 and s.NU_SERVICO is not  null
order by NO_RAZAO_SOCIAL, s.nu_solicitacao
;

select s.ID_SOLICITA_VISTO, s.nu_solicitacao, s.dt_cadastro, s.dt_solicitacao, s.nu_empresa, s.NU_EMBARCACAO_PROJETO, AC.NU_EMPRESA, AC.NU_EMBARCACAO_PROJETO
from solicita_visto s, AUTORIZACAO_CANDIDATO AC
where AC.id_solicita_visto = s.id_solicita_visto
and s.NU_EMBARCACAO_PROJETO is not null
and AC.NU_EMBARCACAO_PROJETO is not null
and s.NU_EMBARCACAO_PROJETO <> AC.NU_EMBARCACAO_PROJETO;

select AC.id_solicita_visto, AC.NU_EMPRESA, AC.NU_EMBARCACAO_PROJETO, count(*)
from solicita_visto s, AUTORIZACAO_CANDIDATO AC
where AC.id_solicita_visto = s.id_solicita_visto
and s.NU_EMBARCACAO_PROJETO is not null
and AC.NU_EMBARCACAO_PROJETO is not null
and s.NU_EMBARCACAO_PROJETO <> AC.NU_EMBARCACAO_PROJETO
group by AC.id_solicita_visto, AC.NU_EMPRESA, AC.NU_EMBARCACAO_PROJETO
having count(*) > 1
;

select * from EMBARCACAO_PROJETO where NU_EMPRESA = 12;