select concat(pro.codigo,',') -- ,sv.id_solicita_visto, sv.nu_solicitacao, sv.NU_SERVICO, mte.cd_candidato, pro.nu_protocolo, pro.dt_prazo_pret, date_format(pro.dt_prazo_pret, '%d/%m/%Y'), mte.prazo_solicitado
from solicita_visto sv
, processo_mte mte 
, processo_prorrog pro
where sv.id_solicita_visto = mte.id_solicita_visto
and sv.id_solicita_visto = pro.id_solicita_visto
and mte.cd_candidato = pro.cd_candidato
and (date_format(pro.dt_prazo_pret, '%d/%m/%Y') = mte.prazo_solicitado or date_format(pro.dt_prazo_pret, '%d/%m/%Y') = '00/00/0000')
and (pro.nu_protocolo is null or trim(pro.nu_protocolo) = '' )
and (pro.fl_vazio = 0 or pro.fl_vazio is null)
and (mte.fl_vazio = 0 or mte.fl_vazio is null)