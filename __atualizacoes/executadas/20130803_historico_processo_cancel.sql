CREATE TABLE historico_processo_cancel (
  hcan_id int(10) unsigned NOT NULL AUTO_INCREMENT,
  hcan_dt_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  codigo int(11) DEFAULT NULL,
  teve_id int(11) DEFAULT NULL,
  cd_usuario int(10) unsigned DEFAULT NULL,
  hcan_tx_observacao text,
  PRIMARY KEY (hcan_id)
) ENGINE=InnoDB