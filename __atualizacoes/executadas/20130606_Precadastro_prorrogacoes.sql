CREATE  TABLE IF NOT EXISTS `historico_processo_prorrog` (
  `hpro_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `hpro_dt_evento` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `codigo` INT NULL ,
  `teve_id` INT NULL ,
  `cd_usuario` INT UNSIGNED NULL ,
  `hpro_tx_observacao` TEXT NULL ,
  PRIMARY KEY (`hpro_id`) )
ENGINE = InnoDB
;

alter table processo_prorrog 
	add nu_pre_cadastro varchar(40) null
,	add dt_pre_cadastro datetime null
,	add cd_usuario_pre_cadastro int(11) unsigned null	
;

insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, nu_ordem, nu_ordem_painel, bo_readonly, bo_exibir, fl_link_mte, fl_link_mj, fl_largura_dupla)
values (7, 6, 'N�mero do pr�-cadastro', 'nu_pre_cadastro', 0, 0, 5, 0, 0, 1, 0, 0, 0);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, nu_ordem, nu_ordem_painel, bo_readonly, bo_exibir, fl_link_mte, fl_link_mj, fl_largura_dupla)
values (7, 2, 'Data do pr�-cadastro', 'dt_pre_cadastro', 0, 0, 7, 0, 0, 1, 0, 0, 0);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, nu_ordem, nu_ordem_painel, bo_readonly, bo_exibir, fl_link_mte, fl_link_mj, fl_largura_dupla)
values (20, 6, 'N�mero do pr�-cadastro', 'nu_pre_cadastro', 0, 0, 5, 0, 1, 1, 0, 0, 0);
insert into campo(id_edicao, id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, nu_ordem, nu_ordem_painel, bo_readonly, bo_exibir, fl_link_mte, fl_link_mj, fl_largura_dupla)
values (20, 2, 'Data do pr�-cadastro', 'dt_pre_cadastro', 0, 0, 7, 0, 1, 1, 0, 0, 0);

ALTER TABLE solicita_visto 
ADD INDEX ix_dt_solicitacao (dt_solicitacao DESC) 
;
drop view if exists vprocesso_prorrog;
CREATE VIEW vprocesso_prorrog AS 
select processo_prorrog.codigo 
,processo_prorrog.cd_candidato 
,processo_prorrog.cd_solicitacao 
,processo_prorrog.nu_protocolo 
,date_format(processo_prorrog.dt_requerimento,'%d/%m/%Y') AS dt_requerimento
,date_format(processo_prorrog.dt_validade,'%d/%m/%Y') AS dt_validade
,date_format(processo_prorrog.dt_prazo_pret,'%d/%m/%Y') AS dt_prazo_pret
,processo_prorrog.observacao 
,processo_prorrog.dt_cad 
,processo_prorrog.dt_ult 
,processo_prorrog.id_solicita_visto 
,processo_prorrog.fl_vazio
,processo_prorrog.fl_processo_atual
,processo_prorrog.codigo_processo_mte
,processo_prorrog.codigo_regcie
,date_format(processo_prorrog.dt_publicacao_dou,'%d/%m/%Y') AS dt_publicacao_dou
,processo_prorrog.ID_STATUS_CONCLUSAO
,processo_prorrog.nu_servico
,processo_prorrog.no_classe
,processo_prorrog.no_metodo
,processo_prorrog.nu_pre_cadastro
,processo_prorrog.dt_pre_cadastro as dt_pre_cadastro_orig
,date_format(processo_prorrog.dt_pre_cadastro,'%d/%m/%Y') AS dt_pre_cadastro
,date_format(processo_prorrog.dt_envio_bsb,'%d/%m/%Y') AS dt_envio_bsb
,SC.NO_STATUS_CONCLUSAO
,S.NO_SERVICO
,S.NO_SERVICO_RESUMIDO
,sv.ID_STATUS_SOL
,sv.nu_solicitacao
,u.nome nome_pre_cadastro
from processo_prorrog
left join STATUS_CONCLUSAO SC on SC.ID_STATUS_CONCLUSAO = processo_prorrog.ID_STATUS_CONCLUSAO
left join SERVICO S on S.NU_SERVICO = processo_prorrog.nu_servico
left join solicita_visto sv ON sv.id_solicita_visto = processo_prorrog.id_solicita_visto
left join usuarios u on u.cd_usuario = processo_prorrog.cd_usuario_pre_cadastro
;

