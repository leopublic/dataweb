drop table if exists log_modificacoes;

/*==============================================================*/
/* Table: log_modificacoes                                      */
/*==============================================================*/
create table log_modificacoes
(
   logm_id              int not null auto_increment,
   logm_data            datetime,
   logm_descricao       text,
   logm_titulo          varchar(500),
   primary key (logm_id)
);