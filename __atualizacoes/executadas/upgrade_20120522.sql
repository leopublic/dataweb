CREATE TABLE configuracoes (
 conf_id int(11) NOT NULL AUTO_INCREMENT,
 cfgg_id int(11) DEFAULT NULL,
 conf_tx_codigo varchar(300) DEFAULT NULL,
 conf_tx_nome varchar(300) DEFAULT NULL,
 conf_tx_valor varchar(1000) DEFAULT NULL,
 KEY fk_configuracoes_grupo_configuracoes ( cfgg_id ),
 PRIMARY KEY  ( conf_id )
)
ENGINE = InnoDB;

CREATE TABLE grupo_configuracoes (
 cfgg_id int(11) NOT NULL AUTO_INCREMENT,
 cfgg_tx_nome varchar(500) DEFAULT NULL,
 PRIMARY KEY  ( cfgg_id )
)
ENGINE = InnoDB;

alter table resumo_cobranca add nu_empresa_requerente int(11) DEFAULT NULL;
alter table resumo_cobranca add NU_EMBARCACAO_PROJETO_COBRANCA int(11) DEFAULT NULL;

update FUNCAO_CARGO set NO_FUNCAO_EM_INGLES= NO_FUNCAO where NO_FUNCAO_EM_INGLES is null or NO_FUNCAO_EM_INGLES = '';
update PROFISSAO set NO_PROFISSAO_EM_INGLES = NO_PROFISSAO where NO_PROFISSAO_EM_INGLES is null or NO_PROFISSAO_EM_INGLES ='';
alter table TIPO_ARQUIVO add NO_TIPO_ARQUIVO_EM_INGLES varchar(200);
update TIPO_ARQUIVO set NO_TIPO_ARQUIVO_EM_INGLES = NO_TIPO_ARQUIVO ;
alter table candidato_tmp add FL_EDICAO_CONCLUIDA tinyint default 1;
