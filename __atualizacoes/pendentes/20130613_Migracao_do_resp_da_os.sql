alter table solicita_visto
	add cd_usuario_responsavel int null
;
update solicita_visto, solicita_visto_acomp
	 set solicita_visto.cd_usuario_responsavel = solicita_visto_acomp.cd_tecnico
	 where solicita_visto.nu_solicitacao = solicita_visto_acomp.cd_solicitacao
;
