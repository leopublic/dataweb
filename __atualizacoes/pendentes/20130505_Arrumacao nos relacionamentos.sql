alter table autorizacao_candidato 
	modify id_solicita_visto int(10) unsigned null
;
alter table autorizacao_candidato
	drop index fk_autorizacao_candidato_solicita_visto
;
alter table autorizacao_candidato
	drop key IU_AUTORIZACAO_CANDIDATO
;
alter table autorizacao_candidato 
	ADD UNIQUE INDEX IU_AUTORIZACAO_CANDIDATO (id_solicita_visto ASC, NU_CANDIDATO ASC)
;
alter table autorizacao_candidato 
	ADD INDEX IX_NU_CANDIDATO (NU_CANDIDATO ASC, id_solicita_visto desc)
;

alter table autorizacao_candidato 
	ADD CONSTRAINT fk_autorizacao_candidato_solicita_visto
    FOREIGN KEY (id_solicita_visto )
    REFERENCES solicita_visto (id_solicita_visto )
    ON DELETE CASCADE
    ON UPDATE NO ACTION
;

alter table autorizacao_candidato 
  ADD CONSTRAINT fk_autorizacao_candidato_candidato
    FOREIGN KEY (nu_candidato )
    REFERENCES candidato (nu_candidato )
    ON DELETE CASCADE
    ON UPDATE NO ACTION
;

alter table empresa 
    modify nu_empresa int(11) unsigned not null auto_increment
;
alter table solicita_visto 
    modify nu_empresa int(11) unsigned null
;
alter table solicita_visto 
  ADD CONSTRAINT fk_solicita_visto_empresa
    FOREIGN KEY (nu_empresa )
    REFERENCES empresa (nu_empresa )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
;
