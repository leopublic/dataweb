use dataweb;
drop table if exists taxa;
drop table if exists perfiltaxa;
CREATE TABLE perfiltaxa (
  id_perfiltaxa int(11) NOT NULL AUTO_INCREMENT,
  descricao varchar(500) DEFAULT NULL,
  PRIMARY KEY (id_perfiltaxa)
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1
;

CREATE TABLE taxa (
  id_taxa int(11) NOT NULL AUTO_INCREMENT,
  id_perfiltaxa int(11) ,
  descricao varchar(500) DEFAULT NULL,
  valor_fixo decimal(10,2) DEFAULT NULL,
  KEY fk_perfiltaxa (`id_perfiltaxa`),
  CONSTRAINT fk_perfiltaxa FOREIGN KEY (id_perfiltaxa) REFERENCES perfiltaxa (id_perfiltaxa) ON DELETE NO ACTION ON UPDATE NO ACTION,
  PRIMARY KEY (id_taxa)
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1
;

CREATE TABLE taxapaga (
  id_taxapaga int(11) NOT NULL AUTO_INCREMENT,
  id_taxa int(11) ,
  id_solicita_visto int(11) unsigned ,
  nu_candidato int(11) ,
  data_taxa datetime DEFAULT NULL,
  valor decimal(10,2) DEFAULT NULL,
  id_usuario int(11) DEFAULT NULL,
  KEY fk_taxa (`id_taxa`),
  CONSTRAINT fk_taxa FOREIGN KEY (id_taxa) REFERENCES taxa (id_taxa) ON DELETE NO ACTION ON UPDATE NO ACTION,
  KEY fk_solicita_visto (`id_solicita_visto`),
  CONSTRAINT fk_solicita_visto FOREIGN KEY (id_solicita_visto) REFERENCES solicita_visto (id_solicita_visto) ON DELETE NO ACTION ON UPDATE NO ACTION,
  KEY fk_nu_candidato (nu_candidato),
  CONSTRAINT fk_nu_candidato FOREIGN KEY (nu_candidato) REFERENCES candidato (nu_candidato) ON DELETE NO ACTION ON UPDATE NO ACTION,
  PRIMARY KEY (id_taxapaga)
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1
;

alter table servico add id_perfiltaxa int(11) null;

