drop view if exists vhistorico_os_sequencial ;
create view vhistorico_os_sequencial as 
select 
	h1.id_solicita_visto
	, null hios_dt_evento
	, min(h1.hios_dt_evento) hios_dt_evento_proximo
from historico_os h1
group by h1.id_solicita_visto
union all
select 
	h1.id_solicita_visto
	, h1.hios_dt_evento hios_dt_evento
	, min(h2.hios_dt_evento) hios_dt_evento_proximo
from historico_os h1
left join historico_os h2
on h2.id_solicita_visto = h1.id_solicita_visto
and h2.hios_dt_evento > h1.hios_dt_evento
and h2.hios_id <> h1.hios_id
where h1.id_status_sol <> h2.id_status_sol
group by h1.id_solicita_visto, h1.hios_dt_evento
;

