drop view if exists vhistorico_os_status ;
create view vhistorico_os_status as 
select distinct historico.id_solicita_visto, historico.hios_dt_evento_proximo hios_dt_evento, hantes.id_status_sol id_status_sol_antes, hdepois.id_status_sol id_status_sol_depois
from vhistorico_os_sequencial historico
left join historico_os hantes on hantes.id_solicita_visto = historico.id_solicita_visto and hantes.hios_dt_evento = historico.hios_dt_evento
join historico_os hdepois on hdepois.id_solicita_visto = historico.id_solicita_visto and hdepois.hios_dt_evento = historico.hios_dt_evento_proximo
;

