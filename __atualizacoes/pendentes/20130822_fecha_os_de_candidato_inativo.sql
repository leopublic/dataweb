select id_solicita_visto, dt_solicitacao, id_status_sol 
from solicita_visto 
where id_status_sol <> 6 and id_solicita_visto in 
(select id_solicita_visto 
from autorizacao_candidato ac, candidato c 
where c.nu_candidato = ac.nu_candidato and fl_inativo = 1)
;
update solicita_visto, autorizacao_candidato, candidato  
set id_status_sol = 6 , de_observacao = concat(de_observacao, '<br/>(22/08/2013 - OS encerrada automaticamente por ser de candidato inativo')
where solicita_visto.id_status_sol <> 6 
and solicita_visto.id_solicita_visto = autorizacao_candidato.id_solicita_visto 
and autorizacao_candidato.nu_candidato = candidato.nu_candidato 
and candidato.fl_inativo = 1
;
-- At� aqui ok!

select id_solicita_visto, solicita_visto.nu_servico, id_status_sol 
from solicita_visto, servico  
where servico.nu_servico = solicita_visto.nu_servico 
and serv_fl_envio_bsb = 0 
and id_status_sol < 5 ;


update solicita_visto, servico 
set id_status_sol = 5, de_observacao = concat(de_observacao, '<br/>(22/08/2013 - Status da OS alterado para "protocolada" automaticamente pela OS ser de servi�o que n�o � enviado para BSB')
where solicita_visto.nu_servico = servico.nu_servico 
and solicita_visto.id_status_sol < 5 
and servico.serv_fl_envio_bsb = 0;
-- Adicionar OBS
select id_solicita_visto, solicita_visto.nu_servico, id_status_sol 
from solicita_visto, servico  
where servico.nu_servico = solicita_visto.nu_servico and serv_fl_revisao_analistas = 0 and  id_status_sol < 5 ;

update solicita_visto, servico 
set id_status_sol = 5, de_observacao = concat(de_observacao, '<br/>(22/08/2013 - Status da OS alterado para "protocolada" automaticamente pela OS ser de servi�o que n�o � revisado pelas analistas')
where solicita_visto.nu_servico = servico.nu_servico 
and solicita_visto.id_status_sol < 5 
and servico.serv_fl_revisao_analistas = 0;

alter table candidato add cand_fl_pit tinyint not null default 0;
