alter table TIPO_AUTORIZACAO add CO_TIPO_AUTORIZACAO_ANT char(2) null
;
update TIPO_AUTORIZACAO set co_tipo_autorizacao_ant = co_tipo_autorizacao
;
alter table tipo_autorizacao drop column co_tipo_autorizacao
;
alter table tipo_autorizacao add CO_TIPO_AUTORIZACAO int unsigned not null auto_increment primary key
;
alter table servico add CO_TIPO_AUTORIZACAO_ANT char(2) null
;
update servico set co_tipo_autorizacao_ant = co_tipo_autorizacao
;
alter table servico drop column co_tipo_autorizacao
;
alter table servico add co_tipo_autorizacao int unsigned null
;
update servico, tipo_autorizacao 
	set servico.co_tipo_autorizacao = tipo_autorizacao.co_tipo_autorizacao 
	where servico.co_tipo_autorizacao_ant = tipo_autorizacao.co_tipo_autorizacao_ant
;
 alter table reparticao_consular modify CO_REPARTICAO_CONSULAR int(11) unsigned not null auto_increment;
 