create table despesa(
  desp_id INT(11) unsigned NOT NULL AUTO_INCREMENT ,
  nu_servico INT(11) unsigned NULL ,
  id_solicita_visto INT(11) unsigned not NULL ,
  cd_usuario_cad INT(11) unsigned NOT NULL ,
  desp_tx_funcionario varchar(200) NULL ,
  desp_vl_valor decimal(12,2) ,
  desp_vl_qtd decimal(12,2) ,
  desp_dt_inicio datetime null,
  desp_dt_fim datetime null,
  desp_tx_tarefa varchar(2000),
  desp_tx_observacao varchar(2000),
  PRIMARY KEY (desp_id) )
ENGINE = InnoDB
;
alter table servico add serv_fl_timesheet tinyint not null default 0;
alter table servico add serv_fl_despesas_adicionais tinyint not null default 0;

