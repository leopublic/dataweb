drop table processo_generico;
create table processo_generico (
 codigo int(11) unsigned not null auto_increment
,cd_candidato int(11) unsigned not null
,id_solicita_visto int(11) unsigned null
,id_tipoprocesso int(11) unsigned not null
,nu_empresa int(11) unsigned null
,nu_embarcacao_projeto int(11) unsigned null
,codigo_processo_mte int(11) unsigned null
,nu_servico int(11) unsigned null
,dt_cad datetime not null
,dt_ult datetime null
,cd_usuario_cad int(11) unsigned  null
,cd_usuario_ult int(11) unsigned null 
,ordem int null
,date_1 date null
,date_2 date null
,date_3 date null
,date_4 date null
,date_5 date null
,date_6 date null
,string_1 varchar(500) null
,string_2 varchar(500) null
,string_3 varchar(500) null
,string_4 varchar(500) null
,string_5 varchar(500) null
,string_6 varchar(500) null
,integer_1 int null
,integer_2 int null
,integer_3 int null
,integer_4 int null
,integer_5 int null
,integer_6 int null
,observacao text
,primary key(codigo)
, index ix_id_solicita_visto (id_solicita_visto)
, index ix_cd_candidato (cd_candidato)
, index ix_codigo_processo_mte (codigo_processo_mte, ordem)
, index ix_ordem (ordem)
) engine=InnoDB
;
INSERT INTO tipo_acompanhamento
(ID_TIPO_ACOMPANHAMENTO,
NO_TIPO_ACOMPANHAMENTO,
CO_TIPO_ACOMPANHAMENTO,
FL_MULTI_CANDIDATOS)
VALUES
(13,
'Mudan�a de embarca��o',
'MEMB',
0);
update servico set id_tipo_acompanhamento = 13 where nu_servico = 31;