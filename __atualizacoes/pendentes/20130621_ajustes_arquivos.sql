-- Adicona chave na tabela de arquivos
alter table arquivos_formularios modify CD_ARQUIVO int(11) unsigned not null auto_increment;
-- Adiciona chave na tabela de procuracao
alter table procurador_empresa add proc_id int(11) unsigned primary key auto_increment;