drop table if exists tipo_envio;
create table tipo_envio(
 id_tipo_envio int(11) unsigned not null auto_increment
,descricao  varchar(200)
,fl_digital  tinyint
,primary key(id_tipo_envio)
) engine=InnoDB
;
insert into tipo_envio(descricao, fl_digital) values ('Bras�lia', 0), ('Digital', 1), ('Maca�', 0)
;


alter table solicita_visto add id_tipo_envio int(11) unsigned null;

update solicita_visto, processo_mte set id_tipo_envio = 1
where processo_mte.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_mte.dt_envio_bsb is not null;

update solicita_visto, processo_prorrog set id_tipo_envio = 1
where processo_prorrog.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_prorrog.dt_envio_bsb is not null;

update solicita_visto, processo_cancel set id_tipo_envio = 1
where processo_cancel.id_solicita_visto = solicita_visto.id_solicita_visto
and processo_cancel.dt_envio_bsb is not null;

