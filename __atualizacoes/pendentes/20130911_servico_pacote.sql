alter table servico drop column tppr_id;
alter table servico add serv_fl_pacote tinyint;

create table pacote_servico (
 nu_servico_pacote int(11) not null
,nu_servico_incluido int(11) not null
,pacs_fl_principal tinyint not null default 0
,primary key(nu_servico_pacote, nu_servico_incluido)
)
engine = InnoDB
;
-- At� aqui ok.

-- Essa tabela foi cancelada porque essa informa��o pode ficar na Ordem de servico que abre o pacote. Separado eu teria um esfo�o adicional para manter isso sincronizado.
-- Criarei uma classe independente para tratar essas informa��es da OS
-- create table pacote(
-- 	pact_id int(11) unsigned not null auto_increment primary key
-- ,	nu_candidato int(11) unsigned
-- ,   nu_servico int(11) unsigned
-- ,   nu_empresa int(11) unsigned
-- )
-- engine = InnoDB
-- ;

create table pacote_os (
	pact_id int(11) unsigned
,	id_solicita_visto int(11) not null
,primary key(pact_id, id_solicita_visto)
)
engine = InnoDB
;
alter table solicita_visto add id_solicita_visto_pacote int null;
alter table solicita_visto add nu_servico_pacote int null;

 delete from campo where id_campo in (532, 1004, 1015, 1006);

-- update campo set no_painel = null, nu_ordem_painel = null where id_edicao = 15;
--  Qual dos dois ou nenhum dos dois...?
-- insert into campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_readonly, bo_exibir) values(3, 'Pacote', 'id_solicita_visto_pacote', 0, 0, 0, 57, 15, 0, 0);
-- insert into campo(id_tipo_campo, no_label, no_campo_bd, bo_obrigatorio, bo_chave, id_tipo_combo, nu_ordem, id_edicao, bo_readonly, bo_exibir) values(3, 'Pacote', 'nu_servico_pacote', 0, 0, 0, 57, 15, 0, 0);
alter table solicita_visto add soli_tx_justificativa varchar(1000) null;
