alter table servico add no_servico_em_ingles varchar(500) null
, add no_servico_resumido_em_ingles varchar(500) null
;
alter table preco add prec_tx_descricao_tabela_precos_em_ingles text null;
;
alter table empresa 
		 add empr_fl_fatura_em_ingles tinyint not null default 0
		,add empr_fl_faturavel tinyint not null default 1
;