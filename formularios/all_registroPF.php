<?php
include("libForm.php");
require('fpdf.php');

class PDF extends FPDF  {
  function Header(){
    global $marg,$NM_LOGO_EMPRESA,$dirimg;
    if($NM_LOGO_EMPRESA == "teste_logo.jpg") {
       $this->Image($dirimg.$NM_LOGO_EMPRESA,$marg,8,33);
    }
  }
  function Footer() {
  }
}

$tpSolicitacao=$_POST['tpSolicitacao'];
$nomeForm = "Registro_PF";
$marg = 15;

if (! isset($_POST['id_solicita_visto']) || $_POST['id_solicita_visto'] == ''){
	print 'N�o foi poss�vel gerar o formul�rio pois a OS n�o foi informada.';
}
elseif (! isset($_POST['idCandidato']) || $_POST['idCandidato'] == ''){
	print 'N�o foi poss�vel gerar o formul�rio pois o candidato n�o foi informado.';
}
else{
	$ret = Inicial();
	if(strlen($ret)>0) {
		echo $ret;
	} 
	else {
		if($tpSolicitacao=="teste") {
			$NM_LOGO_EMPRESA = "teste_logo.jpg";
			$ret = Formulario();
			$ret->Output();
		}
		else {
			$NM_LOGO_EMPRESA = "";
			$id_solicita_visto = $_POST['id_solicita_visto'];
			$NU_CANDIDATO = $_POST['idCandidato'];
			$ID_TIPO_FORMULARIO= $_POST['ID_TIPO_FORMULARIO'];
	 	
			$local = RetornaDIRCandidato($id_solicita_visto, $NU_CANDIDATO);
			$id = RetornaIDformulario();
			$nomeArq = $nomeForm."_".$id.".pdf";
			$ret = GravaArquivoDBCand($id,$idEmpresa,$id_solicita_visto, $idCandidato,$nomeArq, $ID_TIPO_FORMULARIO);
			$ret = Formulario();
			$ret->Output();
			$ret->Output($local.$nomeArq,"F");
			$idCandidato = $_REQUEST['idCandidato'];
			$NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
			$idEmpresa = $_REQUEST['idEmpresa'];
			$cd_admin = $_REQUEST['cd_admin'];
			if(strlen(trim($usulogado))==0) { $usulogado=$cd_admin; }
			$gravado = "/arquivos/formularios/$idEmpresa/$idCandidato/".$nomeArq;
			$texto = "Gera��o do formul�rio de Registro na PF. <a href='$gravado'>(VER)</a>";
			insereEvento($idEmpresa,$idCandidato,$NU_SOLICITACAO,$texto);
		}
	}
}


function Formulario() {
  global $pdf;
  global $NO_PRIMEIRO_NOME,$NO_NOME_MEIO,$NO_ULTIMO_NOME,$nmPaisNacional,$CO_SEXO,$NO_PAI,$NO_MAE,$CO_ESTADO_CIVIL,$NO_LOCAL_NASCIMENTO;
  global $DT_NASCIMENTO,$nmFuncaoCandidato,$NU_CPF,$nmNacional,$NU_CBO,$TE_OBSERVACOES_PROCESSOS;
  global $NO_RAZAO_SOCIAL,$NO_ENDERECO_EMP,$NO_COMPLEMENTO_EMP,$NO_BAIRRO_EMP,$NO_MUNICIPIO_EMP,$CO_UF_EMP,$NU_CEP_EMP,$NU_DDD_EMP,$NU_TELEFONE_EMP,$NO_EMAIL_EMPRESA;
  global $NU_VISTO,$DT_VALIDADE_VISTO,$DT_EMISSAO_VISTO,$NO_LOCAL_EMISSAO_VISTO,$CO_NACIONALIDADE_VISTO,$nmPaisConcesaoVisto,$CO_CLASSIFICACAO_VISTO;
  global $NO_LOCAL_ENTRADA,$NO_UF_ENTRADA,$DT_ENTRADA,$NU_TRANSPORTE_ENTRADA,$NU_PASSAPORTE,$nmPaisPassaporte;
  global $NO_ENDERECO_RESIDENCIA,$NO_CIDADE_RESIDENCIA,$CO_PAIS_RESIDENCIA,$NU_TELEFONE_CANDIDATO,$NU_SOLICITACAO;
  global $bairroResidencia,$cepResidencia,$ufResidencia;
  global $CO_PAIS_PF_NACIONALIDADE, $CO_PAIS_PF_EMISSOR_PASSAPORTE, $CO_PAIS_PF_VISTO;
  
  //$CO_PAIS_PF_NACIONALIDADE = '1234';
  //$CO_PAIS_PF_EMISSOR_PASSAPORTE = '4321';
  //$CO_PAIS_PF_EMISSAO_VISTO = '5678';
  
  $NU_TELEFONE_EMPRESA = "";
  if(strlen($NU_DDD_EMP)>0) {
    $NU_TELEFONE_EMPRESA = "($NU_DDD_EMP) ";
  }
  if(strlen($NU_TELEFONE_EMP)>0) {
    $NU_TELEFONE_EMPRESA = $NU_TELEFONE_EMPRESA.$NU_TELEFONE_EMP;
  }
  if(strlen($NU_CPF)==0) { $NU_CPF="N�o h�"; }

$endEntrega = "R";   # Ver com a Ana

$NU_RNE = ""; # Conversa com Ana Lucia
$condicoesEspeciais = ""; # Vazio - Conversa com Ana Lucia
$TE_OBSERVACOES_PROCESSOS = "Observacoes";
$amparoLegal = ""; # Vazio - Conversa com Ana Lucia
$tipoDocViagem = "1"; # Passaporte - fixo - Conversa com Ana Lucia

$nmTipoPedido = "Registro Tempor�rio V";
if($CO_CLASSIFICACAO_VISTO=="1") { $nmTipoPedido = "Permanente"; }

  $pag[0]=216;
  $pag[1]=330;
  $pdf=new PDF('P','mm',$pag);
  $pdf->SetMargins(0,0,0);
  $pdf->AddPage();
  $pdf->SetFont('Arial','',10);

  $cabeca = $NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME."\nPai: ".$NO_PAI."\nMae: ".$NO_MAE;
  $pdf->SetXY(60,0);//Nome
  $pdf->MultiCell(80,4,$cabeca,0,'L');

  $pdf->SetXY(60,19);//Pais
  $pdf->MultiCell(80,4,$nmPaisNacional,0,'L');

  $pdf->SetXY(60,27);//Tipo de Pedido
  $pdf->MultiCell(80,4,$nmTipoPedido,0,'L');

  if($CO_SEXO == "M") {
    $pdf->SetXY(176,27);//sexo masculino
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_SEXO == "F") {
    $pdf->SetXY(196,27);
    $pdf->MultiCell(5,4,"x",0,'L');
  }

  $pdf->SetXY(61,54);
  $pdf->MultiCell(25,4,$NU_RNE,0,'L');

  $pdf->SetXY(8,62);//Nome Atual, Completo
  $pdf->MultiCell(200,4,$NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME,0,'L');

  $pdf->SetXY(8,78);//Nome anterior Completo.
  $pdf->MultiCell(30,4,"N�o h�",0,'L');

  $pdf->SetXY(8,87);//Nome do Pai
  $pdf->MultiCell(80,4,$NO_PAI,0,'L');

  $pdf->SetXY(8,95);//Nome da m�e
  $pdf->MultiCell(80,4,$NO_MAE,0,'L');

 if($CO_SEXO == "M") {
    $pdf->SetXY(167,87);//Masculino
    $pdf->MultiCell(5,4,"x",0,'L');
 } elseif($CO_SEXO == "F") {
    $pdf->SetXY(167,96);
    $pdf->MultiCell(5,4,"x",0,'L');
 }

  $pdf->SetXY(194,94);//data de nascimento
  $pdf->MultiCell(25,4,$DT_NASCIMENTO,0,'L');

  if($CO_ESTADO_CIVIL=="1") {
    $pdf->SetXY(10,105);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="2") {
    $pdf->SetXY(40,105);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="3") {
    $pdf->SetXY(67,105);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="4") {
    $pdf->SetXY(10,115);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="5") {
    $pdf->SetXY(40,115);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="6") {
    $pdf->SetXY(67,115);
    $pdf->MultiCell(5,4,"x",0,'L');
  }
  
  
  $pdf->SetXY(136,106);//Cidade de Nascimento
  $pdf->MultiCell(75,4,$NO_LOCAL_NASCIMENTO,0,'L');

  $pdf->SetXY(9,125);//Nacionalidade
  $pdf->MultiCell(75,4,$nmPaisNacional,0,'L');

  $pdf->SetXY(93,125);//C�digo PF Pais de Nascimento
  $pdf->MultiCell(75,4,$CO_PAIS_PF_NACIONALIDADE,0,'L');
  
  $pdf->SetXY(112,125);//Pais de Nascimento
  $pdf->MultiCell(75,4,$nmPaisNacional,0,'L');

  $pdf->SetXY(200,125);//C�digo PF Pais de Nascimento
  $pdf->MultiCell(75,4,$CO_PAIS_PF_NACIONALIDADE,0,'L');
  
  $pdf->SetXY(7,143);//Ocupa��o principal
  $pdf->MultiCell(100,4,$nmFuncaoCandidato,0,'L');

  $pdf->SetXY(115,149);//C�digo
  $pdf->MultiCell(75,4,$NU_CBO,0,'L');

  $pdf->SetXY(140,148);
  $pdf->MultiCell(75,4,$NU_CPF,0,'L');

  $pdf->SetXY(5,255);
  $pdf->MultiCell(25,4,$NU_RNE,0,'L');

  $pdf->SetXY(5,263);
  $pdf->MultiCell(140,4,$NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME,0,'L');

  $pdf->SetXY(5,276);//Data de Nascimento no final do formul�rio
  $pdf->MultiCell(40,4,$DT_NASCIMENTO,0,'L');

  $NO_LOCAL_NASCIMENTO = substr($NO_LOCAL_NASCIMENTO,0,30);
  $pdf->SetXY(40,276);
  $pdf->MultiCell(55,4,$NO_LOCAL_NASCIMENTO,0,'L');

  $pdf->SetXY(94,276);
  $pdf->MultiCell(55,4,$nmNacional,0,'L');

  $pdf->AddPage();

  $pdf->SetXY(13,57);
  $pdf->MultiCell(75,4,$NO_LOCAL_ENTRADA,0,'L');

  $pdf->SetXY(78,69);
  $pdf->MultiCell(10,4,$NO_UF_ENTRADA,0,'L');

  $pdf->SetXY(93,57);
  $pdf->MultiCell(25,4,$DT_ENTRADA,0,'L');

if($NU_TRANSPORTE_ENTRADA=="1") {
  $pdf->SetXY(118,57);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($NU_TRANSPORTE_ENTRADA=="2") {
  $pdf->SetXY(140,57);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($NU_TRANSPORTE_ENTRADA=="3") {
  $pdf->SetXY(157,57);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($NU_TRANSPORTE_ENTRADA=="4") {
  $pdf->SetXY(176,57);
  $pdf->MultiCell(5,4,"x",0,'L');
}

  $pdf->SetXY(190,57);
  $pdf->MultiCell(20,4,$NU_VISTO,0,'L');

  $pdf->SetXY(13,55);
  $pdf->MultiCell(25,4,$DT_EMISSAO_VISTO,0,'L');

  $pdf->SetXY(34,55);
  $pdf->MultiCell(75,4,$NO_LOCAL_EMISSAO_VISTO,0,'L');

  $pdf->SetXY(114,55);
  $pdf->MultiCell(75,4,$nmPaisConcesaoVisto,0,'L');

  $pdf->SetXY(200,55);
  $pdf->MultiCell(75,4,$CO_PAIS_PF_EMISSAO_VISTO,0,'L');
  
  $pdf->SetXY(5,65);//Documento de Viagem
  $pdf->MultiCell(75,4,$NU_PASSAPORTE,0,'L');

  $pdf->SetXY(70,65);//Pais exoedidor do documento de viagem
  $pdf->MultiCell(120,4,$nmPaisPassaporte,0,'L');

  $pdf->SetXY(200,65); // Codigo do Pais exoedidor do documento de viagem
  $pdf->MultiCell(120,4,$CO_PAIS_PF_EMISSOR_PASSAPORTE,0,'L');
  
  $pdf->SetXY(105,89);
  $pdf->MultiCell(105,4,$amparoLegal,0,'L');

if($tipoDocViagem=="1") {//Tipo de documento da viagem
  $pdf->SetXY(3,82);//Passaporte
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($tipoDocViagem=="2") {
  $pdf->SetXY(32,82);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($tipoDocViagem=="3") {
  $pdf->SetXY(57,82);
  $pdf->MultiCell(5,4,"x",0,'L');
}

if($CO_CLASSIFICACAO_VISTO=="1") {
  $pdf->SetXY(10,96);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="2") {
  $pdf->SetXY(27,96);//Classificacao temporario
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="3") {
  $pdf->SetXY(56,96);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="4") {
  $pdf->SetXY(79,96);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="5") {
  $pdf->SetXY(10,105);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="6") {
  $pdf->SetXY(28,105);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="7") {
  $pdf->SetXY(45,105);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="8") {
  $pdf->SetXY(66,105);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="9") {
  $pdf->SetXY(86,105);
  $pdf->MultiCell(5,4,"x",0,'L');
}

  $pdf->SetXY(105,104);
  $pdf->MultiCell(100,4,$condicoesEspeciais,0,'L');

  $pdf->SetXY(4,121);//Logradouro
  $pdf->MultiCell(140,4,$NO_ENDERECO_RESIDENCIA,0,'L');

  $pdf->SetXY(155,119);//Telefone contato
  $pdf->MultiCell(50,4,$NU_TELEFONE_CANDIDATO,0,'L');

  $pdf->SetXY(0,130);//Bairro
  $pdf->MultiCell(50,4,$bairroResidencia,0,'L');

  $pdf->SetXY(58,134);//Cidade
  $pdf->MultiCell(105,4,$NO_CIDADE_RESIDENCIA,0,'L');

  $pdf->SetXY(168,132);//cep
  $pdf->MultiCell(30,4,$cepResidencia,0,'L');

  $pdf->SetXY(200,138);//UF
  $pdf->MultiCell(10,4,$ufResidencia,0,'L');

  $pdf->SetXY(8,146);//Raz�o Social
  $pdf->MultiCell(100,4,$NO_RAZAO_SOCIAL,0,'L');

  $pdf->SetXY(109,147);//N�mero Complemento
  $pdf->MultiCell(100,4,$NO_ENDERECO_EMP." ".$NO_COMPLEMENTO_EMP,0,'L');

  $pdf->SetXY(9,162);//Bairro Residencial
  $pdf->MultiCell(50,4,$NO_BAIRRO_EMP,0,'L');

  $pdf->SetXY(57,162);//Cidade Empresa
  $pdf->MultiCell(100,4,$NO_MUNICIPIO_EMP,0,'L');

  $pdf->SetXY(165,162);//Uf da Empresa
  $pdf->MultiCell(10,4,$CO_UF_EMP,0,'L');

  $pdf->SetXY(169,162);//Telefon da Empresa
  $pdf->MultiCell(35,4,$NU_TELEFONE_EMPRESA,0,'L');

if($endEntrega == "R") {//Endere�o de Entega da CIE
  $pdf->SetXY(1,172);//Residencial
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($endEntrega == "C") {
  $pdf->SetXY(58,172);//Comercial
  $pdf->MultiCell(5,4,"x",0,'L');
}

  $pdf->SetXY(52,172);//Cep da Empresa
  $pdf->MultiCell(30,4,$NU_CEP_EMP,0,'L');

  $pdf->SetXY(10,186);//Obsrva��es
  $pdf->MultiCell(200,4,$TE_OBSERVACOES_PROCESSOS,0,'L');


  return $pdf;
}

?>
