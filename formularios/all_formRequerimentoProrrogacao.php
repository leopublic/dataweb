<?php
include("libForm.php");
require('fpdf.php');

class PDF extends FPDF  {
  function Header(){
    global $marg,$NM_LOGO_EMPRESA,$dirimg,$LOGO_DIR;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    if($LOGO_DIR > 0) {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,170,2,33);
    } else {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,$marg,2,33);
    }
    $this->SetFont('Times','',8);  #  Times 8
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,3,$pagina,0,0,'C');
    //Line break
    $this->Ln(20);
  }
  function Footer() {
    global $NO_RAZAO_SOCIAL;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    $lin1 = "O presente material � titularizado com exclusividade pela $NO_RAZAO_SOCIAL, e qualquer reprodu��o, utiliza��o ou divulga��o do mesmo,";
    $lin2 = "sem expressa autoriza��o da titular, importa em ato il�cito, nos termos da legisla��o pertinente, atrav�s da qual ser�o imputadas as responsabilidades cab�veis.";
    //Position at 1.5 cm from bottom
    $this->SetY(-15);
    $this->SetFont('Times','',8);  #  Times 8
    $w1=$this->GetStringWidth($lin1)+6;
    $this->SetX((210-$w1)/2);
    $this->Cell($w1,0,$lin1,0,0,'C');
    $w2=$this->GetStringWidth($lin2)+6;
    $this->SetX((210-$w2)/2);
    $this->Cell($w2,7,$lin2,0,0,'C');
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,15,$pagina,0,0,'C');
  }
}

$ret = Inicial();
$LOGO_DIR = 0;
$aux_raz = strtoupper($NO_RAZAO_SOCIAL);
if( (!(strstr($aux_raz,'NOBLE') === FALSE)) || (!(strstr($aux_raz,'FUGRO') === FALSE)) ) {
   $LOGO_DIR = 1;
}

$tpSolicitacao=$_POST['tpSolicitacao'];
$nomeForm = "RequerimentoProrrogacao_all";
$marg = 15;
if(strlen($ret)==0) {
  $ret = ValidaDadosFormulario($nomeForm);
}
if(strlen($ret)>0) {
  echo $ret;
} else {
 if($tpSolicitacao=="teste") {
  $NM_LOGO_EMPRESA = "teste_logo.jpg";
  $ret = Formulario();
  $ret->Output();
 } else {
   $id_solicita_visto = $_POST['id_solicita_visto'];
  $NU_CANDIDATO = $_POST['idCandidato'];
  $ID_TIPO_FORMULARIO= $_POST['ID_TIPO_FORMULARIO'];
 	
  $local = RetornaDIRCandidato($id_solicita_visto, $NU_CANDIDATO);
  $id = RetornaIDformulario();
  $nomeArq = $nomeForm."_".$id.".pdf";
  $ret = GravaArquivoDBCand($id,$idEmpresa,$id_solicita_visto, $idCandidato,$nomeArq, $ID_TIPO_FORMULARIO);
    $ret = Formulario();
  $ret->Output();
  $ret->Output($local.$nomeArq,"F");
  $idCandidato = $_REQUEST['idCandidato'];
  $NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
  $idEmpresa = $_REQUEST['idEmpresa'];
  $cd_admin = $_REQUEST['cd_admin'];
  if(strlen(trim($usulogado))==0) { $usulogado=$cd_admin; }
  $gravado = "/arquivos/formularios/$idEmpresa/$idCandidato/".$nomeArq;
  $texto = "Gera��o do formul�rio de Requerimento de Prorroga��o. <a href='$gravado'>(VER)</a>";
  insereEvento($idEmpresa,$idCandidato,$NU_SOLICITACAO,$texto);
 }
}

exit;

function Formulario() {
  global $pdf,$marg,$NM_LOGO_EMPRESA,$NO_RAZAO_SOCIAL,$dia,$mes,$mess,$ano;
  global $NO_PROCURADOR,$nmCargoProc,$NO_EMISSOR_PROC,$NU_IDENT_PROC,$NU_CPF_PROC,$nmNacionalProc,$nmEstCivProc;
  global $NO_PROCURADOR2,$NO_EMAIL_PROC2,$nmCargoProc2,$NO_EMISSOR_PROC2,$NU_IDENT_PROC2,$NU_CPF_PROC2;
  global $NO_ENDERECO_EMP,$NO_COMPLEMENTO_EMP,$NO_BAIRRO_EMP,$NO_MUNICIPIO_EMP,$CO_UF_EMP,$NU_RNE;
  global $NO_EMBARCACAO_PROJETO,$nmPaisEmbarcacao,$DT_PRAZO_CONTRATO_PROJ,$NM_LOGO_EMPRESA;
  global $NO_PRIMEIRO_NOME,$NO_NOME_MEIO,$NO_ULTIMO_NOME,$NU_PASSAPORTE,$TE_DESCRICAO_ATIVIDADES,$DS_JUSTIFICATIVA_PROJ;
  global $nmFuncaoCandidato,$DT_PRAZO_ESTADA_SOLICITADO,$DS_JUSTIFICATIVA_CAND,$DT_ENTRADA,$DT_PRAZO_ESTADA;

  $fonte = "Times";

  $candidato = $NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME;
  if(strlen(trim($NU_RNE))>0) {
    $numRNE = "RNE ".$NU_RNE;
  }
  $nomedaembarcacao = $NO_EMBARCACAO_PROJETO[1];
  $prazocontratoembarcacao = $DT_PRAZO_CONTRATO_PROJ[1];
  $justificativa = trim($DS_JUSTIFICATIVA_CAND);
  if(strlen($justificativa) < 5) {
    $justificativa = $DS_JUSTIFICATIVA_PROJ[1];
    $justificativa = str_replace("<EMBARCACAO>",$NO_EMBARCACAO_PROJETO[1],$justificativa);
    $justificativa = str_replace("<BANDEIRA>",$bandeira[1],$justificativa);
  }

  $descricaodasatividades = $TE_DESCRICAO_ATIVIDADES;
  if(strlen($descricaodasatividades) < 5) {
    $text3a = "O Sr(a). ".$candidato." , como ".$nmFuncaoCandidato.", � respons�vel por avaliar a situa��o das m�quinas e ";
    $text3b = "dos equipamentos de bordo da embarca��o ".$nomedaembarcacao.", selecionar os materiais e ferramentas ";
    $text3c = "para manuten��o das m�quinas, apresentar relat�rios devidamente preenchidos com todas as especifica��es ";
    $text3c = "recomendadas, demonstrar as ocorr�ncias, realizar inspe��es e vistorias, identificar as anomalias e defeitos, ";
    $text3d = "seguir normas de seguran�a de trabalho e preserva��o ambiental.";
    $descricaodasatividades = $text3a.$text3b.$text3c.$text3d;
  }

  $pdf=new PDF();
  $pdf->SetMargins(15,10,5);
  $pdf->AliasNbPages();
  $pdf->AddPage();
  $pdf->SetFont($fonte,'',10);
  $pdf->SetXY(0,20);
  $titForm1 = "REQUERIMENTO DE PRORROGA��O DE PRAZO DE ESTADA";
  $titForm2 = "";
  $pdf->SetFont($fonte,'B',10);
  $w01=$pdf->GetStringWidth($titForm1)+6;
  $w02=$pdf->GetStringWidth($titForm2)+6;
  $pdf->SetX((210-$w01)/2);
  $pdf->Cell($w01,0,$titForm1,0,0,'C');
  $y = $pdf->GetY()+5;
  $pdf->SetY($y);
  if(strlen($titForm2)>0) {
    $pdf->SetX((210-$w02)/2);
    $pdf->Cell($w02,0,$titForm2,0,0,'C');
    $y = $y + 5;
    $pdf->SetY($y);
  }
  $y = $y + 5;
  $pdf->SetXY($marg,$y);
  $pdf->SetFont('Times','',10);  #  Times 8

  $pdf->MultiCell(0,5,"AO",0,'L');
  $pdf->MultiCell(0,5,"MINIST�RIO DA JUSTI�A",0,'L');
  $pdf->MultiCell(0,5,"SECRETARIA NACIONAL DE JUSTI�A",0,'L');
  $pdf->MultiCell(0,5,"DEPARTAMENTO DE PERMANENCIA DE ESTRANGEIROS",0,'L');
  $pdf->MultiCell(0,5,"Bras�lia/DF",0,'L');

  $pdf->Ln();
  $pdf->MultiCell(0,5,"At:. Ilmo(a). Chefe do Departamento de Estrangeiros",0,'L');

  $text1a = "Refer�ncia: Requerimento de Prorroga��o de Prazo de Estada";
  $text1b = "Estrangeiro: ".$candidato;
  $text1c = $numRNE;
  $text1d = "Tempor�rio V - RN 72/06 - Mar�timo";
  $pdf->Ln();
  $pdf->MultiCell(0,5,$text1a,0,'L');
  $pdf->MultiCell(0,5,$text1b,0,'L');
  $pdf->SetX($marg+18);
  $pdf->MultiCell(0,5,$text1c,0,'L');
  $pdf->SetX($marg+18);
  $pdf->MultiCell(0,5,$text1d,0,'L');

  $pdf->Ln();
  $pdf->MultiCell(0,5,"Ilmo. Chefe do Departamento de Estrangeiros,",0,'L');

  $text2a = "Vimos pela presente requerer a prorroga��o do prazo de estada em favor do(a) Sr(a). ".$candidato;
  if(strlen(trim($numRNE))>0) {
    $text2b = " portador do ".$numRNE.", tendo ingressado no Brasil em ".$DT_ENTRADA;
  } else {
    $text2b = ", tendo ingressado no Brasil em ".$DT_ENTRADA;
  }
  $text2c = ", sob visto tempor�rio V, obtido sob amparo da  RN 72/06, ";
  $text2d = "com prazo de estada at� ".$DT_PRAZO_ESTADA.", por se tratar de tripulante da embarca��o do tipo plataforma de petr�leo, ";
  $text2e = "denominada ".$nomedaembarcacao." que se encontra operando em AJB, sob responsabilidade de nossa empresa";
  $text2f = ", com prazo de validade at� ".$prazocontratoembarcacao.", cuja opera��o � de responsabilidade desta empresa.";
  $text2 = $text2a.$text2b.$text2c.$text2d.$text2e.$text2f;
  $pdf->Ln();
  $pdf->MultiCell(0,5,$text2,0,'J');

  $pdf->Ln();
  $pdf->MultiCell(0,5,"DESCRI��O DAS ATIVIDADES:",0,'L');

  $pdf->Ln();
  $pdf->MultiCell(0,5,$descricaodasatividades,0,'J');

  $pdf->Ln();
  $pdf->MultiCell(0,5,"JUSTIFICATIVA:",0,'L'); 

  $pdf->Ln();
  $pdf->MultiCell(0,5,$justificativa,0,'J'); 

  $text4a = "Outrossim, informamos que a embarca��o possui equipe de profissionais t�cnicos brasileiros em diversos n�veis";
  $text4b = " e categorias em propor��o acima da exigida, conforme apresentamos na lista de pessoal embarcados (doc. Anexo).";
  $pdf->Ln();
  $pdf->MultiCell(0,5,$text4a.$text4b,0,'J');

  $text5a = "Pelo exposto, requerermos a prorroga��o do prazo de estada por/at� ".$DT_PRAZO_ESTADA_SOLICITADO;
  $text5b = ", para que o mesmo possa permanecer no exerc�cio de suas atividades, considerando a obriga��o ";
  $text5c = "contratual da empresa propriet�ria da embarca��o em guarnecer a embarca��o com m�o de obra ";
  $text5d = "com a qualifica��o e experi�ncia profissional exigida.";
  $text5 = $text5a.$text5b.$text5c.$text5d;
  $pdf->Ln();
  $pdf->MultiCell(0,5,$text5,0,'J');

  $y = $pdf->GetY();
  if($y>230) { $pdf->AddPage(); }
  $pdf->MultiCell(0,5,"Rio de Janeiro, ".$dia." de ".$mes[$mess]." de ".$ano,0,'C');

  $w1[0] = 120;
  $w1[1] = 90;
  $y = $pdf->GetY()+10;
  $pdf->SetXY($marg,$y);
  $linhaaux = "-----------------------------------------------\n";
  $rowX[0] = $linhaaux.$NO_PROCURADOR[1]."\n".$nmCargoProc[1]."\n".$NO_EMISSOR_PROC[1]." ".$NU_IDENT_PROC[1]."\nCPF/MF ".$NU_CPF_PROC[1];
  if(strlen($NO_PROCURADOR2[1])>0) {
    $rowX[1] = $linhaaux.$NO_PROCURADOR2[1]."\n".$nmCargoProc2[1]."\n".$NO_EMISSOR_PROC2[1]." ".$NU_IDENT_PROC2[1]."\nCPF/MF ".$NU_CPF_PROC2[1];
  } else {
    $rowX[1] = "";
  }
  EscreveLinha($rowX,0,$w1);

  return $pdf;
}

function EscreveLinha($row,$borda,$wd) {
   global $pdf,$marg;
   $qtd = count($row);
   for($i=0;$i<$qtd;$i++)    {
      $w = $wd[$i];
      $xx=$pdf->GetX();
      $yy=$pdf->GetY();
      $pdf->MultiCell($w,4,$row[$i],$borda,'L');
      $pdf->SetXY($xx+$w,$yy);
   }
   $pdf->SetXY($marg,$yy+4);
}

?>
