<?php
include("libForm.php");
require('fpdf.php');

class PDF extends FPDF  {
  function Header(){
    global $marg,$NM_LOGO_EMPRESA,$dirimg,$LOGO_DIR;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    if($LOGO_DIR > 0) {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,170,2,33);
    } else {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,$marg,2,33);
    }
    $this->SetFont('Times','',8);  #  Times 8
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,3,$pagina,0,0,'C');
    //Line break
    $this->Ln(20);
  }
  function Footer() {
    global $NO_RAZAO_SOCIAL;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    $lin1 = "O presente material � titularizado com exclusividade pela $NO_RAZAO_SOCIAL, e qualquer reprodu��o, utiliza��o ou divulga��o do mesmo,";
    $lin2 = "sem expressa autoriza��o da titular, importa em ato il�cito, nos termos da legisla��o pertinente, atrav�s da qual ser�o imputadas as responsabilidades cab�veis.";
    //Position at 1.5 cm from bottom
    $this->SetY(-15);
    $this->SetFont('Times','',8);  #  Times 8
    $w1=$this->GetStringWidth($lin1)+6;
    $this->SetX((210-$w1)/2);
    $this->Cell($w1,0,$lin1,0,0,'C');
    $w2=$this->GetStringWidth($lin2)+6;
    $this->SetX((210-$w2)/2);
    $this->Cell($w2,7,$lin2,0,0,'C');
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,15,$pagina,0,0,'C');
  }
}

$ret = Inicial();
$LOGO_DIR = 0;
$aux_raz = strtoupper($NO_RAZAO_SOCIAL);
if( (!(strstr($aux_raz,'NOBLE') === FALSE)) || (!(strstr($aux_raz,'FUGRO') === FALSE)) ) {
   $LOGO_DIR = 1;
}

$tpSolicitacao=$_POST['tpSolicitacao'];
$nomeForm = "DeclaracaoEmbarcacoes_pet";
$marg = 15;
if(strlen($ret)==0) {
  $ret = ValidaDadosFormulario($nomeForm);
}
if(strlen($ret)>0) {
  echo $ret;
} else {
 if($tpSolicitacao=="teste") {
  $NM_LOGO_EMPRESA = "teste_logo.jpg";
  $ret = Formulario();
  $ret->Output();
 } else {
  $id_solicita_visto = $_POST['id_solicita_visto'];
  $NU_CANDIDATO = $_POST['idCandidato'];
  $ID_TIPO_FORMULARIO= $_POST['ID_TIPO_FORMULARIO'];
 	
  $local = RetornaDIRCandidato($id_solicita_visto, $NU_CANDIDATO);
  $id = RetornaIDformulario();
  $nomeArq = $nomeForm."_".$id.".pdf";
  $ret = GravaArquivoDBCand($id,$idEmpresa,$id_solicita_visto, $idCandidato,$nomeArq, $ID_TIPO_FORMULARIO);
  
  $ret = Formulario();
  $ret->Output();
  $ret->Output($local.$nomeArq,"F");
  $idCandidato = $_REQUEST['idCandidato'];
  $NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
  $idEmpresa = $_REQUEST['idEmpresa'];
  $cd_admin = $_REQUEST['cd_admin'];
  if(strlen(trim($usulogado))==0) { $usulogado=$cd_admin; }
  $gravado = "/arquivos/formularios/$idEmpresa/$idCandidato/".$nomeArq;
  $texto = "Gera��o do formul�rio de Declara��o de Embarca��es - Petrobras. <a href='$gravado'>(VER)</a>";
  insereEvento($idEmpresa,$idCandidato,$NU_SOLICITACAO,$texto);
 }
}

exit;

function Formulario() {
  global $pdf,$marg,$dia,$mes,$mess,$ano,$NO_PROCURADOR,$nmCargoProc,$NO_EMISSOR_PROC,$NU_IDENT_PROC,$NU_CPF_PROC;
  global $NO_PROCURADOR2,$NO_EMAIL_PROC2,$nmCargoProc2,$NO_EMISSOR_PROC2,$NU_IDENT_PROC2,$NU_CPF_PROC2;
  global $NO_EMBARCACAO_PROJETO,$ID_TIPO_EMBARCACAO_PROJETO,$nmPaisEmbarcacao,$QT_TRIPULANTES_EST_PROJ,$QT_TRIPULANTES_PROJ;
  global $QT_TRIPULANTES_EST_PROJ,$QT_TRIPULANTES_PROJ,$DT_PRAZO_CONTRATO_PROJ,$totEmbProj;
  global $NM_LOGO_EMPRESA;
  
  $w1[0] = 65;
  $w1[1] = 47;
  $w1[2] = 16;
  $w1[3] = 15;
  $w1[4] = 25;
  $w1[5] = 15;
  
  if(strlen($nmCargoProc[1])==0) { $nmCargoProc[1]="Procurador(a)"; }
  if(strlen($nmNacionalProc[1])==0) { $nmNacionalProc[1] = "brasileira"; }
  if(strlen($nmEstCivProc[1])>0) { $nmEstCivProc[1] = $nmEstCivProc[1].", "; }

  $pdf=new PDF();
  $pdf->SetMargins(15,10,5);
  $pdf->AliasNbPages();
  $pdf->AddPage();
  $pdf->SetFont('Times','',10);

  $pdf->SetXY($marg,20);

  $pdf->MultiCell(0,5,"",0,'L');
  $pdf->MultiCell(0,5,"AO",0,'L');
  $pdf->MultiCell(0,5,"MINIST�RIO DO TRABALHO E EMPREGO",0,'L');
  $pdf->MultiCell(0,5,"COORDENA��O GERAL DE IMIGRA��O",0,'L');
  $pdf->MultiCell(0,5,"Bras�lia/DF",0,'L');

  $pdf->Ln();
  $pdf->MultiCell(0,5,"Att. Ilmo Sr. Coordenador Geral de Imigra��o.",0,'L');
  $pdf->MultiCell(0,5,"Dr. Aldo Candido Costa Filho",0,'L');

  $pdf->Ln();
  $pdf->MultiCell(0,5,"Refer�ncia: Rela��o de Embarca��es e Total de Tripulantes",0,'L');

  $text1a = "Declaramos que atualmente a Petrobr�s possui ".$totEmbProj." navios estrangeiros, ";
  $text1b = "operando em cabotagem e longo curso, com a mesma finalidade, como segue:";
  $pdf->Ln();
  $pdf->MultiCell(0,5,$text1a.$text1b,0,'J');

  $pdf->Ln();
  $pdf->SetFont($fonte,'',7);
  $row1[0] = "Nome da Embarca��o";
  $row1[1] = "Bandeira";
  $row1[2] = "Estrangeiros";
  $row1[3] = "Brasileiros";
  $row1[4] = "Total de Tripulantes";
  $row1[5] = "Contrato";
  EscreveLinhaTitulo($row1,1,$w1);

  $alinha[0] = 'L';
  $alinha[1] = 'C';
  $alinha[2] = 'C';
  $alinha[3] = 'C';
  $alinha[4] = 'C';
  $alinha[5] = 'C';
  if($totEmbProj>0) {
    for($x=1;$x<=$totEmbProj;$x++) {
      if($ID_TIPO_EMBARCACAO_PROJETO[$x]=="1") {   # somente embarcacoes
        $pdf->SetX($marg);
        $row1[0] = html_entity_decode($NO_EMBARCACAO_PROJETO[$x]);
        $row1[1] = html_entity_decode(ucwords(strtolower($nmPaisEmbarcacao[$x])));
        $row1[2] = $QT_TRIPULANTES_EST_PROJ[$x];
        $row1[3] = $QT_TRIPULANTES_PROJ[$x] - $QT_TRIPULANTES_EST_PROJ[$x];
        $row1[4] = $QT_TRIPULANTES_PROJ[$x];
        $row1[5] = $DT_PRAZO_CONTRATO_PROJ[$x];
        if(strlen($row1[0])>40) { $row1[0]=substr($row1[0],0,40); }
        EscreveLinha($row1,1,$w1,$alinha);
      }
      $y = $pdf->GetY();
      if($y>260) { 
         $pdf->AddPage();
      }
    }
  }
  $pdf->SetFont($fonte,'',10);
  
  $pdf->Ln();
  $pdf->MultiCell(0,5,"* H� troca de turma prevista a cada 45 dias.",0,'L');

  $text2a = "Cabe-nos explicar que n�o foram admitidos tripulantes brasileiros em algumas ";
  $text2b = "embarca��es por se tratarem de navios petroleiros que operam carregando e descarregando entre ";
  $text2c = "portos nacionais e internacionais, n�o havendo permanecido, at� o momento, operando em �guas ";
  $text2d = "jurisdicionais brasileiras por prazo superior a 90 dias cont�nuos. ";
  $text2e = "Esse tipo de opera��o � muito din�mica, n�o h� previs�o de que a embarca��o ";
  $text2f = "v� permanecer por prazo superior ao mencionado, caso ocorra ser� observado a necessidade de contratar ";
  $text2g = "m�o-de-obra nacional atendendo o disposto no Art. 3o da Resolu��o Normativa RN72. ";
  $texto2 = $text2a.$text2b.$text2c.$text2d.$text2e.$text2f.$text2g;
  $pdf->Ln();
  $pdf->MultiCell(0,5,$texto2,0,'J');

  $pdf->Ln();
  $pdf->MultiCell(0,5,"Rio de Janeiro, ".$dia." de ".$mes[$mess]." de ".$ano,0,'C');

  $pdf->Ln();
  $pdf->Ln();
  $pdf->MultiCell(0,5,"----------------------------------------------",0,'C');
  $pdf->MultiCell(0,5,$NO_PROCURADOR[1],0,'C');
  $pdf->MultiCell(0,5,$nmCargoProc[1],0,'C');
  $pdf->MultiCell(0,5,$NO_EMISSOR_PROC[1]." ".$NU_IDENT_PROC[1],0,'C');
  $pdf->MultiCell(0,5,"CPF/MF ".$NU_CPF_PROC[1],0,'C');

  if(strlen($NO_PROCURADOR2[1])>0) {
    $pdf->Ln();
    $pdf->Ln();
    $pdf->MultiCell(0,5,"----------------------------------------------",0,'C');
    $pdf->MultiCell(0,5,$NO_PROCURADOR2[1],0,'C');
    $pdf->MultiCell(0,5,$nmCargoProc2[1],0,'C');
    $pdf->MultiCell(0,5,$NO_EMISSOR_PROC2[1]." ".$NU_IDENT_PROC2[1],0,'C');
    $pdf->MultiCell(0,5,"CPF/MF ".$NU_CPF_PROC2[1],0,'C');
  }

  return $pdf;
}

function EscreveLinhaTitulo($row,$borda,$wd) {
   global $pdf,$marg; 
   $qtd = count($row);
   $pdf->SetFont($fonte,'B',7);
   for($i=0;$i<$qtd;$i++)    {
      $w = $wd[$i];
      $xx=$pdf->GetX();
      $yy=$pdf->GetY();
      $pdf->MultiCell($w,5,$row[$i],$borda,'C');
      $pdf->SetXY($xx+$w,$yy);                    
   }
   $pdf->SetXY($marg,$yy+5);
  $pdf->SetFont($fonte,'',7);
}

function EscreveLinha($row,$borda,$wd,$alinha) {
   global $pdf,$marg; 
   $qtd = count($row);
   for($i=0;$i<$qtd;$i++)    {
      $w = $wd[$i];
      $xx=$pdf->GetX();
      $yy=$pdf->GetY();
      $pdf->MultiCell($w,5,$row[$i],$borda,$alinha[$i]);
      $pdf->SetXY($xx+$w,$yy);                    
   }
   $pdf->SetXY($marg,$yy+5);
}

?>

