<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../javascript/formularios_js.php");

$target = "target = 'WILSONS'";
$target2 = "WILSONS";
echo "<script language='javascript'>window.name='WILSONS';</script>";

$montaform = 0;
$empCmb="";
$candCmb="";
$nomeEmpresa = "";
$cmbProcurador = "";
$nomeCandidato = "";
$idCandidato = "";
$idEmpresa = "";

$idCandidato = $_REQUEST['idCandidato'];
$idEmpresa = $_REQUEST['idEmpresa'];
$NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];

$dia = date("d");
$mes = date("m");
$ano = date("Y");

if(strlen($idCandidato)>0) {
   $nomeCandidato = pegaNomeCandidato($idCandidato);
   if(strlen($idEmpresa)>0){
     $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
     $cmbProcurador = montaComboProcurador("",$idEmpresa,"");
   }
   $montaform = 1;
} else {
   if($todos == "S") {
      $candCmb = montaComboCandidatos(""," ORDER BY NO_PRIMEIRO_NOME,NO_ULTIMO_NOME ");
   } else {
     if(strlen($idEmpresa)>0){
       $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
       $candCmb = montaComboCandidatosEmpresa("", $idEmpresa ," ORDER BY C.NO_PRIMEIRO_NOME,C.NO_ULTIMO_NOME ");
     } else {
       $empCmb = montaComboEmpresas($codigo,"ORDER BY NO_RAZAO_SOCIAL");
    }
  }
}
echo Topo();
echo Menu("SOL");
$menu = "";
$menu = $menu.FormTodos();
$menu = $menu.FormPetro();
$menu = $menu.FormOutras();
$menu = $menu.FormVisas();
if(strlen($menu)>0) {
  $menu = MontaMenu($menu);
}
echo $menu;
?>
<br><center>
<table border=0 width=100% class="textoazulPeq">
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Formul&aacute;rios ::</strong></p>
  </td>
 </tr>
</table>
<br>
<form method='post' name=form1 <?=$target?>>
<table border=0 align="center" class="textoazul" width=500>
 <tr>
  <td width=130>Empresa: </td>
<?php  if($idEmpresa>0) {  ?>
  <td nowrap><?=$nomeEmpresa?><input type=hidden name='idEmpresa' value='<?=$idEmpresa?>'></td>
<?php  } else {  ?>
  <td nowrap><select name='idEmpresa'><option value=''>Escolha uma empresa<?=$empCmb?></select></td>
<?php  }  ?>
 </tr>
 <tr>
  <td>Candidato: </td>
<?php  if($idCandidato>0) {  ?>
  <td nowrap><?=$nomeCandidato?><input type=hidden name='idCandidato' value='<?=$idCandidato?>'><input type=hidden name='NU_SOLICITACAO' value='<?=$NU_SOLICITACAO?>'></td>
<?php  } else {  ?>
  <td nowrap><select name='idCandidato'><option value=''>Escolha um candidato<?=$candCmb?></select></td>
<?php  }  ?>
 </tr>
 <tr><td colspan=2>&#160;</td></tr>
 <tr>
  <td>Data do formul&aacute;rio:</td>
  <td>
   <select name='dia'><?=geraComboData(1,31,$dia,"N")?></select>
   <select name='mess'><?=geraComboData(1,12,$mes,"M")?></select>
   <select name='ano'><?=geraComboData(2008,2010,$ano,"N")?></select>
  </td>
 </tr>
 <tr><td colspan=2>&#160;</td></tr>
<?php
if($montaform == 0) {
  echo "<tr><td colspan=2 align=center>\n";
  echo " <input type=button value='Continuar'  onclick='javascript:escolher();' $target>\n";
  echo "</td></tr>\n";
} else {
#  echo "<tr id='procs' style='display:none'><td>Procuradores:</td>\n";
  echo "<tr id='procs'><td>Procurador 1:</td>\n";
  echo "<td><Select name='idProcurador'><option value=''>Escolha o procurador".$cmbProcurador."</select></td></tr>\n";
  echo "<tr id='procs2'><td>Procurador 2 (opcional):</td>\n";
  echo "<td><Select name='idProcurador2'><option value=''>Escolha o procurador".$cmbProcurador."</select></td></tr>\n";
  echo "<tr id='prazo' style='display:none'><td>Prazo do visto:</td>\n";
  echo "<td><input type=text name='prazo_contrato' size=20 maxlength=30></td></tr>\n";
  echo "<tr><td colspan=2>&#160;</td></tr>\n";
  echo "<tr><td valign=top>Tipo de Solicita&ccedil&atilde;o:</td>\n";
  echo "    <td><input type=radio name='tpSolicitacao' value='teste'>Teste Apenas (n&atilde;o grava formul&aacute;rio)\n";
  echo "    <br><input type=radio name='tpSolicitacao' value='novo'>Gera Novo Formul&aacute;rio (grava formul&aacute;rio)</td></tr>\n";
  echo "<tr><td colspan=2>&#160;</td></tr>\n";
  echo "<tr><td colspan=2 align=center>\n";
  echo "Escolha o(a) procurador(a), o tipo de solicita&ccedil;&atilde;o e o formul&aacute;rio desejado.\n";
  echo "<br>Se for o caso, escolha o procurador 2 tamb&eacute;m para que o formul&aacute;rio posua 2 procuradores.\n";
  echo "<br>Ou clique <a href='javascript:recupera();'><b>AQUI</b></a> para hist&oacute;rico dos formul&aacute;rios.\n";
  echo "</td></tr>\n";
}
?>
</table>
<br>
<input type=hidden name=cd_admin value="<?=$usulogado?>">
<input type=hidden name=pedido>
<input type=hidden name=montaform value='<?=$montaform?>'>
</form>

<?php
echo Rodape("");
?>

<?php

function FormTodos() {
  $local = "_top";
  $indis = "javascript:alert('Funcionalidade nao disponivel no momento.');";
  $ret = " <tr><td bgColor='#eeeeee'><A href='#'><b>Formul&aacute;rios Gerais</a></td></tr>\n";
  $ret = $ret.MontaLinhaMenu("javascript:formAutorizacaoTrabalho();","Formul&aacute;rio de Autoriza&ccedil;&atilde;o de Trabalho",$local);
  $ret = $ret.MontaLinhaMenu("javascript:termoResponsabilidadeEmbarcacao() ;","Termo de Responsabilidade de Embarca&ccedil;&otilde;es",$local);
  $ret = $ret.MontaLinhaMenu("javascript:termoResponsabilidadeRepatriamento() ;","Termo de Responsabilidade e Repatriamento",$local);
  $ret = $ret.MontaLinhaMenu("javascript:formRequerimentoProrrogacao() ;","Formul&aacute;rio de Requerimento de Prorroga&ccedil;&atilde;o",$local);
  $ret = $ret.MontaLinhaMenu("javascript:formRegistroPF();","Formul&aacute;rio de Registro na PF",$local);
  return $ret;
}
#  $ret = $ret.MontaLinhaMenu("javascript:modelo();","Teste Modelo",$local);

function FormPetro() {
  $local = "_top";
  $indis = "javascript:alert('Funcionalidade nao disponivel no momento.');";
  $ret = " <tr><td bgColor='#eeeeee'><A href='#'><b>Formul&aacute;rios Equipe Petrobras</a></td></tr>\n";
  $ret = $ret.MontaLinhaMenu("javascript:petroformDadosCadastrais();","Dados Cadastrais (modelo I)",$local);
  $ret = $ret.MontaLinhaMenu("javascript:petroDeclaracaoEmbarcacoes();","Declara&ccedil;&atilde;o de Embarca&ccedil;&otilde;es",$local);
  return $ret;
}

function FormOutras() {
  $local = "_top";
  $indis = "javascript:alert('Funcionalidade nao disponivel no momento.');";
  $ret = " <tr><td bgColor='#eeeeee'><A href='#'><b>Formul&aacute;rios Outras Equipes</a></td></tr>\n";
  $ret = $ret.MontaLinhaMenu("javascript:formDadosCadastrais();","Formul&aacute;rio Dados Cadastrais (modelo 1)",$local);
  $ret = $ret.MontaLinhaMenu("javascript:outrosDeclaracaoEmbarcacoes();","Declara&ccedil;&atilde;o de Embarca&ccedil;&otilde;es",$local);
  $ret = $ret.MontaLinhaMenu("javascript:outrosContratoTrabalhoDependentes() ;","Contrato de Trabalho c/dependente",$local);
  return $ret;
}
#  $ret = $ret.MontaLinhaMenu("javascript:pedVisto('1');","Pedido de Visto pag 1",$local);
#  $ret = $ret.MontaLinhaMenu("javascript:pedVisto('2');","Pedido de Visto pag 2",$local);

function FormVisas() {
  $local = "_top";
  $indis = "javascript:alert('Funcionalidade nao disponivel no momento.');";
  $ret = " <tr><td bgColor='#eeeeee'><A href='#'><b>Visa Form</a></td></tr>\n";
  $ret = $ret.MontaLinhaMenu("javascript:visaform('HOUSTON');","Consulado Geral em Houston",$local);
  $ret = $ret.MontaLinhaMenu("javascript:visaform('CINGAPURA');","Consulado Geral em Cingapura",$local);
  $ret = $ret.MontaLinhaMenu("javascript:visaform('GERAL');","Visa Form Geral",$local);
  return $ret;
}

function MontaMenu($menu) {
  global $usufunc;
  $ret = "<!--Miolo-->\n";
  $ret = $ret."<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
  $ret = $ret."<tr>\n";
  $ret = $ret."<!--Menu-->\n";
  $ret = $ret."<td valign='top' width='280'><img src='images/ponto.gif' width=280 height=10><br>\n";
  $ret = $ret."<table width='100%' border=0>\n";
  $ret = $ret.$menu;
  $ret = $ret."</table>\n";
  $ret = $ret."</td>\n";
  $ret = $ret."<!--/Menu-->\n";
  $ret = $ret."<td valign='top' width='100%'>\n";
  return $ret;
} 

function geraComboData($ini,$fim,$atual,$tipo) {
  $ret = "";
  $meses = pegaMeses();
  for($x=$ini;$x<=$fim;$x++) {
     if($x==$atual) { $sel = "selected"; } else { $sel=""; }
     if($tipo=="N") { $valor=$x; } else { $valor=$meses[$x]; }
     $ret = $ret."<option value='$x' $sel>$valor</option>";
  }
  return $ret;
}
?>

