<?php
include("libForm.php");
require('fpdf.php');

class PDF extends FPDF  {
  function Header(){
  }
  function Footer() {
  }
}

$visaform = $_POST['pedido'];
$tpSolicitacao=$_POST['tpSolicitacao'];
$nomeForm = "VisaForm_".$visaform;
$margx = 10;
$margy = 25;
$ret = Inicial();
if(strlen($ret)==0) {
  $ret = ValidaDadosFormulario($nomeForm);
}
if(strlen($ret)>0) {
  echo $ret;
} else {
 if($tpSolicitacao=="teste") {
  $NM_LOGO_EMPRESA = "teste_logo.jpg";
  $ret = Formulario();
  $ret->Output();
 } else {
  $NM_LOGO_EMPRESA = "visa_form_ae.jpg";
   $id_solicita_visto = $_POST['id_solicita_visto'];
  $NU_CANDIDATO = $_POST['idCandidato'];
  $ID_TIPO_FORMULARIO= $_POST['ID_TIPO_FORMULARIO'];
 	
  $local = RetornaDIRCandidato($id_solicita_visto, $NU_CANDIDATO);
  $id = RetornaIDformulario();
  $nomeArq = $nomeForm."_".$id.".pdf";
  $ret = GravaArquivoDBCand($id,$idEmpresa,$id_solicita_visto, $idCandidato,$nomeArq, $ID_TIPO_FORMULARIO);
    $ret = Formulario();
  $ret->Output();
  $ret->Output($local.$nomeArq,"F");
  $idCandidato = $_REQUEST['idCandidato'];
  $NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
  $idEmpresa = $_REQUEST['idEmpresa'];
  $cd_admin = $_REQUEST['cd_admin'];
  if(strlen(trim($usulogado))==0) { $usulogado=$cd_admin; }
  $gravado = "/arquivos/formularios/$idEmpresa/$idCandidato/".$nomeArq;
  $texto = "Gera��o do VISA FORM (Geral). <a href='$gravado'>(VER)</a>";
  insereEvento($idEmpresa,$idCandidato,$NU_SOLICITACAO,$texto);
 }
}

exit;

function Formulario() {
  global $pdf,$margx,$margy,$dirimg,$NM_LOGO_EMPRESA,$NO_RAZAO_SOCIAL,$dia,$mes,$mess,$ano,$visaform;
  global $NO_PRIMEIRO_NOME,$NO_NOME_MEIO,$NO_ULTIMO_NOME,$NO_LOCAL_NASCIMENTO,$CO_PAIS_ORIGEM,$DT_NASCIMENTO,$NO_PAI,$NO_MAE;
  global $CO_ESTADO_CIVIL,$CO_SEXO,$NU_PASSAPORTE,$DT_EMISSAO_PASSAPORTE,$DT_VALIDADE_PASSAPORTE,$CO_PAIS_EMISSOR_PASSAPORTE;
  global $nmNacional,$nmPaisResidencia,$nmPaisExt,$nmPaisEmpresa;
  global $NO_ENDERECO_EMPRESA,$NO_CIDADE_EMPRESA,$CO_PAIS_EMPRESA,$NU_TELEFONE_EMPRESA;
  global $NO_ENDERECO_RESIDENCIA,$NO_CIDADE_RESIDENCIA,$CO_PAIS_RESIDENCIA,$NU_TELEFONE_CANDIDATO;
  global $NO_ENDERECO_ESTRANGEIRO,$NO_CIDADE_ESTRANGEIRO,$CO_PAIS_ESTRANGEIRO,$NU_TELEFONE_ESTRANGEIRO;
  global $DT_PRAZO_ESTADA_SOLICITADO,$CO_FUNCAO_CANDIDATO,$CO_PROFISSAO_CANDIDATO,$NO_LOCAL_ENTRADA,$NO_CIDADE_RESIDENCIA;
  global $NO_ENDERECO_EMP,$NO_COMPLEMENTO_EMP,$NO_BAIRRO_EMP,$NO_MUNICIPIO_EMP,$CO_UF_EMP,$NU_CEP_EMP,$NU_DDD_EMP,$NU_TELEFONE_EMP,$NO_EMAIL_EMPRESA;

  $NascDia = substr($DT_NASCIMENTO,0,2);
  $NascMes = substr($DT_NASCIMENTO,3,2);
  $NascAno = substr($DT_NASCIMENTO,6,4);

  $PassDia =  substr($DT_VALIDADE_PASSAPORTE,0,2);
  $PassMes =  substr($DT_VALIDADE_PASSAPORTE,3,2);
  $PassAno =  substr($DT_VALIDADE_PASSAPORTE,6,4);

  $nmEstadoCivil = pegaNomeEstadoCivil($CO_ESTADO_CIVIL);
  $nmPaisNacional= pegaNomePais($CO_PAIS_ORIGEM);
  $nmPaisPassaporte= pegaNomePais($CO_PAIS_EMISSOR_PASSAPORTE);
  $nmFuncaoCandidato = pegaNomeFuncao($CO_FUNCAO_CANDIDATO);
  $nmProfissao = pegaNomeProfissao($CO_PROFISSAO_CANDIDATO);

  $campo1 = $NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME;
  $campo2 = $NO_LOCAL_NASCIMENTO."/".$nmPaisNacional;
  $campo4 = $nmNacional;
  if($CO_SEXO == "M") { $campo5="Masculino"; }
  elseif($CO_SEXO == "F") { $campo5 = "Feminino"; }
  $campo6 = $nmEstadoCivil;
  $campo7 = $NU_PASSAPORTE;
  $campo8 = $nmPaisPassaporte;
  $campo10a = $NO_PAI;
  $campo10b = $NO_MAE;
  $campo11 = $NO_ENDERECO_ESTRANGEIRO." - ".$NO_CIDADE_ESTRANGEIRO." - ".$nmPaisExt;
  $campo12 = $NU_TELEFONE_ESTRANGEIRO;
  $campo13 = $nmProfissao; # Vazio por enquanto - profissao
  $campo14 = $NO_ENDERECO_EMPRESA." - ".$NO_CIDADE_EMPRESA." - ".$nmPaisEmpresa;
  $campo15 = $NU_TELEFONE_EMPRESA;
  $campo16 = substr($NO_RAZAO_SOCIAL,0,20); # Vazio por enquanto - empregador
  $campo17 = $nmFuncaoCandidato; # Vazio por enquanto - job position
  $campo18 = $NO_EMAIL_EMPRESA; # Vazio por enquanto - email

  $trip[1]=""; # Vazio por enquanto
  $trip[2]=""; # Vazio por enquanto
  $trip[3]=""; # Vazio por enquanto
  $trip[4]=""; # Vazio por enquanto
  $trip[5]=""; # Vazio por enquanto
  $trip[6]=""; # Vazio por enquanto
  $trip[7]=""; # Vazio por enquanto
  $trip[8]=""; # Vazio por enquanto
  $trip[9]=""; # Vazio por enquanto
  $trip[10]=""; # Vazio por enquanto
  $trip[11]=""; # Vazio por enquanto
  $trip[12]=""; # Vazio por enquanto
  $trip[13]=""; # Vazio por enquanto
  $trip[14]=""; # Vazio por enquanto
  $trip[15]=""; # Vazio por enquanto
  $trip[16]=""; # Vazio por enquanto
  $trip[17]=""; # Vazio por enquanto
  $trip[18]=""; # Vazio por enquanto
  $tripoutros = "Outro motivo qualquer";
  $tripcomentario = "Comentario 1 sdsfsd dsf sdfsdfsdf sdfsdf sdfsd fsdf df sdfsdf sdfsd fsdf sdf sdfsd fsdfdsf sdfs sdf sdfsdf sdfsdf sdfsd fsd fsdf sdfsd fsdf sdfsdf.\nComentario 3";
  $tripoutros = ""; # Vazio por enquanto
  $tripcomentario = ""; # Vazio por enquanto
  $campo20 = $NO_RAZAO_SOCIAL." - ".$NO_ENDERECO_EMP." - ".$NO_MUNICIPIO_EMP." - ".$CO_UF_EMP;
  $campo21 = $NO_ENDERECO_EMP." - ".$NO_MUNICIPIO_EMP." - ".$CO_UF_EMP;
  $campo22 = "(".$NU_DDD_EMP.") ".$NU_TELEFONE_EMP;

  $campo23 = $NO_LOCAL_ENTRADA; # ver porta de entrada
  $campo24 = $NO_CIDADE_RESIDENCIA; # Destino
  $campo25 = $DT_PRAZO_ESTADA_SOLICITADO; # Duracao

  $campo26y = ""; # Vazio por enquanto - Sim esteve no Brasil
  $campo26n = ""; # Vazio por enquanto - Nao esteve no Brasil
  $campo27 = ""; # Vazio por enquanto - Lugar e duracao

  $campo29 = $NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME;

  $bordInt = 0.5;

  $pdf=new PDF();
  $pdf->SetMargins($margx,$margy);
  $pdf->AliasNbPages();
  $pdf->AddPage();

# --------------  1a pagina  ----------------

  $pdf->Image($dirimg.$NM_LOGO_EMPRESA,$margx,$margy-10,16);

  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(35,$margy-10);
  $pdf->MultiCell(80,5,"REPL�BLICA FEDERATIVA DO BRASIL\n\nMINIST�RIO DA RELA��ES EXTERIORES",0,'L');

  $pdf->Rect(113,$margy-10,87,10);
  $pdf->SetFont('Arial','B',11);
  $pdf->SetXY(115,$margy-10);
  $pdf->MultiCell(80,5,"Pedido de Visto",0,'C');
  $pdf->SetFont('Arial','I',11);
  $pdf->SetXY(115,$margy-5);
  $pdf->MultiCell(80,5,"VISA APPLICATION FORM",0,'C');

  $pdf->Rect(113,$margy,87,20);
  $pdf->Line(156,$margy,156,$margy+20);
  $pdf->SetFont('Times','',10);
  $pdf->SetXY(114,$margy);
  $pdf->MultiCell(48,6,"Protocolo",0,'L');
  $pdf->SetXY(157,$margy);
  $pdf->MultiCell(48,6,"Visto\nEtiqueta n.",0,'L');

  $pdf->SetFont('Arial','BU',10);
  $texto = "PLEASE TYPE OR PRINT. ANSWER ITEMS 1 THROUGH 26 (FIRST AND SECOND PAGE) AND SIGN.\nINCOMPLETE FORMS WILL BE RETURNED.";
  EscreveNoInicio($texto,$margx+3,$margy+25,190);

  $pdf->SetFont('Arial','',10);
  $texto = "A - DADOS PESSOAIS ";
  EscreveNoInicio($texto,$margx+3,$margy+40,45);
  $pdf->SetFont('Arial','I',10);
  $texto = "(PERSONAL INFORMATION)";
  EscreveNoInicio($texto,$margx+45,$margy+40,80);

# --------------  1a parte  ----------------

  $margy = $margy+50;

  $pdf->Rect($margx,$margy,190,108); # 1o Quadrado maior
  $pdf->Rect($margx+0.5,$margy+0.5,189,72); # 1o Quadrado medio
  $pdf->Rect($margx+146,$margy+0.5,43.5,54.5); # Retangulo da foto

  $pdf->Rect($margx+0.5,$margy+15,145.5,14); # 1o Quadrado pequeno (Nascimento)
  $pdf->Line($margx+75,$margy+15,$margx+75,$margy+29); # Linha separa meio Nascimento
  $pdf->Line($margx+98,$margy+24,$margx+98,$margy+29); # Linha separa 1/3 Date of Birth
  $pdf->Line($margx+120,$margy+24,$margx+120,$margy+29); # Linha separa 2/3 Date of Birth
  $pdf->Line($margx+50,$margy+29,$margx+50,$margy+55); # Linha separa 1/3 Country
  $pdf->Line($margx+98,$margy+29,$margx+98,$margy+55); # Linha separa 2/3 Country
  $pdf->Line($margx+115,$margy+50,$margx+115,$margy+55); # Linha separa 1/3 Date Passaporte
  $pdf->Line($margx+130,$margy+50,$margx+130,$margy+55); # Linha separa 2/3 Date Passaporte
  $pdf->Rect($margx+0.5,$margy+41,145.5,14); # 3o Quadrado pequeno (Passaporte)

# Par a foto
  $pdf->SetFont('Times','B',12);
  EscreveNoMeio("Attach",$margx+167,$margy+12);
  EscreveNoMeio("3 x 4",$margx+167,$margy+26);
  EscreveNoMeio("photo",$margx+167,$margy+35);
  EscreveNoMeio("here",$margx+167,$margy+44);

  $pdf->SetFont('Times','',10); 
  $pdf->Text($margx+1,$margy+4,"01 - First/Middle/Family Name)");
  EscreveNoInicio($campo1,$margx+1,$margy+6,100);
  $pdf->SetFont('Times','',10);

  $margy = $margy + 17;
  $pdf->Text($margx+1,$margy+1,"02 - Place of Birth (city/state/country)");
  $pdf->Text($margx+76,$margy+1,"03 - Date of birth");
  EscreveNoMeio($campo2,$margx+30,$margy+6);
  EscreveNoMeio("Day",$margx+88,$margy+4);
  EscreveNoMeio("Month",$margx+110,$margy+4);
  EscreveNoMeio("Year",$margx+133,$margy+4);
  EscreveNoMeio($NascDia,$margx+88,$margy+9);
  EscreveNoMeio($NascMes,$margx+110,$margy+9);
  EscreveNoMeio($NascAno,$margx+133,$margy+9);

  $margy = $margy + 15;
  $pdf->Text($margx+1,$margy,"04 - Nationality");
  $pdf->Text($margx+51,$margy,"05 - Sex");
  $pdf->Text($margx+99,$margy,"06 - Marital status");
  $pdf->Text($margx+2,$margy+5,$campo4);
  $pdf->Text($margx+60,$margy+5,$campo5);
  $pdf->Text($margx+100,$margy+5,$campo6);

  $margy = $margy + 12;
  $pdf->Text($margx+1,$margy,"07 - Passport or Travel document");
  $pdf->Text($margx+51,$margy,"08 - Issuing country");
  $pdf->Text($margx+99,$margy,"09 - Expiration date (D/M/Y)");
  EscreveNoMeio($campo7,$margx+24,$margy+7);
  EscreveNoMeio($campo8,$margx+73,$margy+7);
  EscreveNoMeio("Day",$margx+108,$margy+4);
  EscreveNoMeio("Month",$margx+122,$margy+4);
  EscreveNoMeio("Year",$margx+137,$margy+4);
  EscreveNoMeio($PassDia,$margx+107,$margy+9);
  EscreveNoMeio($PassMes,$margx+122,$margy+9);
  EscreveNoMeio($PassAno,$margx+137,$margy+9);
 
  $margy = $margy + 15;
  $pdf->Text($margx+1,$margy,"10 - Parent's Name and Nationality");
  $pdf->Text($margx+20,$margy+5,"Father's:");
  $pdf->Text($margx+20,$margy+10,"Mother's:");
  $pdf->Text($margx+40,$margy+5,$campo10a);
  $pdf->Text($margx+40,$margy+10,$campo10b);
  $pdf->Line($margx+38,$margy+6,$margx+170,$margy+6); # Linha do pai
  $pdf->Line($margx+38,$margy+11,$margx+170,$margy+11); # Linha da mae
  $pdf->SetFont('Times','',10);

# --------------  2a parte  ----------------

  $margy = $margy+14;

  $pdf->Rect($margx+0.5,$margy-0.5,189,11.5); # 1o Quadrado maior

  $pdf->Text($margx+2,$margy+3,"11 - Home Address");
  $pdf->Text($margx+2,$margy+8,$campo11);
  $pdf->Rect($margx+90,$margy-0.5,50,11.5); # 1o Quadrado interno
  $pdf->Text($margx+91,$margy+3,"12 - Telephone #");
  $pdf->Text($margx+91,$margy+8,$campo12);
  $pdf->Text($margx+141,$margy+3,"13 - Profession");
  $pdf->Text($margx+141,$margy+8,$campo13);

  $pdf->Rect($margx+0.5,$margy+11,189,11.5); # 1o Quadrado maior

  $pdf->Text($margx+2,$margy+14,"14 - Business Address");
  $pdf->Text($margx+2,$margy+19,$campo14);
  $pdf->Rect($margx+90,$margy+11,50,11.5); # 2o Quadrado interno
  $pdf->Text($margx+91,$margy+14,"15 - Telephone #");
  $pdf->Text($margx+91,$margy+19,$campo15);
  $pdf->Text($margx+141,$margy+14,"16 - Employer");
  $pdf->Text($margx+141,$margy+19,$campo16);

  $pdf->Rect($margx+0.5,$margy+22.5,189,12); # 1o Quadrado maior

  $pdf->Text($margx+2,$margy+27,"17 - Job Position or Title");
  $pdf->Text($margx+2,$margy+32,$campo17);
  $pdf->Text($margx+91,$margy+27,"18 - E-mail");
  $pdf->Text($margx+91,$margy+32,$campo18);

  $pdf->Line($margx+90,$margy+22.5,$margx+90,$margy+34.5); # Linha separa email

# --------------  3a parte  ----------------

  $margy = $margy+50;

  $pdf->Rect($margx,$margy,190,56); # 1o Quadrado maior
  $pdf->Rect($margx+0.2,$margy+0.2,189.5,55.5); # 1o Quadrado maior
  $pdf->Rect($margx+0.5,$margy+0.5,189,55); # 1o Quadrado maior
  $pdf->Line($margx,$margy+7,200,$margy+7); # Linha divide Horizontalmente 1/3 1o Quadrado medio
  $pdf->Line($margx,$margy+21,200,$margy+21); # Linha divide Horizontalmente 2/3 1o Quadrado medio
  $pdf->Line($margx,$margy+38,200,$margy+38); # Linha divide Horizontalmente 3/3 1o Quadrado medio
  $pdf->Rect($margx+62,$margy+7,64,14); # Quadrado da 1a linha
  $pdf->Rect($margx+48,$margy+21,92,17); # Quadrado maior da 2a linha
  $pdf->Line($margx+95,$margy+21,$margx+95,$margy+56); # Linha que divide ultima linha

  $pdf->SetFont('Times','B',14);
  EscreveNoMeio("PARA USO OFICIAL (FOR OFFICIAL USE ONLY)",$margx+95,$margy+4);
  $pdf->SetFont('Times','',10);

  $pdf->Text($margx+2,$margy+11,"A - Consulta � SERE");
  $pdf->Text($margx+63,$margy+11,"B - Autoriza��o da SERE");
  $pdf->Text($margx+127,$margy+11,"C - Tipo do Visto");
  $pdf->Text($margx+2,$margy+18,"OF         TEL              No. ________");
  $pdf->Text($margx+64,$margy+18,"DESP        DESPTEL         No. ________");
  $pdf->Text($margx+128,$margy+18,"____________________");
  $pdf->Text($margx+2,$margy+25,"D -");
  $pdf->Text($margx+17,$margy+25,"Concess�o");
  $pdf->Text($margx+17,$margy+30,"Denega��o");
  $pdf->Text($margx+17,$margy+35,"Renova��o");
  $pdf->Text($margx+49,$margy+25,"E - Entradas");
  $pdf->Text($margx+61,$margy+30,"Uma ");
  $pdf->Text($margx+61,$margy+35,"Multiplas ");
  $pdf->Text($margx+96,$margy+25,"F - Validade");
  $pdf->Text($margx+99,$margy+35,"_____________ anos/dia");
  $pdf->Text($margx+141,$margy+25,"G - Data");
  $pdf->Text($margx+144,$margy+35,"_____/_____/_______");
  $pdf->Text($margx+2,$margy+42,"H - Observa��es");

  $pdf->Text($margx+96,$margy+42,"I - Assinaturas");
  $pdf->Text($margx+110,$margy+53,"Funcion�rio");
  $pdf->Text($margx+160,$margy+53,"Chefia");


  $pdf->SetFont('Times','',10);
  $pdf->Rect($margx+8,$margy+15.5,3,3); # Quadrado da 1a linha
  $pdf->Rect($margx+23,$margy+15.5,3,3); # Quadrado da 1a linha
  $pdf->Rect($margx+74,$margy+15.5,3,3); # Quadrado da 1a linha
  $pdf->Rect($margx+97,$margy+15.5,3,3); # Quadrado da 1a linha
  $pdf->Rect($margx+12,$margy+22.5,3,3); # Quadrado da 2a linha
  $pdf->Rect($margx+12,$margy+27.5,3,3); # Quadrado da 2a linha
  $pdf->Rect($margx+12,$margy+32.5,3,3); # Quadrado da 2a linha
  $pdf->Rect($margx+56,$margy+27,3,3); # Quadrado da 2a linha
  $pdf->Rect($margx+56,$margy+32,3,3);   # Quadrado da 2a linha

# --------------  2a pagina  ----------------
 $pdf->AddPage();

# --------------  1a parte  ----------------

  $margy = 25;

  $pdf->Rect($margx,$margy,190,184); # 1o Quadrado maior
  $pdf->Rect($margx+0.5,$margy+0.5,189,183.5); # 1o Quadrado medio

  $pdf->Text($margx+2,$margy+5,"19 - Purpose of trip (check item that is the most applicable to the circumstances of your trip)");

  $pdf->Rect($margx+5,$margy+8.5,3,3); # Retangulo da 1a linha
  $pdf->Text($margx+6,$margy+11,$trip[1]);
  $texto = "In-country provision of services of temporary or permanent nature, including in-field services under contract and/or intra-company activities such as project management, technical support, training, auditing/accounting.";
  EscreveNoInicio($texto,$margx+10,$margy+8,180);

  $pdf->Rect($margx+5,$margy+16.5,3,3); # Retangulo da 2a linha
  $pdf->Text($margx+6,$margy+19,$trip[2]);
  $texto = "Headquarters-based business development activities, including negotiating contracts, executive meetings, marketing assessment, specifying orders in contracts, customer relations related activities, performance assessment, establishing framework for doing business in Brazil.";
  EscreveNoInicio($texto,$margx+10,$margy+16,180);

  $pdf->Rect($margx+5,$margy+29.5,3,3); # Retangulo da 3a linha
  $pdf->Text($margx+6,$margy+32,$trip[3]);
  $texto = "Import/Export business.";
  EscreveNoInicio($texto,$margx+10,$margy+29,180);

  $pdf->Rect($margx+5,$margy+34.5,3,3); # Retangulo da 4a linha
  $pdf->Text($margx+6,$margy+37,$trip[4]);
  $texto = "Work on offshore platform/ship.";
  EscreveNoInicio($texto,$margx+10,$margy+34,180);

  $pdf->Rect($margx+5,$margy+39.5,3,3); # Retangulo da 5a linha
  $pdf->Text($margx+6,$margy+42,$trip[5]);
  $texto = "Work under an employment contract with a company/organization in Brazil.";
  EscreveNoInicio($texto,$margx+10,$margy+39,180);

  $pdf->Rect($margx+5,$margy+44.5,3,3); # Retangulo da 6a linha
  $pdf->Text($margx+6,$margy+47,$trip[6]);
  $texto = "Attend conference, seminar or workshop (attendee? Paid/unpaid speaker? Trainer? Name event sponsor).";
  EscreveNoInicio($texto,$margx+10,$margy+44,180);

  $pdf->Rect($margx+5,$margy+49.5,3,3); # Retangulo da 7a linha
  $pdf->Text($margx+6,$margy+52,$trip[7]);
  $texto = "Professional training as an intern.";
  EscreveNoInicio($texto,$margx+10,$margy+49,180);

  $pdf->Rect($margx+5,$margy+54.5,3,3); # Retangulo da 8a linha
  $pdf->Text($margx+6,$margy+57,$trip[8]);
  $texto = "Provide religious or missionary services and/or assistance.";
  EscreveNoInicio($texto,$margx+10,$margy+54,180);

  $pdf->Rect($margx+5,$margy+59.5,3,3); # Retangulo da 9a linha
  $pdf->Text($margx+6,$margy+62,$trip[9]);
  $texto = "Provide community and/or medical services.";
  EscreveNoInicio($texto,$margx+10,$margy+59,180);

  $pdf->Rect($margx+5,$margy+65.5,3,3); # Retangulo da 10a linha
  $pdf->Text($margx+6,$margy+66,$trip[10]);
  $texto = "Attend school or pursue studies";
  EscreveNoInicio($texto,$margx+10,$margy+65,180);

  $pdf->Rect($margx+5,$margy+70.5,3,3); # Retangulo da 11a linha
  $pdf->Text($margx+6,$margy+73,$trip[11]);
  $texto = "Conduct research or pursue scientific-technologic activities under an international cooperation program";
  EscreveNoInicio($texto,$margx+10,$margy+70,180);

  $pdf->Rect($margx+5,$margy+75.5,3,3); # Retangulo da 12a linha
  $pdf->Text($margx+6,$margy+78,$trip[12]);
  $texto = "Pursue professorial studies/research/teaching and/or pursue scientific/technologic activities at an university,  research or similar organization (employment contract? Short term pro-labore? Research scholarship?)";
  EscreveNoInicio($texto,$margx+10,$margy+75,180);

  $pdf->Rect($margx+5,$margy+85.5,3,3); # Retangulo da 13a linha
  $pdf->Text($margx+6,$margy+88,$trip[13]);
  $texto = "Participation in athletic or performing arts events (paid/unpaid participation?)";
  EscreveNoInicio($texto,$margx+10,$margy+85,180);

  $pdf->Rect($margx+5,$margy+91.5,3,3); # Retangulo da 14a linha
  $pdf->Text($margx+6,$margy+94,$trip[14]);
  $texto = "Journalism activities and/or  film making";
  EscreveNoInicio($texto,$margx+10,$margy+91,180);

  $pdf->Rect($margx+5,$margy+96.5,3,3); # Retangulo da 15a linha
  $pdf->Text($margx+6,$margy+99,$trip[15]);
  $texto = "Official government mission/business.";
  EscreveNoInicio($texto,$margx+10,$margy+96,180);

  $pdf->Rect($margx+5,$margy+101.5,3,3); # Retangulo da 16a linha
  $pdf->Text($margx+6,$margy+104,$trip[16]);
  $texto = "Visit friend(s) and/or relatives (inform below relationship; provide name and address on item 20).";
  EscreveNoInicio($texto,$margx+10,$margy+101,180);

  $pdf->Rect($margx+5,$margy+106.5,3,3); # Retangulo da 17a linha
  $pdf->Text($margx+6,$margy+109,$trip[17]);
  $texto = "Tourism (inform below location, nature of trip, etc.)";
  EscreveNoInicio($texto,$margx+10,$margy+106,180);

  $pdf->Text($margx+6,$margy+113,$trip[18]);
  EscreveNoInicio("X Other:  ".$tripoutros,$margx+5,$margy+111,180);
  EscreveNoInicio("Comments:",$margx+15,$margy+116,180);
  $pdf->SetXY($margx+34,$margy+124);
  $pdf->MultiCell(150,5,$tripcomentario,0,'L');
  $pdf->Line($margx+35,$margy+120,$margx+180,$margy+120); # Linha separa 1 do comentario
  $pdf->Line($margx+15,$margy+125,$margx+180,$margy+125); # Linha separa 2 do comentario
  $pdf->Line($margx+15,$margy+130,$margx+180,$margy+130); # Linha separa 3 do comentario

  $pdf->Rect($margx+0.5,$margy+135,189,10); # Retangulo dos numero 20 e 21
  $pdf->Text($margx+2,$margy+138,"20 - Name and address of person, institution or company where you can be contacted in Brazil");
  $pdf->Text($margx+3,$margy+143,$campo20);

  $pdf->Line($margx+120,$margy+145,$margx+120,$margy+155); # Linha separa 21 do 22
  $pdf->Text($margx+2,$margy+148,"21 -Address while in Brazil");
  $pdf->Text($margx+3,$margy+153,$campo21);
  $pdf->Text($margx+121,$margy+148,"22 - Telephone #");
  $pdf->Text($margx+122,$margy+153,$campo22);

  $pdf->Rect($margx+0.5,$margy+155,189,12); # Retangulo dos numero 21 e 23

  $pdf->Line($margx+74,$margy+155,$margx+74,$margy+167); # Linha separa 23 do 24
  $pdf->Line($margx+114,$margy+155,$margx+114,$margy+167); # Linha separa 24 do 25
  $pdf->Text($margx+2,$margy+159,"23 -Place and date of arrival");
  $pdf->Text($margx+3,$margy+164,$campo23);
  $pdf->Text($margx+75,$margy+159,"24 - Destination");
  $pdf->Text($margx+75,$margy+164,$campo24);
  $pdf->Text($margx+115,$margy+159,"25 -Duration of intended stay (in days or years)");
  $pdf->Text($margx+115,$margy+164,$campo25);

  $pdf->Text($margx+2,$margy+171,"26 - Have you ever been to Brazil?");
  $pdf->Text($margx+2,$margy+175,"If yes, inform when, place and duration of stay");

  $pdf->Text($margx+100,$margy+171,"Yes");
  $pdf->Text($margx+140,$margy+171,"No");
  
  $pdf->Text($margx+16,$margy+180,$campo26y);
  $pdf->Text($margx+36,$margy+180,$campo26n);
  $pdf->Text($margx+67,$margy+180,$campo27);

# --------------  2a parte  ----------------

  $margy = $margy + 200;

  $pdf->SetFont('Arial','B',10);
  $pdf->Text($margx+2,$margy,"B - TERMO DE RESPONSABILIDADE  (FORMAL STATEMENT)");

  $pdf->SetFont('Times','',10);

  $pdf->Rect($margx,$margy+7,190,32); # 1o Quadrado maior
  $pdf->Rect($margx+0.5,$margy+7.5,189,31.5); # 1o Quadrado inferior

  $texto = "27 - I have read and understood the instructions contained in the information sheet provided for visa applications and I declare that the above information is true and accurate.";
  EscreveNoInicio($texto,$margx+2,$margy+11,200);
  $pdf->Text($margx+2,$margy+29,"Name");
  $pdf->Text($margx+90,$margy+24,"Date");
  $pdf->Text($margx+140,$margy+29,"Signature");
  $pdf->SetFont('Times','',9);
  $pdf->Line($margx+0.5,$margy+25.5,$margx+189.5,$margy+25.5); # Linha que divide data
  $pdf->Text($margx+5,$margy+36,$campo29);
  $pdf->Text($margx+76,$margy+30,"Day");
  $pdf->Text($margx+89,$margy+30,"Month");
  $pdf->Text($margx+104,$margy+30,"Year");
  $pdf->Line($margx+72,$margy+26,$margx+72,$margy+39); # Linha que divide data
  $pdf->Line($margx+86,$margy+26,$margx+86,$margy+39); # Linha que divide data
  $pdf->Line($margx+100,$margy+26,$margx+100,$margy+39); # Linha que divide data
  $pdf->Line($margx+115,$margy+26,$margx+115,$margy+39); # Linha que divide data

  return $pdf;
}


function EscreveLinha($row,$borda,$wd) {
   global $pdf,$marg; 
   $qtd = count($row);
   for($i=0;$i<$qtd;$i++)    {
      $w = $wd[$i];
      $xx=$pdf->GetX();
      $yy=$pdf->GetY();
      $pdf->MultiCell($w,5,$row[$i],$borda,'L');
      $pdf->SetXY($xx+$w,$yy);                    
   }
   $pdf->SetXY($marg,$yy+10);
}

function EscreveLinhaInterna($row,$borda,$wd) {
   global $pdf,$marg; 
   $qtd = count($row);
   for($i=0;$i<$qtd;$i++)    {
      $w = $wd[$i];
      $xx=$pdf->GetX();
      $yy=$pdf->GetY();
      $pdf->MultiCell($w,4,$row[$i],$borda,'L');
      $pdf->SetXY($xx+$w,$yy);                    
   }
   $pdf->SetXY($marg,$yy+4);
}

function EscreveNoMeio($texto,$x,$y) {
  global $pdf;
  if(strlen($texto)<2) { $texto=$texto." "; }
  $largura=$pdf->GetStringWidth($texto);
  $pdf->SetXY($x-($largura/2),$y);
  $pdf->Cell($largura,0,$texto,0,0,'C');
}

function EscreveNoInicio($texto,$x,$y,$largura) {
  global $pdf;
  $pdf->SetXY($x,$y);
  $pdf->MultiCell($largura,4,$texto,0,'L');
}

?>


