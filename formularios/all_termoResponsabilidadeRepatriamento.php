<?php
include("libForm.php");
require('fpdf.php');

class PDF extends FPDF  {
  function Header(){
    global $marg,$NM_LOGO_EMPRESA,$dirimg,$LOGO_DIR;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    if($LOGO_DIR > 0) {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,170,2,33);
    } else {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,$marg,2,33);
    }
    $this->SetFont('Times','',8);  #  Times 8
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,3,$pagina,0,0,'C');
    //Line break
    $this->Ln(20);
  }
  function Footer() {
    global $NO_RAZAO_SOCIAL;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    $lin1 = "O presente material � titularizado com exclusividade pela $NO_RAZAO_SOCIAL, e qualquer reprodu��o, utiliza��o ou divulga��o do mesmo,";
    $lin2 = "sem expressa autoriza��o da titular, importa em ato il�cito, nos termos da legisla��o pertinente, atrav�s da qual ser�o imputadas as responsabilidades cab�veis.";
    //Position at 1.5 cm from bottom
    $this->SetY(-15);
    $this->SetFont('Times','',8);  #  Times 8
    $w1=$this->GetStringWidth($lin1)+6;
    $this->SetX((210-$w1)/2);
    $this->Cell($w1,0,$lin1,0,0,'C');
    $w2=$this->GetStringWidth($lin2)+6;
    $this->SetX((210-$w2)/2);
    $this->Cell($w2,7,$lin2,0,0,'C');
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,15,$pagina,0,0,'C');
  }
}

$ret = Inicial();
$LOGO_DIR = 0;
$aux_raz = strtoupper($NO_RAZAO_SOCIAL);
if( (!(strstr($aux_raz,'NOBLE') === FALSE)) || (!(strstr($aux_raz,'FUGRO') === FALSE)) ) {
   $LOGO_DIR = 1;
}

$tpSolicitacao=$_POST['tpSolicitacao'];
$nomeForm = "ResponsabilidadeRepatriamento_all";
$marg = 15;
if(strlen($ret)==0) {
  $ret = ValidaDadosFormulario($nomeForm);
}

if(strlen($ret)>0) {
  echo $ret;
} else {
 if($tpSolicitacao=="teste") {
  $NM_LOGO_EMPRESA = "teste_logo.jpg";
  $ret = Formulario();
  $ret->Output();
 } else {
  $id_solicita_visto = $_POST['id_solicita_visto'];
  $ID_TIPO_FORMULARIO= $_POST['ID_TIPO_FORMULARIO'];
  $local = RetornaDIROk($id_solicita_visto );
  $id = RetornaIDformulario();
  $nomeArq = $nomeForm."_".$id.".pdf";
  $ret = GravaArquivoDBSol($id,$idEmpresa,$id_solicita_visto,$nomeArq, $ID_TIPO_FORMULARIO );
 	  
//  $ret = GravaArquivoDB($id,$idEmpresa,$idCandidato,$nomeArq);
  $ret = Formulario();
  $ret->Output();
  $ret->Output($local.$nomeArq,"F");
  $idCandidato = $_REQUEST['idCandidato'];
  $NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
  $idEmpresa = $_REQUEST['idEmpresa'];
  $cd_admin = $_REQUEST['cd_admin'];
  if(strlen(trim($usulogado))==0) { $usulogado=$cd_admin; }
  $gravado = "/arquivos/formularios/$idEmpresa/$idCandidato/".$nomeArq;
  $texto = "Gera��o do formul�rio de Termo de Responsabilidade de Repatriamento. <a href='$gravado'>(VER)</a>";
  insereEvento($idEmpresa,$idCandidato,$NU_SOLICITACAO,$texto);
 }
}

exit;

function Formulario() {
  global $pdf,$marg,$dia,$mes,$mess,$ano,$NO_PROCURADOR,$nmCargoProc,$NO_EMISSOR_PROC,$NU_IDENT_PROC,$NU_CPF_PROC;
  global $NO_PROCURADOR2,$NO_EMAIL_PROC2,$nmCargoProc2,$NO_EMISSOR_PROC2,$NU_IDENT_PROC2,$NU_CPF_PROC2;
  global $NO_RAZAO_SOCIAL,$NO_ENDERECO_EMP,$NO_COMPLEMENTO_EMP,$NO_BAIRRO_EMP,$NO_MUNICIPIO_EMP,$CO_UF_EMP;
  global $nmNacionalProc,$nmEstCivProc,$NO_EMISSOR_PROC,$NU_IDENT_PROC,$NU_CPF_PROC;
  global $NO_EMBARCACAO_PROJETO,$nmPaisEmbarcacao,$NO_CONTRATO_PROJ,$NM_LOGO_EMPRESA;
  global $estilo,$NO_PRIMEIRO_NOME,$NO_NOME_MEIO,$NO_ULTIMO_NOME,$idEmpresa;   # $idEmpresa = 21 -> Petrobras

  $nomedapessoa = $NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME;

  $subsidiariamente = "";
  if($idEmpresa==21) {
    $subsidiariamente = "subsidiariamente";
  }

  if(strlen($nmCargoProc[1])==0) { $nmCargoProc[1]="Procurador(a)"; }
  if(strlen($nmNacionalProc[1])==0) { $nmNacionalProc[1] = "brasileira"; }
  if(strlen($nmEstCivProc[1])>0) { $nmEstCivProc[1] = $nmEstCivProc[1].", "; }

  $pdf=new PDF();
  $pdf->SetMargins(15,10,15);
  $pdf->AliasNbPages();
  $pdf->AddPage();
  $pdf->SetFont('Times','',10);

  $pdf->SetXY($marg,20);

  $pdf->MultiCell(0,5,"",0,'L');
  $pdf->MultiCell(0,5,"AO",0,'L');
  $pdf->MultiCell(0,5,"MINIST�RIO DO TRABALHO E EMPREGO",0,'L');
  $pdf->MultiCell(0,5,"COORDENA��O GERAL DE IMIGRA��O",0,'L');
  $pdf->MultiCell(0,5,"Bras�lia/DF",0,'L');

  $pdf->Ln();
  $pdf->MultiCell(0,5,"Att. Ilmo Sr. Coordenador Geral de Imigra��o.",0,'L');
  $pdf->MultiCell(0,5,"Dr. Aldo Candido Costa Filho",0,'L');

  $pdf->Ln();
  $pdf->MultiCell(0,5,"Refer�ncia: Termo de Compromisso",0,'L');
  $pdf->SetX($marg+34);
  $pdf->MultiCell(0,5,"Despesas M�dicas e/ou hospitalares e Repatria��o",0,'L');

  $pdf->Ln();
  $pdf->Ln();
  $pdf->MultiCell(0,5,$NO_RAZAO_SOCIAL,0,'L');
  $pdf->MultiCell(0,5,$NO_ENDERECO_EMP." - ".$NO_COMPLEMENTO_EMP,0,'L');
  $pdf->MultiCell(0,5,$NO_BAIRRO_EMP." - ".$NO_MUNICIPIO_EMP." - ".$CO_UF_EMP,0,'L');
  $pdf->Ln();
  $pdf->Ln();

  $x = 1;
  $text1a = "Neste ato representada por seu ".$nmCargoProc[1].", Sr(a) ".$NO_PROCURADOR[1].", de nacionalidade ".$nmNacionalProc[1].", ";
  $text1b = $nmEstCivProc[1]."portador(a) da Carteira de Identidade ".$NO_EMISSOR_PROC[1]." ".$NU_IDENT_PROC[1];
  $text1c = " e inscrito(a) no CPF/MF sob o no. ".$NU_CPF_PROC[1].", DECLARA sob as penas da lei, que todas as "; 
  $text1d = "informa��es fornecidas sobre o(a) ".$nomedapessoa." s�o verdadeiras e que, ";
  $text1e = "face a declara��o firmada pelo(a) pr�prio(a), ";
  $text1f = "assume ".$subsidiariamente." a responsabilidade por todas e quaisquer despesas m�dicas e/ou hospitalares durante a ";
  $text1g = "perman�ncia em territ�rio nacional, bem como pela repatria��o dos mesmos ao pa�s de origem, nos termos da lei.";
  $texto1 = $text1a.$text1b.$text1c.$text1d.$text1e.$text1f.$text1g;
  $pdf->Ln();
  $pdf->MultiCell(0,5,$texto1,0,'J');

  
  $pdf->Ln();
  $pdf->Ln();
  $pdf->MultiCell(0,5,"Rio de Janeiro, ".$dia." de ".$mes[$mess]." de ".$ano,0,'C');

  $w1[0] = 120;
  $w1[1] = 90;
  $y = $pdf->GetY()+10;
  $pdf->SetXY($marg,$y);
  $linhaaux = "-----------------------------------------------\n";
  $rowX[0] = $linhaaux.$NO_PROCURADOR[1]."\n".$nmCargoProc[1]."\n".$NO_EMISSOR_PROC[1]." ".$NU_IDENT_PROC[1]."\nCPF/MF ".$NU_CPF_PROC[1];
  if(strlen($NO_PROCURADOR2[1])>0) {
    $rowX[1] = $linhaaux.$NO_PROCURADOR2[1]."\n".$nmCargoProc2[1]."\n".$NO_EMISSOR_PROC2[1]." ".$NU_IDENT_PROC2[1]."\nCPF/MF ".$NU_CPF_PROC2[1];
  } else {
    $rowX[1] = "";
  }
  EscreveLinha($rowX,0,$w1);

  return $pdf;
}

function EscreveLinha($row,$borda,$wd) {
   global $pdf,$marg;
   $qtd = count($row);
   for($i=0;$i<$qtd;$i++)    {
      $w = $wd[$i];
      $xx=$pdf->GetX();
      $yy=$pdf->GetY();
      $pdf->MultiCell($w,4,$row[$i],$borda,'L');
      $pdf->SetXY($xx+$w,$yy);
   }
   $pdf->SetXY($marg,$yy+4);
}

?>
