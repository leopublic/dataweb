<?php
include("libForm.php");
require('fpdf.php');

class PDF extends FPDF  {
  function Header(){
    global $marg,$NM_LOGO_EMPRESA,$dirimg,$LOGO_DIR;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    if($LOGO_DIR > 0) {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,170,2,33);
    } else {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,$marg,2,33);
    }
    $this->SetFont('Times','',8);  #  Times 8
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,3,$pagina,0,0,'C');
    //Line break
    $this->Ln(20);
  }
  function Footer() {
    global $NO_RAZAO_SOCIAL;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    $lin1 = "O presente material � titularizado com exclusividade pela $NO_RAZAO_SOCIAL, e qualquer reprodu��o, utiliza��o ou divulga��o do mesmo,";
    $lin2 = "sem expressa autoriza��o da titular, importa em ato il�cito, nos termos da legisla��o pertinente, atrav�s da qual ser�o imputadas as responsabilidades cab�veis.";
    //Position at 1.5 cm from bottom
    $this->SetY(-15);
    $this->SetFont('Times','',8);  #  Times 8
    $w1=$this->GetStringWidth($lin1)+6;
    $this->SetX((210-$w1)/2);
    $this->Cell($w1,0,$lin1,0,0,'C');
    $w2=$this->GetStringWidth($lin2)+6;
    $this->SetX((210-$w2)/2);
    $this->Cell($w2,7,$lin2,0,0,'C');
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,15,$pagina,0,0,'C');
  }
}

$ret = Inicial();
$LOGO_DIR = 0;
$aux_raz = strtoupper($NO_RAZAO_SOCIAL);
if( (!(strstr($aux_raz,'NOBLE') === FALSE)) || (!(strstr($aux_raz,'FUGRO') === FALSE)) ) {
   $LOGO_DIR = 1;
}

$tpSolicitacao=$_POST['tpSolicitacao'];
$nomeForm = "ContratoTrabalhoDependentes_out";
$marg = 15;
if(strlen($ret)==0) {
  $ret = ValidaDadosFormulario($nomeForm);
}

if(strlen($ret)>0) {
  echo $ret;
} else {
 if($tpSolicitacao=="teste") {
  $NM_LOGO_EMPRESA = "teste_logo.jpg";
  $ret = Formulario();
  $ret->Output();
 } else {
   $id_solicita_visto = $_POST['id_solicita_visto'];
  $NU_CANDIDATO = $_POST['idCandidato'];
  $ID_TIPO_FORMULARIO= $_POST['ID_TIPO_FORMULARIO'];
 	
  $local = RetornaDIRCandidato($id_solicita_visto, $NU_CANDIDATO);
  $id = RetornaIDformulario();
  $nomeArq = $nomeForm."_".$id.".pdf";
  $ret = GravaArquivoDBCand($id,$idEmpresa,$id_solicita_visto, $idCandidato,$nomeArq, $ID_TIPO_FORMULARIO);
  
  $ret = Formulario();
  $ret->Output();
  $ret->Output($local.$nomeArq,"F");
  $idCandidato = $_REQUEST['idCandidato'];
  $NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
  $idEmpresa = $_REQUEST['idEmpresa'];
  $cd_admin = $_REQUEST['cd_admin'];
  if(strlen(trim($usulogado))==0) { $usulogado=$cd_admin; }
  $gravado = "/arquivos/formularios/$idEmpresa/$idCandidato/".$nomeArq;
  $texto = "Gera��o do formul�rio de Contrato de Trabalho Dependentes - Outros. <a href='$gravado'>(VER)</a>";
  insereEvento($idEmpresa,$idCandidato,$NU_SOLICITACAO,$texto);
 }
}

exit;

function Formulario() {
  global $pdf,$marg,$NM_LOGO_EMPRESA,$NO_RAZAO_SOCIAL,$dia,$mes,$mess,$ano;
  global $NO_PROCURADOR,$nmCargoProc,$NO_EMISSOR_PROC,$NU_IDENT_PROC,$NU_CPF_PROC;
  global $NO_PROCURADOR2,$NO_EMAIL_PROC2,$nmCargoProc2,$NO_EMISSOR_PROC2,$NU_IDENT_PROC2,$NU_CPF_PROC2;
  global $NO_ENDERECO_EMP,$NO_COMPLEMENTO_EMP,$NO_BAIRRO_EMP,$NO_MUNICIPIO_EMP,$CO_UF_EMP;
  global $nmNacionalProc,$nmEstCivProc,$NO_EMISSOR_PROC,$NU_IDENT_PROC,$NU_CPF_PROC;
  global $NO_EMBARCACAO_PROJETO,$nmPaisEmbarcacao,$NO_CONTRATO_PROJ,$NM_LOGO_EMPRESA;
  global $NU_DEPENDENTE,$NO_DEPENDENTE,$CO_GRAU_PARENTESCO,$DT_NASCIMENTO_DEP;
  global $CO_NACIONALIDADE_DEP,$NU_PASSAPORTE_DEP,$CO_PAIS_EMISSOR_DEP;
  global $NO_PRIMEIRO_NOME,$NO_NOME_MEIO,$NO_ULTIMO_NOME,$NU_PASSAPORTE,$TE_DESCRICAO_ATIVIDADES;
  global $nmFuncaoCandidato,$nmEstadoCivil,$nmNacional,$nmProfissao,$DT_PRAZO_ESTADA_SOLICITADO;
  global $nmParentescoDep,$nmNacionalDep,$nmEmissorDep,$totdeps,$VA_REMUNERACAO_MENSAL_BRASIL,$DS_BENEFICIOS_SALARIO;

  $txt_acomp = "desacompanhado";

  $endercompleto = $NO_ENDERECO_EMP." - ".$NO_COMPLEMENTO_EMP." - ".$NO_BAIRRO_EMP." - ".$NO_MUNICIPIO_EMP." - ".$CO_UF_EMP;
  $representante = $NO_PROCURADOR[1].", ".$nmCargoProc[1].", identidade ".$NU_IDENT_PROC[1]." (".$NO_EMISSOR_PROC[1].") e CPF/MF ".$NU_CPF_PROC[1];

  $candidato = $NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME.", ".$nmNacional.", ".$nmEstadoCivil.", ".$nmProfissao.", ";
  $candidato = $candidato."passaporte n�mero ".$NU_PASSAPORTE;

  if($totdeps>0) { $txt_acomp = "acompanhado"; }

  $salariomensal = $VA_REMUNERACAO_MENSAL_BRASIL;
  $beneficios = $DS_BENEFICIOS_SALARIO; # (abrir campo livre de 3 linhas para discrever beneficios se houver)

  $pdf=new PDF();
  $pdf->SetMargins(15,10,15);
  $pdf->AliasNbPages();
  $pdf->AddPage();
  $pdf->SetFont('Times','B',10);
  $pdf->SetXY(0,20);
  $titForm1 = "CONTRATO DE TRABALHO POR PRAZO DETERMINADO";
  $titForm2 = "(MODELO II)";
  $w01=$pdf->GetStringWidth($titForm1)+6;
  $w02=$pdf->GetStringWidth($titForm2)+6;
  $pdf->SetX((210-$w01)/2);
  $pdf->Cell($w01,0,$titForm1,0,0,'C');
  $y = $pdf->GetY()+5;
  $pdf->SetY($y);
  if(strlen($titForm2)>0) {
    $pdf->SetX((210-$w02)/2);
    $pdf->Cell($w02,0,$titForm2,0,0,'C');
    $y = $y + 5;
    $pdf->SetY($y);
  }
  $y = $y + 5;
  $pdf->SetXY($marg,$y);
  $pdf->SetFont('Times','',10);

  $pdf->MultiCell(0,5,"CL�USULAS OBRIGAT�RIAS",0,'L');

  $text1 = "A $NO_RAZAO_SOCIAL, situada em $endercompleto, representada por $representante e $candidato, tem contratado o seguinte:";
  $pdf->MultiCell(0,5,"\n".$text1,0,'J');

  $text2a = "CL�USULA PRIMEIRA: O supramencionado contratado na forma da legisla��o em vigor para exercer a fun��o $nmFuncaoCandidato, que abrange as seguintes atividades:";
  $text2b = $TE_DESCRICAO_ATIVIDADES;
  $pdf->MultiCell(0,5,"\n".$text2a,0,'J');
  if(strlen($text2b)>0) {
    $pdf->MultiCell(0,5,$text2b,0,'L');
  }

  $text3 = "CL�USULA SEGUNDA: O prazo deste contrato ter� in�cio na data da chegada do contratado no Brasil e vigorar� por $DT_PRAZO_ESTADA_SOLICITADO.";
  $pdf->MultiCell(0,5,"\n".$text3,0,'J');

  $text4a = "CL�USULA TERCEIRA: Pela execu��o dos servi�os citados, a empresa pagar� sal�rio mensal de R$ $salariomensal";
  $text4b = $beneficios;
  $pdf->MultiCell(0,5,"\n".$text4a,0,'J');
  if(strlen($text4b)>0) {
    $pdf->MultiCell(0,5,$text4b,0,'J');
  }

  $text5a = "CL�USULA QUARTA: O candidato vir� ao Brasil $txt_acomp .";
  $pdf->MultiCell(0,5,"\n".$text5a,0,'J');
  if($totdeps > 0) {
    for($x=1; $x<=$totdeps; $x++) {
      $text5b = $text5b."\n - ".$NO_DEPENDENTE[$x];
    }
    $pdf->MultiCell(0,5,$text5b,0,'L');
  }

  $text6 = "CL�USULA QUINTA: A empresa compromete-se a pagar as despesas relativas a repatria��o do estrangeiro contratado.";
  $pdf->MultiCell(0,5,"\n".$text6,0,'J');

  $text7 = "CL�USULA SEXTA: A repatria��o ao pa�s de origem ser� definitiva ao final do contrato ou ao final da prorroga��o, se houver, ";
  $text7 = $text7."ou no interregno entre os per�odos, caso ocorra distrato, nos termos da Lei, comprometendo-se a contratante a comunicar ";
  $text7 = $text7."o fato, em at� quinze dias, a Coordena��o-Geral de Imigra��o do Minist�rio do Trabalho e Emprego.";
  $pdf->MultiCell(0,5,"\n".$text7,0,'J');

  $text8 = "CL�USULA S�TIMA: O contratado n�o poder� exercer sua atividade profissional para outra empresa, ";
  $text8 = $text8."sen�o �quela que o tiver contratado na oportunidade de concess�o do visto, conforme o disposto na Lei.";
  $pdf->MultiCell(0,5,"\n".$text8,0,'J');

  $text9a = "----------------------------------------------";
  $text9b = $NO_PROCURADOR[1]." - ".$nmCargoProc[1];
  $pdf->MultiCell(0,5,"\n\n\n".$text9a,0,'C');
  $pdf->MultiCell(0,5,$text9b,0,'C');

  if(strlen($NO_PROCURADOR2[1])>0) {
    $text9a = "----------------------------------------------";
    $text9b = $NO_PROCURADOR2[1]." - ".$nmCargoProc2[1];
    $pdf->MultiCell(0,5,"\n\n\n".$text9a,0,'C');
    $pdf->MultiCell(0,5,$text9b,0,'C');
  }

  $text10a = "----------------------------------------------";
  $text10b = $NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME;
  $pdf->MultiCell(0,5,"\n\n\n".$text10a,0,'C');
  $pdf->MultiCell(0,5,$text10b,0,'C');

  return $pdf;
}

function EscreveLinha($row,$borda,$wd) {
   global $pdf,$marg;
   $qtd = count($row);
   for($i=0;$i<$qtd;$i++)    {
      $w = $wd[$i];
      $xx=$pdf->GetX();
      $yy=$pdf->GetY();
      $pdf->MultiCell($w,4,$row[$i],$borda,'L');
      $pdf->SetXY($xx+$w,$yy);
   }
   $pdf->SetXY($marg,$yy+4);
}

?>
