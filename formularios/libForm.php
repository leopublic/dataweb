<?php

include ("../LIB/conecta.php");
include ("../LIB/geral.php");
include ("../LIB/formularios/libDadosCandidato.php");
include ("../LIB/formularios/libDadosEmpresa.php");
// O fpdF utiliza o padrão windows cp1252, por isso o charset da conexão precisa
// set ajustado.
mysql_set_charset('latin1',$mundivisas);

$dirfor = "/home/www/formularios/";
$dirimg = $dirfor . "imagens/";
$dirimg = "./imagens/";
$dirhist = $dirfor . "historico/";

function Inicial() {
	global $mes, $dia, $mess, $ano, $pedido;
	global $idCandidato, $idEmpresa, $idEmbarcacaoProjeto, $idProcurador, $idProcurador2, $NU_EMBARCACAO_PROJETO;
	global $NO_PRIMEIRO_NOME, $NU_SOLICITACAO, $id_solicita_visto;

	$ret = "";
	$mes = pegaMeses();

	$dia = $_POST['dia'];
	$mess = $_POST['mess'];
	$ano = $_POST['ano'];

	if ((strlen($dia) == 0) || (strlen($mess) == 0) || (strlen($ano) == 0)) {
		$dia = date("d");
		$mess = date("m") + 0;
		//$mess = date("m")+1;
		$ano = date("Y");
	}

	$pedido = $_POST['pedido'];
	$idCandidato = $_POST['idCandidato'];
	$idEmpresa = $_POST['idEmpresa'];
	$idEmbarcacaoProjeto = 0;
	$idProcurador = 0 + $_POST['idProcurador'];
	$idProcurador2 = 0 + $_POST['idProcurador2'];
	$id_solicita_visto = $_POST['id_solicita_visto'];

	if (strlen($idCandidato) == 0 || $idCandidato == 0) {
		$ret = "Candidato nao escolhido";
	}

	if (strlen($ret) == 0) {
		$ret = pegaDadosCandidato($idCandidato);
		if (strlen($ret) == 0) {
			$ret = pegaAutorizacao($idEmpresa, $idCandidato, $NU_SOLICITACAO);
		}
		if (strlen($ret) == 0) {
			$ret = pegaDadosDependentes($idCandidato);
		}
		if (strlen($ret) == 0) {
			$idEmbarcacaoProjeto = $NU_EMBARCACAO_PROJETO;
			$ret = pegaDescricoesCandidatos($idEmpresa, $idEmbarcacaoProjeto);
			if ($idEmpresa > 0) {
				if (strlen($ret) == 0) {
					$ret = pegaDadosEmpresa($idEmpresa);
				}
				if (strlen($ret) == 0) {
					$ret = pegaDadosAssociadas($idEmpresa);
				}
				if (strlen($ret) == 0) {
					$ret = pegaDadosAdminPetrobras($idEmpresa);
				}
				if (strlen($ret) == 0) {
					$ret = pegaDadosDiretorPetrobras($idEmpresa);
				}
				if (strlen($ret) == 0) {
					if ($pedido == "listaembarcacoes") {
						$idEmbarcacaoProjeto = "";
					}
					$ret = pegaDadosProjetosEmb($idEmpresa, $idEmbarcacaoProjeto);
				}
				if (strlen($ret) == 0) {
					$ret = pegaDadosProcuradores($idEmpresa, $idProcurador);
				}
				if ((strlen($ret) == 0) && ($idProcurador2 > 0)) {
					$ret = pegaDadosProcuradores2($idEmpresa, $idProcurador2);
				}
			}
		}
	}
	return $ret;
}

function ListaArquivos($idEmpresa, $idCandidato, $idAdmin, $dtInc) {
	global $usulogado;
	$ret = "";
	$sql = "select CD_ARQUIVO,NU_EMPRESA,NU_CANDIDATO,NM_FORMULARIO,CD_ADMIN,DT_INCLUSAO,Date(DT_INCLUSAO) as DIA,Hour(DT_INCLUSAO) as HORA ";
	$sql = $sql . " from ARQUIVOS_FORMULARIOS where NU_EMPRESA=$idEmpresa AND NU_CANDIDATO=$idCandidato";
	if ($idEmpresa > 0) {
		$sql = $sql . " and NU_EMPRESA=$idEmpresa ";
	}
	if ($idCandidato > 0) {
		$sql = $sql . " and NU_CANDIDATO=$idCandidato ";
	}
	if ($idAdmin > 0) {
		$sql = $sql . " and CD_ADMIN=$idAdmin ";
	}
	if (strlen($dtInc) > 0) {
		$sql = $sql . " and DT_INCLUSAO='$dtInc' ";
	}
	$sql = $sql . " order by NU_EMPRESA,NU_CANDIDATO,DT_INCLUSAO ";
	$ret = mysql_query($sql);
	return $ret;
}

function RetornaIDformulario() {
	$id = pegaProximo("ARQUIVOS_FORMULARIOS", "CD_ARQUIVO");
	return $id;
}

function RetornaDIRformulario() {
	global $myfordir;
	$idEmpresa = $_POST['idEmpresa'];
	$idCandidato = $_POST['idCandidato'];
	if (!is_dir($myfordir)) {
		mkdir($myfordir, 0775);
	}
	if ($idEmpresa != '') {
		$updir = $myfordir . "/" . $idEmpresa;
		if (!is_dir($updir)) {
			mkdir($updir, 0775);
		}
		if ($idCandidato != '') {
			$updir = $updir . "/" . $idCandidato;
		} else {
			$updir = $updir . "/geral";
		}
	} else {
		$updir = $myfordir . "/geral";
	}
	if (!is_dir($updir)) {
		mkdir($updir, 0775);
	}
	$ret = $updir . "/";
	return $ret;
}

function RetornaDIRCandidato($pid_solicita_visto, $pNU_CANDIDATO) {
	global $myfordir;
	if (!is_dir($myfordir)) {
		mkdir($myfordir, 0775);
	}
	$updir = $myfordir . "/OS" . $pid_solicita_visto;
	if (!is_dir($updir)) {
		mkdir($updir, 0775);
	}
	$updir = $updir . "/" . $pNU_CANDIDATO;
	if (!is_dir($updir)) {
		mkdir($updir, 0775);
	}
	$ret = $updir . "/";
	return $ret;
}

function RetornaDIROk($id_solicita_visto) {
	global $myfordir;
	if (!is_dir($myfordir)) {
		mkdir($myfordir, 0775);
	}
	$updir = $myfordir . "/OS" . $id_solicita_visto;
	if (!is_dir($updir)) {
		mkdir($updir, 0775);
	}
	$ret = $updir . "/";
	return $ret;
}

function GravaArquivoDB($id, $idEmpresa, $idCandidato, $noArquivo) {
	global $usulogado;
	global $myfordir;
	$ret = "";
	if (strlen($usulogado) == 0) {
		$usulogado = 0 + $_POST['cd_admin'];
	}
	$sql = "insert into ARQUIVOS_FORMULARIOS (CD_ARQUIVO,NU_EMPRESA,NU_CANDIDATO,NM_FORMULARIO,CD_ADMIN,DT_INCLUSAO) ";
	$sql = $sql . "values ($id,$idEmpresa,$idCandidato,'$noArquivo',$usulogado,now())";
	mysql_query($sql);
	if (mysql_errno() > 0) {
		gravaLogErro($sql, mysql_error(), "GERACAO", "ARQUIVOS_FORMULARIOS");
		$ret = mysql_error() . " SQL=$sql";
	} else {
		gravaLog($sql, mysql_error(), "GERACAO", "ARQUIVOS_FORMULARIOS");
	}
	return $ret;
}

function GravaArquivoDBSol($id, $idEmpresa, $id_solicita_visto, $noArquivo, $ID_TIPO_FORMULARIO) {
	global $usulogado;
	global $myfordir;

	$ret = "";
	if (strlen($usulogado) == 0) {
		$usulogado = 0 + $_POST['cd_admin'];
	}
	// Deleta versões anteriores do mesmo form para a mesma OS
	$sql = "select NM_FORMULARIO from ARQUIVOS_FORMULARIOS where id_solicita_visto = " . $id_solicita_visto . ' and ID_TIPO_FORMULARIO=' . $ID_TIPO_FORMULARIO;
	$res = conectaQuery($sql, __FILE__ . '->' . __CLASS__ . '->' . __FUNCTION__);
	if ($rs = mysql_fetch_array($res)) {
		$arq = $myfordir . "/OS" . $id_solicita_visto . '/' . $rs[0];
		if (file_exists($arq)) {
			unlink($arq);
		}
	}
	$sql = "delete from ARQUIVOS_FORMULARIOS where id_solicita_visto = " . $id_solicita_visto . ' and ID_TIPO_FORMULARIO=' . $ID_TIPO_FORMULARIO;
	executeQuery($sql, __FILE__ . '->' . __CLASS__ . '->' . __FUNCTION__);
	$sql = "insert into ARQUIVOS_FORMULARIOS (CD_ARQUIVO,NU_EMPRESA,id_solicita_visto,NM_FORMULARIO,CD_ADMIN,DT_INCLUSAO,ID_TIPO_FORMULARIO) ";
	$sql = $sql . "values ($id,$idEmpresa,$id_solicita_visto,'$noArquivo',$usulogado,now(),$ID_TIPO_FORMULARIO)";
	//print 'SQL='.$sql;
	executeQuery($sql, __FILE__ . '->' . __CLASS__ . '->' . __FUNCTION__);
	return true;
}

function GravaArquivoDBCand($id, $idEmpresa, $id_solicita_visto, $pNU_CANDIDATO, $noArquivo, $ID_TIPO_FORMULARIO) {
	global $usulogado;
	global $myfordir;

	$ret = "";
	if (strlen($usulogado) == 0) {
		$usulogado = 0 + $_POST['cd_admin'];
	}
	// Deleta versões anteriores do mesmo form para a mesma OS
	$sql = "select NM_FORMULARIO from ARQUIVOS_FORMULARIOS ";
	$sql .= " where id_solicita_visto = " . $id_solicita_visto;
	$sql .= "   and NU_CANDIDATO=" . $pNU_CANDIDATO;
	$sql .= "   and ID_TIPO_FORMULARIO=" . $ID_TIPO_FORMULARIO;
	$res = conectaQuery($sql, __FILE__ . '->' . __CLASS__ . '->' . __FUNCTION__);
	if ($rs = mysql_fetch_array($res)) {
		$arq = RetornaDIRCandidato($id_solicita_visto, $pNU_CANDIDATO) . $rs[0];
		if (file_exists($arq)) {
			unlink($arq);
		}
	}
	$sql = "delete from ARQUIVOS_FORMULARIOS ";
	$sql .= "where id_solicita_visto = " . $id_solicita_visto;
	$sql .= "  and NU_CANDIDATO=" . $pNU_CANDIDATO;
	$sql .= "  and ID_TIPO_FORMULARIO=" . $ID_TIPO_FORMULARIO;
	executeQuery($sql, __FILE__ . '->' . __CLASS__ . '->' . __FUNCTION__);
	$sql = "insert into ARQUIVOS_FORMULARIOS (CD_ARQUIVO,NU_EMPRESA,id_solicita_visto,NU_CANDIDATO, NM_FORMULARIO,CD_ADMIN,DT_INCLUSAO,ID_TIPO_FORMULARIO) ";
	$sql = $sql . "values ($id,$idEmpresa,$id_solicita_visto, $pNU_CANDIDATO, '$noArquivo',$usulogado,now(),$ID_TIPO_FORMULARIO)";
	//print 'SQL='.$sql;
	executeQuery($sql, __FILE__ . '->' . __CLASS__ . '->' . __FUNCTION__);
	return true;
}

function ValidaDadosFormulario($formName) {
	$ret = "";

	return $ret;
}
