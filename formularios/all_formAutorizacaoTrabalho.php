<?php

include("libForm.php");
require('fpdf.php');

class PDF extends FPDF {

    function Header() {
        global $marg, $NM_LOGO_EMPRESA, $dirimg, $LOGO_DIR;
//    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
        if ($LOGO_DIR > 0) {
            $this->Image($dirimg . $NM_LOGO_EMPRESA, 170, 2, 33);
        } else {
            $this->Image($dirimg . $NM_LOGO_EMPRESA, $marg, 2, 33);
        }
        $this->SetFont('Times', '', 8);  #  Times 8
//    $w=$this->GetStringWidth($pagina)+6;
//    $this->SetX((210-$w)/2);
//    $this->Cell($w,3,$pagina,0,0,'C');
//    //Line break
//    $this->Ln(20);
    }

    function Footer() {
        global $NO_RAZAO_SOCIAL;
        $pagina = 'P�gina ' . $this->PageNo() . '/{nb}';
        $lin1 = "O presente material � titularizado com exclusividade pela $NO_RAZAO_SOCIAL, e qualquer reprodu��o, utiliza��o ou divulga��o do mesmo,";
        $lin2 = "sem expressa autoriza��o da titular, importa em ato il�cito, nos termos da legisla��o pertinente, atrav�s da qual ser�o imputadas as responsabilidades cab�veis.";
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        $this->SetFont('Times', '', 8);  #  Times 8
        $w1 = $this->GetStringWidth($lin1) + 6;
        $this->SetX((210 - $w1) / 2);
        $this->Cell($w1, 0, $lin1, 0, 0, 'C');
        $w2 = $this->GetStringWidth($lin2) + 6;
        $this->SetX((210 - $w2) / 2);
        $this->Cell($w2, 7, $lin2, 0, 0, 'C');
        $w = $this->GetStringWidth($pagina) + 6;
        $this->SetX((210 - $w) / 2);
        $this->Cell($w, 15, $pagina, 0, 0, 'C');
    }

}

/**
 * ROtina principal
 * @var [type]
 */

$ret = Inicial();
$LOGO_DIR = 0;
$aux_raz = strtoupper($NO_RAZAO_SOCIAL);
if ((!(strstr($aux_raz, 'NOBLE') === FALSE)) || (!(strstr($aux_raz, 'FUGRO') === FALSE))) {
    $LOGO_DIR = 1;
}

$tpSolicitacao = $_POST['tpSolicitacao'];
$nomeForm = "AutorizacaoTrabalho";
$marg = 15;
if (strlen($ret) == 0) {
    $ret = ValidaDadosFormulario($nomeForm);
}
if (strlen($ret) > 0) {
    echo $ret;
} else {
    if ($tpSolicitacao == "teste") {
        $NM_LOGO_EMPRESA = "teste_logo.jpg";
        $ret = Formulario();
        $ret->Output();
    } else {

        $id_solicita_visto = $_POST['id_solicita_visto'];
        $NU_CANDIDATO = $_POST['idCandidato'];
        $ID_TIPO_FORMULARIO = $_POST['ID_TIPO_FORMULARIO'];

        $local = RetornaDIRCandidato($id_solicita_visto, $NU_CANDIDATO);
        $id = RetornaIDformulario();
        $nomeArq = $nomeForm . "_" . $id . ".pdf";
        $ret = GravaArquivoDBCand($id, $idEmpresa, $id_solicita_visto, $idCandidato, $nomeArq, $ID_TIPO_FORMULARIO);

        $ret = Formulario();
        $ret->Output();
        $ret->Output($local . $nomeArq, "F");
        $idCandidato = $_REQUEST['idCandidato'];

        $idEmpresa = $_REQUEST['idEmpresa'];
        $cd_admin = $_REQUEST['cd_admin'];
        if (strlen(trim($usulogado)) == 0) {
            $usulogado = $cd_admin;
        }
        $gravado = "/arquivos/formularios/$idEmpresa/$idCandidato/" . $nomeArq;
        $texto = "Gera��o do formul�rio de Autoriza��o de Trabalho. <a href='$gravado'>(VER)</a>";
        insereEvento($idEmpresa, $idCandidato, $NU_SOLICITACAO, $texto);
    }
}

exit;

function Formulario() {
    global $pdf, $marg, $nomeempresa;
    global $dia, $mes, $mess, $ano, $marg, $NO_PROCURADOR, $NO_EMAIL_PROC, $nmCargoProc, $NO_EMISSOR_PROC, $NU_IDENT_PROC, $NU_CPF_PROC;
    global $NO_PROCURADOR2, $NO_EMAIL_PROC2, $nmCargoProc2, $NO_EMISSOR_PROC2, $NU_IDENT_PROC2, $NU_CPF_PROC2;
    global $NO_PRIMEIRO_NOME, $NO_NOME_MEIO, $NO_ULTIMO_NOME, $NO_RAZAO_SOCIAL, $CO_ATIVIDADE_ECONOMICA, $NO_MUNICIPIO_EMP;
    global $NO_ENDERECO_EMP, $NO_COMPLEMENTO_EMP, $NO_BAIRRO_EMP, $CO_UF_EMP, $NU_CEP_EMP, $NU_DDD_EMP, $NU_TELEFONE_EMP;
    global $NO_EMAIL_EMPRESA, $NU_CNPJ, $nmAutorizacao1, $NO_PAI, $NO_MAE, $CO_SEXO, $nmEstadoCivil, $DT_NASCIMENTO;
    global $nmEscolaridade, $nmProfissao, $nmNacional, $NU_PASSAPORTE, $DT_VALIDADE_PASSAPORTE, $nmPaisPassaporte;
    global $nmFuncaoCandidato, $NU_CBO, $NU_RNE, $idEmbarcacaoProjeto, $idEmpresa, $NU_SERVICO;
    global $totdeps, $NO_DEPENDENTE, $nmParentescoDep, $DT_NASCIMENTO_DEP, $nmNacionalDep, $NU_PASSAPORTE_DEP, $CO_CLASSIFICACAO_VISTO;
    global $nmEmissorDep, $DT_PRAZO_ESTADA, $nmReparticao, $NM_LOGO_EMPRESA, $DT_PRAZO_AUTORIZACAO_MTE, $DS_LOCAL_TRABALHO;
    global $NO_EMBARCACAO_PROJETO, $ID_TIPO_EMBARCACAO_PROJETO, $NO_MUNICIPIO_PROJ;
    global $estilo, $DT_PRAZO_ESTADA_SOLICITADO;

    if ($CO_SEXO == "M") {
        $CO_SEXO = "Masculino";
    } elseif ($CO_SEXO == "F") {
        $CO_SEXO = "Feminino";
    }

    if ($NU_SERVICO == 56 || $NU_SERVICO == 57) {
        $tipoVisto = "Permanente";
        $prazo_contrato = "Indeterminado";
    } else {
        $tipoVisto = "Tempor�rio";
        //$prazo_contrato = $DT_PRAZO_AUTORIZACAO_MTE;
        $prazo_contrato = $DT_PRAZO_ESTADA_SOLICITADO;
        if ($CO_CLASSIFICACAO_VISTO == 1) {
            $tipoVisto = "Permanente";
            if (strlen(trim($prazo_contrato)) < 2) {
                $prazo_contrato = "Indeterminado";
            }
        }
    }

    if ((strlen(trim($DS_LOCAL_TRABALHO)) == 0) || ($idEmpresa == 21)) {
#    $DS_LOCAL_TRABALHO = "A bordo da Embarca��o ".pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa).", por toda costa brasileira";
        if ($idEmbarcacaoProjeto == 199) {
            $DS_LOCAL_TRABALHO = "Os treinamentos ser�o realizados nas bases operacionais dos prestadores de servi�os, bem como nas sedes da Petrobr�s no Centro do Rio e em Maca�.";
        } else {
            if (($ID_TIPO_EMBARCACAO_PROJETO == "2") || ($ID_TIPO_EMBARCACAO_PROJETO == "3")) {
                $DS_LOCAL_TRABALHO = $NO_EMBARCACAO_PROJETO[1] . " (" . $NO_MUNICIPIO_PROJ[1] . ")";
            } else {
                $DS_LOCAL_TRABALHO = "A bordo da Embarca��o " . $NO_EMBARCACAO_PROJETO[1] . ", por toda costa brasileira";
            }
        }
    }

    $w1[0] = 93;
    $w1[1] = 92;
    $w2[0] = 145;
    $w2[1] = 40;
    $w3[0] = 15;
    $w3[1] = 20;
    $w3[2] = 45;
    $w3[3] = 65;
    $w3[4] = 40;
    $w4[0] = 49;
    $w4[1] = 44;
    $w4[2] = 40;
    $w4[3] = 52;
    $w5[0] = 93;
    $w5[1] = 92;
    $w6[0] = 148;
    $w6[1] = 37;
    $w8[0] = 46;
    $w8[1] = 47;
    $w8[2] = 92;

    $pdf = new PDF();
    // $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    // $pdf->SetFont('DejaVu','',14);
    $pdf->SetLeftMargin(15);
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->SetFont('Times', 'B', 12);
    $pdf->SetXY(0, 20);
    $lin1 = "FORMUL�RIO DE REQUERIMENTO DE AUTORIZA��O DE TRABALHO";
    $w0 = $pdf->GetStringWidth($lin1) + 6;
    $pdf->SetX((210 - $w0) / 2);
    $pdf->Cell($w0, 0, $lin1, 0, 0, 'C');
    $pdf->SetFont('Times', '', 10);

    $y = $pdf->GetY() + 5;
    $pdf->SetXY($marg, $y);
    $row0[0] = "";
    $row0[1] = "PROCESSO No";
    EscreveLinha($row0, 1, $w1);

    $row1[0] = "1. Requerente\n" . $NO_RAZAO_SOCIAL;
    $row1[1] = "2. Ativ. Econ�mica\n" . $CO_ATIVIDADE_ECONOMICA;
    $pdf->SetX($marg);
    EscreveLinha($row1, 1, $w2);

    $row2[0] = "3. Endere�o\n" . $NO_ENDERECO_EMP . " - " . $NO_COMPLEMENTO_EMP . " - " . $NO_BAIRRO_EMP;
    $row2[1] = "4. Cidade\n" . $NO_MUNICIPIO_EMP;
    $pdf->SetX($marg);
    EscreveLinha($row2, 1, $w2);

    $row3[0] = "5. UF\n" . $CO_UF_EMP;
    $row3[1] = "6. CEP\n" . $NU_CEP_EMP;
    $row3[2] = "7. Telefone\n(" . $NU_DDD_EMP . ") " . $NU_TELEFONE_EMP;
    $row3[3] = "8. E-mail\n" . $NO_EMAIL_EMPRESA;
    $row3[4] = "9. CNPJ\n" . $NU_CNPJ;
    $pdf->SetX($marg);
    EscreveLinha($row3, 1, $w3);

    $lin1 = "VEM REQUERER, COM FUNDAMENTO LEGAL";
    $w0 = $pdf->GetStringWidth($lin1) + 6;
    $y = $pdf->GetY() + 5;
    $pdf->SetXY((210 - $w0) / 2, $y);
    $pdf->Cell($w0, 0, $lin1, 0, 0, 'C');

    $y = $pdf->GetY() + 5;
    $pdf->SetXY($marg, $y);
    $pdf->MultiCell(0, 5, "10. Lei/Decreto/Resolu��o \n" . $nmAutorizacao1, 1, 'J');

    $y = $pdf->GetY() + 3;
    $pdf->SetXY($marg, $y);
    $pdf->MultiCell(0, 5, "AUTORIZA��O DE TRABALHO PARA O ESTRANGEIRO ABAIXO QUALIFICADO", 0, 'C');

    $y = $pdf->GetY() + 3;
    $pdf->SetXY($marg, $y);
    $pdf->MultiCell(0, 5, "11. Nome\n" . $NO_PRIMEIRO_NOME . " " . $NO_NOME_MEIO . " " . $NO_ULTIMO_NOME, 1, 'J');

    $pdf->SetX($marg);
    $pdf->MultiCell(0, 5, "12. Filia��o\nPai: " . $NO_PAI . "\nM�e: " . $NO_MAE, 1, 'J');

    $row4[0] = "13. Sexo\n" . $CO_SEXO;
    $row4[1] = "14. Estado Civil\n" . $nmEstadoCivil;
    $row4[2] = "15. Data de nascimento\n" . $DT_NASCIMENTO;
    $row4[3] = "16. Escolaridade\n" . $nmEscolaridade;
    $pdf->SetX($marg);
    EscreveLinha($row4, 1, $w4);

    $row5[0] = "17. Profiss�o\n" . $nmProfissao;
    $row5[1] = "18. Nacionalidade\n" . $nmNacional;
    $pdf->SetX($marg);
    EscreveLinha($row5, 1, $w5, 10);

    $pdf->SetX($marg);
    $pdf->MultiCell(0, 5, "19. Documento de viagem\nPassaporte no.: " . $NU_PASSAPORTE . " - Validade: " . $DT_VALIDADE_PASSAPORTE . " - Governo Emissor: " . $nmPaisPassaporte, 1, 'J');


    $row6[0] = "20. Fun��o no Brasil\n" . html_entity_decode($nmFuncaoCandidato);
    $row6[1] = "21. CBO\n " . $NU_CBO;
    $pdf->SetX($marg);
    EscreveLinha($row6, 1, $w6);

    $pdf->SetX($marg);
    $pdf->MultiCell(0, 5, "22. Local de exerc�cio\n" . $DS_LOCAL_TRABALHO, 1, 'J');

    if ($totdeps > 0) {
        $altura = ($totdeps * 5) + 3;
    } else {
        $altura = 8;
    }
    $y1 = $pdf->GetY();
    $pdf->SetX($marg);
    $pdf->MultiCell(0, $altura, " ", 1, 'T');
    $y2 = $pdf->GetY();
    $pdf->SetXY($marg, $y1);
    $row7[0] = "Dependentes Legais";
    $row7[1] = "Parentesco";
    $row7[2] = "Data nasc.";
    $row7[3] = "Nacionalidade";
    $row7[4] = "Doc. de viagem";
    $w7[0] = 70;
    $w7[1] = 25;
    $w7[2] = 20;
    $w7[3] = 30;
    $w7[4] = 40;
    EscreveLinhaInterna($row7, 0, $w7);
    if ($totdeps > 0) {
        for ($a = 1; $a <= $totdeps; $a++) {
            $row7[0] = $NO_DEPENDENTE[$a];
            $row7[1] = $nmParentescoDep[$a];
            $row7[2] = $DT_NASCIMENTO_DEP[$a];
            $row7[3] = $nmNacionalDep[$a];
            $row7[4] = "Passaporte: " . $NU_PASSAPORTE_DEP[$a];
            EscreveLinhaInterna($row7, 0, $w7);
        }
    } else {
        $row7[0] = "---";
        $row7[1] = "---";
        $row7[2] = "---";
        $row7[3] = "---";
        $row7[4] = "---";
        EscreveLinhaInterna($row7, 0, $w7);
    }
    $pdf->SetXY($marg, $y2);

    $row8[0] = "24. Tipo de visto\n" . $tipoVisto;
    $row8[1] = "25. Prazo\n" . $prazo_contrato;
    if ($nmReparticao == '') {
        $nmReparticao = '   ';
    }
    $row8[2] = "26. Reparti��o Consular Brasileira no Exterior\n" . $nmReparticao;
    EscreveLinha($row8, 1, $w8);

    $row9[0] = "27. Procurador\n" . $NO_PROCURADOR[1];
    $row9[1] = "28. E-mail\n" . $NO_EMAIL_PROC[1];
    $pdf->SetX($marg);
    EscreveLinha($row9, 1, $w1);

    $y = $pdf->GetY() + 5;
    $pdf->SetXY($marg, $y);
    $pdf->MultiCell(0, 5, "Termo em que pede deferimento", 0, 'L');

    $y = $pdf->GetY();
    $pdf->SetXY($marg, $y);
    $pdf->MultiCell(0, 5, "Rio de Janeiro, " . $dia . " de " . $mes[$mess] . " de " . $ano, 0, 'J');

    $y = $pdf->GetY();
    $pdf->SetXY($marg, $y);
    $pdf->MultiCell(0, 5, "Atenciosamente,", 0, 'J');

    $linhaaux = "-----------------------------------------------\n";
    $y = $pdf->GetY() + 10;
    $pdf->SetXY($marg, $y);
    $rowX[0] = $linhaaux . $NO_PROCURADOR[1] . "\n" . $nmCargoProc[1] . "\n" . $NO_EMISSOR_PROC[1] . " " . $NU_IDENT_PROC[1] . "\nCPF/MF " . $NU_CPF_PROC[1];
    if (strlen($NO_PROCURADOR2[1]) > 0) {
        $rowX[1] = $linhaaux . $NO_PROCURADOR2[1] . "\n" . $nmCargoProc2[1] . "\n" . $NO_EMISSOR_PROC2[1] . " " . $NU_IDENT_PROC2[1] . "\nCPF/MF " . $NU_CPF_PROC2[1];
    } else {
        $rowX[1] = "";
    }
    EscreveLinha($rowX, 0, $w1);

    return $pdf;
}

function EscreveLinha($row, $borda, $wd, $altura = 10) {
    global $pdf, $marg;
    $qtd = count($row);
    for ($i = 0; $i < $qtd; $i++) {
        $w = $wd[$i];
        $xx = $pdf->GetX();
        $yy = $pdf->GetY();
        $pdf->MultiCell($w, 5, $row[$i], $borda, 'L');
        $pdf->SetXY($xx + $w, $yy);
    }
    $pdf->SetXY($marg, $yy + $altura);
}

function EscreveLinhaInterna($row, $borda, $wd) {
    global $pdf, $marg;
    $qtd = count($row);
    for ($i = 0; $i < $qtd; $i++) {
        $w = $wd[$i];
        $xx = $pdf->GetX();
        $yy = $pdf->GetY();
        $pdf->MultiCell($w, 4, $row[$i], $borda, 'L');
        $pdf->SetXY($xx + $w, $yy);
    }
    $pdf->SetXY($marg, $yy + 4);
}

?>
