<?php
include("libForm.php");
require('fpdf.php');

class PDF extends FPDF  {
  function Header(){
    global $marg,$NM_LOGO_EMPRESA,$dirimg,$LOGO_DIR;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    if($LOGO_DIR > 0) {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,170,2,33);
    } else {
      $this->Image($dirimg.$NM_LOGO_EMPRESA,$marg,2,33);
    }
    $this->SetFont('Times','',8);  #  Times 8
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,3,$pagina,0,0,'C');
    //Line break
    $this->Ln(20);
  }
  function Footer() {
    global $NO_RAZAO_SOCIAL;
    $pagina = 'P�gina '.$this->PageNo().'/{nb}';
    $lin1 = "O presente material � titularizado com exclusividade pela empresa, e qualquer reprodu��o, utiliza��o ou divulga��o do mesmo,";
    $lin2 = "sem expressa autoriza��o da titular, importa em ato il�cito, nos termos da legisla��o pertinente, atrav�s da qual ser�o imputadas as responsabilidades cab�veis.";
    //Position at 1.5 cm from bottom
    $this->SetY(-15);
    $this->SetFont('Times','',8);  #  Times 8
    $w1=$this->GetStringWidth($lin1)+6;
    $this->SetX((210-$w1)/2);
    $this->Cell($w1,0,$lin1,0,0,'C');
    $w2=$this->GetStringWidth($lin2)+6;
    $this->SetX((210-$w2)/2);
    $this->Cell($w2,7,$lin2,0,0,'C');
    $w=$this->GetStringWidth($pagina)+6;
    $this->SetX((210-$w)/2);
    $this->Cell($w,15,$pagina,0,0,'C');
  }
}

$ret = Inicial();
$LOGO_DIR = 0;
$aux_raz = strtoupper($NO_RAZAO_SOCIAL);
if( (!(strstr($aux_raz,'NOBLE') === FALSE)) || (!(strstr($aux_raz,'FUGRO') === FALSE)) ) {
   $LOGO_DIR = 1;
}
$tpSolicitacao=$_POST['tpSolicitacao'];
$nomeForm = "DadosCadastrais_all";
$marg = 15;
if(strlen($ret)==0) {
  $ret = ValidaDadosFormulario($nomeForm);
}
if(strlen($ret)>0) {
  echo $ret;
} else {
 if($tpSolicitacao=="teste") {
  $NM_LOGO_EMPRESA = "teste_logo.jpg";
  $ret = Formulario();
  $ret->Output();
 } else {
   $id_solicita_visto = $_POST['id_solicita_visto'];
  $NU_CANDIDATO = $_POST['idCandidato'];
  $ID_TIPO_FORMULARIO= $_POST['ID_TIPO_FORMULARIO'];
 	
  $local = RetornaDIRCandidato($id_solicita_visto, $NU_CANDIDATO);
  $id = RetornaIDformulario();
  $nomeArq = $nomeForm."_".$id.".pdf";
  $ret = GravaArquivoDBCand($id,$idEmpresa,$id_solicita_visto, $idCandidato,$nomeArq, $ID_TIPO_FORMULARIO);
    $ret = Formulario();
  $ret->Output();
  $ret->Output($local.$nomeArq,"F");
  $idCandidato = $_REQUEST['idCandidato'];
  $NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
  $idEmpresa = $_REQUEST['idEmpresa'];
  $cd_admin = $_REQUEST['cd_admin'];
  if(strlen(trim($usulogado))==0) { $usulogado=$cd_admin; }
  $gravado = "/arquivos/formularios/$idEmpresa/$idCandidato/".$nomeArq;
  $texto = "Gera��o do formul�rio de Dados Cadastrais. <a href='$gravado'>(VER)</a>";
  insereEvento($idEmpresa,$idCandidato,$NU_SOLICITACAO,$texto);
 }
}

exit;

function Formulario() {
  global $pdf,$marg,$nomeempresa; 
  global $dia,$mes,$mess,$ano,$NO_PROCURADOR,$NO_EMAIL_PROC,$nmCargoProc,$NO_EMISSOR_PROC,$NU_IDENT_PROC,$NU_CPF_PROC;
  global $NO_PROCURADOR2,$NO_EMAIL_PROC2,$nmCargoProc2,$NO_EMISSOR_PROC2,$NU_IDENT_PROC2,$NU_CPF_PROC2;
  global $NO_RAZAO_SOCIAL,$TE_OBJETO_SOCIAL,$VA_CAPITAL_INICIAL,$VA_CAPITAL_ATUAL,$DT_CONSTITUICAO_EMPRESA;
  global $DT_ALTERACAO_CONTRATUAL,$NO_EMPRESA_ESTRANGEIRA,$NO_ASSOCIADA,$NU_CNPJ_ASSOC,$nmNacionalAssoc;
  global $NO_CAPITAL_VOTANTE,$NO_CAPITAL_TOTAL,$VA_INVESTIMENTO_ESTRANGEIRO,$DT_INVESTIMENTO_ESTRANGEIRO;
  global $NM_ADMIN1,$NM_CARGO1,$NM_ADMIN2,$NM_CARGO2,$QT_EMPREGADOS,$QT_EMPREGADOS,$QT_EMPREGADOS_ESTRANGEIROS;
  global $NO_PRIMEIRO_NOME,$NO_NOME_MEIO,$NO_ULTIMO_NOME,$nmEscolaridade,$VA_RENUMERACAO_MENSAL;
  global $VA_REMUNERACAO_MENSAL_BRASIL,$TE_TRABALHO_ANTERIOR_BRASIL,$DS_JUSTIFICATIVA_PROJ,$NO_EMBARCACAO_PROJETO;
  global $totEmbProj,$totAssoc,$totAdmin,$totDiretor,$estilo,$bandeira,$TE_DESCRICAO_ATIVIDADES,$DS_JUSTIFICATIVA_CAND;
  global $NM_LOGO_EMPRESA,$DS_SALARIO_EXTERIOR,$idEmpresa,$DT_CADASTRO_BC;

  $w1[0] = 93;
  $w1[1] = 92;
  $w2[0] = 60;
  $w2[1] = 30;
  $w2[2] = 35;
  $w2[3] = 30;
  $w2[4] = 30;

  $justificativa = trim($DS_JUSTIFICATIVA_CAND);
  if(strlen($justificativa) < 5) {
    $justificativa = $DS_JUSTIFICATIVA_PROJ[1];
    $justificativa = str_replace("<EMBARCACAO>",$NO_EMBARCACAO_PROJETO[1],$justificativa);
    $justificativa = str_replace("<BANDEIRA>",$bandeira[1],$justificativa);
  }

  $salarioexterior = "Caso o estrangeiro continue a perceber remunera��o no exterior, informar a mesma e oferecer a tributa��o no Brasil, conforme determina a Secretaria da Receita Federal.";
  if(strlen(trim($DS_SALARIO_EXTERIOR))>0) {
     $salarioexterior = $salarioexterior."\n".trim($DS_SALARIO_EXTERIOR);
  } else if($idEmpresa==21) {
     $salarioexterior = $salarioexterior."\n"."Declaramos que o estrangeiro continuar� recebendo no exterior o valor supracitado, e est� ciente da obrigatoriedade de oferecer tributa��o no Brasil dos rendimentos auferidos no exterior, conforme Instru��o Normativa 208/02 da Secretaria da Receita Federal do Minist�rio da Fazenda.";
  }

  $pdf=new PDF();
#  $pdf->SetMargins(15,10,5);
  $pdf->SetLeftMargin(15);
  $pdf->AliasNbPages();
  $pdf->AddPage();
  $pdf->SetFont('Times','B',10);
  $pdf->SetXY(0,20);
  $lin1 = "FORMUL�RIO DA REQUERENTE E DO CANDIDATO";
  $w0=$pdf->GetStringWidth($lin1)+6;
  $pdf->SetX((210-$w0)/2);
  $pdf->Cell($w0,0,$lin1,0,0,'C');
  $pdf->SetXY($marg,27);
  $pdf->Cell(0,0,'DA EMPRESA',0,0);

  $pdf->SetLineWidth(.3);
  $pdf->SetFont('Times','',8);
  $pdf->SetXY($marg,32);
  $pdf->MultiCell(0,5,"1. Raz�o/Denomina��o Social:\n ".$NO_RAZAO_SOCIAL,1,'L');
  $pdf->SetX($marg);
  $pdf->MultiCell(0,5,"2. Objeto Social:\n".$TE_OBJETO_SOCIAL,1,'J');

  $pdf->SetX($marg);
  $row1[0] = "3. Capital Social inicial:\n ".$VA_CAPITAL_INICIAL;
  $row1[1] = "4. Capital Social atual:\n ".$VA_CAPITAL_ATUAL;
  EscreveLinha($row1,1,$w1);

  $pdf->SetX($marg);
  $row2[0] = "5. Data da constitui��o:\n ".$DT_CONSTITUICAO_EMPRESA;
  $row2[1] = "6. Data da �ltima altera��o contratual:\n ".$DT_ALTERACAO_CONTRATUAL;
  EscreveLinha($row2,1,$w1);

  $altura = ($totAssoc*5) + 20;
  $pdf->SetX($marg);
  $pdf->MultiCell(0,5,"7. Pessoa(s) jur�dica(s) estrangeira(s) associada(s):\n ".$NO_EMPRESA_ESTRANGEIRA,1,'J');
  $y1= $pdf->GetY();
  $pdf->MultiCell(0,$altura," ",1,'T');
  $pdf->SetXY($marg,$y1+3);
  $pdf->MultiCell(0,0,"8. Rela��o das principais associadas, quando se tratar de Sociedade An�nima:",0,'T');
  $pdf->SetXY($marg,$y1+5);
  $row3[0] = "Nome";
  $row3[1] = "CNPJ";
  $row3[2] = "Nacionalidade";
  $row3[3] = "Capital Votante";
  $row3[4] = "Capital Total";
  EscreveLinhaInterna($row3,0,$w2);
  if($totAssoc>0) {
     for($a=1;$a<=$totAssoc;$a++) {
       $row3[0] = $NO_ASSOCIADA[$a];
       $row3[1] = $NU_CNPJ_ASSOC[$a];
       $row3[2] = $nmNacionalAssoc[$a];
       $row3[3] = $NO_CAPITAL_VOTANTE[$a];
       $row3[4] = $NO_CAPITAL_TOTAL[$a];
       EscreveLinhaInterna($row3,0,$w2);
    }
  }

  $pdf->SetXY($marg,$y1+$altura);
  $pdf->MultiCell(0,5,"9. Valor do investimento de capital estrangeiro:\n ".$VA_INVESTIMENTO_ESTRANGEIRO,1,'L');
  $pdf->SetX($marg);
  $pdf->MultiCell(0,5,"10. Data do �ltimo investimento:\n ".$DT_INVESTIMENTO_ESTRANGEIRO,1,'L');
  $pdf->SetX($marg);
  $pdf->MultiCell(0,5,"11. Data de registro no Banco Central do Brasil:\n".$DT_CADASTRO_BC,1,'L');

  $altura = ($totDiretor*5) + ($totAdmin*5) + 5;
  if($altura < 60) { $altura = 60; }
  $x1 = $pdf->GetX();
  $y1= $pdf->GetY();
  $pdf->MultiCell(0,$altura," ",1,'T');
  $pdf->SetXY($x1,$y1+3);
  $pdf->MultiCell(0,0,"12. Administradores: nome - cargo",0,'L');
  $pdf->SetXY($x1,$y1+8);
  $pdf->SetFont('Times','U',8);
  $pdf->MultiCell(0,0,"Conselho Administrativo",0,'L');
  $pdf->SetFont('Times','',8);
  $pdf->SetXY($x1,$y1+11);
  if($totAdmin>0) {
     for($a=1;$a<=$totAdmin;$a++) {
       $row4[0] = $NM_ADMIN1[$a];
       $row4[1] = $NM_CARGO1[$a];
       EscreveLinhaInterna($row4,0,$w1);
    }
  }
  $x2 = $pdf->GetX();
  $y2= $pdf->GetY();
  $pdf->SetXY($x2,$y2+3);
  $pdf->SetFont('Times','U',8);
  $pdf->MultiCell(0,0,"Diretoria Executiva",0,'L');
  $pdf->SetXY($x2,$y2+7);
  $pdf->SetFont('Times','',8);
  if($totDiretor>0) {
     for($a=1;$a<=$totDiretor;$a++) {
       $row4[0] = $NM_ADMIN2[$a];
       $row4[1] = $NM_CARGO2[$a];
       EscreveLinhaInterna($row4,0,$w1);
    }
  }

  $pdf->AddPage();

  $pdf->SetXY($marg,25);
  $pdf->MultiCell(0,5,"13. N�mero atual de empregados:\n ".$QT_EMPREGADOS,1,'L');
  $row5[0] = "13.1 Brasileiros:\n ".($QT_EMPREGADOS-$QT_EMPREGADOS_ESTRANGEIROS);
  $row5[1] = "13.2 Estrangeiros:\n ".$QT_EMPREGADOS_ESTRANGEIROS;
  EscreveLinha($row5,1,$w1);

#  $pdf->SetXY($marg,45);
#  $pdf->MultiCell(0,200," ",1,'T');
  $pdf->SetXY($marg,45);
  $texto0 = "14. Justificativa para a contrata��o do estrangeiro:\n".$justificativa."\n\n\nDescri��o das Atividades:\n".$TE_DESCRICAO_ATIVIDADES;
  $pdf->MultiCell(0,5,$texto0,1,'J');

  $pdf->AddPage();


  $pdf->SetXY($marg,25);
  $pdf->Cell(0,0,'DO CANDIDATO',0,0);

  $pdf->SetXY($marg,30);
  $pdf->MultiCell(0,5,"1. Nome:\n".$NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME,1,'L');
  $pdf->MultiCell(0,5,"2. Escolaridade\n".$nmEscolaridade,1,'L');

  $row6[0] = "3. Informar a �ltima remunera��o percebida pelo estrangeiro no exterior:\n".$VA_RENUMERACAO_MENSAL;
  $row6[1] = "4. Informar a remunera��o que o estrangeiro ir� perceber no Pa�s:\n".$VA_REMUNERACAO_MENSAL_BRASIL;
  EscreveLinha($row6,1,$w1);

  $texto1 = "5. ".$salarioexterior;
  $pdf->SetXY($marg,60);
  $pdf->MultiCell(0,5,$texto1,1,'L');

  $texto2 = "6. Experi�ncia profissional: \n".$TE_TRABALHO_ANTERIOR_BRASIL."\n\n";
  $aux = split("\n",$TE_TRABALHO_ANTERIOR_BRASIL);
  $tamExp = count($aux);
  if($tamExp<10) { $tamExp = 12; }
  $alt1 = 8+$tamExp*4;
  $y = $pdf->GetY();
#  $pdf->SetXY($marg,$y);
#  $pdf->MultiCell(0,$alt1," ",1,'T');
  $pdf->SetXY($marg,$y);
  $pdf->MultiCell(0,4,$texto2,1,'J');

  $texto3 = "Declaro, sob as penas do art. 299 do C�digo Penal Brasileiro, serem verdadeiras as "; 
  $texto3 = $texto3."informa��es transcritas neste documento, comprometendo-me, inclusive, a comprov�-las, ";
  $texto3 = $texto3."mediante a apresenta��o dos documentos pr�prios a fiscaliza��o.";
  $pdf->SetXY($marg,$y+$alt1+15);
  $pdf->MultiCell(0,5,$texto3,0,'J');

  $y = $pdf->GetY()+5;
  $pdf->SetY($y);
  $pdf->MultiCell(0,5,"Rio de Janeiro, ".$dia." de ".$mes[$mess]." de ".$ano,0,'J');

  $y = $pdf->GetY()+5;
  $pdf->SetY($y);
  $pdf->MultiCell(0,5,"Atenciosamente,",0,'J');

  $linhaaux = "-----------------------------------------------\n";
  $y = $pdf->GetY()+10;
  $pdf->SetXY($marg,$y);
  $rowX[0] = $linhaaux.$NO_PROCURADOR[1]."\n".$nmCargoProc[1]."\n".$NO_EMISSOR_PROC[1]." ".$NU_IDENT_PROC[1]."\nCPF/MF ".$NU_CPF_PROC[1];
  if(strlen($NO_PROCURADOR2[1])>0) {
    $rowX[1] = $linhaaux.$NO_PROCURADOR2[1]."\n".$nmCargoProc2[1]."\n".$NO_EMISSOR_PROC2[1]." ".$NU_IDENT_PROC2[1]."\nCPF/MF ".$NU_CPF_PROC2[1];
  } else {
    $rowX[1] = "";
  }
  EscreveLinha($rowX,0,$w1);

  return $pdf;
}

	

function EscreveLinha($row,$borda,$wd) {
   global $pdf,$marg; 
   $qtd = count($row);
   for($i=0;$i<$qtd;$i++)    {
      $w = $wd[$i];
      $xx=$pdf->GetX();
      $yy=$pdf->GetY();
      $pdf->MultiCell($w,5,$row[$i],$borda,'L');
      $pdf->SetXY($xx+$w,$yy);                    
   }
   $pdf->SetXY($marg,$yy+10);
}

function EscreveLinhaInterna($row,$borda,$wd) {
   global $pdf,$marg; 
   $qtd = count($row);
   for($i=0;$i<$qtd;$i++)    {
      $w = $wd[$i];
      $xx=$pdf->GetX();
      $yy=$pdf->GetY();
      $pdf->MultiCell($w,4,$row[$i],$borda,'L');
      $pdf->SetXY($xx+$w,$yy);                    
   }
   $pdf->SetXY($marg,$yy+4);
}

?>


