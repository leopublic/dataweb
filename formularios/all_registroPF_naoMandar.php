<?php
include("libForm.php");
require('fpdf.php');

class PDF extends FPDF  {
  function Header(){
    global $marg,$NM_LOGO_EMPRESA,$dirimg;
    if($NM_LOGO_EMPRESA == "teste_logo.jpg") {
       $this->Image($dirimg.$NM_LOGO_EMPRESA,$marg,8,33);
    }
  }
  function Footer() {
  }
}

$tpSolicitacao=$_POST['tpSolicitacao'];
$nomeForm = "Registro_PF";
$marg = 15;
$ret = Inicial();
if(strlen($ret)>0) {
  echo $ret;
} else {
 if($tpSolicitacao=="teste") {
  $NM_LOGO_EMPRESA = "teste_logo.jpg";
  $ret = Formulario();
  $ret->Output();
 } else {
  $NM_LOGO_EMPRESA = "";
   $id_solicita_visto = $_POST['id_solicita_visto'];
  $NU_CANDIDATO = $_POST['idCandidato'];
  $ID_TIPO_FORMULARIO= $_POST['ID_TIPO_FORMULARIO'];
 	
  $local = RetornaDIRCandidato($id_solicita_visto, $NU_CANDIDATO);
  $id = RetornaIDformulario();
  $nomeArq = $nomeForm."_".$id.".pdf";
  $ret = GravaArquivoDBCand($id,$idEmpresa,$id_solicita_visto, $idCandidato,$nomeArq, $ID_TIPO_FORMULARIO);
    $ret = Formulario();
  $ret->Output();
  $ret->Output($local.$nomeArq,"F");
  $idCandidato = $_REQUEST['idCandidato'];
  $NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
  $idEmpresa = $_REQUEST['idEmpresa'];
  $cd_admin = $_REQUEST['cd_admin'];
  if(strlen(trim($usulogado))==0) { $usulogado=$cd_admin; }
  $gravado = "/arquivos/formularios/$idEmpresa/$idCandidato/".$nomeArq;
  $texto = "Gera��o do formul�rio de Registro na PF. <a href='$gravado'>(VER)</a>";
  insereEvento($idEmpresa,$idCandidato,$NU_SOLICITACAO,$texto);
 }
}


function Formulario() {
  global $pdf; 
  global $NO_PRIMEIRO_NOME,$NO_NOME_MEIO,$NO_ULTIMO_NOME,$nmPaisNacional,$CO_SEXO,$NO_PAI,$NO_MAE,$CO_ESTADO_CIVIL,$NO_LOCAL_NASCIMENTO;
  global $DT_NASCIMENTO,$nmFuncaoCandidato,$NU_CPF,$nmNacional,$NU_CBO,$TE_OBSERVACOES_PROCESSOS;
  global $NO_RAZAO_SOCIAL,$NO_ENDERECO_EMP,$NO_COMPLEMENTO_EMP,$NO_BAIRRO_EMP,$NO_MUNICIPIO_EMP,$CO_UF_EMP,$NU_CEP_EMP,$NU_DDD_EMP,$NU_TELEFONE_EMP,$NO_EMAIL_EMPRESA;
  global $NU_VISTO,$DT_VALIDADE_VISTO,$DT_EMISSAO_VISTO,$NO_LOCAL_EMISSAO_VISTO,$CO_NACIONALIDADE_VISTO,$nmPaisConcesaoVisto,$CO_CLASSIFICACAO_VISTO;
  global $NO_LOCAL_ENTRADA,$NO_UF_ENTRADA,$DT_ENTRADA,$NU_TRANSPORTE_ENTRADA,$NU_PASSAPORTE,$nmPaisPassaporte;
  global $NO_ENDERECO_RESIDENCIA,$NO_CIDADE_RESIDENCIA,$CO_PAIS_RESIDENCIA,$NU_TELEFONE_CANDIDATO,$NU_SOLICITACAO;
  global $bairroResidencia,$cepResidencia,$ufResidencia;

  $NU_TELEFONE_EMPRESA = "";
  if(strlen($NU_DDD_EMP)>0) {
    $NU_TELEFONE_EMPRESA = "($NU_DDD_EMP) ";
  }
  if(strlen($NU_TELEFONE_EMP)>0) {
    $NU_TELEFONE_EMPRESA = $NU_TELEFONE_EMPRESA.$NU_TELEFONE_EMP;
  }
  if(strlen($NU_CPF)==0) { $NU_CPF="N�o h�"; } 

#$bairroResidencia = "Bairro Residencia";    # Ver com a Ana
#$cepResidencia = "12345-678";    # Ver com a Ana
#$ufResidencia = "RJ";    # Ver com a Ana

$endEntrega = "R";   # Ver com a Ana

$NU_RNE = ""; # Conversa com Ana Lucia
$condicoesEspeciais = ""; # Vazio - Conversa com Ana Lucia
$TE_OBSERVACOES_PROCESSOS = "Observacoes";
$amparoLegal = ""; # Vazio - Conversa com Ana Lucia
$tipoDocViagem = "1"; # Passaporte - fixo - Conversa com Ana Lucia

$nmTipoPedido = "Registro Tempor�rio V";
if($CO_CLASSIFICACAO_VISTO=="1") { $nmTipoPedido = "Permanente"; }

  $pag[0]=216;
  $pag[1]=330;
  $pdf=new PDF('P','mm',$pag);
  $pdf->SetMargins(0,0,0);
  $pdf->AddPage();
  $pdf->SetFont('Arial','',10);

  $cabeca = $NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME."\nPai: ".$NO_PAI."\nMae: ".$NO_MAE;
  $pdf->SetXY(60,10+5);
  $pdf->MultiCell(80,4,$cabeca,0,'L');

  $pdf->SetXY(60,31);
  $pdf->MultiCell(80,4,$nmPaisNacional,0,'L');

  $pdf->SetXY(60,39);
  $pdf->MultiCell(80,4,$nmTipoPedido,0,'L');

  if($CO_SEXO == "M") {
    $pdf->SetXY(165,40);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_SEXO == "F") {
    $pdf->SetXY(185,40);
    $pdf->MultiCell(5,4,"x",0,'L');
  } 

  $pdf->SetXY(61,54);
  $pdf->MultiCell(25,4,$NU_RNE,0,'L');

#  $pdf->SetXY(140,54);
#  $pdf->MultiCell(5,4,"x",0,'L');  # restabelecimento de registro (Ana)

  $pdf->SetXY(8,71);
  $pdf->MultiCell(200,4,$NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME,0,'L');

  $pdf->SetXY(8,87);
  $pdf->MultiCell(30,4,"N�o h�",0,'L');

  $pdf->SetXY(8,95);
  $pdf->MultiCell(80,4,$NO_PAI,0,'L');

  $pdf->SetXY(8,103);
  $pdf->MultiCell(80,4,$NO_MAE,0,'L');

  if($CO_SEXO == "M") {
    $pdf->SetXY(156,95);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_SEXO == "F") {
    $pdf->SetXY(156,103);
    $pdf->MultiCell(5,4,"x",0,'L');
  } 

  $pdf->SetXY(181,100);
  $pdf->MultiCell(25,4,$DT_NASCIMENTO,0,'L');

  if($CO_ESTADO_CIVIL=="1") { # solteiro
    $pdf->SetXY(10,112);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="2") {  # casado
    $pdf->SetXY(38,112);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="3") {  # viuvo
    $pdf->SetXY(63,112);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="4") {  # separado
    $pdf->SetXY(10,121);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="5") {  # divorsiado
    $pdf->SetXY(38,121);
    $pdf->MultiCell(5,4,"x",0,'L');
  } elseif($CO_ESTADO_CIVIL=="6") {  # outros 
    $pdf->SetXY(63,121);
    $pdf->MultiCell(5,4,"x",0,'L');
  }

  $pdf->SetXY(133,114);
  $pdf->MultiCell(75,4,$NO_LOCAL_NASCIMENTO,0,'L');

  $pdf->SetXY(9,131);
  $pdf->MultiCell(75,4,$nmPaisNacional,0,'L');

  $pdf->SetXY(109,131);
  $pdf->MultiCell(75,4,$nmPaisNacional,0,'L');

  $pdf->SetXY(7,147);
  $pdf->MultiCell(100,4,$nmFuncaoCandidato,0,'L');

  $pdf->SetXY(110,149);
  $pdf->MultiCell(75,4,$NU_CBO,0,'L');

  $pdf->SetXY(140,148);
  $pdf->MultiCell(75,4,$NU_CPF,0,'L');


# -------  Parte de baixo do formulario - 1a pagina   ------------------------
  $pdf->SetXY(5,255);
  $pdf->MultiCell(25,4,$NU_RNE,0,'L');

  $pdf->SetXY(5,263);
  $pdf->MultiCell(140,4,$NO_PRIMEIRO_NOME." ".$NO_NOME_MEIO." ".$NO_ULTIMO_NOME,0,'L');

  $pdf->SetXY(5,272);
  $pdf->MultiCell(40,4,$DT_NASCIMENTO,0,'L');

  $NO_LOCAL_NASCIMENTO = substr($NO_LOCAL_NASCIMENTO,0,30);
  $pdf->SetXY(40,272);
  $pdf->MultiCell(55,4,$NO_LOCAL_NASCIMENTO,0,'L');

  $pdf->SetXY(94,272);
  $pdf->MultiCell(55,4,$nmNacional,0,'L');


# ------------------   Fim da 1a Pagina  ---------------------------------------

  $pdf->AddPage();

  $pdf->SetXY(13,57);
  $pdf->MultiCell(75,4,$NO_LOCAL_ENTRADA,0,'L');

  $pdf->SetXY(85,57);
  $pdf->MultiCell(10,4,$NO_UF_ENTRADA,0,'L');

  $pdf->SetXY(93,57);
  $pdf->MultiCell(25,4,$DT_ENTRADA,0,'L');

if($NU_TRANSPORTE_ENTRADA=="1") {  # Navio
  $pdf->SetXY(123,57);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($NU_TRANSPORTE_ENTRADA=="2") { # Aviao 
  $pdf->SetXY(140,57);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($NU_TRANSPORTE_ENTRADA=="3") { # Onibus
  $pdf->SetXY(157,57);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($NU_TRANSPORTE_ENTRADA=="4") { # Outros
  $pdf->SetXY(176,57);
  $pdf->MultiCell(5,4,"x",0,'L');
}

  $pdf->SetXY(190,57);
  $pdf->MultiCell(20,4,$NU_VISTO,0,'L');

  $pdf->SetXY(13,66);
  $pdf->MultiCell(25,4,$DT_EMISSAO_VISTO,0,'L');

  $pdf->SetXY(34,66);
  $pdf->MultiCell(75,4,$NO_LOCAL_EMISSAO_VISTO,0,'L');

  $pdf->SetXY(114,66);
  $pdf->MultiCell(75,4,$nmPaisConcesaoVisto,0,'L');

  $pdf->SetXY(13,76);
  $pdf->MultiCell(75,4,$NU_PASSAPORTE,0,'L');

  $pdf->SetXY(70,76);
  $pdf->MultiCell(120,4,$nmPaisPassaporte,0,'L');

  $pdf->SetXY(105,89);
  $pdf->MultiCell(105,4,$amparoLegal,0,'L');

if($tipoDocViagem=="1") {  # Passaporte
  $pdf->SetXY(10,91);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($tipoDocViagem=="2") {  # Carteira de identidade
  $pdf->SetXY(33,91);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($tipoDocViagem=="3") {  # Outros
  $pdf->SetXY(58,91);
  $pdf->MultiCell(5,4,"x",0,'L');
}

if($CO_CLASSIFICACAO_VISTO=="1") {  # Permanente
  $pdf->SetXY(10,104);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="2") {  # Temporario
  $pdf->SetXY(33,104);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="3") {  # Asilado
  $pdf->SetXY(56,104);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="4") {  # Diplomata
  $pdf->SetXY(79,104);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="5") {  # Cortesia
  $pdf->SetXY(10,113);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="6") {  # Oficial
  $pdf->SetXY(28,113);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="7") {  # Provisorio
  $pdf->SetXY(45,113);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="8") {  # Livre Circulacao
  $pdf->SetXY(66,113);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($CO_CLASSIFICACAO_VISTO=="9") {  # Fronteirico
  $pdf->SetXY(86,113);
  $pdf->MultiCell(5,4,"x",0,'L');
}

  $pdf->SetXY(105,104);
  $pdf->MultiCell(100,4,$condicoesEspeciais,0,'L');

  $pdf->SetXY(9,127);
  $pdf->MultiCell(140,4,$NO_ENDERECO_RESIDENCIA,0,'L');

  $pdf->SetXY(155,127);
  $pdf->MultiCell(50,4,$NU_TELEFONE_CANDIDATO,0,'L');

  $pdf->SetXY(9,140);
  $pdf->MultiCell(50,4,$bairroResidencia,0,'L');

  $pdf->SetXY(62,140);
  $pdf->MultiCell(105,4,$NO_CIDADE_RESIDENCIA,0,'L');

  $pdf->SetXY(168,140);
  $pdf->MultiCell(30,4,$cepResidencia,0,'L');

  $pdf->SetXY(199,140);
  $pdf->MultiCell(10,4,$ufResidencia,0,'L');

  $pdf->SetXY(9,152);
  $pdf->MultiCell(100,4,$NO_RAZAO_SOCIAL,0,'L');

  $pdf->SetXY(109,152);
  $pdf->MultiCell(100,4,$NO_ENDERECO_EMP." ".$NO_COMPLEMENTO_EMP,0,'L');

  $pdf->SetXY(9,165);
  $pdf->MultiCell(50,4,$NO_BAIRRO_EMP,0,'L');

  $pdf->SetXY(60,165);
  $pdf->MultiCell(100,4,$NO_MUNICIPIO_EMP,0,'L');

  $pdf->SetXY(160,165);
  $pdf->MultiCell(10,4,$CO_UF_EMP,0,'L');

  $pdf->SetXY(172,165);
  $pdf->MultiCell(35,4,$NU_TELEFONE_EMPRESA,0,'L');

if($endEntrega == "R") {  # Residencial
  $pdf->SetXY(8,176);
  $pdf->MultiCell(5,4,"x",0,'L');
} elseif($endEntrega == "C") {  # Comercial
  $pdf->SetXY(34,176);
  $pdf->MultiCell(5,4,"x",0,'L');
}

  $pdf->SetXY(57,176);
  $pdf->MultiCell(30,4,$NU_CEP_EMP,0,'L');

  $pdf->SetXY(10,189);
  $pdf->MultiCell(200,4,$TE_OBSERVACOES_PROCESSOS,0,'L');


  return $pdf;
}

?>
