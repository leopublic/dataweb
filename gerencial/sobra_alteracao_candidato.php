<?php
include ("../LIB/conecta.php");
include ("../LIB/geral.php");
include ("../LIB/classes/classe_proc_cancel.php");
include ("../LIB/classes/classe_proc_emiscie.php");
include ("../LIB/classes/classe_proc_mte.php");
include ("../LIB/classes/classe_proc_prorrog.php");
include ("../LIB/classes/classe_proc_regcie.php");

$dia = date("d/m/Y as H:i:s");
print "----------------  $dia  --------------------\n";
SobraAlteracaoCandidato();  # Acerta processos que nao se encontram em alguma tabela

function SobraAlteracaoCandidato() {
  #$procs = " NU_CANDIDATO between 0 AND 3000";
  $procs = " NU_CANDIDATO > 0";
  $sql = "select NU_CANDIDATO,NU_SOLICITACAO,NU_EMPRESA from AUTORIZACAO_CANDIDATO where $procs ORDER BY NU_CANDIDATO,NU_SOLICITACAO ";
  print "\nSQL = $sql\n\n";
  $rs = mysql_query($sql);
  while ($rw = mysql_fetch_array($rs)) {
    $cand = $rw['NU_CANDIDATO'];
    $sol = $rw['NU_SOLICITACAO'];
    $emp = $rw['NU_EMPRESA'];
    $ret = InsereEntradaAndamento($cand,$sol,$emp);
    if(strlen($ret)>0) { print "\nFazendo ... $ret"; }
    $x++;
  }
  print "\nForam executadas $x verificacoes.\n";
}

function InsereEntradaAndamento($cand,$sol,$empresa) {
  $ret = "";
  $mte = new proc_mte();
  if($mte->ExisteCandidato($cand,$sol)==false) {
    $mte->cd_candidato = $cand;
    $mte->cd_solicitacao = $sol;
    $mte->InsereProcesso();
    $mte->AcompanhaProcesso($empresa);
    $ret = $ret."incluindo no MTE ($cand - $sol) ... ".$mte->myerr;
  }

  $reg = new proc_regcie();
  if($reg->ExisteCandidato($cand,$sol)==false) {
    $reg->cd_candidato = $cand;
    $reg->cd_solicitacao = $sol;
    $reg->InsereProcesso();
    $ret = $ret."incluindo no REG ($cand - $sol) ... ".$reg->myerr;
  }

  $emi = new proc_emiscie();
  if($emi->ExisteCandidato($cand,$sol)==false) {
    $emi->cd_candidato = $cand;
    $emi->cd_solicitacao = $sol;
    $emi->InsereProcesso();
    $ret = $ret."incluindo no EMI ($cand - $sol) ... ".$reg->myerr;
  }

  $pro = new proc_prorrog();
  if($pro->ExisteCandidato($cand,$sol)==false) {
    $pro->cd_candidato = $cand;
    $pro->cd_solicitacao = $sol;
    $pro->InsereProcesso();
    $ret = $ret."incluindo no PRO ($cand - $sol) ... ".$reg->myerr;
  }

  $can = new proc_cancel();
  if($can->ExisteCandidato($cand,$sol)==false) {
    $can->cd_candidato = $cand;
    $can->cd_solicitacao = $sol;
    $can->InsereProcesso();
    $ret = $ret."incluindo no CANC ($cand - $sol) ... ".$reg->myerr;
  }
  return $ret;
}

?>

