<?php
include ("../LIB/conecta.php");
include ("../LIB/geral.php");
include ("../LIB/classes/classe_proc_cancel.php");
include ("../LIB/classes/classe_proc_emiscie.php");
include ("../LIB/classes/classe_proc_mte.php");
include ("../LIB/classes/classe_proc_prorrog.php");
include ("../LIB/classes/classe_proc_regcie.php");

$dia = date("d/m/Y as H:i:s");
print "----------------  $dia  --------------------\n";
print "\n -----  AcertaMTE  ------\n";
AcertaMTE();  # Acerta processos que nao se encontram em alguma tabela
print "\n -----  AcertaREGCIE  ------\n";
AcertaREGCIE();
print "\n -----  AcertaEMISSCIE  ------\n";
AcertaEMISSCIE();
print "\n -----  AcertaPRORROG  ------\n";
AcertaPRORROG();
print "\n -----  AcertaCANCEL  ------\n";
AcertaCANCEL();

function AcertaMTE() {
  $x = 0;
  $sql = "select b.codigo,b.cd_candidato,b.cd_solicitacao from AUTORIZACAO_CANDIDATO a , processo_mte b ";
  $sql = $sql." WHERE a.NU_CANDIDATO=b.cd_candidato AND a.NU_SOLICITACAO=b.cd_solicitacao AND ( ";
  $sql = $sql." b.nu_processo<>a.NU_PROCESSO_MTE OR b.dt_requerimento<>a.DT_ABERTURA_PROCESSO_MTE OR b.nu_oficio<>a.NU_AUTORIZACAO_MTE ";
  $sql = $sql." OR b.dt_deferimento<>a.DT_AUTORIZACAO_MTE OR b.prazo_solicitado<>a.DT_PRAZO_AUTORIZACAO_MTE ) ";
  $sql = $sql." ORDER BY b.cd_candidato,b.cd_solicitacao ";
  print "\nSQL = $sql\n\n";
  $rs = mysql_query($sql);
  while ($rw = mysql_fetch_array($rs)) {
    $cod = $rw['codigo'];
    $cand = $rw['cd_candidato'];
    $sol = $rw['cd_solicitacao'];
    $mte = new proc_mte();
    $mte->cd_candidato = $cand;
    $mte->cd_solicitacao = $sol;
    $mte->BuscaPorCodigo($cod);
    if($mte->myerr=="") {
      $mte->Verifica();
      $mte->AlteraVisaDetail();
    }
    print "\n * acertando VD do MTE ($cand - $sol) ... ".$mte->myerr;
    $x++;
  }
  print "\nForam executadas $x verificacoes.\n";
}

function AcertaREGCIE() {
  $x = 0;
  $sql = "select b.codigo,b.cd_candidato,b.cd_solicitacao from AUTORIZACAO_CANDIDATO a , processo_regcie b ";
  $sql = $sql." WHERE a.NU_CANDIDATO=b.cd_candidato AND a.NU_SOLICITACAO=b.cd_solicitacao AND ( ";
  $sql = $sql." b.nu_protocolo<>a.NU_PROTOCOLO_CIE OR b.dt_requerimento<>a.DT_PROTOCOLO_CIE ";
  $sql = $sql." OR b.dt_validade<>a.DT_VALIDADE_PROTOCOLO_CIE OR b.dt_prazo_estada<>a.DT_PRAZO_ESTADA ) ";
  $sql = $sql." ORDER BY b.cd_candidato,b.cd_solicitacao ";
  print "\nSQL = $sql\n\n";
  $rs = mysql_query($sql);
  while ($rw = mysql_fetch_array($rs)) {
    $cod = $rw['codigo'];
    $cand = $rw['cd_candidato'];
    $sol = $rw['cd_solicitacao'];
    $reg = new proc_regcie();
    $reg->cd_candidato = $cand;
    $reg->cd_solicitacao = $sol;
    $reg->BuscaPorCodigo($cod);
    if($reg->myerr=="") {
      $reg->Verifica();
      $reg->AlteraVisaDetail();
    }
    print "\n * acertando VD do REGCIE ($cand - $sol) ... ".$reg->myerr;
    $x++;
  }
  print "\nForam executadas $x verificacoes.\n";
}

function AcertaEMISSCIE() {
  $x = 0;
  $sql = "select b.codigo,b.cd_candidato,b.cd_solicitacao from AUTORIZACAO_CANDIDATO a , processo_emiscie b ";
  $sql = $sql." WHERE a.NU_CANDIDATO=b.cd_candidato AND a.NU_SOLICITACAO=b.cd_solicitacao AND ( ";
  $sql = $sql." b.nu_cie<>a.NU_EMISSAO_CIE OR b.dt_validade<>a.DT_VALIDADE_CIE OR b.dt_emissao<>a.DT_EMISSAO_CIE ) ";
  $sql = $sql." ORDER BY b.cd_candidato,b.cd_solicitacao ";
  print "\nSQL = $sql\n\n";
  $rs = mysql_query($sql);
  while ($rw = mysql_fetch_array($rs)) {
    $cod = $rw['codigo'];
    $cand = $rw['cd_candidato'];
    $sol = $rw['cd_solicitacao'];
    $emi = new proc_emiscie();
    $emi->cd_candidato = $cand;
    $emi->cd_solicitacao = $sol;
    $emi->BuscaPorCodigo($cod);
    if($emi->myerr=="") {
      $emi->Verifica();
      $emi->AlteraVisaDetail();
    }
    print "\n * acertando VD do EMISSCIE ($cand - $sol) ... ".$emi->myerr;
    $x++;
  }
  print "\nForam executadas $x verificacoes.\n";
}

function AcertaPRORROG() {
  $x = 0;
  $sql = "select b.codigo,b.cd_candidato,b.cd_solicitacao from AUTORIZACAO_CANDIDATO a , processo_prorrog b ";
  $sql = $sql." WHERE a.NU_CANDIDATO=b.cd_candidato AND a.NU_SOLICITACAO=b.cd_solicitacao AND ( ";
  $sql = $sql." b.nu_protocolo<>a.NU_PROTOCOLO_PRORROGACAO OR b.dt_requerimento<>a.DT_PROTOCOLO_PRORROGACAO ";
  $sql = $sql." OR b.dt_validade<>a.DT_VALIDADE_PROTOCOLO_PROR OR b.dt_prazo_pret<>a.DT_PRETENDIDA_PRORROGACAO )";
  $sql = $sql." ORDER BY b.cd_candidato,b.cd_solicitacao ";
  print "\nSQL = $sql\n\n";
  $rs = mysql_query($sql);
  while ($rw = mysql_fetch_array($rs)) {
    $cod = $rw['codigo'];
    $cand = $rw['cd_candidato'];
    $sol = $rw['cd_solicitacao'];
    $prorr = new proc_prorrog();
    $prorr->cd_candidato = $cand;
    $prorr->cd_solicitacao = $sol;
    $prorr->BuscaPorCodigo($cod);
    if($prorr->myerr=="") {
      $prorr->Verifica();
      $prorr->AlteraVisaDetail();
    }
    print "\n * acertando VD do PRORROG ($cand - $sol) ... ".$emi->myerr;
    $x++;
  }
  print "\nForam executadas $x verificacoes.\n";
}

function AcertaCANCEL() {
  $x = 0;
  $sql = "select b.codigo,b.cd_candidato,b.cd_solicitacao from AUTORIZACAO_CANDIDATO a , processo_cancel b ";
  $sql = $sql." WHERE a.NU_CANDIDATO=b.cd_candidato AND a.NU_SOLICITACAO=b.cd_solicitacao AND ( ";
  $sql = $sql." b.nu_processo<>a.NU_PROCESSO_CANCELAMENTO OR b.dt_cancel<>a.DT_CANCELAMENTO ";
  $sql = $sql." OR b.dt_processo<>a.DT_PROCESSO_CANCELAMENTO )";
  $sql = $sql." ORDER BY b.cd_candidato,b.cd_solicitacao ";
  print "\nSQL = $sql\n\n";
  $rs = mysql_query($sql);
  while ($rw = mysql_fetch_array($rs)) {
    $cod = $rw['codigo'];
    $cand = $rw['cd_candidato'];
    $sol = $rw['cd_solicitacao'];
    $canc = new proc_cancel();
    $canc->cd_candidato = $cand;
    $canc->cd_solicitacao = $sol;
    $canc->BuscaPorCodigo($cod);
    if($canc->myerr=="") {
      $canc->Verifica();
      $canc->AlteraVisaDetail();
    }
    print "\n * acertando VD do PRORROG ($cand - $sol) ... ".$emi->myerr;
    $x++;
  }
  print "\nForam executadas $x verificacoes.\n";
}

?>

