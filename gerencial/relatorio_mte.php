<?php
$opcao = "GER";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/classes/classe_proc_mte.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = $HTTP_GET_VARS['idEmpresa'];
}
$idEmbarcacaoProjeto = $_POST['idEmbarcacaoProjeto'];
if(strlen($idEmpresa)>0) {
  $extraproj = " ORDER BY NO_EMBARCACAO_PROJETO";
  $cmbProj = montaComboProjetos($idEmbarcacaoProjeto,$idEmpresa,$extraproj);
}
$cd_reparticao = $_POST['cd_reparticao'];
$cmbRepar = montaComboReparticoes($cd_reparticao,"");
$tp_visto = $_POST['tp_visto'];
$cmbVisto = montaComboTipoAutorizacao2($tp_visto,"");

$fase = 0+$_POST['fase'];

$cads = $_POST['cads'];
$cmbCads = GeraComboTipoCadastro($cads);
$anda = $_POST['anda'];
$cmbAnda = GeraComboTipoAndamento($anda);

$periodo = $_POST['periodo'];
$grupem = $_POST['grupem'];
if($grupem == "Y") { $grpE="checked"; }
$perini = $_POST['perini'];
$perfim = $_POST['perfim'];
$tipo = $_POST['tipo'];
if($tipo == "C") { $tipoC="selected"; }
if( ($tipo == "L") || (strlen($tipo)==0) ) { $tipoL="selected"; }
$grupo = $_POST['grupo'];
if($grupo == "C") { $grpC="checked"; }
if($grupo == "A") { $grpA="checked"; }
$ordem = $_POST['ordem'];
if($ordem == "P") { $ordP="checked"; }
if($ordem == "C") { $ordC="checked"; }

$empCmb = montaComboEmpresas($idEmpresa,"ORDER BY NO_RAZAO_SOCIAL");
$cmbPer = GeraComboTipoDataPeriodo($periodo);

if($lang=="E") {
  $titulo = "Process MTE";
  $titEmpresa = "Company";
  $titFiltroEmpresa = "Company Filter";
  $titProjeto = "Project/Vessel";
  $titGerarRelatorio = "List Report";
  $titTodos = "All";
  $titAlerta = "Changed";
  $titFim = "Finally";
  $titCadastrado = "Cadastrado";
} else {
  $titulo = "Processos MTE";
  $titEmpresa = "Empresa";
  $titProjeto = "Projeto/Embarca&ccedil;&atilde;o";
  $titGerarRelatorio = "Listar Relat&oacute;rio";
  $titFiltroEmpresa = "Filtro Empresa";
  $titTodos = "Todos";
  $titAlerta = "Alterados";
  $titFim = "Finalizados";
  $titCadastrado = "Cadastrados";
}

if($fase>0) {
  echo IniPag();
} else {
  echo Topo($opcao);
  echo Menu($opcao);
}

if($fase==0) {

?>

<script language="javascript">
function Enviar() {
  var cads = document.relat.cads.selectedIndex;
  var anda = document.relat.anda.selectedIndex;
  var emp = document.relat.idEmpresa.selectedIndex;
  var visto = document.relat.tp_visto.selectedIndex;
  var consul = document.relat.cd_reparticao.selectedIndex;
  var tipo = document.relat.tipo.selectedIndex;
  if( (cads < 1) && (anda < 1) && (emp < 1) && (tipo < 1) && (visto < 1) && (consul < 1) ) {
    alert("Esta opção gera uma pagina muito grande, podendo parar o banco.");
  } else {
    document.relat.submit();
  }
}
function tipo(obj) {
  if(obj.selectedIndex < 1) {
    document.all.agrupar.style.display = 'none';
    document.all.ordenar.style.display = 'block';
  } else {
    document.all.agrupar.style.display = 'block';
    document.all.ordenar.style.display = 'none';
  }
}
function filtraempresa() {
  var indice = document.relat.idEmpresa.selectedIndex;
  if(indice < 1) {
    alert("Escolha uma empresa.");
    document.relat.idEmpresa.focus();
  } else {
    var empresa = document.relat.idEmpresa[indice].value;
    document.filtra.idEmpresa.value = empresa;
    document.filtra.submit();
  }
}
function andamento() {

}
function cadastro() {

}
function Arruma() {
/*
  var tipo = document.relat.tipo.selectedIndex;
  if(tipo < 1) {
    document.all.agrupar.style.display = 'none';
    document.all.ordenar.style.display = 'block';
  } else {
    document.all.agrupar.style.display = 'block';
    document.all.ordenar.style.display = 'none';
  }
*/
}
</script>

<form name=relat method=post action="relatorio_mte.php" target='newwin'>
<input type=hidden name=fase value='1'>
<br><center>
<table border=0 width="680" class='textoazul' cellspacing=0 cellpadding=0>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="3">
	<p align="center" class="textoazul"><strong>:: <?=$titulo?> ::</strong></p>				
  </td>
 </tr>
</table>
<br>
<table width=680 border=1 cellspacing=0 cellpadding=0 class='textoazulpeq'>
   <tr height=25 valign=middle>
    <td><?=$titEmpresa?>:</td>
    <td colspan=2><select name=idEmpresa class='textoazulpeq'><option value=''><?=$titTodos?> ... <?=$empCmb?></select></td>
    <td><input type=button value='Listar Projetos' onclick='javascript:filtraempresa();' style="<?=$estilo?>"></td>
   </tr>
<?php if(strlen($idEmpresa)>0) { ?>
   <tr height=25 valign=middle>
    <td><?=$titProjeto?>:</td>
    <td colspan=3><select name=idEmbarcacaoProjeto class='textoazulpeq'><option value=''><?=$titTodos?> ... <?=$cmbProj?></select></td>
   </tr>
<?php } ?>
   <tr height=25 valign=middle>
    <td>Reparti&ccedil;&atilde;o Consular:</td>
    <td colspan=3><select name=cd_reparticao class='textoazulpeq'><option value=''><?=$titTodos?> ... <?=$cmbRepar?></select></td>
   </tr>
   <tr height=25 valign=middle>
    <td>Tipo de Visto:</td>
    <td colspan=3><select name=tp_visto class='textoazulpeq'><option value=''><?=$titTodos?> ... <?=$cmbVisto?></select></td>
   </tr>
   <tr height=30 valign=middle>
    <td>Cadastro:</td>
    <td colspan=3><select name=cads class='textoazulpeq'><?=$cmbCads?></select></td>
   </tr>
   <tr height=30 valign=middle>
    <td>Andamento:</td>
    <td colspan=3><select name=anda class='textoazulpeq'><?=$cmbAnda?></select></td>
   </tr>
   <tr height=25 valign=middle>
    <td>Periodo:</td>
    <td colspan=3><select name=periodo class='textoazulpeq'><?=$cmbPer?></select>
        <input type=text name=perini value='<?=$perini?>' class='textoazulpeq' size=12 maxlength=10>
         a <input type=text name=perfim value='<?=$perfim?>' class='textoazulpeq' size=12 maxlength=10></td>
   </tr>
   <tr height=25 valign=middle>
    <td>Tipo consulta:</td>
    <td><select name=tipo class='textoazulpeq'  onChange="javascript:tipo(this);">
       <option value='L' <?=$tipoL?> > Lista<!-- option value='C' <?=$tipoC?> > Contagem --> </select>
    </td>
   </tr>
   <!-- tr id='agrupar' height=25>
    <td>Agrupar:</td>
    <td><input type=radio name=grupo value='C' <?=$grpC?> > Cadastro</td>
    <td><input type=radio name=grupo value='A' <?=$grpA?> > Andamento</td>
    <td><input type=checkbox name=grupem value='Y' <?=$grpE?> > Empresa</td>
   </tr -->
   <tr id='ordenar' height=25>
    <td>Ordenar:</td>
    <td><input type=radio name=ordem value='P' <?=$ordP?> > Processo</td>
    <td><input type=radio name=ordem value='C' <?=$ordC?> > Candidato</td>
    <td>&#160;</td>
   </tr>
</table>
<br>
<input type="button" class="textformtopo" style="<?=$estilo?>" value="<?=$titGerarRelatorio?>" onclick="javascript:Enviar();">
</form>

<form name=filtra method=post action="relatorio_mte.php">
<input type=hidden name=idEmpresa>
</form>

<script language="javascript">
Arruma();
</script>
<?php

}

print "<center><br>";
if($tipo=="L") {
  $obj = new proc_mte();
  $extra = MontaSQLextra();
  if($ordem == "P") {
    $orderby = " ORDER BY c.NO_RAZAO_SOCIAL,a.nu_processo,nome_candidato";
  } else if($ordem == "C") {
    $orderby = " ORDER BY c.NO_RAZAO_SOCIAL,nome_candidato";
  }
  $rs = $obj->RelatorioRS($tipo,$extra,$orderby);
  $msg = $obj->myerr;
  $total = 0 + mysql_num_rows($rs);
  print "<table width=750 border=0 cellspacing=0 cellpadding=0 class='textoazulpeq'>\n";
  if(strlen($idEmpresa)>0) {
    $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
    print "<tr><td><b>Nome da Empresa: </b>$nomeEmpresa</td></tr>";
  }
  if(strlen($idEmbarcacaoProjeto)>0) {
    $nomeprojeto = pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa);
    print "<tr><td><b>Projeto/Embarca&ccedil;&atilde;o: </b>$nomeprojeto</td></tr>";
  }
  if(strlen($cd_reparticao)>0) {
    $nomeRepart = pegaNomeReparticao($cd_reparticao);
    print "<tr><td><b>Reparti&ccedil&atilde;o Consular: </b>$nomeRepart</td></tr>";
  }
  if(strlen($tp_visto)>0) {
    $nomeVisto = pegaNomeTipoAutorizacao2($tp_visto);
    print "<tr><td><b>Tipo de Visto: </b>$nomeVisto</td></tr>";
  }
  print "<tr><td>&#160;</td></tr>";
  print "<tr><td>Retorno de $total candidatos<br><font color=red>$msg</font></td></tr>";
  print "</table>\n<br>\n";
  print "<table width=750 border=1 cellspacing=0 cellpadding=0 class='textoazulpeq'>\n";
  print " <tr>\n";
  if(strlen($idEmpresa)==0) {
    print "  <td align=center><b>Empresa</td>\n";
  } else {
    print "  <td align=center><b>Projeto</td>\n";
  }
  if($ordem == "P") {
    print "  <td align=center><b>Processo</td>\n";
    print "  <td align=center><b>Candidato</td>\n";
  } else {
    print "  <td align=center><b>Candidato</td>\n";
    print "  <td align=center><b>Processo</td>\n";
  }
  print "  <td align=center><b>Sol.</td>\n";
  print "  <td align=center><b>Requerimento</td>\n";
  print "  <td align=center><b>Oficio</td>\n";
  print "  <td align=center><b>Deferimento</td>\n";
  print "  <td align=center><b>Prazo Sol.</td>\n";
  print " </tr>\n";
  while($rw=mysql_fetch_array($rs)) {
    $cand = $rw['cd_candidato'];
    $emp = $rw['NU_EMPRESA'];
    $proj = $rw['NU_EMBARCACAO_PROJETO'];
    $sol = $rw['cd_solicitacao'];
    $nmcand = $rw['nome_candidato'];
    $nmemp = $rw['NO_RAZAO_SOCIAL'];
    $proc = $rw['nu_processo'];
    $dtreq = dataMy2BR($rw['dt_requerimento']);
    $oficio = $rw['nu_oficio'];
    $dtdef = dataMy2BR($rw['dt_deferimento']);
    $prazo = $rw['prazo_solicitado'];
    $dt_cad = $rw['dt_cad'];
    print " <tr>\n";
    if(strlen($idEmpresa)==0) {
      print "  <td>$nmemp</td>\n";
    } else {
      print "  <td>".pegaNomeProjeto($proj,$idEmpresa)."</td>\n";
    }
    if($ordem == "P") {
      print "  <td align=center nowrap>&#160;$proc</td>\n";
      print "  <td>&#160;$nmcand</td>\n";
    } else {
      print "  <td>&#160;$nmcand</td>\n";
      print "  <td align=center nowrap>&#160;$proc</td>\n";
    }
    print "  <td align=center nowrap>$sol</td>\n";
    print "  <td align=center nowrap>&#160;$dtreq</td>\n";
    print "  <td align=center nowrap>&#160;$oficio</td>\n";
    print "  <td align=center nowrap>&#160;$dtdef</td>\n";
    print "  <td align=center nowrap>&#160;$prazo</td>\n";
    print " </tr>\n";
  }
  print "</table>\n";
} else if($tipo=="C") {
  $extra = MontaSQLextra();
}

echo Rodape($opcao);

function GeraComboTipoDataPeriodo($cod) {
  $ret = "";
  if($cod=="S") { $chS = "checked"; }
  if($cod=="C") { $chC = "checked"; }
  if($cod=="D") { $chD = "checked"; }
  if($cod=="P") { $chP = "checked"; }
  if($cod=="A") { $chA = "checked"; }
  $ret = $ret."<option value='S' $chS>Sem periodo";
  $ret = $ret."<option value='C' $chC>Data Cadastro MTE";
  $ret = $ret."<option value='D' $chD>Data Deferimento MTE";
  $ret = $ret."<option value='P' $chP>Data Ult. Pesq. MTE";
  $ret = $ret."<option value='A' $chA>Data Ult. Alt. MTE";
  return $ret;
}

function GeraComboTipoCadastro($cod) {
  $ret = "";
  if($cod=="A") { $chA = "checked"; }
  if($cod=="S") { $chS = "checked"; }
  if($cod=="M") { $chM = "checked"; }
  if($cod=="D") { $chD = "checked"; }
  if($cod=="C") { $chC = "checked"; }
  if($cod=="F") { $chF = "checked"; }
  $ret = $ret."<option value='A' $chA>Todos";
  $ret = $ret."<option value='S' $chS>Sem Cadastro (sem No. MTE e sem Data Req.)";
  $ret = $ret."<option value='F' $chF>Cadastrado (com No. MTE)";
  $ret = $ret."<option value='D' $chD>Sem No. MTE e com Data de Requerimento";
  $ret = $ret."<option value='M' $chM>Com No. MTE e sem Data de Requerimento";
  $ret = $ret."<option value='C' $chC>Com No. MTE e com Data de Requerimento";
  return $ret;
}

function GeraComboTipoAndamento($cod) {
  $ret = "";
  if($cod=="A") { $chA = "checked"; }
  if($cod=="S") { $chS = "checked"; }
  if($cod=="O") { $chO = "checked"; }
  if($cod=="D") { $chD = "checked"; }
  if($cod=="C") { $chC = "checked"; }
  if($cod=="F") { $chF = "checked"; }
  $ret = $ret."<option value='A' $chA>Todos";
  $ret = $ret."<option value='S' $chS>Sem Andamento (sem No. Oficio e sem Data Deferimento)";
  $ret = $ret."<option value='F' $chF>Finalizado (com No. MTE ou com Data de Deferimento)";
  $ret = $ret."<option value='D' $chD>Sem No. Oficio e com Data de Deferimento";
  $ret = $ret."<option value='O' $chM>Com No. Oficio e sem Data de Deferimento";
  $ret = $ret."<option value='C' $chC>Com No. Oficio e com Data de Deferimento";
  return $ret;
}

function MontaSQLextra() {
  global $idEmpresa,$cads,$anda,$periodo,$perini,$perfim,$idEmbarcacaoProjeto,$cd_reparticao,$tp_visto;
  if(strlen($idEmpresa)>0) { $ret = $ret." AND c.NU_EMPRESA=$idEmpresa "; }
  if(strlen($idEmbarcacaoProjeto)>0) { $ret = $ret." AND b.NU_EMBARCACAO_PROJETO=$idEmbarcacaoProjeto "; }
  if(strlen($cd_reparticao)>0) { $ret = $ret." AND b.CO_REPARTICAO_CONSULAR=$cd_reparticao "; }
  if(strlen($tp_visto)>0) { $ret = $ret." AND b.CO_TIPO_AUTORIZACAO='$tp_visto' "; }
  if($cads == "S") { $ret = $ret." AND a.nu_processo IS NULL AND a.dt_requerimento IS NULL "; }
  if($cads == "F") { $ret = $ret." AND a.nu_processo IS NOT NULL "; }
  if($cads == "M") { $ret = $ret." AND a.nu_processo IS NOT NULL AND a.dt_requerimento IS NULL "; }
  if($cads == "D") { $ret = $ret." AND a.nu_processo IS NULL AND a.dt_requerimento IS NOT NULL "; }
  if($cads == "C") { $ret = $ret." AND a.nu_processo IS NOT NULL AND a.dt_requerimento IS NOT NULL "; }
  if($anda == "S") { $ret = $ret." AND a.nu_oficio IS NULL AND a.dt_deferimento IS NULL "; }
  if($anda == "F") { $ret = $ret." AND (a.nu_oficio IS NOT NULL OR a.dt_deferimento IS NOT NULL) "; }
  if($anda == "O") { $ret = $ret." AND a.nu_oficio IS NOT NULL AND a.dt_deferimento IS NULL "; }
  if($anda == "D") { $ret = $ret." AND a.nu_oficio IS NULL AND a.dt_deferimento IS NOT NULL "; }
  if($anda == "C") { $ret = $ret." AND a.nu_oficio IS NOT NULL AND a.dt_deferimento IS NOT NULL "; }
  if($periodo == "C") { $tpdata = "a.dt_requerimento"; }
  if($periodo == "D") { $tpdata = "a.dt_deferimento"; }
  if($periodo == "P") { $tpdata = "d.data_pesquisa"; }
  if($periodo == "A") { $tpdata = "d.data_alteracao"; }
  if(strlen($tpdata)>0) {
    $perini = dataBR2My($perini);
    $perfim = dataBR2My($perfim);
    if( (strlen($perini)>0) && (strlen($perfim)>0) ) {
      $ret = $ret." AND $tpdata BETWEEN '$perini' AND '$perfim' ";
    } else if( (strlen($perini)>0) ) {
      $ret = $ret." AND $tpdata >= '$perini' ";
    } else if( (strlen($perfim)>0) ) {
      $ret = $ret." AND $tpdata <= '$perfim' ";
    }
  }
  return $ret;
}

?>

