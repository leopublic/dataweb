<?php
$opcao = "GER";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$fase = 0+$_REQUEST['fase'];
$idEmpresa = $_REQUEST['idEmpresa'];
$idEmbarcacaoProjeto = $_REQUEST['idEmbarcacaoProjeto'];
if(strlen($idEmpresa)>0) {
  $extraproj = " ORDER BY NO_EMBARCACAO_PROJETO";
  $cmbProj = montaComboProjetos($idEmbarcacaoProjeto,$idEmpresa,$extraproj);
}
$empCmb = montaComboEmpresas($idEmpresa,"ORDER BY NO_RAZAO_SOCIAL");
$nome_completo = $_REQUEST['nome_completo'];
$cd_solicitacao = $_REQUEST['cd_solicitacao'];

$idCandidato = $_REQUEST['idCandidato'];
$idSolicitacao = $_REQUEST['idSolicitacao'];

if($lang=="E") {
  $titulo = "Events";
  $titEmpresa = "Company";
  $titFiltroEmpresa = "Company Filter";
  $titProjeto = "Project/Vessel";
  $titGerarRelatorio = "List Candidates";
  $titTodos = "All";
  $titAlerta = "Changed";
} else {
  $titulo = "Acompanhamento de Eventos";
  $titEmpresa = "Empresa";
  $titProjeto = "Projeto/Embarca&ccedil;&atilde;o";
  $titGerarRelatorio = "Listar Candidatos";
  $titFiltroEmpresa = "Filtro Empresa";
  $titTodos = "Todos";
  $titAlerta = "Alterados";
}

if($fase>1) {
  echo IniPag();
} else {
  echo Topo($opcao);
  echo Menu($opcao);
}

if($fase>1) {
  print "<script language='javascript'>window.name='VER';</script>";
  $sql = MontaSQLCandidato($idCandidato,$idSolicitacao);
  $rs1 = mysql_query($sql);
  if($rw1=mysql_fetch_array($rs1)) {
    $nome_completo = $rw1['NOME_COMPLETO'];
    $nomeEmpresa = $rw1['NO_RAZAO_SOCIAL'];
    $idEmbarcacaoProjeto = $rw1['NU_EMBARCACAO_PROJETO'];
    print "<br><center>\n";
    print "<table width=99% border=0 cellspacing=0 cellpadding=2 class='textoazulpeq'>\n";
    print "<tr height=15><td width=130><b>Nome da Empresa: </b></td><td>&#160;$nomeEmpresa</td></tr>";
    if(strlen($idEmbarcacaoProjeto)>0) {
      $nomeprojeto = pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa);
      print "<tr height=15><td><b>Projeto/Embarca&ccedil;&atilde;o: </b></td><td>&#160;$nomeprojeto</td></tr>";
    }
    print "<tr height=15><td><b>Nome do Candidato: </b></td><td>&#160;$nome_completo</td></tr>";
    print "<tr height=15><td><b>N&uacute;mero da Solicita&ccedil;&atilde;o: </b></td><td>&#160;$idSolicitacao</td></tr>";
    print "</table>\n<br>\n";
    $sql = "SELECT date(a.dt_cad) as dia, time(a.dt_cad) as hora, b.login, b.nome, a.texto ";
    $sql = $sql."FROM acompanhamento_eventos a, usuarios b WHERE b.cd_usuario=a.cd_admin AND a.cd_candidato=$idCandidato ";
    $sql = $sql." AND (a.cd_solicitacao = $idSolicitacao OR a.cd_solicitacao is NULL) order by cd_interno ";
    print "<table width=99% border=1 cellspacing=0 cellpadding=2 class='textoazulpeq'>\n";
    print " <tr height=12>";
    print "   <td align=center width=50><b>Data</b></td>";
    print "   <td align=center width=50><b>Hora</b></td>";
    print "   <td align=center width=70><b>Usu&aacute;rio</b></td>";
    print "   <td align=center><b>Evento</b></td>";
    print " </tr>";
    $rs2 = mysql_query($sql);
    if(mysql_errno()!=0) {
      print "<tr><td colspan=4>Erro: ".mysql_error()."</td></tr>";
    } else {
      while($rw2=mysql_fetch_array($rs2)) {
        $dia = dataMy2BR($rw2['dia']);
        $hora = $rw2['hora'];
        $login = $rw2['login'];
        $nome = $rw2['nome'];
        $texto = $rw2['texto'];
        $usuario = "<a href='javascript:alert(\"$nome\");' alt='$nome' target='VER'>$login</a>";
        print " <tr height=12>";
        print "   <td align=center>$dia</td>";
        print "   <td align=center>$hora</td>";
        print "   <td align=center>$usuario</td>";
        print "   <td>$texto</td>";
        print " </tr>";
      }
    }
    print "</table><br>\n";
    print "<input type=button value='Imprimir' onclick='javascript:self.print();' class='textoazulpeq' target='VER'>";
  } else {
    print "<table width=600 border=0 cellspacing=0 cellpadding=0 class='textoazulpeq'>\n";
    print "<tr><td>Candidato n&atilde;o encontrado.</td></tr>";
    print "</table>\n<br>\n";
  }
} else {
?>

<script language="javascript">
function filtraempresa() {
  var indice = document.lista.idEmpresa.selectedIndex;
  document.lista.fase.value = 0;
  if(indice < 1) {
    alert("Escolha uma empresa.");
    document.lista.idEmpresa.focus();
  } else {
    document.lista.submit();
  }
}
function Listar() {
  document.lista.fase.value = 1;
  var emp = document.lista.idEmpresa.selectedIndex;
  var nome = document.lista.nome_completo.value;
  var sol = document.lista.cd_solicitacao.value;
  if( (emp < 1) && (nome < 5) && (sol < 1) ) {
    alert("Esta opção gera uma pagina muito grande, podendo parar o banco.\nPor favor, filtre mais a consulta.");
  } else {
    document.lista.submit();
  }
}
function Escolhe(cand,sol) {
  var pagina = "acompanha_eventos.php?fase=2&idCandidato="+cand+"&idSolicitacao="+sol;
  var MyArgs = new Array(cand,sol);
  var WinSettings = "center:yes;resizable:yes;dialogHeight:600px;dialogWidth=740px";
  var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
}
</script>

<form name=lista method=post action="acompanha_eventos.php">
<input type=hidden name=fase>
<br><center>
<table border=0 width="600" class='textoazul' cellspacing=0 cellpadding=0>
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="3">
	<p align="center" class="textoazul"><strong>:: <?=$titulo?> ::</strong></p>				
  </td>
 </tr>
</table>
<br>
<table width=600 border=1 cellspacing=0 cellpadding=0 class='textoazulpeq'>
   <tr height=25 valign=middle>
    <td><?=$titEmpresa?>:</td>
    <td colspan=2>&#160;<select name=idEmpresa class='textoazulpeq'><option value=''><?=$titTodos?> ... <?=$empCmb?></select></td>
    <td><input type=button value='Listar Projetos' onclick='javascript:filtraempresa();' style="<?=$estilo?>"></td>
   </tr>
<?php if(strlen($idEmpresa)>0) { ?>
   <tr height=25 valign=middle>
    <td><?=$titProjeto?>:</td>
    <td colspan=3>&#160;<select name=idEmbarcacaoProjeto class='textoazulpeq'><option value=''><?=$titTodos?> ... <?=$cmbProj?></select></td>
   </tr>
<?php } ?>
   <tr height=25 valign=middle>
    <td>Nome do Candidato:</td>
    <td colspan=3>&#160;<input name=nome_completo class='textoazulpeq' size=40 maxlength=40 value='<?=$nome_completo?>'></td>
   </tr>
   <tr height=25 valign=middle>
    <td>N&uacute;mero da Solicita&ccedil;&atilde;o:</td>
    <td colspan=3>&#160;<input name=cd_solicitacao class='textoazulpeq' size=10 maxlength=6 value='<?=$cd_solicitacao?>'></td>
   </tr>
</table>
<br>
<input type="button" class="textformtopo" style="<?=$estilo?>" value="<?=$titGerarRelatorio?>" onclick="javascript:Listar();">
</form>

<?php
  if($fase == 1) {
    $msg = "";
    print "<center><br>";
    $sql = MontaSQL();
    $rs = mysql_query($sql);
    if(mysql_errno() != 0) {
      $msg = mysql_error();
    } else {
      $total = 0 + mysql_num_rows($rs);
    }
    print "<table width=600 border=0 cellspacing=0 cellpadding=0 class='textoazulpeq'>\n";
    print "<tr><td><b>Observa&ccedil;&atilde;o: </b>lista apenas eventos ocorridos a partir de 01 de junho de 2009.</td></tr>";
    if(strlen($idEmpresa)>0) {
      $nomeEmpresa = pegaNomeEmpresa($idEmpresa);
      print "<tr><td><b>Nome da Empresa: </b>$nomeEmpresa</td></tr>";
    }
    if(strlen($idEmbarcacaoProjeto)>0) {
      $nomeprojeto = pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa);
      print "<tr><td><b>Projeto/Embarca&ccedil;&atilde;o: </b>$nomeprojeto</td></tr>";
    }
    if(strlen($nome_completo)>0) {
      print "<tr><td><b>Nome do Candidato: </b>$nome_completo</td></tr>";
    }
    if(strlen($cd_solicitacao)>0) {
      print "<tr><td><b>N&uacute;mero da Solicita&ccedil;&atilde;o: </b>$cd_solicitacao</td></tr>";
    }
    print "<tr><td>&#160;</td></tr>";
    print "<tr><td>Retorno de $total candidatos</td></tr>";
    print "</table>\n<br>\n";
    
    print "<table width=650 border=1 cellspacing=0 cellpadding=0 class='textoazulpeq'>\n";
    print "<tr>";
    if(strlen($idEmpresa)==0) {
      print "<td align=center><b>Nome da Empresa</td>";
    }
    print "<td align=center><b>Nome do candidato</td>";
    print "<td align=center><b>Solic.</td>";
    print "<td align=center><b>Embarca&ccedil;&atilde;o</td>";
    print "</tr>";
    while($rw=mysql_fetch_array($rs)) {
      $empresa = substr($rw['NO_RAZAO_SOCIAL'],0,40);
      $cand = $rw['NU_CANDIDATO'];
      $candidato = $rw['NOME_COMPLETO'];
      $sol = $rw['NU_SOLICITACAO'];
      $embarcacao = substr(pegaNomeProjeto($rw['NU_EMBARCACAO_PROJETO'],$rw['NU_EMPRESA']),0,30);
      $ver = "<a href='javascript:Escolhe(\"$cand\",\"$sol\");'><u>$candidato</a>";
      print "<tr>";
      if(strlen($idEmpresa)==0) {
        print "<td>$empresa</td>";
      }
      print "<td>$ver</td>";
      print "<td align=center>$sol</td>";
      print "<td>$embarcacao</td>";
      print "</tr>";
    }
    print "</table>\n";
  }
}

echo Rodape($opcao);

function MontaSQL() {
  global $idEmpresa,$idEmbarcacaoProjeto,$nome_completo,$cd_solicitacao;
  $ret = " select a.NU_CANDIDATO,a.NOME_COMPLETO,b.NU_SOLICITACAO,b.NU_EMPRESA,b.NU_EMBARCACAO_PROJETO,c.NO_RAZAO_SOCIAL ";
  $ret = $ret." from CANDIDATO a, AUTORIZACAO_CANDIDATO b, EMPRESA c ";
  $ret = $ret." where a.NU_CANDIDATO=b.NU_CANDIDATO and b.NU_EMPRESA=c.NU_EMPRESA ";
  if(strlen($idEmpresa)>0) { $ret = $ret." AND b.NU_EMPRESA=$idEmpresa "; }
  if(strlen($idEmbarcacaoProjeto)>0) { $ret = $ret." AND b.NU_EMBARCACAO_PROJETO=$idEmbarcacaoProjeto "; }
  if(strlen($nome_completo)>0) { $ret = $ret." AND a.NOME_COMPLETO like '%$nome_completo%' "; }
  if(strlen($cd_solicitacao)>0) { $ret = $ret." AND b.NU_SOLICITACAO=$cd_solicitacao "; }
  $ret = $ret." order by NO_RAZAO_SOCIAL , NOME_COMPLETO , NU_SOLICITACAO asc ";
  return $ret;
}

function MontaSQLCandidato($cand,$sol) {
  $ret = " select a.NOME_COMPLETO,b.NU_SOLICITACAO,b.NU_EMPRESA,b.NU_EMBARCACAO_PROJETO,c.NO_RAZAO_SOCIAL ";
  $ret = $ret." from CANDIDATO a, AUTORIZACAO_CANDIDATO b, EMPRESA c ";
  $ret = $ret." where a.NU_CANDIDATO=b.NU_CANDIDATO and b.NU_EMPRESA=c.NU_EMPRESA and a.NU_CANDIDATO=$cand ";
  if(strlen($sol)>0) { $ret = $ret." AND b.NU_SOLICITACAO=$sol "; }
  return $ret;
}

?>

