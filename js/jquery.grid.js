$.fn.Grid = function(opt){
	var init = {
		request           	: 'ajax/servico.php',
        qs_load_list        : '?action=list',
        qs_detail_add       : '?action=detailadd',
        qs_add              : '?action=add',
        qs_edit             : '?action=edit',
        qs_delete           : '?action=remove',
        qs_detail           : '?action=detailedit',
        qs_refresh_row      : '?action=refreshrow',
        frm_detailadd       : '#frm_detailadd',
        frm_detailedit      : '#frm_detailedit',
        title               : '',
        actions             : true,
        msgErroDelete       : 'Houve um erro inesperado e não foi possível remover',
        msgCfmDelete        : 'Tem certeza que deseja deletar?',
        msgErroAdd          : 'Não foi possível cadastrar',
        msgErroEdit         : 'Não foi possível salvar. verifique os dados',
        msgAddSuccess       : 'O registro foi inserido com sucesso! ',
        msgEditSuccess      : 'O registro foi modificado com sucesso! ', 
        cursorRow           : 'auto',
        bodyHeight          : '300px',
        BoxEditWidth        : '600px',
        BoxAddWidth         : '600px',
        datatype            : 'post',
        linhaporpagina      : 20,
        qs_rp               : '&rp=',
        qs_page             : '&page=',
        qs_search           : '&search=',
        pagina              : 1,
        cols                : 'NOME|EMAIL',
       // searchcols          : 'NOME|EMAIL',
        search              : ''

	}
	var opt = $.extend(init, opt);
    var id  = '#'+this.attr('id');
    var obj = $(id);
        if(obj[0]){
            var remove          = '<img class="grid_remove" src="../imagens/icons/application_delete.png" />';
            var edit            = '<img class="grid_edit" src="../imagens/icons/application_edit.png" />';
            var grid_action     = '<td class="grid_action">'+edit+' '+remove+' </td>';
            var form_atual      = '.TVVSGridBox .contentGB ';
            var title_actions   = '<th class="title_action">Ação</th>';
            var qtd_rows        = parseInt($(id+' .qtdrow').html());
            var total           = 0;
            var $qtd_rows       = $(id+' .qtdrow');



            obj.addClass('TVVSGrid');
            $(id+' tbody').css('height',opt.bodyHeight);


             function loadgrid(){
                var qs_help_paginator = opt.qs_rp+opt.linhaporpagina+opt.qs_page+opt.pagina+opt.qs_search+opt.search;
              //CARREGANDO GRID ..

                if (opt.datatype == 'xml'){
                     // $(id).prepend('<div class="VVSGridLoad">aaaaaa</div>');
                      CriaColunasCampo();

                      $qtd_rows.html('0');
                      $trs ='';
                      $.ajax({
                         type: 'GET',
                         url: opt.request+opt.qs_load_list+qs_help_paginator,
                         dataType: 'xml',
                         success: function(xml) {

                            var $table = $(id+' tbody');
                            var $tr = null;
                            var $trs = '';

                            $('row',xml).each(function(){
                            //coloco os ID
                            if($(this).attr('id')){
                              $trs += '<tr rel="'+$(this).attr('id')+'">';
                            }else{
                              $trs += '<tr>';
                            }

                             $(this).children().each(function(){
                               $trs += '<td>'+$(this).text()+'</td>';

                             });
                             if(opt.actions){
                                 $trs += grid_action;
                             }
                             $trs +='</tr>';

                             $qtd_rows.html(parseInt($qtd_rows.html())+1);

                            });
                            $table.html('');
                            $table.html($trs);


                            dFirst = '<span class="first">'+img('imagens/first.gif')+'</span>';
                            dLine  = '<span class="line">'+img('imagens/line.gif')+'</span>';
                            dPrev  = '<span class="prev">'+img('imagens/prev.gif')+'</span>';
                            dPage  = '<span class="pg"><input type="text" class="page"></span>';
                            dNext  = '<span class="next">'+img('imagens/next.gif')+'</span>';
                            dLast  = '<span class="last">'+img('imagens/last.gif')+'</span>';
                            dPaginator = dFirst+dPrev+dLine+dPage+dLine+dNext+dLast;

                            $('.paginacao').html(dPaginator);

                           if ($('total', xml)[0]){
                             total = $('total', xml).text();
                             $(id+' .total').html($('total', xml).text());
                           }
                           if ($('page', xml)[0]){
                              $(id+' .page').val($('page', xml).text());
                           }

                           $('.next').click(function(){
                              if( (opt.pagina * opt.linhaporpagina) < total){
                                 opt.pagina = opt.pagina +1; loadgrid();
                              }
                           })
                           $('.prev').click(function(){ if(opt.pagina != 1){ opt.pagina= opt.pagina -1; loadgrid();} })
                         if($('.VVSGridLoad')[0]){
                           //$('.VVSGridLoad').remove();
                         }
                         }



                      });

                }



               if (opt.datatype == 'post'){
                  $.post(opt.request+opt.qs_load_list,{},function(d){
                     $(id+' tbody').html(d);
                     addAcao();
                  });
               }

             }

            function img(src){
              return '<img src="'+src+'" />';
            }



            // cria um coluna no TR do header da Grid
            function CriaColunasCampo(){

                 cols = opt.cols;
                 col = cols.split('|');

                 $('.cols').html('');

                 for (i=0;i<col.length;i++){
                   $('tr.cols').append('<th>'+col[i]+'</th>');
                 }
                 if(opt.actions){
                   $('tr.cols').append('<th class="grid_title_action">Ação</th>');
                 }
            }



          function addTR(xml, type){
            var $trs = '';



                  $('row',xml).each(function(){
                  //coloco os ID
                  if($(this).attr('id')){
                    $trs += '<tr rel="'+$(this).attr('id')+'">';
                  }else{
                    $trs += '<tr>';
                  }

                   $(this).children().each(function(){
                     $trs += '<td>'+$(this).text()+'</td>';

                   });
                   if(opt.actions){
                       $trs += grid_action;
                   }
                   $trs +='</tr>';

                   $qtd_rows.html(parseInt($qtd_rows.html())+1);

                  });



              if(type == 'append'){
                $(id+' tbody').append($trs);
              }else  if(type == 'prepend'){
                $(id+' tbody').prepend($trs);
              }else{
                $(id+' tbody').html('');
                $(id+' tbody').html($trs);
              }



          }





           function Search(){

              $(id+' .search').click(function(){
                 opt.search = $(id+' #search-text').val();

                 if(opt.search!=''){
                     loadgrid();
                 }

              })

           }

            
            function addAcao(){
               if (opt.actions){
                   $(id+' thead tr .title_action').remove();
                   $(id+' thead tr:last').append(title_actions);
                   $(id+' .grid_action').remove();
                   $(id+' tbody tr').append(grid_action);
                   $('#quicksearch').remove();
                   $('.TVVSGrid tbody tr').quicksearch({position: 'prepend',attached: id+' .grid_search',labelText: 'buscar',loaderText: ''});
              }
            }

            if(opt.datatype != 'xml'){

                // INSERIR BUSCA
                if (!$('#quicksearch')[0]){
                    $('.TVVSGrid tbody tr').quicksearch({position: 'prepend',attached: id+' .grid_search',labelText: 'buscar',loaderText: ''});
                    $('.TVVSGrid tbody tr').hover(function(){
                      $(this).css('background','#DDECFE');
                     },function(){
                      $(this).css('background','none');
                    });
                }else{
                    $('#quicksearch').remove();
                    $('.TVVSGrid tbody tr').quicksearch({position: 'prepend',attached: id+' .grid_search',labelText: 'buscar',loaderText: ''});
                }
             }else{
                $('.grid_search').remove();
             }
               loadgrid();
               Search();



                 //DETAIL da grid
                $(id+' .grid_edit').on('click',function(){
                    
                    var post_id  = $(this).parent().parent().attr('rel');
                    var obj = $(this);
                   // Ajax_gridBox('Editar '+opt.title, opt.request+opt.qs_detail, {id: post_id},opt.BoxEditWidth);

                    $.post(opt.request+opt.qs_detail, {id: post_id}, function(d){

                        $win = gridbox(opt.title);
                        $win.find('.content').html(d);
                        $win.width(opt.BoxEditWidth);
                        $.getScript('../js/inicializacoes.js');

                        var form = $win.find('.content form'+opt.frm_detailadd);

                        //EDITANDO
                        $(opt.frm_detailedit).bind('submit',function(){ //#frm_detailedit

                            $.post(opt.request+opt.qs_edit,$(this).serialize(),function(d){
                               post_id = obj.parent().parent().attr('rel');

                              // if(d != 'N'){
                                 if(getMessageError(d, opt.msgErroEdit) == ''){
                                    
                                    $win.find('.content').prepend('<div class="success messageGrid">'+opt.msgEditSuccess+'</div>');
                                    $('.error').remove();
                                    $('.messageGrid').show(300,function(){setTimeout(function(){$('.GridBox-ligthbox').remove();}, 3000);});
                                    
                                    $.post(opt.request+opt.qs_refresh_row,{id:post_id},function(tr){
                                      // $('.GridBox-ligthbox').remove();
                                       obj.parent().parent('tr').before(tr);
                                       obj.parent().parent('tr').fadeOut(300,function(){
                                       obj.parent().parent('tr').remove();
                                       });

                                       addAcao();

                                    })
                               }else{
                                    //alert(opt.msgErroEdit);
                                    $('.messageGrid').remove();
                                    $win.find('.content').prepend('<div class="error messageGrid">'+getMessageError(d, opt.msgErroEdit)+'</div>'); 
                                    $('.messageGrid').show(300);
                               }
                            })
                            return false;
                        });
                    });
                });
     
                 //DELETANDO DA GRID
                $(id+' .grid_remove').on('click',function(){
                    var post_id  = $(this).parent().parent().attr('rel');
                    var obj_tr = $(this).parent().parent();
                    var cfm = confirm(opt.msgCfmDelete);
                    objg = $(this);
            
                    if(cfm){
                        $.post(opt.request+opt.qs_delete,{id: post_id},function(di){
                           if(di != 'N'){
                             // retira a quantidade
                             if(opt.datatype == 'xml'){
                               $qtd_rows.html(parseInt($qtd_rows.html())-1);
                             }
                              obj_tr.fadeOut(400);
                           }else{
                              alert(opt.msgErroDelete);
                           }
                       
                        });
                    }
                });
                
                
                $(id+' #add').on('click',function(){

                      $.post(opt.request+opt.qs_detail_add,function(d){

                            $win = gridbox(opt.title);
                            $win.find('.content').html(d);
                            $win.width(opt.BoxAddWidth);
                            $.getScript('../js/inicializacoes.js');

                            var form = $win.find('.content form'+opt.frm_detailadd);

                            form.bind('submit',function(){

                                if(opt.datatype != 'xml'){

                                    $.post(opt.request+opt.qs_add, $(this).serialize(),function(NewRow){
                                        if(getMessageError(NewRow, opt.msgErroAdd) == ''){    
                                          $win.find('.content').prepend('<div class="success messageGrid">'+opt.msgAddSuccess+'</div>');  
                                          $('.TVVSGrid tbody').prepend(NewRow);
                                          $('.error').remove();
                                          $('.messageGrid').show(300,function(){
                                             setTimeout(function(){
                                                    $('.GridBox-ligthbox').remove();
                                                }, 3000);
                                          });
                                          addAcao();
                                        }else{
                                          $('.messageGrid').remove();  
                                          $win.find('.content').prepend('<div class="error messageGrid">'+getMessageError(NewRow, opt.msgErroAdd)+'</div>'); 
                                          $('.messageGrid').show(300);
                                        }
                                    });
                                }else{

                                     $.post(opt.request+opt.qs_add, $(this).serialize(),function(xml){
                                        res = $('add', xml).text();
                                        msg = $('msg', xml).text();
                                        if(res == 'Y'){
                                          addTR(xml, 'prepend');
                                        }else{
                                          if(msg == ''){
                                            alert('Erro inesperado');
                                          }else{
                                            alert(msg);
                                          }
                                        }
                                     }, 'xml');

                                }
                                return false;
                            });
                       });
                       return false;
                });

         }

        
        function getMessageError(str, msgError){
            var res = '';
            var msg = str.split('|');
            
            if(($.trim(str) == 'N') || ($.trim(str)=='') ){
               res = msgError; 
            }else if($.trim(msg[0]) == 'N'){
               res = msgError;
               if($.trim(msg[1])!=''){
                  res = msgError + ' <br />  '+ msg[1];
               } 
            }
            return res;  
        }


        function Ajax_gridBox(title, url, posts, width){
            var box = $('.TVVSGridBox');
            if(box[0]){
              $('.TVVSGridBox').remove();
            }
            $('.TVVSGrid').before('<div class="TVVSGridBox"><img src="../imagens/loading.gif" /></div>');
            $.post(url, posts ,function(d){
              if(width!=''){$('.TVVSGridBox').css('width', width);}
             $('.TVVSGridBox').addClass('ui-corner-all').html('<div class="barGB"><span class="titleGB">'+title+'</span><span class="closeGB">[X]</span></div><div class="contentGB">'+d+'</div>');
             $('.barGB').addClass('ui-corner-all').show();
             $('.contentGB').show();
             $.getScript('../js/inicializacoes.js');
             $('.TVVSGridBox').draggable({ handle: '.barGB', cancel : '.closeGB',  containment: 'body', scroll: false , opacity: 0.7 ,cursor: 'move' });
             $('.closeGB').addClass('ui-corner-all');
            });
            $('.closeGB').on('click',function(){
              $this = $(this);
              $this.parent().parent().fadeOut(50,function(){$this.parent().parent().remove()});
            });


         }




        function gridbox(title, width){
            var box = $('.GridBox-ligthbox');
            if(box[0]){
              $('.GridBox-ligthbox').remove();
            }
            $('body').before('<div class="GridBox-ligthbox"><div class="TVVSGridBox"></div></div>');
            if(width!=''){$('.TVVSGridBox').css('width', width);}
            $('.TVVSGridBox').html('<div class="barGB"><span class="titleGB">'+title+'</span><span class="closeGB"><img src="../imagens/close.png" /> Fechar</span></div><div class="contentGB"><span class="content"><img src="../imagens/loading.gif" /></span></div>');
            $('.TVVSGridBox').show();

            $('.TVVSGridBox .barGB').show();
            $('.TVVSGridBox .contentGB').show();
            $('.TVVSGridBox').draggable({ handle: '.barGB', cancel : '.closeGB',  containment: 'body', scroll: false , opacity: 0.7 ,cursor: 'move' });

            $('.GridBox-ligthbox .TVVSGridBox .closeGB').on('click',function(){
              $this = $(this);
              $this.parent().parent().fadeOut(200,function(){$this.parent().parent().parent().remove()});
            });

            /*$('.TVVSGridBox .barGB').on('click',function(){

               $('.GridBox-ligthbox .TVVSGridBox').css( {margin:'1px', top: '0px', width : '100%', height: '100%' });
               $('.GridBox-ligthbox').css( {margin:'1px', top: '28px', width : '100%', height: '100%' });
            })*/

            return $('.TVVSGridBox');
        }
        
        
        


   }


   





function isset () {
    var a = arguments, l = a.length, i = 0, undef;
    if (l === 0) { throw new Error('Empty isset'); }
    while (i !== l) {if (a[i] === undef || a[i] === null) {return false;}i++; }
    return true;
}

