var timeout;

function EnviaOrdenacao(pCampo, pColunaOrdenacao) {
    $form = $(pCampo).closest('form');
    $ordemAtual = $($form).children('#CMP___ordenacao').val();
    if ($ordemAtual == pColunaOrdenacao) {
        $sentidoAtual = $($form).children('#CMP___ordenacaoSentido').val();
        if ($sentidoAtual == 'asc') {
            $($form).children('#CMP___ordenacaoSentido').val('desc');
        }
        else {
            $($form).children('#CMP___ordenacaoSentido').val('asc');
        }
    }
    else {
        $($form).children('#CMP___ordenacaoSentido').val('asc');
    }
    $($form).children('#CMP___ordenacao').val(pColunaOrdenacao);
    $($form).submit();
}

function EnviaAcao(pCampo, pNomeAcao, pParametros) {
    if (pCampo.form['CMP___acao']) {
        pCampo.form['CMP___acao'].value = pNomeAcao;
    }
    else {
        if (pCampo.form['CMP_acao']) {
            pCampo.form['CMP_acao'].value = pNomeAcao;
        }
    }
    if (pCampo.form['CMP___parametros']) {
        pCampo.form['CMP___parametros'].value = pParametros;
    }
    var popup = $(pCampo).parents("#formPopup");			// Verifica se estÃ¡ dando o post de dentro de um popup.

    pCampo.form.submit();
}

function EnviaAcaoLinha(pCampo, pNomeAcao, pParametros) {
    if (pCampo.form['CMP___acao']) {
        pCampo.form['CMP___acao'].value = pNomeAcao;
    }
    else {
        if (pCampo.form['CMP_acao']) {
            pCampo.form['CMP_acao'].value = pNomeAcao;
        }
    }
    pCampo.form['CMP___parametros'].value(pParametros);

    var popup = $(pCampo).parents("#formPopup");			// Verifica se estÃ¡ dando o post de dentro de um popup.

    pCampo.form.submit();
}

function METODO_POPUP(pObjeto, pController, pMetodo, pParametros, pConfMsg, pConfTitulo) {
    var confirma = false;
    // Confirma execucao da acao
    if (pConfMsg != '') {
        jConfirm(pConfMsg, pConfTitulo, function (r) {
            if (r) {
                confirma = true;
            }
        }
        );
    }
    else {
        confirma = true;
    }
    //
    // Chama a tela popup
    if (confirma) {
        var parametros = 'controller=' + pController + '&metodo=' + pMetodo + '&' + pParametros;
        var url = '/paginas/carregueAjax.php?' + parametros;
        $.ajax({
            url: url,
            dataType: 'html',
            async: false,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            success: function (data) {
                $('#formPopup').html('<div style="text-align:center;">' + data + '</div>');
                var alt = 300;
                var larg = 850;
                var titulo = '';
                $('#formPopup  #CMP_NU_EMPRESA').change(function () {
                    atualizaEmbarcacaoProjetoLocal('#CMP_NU_EMPRESA', '#formPopup');
                });

                atualizaEmbarcacaoProjetoLocal('#CMP_NU_EMPRESA', '#formPopup');

                $("#formPopup .data").datepicker().mask('99/99/9999');
                $("#formPopup .cmpDat").datepicker().mask('99/99/9999');

                var cmptitulo = $("#formPopup #CMP___titulo:first").val();
                if (cmptitulo != '' && cmptitulo != 'undefined') {
                    titulo = cmptitulo;
                }
                var cmpalt = $("#formPopup #CMP___altura:first").val();
                if (cmpalt > 0) {
                    alt = cmpalt;
                }
                var cmplarg = $("#formPopup #CMP___largura:first").val();
                if (cmplarg > 0) {
                    larg = cmplarg;
                }
//				alert('larg='+larg+' alt='+alt);
                alt = alt * 1;
                larg = larg * 1;
                var posicao = '';
                if (pObjeto) {
                    posicao = '{ my: "left top", at: "left bottom", of: pObjeto, collision: "none" }';
                }
                $('#formPopup').dialog({
                    height: alt
                    , width: larg
                    , title: titulo
                    , modal: true
                    , position: posicao
                });
//				msg.floatingMessage( "destroy" );

            }
        });

    }

}



function METODO_POPUP_POST(pBotao, pNomeAcao, pConfMsg, pConfTitulo) {
    pBotao.form['CMP___acao'].value = pNomeAcao;
    var confirma = false;
    // Confirma execucao da acao
    if (pConfMsg != '') {
        jConfirm(pConfMsg, pConfTitulo, function (r) {
            if (r) {
                confirma = true;
            }
        }
        );
    }
    else {
        confirma = true;
    }
    //
    // Chama a tela popup
    if (confirma) {
        var alt = 300;
        var larg = 850;
        var $form = $(pBotao).closest("form").serialize();
        var url = '/paginas/carregueAjaxPost.php';
        $('#formPopup').html('<div style="text-align:center;">(Carregando formulário. Por favor aguarde...)</div>');
        var cmpalt = $("#formPopup #CMP___altura:first").val();
        if (cmpalt > 0) {
            alt = cmpalt;
        }
        var cmplarg = $("#formPopup #CMP___largura:first").val();
        if (cmplarg > 0) {
            larg = cmplarg;
        }
        //				alert('larg='+larg+' alt='+alt);
        alt = alt * 1;
        larg = larg * 1;
        var janela = $('#formPopup').dialog({
            height: alt,
            width: larg,
            title: 'Popup',
            modal: true,
            close: function (event, ui) {
                if ($(".filtros").length > 0) {
                    $(".filtros").parents("form").submit();
                }
                else {
                    location.reload();
                }
            }
        });
        $.ajax({
            url: url,
            type: 'POST',
            data: $form,
            async: false,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            success: function (data) {
                janela.html(data);
                var titulo = janela.find("#CMP___titulo").val();
                if (titulo != '' && titulo != 'undefined') {
                    janela.dialog("option", "title", titulo);
                }

                var alt = janela.find("#CMP___altura:first").val();
                if (alt > 0) {
                    janela.dialog("option", "height", alt);
                }
                var larg = janela.find("#CMP___largura:first").val();
                if (larg > 0) {
                    janela.dialog("option", "width", larg);
                }

                $('#formPopup  #CMP_NU_EMPRESA').change(function () {
                    atualizaEmbarcacaoProjetoLocal('#CMP_NU_EMPRESA', '#formPopup');
                });
                atualizaEmbarcacaoProjetoLocal('#CMP_NU_EMPRESA', '#formPopup');

                $(".data").datepicker().mask('99/99/9999');
                $(".cmpDat").datepicker().mask('99/99/9999');
                var msg = janela.find("#CMP___msg").val();
                if (msg != '' && msg != 'undefined') {
                    jAlert(msg, 'Atenção');
//					$.floatingMessage(msg, { align:"left",  verticalAlign:"bottom", className : "ui-state-error"});
                }
                if ($('#formPopup #CMP___sucesso').val() == '1') {
                    if ($('#formPopup #CMP___acao_sucesso').val() == '1') {
                        $('#formPopup').dialog("close");
                    }
                }

            }
        });

    }
    return false;
}

function atualizaEmbarcacaoProjeto(pcmbEmpresa) {
    var nu_embarcacao_projeto;
    if ($(pcmbEmpresa).val() > 0) {
        nu_embarcacao_projeto = $('#CMP_NU_EMBARCACAO_PROJETO').val();
        var url = '/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA=' + $(pcmbEmpresa).val() + '&NU_EMBARCACAO_PROJETO=' + $('#CMP_NU_EMBARCACAO_PROJETO').val();
        $.ajax({
            url: url,
            async: false,
            success: function (data) {
                $(pcmbEmpresa).closest("table").find('#tdEmbarcacaoProjeto').html('<select id="CMP_NU_EMBARCACAO_PROJETO" name="CMP_NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>' + data + '</select>');
                $('#CMP_NU_EMBARCACAO_PROJETO').val(nu_embarcacao_projeto);
            }
        });
    }
    else {
        $(pcmbEmpresa).closest("table").find('#tdEmbarcacaoProjeto').html('(selecione a empresa...)');
    }
}

function atualizaEmbarcacaoProjetoLocal(pNomeCampoEmpresa, pContainer) {
    var nu_embarcacao_projeto;
    if ($(pContainer + ' ' + pNomeCampoEmpresa).val() > 0) {
        nu_embarcacao_projeto = $(pContainer + ' #CMP_NU_EMBARCACAO_PROJETO').val();
        var url = '/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA=' + $(pContainer + ' ' + pNomeCampoEmpresa).val() + '&NU_EMBARCACAO_PROJETO=' + $(pContainer + ' #CMP_NU_EMBARCACAO_PROJETO').val();
        $.ajax({
            url: url,
            async: false,
            success: function (data) {
                $(pContainer).find('#tdEmbarcacaoProjeto').html('<select id="CMP_NU_EMBARCACAO_PROJETO" name="CMP_NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>' + data + '</select>');
                $(pContainer + '#CMP_NU_EMBARCACAO_PROJETO').val(nu_embarcacao_projeto);
            }
        });
    }
    else {
        $(pContainer).find('#tdEmbarcacaoProjeto').html('(selecione a empresa...)');
    }
}
function atualizaEmbarcacaoProjetoFiltro(pcmbEmpresa) {
    var nu_embarcacao_projeto;

    if ($(pcmbEmpresa).val() > 0) {
        nu_embarcacao_projeto = $('#CMP_FILTRO_NU_EMBARCACAO_PROJETO').val();
        var url = '/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA=' + $(pcmbEmpresa).val() + '&NU_EMBARCACAO_PROJETO=' + $('#CMP_FILTRO_NU_EMBARCACAO_PROJETO').val();
        $.ajax({
            url: url,
            async: false,
            success: function (data) {
                $('.filtros').find('#tdEmbarcacaoProjeto').html('<select id="CMP_FILTRO_NU_EMBARCACAO_PROJETO" name="CMP_FILTRO_NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>' + data + '</select>');
                $('#CMP_FILTRO_NU_EMBARCACAO_PROJETO').val(nu_embarcacao_projeto);
            }
        });
    }
    else {
        $('.filtros').find('#tdEmbarcacaoProjeto').html('<div style="margin-top:5px;">(selecione a empresa...)</div>');
    }
}

function ConfirmaAcaoxx(pCampo, pNomeAcao, pConfMsg, pConfTitulo) {
    if (pConfMsg != '') {
        jConfirm(pConfMsg, pConfTitulo, function (r) {
            if (r) {
                EnviaAcao(pCampo, pNomeAcao);
            }
        }
        );
    }
    else {
        EnviaAcao(pCampo, pNomeAcao);
    }
}

function ConfirmaAcao(pCampo, pNomeAcao, pConfMsg, pConfTitulo, pParametros) {
    //alert ($(popup).attr('id'));
    // DÃ¡ para saber se vem do popup ou nÃ£o e com isso modificar a forma como o post Ã© feito.

    // Se estiver no popup, enviar o form do popup e remontar a tela
    // SenÃ£o, sÃ³ enviar e esperar para recarregar.
    if (pConfMsg != '') {
        jConfirm(pConfMsg, pConfTitulo, function (r) {
            if (r) {
                EnviaAcao(pCampo, pNomeAcao, pParametros);
            }
        }
        );
    }
    else {
        EnviaAcao(pCampo, pNomeAcao, pParametros);
    }
}

function ConfirmaAcaoLinha(pCampo, pNomeAcao, pConfMsg, pConfTitulo, pParametros) {
    //alert ($(popup).attr('id'));
    // DÃ¡ para saber se vem do popup ou nÃ£o e com isso modificar a forma como o post Ã© feito.

    // Se estiver no popup, enviar o form do popup e remontar a tela
    // SenÃ£o, sÃ³ enviar e esperar para recarregar.
    if (pConfMsg != '') {
        jConfirm(pConfMsg, pConfTitulo, function (r) {
            if (r) {
                EnviaAcaoLinha(pCampo, pNomeAcao, pParametros);
            }
        }
        );
    }
    else {
        EnviaAcao(pCampo, pNomeAcao, pParametros);
    }
}

function ConfirmaRedirect(pConfMsg, pConfTitulo, pLink) {
    if (pConfMsg != '') {
        if (jConfirm(pConfMsg, pConfTitulo, function (r) {
            if (r) {
                window.location = pLink;
            }
        }))
            ;
    }
    else {
        window.location = pLink;
    }
}

function ExibeForm(pAcao, pClasse, pFuncao, pParametros) {
    var url = '';
    url = '/LIB/form_ajax.php?classe=' + pClasse + '&funcao=' + pFuncao;
    if (pParametros != '') {
        url = url + '&' + pParametros;
    }
    window.location = url;
}

function carregaFormPopup(pParam) {
    var url = '/LIB/form_ajax.php?' + pParam;
    $('#formPopup').html('<div style="text-align:center;">(Carregando formulário. Por favor aguarde...)</div>');
    var janela = $('#formPopup').dialog({
        height: 230,
        width: 750,
        title: 'Alteração de processo',
        modal: true
    });
    $.ajax({
        url: url,
        dataType: 'html',
        async: false,
        success: function (data) {
            janela.html(data);
            var titulo = janela.find("#CMP___titulo").val();
            if (titulo != '' && titulo != 'undefined') {
                janela.dialog("option", "title", titulo);
            }
            var alt = janela.find("#CMP___altura").val();
            if (alt > 0) {
                janela.dialog("option", "height", alt);
            }
            var larg = janela.find("#CMP___largura").val();
            if (larg > 0) {
                janela.dialog("option", "width", larg);
            }
            $('#formPopup  select[id="CMP_NU_EMPRESA"]').change(function () {
                atualizaEmbarcacaoProjetoLocal("select[id=\"CMP_NU_EMPRESA\"]", "#formPopup");
            });
            atualizaEmbarcacaoProjetoLocal("select[id=\"CMP_NU_EMPRESA\"]", "#formPopup");
            $(".data").datepicker().mask('99/99/9999');
            $(".cmpDat").datepicker().mask('99/99/9999');

        }
    });
}

function CarregueAjax(pControle, pMetodo, pParametros, pRecipiente, pForm, pObjeto) {
    METODO_POPUP(pObjeto, pControle, pMetodo, pParametros, "", "");
}
function CarregueAjaxOriginal(pControle, pMetodo, pParametros, pRecipiente, pForm) {
    var url = '../paginas/carregueAjax.php?controle=' + pControle + '&metodo=' + pMetodo + '&' + pParametros;
    $.ajax({
        url: url,
        dataType: 'html',
        async: false,
        success: function (data) {
            ApresenteAjax(data, pRecipiente)
        }
    });
}

function SubmitAjax(pBotao, pControle, pMetodo, pNomeAcao, pParametros, pRecipiente, pForm) {
    pBotao.form['CMP_acao'].value = pNomeAcao;
    var url = '../paginas/carregueAjax.php?controle=' + pControle + '&metodo=' + pMetodo + '&' + pParametros;
    var $form = $(pBotao).closest("form").serialize();
    $.ajax({
        type: 'POST',
        data: $form,
        url: url,
//		dataType: 'html',
        async: false,
        success: function (data) {
            ApresenteAjax(data, pRecipiente)
        }
    });
}


function ApresenteAjax(pData, pRecipiente) {
    var $xml = $(pData);
    var $retorno = $xml.find("retorno").text();
    var $conteudo = $xml.find("conteudo").text();
    var $msg = $xml.find("msg").text();
    var $tituloMsg = $xml.find("tituloMsg").text();
    var $tituloJanela = $xml.find("tituloJanela").text();
    $('#' + pRecipiente).html($conteudo);
    $('#' + pRecipiente).children("form").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $(this).children("input[type=button] :first-child").click();
            return false;
        } else {
            return true;
        }
    });
    $('#' + pRecipiente).dialog({
        height: 230,
        width: 750,
        title: $tituloJanela,
        modal: true
    });
    if ($msg != '') {
        jAlert($msg, $tituloMsg, function (r) {
            if ($retorno == 0) {
                $('#' + pRecipiente).dialog("close");
                location.reload(true);
            }

        });
    }
    else {
        if ($retorno == 0) {
            $('#' + pRecipiente).dialog("close");
            location.reload(true);
        }
    }

}


function ConfirmaAcaoPopup(pParam) {
    var url = '/LIB/form_ajax.php?' + pParam;
    $('#formPopup').html('<div style="text-align:center;">(Carregando formulário. Por favor aguarde...)</div>');
    $('#formPopup').dialog({
        height: 230,
        width: 750,
        title: 'Alteração de processo',
        modal: true
    });
    $.ajax({
        url: url,
        dataType: 'html',
        async: false,
        success: function (data) {
            $('#formPopup').html(data);
            $('select[id="CMP_NU_EMPRESA"]').change(function () {
                atualizaEmbarcacaoProjeto(this);
            });
            atualizaEmbarcacaoProjeto();
            $(".data").datepicker().mask('99/99/9999');
            $(".cmpDat").datepicker().mask('99/99/9999');

        }
    });
}



function carregaFormDinamico(pClasse, pMetodo, pParametros, pTitle, pWidth, pHeight) {
    var url = '/LIB/formDinamico_ajax.php?classe=' + pClasse + '&metodo=' + pMetodo + '&' + pParametros;
    if (pWidth == null) {
        pWidth = 750;
    }
    if (pHeight == null) {
        pHeight = 230;
    }
    $.ajax({
        url: url,
        dataType: 'html',
        async: false,
        success: function (data) {
            $('#formPopup').html(data);
            $('select[id="CMP_NU_EMPRESA"]').change(function () {
                atualizaEmbarcacaoProjeto(this);
            });
            atualizaEmbarcacaoProjeto();
            $(".data").datepicker().mask('99/99/9999');
            $(".cmpDat").datepicker().mask('99/99/9999');

            $('#formPopup').dialog({
                height: 230,
                width: 750,
                title: 'Alteração de processo',
                modal: true
            });
        }
    });
}


function carregaMudaVisto(pParam) {
    var url = '/LIB/form_processoMte_ajax.php?' + pParam;
    $.ajax({
        url: url,
        dataType: 'html',
        async: false,
        success: function (data) {
            $('#formPopup').html(data);
            $('#formPopup').dialog({
                height: 230,
                width: 750,
                title: 'Alteração de visto de um processo',
                modal: true
            });
            $(".data").datepicker().mask('99/99/9999');
            $(".cmpDat").datepicker().mask('99/99/9999');

        }
    });
}

function aba() {
    $(".aba").tabs({
        active: getTabAtivo(),
        heightStyle: "content",
        beforeActivate: function (event, ui) {
            if (ui.newTab.context.href.substr(0, 4) == 'http' || ui.newTab.context.href.substr(0, 1) == '/') {
                window.location.href = ui.newTab.context.href;
                return false;
            }
            else {
                return true;
            }
        }
    });

    /* Obtem a TAB que estÃ¡ ativa */
    function getTabAtivo() {
        var li = 0;
        var li_eq = -1;
        $('.aba ul li a').each(function () {

            if ($(this).attr('class') == 'ativo') {
                li_eq = li;

            }
            li++;
        });

        return li_eq;
    }
}
function gridbox(title, width) {
    var box = $('.GridBox-ligthbox');
    if (box[0]) {
        $('.GridBox-ligthbox').remove();
    }
    $('body').before('<div class="GridBox-ligthbox"><div class="TVVSGridBox"></div></div>');
    if (width != '') {
        $('.TVVSGridBox').css('width', width);
    }
    $('.TVVSGridBox').html('<div class="barGB"><span class="titleGB">' + title + '</span><span class="closeGB"><img src="../imagens/close.png" /> Fechar</span></div><div class="contentGB"><span class="content"><img src="../imagens/loading.gif" /></span></div>');
    $('.TVVSGridBox').show();

    $('.TVVSGridBox .barGB').show();
    $('.TVVSGridBox .contentGB').show();
    $('.TVVSGridBox').draggable({
        handle: '.barGB',
        cancel: '.closeGB',
        containment: 'body',
        scroll: false,
        opacity: 0.7,
        cursor: 'move'
    });

    $('.GridBox-ligthbox .TVVSGridBox .closeGB').on('click', function () {
        $this = $(this);
        $this.parent().parent().fadeOut(200, function () {
            $this.parent().parent().parent().remove()
        });
    });

    return $('.TVVSGridBox');
}

function filtroDinamico(idFiltro, idGrid) {
    $(idFiltro + ' input[type=checkbox]').on('click', function () {

        var nome = $(this).val();
        var tdEq = -1;
        var thEq = -1;
        var $tr = $(idGrid + " tbody tr");
        var $th_head = $(idGrid + ' thead tr th');

        if ($(this).attr('checked') == false) {
            $th_head.each(function () {
                tdEq++;
                if ($(this).text() == nome) {
                    thEq = tdEq;
                    th = $(this);
                }
            })
            th.fadeOut(300);
            $tr.each(function () {
                $('td:eq(' + thEq + ')', this).hide();
            })

        } else {

            $th_head.each(function () {
                tdEq++;
                if ($(this).text() == nome) {
                    thEq = tdEq;
                    th = $(this);
                }
            });

            if (thEq == -1) {
                if ($(idGrid)[0]) {
                    $(idGrid).html('<div class="success"> <img src="/imagens/loading.gif" /> Aguarde, carregando campo requisitado</div> ');
                    $(idFiltro).submit();
                }
                // alert('Para exibir este campo no relatÃ³rio Ã© necessÃ¡rio clicar no botÃ£o "Gerar" ')
            }
            //alert('existe: '.th);

            //if(th[0]){alert('Para exibir este campo no relatÃ³rio Ã© necessÃ¡rio clicar no botÃ£o "Gerar" ');}
            th.fadeIn(300);
            $tr.each(function () {
                $('td:eq(' + thEq + ')', this).show();
            })
        }
    });


}

var menuAtual = '';
function mOvr(src, clrOver) {
    if (!src.contains(event.fromElement)) {
        src.style.cursor = 'hand';
        src.bgColor = clrOver;
    }
}

function mOut(src, clrIn) {
    if (!src.contains(event.toElement)) {
        src.style.cursor = 'default';
        src.bgColor = clrIn;
    }
}

function mClk(src) {
    if (event.srcElement.tagName == 'TD') {
        src.children.tags('A')[0].click();
    }
}
function formatar(num) {
    num = num.toString().replace(/[^0-9]/g, '');
    //num = num.toString().replace(/\$|\,|\./g,'');
    if (isNaN(num))
        num = "0";
    if (num < 100)
    {
        return num;
    }
    else
    {
        cents = Math.floor(num % 100);
        num = Math.floor((num) / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
        return ('R$ ' + num + ',' + cents);
    }
}

function formatarData(pdata) {
    pdata = pdata.toString().replace(/[^0-9]/g, '');
    if (pdata.length > 4)
    {
        return pdata.substring(0, 2) + '/' + pdata.substring(2, 4) + '/' + pdata.substring(4);
    }
    else
    if (pdata.length > 2)
    {
        return pdata.substring(0, 2) + '/' + pdata.substring(2);
    }
    else
        return pdata;
}


function formatarCPF(pCampo) {
    pCampo = pCampo.toString().replace(/[^0-9]/g, '');
    if (pCampo.length > 9)
    {
        return pCampo.substring(0, 3) + '.' + pCampo.substring(3, 7) + '.' + pCampo.substring(7, 10) + '-' + pCampo.substring(10);
    }
    else
    if (pCampo.length > 6)
    {
        return pCampo.substring(0, 3) + '.' + pCampo.substring(3, 7) + '.' + pCampo.substring(7);
    }
    else
    if (pCampo.length > 3)
    {
        return pCampo.substring(0, 3) + '.' + pCampo.substring(3);
    }
    else
        return pCampo;
}

function veracomp(codigo, tipo, processo) {
    if (tipo == "MTE") {
        var pagina = "http://www.mte.gov.br/Empregador/TrabEstrang/Pesquisa/ConsultaProcesso.asp?Parametro=" + processo + "&Opcao=1";
//    var pagina = "acomp_mostra_processo_mte.php?codigo=" + codigo;
//    var ret = AbrePagina("",pagina,"","");
        AbrePagina("Acomp", pagina);
    } else {
        alert("Tipo invalido");
    }
}

//function AbrePagina(tipo,pagina,emp,cand) {
function AbrePagina(nome, pagina) {
    var ret = '';
//    var MyArgs = new Array(emp,cand,tipo,"");
//    var WinSettings = "center:yes;resizable:yes;dialogHeight:580px;dialogWidth=700px";
//    var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
//    if (MyArgsRet != null) {
//        var msg = MyArgsRet[0].toString();
//        var erro = MyArgsRet[1].toString();
//    }
    var WinSettings = "status=0,menubar=0,resizeble=1,location=0,scrollbars=1,height=580,width=700";
    window.open(pagina, nome, WinSettings);
    //return msg;
}

function abreMenu(id)
{
    if ((id != menuAtual) && (menuAtual != ''))
    {
        document.getElementById(menuAtual).className = "off";
    }

    if (document.getElementById(id).className == "on") {
        document.getElementById(id).className = "off";
    } else if (document.getElementById(id).className == "off") {
        document.getElementById(id).className = "on";
        menuAtual = id;
    }
}

function RecarregarCombo(pForm, pComboPai, pComboFilho) {
    var nomeTemplate = $('#' + pForm + ' #CMP___nomeTemplate').val();
    var nomeComboPai = 'CMP_' + pComboPai;
    var valorPai = $('#' + pForm + ' #CMP_' + pComboPai).val();
    var valorFilho = $('#' + pForm + ' #CMP_' + pComboFilho).val();
    $.ajax({
        type: 'POST'
        , url: '/paginas/carregueSemMenu.php?controller=cCTRL_FRAMEWORK&metodo=ValoresCombo'
        , data: {nomeTemplate: nomeTemplate, comboFilho: pComboFilho, comboPai: pComboPai, valorPai: valorPai, valorFilho: valorFilho}
        , success:
                function (data) {
                    var cmbFilho = $('#' + pForm + ' #CMP_' + pComboFilho);
                    cmbFilho.html('');
                    $.each(data, function () {
                        cmbFilho.append('<option value="' + this[0] + '">' + this[1] + '</option>');
                    });
                }
        , dataType: 'json'
    });
}

function RecarregarFiltro(pForm, pComboPai, pComboFilho) {
    var nomeTemplate = $('#' + pForm + ' #CMP___nomeTemplate').val();
    var valorPai = $('#' + pForm + ' #CMP_FILTRO_' + pComboPai).val();
    $.ajax({
        type: 'POST'
        , url: '/paginas/carregueSemMenu.php?controller=cCTRL_FRAMEWORK&metodo=ValoresFiltro'
        , data: {nomeTemplate: nomeTemplate, comboFilho: pComboFilho, comboPai: pComboPai, valorPai: valorPai}
        , success:
                function (data) {
                    var cmbFilho = $('#' + pForm + ' #CMP_FILTRO_' + pComboFilho);
                    cmbFilho.html('');
                    $.each(data, function () {
                        cmbFilho.append('<option value="' + this[0] + '">' + this[1] + '</option>');
                    });
                    cmbFilho.trigger('change');
                }
        , dataType: 'json'
    });
}

function CarregueServico($pChamador, $pController, $pMetodo, $pNomeAcao, $pParametrosAdicionais, $pDivDestino, pConfMsg, pConfTitulo) {
    var confirma = false;
    // Confirma execucao da acao
    if (pConfMsg != '') {
        jConfirm(pConfMsg, pConfTitulo, function (r) {
            if (r) {
                confirma = true;
            }
        }
        );
    }
    else {
        confirma = true;
    }
    if (confirma) {
        $form = $($pChamador).closest("form");
        if ($($form).children('#CMP___acao')) {
            $($form).children('#CMP___acao').val($pNomeAcao);
        }
        else {
            if ($($form).children('#CMP_acao')) {
                $($form).children('#CMP_acao').val($pNomeAcao);
            }
        }
        var $url = '/paginas/carregueSemMenu.php?controller=' + $pController + '&metodo=' + $pMetodo + '&' + $pParametrosAdicionais;
        var $formData = $form.serialize();
        $.ajax({
            type: 'POST'
            , url: $url
            , contentType: "application/x-wwwf-orm-urlencoded; charset=UTF-8"
            , data: $formData
            , success:
                    function (data) {
                        $('#' + $pDivDestino).replaceWith(data);
                        var titulo = $('#' + $pDivDestino + ' #dialog').attr("title");
                        ExibirMsg('#' + $pDivDestino + ' #dialog', titulo);
                    }
            , dataType: 'html'
        });
        return false;
    }
}


function CarregueServicoEmPopup($pChamador, $pController, $pMetodo, $pNomeAcao, $pParametrosAdicionais, pConfMsg, pConfTitulo) {
}

function ExibirMsg($pDiv, $pTitulo) {
    var x_width = $(window).width() * 0.7;
    if ($pTitulo == '') {
        $pTitulo = 'Dataweb';
    }
    if (x_width > 500) {
        x_width = 500;
    }
    if ($pDiv == '') {
        $pDiv = '#dialog';
    }
    if ($($pDiv + ' div').html() != '') {
        $($pDiv).dialog({width: x_width, title: $pTitulo});
    }
}
function copyToClipboard(text) {
    window.prompt("Tecle Ctrl+C para copiar o texto para a Área de transferência", text);
    return false;
}

function ExibirRetornoAjax(pRetorno) {
    var tipoMsg = 'success';
    var timeout = 5000;
    if (pRetorno.codRet == 0) {
        tipoMsg = 'success';
    }
    else {
        if (pRetorno.codRet == 1) {
            tipoMsg = 'warning';
        }
        else {
            if (pRetorno.codRet == -1) {
                tipoMsg = 'error';
                timeout = false;
            }
        }
    }


    if (pRetorno.msg != '') {
        var xx = noty({text: pRetorno.msg, timeout: timeout, type: tipoMsg, layout: 'topCenter'});
    }

}

function EnviaLinha(pCampo, pController, pMetodo) {
    var tr = $(this)
}

function AcionaAtualizacao(pCampo, pParametros)
{
    var valor_orig = pCampo.attr('data-valor-orig');
    if (pCampo.val() == '__/__/____') {
        pCampo.val('');
    }
    if (pCampo.val() != valor_orig) {
        var parm = jQuery.parseJSON(pParametros);
        parm.nome_campo = pCampo.attr('id');
        parm.valor = pCampo.val();
        parm.valor_orig = valor_orig;
        var url = '/paginas/carregueAjax.php?controller=' + parm.controller + '&metodo=' + parm.metodo;
        $.ajax({
            type: 'POST'
            , url: url
            , data: parm
            , success:
                    function (data)
                    {
                        if (data.msg.tipoMsg == 'sucess') {
                            pCampo.animate({backgroundColor: "#8de580"}, 250);
                            pCampo.animate({backgroundColor: "transparent"}, 750);
                            pCampo.attr('data-valor-orig', pCampo.val());
                        }
                        else {
//						if(data.msg.tipoMsg == 'nop'){
                            pCampo.animate({backgroundColor: "red"}, 250);
                            pCampo.animate({backgroundColor: "transparent"}, 750);
                            $.noty.closeAll();
                            data.msg.textoMsg = 'Não foi possível atualizar devido ao seguinte erro: ' + data.msg.textoMsg;
                            var xx = noty({text: data.msg.textoMsg, timeout: false, type: data.msg.tipoMsg, layout: 'topCenter'});
                            ZeraMensagens();
//						}
                        }
                        if (data.novoConteudo) {
                            pCampo.val(data.novoConteudo);
                        }
                    }
            , dataType: 'json'
        });
    }
}

function AcionaProcesso(pLink)
{
    var pParametros = $(pLink).attr('data-parameters');
    var parm = jQuery.parseJSON(pParametros);
    var url = '/paginas/carregueAjax.php?controller=' + parm.controller + '&metodo=' + parm.metodo;
    var td = $(pLink).parent();
    $.noty.closeAll();
    $.ajax({
        type: 'POST'
        , url: url
        , data: parm
        , success:
                function (data)
                {
                    if (data.msg.tipoMsg == 'sucess') {
                        td.animate({backgroundColor: "#8de580"}, 250);
                        td.animate({backgroundColor: "transparent"}, 750);
                        if (data.novoConteudo) {
                            $(td).html(data.novoConteudo);
                        }
                    }
                    else {
                        if (data.msg.tipoMsg == 'nop') {
                            $.noty.closeAll();
                            data.msg.textoMsg = 'Não foi possível atualizar devido ao seguinte erro: ' + data.msg.textoMsg;
                            var xx = noty({text: data.msg.textoMsg, timeout: false, type: data.msg.tipoMsg, closeWith: 'click', layout: 'topCenter'});
                        }
                    }
                }
        , dataType: 'json'
    });
}

function AcionaProcessoAtualizandoColunas(pLink){
    if ($(pLink).hasClass('disabled')) return;
    $(pLink).addClass('disabled');
    var pParametros = $(pLink).attr('data-parameters');
    var parm = jQuery.parseJSON(pParametros);
    var url = '/paginas/carregueAjax.php?controller=' + parm.controller + '&metodo=' + parm.metodo;
    var td = $(pLink).parent();
    var tr = $(td).parent();
    $.noty.closeAll();
    $.ajax({
        type: 'POST'
        , url: url
        , data: parm
        , success:
                function (data){
                    $(pLink).removeClass('disabled');
                    if (data.msg.tipoMsg == 'sucess') {
                        td.animate({backgroundColor: "#8de580"}, 250);
                        td.animate({backgroundColor: "transparent"}, 750);
                        if (data.novoConteudo) {
                            $(td).html(data.novoConteudo);
                        }
                        $.each(data.campos, function (chave, atributos) {
                            $.each(atributos, function (attr, valor) {
                                if (attr == 'valor') {
                                    tr.find('#CMP_' + chave).val(valor);
                                } else {
                                    tr.find('#CMP_' + chave).attr(attr, valor);
                                }
                            });
                        });
                    }
                    else {
                        if (data.msg.tipoMsg == 'nop') {
                            $.noty.closeAll();
                            data.msg.textoMsg = 'Não foi possível atualizar devido ao seguinte erro: ' + data.msg.textoMsg;
                            var xx = noty({text: data.msg.textoMsg, timeout: false, type: data.msg.tipoMsg, closeWith: 'click', layout: 'topCenter'});
                        }
                    }
                }
        , dataType: 'json'
    });
}


function ZeraMensagens() {
    clearTimeout(timeout);
    timeout = setTimeout('$.noty.closeAll();', 5000);
}

function abreCertidao(numero){
    var urlCertidao = 'http://portal.mj.gov.br/EstrangeiroWEB/resultado_certidao.asp?auxAnalise=12&CampoTabela=tbDocumento_NumeroDoc&CampoPesquisado='+numero;
    var urlAtivacao = 'http://portal.mj.gov.br/EstrangeiroWEB/resultado_certidao_analise.asp?CampoTabela=tbDocumento_NumeroDoc&CampoPesquisado='+numero;
    var x= window.open(urlAtivacao, numero);
    setTimeout(function(){
        var y= window.open(urlCertidao, numero);
    }, 3000);

}
