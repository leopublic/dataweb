function CarregarCandidatos(pID_SOLICITA_VISTO, pNomeContainer, pNomeChaves, pAlterar){
	if($('#'+pNomeChaves).val()!=''){
		var url = '/LIB/CANDIDATO_LISTA_SOL.php?id_solicita_visto='+pID_SOLICITA_VISTO+'&lista='+$('#'+pNomeChaves).val()+'&alterar='+pAlterar;
		$.ajax({
			  url: url,
			  success: function(data) {
				  $('#'+pNomeContainer).html(data);
	           }
			});	
	}
	else{
		$('#'+pNomeContainer).html('(nenhum candidato adicionado)');
	}
}


function ajaxRemoverArquivo(pNU_SEQUENCIAL, pNU_CANDIDATO){
	if(confirm('Deseja mesmo remover esse arquivo?\n(Essa operação não é reversível)')){
		url = "/LIB/ARQUIVOS_DEL.php?NU_SEQUENCIAL="+pNU_SEQUENCIAL;
		$.ajax({
			  url: url,
			  success: function(data) {
				jAlert(data);
				CarregarListaDeArquivos(pNU_CANDIDATO);
	           }
			});		
	}
}


function ajaxIncluir(pCampo, pValor)
{
	var xChaves = $('#'+pCampo+'_chaves').val();
	var xVirgula = '';
	if (xChaves!='')
	{	xVirgula = ',';}
	
	var valor = pValor.split(',');
	var valorpesq = '';  
	var xChavespesq = ','+xChaves+',';
	var i=0;
	var incluido = false;
	for (i=0;i<valor.length;i++){
		valorpesq = ','+valor[i]+',';
		if (xChavespesq.indexOf(valorpesq)<0){
			xChaves = xChaves+xVirgula+valor[i];
			incluido = true;
			xVirgula = ',';
		}
	}
	if (incluido){
		$('#'+pCampo+'_chaves').val(xChaves);
		CarregarCandidatos();
		return 'Candidato(s) incluído(s) com sucesso!';
	}
	else{
		return 'O(s) candidato(s) indicados já estão na solicitação.';
	}
}

function ajaxCarregar(pCampo, pChamadora)
{
	//alert ('pCampo='+pCampo);
	var xChaves = $('#'+pCampo+'_chaves').val();
	//alert ('chaves='+xChaves);
	if (xChaves=='')
	{
		$('#'+pCampo+'_container').html('(nenhum candidato selecionado)');
	}
	else
	{
		try
		{
			$.ajax({
			  url: '/LIB/ajaxLista.php?tipo='+pCampo+'&chaves='+xChaves+'&chamadora='+pChamadora,
			  success: function(data) {
					$('#'+pCampo+'_container').html(data);
					}
				});
		}
		catch(err)
		{
			$('#'+pCampo+'_container').html('(Não foi possível recuperar os dados) '+err.description);	
		}
	}
}

function abreJanelaxxx(pLink, pNome)
{
	if (typeof(popupWin) != "object"){
		popupWin = window.open(pLink,pNome,'location=1,menubar=1,status=0,scrollbars=1,resizable=1,height=550,width=780');
	} else {
		if (!popupWin.closed){
			//popupWin.focus();
			//popupWin.location.href = pLink;
		} else {
			popupWin = window.open(pLink,pNome,'location=1,menubar=1,status=0,scrollbars=1,resizable=1,height=550,width=780');
		}
	}
	popupWin.focus();
}
