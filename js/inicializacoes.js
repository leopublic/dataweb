jQuery.uaMatch = function( ua ) {
	ua = ua.toLowerCase();
	var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		/(msie) ([\w.]+)/.exec( ua ) ||
		ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
		[];
	return {
		browser: match[ 1 ] || "",
		version: match[ 2 ] || "0"
	};
};
if ( !jQuery.browser ) {
	matched = jQuery.uaMatch( navigator.userAgent );
	browser = {};
	if ( matched.browser ) {
		browser[ matched.browser ] = true;
		browser.version = matched.version;
	}
	// Chrome is Webkit, but Webkit is also Safari.
	if ( browser.chrome ) {
		browser.webkit = true;
	} else if ( browser.webkit ) {
		browser.safari = true;
	}
	jQuery.browser = browser;
}



$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
$.datepicker.setDefaults({
	buttonImage: '/images/calendar3.png'
	,showOn: 'button'
	,buttonImageOnly: true    	
	,showOtherMonths: true
	,selectOtherMonths: true
});

$(document).ready(function() {
	$(".cpf").mask('999.999.999-99');
	$(".processo").mask('99999.999999/9999-99');
	$(".data").datepicker().mask('99/99/9999');
	$(".cmpDat").datepicker().mask('99/99/9999');
	$(".dataOnly").mask('99/99/9999');
	$('#ui-datepicker-div').hide();

	$(".accordion").accordion({
		heightSyle: "content"
		,collapsible: true
	});
    
	// por fabio
	$('body').prepend('<div id="load"><img src="../imagens/loading.gif" /> Carregando </div>');
	$('#load').hide();
	$('#load').ajaxStart(function(){
		$(this).show();
	}).ajaxSuccess(function(evt, request, settings){
		$(this).hide();
	}).ajaxError(function(){
		$(this).fadeOut();
	});

	$('.menubar').navMenu();
     
	$('.error').fadeOut(400,function(){
		$('.error').fadeIn(800);
	})
	$('.success').fadeOut(400,function(){
		$('.success').fadeIn(800);
	})
    
	aba();
      
	$('.cmpVal').maskMoney({
		symbol:'',
		decimal:',',
		thousands:'.'
	})
	$('.cmpDolar').maskMoney({
		symbol:'U$',
		decimal:',',
		thousands:'.'
	});

	$('.cmpReal').maskMoney({
		symbol:'R$',
		decimal:',',
		thousands:'.'
	})
	$('.cmpEuro').maskMoney({
		symbol:'',
		decimal:',',
		thousands:'.'
	})
	// $('#precision').maskMoney({decimal:',',thousands:' ',precision:3})
   
//   
//	$('.masterEdit a.masterEditLink').on('click',function(){
//		var span  = $(this).parent();
//		var pk    = span.find('input[type="hidden"]');
//		var name  = pk.attr('name');
//		//var descr = $('#masterEditDescr_'+name);
//		var descr = span.find('.masterEditDescr');
//		var url   = $(this).attr('href');
//		var title = $(this).attr('title');
//        
//		$win = gridbox(title, '700px');
//		$.post(url, function(d){
//			$win.find('.content').html(d);
//			var table = $('.masterEditTable');
//			if(table[0]){
//				table.find('tbody td').bind('click',function(){
//					pk.val($(this).parent().attr('id'));
//					descr.html($(this).parent().attr('title'));
//					$('.GridBox-ligthbox').remove();
//				});
//			}
//		});
//		return false;
//	});
	//
	// Ativa o cabecalho para os grids se houver
//	try{
//		if ($('.grid').length > 0){
//			$('.grid').floatHeader();					// Ativa o cabecalho fixo
//			$(".grid tr").bind('mouseover', function() {		// Ativa o highlight nas linhas
//					$(this).addClass('impar');
//				});
//			$(".grid tr").bind('mouseout', function() {		// Ativa o highlight nas linhas
//					$(this).removeClass('impar');
//				});
//		}
//
//	}
//	catch($e){
//
//	}
        if ($('table.floaThead').length > 0) {
            $table = $('table.floaThead');
            $table.floatThead({
                scrollingTop: 73
                , zIndex: 500
            });
        }
        if ($('table.grid').not('.semfloatheader').length > 0) {
            $table = $('table.grid');
            $table.floatThead({
                scrollingTop: 73
                , zIndex: 500
            });
        }


	// Ativa a atulização automatica das embarcacoes projeto
	if ($('#CMP_NU_EMBARCACAO_PROJETO').length > 0 ){
		if ($('select[id="CMP_NU_EMPRESA"]').length > 0){
			$('select[id="CMP_NU_EMPRESA"]').change(function() {
				atualizaEmbarcacaoProjeto(this);
			});
			atualizaEmbarcacaoProjeto($('select[id="CMP_NU_EMPRESA"]'));
		}
	}

	// if ($('#CMP_FILTRO_NU_EMBARCACAO_PROJETO').length > 0 ){
	// 	if ($('select[id="CMP_FILTRO_NU_EMPRESA"]').length > 0){
	// 		$('select[id="CMP_FILTRO_NU_EMPRESA"]').change(function() {
	// 			atualizaEmbarcacaoProjetoFiltro(this);
	// 		});
	// 		atualizaEmbarcacaoProjetoFiltro($('select[id="CMP_FILTRO_NU_EMPRESA"]'));
	// 	}
	// }
	
    $('table').find('> tbody > tr:nth-child(even)').addClass('even');
	
});

$(document).ready(function() {
	$("table.grid input.editavel").focus(
		function(){
			$(this).addClass("focus");
		}
	);
	
	$("table.grid input.editavel").keydown(function(e) {
	    if(e.which == 13) {
	        $(this).blur();
	    }
	});

	$("table.grid input.editavel").focusout(
		function(){
			$(this).removeClass("focus");
			AcionaAtualizacao($(this), $(this).attr('data-parametros'));
		}
	);
});

$(document).ready(function() {
	$("table.grid select.editavel").focus(
		function(){
			$(this).addClass("focus");
		}
	);
	
	$("table.grid select.editavel").keydown(function(e) {
	    if(e.which == 13) {
	        $(this).blur();
	    }
	});

	$("table.grid select.editavel").focusout(
		function(){
			$(this).removeClass("focus");
			AcionaAtualizacao($(this), $(this).attr('data-parametros'));
		}
	);
});
	


$(document).ready(function() {
	$("table.grid textarea.editavel").focus(
		function(){
			$(this).addClass("focus");
		}
	);
	
	$("table.grid textarea.editavel").focusout(
		function(){
			$(this).removeClass("focus");
			AcionaAtualizacao($(this), $(this).attr('data-parametros'));
		}
	);
});
