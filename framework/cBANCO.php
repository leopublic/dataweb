<?php
class cBANCO{
	public static function SemCaracteresEspeciaisMS($string) {
		return strtr(
			$string,
			array(
			"\x80" => "e",  "\x81" => " ",    "\x82" => "'", "\x83" => 'f',
			"\x84" => '"',  "\x85" => "...",  "\x86" => "+", "\x87" => "#",
			"\x88" => "^",  "\x89" => "0/00", "\x8A" => "S", "\x8B" => "<",
			"\x8C" => "OE", "\x8D" => " ",    "\x8E" => "Z", "\x8F" => " ",
			"\x90" => " ",  "\x91" => "`",    "\x92" => "'", "\x93" => '"',
			"\x94" => '"',  "\x95" => "*",    "\x96" => "-zz", "\x97" => "--",
			"\x98" => "~",  "\x99" => "(TM)", "\x9A" => "s", "\x9B" => ">",
			"\x9C" => "oe", "\x9D" => " ",    "\x9E" => "z", "\x9F" => "Y", "\xBA" => "&ordm;", "\xAA" => "&ordm;" ));
	}

	public static function StringOk($pString){
		if ($pString==''){
			return 'null';	}
		else{
			// $pString = self::SemCaracteresEspeciaisMS($pString);
			$pString = cENCODING::fixutf8($pString);
			return "'".str_replace("'", "''", stripslashes(trim($pString)))."'";
		}

	}

	public static function ChaveOk($pChave){
		if ($pChave==''||$pChave=='null'){
			return 'null';	}
		else{
			if (is_numeric($pChave)){
				return $pChave;
			}
			else{
				return "'".$pChave."'";
			}
		}
	}

	public static function DataOk($pData) {
		if(strstr($pData, ":")){
			//Tem hora
			$pedaco = explode(" ", trim($pData));
			$pData = $pedaco[0];
		}
		if ($pData==""){
			return "null";
		}
		else{
			// Identifica o separador
			if (strstr($pData, "/")){
				$sep = "/";
			}
			else{
				if(strstr($pData,"-")){
					$sep = "-";
				}
				else{
					throw new Exception("Separador não identificado para data informada:".$pData);
				}
			}
			$aux = preg_split("[$sep]",$pData);
		    if(count($aux)>1) {
				if (strlen($aux[0])==4){
			    	$ret = $aux[0]."-".$aux[1]."-".$aux[2];
			    }
				elseif (strlen($aux[2])==4){
			    	$ret = $aux[2]."-".$aux[1]."-".$aux[0];
				}
				else{
					$aux[2] = '20'.$aux[2];
			    	$ret = $aux[2]."-".$aux[1]."-".$aux[0];
				}
		    }
		    else{
				throw new Exception("Formato de data inválido:".$pData);
			}
			if(isset($pedaco[1])){
				$ret .= ' '.$pedaco[1];
			}
			if($ret != 'null'){
				$ret = "'".$ret."'";
			}
	  		return $ret;
		}
	}

	public static function DataFmt($pData, $pcomSeculo = true, $pcomHora = false) {
		if ($pData==""){
			return "";
		}
		else{
			$xx =  preg_split("[ ]",$pData);
			$xData = $xx[0];
			// Identifica o separador
			if (strstr($xData, "/")){
				$sep = "/";
			}
			else{
				if(strstr($xData,"-")){
					$sep = "-";
				}
				else{
					$sep = "";
				}
			}
			if ($sep != '' ){
				$aux = preg_split("[$sep]",$xData);
				if(count($aux)>1) {
					if (strlen($aux[0])==4){
						if($pcomSeculo){
							$ano = $aux[0];
						}
						else{
							$ano = substr($aux[0], 2);
						}
						$ret = $aux[2]."/".$aux[1]."/".$ano;
					}
					elseif (strlen($aux[2])==4){
						if($pcomSeculo){
							$ano = $aux[2];
						}
						else{
							$ano = substr($aux[2], 2);
						}
						$ret = $aux[0]."/".$aux[1]."/".$ano;
					}
					else{
						if($pcomSeculo){
							$ano = '20'.$aux[2];
						}
						else{
							$ano = $aux[2];
						}
						$ret = $aux[0]."/".$aux[1]."/".$ano;
					}
				}
				else{
					throw new Exception("Formato de data inválido:".$xData);
				}
			}
			else{
				$ret = $pData;
			}
			if($pcomHora){
				$ret .= ' '.$xx[1];
			}
	  		return $ret;
		}
	}

	public static function InteiroOk($pInteiro){
		$pInteiro = $pInteiro.'';
		if ($pInteiro == ''
			|| $pInteiro == 'null'){
			return 'null';	}
		else{
			if (is_numeric($pInteiro)){
				return $pInteiro;
			}
			else{
				throw new Exception("Inteiro informado não numérico:".$pInteiro);
			}
		}
	}

	public static function ValorOk($pValor){
		$pValor = $pValor.'';
		if ($pValor == ''
			|| $pValor == 'null'){
			return 'null';	}
		else{
			$pValor = str_replace(' ', '', $pValor);
			$pValor = str_replace('U$', '', $pValor);
			$pValor = str_replace('R$', '', $pValor);
			$pValor = str_replace('.', '', $pValor);
			$pValor = str_replace(',', '.', $pValor);
			if (is_numeric($pValor)){
				return $pValor;
			}
			else{
				throw new Exception("Valor informado não numérico:".$pValor);
			}
		}
	}

	public static function valorParaBanco($pCampo){
		switch ($pCampo->mTipo){
			case cCAMPO::cpTEXTO :
			case cCAMPO::cpCODIGO:
			case cCAMPO::cpMEMO :
			case cCAMPO::cpHIDDEN :
				return self::StringOk($pCampo->mValor);
				break;
			case cCAMPO::cpDATA :
				return self::DataOk($pCampo->mValor);
				break;
			case cCAMPO::cpCHAVE_ESTR :
			case cCAMPO::cpCHAVE :
				return self::ChaveOk($pCampo->mValor);
				break;
			case cCAMPO::cpSIMNAO :
			case cCAMPO::cpSIMNAO_LITERAL :
				return self::InteiroOk($pCampo->mValor);
				break;
			case cCAMPO::cpVALOR :
			case cCAMPO::cpVALOR_DIN_DOLAR :
			case cCAMPO::cpVALOR_DIN_REAL :
				return self::ValorOk($pCampo->mValor);
				break;
			default :
				throw new Exception("Formatação do valor para o tipo informado ".$pCampo->mNome." tipo=".$pCampo->mTipo." ainda não implementado");
		}
	}

	public static function BancoParaTela($pCampo){
		switch ($pCampo->mTipo){
			case cCAMPO::cpSIMNAO_LITERAL:
				if ($pCampo->mValor == ''){
					return '?';
				}
				else{
					if ($pCampo->mValor == 0){
						return 'Não';
					}
					else{
						return 'Sim';
					}
				}
				break;
			case cCAMPO::cpDATA:
				return $pCampo->mValor;
				break;
			case cCAMPO::cpVALOR:
			case cCAMPO::cpVALOR_DIN_DOLAR:
			case cCAMPO::cpVALOR_DIN_REAL:
				return number_format($pCampo->mValor, 2, ",", ".");
				break;
			case cCAMPO::cpSIMNAO:

				break;
			default:
				return $pCampo->mValor;
				break;
		}
	}
	/**
	 * Monta uma clausula where para os filtros informados. Assume que já existe um "where 1=1 " no sql original.
	 * @param type $pFiltros
	 */
	public static function WhereFiltros($pFiltros){
		$ret = '';
		if (is_array($pFiltros) && count($pFiltros) > 0){
			foreach($pFiltros as $filtro){
				if ($filtro->mValor != ''){
					if ($filtro->mWhere != '' ){
						$ret .= strval($filtro->mOperadorFiltro.$filtro->mWhere);
					}
					else{
						$ret .= strval($filtro->mOperadorFiltro.$filtro->where());
					}
				}
			}
		}
		return $ret;
	}

		/**
	 * Gera uma data formatada em string para ser adicionado a um sql
	 * @param string Valor original
	 * @return string Valor formatado com plics e traços no formato AAAA-MM-DD
	 * TODO: Incluir validação contra sql injection
	 */
	public static function SqlOk($pSql){
		return str_replace("'", "''", $pSql);
	}
	public static function SimNaoOk($pValor) {
		if ($pValor==""){
			return "null";
		}
		else{
			if ($pValor){
				return "1";
			}
			else{
				return "0";
			}
		}
	}

	public static function CarreguePropriedades($pRs, &$pObjeto, $pdif = "m"){
	    if(!is_array($pRs)){
	       return null;
	    }
		foreach($pRs as $coluna => $valor){
			$propriedade = $pdif.$coluna;
			$prop = $coluna;
			if (property_exists($pObjeto, $propriedade)){
				if (strpos($propriedade, '_dt_')){
					$pObjeto->$propriedade = self::DataFmt($valor);
				}
				else{
					$pObjeto->$propriedade = $valor;
				}
			}
			if (property_exists($pObjeto, $prop)){
				if (strpos($prop, '_dt_')){
					$pObjeto->$prop= self::DataFmt($valor);
				}
				else{
					$pObjeto->$prop= $valor;
				}
			}
		}
	}

	public static function DataValida($pData){
		if ($pData != ''){
			$tokens = explode("/", $pData);
			return checkdate($tokens[1], $tokens[0], $tokens[2]);
		}
		else{
			return true;
		}
	}


}
