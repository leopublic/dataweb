<?php
class cCOMBO_SIM_NAO extends cCOMBO_GERAL{
	public function __construct($pCampoBD, $pLabel, $pLabel_en_us = '') {
		parent::__construct($pCampoBD, $pLabel, $pLabel_en_us);
        $this->mPermiteDefault = true;
		$this->mValorDefault = '(todos)';
	}

	public function CarregarValores(){
        $this->InicializarValores();
		$this->AdicionarValor("1", "Sim");
		$this->AdicionarValor("0", "Não");
	}

	public function where(){
		switch($this->mValor){
			case "":
				return "";
				break;
			case "0":
				return "(".$this->getQualificadorFiltro.$this->mCampoBD . " = " . $this->mValor." or ".$this->getQualificadorFiltro.$this->mCampoBD . " is null) ";
				break;
			case "1":
				return $this->getQualificadorFiltro.$this->mCampoBD . " = " . $this->mValor;
				break;
		}
	}
}
