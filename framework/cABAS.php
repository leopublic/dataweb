<?php

/**
 * Representa um conjunto de abas
 */
class cABAS {

    public $abas;
    public $abaAtiva;

    public static function novaAbaOS($id_solicita_visto) {
        $abas = new cABAS;

        $aba = new cABA;
        $aba->nome = 'DadosOs';
        $aba->titulo = 'Dados OS';
        $aba->link = '/operador/os_DadosOS.php?' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->visivel = true;
        $abas->abas[$aba->nome] = $aba;

        $aba = new cABA;
        $aba->nome = 'DadosPessoais';
        $aba->titulo = 'Dados pessoais';
        $aba->link = '/operador/os_DadosPessoais.php?' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->visivel = true;
        $abas->abas[$aba->nome] = $aba;

        $aba = new cABA;
        $aba->nome = 'DadosBasicos';
        $aba->titulo = 'Cadastro básico';
        $aba->link = '/operador/DadosBasicosCandidato.php?' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->visivel = false;
        $abas->abas[$aba->nome] = $aba;

        $aba = new cABA;
        $aba->nome = 'Cadastro';
        $aba->titulo = 'Cadastro';
        $aba->link = '/operador/os_Cadastro.php?' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->visivel = true;
        $abas->abas[$aba->nome] = $aba;

        $aba = new cABA;
        $aba->nome = 'Formularios';
        $aba->titulo = 'Formulários';
        $aba->link = '/operador/os_Formularios.php?' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->visivel = true;
        $abas->abas[$aba->nome] = $aba;

        $aba = new cABA;
        $aba->nome = 'Processo';
        $aba->titulo = 'Processo';
        $aba->link = '/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=EditeProcesso' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->ativa = false;
        $aba->visivel = true;
        $abas->abas[$aba->nome] = $aba;

        $aba = new cABA;
        $aba->nome = 'Liberacao';
        $aba->titulo = 'Qualidade & Liberação';
        $aba->link = '/operador/os_Liberacao.php?' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->ativa = false;
        $aba->visivel = false;
        $abas->abas[$aba->nome] = $aba;

        $aba = new cABA;
        $aba->nome = 'TVS';
        $aba->titulo = 'Formulário TVS';
        $aba->link = '/paginas/carregueComMenu.php?controller=cCTRL_FORM_TVS&metodo=Edite' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->ativa = false;
        $aba->visivel = false;
        $abas->abas[$aba->nome] = $aba;

        $aba = new cABA;
        $aba->nome = 'Despesas';
        $aba->titulo = 'Despesas adicionais';
        $aba->link = '/paginas/carregueComMenu.php?controller=cCTRL_OS_DESP_LIST&metodo=Liste' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->ativa = false;
        $aba->visivel = true;
        $abas->abas[$aba->nome] = $aba;

        $aba = new cABA;
        $aba->nome = 'Financeiro';
        $aba->titulo = 'Financeiro';
        $aba->link = '/paginas/carregueComMenu.php?controller=cCTRL_OS_ABA_COBR_EDIT&metodo=Edite' . self::parametroID_SOLICITA_VISTO($id_solicita_visto);
        $aba->ativa = false;
        $aba->visivel = false;
        $abas->abas[$aba->nome] = $aba;

        return $abas;
    }

    public static function parametro($nome, $valor) {
        return '&' . $nome . '=' . $valor;
    }

    public static function parametroID_SOLICITA_VISTO($valor) {
        return self::parametro('ID_SOLICITA_VISTO', $valor) . self::parametro('id_solicita_visto', $valor);
    }

    public function esconda($nomeAba) {
        $this->abas[$nomeAba]->visivel = false;
    }

    public function exiba($nomeAba) {
        $this->abas[$nomeAba]->visivel = true;
    }

    public function escondaTodas() {
        foreach ($this->abas as &$aba) {
            $aba->visivel = false;
        }
    }

    public function exibaTodas() {
        foreach ($this->abas as &$aba) {
            $aba->visivel = true;
        }
    }

    public function configuraPelaOs($os) {
        if ($os->mID_SOLICITA_VISTO == 0) {
            $this->escondaTodas();
            $this->exiba('DadosOs');
        } else {
            if ($os->mNU_FORMULARIOS > 0 || $os->mNU_SERVICO == '') {
                
            } else {
                $this->abas['Formularios']->visivel = false;
            }
            if ($os->mCandidatos == "") {
                $this->abas['Cadastro']->visivel = false;
            }
        }

        if ($os->mID_SOLICITA_VISTO > 0 && $os->mNU_SERVICO > 0) {
            $serv = new cSERVICO();
            $serv->RecuperePeloId($os->mNU_SERVICO);
            if ($serv->mserv_fl_form_tvs) {
                $this->abas['TVS']->visivel = true;
            }
        } else {
            $this->abas['TVS']->visivel = false;
        }
    }

}
