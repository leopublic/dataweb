<?php
/**
 * Classe padronizada de retorno para chamadas ajax
 *
 * @author leonardo
 */
class cRET_AJAX_PADRAO {
	protected $codRet;
	protected $msg;
	protected $tituloMsg;
	protected $extras = array();
	
	const codRet_SUCESSO = 0;
	const codRet_ALERTA = 1;
	const codRet_ERRO = -1;
	
	public function AdicionaExtra($pChave, $pValor){
		$this->extras[$pChave] = $pValor;
		
	}
	
	public function get_tituloMsg(){
		return self::SeguroEmJS($this->tituloMsg);
	}
	
	public function get_msg(){
		return self::SeguroEmJS($this->msg);		
	}
	
	public static function SeguroEmJS($pValor){
		return str_replace("\n", "<br>", htmlentities($pValor));
	}
	
	public function EmJSON(){
		return json_encode(array(
				"codRet" => $this->codRet
				, "tituloMsg"=> $this->get_tituloMsg()
				, "msg" => $this->get_msg()
				, "extras" => json_encode($this->extras)
			));
	}
	
	public function FoiSucesso($pMsg){
		$this->codRet = self::codRet_SUCESSO;
		$this->tituloMsg = "Dataweb";
		$this->msg = $pMsg;
	}
	
	public function FoiAlerta($pMsg){
		$this->codRet = self::codRet_ALERTA;
		$this->tituloMsg = "Atenção";
		$this->msg = $pMsg;
	}
	
	public function FoiErro($pMsg){
		$this->codRet = self::codRet_ERRO;
		$this->tituloMsg = "Erro!";
		$this->msg = $pMsg;
	}
}
