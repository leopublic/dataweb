<?php

class cEDICAO {

    public $mCampo = array();
    public $mCampoNome = array();
    public $mAcoes = array();
    public $mView;
    public $mno_edicao;
    public $mid_edicao;
    public $mCampoCtrlEnvio;
    public $mTabela;
    public $mAcao;
    public $mClasseProcessadora;
    private $xCampoChave;

    public function __construct($pno_edicao = '') {
        $this->mno_edicao = $pno_edicao;
        $this->AdicioneCampo(new cCAMPO(cINTERFACE::CMP_EDICAO, cINTERFACE::CMP_EDICAO, cCAMPO::cpHIDDEN));
        $this->mCampoNome[cINTERFACE::CMP_EDICAO]->mValor = $this->mno_edicao;
        $this->AdicioneCampo(new cCAMPO(cINTERFACE::CMP_ACAO, cINTERFACE::CMP_ACAO, cCAMPO::cpHIDDEN));
        $this->AdicioneCampo(new cCAMPO_HIDDEN("__altura"));
        $this->AdicioneCampo(new cCAMPO_HIDDEN("__largura"));
        $this->AdicioneCampo(new cCAMPO_HIDDEN("__titulo"));
    }

    /*
     * Identifica a tela (edição) que foi enviada no último post
     * @param $pPOST : O $_POST do php.
     */

    public function PreProcessarPost($pPOST) {
        $this->mno_edicao = $pPOST[cINTERFACE::CMP_EDICAO];
        $this->mAcao = $pPOST[cINTERFACE::CMP_ACAO];
    }

    public function CarregarConfiguracao($pno_edicao = '') {
        if ($pno_edicao != '') {
            $this->mno_edicao = $pno_edicao;
        }
        $sql = "select * from Edicao where no_edicao='" . $this->mno_edicao . "'";
        //die("vai procurar".$sql);
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        if ($rs = mysql_fetch_array($res)) {
            //print " encontrou!!";
            $this->mid_edicao = $rs['id_edicao'];
            $this->mView = $rs['no_view'];
            $this->mTabela = $rs['no_tabela'];
            $this->mClasseProcessadora = $rs['no_classe_processadora'];
        } else {
            throw new Exception('Tela "' . $this->mno_edicao . '" não encontrada.');
        }
        $this->mCampoCtrlEnvio = new cCAMPO($this->mView . '_ctrlEnvio', '', cCAMPO::cpHIDDEN);
        $this->mCampoCtrlEnvio->mValor = '1';
        $sql = "select * from Campo where id_edicao = " . $this->mid_edicao . " order by nu_ordem_painel, nu_ordem";
        //die("vai procurar : ".$sql);
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        while ($rs = mysql_fetch_array($res)) {
            //error_log("\n".'Carregando campo '.$rs["no_label"].'-->'.$rs["no_campo_bd"].' chave='.$rs['bo_chave'].'obr='.$rs['bo_obrigatorio']);
            //print '<br/>Painel '.$rs["no_label"].'-->'.$rs["no_painel"];
            //print '<br/>Readonly '.$rs["no_label"].'-->'.$rs["bo_readonly"];
            if ($rs['id_tipo_campo'] == cCAMPO::cpCOMBO_GERAL) {
                $this->AdicioneCampo(cCOMBO_FABRICA::Novo($rs['id_tipo_combo'], $rs['no_campo_bd'], $rs['no_label']));
            } else {
                $this->AdicioneCampo(new cCAMPO($rs['no_campo_bd'], $rs['no_label'], $rs['id_tipo_campo'], '', $rs['bo_chave'], $rs['bo_obrigatorio'], $rs['no_classe'], $rs['id_tipo_combo'], $rs['no_painel'], $rs['bo_readonly'], $rs['bo_exibir'], $rs['fl_link_mte'], $rs['fl_link_mj'], $rs['fl_largura_dupla']));
            }
        }
    }

    public function IdentificadorCampos() {
        $virg = '';
        foreach ($this->mCampo as $xCampo) {
            if ($xCampo->mChave) {
                $id.= $virg . $xCampo->mValor;
            }
        }
        return $id;
    }

    public function AdicioneCampo($pCampo) {
        $this->mCampo[] = $pCampo;
        $this->mCampoNome[$pCampo->mCampoBD] = &$this->mCampo[count($this->mCampo) - 1];
    }

    public function RecupereRegistro() {
        $where = ' where 1=1 ';
        foreach ($this->mCampo as $xCampo) {
            if ($xCampo->mChave) {
                $where.= " and " . $xCampo->mCampoBD . " = " . cBANCO::valorParaBanco($xCampo);
            }
        }

        $sql = "select * from " . $this->mView . $where;
        $res = cAMBIENTE::$db_pdo->query($sql);
        // $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        if ($rs = $res->fetch()) {
                //var_dump($rs);
                foreach ($this->mCampo as &$xCampo) {
                    //var_dump('testando campo '.$xCampo->mCampoBD);
                    if (isset($rs[$xCampo->mCampoBD])) {
                        $xCampo->mValor = $rs[$xCampo->mCampoBD];
                        //var_dump($xCampo->mCampoBD.'='.$xCampo->mValor);
                    } else {
                        if (substr($xCampo->mCampoBD, 0, 2) != '__') {
                            $xCampo->mValor = '';
                        }
                    }
                }
        }
    }

    public function AtualizarBanco() {
        $sql = "update " . $this->mTabela . " set ";
        $xVirg = '';
        foreach ($this->mCampo as $xCampo) {
            if (!$xCampo->mChave && !$xCampo->mReadonly && $xCampo->mVisivel && substr($xCampo->mCampoBD, 0, 2) != '__') {
                $sql.= $xVirg . $xCampo->mCampoBD . " = " . cBANCO::valorParaBanco($xCampo);
                $xVirg = ', ';
            }
        }
        $sql .= ' where 1=1 ';
        foreach ($this->mCampo as $xCampo) {
            if ($xCampo->mChave) {
                $sql.= " and " . $xCampo->mCampoBD . " = " . cBANCO::valorParaBanco($xCampo);
            }
        }
        //print "<br/>sql=".$sql;
        return executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function BloqueieCamposVisiveis() {
        foreach ($this->mCampo as &$xCampo) {
            if ($xCampo->mVisivel == 1 && !$xCampo->mReadonly) {
                $xCampo->mReadonly = 1;
            }
        }
    }

    public function EscondaCampo($pNome) {
        if (isset($this->mCampoNome[$pNome])) {
            $this->mCampoNome[$pNome]->mVisivel = 0;
        }
    }

    public function MostreCampo($pNome) {
        if (isset($this->mCampoNome[$pNome])) {
            $this->mCampoNome[$pNome]->mVisivel = 1;
        }
    }

    public function ProtejaCampo($pNome) {
        if (isset($this->mCampoNome[$pNome])) {
            $this->mCampoNome[$pNome]->mReadonly = 1;
        }
    }

    public function DesprotejaCampo($pNome) {
        if (isset($this->mCampoNome[$pNome])) {
            $this->mCampoNome[$pNome]->mReadonly = 0;
        }
    }

    public function ProtejaTodosCampos() {
        foreach ($this->mCampo as &$campo) {
            if ($campo->mTipo != cCAMPO::cpCHAVE && $campo->mTipo != cCAMPO::cpCHAVE) {
                $campo->mReadonly = 1;
            }
        }
    }

    public function ResseteChavePara($pNomeCampo) {
        foreach ($this->mCampo as &$campo) {
            $campo->mChave = 0;
        }
        $this->mCampoNome[$pNomeCampo]->mChave = 1;
    }

    public static function SQLEdicoesCadastradas() {
        return "select * from Edicao order by no_edicao";
    }

}
