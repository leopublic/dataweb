<?php
/**
 * Fabrica de objetos tipo cCOMBO_GERAL.
 * Cria o objeto dependendo do tipo especificado.
 */
class cFABRICA_CAMPO{
	/**
	 * Cria um campo, de qualquer tipo
	 * @param int $pTipo			Tipo do campo
	 * @param string $pCampoBD		Nome do campo
	 * @param string $pLabel		Titulo do campo
	 * @param int $pTipoCombo		Tipo do combo, se for um combo
	 * @return cCAMPO
	 */
	public static function Novo($pTipo,  $pCampoBd, $pLabel = null, $pTipoCombo = null , $pLabel_en_us = '' ){
		if ($pLabel_en_us == '' ){
			$pLabel_en_us = $pLabel;
		}
		switch ($pTipo){
			case cCAMPO::cpCHAVE_ESTR:
				return self::NovoCombo($pCampoBd, $pLabel, $pTipoCombo);
				break;
			case cCAMPO::cpDATA:
				return new cCAMPO_DATA($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCAMPO::cpDATA_ONLY:
				return new cCAMPO_DATA_ONLY($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCAMPO::cpDATAHORA:
				return new cCAMPO_DATAHORA($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCAMPO::cpTEXTO:
				return new cCAMPO_TEXTO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCAMPO::cpSENHA:
				return new cCAMPO_SENHA($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCAMPO::cpSIMNAO:
				return new cCAMPO_SIMNAO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCAMPO::cpMEMO:
				return new cCAMPO_MEMO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCAMPO::cpVALOR_INTEIRO:
				return new cCAMPO_VALOR_INTEIRO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCAMPO::cpVALOR_DIN_REAL:
				return new cCAMPO_VALOR_REAL($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			default:
				$cmp = new cCAMPO($pCampoBd, $pLabel, $pTipo);
				return $cmp;
				break;
		}
	}

	public static function NovoCombo($pCampoBd, $pLabel, $pTipoCombo, $pLabel_en_us= '',$pautocomplete = false ){
		if ($pLabel_en_us == ''){
			$pLabel_en_us = $pLabel;
		}
		switch ($pTipoCombo){
			case cCOMBO::cmbTABELAS:
				return new cCOMBO_TABELAS($pCampoBd, $pLabel);
				break;
			case cCOMBO::cmbSEXO:
				return new cCOMBO_SEXO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCOMBO::cmbTIPO_CANDIDATO:
				return new cCOMBO_TIPO_CANDIDATO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCOMBO::cmbSITUACAO_CADASTRAL_CANDIDATO:
				return new cCOMBO_SITUACAO_CADASTRAL_CANDIDATO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCOMBO::cmbSITUACAO_REVISAO_CANDIDATO:
				return new cCOMBO_SITUACAO_REVISAO_CANDIDATO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCOMBO::cmbSIM_NAO:
				return new cCOMBO_SIM_NAO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCOMBO::cmbSITUACAO:
				return new cCOMBO_SITUACAO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCOMBO::cmbORGAO_PROTOCOLO:
				return new cCOMBO_ORGAO_PROTOCOLO($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCOMBO::cmbEMBARCACAO_PROJETO_COM_REVISAO:
				return new cCOMBO_EMBARCACAO_PROJETO_COM_REVISAO($pCampoBd, $pLabel, $pTipoCombo, $pLabel_en_us);
				break;
			case cCOMBO::cmbULTIMOS_PROTOCOLOS_BSB:
				return new cCOMBO_ULTIMOS_PROTOCOLOS($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			case cCOMBO::cmbMES:
				return new cCOMBO_MES($pCampoBd, $pLabel, $pLabel_en_us);
				break;
			default:
				if($pautocomplete){
					return new cCOMBO_TABELA_AUTOCOMPLETE($pCampoBd, $pLabel, $pTipoCombo, $pLabel_en_us);
				}
				else{
					return new cCOMBO_TABELA($pCampoBd, $pLabel, $pTipoCombo, $pLabel_en_us);					
				}
		}
	}
}
