<?php

/**
 *
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cAMBIENTE {

    public static $m_servidor;
    public static $m_loginDb;
    public static $m_senhaDb;
    public static $m_database;
    public static $m_pastaServidor;
    public static $m_dominio;
    public static $m_raiz;

    /**
     * Conexao banco padrão
     * @var cMYSQL
     */
    public static $db;

    /**
     * Conexão PDO
     * @var PDO
     */
    public static $db_pdo;

    /**
     * Inicializa os parametros do ambiente com os valores da configuração indicada
     * @param type $pnomeConfiguracao
     */
    public static function InicializeAmbiente($pnomeConfiguracao) {
        self::$m_servidor = cCONFIGURACAO::$configuracoes[$pnomeConfiguracao]['servidor'];
        self::$m_loginDb = cCONFIGURACAO::$configuracoes[$pnomeConfiguracao]['loginDb'];
        self::$m_senhaDb = cCONFIGURACAO::$configuracoes[$pnomeConfiguracao]['senhaDb'];
        self::$m_database = cCONFIGURACAO::$configuracoes[$pnomeConfiguracao]['database'];
        self::$m_dominio = cCONFIGURACAO::$configuracoes[$pnomeConfiguracao]['dominio'];
        if (key_exists('porta', cCONFIGURACAO::$configuracoes[$pnomeConfiguracao])) {
            self::$m_dominio .= ':' . cCONFIGURACAO::$configuracoes[$pnomeConfiguracao]['porta'];
        }
        self::$m_raiz = cCONFIGURACAO::$configuracoes[$pnomeConfiguracao]['raiz'];

        //self::$db = new cMYSQL();
        //self::$db->abre();
        $dsn = 'mysql:host=' . self::$m_servidor . ';dbname=' . self::$m_database ;
        self::$db_pdo = new PDO($dsn, self::$m_loginDb, self::$m_senhaDb,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_PERSISTENT=> true));
//        $sql = "SET global general_log = 1; SET global log_output = 'table';";
//        self::$db_pdo->exec($sql);

        cHTTP::$flAssociarEmpresaEmbProjAutomaticamente = true;
    }

    public static function getDirDocDr() {
        return self::$m_raiz . DIRECTORY_SEPARATOR . 'arquivos_dr';
    }

    public static function getUrlDocDr() {
        return self::$m_dominio . '/arquivos_dr';
    }

    public static function getDirDocTmp() {
        return self::$m_raiz . DIRECTORY_SEPARATOR . 'arquivos_tmp';
    }

    public static function getUrlDocTmp() {
        return self::$m_dominio . '/arquivos_tmp';
    }

    public static function getDirSol() {
        return self::$m_raiz . DIRECTORY_SEPARATOR . 'arquivos' . DIRECTORY_SEPARATOR . 'solicitacoes';
    }

    public static function getDirDoc() {
        return self::$m_raiz . DIRECTORY_SEPARATOR . 'arquivos' . DIRECTORY_SEPARATOR . 'documentos';
    }

    public static function getUrlSol() {
        return self::$m_dominio . '/' . 'arquivos' . '/' . 'solicitacoes';
    }

    public static function getUrlDoc() {
        return self::$m_dominio . '/' . 'arquivos' . '/' . 'documentos';
    }

    public static function getDirForm() {
        return self::$m_raiz . DIRECTORY_SEPARATOR . 'arquivos' . DIRECTORY_SEPARATOR . 'formularios';
    }

    public static function getUrlForm() {
        return self::$m_dominio . '/' . 'arquivos' . '/' . 'formularios';
    }

    public static function ExecuteQuery($pSql, $pContexto = 'n/d') {
        if (DEBUG) {
            error_log(str_replace("\t", " ", str_replace("     ", " ", str_replace("    ", " ", str_replace("    ", " ", str_replace("  ", " ", $pContexto . " sql=" . $pSql))))));
        }
        try {
            return self::$db_pdo->exec($pSql);
        } catch (Exception $e) {
            throw new cERRO_SQL($e, $pContexto, $pSql);
        }
    }

    public static function ConectaQuery($pSql, $pContexto) {
        if (DEBUG) {
            error_log(str_replace("\t", " ", str_replace("     ", " ", str_replace("    ", " ", str_replace("    ", " ", str_replace("  ", " ", $pContexto . " sql=" . $pSql))))));
        }
        try {
            $res = self::$db_pdo->query($pSql);
            return $res;
        } catch (Exception $e) {
            throw new cERRO_SQL($e, $pContexto, $pSql);
        }
    }

    public function __construct() {
        spl_autoload_register(array($this, 'loader'));
    }

    public function loader($className) {
        if ($className == 'sql') { // inserido por fábio  (conecta automaticamente no banco ao instanciar a classe SQL )
            if (file_exists('LIB' . DS . 'classes' . DS . 'sql' . DS . 'config.php')) {
                require_once 'LIB' . DS . 'classes' . DS . 'sql' . DS . 'config.php';
                require_once 'LIB' . DS . 'classes' . DS . 'sql' . DS . config::$cfg['banco'] . DS . 'sqlexecute.class.php';
                require_once 'LIB' . DS . 'classes' . DS . 'sql' . DS . 'sql.php';
            } elseif (file_exists('../LIB' . DS . 'classes' . DS . 'sql' . DS . 'config.php')) {
                require_once '../LIB' . DS . 'classes' . DS . 'sql' . DS . 'config.php';
                require_once '../LIB' . DS . 'classes' . DS . 'sql' . DS . config::$cfg['banco'] . DS . 'sqlexecute.class.php';
                require_once '../LIB' . DS . 'classes' . DS . 'sql' . DS . 'sql.php';
            } elseif (file_exists('../../LIB' . DS . 'classes' . DS . 'sql' . DS . 'config.php')) {
                require_once '../../LIB' . DS . 'classes' . DS . 'sql' . DS . 'config.php';
                require_once '../../LIB' . DS . 'classes' . DS . 'sql' . DS . config::$cfg['banco'] . DS . 'sqlexecute.class.php';
                require_once '../../LIB' . DS . 'classes' . DS . 'sql' . DS . 'sql.php';
            }

            sql::connect();
        } elseif ($className == "PHPMailer") {
            require "../externos/PHPMailer/class.phpmailer.php";
        } elseif ($className == "mPDF") {
            require('../MPDF56/mpdf.php');
        } else {

            if (file_exists('/LIB/classes/' . $className . '.php')) {
                require_once('/LIB/classes/' . $className . '.php');
            } elseif (file_exists('../LIB/classes/' . $className . '.php')) {
                require_once('../LIB/classes/' . $className . '.php');
            } elseif (file_exists('../../LIB/classes/' . $className . '.php')) {
                require_once('../../LIB/classes/' . $className . '.php');
            } elseif (file_exists('../../../LIB/classes/' . $className . '.php')) {
                require_once('../../../LIB/classes/' . $className . '.php');
            } elseif (file_exists('LIB/classes/' . $className . '.php')) {
                require_once('LIB/classes/' . $className . '.php');
            } elseif (file_exists('LIB/' . $className . '.php')) {
                require_once('LIB/' . $className . '.php');
            } elseif (file_exists('/LIB/' . $className . '.php')) {
                require_once('/LIB/' . $className . '.php');
            } elseif (file_exists('../LIB/' . $className . '.php')) {
                require_once('../LIB/' . $className . '.php');
            } elseif (file_exists('../../LIB/' . $className . '.php')) {
                require_once('../../LIB/' . $className . '.php');
            } elseif (file_exists('LIB/repositorios/' . $className . '.php')) {
                require_once('LIB/repositorios/' . $className . '.php');
            } elseif (file_exists('/LIB/repositorios/' . $className . '.php')) {
                require_once('/LIB/repositorios/' . $className . '.php');
            } elseif (file_exists('../LIB/repositorios/' . $className . '.php')) {
                require_once('../LIB/repositorios/' . $className . '.php');
            } elseif (file_exists('../../LIB/repositorios/' . $className . '.php')) {
                require_once('../../LIB/repositorios/' . $className . '.php');
            } elseif (file_exists('LIB/validadores/' . $className . '.php')) {
                require_once('LIB/validadores/' . $className . '.php');
            } elseif (file_exists('/LIB/validadores/' . $className . '.php')) {
                require_once('/LIB/validadores/' . $className . '.php');
            } elseif (file_exists('../LIB/validadores/' . $className . '.php')) {
                require_once('../LIB/validadores/' . $className . '.php');
            } elseif (file_exists('../../LIB/validadores/' . $className . '.php')) {
                require_once('../../LIB/validadores/' . $className . '.php');
            } elseif (file_exists('/LIB/componentes/' . $className . '.php')) {
                require_once('/LIB/componentes/' . $className . '.php');
            } elseif (file_exists('../LIB/componentes/' . $className . '.php')) {
                require_once('../LIB/componentes/' . $className . '.php');
            } elseif (file_exists('LIB/componentes/' . $className . '.php')) {
                require_once('LIB/componentes/' . $className . '.php');
            } elseif (file_exists('/framework/' . $className . '.php')) {
                require_once('/framework/' . $className . '.php');
            } elseif (file_exists('framework/' . $className . '.php')) {
                require_once('framework/' . $className . '.php');
            } elseif (file_exists('../framework/' . $className . '.php')) {
                require_once('../framework/' . $className . '.php');
            } elseif (file_exists('../../framework/' . $className . '.php')) {
                require_once('../../framework/' . $className . '.php');
            } elseif (file_exists('controles/' . $className . '.php')) {
                require_once('controles/' . $className . '.php');
            } elseif (file_exists('../controles/' . $className . '.php')) {
                require_once('../controles/' . $className . '.php');
            } elseif (file_exists('../../controles/' . $className . '.php')) {
                require_once('../../controles/' . $className . '.php');
            } elseif (file_exists('templates/' . $className . '.php')) {
                require_once('templates/' . $className . '.php');
            } elseif (file_exists('../templates/' . $className . '.php')) {
                require_once('../templates/' . $className . '.php');
            } elseif (file_exists('../../templates/' . $className . '.php')) {
                require_once('../../templates/' . $className . '.php');
            } elseif (file_exists(dirname(__FILE__) . DS . 'classes' . DS . $className . '.php')) {   // inserido por fábio
                require_once(dirname(__FILE__) . DS . 'classes' . DS . $className . '.php');
            }
        }
    }

}
