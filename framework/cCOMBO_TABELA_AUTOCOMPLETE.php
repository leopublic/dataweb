<?php

/**
 * Description of cCOMBO_TABELA_AUTOCOMPLETE
 *
 * @author leonardo
 */
class cCOMBO_TABELA_AUTOCOMPLETE extends cCOMBO_TABELA{
	public function HTML($pNome){
		if($this->mValor != ''){
			$sql = "SELECT ".$this->mCampoChave.",".$this->nomeCampoDescricao()." 
				from ".$this->mTabela." 
				where 1=1 ".$this->mExtra." and ".$this->mCampoChave."=".$this->mValor;
			$res = cAMBIENTE::$db_pdo->query($sql);
			$rs = $res->fetch(PDO::FETCH_ASSOC);
			$valor = $rs[$this->nomeCampoDescricao()];
		}
		
		$ret = '<input type="text" class="cmpTxt"  id="CMP_'.$this->mCampoDescricao.'"  name="CMP_'.$this->mCampoDescricao.'"  value="'.$valor.'"/>';
		$ret .= '<input type="hidden" id="CMP_'.$this->mCampoDescricao.'_novo"  name="CMP_'.$this->mCampoDescricao.'_novo"  value=""/>';
		$ret .= '<input type="hidden" id="CMP_'.$this->mCampoChave.'"  name="CMP_'.$this->mCampoChave.'" value="'.$this->mValor.'"/>';
		$script = '    
$( "#CMP_'.$this->mCampoBD.'" ).autocomplete({
	source: "'.$this->source.'",
	minLength: 1,
	source: function (request, response) {
		$("#CMP_'.$this->mCampoDescricao.'_novo").val(request.term);
		$.ajax({
			url: "'.$this->source.'&term=" + request.term,
			data: request,
			dataType: "json",
			success: function (data) {
				response(data);
			},
			error: function () {
				response([]);
			}
		});
	},
	change: function(event, ui){
		if(ui.item === null){
			$("#CMP_'.$this->mCampoChave.'").val("");
			$("#CMP_'.$this->mCampoDescricao.'").css("border-color", "red");  
		}
		else{
			$("#CMP_'.$this->mCampoChave.'").val(ui.item.'.$this->mCampoChave.');
			$("#CMP_'.$this->mCampoDescricao.'").css("border-color", "black");  
		}
	}
});
		';
		cHTTP::AdicionarScript($script);
		return $ret;
	}
}
