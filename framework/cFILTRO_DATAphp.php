<?php
class cFILTRO_DATA extends cFILTRO{
	private $atributosAdicionais;
	
	public function __construct($pLabel, $pCampo, $pTipoSel=self::tpSEL_LIKE, $pAtributosAdicionais='', $pValorDefault = ''){
		$this->label = $pLabel;
		$this->tipoSel = $pTipoSel;
		$this->campoBusca = $pCampo;
		$this->atributosAdicionais = $pAtributosAdicionais;
		$this->valorDefault = $pValorDefault;
		$this->valor = $this->valorDefault;
		$this->valorNaoSelecionado = '';
	}
		
	public function campoHTML(){
		return '<input type="text" name="'.$this->nomeCampoHTML().'" id="'.$this->nomeCampoHTML().'" value="'.$this->valor.'" class="filtro_data" '.$this->atributosAdicionais.'/>';
	}
	public function Where(){
		$where = '';
		if ($this->valor!=$this->valorNaoSelecionado){
     		$where = " and ".$this->campoBusca." like '%".$this->valor."%'";
		}		
		return $where;
	}
}
