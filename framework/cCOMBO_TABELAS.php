<?php
class cCOMBO_TABELAS extends cCOMBO{
	public function CarregarValores(){
		$this->InicializarValores();
		$sql = "show tables";
		if($cursor = cAMBIENTE::$db_pdo->query($sql)){
			while ($rs = $cursor->fetch(PDO::FETCH_BOTH)){
				$this->AdicionarValor($rs[0], $rs[0]);
			}
		}
	}
}
