<?php

/*
 * Singleton para armazenar dados da sessão atual
 * (Criada para substituir o usuário logado que fica no Session, e não está automaticamente disponível em todas as classes.
 */

class cSESSAO {

    public static $mcd_usuario;
    public static $mnome;
    public static $mcd_perfil;
    public static $mcd_empresa;
    public static $mnm_email;
    public static $mnu_candidato;
    public static $mnu_candidato_tmp;
    public static $candidato;

    public static function AutenticaPeloHash($hash, $cd_usuario){
        $usuario = new usuarios;
        $usuario->RecuperePeloId($cd_usuario);
        if ($usuario->hash_string == $_GET['hash']){
            cSESSAO::RegistraSessao($cd_usuario, $usuario->mnome, $usuario->mcd_perfil, $_SERVER['REMOTE_ADDR']);
            return true;
        } else {
            return false;
        }
    }

    public static function RegistraSessao($pcd_usuario, $pnome, $pcd_perfil, $pIp, $pnm_email = '', $pnu_candidato = '', $pnu_candidato_tmp = '') {
        self::$mcd_usuario = $pcd_usuario;
        self::$mcd_perfil = $pcd_perfil;
        self::$mnome = $pnome;
        self::$mnm_email = $pnm_email;

        if ($pcd_usuario > 0) {
            $usuario = new cusuarios();
            $usuario->cd_usuario = $pcd_usuario;
            $usuario->RecupereSe();
            self::$mcd_empresa = $usuario->cd_empresa;
            $usuario->RegistraUltimoLogin($_SERVER['REMOTE_ADDR']);
            // self::CriaSessao($usuario);

            $GLOBALS['usulogado'] = cHTTP::$SESSION["usuarioLogado"]; # Numero do usuario
            $GLOBALS['usuperfil'] = cHTTP::$SESSION["usuarioPerfil"]; # Codigo do perfil do usuario
            $GLOBALS['usupetro'] = cHTTP::$SESSION["usuarioPetro"];  # S ou N se eh petrobras
            $GLOBALS['usuarmad'] = cHTTP::$SESSION["usuarioArmad"];  # S ou N se eh um armador da petrobras
            $GLOBALS['usufunc'] = cHTTP::$SESSION['usuarioMundi'];  # Se eh funcionario Mundivisas
            $GLOBALS['usuemp'] = cHTTP::$SESSION['usuarioEmpresa']; # Se pertence a uma empresa apenas
            $GLOBALS['usuoper'] = cHTTP::$SESSION['usuarioOper'];  # Se eh operador apenas
            $GLOBALS['usurest'] = cHTTP::$SESSION['usuarioRest'];  # Se eh operador restrito
            $GLOBALS['ehAdmin'] = cHTTP::$SESSION['ehAdmin']; # Se eh administrador ou gerente
            $GLOBALS['ehCliente'] = cHTTP::$SESSION['usuarioClient']; # Se eh cliente
            $GLOBALS['ehFinan'] = cHTTP::$SESSION['usuarioFinan']; # Se eh do financeiro
            $GLOBALS['lstEmpAtende'] = cHTTP::$SESSION['listaEmpresas']; # Lista de empresas que o funcionario atende
            $GLOBALS['ss_nu_candidato'] = cHTTP::$SESSION['ss_nu_candidato']; # Lista de empresas que o funcionario atende
            $GLOBALS['ss_nu_candidato_tmp'] = cHTTP::$SESSION['ss_nu_candidato_tmp']; # ID do DR que está sendo preenchido

            $myAdmin['cd_usuario'] = $usuario->cd_usuario;
            $myAdmin['cd_perfil'] = $usuario->cd_perfil;
            $myAdmin['nm_login'] = $usuario->login;
            $myAdmin['nm_nome'] = $usuario->nome;
            $myAdmin['cd_empresa'] = $usuario->cd_empresa;
            $myAdmin['nm_email'] = $usuario->nm_email;
            $myAdmin['cd_superior'] = $usuario->cd_superior;
            $GLOBALS['myAdmin'] = $myAdmin;
        } elseif($pnu_candidato > 0){
            self::$mnu_candidato = $pnu_candidato;
            self::$candidato = new cCANDIDATO();
            self::$candidato->Recuperar($pnu_candidato);
        } elseif($pnu_candidato_tmp > 0){
            self::$mnu_candidato_tmp = $pnu_candidato_tmp;
            self::$candidato = new cCANDIDATO_TMP();
            self::$candidato->RecuperarPeloTmp($pnu_candidato_tmp);
        } else {
            cHTTP::$SESSION['url'] = $_SERVER[REQUEST_URI];
            throw new Exception("Sessão expirada. Por favor identifique-se novamente.");
        }
    }

    public static function CriaSessao(cusuarios $pusuario) {
        $Acesso = new Acesso();
        $Acesso->GetAcesso($pusuario->cd_usuario);

        $myAdmin['cd_usuario'] = $pusuario->cd_usuario;
        $myAdmin['cd_perfil'] = $pusuario->cd_perfil;
        $myAdmin['nm_login'] = $pusuario->login;
        $myAdmin['nm_nome'] = $pusuario->nome;
        $myAdmin['cd_empresa'] = $pusuario->cd_empresa;
        $myAdmin['nm_email'] = $pusuario->nm_email;
        $myAdmin['cd_superior'] = $pusuario->cd_superior;
        cHTTP::$SESSION['usuario'] = $myAdmin;
        cHTTP::$SESSION['myAdmin'] = $myAdmin;

        cHTTP::$SESSION['usuarioLogado'] = $pusuario->cd_usuario;
        cHTTP::$SESSION['usuarioPerfil'] = $pusuario->cd_perfil;
        cHTTP::$SESSION['usuarioPetro'] = $pusuario->get_ehPetro();
        cHTTP::$SESSION['usuarioEmpresa'] = $pusuario->cd_empresa;

        $perfil = new cusuario_perfil();
//        $perfil->RecupereSe($pusuario->cd_perfil);
        $res_perfis = $perfil->acessosDoUsuario($pusuario->cd_usuario);
        $perfis = $res_perfis->fetchAll();
        cHTTP::$SESSION['perfis'] = $perfis;
        
//        cHTTP::$SESSION['usuarioArmad'] = $perfil->get_eh_armador();
        $ehFunc = $perfil->ehFuncionario($pusuario->cd_usuario);
        cHTTP::$SESSION['usuarioMundi'] = $ehFunc;
//        cHTTP::$SESSION['usuarioOper'] = $perfil->get_eh_operador();
//        cHTTP::$SESSION['usuarioRest'] = $perfil->get_eh_operrestrito();
//        cHTTP::$SESSION['usuarioClient'] = $perfil->get_eh_cliente();
//        cHTTP::$SESSION['usuarioFinan'] = $perfil->get_eh_financeiro();
        $ehAdm = $perfil->ehAdm($pusuario->cd_usuario);
        cHTTP::$SESSION['ehAdmin'] = $ehAdm;

    }

    public static function CriaSessaoCandidato(cCANDIDATO $cand) {
        cHTTP::$SESSION['ss_nu_candidato'] = $cand->mNU_CANDIDATO;

    }

    public static function CriaSessaoCandidatoTmp(cCANDIDATO_TMP $cand) {
        cHTTP::$SESSION['ss_nu_candidato_tmp'] = $cand->mnu_candidato_tmp;

    }

}
