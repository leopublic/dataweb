<?php

class cFILTRO_CHAVE extends cFILTRO {

    const enSERVICO = 1;
    const enEMPRESA = 2;
    const enRESPONSAVEL = 3;
    const enCADASTRADOPOR = 4;
    const enSTATUS_SOLICITACAO = 5;
    const enEMBARCACAO_PROJETO = 6;
    const enSTATUS_COBRANCA = 7;

    private $atributosAdicionais;
    private $campoDescricao;
    private $extra;
    private $entidade;
    private $opcaoSeVazio;
    private $filtro;
    
    public function __construct($pLabel, $pEntidade, $pCampoBusca = '', $pAtributosAdicionais = '', $pFiltro = '', $pInd = '', $pqualificadorQuery = '') {
        $this->entidade = $pEntidade;
        $this->label = $pLabel;
        $this->atributosAdicionais = $pAtributosAdicionais;
        $this->campoBusca = $pCampoBusca;
        $this->opcaoSeVazio = '(todos)';
        $this->filtro = '';
        $this->valorDefault = '0';
        $this->valorNaoSelecionado = '0';
        $this->ind = $pInd;

        if ($this->entidade == self::enSERVICO) {
            $this->tabela = 'SERVICO ';
            $this->campoChave = 'NU_SERVICO';
            $this->campoDescricao = "NO_SERVICO_RESUMIDO";
            $this->extra = 'order by NO_SERVICO_RESUMIDO';
        } elseif ($this->entidade == self::enSTATUS_SOLICITACAO) {
            $this->tabela = 'STATUS_SOLICITACAO';
            $this->campoChave = 'ID_STATUS_SOL';
            $this->campoDescricao = "NO_STATUS_SOL";
            $this->extra = 'order by ID_STATUS_SOL';
        } elseif ($this->entidade == self::enEMPRESA) {
            $this->tabela = 'EMPRESA';
            $this->campoChave = 'NU_EMPRESA';
            $this->campoDescricao = "NO_RAZAO_SOCIAL";
            $this->extra = 'order by NO_RAZAO_SOCIAL';
        } elseif ($this->entidade == self::enRESPONSAVEL) {
            $this->tabela = 'usuarios';
            $this->campoChave = 'cd_usuario';
            $this->campoDescricao = "nome";
            $this->extra = ' order by nome';
        } elseif ($this->entidade == self::enCADASTRADOPOR) {
            $this->tabela = 'usuarios';
            $this->campoChave = 'cd_usuario';
            $this->campoDescricao = "nome";
            $this->extra = 'order by nome';
        } elseif ($this->entidade == self::enEMBARCACAO_PROJETO) {
            $this->tabela = 'EMBARCACAO_PROJETO';
            $this->campoChave = 'NU_EMBARCACAO_PROJETO';
            $this->campoDescricao = "NO_EMBARCACAO_PROJETO";
            $this->extra = $pFiltro . ' order by NO_EMBARCACAO_PROJETO';
            $this->opcaoSeVazio = '(selecione a empresa)';
        } elseif ($this->entidade == self::enSTATUS_COBRANCA) {
            $this->tabela = 'status_cobranca_os';
            $this->campoChave = 'stco_id';
            $this->campoDescricao = "stco_tx_nome";
            $this->extra = ' order by stco_tx_nome';
        }
        if ($this->campoBusca == '') {
            $this->campoBusca = $this->campoChave;
        }
        
        $this->qualificadorQuery = $pqualificadorQuery;
    }

    public function campoHTML() {
        $select = '<select name="' . $this->nomeCampoHTML() . '" id="' . $this->nomeCampoHTML() . '" class="filtro_chave" ' . $this->atributosAdicionais . '/>';
        $select .= '<option value="' . $this->valorNaoSelecionado . '">' . $this->opcaoSeVazio . '</option>';
        $select .= $this->montaCombo($this->tabela, $this->campoChave, $this->campoDescricao, $this->valor, $this->extra);
        $select .= '</select>';
        return $select;
    }

    private function montaCombo($tabela, $indice, $nome, $codigo, $extra) {
        $ret = "";
        $sql = "SELECT $indice,$nome from $tabela $extra ";
        $rs = mysql_query($sql);
        if (mysql_errno() > 0) {
            print "\n<!--\n ERRO: " . mysql_error() . "\n SQL=$sql\n-->\n";
        } else {
            while ($rw = mysql_fetch_array($rs)) {
                if ((strlen($codigo) > 0) && ($rw[0] == $codigo)) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                $ret = $ret . "<option value='" . $rw[0] . "' $sel>" . trim($rw[1]);
            }
        }
        return $ret;
    }

    public function Where() {
        $where = '';
        if ($this->valor != $this->valorNaoSelecionado) {
            $where = " and " . $this->getQualificadorQuery() . $this->campoBusca . " =" . $this->valor;
        }
        return $where;
    }
}
