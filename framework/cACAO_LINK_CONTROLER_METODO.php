<?php
class cACAO_LINK_CONTROLER_METODO extends cACAO{
	/**
	 * Controla como a ação será implementada na página. Pode ser cACAO::modoPOST, cACAO::modoREDIRECT ou cACAO::modoAJAX
	 * @var constante
	 * @return type
	 */
	public $mModo;
	const modoPOST = 'POST';
	const modoREDIRECT = 'REDIRECT';
	const modoREDIRECT_SEM_MENU = 'REDIRECT_SEM_MENU';
	const modoAJAX = 'AJAX';
	const modoDOWNLOAD = 'DOWNLOAD';

	/**
	 * Indica qual o target, caso a acao seja implementada como link.
	 * @var string
	 */
	public $mTarget ;
	public $mContainerDestino ;


	public function __construct($pLabel, $pCodigo , $pTitulo , $pControler, $pMetodo, $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "", $pPagina = ""){
		parent::__construct(cACAO::ACAO_LINK_CONTROLER_METODO, $pLabel, $pCodigo , $pTitulo , $pMsgConfirmacao , $pMsgConfirmacaoTitulo );
		$this->mMetodo = $pMetodo;
		$this->mControle = $pControler;
		$this->mModo = self::modoREDIRECT;
		$this->mTarget = "_self";
		$this->mpagina = $pPagina;
	}
}
