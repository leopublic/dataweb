<?php

class cCOMBO_SITUACAO extends cCOMBO_GERAL {

    public function __construct($pCampoBD, $pLabel, $pLabel_en_us = '') {
        parent::__construct($pCampoBD, $pLabel, $pLabel_en_us);
        $this->mValorDefault = '';
    }

    public function CarregarValores() {
        $this->AdicionarValor("1", "Ativas(os)");
        $this->AdicionarValor("0", "Inativas(os)");
    }

    public function where() {
        switch ($this->mValor) {
            case "":
                return "";
                break;
            case "0":
            case "1":
                return $this->getQualificadorFiltro . $this->mCampoBD . " = " . $this->mValor;
                break;
        }
    }

}
