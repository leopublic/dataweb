<?php
class cCAMPO_TEXTO extends cCAMPO{
	public function __construct($pCampoBD, $pLabel, $pLabel_en_us = ''  ) {
		parent::__construct($pCampoBD, $pLabel, cCAMPO::cpTEXTO, $pCampoBD, 0, false, '', 0, '' , 0, 1, 0, 0 , 0, $pLabel_en_us);

	}
	public function FormatoMustache(){
		$saida = array();
		$saida["cpTEXTO"] = true;
		$saida["label"] = $this->Label();
		$saida["nome"] = $this->mCampoBD;
		$saida["valor"] = $this->mValor;
		$saida["class"] = $this->mClasse;
		return $saida;
	}
}
