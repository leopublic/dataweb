<?php
/**
 * Campo data sem picker
 */
class cCAMPO_DATA_ONLY extends cCAMPO{
	public function __construct($pCampoBD, $pLabel , $pLabel_en_us = ""  ) {
		parent::__construct($pCampoBD, $pLabel, cCAMPO::cpDATA_ONLY, $pCampoBD, 0, false, '', 0, '', 0, 1, 0, 0 , 0, $pLabel_en_us);

	}
}
