<?php

class cINTERFACE_TWIG extends cINTERFACE {

    public function RenderizeHead($pTitle) {
        $loader = new Twig_Loader_Filesystem('../templates_twig');
        $twig = new Twig_Environment($loader, array('charset' => 'UTF-8', 'cache' => '../templates_twig_bin'));
        $template = $twig->loadTemplate('cliente/head.html');
        $data = array('title' => $pTitle);
        $ret = $template->render($data);
        return $ret;
    }

    public function RedererizeCabecalho($ptitulo) {

    }

}
