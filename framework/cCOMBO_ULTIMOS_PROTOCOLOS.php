<?php
class cCOMBO_ULTIMOS_PROTOCOLOS extends cCOMBO_GERAL{
	public function CarregarValores(){
		$sql = "select date_format(dt_envio_bsb, '%d/%m/%Y') dt_envio_bsb_fmt, count(*) qtd
				 from (
					select dt_envio_bsb from processo_mte where dt_envio_bsb is not null  and dt_envio_bsb > '2013-01-01' and dt_envio_bsb <= now()
					union all select dt_envio_bsb from processo_prorrog where dt_envio_bsb is not null  and dt_envio_bsb > '2013-01-01' and dt_envio_bsb <= now()
					union all select dt_envio_bsb from processo_cancel where dt_envio_bsb is not null  and dt_envio_bsb > '2013-01-01' and dt_envio_bsb <= now()
					union all select dt_envio_bsb_pre_cadastro from processo_prorrog where dt_envio_bsb_pre_cadastro is not null  and dt_envio_bsb_pre_cadastro > '2013-01-01' and dt_envio_bsb_pre_cadastro <= now()
				) as x
				group by dt_envio_bsb
				order by dt_envio_bsb desc";
		$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.'.'.__FUNCTION__);
		while($rs = $res->fetch(PDO::FETCH_BOTH)){
			$this->AdicionarValor($rs['dt_envio_bsb_fmt'], $rs['dt_envio_bsb_fmt'].' - '.$rs['qtd'].' processos');			
		}
	}
	
	public function where(){
		return " 1=1 ";
	}
	
}
