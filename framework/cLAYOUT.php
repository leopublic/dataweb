<?php

class cLAYOUT {

    const CHARSET = "UTF-8";

    public static function HeadJavascript() {

    }

    public static function Head_DocType() {
        // return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		// "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' . "\n";
		return '<!DOCTYPE html>' . "\n";
    }

    public static function Head_ContentType($pTipo = 'html') {
        switch ($pTipo) {
            case 'html' :
                $meta = '<meta http-equiv="Content-Type" content="text/html; charset=' . self::CHARSET . '">' . "\n";
                $meta .= '<meta http-equiv="X-UA-Compatible" content="IE=edge" >' . "\n";
                break;
            case 'excel' :
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Content-Type: application/force-download");
                header("Content-Type: application/octet-stream");
                header("Content-Type: application/download");
                ;
                header("Content-Disposition: attachment;filename=" . cHTTP::get_nomeArquivo());
                header("Content-Transfer-Encoding: binary ");
                $meta = '';
                break;
            case 'word' :
                header("Content-type: application/vnd.ms-word");
                header("Content-Type: application/force-download");
                header("Content-Type: application/octet-stream");
                header("Content-Type: application/download");
                ;
                header("Content-Disposition: attachment;Filename=document_name.doc");
                break;

            default:
                $meta = '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
        }
        return $meta;
    }

    public static function Head_Scripts() {
        $ret = '';
        $ret .= '<script type="text/javascript" src="/js/ajaxInterface.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/criamascara.js"></script>' . "\n";
//		$ret .= '<script type="text/javascript" src="/js/jquery-1.6.2.min.js"></script>' . "\n";
//		$ret .= '<script type="text/javascript" src="/js/jquery-ui-1.8.16.custom.min.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/jquery-1.9.1.js"></script>' . "\n";
//		$ret .= '<script type="text/javascript" src="/js/jquery-migrate-1.1.1.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/jquery-ui-1.10.1.custom.min.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/jquery.ui.datepicker-pt-BR.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/jquery.alerts.js"></script>' . "\n";
        if (cHTTP::comCabecalhoFixo()) {
            $ret .= '<script type="text/javascript" src="/js/jquery.floatheader.min.js"></script>';
        } else {
//			$ret .= '<script type="text/javascript" src="/js/jquery.imagetool-1.1.min.js"></script>' . "\n";
        }
        $ret .= '<script type="text/javascript" src="/js/jquery.floatThead.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/jquery.Jcrop.min.js"></script>' . "\n";
//		$ret .= '<script type="text/javascript" src="/js/jquery.corner.js"></script>' . "\n";

        $ret .= '<script type="text/javascript" src="/js/jquery.quicksearch.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/jquery.grid.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/jquery.navMenu.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/funcoes.js?dt=20150701"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/inicializacoes.js"></script>' . "\n";
        $ret .= '<script type="text/javascript" src="/js/tablesorter/jquery.tablesorter.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/jquery.maskMoney.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/jquery.floatingmessage.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/jquery.jeditable.mini.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/noty/jquery.noty.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/noty/layouts/top.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/noty/layouts/topCenter.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/noty/layouts/center.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/noty/themes/default.js"></script>';
//        $ret .= '<script type="text/javascript" src="/js/tinynav.min.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/noty/jquery.noty.js"></script>';
  //      $ret .= '<script type="text/javascript" src="/js/jquery-ui-timepicker-addon.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/noty/layouts/topCenter.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/noty/themes/default.js"></script>';
        $ret .= '<script type="text/javascript" src="/js/jquery.form.min.js"></script>';
        return $ret;
    }

    public static function Head_Css() {
        $ret = '';
        $ret .= '<link href="/js/tablesorter/themes/blue/style.css" rel="stylesheet" type="text/css"/>' . "\n";
        $ret .= '<link href="/css/jquery.grid.css" rel="stylesheet" type="text/css"/>' . "\n";
        $ret .= '<link href="/css/jquery.navMenu.css" rel="stylesheet" type="text/css"/>' . "\n";
//		$ret .= '<link href="/css/start/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css" media="screen"/>' . "\n";
        $ret .= '<link href="/css/start/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen"/>' . "\n";
        $ret .= '<link href="/css/start/jquery.ui.theme.css" rel="stylesheet" type="text/css" media="screen"/>' . "\n";
        $ret .= '<link href="/css/jquery.Jcrop.css" rel="stylesheet" type="text/css" media="screen"/>' . "\n";
        $ret .= '<link href="/css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen"/>' . "\n";
        $ret .= '<link href="/css/ddsmoothmenu.css" rel="stylesheet" type="text/css" />' . "\n";
        $ret .= '<link href="/css/ddsmoothmenu-v.css" rel="stylesheet" type="text/css" />' . "\n";
        $ret .= '<link href="/estilos.css?x=201501" rel="stylesheet" type="text/css"/>' . "\n";
        $ret .= '<link href="/externos/FortAwesome-Font-Awesome-13d5dd3/css/font-awesome.css" rel="stylesheet" type="text/css" />' . "\n";
//		$ret .= "<link href='http://fonts.googleapis.com/css?family=Voltaire|Andika|Yanone+Kaffeesatz:300' rel='stylesheet' type='text/css'>";
//		$ret .= '<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu:regular,bold&subset=Latin">';
        return $ret;
    }

    public static function CabecalhoPadrao() {
        $ret = self::Head_DocType();
        $ret .= "<html>\n<head><title>MV::" . cHTTP::get_titulo_pagina() . " </title>\n";

        //<link rel="shortcut icon" href="http://www.mundivisas.com.br/site/sites/default/files/favicon.ico">
        $ret .= "\n" . '<link rel="shortcut icon"  type="image/vnd.microsoft.icon" href="' . cAMBIENTE::$m_dominio . '/favicon.ico">';
        $ret .= self::Head_ContentType();
        $ret .= self::Head_Scripts();
        $ret .= self::Head_Css();
        $ret .= cHTTP::estilosAdicionais();
        $ret .= "</head>\n";
        $ret .= '<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">' . "\n";
        return $ret;
    }

    public static function CabecalhoDR() {
        $ret = self::Head_DocType();
        $ret .= "<html>\n<head><title>MV::" . cHTTP::get_titulo_pagina() . " </title>\n";
        $ret .= self::Head_ContentType();
        $ret .= self::Head_Scripts();
        $ret .= '<link href="http://www.mundivisas.com.br/site/sites/all/themes/basic/css/style.css" rel="stylesheet" type="text/css" media="screen"/>' . "\n";
        $ret .= cHTTP::estilosAdicionais();
        $ret .= "</head>\n";
        return $ret;
    }

    public static function Logo() {
//		$ret .= '<div class="logo"><img src="/imagens/mundivisas-logo.png" style="margin:0;padding:0;"/></div>';
        return $ret;
    }

    public static function Menu() {

        $ret = '<div id="cabheader">
                    <div class="subheader">
                        <div id="logo" class="slot-0">
                                <img src="/img/mundivisas_logo.PNG" />
                        </div>
                        <div class="nav">' . self::soMenu() . '
                        </div>
                    </div>
                </div>';

        return $ret;
    }

    public static function soMenu(){
        $ret = '';
        $ret .= '<div  class="xmenu">
                    <ul>
                        <li><a href="/paginas/carregue.php?controller=cCTRL_HOME&metodo=home">Home</a></li>';
        if ($GLOBALS['usufunc'] == "S") {
            $ret.= '	<li><a href="#">Sistema</a>
                    <ul>';

            if ($_SESSION['ehAdmin'] == "S") {
                $ret.= '
                        <li><a href="#">Controle de acesso</a>
                                <ul>
                                        <li><a href="/admin/permissao_acesso.php" target="_parent">Perfis </a></li>
                                        <li><a href="/admin/cargaCandidatos.php" target="_parent">Importação de Candidatos</a></li>
                                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_USUARIOS&metodo=Liste" target="_parent">Usuários</a></li>
                                </ul>
                        </li>										';
            }
            $ret.= '<li><a href="#">Tabelas Auxiliares</a>
                    <ul>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_ESCO_LIST&metodo=Liste" target="_parent">Escolaridade</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_ECIV_LIST&metodo=Liste" target="_parent">Estado Civil</a></li>
                        <li><a href="/paginas/carregue.php?controller=cCTRL_FORNECEDOR&metodo=getListar" target="_parent">Fornecedores</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_FUNC_LIST&metodo=Liste" target="_parent">Fun&ccedil;&atilde;o/Cargo</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_GPAR_LIST&metodo=Liste" target="_parent">Grau Parentesco</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_PAIS_LIST&metodo=Liste" target="_parent">Pa&iacute;s/Nacionalidade</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_PROF_LIST&metodo=Liste" target="_parent">Profiss&atilde;o</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_REPC_LIST&metodo=Liste" target="_parent">Reparti&ccedil;&atilde;o Consular</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_TAUT_LIST&metodo=Liste" target="_parent">Tipo Autoriza&ccedil;&atilde;o</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_TARQ_LIST&metodo=Liste">Tipos de arquivo</a></li>
                        <li><a href="/paginas/carregue.php?controller=cCTRL_TIPO_DESPESA&metodo=getListar" target="_parent">Tipos de despesa</a></li>
            		';
            if (Acesso::permitido(Acesso::tp_Sistema_Tabelas_Servico)) {
                $ret.= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_TSRV_LIST&metodo=Liste" target="_parent">Serviço - Tipos</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_SERV_LIST&metodo=Liste" target="_parent">Serviço</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_PACT_LIST&metodo=Liste" target="_parent">Pacotes</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_PTAX_LIST&metodo=Liste" target="_parent">Perfis de taxa</a></li>';
            }

            $ret.= '<li><a href="/admin/auxiliares1.php?id=CNT" target="_parent">N&iacute;vel de Contato</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Configurações</a>
                        <ul>
                            <li><a href="/operador/TextoEmailConfirmacao.php">Texto do e-mail de confirmação</a></li>
                            <li><a href="/operador/CotacaoDolar.php">Cotação do Dólar</a></li>
                        </ul>
                    </li>
                    <li><a href="/paginas/carregue.php?controller=cCTRL_ARQUIVO&metodo=monitoramentoTamanho" target="_parent">Monitoramento arquivos</a></li>
                </ul>
            </li>';
        }

        if ($GLOBALS['usufunc'] == "S") {
            /* 			$ret.= '	<ul>
              <li><a href="#">Gerencial</a>
              <ul>
              <li><a href="/gerencial/acompanha_eventos.php" target="_parent">Eventos de Processos</a></li>
              <li><a href="/gerencial/relatorio_mte.php" target="_parent">Relat&oacute;rios MTE</a></li>
              <li><a href="/atendimento/autorizacao_inicial.php" target="_parent">Acompanhamento de solicita&ccedil;&otilde;es</a></li>
              </ul>
              </li>
              </ul>';
             */
        }
        if (Acesso::permitido(Acesso::tp_Os)) {
            // if($usufunc=="S") {
            $ret.= '<li><a href="#">Ordens de Servi&ccedil;o</a>
							<ul>';

            if (Acesso::permitido(Acesso::tp_Os_Listar)) {
                $ret .= '<li><a href="/intranet/geral/os_listar.php?primeiravez=1" target="_parent">Listar ordens de servi&ccedil;o</a></li>';
            }

            if (Acesso::permitido(Acesso::tp_Os_Nova)) {
                $ret .= '<li><a href="/operador/os_DadosOS.php?ID_SOLICITA_VISTO=0" target="_parent">Nova Ordem de Servi&ccedil;o</a></li>';
            }
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_PROR_PREC_LIST&metodo=Liste" target="_parent">Pré-cadastro prorrogações</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_ACOMP_LIST&metodo=Liste" target="_parent">Acompanhamento</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_ANDA_LIST&metodo=Liste" target="_parent">Andamento</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_HIST_STAT_LIST&metodo=Liste" target="_parent">Histórico de status</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_CAND_PIT_LIST&metodo=Liste" target="_parent">PIT</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_PBSB_LIST&metodo=Liste" target="_parent">Protocolo BSB</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_PBSB_PROTOCOLO&metodo=Liste" target="_parent">Protocolo BSB - Protocolar</a></li>';
            $ret .= '<li><a href="/intranet/geral/os_listar_protocolo_coleta.php" target="_parent">Protocolo de Coleta</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_PREG_LIST&metodo=Liste" target="_parent">Protocolo de Registro</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_TAXA_LIST&metodo=Liste" >Taxas - Controle de taxas</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_TAXA_ACOMP_LIST&metodo=Liste" >Taxas - Acompanhamento</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_TAXA_NPAG_LIST&metodo=Liste" >Taxas - Não pagas</a></li>';
            $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=certidao_tramite&inicio=0&qtd=300" >Certidao de tramite (teste)</a></li>';

            $ret.= '		</ul>
						</li>';
            // }
        }
        if ($GLOBALS['usufunc'] == "S") {
            $ret.= '	<li><a href="#">Cadastros</a>
                <ul>
                    <li><a href="/intranet/geral/cand_listar.php">Candidatos</a></li>';
        }

        if (Acesso::permitido(Acesso::tp_Cadastro_Candidato_Incluir)) {
            $ret.= '			<li><a href="/operador/candidato_novo.php">Incluir candidato</a></li>';
        }

        if ($GLOBALS['usufunc'] == "S") {
            $ret.= '<li><a href="/intranet/geral/cand_listar_duplicados.php">Candidatos com duplicados</a></li>
                    <li><a href="/paginas/carregue.php?controller=cCTRL_DR&metodo=listarPendentes" title="">DATA REQUEST pendentes</a></li>
                    <li><a href="/paginas/carregue.php?controller=cCTRL_DR&metodo=getEnviarNovo" title="">DATA REQUEST enviar novo</a></li>
                    <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=ConsultarDescricoesAtividade">Descrições de atividades</a></li>
                    <li><a href="/intranet/geral/empresas.php">Empresas</a></li>
                    <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_EMBP_LIST&metodo=Liste">Embarcações/Projetos</a></li>';
            if (Acesso::permitido(Acesso::tp_Cadastro_EmpresaNova)) {
                $ret.= '<li><a href="/intranet/geral/novaEmpresa.php">Nova empresa</a></li>';
            }
            $ret .='</ul>
                </li>';
        }

        $ret.= '<li><a href="#">Relat&oacute;rios</a>
                    <ul>
                        <li><a href="/paginas/carregue.php?controller=cCTRL_AGENDA_LIST&metodo=Liste" target="_parent">Agenda de processos pendentes</a></li>
                        <li><a href="/relat/rel_cand_sem_cpf.php" target="_parent">Candidatos sem CPF</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_CAND_CREW_LIST&metodo=Liste" target="_parent">Crew List</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_CANDIDATO&metodo=RelatorioPorProjeto" target="_parent">Estrangeiros por Projeto</a></li>
                        <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_CANDIDATO&metodo=ListarNecessitaProrrogacao" target="_parent">Necessidade de Prorrogação</a></li>
                        <li><a href="/relat/rel_validade_passaporte.php" target="_parent">Prazo de validade de passaporte</a></li>
                        <li><a href="/relat/rel_prorrog_nao_concluida.php" target="_parent">Prorrogações não concluídas</a></li>
                        <li><a href="/relat/rel_mte_nao_coletado.php" target="_parent">Visto atual deferido mas não coletado</a></li>';
        if ($_SESSION['ehAdmin'] == "S") {
            $ret .='<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_USUARIOS&metodo=ListeRevisoes" target="_parent">Evolução das revisões por usuário</a></li>
                    <li><a href="/intranet/geral/evolucaoRevisoes.php" target="_parent">Evolução das revisões por dia</a></li>';
        }
        if ($_SESSION['ehAdmin'] == "S" || cSESSAO::$mcd_usuario == 258) {
            $ret .='<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_PRODUTIVIDADE_ANALISTAS_LIST&metodo=Liste" target="_parent">Produtividade das analistas</a></li>';
        }
        $ret .='</ul>
            </li>';
        if (Acesso::permitido(Acesso::tp_Cobranca)) {
            $ret.= '<li><a href="#">Cobran&ccedil;a</a>
                        <ul>
                            <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=ListeFinanceiro">Ordens de Serviço</a></li>
                            <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_ORDEMSERVICO&metodo=ListeNaoFaturadasPorSolicitante">Faturar OS liberadas</a></li>
                            <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_RESUMO_COBRANCA&metodo=Liste" >Resumos de Cobrança</a></li>
                            <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_TPRC_LIST&metodo=Liste" >Tabelas de preços</a></li>
                            <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_RESUMO_COBRANCA&metodo=DemonstrativoDiario" >Movimentação diária</a></li>
                            <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_EMPRESA_LIST_END_COB&metodo=Liste">Endereços de cobrança</a></li>
                            <li><a href="/paginas/carregueComMenu.php?controller=cCTRL_OS_FAT_LEGADO&metodo=Liste">Conferência de faturamento</a></li>
                        </ul>
                    </li>';
        }
        $ret .= '<li><a href="/paginas/carregueComMenu.php?controller=cCTRL_USUARIOS&metodo=RenoveMinhaSenha">Alterar senha</a></li>
                    <li><a href="/sair.php">Sair</a></li>';

        $ret .= '</ul>';
        $ret .= cINTERFACE_AMBIENTE::ControleLinguagem();
        $ret .= '</div>';

        return $ret;

    }

    public static function MenuCliente() {
        $ret = '';

        $ret .= '<div id="smoothmenu1" class="ddsmoothmenu" style="height:30px;">
					<ul>
						<li><a href="/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO&metodo=ListarCandidatos_cliente">Home</a></li>
					</ul>
					<ul>
						<li><a href="/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO&metodo=ListarCandidatos_cliente">List candidates</a></li>
					</ul>
					<ul>
						<li><a href="/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO&metodo=VerificarCandidatoNovoExistente">Add new candidate</a></li>
					</ul>
					<ul>
						<li><a href="/paginas/carregueCliente.php?controller=cCTRL_USUARIOS&metodo=RenoveMinhaSenha">Change password</a></li>
					</ul>
					<ul>
						<li><a href="/sair.php">Logout</a></li>
					</ul>
					';


        //$ret.= '		<img src="/imagens/brasil.gif" align="right" /><img src="/imagens/ingles.gif" align="right" />
        $ret .= '<div style="float-right;display:inline;clear:left;align:right;">';
        //	$ret .= '    <a href="/home.php?newlang=P"><img src="/imagens/brasil.gif" border=0></a>';
        //	$ret .= '	<a href="/home.php?newlang=E" class="textoazul"><img src="/imagens/ingles.gif" border=0></a>';
        $ret .= cINTERFACE_AMBIENTE::ControleLinguagem();
        $ret .= '</div>';
        //    $ret .='<br style="clear: left" />';
        $ret .= '</div>';

        return $ret;
    }

    public static function boxConteudo_inicio() {
        $ret = '';
        $ret .= '<div class="conteudo">';
        return $ret;
    }

    public static function boxConteudo_fim() {
        $ret = '';
        $ret .= '</div>';
        $ret .= '<div id="formPopup" name="formPopup" style="display:none">';
        $ret .= 'vazio';
        $ret .= '</div>';
        $ret .= '</body>';
        return $ret;
    }

    public static function boxConteudoInterno_inicio() {
        $ret = '';
        return $ret;
    }

    public static function boxConteudoInterno_fim() {
        $ret = '';
        return $ret;
    }

    public static function AdicionarScriptsPadrao() {
//		if (cHTTP::comMenu()) {
//			$ret = '';
//			$ret .= "ddsmoothmenu.init({\n";
//			$ret .= "	mainmenuid: 'smoothmenu1', //menu DIV id\n";
//			$ret .= "	orientation: 'h', //Horizontal or vertical menu: Set to 'h' or 'v'\n";
//			$ret .= "	classname: 'ddsmoothmenu', //class added to menu's outer DIV\n";
//			$ret .= "	customtheme: ['#317082', '#18374a'],\n";
////			$ret .= "	customtheme: ['#e3e3e3', '#333333'],\n";
//			$ret .= "	contentsource: 'markup' //'markup' or ['container_id', 'path_to_menu_file']\n";
//			$ret .= "})\n";
//			cHTTP::AdicionarScript($ret);
//		}
        $ret = "$(document).ready(function(){
				   $('.orderby').tablesorter({
					sortClassAsc: 'headerSortUp',
					sortClassDesc: 'headerSortDown',
					headerClass: 'header'
				   });
				});";
        cHTTP::AdicionarScript($ret);
    }

    public static function AdicionarScriptInicializacaoAbas($pNomeDiv = 'tabOS') {

        $script = '$(document).ready(function() {
					var x = $("#' . $pNomeDiv . '").tabs({
						heightStyle: "content",
						beforeActivate: function(event, ui) {
							if(ui.newTab.context.href.substr(0,4) == "http" ){
								window.location.href = ui.newTab.context.href;
								return false;
							}
							else{
								return true;
							}
						}
					});		';
        cHTTP::AdicionarScript($script);
    }

    public static function AdicionarScriptExibicaoMensagens() {
        if (cHTTP::$SESSION['msg'] != '') {
            cHTTP::AdicionarScript('jAlert(\'' . cHTTP::$SESSION['msg'] . '\');');
            cHTTP::$SESSION['msg'] = '';
        }
    }

}
