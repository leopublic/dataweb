<?php

/**
 * Classe que controla as notificações a serem apresentadas aos usuários
 *
 * @author leonardo
 */
class cNOTIFICACAO {

    protected $msg;
    protected $tituloMsg;
    protected $tipoMsg;
    protected static $notificacao;

    const TM_SUCCESS = 'sucess';
    const TM_INFO = 'info';
    const TM_ERROR = 'error';
    const TM_ALERT = 'atention';
    const TM_NOP = 'nop';

    public function __construct($pMsg = '', $pTipoMsg = self::TM_ALERT, $pTituloMsg = '') {
        $this->msg = $pMsg;
        cHTTP::$SESSION['msg'] = $this->msg;
        $this->tipoMsg = $pTipoMsg;
        cHTTP::$SESSION['tipoMsg'] = $this->tipoMsg;
        $this->tituloMsg = $pTituloMsg;
        $this->RevisaTituloMsg();
        cHTTP::$SESSION['tituloMsg'] = $this->tituloMsg;
    }

    public function get_msg() {
        return $this->msg;
    }

    public function get_tituloMsg() {
        return $this->tituloMsg;
    }

    public function get_tipoMsgTwig() {
        switch ($this->tipoMsg) {
            case self::TM_ERROR:
                return "error";
                break;
            default:
                return "atention";
                break;
        }
    }

    public function get_tipoMsg() {
        return $this->tipoMsg;
    }

    /**
     * Retorna a instancia única da notificação ativa
     * @param type $pMsg
     * @param type $pTipoMsg
     * @param type $pTituloMsg
     * @return type cNOTIFICACAO
     */
    public static function singleton($pMsg = '', $pTipoMsg = self::TM_ALERT, $pTituloMsg = '') {
        if (is_object(self::$notificacao)) {
            return self::$notificacao;
        } else {
            self::$notificacao = new cNOTIFICACAO($pMsg, $pTipoMsg, $pTituloMsg);
            return self::$notificacao;
        }
    }

    protected function RevisaTituloMsg() {
        if ($this->tituloMsg == '') {
            switch ($this->tipoMsg) {
                case cNOTIFICACAO::TM_SUCCESS:
                    $this->tituloMsg = 'Dataweb - OK!';
                case cNOTIFICACAO::TM_ALERT:
                    $this->tituloMsg = 'Dataweb - Atenção';
                    break;
                case cNOTIFICACAO::TM_ERROR:
                    $this->tituloMsg = 'Dataweb - Erro!';
                    break;
                case cNOTIFICACAO::TM_INFO:
                default:
                    $this->tituloMsg = 'Dataweb';
                    break;
            }
        }
    }

    public static function TemNotificacaoPendente() {
        if (is_object(self::$notificacao)) {
            return true;
        } else {
            return false;
        }
    }

    public static function HTML_NotificacaoPendente($pReset = true) {
        self::$notificacao = new cNOTIFICACAO(cHTTP::$SESSION['msg'], cHTTP::$SESSION['tipoMsg'], cHTTP::$SESSION['tituloMsg']);
        if (cHTTP::$SESSION['msg'] != '') {
            return '<div id="dialog" style="display: none;" title="' . self::$notificacao->get_msg() . '"><div class="ui-state-' . self::$notificacao->get_tipo_msg() . '">' . self::$notificacao->get_msg() . '</div></div>';
            cHTTP::AdicionarScript("$('#dialog').dialog({minWidth:500}) ;");
        } else {
            return "\n<!-- Nenhuma mensagem pendente -->\n";
        }
        if ($pReset) {
            unset(cHTTP::$SESSION['msg']);
            unset(cHTTP::$SESSION['tipoMsg']);
            unset(cHTTP::$SESSION['tituloMsg']);
        }
    }

    public static function NotificacaoPendente($pReset = true) {
        self::$notificacao = new cNOTIFICACAO(cHTTP::$SESSION['msg'], cHTTP::$SESSION['tipoMsg'], cHTTP::$SESSION['tituloMsg']);
        if ($pReset) {
            unset(cHTTP::$SESSION['msg']);
            unset(cHTTP::$SESSION['tipoMsg']);
            unset(cHTTP::$SESSION['tituloMsg']);
        }
        return self::$notificacao;
    }

    public static function ConteudoEmJson($pReset = true) {
        $msg = array();
        $ret = array();
        $msg['textoMsg'] = cHTTP::StringOk(self::$notificacao->get_msg());
        $msg['tituloMsg'] = self::$notificacao->get_tituloMsg();
        $msg['tipoMsg'] = self::$notificacao->get_tipoMsg();
        $ret['msg'] = $msg;
        if ($pReset) {
            unset(cHTTP::$SESSION['msg']);
            unset(cHTTP::$SESSION['tipoMsg']);
            unset(cHTTP::$SESSION['tituloMsg']);
        }
        return $ret;
    }

}
