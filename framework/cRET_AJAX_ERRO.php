<?php

/**
 * Description of cRET_AJAX_ERRO
 *
 * @author leonardo
 */
class cRET_AJAX_ERRO extends cRET_AJAX_PADRAO{
	public function __construct($pMsg) {
		$this->codRet = self::codRet_ERRO ;
		$this->tituloMsg = "Erro!";
		$this->msg = $pMsg;
	}
}
