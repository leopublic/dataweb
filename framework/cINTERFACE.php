<?php

class cINTERFACE {

    public $msg;

    const PREFIXO_CAMPOS = 'CMP_';
    const ACAO_SALVAR = '__salvar';
    const CMP_ACAO = '__acao';
    const CMP_CLASSE = '__classe';
    const CMP_METODO = '__metodo';
    const CMP_EDICAO = '__edicao';
    const titPAGINA = 'pagina';
    const titSUB = 'sub';

    public static function iconPendente() {
        return self::icon('/imagens/icons/bullet_red.png', '');
    }

    public static function iconLiberada() {
        return self::icon('/imagens/icons/bullet_yellow.png', '');
    }

    public static function iconDisponivel() {
        return self::icon('/imagens/icons/bullet_green.png', '');
    }

    public static function iconFechada() {
        return self::icon('/imagens/icons/tick.png', '');
    }

    public static function icon($pArquivo, $title) {
        return '<img src="' . $pArquivo . '" style="vertical-align:middle" title="' . $title . '"/>';
    }

    public static function formEdicao($pTela = null, $pMultiPart = false) {
        $ret = "";
        $multipart = '';
        if ($pMultiPart) {
            $multipart = ' enctype="multipart/form-data"';
        }
        $ret .= '<form method="post" onsubmit="return FormValido();"' . $multipart . ' >';
        if (isset($pTela)) {
            $ret .= self::RenderizeCampo($pTela->mCampoCtrlEnvio);
            foreach ($pTela->mCampo as $campo) {
                if ($campo->mTipo == cCAMPO::cpHIDDEN) {
                    $ret .= self::RenderizeCampo($campo);
                }
            }
        }
        return $ret;
    }

    public static function RenderizeFormTag(cTEMPLATE $pTemplate, $pconteudo) {
        $ret = self::formEdicao($pTemplate, $pTemplate->mMultipart);
        $ret .= $pconteudo . '</form>';
        return $ret;
    }

    /**
     *
     * @param cTEMPLATE $pTemplate
     * @param boolean $pMultiPart
     * @return string
     */
    public static function formTemplate($pTemplate = null, $pMultiPart = false) {
        $ret = "";
        $multipart = '';
        if ($pMultiPart) {
            $multipart = ' enctype="multipart/form-data"';
        } else {
            $multipart = ' enctype="application/x-www-form-urlencoded"';
        }
        $ret .= '<form name="' . $pTemplate->mnome . '" id="' . $pTemplate->mnome . '" method="post" ' . $multipart . ' >';
        if (isset($pTemplate)) {
            foreach ($pTemplate->mCampo as $campo) {
                if ($campo->mTipo == cCAMPO::cpHIDDEN && !$campo->mEhArray) {
                    $ret .= self::RenderizeCampo($campo);
                }
            }
        }
        return $ret;
    }

    public static function jsValidacaoForm($pEdicao) {
        $ret = "<script>";
        $ret.= "function FormValido(){var msg = '';";
        for ($i = 0; $i < count($pEdicao->mCampo); $i++) {
            if (!$pEdicao->mCampo[$i]->mChave && $pEdicao->mCampo[$i]->mObrigatorio) {
                $ret.='if ($(\'#' . cINTERFACE::PREFIXO_CAMPOS . $pEdicao->mCampo[$i]->mNome . '\').val()==\'\') {msg = msg+\'\n- "' . $pEdicao->mCampo[$i]->Label() . '" é obrigatório(a).\';}' . "\n";
            }
        }
        $ret.="if (msg!=''){msg = 'Não foi possível salvar pois:'+msg;jAlert(msg);return false;}";
        $ret.="else{return true;}";
        $ret.="}";
        $ret.= "</script>";
        return $ret;
    }

    public static function formatarMascara($campo, $mascara){
        //retira formato
        $codigoLimpo = preg_replace("[' '-./ t]","",$campo);
        $mascaraLimpa = preg_replace("/[^#]/","",$mascara);
        $indice = -1;
        if (strlen($mascaraLimpa)> strlen($codigoLimpo))
        {	$tam = strlen($codigoLimpo);	}
        else
        {	$tam = strlen($mascara);	}
        $ret = '';

        for ($i=0; $i < $tam; $i++) {
            if ($mascara[$i]=='#')
            {	$ret .= $codigoLimpo[++$indice];}
            else
            {	$ret .= $mascara[$i];	}
        }
            //retorna o campo formatado
        $retorno = $ret;

        return $retorno;
    }

    public static function inputHidden($pnome, $pvalor) {
        return '<input type="hidden" name="' . $pnome . '" id="' . $pnome . '" value="' . $pvalor . '"/>';
    }

    public static function inputData($pnome, $pvalor) {
        if ($pvalor != "") {
            $node = explode("-", $pvalor);
            if (strlen($node[0]) == 4) {
                $pvalor = $node[2] . '/' . $node[1] . '/' . $node[0];
            }
        }
        return '<input type="text" name="' . $pnome . '" id="' . $pnome . '" value="' . $pvalor . '" onKeyUp="this.value=formatarData(this.value);" maxlenght="10" size="10" class="cmpDat"/>';
    }

    public static function inputTexto($pnome, $pvalor, $pqtdChar = '50', $ptam = '100%') {
        return '<input type="text" name="' . $pnome . '" id="' . $pnome . '" value="' . $pvalor . '" maxlength="' . $pqtdChar . '"/>';
    }

    public static function inputSimNao($pnome, $pvalor) {
        if ($pvalor > 0) {
            $checkedSim = ' checked ';
            $checkedNao = ' ';
        } else {
            $checkedNao = ' checked ';
            $checkedSim = ' ';
        }
        $x = '<input type="radio" name="' . $pnome . '" value="1" ' . $checedSim . '> Sim';
        $x.='<input type="radio" name="' . $pnome . '" value="0" ' . $checedNao . '> Não';
        return $x;
    }

    public static function CampoPadrao($nome, $valor, $descricao, $tipo, $acao, $extra) {
        $ret = "";
        $nome = 'name="' . $nome . '" id="' . $nome . '"';
        if (($acao == "V") && ($tipo != "H")) {
            $tipo = "V";
        }
        if ($tipo == "H") {  # Colocar so hidden
            $ret = '<input type="hidden" ' . $nome . ' value="' . $valor . '"/>';
        } else if ($tipo == "T") { # colocar text
            $ret = '<input type="text"   ' . $nome . ' value="' . $valor . '" ' . $extra . '/>';
        } else if ($tipo == "M") { # MOEDA
            $ret = '<input type="text"   ' . $nome . ' value="' . $valor . '" ' . $extra . ' onKeyUp="this.value=formatar(this.value);" style="text-align:right;">';
        } else if ($tipo == "D") { # campo data
            $ret = '<input type="text"   ' . $nome . ' value="' . $valor . '" onKeyUp="this.value=formatarData(this.value);" maxlenght="10" size="10" ' . $extra . ' class="data"/>';
        } else if ($tipo == "P") { # colocar text
            $ret = '<input type="password" ' . $nome . ' value="' . $valor . '" ' . $extra . '/>';
        } else if ($tipo == "R") { # colocar radio
            $ret = '<input type="radio" ' . $nome . ' value="' . $valor . '" ' . $extra . '/> ' . $descricao;
        } else if ($tipo == "S") { # colocar select (chega o option no valor
            $ret = '<select ' . $nome . ' ' . $extra . '>' . $valor . '</select>';
        } else if ($tipo == "A") { # colocar textarea
            $ret = '<textarea ' . $nome . ' ' . $extra . '>' . $valor . '</textarea>';
        } else if ($tipo == "C") { # colocar textarea
            $ret = '<checkbox ' . $nome . ' value="' . $valor . '" ' . $extra . '/>';
        } else { # colocar so a descricao
            $ret = $descricao;
        }
        if ($acao == "H") {
            $ret = '<input type="hidden" ' . $nome . ' value="' . $valor . '"/>';
        }
        return $ret;
    }

    public static function CampoFormValido($pPOST, $pNomeCampo) {
        if (isset($pPOST[$pNomeCampo])) {
            return $pPOST[$pNomeCampo];
        } else {
            return '';
        }
    }

    public static function GetValido($pGET, $pNomeCampo) {
        if (isset($pGET[$pNomeCampo])) {
            return $pGET[$pNomeCampo];
        } else {
            return '';
        }
    }

    public static function RenderizeLinhaEdicao($pCampo, $pReadonly = false, $pDif = '') {
        $class = "";
        if (!$pReadonly && !$pCampo->mReadonly) {
            if ($pCampo->mObrigatorio) {
                $class = ' class="obr"';
            }
        }
        if ($pCampo->mLarguraDupla != 0) {
            $largura = ' colspan="4"';
        } else {
            $largura = '';
        }
        return '<th' . $class . '>' . $pCampo->Label(cHTTP::getLang()) . '</th><td' . $largura . '>' . self::RenderizeCampo($pCampo, $pReadonly, $pDif) . '</td>';
    }

    /**
     *
     * @param cCAMPO $pCampo
     * @param type $pReadonly
     * @param type $pDif
     * @return string
     */
    public static function RenderizeFiltro($pCampo, $pReadonly = false, $pDif = '') {
        $pCampo->mNome = 'FILTRO_' . $pCampo->mNome;
        $ret = self::RenderizeCampo($pCampo, $pReadonly, $pDif);
//		if (substr($pCampo->mCampoBD,0,21) == 'NU_EMBARCACAO_PROJETO'){
//			$ret = '<div id="tdEmbarcacaoProjeto">'.$ret.'</div>';
//		}
        return $ret;
    }

    public static function RenderizeCampo($pCampo, $pReadonly = false, $pDif = '') {
        // Determina a classe
        if ($pCampo->mClasse == '') {
            $xClasse = "cmp";
        } else {
            $xClasse = $pCampo->mClasse;
        }
        if ($pCampo->mlinkMte) {
            $xClasse .= ' cmpMte';
        }

        $xNome = self::Nome($pCampo->mNome . $pDif, $pCampo->mEhArray);

        if ($pReadonly || $pCampo->mReadonly) {
            if (cHTTP::$MIME == cHTTP::mmXLS) {
                if ($pCampo->mTipo == cCAMPO::cpHIDDEN || $pCampo->mTipo == cCAMPO::cpCHAVE) {

                } elseif ($pCampo->mTipo == cCAMPO::cpSIMNAO) {
                    $checked = '';
                    if ($pCampo->mValor) {
                        $xCampo = 'sim';
                    } else {
                        $xCampo = 'não';
                    }
                } else {
                    $xCampo = cBANCO::BancoParaTela($pCampo);
                    if (trim($xCampo) == '') {
                        $xCampo = "&nbsp;";
                    }
                }
            } else {
                if ($pCampo->mTipo == cCAMPO::cpHIDDEN || $pCampo->mTipo == cCAMPO::cpCHAVE) {
                    $xCampo = '<input type="hidden" ' . $xNome . ' value="' . $pCampo->mValor . '"/>';
                } elseif ($pCampo->mTipo == cCAMPO::cpSIMNAO) {
                    $checked = '';
                    if ($pCampo->mValor) {
                        $checked = 'checked';
                    }

                    $xCampo = '<input type="checkbox" ' . $xNome . ' class="' . $xClasse . '" ' . $checked . ' value="1" disabled />';
                } elseif ($pCampo->mTipo == cCAMPO::cpMEMO) {
                    $pCampo->mValor = str_replace("\n", "<br/>", $pCampo->mValor);
                    $xCampo = cBANCO::BancoParaTela($pCampo);
                } else {
                    $classe = $pCampo->mClasse;
                    if (strstr($classe, "processo")) {
                        $pCampo->mValor = formatarMascara(preg_replace("/[^0-9]/", "", $pCampo->mValor), '#####.######/####-##');
                    }
                    $xCampo = cBANCO::BancoParaTela($pCampo);
                }
            }
        } else {
            $xCampo = "";
            if ($pCampo->mDisabled) {
                $disable = ' disabled="disabled"';
            } else {
                $disable = '';
            }

            if (strstr($pCampo->mClasse, 'editavel')) {
                $parametros = ' data-parametros=\'' . $pCampo->get_parametros() . '\'  data-valor-orig="' . $pCampo->mValor . '"';
            } else {
                $parametros = '';
            }

            switch ($pCampo->mTipo) {
                case cCAMPO::cpHIDDEN :
                case cCAMPO::cpCHAVE :
                    $xCampo = '<input type="hidden" ' . $xNome . ' value="' . $pCampo->mValor . '" ' . $disable . '/>';
                    break;
                case cCAMPO::cpCODIGO :
                    $xCampo = '<input type="text" ' . $xNome . ' value="' . $pCampo->mValor . '" class="cmpCod ' . $xClasse . '" ' . $disable . $parametros . '/>';
                    break;
                case cCAMPO::cpDATA :
                    $xCampo = '<input type="text" ' . $xNome . ' value="' . $pCampo->mValor . '" class="cmpDat ' . $xClasse . '" ' . $disable . $parametros . '/>';
                    break;
                case cCAMPO::cpDATA_ONLY :
                    $xCampo = '<input type="text" ' . $xNome . ' value="' . $pCampo->mValor . '" class="dataOnly ' . $xClasse . '" ' . $disable . $parametros . '/>';
                    break;
                case cCAMPO::cpDATAHORA :
                    $xCampo = '<input type="text" ' . $xNome . ' value="' . $pCampo->mValor . '" class="cmpDath ' . $xClasse . '" ' . $disable . $parametros . '/>';
                    break;
                case cCAMPO::cpTEXTO :
                    $xCampo = '<input type="text" ' . $xNome . ' value="' . $pCampo->mValor . '" class="cmpTxt ' . $xClasse . '" ' . $disable . $parametros . '/>';
                    break;
                case cCAMPO::cpSENHA :
                    $xCampo = '<input type="password" ' . $xNome . ' value="' . $pCampo->mValor . '" class="cmpCod ' . $xClasse . '" ' . $disable . '/>';
                    break;
                case cCAMPO::cpVALOR_DIN_DOLAR :
                    $xCampo = '<input type="text" ' . $xNome . ' value="' . $pCampo->mValor . '" class="cmpDolar ' . $xClasse . '" ' . $disable . $parametros . '/>';
                    break;
                case cCAMPO::cpVALOR_DIN_REAL :
                    $xCampo = '<input type="text" ' . $xNome . ' value="' . $pCampo->mValor . '" class="cmpReal ' . $xClasse . '" ' . $disable . $parametros . '/>';
                    break;
                case cCAMPO::cpVALOR :
                    $xCampo = '<input type="text" ' . $xNome . ' value="' . $pCampo->mValor . '" class="cmpVal ' . $xClasse . '" ' . $disable . $parametros . '/>';
                    break;
                case cCAMPO::cpMEMO :
                    $xCampo = '<textarea ' . $xNome . ' class="cmpMEMO ' . $xClasse . '" rows="5" ' . $disable . $parametros . '>' . $pCampo->mValor . '</textarea>';
                    break;
                case cCAMPO::cpSELETOR :
                    $checked = '';
                    if ($pCampo->mValor) {
                        $checked = 'checked';
                    }
                    $xCampo = '<input type="checkbox" ' . $xNome . ' class="' . $xClasse . '" ' . $checked . ' value="' . $pCampo->get_valorChave() . '" style="margin:0px;" ' . $disable . '/>';
                    break;
                case cCAMPO::cpSIMNAO :
                    if ($pCampo->valorCustomizado) {
                        $valor = $pCampo->mValor;
                        if ($pCampo->checked){
                            $checked = 'checked';
                        } else {
                            $checked = '';
                        }

                    } else {
                        $valor = 1;
                        if ($pCampo->mValor) {
                            $checked = 'checked';
                        } else {
                            $checked = '';
                        }
                    }
                    $xCampo = '<input type="checkbox" ' . $xNome . ' class="' . $xClasse . '" ' . $checked . ' value="' . $valor . '" ' . $disable . $parametros . '/>';
                    break;
                case cCAMPO::cpSIMNAO_LITERAL :
                    $checkedSim = '';
                    $checkedNao = '';
                    if ($pCampo->mValor != '') {
                        if ($pCampo->mValor == 1) {
                            $checkedSim = 'checked';
                        } elseif ($pCampo->mValor == 0) {
                            $checkedNao = 'checked';
                        }
                    }

                    $xCampo = '<input type="radio" ' . $xNome . ' class="' . $xClasse . '" ' . $checkedSim . ' value="1" style="width:auto;" ' . $disable . '/>Sim';
                    $xCampo .= '&nbsp;&nbsp;<input type="radio" ' . $xNome . ' class="' . $xClasse . '" ' . $checkedNao . ' value="0"  style="width:auto;" ' . $disable . '/>Não';
                    break;
                case cCAMPO::cpCHAVE_ESTR :
                    $xCampo = self::RenderizeCombo($pCampo, $pDif);
                    break;
                case cCAMPO::cpCOMBO_GERAL:
                    $xCampo = $pCampo->HTML($xNome);
                    break;
                case cCAMPO::cpLINK_EXTERNO:
                    $xCampo = '<a href="' . $pCampo->mLinkExterno . '?' . $pCampo->mParametros . '">' . $pCampo->mValor . '</a>';
                    break;
                case cCAMPO::cpFILE:
                    $xCampo = '<input ' . $xNome . ' type="file" style="width:auto" value="...">';
                    break;
                default:
                    throw new Exception("Renderização do campo " . $pCampo->mCampoBD . " para o tipo (" . $pCampo->mNome . '->' . $pCampo->mTipo . ") informado ainda não implementada");
            }
        }
        if ($pCampo->mlinkMte && $pCampo->mValor != '') {
            $xCampo .= '&nbsp;<input type="button" value="MTE" onClick="javascript:veracomp(' . $pCampo->mValor . ',\'MTE\',\'' . preg_replace("/[^0-9]/", "", $pCampo->mValor) . '\');"  class="botaoLink" style="width:auto;padding:0;padding-top:0;margin:0;padding-left:1px;padding-right:1px;" title="Clique para ver o processo no Minist&eacute;rio do Trabalho" />';
        }
        if ($pCampo->mlinkCertidao && $pCampo->mValor != '') {
            $xCampo .= '&nbsp;<input type="button" value="Certidão" onClick="javascript:abreCertidao(\''. formatarMascara(preg_replace("/[^0-9]/", "", $pCampo->mValor), '#####.######/####-##') . '\');" class="botaoLink"  style="width:auto;padding:0;padding-top:0;margin:0;padding-left:1px;padding-right:1px;" title="Clique para ver a certidão de trâmite" />';
        }
        if ($pCampo->mlinkMJ && $pCampo->mValor != '') {
            $xCampo .= '&nbsp;<input type="button" value="MJ" onClick="javascript:AbrePagina(\'MJ\', \'http://portal.mj.gov.br/EstrangeiroWEB/resultado_prorrogacao.asp?CodigoProcesso=' . formatarMascara(preg_replace("/[^0-9]/", "", $pCampo->mValor), '#####.######/####-##') . '\');" class="botaoLink"  style="width:auto;padding:0;padding-top:0;margin:0;padding-left:1px;padding-right:1px;" title="Clique para ver o processo no Minist&eacute;rio da Justiça" />';
        }

        return $xCampo;
    }

    public static function Nome($pNome, $pArray = false) {
        if ($pArray) {
            $array = '[]';
        } else {
            $array = '';
        }
        $xNome = self::PREFIXO_CAMPOS . $pNome . $array;
        return ' id="' . $xNome . '" name="' . $xNome . '" ';
    }

    /**
     *
     * @param cCAMPO $pCampo
     * @param type $pDif
     * @return type
     */
    public static function RenderizeCombo($pCampo, $pDif = '') {
        $ret = $pCampo->mCombo->HTML($pCampo->mNome . $pDif, $pCampo->mValor, $pCampo->mDisabled, $pCampo->mClasse);
        return $ret;
    }

    public static function RecupereValoresEdicao(&$pEdicao, $pPost, $pGet = null) {
        $pDif = '';
        if (isset($pPost['IdentificadorCampos'])) {
            $pDif = $pPost['IdentificadorCampos'];
        }

        if (isset($pPost[self::PREFIXO_CAMPOS . 'acao'])) {
            $pEdicao->mAcao = $pPost[self::PREFIXO_CAMPOS . 'acao'];
        }

//		if ($pPost['CMP___edicao'] == $pEdicao->mno_edicao){
        foreach ($pEdicao->mCampo as &$xCampo) {
            if ($xCampo->mTipo == cCAMPO::cpSIMNAO) {
                if (isset($pPost[self::PREFIXO_CAMPOS . $xCampo->mNome]) && $pPost[self::PREFIXO_CAMPOS . $xCampo->mNome] == "1") {
                    $xCampo->mValor = "1";
                } else {
                    $xCampo->mValor = "0";
                }
            } elseif ($xCampo->mTipo == cCAMPO::cpSIMNAO_LITERAL) {
                if (isset($pPost[self::PREFIXO_CAMPOS . $xCampo->mNome])) {
                    $xCampo->mValor = $pPost[self::PREFIXO_CAMPOS . $xCampo->mNome];
                }
            } else {
                if (isset($pPost[self::PREFIXO_CAMPOS . $xCampo->mNome . $pDif])) {
                    $xCampo->mValor = stripslashes($pPost[self::PREFIXO_CAMPOS . $xCampo->mNome . $pDif]);
                } elseif (isset($pGet[self::PREFIXO_CAMPOS . $xCampo->mNome . $pDif])) {
                    $xCampo->mValor = stripslashes($pGet[self::PREFIXO_CAMPOS . $xCampo->mNome . $pDif]);
                } elseif (isset($pGet[$xCampo->mNome . $pDif])) {
                    $xCampo->mValor = stripslashes($pGet[$xCampo->mNome . $pDif]);
                }
            }
        }
//		}
    }

    public static function CampoEnviado($pPost, $pCampo, $pDif = '') {
        return isset($pPost[self::PREFIXO_CAMPOS . $pCampo->mNome . $pDif]);
    }

    public static function RenderizeEdicao($pTela, $pColunas = 2) {
        $btr = "";
        $par = false;
        $painelAnt = '';
        $ret = '';
        $ret .= '<table class="edicao dupla">';
        $ret .= '<col width="18%"></col>';
        $ret .= '<col width="28%"></col>';
        $ret .= '<col width="5%"></col>';
        $ret .= '<col width="18%"></col>';
        $ret .= '<col width="28%"></col>';
        foreach ($pTela->mCampo as $campo) {
            if (!$campo->mChave && $campo->mTipo != cCAMPO::cpHIDDEN && $campo->mVisivel) {
                if (!$par) {
                    $linha = '<tr>';
                } else {
                    $linha .= '<td>&#160;</td>';
                }
                $linha .= cINTERFACE::RenderizeLinhaEdicao($campo, false);
                if ($par) {
                    $linha .='</tr>';
                    $ret.= $linha . "\n";
                }
                $par = !$par;
            }
        }
        if ($par) {
            $ret.= $linha . '<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>' . "\n";
        }
        return $ret;
    }

    public static function RenderizeEdicaoComPaineis($pTela, $pColunas = 2, $pDif = '') {
        $btr = "";
        $par = false;
        $painelAnt = '';
        $ret = '';
        $ret .= '<table class="edicao dupla">';
        $ret .= '<col width="18%"></col>';
        $ret .= '<col width="28%"></col>';
        $ret .= '<col width="5%"></col>';
        $ret .= '<col width="18%"></col>';
        $ret .= '<col width="28%"></col>';
        $painel = '';
        $linha = '';
        $estiloPrimeira = ' style="border-top:none;"';
        foreach ($pTela->mCampo as $campo) {
            //error_log("Montando campo:".$campo->mNome." tipo=".$campo->mTipo);
            if (!$campo->mChave && $campo->mTipo != cCAMPO::cpHIDDEN && $campo->mVisivel) {
                if ($campo->mPainel != $painel && $campo->mPainel != '') {
                    if ($linha != '') {
                        if ($par) {
                            $linha .= '<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>';
                            $ret .= $linha;
                        }
                        $linha = '';
                        $par = false;
                    }
                    $ret .= '<tr class="subTitulo"><td colspan="5"' . $estiloPrimeira . '>' . $campo->mPainel . '</td></tr>' . "\n";
                    $estiloPrimeira = "";
                    $painel = $campo->mPainel;
                }
                if ($campo->mLarguraDupla != 0) {
                    if ($par) {
                        $linha .= '<td>&#160;</td><td>&nbsp;</td><td>&nbsp;</td>';
                        $ret .= $linha;
                        $linha = '<tr>';
                    } else {
                        $linha = '<tr>';
                        $par = true;
                    }
                } else {
                    if (!$par) {
                        $linha = '<tr>';
                    } else {
                        $linha .= '<td>&#160;</td>';
                    }
                }
                $linha .= cINTERFACE::RenderizeLinhaEdicao($campo, false, $pDif);
                if ($par) {
                    $linha .='</tr>';
                    $ret.= $linha . "\n";
                }
                $par = !$par;
            }
        }
        if ($par) {
            $ret.= $linha . '<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>' . "\n";
        }
        return $ret;
    }

    /**
     *
     * @param cTEMPLATE $pTemplate
     * @param type $pColunas
     * @param type $pDif
     * @return string
     */
    public static function RenderizeTemplateEdicao($pTemplate, $pDif = '') {
        $btr = "";
        $par = false;
        $painelAnt = '';
        $ret = '';
        if ($pTemplate->qtd_colunas == 2) {
            $ret .= '<table class="edicao dupla">';
            $ret .= '<col width="18%"></col>';
            $ret .= '<col width="28%"></col>';
            $ret .= '<col width="5%"></col>';
            $ret .= '<col width="18%"></col>';
            $ret .= '<col width="28%"></col>';
            $painel = '';
            $linha = '';
            $estiloPrimeira = ' style="border-top:none;"';
            foreach ($pTemplate->mCampo as $campo) {
                //error_log("Montando campo:".$campo->mNome." tipo=".$campo->mTipo);
                if (!$campo->mChave && $campo->mTipo != cCAMPO::cpHIDDEN && $campo->mVisivel) {
                    if ($campo->mPainel != $painel && $campo->mPainel != '') {
                        if ($linha != '') {
                            if ($par) {
                                $linha .= '<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>';
                                $ret .= $linha;
                            }
                            $linha = '';
                            $par = false;
                        }
                        $ret .= '<tr class="subTitulo"><td colspan="5"' . $estiloPrimeira . '>' . $campo->mPainel . '</td></tr>' . "\n";
                        $estiloPrimeira = "";
                        $painel = $campo->mPainel;
                    }
                    if ($campo->mLarguraDupla != 0) {
                        if ($par) {
                            $linha .= '<td>&#160;</td><td>&nbsp;</td><td>&nbsp;</td>';
                            $ret .= $linha;
                            $linha = '<tr>';
                        } else {
                            $linha = '<tr>';
                            $par = true;
                        }
                    } else {
                        if (!$par) {
                            $linha = '<tr>';
                        } else {
                            $linha .= '<td>&#160;</td>';
                        }
                    }
                    $linha .= self::RenderizeLinhaEdicao($campo, false, $pDif);
                    if ($par) {
                        $linha .='</tr>';
                        $ret.= $linha . "\n";
                    }
                    $par = !$par;
                }
                if (is_object($campo->campoPai)) {
                    cHTTP::$flAssociarEmpresaEmbProjAutomaticamente = false;
                    cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieCombos($pTemplate, $campo->campoPai, $campo));
                }
            }
            if ($par) {
                $ret.= $linha . '<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>' . "\n";
            }
        } else {
            $ret .= '<table class="edicao">';
            $ret .= '<col width="18%"></col>';
            $ret .= '<col width="28%"></col>';
            $painel = '';
            $linha = '';
            $estiloPrimeira = ' style="border-top:none;"';
            foreach ($pTemplate->mCampo as $campo) {
                //error_log("Montando campo:".$campo->mNome." tipo=".$campo->mTipo);
                if (!$campo->mChave && $campo->mTipo != cCAMPO::cpHIDDEN && $campo->mVisivel) {
                    if ($campo->mPainel != $painel && $campo->mPainel != '') {
                        $ret .= '<tr class="subTitulo"><td colspan="2"' . $estiloPrimeira . '>' . $campo->mPainel . '</td></tr>' . "\n";
                        $estiloPrimeira = "";
                        $painel = $campo->mPainel;
                    }
                    $ret.= '<tr>' . self::RenderizeLinhaEdicao($campo, false, $pDif) . '</tr>' . "\n";
                }
                if (is_object($campo->campoPai)) {
                    cHTTP::$flAssociarEmpresaEmbProjAutomaticamente = false;
                    cHTTP::AdicionarScript(cJAVASCRIPTS::ScriptAssocieCombos($pTemplate, $campo->campoPai, $campo));
                }
            }
        }
        return $ret;
    }

    public static function RenderizeForm($pTela, $pColunas = 2) {
        $btr = "";
        $par = false;
        $painelAnt = '';
        $ret = '';
        $ret .= self::formEdicao($pTela);

        $ret .= '<table class="edicao dupla">';
        $ret .= '<col width="18%"></col>';
        $ret .= '<col width="28%"></col>';
        $ret .= '<col width="5%"></col>';
        $ret .= '<col width="18%"></col>';
        $ret .= '<col width="28%"></col>';
        foreach ($pTela->mCampo as $campo) {
            if (!$campo->mChave && $campo->mVisivel) {
                if (!$par) {
                    $linha = '<tr>';
                } else {
                    $linha .= '<td>&#160;</td>';
                }
                $linha .= cINTERFACE::RenderizeLinhaEdicao($campo, false);
                if ($par) {
                    $linha .='</tr>';
                    $ret.= $linha . "\n";
                }
                $par = !$par;
            }
        }
        if ($par) {
            $ret.= $linha . '<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>' . "\n";
        }
        return $ret;
    }

    /**
     * Atualiza os valores dos atributos de uma classe a partir dos campos de uma tela (cEDICAO)
     * @param cEDICAO $pedicao Tela de edição
     * @param qualquerUma $pclasse Qualquer classe de objeto
     * @return null
     * @static
     */
    public static function MapeiaEdicaoClasse($pedicao, &$pclasse) {
        foreach ($pedicao->mCampo as $campo) {
            $nome = 'm' . $campo->mNome;
            if (property_exists($pclasse, $nome)) {
                $pclasse->$nome = $campo->mValor;
            } else {
                $nome = $campo->mNome;
                if (property_exists($pclasse, $nome)) {
                    $pclasse->$nome = $campo->mValor;
                }
            }
        }
    }

    public static function MapeiaEdicaoClasseUm($pedicao, &$pclasse, $ppropriedade_edicao, $ppropriedade_classe) {
        if ($ppropriedade_classe == '') {
            $ppropriedade_classe = $ppropriedade_edicao;
        }
        $ppropriedade_classe = 'm' . $ppropriedade_classe;
        $pclasse->$propriedade_classe = $pedicao->mCampoNome[$ppropriedade_edicao];
    }

    /**
     * Faz o processamento inicial do post, redirecionando para a classe processadora, quando disponível.
     * @param array $pPOST
     * @param array $pGET
     * @param array $pFILES
     * @param array $pSESSION
     * @return string Mensagem com o resultado da operação
     * @static
     */
    public static function ProcessePost($pPOST, $pGET, $pFILES, &$pSESSION) {
        // Recupera qual foi a tela enviada e carrega a tela
        if (isset($pPOST[self::PREFIXO_CAMPOS . self::CMP_EDICAO])) {
            $tela = new cEDICAO();
            $tela->CarregarConfiguracao($pPOST[self::PREFIXO_CAMPOS . self::CMP_EDICAO]);
            self::RecupereValoresEdicao($tela, $pPOST);
            $nomeMetodo = 'Post' . $tela->mno_edicao;
            if (method_exists($tela->mClasseProcessadora, $nomeMetodo)) {
                eval('$msg = ' . $tela->mClasseProcessadora . '::Post' . $tela->mno_edicao . '($tela, $pPOST, $pFILES, $pSESSION);');
                return $msg;
            } else {
                return 'Método processador (' . $tela->mClasseProcessadora . '->' . $nomeMetodo . ') não encontrado';
            }
        }
    }

    /**
     * Renderiza ações no formato antigo
     * @deprecated
     * @return string código html
     * @static
     */
    public static function RenderizeAcoes($pTela) {
        $x = '';
        if (count($pTela->mAcoes) > 0) {
            foreach ($pTela->mAcoes as $xAcao) {
                $onclick = "document.forms['" . $pTela->mno_edicao . "']['CMP_acao'].value='" . $xAcao->mCodigo . "';document.forms['" . $pTela->mno_edicao . "'].submit();";
                if ($xAcao->mMsgConfirmacao != '') {
                    $onclick = "jConfirm('" . $xAcao->mMsgConfirmacao . "', 'Atenção', function(r) { if (r){" . $onclick . "} });";
                }
                $x .= '<input type="button" value="' . $xAcao->Label() . '" onclick="' . $onclick . '" title="' . $xAcao->mTitulo . '" />';
            }
            $x = '<div class="barraBotoesRodape">' . $x . '</div>';
        }
        return $x;
    }

    /**
     * Gera o código HTML para uma instância de cACAO()
     * @param cACAO $pAcao
     * @return string Código html
     * @static
     */
    public static function RenderizeAcao($pAcao) {
        switch ($pAcao->mTipo) {
            case cACAO::ACAO_JAVASCRIPT:
                $ret = '<a';
                $ret .= ' href="#"';
                $ret .= ' title="' . $pAcao->mTitulo . '"';
                $ret .= ' class="' . $pAcao->mno_classe . '"';
                $ret .= ' onClick="javascript:' . $pAcao->mOnClick . ';return false;"';
                $ret .= '>';
                if (substr($pAcao->mLabel, 0, 1) == "/") {
                    $ret .= '<img src="' . $pAcao->mLabel . '" />';
                } else {
                    $ret .= $pAcao->mLabel;
                }
                $ret .= '</a>';
                break;
            // case cACAO::ACAO_JAVASCRIPT:
            // 	$ret = '<input';
            // 	if (substr($pAcao->mLabel, 0, 1) == "/" ){
            // 		$ret .= ' type="image"';
            // 		$ret .= ' src="'.$pAcao->mLabel.'"';
            // 	}
            // 	else{
            // 		$ret .= ' type="button"';
            // 		$ret .= ' value="'.$pAcao->mLabel.'"';
            // 	}
            // 	$ret .= ' value="'.$pAcao->mLabel.'"';
            // 	$ret .= ' title="'.$pAcao->mTitulo.'"';
            // 	$ret .= ' class="'.$pAcao->mno_classe.'"';
            // 	$ret .= ' onClick="javascript:'.$pAcao->mOnClick.';return false;"';
            // 	$ret .= '/>';
            // 	break;
            case cACAO::ACAO_METODO_POPUP:
                $ret = '<a';
                $ret .= ' href="#"';
//				$ret = '<input';
//				if (substr($pAcao->mLabel, 0, 1) == "/" ){
//					$ret .= ' type="image"';
//					$ret .= ' src="'.$pAcao->mLabel.'"';
//				}
//				else{
//					$ret .= ' type="button"';
//					$ret .= ' value="'.$pAcao->mLabel.'"';
//				}
                $ret .= ' title="' . $pAcao->mTitulo . '"';
                $ret .= ' class="' . $pAcao->mno_classe . '"';
                $ret .= ' onClick="javascript:METODO_POPUP(this, \'' . $pAcao->mControle . '\', \'' . $pAcao->mMetodo . '\', \'' . $pAcao->mParametros . '\', \'' . $pAcao->mMsgConfirmacao . '\', \'' . $pAcao->mMsgConfirmacaoTitulo . '\');return false;" ';
                $ret .= '>';
                if (substr($pAcao->mLabel, 0, 1) == "/") {
                    $ret .= ' <img src="' . $pAcao->mLabel . '" />';
                } else {
                    $ret .= $pAcao->mLabel;
                }
                $ret .= '</a>';
                break;
            case cACAO::ACAO_METODO_POPUP_POST:
                $ret = '<input';
                $ret .= ' type="button"';
                $ret .= ' value="' . $pAcao->mLabel . '"';
                $ret .= ' title="' . $pAcao->mTitulo . '"';
                $ret .= ' class="' . $pAcao->mno_classe . '"';
                $ret .= ' onClick="javascript:METODO_POPUP_POST(this, \'' . $pAcao->mCodigo . '\', \'' . $pAcao->mMsgConfirmacao . '\', \'' . $pAcao->mMsgConfirmacaoTitulo . '\');"';
                $ret .= '/>';
                break;
            case cACAO::ACAO_FORM:
                $ret = '<input';
                if (substr($pAcao->mLabel, 0, 1) == "/") {
                    $ret .= ' type="image"';
                    $ret .= ' src="' . $pAcao->mLabel . '"';
                } else {
                    $ret .= ' type="button"';
                    $ret .= ' value="' . $pAcao->mLabel . '"';
                }
                $ret .= ' title="' . $pAcao->mTitulo . '"';
                $ret .= ' class="' . $pAcao->mno_classe . '"';
                $ret .= ' onClick="javascript:ConfirmaAcao(this, \'' . $pAcao->mCodigo . '\', \'' . $pAcao->mMsgConfirmacao . '\', \'' . $pAcao->mMsgConfirmacaoTitulo . '\', \'' . $pAcao->mParametros . '\');return false;"';
                $ret .= '/>';
                break;
            case cACAO::ACAO_SUBMIT_ORDER:
                $ret = '<a href="#"';
                $ret .= ' title="' . $pAcao->mTitulo . '"';
                $ret .= ' class="' . $pAcao->mno_classe . '"';
                $ret .= ' onClick="javascript:EnviaOrdenacao(this, \'' . $pAcao->mCampoOrdenacao . '\');"';
                $ret .= '>';
                if (substr($pAcao->mLabel, 0, 8) == '/imagens') {
                    $ret .= '<img src="' . $pAcao->mLabel . '" />';
                } else {
                    $ret .= $pAcao->mLabel;
                }
                $ret .= '</a>';
                break;
            case cACAO::ACAO_LINK:
                $ret = '<a href="' . $pAcao->getLinkComParametros() . '" ';
                $ret .= ' title="' . $pAcao->mTitulo . '"';
                $ret .= ' class="' . $pAcao->mno_classe . '"';
                $ret .= ' target="' . $pAcao->mTarget . '"';
                $ret .= '>';
                if (substr($pAcao->mLabel, 0, 8) == '/imagens') {
                    $ret .= '<img src="' . $pAcao->mLabel . '" />';
                } else {
                    $ret .= $pAcao->mLabel;
                }
                $ret .= '</a>';
                break;
            case cACAO::ACAO_SUBMIT_AJAX:
                $ret = '<input';
                $ret .= ' type="button"';
                $ret .= ' value="' . $pAcao->mLabel . '"';
                $ret .= ' title="' . $pAcao->mTitulo . '"';
                $ret .= ' class="' . $pAcao->mno_classe . '"';
                $ret .= ' onclick="javascript:SubmitAjax(this,\'' . $pAcao->mControle . '\', \'' . $pAcao->mMetodo . '\', \'' . $pAcao->mCodigo . '\', \'' . $pAcao->mParametros . '\', \'formPopup\' );"';
                $ret .= '/>';
                break;
            case cACAO::ACAO_LINK_CONTROLER_METODO:
                if ($pAcao->mpagina == ''){
                    if (cHTTP::$SESSION["usuarioPerfil"] == '10') {
                        $pagina = 'carregueCliente.php';
                    } else {
                        $pagina = 'carregueComMenu.php';
                    }
                } else {
                    $pagina = $pAcao->mpagina;
                }
                switch ($pAcao->mModo) {
                    case cACAO_LINK_CONTROLER_METODO::modoREDIRECT:
                        $ret = '<a';
                        $ret .= ' href="#"';
                        $ret .= ' onclick="javascript:ConfirmaRedirect(\'' . $pAcao->mMsgConfirmacao . '\',\'' . $pAcao->mMsgConfirmacaoTitulo . '\',\'/paginas/' . $pagina . '?controller=' . $pAcao->mControle . '&metodo=' . $pAcao->mMetodo . $pAcao->mParametros . '\'); return false;"';
                        $ret .= ' title="' . $pAcao->mTitulo . '"';
                        $ret .= ' class="' . $pAcao->mno_classe . '"';
                        $ret .= ' target="' . $pAcao->mTarget . '"';

                        $ret .= ">";
                        if (substr($pAcao->mLabel, 0, 1) == "/") {
                            //					$ret .= ' type="image"';
                            $ret .= '<img src="' . $pAcao->mLabel . '" />';
                        } else {
                            //					$ret .= ' type="button"';
                            $ret .= $pAcao->mLabel;
                        }
                        $ret .= '</a>';
                        break;
                    case cACAO_LINK_CONTROLER_METODO::modoREDIRECT_SEM_MENU:
                        $pagina = "carregueSemMenu.php";
                        $ret = '<a';
                        $ret .= ' href="#"';
                        $ret .= ' onclick="javascript:ConfirmaRedirect(\'' . $pAcao->mMsgConfirmacao . '\',\'' . $pAcao->mMsgConfirmacaoTitulo . '\',\'/paginas/' . $pagina . '?controller=' . $pAcao->mControle . '&metodo=' . $pAcao->mMetodo . $pAcao->mParametros . '\'); return false;"';
                        $ret .= ' title="' . $pAcao->mTitulo . '"';
                        $ret .= ' class="' . $pAcao->mno_classe . '"';
                        $ret .= ' target="' . $pAcao->mTarget . '"';

                        $ret .= ">";
                        if (substr($pAcao->mLabel, 0, 1) == "/") {
                            //					$ret .= ' type="image"';
                            $ret .= '<img src="' . $pAcao->mLabel . '" />';
                        } else {
                            //					$ret .= ' type="button"';
                            $ret .= $pAcao->mLabel;
                        }
                        $ret .= '</a>';
                        break;
                    case cACAO_LINK_CONTROLER_METODO::modoPOST:
                        break;
                    case cACAO_LINK_CONTROLER_METODO::modoAJAX:
                        $ret = '<a';
                        $ret .= ' title="' . $pAcao->mTitulo . '"';
                        $ret .= ' class="' . $pAcao->mno_classe . '"';
                        $ret .= ' onclick="javascript:CarregueServico(this,\'' . $pAcao->mControle . '\', \'' . $pAcao->mMetodo . '\', \'' . $pAcao->mCodigo . '\', \'' . $pAcao->mParametros . '\', \'' . $pAcao->mContainerDestino . '\', \'' . $pAcao->mMsgConfirmacao . '\', \'' . $pAcao->mMsgConfirmacaoTitulo . '\' );"';
                        $ret .= ' href="#"';
                        $ret .= ">";
                        if (substr($pAcao->mLabel, 0, 1) == "/") {
                            $ret .= '<img src="' . $pAcao->mLabel . '" />';
                        } else {
                            $ret .= $pAcao->mLabel;
                        }
                        $ret .= '</a>';
                        /*
                          if (substr($pAcao->mLabel, 0, 1) == "/" ){
                          $ret .= ' type="image"';
                          $ret .= ' src="'.$pAcao->mLabel.'"';
                          }
                          else{
                          $ret .= ' type="button"';
                          $ret .= ' value="'.$pAcao->mLabel.'"';
                          }
                          $ret .= '/>';
                         */
                        break;
                }
                break;
            case cACAO::ACAO_LINK_ABA:
                $ret = '<a href="/paginas/carregueComMenu.php?controller=' . $pAcao->mController . '&metodo=' . $pAcao->mMetodo . '&' . $pAcao->mParametros . '"';
                $ret .= ' title="' . $pAcao->mTitulo . '"';
                $ret .= ' class="' . $pAcao->mno_classe . '"';
                $ret .= '>';
                if (substr($pAcao->mLabel, 0, 8) == '/imagens') {
                    $ret .= '<img src="../' . $pAcao->mLabel . '" />';
                } else {
                    $ret .= $pAcao->mLabel;
                }
                $ret .= '</a>';
                break;
        }
        return $ret;
    }

    /**
     * Renderiza barra de ações para titulo
     * @param array() $pAcoes	Array das ações da tela
     * @return html com o div de ácões em forma de botão
     * @static
     */
    public static function RenderizeBarraAcoesTitulo($pAcoes) {
        return self::RenderizeBarraAcoes($pAcoes, "barraBotoesTitulo");
    }

    /**
     * Renderiza barra de ações para rodape
     * @param array() $pAcoes	Array das ações da tela
     * @return html com o div de ácões em forma de botão
     * @static
     */
    public static function RenderizeBarraAcoesRodape($pAcoes) {
        return self::RenderizeBarraAcoes($pAcoes, "barraBotoesRodape");
    }

    public static function RenderizeBarraAcoesSemEstilo($pAcoes) {
        return self::RenderizeBarraAcoes($pAcoes, "");
    }

    /**
     * Renderiza barra de ações para forms. Melhor utilizada quando chamada pelos métodos RenderizeBarraAcoesTitulo ou RenderizeBarraAcoesRodape
     * @param array() $pAcoes	Array das ações da tela
     * @param string $pClasse	Indica o tipo de barra a ser usada: "Rodape" ou "Titulo"
     * @return html com o div de ácões em forma de botão
     * @static
     */
    public static function RenderizeBarraAcoes($pAcoes, $pClasse) {
        $ret = '';
        if (count($pAcoes > 0)) {
            foreach ($pAcoes as $acao) {
                if ($acao->mVisivel) {
                    $ret.= self::RenderizeAcao($acao);
                }
            }
        }
        if ($ret != '') {
            if ($pClasse != '') {
                $ret = '<div class="' . $pClasse . '">' . $ret . '</div>';
            }
        }
        return $ret;
    }

    /**
     * Formata mensagem de erro para exibição correta via javascript
     * @param $pMsg Mensagem a ser formatada
     * @return string Mensagem codificada corretamente
     * @static
     */
    public static function MsgJavascriptOk($pMsg) {
        $msg = $pMsg;
        $msg = str_replace("\n", '\n', $msg);
        $msg = str_replace('"', '\"', $msg);
        return $msg;
    }

    public static function GeraOutputXML($pRetorno, $pConteudo, $pMsg, $pTituloMsg = 'Atenção', $pTituloJanela) {
        return '<?xml version="1.0" encoding="UTF-8"?>
				 <ajax>
					 <retorno>' . $pRetorno . '</retorno>
					 <conteudo>' . htmlentities($pConteudo) . '</conteudo>
					 <msg>' . $pMsg . '</msg>
					 <tituloMsg>' . $pTituloMsg . '</tituloMsg>
					 <tituloJanela>' . $pTituloJanela . '</tituloJanela>
				</ajax>
				';
    }

    public static function RenderizeTemplateBox($pTemplate) {
        $ret = '';
        $ret .= '<div class="ui-widget ui-widget-header tituloProcesso" style="padding:9px;margin-top:5px;">' . $pTemplate->mTitulo . '</div>';
        $ret .= self::RenderizeTemplate($pTemplate);
        $ret .= '</div>';
        return $ret;
    }

    /**
     *
     * @param cTEMPLATE $pTemplate
     * @return string
     */
    public static function RenderizeTemplate($pTemplate, $pComAcoes = false, $pEstilo = "") {
        $ret = '';
        if ($pTemplate->mTitulo != '') {
            $ret .= self::RenderizeTitulo($pTemplate, $pComAcoes, $pEstilo);
        }
        $ret .= self::formTemplate($pTemplate);
//		$ret .= '<div class="detalheProcesso">';
        if ($pTemplate->mMsgInicial != '') {
            $ret.= $pTemplate->mMsgInicial;
        }
        $ret .= self::RenderizeTemplateEdicao($pTemplate);
        $ret .= '</table>';
        $ret .= self::RenderizeBarraAcoesRodape($pTemplate->mAcoes);
        $ret .= '</form>';
//		$ret .= '</div>';

        return $ret;
    }

    /**
     *
     * @param cTEMPLATE $pTemplate
     * @return string
     */
    public static function RenderizeTemplateComAcoesNoCabecalho($pTemplate, $pEstilo = "") {
        $ret = '';
        if ($pTemplate->mTitulo != '') {
            $ret .= self::RenderizeTitulo($pTemplate, true, $pEstilo);
        }
        $ret .= self::formTemplate($pTemplate);
//		$ret .= '<div class="detalheProcesso">';
        if ($pTemplate->mMsgInicial != '') {
            $ret.= $pTemplate->mMsgInicial;
        }
        $ret .= self::RenderizeTemplateEdicao($pTemplate);
        $ret .= '</table>';
        $ret .= '</form>';
//		$ret .= '</div>';

        return $ret;
    }

    /**
     *
     * @param cTEMPLATE $pTemplate
     * @return string
     */
    public static function RenderizeTemplateComAcoesNoRodape($pTemplate, $pEstilo = "") {
        $ret = '';
        if ($pTemplate->mTitulo != '') {
            $ret .= self::RenderizeTitulo($pTemplate, false, $pEstilo);
        }
        $ret .= self::formTemplate($pTemplate);
        if ($pTemplate->mMsgInicial != '') {
            $ret.= $pTemplate->mMsgInicial;
        }
        $ret .= self::RenderizeTemplateEdicao($pTemplate);
        $ret .= '</table>';
        $ret .= self::RenderizeBarraAcoesRodape($pTemplate->mAcoes);
        $ret .= '</form>';

        return $ret;
    }

    /**
     * @param cTEMPLATE $pTemplate
     * @return string
     */
    public static function RenderizeTemplateAcoes($pTemplate) {
        $ret .= self::formTemplate($pTemplate);
//		$ret .= '<div class="detalheProcesso">';
        if ($pTemplate->mMsgInicial != '') {
            $ret.= $pTemplate->mMsgInicial;
        }
        $ret .= self::RenderizeBarraAcoesSemEstilo($pTemplate->mAcoes);
        $ret .= '</form>';
//		$ret .= '</div>';

        return $ret;
    }

    public static function RenderizeTitulo($pTemplate, $pComAcoes = false, $pEstilo = "") {
        $ret = '';
        if ($pTemplate->mTitulo != '') {
            if (cHTTP::$MIME != cHTTP::mmXLS) {
                if ($pEstilo == self::titPAGINA) {
                    $class = 'class="titulo"';
                } else {
                    $class = 'class="ui-widget ui-widget-header tituloProcesso"';
                }
                $ret .= '<div ' . $class . '>';
                if ($pComAcoes) {
                    $acoesTela = '';
                    foreach ($pTemplate->mAcoes as $acao) {
                        if ($acao->mVisivel) {
                            $acoesTela.= self::RenderizeAcao($acao);
                        }
                    }
                    $ret .= '<div class="barraBotoesTitulo">' . $acoesTela . '</div>';
                }
                $ret .= '<div class="texto">' . $pTemplate->mTitulo . '</div>';
                $ret .= '</div>';
            } else {
                $ret = '<tr><td>' . $pTemplate->mTitulo . '</td></tr>';
            }
        }
        return $ret;
    }

    public static function RenderizeListagem_Cabecalho($pTemplate) {
        $ret = '';
        $estilo = "";
        if (cHTTP::$MIME == cHTTP::mmXLS){
            $estilo = ' border="1"';
        }
        $ret .= '<table class="' . $pTemplate->get_estilosAdicionaisGrid() . '" '.$estilo.'>';
        $cols = '';
        if (is_array($pTemplate->mCols)) {
            foreach ($pTemplate->mCols as $val) {
                $cols .= '<col width="' . $val . '"/>';
            }
            $cols = '<colgroup>' . $cols . '</colgroup>';
        }
        $ret.= $cols;
        $ret .= '<thead>';
        $ret .= '<tr>';
        if (cHTTP::$MIME != cHTTP::mmXLS) {
            if (isset($pTemplate->mCampo['SELETOR'])) {
                $ret.= '<th class="cen"><input type="checkbox" id="SELETOR_ALL"/></th>';
            }
            if (count($pTemplate->mAcoesLinha) > 0) {
                if (cHTTP::getLang() == 'pt_br') {
                    $titAcoes = 'Ações';
                } else {
                    $titAcoes = 'Actions';
                }
                $ret.= '<th class="cen">' . $titAcoes . '</th>';
            }
        }

        foreach ($pTemplate->mCampo as $campo) {
            if ($campo->mTipo != cCAMPO::cpHIDDEN && $campo->mVisivel && $campo->mCampoBD != 'SELETOR') {
                $class = '';
                if ($campo->mClasse != '') {
                    $class = ' class="' . $campo->mClasse . '"';
                }
                if ($campo->mOrdenavel && cHTTP::$MIME != cHTTP::mmXLS) {
                    $acao = self::RenderizeAcao(new cACAO_SUBMIT_ORDER($campo->mLabel, 'ORDENAR_' . $campo->mCampoBD, $campo->mCampoOrdenacao));
                    //					var_dump($pTemplate->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor);
                    if ($campo->mNome == $pTemplate->mCampo[cTEMPLATE::CMP_ORDENACAO]->mValor) {
                        if ($pTemplate->mCampo[cTEMPLATE::CMP_ORDENACAO_SENTIDO]->mValor == 'desc') {
                            $acao .= '<img src = "/imagens/icons/bullet_arrow_down.png"/>';
                        } else {
                            $acao .= '<img src = "/imagens/icons/bullet_arrow_up.png"/>';
                        }
                    }
                } else {
                    $acao = $campo->mLabel;
                }

                if ($campo->IPE_habilitado()) {
                    $script = cJAVASCRIPTS::EditInPlaceTexto($campo->mCampoBD);
                    cHTTP::AdicionarScript($script);
                }

                $ret.= '<th' . $class . '>' . $acao . '</th>';
            }
        }
        $ret .= '</tr>';
        $ret .= '</thead>';
        return $ret;
    }

    public static function RenderizeListagem_Rodape($pTemplate) {
        return "</table>";
    }

    public static function RenderizeListagem_RodapePagina($pTemplate) {
        return "</form>";
    }

    public static function RenderizeListagem_Filtros($pTemplate) {
        $ret = '';
        /* 		if (count($pTemplate->mFiltro) > 0){
          $ret .= '<div class="filtros">';
          foreach($pTemplate->mFiltro as $filtro){
          if ($filtro->mVisivel){
          $ret.= '<div class="filtro">' . $filtro->mLabel . ':' . self::RenderizeFiltro($filtro) . '</div>';
          }
          }
          $ret .= '<div>&nbsp;</div>';
          $acao = new cACAO_SUBMIT_BUTTON("FILTRAR", "Filtrar", "Clique para atualizar a consulta");
          $ret.= self::RenderizeAcao($acao);
          $ret .= '</div>';
          }
         */
        $filtrosVisiveis = false;
        $ret = '';
        $vinculos = array();
        if (count($pTemplate->mFiltro) > 0) {
            foreach ($pTemplate->mFiltro as $filtro) {
                if ($filtro->mVisivel) {
                    $emUso = '';
                    if ($filtro->mValor != '') {
                        $emUso = " emUso";
                    }
                    $ret.= '<div class="filtro' . $emUso . '"><div class="label">' . $filtro->mLabel . ':</div>' . self::RenderizeFiltro($filtro) . '</div>';
                    $filtrosVisiveis = true;
                    // Monta scripts de vinculo
                    if (is_object($filtro->campoPai)) {
                        $vinculos[$filtro->campoPai->mCampoBD][] = $filtro->mCampoBD;
                    }
                }
            }
            if ($filtrosVisiveis) {
                $ret = '<div class="filfiltros"><table class="filtros"><tr><td>' . $ret;
                $ret .= '</td><td width="60px">';
                $acao = new cACAO_SUBMIT_BUTTON("FILTRAR", "Filtrar", "Clique para atualizar a consulta");
                $ret.= self::RenderizeAcao($acao);
                $ret .= '</td></tr></table></div>';
            } else {
                $ret = '';
            }
        }
        if (count($vinculos) > 0) {
            $scriptVinculos = '';
            foreach ($vinculos as $campoPai => $arrayFilhos) {
                $scriptCampo = '';
                foreach ($arrayFilhos as $campoFilho) {
                    $scriptCampo .= "\n			RecarregarFiltro('" . $pTemplate->mnome . "', '" . $campoPai . "', '" . $campoFilho . "');";
                }
                $scriptVinculos .= "\n$('#" . $pTemplate->mnome . " #" . cINTERFACE::PREFIXO_CAMPOS . 'FILTRO_' . $campoPai . "').change(function() {";
                $scriptVinculos .= $scriptCampo;
                $scriptVinculos .= "\n		});";
            }
            cHTTP::AdicionarScript($scriptVinculos);
        }
        return $ret;
    }

    /**
     * Gera a linha com as colunas visíveis da listagem
     * @param cTEMPLATE $pTemplate
     */
    public static function RenderizeListagem_Linha(&$pTemplate, $pEstilo = '', &$pIndLinha = '', $pid="") {
        $ret = '';
        $estilo = '';
        if ($pEstilo != '') {
            $estilo = ' class="' . $pEstilo . '"';
        }

        $ret .= '<tr' . $estilo . ' id="'.$pid.'" name="'.$pid.'">';
        if (cHTTP::$MIME != cHTTP::mmXLS) {
            // Renderiza as ações
            $acoes = '';
            if (isset($pTemplate->mCampo['SELETOR'])) {
                $pTemplate->mCampo['SELETOR']->mClasse = "seletor";
                $ret.= '<td class="cen">' . self::RenderizeCampo($pTemplate->mCampo['SELETOR'], false, $pIndLinha) . '</td>';
            }
            if (count($pTemplate->mAcoesLinha) > 0) {
                foreach ($pTemplate->mAcoesLinha as &$acao) {
                    $parametros = '';
                    if ($acao->mVisivel) {
                        foreach ($pTemplate->mCampo as $campo) {
                            if ($campo->mChave) {
                                $parametros .= '&' . $campo->mCampoBD . '=' . $campo->mValor;
                            }
                        }
                        if ($pIndLinha != '') {
                            $parametros.= '&linha=' . $pIndLinha;
                        }

                        $acao->mParametros = $parametros;
                        $acoes.= self::RenderizeAcao($acao);
                    }
                }
                if ($acoes == '') {
                    $acoes = '&nbsp;';
                }
                $ret .= '<td class="cen acoes">' . $acoes . '</td>';
            }
        }
        // Renderiza os campos chave
        foreach ($pTemplate->mCampo as $campo) {
            if ($campo->mChave) {
                $ret.= self::RenderizeCampo($campo, false, $pIndLinha);
            }
        }

        // Renderiza as colunas
        foreach ($pTemplate->mCampo as $campo) {
            if ($campo->mTipo != cCAMPO::cpHIDDEN && $campo->mVisivel && $campo->mCampoBD != 'SELETOR') {
                $class = ' class="' . $campo->mClasse . ' ' . $campo->mCampoBD . '"';

                if (method_exists($campo, 'IPE_habilitado')) {
                    if ($campo->IPE_habilitado()) {
                        $ipe = ' data-inplace="' . htmlentities($campo->IPE_getConfiguracao(), ENT_QUOTES) . '"';
                    } else {
                        $ipe = '';
                    }
                }
                $rowspan = '';
                if ($campo->mRowspan != '') {
                    $rowspan = ' rowspan="' . $campo->mRowspan . '" ';
                }

                $ret.= '<td' . $rowspan . $class . $ipe . '>' . self::RenderizeCampo($campo, false, $pIndLinha) . '</td>';
            }
        }
        $ret .= '</tr>';
        return $ret;
    }

    public static function RenderizeIndiceDeAbas($pTemplate, $pAbaAtiva, $pNomeAba = 'tabs') {
        $lista = '';
//		var_dump($pTemplate->mAbas);
        if (is_array($pTemplate->mAbas)) {
            $i = 0;
            foreach ($pTemplate->mAbas as $aba) {
                if ($aba->mCodigo == $pAbaAtiva) {
                    $lista .= '<li><a href="#abaAtiva">' . $aba->mLabel . '</a></li>';
                    $sel = $i;
                } else {
                    $lista .= '<li>' . self::RenderizeAcao($aba) . '</li>';
                }
                $i++;
            }
        }
//		var_dump($lista);
        $script = "
	$(document).ready(function(){
		$('#" . $pNomeAba . "').tabs({
			active: " . $sel . ",
			heightStyle: 'content',
			beforeActivate: function(event, ui) {
				if(ui.newTab.context.href.substr(0,4) == 'http' ){
					window.location.href = ui.newTab.context.href;
					return false;
				}
				else{
					return true;
				}
			}
		})
	});
";
        cHTTP::AdicionarScript($script);
        return '<ul>' . $lista . '</ul>';
    }

    public static function RederizeAbas($pTemplate) {
        $lista = '';
        $conteudo = '';
        $i = 0;
        if (is_array($pTemplate->mAbas)) {
            foreach ($pTemplate->mAbas as $aba) {
                $lista .= '<li><a href="' . $aba['conteudo'] . '">' . $aba['titulo'] . '</a></li>';
                $i++;
            }
        }
        return '<div id="tabs"><ul>' . $lista . '</ul></div>';
    }

}
