<?php

class cFILTRO_VALOR extends cFILTRO {

    private $atributosAdicionais;

    public function __construct($pLabel, $pCampo, $pTipoSel = self::tpSEL_IGUAL, $pAtributosAdicionais = '', $pValorDefault = '', $pqualificadorQuery = '') {
        $this->label = $pLabel;
        $this->tipoSel = $pTipoSel;
        $this->campoBusca = $pCampo;
        $this->atributosAdicionais = $pAtributosAdicionais;
        $this->valorDefault = $pValorDefault;
        $this->valor = $this->valorDefault;
        $this->valorNaoSelecionado = '';
        $this->qualificadorQuery= $pqualificadorQuery;
    }

    public function campoHTML() {
        return '<input type="text" name="' . $this->nomeCampoHTML() . '" id="' . $this->nomeCampoHTML() . '" value="' . $this->valor . '" class="filtro_valor" ' . $this->atributosAdicionais . '/>';
    }

    public function Where() {
        $where = '';
        if ($this->valor != $this->valorNaoSelecionado) {
            if ($this->tipoSel == self::tpSEL_IGUAL) {
                $where = " and " . $this->getQualificadorQuery(). $this->campoBusca . " =" . cBANCO::SqlOk($this->valor);
            }

            if ($this->tipoSel == self::tpSEL_MAIORQ) {
                $where = " and " . $this->getQualificadorQuery() . $this->campoBusca . " >" . cBANCO::SqlOk($this->valor);
            }

            if ($this->tipoSel == self::tpSEL_MENORQ) {
                $where = " and " . $this->getQualificadorQuery() . $this->campoBusca . " <" . cBANCO::SqlOk($this->valor);
            }
        }
        return $where;
    }

}
