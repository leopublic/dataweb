<?php
class cCONFIGURACAO{
	public $servidor;
	public $loginDb;
	public $senhaDb;
	public $database;

	const cfgDESENVOLVIMENTO	= 'DES';
	const cfgTESTES				= 'TST';
	const cfgPRODUCAO			= 'PRD';
	const cfgPRODUCAO2			= 'PRD2';
	const cfgHOMOLOGACAO		= 'HOM';
	const cfgM2		= 'M2';

	public static $configuracoes = array(
		 "DES"	=> array(
			 'servidor' => 'localhost'
			,'loginDb'	=> 'root'
			,'senhaDb'	=> 'mysql'
			,'database'	=> 'dataweb'
			,'dominio'	=> 'http://dataweb.dev'
			,'raiz'		=> '/var/www/dataweb'
		)
		,"TST"	=> array(
			 'servidor' => 'localhost'
			,'loginDb'	=> 'root'
			,'senhaDb'	=> 'mysql'
			,'database'	=> 'dataweb'
			,'dominio'	=> 'http://dataweb.gatewayleo.com'
			,'raiz'		=> 'D:\projetos\mundivisas\trunk'
		)
		,"PRD2"	=> array(
			 'servidor' => '127.0.0.1'
			,'loginDb'	=> 'dataweb'
			,'senhaDb'	=> 'gg0rezqn'
			,'database'	=> 'dataweb'
			,'dominio'	=> 'http://dataweb.mundivisas.com.br'
			,'raiz'		=> 'c:\dataweb'
		)
		,"PRD"	=> array(
			 'servidor' => '127.0.0.1'
			,'loginDb'	=> 'dataweb'
			,'senhaDb'	=> 'gg0rezqn'
			,'database'	=> 'dataweb'
			,'dominio'	=> 'http://dataweb.mundivisas.com.br'
			,'raiz'		=> '/var/www/dataweb'
		)
		,"M2"	=> array(
			 'servidor' => '186.202.152.37'
			,'loginDb'	=> 'm2software8'
			,'senhaDb'	=> 'gg0rezqn'
			,'database'	=> 'dataweb'
			,'dominio'	=> 'http://datawebdev.m2software.com.br'
			,'raiz'		=> 'e:/m2software/web/dataweb'
		)
		,"HOM"	=> array(
			 'servidor' => '127.0.0.1'
			,'loginDb'	=> 'dataweb'
			,'senhaDb'	=> 'gg0rezqn'
			,'database'	=> 'dataweb'
			,'dominio'	=> 'http://dataweb2.mundivisas.com.br'
			,'raiz'		=> '/var/www/dataweb/'
                        ,'porta'        => '81'
		)
	);
}
