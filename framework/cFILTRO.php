<?php
abstract class cFILTRO
{
	const tpSEL_LIKE = 1;
	const tpSEL_LIKE_INI = 2;
	const tpSEL_IGUAL = 3;
	const tpSEL_MAIORQ = 4;
	const tpSEL_MENORQ = 5;
	const tpSEL_DIFERENTE = 6;
	
	public $label;
	protected $valor;  
	protected $tabela ;
	protected $campoChave;
	protected $campoBusca;
	protected $valorDefault;
	protected $valorNaoSelecionado;
	protected $tipoSel;
	protected $ind;
        protected $qualificadorQuery;

	public function label(){
		return $this->label;
	}
	
        public function getQualificadorQuery(){
            if ($this->qualificadorQuery != '' && substr($this->qualificadorQuery, -1) != '.'){
                $this->qualificadorQuery .= '.';
            }
            return $this->qualificadorQuery;
        }
        
	public function nomeCampoHTML(){
		return 'cFILTRO_'.$this->campoBusca.$this->ind;
	}

	public abstract function campoHTML();
	public function valorInformado(){
		return $this->valor;
	}
	public function ObterValorInformado($pPOST, &$pSESSION = '', $pNomeTela = '' ){
		if (isset($pPOST[$this->nomeCampoHTML()])){
			$this->valor = trim($pPOST[$this->nomeCampoHTML()]);
			$pSESSION['cFILTRO'.$pNomeTela.$this->nomeCampoHTML()]=$this->valor;
		}
		else{
			$this->valor = $this->valorDefault;
			if (isset($pSESSION['cFILTRO'.$pNomeTela.$this->nomeCampoHTML()])){
				$this->valor = $pSESSION['cFILTRO'.$pNomeTela.$this->nomeCampoHTML()];
			}
		}
	}		
	public abstract function Where();	
	public function setValor($pValor){
		$this->valor = $pValor;
	}
	public function setInd($pValor){
		$this->ind = $pValor;
	}
	public function getInd(){
		return $this->ind;
	}
}
?>
