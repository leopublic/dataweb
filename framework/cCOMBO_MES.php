<?php
class cCOMBO_MES extends cCOMBO_GERAL{
	public function CarregarValores(){
		$this->AdicionarValor("1", "Janeiro");
		$this->AdicionarValor("2", "Fevereiro");
		$this->AdicionarValor("3", "Março");
		$this->AdicionarValor("4", "Abril");
		$this->AdicionarValor("5", "Maio");
		$this->AdicionarValor("6", "Junho");
		$this->AdicionarValor("7", "Julho");
		$this->AdicionarValor("8", "Agosto");
		$this->AdicionarValor("9", "Setembro");
		$this->AdicionarValor("10", "Outubro");
		$this->AdicionarValor("11", "Novembro");
		$this->AdicionarValor("12", "Dezembro");
	}
	
	public function where(){
		return " 1=1 ";
	}
	
}
