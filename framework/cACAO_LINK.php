<?php
class cACAO_LINK extends cACAO{
	public function __construct($pLabel, $pCodigo, $pTitulo, $pLink, $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "", $pLabel_en_us = "") {
		parent::__construct(cACAO::ACAO_LINK, $pLabel, $pCodigo, $pTitulo, $pMsgConfirmacao, $pMsgConfirmacaoTitulo, $pLabel_en_us);
		$this->mLink = $pLink;
	}
	public function getLinkComParametros(){
		return $this->mLink.$this->getParametrosParaLink();
	}

}
