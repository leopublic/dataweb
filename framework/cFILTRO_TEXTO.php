<?php

class cFILTRO_TEXTO extends cFILTRO{

	private $atributosAdicionais;

	

	public function __construct($pLabel, $pCampo, $pTipoSel=self::tpSEL_LIKE, $pAtributosAdicionais='', $pValorDefault = ''){

		$this->label = $pLabel;

		$this->tipoSel = $pTipoSel;

		$this->campoBusca = $pCampo;

		$this->atributosAdicionais = $pAtributosAdicionais;

		$this->valorDefault = $pValorDefault;

		$this->valor = $this->valorDefault;

		$this->valorNaoSelecionado = '';

	}

		

	public function campoHTML(){

		return '<input type="text" name="'.$this->nomeCampoHTML().'" id="'.$this->nomeCampoHTML().'" value="'.$this->valor.'" class="filtro_texto" '.$this->atributosAdicionais.'/>';

	}



	public function Where(){

		$where = '';

		if ($this->valor!=$this->valorNaoSelecionado){

			if ($this->tipoSel == self::tpSEL_LIKE) {

	     		$where = " and ".$this->campoBusca." like '%".str_replace(' ', '%', cBANCO::SqlOk($this->valor))."%'";

			}

			if ($this->tipoSel == self::tpSEL_LIKE_INI) {

	     		$where = " and ".$this->campoBusca." like '".cBANCO::SqlOk($this->valor)."%'";

			}

			if ($this->tipoSel == self::tpSEL_IGUAL) {

	     		$where = " and ".$this->campoBusca." = '".cBANCO::SqlOk($this->valor)."'";

			}

		}		

		return $where;

	}

}
