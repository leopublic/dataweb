<?php
class cACAO_AJAX extends cACAO{
    protected $campos = array();

	public function __construct($controller, $metodo,  $pLabel, $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "", $pLabel_en_us = "") {
		parent::__construct(cACAO::ACAO_JAVASCRIPT, $pLabel, $pCodigo, $pTitulo, $pMsgConfirmacao, $pMsgConfirmacaoTitulo, $pLabel_en_us);
        $this->mControle = $controller;
        $this->mMetodo = $metodo;
	}

    public function adicionaCampo($nome, $valor){
        $this->campos[$nome] = $valor;
    }

    public function getLinkComParametros(){
		return $this->mLink;
	}

    public function emHtml(){
        $parametros = '{"controller": "'.$this->mControle.'"';
        $parametros.= ', "metodo": "'.$this->mMetodo.'"';
        foreach($this->campos as $chave => $valor){
            $parametros.= ', "'.$chave.'": "'.$valor.'"';
        }
        $parametros.= ', "cd_usuario": "'.cSESSAO::$mcd_usuario.'"';
        $parametros.= '}';
    	$link = '<a href="#" onclick="AcionaProcesso(this);return false;" data-parameters=\''.$parametros.'\'>'.$this->Label().'</a>';
        return $link;
    }


}
