<?php
/**
 * Sub-tipo de acao que gera um post para o formulario onde ela está, alterando o código da açao para o valor do parametro (via javascript)
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cACAO_SUBMIT_ORDER extends cACAO{
	public function __construct($pLabel, $pCodigo , $pCampoOrdenacao , $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "", $pLabel_en_us = "" ){
			parent::__construct(cACAO::ACAO_SUBMIT_ORDER, $pLabel, $pCodigo , $pTitulo , $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "", $pLabel_en_us);
			$this->mCampoOrdenacao = $pCampoOrdenacao;
	}
}
