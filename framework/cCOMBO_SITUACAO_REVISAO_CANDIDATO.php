<?php
class cCOMBO_SITUACAO_REVISAO_CANDIDATO extends cCOMBO_GERAL{
	public function __construct($pCampoBD, $pLabel, $pLabel_en_us = '') {
		parent::__construct($pCampoBD, $pLabel, $pLabel_en_us);
		$this->mValorDefault = '0';
	}
	public function CarregarValores(){
		$this->AdicionarValor("", "(todos)");
		$this->AdicionarValor("0", "(não revisado)");
		$this->AdicionarValor("1", "Revisado");
		$this->AdicionarValor("2", "Pendente");
	}

	public function where(){
		switch($this->mValor){
			case "":
				return "";
				break;
			case "0":
				return "(".$this->getQualificadorFiltro.$this->mCampoBD . " = " . cBANCO::SimNaoOk($this->mValor)." or ".$this->getQualificadorFiltro.$this->mCampoBD . " is null) ";
				break;
			case "1":
			case "2":
				return $this->getQualificadorFiltro.$this->mCampoBD . " = " . cBANCO::SimNaoOk($this->mValor);
				break;
		}
	}
}
