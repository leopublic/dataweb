<?php
class cACAO_METODO_POPUP extends cACAO{
	public function __construct($pLabel, $pCodigo , $pController, $pMetodo, $pTitulo , $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = ""){
		parent::__construct(cACAO::ACAO_METODO_POPUP, $pLabel, $pCodigo , $pTitulo , $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "");
		$this->mControle = $pController;
		$this->mMetodo = $pMetodo;
	}
}
