<?php
/**
 * Organiza os elementos de uma ação disponibilizada para o usuário
 * @param string $pTipo => Tipo da ação: FORM, LINK ou MSG
 * @param string $pCodigo => nome da acao
 * @param string $pTitulo => dica que aparece no botao
 * @param string $pMsgConfirmacao [opcional]=> mensagem que pede para confirmar a acao
 * @param string $pMsgConfirmacaoTitulo [opcional]=> mensagem que pede para confirmar a acao
 */
class cACAO{
	/**
	 * Botão submit de formulário
	 */
	const ACAO_FORM = "1";
	/**
	 * Botão de redirecionamento para outra página
	 */
	const ACAO_LINK = "2";
	/**
	 * Botão de exibição de mensagem
	 */
	const ACAO_MSG = "3";
	/**
	 * Botão de execução de código javascript no cliente
	 */
	const ACAO_JAVASCRIPT = "4";
	/**
	 * Botão para enviar o form por ajax e aguardar uma resposta
	 */
	const ACAO_SUBMIT_AJAX = "5";
	/**
	 * Link do título das colunas
	 */
	const ACAO_SUBMIT_ORDER = "6";
	/**
	 * Botão para abrir uma janela popup
	 */
	const ACAO_METODO_POPUP = "7";
	/**
	 * Botão para abrir uma janela popup com post
	 */
	const ACAO_METODO_POPUP_POST = "8";
	/**
	 * Botão de redirecionamento para outro metodo
	 */
	const ACAO_LINK_CONTROLER_METODO = "9";
	const ACAO_LINK_ABA = "10";



	public $mid_edicao;
	public $mno_acao;
	public $mno_classe;
	public $mno_funcao;
	private $mLabel = array();
	public $mTitulo;
	public $mMsgConfirmacao;
	public $mMsgConfirmacaoTitulo;
	public $mCodigo;
	public $mTipo;
	public $mLink;
	public $mOnClick;
	public $mControle;
	public $mMetodo;
	public $mParametros;
	public $mVisivel;
	public $mCampoOrdenacao;
	public $mTarget;
	public $mpagina;


	public function Label($pLang = 'pt_br'){
		if (trim($pLang) == ''){
			$pLang = 'pt_br';
		}
		return $this->mLabel[$pLang];
	}

	public function setLabel($pValor, $pLang = "pt_br"){
		$this->mLabel[$pLang] = $pValor;
	}

	public function __set($pPropriedade, $pValor){
		if($pPropriedade == "mLabel"){
			$this->mLabel['pt_br'] = $pValor;
		}
		else{
			$this->$pPropriedade = $pValor;
		}
	}

	public function __get($pPropriedade){
		if($pPropriedade == "mLabel"){
			return $this->mLabel[cHTTP::getLang()];
		}
		else{
			return $this->$pPropriedade;
		}
	}

	public function __construct($pTipo, $pLabel, $pCodigo , $pTitulo , $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "", $pLabel_en_us = "" ){
		$this->mTipo = $pTipo;
		$this->mLabel['pt_br'] = $pLabel;
		if (trim($pLabel_en_us) =='' ){
			$pLabel_en_us = $pLabel;
		}
		$this->mLabel['en_us'] = $pLabel_en_us;
		$this->mCodigo = $pCodigo;
		$this->mTitulo = $pTitulo;
		$this->mMsgConfirmacao = $pMsgConfirmacao;
		$this->mMsgConfirmacaoTitulo = $pMsgConfirmacaoTitulo;
		$this->mVisivel = true;
		$this->mTarget = "_self";
	}

	public function getParametrosParaLink(){
		$interroga = '';
		if (!strstr($this->mLink, "?")){
			$interroga = '?';
		}
		if($this->mParametros != '' ){
			return $interroga.$this->mParametros;
		}
	}

	public function AdicionaParametro($pParametro){
		$this->mParametros.= $pParametro;
	}

	public function getLinkComParametros(){
		$interroga = '';
		if (!strstr($this->mLink, "?")){
			$interroga = '?';
		}
		return $this->mLink.$interroga.$this->mParametros;
	}
}
