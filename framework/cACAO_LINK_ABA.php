<?php
/**
 * Sub tipo de acao que redireciona para outra pagina.
  * @copyright M2 Software - Utilizacao fora de projetos da M2 Software nao autorizada.
*/
class cACAO_LINK_ABA extends cACAO{
	public function __construct($pCodigo, $pLabel, $pTitulo, $pController, $pMetodo, $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "") {
		parent::__construct(cACAO::ACAO_LINK_ABA, $pLabel, $pCodigo, $pTitulo, $pMsgConfirmacao, $pMsgConfirmacaoTitulo);
		$this->mController = $pController;
		$this->mMetodo = $pMetodo;
	}
}
