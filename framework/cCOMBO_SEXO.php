<?php
class cCOMBO_SEXO extends cCOMBO_GERAL{
	public function CarregarValores(){
		if (cHTTP::getLang() == "pt_br"){
			$this->AdicionarValor("F", "Feminino");
			$this->AdicionarValor("M", "Masculino");
		}
		else{
			$this->AdicionarValor("F", "Feminine");
			$this->AdicionarValor("M", "Masculine");
		}
	}
}
