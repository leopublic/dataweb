<?php
class cACAO_JAVASCRIPT extends cACAO{
	public function __construct($pLabel, $pCodigo, $pTitulo, $pOnClick, $pMsgConfirmacao = "", $pMsgConfirmacaoTitulo = "", $pLabel_en_us = "") {
		parent::__construct(cACAO::ACAO_JAVASCRIPT, $pLabel, $pCodigo, $pTitulo, $pMsgConfirmacao, $pMsgConfirmacaoTitulo, $pLabel_en_us);
		$this->mOnClick = $pOnClick;
	}
	public function getLinkComParametros(){
		return $this->mLink.$this->getParametrosParaLink();
	}
}
