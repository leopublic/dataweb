<?php

abstract class cCOMBO_GERAL extends cCAMPO {

    public $mTipoCombo;
    public $mPermiteDefault;
    public $mValorDefault;
    protected $valores;
    protected $valoresMustache;
    protected $autocomplete;
    protected $source;

    /**
     * Retorna o array de valores do combo
     * @return array(array(2)) Retorna um array de arrays com chave, descricao
     */
    public function valores() {
        return $this->valores;
    }

    public function valoresMustache() {
        return $this->valoresMustache;
    }

    public function __construct($pCampoBD, $pLabel, $pLabel_en_us = '') {
        parent::__construct($pCampoBD, $pLabel, cCAMPO::cpCOMBO_GERAL);
        $this->setLabel($pLabel_en_us, "en_us");
        $this->mPermiteDefault = true;
        $this->mValorDefault = "(selecione...)";
        $this->autocomplete = false;
    }

    public function set_autocomplete($pvalor) {
        $this->autocomplete = $pvalor;
    }

    public function set_source($pvalor) {
        $this->source = $pvalor;
    }

    /**
     * Renderiza o combo como select 
     * @param type $pNome
     * @return string 
     */
    public function HTML($pNome) {
        $ret = "";
        $this->CarregarValores();

        $ret .= $this->Select($pNome);
        for ($index = 0; $index < count($this->valores); $index++) {
            $ret .= $this->Opcao($this->mValor, $this->valores[$index][0], $this->valores[$index][1]);
        }
        $ret .= '</select>';
        return $ret;
    }

    /**
     *
     * @param type $pNome
     * @return string
     */
    public function HTMLEmLista() {
        $ret = "";
        $this->CarregarValores();

        $espaco = '';
        for ($index = 0; $index < count($this->valores); $index++) {
            $ret .= $espaco . $this->OpcaoEmLista($this->mValor, $this->valores[$index][0], $this->valores[$index][1]);
            $espaco = '&nbsp;&nbsp;';
        }
        return $ret;
    }

    /**
     * Gera a cláusula select do hmtl
     * @param type $pNome
     * @return type
     */
    public function Select($pNome) {
        if ($this->mClasse != '') {
            $class = ' class="' . $this->mClasse . '" ';
        } else {
            $class = '';
        }
        if ($this->mDisabled) {
            $disabled = ' disabled="disabled"';
        } else {
            $disabled = '';
        }

        if (strstr($this->mClasse, 'editavel')) {
            $parametros = ' data-parametros=\'' . $this->get_parametros() . '\'  data-valor-orig="' . $this->mValor . '"';
        } else {
            $parametros = '';
        }

        return '<select ' . $pNome . ' ' . $class . $disabled . $this->mOnChange . $parametros . '>';
    }

    public function InicializarValores() {
        $this->valores = array();
        if ($this->mPermiteDefault) {
            $this->valores[] = array("", $this->mValorDefault);
            $this->valoresMustache[] = array("valor" => "", "descricao" => $this->mValorDefault, "selected" => false);
        }
    }

    public function AdicionarValor($pValor, $pTexto) {
        $this->valores[] = array($pValor, $pTexto);
        if ($pValor == $this->mValor) {
            $selected = true;
        } else {
            $selected = false;
        }
        $this->valoresMustache[] = array("valor" => $pValor, "descricao" => $pTexto, "selected" => $selected);
    }

    public function OpcaoDefault($pValor) {
        if ($this->mPermiteDefault) {
            $ret .= self::Opcao($pValor, "", $this->mValorDefault);
        }
        return $ret;
    }

    public function Opcao($pValorInformado, $pValor, $pDescricao) {
        $sel = "";
        if ($pValorInformado != '') {
            if ($pValorInformado == $pValor) {
                $sel = " selected ";
            }
        }
        $ret = '<option value="' . $pValor . '"' . $sel . '>' . $pDescricao . '</option>';
        return $ret;
    }

    public function OpcaoEmLista($pValorInformado, $pValor, $pDescricao) {
        $sel = "";
        if ($pValorInformado != '') {
            if ($pValorInformado == $pValor) {
                $sel = "X";
            } else {
                $sel = '&nbsp;&nbsp;';
            }
        } else {
            $sel = '&nbsp;';
        }
        $ret = '(' . $sel . ')&nbsp;' . $pDescricao . '';
        return $ret;
    }

    public abstract function CarregarValores();

    public function FormatoMustache() {
        $this->CarregarValores();
        $saida = array();
        $saida["cpCHAVE_ESTRANGEIRA"] = true;
        if ($this->mTipoCombo == cCOMBO::cmbEMBARCACAO_PROJETO || $this->mTipoCombo == cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS) {
            $saida["td"] = "EmbarcacaoProjeto";
        }
        $saida["label"] = $this->Label();
        $saida["nome"] = $this->mNome;
        $saida["valores"] = $this->valoresMustache;
        return $saida;
    }

    public function JSON() {
        $this->CarregarValores(false); // Carrega os valores sem codificar os caracteres especiais 
        $ret = array(
            "CampoBD" => $this->mCampoBD
            , "Classe" => $this->mClasse
            , "valores" => $this->valores
        );
        return $ret;
    }

}
