<?php

/**
 * Description of cRET_AJAX_SUCESSO
 *
 * @author leonardo
 */
class cRET_AJAX_SUCESSO extends cRET_AJAX_PADRAO{
	public function __construct($pMsg) {
		$this->codRet = self::codRet_SUCESSO ;
		$this->tituloMsg = "Dataweb";
		$this->msg = $pMsg;
	}
}
