<?php
class cTEMPO_EXECUCAO{
	protected static $inicio;
	protected static $lap;
	public static function microtime_float() 
	{ 
	    list($usec, $sec) = explode(" ", microtime()); 
	    return ((float)$usec + (float)$sec); 
	} 

	public static function Inicia(){
		self::$inicio = self::microtime_float();
		self::$lap = self::$inicio;
	}

	public static function RegistraDuracao($pEtapa){
		$agora = self::microtime_float();
		$time = $agora - self::$lap;
		$total = $agora - self::$inicio;
		self::$lap = $agora;
		error_log($pEtapa." Lap: ".number_format($time, 5)." Total:".number_format($total, 5));
	}
	    

}
