<?php
class cJAVASCRIPTS{
	/**
	 * Renderiza o script que associa dois combos
	 * @param cTEMPLATE $pTemplate
	 * @param cCOMBO $pComboPai
	 * @param cCOMBO $pComboFilho
	 */
	public static function ScriptAssocieCombosEmpresaEmbProj(){
		$ret = '
			// Ativa a atulização automatica das embarcacoes projeto
			if ($(\'#CMP_NU_EMBARCACAO_PROJETO\').length > 0 ){
				if ($(\'select[id="CMP_NU_EMPRESA"]\').length > 0){
					$(\'select[id="CMP_NU_EMPRESA"]\').change(function() {
						atualizaEmbarcacaoProjeto(this);
					});
					atualizaEmbarcacaoProjeto($(\'select[id="CMP_NU_EMPRESA"]\'));
				}
			}

			if ($(\'#CMP_FILTRO_NU_EMBARCACAO_PROJETO\').length > 0 ){
				if ($(\'select[id="CMP_FILTRO_NU_EMPRESA"]\').length > 0){
					$(\'select[id="CMP_FILTRO_NU_EMPRESA"]\').change(function() {
						atualizaEmbarcacaoProjetoFiltro(this);
					});
					atualizaEmbarcacaoProjetoFiltro($(\'select[id="CMP_FILTRO_NU_EMPRESA"]\'));
				}
			}';
		return $ret;
	}
	public static function ScriptAssocieCombos($pTemplate, $pComboPai, $pComboFilho, $pPrefixo = "CMP_"){
		$ret = '' ;
		$ret .= "\n$('#".$pTemplate->mnome." #".cINTERFACE::PREFIXO_CAMPOS.$pPrefixo.$pComboPai->mNome."').change(function() {";
		$ret .= "\n			RecarregarCombo('".$pTemplate->mnome."', '".$pComboPai->mCampoBD."', '".$pComboFilho->mCampoBD."');";
		$ret .= "\n		});";
		$ret .= "\nif ($('#".$pTemplate->mnome." #".cINTERFACE::PREFIXO_CAMPOS.$pPrefixo.$pComboPai->mNome."').val() != ''){";
		$ret .= "\n		//RecarregarCombo('".$pTemplate->mnome."', '".$pComboPai->mCampoBD."', '".$pComboFilho->mCampoBD."');";
		$ret .= "\n};";
		return $ret;
	}
	public static function ScriptAssocieFiltros($pTemplate, $pComboPai, $pComboFilho){
		$ret = '' ;
		$ret .= "\n$('#".$pTemplate->mnome." #".  cINTERFACE::PREFIXO_CAMPOS.'FILTRO_'.$pComboPai->mNome."').change(function() {";
		$ret .= "\n			RecarregarFiltro('".$pTemplate->mnome."', '".$pComboPai->mCampoBD."', '".$pComboFilho->mCampoBD."');";
		$ret .= "\n		});";
		return $ret;
	}
	
	public static function ScriptAbas($pSeletor){
		$ret = '
		var x = $("'.$pSeletor.'").tabs({
			heightStyle: "content",
			beforeActivate: function(event, ui) {
				if(ui.newTab.context.href.substr(0,4) == "http" ){
					window.location.href = ui.newTab.context.href;
					return false;
				}
				else{
					return true;
				}
			}
		});';
		return $ret;
	}

	public static function EditInPlaceTexto($pNomeColuna){
		$ret = "$('.".$pNomeColuna."').editable(
				function(value, settings) { 
					$.noty.closeAll();
					var tokens = jQuery.parseJSON($(this).attr('data-inplace'));
					tokens.valorAtributo = value;
					$.post('carregueSemMenu.php?controller=cCTRL_EDIT_IN_PLACE&metodo=AtualizeAtributo',
						tokens,
						function( data ) {
							ExibirRetornoAjax(data);
						},
						'json'
					);
					return(value);
				},
				{ 
					type   : 'text',
					submit : 'OK',
					cssclass: 'editInPlaceTextoSimples dir',
					style: '',
					width: 'none',
					placeholder: ''
				}
			);
		";
		return $ret;

	}
}
