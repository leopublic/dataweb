<?php
class cCONSULTA{
	public $view ;
	private $where ;
	private $filtros = array();
	public $order;
	
	public function AdicionarFiltro($pNome, $pFiltro)
	{
		$this->filtros[$pNome]=$pFiltro;
	}
	
	public function filtro($pNome)
	{
		return $this->filtros[$pNome];
	}

	public function RecuperarFiltrosEscolhidos($pPost, &$pSession)
	{
		foreach($this->filtros as $nome => &$filtro)
		{
			$filtro->ObterValorInformado($_POST, $_SESSION);
		}
	}
	
	public function where() 
	{
		$this->where = '';
		foreach($this->filtros as $nome => $filtro)
		{
			$this->where.=$filtro->Where();
		}
		return $this->where; 
	}
	
	public function Query()
	{
		$query = "select * from ".$this->view." where 1=1 ".$this->where.$this->order;
		return $query;
	}
	
}
?>
