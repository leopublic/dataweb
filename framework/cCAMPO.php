<?php
/**
 * Campo
 */
class cCAMPO{
	const cpHIDDEN = 1;
	const cpDATA = 2;
	const cpCHAVE_ESTR = 3;
	const cpCODIGO = 4;
	const cpVALOR = 5;
	const cpTEXTO = 6;
	const cpMEMO = 7;
	const cpLISTA = 8;
	const cpSIMNAO = 9;
	const cpOPCOES = 10;
	const cpCUSTOM = 11;
	const cpCHAVE = 12;
	const cpSIMNAO_LITERAL = 13;
	const cpLINK_INTERNO = 14;
	const cpLINK_EXTERNO = 15;
	const cpSENHA = 16;
	const cpSELETOR = 17;
	const cpCOMBO_GERAL = 18;
	const cpVALOR_DIN_REAL = 19;
	const cpVALOR_DIN_DOLAR = 20;
	const cpVALOR_INTEIRO = 21;
	const cpFILE = 22;
	const cpDATAHORA = 23;
	const cpFILE_COM_FORM = 24;
	const cpDATA_ONLY = 25;

	public $mNome;				// Nome que identifica o campo no php
	public $mCampoBD;			// Campo no banco que armazena o conteúdo do campo
	public $mValor;				// Valor do campo
	public $mClasse;			// Classe css de formatação do campo
	public $mObrigatorio;		// Indica obrigatoriedade de preenchimento
	public $mChave;				// Indica se o campo é chave dos registros da view onde o campo está
	public $mTipo;				// Indica o tipo de campo
	public $mTipoCombo;			// Indica o tipo de combo (cCOMBO::cmb***)
	public $mPainel;
	public $mReadonly;
	public $mDisabled;
	public $mVisivel;
	private $mCombo;


	public $mlinkMte;
	public $mlinkMJ;
	public $mlinkCertidao;
	public $mLarguraDupla;
	public $HTMLPronto;
	// Campo link
	public $mControle;
	public $mMetodo;
	public $mLinkExterno;
	public $mParametros;
	public $mEhArray;
	public $mCampoOrdenacao ;
	public $mOrdenavel;
	public $mOnChange;
	public $mColunaAtiva;
	public $mColunaSelecionavel;
	public $mRowspan;

	protected $width;
	protected $maxlength;
	protected $mLabel = array();

	public $mWhere ;		// Para filtros especiais
	public $mQualificadorFiltro;
	public $mTipoComparacaoFiltro;
	public $mOperadorFiltro; // Indica qual operador a ser usado no filtro and, or. Default and
	public $mTitle; // Dica de preenchimento
	
	// inplace editing
	/**
	 * @var boolean Indica se a atualizaçao inplace esta habilitada para esse campo 
	 */
	private $mIPE_Habilitado;
	/**
	 * @var boolean Chaves para atualizacao do campo
	 */
	private $mIPE_Chaves;
	/**
	 * @var boolean Nome do campo que deve ser atualizado
	 */
	private $mIPE_CampoAtributo;
	/**
	 * @var boolean Controller que deve ser acionado para atualizacao. Default cCTRL_EDIT_IN_PLACE
	 */
	private $mIPE_ControllerAtualizacao;
	/**
	 * @var boolean Metodo que deve ser acionado para atualizacao. Default Atualize
	 */
	private $mIPE_MetodoAtualizacao;
	/**
	 * @var boolean Entidade a ser atualizada
	 */
	private $mIPE_Entidade;

	protected $parametros;

    /**
     * Get e set para a propriedade maxlength
     */
    public function get_maxlength(){
        return $this->maxlength;
    }
    public function set_maxlength($pVal){
        $this->maxlength = $pVal;
    }
    /**
     * Get e set para a propriedade width
     */
    public function get_width(){
        return $this->width;
    }
    public function set_width($pVal){
        $this->width = $pVal;
    }

	public function Label($pLang = ''){
		if (trim($pLang) == ''){
			$pLang = cHTTP::getLang();
		}
		if (trim($pLang) == ''){
			$pLang = 'pt_br';
		}

		return $this->mLabel[$pLang];
	}

	public function set_data($pValor){
		$this->mData = $pValor;
	}
	public function get_data(){
		return $this->mData;
	}

	public function set_title($pValor){
		$this->mTitle = $pValor;
	}

	public function get_title(){
		return $this->mTitle;
	}

	public function get_ehVisivel(){
		if($this->mVisivel && $this->mTipo != 1){
			return true;
		}
		else{
			return false;
		}
	}

	public function AdicioneExtraAoComboInterno($pExtra){
		$this->mCombo->mExtra = $pExtra;
	}
	
	public function IPE_Configura($pentidade ,$pcampoAtributo = '', $pcontrollerAtualizacao = 'cCTRL_EDIT_IN_PLACE', $pmetodoAtualizacao = 'Atualize'){
		$this->mIPE_Habilitado = true;
		$this->mIPE_Entidade = $pentidade;
		if ($pcampoAtributo == ''){
			$pcampoAtributo = $this->mCampoBD;
		}
		$this->mIPE_CampoAtributo = $pcampoAtributo;
		$this->mIPE_ControllerAtualizacao = $pcontrollerAtualizacao;
		$this->mIPE_MetodoAtualizacao = $pmetodoAtualizacao;
	}
	
	public function IPE_InicializaChave(){
		$this->mIPE_Chaves = array();
	}	
	public function IPE_AdicionaChave($pcampo, $pvalor){
		$this->mIPE_Chaves[] = array("campo" => $pcampo, "valor" => $pvalor);
	}

	public function IPE_habilitado(){
		return $this->mIPE_Habilitado;
	}

	public function IPE_getConfiguracao(){
		$ret= array();
		$ret['entidade'] = $this->mIPE_Entidade;
		$ret['tipoAtributo'] = $this->mTipo;
		$ret['campoAtributo'] = $this->mIPE_CampoAtributo;
		$ret['controllerAtualizacao'] = $this->mIPE_ControllerAtualizacao;
		$ret['metodoAtualizacao'] = $this->mIPE_MetodoAtualizacao;		
		$ret['chave'] = $this->mIPE_Chaves;
		return json_encode($ret);
	}
	
	public function setLabel($pValor, $pLang = "pt_br"){
		$this->mLabel[$pLang] = $pValor;
	}
	
	public function __set($pPropriedade, $pValor){
		if($pPropriedade == "mLabel"){
			$this->mLabel['pt_br'] = $pValor;
		}
		else{
			$this->$pPropriedade = $pValor;
		}
	}

	public function __get($pPropriedade){
		if($pPropriedade == "mLabel"){
			return $this->mLabel[cHTTP::getLang()];
		}
		else{
			return $this->$pPropriedade;
		}
	}
	
	public function __construct($pCampoBD, $pLabel, $pTipo, $pNome='', $pChave='', $pObrigatorio=false, $pClasse='', $pTipoCombo=0, $pPainel='', $pReadonly=0, $pVisivel = true, $pfl_link_mte=0 , $pfl_link_mj=0, $pLarguraDupla=0 , $pLabel_en_us = '' ){

		$this->mLabel["pt_br"] = $pLabel;
		if ($pLabel_en_us == '' ){
			$pLabel_en_us = $pLabel;
		}

		$this->mLabel["en_us"] = $pLabel_en_us;
		$this->mTipo = $pTipo;
		$this->mCampoBD = $pCampoBD;
		$this->mNome = $pNome;

		if ($this->mNome==''){
			$this->mNome = $this->mCampoBD;
		}

		$this->mObrigatorio = $pObrigatorio;
		$this->mClasse = $pClasse;
		$this->mChave = $pChave;
		$this->mTipoCombo = $pTipoCombo;
		$this->mPainel = $pPainel;
		$this->mReadonly = $pReadonly;
		$this->mVisivel= $pVisivel;
		$this->mlinkMte = $pfl_link_mte;
		$this->mlinkMJ = $pfl_link_mj;
		$this->mLarguraDupla = $pLarguraDupla;
		$this->mEhArray = false;
		$this->mDisabled = false;
		$this->mCampoOrdenacao = $this->mCampoBD;
		$this->mWhere = '' ;
		$this->mColunaSelecionavel = true;
		if ($this->mTipo==cCAMPO::cpCHAVE_ESTR){
			$this->mCombo = new cCOMBO($pTipoCombo);
		}
		$this->mIPE_Habilitado = false;
		$this->mIPE_Chaves = array();

	}

	public static function SQLCamposCadastradas($pid_edicao){
		return "select * from Campo where id_edicao = ".$pid_edicao." order by nu_ordem";
	}

	public function getQualificadorFiltro(){
		if ($this->mQualificadorFiltro != ''){
			return $this->mQualificadorFiltro.'.';
		}
		else{
			return "";
		}
	}
	
	public function where() {

		$qualificador = '';

		if ($this->mQualificadorFiltro != ''){
			$qualificador = $this->mQualificadorFiltro.'.';
		}

		if ($this->mWhere == '' ){
			switch ($this->mTipo){
				case self::cpTEXTO:
					if ($this->mTipoComparacaoFiltro == cFILTRO::tpSEL_LIKE_INI){
						return $qualificador.$this->mCampoBD . " like ". cBANCO::StringOk($this->mValor.'%');
					}
					else{
						return $qualificador.$this->mCampoBD . " like ". cBANCO::StringOk('%'.$this->mValor.'%');
					}
					break;

				case self::cpDATA:
				case self::cpDATAHORA:
					return $qualificador.$this->mCampoBD . " = " . cBANCO::DataOk($this->mValor);
					break;

				case self::cpVALOR:
				case self::cpHIDDEN:
					return $qualificador.$this->mCampoBD . " = " . cBANCO::InteiroOk($this->mValor);
					break;

				case self::cpCOMBO_GERAL:
				case self::cpCHAVE_ESTR:
					if ($this->mTipoComparacaoFiltro == cFILTRO::tpSEL_DIFERENTE){
						return $qualificador.$this->mCampoBD . " <> " . cBANCO::ChaveOk($this->mValor);
					}
					else{
						return $qualificador.$this->mCampoBD . " = " . cBANCO::ChaveOk($this->mValor);						
					}
					break;

				case self::cpSIMNAO:
					return $qualificador.$this->mCampoBD . " = " . cBANCO::SimNaoOk($this->mValor);
					break;

				default:
					return "nao sei!!!";
					break;
			}
		}
		else{
			return $this->mWhere;
		}
	}

	public function set_parametros($pvalor){
		$this->parametros = $pvalor;
	}

	public function get_parametros(){
		return $this->parametros;
	}
}
