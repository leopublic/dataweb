<?php

class cCOMBO_STATUS_EDICAO extends cCOMBO_GERAL {
    public function CarregarValores() {
        $this->AdicionarValor("1", "Edição disponível");
        $this->AdicionarValor("2", "Edição iniciada pelo candidato");
        $this->AdicionarValor("3", "Edição aguardando revisão (bloqueada)");
    }



}
