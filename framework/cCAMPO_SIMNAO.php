<?php

class cCAMPO_SIMNAO extends cCAMPO {
    public $valorCustomizado;
    public $checked;
    public function __construct($pCampoBD, $pLabel, $pLabel_en_us = '') {
        parent::__construct($pCampoBD, $pLabel, cCAMPO::cpSIMNAO, $pCampoBD, 0, false, '', 0, '', 0, 1, 0, 0, 0, $pLabel_en_us);
        $this->valorCustomizado = false;
    }

    public function HTMLEmLista() {
        $ret = '';

        if (cHTTP::getLang() == 'en_us') {
            $sim = "Yes";
            $nao = "No";
        } else {
            $sim = "Sim";
            $nao = "Não";
        }

        if (intval($this->mValor) != 0) {
            $ret = '(X) ' . $sim . '&nbsp;(&nbsp;)' . $nao;
        } else {
            $ret = '(&nbsp;)&nbsp;' . $sim . '&nbsp;&nbsp;(X)' . $nao;
        }
        return $ret;
    }

    public function FormatoMustache() {
        $saida = array();
        $saida["cpSIMNAO"] = true;
        $saida["label"] = $this->Label();
        $saida["nome"] = $this->mNome;
        $saida["valor"] = "1";
        if ($this->mValor == 1) {
            $saida["checked"] = "checked";
        }
        return $saida;
    }

}
