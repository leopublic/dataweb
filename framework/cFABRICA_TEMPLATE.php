<?php
/**
 * Description of mustache_engine
 *
 * @author Leonardo
 */
class cFABRICA_TEMPLATE{
	private static $xsingleton;
	/**
	 * @return Mustache_engine Description
	 */
	public static function singleton(){
		if (!is_object(self::$xsingleton)){
			self::$xsingleton = new Mustache_Engine(
				array(
					'loader' => new Mustache_Loader_FilesystemLoader('../views')
				,   'charset' => 'UTF-8'
				,	'partials_loader' => new Mustache_Loader_FilesystemLoader('../views')
					)
			);
		}
		return self::$xsingleton;
	}
}
