<?php
class cCAMPO_SELETOR extends cCAMPO{
	protected $mValorChave;	// Para seletor
	
	public function set_valorChave($pValor){
		$this->mValorChave = $pValor;
	}
	
	public function get_valorChave(){
		return $this->mValorChave;
	}
	
	public function __construct() {
		parent::__construct('SELETOR', '-', cCAMPO::cpSELETOR, 'SELETOR', 0, false, '', 0, '' , 0, 1, 0, 0 , 0);
	}
	
}
