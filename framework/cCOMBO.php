<?php
class cCOMBO{
	const cmbTIPO_AUTORIZACAO = 1;
	const cmbESTADO_CIVIL = 2;
	const cmbSERVICO = 3;
	const cmbSTATUS_SOLICITACAO = 4;
	const cmbNACIONALIDADE = 5;
	const cmbPAIS = 6;
	const cmbESCOLARIDADE = 7;
	const cmbREPARTICAO_CONSULAR = 8;
	const cmbPROFISSAO = 9;
	const cmbEMPRESA = 10;
	const cmbEMBARCACAO_PROJETO = 11;
	const cmbFUNCAO = 12;
	const cmbCLASSIFICA_VISTO = 13;
	const cmbTRANSPORTE_ENTRADA= 14;
	const cmbTIPO_SERVICO= 15;
	const cmbSEXO= 16;
	const cmbTIPO_ARQUIVO= 17;
	const cmbUF = 18;
	const cmbRESPONSAVEL = 19;
	const cmbSTATUS_ANDAMENTO = 20;
	const cmbSTATUS_CONCLUSAO = 21;
	const cmbEMPRESA_ATIVA = 22;
	const cmbTIPO_COMBO = 23;
	const cmbTIPO_CAMPO = 24;
	const cmbPROCESSO_MTE = 25;
	const cmbCANDIDATO_PROCESSO = 26;
	const cmbNACIONALIDADE_EN = 27;
	const cmbESCOLARIDADE_EN = 28;
	const cmbESTADO_CIVIL_EN = 29;
	const cmbSEXO_EN= 30;
	const cmbPAIS_EN = 31;
	const cmbLOCAL_PROJETO = 32;
	const cmbSERVICO_SIMPLES = 33;
	const cmbTipoEnsino = 34;
	const cmbEDICAO = 35;
	const cmbSERVICO_TIPO_1 = 36;
	const cmbPONTO_FOCAL = 37;
	const cmbTIPO_EMBARCACAO_PROJETO = 38;
	const cmbSITUACAO_CADASTRAL_CANDIDATO = 39;
	const cmbEMBARCADO = 40;
	const cmbPROXIMOS_18_MESES = 41;
	const cmbTIPO_CANDIDATO = 42;
	const cmbSITUACAO_VISTO = 43;
	const cmbPONTO_FOCAL_ATIVO = 44;
	const cmbSITUACAO_COBRANCA = 45;
	const cmbSITUACAO_RESUMO_COBRANCA = 46;
	const cmbTABELAS = 47;
	const cmbEMBARCACAO_PROJETO_ATIVOS = 48;
	const cmbTABELA_PRECO = 49;
	const cmbTIPO_ACOMPANHAMENTO = 50;
	const cmbCANDIDATO_OS = 51;
	const cmbTIPO_PRECO = 52;
	const cmbEMBARCACAO_PROJETO_COBRANCA = 53;
	const cmbTRAVEL_REASON = 54;
	const cmbVISA_ENTRIES = 55;
	const cmbPASSPORT_REQUEST = 56;
	const cmbACCOUNTING_CATEGORY = 57;
	const cmbPERFIL = 58;
	const cmbSITUACAO_REVISAO_CANDIDATO = 59;
	const cmbSITUACAO_EXCLUSAO_CANDIDATO = 60;
	const cmbSERVICO_CANCELAMENTO = 61;
	const cmbSERVICO_TIPO_4 = 62;
	const cmbSIM_NAO = 63;
	const cmbULTIMOS_PROTOCOLOS_BSB = 64;
	const cmbEMBARCACAO_PROJETO_COM_REVISAO= 65;
	const cmbEMPRESA_FATURAMENTO= 66;
	const cmbEMPRESA_TERCEIRIZADA = 67;
	const cmbGRAU_PARENTESCO = 68;
	const cmbSITUACAO = 69;
	const cmbTIPO_FORMULARIO = 70;
	const cmbPROCURADOR = 71;
	const cmbSERVICO_ATIVO = 72;
	const cmbORGAO_PROTOCOLO = 73;
	const cmbDELEGACIA_PF = 74;
	const cmbMES = 75;
	const cmbSERVICO_TIMESHEET = 76;
	const cmbSERVICO_DESPESAS_ADICIONAIS = 77;
	const cmbPACOTE = 78;
	const cmbUF_PELO_CODIGO = 79;
	const cmbEMPRESA_COBRANCA = 80;
	const cmbSTATUS_SOLICITACAO_PROCESSOS = 81;
	const cmbTIPO_ENVIO = 82;
	const cmbTIPO_ENVIO_NAO_DIGITAL = 83;
	const cmbPERFILTAXA = 84;
	const cmbEMPRESA_PRESTADORA = 85;
	const cmbEMPRESA_DA_PRESTADORA = 86;
	const cmbCONFIGURACAO_RELATORIO = 87;
	const cmbPROCESSO_PRORROG = 88;

	public $mTipoCombo ;
	public $mExtra ;
	public $mTabela;
	public $mCampoChave;
	public $mCampoDescricao;
	public $mCampoDescricao_en_us;
	public $mPermiteDefault;
	public $mValorDefault;
	public $mOrdem;
	public $mOrdem_en_us;
	public $mCampoPai;
	public static $config=array(
		  array(" "," "," ", " ", "")
		, array("TIPO_AUTORIZACAO","CO_TIPO_AUTORIZACAO","NO_REDUZIDO_TIPO_AUTORIZACAO", "", "", "", "")
		, array("ESTADO_CIVIL","CO_ESTADO_CIVIL","NO_ESTADO_CIVIL", "", "", "NO_ESTADO_CIVIL_EM_INGLES", "")
//		, array("TIPO_SERVICO TS JOIN SERVICO S ON S.NU_TIPO_SERVICO = TS.NU_TIPO_SERVICO","NU_SERVICO","concat(CO_TIPO_SERVICO , concat('.', concat(lpad(CO_SERVICO,2,'0' ), concat(' ', NO_SERVICO_RESUMIDO))))", "", "ORDER BY NO_SERVICO_RESUMIDO")
		, array("SERVICO","NU_SERVICO","NO_SERVICO_RESUMIDO", "", "", "", "")
		, array("STATUS_SOLICITACAO","ID_STATUS_SOL","NO_STATUS_SOL", "", "", "", "")
		, array("PAIS_NACIONALIDADE","CO_PAIS","CONCAT(NO_NACIONALIDADE,' (',NO_PAIS,')')", "where CO_PAIS_PF <> ''", "", "CONCAT(NO_NACIONALIDADE_EM_INGLES,' (',NO_PAIS_EM_INGLES,')')", "")
		, array("PAIS_NACIONALIDADE","CO_PAIS","NO_PAIS", "", "", "NO_PAIS_EM_INGLES", "")
		, array("ESCOLARIDADE","CO_ESCOLARIDADE","NO_ESCOLARIDADE", "", "", "NO_ESCOLARIDADE_EM_INGLES", "")
		, array("REPARTICAO_CONSULAR","CO_REPARTICAO_CONSULAR","NO_REPARTICAO_CONSULAR", "WHERE CO_REPARTICAO_CONSULAR>999", "", "", "")
		, array("PROFISSAO","CO_PROFISSAO","NO_PROFISSAO", "", "", "NO_PROFISSAO_EM_INGLES", "")
		, array("EMPRESA","NU_EMPRESA","NO_RAZAO_SOCIAL", "", " ORDER BY NO_RAZAO_SOCIAL", "", "")
		, array("EMBARCACAO_PROJETO","NU_EMBARCACAO_PROJETO","NO_EMBARCACAO_PROJETO", "", "ORDER BY embp_fl_ativo desc, NO_EMBARCACAO_PROJETO", "", "")
		, array("FUNCAO_CARGO","CO_FUNCAO","NO_FUNCAO", "", "", "NO_FUNCAO_EM_INGLES", "")
		, array("CLASSIFICACAO_VISTO","CO_CLASSIFICACAO_VISTO","NO_CLASSIFICACAO_VISTO", "", "", "", "")
		, array("TRANSPORTE_ENTRADA","NU_TRANSPORTE_ENTRADA","NO_TRANSPORTE_ENTRADA", "", "", "", "")
		, array("TIPO_SERVICO","NU_TIPO_SERVICO","NO_TIPO_SERVICO", "", "", "", "")
		, array("sexo","CO_SEXO","sexo_no_nome", "", "", "sexo_no_nome_ingles", "")
		, array("TIPO_ARQUIVO","ID_TIPO_ARQUIVO","CO_TIPO_ARQUIVO", "", "", "", "")
		, array("estado","nu_estado","no_nome", "", "", "", "")
		, array("usuarios","cd_usuario","nome", " WHERE cd_perfil < 10 or cd_usuario in (select cd_usuario from perfil_usuario where cd_perfil <10)", "", "", "")
		, array("STATUS_ANDAMENTO","ID_STATUS_ANDAMENTO","NO_STATUS_ANDAMENTO", "", "", "", "")
		, array("STATUS_CONCLUSAO","ID_STATUS_CONCLUSAO","NO_STATUS_CONCLUSAO", "", "", "", "")
		, array("EMPRESA","NU_EMPRESA","NO_RAZAO_SOCIAL", "WHERE FL_ATIVA = 1", "", "", "")
		, array("Tipo_Combo","id_tipo_combo","no_nome", "", "", "", "")
		, array("Tipo_Campo","id_tipo_campo","no_tipo_campo", "", "", "", "")
		, array("vprocesso_mte","codigo","nu_processo_visto", "", "ORDER BY nu_processo", "", "")
		, array("CANDIDATO","NU_CANDIDATO", "concat(NOME_COMPLETO, ' (', NU_CANDIDATO, ')') NOME_COMPLETO","", "ORDER BY NOME_COMPLETO", "", "")
		, array("PAIS_NACIONALIDADE","CO_PAIS","CONCAT(NO_NACIONALIDADE_EM_INGLES,' (',NO_PAIS_EM_INGLES,')')", "where CO_PAIS_PF <> ''", " ORDER BY NO_NACIONALIDADE_EM_INGLES ", "", "")
		, array("ESCOLARIDADE","CO_ESCOLARIDADE","NO_ESCOLARIDADE_EM_INGLES", "", "", "", "")
		, array("ESTADO_CIVIL","CO_ESTADO_CIVIL","NO_ESTADO_CIVIL_EM_INGLES", "", "", "", "")
		, array("--","--","--", "", "", "", "")
		, array("PAIS_NACIONALIDADE","CO_PAIS","NO_PAIS_EM_INGLES", "", "", "", "")
		, array("local_projeto","loca_id","loca_no_nome", "", "", "", "")
		, array("SERVICO","NU_SERVICO","NO_SERVICO_RESUMIDO", "", "", "", "")
		, array("tipo_ensino","tpen_id","tpen_no_nome", "", "", "tpen_no_nome_en_us", "")
		, array("Edicao","id_edicao","no_edicao", "", "", "", "")
		, array("SERVICO","NU_SERVICO","NO_SERVICO_RESUMIDO", "where ID_TIPO_ACOMPANHAMENTO=1", "", "", "")
		, array("usuarios","cd_usuario","nome", " WHERE cd_perfil < 10 ", "", "", "")
		, array("TIPO_EMBARCACAO_PROJETO","ID_TIPO_EMBARCACAO_PROJETO","NO_TIPO_EMBARCACAO_PROJETO", "", "", "", "")
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("usuarios","cd_usuario","nome", " WHERE cd_usuario in (select distinct cd_usuario from EMBARCACAO_PROJETO) ", "", "", "")
		, array("status_cobranca_os","stco_id","stco_tx_nome", " ", "", "", "")
		, array("status_resumo_cobranca","strc_id","strc_tx_nome", " ", "", "", "")
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("EMBARCACAO_PROJETO","NU_EMBARCACAO_PROJETO","NO_EMBARCACAO_PROJETO", " where embp_fl_ativo = 1", "", "", "")
		, array("tabela_precos","tbpc_id","concat(tbpc_tx_nome, ' (',tbpc_nu_ano,')') tbpc_tx_nome", "", "", "", "")
		, array("TIPO_ACOMPANHAMENTO","ID_TIPO_ACOMPANHAMENTO","NO_TIPO_ACOMPANHAMENTO", "", "", "", "")
		, array("--","--","--", "", "", "", "")	// Candidato OS
		, array("tipo_preco","tppr_id","tppr_tx_nome", "", "", "", "")	// Tipo preco
		, array("EMBARCACAO_PROJETO","NU_EMBARCACAO_PROJETO","NO_EMBARCACAO_PROJETO", "", "ORDER BY embp_fl_ativo desc, NO_EMBARCACAO_PROJETO", "", "")
		, array("--","--","--", "", "", "", "")	// Travel reason
		, array("--","--","--", "", "", "", "")	// Visa entries
		, array("--","--","--", "", "", "", "")	// Passport request
		, array("--","--","--", "", "", "", "")	// Accounting category
		, array("--","--","--", "", "", "", "")	// Perfil
		, array("--","--","--", "", "", "", "")	// Situacao revisao candidato
		, array("--","--","--", "", "", "", "")	// Situacao exclusao candidato
		, array("SERVICO","NU_SERVICO","NO_SERVICO_RESUMIDO", "where ID_TIPO_ACOMPANHAMENTO=6", "", "", "")// Servico cancelamento
		, array("SERVICO","NU_SERVICO","NO_SERVICO_RESUMIDO", "where ID_TIPO_ACOMPANHAMENTO=4", "", "", "")// Servico registro
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("--","--","--", "", "", "", "")	// Proximos 18 meses
		, array("SERVICO","NU_SERVICO","NO_SERVICO_RESUMIDO", "where serv_fl_ativo=1", "", "", "")// Servico registro
		, array("--","--","--", "", "", "", "")	// 73
		, array("--","--","--", "", "", "", "")	// 74
		, array("--","--","--", "", "", "", "")	// 75
		, array("--","--","--", "", "", "", "")	// 76
		, array("--","--","--", "", "", "", "")	// 77
		, array("--","--","--", "", "", "", "")	// 78
		, array("--","--","--", "", "", "", "")	// 79
        , array("EMPRESA","nu_empresa","no_razao_social", "where coalesce(fl_ativa, 0) = 1 and coalesce(empr_fl_faturavel, 0) = 1", "order by no_razao_social", "", "")	// 80
		, array("STATUS_SOLICITACAO","ID_STATUS_SOL","NO_STATUS_SOL", "where fl_processos=1", "order by id_status_sol", "", "")	// 81
		, array("tipo_envio","id_tipo_envio","descricao", "", "order by descricao", "", "")	// 82
		, array("tipo_envio","id_tipo_envio","descricao", "where fl_digital = 0", "order by descricao", "", "")	// 83


		);

	public $meses = array("", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro","Novembro", "Dezembro");

	public function __construct ($pTipo){
		$this->mTipoCombo = $pTipo;
		$this->mTabela = self::$config[$pTipo][0];
		$this->mCampoChave = self::$config[$pTipo][1];
		$this->mCampoDescricao = self::$config[$pTipo][2];
		$this->mCampoDescricao_en_us = self::$config[$pTipo][5];
		$this->mExtra= self::$config[$pTipo][3];
        $this->mOrdem= self::$config[$pTipo][4];
        $this->mOrdem_en_us= self::$config[$pTipo][6];
        $this->mPermiteDefault = true;
		$this->mValorDefault = "(selecione...)";
	}

	public static function campoHTML($pTipo, $pValor="0", $pExtra="", $pNome="", $pClasse=""  ){
		$tabela = self::$config[$pTipo][0];
		$indice = self::$config[$pTipo][1];
		$nome = self::$config[$pTipo][2];
		$extra = self::$config[$pTipo][3];
        $ordem = (self::$config[$pTipo][4]!=''?self::$config[$pTipo][4]:"order by $nome");
		if($pNome == ""){
			$pNome =$indice ;
		}

		$ret = "";
		$sql = "SELECT $indice,$nome from $tabela $extra $ordem";

//        die("SQL : $sql");

		$rs = mysql_query($sql);
		if(mysql_errno()>0) {
			$ret="ERRO:".mysql_error()."SQL=".$sql;
		}
		else {
			if ($pTipo == self::cmbEMBARCACAO_PROJETO){
				$ret .= '<div id="tdEmbarcacaoProjeto">';
			}
			if ($pClasse != '' ){
				$class = ' class="'.$pClasse.'" ';
			}
			else{
				$class = '';
			}
			$ret .= '<select id="'.$pNome.'" name="'.$pNome.'" '.$class.'>';
			while($rw=mysql_fetch_array($rs)) {
				if( (strlen($pValor)>0) && ($rw[0]==$pValor) ) {
					$sel = " selected";
				}
				else {
					$sel="";
				}
    			$ret .= "<option value='".$rw[0]."'$sel>".trim(substr($rw[1],0,50));
			}
			$ret.= '</select>';
			if ($pTipo == self::cmbEMBARCACAO_PROJETO){
				$ret .= '</div>';
			}

		}
		return $ret;
	}

	public function HTML($pNome, $pValor, $pDisabled = false, $pClasse=""  ){
		$tabela = $this->mTabela;
		$indice = $this->mCampoChave;
		$nome_pt_br = $this->mCampoDescricao;
		$nome_en_us = $this->mCampoDescricao_en_us;
		$extra = $this->mExtra;
		$ordem = $this->mOrdem;

		$ret = "";
		if ($this->mTipoCombo == self::cmbLOCAL_PROJETO){
			$ret .= '<select id="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'" name="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'">';
			$ret .= '<option value="Macae">Macaé</option>';
			$ret .= '<option value="Vitoria">Vitória</option>';
			$ret .= '<option value="Rio de Janeiro">Rio de Janeiro</option>';
			$ret.= '</select>';
		}
		elseif ($this->mTipoCombo == self::cmbSITUACAO_CADASTRAL_CANDIDATO){
			$ret .= '<select id="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'" name="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'">';
			if ($this->mPermiteDefault){
				$ret .= '<option value="">'.$this->mValorDefault.'</option>';
			}
			if( (strlen($pValor)>0) && ($pValor=="0") ) {
				$sel = " selected ";
			}
			else {
				$sel="";
			}
			$ret .= '<option value="0"'.$sel.'>Ativo</option>';
			if( (strlen($pValor)>0) && ($pValor=="1") ) {
				$sel = " selected ";
			}
			else {
				$sel="";
			}
			$ret .= '<option value="1"'.$sel.'>Inativo</option>';
			$ret.= '</select>';
		}
		elseif ($this->mTipoCombo == self::cmbEMBARCADO){
			$ret .= '<select id="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'" name="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'">';
			if ($this->mPermiteDefault){
				$ret .= '<option value="">'.$this->mValorDefault.'</option>';
			}
			if( (strlen($pValor)>0) && ($pValor=="1") ) {
				$sel = " selected ";
			}
			else {
				$sel="";
			}
			$ret .= '<option value="1"'.$sel.'>Sim</option>';
			if( (strlen($pValor)>0) && ($pValor=="0") ) {
				$sel = " selected ";
			}
			else {
				$sel="";
			}
			$ret .= '<option value="0"'.$sel.'>Não</option>';
			$ret.= '</select>';
		}
		elseif ($this->mTipoCombo == self::cmbPROXIMOS_18_MESES){
			$ret .= '<select id="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'" name="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'">';
			if ($this->mPermiteDefault){
				if( $pValor == '' ) {
					$sel = " selected ";
				}
				else {
					$sel="";
				}
				$ret .= '<option value=""'.$sel.'>'.$this->mValorDefault.'</option>';
			}
			$mes = intval(date('n'));
			$ano = date('Y');
			for($i=0;$i<19;$i++){
				if( strval($pValor) == strval($i) ) {
					$sel = " selected ";
				}
				else {
					$sel="";
				}
				$mes_ano = $this->meses[$mes].'/'.$ano;
				$ret .= '<option value="'.$i.'"'.$sel.'>'.$mes_ano.'</option>';
				if ($mes == 12){
					$mes = 0;
					$ano++;
				}
				$mes++;
			}
			$ret.= '</select>';
		}
		elseif ($this->mTipoCombo == self::cmbTIPO_CANDIDATO){
			$ret .= '<select id="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'" name="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'">';
			if ($this->mPermiteDefault){
				$ret .= $this->Opcao($pValor, "", $this->mValorDefault);
			}
			$ret .= $this->Opcao($pValor, "1", "somente quem tem visto");
			$ret .= $this->Opcao($pValor, "2", "somente quem não tem visto");
			$ret.= '</select>';
		}
		elseif ($this->mTipoCombo == self::cmbSITUACAO_VISTO){
			$ret .= '<select id="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'" name="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'">';
			if ($this->mPermiteDefault){
				$ret .= $this->Opcao($pValor, "", $this->mValorDefault);
			}
			$ret .= $this->Opcao($pValor, "1", "visto válido");
			$ret .= $this->Opcao($pValor, "2", "visto vencido");
			$ret.= '</select>';
		}
		else{
			if ($this->mTipoCombo == self::cmbEMPRESA_ATIVA && $pValor != '' ){
				$extra = $extra. " OR ".$this->mCampoChave." = ".$pValor;
			}
			if ($nome_en_us != ''){
				$xnome_en_us = ','.$nome_en_us;
			}
			else{
				$xnome_en_us = ','.$nome_pt_br;
			}

			if ($ordem == '' ){
				if(cHTTP::getLang() == 'pt_br'){
					$ordem = "order by 2";
				}
				else{
					$ordem = " order by 3";
				}
			}
			else{
				if(cHTTP::getLang() == 'en_us'){
					$ordem = $this->mOrdem_en_us;
				}
			}

			if ($this->mTipoCombo == self::cmbEMBARCACAO_PROJETO && is_object($this->mCampoPai) ){
				$extra = ' where '.$this->mCampoPai->mCampoBD.'='.$this->mCampoPai->mValor;
			}
			$sql = "SELECT $indice,$nome_pt_br $xnome_en_us from $tabela $extra $ordem";
			if ($this->mTipoCombo == self::cmbEMBARCACAO_PROJETO_COBRANCA && is_object($this->mCampoPai) ){
				$extra = ' where '.$this->mCampoPai->mCampoBD.'='.$this->mCampoPai->mValor;
			}
			$sql = "SELECT $indice,$nome_pt_br $xnome_en_us from $tabela $extra $ordem";

//	        var_dump($sql);

			$rs = mysql_query($sql);
			if(mysql_errno()>0) {
				$ret="ERRO:".mysql_error()."SQL=".$sql;
			}
			else{
				if ($this->mTipoCombo == self::cmbEMBARCACAO_PROJETO){
					$ret .= '<div id="tdEmbarcacaoProjeto">';
				}
				if ($this->mTipoCombo == self::cmbEMBARCACAO_PROJETO_COBRANCA){
					$ret .= '<div id="tdEmbarcacaoProjetoCobranca">';
				}
				if ($pDisabled){
					$disabled = ' disabled="disabled"';
				}
				else{
					$disabled = '';
				}
				if ($pClasse != '' ){
					$class = ' class="'.$pClasse.'" ';
				}
				else{
					$class = '';
				}
				$ret .= '<select id="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'" name="'.cINTERFACE::PREFIXO_CAMPOS.$pNome.'" '.$disabled.' '.$class.'>';
				if ($this->mPermiteDefault){
					$ret .= '<option value="">'.$this->mValorDefault.'</option>';
				}
				while($rw=mysql_fetch_array($rs)) {
					if( (strlen($pValor)>0) && ($rw[0]==$pValor) ) {
						$sel = "selected";
					}
					else {
						$sel="";
					}
					if (cHTTP::getLang()=='pt_br'){
						$ind = 1 ;
					}
					else{
						$ind = 2;
					}
					$ret = $ret."<option value='".$rw[0]."' $sel>".trim(substr($rw[$ind],0,50));
				}
				$ret.= '</select>';
				if ($this->mTipoCombo == self::cmbEMBARCACAO_PROJETO ||$this->mTipoCombo == self::cmbEMBARCACAO_PROJETO_COBRANCA){
					$ret .= '</div>';
				}
			}

		}
		return $ret;
	}

	public function Opcao($pValorInformado, $pValor, $pDescricao){
		$sel = "" ;
		if( $pValorInformado != ''){
			if ($pValorInformado == $pValor) {
				$sel = " selected ";
			}
		}
		$ret = '<option value="'.$pValor.'"'.$sel.'>'.$pDescricao.'</option>';
		return $ret;
	}
}
