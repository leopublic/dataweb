<?php

/**
 * Versão especial do combo que é alimentado por select de uma tabela
 */
class cCOMBO_TABELA extends cCOMBO_GERAL {

    public $mExtra;
    public $mTabela;
    public $mCampoChave;
    public $mCampoDescricao;
    public $mCampoDescricao_en_us;
    public $mOrdem;
    public $mOrdem_en_us;
    public $colunaPai;
    public $campoPai;

    public function __construct($pCampoBD, $pLabel, $pTipoCombo, $pLabel_en_us) {
        parent::__construct($pCampoBD, $pLabel, $pLabel_en_us);

        $this->mTipoCombo = $pTipoCombo;
        if (cHTTP::getLang() != "pt_br") {
            $this->mValorDefault = "(not available)";
        }

        switch ($pTipoCombo) {
            case cCOMBO::cmbTIPO_AUTORIZACAO:
                $this->mTabela = "TIPO_AUTORIZACAO";
                $this->mCampoChave = "CO_TIPO_AUTORIZACAO";
                $this->mCampoDescricao = "NO_REDUZIDO_TIPO_AUTORIZACAO";
                break;
            case cCOMBO::cmbESTADO_CIVIL:
                $this->mTabela = "ESTADO_CIVIL";
                $this->mCampoChave = "CO_ESTADO_CIVIL";
                $this->mCampoDescricao = "NO_ESTADO_CIVIL";
                $this->mCampoDescricao_en_us = "NO_ESTADO_CIVIL_EM_INGLES";
                $this->mPermiteDefault = false;
                break;
            case cCOMBO::cmbSERVICO:
            case cCOMBO::cmbSERVICO_SIMPLES:
                $this->mTabela = "SERVICO";
                $this->mCampoChave = "NU_SERVICO";
                $this->mCampoDescricao = "NO_SERVICO_RESUMIDO";
                break;
            case cCOMBO::cmbSERVICO_TIPO_1:
                $this->mTabela = "SERVICO";
                $this->mCampoChave = "NU_SERVICO";
                $this->mCampoDescricao = "NO_SERVICO_RESUMIDO";
                $this->mExtra = "and ID_TIPO_ACOMPANHAMENTO = 1";
                break;
            case cCOMBO::cmbSERVICO_TIPO_4:
                $this->mTabela = "SERVICO";
                $this->mCampoChave = "NU_SERVICO";
                $this->mCampoDescricao = "NO_SERVICO_RESUMIDO";
                $this->mExtra = "and ID_TIPO_ACOMPANHAMENTO=4";
                break;
            case cCOMBO::cmbSTATUS_SOLICITACAO:
                $this->mTabela = "STATUS_SOLICITACAO";
                $this->mCampoChave = "ID_STATUS_SOL";
                $this->mCampoDescricao = "NO_STATUS_SOL";
                $this->mOrdem = "id_status_sol";
                break;
            case cCOMBO::cmbNACIONALIDADE:
            case cCOMBO::cmbNACIONALIDADE_EN:
                $this->mTabela = "PAIS_NACIONALIDADE";
                $this->mCampoChave = "CO_PAIS";
                $this->mCampoDescricao = "CONCAT(NO_NACIONALIDADE,' (',NO_PAIS,')')";
                $this->mCampoDescricao_en_us = "CONCAT(NO_NACIONALIDADE_EM_INGLES,' (',NO_PAIS_EM_INGLES,')')";
                $this->mOrdem = "NO_NACIONALIDADE";
                $this->mOrdem_en_us = "NO_NACIONALIDADE_EM_INGLES";
                $this->mExtra = " and CO_PAIS_PF <> ''";
                $this->mPermiteDefault = false;
                break;
            case cCOMBO::cmbPAIS:
            case cCOMBO::cmbPAIS_EN:
                $this->mTabela = "PAIS_NACIONALIDADE";
                $this->mCampoChave = "CO_PAIS";
                $this->mCampoDescricao = "NO_PAIS";
                $this->mCampoDescricao_en_us = "coalesce(NO_PAIS_EM_INGLES, NO_PAIS)";
                $this->mExtra = " and CO_PAIS_PF <> '' ";
                $this->mPermiteDefault = false;
                break;
            case cCOMBO::cmbESCOLARIDADE:
            case cCOMBO::cmbESCOLARIDADE_EN:
                $this->mTabela = "ESCOLARIDADE";
                $this->mCampoChave = "CO_ESCOLARIDADE";
                $this->mCampoDescricao = "NO_ESCOLARIDADE";
                $this->mCampoDescricao_en_us = "NO_ESCOLARIDADE_EM_INGLES";
                $this->mPermiteDefault = false;
                break;
            case cCOMBO::cmbREPARTICAO_CONSULAR:
                $this->mTabela = "REPARTICAO_CONSULAR";
                $this->mCampoChave = "CO_REPARTICAO_CONSULAR";
                $this->mCampoDescricao = "NO_REPARTICAO_CONSULAR";
                $this->mExtra = "and CO_REPARTICAO_CONSULAR>999";
                break;
            case cCOMBO::cmbPROFISSAO:
                $this->mTabela = "PROFISSAO";
                $this->mCampoChave = "CO_PROFISSAO";
                $this->mCampoDescricao = "NO_PROFISSAO";
                $this->mPermiteDefault = false;
                break;
            case cCOMBO::cmbEMPRESA:
                $this->mTabela = "EMPRESA";
                $this->mCampoChave = "NU_EMPRESA";
                $this->mCampoDescricao = "NO_RAZAO_SOCIAL";
                break;
            case cCOMBO::cmbEMBARCACAO_PROJETO:
                $this->mTabela = "EMBARCACAO_PROJETO";
                $this->mCampoChave = "NU_EMBARCACAO_PROJETO";
                $this->mCampoDescricao = "NO_EMBARCACAO_PROJETO";
                $this->colunaPai = "NU_EMPRESA";
                break;
            case cCOMBO::cmbFUNCAO:
                $this->mTabela = "FUNCAO_CARGO";
                $this->mCampoChave = "CO_FUNCAO";
                $this->mCampoDescricao = "NO_FUNCAO";
                $this->mCampoDescricao_en_us = "NO_FUNCAO_EM_INGLES";
                $this->mPermiteDefault = false;
                break;
            case cCOMBO::cmbCLASSIFICA_VISTO:
                $this->mTabela = "CLASSIFICACAO_VISTO";
                $this->mCampoChave = "CO_CLASSIFICACAO_VISTO";
                $this->mCampoDescricao = "NO_CLASSIFICACAO_VISTO";
                break;
            case cCOMBO::cmbTRANSPORTE_ENTRADA:
                $this->mTabela = "TRANSPORTE_ENTRADA";
                $this->mCampoChave = "NU_TRANSPORTE_ENTRADA";
                $this->mCampoDescricao = "NO_TRANSPORTE_ENTRADA";
                break;
            case cCOMBO::cmbTIPO_SERVICO:
                $this->mTabela = "TIPO_SERVICO";
                $this->mCampoChave = "NU_TIPO_SERVICO";
                $this->mCampoDescricao = "NO_TIPO_SERVICO";
                break;
            case cCOMBO::cmbSEXO:
                $this->mTabela = "sexo";
                $this->mCampoChave = "CO_SEXO";
                $this->mCampoDescricao = "sexo_no_nome";
                $this->mCampoDescricao_en_us = "sexo_no_nome_ingles";
                break;
            case cCOMBO::cmbTIPO_ARQUIVO:
                $this->mTabela = "TIPO_ARQUIVO";
                $this->mCampoChave = "ID_TIPO_ARQUIVO";
                $this->mCampoDescricao = "concat(CO_TIPO_ARQUIVO, '-', NO_TIPO_ARQUIVO)";
                break;
            case cCOMBO::cmbUF:
                $this->mTabela = "estado";
                $this->mCampoChave = "nu_estado";
                $this->mCampoDescricao = "no_nome";
                break;
            case cCOMBO::cmbPONTO_FOCAL:
            case cCOMBO::cmbRESPONSAVEL:
                $this->mTabela = "usuarios";
                $this->mCampoChave = "cd_usuario";
                $this->mCampoDescricao = "nome";
                $this->mExtra = "and ( cd_perfil < 10 or cd_usuario in (select cd_usuario from perfil_usuario where cd_perfil <10) )";
                break;
            case cCOMBO::cmbSTATUS_ANDAMENTO:
                $this->mTabela = "STATUS_ANDAMENTO";
                $this->mCampoChave = "ID_STATUS_ANDAMENTO";
                $this->mCampoDescricao = "NO_STATUS_ANDAMENTO";
                break;
            case cCOMBO::cmbSTATUS_CONCLUSAO:
                $this->mTabela = "STATUS_CONCLUSAO";
                $this->mCampoChave = "ID_STATUS_CONCLUSAO";
                $this->mCampoDescricao = "NO_STATUS_CONCLUSAO";
                break;
            case cCOMBO::cmbEMPRESA_ATIVA:
                $this->mTabela = "EMPRESA";
                $this->mCampoChave = "NU_EMPRESA";
                $this->mCampoDescricao = "NO_RAZAO_SOCIAL";
                $this->mExtra = "and FL_ATIVA = 1";
                break;
            case cCOMBO::cmbTIPO_COMBO:
                $this->mTabela = "Tipo_Combo";
                $this->mCampoChave = "id_tipo_combo";
                $this->mCampoDescricao = "no_nome";
                break;
            case cCOMBO::cmbTIPO_CAMPO:
                $this->mTabela = "Tipo_Campo";
                $this->mCampoChave = "id_tipo_campo";
                $this->mCampoDescricao = "no_tipo_campo";
                break;
            case cCOMBO::cmbPROCESSO_MTE:
                $this->mTabela = "vprocesso_mte";
                $this->mCampoChave = "codigo";
                $this->mCampoDescricao = "ifnull(nu_processo_visto, '(processo sem número)') nu_processo_visto";
                $this->mOrdem = " nu_processo";
                break;
            case cCOMBO::cmbPROCESSO_PRORROG:
                $this->mTabela = "vprocesso_prorrog";
                $this->mCampoChave = "codigo";
                $this->mCampoDescricao = "ifnull(nu_protocolo_visto, '(processo sem número)') nu_protocolo_visto";
                $this->mOrdem = " codigo desc";
                break;
            case cCOMBO::cmbCANDIDATO_PROCESSO:
                $this->mTabela = "CANDIDATO";
                $this->mCampoChave = "NU_CANDIDATO";
                $this->mCampoDescricao = "concat(NOME_COMPLETO, ' (', NU_CANDIDATO, ')') NOME_COMPLETO";
                $this->mOrdem = "NOME_COMPLETO";
                break;
            case cCOMBO::cmbSERVICO_SIMPLES:
                $this->mTabela = "REPARTICAO_CONSULAR";
                $this->mCampoChave = "CO_REPARTICAO_CONSULAR";
                $this->mCampoDescricao = "NO_REPARTICAO_CONSULAR";
                $this->mCampoDescricao_en_us = "NO_ESCOLARIDADE_EM_INGLES";
                $this->mExtra = "and CO_REPARTICAO_CONSULAR>999";
                break;
            case cCOMBO::cmbTipoEnsino:
                $this->mTabela = "tipo_ensino";
                $this->mCampoChave = "tpen_id";
                $this->mCampoDescricao = "tpen_no_nome";
                $this->mCampoDescricao_en_us = "tpen_no_nome_en_us";
                break;
            case cCOMBO::cmbEDICAO:
                $this->mTabela = "Edicao";
                $this->mCampoChave = "id_edicao";
                $this->mCampoDescricao = "no_edicao";
                break;
            case cCOMBO::cmbTIPO_EMBARCACAO_PROJETO:
                $this->mTabela = "TIPO_EMBARCACAO_PROJETO";
                $this->mCampoChave = "ID_TIPO_EMBARCACAO_PROJETO";
                $this->mCampoDescricao = "NO_TIPO_EMBARCACAO_PROJETO";
                break;
            case cCOMBO::cmbPONTO_FOCAL_ATIVO:
                $this->mTabela = "usuarios";
                $this->mCampoChave = "cd_usuario";
                $this->mCampoDescricao = "nome";
                $this->mExtra = "and cd_usuario in (select distinct cd_usuario from EMBARCACAO_PROJETO) ";
                break;
            case cCOMBO::cmbSITUACAO_COBRANCA:
                $this->mTabela = "status_cobranca_os";
                $this->mCampoChave = "stco_id";
                $this->mCampoDescricao = "stco_tx_nome";
                break;
            case cCOMBO::cmbTIPO_ACOMPANHAMENTO:
                $this->mTabela = "TIPO_ACOMPANHAMENTO";
                $this->mCampoChave = "ID_TIPO_ACOMPANHAMENTO";
                $this->mCampoDescricao = "NO_TIPO_ACOMPANHAMENTO";
                break;
            case cCOMBO::cmbCANDIDATO_OS:
                $this->mTabela = "AUTORIZACAO_CANDIDATO AC, CANDIDATO C ";
                $this->mCampoChave = "C.NU_CANDIDATO";
                $this->mCampoDescricao = "concat(NOME_COMPLETO, ' (', C.NU_CANDIDATO, ')') NOME_COMPLETO";
                $this->mOrdem = " NOME_COMPLETO";
                $this->mExtra = " and C.NU_CANDIDATO = AC.NU_CANDIDATO";
                $this->mValorDefault = "(geral)";
                break;
            case cCOMBO::cmbTIPO_PRECO:
                $this->mTabela = "tipo_preco";
                $this->mCampoChave = "tppr_id";
                $this->mCampoDescricao = "tppr_tx_nome";
                break;
            case cCOMBO::cmbEMBARCACAO_PROJETO_ATIVOS:
                $this->mTabela = "EMBARCACAO_PROJETO";
                $this->mCampoChave = "NU_EMBARCACAO_PROJETO";
                $this->mCampoDescricao = "NO_EMBARCACAO_PROJETO";
                $this->mExtra = "and embp_fl_ativo = 1";
                $this->colunaPai = "NU_EMPRESA";
                break;
            case cCOMBO::cmbLOCAL_PROJETO:
                $this->mTabela = "local_projeto";
                $this->mCampoChave = "local_id";
                $this->mCampoDescricao = "loca_no_nome";
                break;
            case cCOMBO::cmbTABELA_PRECO:
                $this->mTabela = "tabela_precos";
                $this->mCampoChave = "tbpc_id";
                $this->mCampoDescricao = "tbpc_tx_nome";
                break;
            case cCOMBO::cmbTRAVEL_REASON:
                $this->mTabela = "travel_reason";
                $this->mCampoChave = "trrs_id";
                $this->mCampoDescricao = "trrs_tx_nome";
                break;
            case cCOMBO::cmbVISA_ENTRIES:
                $this->mTabela = "visa_entry_type";
                $this->mCampoChave = "entt_id";
                $this->mCampoDescricao = "entt_tx_nome";
                break;
            case cCOMBO::cmbPASSPORT_REQUEST:
                $this->mTabela = "passport_request_type";
                $this->mCampoChave = "past_id";
                $this->mCampoDescricao = "past_tx_nome";
                break;
            case cCOMBO::cmbACCOUNTING_CATEGORY:
                $this->mTabela = "accounting_category";
                $this->mCampoChave = "accc_id";
                $this->mCampoDescricao = "accc_tx_nome";
                break;
            case cCOMBO::cmbPERFIL:
                $this->mTabela = "usuario_perfil";
                $this->mCampoChave = "cd_perfil";
                $this->mCampoDescricao = "nm_perfil";
                break;
            case cCOMBO::cmbSERVICO_CANCELAMENTO:
                $this->mTabela = "SERVICO";
                $this->mCampoChave = "NU_SERVICO";
                $this->mCampoDescricao = "NO_SERVICO_RESUMIDO";
                $this->mExtra = "and ID_TIPO_ACOMPANHAMENTO = 6";
                break;
            case cCOMBO::cmbULTIMOS_PROTOCOLOS_BSB:
                $this->mTabela = "vORDEM_SERVICO_PROTOCOLO";
                $this->mCampoChave = "date_format(dt_envio_bsb,'%d/%m/%Y') dt_envio_bsb_fmt";
                $this->mCampoDescricao = "concat(date_format(dt_envio_bsb,'%d/%m/%y'), ' (', case dayofweek(dt_envio_bsb)  when 1 then 'dom' when 2 then 'seg' when 3 then 'ter'  when 4 then 'qua'  when 5 then 'qui'  when 6 then 'sex'  when 7 then 'sab' end, ') - ', count(*),' processos') dt_envio_bsb_desc";
                $this->mExtra = "group by dt_envio_bsb";
                $this->mOrdem = 'dt_envio_bsb desc';
                break;
            case cCOMBO::cmbEMBARCACAO_PROJETO_COM_REVISAO:
                $this->mTabela = "EMBARCACAO_PROJETO EP left join usuarios u on u.cd_usuario = EP.cd_usuario_ult_revisao";
                $this->mCampoChave = "NU_EMBARCACAO_PROJETO";
                $this->mCampoDescricao = "concat(NO_EMBARCACAO_PROJETO, ' (', ifnull(date_format(embp_dt_ult_revisao, 'revisada em %d/%m/%y'), 'não revisada'), ifnull(concat(' por ',u.usua_tx_apelido),''), ')') NO_EMBARCACAO_PROJETO";
                $this->mExtra = "and embp_fl_ativo = 1";
                $this->colunaPai = "NU_EMPRESA";
                $this->mOrdem = 'NO_EMBARCACAO_PROJETO ';
                break;
            case cCOMBO::cmbEMPRESA_FATURAMENTO:
                $this->mTabela = "empresa_faturamento";
                $this->mCampoChave = "empf_id";
                $this->mCampoDescricao = "empf_tx_descricao";
                break;
            case cCOMBO::cmbEMPRESA_TERCEIRIZADA:
                $this->set_autocomplete(true);
                $this->set_source('/paginas/carregueSemMenu.php?controller=cCTRL_EMPRESA_TERCEIRIZADA&metodo=AtualizeCombo');
                $this->mTabela = "empresa_terceirizada";
                $this->mCampoChave = "empt_id";
                $this->mCampoDescricao = "empt_tx_descricao";
                break;
            case cCOMBO::cmbGRAU_PARENTESCO:
                $this->mTabela = "grau_parentesco";
                $this->mCampoChave = "CO_GRAU_PARENTESCO";
                $this->mCampoDescricao = "NO_GRAU_PARENTESCO";
                $this->mCampoDescricao_en_us = "NO_GRAU_PARENTESCO_EM_INGLES";
                break;
            case cCOMBO::cmbTIPO_FORMULARIO:
                $this->mTabela = "tipo_formulario a left join formulario_acompanhamento fa on fa.id_tipo_formulario = a.id_tipo_formulario";
                $this->mCampoChave = "a.id_tipo_formulario";
                $this->mCampoDescricao = "a.no_tipo_formulario";
                break;
            case cCOMBO::cmbPROCURADOR:
                $this->mTabela = "procurador_empresa ";
                $this->mCampoChave = "proc_id";
                $this->mCampoDescricao = "no_procurador";
                break;
            case cCOMBO::cmbSERVICO_ATIVO:
                $this->mTabela = "SERVICO";
                $this->mCampoChave = "NU_SERVICO";
                $this->mCampoDescricao = "NO_SERVICO_RESUMIDO";
                $this->mExtra = "and serv_fl_ativo = 1 and coalesce(serv_fl_pacote, 0) = 0";
                break;
            case cCOMBO::cmbSERVICO_TIMESHEET:
                $this->mTabela = "SERVICO";
                $this->mCampoChave = "NU_SERVICO";
                $this->mCampoDescricao = "NO_SERVICO_RESUMIDO";
                $this->mExtra = "and serv_fl_ativo = 1 and serv_fl_timesheet = 1";
                break;
            case cCOMBO::cmbSERVICO_DESPESAS_ADICIONAIS:
                $this->mTabela = "SERVICO";
                $this->mCampoChave = "NU_SERVICO";
                $this->mCampoDescricao = "NO_SERVICO_RESUMIDO";
                $this->mExtra = "and serv_fl_ativo = 1 and serv_fl_despesas_adicionais = 1";
                break;
            case cCOMBO::cmbDELEGACIA_PF:
                $this->mTabela = "delegacia_pf";
                $this->mCampoChave = "depf_id";
                $this->mCampoDescricao = "concat(depf_no_nome, ' - ', depf_cd_uf ) ";
                break;
            case cCOMBO::cmbUF_PELO_CODIGO:
                $this->mTabela = "estado";
                $this->mCampoChave = "no_sigla";
                $this->mCampoDescricao = "no_nome";
                break;
            case cCOMBO::cmbEMPRESA_COBRANCA:
                $this->mTabela = "EMPRESA";
                $this->mCampoChave = "NU_EMPRESA";
                $this->mCampoDescricao = "NO_RAZAO_SOCIAL";
                $this->mExtra = "and coalesce(FL_ATIVA, 0)=1 and coalesce(empr_fl_faturavel, 0) = 1";
                break;
            case cCOMBO::cmbSTATUS_SOLICITACAO_PROCESSOS:
                $this->mTabela = "STATUS_SOLICITACAO";
                $this->mCampoChave = "ID_STATUS_SOL";
                $this->mCampoDescricao = "NO_STATUS_SOL";
                $this->mOrdem = "id_status_sol";
                break;
            case cCOMBO::cmbTIPO_ENVIO:
                $this->mTabela = "tipo_envio";
                $this->mCampoChave = "id_tipo_envio";
                $this->mCampoDescricao = "descricao";
                $this->mOrdem = "descricao";
                break;
            case cCOMBO::cmbTIPO_ENVIO_NAO_DIGITAL:
                $this->mTabela = "tipo_envio";
                $this->mCampoChave = "id_tipo_envio";
                $this->mCampoDescricao = "descricao";
                $this->mOrdem = "descricao";
                $this->mExtra = "and fl_digital = 0";
                break;
            case cCOMBO::cmbPERFILTAXA:
                $this->mTabela = "perfiltaxa";
                $this->mCampoChave = "id_perfiltaxa";
                $this->mCampoDescricao = "descricao";
                $this->mOrdem = "descricao";
                break;
            case cCOMBO::cmbEMPRESA_PRESTADORA:
                $this->mTabela = "empresa_prestadora";
                $this->mCampoChave = "nu_empresa_prestadora";
                $this->mCampoDescricao = "nome";
                $this->mOrdem = "nome";
                break;
            case cCOMBO::cmbEMPRESA_DA_PRESTADORA:
                $this->mTabela = "EMPRESA";
                $this->mCampoChave = "NU_EMPRESA";
                $this->mCampoDescricao = "NO_RAZAO_SOCIAL";
                $this->colunaPai = "nu_empresa_prestadora";
                break;
            case cCOMBO::cmbCONFIGURACAO_RELATORIO:
                $this->mTabela = "configuracao_relatorio";
                $this->mCampoChave = "cfgr_id";
                $this->mCampoDescricao = "cfgr_nome";
                break;
        }
    }

    /**
     * Gera a relação de valores do combo, a partir das propriedades que descrevem o select. Deve ser overriden pelos combos de valores específicos.
     * @
     * @return array(array(2)) Retorna um array de arrays com chave, descricao
     */
    public function CarregarValores($pCodificar = true) {
        $sql = $this->sql_select();
        $this->InicializarValores();
        $res = cAMBIENTE::$db_pdo->query($sql);
        while ($rw = $res->fetch(PDO::FETCH_BOTH)) {
            $descricao = $rw[1];
            $this->AdicionarValor($rw[0], $descricao);
        }
    }

    public function nomeCampoDescricao() {
        $descricao = $this->mCampoDescricao;
        if (cHTTP::getLang() == 'en_us') {
            if ($this->mCampoDescricao_en_us != '') {
                $descricao = $this->mCampoDescricao_en_us;
            }
        }
        return $descricao;
    }

    public function nomeCampoOrdem() {
        $ordem = $this->mOrdem;
        if (cHTTP::getLang() == 'en_us') {
            if ($this->mOrdem_en_us != '') {
                $ordem = $this->mOrdem_en_us;
            }
        }
        if ($ordem == '') {
            $ordem = $this->nomeCampoDescricao();
        }
        return $ordem;
    }

    public function sql_select() {
        if (is_object($this->campoPai)) {
            if ($this->campoPai->mValor != '') {
                $this->mExtra .= ' and ' . $this->colunaPai . ' = ' . $this->campoPai->mValor;
            } else {
                $this->mExtra = ' and 1=2';
                $this->mPermiteDefault = true;
                $this->mValorDefault = '(selecione o campo ' . $this->campoPai->Label() . ')';
            }
        }
        $sql = "SELECT " . $this->mCampoChave . "," . $this->nomeCampoDescricao() . "
				from " . $this->mTabela . '
				where 1=1 ' . $this->mExtra . '
				order by ' . $this->nomeCampoOrdem();
        return $sql;
    }

}
