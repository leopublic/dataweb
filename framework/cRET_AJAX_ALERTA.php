<?php

/**
 * Description of cRET_AJAX_ALERTA
 *
 * @author leonardo
 */
class cRET_AJAX_ALERTA extends cRET_AJAX_PADRAO{
	public function __construct($pMsg) {
		$this->codRet = self::codRet_ALERTA ;
		$this->tituloMsg = "Atenção";
		$this->msg = $pMsg;
	}
}
