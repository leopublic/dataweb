<?php

class cTWIG {

    protected static $dir_templates;
    protected static $charset;
    protected static $cache;
    protected static $loader;
    protected static $twig;

    public static function DefineDefaults($pcache = '../templates_twig_bin', $pcharset = 'UTF-8', $pdir = '../templates_twig') {
        self::$cache = $pcache;
        self::$charset = $pcharset;
        self::$dir_templates = $pdir;
    }

    public static function get_dir_templates() {
        if (self::$dir_templates == '') {
            self::$dir_templates = '../templates_twig';
        }
        return self::$dir_templates;
    }

    public static function get_loader() {
        if (!is_object(self::$loader)) {
            self::$loader = new Twig_Loader_Filesystem(self::get_dir_templates());
        }
        return self::$loader;
    }

    public static function get_charset() {
        if (self::$charset == '') {
            self::$charset = 'UTF-8';
        }
        return self::$charset;
    }

    public static function get_cache() {
        if (!isset(self::$cache)) {
            self::$cache = '../templates_twig_bin';
        }
        return self::$cache;
    }

    public static function get_parametros() {
        $parm = array(
            'charset' => self::get_charset()
            , 'cache' => self::get_cache());
        return $parm;
    }

    public static function get_twig() {
        if (!is_object(self::$twig)) {
            self::$twig = new Twig_Environment(self::get_loader(), self::get_parametros());
        }
        return self::$twig;
    }

    public function Render($pnome_template, $pdata) {
        $pdata = array('http' => array('charset' => cLAYOUT::CHARSET, 'dominio' => cAMBIENTE::$m_dominio)) + $pdata;
        $template = self::get_twig()->loadTemplate($pnome_template);
        $ret = $template->render($pdata);
        return $ret;
    }

}
