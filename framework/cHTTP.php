<?php

/**
 * Permite o acesso ao ambiente HTTP a partir das classes
 * É carregado na inicializacao.php
 * O objeto $SESSION é passado por referência para permitir escrita
 * @copyright M2 Software - Utilização fora de projetos da M2 Software não autorizada.
 */
class cHTTP {

    public static $GET;
    public static $POST;
    public static $SESSION;
    public static $FILES;
    // Parametros especiais que devem ser sempre passados
    public static $controller;
    public static $metodo;
    public static $MIME;
    public static $msg;
    public static $flAssociarEmpresaEmbProjAutomaticamente;
    public static $tamanhoArq;
    public static $nomeArq;

    const mmHTML = "1";
    const mmXLS = "2";
    const mo_PAGINA = 'PAGINA';
    const mo_POST_AJAX = 'POST_AJAX';
    const mo_POST_POPUP = 'POST_POPUP';
    const mo_DOWNLOAD = 'DOWNLOAD';

    /*     * ======================================================
     * Propriedades para as telas TWIG
     * ======================================================
     */

    private static $titulo_pagina;
    private static $menu_esquerdo;
    private static $menu_direito;

    /**
     * Get e set para a propriedade titulo_pagina
     */
    public function get_titulo_pagina() {
        if (self::$titulo_pagina == '') {
            self::$titulo_pagina = 'Mundivisas::Dataweb';
        }
        return self::$titulo_pagina;
    }

    public function set_titulo_pagina($pVal) {
        self::$titulo_pagina = $pVal;
    }

    /**
     * Get e set para a propriedade menu_esquerdo
     */
    public function get_menu_esquerdo() {
        return self::$menu_esquerdo;
    }

    public function set_menu_esquerdo($pVal) {
        self::$menu_esquerdo = $pVal;
    }

    /**
     * Get e set para a propriedade menu_direito
     */
    public function get_menu_direito() {
        return self::$menu_direito;
    }

    public function set_menu_direito($pVal) {
        self::$menu_direito = $pVal;
    }

    /*     * ======================================================
     * Controle do modo de operação da página
     * ======================================================
     */

    protected static $mMetodoOperacao;

    public static function setMetodoOperacaoPAGINA() {
        self::$mMetodoOperacao = self::mo_PAGINA;
    }

    public static function setMetodoOperacaoPOST_AJAX() {
        self::$mMetodoOperacao = self::mo_POST_AJAX;
    }

    public static function setMetodoOperacaoPOST_POPUP() {
        self::$mMetodoOperacao = self::mo_POST_POPUP;
    }

    public static function setMetodoOperacaoDOWNLOAD() {
        self::$mMetodoOperacao = self::mo_DOWNLOAD;
    }

    public static function get_nomeArquivo() {

        if (self::$nomeArq == '') {
            if (self::$MIME == self::mmXLS) {
                $ext = '.xls';
            }
            self::$nomeArq = 'relatorio' . $ext;
        }
        return self::$nomeArq;
    }

    public static function set_nomeArquivo($pnome) {
        self::$nomeArq = $pnome;
    }

    /*     * ======================================================
     * Controle de scripts ad-hoc serem disponibilizados
     * ======================================================
     */

    public static $scripts;

    public static function AdicionarScript($pScript) {
        self::$scripts[] = $pScript;
    }

    public static function ScriptsOnLoad() {
        $ret = '';
        if (is_array(self::$scripts)) {
            $i = 1;
            foreach (self::$scripts as $script) {
                $ret .= "\n//Script " . $i . " ";
                $i++;
                $ret .= "\n" . $script;
            }
        } else {
        }
        if ($ret != '') {
            $ret = '<script language="javascript">' . $ret . '</script>';
        }
        return $ret;
    }

    /*     * ======================================================
     * Controle de estilos ad-hoc a serem apresentados
     * ======================================================
     */

    public static $estilosAdicionais;

    public static function AdicionarEstilo($pEstilo) {
        self::$estilosAdicionais[] = $pEstilo;
    }

    public static function estilosAdicionais() {
        $ret = '';
        if (is_array(self::$estilosAdicionais)) {
            foreach (self::$estilosAdicionais as $estilo) {
                $ret .= $estilo;
            }
        }
        if ($ret != '') {
            $ret = '<style>' . $ret . '</style>';
        }
        return $ret;
    }

    /*     * ======================================================
     * Controle de recursos de layout da página a ser apresentada
     * ======================================================
     */

    public static $comMenu;
    public static $comCabecalhoFixo;

    public static function comMenu() {
        if (self::$comMenu == '') {
            return false;
        } else {
            return self::$comMenu;
        }
    }

    public static function comCabecalhoFixo() {
        if (self::$comCabecalhoFixo == '') {
            return false;
        } else {
            return self::$comCabecalhoFixo;
        }
    }

    /*     * ======================================================
     * Controle de redirecionamento de fluxo
     * ======================================================
     */

    private static $redirect_link_externo;
    public static $redirect_link_interno;
    private static $redirect_controller;
    private static $redirect_metodo;
    private static $redirect_parametro;

    public static function RedirecioneParaExterno($pLink) {
        session_write_close();
        header("Location:" . $pLink);
    }

    public static function RedirecionePara($pController, $pMetodo, $pParametrosAdicionais = '', $pPagina = 'carregueComMenu.php') {
        session_write_close();
        $url = "/paginas/" . $pPagina . "?controller=" . $pController . "&metodo=" . $pMetodo . "&" . $pParametrosAdicionais;
        header("Location:" . $url);
    }

    public static function RedirecionaLinkInterno($pLink) {
        session_write_close();
        self::$redirect_link_interno = $pLink;
    }

    public static function TemRedirecionamento() {
        if (self::$redirect_link_interno != '') {
            return true;
        } else {
            return false;
        }
    }

    public static function RedirecionarSeSolicitado() {
        if (self::$redirect_link_interno != '') {
            session_write_close();
            $dominio = cCONFIGURACAO::$configuracoes[AMBIENTE]['dominio'] ;
            if (key_exists('porta', cCONFIGURACAO::$configuracoes[AMBIENTE])){
                $dominio .= ':'.cCONFIGURACAO::$configuracoes[AMBIENTE]['porta'];
            }
            $url = self::$redirect_link_interno;
            header("Location:" . $url );
            return true;
        } else {
            return false;
        }
    }

    /*     * ======================================================
     * Controle de idioma a ser utilizado
     * ======================================================
     */

    public static function setLang($pLang) {
        self::$SESSION["lang"] = $pLang;
    }

    public static function getLang() {
        $lang = self::$SESSION["lang"];
        if (trim($lang) == '') {
            $lang = 'pt_br';
        }
        return $lang;
    }

    public static function ParametroValido($pNome) {
        if (isset(self::$GET[$pNome])) {
            return self::$GET[$pNome];
        } elseif (isset(self::$POST[$pNome])) {
            return self::$POST[$pNome];
        }
    }

    /*     * ======================================================
     * Controle de resultado de operaçoes ajax
     * ======================================================
     */

    private static $sucesso;

    public static function setResultadoOk() {
        self::$sucesso = true;
    }

    public static function setResultadoNaoOk() {
        self::$sucesso = false;
    }

    public static function getResultadoOperacao() {
        if (!isset(self::$sucesso)) {
            self::$sucesso = true;
        }
        return self::$sucesso;
    }

    /*     * ======================================================
     * Controle de fluxo de informacoes
     * ======================================================
     */

    public static function TituloMsg() {
        if (cHTTP::$SESSION['msgTitulo'] != '') {
            $titulo = cHTTP::$SESSION['msgTitulo'];
        } else {
            switch ($tipo_msg) {
                case cNOTIFICACAO::TM_SUCCESS:
                    $titulo = 'Dataweb - OK!';
                case cNOTIFICACAO::TM_ALERT:
                    $titulo = 'Dataweb - Atenção';
                    break;
                case cNOTIFICACAO::TM_ERROR:
                    $titulo = 'Dataweb - Erro!';
                    break;
                case cNOTIFICACAO::TM_INFO:
                default:
                    $titulo = 'Dataweb';
                    break;
            }
        }
        return $titulo;
    }

    public static function HouvePost() {
        if (isset(self::$POST['CMP___nomeTemplate'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function get_telaPostada() {
        return cHTTP::$POST[cINTERFACE::PREFIXO_CAMPOS . cTEMPLATE::CMP_EDICAO];
    }

    public static function getLinhasSelecionadas($pNomeSeletor = 'seletor') {
        $virg = "";
        $lote = "";
        foreach (cHTTP::$POST[$pNomeSeletor] as $chave) {
            $lote .= $virg . intval($chave);
            $virg = ',';
        }
        return $lote;
    }

    /*     * ======================================================
     * Controle de qual controller/metodo foi acionado 
     * ======================================================
     */

    protected static $audit_controller;
    protected static $audit_metodo;

    public static function set_audit_controller($pvalor) {
        if (self::$audit_controller == $pvalor) {
            self::$audit_controller = $pvalor;
        }
    }

    public static function get_audit_controller() {
        return self::$audit_controller;
    }

    public static function set_audit_metodo($pvalor) {
        if (self::$audit_metodo == $pvalor) {
            self::$audit_metodo = $pvalor;
        }
    }

    public static function get_audit_metodo() {
        return self::$audit_metodo;
    }

    public static function StringOk($pstring) {
        $ret = $pstring;
        $ret = str_replace('á', '&aacute', $ret);
        $ret = str_replace('â', '&acirc', $ret);
        $ret = str_replace('à', '&agrave', $ret);
        $ret = str_replace('ã', '&atilde', $ret);
        $ret = str_replace('Á', '&Aacute', $ret);
        $ret = str_replace('À', '&Agrave', $ret);
        $ret = str_replace('Â', '&Acirc', $ret);
        $ret = str_replace('Ã', '&Atilde', $ret);
        $ret = str_replace('é', '&eacute', $ret);
        $ret = str_replace('ê', '&ecirc', $ret);
        $ret = str_replace('É', '&Eacute', $ret);
        $ret = str_replace('Ê', '&Ecirc', $ret);
        $ret = str_replace('í', '&iacute', $ret);
        $ret = str_replace('Í', '&Iacute', $ret);
        $ret = str_replace('ó', '&oacute', $ret);
        $ret = str_replace('ô', '&ocirc', $ret);
        $ret = str_replace('õ', '&otilde', $ret);
        $ret = str_replace('ò', '&ograve', $ret);
        $ret = str_replace('ú', '&uacute', $ret);
        $ret = str_replace('Ú', '&Uacute', $ret);
        $ret = str_replace('ç', '&ccedil', $ret);
        $ret = str_replace('Ç', '&Ccedil', $ret);
        return $ret;
    }

}
