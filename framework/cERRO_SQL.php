<?php
class cERRO_SQL
	extends Exception
{	
	public $mMsg;
	public $mSQL;
	public $mContexto;
	public $mErro;
	
	public function __construct($pErro, $pContexto, $pSQL){
		$this->mErro = $pErro;
		parent::__construct($pErro->getMessage());
		$this->mMsg = "\n<span style=\"font-size:10px;\"><b>[Mensagem]</b>\n".$pErro->getMessage()."\n<b>[Contexto]</b>\n".$pContexto."\n<b>[Comando]</b>\n".$pSQL."</span>";
	}	
}
	
