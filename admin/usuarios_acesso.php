<?php

    /**
     * @author Fábio Pereira
     * @copyright M2 Software 2010
     */

    $_dir = realpath('../LIB/').DIRECTORY_SEPARATOR;
       
    require_once $_dir.'autenticacao.php';
    require_once $_dir.'cabecalho.php';
    require_once $_dir.'geral.php';
    require_once $_dir.'combos.php';
    require_once $_dir.'auxiliares.php';

    echo Topo('USU');
    echo Menu('USU');
    

    
    
    function remover($msg = ''){
        $app = new AppRequest;
        if($app->g('action') == 'remove'){     
            if($app->g('cod')){
                
                $verifica = sql::query("SELECT * FROM usuarios where cd_usuario = ".$app->g('cod'))->count();
                if($verifica == 1){  
                    
                    $inativo = sql::query("UPDATE usuarios set flagativo = 'N' where cd_usuario = ".$app->g('cod'));
                    
                    if($inativo->saved()){
                        
                        $msg = 'O usuário foi inativo com sucesso. <br />';
                    }else{
                        $msg = 'Por um erro inesperado não foi possível realizar esta operação. por favor tente novamente';
                    }
                    
                 }else{       
                   $msg = 'Usuário indisponível !';  
                }
            }
        } 
        if($msg <> ''){
            $msg = 'jAlert("'.$msg.'", "Atenção");'; 
        }
        return $msg; 
    }
    
    
?>
<style>
#TUsuarioAcesso{ border:1px solid #fff; font:12px arial; color: black;margin:10px;width: 98%}
#TUsuarioAcesso tbody{ height: auto;overflow:visible;}
#TUsuarioAcesso tbody td .confirm_del{cursor:pointer;}
#search{font:16px verdana;   margin:10px;   padding:10px;  background: #F3F3F3;}
#search input[type="text"]{   width:500px;  font:bold 16px verdana;  background: #fff;}
</style>
<script>
$(document).ready(function(){
    <?=remover();?>
   $('.confirm_del').live('click',function(){
        var cod = $(this).attr('alt');
        jConfirm('Tem CERTEZA que deseja colocar este usuário como INATIVO ?', 'Atenção', function(d){
            if(d){
                location.href = 'usuarios_acesso.php?action=remove&cod='+cod;
            }
        });
   });
   
   function img(src){
     return '<img src="'+src+'" />';
   } 
   var image = '/imagens/icons/zoom.png';
   $('.TVVSGrid tbody tr').quicksearch({position: 'append',attached: '#search', labelText: img(image)+' Localizar Usuário: ',loaderText: '...'});  
});
</script>
<div class="titulo" style="padding-top:0px;height:38px;">
<?php 
if(Acesso::permitido(Acesso::tp_Sistema_Controle_Usuario_Cadastrar)){  
?> 
	<div style="float:right;margin-left:10px;margin-top:9px;">
	    <ul id="menubar" class="menubar">
	        <li>
	            <a href="usuarios_acesso_add.php"> <img src="/imagens/icons/add.png" style="vertical-align:middle;"/> Adicionar usuário </a>
	        </li>
	    </ul>
	</div>
<?php 
}
?>
	<div style="padding-top:10px;">
	Listagem de Usuários</div> 
</div>
        <div id="search"></div>
        <table id="TUsuarioAcesso" class="TVVSGrid">
        <thead>
            <tr>
              <th>Usuário</th>
              <th width="250">Login</th>
              <th width="200">Perfil</th>
              <th width="120">Qtd. Acessos </th>  
              <th width="60" class="actions">Ações</th>
            </tr>
        </thead>
        <tbody>
        <?
            $strsql = 
            "
              SELECT U.cd_usuario CODUSUARIO ,U.nome NOME, U.login LOGIN, UP.nm_perfil NOMEPERFIL, UP.cd_perfil CODPERFIL,                     
                     CASE WHEN 1=1 THEN                     
                          (SELECT COUNT(CD_PERFIL) FROM ACESSO_PERFIL WHERE CD_PERFIL = UP.cd_perfil )                          
                     END QTD_ACESSOS
                FROM usuario_perfil  UP
          INNER JOIN usuarios U
                  ON U.cd_perfil = UP.cd_perfil
               WHERE U.flagativo = 'Y'
            ORDER BY U.nome ASC 
                               
            ";
        
            $sql = new sql; 
            $res = $sql ->query($strsql);
            
            while($r = $res->fetch()){
                
                $icone_excluir_perfil = ' <img class="confirm_del" title="Colocar inativo" src="/imagens/icons/application_delete.png" alt="'.$r['CODUSUARIO'].'" />';
                 echo '
                 <tr>
                    <td>'.$r['NOME'].'</td>
                    <td>'.$r['LOGIN'].'</td>
                    <td>'.$r['NOMEPERFIL'].'</td>
                    <td>'.$r['QTD_ACESSOS'].'</td>
                    <td>';
                 if (Acesso::permitido(Acesso::tp_Sistema_Controle_Usuario_Editar)){
                        echo '<a href="usuarios_acesso_edit.php?cod='.$r['CODUSUARIO'].'" title="Editar Usuário">
                        <img src="/imagens/icons/application_edit.png" /> </a>';
                 }
                 if (Acesso::permitido(Acesso::tp_Sistema_Controle_Usuario_Excluir)){
                        echo $icone_excluir_perfil;
                 }
                 echo '
                    </td>
                 </tr>';      
                      
            }
        ?>
       </tbody>
       <tfoot>
        <tr>
            <td colspan="5" style="font:10px arial;text-align:right">Todos os usuários encontrados (<?=$res->count()?>)</td>
        </tr>
       </tfoot>
       </table>
</body>
</html>
