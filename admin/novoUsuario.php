<?php
$opcao = "USU";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../javascript/novoUsuario_js.php");

$acao = $_POST['acao'];
if(strlen($acao)==0) {
  $acao = $_GET['acao'];
}

$target = " target = 'USUAR' ";
echo "<script language='javascript'>window.name='USUAR';</script>";

echo IniPag();

$cd_usuario = 0+$_POST['cd_usuario'];
if($cd_usuario==0) {
  $cd_usuario = 0+$_GET['cd_usuario'];
}

if( $cd_usuario > 0){
  $lstUsu = buscaUsuarios($cd_usuario,"","","","","");
  if(count($lstUsu)>0) {
    $obj = $lstUsu[0];
    $cdusu = $obj->GETcd_usuario();
    $objP = $obj->GETcd_perfil();
    $cdemp = $obj->GETcd_empresa();
    $nmlogin = $obj->GETnm_login();
    $nmsenha = $obj->GETnm_senha();
    $nmnome = $obj->GETnm_nome();
    $nmemail = $obj->GETnm_email();
    $cdsup = $obj->GETcd_superior();
    $lstemp = $obj->GETlista_empresas();
    if(count($objP)>0) {
      $cdper = $objP[0]->GETcd_perfil();
      $nmper = $objP[0]->GETnm_perfil();
      $cdniv = $objP[0]->GETcd_nivel();
    }
    if($cdsup>0) {
      $nmsuper = pegaNomeUsuario($cdsup);
    }
  }
} else {
  $cd_usuario = "";
  $lstemp = buscaEmpresas(0);
}

$cmbPerfil = montaComboPerfis($cdper,"ORDER BY cd_perfil");
$cmbEmpresa = montaComboEmpresas($cdemp,"ORDER BY NO_RAZAO_SOCIAL");
$cmbSuperior = "<option value=0>Nenhum".montaComboUsuarioSuperior($cdsup,$cdper,"ORDER BY nome");

if($acao=="V") {
  $nmsenha = "*******";
  $confsenha = "*******";
  $nmEmpresa = pegaNomeEmpresa($cdemp);
} else {
  $nmEmpresa = pegaNomeEmpresa($cdemp);
  $confsenha = $nmsenha;
}
?>

<br><center>
<table border=0 width="700">
 <tr>
  <td class="textobasico" align="center" valign="top">
	<p align="center" class="textoazul"><strong>:: Cadastro de Usuários ::</strong></p>
  </td>
 </tr>
</table>
<br>
<table border=0 width="700" class='textoazulpeq'>
 <tr>
  <td colspan="5"><font color="Red">*</font>&nbsp;<font class="textoazulPeq">Campos obrigatórios</font></td>
 </tr>

 <form action="alteraUsuario.php" method="post" name="usuario" $target>
 <input type="hidden" name="cd_usuario" value='<?=$cd_usuario?>'>

 <tr height=20>
  <td><b><font color="Red">*</font>Login:</td>
  <td><?=montaCampos("nm_login",$nmlogin,$nmlogin,"T",$acao,"maxlength=20 size=30 class='textoazulpeq'")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Nome:</td>
  <td><?=montaCampos("nm_nome",$nmnome,$nmnome,"T",$acao,"maxlength=50 size=30 class='textoazulpeq'")?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Senha:</td>
  <td><?=montaCampos("nm_senha",$nmsenha,$nmsenha,"P",$acao,"maxlength=20 size=30 class='textoazulpeq'")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Confirmação:</td>
  <td><?=montaCampos("confsenha",$confsenha,$confsenha,"P",$acao,"maxlength=20 size=30 class='textoazulpeq'")?></td>
 </tr>

 <tr height=20>
  <td><b><font color="Red">*</font>Email:</td>
  <td><?=montaCampos("nm_email",$nmemail,$nmemail,"T",$acao,"maxlength=50 size=30 class='textoazulpeq'")?></td>
  <td>&#160;</td>
  <td><b><font color="Red">*</font>Perfil de acesso:</td>
  <td><?=montaCampos("cd_perfil",$cmbPerfil,$nmper,"S",$acao,"class='textoazulpeq' onchange='javascript:mudaperfil();'")?></td>
 </tr>

 <tr height=20>
  <td><b>Superior:</td>
  <td colspan="4"><?=montaCampos("cd_superior",$cmbSuperior,$nmsuper,"S",$acao,"class='textoazulpeq'")?></td>
 </tr>

 <tr height=20 style="display:none;" id='mostraemp'>
  <td><b><font color="Red">*</font>Cliente Empresa:</td>
  <td colspan="4">
<?php
if($acao=="V") {
  echo "<input type=hidden name=cd_empresa value='$cdemp'>$nmEmpresa";
} else {
  echo "<select name=cd_empresa class='textoazulpeq'><option value=''>Não é cliente</option>$cmbEmpresa</select>\n";
}
?>
  </td>
 </tr>

 <tr><td>&#160;</td></tr>

 <tr id='mostraemps'><td colspan=5><b>&#160;Empresas que o usuário atende:</b><br>
<?php
$total = count($lstemp);
if($total>0) {
  for($x=0;$x<$total;$x++) {
    $obj=$lstemp[$x];
    $ud = $obj->GETcd_usuario();
    $cd = $obj->GETcd_empresa();
    $nm = $obj->GETnm_empresa();
    $chk = "";
    if($acao=="V") {
      if($ud>0) {
        print "<br><li> $nm";
      }
    } else {
      if($ud>0) { $chk="checked"; }
      print "<br><input type=checkbox name='cd_empresa$x' value='$cd' $chk> $nm";
    }
  }
}
?>
 </td></tr>
<input type=hidden name=tot_emp value='<?=$total?>'>

 <tr><td>&#160;</td></tr>
 <tr>
  <td colspan=5 align="center">
<?php
if($acao=="V") {
  echo "<input name='fechar' type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:window.close();'>";
} else {
  if($ehAdmin=="S") {
    echo "<input name='salvar' type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Salvar' onclick='javascript:validarDados();'>";
  }
}
?>
  </td>
 </tr>
 </form>
</table>

<?php
echo Rodape("");
?>
