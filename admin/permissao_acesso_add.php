<?php

    /**
     * @author Fábio Pereira
     * @copyright M2 Software 2010
     */

    $_dir = realpath('../LIB/').DIRECTORY_SEPARATOR;
       
    require_once $_dir.'autenticacao.php';
    require_once $_dir.'cabecalho.php';
    require_once $_dir.'geral.php';
    require_once $_dir.'combos.php';
    require_once $_dir.'auxiliares.php';

    echo Topo('USU');
    echo Menu('USU'); 
?>
<style>
#TPerfilAcesso{
    border:1px solid #fff;
    font:12px arial;
    color: black
}
#TPerfilAcesso th{
    padding:5px;
    font: bold 12px verdana;    
}
#TPerfilAcesso tbody{
    height: auto;
    overflow:visible;
}
#TPerfilAcesso tbody td{
    border-bottom:1px solid #c0c0c0
}
</style>
<script>
$(document).ready(function(){


    $('#TPerfilAcesso tbody tr td input[type="checkbox"]').each(function(){ 
        if($(this).attr('checked') == true){
           $(this).parent().parent().css('background', '#DDECFE'); 
        }
    });

    $('#TPerfilAcesso tbody tr td input[type="checkbox"]').live('click',function(){
        if($(this).attr('checked')){
           $(this).parent().parent().css('background', '#DDECFE');     
        }else{
           $(this).parent().parent().css('background','#FFF');   
        }
    });
    
    $('#todos').click(function(){
        if($('#todos').attr('checked')){
           $('#TPerfilAcesso tbody input').attr('checked', true);
           $('#TPerfilAcesso tbody tr').css('background','#DDECFE');  
        }else{
           $('#TPerfilAcesso tbody input').attr('checked', false);
           $('#TPerfilAcesso tbody tr').css('background','#FFF');   
        }
    });    

})
</script>
<div class="titulo">  Perfis / novo     <?=menuPerfil()?> </div>
<?
    // Salvando o Perfil
    $app = new Generic;

    if($app->g('action') == 'save'){
        
        if($app->p('NOME')){
            
           
            $perfil = sql::query("INSERT INTO usuario_perfil (nm_perfil)  VALUES ('".$app->p('NOME')."') ");
 
            if($perfil->saved()){
                
               $id     = $perfil->id(); 
               $sqlsql = "INSERT INTO ACESSO_PERFIL(CD_PERFIL, CD_ACESSO) VALUES ";
               $vals   = ''; 
                
               if($_POST['acs']){ 
                   foreach($_POST['acs'] as $p => $k){
                      if($k == 'on'){
                          if($vals <> ''){
                             $vals .= ', ('.$id.' ,'.$p.')';  
                          }else{
                             $vals .= ' ('.$id.','.$p.')'; 
                           } 
                      }
                      $vals   .= '';
                   }
                   
                  if($vals <> ''){ 
                      $acessos = sql::query($sqlsql.$vals);
                                   
                      if($acessos->saved()){     // salvou
                            echo '
                            <div class="success">
                              <img src="/imagens/icons/accept.png" />
                              O perfil <b>'.$app->p('NOME').'</b> e suas permissões de acesso, foram salvas com sucesso ! 
                               <a href="permissao_acesso.php"> Voltar para Perfis </a>
                              
                             </div>';
                      }else{                    
                            echo '
                            <div class="error">
                                <img src="/imagens/icons/error_add.png" />                        
                                Perfil <b>'.$app->p('NOME').'</b> cadastrado. <br />
                                Mas não foi possível salvar as permissões de acesso para este perfil por um erro inesperado. <br />
                                Para solucionar, edite este mesmo perfil inserindo os acessos corretamente.
                                
                                 <a href="permissao_acesso.php"> Voltar para Perfis </a>
                            </div> DEBUG SQL: <BR />'.$sqlsql.$vals;
                      } 
                  } 
              }else{
                    echo '
                    <div class="success">
                      <img src="/imagens/icons/accept.png" />
                      O perfil <b>'.$app->p('NOME').'</b> foi salvo com sucesso ! <br />
                      <h4>ATENÇÃO: Você não permitiu nenhum acesso ao sistema para este perfil.</h4>
                      <br />
                      
                      Se desejar editar o perfil. Clique <a href="permissao_acesso_edit.php?cod='.$id.'"> aqui </a> ou 
                       <a href="permissao_acesso.php"> Voltar para Perfis </a>
                      
                     </div>';
              } 
            }
            
        }
        
    }
    



?>  <br />
    <form method="post" action="?action=save">
    
     <table border="0" width="100%">
        <tr>
            <th width="140">Nome do perfil:</th>
            <td>
                <input type="text" name="NOME" style="font:bold 16px verdana;width:400px;" />
                <!--<input type="submit" value="Gravar Perfil" /> -->
                  <button> <img src="/imagens/save.png"  /> Salvar Perfil</button>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>    
        <tr>
            <th>Acessos:</th>
            <td>   
                     <!-- Início da tabela Perfil acessos -->
                    <table id="TPerfilAcesso" class="TVVSGrid">
                    <thead>
                        <tr>
                          <th> Descrição do acesso</th><th width="90">  <input type="checkbox" id="todos" checked="true" title="marcar todos" />  Permitir ? </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?
 
                        $res = sql::query('SELECT * FROM ACESSO');
                        
                        while($r = $res->fetch()){
                            
                             echo '<tr><td>'.$r['NOME_ACESSO'].'</td> <td><input checked="true" type="checkbox" name="acs['.$r['CD_ACESSO'].']" /></td> </tr>';      
                                  
                        }
                    ?>
                   </tbody>
                   </table>
                   <!-- FIM da Tabela Perfil acessos -->
                   <div style="text-align: right;"><br />
                        <button > <img src="/imagens/save.png"  /> Salvar Perfil</button>
                   </div>
            </td>
        </tr>
</table> 
    </form>

</body>
</html>
