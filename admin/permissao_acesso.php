<?php

    /**
     * @author Fábio Pereira
     * @copyright M2 Software 2010
     */

    $_dir = realpath('../LIB/').DIRECTORY_SEPARATOR;
       
    require_once $_dir.'autenticacao.php';
    require_once $_dir.'cabecalho.php';
    require_once $_dir.'geral.php';
    require_once $_dir.'combos.php';
    require_once $_dir.'auxiliares.php';

    echo Topo('USU');
    echo Menu('USU');
    
    $app = new AppRequest;
    
    
    function remover($msg = ''){
        $app = new AppRequest;
        if($app->g('action') == 'remove'){     
            if($app->g('cod')){
                $sql = new sql;
                $verifica = $sql->query('SELECT * FROM usuario_perfil where cd_perfil ='.$app->g('cod'))->count();
                if($verifica == 1){  
                    $deleta_perfil = $sql->query('DELETE FROM usuario_perfil where cd_perfil = '.$app->g('cod'));
                    if($deleta_perfil->saved()){
                        
                        $msg ='Perfil Excluido com sucesso. <br />'; 
                        
                        $deleta_acessos = $sql->query("DELETE FROM ACESSO_PERFIL WHERE CD_PERFIL = ".$app->g('cod'));
                        
                                           
                    }else{ 
                      $msg = 'Não foi possível excluir o perfil Por um erro inesperado. Tente novamente ';
                    }
                 }else{       
                   $msg = 'Perfil indisponível para Exclusão! <br /> Possívelmente já foi excluido anteriormente';  
                }
            }
        } 
        if($msg <> ''){
            $msg = 'jAlert("'.$msg.'", "Atenção");'; 
        }
        return $msg; 
    }
    
    
?>
<style>
#TPerfil{
    border:1px solid #fff;
    font:12px arial;
    color: black
}
#TPerfil th{
    padding:5px;
    font: bold 12px verdana;    
}
#TPerfil tbody{
    height: auto;
    overflow:visible;
}
#TPerfil tbody td{
    border-bottom:1px solid #c0c0c0
}
#TPerfil tbody td .confirm_del{
    cursor:pointer;
}
</style>
<script>
$(document).ready(function(){
   
    <?=remover();?>
      
   $('.confirm_del').live('click',function(){
        var cod = $(this).attr('alt');
        jConfirm('Tem certeza que deseja EXCLUIR este perfil ?', 'Atenção', function(d){
            if(d){
                location.href = 'permissao_acesso.php?action=remove&cod='+cod;
            }
        });
   }); 
});
</script>
<div class="titulo"> Perfis
    <?//=menuPerfil()?>
</div>
 <br />
 

 <br />
 <br />
      
        <table id="TPerfil" class="TVVSGrid">
        <thead>
            <tr>
              <th> Perfil</th> <th width="120">Qtd. Acessos </th> <th width="120"> Qtd. Usuários</th> <th width="60">Ações</th>
            </tr>
        </thead>
        <tbody>
        <?
            $strsql = 
            '
              SELECT UP.nm_perfil NOME, UP.cd_perfil CODPERFIL,
                     CASE WHEN 1=1 THEN 
                          (SELECT COUNT(cd_perfil) from usuarios where cd_perfil = UP.cd_perfil)                    
                     END QTD_USUARIOS,                     
                     CASE WHEN 1=1 THEN                     
                          (SELECT COUNT(CD_PERFIL) FROM ACESSO_PERFIL WHERE CD_PERFIL = UP.cd_perfil )                          
                     END QTD_ACESSOS
                FROM usuario_perfil  UP             
            ';
        
            $sql = new sql; 
            $res = $sql ->query($strsql);
            
            while($r = $res->fetch()){
                
                if($r['QTD_USUARIOS'] == 0){
                    $icone_excluir_perfil = ' <img class="confirm_del" title="Excluir Perfil" src="/imagens/icons/application_delete.png" alt="'.$r['CODPERFIL'].'" />';
                }else{
                    $icone_excluir_perfil = '';
                }
                
                
                 echo '
                 <tr>
                    <td>'.$r['NOME'].'</td>
                    <td>'.$r['QTD_ACESSOS'].'</td>
                    <td>'.$r['QTD_USUARIOS'].'</td>
                    <td>
                        <a href="permissao_acesso_edit.php?cod='.$r['CODPERFIL'].'" title="Editar Perfil">
                        <img src="/imagens/icons/application_edit.png" /> </a>
                        '.$icone_excluir_perfil.'
                    </td>
                 </tr>';      
                      
            }
        ?>
       </tbody>
       </table>
</body>
</html>
