<?php
include ("../LIB/conecta.php");

function valida_data($_data){
	$data = split("[-,/]", $_data);
	if(!@checkdate($data[1], $data[0], $data[2]) and !@checkdate($data[1], $data[2], $data[0])) {
		return false;
	}
	return true;
}	

function iData($_data){
    if($_data==''){
        $_data = '00/00/0000';
    }
    list($data, $hora) = split(" ",$_data);		
	if(valida_data($_data)){
		$data_nova = implode(preg_match("~\/~", $data) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data) == 0 ? "-" : "/", $data)));
	    $GLOBALS['sumary'] = $GLOBALS['sumary'];   
		return $data_nova." ".$hora;
	}else{
	    if($_data!='00/00/0000'){   
	       $GLOBALS['sumary'] = "data informada inválida ($_data)!";
        }   
		return false;
	}
}    	

print "<style>*{color:#0D0;font-family: courier, helvetica, sans-serif; font-size: 12px; } b{color:#FFF;}</style>";

$empresa = $_POST['idEmpresa'];
if($empresa==''){
    $_SESSION['msg'] = "Informe uma empresa...";
    header("cargaCandidatos.php");
}
$debug = $_POST['debug']==1?true:false;

$nome = "tempFiles/".date('dmYhis').".csv"; 
move_uploaded_file($_FILES['arqCSV']['tmp_name'],$nome)or die("Erro :".$_FILES['arqCSV']['error']);

//$debug = true;

$sumary = 'sucesso';
$erro = "";   

$nomes = file($nome);
$key = 0;
for($x=0;$x<count($nomes);$x++){
    if($key>2){
        $candiato = split("\t",$nomes[$x]);
        $linha = ($key+1);       
        
        $PN = mysql_query("SELECT CO_PAIS FROM PAIS_NACIONALIDADE WHERE UPPER(NO_PAIS) = '".strtoupper($candiato[7])."' OR UPPER(NO_NACIONALIDADE) = '".strtoupper($candiato[7])."'") or die (mysql_error());
        $regs = mysql_num_rows($PN);
        if($debug){
            if($regs>1){
                print "Linha <b style='color:#FC0;'>$linha</b>, Coluna NACIONALIDADE : Foram encontrados $regs <b>'{$candiato[7]}'</b>.<br />";
            }elseif($regs==0){
                print "Linha <b style='color:#FC0;'>$linha</b>, Coluna NACIONALIDADE : A nacionalidade <b>'{$candiato[7]}'</b> não foi encontrada.<br />";
            }
        }

        $nac = mysql_fetch_array($PN);
        $nac = $nac[0]==''?'NULL':$nac[0];
        

        
        $PEP = mysql_query("SELECT CO_PAIS FROM PAIS_NACIONALIDADE WHERE UPPER(NO_PAIS) = '".strtoupper($candiato[14])."' OR UPPER(NO_NACIONALIDADE) = '".strtoupper($candiato[14])."'") or die (mysql_error());
        $regs = mysql_num_rows($PN);
        if($debug){
            if($regs>1){
                print "Linha <b style='color:#FC0;'>$linha</b>, Coluna PAIS DE EMISSÃO : Foram encontrados $regs '{$candiato[14]}'</b>.<br />";
            }elseif($regs==0){
                print "Linha <b style='color:#FC0;'>$linha</b>, Coluna PAIS DE EMISSÃO : O país <b>'{$candiato[14]}'</b> não foi encontrado.<br />";
            }
        }

        $pPas = mysql_fetch_array($PEP);
        $pPas = $pPas[0]==''?'NULL':$pPas[0];;

        if($candiato[1]!=''){
            $EMB = mysql_query("SELECT NU_EMBARCACAO_PROJETO FROM EMBARCACAO_PROJETO WHERE UPPER(NO_EMBARCACAO_PROJETO) = '".strtoupper($candiato[1])."'") or die (mysql_error());
            $regs = mysql_num_rows($EMB);
            if($debug){
                if($regs>1){
                    print "Linha <b style='color:#FC0;'>$linha</b>, Coluna EMBARCAÇÂO : Foram encontrados $regs embarcações <b>'{$candiato[1]}'</b>.<br />";
                }elseif($regs==0){
                    print "Linha <b style='color:#FC0;'>$linha</b>, Coluna EMBARCAÇÂO : A embarcação <b>'{$candiato[1]}'</b> não foi encontrada.<br />";
                }
            }
    
            $barquinho = mysql_fetch_array($EMB);
            $barquinho = $barquinho[0]==''?'NULL':$barquinho[0];;
        }else{
            $barquinho = 'NULL';
        }

		// Verifica se o candidato já existe
		$nomeCompleto = cCANDIDATO::CalculaNomeCompleto($candiato[2], $candiato[3], $candiato[4]);

		$cand = '0';
		$sql_cand = "select NU_CANDIDATO from CANDIDATO where NOME_COMPLETO like '".'%'.str_replace(' ', '%', $nomeCompleto).'%'. "'";
        if ($res = mysql_query($sql_cand)){
			if ($rs = mysql_fetch_array($res)){
				$cand = $rs['NU_CANDIDATO'];
		        print "Candidato '".$nomeCompleto."' ( linha #<b style='color:#FC0;'>$linha</b>) $yn encontrado no banco (".$cand.")!<i style='color:#F6C'>$erro</i><br />";
			}
		}

		if($cand == '0'){
			$sql_1 = "INSERT INTO CANDIDATO (
							NU_EMPRESA,
							NU_EMBARCACAO_PROJETO,
							NO_PRIMEIRO_NOME,
							NO_NOME_MEIO,
							NO_ULTIMO_NOME,
							NOME_COMPLETO,
							NO_MAE,
							NO_PAI,
							CO_NACIONALIDADE,
							CO_SEXO,
							DT_NASCIMENTO,
							NO_LOCAL_NASCIMENTO,
							NU_PASSAPORTE,
							DT_EMISSAO_PASSAPORTE,
							DT_VALIDADE_PASSAPORTE,
							CO_PAIS_EMISSOR_PASSAPORTE,
							NU_RNE ,
							DT_CADASTRAMENTO,
							CO_USU_CADASTAMENTO
						) VALUES (
							$empresa,
							$barquinho,
							'{$candiato[2]}',
							'{$candiato[3]}',
							'{$candiato[4]}',
							'{$nomeCompleto}',
							'{$candiato[5]}',
							'{$candiato[6]}',
							$nac,
							'{$candiato[8]}',
							'".iData($candiato[9])."',
							'{$candiato[10]}',
							'{$candiato[11]}',
							'{$candiato[12]}',
							'".iData($candiato[13])."',
							$pPas,
							'{$candiato[15]}'    ,
							now(),
							38
						)";

			if(!$debug){
				$res = mysql_query($sql_1);
				if(!$res){
					$yn = "não ";
					$erro = mysql_error();
				}
				$cand = mysql_insert_id();
			}
	        print "Candidato '".$nomeCompleto."' ( linha #<b style='color:#FC0;'>$linha</b>) $yn inserido com sucesso (".$cand.")!<i style='color:#F6C'>$erro</i><br />";
		}


        $tipo = 'NULL';
        if(strstr(strtolower($candiato[27]),'n')){
        $tipo = "'y'";
        }
        if(strstr(strtolower($candiato[27]),'m')){
        $tipo ="'m'";
        }
        if(strstr(strtolower($candiato[27]),'d')){
        $tipo = "'d'";
        }
		$observacao = '';
		if ($candiato[44] != ''){
			$observacao .= 'Acao:'.$candiato[44]."\n";
		}
		if ($candiato[46] != ''){
			$observacao .= $candiato[46];
		}

        $AUT = mysql_query("SELECT NU_SERVICO, TIPO_AUTORIZACAO.CO_TIPO_AUTORIZACAO FROM TIPO_AUTORIZACAO, SERVICO WHERE UPPER(NO_REDUZIDO_TIPO_AUTORIZACAO) = '".strtoupper($candiato[21])."' and SERVICO.CO_TIPO_AUTORIZACAO = TIPO_AUTORIZACAO.CO_TIPO_AUTORIZACAO and SERVICO.ID_TIPO_ACOMPANHAMENTO = 1") or die (mysql_error());
        $regs = mysql_num_rows($AUT);
        if($debug){
            if($regs>1){
                print "Linha <b style='color:#FC0;'>$linha</b>, Coluna CLASSIFICAÇÃO : Foram encontrados $regs <b>'{$candiato[21]}'</b>.<br />";
            }elseif($regs==0){
                print "Linha <b style='color:#FC0;'>$linha</b>, Coluna CLASSIFICAÇÃO : O código <b>'{$candiato[21]}'</b> não foi encontrado.<br />";
            }
        }
        $rec_classif = mysql_fetch_array($AUT);
        $classif = $rec_classif['CO_TIPO_AUTORIZACAO']==''?'NULL':$rec_classif['CO_TIPO_AUTORIZACAO'];;
		if($rec_classif['NU_SERVICO'] != ''){
			$NU_SERVICO = $rec_classif['NU_SERVICO'] ;
		}
		else{
			$NU_SERVICO = 'null';
		}

        $sql_2 = "INSERT INTO processo_mte (
                    cd_candidato, 
                    nu_processo,
                    nu_oficio,
                    dt_deferimento,
                    dt_prazo_tipo,
                    dt_prazo_unid,
                    dt_prazo_data,
					nu_servico,
					observacao_visto,
					fl_visto_atual
                ) VALUES (
					{$cand},
                    '{$candiato[23]}',                    
                    '{$candiato[24]}',                    
                    '".iData($candiato[25])."',                    
                    {$tipo},
                    '{$candiato[26]}',                    
                    '".iData($candiato[28])."',
					{$NU_SERVICO},
                    '{$observacao}',
					1
                )";
		$erro = '';
        if(!$debug){
            if($cand!=''){
                $res = mysql_query($sql_2);
                if(!$res){
                        $erro = mysql_error(); 
                }
                $mte = mysql_insert_id();
		        print "       ==>Processo mte adicionado com sucesso!<i style='color:#F6C'>$erro</i><br />";

            
				$erro = '';
                $res2 = mysql_query("UPDATE CANDIDATO SET codigo_processo_mte_atual = $mte WHERE NU_CANDIDATO = $cand");
                if(!$res2){
                    $erro = mysql_error(); 
                }
		        print "       ==>Processo mte atual do candidato atualizado com sucesso!<i style='color:#F6C'>$erro</i><br />";

				$erro = '';
				$res2 = null;
				$sql = "update processo_mte set fl_visto_atual = 0 where cd_candidato = ".$cand." and codigo <>".$mte;
				$res2 = mysql_query($sql);
                if(!$res2){
                    $erro = mysql_error();
                }
		        print "       ==>Processo mte atual atualizado com sucesso !<i style='color:#F6C'>$erro</i><br />";
            }
        }

        $tipo = 'NULL';
        if(strstr(strtolower($candiato[17]),'n')){
        $tipo = "'y'";
        }
        if(strstr(strtolower($candiato[17]),'m')){
        $tipo = "'m'";
        }
        if(strstr(strtolower($candiato[17]),'d')){
        $tipo = "'d'";
        }
        

        $sql_3 = "INSERT INTO processo_coleta (
                    cd_candidato, 
                    dt_validade_tipo,
                    dt_validade_unid,   
                    dt_validade_data,
                    no_local_emissao,
                    dt_emissao_visto,
                    dt_entrada,
                    codigo_processo_mte 
                ) VALUES (
                    {$cand},
                    {$tipo},
                    '{$candiato[16]}',
                    '".iData($candiato[18])."',                    
                    '{$candiato[19]}',                    
                    '".iData($candiato[20])."',                    
                    '".iData($candiato[22])."',
                    {$mte}
                )"; 
		$erro = '';
        if(!$debug){
            if($cand!=0){
                $res = mysql_query($sql_3);
                if(!$res){
                    $erro = mysql_error(); 
                }
		        print "       ==>Processo coleta criado com sucesso !<i style='color:#F6C'>$erro</i><br />";
			}
        }
        
        $sql_4 = "INSERT INTO processo_regcie (
                    cd_candidato, 
                    nu_protocolo,
                    dt_requerimento,
                    dt_validade,
                    dt_prazo_estada,
                    codigo_processo_mte
                ) VALUES (
                    $cand,
                    '{$candiato[29]}',                    
                    '".iData($candiato[30])."',                    
                    '".iData($candiato[31])."',                    
                    '".iData($candiato[32])."',
                    {$mte}
                )";
		$erro = '';
        if(!$debug){
            if($cand!=0){
                $res = mysql_query($sql_4);
                if(!$res){
                     $erro = mysql_error();
                }
		        print "       ==>Processo regcie criado com sucesso !<i style='color:#F6C'>$erro</i><br />";
            }
        }
                
        $sql_5 = "INSERT INTO processo_prorrog (
                    cd_candidato, 
                    nu_protocolo,
                    dt_requerimento,
                    dt_prazo_pret,
                    dt_publicacao_dou,
                    codigo_processo_mte
                ) VALUES (
                    $cand,
                    '{$candiato[33]}',                    
                    '".iData($candiato[34])."',                    
                    '".iData($candiato[35])."',                    
                    '".iData($candiato[36])."',
                    $mte        
                )";
		$erro = '';
        if(!$debug){
            if($cand!=0){
                $res = mysql_query($sql_5);
                if(!$res){
                        $erro = mysql_error(); 
                }
		        print "       ==>Processo prorrog criado com sucesso !<i style='color:#F6C'>$erro</i><br />";
            }
        }

		if (trim($candiato[37]) != '' ){
			$sql_4 = "INSERT INTO processo_regcie (
						cd_candidato,
						nu_servico,
						nu_protocolo,
						dt_requerimento,
						dt_validade,
						dt_prazo_estada,
						codigo_processo_mte
					) VALUES (
						{$cand},
						58,
						'{$candiato[37]}',
						'".iData($candiato[38])."',
						'".iData($candiato[39])."',
						'".iData($candiato[40])."',
						$mte
					)";
			$erro = '';
			if(!$debug){
				if($cand!=0){
					$res = mysql_query($sql_4);
					if(!$res){
							$erro = mysql_error();
					}
			        print "       ==>Processo restabelecimento criado com sucesso !<i style='color:#F6C'>$erro</i><br />";
				}
			}
		}


    }
    $key++;
    $sumary = 'sucesso';
    $yn = $erro = "";   
    print "<br />";
}
unlink($nome);
?>
