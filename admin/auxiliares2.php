<?php
$opcao = "USU";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/auxiliares.php");

echo Topo($opcao);
echo Menu($opcao);

$orderby = trim($_POST['orderby']);
$id=trim($_POST['id']);
if(strlen($id)==0) { $id=$_GET['id']; }
$acao = $_POST['acao'];

$val1 = $_POST['val1'];
$val2 = $_POST['val2'];
$val3 = $_POST['val3'];
$val4 = $_POST['val4'];
$val5 = $_POST['val5'];
$val6 = $_POST['val6'];

$col1 = "C&oacute;digo";
$col2 = "Nome";
$col3 = "Nacional.";
$col4 = "Ingl&ecirc;s";
$col5 = "Nac. Ingl&ecirc;s";
$col6 = "";

$ret = "";

if($id=="PAI") {
  if(strlen($orderby)==0) { $orderby="NO_PAIS"; }
  $titulo = "Pa&iacute;s/Nacionalidade";
  if($acao=="I") { $ret = inserePaisNacionalidade($val1,$val2,$val3,$val4,$val5); }
  else if($acao=="A") { $ret = alteraPaisNacionalidade($val1,$val2,$val3,$val4,$val5); }
  else if($acao=="R") { $ret = removePaisNacionalidade($val1); }
  $lista = listaGeral("PAIS_NACIONALIDADE","ORDER BY $orderby");
} else if($id=="REC") {
  if(strlen($orderby)==0) { $orderby="NO_REPARTICAO_CONSULAR"; }
  $titulo = "Reparti&ccedil;&atilde;o Consular";
  if($acao=="I") { $ret = insereReparticaoConsular($val1,$val2,$val3,$val4,$val5,$val6); }
  else if($acao=="A") { $ret = alteraReparticaoConsular($val1,$val2,$val3,$val4,$val5,$val6); }
  else if($acao=="R") { $ret = removeReparticaoConsular($val1); }
  $lista = listaGeral("REPARTICAO_CONSULAR"," WHERE CO_REPARTICAO_CONSULAR>999 ORDER BY $orderby");
  $col3 = "Telefone";
  $col4 = "Endere&ccedil;o";
  $col5 = "Cidade";
  $col6 = "Pa&iacute;s"; 
} else {
  $titulo = "Tabelas Auxiliares";
}
print "<!-- ret=$ret -->";
$botao = " type='Button' class='textformtopo' style=\"<?=$estilo?>\" ";
?>

    <br><center>
  	<table border=0 width="95%">
	 <tr>
	  <td class="textobasico" align="center" valign="top" colspan="3">
		<p align="center" class="textoazul"><strong>:: <?=$titulo?> ::</strong></p>				
	  </td>
	 </tr>
	 <tr><td colspan="3"><br></td></tr>
	 <form action="auxiliares2.php" method="post" name='forms'>
	 <input type="hidden" name="id" value='<?=$id?>'>
	 <input type="hidden" name="acao" value='I'>
		
     <tr>
      <td width=100% align=center valign=top>
      
<?php 
echo "<table width=100% cellpadding=0 cellspacing=0 border=1 class='textoazulPeq'>\n";
echo "<tr bgcolor='#DADADA'>\n";
echo "<td align=center><b>$col1</td>\n";
echo "<td align=center><b>$col2</td>\n";
if($id=="REC") { 
  echo "<td align=center><b>$col6</td>\n"; 
} else {
  echo "<td align=center><b>$col3</td>\n"; 
}
echo "<td align=center><b>A&ccedil;&atilde;o</td>\n";
echo "</tr>\n";

while($rw=mysql_fetch_array($lista)) {
  $val1 = $rw[0];
  $val2 = $rw[1];
  $val3 = $rw[2];
  $val4 = $rw[3];
  $val5 = $rw[4];
  if($id=="REC") { 
     $val3=$rw['NU_TELEFONE']; 
     $val4=$rw['NO_ENDERECO']; 
     $val5=$rw['NO_CIDADE']; 
     $val6=$rw['CO_PAIS']; 
  }
  $remove = "<a href=\"javascript:remove2('$val1','$val2','$val3','$val4','$val5','$val6');\"><b>R</a>";
  $altera = "<a href=\"javascript:altera2('$val1','$val2','$val3','$val4','$val5','$val6');\"><b>A</a>";
  echo "<tr>\n<td align=center>$val1</td>\n";
  echo "<td>&#160;$val2</td>\n";
  if($id=="REC") { 
    echo "<td>&#160;<b>".pegaNomePais($val6)."</td>\n";
  } else {
    echo "<td>&#160;$val3</td>\n";
  }
  echo "<td align=center>$altera &#160; $remove</td>\n";
  echo "</tr>\n";
}

echo "</table>\n";
echo "</td>\n";
echo "<td width=20>&#160;</td>\n";
echo "<td width=250 align=center valign=top>\n";
echo "<table width=100% border=1 cellspacing=0 cellpadding=0 class='textoazulPeq'>\n";
echo "<input type=hidden name=val1>\n"; 
echo "<tr height=20><td><b>$col2:</td><td><input type=text name=val2 size=20></td></tr>\n";
echo "<tr height=20><td><b>$col3:</td><td><input type=text name=val3 size=20></td></tr>\n";
echo "<tr height=20><td><b>$col4:</td><td><input type=text name=val4 size=20></td></tr>\n";
echo "<tr height=20><td><b><nobr>$col5:</nobr></td><td><input type=text name=val5 size=20></td></tr>\n";
if($id=="REC") { 
  $lstPaises = montaComboPais("","");
  echo "<tr height=20><td><b>$col6 :</td><td><select name='val6' class='textoPretoPeq'><option value=''>Selecione ...$lstPaises</select></td></tr>\n";
} else {
  echo "<input type=hidden name=val6>\n"; 
}
?>     
        <tr id="mostra_inserir" style="display:block" height=20><td colspan=2 align=center>
            <input <?=$botao?> value="Incluir" onclick="javascript:Continuar2('I');"></tr>
        <tr id="mostra_alterar" style="display:none" height=20><td colspan=2 align=center>
            <input <?=$botao?> value="Alterar" onclick="javascript:Continuar2('A');"></tr>
        <tr id="mostra_remover" style="display:none" height=20><td colspan=2 align=center>
            <input <?=$botao?> value="Remover" onclick="javascript:Continuar2('R');"></tr>
       </table>
      </td>
     </tr>
	 </form>
	</table>

<?php
echo Rodape($opcao);
include ("../javascript/auxiliares_js.php");
?>

