<?php

    /**
     * @author Fábio Pereira
     * @copyright M2 Software 2010
     */

    $_dir = realpath('../LIB/').DIRECTORY_SEPARATOR;
       
    require_once $_dir.'autenticacao.php';
    require_once $_dir.'cabecalho.php';
    require_once $_dir.'ControleEventos.php';
    
    
    echo Topo('USU');
    echo Menu('USU'); 
    

?>
<div class="titulo"> Usuários / Novo /  </div>
<form name="frmsave" action="?action=save" method="post">

   
        <?
        
         /*foreach($perfil as $p){ echo ' - '.$p['nome'].'<br/>'; }*/
     
        if($isPost){
            if($sucesso){ 
                echo '<div class="success">'.$msgSucesso.'</div>'; 
            }else{
                echo '<div class="error">'.$msgErro.'</div>';
            }
        }
     ?>

    <div id="barMain">
        <ul id="usuarioAcesso" class="menubar" style="float: right;">
            <li> <img src="/imagens/icons/group.png" /> <a href="usuarios_acesso.php"> Listagem de Usuários </a></li>
            <li onclick="forms['frmsave'].submit()"> <img src="/imagens/save.png" /> Salvar</li>
        </ul>
    </div>

    <table id="TEditUsuario" class="edicao" border="0" width="100%">
        <tr>
            <th width="150">Nome:</th>
            <td>
                <input type="text" name="nome" value="" style="width:750px;" />
            </td>
        </tr>
        <tr>
            <th width="150">Login:</th>
            <td>
                <input type="text" name="login" value="" style="width:450px;" />
            </td>        
        </tr>
        <tr>
            <th width="150">Senha:</th>
            <td>
                <input type="password" name="nm_senha" value="" style="width:450px;" />
            </td>        
        </tr>
        <tr>
            <th>E-mail:</th>
            <td><input type="text" name="nm_email" value="" style="width:450px;" /></td>
        </tr>
        <tr>
            <th>Perfil:</th>
            <td><?=$app->combo_perfil()?></td>
        </tr>
        <tr>
            <th>Superior:</th>
            <td><?=$app->combo_superior()?></td>
        </tr>      
        <tr>
            <th>Empresa:</th>
            <td><?=$app->combo_empresa($cd_empresa)?></td>
        </tr>
    </table>
    <div style="text-align: center;">
    <button> <img src="/imagens/save.png" /> Salvar Cadastro</button>
    </div> 
 </form>
 </body>
</html>
