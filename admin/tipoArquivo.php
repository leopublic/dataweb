<?php
/**
 * @author Everton
 * @copyright M2 Software 2010
 */
    include ("../LIB/autenticacao.php");
    include ("../LIB/cabecalho.php");
    include ("../LIB/geral.php");
    include ("../LIB/combos.php");
    include ("../LIB/auxiliares.php");

    echo Topo('Tipo Autorização');
    echo Menu();    
?>
    <style>
        .edicao th{ background-color: #CCC;}
        .edicao td{padding-top: 3px;padding-bottom:3px}
        .edicao tr:hover{background-color: #EEE;}
    </style>
    <div class="titulo"> Tipos de arquivo </div>
    <table class="edicao" style="width:98%; margin-left: 1%;">
        <thead>
            <tr>
                <td colspan="3" style="border-bottom: none;text-align: right; padding:5px; background: white;" >
                    <button id="add" onclick="window.location.href='editArquivo.php'"><img src="/imagens/icons/add.png" /> Adicionar novo tipo de arquivo</button>
                </td>  
            </tr> 
            <tr>
                <th style="width:100px">Código</th>
                <th colspan="2">Tipo de Arquivo</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sql = "SELECT * FROM TIPO_ARQUIVO order by CO_TIPO_ARQUIVO";
            $res = mysql_query($sql);
            while($row=mysql_fetch_array($res)){
                print '<tr>
                           <td align="center">'.$row['CO_TIPO_ARQUIVO'].'</td>
                           <td>'.$row['NO_TIPO_ARQUIVO'].'</td>
                           <td style="width:50px; text-align:right">
                            <a href="editArquivo.php?id='.$row['ID_TIPO_ARQUIVO'].'"><img src="../imagens/edit.png" /></a>
                            <a href="delArquivo.php?id='.$row['ID_TIPO_ARQUIVO'].'" onclick="return confirm(\'Deseja realmente excluir este arquivo?\\n\\rEsta operação não tem retorno.\');" ><img src="../imagens/trash.png" /></a>
                           </td>
                       </tr>';
            }
            ?>
        </tbody>
    </table>
</body>
</html>
