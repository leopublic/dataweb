<?php

    /**
     * @author Fábio Pereira
     * @copyright M2 Software 2010
     */

    $_dir = realpath('../LIB/').DIRECTORY_SEPARATOR;
       
    require_once $_dir.'autenticacao.php';
    require_once $_dir.'cabecalho.php';
    require_once $_dir.'geral.php';
    require_once $_dir.'combos.php';
    require_once $_dir.'auxiliares.php';

    echo Topo('USU');
    echo Menu('USU'); 
    
    $app = new AppRequest;
    $sql = new sql;
    
    $perfil = $sql->query("SELECT cd_perfil, nm_perfil FROM usuario_perfil WHERE cd_perfil = ".$app->g('cod') );
    $perfil = $perfil->fetch();
    
    extract($perfil);
    
?>
<style>
#TPerfilAcesso{
    border:1px solid #fff;
    font:12px arial;
    color: black
}
#TPerfilAcesso th{
    padding:5px;
    font: bold 12px verdana;    
}
#TPerfilAcesso tbody{
    height: auto;
    overflow:visible;
}
#TPerfilAcesso tbody td{
    border-bottom:1px solid #c0c0c0
}
</style>
<script>
$(document).ready(function(){
    
    $('#TPerfilAcesso tbody tr td input[type="checkbox"]').each(function(){ 
        if($(this).attr('checked') == true){
           $(this).parent().parent().css('background', '#DDECFE'); 
        }
    });

    $('#TPerfilAcesso tbody tr td input[type="checkbox"]').live('click',function(){
        if($(this).attr('checked')){
           $(this).parent().parent().css('background', '#DDECFE');     
        }else{
           $(this).parent().parent().css('background','none');   
        }
    });
    
    $('#todos').click(function(){
        if($('#todos').attr('checked')){
           $('#TPerfilAcesso tbody input').attr('checked', true);
           $('#TPerfilAcesso tbody tr').css('background','#DDECFE');  
        }else{
           $('#TPerfilAcesso tbody input').attr('checked', false);
           $('#TPerfilAcesso tbody tr').css('background','#FFF');   
        }
    });

})
</script>
<div class="titulo">  Perfis / Editar / <?=$nm_perfil;?>
    <?//=menuPerfil()?>
</div>
<?
    // Salvando o Perfil
    $app = new AppRequest;

    if($app->g('action') == 'save'){  
        
       if($app->g('cod')){  // chave do perfil
        
            if($app->p('NOME')){  // nome perfil

                // Atualizando o nome do Perfil
                $up_perfil = $sql->query("UPDATE usuario_perfil SET nm_perfil = '".$app->p('NOME')."' where cd_perfil = ".$app->g('cod')." ");
     
                if($up_perfil->saved()){
                    
                    $sql_ins = 'INSERT INTO ACESSO_PERFIL (CD_PERFIL, CD_ACESSO) VALUES'; 
                    $sql_del = 'DELETE FROM ACESSO_PERFIL WHERE (CD_PERFIL = '.$app->g('cod').' ) AND ( '; 
               
                    $vals    = '';
                    $dels    = '';
                    $id      = $app->g('cod');
                    
                    
                    // INSERINDO ..  
                    if($_POST['acs']){                  
                        foreach($_POST['acs'] as $p => $k){
                          // verifica se já existe o vínculo com o perfil.
                          $busca = $sql->query("SELECT * FROM ACESSO_PERFIL AP WHERE AP.cd_perfil = ".$app->g('cod')." AND AP.CD_ACESSO = ".$p)->count();                        
                             if($busca == 0){  // senão existir o vinculo com perfil. Então, DEVEMOS INSERIR ! (criaremos o SQL de INSERT)
                                if($vals <> ''){
                                 $vals .= ', ('.$id.' ,'.$p.')';  
                                }else{
                                 $vals .= ' ('.$id.','.$p.')'; 
                                }                              
                             }
                          $vals   .= '';
                        }
                    }
                    
                    //DELETANDO..
                    $exe = $sql->query("SELECT * FROM ACESSO_PERFIL AP WHERE AP.cd_perfil = ".$app->g('cod'));
                    while($r = $exe->fetch()){

                        // Tem no banco mais nao foi enviado (por POST) novamente. Então DELETA !
                        if(!isset($_POST['acs'][$r['CD_ACESSO']])){
                            if($dels == ''){
                                $dels .= ' (CD_ACESSO = '.$r['CD_ACESSO'].' )';
                            }else{
                                $dels .= ' OR (CD_ACESSO = '.$r['CD_ACESSO'].' )';
                            }
                            // $dels .= 'DELETE FROM ACESSO_PERFIL WHERE CD_PERFIL = '.$app->g('cod').' AND CD_ACESSO =  '.$r['CD_ACESSO'].'; '; 
                        }
                    }
                    
                    // senão estiver vazio é porque tem registro para serem deletados ..
                    if($dels <> ''){
                        $delete_acessos = $sql->query($sql_del.$dels.')');
                        if(!$delete_acessos->saved()){
                            echo '
                            <div class="error">
                                <img src="/imagens/icons/error_add.png" />                        
                                Houve um erro inesperado no momento de atualizar os acessos. Por favor, preencha o perfil corretamente e tente novamente.
                                <br />
                                <br />
                                <a href="?cod='.$app->g('cod').'"> voltar para a edição </a>   
                            </div> ';
                        } 
                    }
                    
                    
                    if($vals <> ''){  // Senão estiver vazio. é porque contem registros para inserir !
                          
                        $insert_acessos = $sql->query($sql_ins.$vals);    
     
                        if($insert_acessos->saved()){  // salvo com sucesso.  exibe a mensagem .
                            echo ' 
                            <div class="success">
                              <img src="/imagens/icons/accept.png" />
                              O perfil <b>'.$app->p('NOME').'</b> e suas permissões de acesso, foram salvos com sucesso !
                              
                             
                              <a href="permissao_acesso.php"> Voltar para Perfis </a>
                               
                             </div>';
                             
                        }else{  // Problema inesperado ao inserir acessos
                            
                            echo '
                            <div class="error">
                                <img src="/imagens/icons/error_add.png" />                        
                                Perfil <b>'.$app->p('NOME').'</b> cadastrado. <br />
                                Mas não foi possível salvar as permissões de acesso para este perfil por um erro inesperado. <br />
                                Para solucionar, edite este mesmo perfil inserindo os acessos corretamente.
                            </div> DEBUG SQL: <BR />'.$sql_ins.$vals;
                        }
                       
   
                    }else{  // Não temos registro novo para inserir. Apenas modificou o nome do perfil com sucesso.
                        echo '
                        <div class="success">
                          <img src="/imagens/icons/accept.png" />
                          O perfil <b>'.$app->p('NOME').'</b> Foi modificado com sucesso !
                          
                          <a href="permissao_acesso.php"> Voltar para Perfis </a> 
                         </div>';
                    }
                    
                   
                }else{  // Não conseguiu modificar o nome do perfil. erro inesperado
                    echo '
                    <div class="error">
                        <img src="/imagens/icons/error_add.png" />                        
                        Não foi possível alterar o perfil devido um erro inesperado. Por favor tente novamente.
                        <br />
                        <br />
                        <a href="?cod='.$app->g('cod').'"> voltar para a edição </a>   
                    </div> ';   
                }
                
            }else{ // O nome do perfil esta em branco
                echo '
                <div class="error">
                    <img src="/imagens/icons/error_add.png" />                        
                    Por favor, coloque um nome correto para o Perfil.
                    <br />
                    <br />
                    <a href="?cod='.$app->g('cod').'"> voltar para a edição </a>   
                </div> ';           
            }
            
        }else{  // Código do perfil não informado
            echo '
            <div class="error">
                <img src="/imagens/icons/error_add.png" />                        
                Faltam dados necessários para a edição.
                <br />
                <br />
                <a href="permissao_acesso.php"> < voltar  </a>
            </div> ';             
        }
        
    }
    



?>  <br />
    <form method="post" action="?cod=<?=$cd_perfil;?>&action=save">
    <table border="0" width="100%">
        <tr>
            <th width="140">Nome do perfil:</th>
            <td>
                <input type="text" name="NOME" value="<?=$nm_perfil;?>" style="font:bold 16px verdana;width:400px;" />
               <!-- <input type="submit" value="Salvar edição" style="float: right;" /> -->
                <button> <img src="/imagens/save.png"  /> Salvar Edição</button>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>    
        <tr>
            <th>Acessos:</th>
            <td>
                    <!-- Início da tabela Perfil acessos -->
                    <table id="TPerfilAcesso" class="TVVSGrid">
                    <thead>
                        <tr>
                          <th> Descrição do acesso</th><th width="90"> <input type="checkbox" id="todos" title="marcar todos" /> Permitir ? </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?
                        $strsql =
                        "
                            SELECT A.CD_ACESSO, A.NOME_ACESSO , 
                                   CASE WHEN (
                                          (SELECT COUNT(CD_ACESSO) CNT 
                                             FROM ACESSO_PERFIL 
                                            WHERE CD_ACESSO = A.CD_ACESSO 
                                              AND CD_PERFIL = ".$app->g('cod')." ) = 0 
                                   )THEN 
                                       'N'  
                                   ELSE    
                                       'Y' 
                                   END  FLAGPOSSUI 
                              FROM ACESSO A      
                              order by TIPO_ACESSO          
                       ";       
                    
                        $sql = new sql; 
                        $res = $sql ->query($strsql);
                        
                      
                        while($r = $res->fetch()){
                            
                            if($r['FLAGPOSSUI'] == 'Y'){
                               $attr_check = ' checked="true" value="on" ';                    
                            }else{
                               $attr_check = ' value="off"'; 
                            }
             
                            echo '<tr><td>'.$r['NOME_ACESSO'].'</td> <td><input '.$attr_check.' type="checkbox" name="acs['.$r['CD_ACESSO'].']" /></td> </tr>';      
                                  
                        }
                    ?>
                   </tbody>
                   </table>
                   <!-- FIM da Tabela Perfil acessos -->
                   
                   <div style="text-align: right;"><br />
                    <button > <img src="/imagens/save.png"  /> Salvar Edição</button>
                   </div>
            </td>
        </tr>
</table>                   
    </form>

</body>
</html>
