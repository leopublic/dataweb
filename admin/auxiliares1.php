<?php
$opcao = "USU";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/auxiliares.php");

echo Topo($opcao);
echo Menu($opcao);

$orderby = trim($_POST['orderby']);
$id=trim($_POST['id']);
if(strlen($id)==0) { $id=$_GET['id']; }
$codigo = $_POST['codigo'];
$nome = $_POST['nome'];
$ingles = $_POST['ingles'];
$extra = $_POST['extra'];
$acao = $_POST['acao'];

$col1 = "C&oacute;digo";
$col2 = "Nome";
$col3 = "Ingles";
$col4 = "";
$ret = "";

if($id=="ESC") {
  if(strlen($orderby)==0) { $orderby="NO_ESCOLARIDADE"; }
  $titulo = "Escolaridade";
  if($acao=="I") { $ret = insereEscolaridade($codigo,$nome,$ingles); }
  else if($acao=="A") { $ret = alteraEscolaridade($codigo,$nome,$ingles); }
  else if($acao=="R") { $ret = removeEscolaridade($codigo); }
  $lista = listaGeral("ESCOLARIDADE","ORDER BY $orderby");
} else if($id=="EST") {
  if(strlen($orderby)==0) { $orderby="NO_ESTADO_CIVIL"; }
  $titulo = "Estado Civil";
  if($acao=="I") { $ret = insereEstadoCivil($codigo,$nome,$ingles); }
  else if($acao=="A") { $ret = alteraEstadoCivil($codigo,$nome,$ingles); }
  else if($acao=="R") { $ret = removeEstadoCivil($codigo); }
  $lista = listaGeral("ESTADO_CIVIL","ORDER BY $orderby");
} else if($id=="FUN") {
  if(strlen($orderby)==0) { $orderby="NO_FUNCAO"; }
  $titulo = "Fun&ccedil;&atilde;o/Cargo";
  $col4 = "Num. CBO";
  if($acao=="I") { $ret = insereFuncaoCargo($codigo,$nome,$ingles,$extra); }
  else if($acao=="A") { $ret = alteraFuncaoCargo($codigo,$nome,$ingles,$extra); }
  else if($acao=="R") { $ret = removeFuncaoCargo($codigo); }
  $lista = listaGeral("FUNCAO_CARGO","ORDER BY $orderby");
} else if($id=="PAR") {
  if(strlen($orderby)==0) { $orderby="NO_GRAU_PARENTESCO"; }
  $titulo = "Grau Parentesco";
  if($acao=="I") { $ret = insereGrauParentesco($codigo,$nome,$ingles); }
  else if($acao=="A") { $ret = alteraGrauParentesco($codigo,$nome,$ingles); }
  else if($acao=="R") { $ret = removeGrauParentesco($codigo); }
  $lista = listaGeral("GRAU_PARENTESCO","ORDER BY $orderby");
} else if($id=="PRO") {
  if(strlen($orderby)==0) { $orderby="NO_PROFISSAO"; }
  $titulo = "Profiss&atilde;o";
  $col4 = "Num. CBO";
  if($acao=="I") { $ret = insereProfissao($codigo,$nome,$ingles,$extra); }
  else if($acao=="A") { $ret = alteraProfissao($codigo,$nome,$ingles,$extra); }
  else if($acao=="R") { $ret = removeProfissao($codigo); }
  $lista = listaGeral("PROFISSAO","ORDER BY $orderby");
} else if($id=="TPA") {
  if(strlen($orderby)==0) { $orderby="NO_TIPO_AUTORIZACAO"; }
  $titulo = "Tipo Autoriza&ccedil;&atilde;o";
  $col3 = "Reduzido";
  if($acao=="I") { $ret = insereTipoAutorizacao($codigo,$nome,$ingles); }
  else if($acao=="A") { $ret = alteraTipoAutorizacao($codigo,$nome,$ingles); }
  else if($acao=="R") { $ret = removeTipoAutorizacao($codigo); }
  $lista = listaGeral("TIPO_AUTORIZACAO","ORDER BY $orderby");
} else if($id=="TPS") {
  if(strlen($orderby)==0) { $orderby="NO_TIPO_SOLICITACAO"; }
  $titulo = "Tipo Solicita&ccedil;&atilde;o";
  $col3 = "Dias Atend.";
  if($acao=="I") { $ret = insereTipoSolicitacao($codigo,$nome,$ingles); }
  else if($acao=="A") { $ret = alteraTipoSolicitacao($codigo,$nome,$ingles); }
  else if($acao=="R") { $ret = removeTipoSolicitacao($codigo); }
  $lista = listaGeral("TIPO_SOLICITACAO","ORDER BY $orderby");
} else if($id=="CNT") {
  if(strlen($orderby)==0) { $orderby="nm_nivel"; }
  $titulo = "Nivel do Contato";
  if($acao=="I") { $ret = insereNivelContato($codigo,$nome,$ingles); }
  else if($acao=="A") { $ret = alteraNivelContato($codigo,$nome,$ingles); }
  else if($acao=="R") { $ret = removeNivelContato($codigo); }
  $lista = listaGeral("contato_nivel","ORDER BY $orderby");
} else {
  $titulo = "Tabelas Auxiliares";
}

$botao = " type='Button' class='textformtopo' style=\"<?=$estilo?>\" ";
?>

    <br><center>
  	<table border=0 width="95%">
	 <tr>
	  <td class="textobasico" align="center" valign="top" colspan="3">
		<p align="center" class="textoazul"><strong>:: <?=$titulo?> ::</strong></p>				
	  </td>
	 </tr>
	 <tr><td colspan="3"><br></td></tr>
	 <form action="auxiliares1.php" method="post" name='forms'>
	 <input type="hidden" name="id" value='<?=$id?>'>
	 <input type="hidden" name="acao" value='I'>
		
     <tr>
      <td width=100% align=center valign=top>
      
<?php

echo "<table width=100% cellpadding=0 cellspacing=0 border=1 class='textoazulPeq'>\n";
echo "<tr bgcolor='#DADADA'>\n";
echo "<td align=center><b>$col1</td>\n";
echo "<td align=center><b>$col2</td>\n";
echo "<td align=center><b>$col3</td>\n";
if( ($id=="PRO") || ($id=="FUN") ) { echo "<td align=center><b>$col4</td>\n"; } 
echo "<td align=center><b>A&ccedil;&atilde;o</td>\n";
echo "</tr>\n";

while($rw=mysql_fetch_array($lista)) {
  $codigo = $rw[0];
  $nome = $rw[1];
  $ingles = $rw[2];
  if( ($id=="PRO") || ($id=="FUN") ) { $extra=$rw[3]; }
  $remove = "<a href=\"javascript:remove1('$codigo','$nome','$ingles','$extra');\"><b>R</a>";
  $altera = "<a href=\"javascript:altera1('$codigo','$nome','$ingles','$extra');\"><b>A</a>";
  echo "<tr>\n<td align=center>$codigo</td>\n";
  echo "<td>&#160;$nome</td>\n";
  echo "<td>&#160;$ingles</td>\n";
  if( ($id=="PRO") || ($id=="FUN") ) { echo "<td><b>&#160;$extra</td>\n"; }
  echo "<td align=center>$altera &#160; $remove</td>\n";
  echo "</tr>\n";
}

echo "</table>\n";
echo "</td>\n";
echo "<td width=20>&#160;</td>\n";
echo "<td width=220 align=center valign=top>\n";
echo "<table width=100% border=1 cellspacing=0 cellpadding=0 class='textoazulPeq'>\n";
if( ($id=="FUN") || ($id=="PRO") ) { 
  echo "<input type=hidden name=codigo>\n"; 
} else {
  echo "<tr height=20><td><b>$col1:</td><td><input type=text name=codigo size=10></td></tr>\n"; 
}
echo "<tr height=20><td><b>$col2:</td><td><input type=text name=nome size=20></td></tr>\n";
echo "<tr height=20><td><b><nobr>$col3:</nobr></td><td><input type=text name=ingles size=20></td></tr>\n";
if( ($id=="PRO") || ($id=="FUN") ) { 
  echo "<tr height=20><td><b>$col4:</td><td><input type=text name=extra size=20></td></tr>\n"; 
} else {
  echo "<input type=hidden name=extra>\n"; 
}
?>     
        <tr id="mostra_inserir" style="display:block" height=20><td colspan=2 align=center>
            <input <?=$botao?> value="Incluir" onclick="javascript:Continuar1('I');"></tr>
        <tr id="mostra_alterar" style="display:none" height=20><td colspan=2 align=center>
            <input <?=$botao?> value="Alterar" onclick="javascript:Continuar1('A');"></tr>
        <tr id="mostra_remover" style="display:none" height=20><td colspan=2 align=center>
            <input <?=$botao?> value="Remover" onclick="javascript:Continuar1('R');"></tr>
       </table>
      </td>
     </tr>
	 </form>
	</table>

<?php
echo Rodape($opcao);
include ("../javascript/auxiliares_js.php");
?>

