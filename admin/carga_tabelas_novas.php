<?php
include ("../LIB/conecta.php");
include ("../LIB/libOperacoes.php");
include ("../LIB/libVisaDetail.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/classes/classe_proc_cancel.php");
include ("../LIB/classes/classe_proc_emiscie.php");
include ("../LIB/classes/classe_proc_mte.php");
include ("../LIB/classes/classe_proc_prorrog.php");
include ("../LIB/classes/classe_proc_regcie.php");

$ini = date("d/m/Y H:i:s");

print "$ini - Limpando DB ... ";
LimpaDB();
print "OK <br>\n ";

$sql = "select NU_EMPRESA,NU_CANDIDATO,NU_SOLICITACAO from AUTORIZACAO_CANDIDATO where NU_CANDIDATO between 1001 and 2000";
$rs = mysql_query($sql);
while($rw=mysql_fetch_array($rs)) {
  $idCandidato = $rw['NU_CANDIDATO'];
  $idSolicita = $rw['NU_SOLICITACAO'];
  $idEmpresa = $rw['NU_EMPRESA'];
  $aut = lerVisaDetail($idEmpresa,$idCandidato,$idSolicita);
  $tipo = $aut->RetornaArray();
  $ret = Carrega1($idCandidato,$idSolicita,$tipo);
  if(strlen($ret)>0) { Print "\n<br>$ret"; }
}

$fim = date("d/m/Y H:i:s");

print "FIM = $fim";

function Carrega1($idCandidato,$idSolicita,$tipo) {
   $obj = new proc_mte();
   $msg = $msg.Carrega2($obj,$idCandidato,$idSolicita,$tipo);
   $obj = new proc_regcie();
   $msg = $msg.Carrega2($obj,$idCandidato,$idSolicita,$tipo);
   $obj = new proc_emiscie();
   $msg = $msg.Carrega2($obj,$idCandidato,$idSolicita,$tipo);
   $obj = new proc_prorrog();
   $msg = $msg.Carrega2($obj,$idCandidato,$idSolicita,$tipo);
   $obj = new proc_cancel();
   $msg = $msg.Carrega2($obj,$idCandidato,$idSolicita,$tipo);
   return $msg;
}


function Carrega2($obj,$idCandidato,$idSolicita,$tipo) {
    $tipo['cd_candidato']=$idCandidato;
    $tipo['cd_solicitacao']=$idSolicita;
    $obj->LeituraRequest($tipo);
    if($obj->ExisteCandidato($idCandidato,$idSolicita)==true) {
      $obj->AlteraProcesso();
    } else {
      $obj->InsereProcesso();
    }
    $obj->AlteraVisaDetail();
    return $obj->myerr;
}

function LimpaDB() {
  $sql = "DELETE FROM processo_prorrog";
  mysql_query($sql);
  $sql = "DELETE FROM processo_regcie";
  mysql_query($sql);
  $sql = "DELETE FROM processo_cancel";
  mysql_query($sql);
  $sql = "DELETE FROM processo_emiscie";
  mysql_query($sql);
  $sql = "DELETE FROM processo_mte";
  mysql_query($sql);
}

?>
