<?php


/**
 * @author Fábio Pereira
 * @copyright M2 Software 2010
 */

  
    include ("../LIB/autenticacao.php");
    include ("../LIB/cabecalho.php");
    include ("../LIB/geral.php");
    include ("../LIB/combos.php");
    include ("../LIB/auxiliares.php");

    echo Topo('Serviços');
    echo Menu();
    
?>
<style>
#TServico{          margin:10px;width: 98%;}
#TServico tbody{    overflow: visible;}
.edicao td{padding-top: 3px;padding-bottom:3px}
.edicao td select {width:auto}
</style>
<script>
$(document).ready(function(){
     $('#TServico').Grid({
        request : '../ajax/servico.php', 
        title: 'Serviço', 
        msgCfmDelete: 'Para segurança, NÃO É PERMITIDO A EXCLUSÃO DE SERVIÇOS.  Pois podem haver outros vínculos com este registro !',
        msgErroDelete: 'O sistema não permite exclusão de serviços',
        bodyHeight: '570px',
        BoxEditWidth: '800px',
        BoxAddWidth : '800px'
    });
});
</script>
<div class="titulo"> Serviços </div>
<table id="TServico">
<thead>
    <tr>
        <th colspan="4" style="border-bottom: none;text-align: right;">
            <button id="add"> <img src="/imagens/icons/add.png" /> Adicionar novo serviço</button>
        </th>  
    </tr> 
    <tr>
        <th width="80">Código</th>
        <th width="300">Nome Serviço Resumido</th>
        <th>Nome Serviço Completo</th>    
    </tr>
</thead>
<tbody><tr><td colspan="4" align="center">Aguarde, carregando dados </td></tr></tbody>
</table>

</body>
</html>
