<?php
$opcao = "USU";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$nomeUsuario = $_POST["nomeUsuario"];
$idEmpresa = $_POST["idEmpresa"];
$usuario = $_POST["cd_usuario"];
$acao = $_POST["acao"];
if (strlen($idEmpresa) > 0) {
  $condicaoEmpresa = " and cd_empresa=$idEmpresa ";
  $nome_empresa = pegaNomeEmpresa($idEmpresa);
} else {
  $empresas = montaComboEmpresas("","ORDER BY NO_RAZAO_SOCIAL");
}
if($acao=="R") {
  $sql = "DELETE FROM usuarios WHERE cd_usuario=$usuario";
  mysql_query($sql);
  if(mysql_errno()>0) {
    gravaLogErro($sql,mysql_error(),"REMOCAO","USUARIOS");
  } else {
    gravaLog($sql,mysql_error(),"REMOCAO","USUARIOS");
    $sql = "DELETE FROM usuario_empresa WHERE cd_usuario=$usuario";
    mysql_query($sql);
  }
  if(mysql_errno()>0) { $msg="<br>Erro ao remover usuário. <!-- SQL=$sql\nERRO: ".mysql_error()."-->"; }
}

$usuarios = buscaUsuarios("","",$idEmpresa,"",$nomeUsuario,"order by nome"); # buscaUsuarios($cd_usuario,$cd_nivel,$cd_empresa,$nm_login,$nm_nome,$orderby)

echo Topo($opcao);
echo Menu($opcao);

?>

<br><center>
<table border=0 width="90%">
 <tr>
  <td class="textobasico" align="center" valign="top" colspan="2">
	<p align="center" class="textoazul"><strong>:: Cadastro de Usu&aacute;rios ::</strong></p>
  </td>
 </tr>
 <tr><td><?=$msg?><br></td></tr>
 <form action="usuarios.php" method="post" name=usuarios>
 <input type=hidden name=acao>
 <input type=hidden name=cd_usuario>
 <tr>
  <td class="textoazul" width="20%">Buscar usu&aacute;rio:</td>
  <td><input type="text" name="nomeUsuario" value="" size="30"></td>
 </tr>
 <tr>
  <td class='textoazul'>Empresa:</td>
<?php
if (strlen($idEmpresa) > 0) {
  echo "<input type=hidden name=idEmpresa value='$idEmpresa'>";
  echo "<td>$nome_empresa\n";
} else {
  echo "<td><select name='idEmpresa'><option value=''>Todos...</option>$empresas</select>\n";
}
?>
  </td>
 </tr>
 <tr><td><br /></td></tr>
 <tr>
  <td colspan="2" align="center">
	<input name="buscar" type="button" class="textformtopo" style="<?=$estilo?>" value="Busca" onclick="javascript:filtra();">&nbsp;&nbsp;
   	<input name="novo" type="Button" class="textformtopo" style="<?=$estilo?>" value="Novo usuário" onclick="javascript:inclui();">
  </td>
 </tr>
 </form>
</table>
<br>
<p class="textoazulpeq"><b>Total de usuários encontrados (<?=count($usuarios)?>)</p>
<br />
<table border=1 width="700" cellspacing=0 cellpadding=0 class="textoazulpeq">
 <tr align="center">
  <!-- td bgcolor="#DADADA"><b>No.</td -->
  <td bgcolor="#DADADA"><b>A&ccedil;&atilde;o</td>
  <td bgcolor="#DADADA"><b>Login</td>
  <td bgcolor="#DADADA"><b>Nome</td>
  <td bgcolor="#DADADA"><b>Perfil</td>
  <td bgcolor="#DADADA"><b>Empresa</td>
 </tr>
<?php
  if(count($usuarios)>0) {
    
    for($x=0;$x<count($usuarios);$x++) {      
      $obj       = $usuarios[$x];
      $id        = $obj->GETcd_usuario();
      $login     = $obj->GETnm_login();
      $nome      = $obj->GETnm_nome();
      $objP      = $obj->GETcd_perfil();
      $perfil    = $objP[0]->GETnm_perfil();
      $axempresa = $obj->GETcd_empresa();
      
      if($axempresa>0) {
        $nmempresa = pegaNomeEmpresa($axempresa);
        if(strlen($nmempresa)==0) { $nmempresa="Não encontrado"; }
      }else{   
        $nmempresa = "---";
      }
      echo "<tr>";
      echo "<td align='center' nowrap><b><a href='javascript:mostra($id);'>V</a>\n";
      if( ($ehAdmin=="S") || ($id==$usulogado) ) {
        echo "&#160;&#160;<a href='javascript:altera($id);'>A</a>\n";
      } 
      if($ehAdmin=="S") {
        echo "&#160;&#160;<a href='javascript:remove($id,\"$nome\");'>R</a></td>\n";
      }
      echo "<td><nobr>&#160;$login</td>";
      echo "<td><nobr>&#160;$nome</td>";
      echo "<td><nobr>&#160;$perfil</td>";
      echo "<td><nobr>&#160;$nmempresa</td>";
      echo "</tr>\n";
    }
  }
?>			
</table>
<br>

<script language="javascript">
function inclui() {
  openChild("","I");
}
function altera(usu) {
  openChild(usu,"A");
}
function mostra(usu) {
  openChild(usu,"V");
}
function filtra() {
  document.usuarios.cd_usuario.value='';
  document.usuarios.acao.value='F';
  document.usuarios.action='usuarios.php';
  document.usuarios.target='_top';
  document.usuarios.submit();
}
function remove(usu,nome) {
  if(confirm("Voce deseja remover o usuario "+nome+" ? ")) {
    document.usuarios.cd_usuario.value=usu;
    document.usuarios.acao.value='R';
    document.usuarios.action='usuarios.php';
    document.usuarios.target='_top';
    document.usuarios.submit();
  } else {
    alert("Operacao nao realizada.");
  }
}

function openChild(usu,acao) {
  var MyArgs = new Array(usu,acao);
  var pagina = "novoUsuario.php?cd_usuario="+usu+"&acao="+acao;
//  var WinSettings = "center:yes;resizable:yes;dialogHeight:550px;dialogWidth=700px"
//  var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
  var features = "width=700,height=550,top=100,left=50,scrollbars=1";
  window.open(pagina,"USUAR",features);
}

function openNewWindow(theURL,winName,features) {
  newWindow = window.open(theURL,winName,features);
}
</script>

<?php
echo Rodape($opcao);
?>
