<?php

    /**
     * @author Fábio Pereira
     * @copyright M2 Software 2010
     */

    $_dir = realpath('../LIB/').DIRECTORY_SEPARATOR;
       
    require_once $_dir.'autenticacao.php';
    require_once $_dir.'cabecalho.php';
    require_once $_dir.'geral.php';
    require_once $_dir.'combos.php';
    require_once $_dir.'auxiliares.php';


    $app = new Controle(__FILE__);   
    $win = new usuario_acesso_edit;
       
 
    extract($win->onLoad());
    $win->save($login);
    
    
    
    echo Topo('USU');
    echo Menu('USU');  
    
    
?>
<style>
#TEditUsuario{           margin:10px; width: 98%;}
#TEditUsuario th{        color:#000;}
#barMain{                font:16px verdana; margin:10px;padding:10px; background: #F3F3F3; height: 30px;}
#TUsuarioEmpresa tbody{  overflow: visible;height:auto; }
/*#cd_perfil{              font:bold 14px verdana;}*/
</style>
<script>
$(document).ready(function(){
    var cd_usuario = '&cd_usuario='+<?=$cd_usuario?>;
    $('#TUsuarioEmpresa').Grid({
        request        : '../ajax/usuario_empresa.php',
        title          : 'Empresa',
        qs_load_list   : '?action=list'+cd_usuario,
        qs_add         : '?action=add'+cd_usuario,
        qs_edit        : '?action=edit'+cd_usuario,
        qs_delete      : '?action=remove'+cd_usuario,
        qs_detail      : '?action=detailedit'+cd_usuario,
        qs_detail_add  : '?action=detailadd'+cd_usuario,
        qs_refresh_row : '?action=refreshrow'+cd_usuario,
        msgCfmDelete   : 'Tem certeza que deseja excluir?',
        bodyHeight     : '100px',
        BoxEditWidth   : '550px',
        BoxAddWidth    : '550px',
        actions        : true
    })
});
</script>
<div class="titulo"> Usuários / Editar / <?=$nome?> </div>
<form name="frmsave" action="?cod=<?=$cd_usuario?>&action=save" method="post">
    <? 
        if($win->isPost()){
            
              // carrego os novos dados que sofreram update  
              extract($win->onLoad());
              echo $win->msgHtml(); 
        }
     ?>
    <div id="barMain">
        <ul id="usuarioAcesso" class="menubar" style="float: right;">
            <li> <img src="/imagens/icons/group.png" /> <a href="usuarios_acesso.php"> Listagem de Usuários </a></li>
            <li onclick="forms['frmsave'].submit()"> <img src="/imagens/save.png" /> Salvar</li>
        </ul>
    </div>

    <table id="TEditUsuario" class="edicao" border="0" width="100%">
        <tr>
            <th width="150">Nome:</th>
            <td>
                <input type="text" name="nome" value="<?=$nome;?>" style="width:750px;" />
            </td>
        </tr>
        <tr>
            <th width="150">Login:</th>
            <td>
                <input type="text" name="login" value="<?=$login;?>" style="width:450px;" />
            </td>        
        </tr>
        <tr>
            <th width="150">Senha:</th>
            <td>
                <input type="password" name="nm_senha" value="<?=$nm_senha;?>" style="width:450px;" />
            </td>        
        </tr>
        <tr>
            <th>E-mail:</th>
            <td><input type="text" name="nm_email" value="<?=$nm_email;?>" style="width:450px;" /></td>
        </tr>
        <tr>
            <th>Perfil:</th>
            <td><?=$app->combo_perfil($cd_perfil)?></td>
        </tr>
        <tr>
            <th>Superior:</th>
            <td><?=$app->combo_superior($cd_superior)?></td>
        </tr>      
        <tr>
            <th>Empresa:</th>
            <td><?=$app->combo_empresa($cd_empresa)?></td>
        </tr>
    </table>
    <div style="text-align: center;">
    <button> <img src="/imagens/save.png" /> Salvar Cadastro</button>
    </div>

 </form>
 </body>
</html>
