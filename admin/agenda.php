<?php
$opcao = "USU";
include("../LIB/autenticacao.php");
include("../LIB/cabecalho.php");
include("../LIB/geral.php");
include("../LIB/combos.php");
include("../LIB/agenda/calendario_inc.php");
include("../LIB/agenda/agenda_inc.php");

$ax_admin = $_POST['ax_admin'];
if(strlen($ax_admin)==0 || ($ax_admin==0) ) { $ax_admin=$usulogado; }

$acao = $_POST['acao'];
$axhora = $_POST['axhora'];
$axtipo = $_POST['axtipo'];
$axtarefa = $_POST['axtarefa'];

$dia = PegaData();

if($acao == "A") { 
  $axcodigo = $_POST['axcodigo'];
  $ret = AlteraAgenda($axcodigo,$ax_admin,$usulogado,$dia,$axhora,$axtipo,$axtarefa);
} elseif($acao == "I") { 
  $ret = InsereAgenda($ax_admin,$usulogado,$dia,$axhora,$axtipo,$axtarefa); 
}

$calend = PegaCal(date("m",$dia),date("Y",$dia)," AND cd_usuario=$ax_admin ");

if(strlen($ret)>0) { $ret="<br>ERRO: $ret<br>\n"; }

$nomeUsuario = pegaNomeUsuario($ax_admin);

echo Topo($opcao);
echo Menu($opcao);

?>

<br>
<center>
<table border=0 width="700">
 <tr>
  <td class="textobasico" align="center" valign="top">
	<p align="center" class="textoazul"><strong>:: Agenda do Usuário - <?=$nomeUsuario?> ::</strong></p>
  </td>
 </tr>
</table>
<?=MontaJSCalendario("agenda.php",$ax_admin)?>
<br>
<table width=720 border=0 cellspacing=0 cellpadding=0 valign=top class='textoazulpeq'>
 <tr valign=top>
  <td width=250 align=center>
    <?=MontaCalendario($dia,$calend)?>
  </td>
  <td width=20>&#160;</td>
  <td width=450 align=center>
   <form action="agenda.php" method=post name=agenda>
   <input type=hidden name=acao value="I">
   <input type=hidden name=axcodigo>
   <input type=hidden name=ax_admin value='<?=$ax_admin?>'>
   <input type=hidden name=novadata value='<?=$dia?>'>
   <table width=94% border=1 cellspacing=0 cellpadding=0 valign=top class='textoazulpeq'>
    <tr>
     <td> 
      <table border=0 width=100% class='textoazulpeq'>
       <td width=120><b>Dia: <?=date("d/m/Y",$dia)?></td>
       <td width=120><b>Hora: <select name=axhora class='textoazulpeq'><?=GeraHoras()?></select></td>
       <td><b>Status: <select name=axtipo class='textoazulpeq'><?=GeraTipos()?></select></td>
      </table>
     </td>
    </tr>
    <tr> 
     <td>
      <table border=0 width=100% class='textoazulpeq'>
       <tr valign=top>
        <td width=75><b>Tarefa:</td>
        <td><textarea cols=60 rows=5 name=axtarefa class='textoazulpeq'></textarea></td>
       </tr>
      </table>
     </td>
    </tr>
    <tr id=btinc style='display:block'><td align=center><input type=submit value="Incluir" class='textoazulpeq'></td></tr>
    <tr id=btalt style='display:none'><td align=center><input type=submit value="Alterar" class='textoazulpeq'></td></tr>
    <tr id=btrem style='display:none'><td align=center><input type=submit value="Remover" class='textoazulpeq'></td></tr>
   </table>
   </form>
  </td>
 </tr>
 <tr>
  <td colspan=3>
    <?=AgendaDoDia($dia,$ax_admin)?>
  </td>
 </tr>
</table>

<?php    if($usuperfil<5) {   ?>

<br>
<table border=0 width=700 class='textoazulpeq'>
 <form name="axusu" method=post action='agenda.php'>
 <input type=hidden name=novadata value='<?=$dia?>'>
 <tr valign=top>
  <td width=100><b>Usuário:</td>
  <td width=300><select name=ax_admin class='textoazulpeq'><?=montaComboUsuarios($ax_admin," order by nome")?></select></td>
  <td width=300><input type=submit value="Altera Usuário" class='textoazulpeq'></td>
 </tr>
 </form>
</table>

<?php   }   ?>

<?php
echo Rodape($opcao);
?>

<script language="JavaScript">
function Altera(cod,hora,tarefa,tipo) {
  document.agenda.axtarefa.value=tarefa;
  document.agenda.acao.value="A";
  document.agenda.axcodigo.value=cod;
  document.agenda.axhora[hora].selected = true;
  document.all.btinc.style.display = 'none';
  document.all.btalt.style.display = 'block';
  document.all.btrem.style.display = 'none';
}

</script>
