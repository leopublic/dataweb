<?php
/**
 * @author Leonardo Medeiros
 * @copyright M2 Software 2010
 */
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/auxiliares.php");

echo Topo('País/Nacionalidade');
echo Menu();
    
?>
<style>
#TServico		{margin:10px; width: 98%;}
#TServico tbody	{overflow:visible;}
.edicao td		{padding-top: 3px; padding-bottom:3px;}
.edicao td select {width:auto;}
</style>
<script>
$(document).ready(function(){
     $('#TServico').Grid({
        request : '../ajax/pais_nacionalidade.php', 
        title: 'País/Nacionalidade', 
        msgCfmDelete: 'Para segurança, NÃO É PERMITIDO A EXCLUSÃO DE PAÍSES/NACIONALIDADES, pois podem haver outros vínculos com este registro !',
        msgErroDelete: 'O sistema não permite exclusão de países/nacionalidades',
        bodyHeight: '570px',
        BoxEditWidth: '800px',
        BoxAddWidth : '800px'
    });
});
</script>
<div class="titulo"> Países/Nacionalidades </div>
	<table id="TServico">
		<thead>
<?php 
   if(Acesso::permitido(Acesso::tp_Sistema_Tabelas_Pais_Cadastrar)){  
?>		    <tr>
		        <th colspan="7" style="border-bottom: none;text-align: right;">
		            <button id="add"> <img src="/imagens/icons/add.png" /> Adicionar novo país/nacionalidade</button>
		        </th>  
		    </tr> 
<?php 
   }?>
		    <tr>
		        <th width="100px">País</th>
		        <th width="100px">Nacionalidade</th>
		        <th width="100px">País (em inglês)</th>
		        <th width="100px">Nacionalidade (em inglês)</th>
		        <th width="100px">Código PF</th>
		    </tr>
		</thead>
		<tbody>
			<tr><td colspan="5" align="center">Aguarde, carregando dados ...</td></tr>
		</tbody>
	</table>
</body>
</html>
