<?php


/**
 * @author Leonardo
 * @copyright M2 Software 2010
 */

  
    include ("../LIB/autenticacao.php");
    include ("../LIB/cabecalho.php");
    include ("../LIB/geral.php");
    include ("../LIB/combos.php");
    include ("../LIB/auxiliares.php");

    echo Topo('Tipo Autorização');
    echo Menu();
    
?>
<style>
#TServico{          margin:10px;width: 98%;}
#TServico tbody{    overflow: visible;}
.edicao td{padding-top: 3px;padding-bottom:3px}
.edicao td select {width:auto}
</style>
<script>
$(document).ready(function(){
     $('#TServico').Grid({
        request : '../ajax/tipoAutorizacao.php', 
        title: 'Tipo Autorização', 
        msgCfmDelete: 'Para segurança, NÃO É PERMITIDA A EXCLUSÃO DE TIPOS DE AUTORIZAÇÃO.  Pois podem haver outros vínculos com este registro !',
        msgErroDelete: 'O sistema não permite exclusão de tipo de autorização',
        bodyHeight: '570px',
        BoxEditWidth: '800px',
        BoxAddWidth : '800px'
    });
});
</script>
<div class="titulo"> Serviços </div>
<table id="TServico">
<thead>
    <tr>
        <th colspan="3" style="border-bottom: none;text-align: right;">
            <button id="add"> <img src="/imagens/icons/add.png" /> Adicionar novo tipo de autorização</button>
        </th>  
    </tr> 
    <tr>
        <th width="50">Código</th>
        <th width="450">Descrição</th>
        <th>Descrição (resumida)</th>    
    </tr>
</thead>
<tbody><tr><td colspan="3" align="center">Aguarde, carregando dados </td></tr></tbody>
</table>

</body>
</html>
