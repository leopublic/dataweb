<?php
//error_reporting(E_ALL); 
//ini_set("display_errors", 1);
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/arquivos.php");

$idEmpresa = 0+$_POST['idEmpresa'];
$idCandidato = 0+$_POST['idCandidato'];
$idProjeto = 0+$_POST['idProjeto'];


if($idEmpresa>0) {
  $nmEmpresa = pegaNomeEmpresa($idEmpresa);
} else {
  $nmEmpresa = "Nao informada a Empresa";
}
if($idProjeto>0) {
  $nmProjeto = pegaNomeProjeto($idProjeto,$idEmpresa);
} else {
  $nmProjeto = "Nao informado Projeto/Embarcacao";
}
if($idCandidato>0)  {
  $nmCandidato = pegaNomeCandidato($idCandidato);
} else {
  $nmCandidato = "Nao informado o Candidato";
}

if ($_POST['tipo']=='')
{
	$msgErro = 'Por favor informe o tipo do arquivo.';
}
else
{
	if ($_FILES["userfile"]["error"] > 0)
	  {
	  //echo "Error: " . $_FILES["userfile"]["error"] . "<br />";
	  $msgErro = 'Não foi possível carregar o arquivo. Erro='.$_FILES["userfile"]["error"];
	  }
	else
	  {
	  //echo "Upload: " . $_FILES["userfile"]["name"] . "<br />";
	  //echo "Type: " . $_FILES["userfile"]["type"] . "<br />";
	  //echo "Size: " . ($_FILES["userfile"]["size"] / 1024) . " Kb<br />";
	  //echo "Stored in: " . $_FILES["userfile"]["tmp_name"];
		$tipos = ListaTipoDocumentos();
		$msgErro = GravaDocumentos();
	  }
}

$nmArq = "";
if($msgErro == "") {
  $msg = "Inclusão de documento executada com sucesso!";
  if($tipoArquivo>0) { $nmArq = $tipos[$tipoArquivo]; }
  $to  = "mundivisas@mundivisas.com.br";
#  $to  = $to.",romulo@oceanicawebhome.com.br";
  $texto = "Prezado(a),\n\nFoi enviado documentos para:\n Empresa: $nmEmpresa\n";
  $texto = $texto." Embarcacao: $nmProjeto\n Candidato: $nmCandidato\n";
  $texto = $texto." Arquivo: $noOriginal\n Tipo do arquivo: $nmArq\n";
  $texto = $texto." Data: ".date("d/m/Y H:i:s")."\n Admin: ".pegaNomeUsuario($usulogado);
  mail($to, $nmProjeto, $texto, "From: sistema@mundivisas.com.br\r\n");
} else {
  $msg = "Inclusão de documento não foi executada.";
}
?>
<html>
<body onload="javascript:Done();">
<br><br>
<p align=center><font color=red>Aguarde !!!</font>
<script language="javascript">
window.name = "ARQ";
function Done() {
    alert("<?=$msg?>\n"+"<?=$msgErro?>");
    var msg = "<?=$msg?>";
    var msgErro = "<?=$msgErro?>";
    var MyArgs = new Array(msg,msgErro);
    window.returnValue = MyArgs;
    window.close();
}
</script>
