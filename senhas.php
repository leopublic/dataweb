<?php
$opcao="";
include ("LIB/autenticacao.php");
include ("LIB/cabecalho.php");
include ("LIB/geral.php");

$fase = 0 + $_POST['fase'];

if($lang=="E") {
  $titulo = "Change Password";
  $txtNome = "Name";
  $txtSenha = "Password";
  $txtConfirma = "Confirm";
  $txtTrocar = "Change";
  $txtSuc = "Success change password.";
  $txtInSuc = "Password not changed.";
  $txtAlerta = "The password and the confirmation are diferents.";
} else {
  $titulo = "Troca de Senha";
  $txtNome = "Nome";
  $txtSenha = "Senha";
  $txtConfirma = "Confirmação";
  $txtTrocar = "Trocar";
  $txtSuc = "Senha alterada com sucesso.";
  $txtInSuc = "Senha não foi alterada.";
  $txtAlerta = "A senha e a confirmação são diferentes.";
}

$txtInpSenha = "";
$txtInpConfirm = "";
$txtInpNome = pegaNomeUsuario($usulogado);
if($fase > 0) {
  $trsenha = $_POST['trsenha'];
  if(strlen($trsenha)>5) {
     $msg = TrocaSenha($usulogado,$trsenha);
     if(strlen($msg)>0) {
        $msg = $msg.$txtInSuc;
     } else {
        $msg = $txtSuc;
        $txtInpSenha = "******";
        $txtInpConfirm = "******";
     }
  } else {
    if($lang=="E") {
      $msg = "<font color=red>Invalid Password.</font><br><br>";
    } else {
      $msg = "<font color=red>Senha inválida.</font><br><br>";
    }
  }
} 
if(strlen($txtInpSenha)==0) {
  $txtInpSenha = "<input type=password name=trsenha class='textoazulpeq' size=20 maxlength=10>";
}
if(strlen($txtInpConfirm)==0) {
  $txtInpConfirm = "<input type=password name=trconf class='textoazulpeq' size=20 maxlength=10>";
}

echo Topo($opcao);
echo Menu($opcao);

print "<br><center><table border=0 width=600><tr><td class=textobasico align=center valign=top>";

print "<p align=center class=textoazul><strong>:: $titulo ::</strong></p>";	

print "<form action='senhas.php' method=post name=senha>\n";
print "<input type=hidden name=fase value=1>\n";

print "$msg<br><table border=0 width=600 class=textoazulpeq ><tr><td align=center valign=top>";
print "<tr><td>$txtNome :</td><td colspan=4>$txtInpNome</td></tr>\n";
print "<tr><td>$txtSenha :</td><td>$txtInpSenha</td><td width=30>&#160;</td>\n";
print " <td>$txtConfirma :</td><td>$txtInpConfirm</td></tr>\n";
echo "<tr><td colspan=5>&#160;</td></tr>\n";
if($fase == 0) {
  echo "<tr><td colspan=5 align=center width=100?><input type='button' class='textformtopo' style=\"$estilo\" value='$txtTrocar' onclick=\"javascript:Trocar();\"></td></tr>";
}
echo "</table>\n";

print "</td></tr></table></form>";

echo Rodape($opcao);

function TrocaSenha($usu,$trsenha) {
  global $lang;
  $ret = "";
  $sql = "UPDATE usuarios SET nm_senha='$trsenha' WHERE cd_usuario=$usu";
  mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = "<!-- ".mysql_error()." -->";
  }      
  return $ret;
}

?>
<script language="javascript">
function Trocar() {
   var sen1 = document.senha.trsenha.value;
   var sen2 = document.senha.trconf .value;
   if(sen1 == sen2) {
     document.senha.submit();
   } else {
     alert('<?=$txtAlerta?>');
   }
}
</script>
