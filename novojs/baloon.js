// BALLON TIP 
// baloes sobre os icones de menu
$(function() {
 	$('.main_options li.action').balloon({
		classname: 'balloon',
	});
});

// baloes sobre os itens de formulario
$(function() {
 	$('.tip').balloon({
		classname: 'balloon',
		position:'top',
	});
});

// RESPONSIVE MENU
$(function () {
    $("#header .nav.main ul").tinyNav({
		header: 'Main Menu',
	});
	$("#header .nav.option ul").tinyNav({
		header: 'Options',
	});
});

/*selecionar o texto ao clicar
$(function(){
	$('#content_main .box li input[readonly=readonly]').click(function() {
		$(this).select();	
	});
});

$(function(){
	$('#content_main .box li input[readonly=readonly]').dblclick(function() {
		$(this).removeAttr("readonly");
		$(this).addClass("editable");
		$(this).addClass("edited");
		// VERIFICAR 
		$('#top_main .info_box.atention').addClass("open",1000);
	});
});
$(function(){
	$('#content_main .box li p.content input').focusout(function() {
		$(this).attr("readonly","readonly");
		$(this).removeClass("editable");
	});
});*/

//ACTION ICONS ON LASTEST FILES
$(function() {
	$( "#sidebar_right .file_box li.iten" ).mouseenter( 	
		function() {
			$(this).children('.action').addClass("open",0);
		})
	$( "#sidebar_right .file_box li.iten" ).mouseleave( 	
		function() {
			$(this).children('.action').removeClass("open",0);
		})
});

// MENU PRINCIPAL 
$(function() {
	$( "#header .nav li" ).mouseover(
		function() {
			$( this ).addClass("open");
		})
	$( "#header .nav li" ).mouseout(
		function() {
			$( this ).removeClass("open");
		})
	});

// MENU ACTION
$(function() {
	$( "#content_main .service_box .adiction_menu.icon" ).mouseover(
		function() {
			$(this).children(".list" ).css("display","block");
		})
	});
$(function() {
	$( "#content_main .service_box .adiction_menu.icon" ).mouseout(
		function() {
			$(this).children(".list" ).css("display","none");
		})
	});

$(function() {
	$( "#content_main .service_box .action_menu.icon" ).mouseover(
		function() {
			$(this).children(".list" ).css("display","block");
		})
	});
$(function() {
	$( "#content_main .service_box .action_menu.icon" ).mouseout(
		function() {
			$(this).children(".list" ).css("display","none");
		})
	});
 
// WEB FONTS
  WebFontConfig = {
    google: { families: [ 'Open+Sans+Condensed:300:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();

// preenchimento automatico do title
$(function() {
	 var txtFirst = $('input[id$=CMP_NO_PRIMEIRO_NOME]').val();
	 var txtMiddle = $('input[id$=CMP_NO_NOME_MEIO]').val();
	 var txtLast = $('input[id$=CMP_NO_ULTIMO_NOME]').val();
	 var txtClone = txtFirst + txtMiddle + txtLast;
	 
    $('input[id$=CMP_NO_PRIMEIRO_NOME]').keyup(function() {
		var txtFirst = $(this).val();		
		txtClone = $('input[id$=CMP_NO_PRIMEIRO_NOME]').val() +" "+ $('input[id$=CMP_NO_NOME_MEIO]').val() +" "+ $('input[id$=CMP_NO_ULTIMO_NOME]').val();
		if ($('#title_main h2 .title') == '') {
			$('#title_main h2 .title').text("Novo Candidato");
		} else {
			$('#title_main h2 .title').text(txtClone);
		}
	});
	
	 $('input[id$=CMP_NO_NOME_MEIO]').keyup(function() {
		var txtMiddle = $(this).val();		
		txtClone = $('input[id$=CMP_NO_PRIMEIRO_NOME]').val() +" "+ $('input[id$=CMP_NO_NOME_MEIO]').val() +" "+ $('input[id$=CMP_NO_ULTIMO_NOME]').val();
		if ($('#title_main h2 .title') == '') {
			$('#title_main h2 .title').text("Novo Candidato");
		} else {
			$('#title_main h2 .title').text(txtClone);
		}
	});
	 
	  $('input[id$=CMP_NO_ULTIMO_NOME]').keyup(function() {
		txtClone = $('input[id$=CMP_NO_PRIMEIRO_NOME]').val() +" "+ $('input[id$=CMP_NO_NOME_MEIO]').val() +" "+ $('input[id$=CMP_NO_ULTIMO_NOME]').val();
		if ($('#title_main h2 .title') == '') {
			$('#title_main h2 .title').text("Novo Candidato");
		} else {
			$('#title_main h2 .title').text(txtClone);
		}
	});
});
