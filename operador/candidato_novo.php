<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/libVisaDetail.php");

/*
 function exception_error_handler($errno, $errstr, $errfile, $errline ) {
 throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
 }
 set_error_handler("exception_error_handler");
 */
try{
	$msg = cINTERFACE::ProcessePost($_POST, $_GET, $_FILES, $_SESSION);
}
catch(Exception $e){
	$msg = 'Não foi possível salvar o candidato pois:'.cINTERFACE::MsgJavascriptOk($e->getMessage());
}

$CAND = new cINTERFACE_CANDIDATO();


// Cabecalho padrão
echo Topo();
echo Menu();
?>
<div class="conteudo">
	<div class="titulo"><div style="float: left">Novo candidato </div></div>
	<div class="conteudoInterno">
		<div>
		<?
		print $CAND->Formcandidato_new($_POST, $_GET);
		?>
		</div>
	</div>
</div>
<br>		
<?php
if ($msg!=''){
?>	
<script language="javascript">jAlert("<?=html_entity_decode($msg, ENT_QUOTES);?>");</script>
<?
}
echo Rodape("");
?> 
<script language="javascript">
$(document).ready(function() {
	$('#CMP_NU_EMPRESA').change(function() {
	    atualizaEmbarcacaoProjeto();
	});
	atualizaEmbarcacaoProjeto();
})

function atualizaEmbarcacaoProjeto(){
	if ($('#CMP_NU_EMPRESA').val() >0){
	    var url ='/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA='+$('#CMP_NU_EMPRESA').val()+'&NU_EMBARCACAO_PROJETO='+$('#CMP_NU_EMBARCACAO_PROJETO').val();
		$.ajax({
			  url: url,
			  async:false,
			  success: function(data) {
				$('#tdEmbarcacaoProjeto').html('<select id="CMP_NU_EMBARCACAO_PROJETO" name="CMP_NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>'+data+'</select>');
			  }
			});				
	}
	else{
		$('#tdEmbarcacaoProjeto').html('(selecione a empresa...)');
	}
}

</script>
