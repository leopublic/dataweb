<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/libVisaDetail.php");

/*
 function exception_error_handler($errno, $errstr, $errfile, $errline ) {
 throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
 }
 set_error_handler("exception_error_handler");
 */

$msg = cINTERFACE::ProcessePost($_POST, $_GET, $_FILES, $_SESSION);

$index = 0;
$mensagem='';
if(isset($_GET['NU_CANDIDATO']))
{
	$pNU_CANDIDATO = $_GET['NU_CANDIDATO'];
	$cand = new cINTERFACE_CANDIDATO();

	$tela = new cEDICAO("vCANDIDATO_CONS");
	$tela->CarregarConfiguracao();
	
	if (cINTERFACE::CampoEnviado($_POST, $tela->mCampoCtrlEnvio)){
		// Obtem valores digitados
		cINTERFACE::RecupereValoresEdicao($tela, $_POST);
	
		// Salva 
		//$cand->SalvarEdicao($editaCandidato);
		if($tela->AtualizarBanco()){
			$cand->Recuperar($pNU_CANDIDATO);
			$ac = $cand->UltimaSolicitacao();
			$cand->CorrijaNomeCompleto();
			$cand->LogueUltimaAtualizacao();			
			$cand->VerifiqueCadastroCompleto();
			
			$msg = 'Identificação do candidato atualizada com sucesso';

			$tela->RecupereRegistro();
			$cand->Recuperar($pNU_CANDIDATO);
			
		}
		else{
			$cand->Recuperar($pNU_CANDIDATO);
			$ac = $cand->UltimaSolicitacao();
			$msg = 'Não foi possível atualizar o candidato.<br>'.str_replace("'", "\'", mysql_error());
		}
	}
	else{   
		$cand->Recuperar($pNU_CANDIDATO);
		$ac = $cand->UltimaSolicitacao();
		$tela->mCampoNome['NU_CANDIDATO']->mValor =$pNU_CANDIDATO ;
		$tela->RecupereRegistro();
	}
}
else{
	$msg = "Candidato não informado";
}

$voltar = $_SESSION['voltar'];
if ($voltar == ''){
	$voltar = "os_listar.php";
}
// Cabecalho padrão
echo Topo($cand->mNOME_COMPLETO, '<style>div#tabsOS div{padding-left:10px;padding-right:10px;padding-bottom:10px;}</style>' , false);

?>
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $("#tabsInt").tabs();
    });
    
</script>
<div style="background-color:#fff;background-image:none;height:auto;padding-top:10px;padding-bottom:10px;text-align:left;padding-left:10px;"><img src="/imagens/logo_mundivisas_red.jpg" style="margin:0;padding:0;"/>
	<div style="padding-top:50px;padding-right:10px;float:right;vertical-align: bottom;"><?=$_SESSION['myAdmin']['nm_nome'];?></div>
</div>
<div class="conteudo">
	<div class="titulo">
		<div style="float:right">
			<input type="button" value="Back to the list" onclick="javascript:window.location='candListar.php';"/>
			<input type="button" value="Logout" onclick="javascript:window.location='/sair.php';"/>

		</div>
		<div style="float:left">Detailed information of "<?=$cand->mNOME_COMPLETO;?>"</div>&nbsp;
	</div>
	<div class="conteudoInterno">
		<div id="tabsOS" class="aba">
			<ul>
				<li><a href="detalheCandidatoCliente_ID.php?NU_CANDIDATO=<?=$pNU_CANDIDATO;?>">Personal data</a></li>
				<li><a href="VISTO" class="ativo">Visa</a></li>
				<li><a href="detalheCandidatoCliente_ARQUIVOS.php?NU_CANDIDATO=<?=$pNU_CANDIDATO;?>">Files & Docs</a></li>
			</ul>
			<div id="VISTO">
			<?
				print $cand->FormVistoAtual();
			?>
			</div>
		</div>
	</div>
</div>

<?php
if ($msg!='')
{
?>	
<script language="javascript">jAlert('<?=$msg;?>');</script>
<?
}
echo Rodape("");
?> 
<script language="javascript">

$(document).ready(function() {
    var nu_candidato = '<?=$cand->mNU_CANDIDATO;?>';
	$("#accordionCand").accordion({ autoHeight: false, collapsible: true, active: 0});
	
   
    //ArquivosOS
   //$.post('/LIB/ARQUIVOS_OS_GRUPO.php?NU_CANDIDATO='+nu_candidato, function(d){$('#ArquivosOS').html(d);});

    
})

var idEmpresa = "<?=$idEmpresa?>";
var idEmbarcacaoProjeto = "<?=$idEmbarcacaoProjeto?>";

//document.getElementById('botao1').focus();



function CarregarListaDeArquivos(pNU_CANDIDATO, pNomeDiv){
	var url = '../LIB/ARQUIVOS_LISTA.php?NU_CANDIDATO='+pNU_CANDIDATO+'&NomeDiv='+pNomeDiv;
	$.ajax({
		  url: url,
		  async:false,
		  success: function(data) {
			$('#'+pNomeDiv+pNU_CANDIDATO).html(data);
		  }
		});			
}

function enviar(tipo) {
  var cand = document.wilson.idCandidato.value;
  var emp = document.wilson.idEmpresa.value;
  var sol = document.wilson.NU_SOLICITACAO.value;
  var cd_admin = document.wilson.cd_admin.value;
  var pagina = "operacao3.php?idCandidato="+cand+"&idEmpresa="+emp+"&opvisa="+tipo+"&NU_SOLICITACAO="+sol+"&cd_admin="+cd_admin+"&A=";


  //alert("pagina="+pagina);
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
  document.wilson.action="operacao3.php";
//  window.open(pagina);
  var msg = OpenChild(tipo,pagina);
  document.all.aguarde.innerHTML = msg;
}

function arquivos(tipo) {
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
//  var msg = OpenChild(tipo,"operacao3up.php");
  var msg = OpenChild(tipo,"/admin/documentos1.php");
  document.all.aguarde.innerHTML = msg;
}

function formulario(tipo) {
  document.wilson.action="/formularios/all_registroPF.php";
  document.wilson.target="formPF";
  document.wilson.submit();
}

</script>
