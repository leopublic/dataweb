<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

/*
function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler("exception_error_handler");
*/ 


 Acesso::SetBloqueio(Acesso::tp_Os_Aba_DadosPessoais);


$tabsOS_selected = '';

$mensagem='';
if (isset($_GET['ID_SOLICITA_VISTO'])){
	$pID_SOLICITA_VISTO = $_GET['ID_SOLICITA_VISTO'];
}else{
	$pID_SOLICITA_VISTO = $_POST['ID_SOLICITA_VISTO'];
}

$OrdemServico = new cORDEMSERVICO(); 
$OS_i = new cINTERFACE_OS();

if($pID_SOLICITA_VISTO >0 ){
	$OrdemServico->Recuperar($pID_SOLICITA_VISTO);
	$OrdemServico->RecuperarCandidatos($pID_SOLICITA_VISTO);
	$arrayIds = explode(",",$OrdemServico->mCandidatos);
	$OS_i->Recuperar($pID_SOLICITA_VISTO);
}
else{
	$OrdemServico->mID_STATUS_SOL = 0;
	$OrdemServico->mID_SOLICITA_VISTO = 0;
	$OS_i->mID_SOLICITA_VISTO = 0;
}



if(isset($_POST['naoConformidade'])){
	if($OrdemServico->mfl_nao_conformidade){
		$fl_nao_conformidade = 0;
	}
	else{
		$fl_nao_conformidade = 1;
	}
	$sql = "update solicita_visto set fl_nao_conformidade = ".$fl_nao_conformidade." where id_solicita_visto = ".$OrdemServico->mID_SOLICITA_VISTO;
	mysql_query($sql);
	$OrdemServico->mfl_nao_conformidade = $fl_nao_conformidade;	
}

if ($OrdemServico->mfl_nao_conformidade){
	$labelNC = "Suspender não conformidade";
	$estiloNC = "NC_ON";
}
else{
	$labelNC = "Indicar não conformidade";
	$estiloNC = "NC_OFF";
}


$js = '';

$voltar = $_SESSION['voltar'];
if ($voltar == ''){
	$voltar = "os_listar.php";
}
//###################################################################
// Inicio do documento
//###################################################################
echo Topo();
echo Menu("SOL");
?>
<div class="conteudo">
	<div class="titulo">
       <div style="float:right;margin-left:10px;margin-top:4px;">
        <?php

        if($pID_SOLICITA_VISTO <> 0){

            echo menuBarOS($usulogado, $OrdemServico->mID_SOLICITA_VISTO, $OrdemServico->mNU_SERVICO, $estiloNC, $labelNC, $OrdemServico->mReadonly);

        }

        ?>
        </div>
        <?=$acao;?>
		<div style="float:left">Ordem de serviço <?=$OrdemServico->mNU_SOLICITACAO;?>&nbsp;&nbsp;(<?=$OrdemServico->mNO_STATUS_SOL;?>)</div>
	</div>
<!--
	<div style="border-bottom:solid 1px #cccccc;padding-bottom:1px;padding-left:1px;color:#cccccc"><a href="../intranet/geral/<?=$voltar;?>" style="color:#999999;text-decoration:underline">voltar para listagem</a></div>
-->
	<div class="conteudoInterno">				
	<div id="tabsOS" class="aba">
<?php 
print $OS_i->aba("DadosPessoais", '/operador/os_DadosPessoais.php');

//###################################################################
// Dados pessoais
//###################################################################
if ($OrdemServico->mID_STATUS_SOL >= 1){?>	
	<div id="DadosPessoais">
   		(carregando...)
	</div>
<?php 
}?>

	</div>
</div>
<br />
<?php
echo Rodape("");

if ($tabOS_selected == ''){
	$tabOS_selected = 0;
}

if (isset($_SESSION['msg']) && $_SESSION['msg']!=''){
	$mensagem.="\njAlert('".$_SESSION['msg']."');";
	$_SESSION['msg']=''; 
}
?>
<script language="javascript">

var NU_EMPRESA_ANT;
$(document).ready(function() {
    //$( "#tabsOS" ).tabs("select", "#DadosPessoais" );
    <?=$js;?>
    
    <?=$mensagem;?>
	CarregarCandidatosX('<?=$OrdemServico->mID_SOLICITA_VISTO;?>', 'DadosPessoais', '<?=$OrdemServico->mCandidatos;?>','1' );
	    
  });

function atualizaEmbarcacaoProjeto(){
    var url ='/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA='+$('#NU_EMPRESA').val()+'&NU_EMBARCACAO_PROJETO='+$('#NU_EMBARCACAO_PROJETO').val();
	$.ajax({
		  url: url,
		  async:false,
		  success: function(data) {
			$('#tdEmbarcacaoProjeto').html('<select name="CMP_NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>'+data+'</select>');
		  }
		});				
}

function CarregarCandidatosX(pID_SOLICITA_VISTO, pNomeContainer, pChaves, pAlterar){
	if(pChaves!=''){
		var url = '/LIB/CANDIDATO_LISTA_SOL.php?id_solicita_visto='+pID_SOLICITA_VISTO+'&lista='+pChaves+'&alterar='+pAlterar;
		$.ajax({
			  url: url,
			  success: function(data) {
				  $('#'+pNomeContainer).html(data);
	           }
			});	
	}
	else{
		$('#'+pNomeContainer).html('(nenhum candidato adicionado)');
	}
}


function CarregarListaDeArquivos(pNU_CANDIDATO, pNomeDiv){
	var url = '../LIB/ARQUIVOS_LISTA.php?NU_CANDIDATO='+pNU_CANDIDATO+'&NomeDiv='+pNomeDiv;
	$.ajax({
		  url: url,
		  async:false,
		  success: function(data) {
			$('#'+pNomeDiv+pNU_CANDIDATO).html(data);
		  }
		});			
}

function CarregarArquivo(pCaminho, pNU_SEQUENCIAL, pNomeDiv){
  	var dimensoes = new Array();
	$.ajax({
		  url: '/LIB/imagemTam.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL,
		  async:false,
		  success: function(data) {
			dimensoes = data.split("#");
		
		  }
		});
	var pWidth = +dimensoes[0];
	var pHeight = +dimensoes[1];
	//alert('pWidth='+pWidth+';pHeight='+pHeight);
	var maxW = 400;
	var maxH = 300;
	var w = maxW;
	var h = maxH;
	if (pWidth>pHeight)
	{
		h = Math.round(pHeight/pWidth * maxW);
	}
	else
	{
		w = Math.round(pWidth/pHeight * maxH);
	}
	var d = new Date();
	var xImagem = '<img src="'+pCaminho+'?'+d.getTime()+'" ';
	xImagem = xImagem + 'title="Use ctrl+mouse para cima ou para baixo (com botão pressionado) para dar zoom"/>';
	xImagem = xImagem + '<br/><input type="button" onclick="abreJanelaxxx(\'/LIB/editaImagem.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL+'\',\'wteste\');" value="Editar"/>';
	xImagem = xImagem + '<input type="button" value="Atualizar" onclick="CarregarArquivo(\''+pCaminho+'\',\''+pNU_SEQUENCIAL+'\',\''+pNomeDiv+'\')"/>';
	$('#'+pNomeDiv).html(xImagem);	 
	$('#'+pNomeDiv+' img').imagetool({
	    viewportWidth: w
	   ,vIewportHeight: h
	  });
}		

function RecuperarFormularios(pid_solicita_visto, pNU_CANDIDATO){
	$('#formulariosCand'+pNU_CANDIDATO).html('(recuperando formulários disponíveis. Por favor, aguarde...)');
	var url = "/LIB/OS_FORMULARIOS.php?id_solicita_visto="+pid_solicita_visto+"&FL_OS=0&FL_CANDIDATO=1&NU_CANDIDATO="+pNU_CANDIDATO;
	$.ajax({
		url: url,
		success: function(data) {
			$('#formulariosCand'+pNU_CANDIDATO).html(data);
		}
	});
}

function RecuperarFormulariosOs(pid_solicita_visto){
	$('#formulariosOS').html('(recuperando formulários disponíveis. Por favor, aguarde...)');
	var url = "/LIB/OS_FORMULARIOS.php?id_solicita_visto="+pid_solicita_visto+"&FL_OS=1&FL_CANDIDATO=0"
	$.ajax({
		url: url,
		success: function(data) {
			$('#formulariosOS').html(data);
		}
	});
}

function novo( pcmbEmbarcacaoProjeto){
	var xNU_EMPRESA = '<?=$OrdemServico->mNU_EMPRESA;?>';
	var xNU_EMBARCACAO_PROJETO = $('#'+pcmbEmbarcacaoProjeto).val();

	if(xNU_EMPRESA=='0'){
		jAlert('Selecione a empresa antes de incluir candidatos.<br>(A empresa é necessária caso você precise incluir um novo candidato.)', 'Atenção');
	}else{
		var nome = "";
		var pagina = "/solicitacoes/novaReqVistoCandNovo.php?NU_EMPRESA="+xNU_EMPRESA+"&NU_EMBARCACAO_PROJETO="+xNU_EMBARCACAO_PROJETO;
		
   	    abreJanelaxxx(pagina,'wteste');
	   	//w.focus();
	}
}

function validaFormOS(){
	msg = '';
	if(document.sol.idEmpresa.selectedIndex<1) {
		msg +="<br/>- Empresa não indicada;";
	}
    if(document.sol.NU_EMBARCACAO_PROJETO[document.sol.NU_EMBARCACAO_PROJETO.selectedIndex].value=='') {
		msg+="<br/>- Projeto/embarcação não indicado;";
	}
    if($('#NU_SERVICO').val()=='0') {
		msg +="<br/>- Tipo de serviço não indicado;";
	}
    if(document.sol.NO_SOLICITADOR.value=='') {
		msg +="<br/>- Solicitante não informado;";
	}
    if(document.sol.DT_SOLICITACAO.value=='') {
    	msg+="<br/>- Data da solicitação não informada";
	}
	if(document.sol.cd_Tecnico[document.sol.cd_Tecnico.selectedIndex].value==''){
		msg+="<br/>- Responsável pelo complemento do cadastro não informado;";
	}
	if (msg!=''){
		jAlert('Corrija os seguintes itens:'+msg, 'Não foi possível salvar a OS');
	}else{
		jConfirm('Deseja mesmo alterar a OS?', 'Alterar OS', function(r){ if (r){document.sol.submit();}})
	}
}

var idEmpresa = "<?=$OrdemServico->mNU_EMPRESA?>";
var idEmbarcacaoProjeto = $('#NU_EMBARCACAO_PROJETO').val();

function enviar(tipo) {
  var cand = document.wilson.idCandidato.value;
  var emp = document.wilson.idEmpresa.value;
  var sol = document.wilson.NU_SOLICITACAO.value;
  var cd_admin = document.wilson.cd_admin.value;
  var pagina = "operacao3.php?idCandidato="+cand+"&idEmpresa="+emp+"&opvisa="+tipo+"&NU_SOLICITACAO="+sol+"&cd_admin="+cd_admin+"&A=";

  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
  document.wilson.action="operacao3.php";
  //window.open(pagina);
  var msg = OpenChild(tipo,pagina);
  document.all.aguarde.innerHTML = msg;
}

function arquivos(tipo) {
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
  //var msg = OpenChild(tipo,"operacao3up.php");
  var msg = OpenChild(tipo,"/admin/documentos1.php");
  document.all.aguarde.innerHTML = msg;
}

function formulario(tipo) {
  document.wilson.action="/formularios/all_registroPF.php";
  document.wilson.target="formPF";
  document.wilson.submit();
}

function visadetail(codigo,acao,visa) {
	  var cd_admin = "<?=$usulogado?>";
	  var pagina = "../intranet/geral/novoVisaDetail.php?idCandidato=" + codigo + "&acao="+acao+"&idEmpresa=" + idEmpresa + "&NU_SOLICITACAO=" + visa + "&cd_admin=" + cd_admin;
	  var ret = AbrePagina(acao,pagina,idEmpresa,codigo,idEmbarcacaoProjeto);
	}

function carregarArquivos(){
	var id = $('#CMP_id_solicita_visto').val();
	if (id!='0'){
		$.ajax({
		  url: "/LIB/ARQUIVOS_SOL_LISTA.php?ID_SOLICITA_VISTO="+$('#CMP_id_solicita_visto').val(),
		  success: function(data) {
			 $('#ARQUIVOS_container').html(data);
           }
		});
	}
	else{
		 $('#ARQUIVOS_container').html('');			
	}
}

function ajaxRemoverArquivoSol(pNU_SEQUENCIAL, pNome){
	jConfirm('Deseja mesmo excluir o arquivo "'+pNome+'"? Essa operacao é irreversível.', 'Exclusão de arquivo', function (r){
			if (r){
				var url = "/LIB/ARQUIVOS_DEL.php?NU_SEQUENCIAL="+pNU_SEQUENCIAL;
				$.ajax({
					  url: url,
					  success: function(data) {
						 carregarArquivos();
						 jAlert(data);
			         }
				});
			}
		});
}
</script>
