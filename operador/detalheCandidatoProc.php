<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/libVisaDetail.php");

/*
 function exception_error_handler($errno, $errstr, $errfile, $errline ) {
 throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
 }
 set_error_handler("exception_error_handler");
 */

$index = 0;
$mensagem='';
if(isset($_GET['NU_CANDIDATO']))
{
	$pNU_CANDIDATO = $_GET['NU_CANDIDATO'];
	$cand = new cCANDIDATO();
	$cand->Recuperar($pNU_CANDIDATO);
	
	$ac = $cand->UltimaSolicitacao();
	
	$tela = new cEDICAO("vCANDIDATO");
	$tela->CarregarConfiguracao();
	
	if (cINTERFACE::CampoEnviado($_POST, $tela->mCampoCtrlEnvio))
	{
		// Obtem valores digitados
		cINTERFACE::RecupereValoresEdicao($tela, $_POST);
	
		// Salva 
		//$cand->SalvarEdicao($editaCandidato);
		if($tela->AtualizarBanco())
		{
			$sql = "update CANDIDATO SET NOME_COMPLETO =  trim(concat(trim(NO_PRIMEIRO_NOME), concat(' ',trim(concat(trim(ifnull(NO_NOME_MEIO, ' ')), concat(' ',trim(NO_ULTIMO_NOME))))))) where NU_CANDIDATO = ".$tela->mCampoNome['NU_CANDIDATO']->mValor;
			error_log("SQL =".$sql );
			mysql_query($sql);
			$msg = 'Identificação do candidato atualizada com sucesso';
			$sql = "update CANDIDATO set CO_USU_ULT_ALTERACAO = ".$usulogado.", DT_ULT_ALTERACAO = now() where NU_CANDIDATO = ".$pNU_CANDIDATO;
			mysql_query($sql);
			//
			// Atualiza o status do candidato se o cadastro estiver completo
			if ($cand->mNO_EMAIL_CANDIDATO != ''
				&& $cand->mNO_PRIMEIRO_NOME != ''
				&& $cand->mNO_ULTIMO_NOME != ''
				&& $cand->mNO_MAE != ''
				&& $cand->mNO_PAI != ''
				&& $cand->mCO_PROFISSAO_CANDIDATO != ''
				&& $cand->mCO_PROFISSAO_CANDIDATO != '0'
				&& $cand->mCO_ESTADO_CIVIL != ''
				&& $cand->mCO_ESTADO_CIVIL != '0'
				&& $cand->mCO_SEXO != ''
				&& $cand->mDT_NASCIMENTO != ''
				&& $cand->mCO_NACIONALIDADE != ''
				&& $cand->mCO_NACIONALIDADE != '0'
				&& $cand->mCO_NIVEL_ESCOLARIDADE != ''
				&& $cand->mCO_NIVEL_ESCOLARIDADE != '0'
				&& $cand->mNO_ENDERECO_RESIDENCIA != ''
				&& $cand->mNU_PASSAPORTE != ''
				&& $cand->mDT_EMISSAO_PASSAPORTE != ''
				&& $cand->mDT_VALIDADE_PASSAPORTE != ''
				&& $cand->mCO_PAIS_EMISSOR_PASSAPORTE != ''
				&& $cand->mCO_PAIS_EMISSOR_PASSAPORTE != '0'
				){

				$sql = "update CANDIDATO set BO_CADASTRO_MINIMO_OK = 1 where NU_CANDIDATO = ".$pNU_CANDIDATO;
				mysql_query($sql);
			}
			else{
				$sql = "update CANDIDATO set BO_CADASTRO_MINIMO_OK = 0 where NU_CANDIDATO = ".$pNU_CANDIDATO;
				mysql_query($sql);
			}
			//
			// Verifica se tem solicitação aberta para esse candidato e atualiza o status
			$sql = " select s.id_solicita_visto , ifnull(count(COK.NU_CANDIDATO), 0) totalOK, ifnull(count(CNOK.NU_CANDIDATO), 0) totalNOK";
			$sql .= "  from solicita_visto s, AUTORIZACAO_CANDIDATO AC";
			$sql .= "  left join CANDIDATO COK on COK.NU_CANDIDATO       = AC.NU_CANDIDATO and COK.BO_CADASTRO_MINIMO_OK = 1";
			$sql .= "  left join CANDIDATO CNOK on CNOK.NU_CANDIDATO       = AC.NU_CANDIDATO and  CNOK.BO_CADASTRO_MINIMO_OK <> 1";
			$sql .= " where AC.id_solicita_visto = s.id_solicita_visto";
			$sql .= "   and s.ID_STATUS_SOL = 1";
			$sql .= "   and s.id_solicita_visto in (select id_solicita_visto from AUTORIZACAO_CANDIDATO where NU_CANDIDATO = ".$pNU_CANDIDATO.")";
			$sql .= " group by s.id_solicita_visto";
			$res=mysql_query($sql);
			while ($rs = mysql_fetch_array($res)){
				if ($rs['totalNOK']==0){
					//$sql = "update solicita_visto set ID_STATUS_SOL = 2 where id_solicita_visto = ".$rs['id_solicita_visto'];
					//mysql_query($sql);
				}
				else{
					//$sql = "update solicita_visto set ID_STATUS_SOL = 1 where id_solicita_visto = ".$rs['id_solicita_visto'];
					//mysql_query($sql);
				}
			}			
			
            
			$tela->RecupereRegistro();
			$cand->Recuperar($pNU_CANDIDATO);
			
		}
		else
		{
			$msg = 'Não foi possível atualizar o candidato.<br>'.str_replace("'", "\'", mysql_error());
		}
	}
	else
	{   
		$tela->mCampoNome['NU_CANDIDATO']->mValor =$pNU_CANDIDATO ;
		$tela->RecupereRegistro();
	}
}
else
{
	$msg = "Candidato não informado";
}

$voltar = $_SESSION['voltar'];
if ($voltar == ''){
	$voltar = "os_listar.php";
}
// Cabecalho padrão
echo Topo(' - '.$cand->mNOME_COMPLETO);
echo Menu("SOL");

$qtdH = $cand->QtdHomonimos();
if($qtdH>0){
	$alertaH = '&nbsp;&nbsp;&nbsp;&nbsp;<button onclick="javascript:window.open(\'processoMigrarCandidato.php?NU_CANDIDATO='.$cand->mNU_CANDIDATO.'\');"><span style="font-weight:bold;color:#000000;">ATENÇÃO: Candidato com processos no arquivo morto</span></button>';
}
?>
<script>
$(document).ready(function() {
    $("#tabsInt").tabs();
  });
</script>
<div class="conteudo">
	<div class="titulo"><div style="float: left">Candidato "<?=$cand->mNOME_COMPLETO;?>"&nbsp;(<?=$cand->mNU_CANDIDATO;?>)&nbsp;<?=$alertaH;?></div></div>
	<div style="border-bottom: solid 1px #cccccc; padding-bottom: 1px; padding-left: 1px; color: #cccccc">
		<a href="../intranet/geral/<?=$voltar;?>" style="color: #999999; text-decoration: underline">voltar para listagem</a>
	</div>
	<div class="conteudoInterno">
		<div id="accordionCand">
		
			<h3 class="interno">Histórico</h3>
			
			<div>
				<?=cINTERFACE_CANDIDATO::MontaHistoricoProc($cand->mNU_CANDIDATO);?>
			</div>		

<?php
if (isset($ac)){
?>
			<h3 class="interno">Visa Details</h3>
			<div>
<?php 
$tela = new cEDICAO("VISA_DETAILS");
$tela->CarregarConfiguracao();
$tela->mCampoNome['id_solicita_visto']->mValor = $ac->mid_solicita_visto; 
$tela->mCampoNome['NU_CANDIDATO']->mValor = $cand->mNU_CANDIDATO; 
$tela->RecupereRegistro();

if (isset($_POST[cINTERFACE::PREFIXO_CAMPOS.$tela->mCampoCtrlEnvio->mNome])){
	cINTERFACE::RecupereValoresEdicao($tela, $_POST);
	try{
		$tela->AtualizarBanco();
		$index = 3;
		$msg = "Visa details atualizados com sucesso";
	}
	catch (Exception $exc){
		error_log($exc->getMessage() . $exc->getTraceAsString());
		$msg = "Não foi possível atualizar.<br/>".addslashes(str_replace("\n", "<br/>", $exc->getMessage()));
	}	
}
?>			
				<form id="formVisaDetails" method="post">
					<?=cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio);?>
					<?=cINTERFACE::RenderizeCampo($tela->mCampoNome["id_solicita_visto"]);?>
					<?=cINTERFACE::RenderizeEdicao($tela);?>
					</table>
					<center><input type="submit" value="Salvar" /></center>
				</form>
			</div>
<?php 
}
?>


			<h3 class="interno">Identificação do Candidato</h3>
			<div>
			<?=cINTERFACE::formEdicao();?>
				<?=cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio);?>
				<?=cINTERFACE::RenderizeCampo($tela->mCampoNome['NU_CANDIDATO']);?>
				<table class="edicao dupla">
					<tr class="estilo">
						<th width="18%">&nbsp;</th>
						<th width="28%">&nbsp;</th>
						<th width="5%">&nbsp;</th>
						<th width="18%">&nbsp;</th>
						<th width="28%">&nbsp;</th>
					</tr>
<?
$tr = "<tr>";
$btr = "";
$par = false;
$painelAnt = '';
foreach($tela->mCampo as $campo)
{
	if ($painelAnt!=$campo->mPainel){
		if ($par&&$linha!=''){
			$linha .= '<td>&#160;</td>';
			$linha .='</tr>';
			print $linha."\n";
		}
		print '<tr class="subTitulo"><td colspan="5">'.$campo->mPainel.'</td></tr>';
		$painelAnt = $campo->mPainel;
		$par = false;
	}
	if (!$campo->mChave){
		if (!$par){
			$linha = '<tr>';
		}
		else{
			$linha .= '<td>&#160;</td>';
		}
		if ($campo->mNome == 'NU_EMBARCACAO_PROJETO'){
			$linha .= '<th>'.$campo->Label().'</th><td><div id="tdEmbarcacaoProjeto">'.cINTERFACE::RenderizeCampo($campo).'</div></td>';
		}
		else{	
			$linha .= cINTERFACE::RenderizeLinhaEdicao($campo);
		}
	
		if ($par){
			$linha .='</tr>';
			print $linha."\n";
		}
		
		$par = !$par;
	}
}

?>				
				
				</table>
				<center><input type="submit" value="Salvar"/></center>
				</form>
			</div>

			<h3 class="interno">Dependentes</h3>
			<div>
				<form method="post" action="/intranet/geral/dependentes.php">
					<input type="hidden" name="idCandidato" value="<?=$cand->mNU_CANDIDATO;?>"/>
					<input type="hidden" name="idEmpresa" value="<?=$cand->mNU_EMPRESA;?>"/>
					<input type="submit" value="Administrar dependentes"/>
				</form>
			</div>
		</div>
		<div id="accordionCand2" class="accordion">
			<h3 class="interno">Arquivos<?=$qtdArqs;?></h3>
			<div>
				<div id="containerVisualizacao"
					style="float: right; border: solid 1px #999999; padding: 3px; text-align: center;">
				</div>
				<form id="novoArquivo<?=$cand->mNU_CANDIDATO;?>" action="arquivoPost.php" enctype="multipart/form-data" target="_blank" method="post">
			<?=cINTERFACE::inputHidden("formArquivo", "1");?>
			<?=cINTERFACE::inputHidden("NU_CANDIDATO", $cand->mNU_CANDIDATO);?>
			<?=cINTERFACE::inputHidden("NU_EMPRESA", $ac->mNU_EMPRESA);?>
					<div id="arquivosListados<?=$cand->mNU_CANDIDATO;?>"></div>
				</form>
				<script>
					function CarregarArquivo(pCaminho,pNU_SEQUENCIAL)
					{
					  	var dimensoes = new Array();
						$.ajax({
							  url: '/LIB/imagemTam.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL,
							  async:false,
							  success: function(data) {
								dimensoes = data.split("#");
							
							  }
							});
						var pWidth = +dimensoes[0];
						var pHeight = +dimensoes[1];
						//alert('pWidth='+pWidth+';pHeight='+pHeight);
						var maxW = 400;
						var maxH = 300;
						var w = maxW;
						var h = maxH;
						if (pWidth>pHeight)
						{
							h = Math.round(pHeight/pWidth * maxW);
						}
						else
						{
							w = Math.round(pWidth/pHeight * maxH);
						}
						var d = new Date();						
						$('#containerVisualizacao').html('<img src="'+pCaminho+'?'+d.getTime()+'"/><br/><input type="button" onclick="abreJanelaxxx(\'/LIB/editaImagem.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL+'\',\'wteste\');" value="Editar"/><input type="button" value="Atualizar" onclick="CarregarArquivo(\''+pCaminho+'\',\''+pNU_SEQUENCIAL+'\', '+pWidth+', '+pHeight+')"/>');	 
						$("#containerVisualizacao img").imagetool({
						    viewportWidth: w
						   ,viewportHeight: h
						  });
					}
				</script>

                <!--  <div id="ArquivosOS"> Carregando Arquivos agrupados por OS, Aguarde ...</div>-->
			</div>
			<h3 class="interno">Alterações nos Dados do Candidato</h3>
			<div>
				<table class="edicao">
					<tr>
						<th>Data do Cadastro:</th>
						<td><?=$cand->mDT_CADASTRAMENTO;?></td>
						<td>&#160;</td>
						<th>Admin Cadastro:</th>
						<td><?=$ac->mNU_USUARIO_CAD;?></td>
					</tr>
					<tr>
						<th>&Uacute;ltima Altera&ccedil;&atilde;o:</th>
						<td><?=$cand->mDT_ULT_ALTERACAO;?></td>
						<td>&#160;</td>
						<th>Admin Altera&ccedil;&atilde;o:</th>
						<td><?=$nmUsuAlt?></td>
					</tr>
				</table>
			</div>
		
			<h3 class="interno">Solicitações anteriores</h3>
			<div><?php 
			// Montagem do histórico das solicitações anteriores
			$tabs = '';
			$sql = " select AC.NU_SOLICITACAO, AC.ID_SOLICITA_VISTO, ID_TIPO_ACOMPANHAMENTO ";
			$sql .= "  from AUTORIZACAO_CANDIDATO AC";
			$sql .= "       join solicita_visto sv on sv.id_solicita_visto = AC.id_solicita_visto";
			$sql .= "  left join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO";
			$sql .= " WHERE AC.NU_CANDIDATO  = ".$cand->mNU_CANDIDATO;
			$sql .= " order by sv.nu_solicitacao desc";
			$rs = mysql_query($sql);
			while($rw = mysql_fetch_array($rs)){
				$link = "(OS ".$rw['NU_SOLICITACAO'].')';
				if ($rw['ID_TIPO_ACOMPANHAMENTO']==1){
					$titulo = "Processo MTE  ".$link;
				}
				elseif ($rw['ID_TIPO_ACOMPANHAMENTO']==3){
					$titulo = "Prorrogação  ".$link;
				}
				elseif ($rw['ID_TIPO_ACOMPANHAMENTO']==4){
					$titulo = "Registro CIE  ".$link;
				}
				elseif ($rw['ID_TIPO_ACOMPANHAMENTO']==5){
					$titulo = "Emissão CIE  ".$link;
				}
				elseif ($rw['ID_TIPO_ACOMPANHAMENTO']==6){
					$titulo = "Cancelamento  ".$link;
				}
				else{
					$titulo = "Versão antiga  ".$link;
				}
				$tabs .= '<li><a href="/LIB/SOLICITACAO_VIEW.php?NU_CANDIDATO='.$cand->mNU_CANDIDATO.'&ID_SOLICITA_VISTO='.$rw['ID_SOLICITA_VISTO'].'"><span>'.$titulo.'</span></a></li>';
			}
			if($tabs!=''){
				$tabs = '<div id="tabsInt"><ul>'.$tabs.'</ul></div>';
			}
			else{
				$tabs = '(nenhuma solicitação encontrada)';
			}
			print $tabs;
			?>
			</div>
		</div>
	</div>
</div>
<br>		
<?php
if ($msg!='')
{
?>	
<script language="javascript">jAlert('<?=$msg;?>');</script>
<?
}
echo Rodape("");
?> 
<script language="javascript">

$(document).ready(function() {
    var nu_candidato = '<?=$cand->mNU_CANDIDATO;?>';
	$("#accordionCand").accordion({ autoHeight: false, collapsible: true, active: <?=$index;?>});
	CarregarListaDeArquivos('<?=$cand->mNU_CANDIDATO;?>','arquivosListados');

	$('#CMP_NU_EMPRESA').change(function() {
	    atualizaEmbarcacaoProjeto();
	    if($('#CMP_nu_empresa_requerente').val()=='' || $('#CMP_nu_empresa_requerente').val()==NU_EMPRESA_ANT){
	    	$('#CMP_nu_empresa_requerente').val($('#CMP_NU_EMPRESA').val());
	    }
	    NU_EMPRESA_ANT = $('#CMP_NU_EMPRESA').val();
	});
	atualizaEmbarcacaoProjeto();
    
    //ArquivosOS
   //$.post('/LIB/ARQUIVOS_OS_GRUPO.php?NU_CANDIDATO='+nu_candidato, function(d){$('#ArquivosOS').html(d);});

    
})

function atualizaEmbarcacaoProjeto(){
	if ($('#CMP_NU_EMPRESA').val() >0){
	    var url ='/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA='+$('#CMP_NU_EMPRESA').val()+'&NU_EMBARCACAO_PROJETO='+$('#CMP_NU_EMBARCACAO_PROJETO').val();
		$.ajax({
			  url: url,
			  async:false,
			  success: function(data) {
				$('#tdEmbarcacaoProjeto').html('<select id="CMP_NU_EMBARCACAO_PROJETO" name="CMP_NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>'+data+'</select>');
			  }
			});				
	}
	else{
		$('#tdEmbarcacaoProjeto').html('(selecione a empresa...)');
	}
}


function validaFormOS() {
	msg = '';
	if(document.sol.idEmpresa.selectedIndex<1) {
		msg +="<br/>- Empresa não indicada;";
	}
	if(document.sol.NU_EMBARCACAO_PROJETO[document.sol.NU_EMBARCACAO_PROJETO.selectedIndex].value=='') {
		msg+="<br/>- Projeto/embarcação não indicado;";
	}
	if($('#NU_SERVICO').val()=='0') {
		msg +="<br/>- Tipo de serviço não indicado;";
	}
	if(document.sol.NO_SOLICITADOR.value=='') {
		msg +="<br/>- Solicitante não informado;";
	}
	if(document.sol.DT_SOLICITACAO.value=='') {
    	msg+="<br/>- Data da solicitação não informada";
	}
	if(document.sol.cdTecnico[document.sol.cdTecnico.selectedIndex].value=='') {
		msg+="<br/>- Responsável pelo complemento do cadastro não informado;";
	}
	if (msg!='')
	{
		jAlert('Corrija os seguintes itens:'+msg, 'Não foi possível salvar a OS');
	}
	else
	{
		jConfirm('Deseja mesmo alterar a OS?', 'Alterar OS', function(r){ if (r){document.sol.submit();}})
	}
}


var idEmpresa = "<?=$idEmpresa?>";
var idEmbarcacaoProjeto = "<?=$idEmbarcacaoProjeto?>";

//document.getElementById('botao1').focus();



function CarregarListaDeArquivos(pNU_CANDIDATO, pNomeDiv){
	var url = '../LIB/ARQUIVOS_LISTA.php?NU_CANDIDATO='+pNU_CANDIDATO+'&NomeDiv='+pNomeDiv;
	$.ajax({
		  url: url,
		  async:false,
		  success: function(data) {
			$('#'+pNomeDiv+pNU_CANDIDATO).html(data);
		  }
		});			
}

function enviar(tipo) {
  var cand = document.wilson.idCandidato.value;
  var emp = document.wilson.idEmpresa.value;
  var sol = document.wilson.NU_SOLICITACAO.value;
  var cd_admin = document.wilson.cd_admin.value;
  var pagina = "operacao3.php?idCandidato="+cand+"&idEmpresa="+emp+"&opvisa="+tipo+"&NU_SOLICITACAO="+sol+"&cd_admin="+cd_admin+"&A=";


  //alert("pagina="+pagina);
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
  document.wilson.action="operacao3.php";
//  window.open(pagina);
  var msg = OpenChild(tipo,pagina);
  document.all.aguarde.innerHTML = msg;
}

function arquivos(tipo) {
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
//  var msg = OpenChild(tipo,"operacao3up.php");
  var msg = OpenChild(tipo,"/admin/documentos1.php");
  document.all.aguarde.innerHTML = msg;
}

function formulario(tipo) {
  document.wilson.action="/formularios/all_registroPF.php";
  document.wilson.target="formPF";
  document.wilson.submit();
}

function OpenChild(tipo,pagina) {
    var ret = '';
    var cand = '<?=$idCandidato?>';
    var emp = '<?=$idEmpresa?>';
    var proj = '<?=$numProjeto?>';
    var proj = '<?=$numProjeto?>';
    var sol = '<?=$NU_SOLICITACAO?>';
    var admin = '<?=$usulogado?>';
    var MyArgs = new Array(emp,cand,tipo,proj,sol,admin);
    var WinSettings = "center:yes;resizable:yes;dialogHeight:450px;dialogWidth=780px"
    var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
    if (MyArgsRet != null) {
        var msg = MyArgsRet[0].toString();
        var erro = MyArgsRet[1].toString();
        if(erro.length > 0) {
            document.all.aguarde.innerHTML = "<!--"+erro+"-->";
            document.all.aguardeerr.innerHTML = "<!--"+msg+"-->";
        }
    } else {
        document.all.aguardeerr.innerHTML = "<!-- retorno nulo, provavelmente cancelou a operação -->";
        msg = "Houve um erro ao executar o pedido, favor tentar mais tarde.";
    }
    return msg;
}

function visadetail(codigo,acao,visa) {
	  var cd_admin = "<?=$usulogado?>";
	  var pagina = "../intranet/geral/novoVisaDetail.php?idCandidato=" + codigo + "&acao="+acao+"&idEmpresa=" + idEmpresa + "&NU_SOLICITACAO=" + visa + "&cd_admin=" + cd_admin;
	  var ret = AbrePagina(acao,pagina,idEmpresa,codigo,idEmbarcacaoProjeto);
	}
</script>
