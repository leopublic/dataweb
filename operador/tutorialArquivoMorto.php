<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

//###################################################################
// Inicio do documento
//###################################################################
echo Topo();
echo Menu("SOL");
?>
<style>
.conteudoInterno img {border:solid 5px #000000;}
</style>
<div class="conteudo">
	<div class="titulo">
		Tutorial sobre arquivo morto de candidatos duplicados
	</div>
	<div class="conteudoInterno">
Em breve estaremos implementando a limpeza do cadastro de candidatos. Essa limpeza vai remover os candidatos com o mesmo nome do cadastro para um arquivo morto. Em conjunto com a diretoria da Mundivisas, foi adotado o critério de se manter o candidato com o cadastro mais antigo, e remover os mais novos. Caso algum processo tenha sido registrado em um dos candidatos removidos, será possível "transferir" o processo para o candidato original, coforme a seguir:
<br/><br/>
1) Para saber quais são os candidatos que possuiam duplicados, foi criada uma nova consulta chamada "Candidatos com duplicados":
<br/><img src="/imagens/tutoriais/tut1.png" style="width:600px"/>
<br/><br/>
2) Essa tela apresenta a lista dos candidatos que estão com duplicados. Clique no candidato para ver mais detalhes.
<br/><img src="/imagens/tutoriais/tut2.png" style="width:600px"/>
<br/><br/>
3) Aqui é a tela padrão de detalhes do candidato. Sempre que o candidato apresentar duplicados esse botão estará disponível. Nessa tela você vai poder verificar a situação do candidato mantido.
<br/><img src="/imagens/tutoriais/tut3.png" style="width:600px"/>
<br/><br/>
4) Clicando no botão "Atenção: Candidato com processos no arquivo morto" você poderá escolher quais processos (de quais candidatos excluídos) deseja migrar para o candidato mantido. Caso algum processo não seja aproveitado, você pode marcar a opção "Deletar processos não migrados e eliminar pendências para esse candidato". Essa opção exclui os processos não migrados e elimina a mensagem de pendências para o candidato mantido.  
<br/><img src="/imagens/tutoriais/tut4.png" style="width:600px"/>
<br/><br/>
5) Depois de clicar no botão "Migrar" e fechar a janela, você poderá ver o processo já no candidato mantido.
<br/><img src="/imagens/tutoriais/tut5.png" style="width:600px"/>

	</div>	
</div>
<?php
echo Rodape("");
?>
