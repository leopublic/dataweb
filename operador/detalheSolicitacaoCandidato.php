<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

/*
  function exception_error_handler($errno, $errstr, $errfile, $errline ) {
  throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
  }
  set_error_handler("exception_error_handler");
 */

$tabsOS_selected = '';

$mensagem = '';
if (isset($_GET['ID_SOLICITA_VISTO'])) {
    $pID_SOLICITA_VISTO = $_GET['ID_SOLICITA_VISTO'];
} else {
    $pID_SOLICITA_VISTO = $_POST['ID_SOLICITA_VISTO'];
}
if (isset($_GET['OS_REF'])) {
    $pOS_Ref = $_GET['OS_REF'];
} else {
    $pOS_Ref = '';
}

$OrdemServico = new cORDEMSERVICO();

if ($pID_SOLICITA_VISTO > 0) {
    $OrdemServico->Recuperar($pID_SOLICITA_VISTO);
    $OrdemServico->RecuperarCandidatos($pID_SOLICITA_VISTO);
    $arrayIds = explode(",", $OrdemServico->mCandidatos);
} else {
    $OrdemServico->mID_STATUS_SOL = 0;
    $OrdemServico->mID_SOLICITA_VISTO = 0;
}

if (isset($_POST['ctrlAvancarStatus'])) {
    try {
        $OrdemServico->AvancarStatus();
        $OrdemServico->Recuperar($pID_SOLICITA_VISTO);
        $OrdemServico->RecuperarCandidatos($pID_SOLICITA_VISTO);
        $arrayIds = explode(",", $OrdemServico->mCandidatos);
        $mensagem = "jAlert('Status da OS alterado com sucesso', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    } catch (cERRO_CONSISTENCIA $exc) {
        $mensagem = "jAlert('Não foi possível salvar a OS pois:" . $exc->mMsg . "', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    } catch (Exception $exc) {
        $mensagem = "jAlert('Não foi possível salvar a OS pois:" . addslashes(str_replace("\n", "<br/>", $exc->getMessage() . '<br/>' . $exc->getTraceAsString())) . "', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    }
}

//
// Grava alterações na solicitação 
if ($OrdemServico->mReadonly) {
    $nomeTela = 'SOLICITA_VISTO_view';
} else {
    $nomeTela = 'SOLICITA_VISTO_edit';
}
$tela = new cEDICAO($nomeTela);
$tela->CarregarConfiguracao();
$tela->mCampoNome['id_solicita_visto']->mValor = $pID_SOLICITA_VISTO;

if ($OrdemServico->mID_SOLICITA_VISTO == 0) {
    $tela->mCampoNome['NU_EMPRESA']->mVisivel = 1;
    $tela->mCampoNome['NU_EMPRESA']->mReadonly = 0;
    $tela->mCampoNome['NO_RAZAO_SOCIAL']->mVisivel = 0;
} else {
    $tela->RecupereRegistro();
}

// Tratamento do RádioButton
if ($pOS_Ref != '') {
    $OrdemRef = new cORDEMSERVICO();
    $OrdemRef->Recuperar($_GET['OS_REF']);
    $tela->mCampoNome['id_solicita_visto']->mValor = $pOS_Ref;
    $tela->mCampoNome['mNU_SERVICO']->mValor = $OrdemRef->mNU_SERVICO;
    $tela->mCampoNome['mno_solicitador']->mValor = $OrdemRef->mno_solicitador;
    $tela->mCampoNome['mdt_solicitacao']->mValor = $OrdemRef->mdt_solicitacao;
    $tela->mCampoNome['mNU_EMBARCACAO_PROJETO']->mValor = $OrdemRef->mNU_EMBARCACAO_PROJETO;
    $tela->mCampoNome['mcd_tecnico']->mValor = $OrdemRef->mcd_tecnico;
    $tela->mCampoNome['mde_observacao']->mValor = $OrdemRef->mde_observacao;
    $tela->mCampoNome['mNU_USUARIO_CAD']->mValor = $OrdemRef->mNU_USUARIO_CAD;
    $tela->mCampoNome['mDT_PRAZO_ESTADA_SOLICITADO']->mValor = $OrdemRef->mDT_PRAZO_ESTADA_SOLICITADO;
    $tela->mCampoNome['mnu_empresa_requerente']->mValor = $OrdemRef->mnu_empresa_requerente;
    $tela->RecupereRegistro();
    $tela->mCampoNome['id_solicita_visto']->mValor = 0;
}

//
// Manda gravar se recebeu o post
if (!$OrdemServico->mReadonly) {
    if (isset($_POST[cINTERFACE::PREFIXO_CAMPOS . $tela->mCampoCtrlEnvio->mNome])) {
        $tabOS_selected = 0;
        cINTERFACE::RecupereValoresEdicao($tela, $_POST);
        if ($OrdemServico->mID_SOLICITA_VISTO == 0) {
            $OrdemServico->mNU_EMPRESA = $tela->mCampoNome['NU_EMPRESA']->mValor;
        }
        $OrdemServico->mNU_SERVICO = $tela->mCampoNome['NU_SERVICO']->mValor;
        $OrdemServico->mno_solicitador = $tela->mCampoNome['no_solicitador']->mValor;
        $OrdemServico->mdt_solicitacao = $tela->mCampoNome['dt_solicitacao']->mValor;
        $OrdemServico->mNU_EMBARCACAO_PROJETO = $tela->mCampoNome['NU_EMBARCACAO_PROJETO']->mValor;
        $OrdemServico->mcd_tecnico = $tela->mCampoNome['cd_tecnico']->mValor;
        $OrdemServico->mde_observacao = $tela->mCampoNome['de_observacao']->mValor;
        $OrdemServico->mNU_USUARIO_CAD = $usulogado;
        $OrdemServico->mDT_PRAZO_ESTADA_SOLICITADO = $tela->mCampoNome['DT_PRAZO_ESTADA_SOLICITADO']->mValor;
        $OrdemServico->mnu_empresa_requerente = $tela->mCampoNome['nu_empresa_requerente']->mValor;
        $OrdemServico->mCandidatos = $_POST['DadosOS_CANDIDATO_chaves'];

        try {
            $OrdemServico->SalvarInstancia();
            $qtdArquivos = $_POST['qtdArquivos'];
            $i = 1;
            while ($i <= $qtdArquivos) {
                if (isset($_FILES['presoFile' . $i]['name']) && $_FILES['presoFile' . $i]['name'] != '') {
                    $x = cARQUIVO::CriarArquivoSolicitacao($_FILES['presoFile' . $i], $OrdemServico->mNU_EMPRESA, $OrdemServico->mID_SOLICITA_VISTO, 18, $usulogado);
                }
                $i++;
            }
            if ($pID_SOLICITA_VISTO == 0) {
                $_SESSION['msg'] = "OS criada com sucesso.";
                session_write_close();
                if ($_POST['continuacao'] == '1') { //Continuar
                    header("Location:detalheSolicitacaoCandidato.php?ID_SOLICITA_VISTO=" . $OrdemServico->mID_SOLICITA_VISTO);
                } elseif ($_POST['continuacao'] == '2') { //Nova
                    header("Location:detalheSolicitacaoCandidato.php?ID_SOLICITA_VISTO=0&OS_REF=" . $OrdemServico->mID_SOLICITA_VISTO);
                } else { //nenhum
                    header("Location:detalheSolicitacaoCandidato.php?ID_SOLICITA_VISTO=" . $OrdemServico->mID_SOLICITA_VISTO);
                }
            } else {
                $mensagem = "jAlert('OS atualizada com sucesso', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
            }
        } catch (cERRO_CONSISTENCIA $exc) {
            $mensagem = "jAlert('Não foi possível salvar a OS pois:" . $exc->mMsg . "', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
        } catch (Exception $exc) {
            $mensagem = "jAlert('Não foi possível salvar a OS pois:" . addslashes(str_replace("\n", "<br/>", $exc->getMessage() . '<br/>' . $exc->getTraceAsString())) . "', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
        }
    }
}

//
// PAINEL CADASTRO
//
// Trata clique no botão "Concluir"  
if (isset($_POST['formCadastro'])) {
    $tabOS_selected = 2;
    if ($_POST['acao'] == 'concluir') {
        $status = '3';
        $xmsg = "OS concluída com sucesso";
    } else {
        $status = '4';
        $xmsg = "OS liberada com sucesso";
    }
    $res = mysql_query("SELECT * FROM ARQUIVOS A WHERE ID_SOLICITA_VISTO = " . $OrdemServico->mID_SOLICITA_VISTO);
    $arqs = mysql_num_rows($res);
    if ($arqs > 0) {
        $sql = "update solicita_visto set ID_STATUS_SOL = " . $status . " where id_solicita_visto = " . $OrdemServico->mID_SOLICITA_VISTO;
        mysql_query($sql);
        $OrdemServico->mID_STATUS_SOL = $status;
    } else {
        $xmsg = "Você precisa adicionar um aquivo à solicitação";
    }
    $mensagem = "jAlert('" . $xmsg . "', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
}

//
// Verifica recebimento de dados
if (isset($_POST['formCobrarProcesso'])) {
    try {
        $id_solicita_visto = $_POST['id_solicita_visto'];

        $tabOS_selected = $_POST['tab_selected'];
        if (isset($_POST['de_observacao_cobrar'])) {
            $obs = ", de_observacao = " . cBANCO::StringOk($_POST['de_observacao_cobrar']);
        } else {
            $obs = '';
        }
        $sql = "update solicita_visto set fl_cobrado = 1, de_observacao = " . $obs . " where id_solicita_visto = " . $id_solicita_visto;
        mysql_query($sql);
        $OrdemServico->mfl_cobrado = 1;
        $mensagem = "jAlert('Situação de cobrança atualizada com sucesso.', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    } catch (Exception $exc) {
        error_log($exc->getMessage() . $exc->getTraceAsString());
        $mensagem = "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    }
}


if (isset($_POST['naoConformidade'])) {
    if ($OrdemServico->mfl_nao_conformidade) {
        $fl_nao_conformidade = 0;
    } else {
        $fl_nao_conformidade = 1;
    }
    $sql = "update solicita_visto set fl_nao_conformidade = " . $fl_nao_conformidade . " where id_solicita_visto = " . $OrdemServico->mID_SOLICITA_VISTO;
    mysql_query($sql);
    $OrdemServico->mfl_nao_conformidade = $fl_nao_conformidade;
}

if ($OrdemServico->mfl_nao_conformidade) {
    $labelNC = "Suspender não conformidade";
    $estiloNC = "NC_ON";
} else {
    $labelNC = "Indicar não conformidade";
    $estiloNC = "NC_OFF";
}
//
// Conclui 
if (isset($_POST['formConcluirAndamento'])) {
    cINTERFACE::RecupereValoresEdicao($mte, $_POST);
    try {
        $id_solicita_visto = $_POST['id_solicita_visto'];
        $tabOS_selected = $_POST['tab_selected'];
        if (isset($_POST['de_observacao_cobrar'])) {
            $obs = ", de_observacao = " . cBANCO::StringOk($_POST['de_observacao_cobrar']);
        } else {
            $obs = '';
        }
        $sql = "update solicita_visto set ID_STATUS_SOL = 6 " . $obs . "where id_solicita_visto = " . $id_solicita_visto;
        mysql_query($sql);
        $OrdemServico->mID_STATUS_SOL = 6;
        $OrdemServico->mReadonly = true;
        $mensagem = "jAlert('Andamento concluído com sucesso.', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    } catch (Exception $exc) {
        error_log($exc->getMessage() . $exc->getTraceAsString());
        $mensagem = "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    }
}


$js = '';
$tabs = '';
$tabsF = '';
$divs = '';
$divsF = '';
//print '<br/>Candidatos:'.$OrdemServico->mCandidatos;
$ids = explode(",", $OrdemServico->mCandidatos);
$acCand = new cEDICAO("AUTORIZACAO_CANDIDATO_OS");
$acCand->CarregarConfiguracao();
$tabsIntx_selected = 0;
// Verifica se o form foi enviado para salvar
if (isset($_POST[cINTERFACE::PREFIXO_CAMPOS . $acCand->mCampoCtrlEnvio->mNome])) {
    $tabOS_selected = 2;
    $tabsIntx_selected = $_POST['tabsIntx_selected'];
    cINTERFACE::RecupereValoresEdicao($acCand, $_POST);
    try {
        $acCand->AtualizarBanco();
        if (trim($acCand->mCampoNome['TE_DESCRICAO_ATIVIDADES']->mValor) == '') {
            $sql = "update AUTORIZACAO_CANDIDATO SET TE_DESCRICAO_ATIVIDADES = (select TX_DESCRICAO_ATIVIDADES from FUNCAO_CARGO where CO_FUNCAO = AUTORIZACAO_CANDIDATO.CO_FUNCAO_CANDIDATO) ";
            $sql .= " where id_solicita_visto = " . $acCand->mCampoNome['mid_solicita_visto']->mValor;
            $sql .= " and NU_CANDIDATO = " . $acCand->mCampoNome['NU_CANDIDATO']->mValor;
            print "SQL=" . $sql;
            mysql_query($sql);
        }
        $mensagem = "jAlert('Cadastro atualizado com sucesso', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    } catch (Exception $exc) {
        error_log($exc->getMessage() . $exc->getTraceAsString());
        $mensagem = "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    }
}


for ($i = 0; $i < count($ids); $i++) {
    if ($selecionada == '') {
        $selecionada = $ids[$i];
    }
    error_log('Recuperando candidato:' . $ids[$i]);
    $acCand->mCampoNome['id_solicita_visto']->mValor = $OrdemServico->mID_SOLICITA_VISTO;
    $acCand->mCampoNome['NU_CANDIDATO']->mValor = $ids[$i];
    $acCand->RecupereRegistro();
    error_log('Recuperou candidato:' . $ids[$i]);
    error_log('NU_CANDIDATO:' . $acCand->mCampoNome["NU_CANDIDATO"]->mValor);

    $tabs .= '<li><a href="#cand' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . $acCand->mCampoNome['NOME_COMPLETO']->mValor . '</a></li>';
    $divs .= '<div id="cand' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
    $divs .= '<form id="form' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" method="post">' . "\n";
    //$divs .= '<input type="hidden" id="form" name="IdentificadorCampos" value="'.$acCand->mCampoNome['NU_CANDIDATO']->mValor.'"/>';
    $divs .= cINTERFACE::RenderizeCampo($acCand->mCampoCtrlEnvio);
    $divs .= cINTERFACE::RenderizeCampo($acCand->mCampoNome["id_solicita_visto"]);
    $divs .= cINTERFACE::RenderizeCampo($acCand->mCampoNome["NU_CANDIDATO"]);
    $divs .= cINTERFACE::inputHidden('tabsIntx_selected', $i);

    $divs .= cINTERFACE::RenderizeEdicao($acCand);
    $divs .= '</table>' . "\n";
    if (!$OrdemServico->mReadonly) {
        $divs .= '<center><input type="submit" value="Salvar" style="width:auto"/></center>' . "\n";
    }
    $divs .= '</form>' . "\n";

    $divs .= '<div style="clear:both;overflow: auto;">' . "\n";
    $divs .= '<div class="duplasubTitulo">Arquivos do candidato</div>' . "\n";
    $divs .= '<div id="arquivosListadosVisualizacao' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" style="float:right;border:solid 1px #999999;padding:3px;text-align:center;">' . "\n";
    $divs .= '</div>' . "\n";
    $divs .= '<div>' . "\n";
    $divs .= '	<form id="novoArquivo' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" action="arquivoPost.php" enctype="multipart/form-data" target="_blank" method="post">' . "\n";
    $divs .= cINTERFACE::inputHidden("formArquivo", "1");
    $divs .= cINTERFACE::inputHidden("NU_CANDIDATO", $acCand->mCampoNome['NU_CANDIDATO']->mValor);
    $divs .= cINTERFACE::inputHidden("NU_EMPRESA", $acCand->mCampoNome['NU_EMPRESA']->mValor);
    $divs .= '	<div id="arquivosListados' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
    $divs .= '	</div>' . "\n";
    $divs .= '	</form>' . "\n";
    $divs .= '</div>' . "\n";
    $divs .= '</div><br/>' . "\n";

    $divsArqsProcesso = '';
    $divsArqsProcesso .= '<div style="clear:both;overflow: auto;">' . "\n";
    $divsArqsProcesso.= '<div class="duplasubTitulo">Arquivos do candidato</div>' . "\n";
    $divsArqsProcesso .= '<div id="arquivosListadosProcessoVisualizacao' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" style="float:right;border:solid 1px #999999;padding:3px;text-align:center;">' . "\n";
    $divsArqsProcesso.= '</div>' . "\n";
    $divsArqsProcesso.= '<div>' . "\n";
    $divsArqsProcesso.= '	<form id="novoArquivoProcesso' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" action="arquivoPost.php" enctype="multipart/form-data" target="_blank" method="post">' . "\n";
    $divsArqsProcesso.= cINTERFACE::inputHidden("formArquivo", "1");
    $divsArqsProcesso.= cINTERFACE::inputHidden("NU_CANDIDATO", $acCand->mCampoNome['NU_CANDIDATO']->mValor);
    $divsArqsProcesso.= cINTERFACE::inputHidden("NU_EMPRESA", $acCand->mCampoNome['NU_EMPRESA']->mValor);
    $divsArqsProcesso.= '	<div id="arquivosListadosProcesso' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
    $divsArqsProcesso.= '	</div>' . "\n";
    $divsArqsProcesso.= '	</form>' . "\n";
    $divsArqsProcesso.= '</div>' . "\n";
    $divsArqsProcesso.= '</div><br/>' . "\n";

    $divs .= '</div>';
    $js .= "	CarregarListaDeArquivos('" . $acCand->mCampoNome['NU_CANDIDATO']->mValor . "','arquivosListados');\n";

    $tabsF .= '<li><a href="#formularios' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . $acCand->mCampoNome['NOME_COMPLETO']->mValor . '</a></li>';
    $divsF .= '<div id="formularios' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
    $divsF .= '<div id="formulariosCand' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '"></div>' . "\n";
    if (!$OrdemServico->mReadonly) {
        $divsF .= '<center><input type="button" value="Refresh" onclick="RecuperarFormularios(\'' . $acCand->mCampoNome['id_solicita_visto']->mValor . '\',\'' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '\');"/></center>' . "\n";
    }
    $divsF .= '</div>' . "\n";
    $js .= '	RecuperarFormularios("' . $acCand->mCampoNome['id_solicita_visto']->mValor . '","' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '");' . "\n";
    $js .= "	RecuperarFormulariosOs('" . $OrdemServico->mID_SOLICITA_VISTO . "');\n";
}

$tabs = '<ul>' . $tabs . '</ul>' . "\n";
$tabsF = '<ul>' . $tabsF . '</ul>' . "\n";



//###################################################################
// Inicio do documento
//###################################################################
echo Topo();
echo Menu("SOL");
?>
<div class="conteudo">
    <div class="titulo">
        <div class="nc">
            <form method="post">
                <input type="hidden" id="naoConformidade" name="naoConformidade" value="1"/>
                <input type="submit" value="NC" title="<?= $labelNC; ?>" class="<?= $estiloNC; ?>""/>
            </form>
        </div>
        <div style="float:right;margin-left:10px;margin-top:4px;"><a href="../LIB/OS_rel.php?ID_SOLICITA_VISTO=<?= $OrdemServico->mID_SOLICITA_VISTO; ?>" target="_blank" title="Clique para imprimir a OS"><img src="../imagens/icons/printer.png"/></a></div>
<?= $acao; ?>
        <div style="float:left">Ordem de serviço <?= $OrdemServico->mNU_SOLICITACAO; ?>&nbsp;&nbsp;(<?= $OrdemServico->mNO_STATUS_SOL; ?>)</div>
    </div>
    <div style="border-bottom:solid 1px #cccccc;padding-bottom:1px;padding-left:1px;color:#cccccc"><a href="../intranet/geral/os_listar.php" style="color:#999999;text-decoration:underline">voltar para listagem</a></div>
    <div class="conteudoInterno">				
        <div id="tabsOS" class="tabs">
            <ul>
                <li><a href="#DadosOS">Dados OS</a></li>
<?php if ($OrdemServico->mID_STATUS_SOL >= 1) { ?>
                    <li><a href="#DadosPessoais">Dados pessoais</a></li>
<?php } ?>
<?php if ($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 1 && $OrdemServico->mID_STATUS_SOL >= 2) { ?>
                    <li><a href="#Cadastro">Cadastro</a></li>
        <?php } ?>
<?php if ($OrdemServico->mID_SOLICITA_VISTO != 0 && $OrdemServico->mNU_FORMULARIOS > 0 && (($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 1 && $OrdemServico->mID_STATUS_SOL >= 4) || $OrdemServico->mID_TIPO_ACOMPANHAMENTO != 1)) { ?>
                    <li><a href="#Formularios">Formul&aacute;rios</a></li>
<?php } ?>
<?php if ($OrdemServico->mID_SOLICITA_VISTO != 0 && (($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 1 && $OrdemServico->mID_STATUS_SOL >= 4 ) || $OrdemServico->mID_TIPO_ACOMPANHAMENTO != 1)) { ?>
                    <li><a href="#Processo">Processo</a></li>
<?php } ?>
            </ul>

                <?php
//###################################################################
// Dados da solicitacao
//###################################################################
                ?>
            <div id="DadosOS">
                <form id="dadosOS" method="post"  enctype="multipart/form-data">
                <?= cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio); ?>
                <?= cINTERFACE::RenderizeCampo($tela->mCampoNome["id_solicita_visto"]); ?>
                <?= cINTERFACE::inputHidden('NU_EMPRESA', $tela->mCampoNome['NU_EMPRESA']->mValor); ?>
                <?
                $ret = '';
                $ret .= '<table class="edicao dupla">';
                $ret .= '<col width="18%"></col>';
                $ret .= '<col width="28%"></col>';
                $ret .= '<col width="5%"></col>';
                $ret .= '<col width="18%"></col>';
                $ret .= '<col width="28%"></col>';
                foreach ($tela->mCampo as $campo) {
                    if (!$campo->mChave && $campo->mVisivel) {
                        if (!$par) {
                            $linha = '<tr>';
                        } else {
                            $linha .= '<td>&#160;</td>';
                        }
                        if ($campo->mNome == 'NU_EMBARCACAO_PROJETO') {
                            $linha .= '<th>' . $campo->Label() . '</th><td>' . cINTERFACE::inputHidden('NU_EMBARCACAO_PROJETO', $campo->mValor) . '<div id="tdEmbarcacaoProjeto">(escolha a empresa...)</div></td>';
                        } else {
                            $linha .= cINTERFACE::RenderizeLinhaEdicao($campo, false);
                        }
                        if ($par) {
                            $linha .='</tr>';
                            $ret.= $linha . "\n";
                        }
                        $par = !$par;
                    }
                }
                if ($par) {
                    $ret.= $linha . '<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>' . "\n";
                }
                print $ret;
                ?>
                    <?php if ($pID_SOLICITA_VISTO > 0) { ?>
                        <tr><th>Arquivo(s):</th>
                            <td colspan="4">
                                <div class="container" id="ARQUIVOS_container">(carregando...)</div>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <th>Candidatos<?= cINTERFACE::inputHidden('DadosOS_CANDIDATO_chaves', $OrdemServico->mCandidatos); ?></th>
                        <td colspan="4">
                            <div class="container" id="DadosOS_CANDIDATO_container" style="margin:0;padding:0;">(carregando...)</div>
                            <script>
                                CarregarCandidatos('<?= $pID_SOLICITA_VISTO; ?>', 'DadosOS_CANDIDATO_container', 'DadosOS_CANDIDATO_chaves', '0');
                            </script>
                        </td>
                    </tr>
                    </table>
                    <?php if (!$OrdemServico->mReadonly) { ?>
                        <center>
                            <input type="button" onclick="javascript:novo('idEmpresa', 'NU_EMBARCACAO_PROJETO');" style="width:auto" value="Adicionar candidatos" />
                            <input type="submit" value="Salvar" style="width:auto"/>
                        </center>
    <?php
}
if ($pID_SOLICITA_VISTO == 0) {
    ?>
                        <input type="radio" name="continuacao" value="1" checked="true" /> Continuar na OS &nbsp;&nbsp;&nbsp; <input type="radio" name="continuacao" value="2" /> Criar nova OS
    <?
}
?>
                </form>
            </div>	

<?php
//###################################################################
// Dados pessoais
//###################################################################
if ($OrdemServico->mID_STATUS_SOL >= 1) {
    ?>	
                <div id="DadosPessoais">
                    (carregando...)
                </div>
                <script>
                    CarregarCandidatos('<?= $pID_SOLICITA_VISTO; ?>', 'DadosPessoais', 'DadosOS_CANDIDATO_chaves', '1');
                </script>	
                        <?php }
                    ?>

                    <?php
//###################################################################
// Cadastro
//###################################################################
                    if ($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 1 && $OrdemServico->mID_STATUS_SOL >= 2) {
                        print '<div id="Cadastro">' . "\n";
                        print '<div id="tabsIntx">' . "\n";
                        print $tabs . "\n";
                        print $divs . "\n";
                        print '</div>' . "\n";
                        print '<form method="post">' . "\n";
                        print cINTERFACE::inputHidden('formCadastro', '1');
                        print cINTERFACE::inputHidden('nuCand', $acCand->mCampoNome['NU_CANDIDATO']->mValor);
                        print cINTERFACE::inputHidden('id_solicita_visto', $OrdemServico->mID_SOLICITA_VISTO);
                        print '<center>' . "\n";
                        if ($OrdemServico->mID_STATUS_SOL == 2) {
                            print cINTERFACE::inputHidden('acao', 'concluir');
                            print '<input type="submit" value="Concluir"/>' . "\n";
                        }
                        if ($OrdemServico->mID_STATUS_SOL == 3) {
                            print cINTERFACE::inputHidden('acao', 'liberar');
                            print '<input type="submit" value="Liberar"/>' . "\n";
                        }
                        print '</center>' . "\n";
                        print '</form>' . "\n";
                        ?>
                <div id="accordion" class="accordion">
                    <h3><a href="#">Dados dos Candidatos</a></h3>
                    <div>
                        <form name="candidato" id="candidato" method="post" action="detalheSolicitacaoCandidato.php" enctype="multipart/form-data">
                <?= cINTERFACE::inputHidden('MAX_FILE_SIZE', '8000000'); ?>
                <?= cINTERFACE::inputHidden('gravarCandidato', '1'); ?>
                <?= cINTERFACE::inputHidden('ID_SOLICITA_VISTO', $pID_SOLICITA_VISTO); ?>
                            Escolha o candidato:<select id="NU_CANDIDATO_SOL" name="NU_CANDIDATO_SOL" onChange="RecuperarCandidato();"><option value="0">(selecione o candidato para ver os dados)</option><?= montaComboCandidatosSolicitacao($pNU_CANDIDATO, $pID_SOLICITA_VISTO, ''); ?></select>&nbsp;<input id="btnCandEditar" type="button" value="Editar esse candidato"/>
                            <script>
                                $('#btnCandEditar').hide();
                                function RecuperarCandidato() {
                                    if ($('#NU_CANDIDATO_SOL').val() == '0') {
                                        $('#containerCandidato').html('--');
                                    } else {
                                        var abaAberta = $("#accordionCand").accordion("option", "active");
                                        $('#btnCandEditar').click(function () {
                                            window.location = 'detalheCandidatoAuto.php?NU_CANDIDATO=' + $('#NU_CANDIDATO_SOL').val();
                                        });
                                        $('#btnCandEditar').show();

                                        $('#containerCandidato').html('(recuperando informações de ' + $('#NU_CANDIDATO_SOL  option:selected').text() + '. Por favor, aguarde...)');
                                        $.ajax({
                                            url: "/LIB/CANDIDATO_VIEW_ACC.php?ID_SOLICITA_VISTO=<?= $pID_SOLICITA_VISTO; ?>&NU_CANDIDATO=" + $('#NU_CANDIDATO_SOL').val(),
                                            success: function (data) {
                                                $('#containerCandidato').html(data);
                                                $('#containerCandidato').slideDown("slow");
                                                $(".data").datepicker();
                                                $(".data").datepicker("enable")
                                                $(".accordion").accordion({autoHeight: false, collapsible: true});
                                                $("#accordionCand").accordion("option", "active", abaAberta);
                                                $("#tabsInt").tabs();
                                            }
                                        });
                                    }
                                }
                            </script>
                            <div id="containerCandidato">
                            </div>		    
                        </form>
                    </div>
                </div>
            </div>
    <?php
}
//########################################################################
// Página FORMULARIOS
//########################################################################
if ($OrdemServico->mID_SOLICITA_VISTO != 0 && $OrdemServico->mNU_FORMULARIOS > 0 && (($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 1 && $OrdemServico->mID_STATUS_SOL >= 4) || $OrdemServico->mID_TIPO_ACOMPANHAMENTO != 1)) {
    if ($OrdemServico->mNU_FORMULARIOS_CAND > 0) {
        print '<div id="Formularios">' . "\n";
        print '<div id="tabsForms">' . "\n";
        print $tabsF;
        print $divsF;
        print '</div>' . "\n";
        print '<br/>' . "\n";
    }
    if ($OrdemServico->mNU_FORMULARIOS_OS > 0) {
        print '<div id="formulariosOS"></div>' . "\n";
    }
    print '</div>' . "\n";
}

//########################################################################
// Página PROCESSO
//########################################################################
if ($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 1) {
    if ($OrdemServico->mID_STATUS_SOL >= 4 && $pID_SOLICITA_VISTO != 0) {
        $js .= "	CarregarListaDeArquivos('" . $acCand->mCampoNome['NU_CANDIDATO']->mValor . "','arquivosListadosProcesso');\n";
        ?>	

                <div id="Processo">
                <?php
                $mte = new cEDICAO("PROCESSO_MTE");
                $mte->CarregarConfiguracao();
                $mte->mCampoNome['id_solicita_visto']->mValor = $OrdemServico->mID_SOLICITA_VISTO;
                $mte->RecupereRegistro();
                //
                // Manda gravar se recebeu o post
                if (isset($_POST[cINTERFACE::PREFIXO_CAMPOS . $mte->mCampoCtrlEnvio->mNome])) {
                    cINTERFACE::RecupereValoresEdicao($mte, $_POST);
                    try {
                        $mte->AtualizarBanco();
                        $tabOS_selected = 4;
                        $mensagem = "jAlert('Dados do processo atualizados com sucesso', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                    } catch (Exception $exc) {
                        error_log($exc->getMessage() . $exc->getTraceAsString());
                        $mensagem = "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                    }
                }
                //
                // Conclui 
                if (isset($_POST['formConcluirProtocolo'])) {
                    cINTERFACE::RecupereValoresEdicao($mte, $_POST);
                    try {
                        $id_solicita_visto = $_POST['id_solicita_visto'];
                        $tabOS_selected = 4;
                        $sql = "update solicita_visto set ID_STATUS_SOL = 5 where id_solicita_visto = " . $id_solicita_visto;
                        mysql_query($sql);
                        $OrdemServico->mID_STATUS_SOL = 5;
                        $mensagem = "jAlert('Protocolo encerrado com sucesso.', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                    } catch (Exception $exc) {
                        error_log($exc->getMessage() . $exc->getTraceAsString());
                        $mensagem = "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                    }
                }

                if ($mte->mCampoNome['nu_processo']->mValor != '') {
                    $botaoMTE = "&nbsp;<input type=\"button\" value=\"MTE\" onClick=\"javascript:veracomp(" . $mte->mCampoNome['codigo']->mValor . ",'MTE','" . preg_replace("/[^0-9]/", "", $mte->mCampoNome['nu_processo']->mValor) . "');\" style=\"font-size:8pt;font-weight:bold;padding-left:1px;padding-right:1px;\" title=\"Clique para ver o processo no Minist&eacute;rio do Trabalho\" />";
                } else {
                    $botaoMTE = '';
                }
                ?>
                    <form id="formProtocolo" method="post">
                        <div class="duplasubTitulo">Protocolo<?= $botaoMTE; ?></div>
                    <?= cINTERFACE::RenderizeCampo($mte->mCampoCtrlEnvio); ?>
                    <?= cINTERFACE::RenderizeCampo($mte->mCampoNome["id_solicita_visto"]); ?>
                    <?= cINTERFACE::RenderizeEdicao($mte); ?>
                        </table>
                    </form>
                    <form method="post" id="formConcluirProtocolo">
                    <?= cINTERFACE::inputHidden('formConcluirProtocolo', '1'); ?>
                    <?= cINTERFACE::inputHidden('id_solicita_visto', $pID_SOLICITA_VISTO); ?>
                    </form>		
                    <div style="text-align:right">
                    <?php if ($OrdemServico->mID_STATUS_SOL == 4) { ?>
                            <input type="button" value="Salvar" onclick="document.forms['formProtocolo'].submit();"/>
                            <input type="button" value="Concluir" onclick="document.forms['formConcluirProtocolo'].submit();"/>
                        <?php
                    }
                    ?>
                    </div>
                    <?php
                    if ($OrdemServico->mID_STATUS_SOL >= 5) {
                        $tela = new cEDICAO("ANDAMENTO_MTE");
                        $tela->CarregarConfiguracao();
                        $tela->mCampoNome['id_solicita_visto']->mValor = $OrdemServico->mID_SOLICITA_VISTO;
                        $tela->RecupereRegistro();
                        //
                        // Manda gravar se recebeu o post
                        if (isset($_POST[cINTERFACE::PREFIXO_CAMPOS . $tela->mCampoCtrlEnvio->mNome])) {
                            cINTERFACE::RecupereValoresEdicao($tela, $_POST);
                            try {
                                $tela->AtualizarBanco();
                                $tabOS_selected = 4;
                                $mensagem = "jAlert('Andamento atualizado com sucesso', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                            } catch (Exception $exc) {
                                error_log($exc->getMessage() . $exc->getTraceAsString());
                                $mensagem = "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                            }
                        }
                        ?>
                        <div class="duplasubTitulo">Andamento</div>
                        <form id="formAndamento" method="post">		
                            <div>
                        <?= cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio); ?>
                        <?= cINTERFACE::RenderizeCampo($tela->mCampoNome["id_solicita_visto"]); ?>
                        <?= cINTERFACE::RenderizeEdicao($tela); ?>
                                </table>
                            </div>
                        </form>
                        <form method="post" id="formConcluirAndamento">
                        <?= cINTERFACE::inputHidden('formConcluirAndamento', '1'); ?>
                        <?= cINTERFACE::inputHidden('id_solicita_visto', $pID_SOLICITA_VISTO); ?>
                        </form>		
                        <div style="text-align:right">
                        <?php if ($OrdemServico->mID_STATUS_SOL == 5) { ?>
                                <input type="button" value="Salvar" onclick="document.forms['formAndamento'].submit();"/>
                                <input type="button" value="Concluir" onclick="document.forms['formConcluirAndamento'].submit();"/>
                            <?php
                        }
                        ?>
                        </div>
                        <?php
                    }  // If servico >=5
                    //
		// INICIO AREA PROCESSO MTE CONCLUSAO
                    if ($OrdemServico->mID_STATUS_SOL >= 6) {
                        $tela = new cEDICAO("CONCLUSAO_MTE");
                        $tela->CarregarConfiguracao();
                        $tela->mCampoNome['id_solicita_visto']->mValor = $OrdemServico->mID_SOLICITA_VISTO;
                        $tela->RecupereRegistro();
                        //
                        // Manda gravar se recebeu o post
                        if (isset($_POST[cINTERFACE::PREFIXO_CAMPOS . $tela->mCampoCtrlEnvio->mNome])) {
                            cINTERFACE::RecupereValoresEdicao($tela, $_POST);
                            try {
                                $tela->AtualizarBanco();
                                $tabOS_selected = 4;
                                $mensagem = "jAlert('Processo concluído com sucesso', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                            } catch (Exception $exc) {
                                error_log($exc->getMessage() . $exc->getTraceAsString());
                                $mensagem = "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                            }
                        }
                        ?>
                        <div class="duplasubTitulo">Conclusão</div>
                        <form id="formProcesso" method="post">		
                            <div>
                        <?= cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio); ?>
                        <?= cINTERFACE::RenderizeCampo($tela->mCampoNome["id_solicita_visto"]); ?>
                        <?= cINTERFACE::RenderizeEdicao($tela); ?>
                        <?= cINTERFACE::inputHidden('tab_selected', '4'); ?>
                                </table>
                            </div>
                        </form>
                        <form method="post" id="formCobrarProcesso">
                        <?= cINTERFACE::inputHidden('formCobrarProcesso', '1'); ?>
                        <?= cINTERFACE::inputHidden('id_solicita_visto', $pID_SOLICITA_VISTO); ?>
                        <?= cINTERFACE::inputHidden('tab_selected', '4'); ?>
                        </form>
                        <?
                        print '<div style="text-align:right">' . "\n";
                        if ($OrdemServico->mID_STATUS_SOL == 6) {
                            print '<input type="button" value="Salvar" onclick="document.forms[\'formProcesso\'].submit();"/>' . "\n";
                            print '<input type="button" value="Encerrar" onclick="document.forms[\'formEncerrarProcesso\'].submit();"/>' . "\n";
                        }
                        if ($OrdemServico->mfl_cobrado == 0) {
                            print '<input type="button" value="Cobrar" onclick="document.forms[\'formCobrarProcesso\'].submit();"/>' . "\n";
                        } else {
                            print '<input type="button" value="Cobrada" onclick="jAlert(\'Essa solicitação já foi marcada como cobrada\');" style="color:#999;"/>' . "\n";
                        }
                        print '</div>' . "\n";
                    } // if servico =6
                } // if >= 4
                print "</div>\n";
            } // if acompanhamento = 1
            else {
                if ($OrdemServico->mID_SOLICITA_VISTO != 0) {
                    $nomeTela = '';
                    if ($OrdemServico->mReadonly) {
                        $tipoTela = 'view';
                    } else {
                        $tipoTela = 'edit';
                    }
                    if ($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 3) {
                        $nomeTela = 'processo_prorrog_' . $tipoTela;
                    } elseif ($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 4) {
                        $nomeTela = 'processo_regcie_' . $tipoTela;
                    } elseif ($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 5) {
                        $nomeTela = 'processo_emiscie_' . $tipoTela;
                    } elseif ($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 6) {
                        $nomeTela = 'processo_cancel_' . $tipoTela;
                    }
                    if ($nomeTela != '') {
                        $tela = new cEDICAO($nomeTela);
                        $tela->CarregarConfiguracao();
                        $tela->mCampoNome['id_solicita_visto']->mValor = $pID_SOLICITA_VISTO;
                        $tela->RecupereRegistro();
                        //
                        // Testa envio e salva
                        if (isset($_POST[cINTERFACE::PREFIXO_CAMPOS . $tela->mCampoCtrlEnvio->mNome])) {
                            $tabOS_selected = 3;
                            cINTERFACE::RecupereValoresEdicao($tela, $_POST);
                            try {
                                $tela->AtualizarBanco();
                                $mensagem = "jAlert('Processo atualizado com sucesso', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                            } catch (Exception $exc) {
                                error_log($exc->getMessage() . $exc->getTraceAsString());
                                $mensagem = "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
                            }
                        }
                    }

                    print '<div id="Processo">' . "\n";
                    if ($nomeTela != '') {
                        print '<form id="formProcesso" method="post">' . "\n";
                        print cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio);
                        print cINTERFACE::RenderizeCampo($tela->mCampoNome["id_solicita_visto"]);
                        print cINTERFACE::RenderizeEdicao($tela);
                        print cINTERFACE::inputHidden('tab_selected', '3');
                        print '</table>' . "\n";
                        print '</form>' . "\n";
                    }
                    print '<form method="post" id="formConcluir">' . "\n";
                    print cINTERFACE::inputHidden('formConcluirProcesso', '1') . "\n";
                    print cINTERFACE::inputHidden('id_solicita_visto', $pID_SOLICITA_VISTO) . "\n";
                    print cINTERFACE::inputHidden('tab_selected', '3');
                    print cINTERFACE::inputHidden('de_observacao_concluir', '');
                    print '</form>' . "\n";
                    print '<form method="post" id="formCobrarProcesso">' . "\n";
                    print cINTERFACE::inputHidden('formCobrarProcesso', '1') . "\n";
                    print cINTERFACE::inputHidden('id_solicita_visto', $pID_SOLICITA_VISTO) . "\n";
                    print cINTERFACE::inputHidden('tab_selected', '3');
                    print cINTERFACE::inputHidden('de_observacao_cobrar', '');
                    print '</form>' . "\n";
                    if ($OrdemServico->mReadonly) {
                        $readonly = " readonly ";
                    } else {
                        $readonly = "";
                    }

                    if ($nomeTela == '') {
                        print '<table class="edicao dupla">';
                        print '<col width="18%"></col>';
                        print '<col width="28%"></col>';
                        print '<col width="5%"></col>';
                        print '<col width="18%"></col>';
                        print '<col width="28%"></col>';
                        print '<tr><th>Observação</th><td colspan="4"><textarea id="de_observacao" name="de_observacao" ' . $readonly . ' rows="3">' . $OrdemServico->mde_observacao . '</textarea></td></tr>';
                        print '</table><br/>';
                    }
                    if (!$OrdemServico->mReadonly) {
                        print '<div style="text-align:center">' . "\n";
                        if ($nomeTela != '') {
                            print '<input type="button" value="Salvar" onclick="document.forms[\'formProcesso\'].submit();"/>' . "\n";
                        }
                        print '<input type="button" value="Concluir" onclick="$(\'de_observacao_concluir\').val($(\'de_observacao\').val());document.forms[\'formConcluir\'].submit();"/>' . "\n";
                        if ($OrdemServico->mfl_cobrado == 0) {
                            print '<input type="button" value="Cobrar" onclick="$(\'de_observacao_cobrar\').val($(\'de_observacao\').val());document.forms[\'formCobrarProcesso\'].submit();"/>' . "\n";
                        } else {
                            print '<input type="button" value="Cobrada" onclick="jAlert(\'Essa solicitação já foi marcada como cobrada\');" style="color:#999;"/>' . "\n";
                        }
                        print '</div>' . "\n";
                    }
                    print $divsArqsProcesso;
                    print '</div>' . "\n";
                }
            }
            ?>
        </div>
    </div>
    <br />
            <?php
            echo Rodape("");

            if ($tabOS_selected == '') {
                $tabOS_selected = 0;
            }

            if (isset($_SESSION['msg']) && $_SESSION['msg'] != '') {
                $mensagem.="\njAlert('" . $_SESSION['msg'] . "');";
                $_SESSION['msg'] = '';
            }
            ?>
    <script language="javascript">

        var NU_EMPRESA_ANT;
        $(document).ready(function () {
            var x = $("#tabsOS").tabs({fxAutoHeight: true});
            $("#tabsOS").tabs("select", <?= $tabOS_selected; ?>);
            <?= $js; ?>

            <?php if ($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 1 && $OrdemServico->mID_STATUS_SOL >= 2) { ?>
                var y = $("#tabsIntx").tabs({fxAutoHeight: true});
                $("#tabsIntx").tabs("select", <?= $tabsIntx_selected; ?>);
            <?php } ?>
            <?php if (($OrdemServico->mID_TIPO_ACOMPANHAMENTO == 1 && $OrdemServico->mID_STATUS_SOL >= 4) || ($OrdemServico->mID_TIPO_ACOMPANHAMENTO != 1)) { ?>
                var z = $("#tabsForms").tabs({fxAutoHeight: true});
            <?php } ?>
            <?= $mensagem; ?>
            carregarArquivos();
            // Adiciona refresh via javascript na embarcação
            <?php if ($pID_SOLICITA_VISTO == 0) { ?>
                $('#CMP_NU_EMPRESA').change(function () {
                    $('#NU_EMPRESA').val($('#CMP_NU_EMPRESA').val());
                    atualizaEmbarcacaoProjeto();
                    if ($('#CMP_nu_empresa_requerente').val() == '' || $('#CMP_nu_empresa_requerente').val() == NU_EMPRESA_ANT) {
                        $('#CMP_nu_empresa_requerente').val($('#CMP_NU_EMPRESA').val());
                    }
                    NU_EMPRESA_ANT = $('#CMP_NU_EMPRESA').val();
                });
    <?php } ?>
            if ($('#NU_EMPRESA').val() != '' && $('#CMP_NU_EMPRESA').val() != '0') {
                atualizaEmbarcacaoProjeto();
            }
        });

        function atualizaEmbarcacaoProjeto() {
            var url = '/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA=' + $('#NU_EMPRESA').val() + '&NU_EMBARCACAO_PROJETO=' + $('#NU_EMBARCACAO_PROJETO').val();
            $.ajax({
                url: url,
                async: false,
                success: function (data) {
                    $('#tdEmbarcacaoProjeto').html('<select name="CMP_NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>' + data + '</select>');
                }
            });
        }

        function ajaxIncluir(pCampo, pValor) {
            var xChaves = $('#DadosOS_' + pCampo + '_chaves').val();
            var xVirgula = '';
            if (xChaves != '')
            {
                xVirgula = ',';
            }

            var valor = pValor.split(',');
            var valorpesq = '';
            var xChavespesq = ',' + xChaves + ',';
            var i = 0;
            var incluido = false;
            for (i = 0; i < valor.length; i++) {
                valorpesq = ',' + valor[i] + ',';
                if (xChavespesq.indexOf(valorpesq) < 0) {
                    xChaves = xChaves + xVirgula + valor[i];
                    incluido = true;
                    xVirgula = ',';
                }
            }
            if (incluido) {
                $('#DadosOS_' + pCampo + '_chaves').val(xChaves);
                CarregarCandidatos('<?= $pID_SOLICITA_VISTO; ?>', 'DadosOS_CANDIDATO_container', 'DadosOS_CANDIDATO_chaves', '0');
                return 'Candidato(s) incluído(s) com sucesso!';
            }
            else {
                return 'O(s) candidato(s) indicados já estão na solicitação.';
            }
        }

        function ajaxRemoverCandidato(pID_SOLICITA_VISTO, pNU_CANDIDATO) {
            if (confirm('Deseja mesmo remover esse candidato da solicitação?\n(Essa operação não é reversível)')) {
                var xChaves = $('#DadosOS_CANDIDATO_chaves').val();
                var xPos = xChaves.indexOf(pNU_CANDIDATO);
                if (xPos >= 0) {
                    var xChavesNovo = '';
                    var arr = xChaves.split(',');
                    xVirg = '';
                    xI = 0;
                    while (xI < arr.length) {
                        if (arr[xI] != pNU_CANDIDATO) {
                            xChavesNovo = xChavesNovo + xVirg + arr[xI];
                            xVirg = ',';
                        }
                        xI++;
                    }
                    $('#DadosOS_CANDIDATO_chaves').val(xChavesNovo);
                    url = "/LIB/CANDIDATO_DEL.php?id_solicita_visto=" + pID_SOLICITA_VISTO + "&NU_CANDIDATO=" + pNU_CANDIDATO;
                    if (pID_SOLICITA_VISTO > 0) {
                        $.ajax({
                            url: url,
                            success: function (data) {
                                jAlert(data);
                                CarregarCandidatos('<?= $pID_SOLICITA_VISTO; ?>', 'DadosOS_CANDIDATO_container', 'DadosOS_CANDIDATO_chaves', '1');
                            }
                        });
                    }
                }
            }
            return '';
        }

        function CarregarListaDeArquivos(pNU_CANDIDATO, pNomeDiv) {
            var url = '../LIB/ARQUIVOS_LISTA.php?NU_CANDIDATO=' + pNU_CANDIDATO + '&NomeDiv=' + pNomeDiv;
            $.ajax({
                url: url,
                async: false,
                success: function (data) {
                    $('#' + pNomeDiv + pNU_CANDIDATO).html(data);
                }
            });
        }

        function CarregarArquivo(pCaminho, pNU_SEQUENCIAL, pNomeDiv) {
            var dimensoes = new Array();
            $.ajax({
                url: '/LIB/imagemTam.php?NU_SEQUENCIAL=' + pNU_SEQUENCIAL,
                async: false,
                success: function (data) {
                    dimensoes = data.split("#");

                }
            });
            var pWidth = +dimensoes[0];
            var pHeight = +dimensoes[1];
            //alert('pWidth='+pWidth+';pHeight='+pHeight);
            var maxW = 400;
            var maxH = 300;
            var w = maxW;
            var h = maxH;
            if (pWidth > pHeight)
            {
                h = Math.round(pHeight / pWidth * maxW);
            }
            else
            {
                w = Math.round(pWidth / pHeight * maxH);
            }
            var d = new Date();
            var xImagem = '<img src="' + pCaminho + '?' + d.getTime() + '" ';
            xImagem = xImagem + 'title="Use ctrl+mouse para cima ou para baixo (com botão pressionado) para dar zoom"/>';
            xImagem = xImagem + '<br/><input type="button" onclick="abreJanelaxxx(\'/LIB/editaImagem.php?NU_SEQUENCIAL=' + pNU_SEQUENCIAL + '\',\'wteste\');" value="Editar"/>';
            xImagem = xImagem + '<input type="button" value="Atualizar" onclick="CarregarArquivo(\'' + pCaminho + '\',\'' + pNU_SEQUENCIAL + '\',\'' + pNomeDiv + '\')"/>';
            $('#' + pNomeDiv).html(xImagem);
            $('#' + pNomeDiv + ' img').imagetool({
                viewportWidth: w
                , vIewportHeight: h
            });
        }

        function RecuperarFormularios(pid_solicita_visto, pNU_CANDIDATO) {
            $('#formulariosCand' + pNU_CANDIDATO).html('(recuperando formulários disponíveis. Por favor, aguarde...)');
            var url = "/LIB/OS_FORMULARIOS.php?id_solicita_visto=" + pid_solicita_visto + "&FL_OS=0&FL_CANDIDATO=1&NU_CANDIDATO=" + pNU_CANDIDATO;
            $.ajax({
                url: url,
                success: function (data) {
                    $('#formulariosCand' + pNU_CANDIDATO).html(data);
                }
            });
        }

        function RecuperarFormulariosOs(pid_solicita_visto) {
            $('#formulariosOS').html('(recuperando formulários disponíveis. Por favor, aguarde...)');
            var url = "/LIB/OS_FORMULARIOS.php?id_solicita_visto=" + pid_solicita_visto + "&FL_OS=1&FL_CANDIDATO=0"
            $.ajax({
                url: url,
                success: function (data) {
                    $('#formulariosOS').html(data);
                }
            });
        }

        function novo(pcmbEmbarcacaoProjeto) {
            var xNU_EMPRESA = '<?= $OrdemServico->mNU_EMPRESA; ?>';
            var xNU_EMBARCACAO_PROJETO = $('#' + pcmbEmbarcacaoProjeto).val();

            if (xNU_EMPRESA == '0') {
                jAlert('Selecione a empresa antes de incluir candidatos.<br>(A empresa é necessária caso você precise incluir um novo candidato.)', 'Atenção');
            } else {
                var nome = "";
                var pagina = "/solicitacoes/novaReqVistoCandNovo.php?NU_EMPRESA=" + xNU_EMPRESA + "&NU_EMBARCACAO_PROJETO=" + xNU_EMBARCACAO_PROJETO;

                abreJanelaxxx(pagina, 'wteste');
                //w.focus();
            }
        }

        function validaFormOS() {
            msg = '';
            if (document.sol.idEmpresa.selectedIndex < 1) {
                msg += "<br/>- Empresa não indicada;";
            }
            if (document.sol.NU_EMBARCACAO_PROJETO[document.sol.NU_EMBARCACAO_PROJETO.selectedIndex].value == '') {
                msg += "<br/>- Projeto/embarcação não indicado;";
            }
            if ($('#NU_SERVICO').val() == '0') {
                msg += "<br/>- Tipo de serviço não indicado;";
            }
            if (document.sol.NO_SOLICITADOR.value == '') {
                msg += "<br/>- Solicitante não informado;";
            }
            if (document.sol.DT_SOLICITACAO.value == '') {
                msg += "<br/>- Data da solicitação não informada";
            }
            if (document.sol.cd_Tecnico[document.sol.cd_Tecnico.selectedIndex].value == '') {
                msg += "<br/>- Responsável pelo complemento do cadastro não informado;";
            }
            if (msg != '') {
                jAlert('Corrija os seguintes itens:' + msg, 'Não foi possível salvar a OS');
            } else {
                jConfirm('Deseja mesmo alterar a OS?', 'Alterar OS', function (r) {
                    if (r) {
                        document.sol.submit();
                    }
                })
            }
        }

        var idEmpresa = "<?= $OrdemServico->mNU_EMPRESA ?>";
        var idEmbarcacaoProjeto = $('#NU_EMBARCACAO_PROJETO').val();

        function enviar(tipo) {
            var cand = document.wilson.idCandidato.value;
            var emp = document.wilson.idEmpresa.value;
            var sol = document.wilson.NU_SOLICITACAO.value;
            var cd_admin = document.wilson.cd_admin.value;
            var pagina = "operacao3.php?idCandidato=" + cand + "&idEmpresa=" + emp + "&opvisa=" + tipo + "&NU_SOLICITACAO=" + sol + "&cd_admin=" + cd_admin + "&A=";

            document.wilson.opvisa.value = tipo;
            document.wilson.target = "_top";
            document.wilson.action = "operacao3.php";
            //window.open(pagina);
            var msg = OpenChild(tipo, pagina);
            document.all.aguarde.innerHTML = msg;
        }

        function arquivos(tipo) {
            document.wilson.opvisa.value = tipo;
            document.wilson.target = "_top";
            //var msg = OpenChild(tipo,"operacao3up.php");
            var msg = OpenChild(tipo, "/admin/documentos1.php");
            document.all.aguarde.innerHTML = msg;
        }

        function formulario(tipo) {
            document.wilson.action = "/formularios/all_registroPF.php";
            document.wilson.target = "formPF";
            document.wilson.submit();
        }

        function visadetail(codigo, acao, visa) {
            var cd_admin = "<?= $usulogado ?>";
            var pagina = "../intranet/geral/novoVisaDetail.php?idCandidato=" + codigo + "&acao=" + acao + "&idEmpresa=" + idEmpresa + "&NU_SOLICITACAO=" + visa + "&cd_admin=" + cd_admin;
            var ret = AbrePagina(acao, pagina, idEmpresa, codigo, idEmbarcacaoProjeto);
        }

        function carregarArquivos() {
            var id = $('#CMP_id_solicita_visto').val();
            if (id != '0') {
                $.ajax({
                    url: "/LIB/ARQUIVOS_SOL_LISTA.php?ID_SOLICITA_VISTO=" + $('#CMP_id_solicita_visto').val(),
                    success: function (data) {
                        $('#ARQUIVOS_container').html(data);
                    }
                });
            }
            else {
                $('#ARQUIVOS_container').html('');
            }
        }

        function ajaxRemoverArquivoSol(pNU_SEQUENCIAL, pNome) {
            jConfirm('Deseja mesmo excluir o arquivo "' + pNome + '"? Essa operacao é irreversível.', 'Exclusão de arquivo', function (r) {
                if (r) {
                    var url = "/LIB/ARQUIVOS_DEL.php?NU_SEQUENCIAL=" + pNU_SEQUENCIAL;
                    $.ajax({
                        url: url,
                        success: function (data) {
                            carregarArquivos();
                            jAlert(data);
                        }
                    });
                }
            });
        }
    </script>
