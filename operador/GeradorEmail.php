<?php

    require_once '../LIB/ControleEventos.php';
    require_once '../PHPMailer/class.phpmailer.php';
    
        
    $html = new Generic;
    $html->cache_submit = true;

?>
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/js/ckeditor/adapters/jquery.js"></script>
<script>
$(document).ready(function(){
    $('textarea[name="mail"]').ckeditor(function() { /* callback code */ }, { skin : 'v2', extraPlugins : 'autogrow' });
})
</script>
<div class="titulo"> 
    Ordem de serviço - Gerador E-mail <div style="float: right;"><?=menuBarOS($usulogado, $os['id_solicita_visto'], $os['NU_SERVICO'], $os['fl_nao_conformidade'], $os['fl_nao_conformidade'], 1)?></div>
</div>

<?

    if($isPost){
        
        if($sender){
            echo '<div class="success">Email enviado com sucesso.</div>';
        }else{
            echo '<div class="error">Houve falha no envio o e-mail: '.$ErrorInfo.'</div>';
        }
    }



?>
<div style="padding: 15px;">
<div class="boxer" style="width: auto;">
<form method="post">
<table style="width:100%;">
    <tr>
        <td style="width:80px;">De: </td>
        <td><?=$html->input('text', array('name'=>'remetente', 'value'=>cSESSAO::$mnm_email , 'style'=>'font:12px arial;width:100%;padding:3px;background:#fff'))?></td>
    </tr>
    <tr>
        <td>Para:</td>
        <td><?=$html->input('text', array('name'=>'destinatario', 'style'=>'font:12px arial;width:100%;background:#fff;padding:3px;color:#575757'))?></td>
    </tr>
    <tr>
        <td>Assunto:</td>
        <td>
            <?=$html->input('text', array('name'=>'titulo', 'style'=>'font:12px arial;width:100%;padding:3px;background:#fff;color:#575757'))?>
        </td>
    </tr>
</table>
</div>
<textarea name="mail">
<?=$textoemail?>
<table border="1" cellpadding="3" cellspacing="0">
    <thead>
        <tr>
            <th>NAME</th><th>POSITION</th><th>SALARY EXTERIOR US$</th><th>CONSULATE</th>
        </tr>
    </thead>    
    <tbody>     
       <?foreach($candidatos as $candidato): $id = '['.$candidato['NU_CANDIDATO'].']'; ?>
          <tr>
            <td><b><?=$candidato['NOME_COMPLETO'];?></b></td>
            <td><?=$candidato['NO_FUNCAO_EM_INGLES'];?></td>
            <td>US$ <?=$html->moedaAmericana($candidato['SALARIO_DOLAR']);?></td>
            <td><?=$candidato['NO_CIDADE'];?></td>
          </tr>
       <?endforeach;?>
    </tbody>
</table>
</textarea>
<input type="submit" value="Enviar" />
 
</form>    
  </div>
 </body>
</html>
