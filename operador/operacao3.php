<?php
include ("../LIB/conecta.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/libVisaDetail.php");
include ("../LIB/classes/classe_proc_cancel.php");
include ("../LIB/classes/classe_proc_emiscie.php");
include ("../LIB/classes/classe_proc_mte.php");
include ("../LIB/classes/classe_proc_prorrog.php");
include ("../LIB/classes/classe_proc_regcie.php");

$opvisa = trim($_REQUEST['opvisa']);
$fase = 0+$_REQUEST['fase'];
$idEmpresa = $_REQUEST['idEmpresa'];
$idCandidato = $_REQUEST['idCandidato'];
$NU_SOLICITACAO = $_REQUEST['NU_SOLICITACAO'];
$cd_admin = $_REQUEST['cd_admin'];

#print "<br>opvisa=$opvisa";
#print "<br>fase=$fase";
#print "<br>idEmpresa=$idEmpresa";
#print "<br>idCandidato=$idCandidato";
#print "<br>NU_SOLICITACAO=$NU_SOLICITACAO";
#print "<br>cd_admin=$cd_admin";

$acao = "A";
$nmEmpresa = pegaNomeEmpresa($idEmpresa);
$nmCandidato = pegaNomeCandidato($idCandidato);

if(strlen($idCandidato)>0)  {
  print IniPag();
  if(strlen($NU_SOLICITACAO) > 0) {
    $ultimaSol = $NU_SOLICITACAO;
  } else {
    $ultimaSol = pegaUltimaSolicitacao($idCandidato);
    $NU_SOLICITACAO = $ultimaSol;
  }
  if($opvisa == "PROCMTE") {
    $obj = new proc_mte();
    $obj->BuscaPorCandidato($idCandidato,$ultimaSol);
    $msg = $obj->myerr;
  } else if($opvisa == "REGCIE") {
    $obj = new proc_regcie();
    $obj->BuscaPorCandidato($idCandidato,$ultimaSol);
    $msg = $obj->myerr;
  } else if($opvisa == "EMISCIE") {
    $obj = new proc_emiscie();
    $obj->BuscaPorCandidato($idCandidato,$ultimaSol);
    $msg = $obj->myerr;
  } else if($opvisa == "PRORR") {
    $obj = new proc_prorrog();
    $obj->BuscaPorCandidato($idCandidato,$ultimaSol);
    $msg = $obj->myerr;
  } else if($opvisa == "CANC") {
    $obj = new proc_cancel();
    $obj->BuscaPorCandidato($idCandidato,$ultimaSol);
    $msg = $obj->myerr;
  } else {
    $autorizacao = lerVisaDetail($idEmpresa,$idCandidato,$ultimaSol);
    if($autorizacao != null){
     $NU_SOLICITACAO=$autorizacao->getNU_SOLICITACAO();
     $NU_EMBARCACAO_PROJETO=$autorizacao->getNU_EMBARCACAO_PROJETO();
     $CO_TIPO_AUTORIZACAO=$autorizacao->getCO_TIPO_AUTORIZACAO();
     $VA_RENUMERACAO_MENSAL=$autorizacao->getVA_RENUMERACAO_MENSAL();
     $NO_MOEDA_REMUNERACAO_MENSAL=$autorizacao->getNO_MOEDA_REMUNERACAO_MENSAL();
     $VA_REMUNERACAO_MENSAL_BRASIL=$autorizacao->getVA_REMUNERACAO_MENSAL_BRASIL();
     $CO_REPARTICAO_CONSULAR=$autorizacao->getCO_REPARTICAO_CONSULAR();
     $CO_FUNCAO_CANDIDATO=$autorizacao->getCO_FUNCAO_CANDIDATO();
     $TE_DESCRICAO_ATIVIDADES=$autorizacao->getTE_DESCRICAO_ATIVIDADES();
     $DT_ABERTURA_PROCESSO_MTE=$autorizacao->getDT_ABERTURA_PROCESSO_MTE();
     $NU_PROCESSO_MTE=$autorizacao->getNU_PROCESSO_MTE();
     $NU_AUTORIZACAO_MTE=$autorizacao->getNU_AUTORIZACAO_MTE();
     $DT_AUTORIZACAO_MTE=$autorizacao->getDT_AUTORIZACAO_MTE();
     $DT_PRAZO_AUTORIZACAO_MTE=$autorizacao->getDT_PRAZO_AUTORIZACAO_MTE();
     $DT_VALIDADE_VISTO=$autorizacao->getDT_VALIDADE_VISTO();
     $NU_VISTO=$autorizacao->getNU_VISTO();
     $DT_EMISSAO_VISTO=$autorizacao->getDT_EMISSAO_VISTO();
     $NO_LOCAL_EMISSAO_VISTO=$autorizacao->getNO_LOCAL_EMISSAO_VISTO();
     $CO_NACIONALIDADE_VISTO=$autorizacao->getCO_NACIONALIDADE_VISTO();
     $NU_PROTOCOLO_CIE=$autorizacao->getNU_PROTOCOLO_CIE();
     $DT_PROTOCOLO_CIE=$autorizacao->getDT_PROTOCOLO_CIE();
     $DT_VALIDADE_PROTOCOLO_CIE=$autorizacao->getDT_VALIDADE_PROTOCOLO_CIE();
     $DT_PRAZO_ESTADA=$autorizacao->getDT_PRAZO_ESTADA();
     $DT_VALIDADE_CIE=$autorizacao->getDT_VALIDADE_CIE();
     $DT_EMISSAO_CIE=$autorizacao->getDT_EMISSAO_CIE();
     $NU_PROTOCOLO_PRORROGACAO=$autorizacao->getNU_PROTOCOLO_PRORROGACAO();
     $DT_PROTOCOLO_PRORROGACAO=$autorizacao->getDT_PROTOCOLO_PRORROGACAO();
     $DT_VALIDADE_PROTOCOLO_PROR=$autorizacao->getDT_VALIDADE_PROTOCOLO_PROR();
     $DT_PRETENDIDA_PRORROGACAO=$autorizacao->getDT_PRETENDIDA_PRORROGACAO();
     $DT_CANCELAMENTO=$autorizacao->getDT_CANCELAMENTO();
     $NU_PROCESSO_CANCELAMENTO=$autorizacao->getNU_PROCESSO_CANCELAMENTO();
     $DT_PROCESSO_CANCELAMENTO=$autorizacao->getDT_PROCESSO_CANCELAMENTO();
     $TE_OBSERVACOES_PROCESSOS=$autorizacao->getTE_OBSERVACOES_PROCESSOS();
     $DT_PRAZO_ESTADA_SOLICITADO=$autorizacao->getDT_PRAZO_ESTADA_SOLICITADO();
     $DT_ENTRADA = $autorizacao->getDT_ENTRADA();
     $NO_LOCAL_ENTRADA = $autorizacao->getNO_LOCAL_ENTRADA();
     $NU_TRANSPORTE_ENTRADA = $autorizacao->getNU_TRANSPORTE_ENTRADA();
     $CO_CLASSIFICACAO_VISTO = $autorizacao->getCO_CLASSIFICACAO_VISTO();
     $CO_NACIONALIDADE_VISTO = $autorizacao->getCO_NACIONALIDADE_VISTO();
     $NO_UF_ENTRADA = $autorizacao->getNO_UF_ENTRADA();
     $nmProjetos = pegaNomeProjeto($NU_EMBARCACAO_PROJETO,$idEmpresa);
     $nmAutorizacao = pegaNomeTipoAutorizacao2($CO_TIPO_AUTORIZACAO);
     $nmReparticao = pegaNomeReparticao($CO_REPARTICAO_CONSULAR);
     $nmFuncao = pegaNomeFuncao($CO_FUNCAO_CANDIDATO);
     $nmNacVisto = pegaNomeNacionalidade($CO_NACIONALIDADE_VISTO);
     $nmArmador = pegaNomeUsuario($NU_ARMADOR);
     $nmClassVisto = pegaNomeClassVisto($CO_CLASSIFICACAO_VISTO);
     $nmUFEntrada = $NO_UF_ENTRADA;
     $nmTransEntrada = pegaNomeTransEntrada($NU_TRANSPORTE_ENTRADA);
    }
    $aux_esc = "<option value=''>Escolha ";
    $cmbNacVisto = "$aux_esc uma nacionalidade".montaComboNacionalidade($CO_NACIONALIDADE_VISTO,"");
    $cmbClassVisto = "$aux_esc uma classificacao".montaComboClassVisto($CO_CLASSIFICACAO_VISTO);
    $cmbUFEntrada = "$aux_esc um estado".montaComboEstados($NO_UF_ENTRADA);
    $cmbTransEntrada = "$aux_esc um transporte".montaComboTransEntrada($NU_TRANSPORTE_ENTRADA);
  }

  print "<table width=100% border=0 class='textoazulpeq' cellspacing=0>\n";
  print "<tr><td width=100>Empresa:</td><td> $nmEmpresa</td></tr>\n";
  print "<tr><td>Candidato:</td><td> $nmCandidato ($ultimaSol)</td></tr>\n";
  print "</table>\n<br>";
} else if($fase==0) {
  $opvisa="ENTRADA"; 
  print "<html><body onload='javascript:doInit();'>";
} else {
  $msg = "N&atilde; foi informado o candidato.";
}

?>

 <form action="operacao4.php" method="post" name=visto target = "WILSONS">
 <input type=hidden name=fase value="1">
 <input type=hidden name=opvisa value="<?=$opvisa?>">
 <input type=hidden name=cd_admin value="<?=$cd_admin?>">
 <input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
 <input type=hidden name=idCandidato value='<?=$idCandidato?>'>
 <input type=hidden name=NU_SOLICITACAO value='<?=$NU_SOLICITACAO?>'>

<center>
<?php
print "<table width=100% border=0 class='textoazulpeq' cellspacing=0>\n";
if(strlen($msg)>0) {
  print "<tr><td><font color=red>$msg</font></td></tr>";
} else {
  if($opvisa == "PROCMTE") {
    $obja = new proc_mte();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoProcessoMTE("A");
  } else if($opvisa == "REGCIE") {
    $obja = new proc_regcie();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoRegistroCIE("A");
  } else if($opvisa == "EMISCIE") {
    $obja = new proc_emiscie();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoEmissaoCIE("A");
  } else if($opvisa == "PRORR") {
    $obja = new proc_prorrog();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoProrrogacao("A");
  } else if($opvisa == "CANC") {
    $obja = new proc_cancel();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoCancel("A");
  } else {
    include ("../intranet/inc/dados_visto_inc.php");
  }
}
print "</table>\n";

if ($idCandidato>0) {
   print "<br><input onclick='javascript:enviar();' type=\"button\" value='Enviar' style=\"<?=$estilo?>\">&nbsp;<input  type=\"button\" value=\"Fechar\" onClick=\"fechar();\" style=\"<?=$estilo?>\"/>";
}

?>
</form>

<br><br>
<span id=aguarde class='textoazul' style='display:none'>Aguarde !!!</span>

 <form action="operacao3.php" method="post" name=will target = "WILSONS">
 <input type=hidden name=fase value="1">
 <input type=hidden name=opvisa>
 <input type=hidden name=idEmpresa>
 <input type=hidden name=cd_admin>
 <input type=hidden name=idCandidato>
 <input type=hidden name=NU_SOLICITACAO value='<?=$ultimaSol?>'>
</form>

<script language="javascript">
window.name = "WILSONS";

function fechar(){
    var msg = "Operacao cancelada.";
    var msgErro = "";
    var MyArgs = new Array(msg,msgErro);
    window.returnValue = MyArgs;
    window.close();
}

function doInit() {
    var MyArgs =  window.dialogArguments;
    document.will.idEmpresa.value = MyArgs[0].toString();
    document.will.idCandidato.value = MyArgs[1].toString();
    document.will.opvisa.value = MyArgs[2].toString();
    document.will.NU_SOLICITACAO.value = MyArgs[4].toString();
    document.will.cd_admin.value = MyArgs[5].toString();
    document.all.aguarde.style.display = 'block';
    document.will.submit();
}
function enviar() {
	if (confirm('Voce deseja atualizar as informacoes alteradas?'))
	{
		  var ret = "";
		  var tipo=document.visto.opvisa.value;
		  if(tipo == "PROCMTE") {
		     ret = ProcMTE();
		  } else if(tipo == "VISTO") {
		     ret = Visto();
		  } else if(tipo == "REGCIE") {
		     ret = RegCie();
		  } else if(tipo == "EMISCIE") {
		     ret = EmisCie(); 
		  } else if(tipo == "PRORR") {
		     ret = Prorrog();
		  } else if(tipo == "CANC") {
		     ret = Cancel();
		  } else {
		     ret = 'Funcao invalida: '+tipo;
		  }
		  if(ret == "") {
		      document.visto.submit();
		  } else {
		      alert(ret);
		  }
	}
	else
	{
		alert('Operacao cancelada.');
	}
}

function ProcMTE() {
   var ret = "";
   if(document.visto.cd_candidato.value=="") {
      ret=ret+"Nao foi informado o candidato.";
   }
   if(document.visto.cd_solicitacao.value=="") {
      ret=ret+"Nao foi informada a solicitacao.";
   }
   return ret;
}
function Visto() {
   var ret = "";
   if(document.visto.idCandidato.value=="") {
      ret=ret+"Nao foi informado o candidato.";
   }
   if(document.visto.DT_VALIDADE_VISTO.value=="") {
      ret=ret+"Nao foi informado a data de validade do visto.\n";
   }
   if(document.visto.NU_VISTO.value=="") {
      ret=ret+"Nao foi informado o numero do visto.\n";
   }
   if(document.visto.DT_EMISSAO_VISTO.value=="") {
      ret=ret+"Nao foi informado a data de emissao do visto.\n";
   }
   if(document.visto.NO_LOCAL_EMISSAO_VISTO.value=="") {
      ret=ret+"Nao foi informado o local da emissao do visto.\n";
   }
   return ret;
}
function RegCie() {
   var ret = "";
   if(document.visto.idCandidato.value=="") {
      ret=ret+"Nao foi informado o candidato.\n";
   }
   if(document.visto.NU_PROTOCOLO_CIE.value=="") {
      ret=ret+"Nao foi informado o numero do protocolo CIE.\n";
   }
   if(document.visto.DT_PROTOCOLO_CIE.value=="") {
      ret=ret+"Nao foi informado a data do protocolo CIE.\n";
   }
   if(document.visto.DT_VALIDADE_PROTOCOLO_CIE.value=="") {
      ret=ret+"Nao foi informado a data de validade do protocolo CIE.\n";
   }
   if(document.visto.DT_PRAZO_ESTADA.value=="") {
      ret=ret+"Nao foi informado o prazo de estada requerido.\n";
   }
   return ret;
}
function EmisCie() {
   var ret = "";
   if(document.visto.idCandidato.value=="") {
      ret=ret+"Nao foi informado o candidato.\n";
   }
   if(document.visto.NU_PROTOCOLO_CIE.value=="") {
      ret=ret+"Nao foi informado numero da CIE.\n";
   }
   if(document.visto.DT_EMISSAO_CIE.value=="") {
      ret=ret+"Nao foi informado a data de emissao da CIE.\n";
   }
   if(document.visto.DT_VALIDADE_CIE.value=="") {
      ret=ret+"Nao foi informado a data de validade da CIE.\n";
   }
   return ret;
}
function Prorrog() {
   var ret = "";
   if(document.visto.idCandidato.value=="") {
      ret=ret+"Nao foi informado o candidato.\n";
   }
   if(document.visto.NU_PROTOCOLO_PRORROGACAO.value=="") {
      ret=ret+"Nao foi informado o numero do protocolo de prorrogacao.\n";
   }
   if(document.visto.DT_PROTOCOLO_PRORROGACAO.value=="") {
      ret=ret+"Nao foi informado a data do protocolo de prorrogacao.\n";
   }
   if(document.visto.DT_VALIDADE_PROTOCOLO_PROR.value=="") {
      ret=ret+"Nao foi informado a data de validade do protocolo de prorrogacao.\n";
   }
   if(document.visto.DT_PRETENDIDA_PRORROGACAO.value=="") {
      ret=ret+"Nao foi informado a data pretendida na prorrogacao.\n";
   }
   return ret;
}
function Cancel() {
   var ret = "";
   if(document.visto.idCandidato.value=="") {
      ret=ret+"Nao foi informado o candidato.\n";
   }
   return ret;
}
function criaMascara(_RefObjeto, _Modelo){
    var valorAtual = _RefObjeto.value;        
    var valorNumerico = '';
    var nIndexModelo = 0;
    var nIndexString = 0;
    var valorFinal = '';
    var adicionarValor = true;
    for (i=0;i<_Modelo.length;i++){
      if (_Modelo.substr(i,1) != '#'){
          valorAtual = valorAtual.replace(_Modelo.substr(i,1),'');
      }
    }
    for (i=0;i<valorAtual.length;i++){
      if (!isNaN(parseFloat(valorAtual.substr(i,1)))) {
          valorNumerico = valorNumerico + valorAtual.substr(i,1);
      }
    }
    for (i=0;i<_Modelo.length;i++){
      if (_Modelo.substr(i,1) == '#'){
        if (valorNumerico.substr(nIndexModelo,1) != ''){
          valorFinal = valorFinal + valorNumerico.substr(nIndexModelo,1);
          nIndexModelo++;nIndexString++;
        } else {
          adicionarValor = false;
        }
      } else {
        if (adicionarValor && valorNumerico.substr(nIndexModelo,1) != ''){
           valorFinal = valorFinal + _Modelo.substr(nIndexString,1)
           nIndexString++;
        }
      }
    }
    _RefObjeto.value = valorFinal 
}
</script>
