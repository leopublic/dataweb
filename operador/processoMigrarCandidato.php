<?php

include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

 function exception_error_handler($errno, $errstr, $errfile, $errline ) {
 throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
 }
 set_error_handler("exception_error_handler");


$pNU_CANDIDATO = $_GET['NU_CANDIDATO'];
$pid_solicita_visto = $_GET['id_solicita_visto'];

$cand = new cCANDIDATO();
$cand->Recuperar($pNU_CANDIDATO);

$os = new cORDEMSERVICO();
$os->Recuperar($pid_solicita_visto);


$tela = cINTERFACE_CANDIDATO::ExibeSolicitacao($pid_solicita_visto, $pNU_CANDIDATO, $os->mNU_SOLICITACAO, $os->mID_TIPO_ACOMPANHAMENTO, false);

echo Topo(' - Migrar processo de '.$cand->mNOME_COMPLETO);

if(isset($_POST['NU_CANDIDATO'])){
	$pNU_CANDIDATO_novo =$_POST['NU_CANDIDATO'];
	$os->MigrarServico($pNU_CANDIDATO, $pNU_CANDIDATO_novo);
	$alert="jAlert('Processo migrado com sucesso!');";
}

?>
<div class="conteudo" >
	<div class="titulo"><div style="float: left">Migrar processos do candidato "<?=$cand->mNOME_COMPLETO;?>"&nbsp(<?=$cand->mNU_CANDIDATO;?>);</div></div>

	<br/>	
	<div style="padding:10px;">
		<p>Esse é o processo que está sendo migrado</p>
			<?=$tela;?>
		
		<br/>	
		<br/>	
		<p style="font-weight:bold;">Agora indique para qual candidato/processo você deseja migrar o processo acima:</p>
		<form method="post">
		<table class="edicao dupla">
			<tr>
				<th style="width: 50px" >Sel.</th>
				<th>Candidato</th>
				<th>Empresa e Projeto</th>
				<th>Processos existentes</th>
			</tr>
		<?php 
		
		$sql = "select NU_CANDIDATO, NOME_COMPLETO, NO_RAZAO_SOCIAL, NO_EMBARCACAO_PROJETO, NU_PROCESSO_MTE, NU_PROTOCOLO_REG, NU_PROTOCOLO_CIE, NU_PROTOCOLO_PRO  
				from vCANDIDATO_SITUACAO_ATUAL C
				left join EMPRESA E ON E.NU_EMPRESA = C.NU_EMPRESA
				left join EMBARCACAO_PROJETO P ON P.NU_EMPRESA = C.NU_EMPRESA and P.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO  
				where NOME_COMPLETO = '".$cand->mNOME_COMPLETO."'
				and NU_CANDIDATO <> ".$cand->mNU_CANDIDATO;
		$rs = mysql_query($sql);
		while($rw = mysql_fetch_array($rs)) {
		?>
			<tr>
				<td><input type="radio" id="NU_CANDIDATO" name="NU_CANDIDATO" value="<?=$rw['NU_CANDIDATO'];?>" style="width:auto;"/></td>
				<td><?=$rw['NOME_COMPLETO']?> (<?=$rw['NU_CANDIDATO'];?>)</td>
				<td><?=$rw['NO_RAZAO_SOCIAL']?>: <?=$rw['NO_EMBARCACAO_PROJETO'];?>)</td>
				<td>
					<table>
						<tr><td>Autorização</td><td><?=$rw['NU_PROCESSO_MTE'];?></td></tr>
						<tr><td>Registro</td><td><?=$rw['NU_PROTOCOLO_REG'];?></td></tr>
						<tr><td>Emissão</td><td><?=$rw['NU_PROTOCOLO_CIE'];?></td></tr>
						<tr><td>Prorrogação</td><td><?=$rw['NU_PROTOCOLO_PRO'];?></td></tr>
					</table>
				</td>
			</tr>
		<?php 
		}
		?>
		
		</table>
		</form>
		<center><button onclick="document.forms[0].submit();">Migrar</button></center>
	</div>
	
</div>
<script><?=$alert;?></script>
