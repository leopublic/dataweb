<?php

$opcao = "EMP";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
/*
function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler("exception_error_handler");
*/
$x = '';


ValorParametro($_SESSION, $_POST, 'NU_EMBARCACAO_PROJETO', $pNU_EMBARCACAO_PROJETO, "PESQ_CAND");

$fNome = new cFILTRO_TEXTO('Name', 'NOME_COMPLETO');
$fEmpresa = new cFILTRO_CHAVE('Empresa', cFILTRO_CHAVE::enEMPRESA, '','onChange="javascript:handleOnChange(this);" class="formulario"');

$fNome->ObterValorInformado($_POST, $_SESSION, "PESQ_CAND");
$qtdInformados = 0;
if($_SESSION['myAdmin']['cd_empresa'] > 0) {
  $cmbProjetos = '<option value="">(select...)</option>'.  montaComboProjetosAtivos($pNU_EMBARCACAO_PROJETO,$_SESSION['myAdmin']['cd_empresa'],"");
}

if($fNome->valorInformado() != ''){
	$qtdInformados++;
}
if($pNU_EMBARCACAO_PROJETO > 0){
	$qtdInformados++;
}

if (isset($_POST['ordenacao']) && trim($_POST['ordenacao'])!='' ){
	$ordenacao = $_POST['ordenacao'];
}
else{
	$ordenacao = 'NOME_COMPLETO';
}

$ativoNOME_COMPLETO='';
$ativoNO_EMBARCACAO_PROJETO='';
$ativoNO_MAE='';
$ativoNO_NACIONALIDADE='';
$ativoNU_PASSAPORTE='';
$ativoNU_CPF='';
$ativoDT_VALIDADE_PASSAPORTE_ORIG='';
$ativoDT_CADASTRAMENTO_ORIG='';
$ativoTIPO_VISTO='';
$ativoDT_PRAZO_ESTADA_ORIG='';

eval('$ativo'.$ordenacao." = '_ativo';");



$sql1 = "   select distinct
    C.NU_CANDIDATO,
	  C.NOME_COMPLETO,
	  C.NU_PASSAPORTE,
	  C.NU_RNE,
	  C.NU_CPF,
	  C.DT_VALIDADE_PASSAPORTE DT_VALIDADE_PASSAPORTE_ORIG ,
	  DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE ,
	  DATE_FORMAT(MAX_PRAZO_ESTADA.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA,
	  MAX_PRAZO_ESTADA.dt_prazo_estada DT_PRAZO_ESTADA_ORIG,
	  datediff(MAX_PRAZO_ESTADA.dt_prazo_estada, now()) DIAS_VENCIMENTO,
	  ifnull(QTD_PRORROG.qtd_prorrog, 0) QTD_PRORROG,

	  DATE_FORMAT(REG.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_CIE,
	  REG.nu_protocolo NU_PROTOCOLO_REG,

	  DATE_FORMAT(PRO.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_PRO,
	  PRO.nu_protocolo NU_PROTOCOLO_PRO,
	  DATE_FORMAT(PRO.dt_prazo_pret,'%d/%m/%Y')  DT_PRAZO_PRET_PRO,
	  sv.ID_STATUS_SOL,
	  sv.nu_solicitacao NU_SOLICITACAO_PRO,
	  SS.NO_STATUS_SOL,

	  MTE.nu_processo NU_PROCESSO_MTE,
	  MTE.prazo_solicitado PRAZO_SOLICITADO,
	  DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO ,
	  DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO,
	  MTE.dt_deferimento DT_DEFERIMENTO_ORIG,

	  DATE_FORMAT(COLETA.dt_entrada,'%d/%m/%Y')  DT_ENTRADA,
	  PN.NO_NACIONALIDADE,
	  F.NO_FUNCAO,
	  S.NO_SERVICO_RESUMIDO TIPO_VISTO,
	  RC.NO_REPARTICAO_CONSULAR CONSULADO,
	  EP.NO_EMBARCACAO_PROJETO
	from CANDIDATO C
	  left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual
		  and MTE.cd_candidato = C.NU_CANDIDATO
		  and MTE.fl_vazio = 0
	  left join (select cd_candidato, codigo_processo_mte, count(*) qtd_prorrog from processo_prorrog where codigo_processo_mte is not null group by cd_candidato, codigo_processo_mte) as QTD_PRORROG
		  on QTD_PRORROG.codigo_processo_mte = MTE.codigo
		  and QTD_PRORROG.cd_candidato = MTE.cd_candidato
	  left join (select cd_candidato, codigo_processo_mte, max(dt_prazo_estada) dt_prazo_estada from processo_regcie
				  where codigo_processo_mte is not null
				  group by cd_candidato, codigo_processo_mte) as MAX_PRAZO_ESTADA
		  on MAX_PRAZO_ESTADA.codigo_processo_mte = MTE.codigo
		  and MAX_PRAZO_ESTADA.cd_candidato = MTE.cd_candidato
	  left join processo_regcie REG on REG.dt_requerimento = (select max(dt_requerimento) from processo_regcie where codigo_processo_mte = MTE.codigo and cd_candidato = C.NU_CANDIDATO and fl_vazio =0 and dt_requerimento is not null)
		  and REG.codigo_processo_mte = MTE.codigo
		  and REG.cd_candidato = C.NU_CANDIDATO
		  and (REG.fl_vazio = 0 or REG.fl_vazio is null)
	  left join (select cd_candidato, codigo_processo_mte, max(dt_requerimento) dt_requerimento from processo_prorrog where codigo_processo_mte is not null group by cd_candidato, codigo_processo_mte) as MAX_PRORROG
		  on MAX_PRORROG.codigo_processo_mte = MTE.codigo
		  and MAX_PRORROG.cd_candidato = MTE.cd_candidato
		  and MAX_PRORROG.dt_requerimento > REG.dt_requerimento
	  left join processo_prorrog PRO
		  on PRO.codigo_processo_mte = MTE.codigo
		  and PRO.cd_candidato = MTE.cd_candidato
		  and PRO.dt_requerimento = MAX_PRORROG.dt_requerimento
	  left join solicita_visto sv on sv.id_solicita_visto = PRO.id_solicita_visto
	  left join processo_coleta COLETA on COLETA.codigo = (select max(codigo) from processo_coleta where COLETA.cd_candidato = C.NU_CANDIDATO
		  and COLETA.codigo_processo_mte = MTE.codigo)
	  left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
	  left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
	  left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
	  left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = MTE.cd_reparticao
	  left join EMBARCACAO_PROJETO EP on EP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = C.NU_EMPRESA
	  left join STATUS_SOLICITACAO SS ON SS.ID_STATUS_SOL = sv.ID_STATUS_SOL
	  where (C.FL_INATIVO = 0 or C.FL_INATIVO is null) ";
$where = "";
$where.=$fNome->Where();
if($_SESSION['myAdmin']['cd_empresa'] > 0){
	$where = " and C.NU_EMPRESA = ".$_SESSION['myAdmin']['cd_empresa'];
}
if(trim($fNome->valorInformado()) != ''){
	$where .= " and NOME_COMPLETO LIKE '%".$fNome->valorInformado()."%'";
}
if($pNU_EMBARCACAO_PROJETO > 0){
	$where .= " and C.NU_EMBARCACAO_PROJETO=$pNU_EMBARCACAO_PROJETO";
}
if (trim($where) != '' && isset($_POST['postou'])){
	$sql3 = " ORDER BY ".$ordenacao." asc, NOME_COMPLETO asc";
	$sql = $sql1.$where.$where.$sql3;
	$msg = '';
	if ($linhas = mysql_query($sql)){

	}
	else{
	  	print "<br><br><br><br>SQL=".$sql." Erro ".mysql_error();
	}
	if(mysql_errno()>0) {
	  	$msg = mysql_error().' SQL='.$sql;
	}
}
$pestiloAdicional = "<style>table.grid tr td a img {padding-left:2px;padding-right:2px;}</style>";

if ($_POST['excel']=='1'){
       $nome_arquivo = 'candidates_mundivisas';
       header('Content-type: application/vnd.ms-excel');
       header('Content-type: application/force-download');
       header('Content-Disposition: attachment; filename='.$nome_arquivo.'.xls');
       header('Pragma: no-cache');

       echo '
       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
       <html><head>
       <style rel="stylesheet" type="text/css"/>'.file_get_contents('../estilos.css').'
			table.grid {font:10px verdana;color:#000}
			table.grid thead tr th {border:solid 1px #000;font:10px verdana;color:#000;background-color:#c0c0c0;font-weight:bold;}
			table.grid thead tr.titulo th {text-align:left;border:solid 1px #000;font:10px verdana;color:#000;background-color:#ffffff;font-weight:bold;}
			table.grid tbody tr td {border:solid 1px #000;font:10px verdana;color:#000;text-align:left;}
			br { mso-data-placement:same-cell; }
		</style></head><body>';
}
else{
	echo Topo($title, $pestiloAdicional, false);
?>
	<div style="background-color:#fff;background-image:none;height:auto;padding-top:10px;padding-bottom:10px;text-align:left;padding-left:10px;"><img src="/imagens/logo_mundivisas_red.jpg" style="margin:0;padding:0;"/>
		<div style="padding-top:50px;padding-right:10px;float:right;vertical-align: bottom;"><?=$_SESSION['myAdmin']['nm_nome'];?></div>
	</div>
	<div class="conteudo" style="background-color:transparent;">
		<div class="titulo">
			<div style="float:right">
				<input type="button" value="Sair" onclick="javascript:window.location='/sair.php';"/>

			</div>
			<div style="float:left">Candidates - <?=$_SESSION['myAdmin']['nm_empresa'];?></div>&nbsp;
		</div>
		<div class="conteudoInterno">
			<div style="background-color:#dfe4ff;padding:0px;margin:0;">
				<form method="post" name="filtro">
					<input type="hidden" name="postou" id="postou" value="1"/>
					<input type="hidden" name="excel" id="excel" value="0"/>
					<table style="margin:0px;padding:10px;width:100%;border:solid 1px black;">
						<tr>
							<td nowrap>Ship/project:</td>
							<td ><div id="tdEmbarcacaoProjeto"><select name="NU_EMBARCACAO_PROJETO" class="formulario"><?=$cmbProjetos?></select></div></td>
							<td width="200px" rowspan="2" style="text-align:right;">
								<input type="button" class="textformtopo" value="Excel" onclick="javascript:this.form['excel'].value='1';this.form.submit();this.form['excel'].value = '0';">
								<input type="submit" class="textformtopo" value="Buscar">
							</td>
						</tr>
						<tr><td nowrap><?=$fNome->label();?>:</td><td><?=$fNome->campoHTML();?></td></tr>
					</table>
					<input type="hidden" name="ordenacao" id="ordenacao" value="<?=$ordenacao;?>"/>
				</form>
			</div>
			<br>
<?php
}
?>
<table class="grid">
	<thead>
 <tr>
<? if($_POST['excel']== '1'){ ?>
  <th class="esq">Name</th>
  <th class="esq">Ship/project</th>
  <th class="esq">Nationality</th>
  <th class="esq">CPF</th>
  <th>Passport #</th>
  <th>Passport expiry date</th>
  <th class="esq">Visa type</th>
  <th>Process granted</th>
  <th>Period of stay</th>
<? }else{ ?>
  <th class="cen">&nbsp;</th>
  <th class="esq">Name<img src="/imagens/icons/bullet_arrow_down<?=$ativoNOME_COMPLETO;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NOME_COMPLETO';document.forms[0].submit();"/></th>
  <th class="esq">Ship/project<img src="/imagens/icons/bullet_arrow_down<?=$ativoNO_EMBARCACAO_PROJETO;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NO_EMBARCACAO_PROJETO';document.forms[0].submit();"/></th>
  <th class="esq">Nationality<img src="/imagens/icons/bullet_arrow_down<?=$ativoNO_NACIONALIDADE;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NO_NACIONALIDADE';document.forms[0].submit();"/></th>
  <th class="esq">CPF<img src="/imagens/icons/bullet_arrow_down<?=$ativoNU_CPF;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NU_CPF';document.forms[0].submit();"/></th>
  <th>Passport #<img src="/imagens/icons/bullet_arrow_down<?=$ativoNU_PASSAPORTE;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='NU_PASSAPORTE';document.forms[0].submit();"/></th>
  <th>Passport expiry date<img src="/imagens/icons/bullet_arrow_down<?=$ativoDT_VALIDADE_PASSAPORTE_ORIG;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='DT_VALIDADE_PASSAPORTE_ORIG';document.forms[0].submit();"/></th>
  <th class="esq">Visa type<img src="/imagens/icons/bullet_arrow_down<?=$ativoTIPO_VISTO;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='TIPO_VISTO';document.forms[0].submit();"/></th>
  <th>Process granted<img src="/imagens/icons/bullet_arrow_down<?=$ativoDT_DEFERIMENTO_ORIG;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='DT_DEFERIMENTO_ORIG';document.forms[0].submit();"/></th>
  <th>Period of stay<img src="/imagens/icons/bullet_arrow_down<?=$ativoDT_PRAZO_ESTADA_ORIG;?>.png" style="border:0;margin:0;padding:0" onclick="javascript:document.forms[0].ordenacao.value='DT_PRAZO_ESTADA_ORIG';document.forms[0].submit();"/></th>
 <? }?>
</tr>
	</thead>
	<tbody>
<?php
	$status = '';
	$candidato = '';
	$solicit = '';
	$NO_USUARIO = '';
	$dataCad = '';
	$servico = '';
	$nomeProjetoEmbarcacao = '';
	$NO_USUARIO_TECNICO = '';
	if (isset($linhas)){
		while($rw1=mysql_fetch_array($linhas)) {
			echo '<tr height="20" onmouseover="this.style.backgroundColor=\'#dfe4ff\';" onmouseout="this.style.backgroundColor=\'#ffffff\';">'."\n";
			if($_POST['excel']!= '1'){
				$candidato = "<a href=\"/operador/detalheCandidatoCliente.php?NU_CANDIDATO=".$rw1['NU_CANDIDATO']."\" class='textoazulpeq'>".$rw1['NOME_COMPLETO']." (".$rw1['NU_CANDIDATO'].")</a>";
				$acoes  = '<a href="detalheCandidatoCliente_ID.php?NU_CANDIDATO='.$rw1['NU_CANDIDATO'].'" title="Clique para consultar os dados pessoais"><img src="../imagens/man.png"/></a>';
				$acoes .= '<a href="detalheCandidatoCliente_VISTO.php?NU_CANDIDATO='.$rw1['NU_CANDIDATO'].'" title="Clique para consultar o visto atual do candidato"><img src="../imagens/travel.png"/></a>';
				$acoes .= '<a href="detalheCandidatoCliente_ARQUIVOS.php?NU_CANDIDATO='.$rw1['NU_CANDIDATO'].'" title="Clique para consultar os anexos do candidato"><img src="../imagens/attach.png"/></a>';
				echo '<td align=left>'.$acoes.'</td>';
			}
			echo "<td align=left>".$rw1['NOME_COMPLETO']."</td>\n";
			echo "<td align=left>".$rw1['NO_EMBARCACAO_PROJETO']."</td>\n";
			echo "<td align=left>".$rw1['NO_NACIONALIDADE']."</td>\n";
			echo "<td align=left>".$rw1['NU_CPF']."</td>\n";
			echo "<td align=center>".$rw1['NU_PASSAPORTE']."</td>\n";
			echo "<td align=center>".$rw1['DT_VALIDADE_PASSAPORTE']."</td>\n";
			echo "<td align=left>".$rw1['TIPO_VISTO']."</td>\n";
			echo "<td align=center>".$rw1['DT_DEFERIMENTO']."</td>\n";
			echo "<td align=center>".$rw1['DT_PRAZO_ESTADA']."</td>\n";
			echo "</tr>\n";
		}
		echo "<input type=hidden name=totcand value='$x'>";
	}
	echo "</tbody>";
	echo "</table>";
	if (!isset($linhas)){
		print "Please select a ship or inform a part of the person name";
	}

?>
<p class=textoazul><b>
   <span id=aguarde class='textoazul'></span><br><span id=aguardeerr class='textoazul'></span>
</p>
</div>
</div>
<script>
	$(document).ready(
		function(){
			$("#tabsInt").tabs();
		}
	);
<?php
if ($msg!=''){
?>
jAlert('<?=$msg;?>');
<?php
}
?>
</script>
<?
echo Rodape("");
?>
