<?php

    require_once '../LIB/ControleEventos.php';
         
?>
<style>
	.TVVSGridBox {height:500px;overflow:scroll;}
	
</style>
<div class="titulo"> 
    Ordem de serviço - Cadastro Básico do(s) Candidato(s)  
    <?=$os->menu();?>
   
</div>
<div class="content">

  <div id="tabsOS" class="aba">
    <?=$os->aba('#DadosBasicos', basename($_SERVER['PHP_SELF']))?>
    <div id="DadosBasicos">
        <?if($isPost):?>
            <div class="success">Os dados foram atualizados com sucesso</div>
        <?endif;?> 
        <form method="post">
        <div class="boxer" style="width: auto;font:11px arial"> <img src="/imagens/icons/information.png" /> Cotação do dólar: <b>R$ <?=$cotacao?>. </b> (atualizada por <b><?=$cotacao_usuario?></b> em <?=$cotacao_data?>)</div>
        <table class="relatorio grade destaque">
            <thead>
                <th>Nome Completo</th><th>Função</th><th>Salário em US$</th><th>Salário Exterior R$</th><th>Consulado</th>
            </thead>    
            <tbody>     
               <?foreach($candidatos as $candidato): $id = '['.$candidato['NU_CANDIDATO'].']'; ?>
                  <tr>
                    <input type="hidden" name="candidato<?=$id?>" value="" />
                    <td><b><?=$candidato['NOME_COMPLETO'];?></b></td>
                    <td><?=$html->masterEdit('CO_FUNCAO_CANDIDATO'.$id, $action, $candidato['CO_FUNCAO_CANDIDATO'])?></td>
                    <td><?=$html->dbedit('SALARIO_DOLAR'.$id, $os->moeda($candidato['SALARIO_DOLAR']), array('class'=>'dolar'));?></td>
                    <td><?=$html->dbedit('VA_RENUMERACAO_MENSAL'.$id, $candidato['VA_RENUMERACAO_MENSAL'], array('class'=>'real', 'disabled'=>'true'));?></td>
                    <td><?=$html->masterEdit('CO_REPARTICAO_CONSULAR'.$id, $action_consulado, $candidato['CO_REPARTICAO_CONSULAR']);?>
                    </td>
                  </tr>
               <?endforeach;?>
            </tbody>
        </table>
        <p align="center"><br /><input type="submit" value="Salvar" /> <a class="btnlink" href="GeradorEmail.php?ID_SOLICITA_VISTO=<?=$html->g('ID_SOLICITA_VISTO')?>" ><img src="../imagens/icons/email_go.png" /> E-mail de confirmação</a></p>
        </form>
      </div>
  </div>

</div>
</body>
</html>
