<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa) == 0) {
  $idEmpresa = $_GET['idEmpresa'];
}
$idEmbarcacaoProjeto = $_POST['idEmbarcacaoProjeto'];
if(strlen($idEmbarcacaoProjeto) == 0) {
  $idEmbarcacaoProjeto = $_GET['idEmbarcacaoProjeto'];
}
$idCandidato = $_POST['idCandidato'];
if(strlen($idCandidato) == 0) {
  $idCandidato = $_GET['idCandidato'];
}
$NU_SOLICITACAO = $_POST['NU_SOLICITACAO'];
if(strlen($NU_SOLICITACAO) == 0) {
  $NU_SOLICITACAO = $_GET['NU_SOLICITACAO'];
}
$nmEmpresa = pegaNomeEmpresa($idEmpresa);

if($idCandidato>0) {
  $sql = "select a.NU_CANDIDATO,NO_PRIMEIRO_NOME,NO_NOME_MEIO,NO_ULTIMO_NOME,NU_EMBARCACAO_PROJETO,NU_SOLICITACAO ";
  $sql = $sql."from CANDIDATO a,AUTORIZACAO_CANDIDATO b where NU_EMPRESA=$idEmpresa and a.NU_CANDIDATO = b.NU_CANDIDATO ";
  if(strlen($NU_SOLICITACAO)>0) {
    $sql = $sql."and a.NU_CANDIDATO=$idCandidato AND b.NU_SOLICITACAO=$NU_SOLICITACAO ";
  } else {
    $sql = $sql."and a.NU_CANDIDATO=$idCandidato AND b.NU_SOLICITACAO in (SELECT max(NU_SOLICITACAO) FROM AUTORIZACAO_CANDIDATO b where NU_EMPRESA=$idEmpresa AND NU_CANDIDATO=$idCandidato) ";
  }
  echo "\n<!-- \nSQL=$sql\n -->\n";
  $rs = mysql_query($sql);
  if(mysql_errno()>0) {
     $msg = "<br><br>Erro ao consultar candidatos.<br><br>";
     echo "\n<!-- \nSQL=$sql\n -->\n";
  } else {
    if($rw1=mysql_fetch_array($rs)) {
      $nomeProjetoEmbarcacao = "";
      $idCandidato = $rw1['NU_CANDIDATO'];
      $primeiroNomeCandidato = $rw1['NO_PRIMEIRO_NOME'];
      $meioNomeCandidato = $rw1['NO_NOME_MEIO'];
      $ultimoNomeCandidato = $rw1['NO_ULTIMO_NOME'];
      $numProjeto = $rw1['NU_EMBARCACAO_PROJETO'];
      $NU_SOLICITACAO = $rw1['NU_SOLICITACAO'];
      if(strlen($numProjeto)>0) { $nomeProjetoEmbarcacao=pegaNomeProjeto($numProjeto,$idEmpresa); }
      $nomecompleto = $primeiroNomeCandidato." ".$meioNomeCandidato." ".$ultimoNomeCandidato;
    }
  }
} else {
  $msg = "N&atilde; foi informado o candidato.";
}

echo Topo("OPERADOR");

?>
 <br>
  <table border=0 width="100%">
   <tr>
    <td class="textobasico" align="center" valign="top">
     <p align="center" class="textoazul"><strong>:: Bem vindo a Intranet da Mundivisas ::</strong></p>
     Pagina da Opera&ccedil;&atilde;o.
     <font color=red><b><?=$msg?></font><br><br>
    </td>
   </tr>
  </table>

<center>

<table border=0 width="400" class="textoazul" cellspacing=0>
 <form action="operacao1.php" method="post" name=will>
 <input type=hidden name=fase value="1">
 <input type=hidden name=idEmpresa value='<?=$idEmpresa?>'>
 <input type=hidden name=idCandidato>
 <input type=hidden name=cd_admin value="<?=$usulogado?>">
 <input type=hidden name=NU_SOLICITACAO value="<?=$NU_SOLICITACAO?>">
 <tr>
  <td>Filtrar candidato:</td>
  <td><input type="text" name="nomeCandidato" value="" size="30"></td>
  <td></td>
 </tr>
 <tr>
  <td>Filtrar Embarca&ccedil;&atilde;o: </td>
  <td>
   <select name="idEmbarcacaoProjeto">
        <option value="">Selecione...</option>
        <?=$cmbProjetos?>
  </select>
  </td>
  <td>
   <input type="submit" class="textformtopo" style="<?=$estilo?>" value="Listar">
  </td>
 </tr>
 </form>
</table>

<br><br>
<table width=550 border=0 class="textoazul" cellspacing=0>
 <tr><td>Nome da Empresa:</td><td colspan=3><?=$nmEmpresa?></td></tr>
 <tr><td nowrap>Nome da Embarca&ccedil;&atilde;o:</td><td colspan=3><?=$nomeProjetoEmbarcacao?></td></tr>
 <tr><td>Nome do Candidato:</td><td colspan=3><?=$nomecompleto?></td></tr>
 <tr><td>Solicita&ccedil;&atilde;o:</td><td colspan=3><?=$NU_SOLICITACAO?></td></tr>
 <tr><td colspan=4>&#160;</td></tr>
 
 <tr>
<?php if ( ($usurest=="N") || ($usurest=="B") ) { ?>
  <td align=center><input onclick="javascript:enviar('PROCMTE');" type=button value='Processo MTE' style="<?=$estilo?>"></td>
<?php } ?>
<?php if ($usurest=="N") { ?>
  <td align=center><input onclick="javascript:enviar('VISTO');" type=button value='Dados do Visto' style="<?=$estilo?>"></td>
<?php } ?>
<?php if ( ($usurest=="N") || ($usurest=="B") ) { ?>
  <td align=center><input onclick="javascript:enviar('REGCIE');" type=button value='Registro CIE' style="<?=$estilo?>"></td>
<?php } ?>
<?php if ($usurest=="N") { ?>
  <td align=center><input onclick="javascript:enviar('EMISCIE');" type=button value='Emissao CIE' style="<?=$estilo?>"></td>
<?php } ?>
<?php if ( ($usurest=="N") || ($usurest=="B") ) { ?>
  <td align=center><input onclick="javascript:enviar('PRORR');" type=button value='Prorroga&ccedil;&atilde;o' style="<?=$estilo?>"></td>
<?php } ?>
 </tr> 
 <tr><td colspan=5>&#160;</td></tr>

<?php if ($usurest=="N") { ?>
 <tr>
  <td colspan=2 align=center><input onclick="javascript:arquivos('ARQ');" type=button value='Arquivos' style="<?=$estilo?>"></td>
  <td colspan=3 nowrap align=center>
  <input onclick="javascript:formulario();" type=button value='Formulario de Registro na PF' style="<?=$estilo?>"></td>
 </tr> 
<?php } ?>

</table>
<br>
<p class=textoazul><b>
   <span id=aguarde class='textoazul'></span>
   <br><br>
   <span id=aguardeerr class='textoazul'></span>
</p>

<?php
echo Rodape("");
?>

<form name=wilson method=post>
 <input type=hidden name=opvisa>
 <input type=hidden name='idCandidato' value='<?=$idCandidato?>'>
 <input type=hidden name='idEmpresa' value='<?=$idEmpresa?>'>
 <input type=hidden name=cd_admin value="<?=$usulogado?>">
 <input type=hidden name=NU_SOLICITACAO value="<?=$NU_SOLICITACAO?>">
</form>

<script language="javascript">
function enviar(tipo) {
  var cand = document.wilson.idCandidato.value;
  var emp = document.wilson.idEmpresa.value;
  var cd_admin = document.wilson.cd_admin.value;
  var NU_SOLICITACAO = document.wilson.NU_SOLICITACAO.value;
  var pagina = "operacao3.php?idCandidato="+cand+"&idEmpresa="+emp+"&opvisa="+tipo+"&NU_SOLICITACAO="+NU_SOLICITACAO+"&cd_admin="+cd_admin+"&A=";
//alert("pagina="+pagina);
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
  document.wilson.action="operacao3.php";
//  window.open(pagina);
  var msg = OpenChild(tipo,"operacao3.php");
  document.all.aguarde.innerHTML = msg;
}

function arquivos(tipo) {
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
//  var msg = OpenChild(tipo,"operacao3up.php");
  var msg = OpenChild(tipo,"/admin/documentos1.php");
  document.all.aguarde.innerHTML = msg;
}

function formulario(tipo) {
  document.wilson.action="/formularios/all_registroPF.php";
  document.wilson.target="formPF";
  document.wilson.submit();
}

function OpenChild(tipo,pagina) {
    var ret = '';
    var cand = '<?=$idCandidato?>';
    var emp = '<?=$idEmpresa?>';
    var proj = '<?=$numProjeto?>';
    var proj = '<?=$numProjeto?>';
    var sol = '<?=$NU_SOLICITACAO?>';
    var admin = '<?=$usulogado?>';
    var MyArgs = new Array(emp,cand,tipo,proj,sol,admin);
    var WinSettings = "center:yes;resizable:yes;dialogHeight:450px;dialogWidth=780px"
    var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
    if (MyArgsRet != null) {
        var msg = MyArgsRet[0].toString();
        var erro = MyArgsRet[1].toString();
        if(erro.length > 0) {
            document.all.aguarde.innerHTML = "<!--"+erro+"-->";
            document.all.aguardeerr.innerHTML = "<!--"+msg+"-->";
        }
    } else {
        document.all.aguardeerr.innerHTML = "<!-- retorno nulo, provavelmente cancelou a operação -->";
        msg = "Houve um erro ao executar o pedido, favor tentar mais tarde.";
    }
    return msg;
}
</script>
