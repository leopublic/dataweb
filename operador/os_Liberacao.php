<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/classes/cORDEMSERVICO.php");

/*
function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler("exception_error_handler");
*/ 

 Acesso::SetBloqueio(Acesso::tp_Os_Aba_Liberacao);

$mensagem='';

$OS_i = new cINTERFACE_OS();

$mensagem .= $OS_i->IdInformado($_POST, $_GET);

if($OS_i->mID_SOLICITA_VISTO >0 ){
	$mensagem .= $OS_i->ProcessaPost($_POST);
}

$voltar = $_SESSION['voltar'];
if ($voltar == ''){
	$voltar = "os_listar.php";
}
//###################################################################
// Inicio do documento
//###################################################################
echo Topo();
echo Menu("SOL");
?>
<div class="conteudo">
<?	print $OS_i->FormCabecalhoOS(); ?>
	<div class="conteudoInterno">				
		<div id="tabsOS" class="aba">
			<?php 
			$OS_i->RecuperarCandidatos();
			print $OS_i->aba("Liberacao", '/operador/os_Liberacao.php');
			?>
			<div id="Liberacao"></div>
			<?
			if($OS_i->mID_SOLICITA_VISTO <> 0){
				print $OS_i->AbaLiberacao();
        	}
			?>
			</div>
		</div>
	</div>
</div>
<?php
echo Rodape("");

if (isset($_SESSION['msg']) && $_SESSION['msg']!=''){
	$mensagem.="\njAlert('".$_SESSION['msg']."');";
	$_SESSION['msg']=''; 
}
?>
<script language="javascript">
	$(document).ready(
		function() {
		    <?=$mensagem;?>
		}
	);
</script>
