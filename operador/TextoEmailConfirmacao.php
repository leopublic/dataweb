<?php

    require_once '../LIB/ControleEventos.php';
        
    $html = new html;

?>
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/js/ckeditor/adapters/jquery.js"></script>
<script>
$(document).ready(function(){
    $('textarea[name="valor"]').ckeditor(function() { /* callback code */ }, { skin : 'v2', extraPlugins : 'autogrow' });
})
</script>
<div class="titulo"> 
    Configuração - Texto do E-mail de Confirmação </div>
</div>
<?
    if( $isPost){
        if($saved){
            echo '<div class="success">Salvo com sucesso!</div>';
        }else{
            echo '<div class="error">Houve um problema ao salvar</div>';
        }
    }

?>
<div style="padding: 15px;">
<form method="post">
<textarea name="valor"><?=$valor?></textarea>
<br />
<input type="submit" value="Salvar texto de confirmação" />
</form>
    

</div>
</body>
</html>
