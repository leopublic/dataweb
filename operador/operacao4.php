<?php
include ("../LIB/conecta.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/libOperacoes.php");
include ("../LIB/classes/classe_proc_cancel.php");
include ("../LIB/classes/classe_proc_emiscie.php");
include ("../LIB/classes/classe_proc_mte.php");
include ("../LIB/classes/classe_proc_prorrog.php");
include ("../LIB/classes/classe_proc_regcie.php");

$opvisa = trim($_POST['opvisa']);
$fase = 0+$_POST['fase'];
$idEmpresa = 0+$_POST['idEmpresa'];
$idCandidato = 0+$_POST['idCandidato'];
$NU_SOLICITACAO = 0+$_POST['NU_SOLICITACAO'];
$acao = "A";
$cd_admin = $_REQUEST['cd_admin'];
if(strlen($usulogado)==0) {
  $usulogado = $cd_admin;
}

if (  ($idCandidato>0) && (strlen($opvisa)>0)  )  {
  if($opvisa=="VISTO") {
    $msgErro = GravaVisto();
    insereEvento($idEmpresa,$idCandidato,$NU_SOLICITACAO,"Alteração do cadastro de Visto do candidato.");
  } else {
    if($opvisa == "PROCMTE") {
      $obj = new proc_mte();
    } else if($opvisa == "REGCIE") {
      $obj = new proc_regcie();
    } else if($opvisa == "EMISCIE") {
      $obj = new proc_emiscie();
    } else if($opvisa == "PRORR") {
      $obj = new proc_prorrog();
    } else if($opvisa == "CANC") {
      $obj = new proc_cancel();
    }
    $obj->LeituraRequest($_POST);
    $todossol = $_POST['todossol'];
    if($todossol=="y") {
      $rs = $obj->ListaProcessos(" WHERE cd_solicitacao=".$obj->cd_solicitacao);
      while($rw=mysql_fetch_array($rs)) {
        $cand = $rw['cd_candidato'];
        $msgErro=ExecutaAlteracoes($obj,$cand);
      }
    } else {
      $cand = $obj->cd_candidato;
      $msgErro=ExecutaAlteracoes($obj,$cand);
    }
  }
  if($msgErro == "") {
     $msg = "Alteração executada com sucesso!";
  } else {
     $msg = "Alteração não foi executada.<br><br><font color=red>$msgErro</font>";
  }
} else {
  $msg = "N&atilde; foi informado o candidato ou função inválida.";
  $msgErro = "idEmpresa=$idEmpresa e idCandidato=$idCandidato e opvisa=$opvisa";
}

function ExecutaAlteracoes($obj,$cand) {
  global $opvisa,$idEmpresa,$extra;
  $obj->LeituraRequest($_POST);
  $obj->cd_candidato=$cand;
  $sol = $obj->cd_solicitacao;
  if($obj->ExisteCandidato($cand,$sol)==true) {
    $obj->AlteraProcesso();
    $obj->insereEvento($idEmpresa,"Alteração");
  } else {
    $obj->InsereProcesso();
    $obj->insereEvento($idEmpresa,"Inclusão");
  }
  $obj->AlteraVisaDetail();
  if($opvisa == "PROCMTE") {
    $obj->AcompanhaProcesso($idEmpresa);
  }
  $msgErro = $obj->myerr;
  return $msgErro;
}

?>
<html>
<body onload="javascript:Done();">
<br><br>
<p align=center><font color=red>Aguarde !!!</font>
<script language="javascript">
window.name = "WILSONS";
function Done() {
    var msg = "<?=$msg?>";
    var msgErro = "<?=$msgErro?>";
    var MyArgs = new Array(msg,msgErro);
    window.returnValue = MyArgs;
    window.close();
}
</script>

