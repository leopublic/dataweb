<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/classes/cORDEMSERVICO.php");

/*
function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler("exception_error_handler");
*/ 



 Acesso::SetBloqueio(Acesso::tp_Os_Aba_Processo);

$tabsOS_selected = '';

$mensagem='';

/*
 * Processa o form enviado se houver
 */
$msg = cINTERFACE::ProcessePost($_POST, $_GET, $_FILES, $_SESSION);

/*
 * Obter parametros
 */
$OS_i = new cINTERFACE_OS();
$OS_i->ObterParametros($_POST, $_GET);

if (isset($_GET['NU_CANDIDATO'])){
	$pNU_CANDIDATO = $_GET['NU_CANDIDATO'];
}else{
	if (isset($_POST['NU_CANDIDATO'])){
		$pNU_CANDIDATO = $_POST['NU_CANDIDATO'];
	}
}


$js = '';
$tabs = '';
$tabsF = '';
$divs = '';
$divsF = '';
//$ids = explode(",",$OrdemServico->mCandidatos);
$voltar = $_SESSION['voltar'];
if ($voltar == ''){
	$voltar = "os_listar.php";
}
//###################################################################
// Inicio do documento
//###################################################################
echo Topo();
echo Menu("SOL");
?>
<div class="conteudo">
<? 	echo $OS_i->FormCabecalhoOS();?>
	<div class="conteudoInterno">				
		<div id="tabsOS" class="aba">
<?
echo $OS_i->aba("Processo", basename($_SERVER['PHP_SELF']));?>
			<div id="Processo" style="padding-right:10px;padding-left:10px;">	
<?php
//########################################################################
// Página PROCESSO
//########################################################################
			echo $OS_i->FormProcesso();
?>
		</div>
<?php
if ($OS_i->mID_TIPO_ACOMPANHAMENTO == 3
 || $OS_i->mID_TIPO_ACOMPANHAMENTO == 4
 || $OS_i->mID_TIPO_ACOMPANHAMENTO == 5
 || $OS_i->mID_TIPO_ACOMPANHAMENTO == 6
 || $OS_i->mID_TIPO_ACOMPANHAMENTO == 8){
			
?>
	<br />
	<div id="historico" class="ui-widget-content" style="padding:2px;">
		<div id="historicoHeader" class="ui-widget ui-widget-header" style="padding:9px;">Histórico do visto</div>
		<div style="padding:15px;">
		<?php print  cINTERFACE_CANDIDATO::FormHistoricoVistoDeOs($OS_i->mID_SOLICITA_VISTO);   ?>
			
		</div>
	</div>
	<div id="formPopup" name="formPopup" style="display:none">
<?php
}
	 
echo Rodape("");

if ($tabOS_selected == ''){
	$tabOS_selected = 0;
}

if (isset($_SESSION['msg']) && $_SESSION['msg']!=''){
	$mensagem.="\njAlert('".$_SESSION['msg']."');";
	$_SESSION['msg']=''; 
}
if ($msg != "" ){
	$mensagem.="\njAlert('".$msg."', 'Atenção');";			
}
?>
<script language="javascript">

var NU_EMPRESA_ANT;
$(document).ready(function() {
    
    <?=$js;?>
    
    <?=$mensagem;?>
    $("#historico").corner('round 5px');
    $("#historicoHeader").corner('round 5px');
  });

</script>
