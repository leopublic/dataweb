<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

/*
  function exception_error_handler($errno, $errstr, $errfile, $errline ) {
  throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
  }
  set_error_handler("exception_error_handler");
 */
cHTTP::$comCabecalhoFixo = false;
Acesso::SetBloqueio(Acesso::tp_Os_Aba_Cadastro);

// Testa se houve post de algum popup
if (isset($_POST['CMP___nomeTemplate'])) {
    
}

$tabsOS_selected = '';

$mensagem = '';
if (isset($_GET['ID_SOLICITA_VISTO'])) {
    $pID_SOLICITA_VISTO = $_GET['ID_SOLICITA_VISTO'];
} else {
    $pID_SOLICITA_VISTO = $_POST['ID_SOLICITA_VISTO'];
}

if (isset($_GET['NU_CANDIDATO'])) {
    $pNU_CANDIDATO = $_GET['NU_CANDIDATO'];
} else {
    $pNU_CANDIDATO = $_POST['NU_CANDIDATO'];
}


$OrdemServico = new cORDEMSERVICO();
$OS_i = new cINTERFACE_OS();

if ($pID_SOLICITA_VISTO > 0) {
    $OrdemServico->Recuperar($pID_SOLICITA_VISTO);
    $OrdemServico->RecuperarCandidatos($pID_SOLICITA_VISTO);
    $arrayIds = explode(",", $OrdemServico->mCandidatos);
    $OS_i->Recuperar($pID_SOLICITA_VISTO);
    $OS_i->RecuperarCandidatos();
} else {
    $OrdemServico->mID_STATUS_SOL = 0;
    $OrdemServico->mID_SOLICITA_VISTO = 0;
    $OS_i->mID_SOLICITA_VISTO = 0;
}
//
// PAINEL CADASTRO
//
// Trata clique no botï¿½o "Concluir"  
if (isset($_POST['formCadastro'])) {
    $tabOS_selected = 2;
    /*
      if($_POST['acao'] == 'concluir'){
      $status = '3';
      $xmsg = "OS concluï¿½da com sucesso";
      }
      else{
      $status = '4';
      $xmsg = "";
      }
     */
    $status = '4';
    $sql = "update solicita_visto set ID_STATUS_SOL = " . $status . " where id_solicita_visto = " . $OrdemServico->mID_SOLICITA_VISTO;
    mysql_query($sql);
    $OrdemServico->mID_STATUS_SOL = $status;
    $mensagem = "jAlert('OS concluída com sucesso', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
}
//
// Nï¿½o conformidade
if (isset($_POST['naoConformidade'])) {
    if ($OrdemServico->mfl_nao_conformidade) {
        $fl_nao_conformidade = 0;
    } else {
        $fl_nao_conformidade = 1;
    }
    $sql = "update solicita_visto set fl_nao_conformidade = " . $fl_nao_conformidade . " where id_solicita_visto = " . $OrdemServico->mID_SOLICITA_VISTO;
    mysql_query($sql);
    $OrdemServico->mfl_nao_conformidade = $fl_nao_conformidade;
}

if ($OrdemServico->mfl_nao_conformidade) {
    $labelNC = "Suspender não conformidade";
    $estiloNC = "NC_ON";
} else {
    $labelNC = "Indicar não conformidade";
    $estiloNC = "NC_OFF";
}


$js = '';
$tabs = '';
$tabsF = '';
$divs = '';
$divsF = '';
//print '<br/>Candidatos:'.$OrdemServico->mCandidatos;
$ids = explode(",", $OrdemServico->mCandidatos);
$nomeTela = "AUTORIZACAO_CANDIDATO_OS";
if ($OrdemServico->mReadonly) {
    $nomeTela .= '_view';
}
$acCand = new cEDICAO($nomeTela);
$acCand->CarregarConfiguracao();
$tabsIntx_selected = 0;
// Verifica se o form foi enviado para salvar
if (isset($_POST[cINTERFACE::PREFIXO_CAMPOS . $acCand->mCampoCtrlEnvio->mNome])) {
    $tabOS_selected = 2;
    $tabsIntx_selected = $_POST['tabsIntx_selected'];
    cINTERFACE::RecupereValoresEdicao($acCand, $_POST);
    try {
        $acCand->mCampoNome['NU_USUARIO_CAD']->mValor = cSESSAO::$mcd_usuario;
        $acCand->mCampoNome['NU_USUARIO_CAD']->mReadonly = false;
        $acCand->mCampoNome['NU_USUARIO_CAD']->mVisivel = true;
        $acCand->AtualizarBanco();
        $acCand->mCampoNome['NU_USUARIO_CAD']->mReadonly = true;
        $acCand->mCampoNome['NU_USUARIO_CAD']->mVisivel = false;
        if (trim($acCand->mCampoNome['TE_DESCRICAO_ATIVIDADES']->mValor) == '') {
            $sql = "update AUTORIZACAO_CANDIDATO SET TE_DESCRICAO_ATIVIDADES = (select TX_DESCRICAO_ATIVIDADES from FUNCAO_CARGO where CO_FUNCAO = AUTORIZACAO_CANDIDATO.CO_FUNCAO_CANDIDATO)";
            $sql .= " where id_solicita_visto = " . $acCand->mCampoNome['id_solicita_visto']->mValor;
            $sql .= " and NU_CANDIDATO = " . $acCand->mCampoNome['NU_CANDIDATO']->mValor;
            mysql_query($sql);
        }
        $mensagem = "jAlert('Cadastro atualizado com sucesso', 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
        $cand = new cCANDIDATO();
        $cand->mNU_CANDIDATO = $acCand->mCampoNome['NU_CANDIDATO']->mValor;
        $cand->SincronizeSatelites($acCand->mCampoNome['id_solicita_visto']->mValor);
    } catch (Exception $exc) {
        error_log($exc->getMessage() . $exc->getTraceAsString());
        $mensagem = "jAlert('Não foi possível atualizar.<br/>" . addslashes(str_replace("\r\n", "<br/>", $exc->getMessage())) . "' , 'Ordem de Serviço " . $OrdemServico->mNU_SOLICITACAO . "');";
    }
}

if ($OrdemServico->mNU_SOLICITACAO > 0) {
    for ($i = 0; $i < count($ids); $i++) {
        if ($selecionada == '') {
            $selecionada = $ids[$i];
        }
        $acCand->mCampoNome['id_solicita_visto']->mValor = $OrdemServico->mID_SOLICITA_VISTO;
        $acCand->mCampoNome['NU_CANDIDATO']->mValor = $ids[$i];
        $acCand->RecupereRegistro();

        $tabs .= '<li><a href="#cand' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . $acCand->mCampoNome['NOME_COMPLETO']->mValor . '</a></li>';
        $divs .= '<div id="cand' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
        $divs .= '<form id="form' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" method="post">' . "\n";
        //$divs .= '<input type="hidden" id="form" name="IdentificadorCampos" value="'.$acCand->mCampoNome['NU_CANDIDATO']->mValor.'"/>';
        $divs .= cINTERFACE::RenderizeCampo($acCand->mCampoCtrlEnvio);
        $divs .= cINTERFACE::inputHidden('CMP_' . $acCand->mCampoNome["id_solicita_visto"]->mCampoBD, $acCand->mCampoNome["id_solicita_visto"]->mValor);
        $divs .= cINTERFACE::inputHidden('CMP_' . $acCand->mCampoNome["NU_CANDIDATO"]->mCampoBD, $acCand->mCampoNome["NU_CANDIDATO"]->mValor);
        $divs .= cINTERFACE::inputHidden('tabsIntx_selected', $i);

        $divs .= cINTERFACE::RenderizeEdicao($acCand);
        $divs .= '</table>' . "\n";
        if (!$OrdemServico->mReadonly) {

            if (Acesso::permitido(Acesso::tp_Os_Aba_Cadastro_Salvar)) {
                $disable = '';
            } else {
                $disable = ' disabled="true" onclick="jAlert("Você não tem acesso para salvar");" ';
            }

            //$divs .= '<center><input type="submit" '.$disable.' value="Salvar" style="width:auto"/></center>'."\n";
            $divs .= '<div class="barraBotoesRodape"><button ' . $disable . '>Salvar</button></div>' . "\n";
        }
        $divs .= '</form>' . "\n";
//		   	$divs .= '<div class="duplasubTitulo">Dependentes</div> '."\n";
//			$divs .= '<div>';
//		   /*	$divs .= '<form method="post" action="/intranet/geral/dependentes.php">';
//			$divs .= '<input type="hidden" name="idCandidato" value="'.$acCand->mCampoNome['NU_CANDIDATO']->mValor.'"/>';
//			$divs .= '<input type="hidden" name="idEmpresa" value="'.$acCand->mCampoNome['NU_EMPRESA']->mValor.'"/>';
//			$divs .= '<input type="submit" value="Admnistrar dependentes"/>';
//			$divs .= '</form>'; */
//
//            // Grid Depedentes
//            // $divs .=  AcessoHtml ::Os_Aba_Cadastro_Dependentes('Dependentes', $acCand->mCampoNome["NU_CANDIDATO"]->mValor, $acCand->mCampoNome["id_solicita_visto"]->mValor  );
//            $ctrl = new cCTRL_DEPENDENTE_LIST();
//            cHTTP::$GET['NU_CANDIDATO'] = $ids[$i];
//			$divs .= $ctrl->Liste();
//
//
//
//			$divs .= '</div>';

        $divs .= '<div style="clear:both;overflow: auto;">' . "\n";
        $divs .= '<div class="duplasubTitulo">Arquivos do candidato</div>' . "\n";
        $divs .= '<div id="arquivosListadosVisualizacao' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" style="float:right;border:solid 1px #999999;padding:3px;text-align:center;">' . "\n";
        $divs .= '</div>' . "\n";
        $divs .= '<div>' . "\n";
        $divs .= '	<form id="novoArquivo' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" action="arquivoPost.php" enctype="multipart/form-data" target="_blank" method="post">' . "\n";
        $divs .= cINTERFACE::inputHidden("formArquivo", "1");
        $divs .= cINTERFACE::inputHidden("NU_CANDIDATO", $acCand->mCampoNome['NU_CANDIDATO']->mValor);
        $divs .= cINTERFACE::inputHidden("NU_EMPRESA", $acCand->mCampoNome['NU_EMPRESA']->mValor);
        $divs .= '	<div id="arquivosListados' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
        $divs .= '	</div>' . "\n";
        $divs .= '	</form>' . "\n";
        $divs .= '</div>' . "\n";

        $divs .= '</div><br/>' . "\n";

        $divs .= '</div>';
        $js .= "	CarregarListaDeArquivos('" . $acCand->mCampoNome['NU_CANDIDATO']->mValor . "','arquivosListados');\n";
    }
} else {
    $i = 0;
    $selecionada = $pNU_CANDIDATO;
    $acCand->mCampoNome['id_solicita_visto']->mValor = $OrdemServico->mID_SOLICITA_VISTO;
    $acCand->mCampoNome['NU_CANDIDATO']->mValor = $pNU_CANDIDATO;
    $acCand->RecupereRegistro();
    //error_log ('Recuperou candidato:'.$pNU_CANDIDATO);
    //error_log ('NU_CANDIDATO:'.$acCand->mCampoNome["NU_CANDIDATO"]->mValor);

    $tabs .= '<li><a href="#cand' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . $acCand->mCampoNome['NOME_COMPLETO']->mValor . '</a></li>';
    $divs .= '<div id="cand' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
    $divs .= '<form id="form' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" method="post">' . "\n";
    //$divs .= '<input type="hidden" id="form" name="IdentificadorCampos" value="'.$acCand->mCampoNome['NU_CANDIDATO']->mValor.'"/>';
    $divs .= cINTERFACE::RenderizeCampo($acCand->mCampoCtrlEnvio);
    $divs .= cINTERFACE::inputHidden('CMP_' . $acCand->mCampoNome["id_solicita_visto"]->mCampoBD, $acCand->mCampoNome["id_solicita_visto"]->mValor);
    $divs .= cINTERFACE::inputHidden('CMP_' . $acCand->mCampoNome["NU_CANDIDATO"]->mCampoBD, $acCand->mCampoNome["NU_CANDIDATO"]->mValor);
    $divs .= cINTERFACE::inputHidden('tabsIntx_selected', $i);

    $divs .= cINTERFACE::RenderizeEdicao($acCand);
    $divs .= '</table>' . "\n";
    if (!$OrdemServico->mReadonly) {
        $divs .= '<center><input type="submit" value="Salvar" style="width:auto"/></center>' . "\n";
    }
    $divs .= '</form>' . "\n";
//		$divs .= '<div class="duplasubTitulo">Dependentes</div>'."\n";
//		$divs .= '<div>';
//	  /*	$divs .= '<form method="post" action="/intranet/geral/dependentes.php">';
//		$divs .= '<input type="hidden" name="idCandidato" value="'.$acCand->mCampoNome['NU_CANDIDATO']->mValor.'"/>';
//		$divs .= '<input type="hidden" name="idEmpresa" value="'.$acCand->mCampoNome['NU_EMPRESA']->mValor.'"/>';
//		$divs .= '<input type="submit" value="Administrar dependentes"/>';
//		$divs .= '</form>';
//      */
//
//  
//        // Grid Depedentes 2
//        // $divs .=  AcessoHtml::Os_Aba_Cadastro_Dependentes('Dependentes2', $acCand->mCampoNome["NU_CANDIDATO"]->mValor, $acCand->mCampoNome["id_solicita_visto"]->mValor );
//		$ctrl_dep = new cCTRL_DEPENDENTE_LIST();
//		$div.= $ctrl_dep->Liste();
//        
//		$divs .= '</div>';

    $divs .= '<div style="clear:both;overflow: auto;">' . "\n";
    $divs .= '<div class="duplasubTitulo">Arquivos do candidato</div>' . "\n";
    $divs .= '<div id="arquivosListadosVisualizacao' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" style="float:right;border:solid 1px #999999;padding:3px;text-align:center;">' . "\n";
    $divs .= '</div>' . "\n";
    $divs .= '<div>' . "\n";
    $divs .= '	<form id="novoArquivo' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '" action="arquivoPost.php" enctype="multipart/form-data" target="_blank" method="post">' . "\n";
    $divs .= cINTERFACE::inputHidden("formArquivo", "1");
    $divs .= cINTERFACE::inputHidden("NU_CANDIDATO", $acCand->mCampoNome['NU_CANDIDATO']->mValor);
    $divs .= cINTERFACE::inputHidden("NU_EMPRESA", $acCand->mCampoNome['NU_EMPRESA']->mValor);
    $divs .= '	<div id="arquivosListados' . $acCand->mCampoNome['NU_CANDIDATO']->mValor . '">' . "\n";
    $divs .= '	</div>' . "\n";
    $divs .= '	</form>' . "\n";
    $divs .= '</div>' . "\n";

    $divs .= '</div><br/>' . "\n";

    $divs .= '</div>';
    $js .= "	CarregarListaDeArquivos('" . $acCand->mCampoNome['NU_CANDIDATO']->mValor . "','arquivosListados');\n";
}
$tabs = '<ul>' . $tabs . '</ul>' . "\n";
$voltar = $_SESSION['voltar'];
if ($voltar == '') {
    $voltar = "os_listar.php";
}
//###################################################################
// Inicio do documento
//###################################################################
echo Topo();
echo Menu("SOL");
?>
<div class="conteudo">
    <div class="titulo">
        <div style="float:right;margin-left:10px;margin-top:4px;">
<?php
if ($pID_SOLICITA_VISTO > 0) {
    echo menuBarOS($usulogado, $OrdemServico->mID_SOLICITA_VISTO, $OrdemServico->mNU_SERVICO, $estiloNC, $labelNC, $OrdemServico->mReadonly);
}
?>
        </div>
            <?= $acao; ?>
        <div class="texto">Ordem de serviço <?= $OrdemServico->mNU_SOLICITACAO; ?>&nbsp;&nbsp;(<?= $OrdemServico->mNO_STATUS_SOL; ?>)</div>
    </div>
    <!--
            <div style="border-bottom:solid 1px #cccccc;padding-bottom:1px;padding-left:1px;color:#cccccc"><a href="../intranet/geral/<?= $voltar; ?>" style="color:#999999;text-decoration:underline">voltar para listagem</a></div>
    -->
    <div class="conteudoInterno">				
        <div id="tabsOS" class="aba">
        <?php
        $x = $OS_i->aba("Cadastro", '/operador/os_Cadastro.php');
        print $x;
        ?>
            <div id="Cadastro">
<?php
//###################################################################
// Cadastro
//###################################################################
print '<div id="tabsIntx">' . "\n";
print $tabs . "\n";
print $divs . "\n";
print '</div>' . "\n";
print '<form method="post">' . "\n";
print cINTERFACE::inputHidden('formCadastro', '1');
print cINTERFACE::inputHidden('nuCand', $acCand->mCampoNome['NU_CANDIDATO']->mValor);
print cINTERFACE::inputHidden('id_solicita_visto', $OrdemServico->mID_SOLICITA_VISTO);
print '<center>' . "\n";
/*
  if ($OrdemServico->mID_STATUS_SOL == 2){
  print cINTERFACE::inputHidden('acao','concluir');
  print '<input type="submit" value="Concluir"/>'."\n";
  }
 */
/*
  if ($OrdemServico->mID_STATUS_SOL == 3){
  print cINTERFACE::inputHidden('acao','liberar');
  print '<input type="submit" value="Liberar"/>'."\n";
  }
 */
print '</center>' . "\n";
print '</form>' . "\n";
?>
            </div>

        </div>
    </div>
    <br />
    <div id="formPopup" name="formPopup" style="display:none">vazio</div>
<?php
echo Rodape("");

if ($tabOS_selected == '') {
    $tabOS_selected = 0;
}

if (isset($_SESSION['msg']) && $_SESSION['msg'] != '') {
    $mensagem.="\njAlert('" . $_SESSION['msg'] . "');";
    $_SESSION['msg'] = '';
}
?>
    <script language="javascript">

        var NU_EMPRESA_ANT;
        $(document).ready(function() {
    <?= $js; ?>
            var y = $("#tabsIntx").tabs({heightStyle: "content"});
            $("#tabsIntx").tabs("option", "active", <?= $tabsIntx_selected; ?>);
<?= $mensagem; ?>
            carregarArquivos();
        });

        function CarregarListaDeArquivos(pNU_CANDIDATO, pNomeDiv) {
            var url = '../LIB/ARQUIVOS_LISTA.php?NU_CANDIDATO=' + pNU_CANDIDATO + '&NomeDiv=' + pNomeDiv;
            $.ajax({
                url: url,
                async: false,
                success: function(data) {
                    $('#' + pNomeDiv + pNU_CANDIDATO).html(data);
                }
            });
        }

        function CarregarArquivo(pCaminho, pNU_SEQUENCIAL, pNomeDiv) {
            var dimensoes = new Array();
            $.ajax({
                url: '/LIB/imagemTam.php?NU_SEQUENCIAL=' + pNU_SEQUENCIAL,
                async: false,
                success: function(data) {
                    dimensoes = data.split("#");

                }
            });
            var pWidth = +dimensoes[0];
            var pHeight = +dimensoes[1];
            //alert('pWidth='+pWidth+';pHeight='+pHeight);
            var maxW = 400;
            var maxH = 300;
            var w = maxW;
            var h = maxH;
            if (pWidth > pHeight)
            {
                h = Math.round(pHeight / pWidth * maxW);
            }
            else
            {
                w = Math.round(pWidth / pHeight * maxH);
            }
            var d = new Date();
            var xImagem = '<img src="' + pCaminho + '?' + d.getTime() + '" ';
            xImagem = xImagem + 'title="Use ctrl+mouse para cima ou para baixo (com botão pressionado) para dar zoom"/>';
            xImagem = xImagem + '<br/><input type="button" onclick="abreJanelaxxx(\'/LIB/editaImagem.php?NU_SEQUENCIAL=' + pNU_SEQUENCIAL + '\',\'wteste\');" value="Editar"/>';
            xImagem = xImagem + '<input type="button" value="Atualizar" onclick="CarregarArquivo(\'' + pCaminho + '\',\'' + pNU_SEQUENCIAL + '\',\'' + pNomeDiv + '\')"/>';
            $('#' + pNomeDiv).html(xImagem);
            $('#' + pNomeDiv + ' img').imagetool({
                viewportWidth: w
                , vIewportHeight: h
            });
        }

        function RecuperarFormularios(pid_solicita_visto, pNU_CANDIDATO) {
            $('#formulariosCand' + pNU_CANDIDATO).html('(recuperando formulários disponíveis. Por favor, aguarde...)');
            var url = "/LIB/OS_FORMULARIOS.php?id_solicita_visto=" + pid_solicita_visto + "&FL_OS=0&FL_CANDIDATO=1&NU_CANDIDATO=" + pNU_CANDIDATO;
            $.ajax({
                url: url,
                success: function(data) {
                    $('#formulariosCand' + pNU_CANDIDATO).html(data);
                }
            });
        }

        function RecuperarFormulariosOs(pid_solicita_visto) {
            $('#formulariosOS').html('(recuperando formulários disponíveis. Por favor, aguarde...)');
            var url = "/LIB/OS_FORMULARIOS.php?id_solicita_visto=" + pid_solicita_visto + "&FL_OS=1&FL_CANDIDATO=0"
            $.ajax({
                url: url,
                success: function(data) {
                    $('#formulariosOS').html(data);
                }
            });
        }

        function novo(pcmbEmbarcacaoProjeto) {
            var xNU_EMPRESA = '<?= $OrdemServico->mNU_EMPRESA; ?>';
            var xNU_EMBARCACAO_PROJETO = $('#' + pcmbEmbarcacaoProjeto).val();

            if (xNU_EMPRESA == '0') {
                jAlert('Selecione a empresa antes de incluir candidatos.<br>(A empresa é necessária caso você precise incluir um novo candidato.)', 'Atenï¿½ï¿½o');
            } else {
                var nome = "";
                var pagina = "/solicitacoes/novaReqVistoCandNovo.php?NU_EMPRESA=" + xNU_EMPRESA + "&NU_EMBARCACAO_PROJETO=" + xNU_EMBARCACAO_PROJETO;

                abreJanelaxxx(pagina, 'wteste');
                //w.focus();
            }
        }

        function validaFormOS() {
            msg = '';
            if (document.sol.idEmpresa.selectedIndex < 1) {
                msg += "<br/>- Empresa nï¿½o indicada;";
            }
            if (document.sol.NU_EMBARCACAO_PROJETO[document.sol.NU_EMBARCACAO_PROJETO.selectedIndex].value == '') {
                msg += "<br/>- Projeto/embarcaï¿½ï¿½o nï¿½o indicado;";
            }
            if ($('#NU_SERVICO').val() == '0') {
                msg += "<br/>- Tipo de serviço não indicado;";
            }
            if (document.sol.NO_SOLICITADOR.value == '') {
                msg += "<br/>- Solicitante não informado;";
            }
            if (document.sol.DT_SOLICITACAO.value == '') {
                msg += "<br/>- Data da solicitação não informada";
            }
            if (document.sol.cd_Tecnico[document.sol.cd_Tecnico.selectedIndex].value == '') {
                msg += "<br/>- Responsável pelo complemento do cadastro não informado;";
            }
            if (msg != '') {
                jAlert('Corrija os seguintes itens:' + msg, 'Não foi possível salvar a OS');
            } else {
                jConfirm('Deseja mesmo alterar a OS?', 'Alterar OS', function(r) {
                    if (r) {
                        document.sol.submit();
                    }
                })
            }
        }

        var idEmpresa = "<?= $OrdemServico->mNU_EMPRESA ?>";
        var idEmbarcacaoProjeto = $('#NU_EMBARCACAO_PROJETO').val();

        function enviar(tipo) {
            var cand = document.wilson.idCandidato.value;
            var emp = document.wilson.idEmpresa.value;
            var sol = document.wilson.NU_SOLICITACAO.value;
            var cd_admin = document.wilson.cd_admin.value;
            var pagina = "operacao3.php?idCandidato=" + cand + "&idEmpresa=" + emp + "&opvisa=" + tipo + "&NU_SOLICITACAO=" + sol + "&cd_admin=" + cd_admin + "&A=";

            document.wilson.opvisa.value = tipo;
            document.wilson.target = "_top";
            document.wilson.action = "operacao3.php";
            //window.open(pagina);
            var msg = OpenChild(tipo, pagina);
            document.all.aguarde.innerHTML = msg;
        }

        function arquivos(tipo) {
            document.wilson.opvisa.value = tipo;
            document.wilson.target = "_top";
            //var msg = OpenChild(tipo,"operacao3up.php");
            var msg = OpenChild(tipo, "/admin/documentos1.php");
            document.all.aguarde.innerHTML = msg;
        }

        function formulario(tipo) {
            document.wilson.action = "/formularios/all_registroPF.php";
            document.wilson.target = "formPF";
            document.wilson.submit();
        }

        function visadetail(codigo, acao, visa) {
            var cd_admin = "<?= $usulogado ?>";
            var pagina = "../intranet/geral/novoVisaDetail.php?idCandidato=" + codigo + "&acao=" + acao + "&idEmpresa=" + idEmpresa + "&NU_SOLICITACAO=" + visa + "&cd_admin=" + cd_admin;
            var ret = AbrePagina(acao, pagina, idEmpresa, codigo, idEmbarcacaoProjeto);
        }

        function carregarArquivos() {
            var id = $('#CMP_id_solicita_visto').val();
            if (id != '0') {
                $.ajax({
                    url: "/LIB/ARQUIVOS_SOL_LISTA.php?ID_SOLICITA_VISTO=" + $('#CMP_id_solicita_visto').val(),
                    success: function(data) {
                        $('#ARQUIVOS_container').html(data);
                    }
                });
            }
            else {
                $('#ARQUIVOS_container').html('');
            }
        }

        function ajaxRemoverArquivoSol(pNU_SEQUENCIAL, pNome) {
            jConfirm('Deseja mesmo excluir o arquivo "' + pNome + '"? Essa operacao é irreversível.', 'Exclusão de arquivo', function(r) {
                if (r) {
                    var url = "/LIB/ARQUIVOS_DEL.php?NU_SEQUENCIAL=" + pNU_SEQUENCIAL;
                    $.ajax({
                        url: url,
                        success: function(data) {
                            carregarArquivos();
                            jAlert(data);
                        }
                    });
                }
            });
        }
    </script>
