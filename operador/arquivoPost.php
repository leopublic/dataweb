<?php

//error_reporting(E_ALL); 
//ini_set("display_errors", 1);
// TODO: Verificar diferencas em relacao a versao de producao
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/arquivos.php");
include ("../LIB/classes/cARQUIVO.php");

$idEmpresa = 0 + $_POST['NU_EMPRESA'];
$idCandidato = 0 + $_POST['NU_CANDIDATO'];
$msgErro = '';
if ($_FILES["ARQUIVO" . $idCandidato]["error"] > 0) {
    //echo "Error: " . $_FILES["userfile"]["error"] . "<br />";
    $msgErro = 'Não foi possível carregar o arquivo. Erro=' . $_FILES["ARQUIVO" . $idCandidato]["error"];
} else {
    //echo "Upload: " . $_FILES["userfile"]["name"] . "<br />";
    //echo "Type: " . $_FILES["userfile"]["type"] . "<br />";
    //echo "Size: " . ($_FILES["userfile"]["size"] / 1024) . " Kb<br />";
    //echo "Stored in: " . $_FILES["userfile"]["tmp_name"];
    if (!is_dir($mydocdir)) {
        mkdir($mydocdir, 0770);
    }
    $updir = $mydocdir . DS . $idEmpresa;
    if (!is_dir($updir)) {
        mkdir($updir, 0770);
    }
    $updir = $updir . DS . $idCandidato;

    if (!is_dir($updir)) {
        mkdir($updir, 0770);
    }

    $msgErro = cARQUIVO::CriarArquivo($_FILES["ARQUIVO" . $idCandidato], $updir, $idEmpresa, 'C', $idCandidato, $_POST['CMP_ID_TIPO_ARQUIVO' . $idCandidato], $usulogado);
}
if ($msgErro == '' || $msgErro == 'Arquivo adicionado com sucesso') {
    $extencoes_validas = array("jpg", "jpeg", "gif", "png", "bmp", "tiff");
    print "<script>window.opener.CarregarListaDeArquivos('" . $idCandidato . "','arquivosListados');window.opener.jAlert('Arquivo carregado com sucesso');window.close();</script>";
} else {
    print 'Não foi possível adicionar o arquivo.<br>' . $msgErro;
}
