<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

/*
  function exception_error_handler($errno, $errstr, $errfile, $errline ) {
  throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
  }
  set_error_handler("exception_error_handler");
 */

cHTTP::$comCabecalhoFixo = false;


Acesso::SetBloqueio(Acesso::tp_Os_Aba_Dados);
/*
 * Processa o form enviado se houver
 * Ver cINTERFACE_OS::PostSOLICITA_VISTO_edit
 */
$msg = cINTERFACE::ProcessePost($_POST, $_GET, $_FILES, $_SESSION);
if ($msg != '') {
    $_SESSION['msg'] = $msg;
}
//
// Redireciona para outra pagina de o controller tiver definido isso.
cHTTP::RedirecionarSeSolicitado();
/*
 * Obter parametros
 */
$OS_i = new cINTERFACE_OS();
$OS_i->ObterParametros($_POST, $_GET);
if (isset($_POST['DadosOS_CANDIDATO_chaves'])) {
    $OS_i->mCandidatos = $_POST['DadosOS_CANDIDATO_chaves'];
}
$tabsOS_selected = '';


$js = '';
$voltar = $_SESSION['voltar'];
if ($voltar == '') {
    $voltar = "os_listar.php";
}

//###################################################################
// Inicio do documento
//###################################################################
echo Topo();
echo Menu("SOL");
?>
<div class="conteudo">
    <?php print $OS_i->FormCabecalhoOS(); ?>
    <div class="conteudoInterno">
        <div id="tabsOS" class="aba">
            <?php
            print $OS_i->aba("DadosOS", '/operador/os_DadosOS.php');
//###################################################################
// Dados da solicitacao
//###################################################################
            $tela = $OS_i->FormDadosOS();
            cINTERFACE::RecupereValoresEdicao($tela, $_POST);
            if ($OS_i->mID_SOLICITA_VISTO == "0") {
                $tela->mCampoNome['dt_solicitacao']->mValor = date('%d/%m/%Y');
                $tela->mCampoNome['cd_tecnico']->mValor = cSESSAO::$mcd_usuario;
            } else {
                $tela->mCampoNome['NU_SERVICO']->mDisabled = true;
            }
            ?>
            <div id="DadosOS">
                <div style="padding-left:10px;padding-right:10px;">
                    <?php
                    $ret = cINTERFACE::formEdicao($tela, true);
                    $ret.= '<input type="hidden" id="NU_SERVICO_ORIGINAL" value="'.$OS_i->mNU_SERVICO.'"/>';
                    $ret .= '<table class="edicao">';
                    $ret .= '<col width="18%"></col>';
                    $ret .= '<col width="28%"></col>';
                    $ret .= '<col width="5%"></col>';
                    $ret .= '<col width="18%"></col>';
                    $ret .= '<col width="28%"></col>';
                    foreach ($tela->mCampo as $campo) {
                        if (!$campo->mChave && $campo->mVisivel && $campo->mTipo != cCAMPO::cpHIDDEN) {
                            if (!$par) {
                                $linha = '<tr>';
                            } else {
                                $linha .= '<td>&#160;</td>';
                            }

                            if ($campo->mCampoBD == 'NU_SERVICO') {
                                if ($campo->mValor != '') {
                                    $campo->AdicioneExtraAoComboInterno(' where 1=2');
                                } else {
                                    $campo->AdicioneExtraAoComboInterno(' where 1=2');
                                }
                            }

                            if ($campo->mCampoBD == 'NU_EMPRESA') {
                                if ($campo->mValor != '') {
                                    $campo->AdicioneExtraAoComboInterno(' where (FL_ATIVA = 1 or NU_EMPRESA = ' . $campo->mValor . ')');
                                }
                            }

                            if ($campo->mCampoBD == 'nu_empresa_requerente') {
                                if ($campo->mValor != '') {
                                    $campo->AdicioneExtraAoComboInterno(' where (FL_ATIVA = 1 or NU_EMPRESA = ' . $campo->mValor . ')');
                                }
                            }

                            $campoHTML = cINTERFACE::RenderizeLinhaEdicao($campo, false);
                            if ($campo->mReadonly) {
                                $campoHTML = str_replace("\n", "<br/>", $campoHTML);
                            }
                            $linha .= $campoHTML;
                            if ($par) {
                                $linha .='</tr>';
                                $ret.= $linha . "\n";
                            }
                            $par = !$par;
                        }
                    }
                    if ($par) {
                        $ret.= $linha . '<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>' . "\n";
                    }
                    print $ret;


                    if ($OS_i->mID_SOLICITA_VISTO > 0) {
                        ?>
                        <tr><th>Arquivo(s):</th>
                            <td colspan="5">
                                <div class="container" id="ARQUIVOS_container">(carregando...)</div>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <th>Candidatos<?= cINTERFACE::inputHidden('DadosOS_CANDIDATO_chaves', $OS_i->mCandidatos); ?></th>
                        <td colspan="4">
                            <div class="container" id="DadosOS_CANDIDATO_container" name="DadosOS_CANDIDATO_container" style="margin:0;padding:0;">(carregando...)</div>
                            <script>
                            </script>
                        </td>
                    </tr>
                    </table>
                    <?php if (!$OS_i->mReadonly) { ?>
                        <div class="barraBotoesRodape">
                            <input type="button" onclick="javascript:novo();" style="width:auto" value="Adicionar candidatos" accesskey="A"/>
                            <input type="submit" value="Salvar" id="salvar"  accesskey="S"/>
                        </div>
                        <?php
                    }
                    if ($OS_i->mID_SOLICITA_VISTO == 0) {

                        if (isset($_GET['OS_REF'])) {
                            $chk_continua = '';
                            $chk_nova = 'checked = "true"';
                        } else {
                            $chk_continua = 'checked = "true"';
                            $chk_nova = '';
                        }
                        ?>
                        <input type="radio" name="continuacao" value="1" <?= $chk_continua ?> title="Após salvar, ir para a edição da OS recém criada."/> Continuar na OS &nbsp;&nbsp;&nbsp;

                        <input type="radio" name="continuacao" value="2" <?= $chk_nova ?> title="Após salvar, abrir formulário em branco para incluir outra OS semelhante à recém criada."/> Criar nova OS
                        <?
                    }
                    ?>
                    </form>
                </div>
            </div>
        </div>
        <?php
        $mensagem = '';
//var_dump($xmsg);
        if (isset($_SESSION['msg']) && $_SESSION['msg'] != '') {
            $mensagem = "\njAlert('" . nl2br($_SESSION['msg']) . "');";
            $_SESSION['msg'] = '';
        }

        if (intval($OS_i->mID_SOLICITA_VISTO) > 0) {
            print '<div style="padding-top:20px; padding-left:10px;padding-right:10px;">';
            $x = new cCTRL_OS_HIST_LIST();
            $x->id_solicita_visto = $_GET['id_solicita_visto'];
            if ($x->id_solicita_visto == '') {
                $x->id_solicita_visto = $_GET['ID_SOLICITA_VISTO'];
            }
            echo $x->Liste();
            print '</div>';
        }
        ?>
    </div>
</div>

<?php
echo Rodape("");

if ($tabOS_selected == '') {
    $tabOS_selected = 0;
}


$msg_impressao = '';
if (isset($_GET['OS_REF']) <> '') {
    $xOS = new cORDEMSERVICO();
    $xOS->Recuperar($_GET['OS_REF']);

    if ($xOS->mNU_SERVICO <> '') {
        $sql = "
			SELECT C.NU_CHECKLIST, CS.NU_SERVICO, C.NO_CHECKLIST
			FROM CHECKLIST_SERVICO CS
		INNER JOIN CHECKLIST C
				ON C.NU_CHECKLIST = CS.NU_CHECKLIST
			WHERE NU_SERVICO = " . $xOS->mNU_SERVICO . "
		";
        $exe = mysql_query($sql);
        $r = mysql_fetch_array($exe);
        $NU_CHECKLIST = $r['NU_CHECKLIST'];
    }



    $msg_impressao = 'jConfirm("A OS ' . $xOS->mNU_SOLICITACAO . ' foi cadastrada com sucesso <b>e você foi direcionada(o) para uma nova OS</b>.<br /> Deseja imprimir a OS que foi criada (' . $xOS->mNU_SOLICITACAO . ')?", "Impressão",
    function(res){
        if(res){
			window.open("../LIB/OS_rel.php?ID_SOLICITA_VISTO=' . $_GET['OS_REF'] . '", "ImpressaoOS",  "height = 600 , width = 800");';
    if ($NU_CHECKLIST > 0) {
        $msg_impressao .= '  window.open("../LIB/checklist.php?ID_SOLICITA_VISTO=' . $_GET['OS_REF'] . '&CHECKLIST=' . $NU_CHECKLIST . '", "Impressaochecklist",  "height = 600 , width = 800");';
    }
    $msg_impressao .= '  }
    });';
}
?>
<script>
    var NU_EMPRESA_ANT;

    $(document).ready(function () {
//					$(".x_x").bind("click", function(event){
//					var str = "( " + event.pageX + ", " + event.pageY + " )";
//					$("#steste").text("Click happened! " + str);
//					});
//					$(".x_x").bind("dblclick", function(){
//					$("#steste").text("Double-click happened in " + this.nodeName);
//					});
//					$("#pteste").bind("mouseenter mouseleave", function(event){
//					});



//		$('.remove_candidato').bind('click', function(event){
//				alert('entrou');
//			}
//		);

<?= $js; ?>
<?= $mensagem; ?>
<?= $msg_impressao ?>

        carregarArquivos();
        // Adiciona refresh via javascript na embarcação
        $('#CMP_NU_EMPRESA').change(function () {
            atualizaEmbarcacaoProjeto();
            atualizaEmbarcacaoProjetoCobranca();
            if ($('#CMP_nu_empresa_requerente').val() == '' || $('#CMP_nu_empresa_requerente').val() == NU_EMPRESA_ANT) {
                $('#CMP_nu_empresa_requerente').val($('#CMP_NU_EMPRESA').val());
                atualizaServicos();
            }
            NU_EMPRESA_ANT = $('#CMP_NU_EMPRESA').val();
        });
        $('#CMP_nu_empresa_requerente').change(function () {
            atualizaServicos();
        });
        atualizaEmbarcacaoProjeto();
        atualizaEmbarcacaoProjetoCobranca();
        atualizaServicos();
        CarregarCandidatos('<?= $OS_i->mID_SOLICITA_VISTO; ?>', 'DadosOS_CANDIDATO_container', 'DadosOS_CANDIDATO_chaves', '1');
        ConectaExclusao();
        ConectaInclusao();

        $('#CMP_NU_SERVICO').change(function () {
            var id;
            id = $('#CMP_id_solicita_visto').val();
            $('#DadosOS_CANDIDATO_chaves').val('');
            $('#DadosOS_CANDIDATO_container').html('&nbsp;');
        });

    });

    function FormValido() {
        $('#salvar').val('enviando...');
        $('#salvar').attr('disabled', 'disabled');
        return true;
    }

// Fazer o código para a inclusao do dependente
    function ConectaExclusao() {
        $('#DadosOS_CANDIDATO_container').on('click', 'img.remove_candidato', function (event) {
            var controle = event.currentTarget;
            pNU_CANDIDATO = $(controle).attr('rel');
            pID_SOLICITA_VISTO = $(controle).attr('alt');
            nome = $(controle).parent().text();

            if (confirm('Deseja mesmo remover o candidato: ' + nome + ' da solicitação?\n(Essa operação não é reversível)')) {
                var xChaves = $('#DadosOS_CANDIDATO_chaves').val();
                var xPos = xChaves.indexOf(pNU_CANDIDATO);
                if (xPos >= 0) {
                    var xChavesNovo = '';
                    var arr = xChaves.split(',');
                    xVirg = '';
                    xI = 0;
                    while (xI < arr.length) {
                        if (arr[xI] != pNU_CANDIDATO) {
                            xChavesNovo = xChavesNovo + xVirg + arr[xI];
                            xVirg = ',';
                        }
                        xI++;
                    }
                    $('#DadosOS_CANDIDATO_chaves').val(xChavesNovo);
                    url = "/LIB/CANDIDATO_DEL.php?id_solicita_visto=" + pID_SOLICITA_VISTO + "&NU_CANDIDATO=" + pNU_CANDIDATO;
                    if (pID_SOLICITA_VISTO > 0) {
                        $.ajax({
                            url: url,
                            success: function (data) {
                                jAlert(data);
                                CarregarCandidatos('<?= $OS_i->mID_SOLICITA_VISTO; ?>', 'DadosOS_CANDIDATO_container', 'DadosOS_CANDIDATO_chaves', '0');
                            }
                        });
                    }

                    CarregarCandidatos('<?= $OS_i->mID_SOLICITA_VISTO; ?>', 'DadosOS_CANDIDATO_container', 'DadosOS_CANDIDATO_chaves', '0');
                }
            }
            return '';
        }
        );
    }
// Fazer o código para a inclusao do dependente
//
    function ConectaInclusao() {
        $('#DadosOS_CANDIDATO_container').on('click', 'img.adicionar_candidato', function (event) {
            var controle = event.currentTarget;
            pNU_CANDIDATO = $(controle).attr('rel');
            pID_SOLICITA_VISTO = $(controle).attr('alt');
            nome = $(controle).parent().text();

            if (confirm('Deseja mesmo adicionar o dependente nessa OS?')) {
                var xChaves = $('#DadosOS_CANDIDATO_chaves').val();
                var xPos = xChaves.indexOf(pNU_CANDIDATO);
                if (xPos == -1) {
                    $('#DadosOS_CANDIDATO_chaves').val(xChaves + ',' + pNU_CANDIDATO);
                    url = "/paginas/carregueAjax.php?controller=cCTRL_ORDEMSERVICO&metodo=AdicioneCandidato&id_solicita_visto=" + pID_SOLICITA_VISTO + "&nu_candidato=" + pNU_CANDIDATO;
                    if (pID_SOLICITA_VISTO > 0) {
                        $.ajax({
                            url: url,
                            success: function (data) {
                                jAlert(data);
                                CarregarCandidatos('<?= $OS_i->mID_SOLICITA_VISTO; ?>', 'DadosOS_CANDIDATO_container', 'DadosOS_CANDIDATO_chaves', '0');
                            }
                        });
                    }
                    else {
                        $('#DadosOS_CANDIDATO_chaves').val(xChaves + ',' + pNU_CANDIDATO);
                        CarregarCandidatos('<?= $OS_i->mID_SOLICITA_VISTO; ?>', 'DadosOS_CANDIDATO_container', 'DadosOS_CANDIDATO_chaves', '0');
                    }
                }
            }
            return '';
        }
        );
    }

    function atualizaEmbarcacaoProjeto() {
        if ($('#CMP_NU_EMPRESA').val() > 0) {
            var url = '/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA=' + $('#CMP_NU_EMPRESA').val() + '&NU_EMBARCACAO_PROJETO=' + $('#CMP_NU_EMBARCACAO_PROJETO').val();
            $.ajax({
                url: url,
                async: false,
                success: function (data) {
                    $('#tdEmbarcacaoProjeto').html('<select id="CMP_NU_EMBARCACAO_PROJETO" name="CMP_NU_EMBARCACAO_PROJETO"><option value="">Selecione...</option>' + data + '</select>');
                }
            });
        }
        else {
            $('#tdEmbarcacaoProjeto').html('(selecione a empresa...)');
        }
    }

    function atualizaEmbarcacaoProjetoCobranca() {
        if ($('#CMP_NU_EMPRESA').val() > 0) {
            var url = '/LIB/combosAjax.php?tipo=EMBARCACAO_PROJETO&NU_EMPRESA=' + $('#CMP_NU_EMPRESA').val() + '&NU_EMBARCACAO_PROJETO=' + $('#CMP_NU_EMBARCACAO_PROJETO_COBRANCA').val();
            $.ajax({
                url: url,
                async: false,
                success: function (data) {
                    $('#tdEmbarcacaoProjetoCobranca').html('<select id="CMP_NU_EMBARCACAO_PROJETO_COBRANCA" name="CMP_NU_EMBARCACAO_PROJETO_COBRANCA"><option value="">Selecione...</option>' + data + '</select>');
                }
            });
        }
        else {
            $('#tdEmbarcacaoProjetoCobranca').html('(selecione a empresa...)');
        }
    }

    function atualizaServicos(){
        if ($('#CMP_nu_empresa_requerente').val() > 0) {
            $("#CMP_NU_SERVICO").find('option')
                            .remove()
                            .end()
                            .append('<option value="carregando">(carregando...)</option>')
                            .val('carregando');
            var url = '/paginas/carregue.php?controller=cCTRL_SERVICO&metodo=servicosDaEmpresa&nu_empresa=' + $('#CMP_nu_empresa_requerente').val() + '&nu_servico=' + $('#CMP_SERVICO_ORIGINAL').val();
            $.getJSON(url, function(data){
                $("#CMP_NU_SERVICO").find('option')
                                .remove();
                var servicos = data.servicos;
                if(data.servicos.length == 0){
                    $('#CMP_NU_SERVICO').append( new Option('(nenhum serviço disponível)','') );
                } else {
                    $('#CMP_NU_SERVICO').append( new Option('(selecione...)','') );
                    $.each(data.servicos, function(key, servico) {
                        $('#CMP_NU_SERVICO').append( new Option(servico.no_servico_resumido,servico.nu_servico) );
                    });
                    $("#CMP_NU_SERVICO").val($('#NU_SERVICO_ORIGINAL').val());
                }
            });
        }
        else {
            $("#CMP_NU_SERVICO").find('option')
                            .remove()
                            .end()
                            .append('<option value="">(selecione a empresa)</option>')
                            .val('');
        }

    }

    function ajaxIncluir(pCampo, pValor) {
        var xChaves = $('#DadosOS_' + pCampo + '_chaves').val();
        var xVirgula = '';
        if (xChaves != '')
        {
            xVirgula = ',';
        }

        var valor = pValor.split(',');
        var valorpesq = '';
        var xChavespesq = ',' + xChaves + ',';
        var i = 0;
        var incluido = false;
        for (i = 0; i < valor.length; i++) {
            valorpesq = ',' + valor[i] + ',';
            if (xChavespesq.indexOf(valorpesq) < 0) {
                xChaves = xChaves + xVirgula + valor[i];
                incluido = true;
                xVirgula = ',';
            }
        }
        if (incluido) {
            $('#DadosOS_' + pCampo + '_chaves').val(xChaves);
            CarregarCandidatos('<?= $OS_i->mID_SOLICITA_VISTO; ?>', 'DadosOS_CANDIDATO_container', 'DadosOS_CANDIDATO_chaves', '0');
            return 'Candidato(s) incluído(s) com sucesso!';
        }
        else {
            return 'O(s) candidato(s) indicados já estão na solicitação.';
        }
    }

    function removeTodosCandidatos()
    {
        var id;
        id = $('#CMP_id_solicita_visto').val();
        $('#DadosOS_CANDIDATO_chaves').val('');
        $('#DadosOS_CANDIDATO_container').html('&nbsp;');

    }

    function CarregarListaDeArquivos(pNU_CANDIDATO, pNomeDiv) {
        var url = '../LIB/ARQUIVOS_LISTA.php?NU_CANDIDATO=' + pNU_CANDIDATO + '&NomeDiv=' + pNomeDiv;
        $.ajax({
            url: url,
            async: false,
            success: function (data) {
                $('#' + pNomeDiv + pNU_CANDIDATO).html(data);
            }
        });
    }

    function CarregarArquivo(pCaminho, pNU_SEQUENCIAL, pNomeDiv) {
        var dimensoes = new Array();
        $.ajax({
            url: '/LIB/imagemTam.php?NU_SEQUENCIAL=' + pNU_SEQUENCIAL,
            async: false,
            success: function (data) {
                dimensoes = data.split("#");

            }
        });
        var pWidth = +dimensoes[0];
        var pHeight = +dimensoes[1];
        //alert('pWidth='+pWidth+';pHeight='+pHeight);
        var maxW = 400;
        var maxH = 300;
        var w = maxW;
        var h = maxH;
        if (pWidth > pHeight)
        {
            h = Math.round(pHeight / pWidth * maxW);
        }
        else
        {
            w = Math.round(pWidth / pHeight * maxH);
        }
        var d = new Date();
        var xImagem = '<img src="' + pCaminho + '?' + d.getTime() + '" ';
        xImagem = xImagem + 'title="Use ctrl+mouse para cima ou para baixo (com botão pressionado) para dar zoom"/>';
        xImagem = xImagem + '<br/><input type="button" onclick="abreJanelaxxx(\'/LIB/editaImagem.php?NU_SEQUENCIAL=' + pNU_SEQUENCIAL + '\',\'wteste\');" value="Editar"/>';
        xImagem = xImagem + '<input type="button" value="Atualizar" onclick="CarregarArquivo(\'' + pCaminho + '\',\'' + pNU_SEQUENCIAL + '\',\'' + pNomeDiv + '\')"/>';
        $('#' + pNomeDiv).html(xImagem);
        $('#' + pNomeDiv + ' img').imagetool({
            viewportWidth: w
            , vIewportHeight: h
        });
    }

    // Adiciona candidatos na OS
    function novo() {
        var xNU_EMPRESA;
        var xNU_SERVICO;
        xNU_EMPRESA = $('#CMP_NU_EMPRESA').val();
        xNU_SERVICO = $('#CMP_NU_SERVICO').val();
        var xNU_EMBARCACAO_PROJETO = $('#CMP_NU_EMBARCACAO_PROJETO').val();

        if (xNU_EMPRESA == '0' || xNU_EMPRESA == '') {
            jAlert('Selecione a empresa antes de incluir candidatos.', 'Atenção');
        } else {
            if (xNU_SERVICO == '0' || xNU_SERVICO == '') {
                jAlert('Selecione o serviço antes de incluir candidatos.', 'Atenção');
            } else {
                var nome = "";
                //var pagina = '/paginas/carregue.php?controller=cCTRL_CANDIDATO&metodo=pesquisarParaOs&nu_empresa='+xNU_EMPRESA+"&nu_servico="+xNU_SERVICO;
                var pagina = "/solicitacoes/novaReqVistoCandNovo.php?NU_EMPRESA=" + xNU_EMPRESA + "&NU_SERVICO=" + xNU_SERVICO;
                abreJanelaxxx(pagina, 'wteste');
                //w.focus();
            }
        }
    }

    var idEmpresa = "<?= $OS_i->mNU_EMPRESA ?>";
    var idEmbarcacaoProjeto = $('#CMP_NU_EMBARCACAO_PROJETO').val();

    function enviar(tipo) {
        var cand = document.wilson.idCandidato.value;
        var emp = document.wilson.idEmpresa.value;
        var sol = document.wilson.NU_SOLICITACAO.value;
        var cd_admin = document.wilson.cd_admin.value;
        var pagina = "operacao3.php?idCandidato=" + cand + "&idEmpresa=" + emp + "&opvisa=" + tipo + "&NU_SOLICITACAO=" + sol + "&cd_admin=" + cd_admin + "&A=";

        document.wilson.opvisa.value = tipo;
        document.wilson.target = "_top";
        document.wilson.action = "operacao3.php";
        //window.open(pagina);
        var msg = OpenChild(tipo, pagina);
        document.all.aguarde.innerHTML = msg;
    }

    function arquivos(tipo) {
        document.wilson.opvisa.value = tipo;
        document.wilson.target = "_top";
        //var msg = OpenChild(tipo,"operacao3up.php");
        var msg = OpenChild(tipo, "/admin/documentos1.php");
        document.all.aguarde.innerHTML = msg;
    }

    function formulario(tipo) {
        document.wilson.action = "/formularios/all_registroPF.php";
        document.wilson.target = "formPF";
        document.wilson.submit();
    }

    function visadetail(codigo, acao, visa) {
        var cd_admin = "<?= $usulogado ?>";
        var pagina = "../intranet/geral/novoVisaDetail.php?idCandidato=" + codigo + "&acao=" + acao + "&idEmpresa=" + idEmpresa + "&NU_SOLICITACAO=" + visa + "&cd_admin=" + cd_admin;
        var ret = AbrePagina(acao, pagina, idEmpresa, codigo, idEmbarcacaoProjeto);
    }

    function carregarArquivos() {
        var id = $('#CMP_id_solicita_visto').val();
        if (id != '0') {
            $.ajax({
                url: "/LIB/ARQUIVOS_SOL_LISTA.php?ID_SOLICITA_VISTO=" + $('#CMP_id_solicita_visto').val(),
                success: function (data) {
                    $('#ARQUIVOS_container').html(data);
                }
            });
        }
        else {
            $('#ARQUIVOS_container').html('');
        }
    }

    function ajaxRemoverArquivoSol(pNU_SEQUENCIAL, pNome) {
        jConfirm('Deseja mesmo excluir o arquivo "' + pNome + '"? Essa operacao é irreversível.', 'Exclusão de arquivo', function (r) {
            if (r) {
                var url = "/LIB/ARQUIVOS_DEL.php?NU_SEQUENCIAL=" + pNU_SEQUENCIAL;
                $.ajax({
                    url: url,
                    success: function (data) {
                        carregarArquivos();
                        jAlert(data);
                    }
                });
            }
        });
    }

    function enviaSenha(nu_candidato) {
        $.getJSON("/paginas/carregue.php?controller=cCTRL_USUARIO_EXTERNO&metodo=getEnvieNovaSenhaId&NU_CANDIDATO="+nu_candidato, function (data) {
            alert(data.msg);
        });
    }
</script>
<style>
    #dadosOS{
        margin-top:5px;
    }
</style>
