<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/libVisaDetail.php");

/*
 function exception_error_handler($errno, $errstr, $errfile, $errline ) {
 throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
 }
 set_error_handler("exception_error_handler");
 */

$msg = cINTERFACE::ProcessePost($_POST, $_GET, $_FILES, $_SESSION);

$index = 0;
$mensagem='';
if(isset($_GET['NU_CANDIDATO']))
{
	$pNU_CANDIDATO = $_GET['NU_CANDIDATO'];
	$cand = new cINTERFACE_CANDIDATO();

	$tela = new cEDICAO("vCANDIDATO_CONS");
	$tela->CarregarConfiguracao();
	
	if (cINTERFACE::CampoEnviado($_POST, $tela->mCampoCtrlEnvio)){
		// Obtem valores digitados
		cINTERFACE::RecupereValoresEdicao($tela, $_POST);
	
		// Salva 
		//$cand->SalvarEdicao($editaCandidato);
		if($tela->AtualizarBanco()){
			$cand->Recuperar($pNU_CANDIDATO);
			$ac = $cand->UltimaSolicitacao();
			$cand->CorrijaNomeCompleto();
			$cand->LogueUltimaAtualizacao();			
			$cand->VerifiqueCadastroCompleto();
			
			$msg = 'Identificação do candidato atualizada com sucesso';

			$tela->RecupereRegistro();
			$cand->Recuperar($pNU_CANDIDATO);
			
		}
		else{
			$cand->Recuperar($pNU_CANDIDATO);
			$ac = $cand->UltimaSolicitacao();
			$msg = 'Não foi possível atualizar o candidato.<br>'.str_replace("'", "\'", mysql_error());
		}
	}
	else{   
		$cand->Recuperar($pNU_CANDIDATO);
		$ac = $cand->UltimaSolicitacao();
		$tela->mCampoNome['NU_CANDIDATO']->mValor =$pNU_CANDIDATO ;
		$tela->RecupereRegistro();
	}
}
else{
	$msg = "Candidato não informado";
}

$voltar = $_SESSION['voltar'];
if ($voltar == ''){
	$voltar = "os_listar.php";
}
// Cabecalho padrão
echo Topo($cand->mNOME_COMPLETO, '' , false);

?>
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $("#tabsInt").tabs();
    });
    
</script>
<div style="background-color:#fff;background-image:none;height:auto;padding-top:10px;padding-bottom:10px;text-align:left;padding-left:10px;"><img src="/imagens/logo_mundivisas_red.jpg" style="margin:0;padding:0;"/>
	<div style="padding-top:50px;padding-right:10px;float:right;vertical-align: bottom;"><?=$_SESSION['myAdmin']['nm_nome'];?></div>
</div>
<div class="conteudo">
	<div class="titulo">
		<div style="float:right">
			<input type="button" value="Voltar para listagem" onclick="javascript:window.location='candListar.php';"/>
			<input type="button" value="Sair" onclick="javascript:window.location='/sair.php';"/>

		</div>
		<div style="float:left">Detalhes de "<?=$cand->mNOME_COMPLETO;?>"</div>&nbsp;
	</div>
	<div class="conteudoInterno">
		<div id="accordionCand">
		
			<h3 class="interno">Visto atual</h3>
			
			<div>
			<?
				print $cand->FormVistoAtual();
			?>
			</div>		

			<h3 class="interno">Identificação do Candidato</h3>
			<div>
			<?=cINTERFACE::formEdicao();?>
				<?=cINTERFACE::RenderizeCampo($tela->mCampoCtrlEnvio);?>
				<?=cINTERFACE::RenderizeCampo($tela->mCampoNome['NU_CANDIDATO']);?>
				<table class="edicao dupla">
					<colgroup>
						<col width="18%"></col>
						<col width="28%"></col>
						<col width="5%"></col>
						<col width="18%"></col>
						<col width="28%"></col>
					</colgroup>
<?
$tr = "<tr>";
$btr = "";
$par = false;
$painelAnt = '';
foreach($tela->mCampo as $campo){
	if ($painelAnt!=$campo->mPainel){
		if ($par&&$linha!=''){
			$linha .= '<td>&#160;</td>';
			$linha .='</tr>';
			print $linha."\n";
		}
		print '<tr class="subTitulo"><td colspan="5">'.$campo->mPainel.'</td></tr>';
		$painelAnt = $campo->mPainel;
		$par = false;
	}
	if (!$campo->mChave && $campo->mTipo != '1'){
		if (!$par){
			$linha = '<tr>';
		}
		else{
			$linha .= '<td>&#160;</td>';
		}
		if ($campo->mNome == 'NU_EMBARCACAO_PROJETO'){
			$linha .= '<th>'.$campo->Label().'</th><td><div id="tdEmbarcacaoProjeto">'.cINTERFACE::RenderizeCampo($campo).'</div></td>';
		}
		else{	
			$linha .= cINTERFACE::RenderizeLinhaEdicao($campo);
		}
	
		if ($par){
			$linha .='</tr>';
			print $linha."\n";
		}
		
		$par = !$par;
	}
}

?>				
				
				</table>
				</form>
			</div>

			<h3 class="interno">Dependentes</h3>
			<div>
				<form method="post" action="/intranet/geral/dependentes.php">
					<input type="hidden" name="idCandidato" value="<?=$cand->mNU_CANDIDATO;?>"/>
					<input type="hidden" name="idEmpresa" value="<?=$cand->mNU_EMPRESA;?>"/>
					<input type="submit" value="Administrar dependentes"/>
				</form>
			</div>
		</div>
		<div id="accordionCand2" class="accordion">
			<h3 class="interno">Arquivos<?=$qtdArqs;?></h3>
			<div>
				<div id="containerVisualizacao"
					style="float: right; border: solid 1px #999999; padding: 3px; text-align: center;">
				</div>
				<form id="novoArquivo<?=$cand->mNU_CANDIDATO;?>" action="arquivoPost.php" enctype="multipart/form-data" target="_blank" method="post">
			<?=cINTERFACE::inputHidden("formArquivo", "1");?>
			<?=cINTERFACE::inputHidden("NU_CANDIDATO", $cand->mNU_CANDIDATO);?>
			<?=cINTERFACE::inputHidden("NU_EMPRESA", $ac->mNU_EMPRESA);?>
					<div id="arquivosListados<?=$cand->mNU_CANDIDATO;?>"></div>
				</form>
				<script>
					function CarregarArquivo(pCaminho,pNU_SEQUENCIAL){
					  	var dimensoes = new Array();
						$.ajax({
							  url: '/LIB/imagemTam.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL,
							  async:false,
							  success: function(data) {
								dimensoes = data.split("#");
							
							  }
							});
						var pWidth = +dimensoes[0];
						var pHeight = +dimensoes[1];
						//alert('pWidth='+pWidth+';pHeight='+pHeight);
						var maxW = 400;
						var maxH = 300;
						var w = maxW;
						var h = maxH;
						if (pWidth>pHeight){
							h = Math.round(pHeight/pWidth * maxW);
						}
						else{
							w = Math.round(pWidth/pHeight * maxH);
						}
						var d = new Date();						
						$('#containerVisualizacao').html('<img src="'+pCaminho+'?'+d.getTime()+'"/><br/><input type="button" onclick="abreJanelaxxx(\'/LIB/editaImagem.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL+'\',\'wteste\');" value="Editar"/><input type="button" value="Atualizar" onclick="CarregarArquivo(\''+pCaminho+'\',\''+pNU_SEQUENCIAL+'\', '+pWidth+', '+pHeight+')"/>');	 
						$("#containerVisualizacao img").imagetool({
						    viewportWidth: w
						   ,viewportHeight: h
						  });
					}
				</script>

                <!--  <div id="ArquivosOS"> Carregando Arquivos agrupados por OS, Aguarde ...</div>-->
			</div>
		</div>
		</div>
	</div>
</div>
<div id="formPopup" name="formPopup" style="display:none">
vazio
</div>

<?php
if ($msg!='')
{
?>	
<script language="javascript">jAlert('<?=$msg;?>');</script>
<?
}
echo Rodape("");
?> 
<script language="javascript">

$(document).ready(function() {
    var nu_candidato = '<?=$cand->mNU_CANDIDATO;?>';
	$("#accordionCand").accordion({ autoHeight: false, collapsible: true, active: 0});
	CarregarListaDeArquivos('<?=$cand->mNU_CANDIDATO;?>','arquivosListados');

	$('select[id="CMP_NU_EMPRESA"]').change(function() {
		atualizaEmbarcacaoProjeto(this);
	});
	$('select[id="CMP_NU_EMPRESA"]').each(function (){
			atualizaEmbarcacaoProjeto(this);
		});
	
   
    //ArquivosOS
   //$.post('/LIB/ARQUIVOS_OS_GRUPO.php?NU_CANDIDATO='+nu_candidato, function(d){$('#ArquivosOS').html(d);});

    
})

var idEmpresa = "<?=$idEmpresa?>";
var idEmbarcacaoProjeto = "<?=$idEmbarcacaoProjeto?>";

//document.getElementById('botao1').focus();



function CarregarListaDeArquivos(pNU_CANDIDATO, pNomeDiv){
	var url = '../LIB/ARQUIVOS_LISTA.php?NU_CANDIDATO='+pNU_CANDIDATO+'&NomeDiv='+pNomeDiv;
	$.ajax({
		  url: url,
		  async:false,
		  success: function(data) {
			$('#'+pNomeDiv+pNU_CANDIDATO).html(data);
		  }
		});			
}

function enviar(tipo) {
  var cand = document.wilson.idCandidato.value;
  var emp = document.wilson.idEmpresa.value;
  var sol = document.wilson.NU_SOLICITACAO.value;
  var cd_admin = document.wilson.cd_admin.value;
  var pagina = "operacao3.php?idCandidato="+cand+"&idEmpresa="+emp+"&opvisa="+tipo+"&NU_SOLICITACAO="+sol+"&cd_admin="+cd_admin+"&A=";


  //alert("pagina="+pagina);
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
  document.wilson.action="operacao3.php";
//  window.open(pagina);
  var msg = OpenChild(tipo,pagina);
  document.all.aguarde.innerHTML = msg;
}

function arquivos(tipo) {
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
//  var msg = OpenChild(tipo,"operacao3up.php");
  var msg = OpenChild(tipo,"/admin/documentos1.php");
  document.all.aguarde.innerHTML = msg;
}

function formulario(tipo) {
  document.wilson.action="/formularios/all_registroPF.php";
  document.wilson.target="formPF";
  document.wilson.submit();
}

</script>
