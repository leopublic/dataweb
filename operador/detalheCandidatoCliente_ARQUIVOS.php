<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/libVisaDetail.php");

/*
 function exception_error_handler($errno, $errstr, $errfile, $errline ) {
 throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
 }
 set_error_handler("exception_error_handler");
 */

$msg = cINTERFACE::ProcessePost($_POST, $_GET, $_FILES, $_SESSION);

$index = 0;
$mensagem='';
if(isset($_GET['NU_CANDIDATO']))
{
	$pNU_CANDIDATO = $_GET['NU_CANDIDATO'];
	$cand = new cINTERFACE_CANDIDATO();

	$tela = new cEDICAO("vCANDIDATO_CONS");
	$tela->CarregarConfiguracao();
	
	if (cINTERFACE::CampoEnviado($_POST, $tela->mCampoCtrlEnvio)){
		// Obtem valores digitados
		cINTERFACE::RecupereValoresEdicao($tela, $_POST);
	
		// Salva 
		//$cand->SalvarEdicao($editaCandidato);
		if($tela->AtualizarBanco()){
			$cand->Recuperar($pNU_CANDIDATO);
			$ac = $cand->UltimaSolicitacao();
			$cand->CorrijaNomeCompleto();
			$cand->LogueUltimaAtualizacao();			
			$cand->VerifiqueCadastroCompleto();
			
			$msg = 'File added sucessfuilly';

			$tela->RecupereRegistro();
			$cand->Recuperar($pNU_CANDIDATO);
			
		}
		else{
			$cand->Recuperar($pNU_CANDIDATO);
			$ac = $cand->UltimaSolicitacao();
			$msg = 'We were not able to complete the request.<br>'.str_replace("'", "\'", mysql_error());
		}
	}
	else{   
		$cand->Recuperar($pNU_CANDIDATO);
		$ac = $cand->UltimaSolicitacao();
		$tela->mCampoNome['NU_CANDIDATO']->mValor =$pNU_CANDIDATO ;
		$tela->RecupereRegistro();
	}
}
else{
	$msg = "Candidato não informado";
}

if ($_SESSION['msg']!= '' ){
	$msg .= '<br/>'.$_SESSION['msg'];
	$_SESSION['msg'] = '';
}

$voltar = $_SESSION['voltar'];
if ($voltar == ''){
	$voltar = "os_listar.php";
}
// Cabecalho padrão
echo Topo($cand->mNOME_COMPLETO, '<style>div#tabsOS div{padding-left:10px;padding-right:10px;padding-bottom:10px;}</style>' , false);

?>
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $("#tabsInt").tabs();
    });
    
</script>
<div style="background-color:#fff;background-image:none;height:auto;padding-top:10px;padding-bottom:10px;text-align:left;padding-left:10px;"><img src="/imagens/logo_mundivisas_red.jpg" style="margin:0;padding:0;"/>
	<div style="padding-top:50px;padding-right:10px;float:right;vertical-align: bottom;"><?=$_SESSION['myAdmin']['nm_nome'];?></div>
</div>
<div class="conteudo">
	<div class="titulo">
		<div style="float:right">
			<input type="button" value="Back to the list" onclick="javascript:window.location='candListar.php';"/>
			<input type="button" value="Logout" onclick="javascript:window.location='/sair.php';"/>

		</div>
		<div style="float:left">Detailed information of "<?=$cand->mNOME_COMPLETO;?>"</div>&nbsp;
	</div>
	<div class="conteudoInterno">
		<div id="tabsOS" class="aba">
			<ul>
				<li><a href="detalheCandidatoCliente_ID.php?NU_CANDIDATO=<?=$pNU_CANDIDATO;?>">Personal data</a></li>
				<li><a href="detalheCandidatoCliente_VISTO.php?NU_CANDIDATO=<?=$pNU_CANDIDATO;?>">Visa</a></li>
				<li><a href="ARQUIVOS" class="ativo">Files & Docs</a></li>
			</ul>


			<div id="ARQUIVOS">
				<div id="containerVisualizacao"
					style="float: right; border: solid 1px #999999; padding: 3px; text-align: center;">
				</div>
				<form id="novoArquivo<?=$cand->mNU_CANDIDATO;?>" action="arquivoPostCliente.php" enctype="multipart/form-data" target="_blank" method="post">
			<?=cINTERFACE::inputHidden("formArquivo", "1");?>
			<?=cINTERFACE::inputHidden("NU_CANDIDATO", $cand->mNU_CANDIDATO);?>
			<?=cINTERFACE::inputHidden("NU_EMPRESA", $ac->mNU_EMPRESA);?>
					<div id="arquivosListados<?=$cand->mNU_CANDIDATO;?>"></div>
				</form>
				<script>
					function CarregarArquivo(pCaminho,pNU_SEQUENCIAL){
					  	var dimensoes = new Array();
						$.ajax({
							  url: '/LIB/imagemTam.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL,
							  async:false,
							  success: function(data) {
								dimensoes = data.split("#");
							
							  }
							});
						var pWidth = +dimensoes[0];
						var pHeight = +dimensoes[1];
						//alert('pWidth='+pWidth+';pHeight='+pHeight);
						var maxW = 400;
						var maxH = 300;
						var w = maxW;
						var h = maxH;
						if (pWidth>pHeight){
							h = Math.round(pHeight/pWidth * maxW);
						}
						else{
							w = Math.round(pWidth/pHeight * maxH);
						}
						var d = new Date();						
						$('#containerVisualizacao').html('<img src="'+pCaminho+'?'+d.getTime()+'"/><br/><input type="button" onclick="abreJanelaxxx(\'/LIB/editaImagem.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL+'\',\'wteste\');" value="Editar"/><input type="button" value="Atualizar" onclick="CarregarArquivo(\''+pCaminho+'\',\''+pNU_SEQUENCIAL+'\', '+pWidth+', '+pHeight+')"/>');	 
						$("#containerVisualizacao img").imagetool({
						    viewportWidth: w
						   ,viewportHeight: h
						  });
					}
				</script>

                <!--  <div id="ArquivosOS"> Carregando Arquivos agrupados por OS, Aguarde ...</div>-->
			</div>
		</div>
	</div>
</div>

<?php
if ($msg!='')
{
?>	
<script language="javascript">jAlert('<?=$msg;?>');</script>
<?
}
echo Rodape("");
?> 
<script language="javascript">

$(document).ready(function() {
    var nu_candidato = '<?=$cand->mNU_CANDIDATO;?>';
	CarregarListaDeArquivos('<?=$cand->mNU_CANDIDATO;?>','arquivosListados');
   
})

var idEmpresa = "<?=$idEmpresa?>";
var idEmbarcacaoProjeto = "<?=$idEmbarcacaoProjeto?>";

//document.getElementById('botao1').focus();



function CarregarListaDeArquivos(pNU_CANDIDATO, pNomeDiv){
	var url = '../LIB/ARQUIVOS_LISTA.php?NU_CANDIDATO='+pNU_CANDIDATO+'&NomeDiv='+pNomeDiv;
	$.ajax({
		  url: url,
		  async:false,
		  success: function(data) {
			$('#'+pNomeDiv+pNU_CANDIDATO).html(data);
		  }
		});			
}

function enviar(tipo) {
  var cand = document.wilson.idCandidato.value;
  var emp = document.wilson.idEmpresa.value;
  var sol = document.wilson.NU_SOLICITACAO.value;
  var cd_admin = document.wilson.cd_admin.value;
  var pagina = "operacao3.php?idCandidato="+cand+"&idEmpresa="+emp+"&opvisa="+tipo+"&NU_SOLICITACAO="+sol+"&cd_admin="+cd_admin+"&A=";


  //alert("pagina="+pagina);
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
  document.wilson.action="operacao3.php";
//  window.open(pagina);
  var msg = OpenChild(tipo,pagina);
  document.all.aguarde.innerHTML = msg;
}

function arquivos(tipo) {
  document.wilson.opvisa.value=tipo;
  document.wilson.target="_top";
//  var msg = OpenChild(tipo,"operacao3up.php");
  var msg = OpenChild(tipo,"/admin/documentos1.php");
  document.all.aguarde.innerHTML = msg;
}

function formulario(tipo) {
  document.wilson.action="/formularios/all_registroPF.php";
  document.wilson.target="formPF";
  document.wilson.submit();
}

</script>
