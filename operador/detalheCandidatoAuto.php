<?php
// TODO: Verificar diferenças em relaçã a versao de producao
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/libVisaDetail.php");


cHTTP::$FILES = $_FILES;
cHTTP::$GET = $_GET;
cHTTP::$POST = $_POST;
cHTTP::$SESSION = &$_SESSION;
if (isset($_GET['controller'])) {
    cHTTP::$controller = $_GET['controller'];
} elseif (isset($_POST['controller'])) {
    cHTTP::$controller = $_POST['controller'];
} else {
    cHTTP::$controller = '';
}
if (isset($_GET['MIME'])) {
    cHTTP::$MIME = $_GET['MIME'];
} else {
    cHTTP::$MIME = 'HTM';
}
if (isset($_GET['metodo'])) {
    cHTTP::$metodo = $_GET['metodo'];
} elseif (isset($_POST['metodo'])) {
    cHTTP::$metodo = $_POST['metodo'];
} else {
    cHTTP::$metodo = '';
}

/*
  function exception_error_handler($errno, $errstr, $errfile, $errline ) {
  throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
  }
  set_error_handler("exception_error_handler");
 */

$msg = cINTERFACE::ProcessePost($_POST, $_GET, $_FILES, $_SESSION);
$painelAtivo = 0;

if ($_POST['CMP___nomeTemplate'] == "cTEMPLATE_CANDIDATO_ACESSO_EXTERNO") {
    $painelAtivo = 4;
} elseif ($_POST['CMP___nomeEdicao'] == "cTEMPLATE_CANDIDATO_EDIT") {
    $painelAtivo = 3;
} elseif ($_POST['CMP___nomeTemplate'] == "cTMPL_DEPS_EDIT") {
    $painelAtivo = 6;
} else {
    if (isset($_GET['painelAtivo'])) {
        $painelAtivo = $_GET['painelAtivo'];
    }
}
$index = 0;
$mensagem = '';
if (isset($_GET['NU_CANDIDATO'])) {
    $pNU_CANDIDATO = $_GET['NU_CANDIDATO'];
    $cand = new cINTERFACE_CANDIDATO();

    $tela = new cEDICAO("vCANDIDATO");
    $tela->CarregarConfiguracao();

    if (cINTERFACE::CampoEnviado($_POST, $tela->mCampoCtrlEnvio)) {
        // Obtem valores digitados
        cINTERFACE::RecupereValoresEdicao($tela, $_POST);

        // Salva 
        //$cand->SalvarEdicao($editaCandidato);
        if ($tela->AtualizarBanco()) {
            $cand->Recuperar($pNU_CANDIDATO);
            $ac = $cand->UltimaSolicitacao();
            $cand->CorrijaNomeCompleto();
            $cand->VerifiqueCadastroCompleto();

            $msg = 'Identificação do candidato atualizada com sucesso';

            $tela->RecupereRegistro();
            $cand->Recuperar($pNU_CANDIDATO);
        } else {
            $cand->Recuperar($pNU_CANDIDATO);
            $ac = $cand->UltimaSolicitacao();
            $msg = 'Não foi possível atualizar o candidato.<br>' . str_replace("'", "\'", mysql_error());
        }
    } else {
        $cand->Recuperar($pNU_CANDIDATO);
        $ac = $cand->UltimaSolicitacao();
        $tela->mCampoNome['NU_CANDIDATO']->mValor = $pNU_CANDIDATO;
        $tela->RecupereRegistro();
    }
    
    $sql = "select * from candidato_tmp where nu_candidato = ".$pNU_CANDIDATO;
    $res = cAMBIENTE::$db_pdo->query($sql);
    $rs = $res->fetch();
    if ($rs && $rs['nu_candidato_tmp'] > 0){
        $nu_candidato_tmp = $rs['nu_candidato_tmp'];
    } else {
        $nu_candidato_tmp = 0;
    }
    
} else {
    $msg = "Candidato não informado";
}

$voltar = $_SESSION['voltar'];
if ($voltar == '') {
    $voltar = "os_listar.php";
}
// Cabecalho padrão
echo Topo($cand->mNOME_COMPLETO);
echo Menu("SOL");

$qtdH = $cand->QtdHomonimos();
if ($qtdH > 0) {
    $alertaH = '&nbsp;&nbsp;&nbsp;&nbsp;<button onclick="javascript:window.open(\'processoMigrarCandidato.php?NU_CANDIDATO=' . $cand->mNU_CANDIDATO . '\');"><span style="font-weight:bold;color:#000000;">ATENÇÃO: Candidato com processos no arquivo morto</span></button>';
}
?>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabsInt").tabs();
    });

</script>
<div class="conteudo">
        <!--<div class="titulo"><div style="float: left">Candidato "<?= $cand->mNOME_COMPLETO; ?>"&nbsp;(<?= $cand->mNU_CANDIDATO; ?>)&nbsp;<?= $alertaH; ?></div></div>
    -->
<?
print $cand->FormCabecalho();
?>

    <div style="border-bottom: solid 1px #cccccc; padding-bottom: 1px; padding-left: 1px; color: #cccccc">
        <a href="../intranet/geral/<?= $voltar; ?>" style="color: #999999; text-decoration: underline">voltar para listagem</a>
    </div>
    <div class="conteudoInterno">
        <div id="tabsOS">
            <ul>
                <li><a href="#DadosOS" class="ativo">Perfil do candidato</a></li>
                <? if ($nu_candidato_tmp > 0){ ?>
                <li><a href="/paginas/candidato_comparar_externo.php?NU_CANDIDATO=<?= $pNU_CANDIDATO; ?>&nu_candidato_tmp=<?=$nu_candidato_tmp;?>">Verificar atualização de cadastro</a></li>
                <?}?>
            </ul>
            <div id="DadosOS">

                <div id="accordionCand">

                    <h3 class="interno">Visto atual</h3>

                    <div>
<?
$qtd = $cand->QtdOutrosProcessosOrfaos();
if ($qtd > 0) {
    print '<p class="alerta">Atenção: Este candidato possui processos que não foram automaticamente associados a nenhum visto. Verifique na aba "Processos órfãos" no painel "Outros vistos" ao final desta página.</p>';
}
print $cand->FormObservacaoProcessos($_POST, $_GET);
try{
    print $cand->FormVistoAtual();
} catch(Exception $e){
    print 'Não foi possível carregar o visto pois: '.$e->getMessage();
}
print $cand->FormCPF();
$qtd = $cand->QtdOutrosServicos();
if ($qtd == '0') {
    $qtd = ' (nenhum encontrado)';
} else {
    $qtd = ' (' . $qtd . ' encontrado(s))';
}
?>
                    </div>		

                    <h3 class="interno">Outros serviços<?= $qtd; ?></h3>

                    <div>
                        <?
                        print $cand->FormOutrosProcessos();
                        ?>
                    </div>
                        <?
                        $qtd = $cand->QtdOutrosVistos();
                        if (intval($qtd) == '0') {
                            $comentario .= 'nenhum visto';
                        } else {
                            $s = '';
                            if (intval($qtd) > 1) {
                                $s = 's';
                            }
                            $comentario .= $qtd . ' visto' . $s;
                            $qtd = $cand->QtdOutrosProcessos();
                            if (intval($qtd) == '0') {
                                $comentario .= ', nenhum processo';
                            } else {
                                $s = '';
                                if (intval($qtd) > 1) {
                                    $s = 's';
                                }
                                $comentario .= ', ' . $qtd . ' processo' . $s;
                            }
                        }
                        $qtd = $cand->QtdOutrosProcessosOrfaos();
                        if (intval($qtd) == '0') {
                            $comentario .= ', nenhum processo órfão';
                        } else {
                            $s = '';
                            if (intval($qtd) > 1) {
                                $s = 's';
                            }
                            $comentario .= ', ' . $qtd . ' processo' . $s . ' órfão' . $s;
                        }

                        $comentario = $comentario;
                        ?>
                    <h3 class="interno">Outros vistos: <?= $comentario; ?></h3>
                    <div><?php
                    // Montagem do histórico das solicitações anteriores
                    $tabs = '';
                    $sql = " SELECT * from processo_mte ";
                    $sql .= " WHERE cd_candidato = " . $cand->mNU_CANDIDATO;
                    $sql .= "   AND (fl_visto_atual = 0 OR fl_visto_atual is null)";
                    $sql .= "   AND (fl_vazio  = 0 or fl_vazio is null)";
                    $sql .= "   and (nu_servico not in (49,31) or nu_servico is null) ";
                    $sql .= " ORDER BY codigo desc";
                    $rs = mysql_query($sql);
                    while ($rw = mysql_fetch_array($rs)) {
                        if ($rw['nu_processo'] == '') {
                            $processo = '--';
                        } else {
                            $processo = $rw['nu_processo'];
                        }
                        $tabs .= '<li><a href="/LIB/VISTO_VIEW.php?codigo=' . $rw['codigo'] . '"><span>' . $processo . '</span></a></li>';
                    }

                    if ($qtd > 0) {
                        $tabs .= '<li><a href="/LIB/PROCESSOS_ORFAOS.php?NU_CANDIDATO=' . $cand->mNU_CANDIDATO . '"><span>Processos orfãos</span></a></li>';
                    }
                    if ($tabs != '') {
                        $tabs = '<div id="tabsInt"><ul>' . $tabs . '</ul></div>';
                    } else {
                        $tabs = '(nenhum visto encontrado)';
                    }
                    print $tabs;
                    ?>
                    </div>
                    <h3 class="interno">Identificação do Candidato</h3>
                    <div>
                        <?
                        $x = new cCTRL_CANDIDATO_DADOS_PESSOAIS_EDIT();
                        print $x->Edite();
                        if ($x->get_houvePost()) {
                            $painelAtivo = 3;
                        }
                        ?>    
                    </div>

                    <h3 class="interno">Acesso externo</h3>
                    <div>
                        <?
                        $x = new cCTRL_CANDIDATO_ACESSO_EXTERNO_EDIT();
                        print $x->Edite();
                        if ($x->get_houvePost()) {
                            $painelAtivo = 4;
                        }
                        ?>    
                    </div>

                    <h3 class="interno">Histórico de alterações</h3>
                    <div>
                        <?php
                        $x = new cCTRL_CAND_HISTORICO();
                        print $x->Liste();
                        ?>
                    </div>

                    <h3 class="interno">Dependentes</h3>
                    <div>
<?php
// Processa a edição se houve post
$x = new cCTRL_DEPS_EDIT();
$edicao = $x->Edite();

$x = new cCTRL_DEPENDENTE_LIST();
print $x->Liste();
?>
                        <?php
                        print $edicao;
                        ?>
                    </div>

                    <h3 class="interno">Arquivos físicos</h3>
                    <div>
                        <?php
                        $x = new cCTRL_CAND_DOCS_EDIT();
                        print $x->Edite();
                        ?>
                    </div>
                </div>
                <div id="accordionCand2">
                    <h3 class="interno" id="pnlArquivos">Arquivos<?= $qtdArqs; ?></h3>
                    <div>
                        <div id="containerVisualizacao" style="float: right; border: solid 1px #999999; padding: 3px; text-align: center;"></div>
                        <div id="arquivosListados">
                        <?php
                        $x = new cCTRL_ARQUIVO();
                        print $x->arquivosDoCandidato();
                        ?>
                        </div>
                    </div>
                </div>
    </div>
        </div>
    </div>
    <div id="formPopup" name="formPopup" style="display:none">
        vazio
    </div>

<?php
if (cHTTP::$SESSION['msg'] != '') {
    $msg = cHTTP::$SESSION['msg'];
}


if ($msg != '') {
    ?>	
        <script language="javascript">jAlert('<?= $msg; ?>');</script>
    <?
}
echo Rodape("");
echo cHTTP::ScriptsOnLoad();
?> 
    <script language="javascript">
        $(document).ready(function () {
            $('#processos').sortable({
                axis: 'y'
                , handle : 'div.tituloProcesso'
                , update : function(event, ui){
                    $('#processos').css('background-color', '#ffeeaa');
                    var x = $('#processos').sortable('toArray');
                    $.get( "/paginas/carregue.php?controller=cCTRL_PROCESSO&metodo=reordenarProcessos&ordem="+x)
                    .done(function( data ) {
                        if (data == ''){
                        $( "#processos" ).animate({
                            backgroundColor: "#8de580"
                          }, 200 );
                        $( "#processos" ).animate({
                            backgroundColor: "#fff"
                          }, 1000 );
                        } 
                });
                }
            });
        });

        $(document).ready(function () {

            var x = $("#tabsOS").tabs({
                heightStyle: "content",
                beforeActivate: function (event, ui) {
                    if (ui.newTab.context.href.substr(0, 4) == 'http') {
                        window.location.href = ui.newTab.context.href;
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            });

            var nu_candidato = '<?= $cand->mNU_CANDIDATO; ?>';
            $("#accordionCand").accordion({heightStyle: "content", collapsible: true, active: <?= $painelAtivo; ?>});
            CarregarListaDeArquivos('<?= $cand->mNU_CANDIDATO; ?>', 'arquivosListados');
            $("#accordionCand2").accordion({heightStyle: "content", collapsible: true, active: 0});


        });

        var idEmpresa = "<?= $idEmpresa ?>";
        var idEmbarcacaoProjeto = "<?= $idEmbarcacaoProjeto ?>";

        function CarregarListaDeArquivos(pNU_CANDIDATO, pNomeDiv) {
            var url = '../LIB/ARQUIVOS_LISTA.php?NU_CANDIDATO=' + pNU_CANDIDATO + '&NomeDiv=' + pNomeDiv;
            $.ajax({
                url: url,
                async: false,
                success: function (data) {
                    $('#' + pNomeDiv + pNU_CANDIDATO).html(data);
                }
            });
        }
        function CarregarArquivo(pCaminho, pNU_SEQUENCIAL, pEhOS) {
            var dimensoes = new Array();
            $.ajax({
                url: '/LIB/imagemTam.php?NU_SEQUENCIAL=' + pNU_SEQUENCIAL,
                async: false,
                success: function (data) {
                    dimensoes = data.split("#");
                }
            });
            var pWidth = +dimensoes[0];
            var pHeight = +dimensoes[1];
            //alert('pWidth='+pWidth+';pHeight='+pHeight);
            var maxW = 400;
            var maxH = 300;
            var w = maxW;
            var h = maxH;
            if (pWidth > pHeight) {
                h = Math.round(pHeight / pWidth * maxW);
            }
            else {
                w = Math.round(pWidth / pHeight * maxH);
            }
            var d = new Date();
            if (pEhOS) {
                $link = 'editaImagem.php';
            }
            else {
                $link = 'editaImagemOS.php';
            }
            $('#containerVisualizacao').html('<img src="' + pCaminho + '?' + d.getTime() + '" style="width:' + w + 'px;height:' + h + 'px;"/><br/><input type="button" onclick="abreJanelaxxx(\'/LIB/' + $link + '?NU_SEQUENCIAL=' + pNU_SEQUENCIAL + '\',\'wteste\');" value="Editar"/><input type="button" value="Atualizar" onclick="CarregarArquivo(\'' + pCaminho + '\',\'' + pNU_SEQUENCIAL + '\', ' + pWidth + ', ' + pHeight + ')"/>');
            $("#containerVisualizacao img").imagetool({
                viewportWidth: w
                , viewportHeight: h
            });
        }


        function arquivos(tipo) {
            document.wilson.opvisa.value = tipo;
            document.wilson.target = "_top";
            //  var msg = OpenChild(tipo,"operacao3up.php");
            var msg = OpenChild(tipo, "/admin/documentos1.php");
            document.all.aguarde.innerHTML = msg;
        }

        function formulario(tipo) {
            document.wilson.action = "/formularios/all_registroPF.php";
            document.wilson.target = "formPF";
            document.wilson.submit();
        }

    </script>
