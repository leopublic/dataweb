<?php
function montaComboStatusSolicitacao($codigo,$extra) {
  return montaCombo("STATUS_SOLICITACAO","ID_STATUS_SOL","NO_STATUS_SOL",$codigo,$extra);
}
function montaComboServico($codigo,$extra) {
  return montaCombo("TIPO_SERVICO TS JOIN SERVICO S ON S.NU_TIPO_SERVICO = TS.NU_TIPO_SERVICO","NU_SERVICO","concat(CO_TIPO_SERVICO , concat('.', concat(CO_SERVICO, concat(' ', NO_SERVICO))))",$codigo,$extra);
}

function montaComboEscolaridade($codigo,$extra) {
  return montaCombo("ESCOLARIDADE","CO_ESCOLARIDADE","NO_ESCOLARIDADE",$codigo,$extra);
}

function montaComboEstadoCivil($codigo,$extra) {
  return montaCombo("ESTADO_CIVIL","CO_ESTADO_CIVIL","NO_ESTADO_CIVIL",$codigo,$extra);
}

function montaComboFuncoes($codigo,$extra) {
  return montaCombo("FUNCAO_CARGO","CO_FUNCAO","NO_FUNCAO",$codigo,$extra);
}

function montaComboGrauParentesco($codigo,$extra) {
  return montaCombo("GRAU_PARENTESCO","CO_GRAU_PARENTESCO","NO_GRAU_PARENTESCO",$codigo,$extra);
}

function montaComboPais($codigo,$extra) {
  $extra = $extra." ORDER BY NO_PAIS";
  return montaCombo("PAIS_NACIONALIDADE","CO_PAIS","NO_PAIS",$codigo,$extra);
}

function montaComboNacionalidade($codigo,$extra) {
  $extra = $extra." ORDER BY NO_NACIONALIDADE";
  return montaCombo("PAIS_NACIONALIDADE","CO_PAIS","NO_NACIONALIDADE",$codigo,$extra);
}

function montaComboPaisNacionalidade($codigo,$extra) {
  $extra = $extra." ORDER BY NO_PAIS";
  return montaCombo("PAIS_NACIONALIDADE","CO_PAIS","concat(NO_PAIS,' - ',NO_NACIONALIDADE)",$codigo,$extra);
}

function montaComboProfissoes($codigo,$extra) {
  return montaCombo("PROFISSAO","CO_PROFISSAO","NO_PROFISSAO",$codigo,$extra);
}

function montaComboReparticoes($codigo,$extra) {
  $extra = $extra." WHERE CO_REPARTICAO_CONSULAR>999 ORDER BY NO_REPARTICAO_CONSULAR";
  return montaCombo("REPARTICAO_CONSULAR","CO_REPARTICAO_CONSULAR","NO_REPARTICAO_CONSULAR",$codigo,$extra);
}

function montaComboTipoAutorizacao1($codigo,$extra) {
  return montaCombo("TIPO_AUTORIZACAO","CO_TIPO_AUTORIZACAO","NO_TIPO_AUTORIZACAO",$codigo,$extra);
}
function montaComboTipoAutorizacao2($codigo,$extra) {
  return montaCombo("TIPO_AUTORIZACAO","CO_TIPO_AUTORIZACAO","NO_REDUZIDO_TIPO_AUTORIZACAO",$codigo,$extra);
}

function montaComboTipoSolicitacao($codigo,$extra) {
  return montaCombo("TIPO_SOLICITACAO","CO_TIPO_SOLICITACAO","NO_TIPO_SOLICITACAO",$codigo,$extra);
}

function montaComboUsuarios($codigo,$extra) {
  return montaCombo("usuarios","cd_usuario","nome",$codigo,$extra);
}

function montaComboUsuarioSuperior($codigo,$perfil,$extra) {
  if(strlen($perfil)>0) {
    $extra = "WHERE cd_perfil BETWEEN 2 AND 4 AND cd_perfil<$perfil ".$extra;
  } else {
    $extra = "WHERE cd_perfil BETWEEN 2 AND 4 ".$extra;
  }
  return montaCombo("usuarios","cd_usuario","nome",$codigo,$extra);
}

function montaComboUsuarioTecnico($codigo,$extra) {
  $extra = "WHERE cd_perfil in (5,3) ".$extra;
  return montaCombo("usuarios","cd_usuario","nome",$codigo,$extra);
}

function montaComboArmador($codigo,$extra) {
  $extra = "WHERE cd_perfil in (SELECT cd_perfil FROM usuario_perfil WHERE eh_armador='S') $extra";
  return montaCombo("usuarios","cd_usuario","nome",$codigo,$extra);
}

function montaComboPerfis($codigo,$extra) {
  return montaCombo("usuario_perfil","cd_perfil","nm_perfil",$codigo,$extra);
}

function montaComboClassVisto($cod) {
  $ret = "";
  if($cod=="1") {   $ret = "<option value='1' selected>Permanente<option value='2'>Temporário"; }
  elseif($cod=="2") {   $ret = "<option value='1'>Permanente<option value='2' selected>Temporário"; }
  else { $ret = "<option value='1'>Permanente<option value='2'>Temporário"; }
  return $ret;
}

function montaComboTransEntrada($cod) {
  $ret = "";
  if($cod=="1") {   $ret = "<option value='1' selected>Navio<option value='2'>Aviao<option value='3'>Onibus<option value='4'>Outros"; }
  elseif($cod=="2") {   $ret = "<option value='1'>Navio<option value='2' selected>Aviao<option value='3'>Onibus<option value='4'>Outros"; }
  elseif($cod=="3") {   $ret = "<option value='1'>Navio<option value='2' selected>Aviao<option value='3'>Onibus<option value='4'>Outros"; }
  elseif($cod=="4") {   $ret = "<option value='1'>Navio<option value='2'>Aviao<option value='3'>Onibus<option value='4' selected>Outros"; }
  else { $ret = "<option value='1'>Navio<option value='2'>Aviao<option value='3'>Onibus<option value='4'>Outros"; }
  return $ret;
}

function montaCombo($tabela,$indice,$nome,$codigo,$extra) {
  $ret = "";
  $sql = "SELECT $indice,$nome from $tabela $extra ";
  $rs = mysql_query($sql);
  if(mysql_errno()>0) { 
     print "\n<!--\n ERRO: ".mysql_error()."\n SQL=$sql\n-->\n"; 
  } else {
   while($rw=mysql_fetch_array($rs)) {
    if( (strlen($codigo)>0) && ($rw[0]==$codigo) ) { $sel = "selected"; } else { $sel=""; }
    $ret = $ret."<option value='".$rw[0]."' $sel>".trim(substr($rw[1],0,50));
   }
  }
  return $ret;
}


function montaComboJquery($tabela,$indice,$nome,$codigo,$extra) {
	$ret = "";
	$sql = "SELECT $indice,$nome from $tabela $extra ";
	$rs = mysql_query($sql);
	if(mysql_errno()>0) { 
		print "\n<!--\n ERRO: ".mysql_error()."\n SQL=$sql\n-->\n"; 
	} else {
		$virg ='';
		while($rw=mysql_fetch_array($rs)) {
			if( (strlen($codigo)>0) && ($rw[0]==$codigo) ) { $sel = "selected"; } else { $sel=""; }
			$ret = $ret."{ value:'".$rw[0]."' , label:'".$rw[1]."'}";
			$virg = ',';
		}
	}
	return $ret;
}


function montaComboEmpresas($codigo,$extra) {
  return montaCombo("EMPRESA","NU_EMPRESA","NO_RAZAO_SOCIAL",$codigo,$extra);
}

function montaComboCandidatos($codigo,$extra) { # VERIFICAR QUE NAO TEM COLUNA NU_EMPRESA
  return montaCombo("CANDIDATO","NU_CANDIDATO","concat(NO_PRIMEIRO_NOME,' ',NO_NOME_MEIO,' ',NO_ULTIMO_NOME) as NOME",$codigo,$extra);
}

function montaComboCandidatosEmpresa($codigo,$empresa,$extra) { 
  $ret = "";
  $sql = "select C.NU_CANDIDATO, concat(C.NO_PRIMEIRO_NOME,' ',NO_NOME_MEIO,' ',NO_ULTIMO_NOME) as NOME_CANDIDATO ";
  $sql = $sql."from CANDIDATO C, AUTORIZACAO_CANDIDATO AC  where C.NU_CANDIDATO = AC.NU_CANDIDATO and AC.NU_EMPRESA=$empresa  $extra";
  $rs = mysql_query($sql);
  while($rw=mysql_fetch_array($rs)) {
    if( (strlen($codigo)>0) && ($rw[0]==$codigo) ) { $sel = "selected"; } else { $sel=""; }
    $ret = $ret."<option value='".$rw[0]."' $sel>".$rw[1];
  }
  return $ret;
}

function montaComboCandidatosSolicitacao($codigo,$pID_SOLICITA_VISTO,$extra) { 
  $ret = "";
  $sql = "select C.NU_CANDIDATO, NOME_COMPLETO";
  $sql = $sql." from CANDIDATO C, AUTORIZACAO_CANDIDATO AC  where C.NU_CANDIDATO = AC.NU_CANDIDATO and AC.id_solicita_visto=$pID_SOLICITA_VISTO  $extra";
  $rs = mysql_query($sql);
  while($rw=mysql_fetch_array($rs)) {
    if( (strlen($codigo)>0) && ($rw[0]==$codigo) ) { $sel = "selected"; } else { $sel=""; }
    $ret = $ret."<option value='".$rw[0]."' $sel>".$rw[1];
  }
  return $ret;
}

function montaComboProjetos($codigo,$empresa,$extra, $order = '' ) {
  return montaComboPorEmpresa("EMBARCACAO_PROJETO","NU_EMBARCACAO_PROJETO","NO_EMBARCACAO_PROJETO",$codigo,$empresa,$extra, $order);
}

function montaComboProjetosAtivos($codigo,$empresa,$extra) {
  return montaComboPorEmpresa("EMBARCACAO_PROJETO","NU_EMBARCACAO_PROJETO","NO_EMBARCACAO_PROJETO",$codigo,$empresa," and  embp_fl_ativo =1 order by NO_EMBARCACAO_PROJETO");
}

function montaComboProcurador($codigo,$empresa,$extra) {
	return montaComboPorEmpresa("PROCURADOR_EMPRESA","NU_PROCURADOR","NO_PROCURADOR",$codigo,$empresa,$extra);
}

function montaComboPorEmpresa($tabela,$indice,$nome,$codigo,$empresa,$extra) {
  $ret = "";
  $sql = "SELECT $indice,$nome from $tabela WHERE NU_EMPRESA=$empresa $extra";
  $rs = conectaQuery($sql, __FILE__.'->'.__FUNCTION__);
  while($rw=mysql_fetch_array($rs)) {
    if( (strlen($codigo)>0) && ($rw[0]==$codigo) ) { $sel = "selected"; } else { $sel=""; }
    $ret = $ret."<option value='".$rw[0]."' $sel>".$rw[1]."</option>";
  }
  return $ret;
}

function retornaSelect($v1,$v2) {
  $ret = "";
  if( strtoupper($v1) == strtoupper($v2) ) { $ret=" selected "; }
  return $ret;
}

function montaComboEstados($uf) {
  $ret = "<option value='AC' ".retornaSelect($uf,"AC").">Acre</option>\n";
  $ret = $ret."<option value='AL' ".retornaSelect($uf,"AL").">Alagoas</option>\n";
  $ret = $ret."<option value='AM' ".retornaSelect($uf,"AM").">Amazonas</option>\n";
  $ret = $ret."<option value='AP' ".retornaSelect($uf,"AP").">Amap&aacute;</option>\n";
  $ret = $ret."<option value='BA' ".retornaSelect($uf,"BA").">Bahia</option>\n";
  $ret = $ret."<option value='CE' ".retornaSelect($uf,"CE").">Cear&aacute;</option>\n";
  $ret = $ret."<option value='DF' ".retornaSelect($uf,"DF").">Distrito Federal</option>\n";
  $ret = $ret."<option value='ES' ".retornaSelect($uf,"ES").">Esp&iacute;rito Santo</option>\n";
  $ret = $ret."<option value='GO' ".retornaSelect($uf,"GO").">Goi&aacute;s</option>\n";
  $ret = $ret."<option value='MA' ".retornaSelect($uf,"MA").">Maranh&atilde;o</option>\n";
  $ret = $ret."<option value='MG' ".retornaSelect($uf,"MG").">Minas Gerais</option>\n";
  $ret = $ret."<option value='MS' ".retornaSelect($uf,"MS").">Mato Grosso do Sul</option>\n";
  $ret = $ret."<option value='MT' ".retornaSelect($uf,"MT").">Mato Grosso</option>\n";
  $ret = $ret."<option value='PA' ".retornaSelect($uf,"PA").">Par&aacute;</option>\n";
  $ret = $ret."<option value='PB' ".retornaSelect($uf,"PB").">Para&iacute;ba</option>\n";
  $ret = $ret."<option value='PE' ".retornaSelect($uf,"PE").">Pernambuco</option>\n";
  $ret = $ret."<option value='PI' ".retornaSelect($uf,"PI").">Piau&iacute;</option>\n";
  $ret = $ret."<option value='PR' ".retornaSelect($uf,"PR").">Paran&aacute;</option>\n";
  $ret = $ret."<option value='RJ' ".retornaSelect($uf,"RJ").">Rio de Janeiro</option>\n";
  $ret = $ret."<option value='RN' ".retornaSelect($uf,"RN").">Rio Grande do Norte</option>\n";
  $ret = $ret."<option value='RO' ".retornaSelect($uf,"RO").">Rond&ocirc;nia</option>\n";
  $ret = $ret."<option value='RR' ".retornaSelect($uf,"RR").">Roraima</option>\n";
  $ret = $ret."<option value='RS' ".retornaSelect($uf,"RS").">Rio Grande do Sul</option>\n";
  $ret = $ret."<option value='SC' ".retornaSelect($uf,"SC").">Santa Catarina</option>\n";
  $ret = $ret."<option value='SE' ".retornaSelect($uf,"SE").">Sergipe</option>\n";
  $ret = $ret."<option value='SP' ".retornaSelect($uf,"SP").">S&atilde;o Paulo</option>\n";
  $ret = $ret."<option value='TO' ".retornaSelect($uf,"TO").">Tocantins</option>\n";
  return $ret;
}
