<?php

function MontaCalendario($diastamp,$calend) {
  $dia = date("d",$diastamp);
  $mes = date("m",$diastamp);
  $ano = date("Y",$diastamp);
  $ultmes = date("t",$diastamp);
  $descmes = NomeMes($mes);
  $contday = 1;
  $auxday = mktime(0,0,0,$mes,$contday,$ano);
  $antmes = mktime(0,0,0,$mes-1,$dia,$ano);
  $promes = mktime(0,0,0,$mes+1,$dia,$ano);
  $ret = "";
  $ret = $ret."<table width=240 border=1 cellspacing=0 cellpadding=0 class=textoPretoPeq bgcolor=#ffffff>\n";
  $ret = $ret."<tr><td width=100% colspan=7>\n";
  $ret = $ret."<table border=0 width=100% cellspacing=0 cellpadding=0 class=textoPretoPeq bgcolor=#0030D0>\n";
  $ret = $ret."<td align=center><a href='javascript:CalNovaData($antmes);'>";
  $ret = $ret."<img src='/images/cal_ant.gif' border=0></a></td>\n";
  $ret = $ret."<td colspan=5 align=center><b><font color=white>$descmes de $ano</td>\n";
  $ret = $ret."<td align=center><a href='javascript:CalNovaData($promes);'>";
  $ret = $ret."<img src='/images/cal_pos.gif' border=0></a></td>\n";
  $ret = $ret."</table></td></tr>\n";
  $ret = $ret."<tr><td align=center><b>D</td><td align=center><b>S</td>\n";
  $ret = $ret."<td align=center><b>T</td><td align=center><b>Q</td>\n";
  $ret = $ret."<td align=center><b>Q</td><td align=center><b>S</td>\n";
  $ret = $ret."<td align=center><b>S</td>\n</tr>\n";
  while ($contday<=$ultmes) {
    $ret = $ret."<tr>";
    for ($x=0;$x<7;$x++) {
      if( (date("w",$auxday)==$x) && ($contday<=$ultmes) ){
        if(date("d",$auxday)==$dia) {
          $ret = $ret."<td align=center bgcolor=#0030D0><b><font color=#FFFFFF>";
          $ret = $ret."<a href='javascript:CalNovaData($auxday);' class=textoPretoPeq>";
          $ret = $ret."<font color=white>".date("d",$auxday)."</a></td>\n";
        } else {
          if($calend[$contday]>0) {
            $ret = $ret."<td align=center bgcolor=#DDDDDD><a href='javascript:CalNovaData($auxday);' class=texto>";
            $ret = $ret."<font color=black>".date("d",$auxday)."</a></td>\n";
          } else {
            $ret = $ret."<td align=center bgcolor=#FFFFFF><a href='javascript:CalNovaData($auxday);' class=texto>";
            $ret = $ret."<font color=black>".date("d",$auxday)."</a></td>\n";
          }
        }
        $contday++;
        $auxday = mktime(0,0,0,$mes,$contday,$ano);
      } else {
        $ret = $ret."<td>&#160;</td>\n";
      }
    }
    $ret = $ret."</tr>\n";
  }
  $ret = $ret."</table>\n";
  return $ret;
}

function PegaDiasCal($mes,$ano,$tabela,$coluna,$extra) {
  $calend[0] = 0;;
  $ultday = date("t",mktime(0,0,0,$mes,1,$ano));
  for($x=1;$x<=$ultday;$x++) {
    $hoje = date("Y-m-d",mktime(0,0,0,$mes,$x,$ano));
    $sql = "SELECT count(*) FROM $tabela WHERE $coluna='$hoje' $extra";
    $rs = mysql_query($sql);
    if($rw = mysql_fetch_array($rs)) {
      $aux = $rw[0];
      $calend[$x]=$aux;
    }
  }
  return $calend;
}

function NomeMes($mes) {
  $aux = array('Janeiro'=>1,'Fevereiro'=>2,'Marco'=>3,'Abril'=>4,'Maio'=>5,'Junho'=>6,'Julho'=>7,'Agosto'=>8,'Setembro'=>9,'Outubro'=>10,'Novembro'=>11,'Dezembro'=>12);
  $ret = array_search($mes,$aux);
  return $ret;
}

function PegaData() {
  $dia = $_POST['novadata'];
  if(strlen($dia)==0) {
    $dia = mktime(0,0,0,date("m"),date("d"),date("Y"));
  }
  return $dia;
}

function MontaJSCalendario($pagina,$usuario) {
  $ret = "<script language='JavaScript'>\n";
  $ret = $ret."function CalNovaData(dia) {\n";
  $ret = $ret."document.Chama.action='$pagina';\n";
  $ret = $ret."document.Chama.novadata.value=dia;\n";
  $ret = $ret."document.Chama.submit();\n";
  $ret = $ret."}\n";
  $ret = $ret."</script>\n";
  $ret = $ret."<form name=Chama method=post>\n";
  $ret = $ret."<input type=hidden name=novadata>\n";
  $ret = $ret."<input type=hidden name=attipo>\n";
  $ret = $ret."<input type=hidden name=atstatus>\n";
  $ret = $ret."<input type=hidden name=ax_admin value='$usuario'>\n";
  $ret = $ret."</form>\n";
  return $ret;
}

?>
