<?php

function PegaCal($mes,$ano,$extra) {
  $tabela = "agenda_usuario";
  $coluna = "dia";
  $calend = PegaDiasCal($mes,$ano,$tabela,$coluna,$extra);
  return $calend;
}

function AgendaDoDia($dia,$axadmin) {
  $hoje = date("Y-m-d",$dia);
  $axhoje = date("d/m/Y",$dia);
  $mes = date("m",$dia);
  $areaold = "";
  $seq = 0;
  $sql = "SELECT cd_agenda, cd_usuario, dia, hora, cd_adm_ult, ds_agenda, cd_status, date(dt_ult) as dt,time(dt_ult) as hs ";
  $sql = $sql ." FROM agenda_usuario WHERE dia='$hoje' AND cd_usuario=$axadmin ORDER BY cd_status, hora, cd_agenda";
  $rs = mysql_query($sql);
  $ret = $ret."<table width=100% border=0 class='textoazulpeq' cellspacing=0 cellpadding=0>\n";
  $ret = $ret."<tr><td colspan=5 align=center><h4><b>Agenda do dia $axhoje </h4></td></tr>\n";
  if(mysql_num_rows($rs)>0) {
    $ret = $ret."<tr><td align=center width=50><b>HORA</td>\n";
    $ret = $ret."<td align=center width=80><b>Status</td>\n";
    $ret = $ret."<td align=center width=320><b>Tarefa</td>\n";
    $ret = $ret."<td align=center width=120><b>Admin</td>\n";
    $ret = $ret."<td align=center width=130><b>Atualizado</td></tr>\n";
    while($rw = mysql_fetch_array($rs)) {
      $seq++;
      $cod1 = $rw["cd_agenda"];
      $dia1 = dataMy2BR($rw["dia"]);
      $hora1 = $rw["hora"];
      $cd_usuario = pegaNomeUsuario($rw["cd_usuario"]);
      $cd_adm_cad = pegaNomeUsuario($rw["cd_adm_ult"]);
      $tipo1 = $rw["cd_status"];
      $texto1 = $rw["ds_agenda"];
      $dt = dataMy2BR($rw['dt']);
      $hs = $rw['hs'];
      $aux="&#160;";
      $tarefa = str_replace(chr(10),chr(92).chr(110),$texto1);
      $ret = $ret."<tr><td align=center><a href=\"JavaScript:Altera('$cod1','$hora1','$tarefa','$tipo1');\" class=texto>";
      $ret = $ret."<font color=red>$hora1:00</a></td>\n";
      $ret = $ret."<td align=center>".NomeTipos($tipo1)."</td>\n";
      $ret = $ret."<td align=left>$tarefa</td>";
      $ret = $ret."<td align=left>".$cd_adm_cad."</td>";
      $ret = $ret."<td align=center nowrap>$dt $hs</td>";
      $ret = $ret."<tr>\n";
    }
  } else {
    $ret = $ret."<tr><td width=100% align=center>Nao existe registro para este dia.</td></tr>";
  }
  $ret = $ret."</table>";
  return $ret;
}

function GeraHoras() {
  $x = 0;
  while($x < 23) {
    if($x < 10) { $y = "0$x"; } else { $y = $x; }
    $ret = $ret."<option value='$x'>$y:00</option>";
    $x++;
  }
  return $ret;
}

function GeraTipos() {
  $ret = "";
  $ret = $ret."<option value='C'>Cadastrado\n";
  $ret = $ret."<option value='E'>Executando\n";
  $ret = $ret."<option value='F'>Finalizado\n";
  return $ret;
}

function NomeTipos($tipo) {
  $ret = "";
  if($tipo=="C") { $ret = "Cadastrado"; }
  if($tipo=="E") { $ret = "Executando"; }
  if($tipo=="F") { $ret = "Finalizado"; }
  return $ret;
}

function InsereAgenda($cd_usuario,$cd_admin,$dia1,$hora,$cd_status,$texto) {
  $codigo = GeraCodigo('agenda_usuario','cd_agenda');
  $dia2 = date("Y-m-d",$dia1);
  $sql = "insert into agenda_usuario (cd_agenda,cd_usuario,cd_adm_cad,cd_adm_ult,dia,hora,cd_status,ds_agenda,dt_cadastro,dt_ult) ";
  $sql = $sql." values ($codigo,$cd_usuario,$cd_admin,$cd_admin,'$dia2','$hora','$cd_status','$texto',now(),now())";
  mysql_query($sql);
  if(mysql_errno()>0) { $ret = "Erro: ".mysql_error()." <!-- SQL = $sql -->"; }
  return $ret;
}

function InsereAgendaExterna($cd_usuario,$cd_admin,$dia,$hora,$cd_status,$texto) {
  $codigo = GeraCodigo('agenda_usuario','cd_agenda');
  $sql = "insert into agenda_usuario (cd_agenda,cd_usuario,cd_adm_cad,cd_adm_ult,dia,hora,cd_status,ds_agenda,dt_cadastro,dt_ult) ";
  $sql = $sql." values ($codigo,$cd_usuario,$cd_admin,$cd_admin,'$dia','$hora','$cd_status','$texto',now(),now())";
  mysql_query($sql);
  if(mysql_errno()>0) { $ret = "Erro: ".mysql_error()." <!-- SQL = $sql -->"; }
  return $ret;
}

function AlteraAgenda($codigo,$cd_usuario,$cd_admin,$dia1,$hora,$cd_status,$texto) {
  $dia2 = date("Y-m-d",$dia1);
  $sql = "update agenda_usuario set cd_adm_ult=$cd_admin,cd_usuario=$cd_usuario, dia='$dia2', hora='$hora',";
  $sql = $sql." cd_status='$cd_status', ds_agenda='$texto', dt_ult=now() WHERE cd_agenda=$codigo";
  mysql_query($sql);
  if(mysql_errno()>0) { $ret = "Erro: ".mysql_error()." <!-- SQL = $sql -->"; }
  return $ret;
}

function GeraCodigo($tabela,$coluna) {
  $ret = 0;
  $sql = "SELECT MAX($coluna) FROM $tabela";
  $rs = mysql_query($sql);
  if($rw = mysql_fetch_array($rs)) {
     $ret = $ret + $rw[0];
  }
  $ret = $ret + 1;
  return $ret;
}
?>
