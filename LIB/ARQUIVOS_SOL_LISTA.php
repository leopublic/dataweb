<?php
// TODO: Verificar diferencas em relacao a versao de producao
header('Content-Type: text/html; charset=UTF-8');
include ("autenticacao.php");
include ("geral.php");
/*
 * TODO Migrar esse processo para cINTERFACE_OS
 */
$ID_SOLICITA_VISTO = $_GET['ID_SOLICITA_VISTO'];

$sql = "select nu_empresa, ifnull(FL_ENCERRADA, 0) FL_ENCERRADA , nu_solicitacao"
        . " from solicita_visto sv "
        . " left join status_solicitacao ss on ss.id_status_sol = sv.id_status_sol "
        . " where id_solicita_visto = ".$ID_SOLICITA_VISTO." ";
$rs = conectaQuery($sql, __FILE__);
if($rw = mysql_fetch_array($rs)) {
	$NU_EMPRESA = $rw['nu_empresa'];
	$FL_ENCERRADA = $rw['FL_ENCERRADA'];
    $nu_solicitacao = $rw['nu_solicitacao'];
}

$sql = "SELECT NU_SEQUENCIAL, NO_ARQ_ORIGINAL, NO_ARQUIVO, ARQUIVOS.NU_EMPRESA, NO_TIPO_ARQUIVO, CO_TIPO_ARQUIVO, NOME_COMPLETO";
$sql.= "  FROM ARQUIVOS ";
$sql.= "  left join TIPO_ARQUIVO TA on TA.ID_TIPO_ARQUIVO = ARQUIVOS.TP_ARQUIVO";
$sql.= "  left join CANDIDATO C on C.NU_CANDIDATO = ARQUIVOS.NU_CANDIDATO";
$sql.= " where ARQUIVOS.ID_SOLICITA_VISTO = ".$ID_SOLICITA_VISTO;
$sql.= " ORDER BY CO_TIPO_ARQUIVO, NOME_COMPLETO asc ";
$rs = conectaQuery($sql, __FILE__);
$qtdArqs = 0;
$arquivos = "";
while($rw = mysql_fetch_array($rs)) {
	$nmDir = $mysolurl.'/'.$rw['NU_EMPRESA'].'/';
	$qtdArqs = $qtdArqs + 1;
	$seq = $rw['NU_SEQUENCIAL'];
	$nmArq = $rw['NO_ARQUIVO'];
	$tpArq = $rw['TP_ARQUIVO'];
	$CO_TIPO_ARQ = $rw['CO_TIPO_ARQUIVO'];
	$nmArqOrg = $rw['NO_ARQ_ORIGINAL'];
	$dtInc = $rw['DT_INCLUSAO'];
	$numAdmin = $rw['NU_ADMIN'];
	$docpagina = $nmDir.$nmArq;
	if ($rw['NOME_COMPLETO'] == '' ){
		$NOME_COMPLETO = "(geral)";
	}
	else{
		$NOME_COMPLETO = $rw['NOME_COMPLETO'];
	}
	//error_log('Docpagina='.$docpagina);

	$src = $mysoldir.'/'.$rw['NU_EMPRESA'].'/'.$rw['NO_ARQUIVO'];
	$acoes = '' ;
	if (file_exists($src)){
		$NO_ARQ_ORIGINAL = '<a class="img" href="download.php?arq='.$src.'&nmf='.$rw['NO_ARQ_ORIGINAL'].'">'.$rw['NO_ARQ_ORIGINAL'].'</a>';
		$acoes .='<a class="img" href="download.php?arq='.$src.'&nmf='.$rw['NO_ARQ_ORIGINAL'].'"><img src="/imagens/icons/downld.gif" title="clique para baixar esse arquivo" style="border:none" /></a>';
	}
	else{
		$NO_ARQ_ORIGINAL = '<span style="text-decoration:line-through;color:#cccccc;">'.$rw['NO_ARQ_ORIGINAL']."</span>";
	}
	if(!$FL_ENCERRADA) {
		$del = '<img';
		$del.= ' style="cursor:pointer"';
		$del.= ' title="clique para remover esse arquivo"';
		$del.= ' src="/imagens/icons/delete.png"';
		$del.= ' onclick="ajaxRemoverArquivoSol(\''.$rw['NU_SEQUENCIAL'].'\', \''.str_replace('"', "",str_replace("'", "", $rw['NO_ARQ_ORIGINAL'])).'\');"';
		$del.= ' />';
		$acoes.=$del;
    }
	$arquivos.= '<tr>';
	$arquivos.= '<td style="text-align:center;">'.$acoes.'</td>';
	$arquivos.= '<td>'.$rw['CO_TIPO_ARQUIVO'].'</td>';
	$arquivos.= '<td>'.$NO_ARQ_ORIGINAL.'</td>';
	$arquivos.= '<td>'.$NOME_COMPLETO.'</td>';
	$arquivos.= "</tr>";
}
$tabela = '<form id="novoArquivoOS" method="post" action="/LIB/ARQUIVO_SOL_INCLUIR.php" enctype="multipart/form-data" target="_blank">';
$tabela .= cINTERFACE::inputHidden('NU_EMPRESA', $NU_EMPRESA);
$tabela .= cINTERFACE::inputHidden('ID_SOLICITA_VISTO', $ID_SOLICITA_VISTO);
$tabela .= '<table class="edicao dupla" style="width:100%">';
$tabela .= '<col width="80px"></col><col width="120px"></col><col width="auto"></col><col width="auto"></col>';
$tabela .= '<tr><th style="text-align:center;">Ações</th><th>Tipo</th><th>Arquivo</th><th>Candidato</th></tr>';
$tabela .= $arquivos;
$tabela .= '</table>';
if(!$FL_ENCERRADA) {
	$tabela .= '<div style="margin:auto; text-align: center; margin-top:10px; margin-bottom:10px;">';
	$tabela .= '<input type="button" value="Adicionar foto (celular)" onclick="window.open(\'/paginas/carregueComEstilos.php?controller=cCTRL_ARQUIVO&metodo=getAdicionarArquivoOsFoto&id_solicita_visto='.$ID_SOLICITA_VISTO.'&nu_solicitacao='.$nu_solicitacao.'\', \'novoArquivo\', \'width=450,height=300\');" style="width:auto; padding: 4px; margin-right:10px;"/>';
	$tabela .= '<input type="button" value="Adicionar novo" onclick="window.open(\'/paginas/carregueComEstilos.php?controller=cCTRL_ARQUIVO&metodo=getAdicionarArquivoOs&id_solicita_visto='.$ID_SOLICITA_VISTO.'&nu_solicitacao='.$nu_solicitacao.'\', \'novoArquivo\', \'width=450,height=300\');" style="width:auto; padding: 4px; margin-right:10px;"/>';
    $tabela .= '<input type="button" value="Refresh" onclick="carregarArquivos();" style="width:auto; padding: 4px;"/>';
    $tabela .= "</div>";
}
$tabela .= '</form>';
print $tabela;
