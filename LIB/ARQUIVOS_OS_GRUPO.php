<?php


    header('Content-Type: text/html; charset=UTF-8');
    require_once realpath("conecta.php");


    $app    = new Generic;

    function getSQLArquivo($id){
        return '
         SELECT A.NO_ARQUIVO, TA.NO_TIPO_ARQUIVO, A.NU_EMPRESA, A.NU_CANDIDATO, A.NU_SEQUENCIAL
           FROM ARQUIVOS A
      LEFT JOIN TIPO_ARQUIVO TA
             ON TA.ID_TIPO_ARQUIVO = A.TP_ARQUIVO
          WHERE A.ID_SOLICITA_VISTO = '.$id.'
        ';
     }

    function getSQLOrdemServico($id){
        return '
          SELECT OS.id_solicita_visto, OS.dt_solicitacao, AC.NU_SOLICITACAO
            FROM solicita_visto OS
      INNER JOIN AUTORIZACAO_CANDIDATO AC
              ON OS.id_solicita_visto = AC.id_solicita_visto
      INNER JOIN CANDIDATO C
              ON C.NU_CANDIDATO = AC.NU_CANDIDATO
           WHERE C.NU_CANDIDATO = '.$id.'
        ';
    }

    function extensao($file){
        $res = null;
        str_replace('.', '', $file, $count);

        if($count>0){
            $res = strtolower(array_pop(explode('.', $file)));
        }

        return $res;
    }




    $res = sql::query(getSQLOrdemServico($app->g('NU_CANDIDATO')));

    while($r = $res->fetch()){

        $exe   = sql::query(getSQLArquivo($r['id_solicita_visto']));
        $count = $exe->count();

        echo '<table class="TArquivo edicao dupla">
              <thead>
                 <th>OS '.$r['NU_SOLICITACAO'].' - '.$app->dataBr($r['dt_solicitacao']).' </th><th></th>
              </thead>
              <tbody>';

        while($row = $exe->fetch()){

            $file     = '../arquivos/solicitacoes/'.$row['NU_EMPRESA'].'/'.$row['NU_CANDIDATO'].''.$row['NO_ARQUIVO'];
            $icone    = '<a href="'.$file.'" target="_blank"> <img src="/imagens/icons/downld.gif" /></a> ';
            $extensao = extensao($row['NO_ARQUIVO']);
            $fileload = null;

            if(($extensao == 'jpg') OR ($extensao == 'tiff') OR ($extensao == 'bmp') ){
                $fileload = '<a href="javascript:CarregarArquivo(\''.$file.'\', \''.$row['NU_SEQUENCIAL'].'\', \'container_img_os\');" > '.$row['NO_ARQUIVO'].'</a>';
            }else{
                $fileload = $row['NO_ARQUIVO'];
            }

            echo '<tr>
                    <td> ' . $icone. $row['NO_TIPO_ARQUIVO'] . ' ('.$fileload.')</td>
                  </tr>';
        }

        echo '</tbody>';
        echo '</table><div id="container_img_os"><div>';


    }



?>
<style>
.TArquivo{
   border:1px solid #c0c0c0;
   width:auto;
}
.TArquivo th{
  font:bold 13px verdana;
  text-align:left;
}
.TArquivo tbody{
   height:auto
}
.TArquivo tbody td{
    padding-left: 10px;
}
#ArquivosOS{
   margin-top:30px;
}
#ArquivosOS h4{
   font:18px verdana;
   margin-bottom:5px;
}
</style>
