<?php


		$sql = "
         SELECT ac.NU_CANDIDATO, ac.NU_EMBARCACAO_PROJETO, c.NOME_COMPLETO
		   FROM AUTORIZACAO_CANDIDATO ac
	       JOIN CANDIDATO  c
             ON c.NU_CANDIDATO = ac.NU_CANDIDATO
          WHERE ac.ID_SOLICITA_VISTO =".$pID_SOLICITA_VISTO."
		  ORDER BY NOME_COMPLETO";

        $exe   = mysql_query($sql);
        $count = mysql_num_rows($exe);

        $candidatos = '';

        while($r = mysql_fetch_array($exe)){
            if($candidatos <> ''){
                $candidatos .= ', '.$r['NOME_COMPLETO'];
            }else{
                $candidatos .= $r['NOME_COMPLETO'];
            }
        }



$mark = '<img src="../imagens/mark.jpg" />';

$html = '
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
<title>OS</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>
body{
 font:10px arial;
 text-align:left
}
 table{
    border: medium none;
    border-collapse: collapse;
    text-align: left;
    width:100%;

    font-family: Arial;
    border:1px solid #000;
 }
 table td{
    border:1px solid #000;
 }

</style>
</head>
<body basefont="arial">
<p align="center" >
    CHECK LIST RN 84/2009 - PERMANENTE INVESTIDOR (PESSOA FÍSICA)
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    ITM02 ANEXO I.6 - REV.01
</p>
<table style="width:100%">
  <tbody>
    <tr>
      <td><b>OS:</b> '.$tela->mCampoNome['NU_SOLICITACAO']->mValor.' </td>
      <td colspan="2"><b>Data da solicitação: </b>'.htmlentities($tela->mCampoNome['dt_solicitacao']->mValor).'</td>
    </tr>
    <tr>
      <td><b>Empresa requerente:</b> '.htmlentities($tela->mCampoNome['NO_RAZAO_SOCIAL']->mValor).'</td>
      <td><b>Projeto/embarcação:</b> '.htmlentities($tela->mCampoNome['NO_EMBARCACAO_PROJETO']->mValor).'</td>
      <td width="170"><b>Prazo solicitado:</b> '.htmlentities($tela->mCampoNome['DT_PRAZO_ESTADA_SOLICITADO']->mValor).'</td>
    </tr>
    <tr>
    </tr>
    <tr>
      <td  colspan="3"><b>Candidato(s):</b> '.$candidatos.'
      </td>
    </tr>
  </tbody>
</table>

<br />
<p style="font-weight: bold;">Documentação obrigatória</p>
'.$mark.' 01 - Formulário de requerimento de autorização de trabalho <br />
'.$mark.' 02 - GRU <br />
'.$mark.' 03 - Cópia da página de Identificação do Passaporte do Candidato e quando houver cópia dos dependentes - legível e válido(s)<br />
'.$mark.' 04 - Formulário Modelo I - dados da Requerente a e do Candidato <br />
'.$mark.' 05 - Termo de responsabilidade <br />
'.$mark.' 06 - Plano de Investimento <br />
'.$mark.' 07 - Consta o Capital integralizado no ato constitutivo <br />
'.$mark.' 08 - Extrato Consolidado do SISBACEN ou Contrato de Câmbio<br />
'.$mark.' 09 - Outros:______________________________________________________ <br />
'.$mark.' 10 - Protocolo do CERTE atualizado (validade 180 dias) '.$mark.' SIM  '.$mark.' NÃO <br />

 <BR />
 Caso a resposta do item 10 for <b><u>não</u>, favor completar:</b><br />

<div style="margin-left:20px">
   '.$mark.' 10.1 - Procuração e quando houver a respectiva cascata do substabelecimento  <br />
   '.$mark.' 10.2 - A procuração está válida e atualizada de acordo com ato constitutivo   <br />
   '.$mark.' 10.3 - CNPJ <br />
   '.$mark.' 10.4 - Contrato Social ou Estatuto Social e ata de nomeação Diretoria atualizada<br />
 </div>
 <br />
 <BR />
     Caso exista autorização de trabalho anterior, completar:
 <br />
'.$mark.' 11 - Petição de esclarecimento de autorização de trabalho anterior <br />
'.$mark.' 12 - Cópia protocolada do cancelamento  <br />
'.$mark.' 13 - Cópia simples de todas as páginas do passaporte, caso seja a mesma Requerente.<br />

<p style="margin-right: 11.35pt;"></p>
 <br />
<table>
  <tbody>
    <tr>
      <td>
        Verificação - MV Niterói/Macaé
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;
        Documentação: '.$mark.'  Ok?</td>
    </tr>
    <tr>
      <td><br/><br/>Responsável: ___________________________________________________________________________________ Data: _____/_____/_______</td>
    </tr>
  </tbody>
</table>
<div>
<br />
<p>Este pedido somente poderá ser liberado para o malote após análise do responsável.</p>
<table>
  <tbody>
    <tr>
      <td>Verificação - MV Brasília
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       Documentação: '.$mark.' Ok? </td>
    </tr>
    <tr>
      <td><br/><br/>Responsável: ____________________________________________________________________________________ Data: _____/_____/_______</td>
    </tr>
  </tbody>
</table>
    <br />
    <br />
    <br />
  </div>
</body>
</html>
';





?>
