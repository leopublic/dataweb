<?php

		$sql = "
         SELECT ac.NU_CANDIDATO, ac.NU_EMBARCACAO_PROJETO, c.NOME_COMPLETO
		   FROM AUTORIZACAO_CANDIDATO ac
	       JOIN CANDIDATO  c
             ON c.NU_CANDIDATO = ac.NU_CANDIDATO
          WHERE ac.ID_SOLICITA_VISTO =".$pID_SOLICITA_VISTO."
		  ORDER BY NOME_COMPLETO";

        $exe   = mysql_query($sql);
        $count = mysql_num_rows($exe);

        $candidatos = '';

        while($r = mysql_fetch_array($exe)){
            if($candidatos <> ''){
                $candidatos .= ', '.$r['NOME_COMPLETO'];
            }else{
                $candidatos .= $r['NOME_COMPLETO'];
            }
        }



$mark = '<img src="../imagens/mark.jpg" />';

$html = '
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
<title>OS</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>
body{
 font:14px arial;
 text-align:left
}
 table{
    border: medium none;
    border-collapse: collapse;
    text-align: left;
    width:100%;

    font-family: Arial;
    border:1px solid #000;
 }
 table td{
    border:1px solid #000;
 }
 table.detaque td{
    font-size:14px;
 }
 p.itens{
	padding:0;
	margin:0;
	margin-top:1px;
	margin-bottom:1px;
 }
</style>
</head>
<body basefont="arial">
<p align="center" style="font-size:16px;font-weight:bold;">
    CHECK LIST DE PRORROGAÇÃO DO PRAZO DE ESTADA - RN 72/2006 ATÉ 02 ANOS
</p>
<table style="width:100%">
  <tbody>
    <tr>
      <td><b>OS:</b> '.$tela->mCampoNome['NU_SOLICITACAO']->mValor.' </td>
      <td><b>Data da solicitação: </b>'.htmlentities($tela->mCampoNome['dt_solicitacao']->mValor).'</td>
    </tr>
    <tr>
      <td><b>Empresa requerente:</b> '.htmlentities($tela->mCampoNome['NO_RAZAO_SOCIAL']->mValor).'</td>
      <td><b>Projeto/embarcação:</b> '.htmlentities($tela->mCampoNome['NO_EMBARCACAO_PROJETO']->mValor).'</td>
    </tr>
    <tr>
      <td><b>Projeto/embarcação REAL:</b> '.htmlentities($tela->mCampoNome['NO_EMBARCACAO_PROJETO_COBRANCA']->mValor).'</td>
      <td><b>Prazo pretendido:</b> '.htmlentities($tela->mCampoNome['DT_PRAZO_ESTADA_SOLICITADO']->mValor).'</td>
    </tr>
    <tr>
      <td  colspan="2"><b>Candidato(s):</b> '.$candidatos.'
      </td>
    </tr>
  </tbody>
</table>

<p style="font-weight: bold;">Documentação obrigatória</p>
<p class="itens">'.$mark.' 01 - Formulário 1344 assinado </p>
<p class="itens">'.$mark.' 02 - Formulário de Requerimento de Autorização de Trabalho</p>
<p class="itens">'.$mark.' 03 - Petição Completa</p>
<div style="margin-left:20px">
  <p class="itens">'.$mark.' 03.1 - Justificativa para o Pedido</p>
  <p class="itens">'.$mark.' 03.2 - Descrição das Atividades <b>(experiência profissional)</b></p>
</div>
<p class="itens">'.$mark.' 04 - Termo de Responsabilidade (Despesas Médicas e/ou Hospitalares e Repatriação)</p>
<p class="itens">'.$mark.' 05 - Declaração de Embarcação e Total de Tripulantes</p>
<p class="itens">'.$mark.' 06 - Cópia do D.O.U que consta a Autorização do Visto Inicial concedido pelo M.T.E</p>
<p class="itens">'.$mark.' 07 - Cópia Autenticada do Protocolo de Registro ou da CIE</p>
<p class="itens">'.$mark.' 08 - Cópia Autenticada de todas as páginas do Passaporte</p>
<p class="itens">'.$mark.' 09 - Declaração de 90 dias ou D.O.U (Comprovação de Mão-de-Obra Nacional)</p>
<p class="itens">'.$mark.' 10 - Cópia Autenticada da Crew List da embarcação contendo o nome de todos os tripulantes (brasileiros e estrangeiros), suas nacionalidades e assinada pelo capitão ou responsável.</p>
<p style="margin-right: 11.35pt;"></p>
 <br />
<table class="destaque">
  <tbody>
    <tr>
      <td>
        <b>Verificação - MV Niterói/Macaé</b>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Documentação: '.$mark.'  Ok?</td>
    </tr>
    <tr>
      <td><br/><br/>Responsável: _______________________________________________________________ Data: _____/_____/_______</td>
    </tr>
  </tbody>
</table>
 <br />
<p class="itens">'.$mark.' 11 - GRU pago q Sim? q Não?  c/ Multa q Sim? q Não?</p>
<p class="itens">'.$mark.' 12 - Procuração está válida e atualizada de acordo com ato constitutivo</p>
<p class="itens">'.$mark.' 13 - Páginas do Contrato de Afretamento da Embarcação contendo: </p>
<div style="margin-left:20px">
  <p class="itens"> '.$mark.' 13.1 - Partes ? Requerente e Proprietária</p>
   <p class="itens">'.$mark.' 13.2 - Prazo de Validade > ou = ao prazo solicitado</p>
   <p class="itens">'.$mark.' 13.3 - Objeto ? mencionando a embarcação</p>
   <p class="itens">'.$mark.' 13.4 - Assinaturas das Partes</p>
</div>
 <br />
<p style="text-decoration: italic;">Este pedido somente poderá ser liberado para o malote após análise do responsável.</p>
<table>
  <tbody>
    <tr>
      <td><b>Verificação - MV Brasília</b>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       Documentação: '.$mark.' Ok? </td>
    </tr>
    <tr>
      <td><br/><br/>Responsável: _______________________________________________________________ Data: _____/_____/_______</td>
    </tr>
  </tbody>
</table>
</body>
</html>
';
?>
