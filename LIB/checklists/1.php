<?php


		$sql = "
         SELECT ac.NU_CANDIDATO, ac.NU_EMBARCACAO_PROJETO, c.NOME_COMPLETO
		   FROM AUTORIZACAO_CANDIDATO ac
	       JOIN CANDIDATO  c
             ON c.NU_CANDIDATO = ac.NU_CANDIDATO
          WHERE ac.ID_SOLICITA_VISTO =".$pID_SOLICITA_VISTO."
		  ORDER BY NOME_COMPLETO";

        $exe   = mysql_query($sql);
        $count = mysql_num_rows($exe);

        $candidatos = '';

        while($r = mysql_fetch_array($exe)){
            if($candidatos <> ''){
                $candidatos .= ', '.$r['NOME_COMPLETO'];
            }else{
                $candidatos .= $r['NOME_COMPLETO'];
            }
        }



$mark = '<img src="../imagens/mark.jpg" />';

$html = '
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
<title>OS</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>
body{
 font:10px arial;
 text-align:left
}
 table{
    border: medium none;
    border-collapse: collapse;
    text-align: left;


    font-family: Arial;
    border:1px solid #000;
 }
 table td{
    border:1px solid #000;
 }

</style>
</head>
<body basefont="arial">
ITM 02  Revisão: 01
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

CHECK LIST RN 72/2006 - ATÉ 02 ANOS<BR /><BR />
<table style="width:100%">
  <tbody>
    <tr>
      <td><b>OS:</b> '.$tela->mCampoNome['NU_SOLICITACAO']->mValor.' </td>
      <td><b>Total de candidatos:</b> '. $count.' </td>
      <td width="170"><b>Data da solicitação: </b>'.htmlentities($tela->mCampoNome['dt_solicitacao']->mValor).'</td>
    </tr>
    <tr>
      <td><b>Empresa :</b> '.htmlentities($tela->mCampoNome['NO_RAZAO_SOCIAL']->mValor).'</td>
      <td><b>Projeto/embarcação:</b> '.htmlentities($tela->mCampoNome['NO_EMBARCACAO_PROJETO']->mValor).'</td>
      <td><b>Prazo solicitado:</b> '.htmlentities($tela->mCampoNome['DT_PRAZO_ESTADA_SOLICITADO']->mValor).'</td>
    </tr>
    <tr>
      <td colspan="3"><b>Centro de Custo MV:</b>'.htmlentities($OS->mNO_EMBARCACAO_PROJETO_COBRANCA).'

      </td>
    </tr>
    <tr>
      <td  colspan="3"><b>Candidato(s):</b> '.$candidatos.'
      </td>
    </tr>
  </tbody>
</table>

<br />
<p style="font-weight: bold;">Documentação obrigatória</p>
'.$mark.' 01 - Folha de Rosto contendo Nome - Nacionalidade - Repartição Consular, quando aplicável.<br />
'.$mark.' 02 - Formulário de requerimento de autorização de trabalho<br />
'.$mark.' 03 - GRU pago  '.$mark.' Sim?  '.$mark.' Não?<br />
'.$mark.' 04 - Página de identificação do passaporte do titular - legível e válido <br />
'.$mark.' 05 - Formulário Modelo I - dados da Requerente e do Candidato<br />
'.$mark.' 06 - Termo de responsabilidade da Requerente (despesas médicas e repatriação)<br />
'.$mark.' 07 - Declaração da embarcação e total de tripulantes, caso não esteja no protocolo do CERTE<br />
'.$mark.' 08 - Protocolo do CERTE atualizado (validade 180 dias) '.$mark.'  SIM '.$mark.'  NÃO<br /><br />
Caso a resposta do item 08 for <b><u>não</u>, favor completar:</b><br />

<div style="margin-left:20px">
   '.$mark.' 08.1 - Procuração e quando houver a respectiva cascata do substabelecimento <br />
   '.$mark.' 08.2 - Páginas do contrato da Embarcação e sua tradução contendo:<br />
    <div style="margin-left:20px">
          Partes - Requerente e Proprietária <br />
          Prazo validade >ou = ao prazo solicitado <br />
          Objeto - mencionando a embarcação <br />
          Assinaturas das partes ou Declaração de ratificação <br />
    </div><br />
   '.$mark.' 08.3 - CNPJ<br />
   '.$mark.' 08.4 - Contrato Social ou Estatuto Social e ata de nomeação Diretoria atualizada<br />
   '.$mark.' 08.5 - A procuração está válida e atualizada de acordo com ato constitutivo <br />
 </div>
 <br />
 Caso exista autorização de trabalho anterior, completar: <br />
'.$mark.' 09 - Petição de esclarecimento de autorização de trabalho anterior vigente <br />
'.$mark.' 10 - Cópia protocolada do cancelamento <br />
'.$mark.' 11 - Cópia simples de todas as páginas do passaporte <br />

<p style="margin-right: 11.35pt;"></p>
 <br />
<table style="width:100%">
  <tbody>
    <tr>
      <td>
        Verificação - MV Niterói/Macaé
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;
        Documentação: '.$mark.'  Ok?</td>
    </tr>
    <tr>
      <td><br/><br/>Responsável: ____________________________________________________________________________________ Data: _____/_____/_______</td>
    </tr>
  </tbody>
</table>
<div>
<br />
<p>Este pedido somente poderá ser liberado para o malote após análise do responsável.</p>
<table style="width:100%">
  <tbody>
    <tr>
      <td>Verificação - MV Brasília
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       Documentação: '.$mark.' Ok? </td>
    </tr>
    <tr>
      <td><br/><br/>Responsável: ____________________________________________________________________________________ Data: _____/_____/_______</td>
    </tr>
  </tbody>
</table>
    <br />
    <br />
    <br />
  </div>
</body>
</html>
';





?>
