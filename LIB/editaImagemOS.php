<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

$pNU_SEQUENCIAL = $_GET['NU_SEQUENCIAL'];
$sql = "select * from ARQUIVOS where NU_SEQUENCIAL=".$pNU_SEQUENCIAL;
$rs = mysql_query($sql);
$qtdArqs = 0;
$arquivos = "";	
if ($rw = mysql_fetch_array($rs)) 
{
	$nmDir = $mysolurl."/".$rw['NU_EMPRESA']."/".$rw['NO_ARQUIVO'];
	$src = cAMBIENTE::getDirSol().DIRECTORY_SEPARATOR.$rw['NU_EMPRESA'].DIRECTORY_SEPARATOR.$rw['NO_ARQUIVO'];
}	
	
$size = getimagesize($src);
$xArq = $size[0];
$yArq = $size[1];

$heightPadrao = 450;
$widthPadrao = 600;
if ($xArq>$yArq)
{
	$y = $yArq/$xArq * $widthPadrao;
	$x = $widthPadrao;
	$estilo="width:".$widthPadrao."px;";
	$tam="width:".$widthPadrao."px;height:".$y."px;";
}
else
{
	$x = $xArq/$yArq * $heightPadrao;
	$y = $heightPadrao;
	$estilo="height:".$heightPadrao."px;width:".$x."px;";
	$tam="height:".$heightPadrao."px;width:".$x."px;";
}

echo Topo();
?>
	<div class="titulo"><div style="float:left">Editar imagem</div>&nbsp;</div>

	<div id="containerEdicao" style="border:solid 1px #000000;padding:3px; margin-top:5px;margin-left: auto ;margin-right: auto;<?=$estilo;?>">
		<img src="<?=$nmDir.'?'.time();?>" style="<?=$tam;?>"/>
	</div>
	<br/>
	<form action="alterarImagemOS.php" method="post"  onsubmit="confirma();">
		<div id="botoes" name="botoes" style="text-align:center;">
			<input type="submit" value="Cortar para seleção"/>
			<input type="button" value="Girar no sentido horário" onclick="girar('90')"/>
			<input type="button" value="Girar no sentido anti-horário" onclick="girar('270')"/>
			<input type="button" onclick="window.opener.carregarArquivos(); window.close();" value="Sair"/>	
		</div>
		<div id="aguarde" name="aguarde" style="text-align:center;">(por favor aguarde enquanto a imagem é processada... dependendo do tamanho do arquivo isso pode levar alguns segundos)</div>
		<input type="hidden" id="NU_SEQUENCIAL" name="NU_SEQUENCIAL" value="<?=$pNU_SEQUENCIAL;?>"/> 
		<input type="hidden" id="xOrig" name="xOrig" value="<?=$x;?>" /> 
		<input type="hidden" id="yOrig" name="yOrig" value="<?=$y;?>" /> 
		<input type="hidden" id="x" name="x" /> 
		<input type="hidden" id="y" name="y" /> 
		<input type="hidden" id="x2" name="x2" /> 
		<input type="hidden" id="y2" name="y2" /> 
		<input type="hidden" id="w" name="w" /> 
		<input type="hidden" id="h" name="h" />
		<input type="hidden" id="rotation" name="rotation" />
	</form> 
	<script>
		var crop = $('#containerEdicao img').Jcrop({onChange: showCoords});
		$('#aguarde').hide();
		function girar(pValor)
		{
			$('#rotation').val(pValor);
			$('#botoes').hide();
			$('#aguarde').show();
			document.forms[0].submit();
		}
		function confirma()
		{
			if (confirm('Deseja mesmo alterar esse arquivo? Essa operação não poderá ser desfeita.'))
			{
				return true;
			}
			else
			{
				return false;
			}		
		}
		
		function showCoords(c)
		{
			jQuery('#x').val(c.x);
			jQuery('#y').val(c.y);
			jQuery('#x2').val(c.x2);
			jQuery('#y2').val(c.y2);
			jQuery('#w').val(c.w);
			jQuery('#h').val(c.h);
		}
		$(window).bind("beforeunload", function() { 
			window.opener.carregarArquivos(); 
		})
	</script>
		
	</body>
</html>
