<?php

class cvalidador_candidato extends cvalidador {

    public function identificacaoOk(cCANDIDATO $candidato) {
        $this->msg = '';
//        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
//        if (!preg_match($regex, $candidato->mNO_EMAIL_CANDIDATO)) {
//            $this->adicionaMsg("- O e-mail de notificação é inválido.");
//        }
        if ($this->msg != '') {
            return false;
        } else {
            return true;
        }
    }

    public function acessoOk(cCANDIDATO $candidato) {
        $this->msg = '';
        // Verifica se já existe algum

        if (trim($candidato->mNO_SENHA) != ''){
            if (strstr($candidato->mNO_SENHA, ' ')){
                $this->adicionaMsg("- A senha não pode conter espaços em branco.");                
            }
            if (strlen($candidato->mNO_SENHA) < 8){
                $this->adicionaMsg("- A senha deve ter pelo menos 8 caracteres.");                
            }
            if (strlen($candidato->mNO_SENHA) > 10){
                $this->adicionaMsg("- A senha deve ter no máximo 10 caracteres.");                
            }
        }
        
        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
        if (trim($candidato->memail_pessoal) != '') {
            if (!preg_match($regex, $candidato->memail_pessoal)) {
                $this->adicionaMsg("- O e-mail pessoal é inválido.");
            } elseif (strstr($candidato->memail_pessoal, 'mundivisa')) {
                $this->adicionaMsg("- Você deve informar o e-mail pessoal do candidato. Se não houver um, deixe em branco.");
            } else {
                $sql = "select ifnull(count(*), 0) from candidato where email_pessoal = " . cBANCO::StringOk($candidato->memail_pessoal);
                $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
                if ($rw = mysql_fetch_array($res)) {
                    if (intval($rw[0]) > 0) {
                        $this->adicionaMsg("- Já existe um estrangeiro cadastrado com esse e-mail. Não é possível cadastrar 2 estrangeiros no mesmo e-mail.");
                    }
                } else {
                    $sql = "select ifnull(count(*), 0) from candidato_tmp where email_pessoal = " . cBANCO::StringOk($candidato->memail_pessoal);
                    $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
                    if ($rw = mysql_fetch_array($res)) {
                        if (intval($rw[0]) > 0) {
                            $this->adicionaMsg("- Já existe um estrangeiro cadastrado com esse e-mail. Não é possível cadastrar 2 estrangeiros no mesmo e-mail.");
                        }
                    }
                }
            }
        }

        if ($this->msg != '') {
            return false;
        } else {
            return true;
        }
    }

}
