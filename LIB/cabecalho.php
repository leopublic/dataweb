<?php

function IniPag($menu = true, $title = null, $pestiloAdicional = '', $pEhConsulta = false) {
    cHTTP::$comMenu = $menu;
    cHTTP::$comCabecalhoFixo = $pEhConsulta;
    if ($title != '') {
        cHTTP::set_titulo_pagina($title);
    }
    cHTTP::AdicionarEstilo($pestiloAdicional);
    cLAYOUT::AdicionarScriptsPadrao();
    $x = cHTTP::ScriptsOnLoad();
    $y = cLAYOUT::CabecalhoPadrao();
    return $y . $x;
}

function Topo($title = null, $pestiloAdicional = '', $pComMenu = true, $pEhConsulta = false) {
    $ret = IniPag($pComMenu, $title, $pestiloAdicional, $pEhConsulta);
    return $ret;
}

function Menu($opcao = "ALL") {
    global $usufunc, $ehCliente;
    $ret = cLAYOUT::Logo();
    $ret .= cLAYOUT::Menu();
    return $ret;
}

function Rodape($opcao) {
    $ret = " <!--Rodape-->\n";
    if ($opcao == "ALL") {
        $ret = $ret . "<table width='100%' height='23' border='0' cellpadding='0' cellspacing='0'>\n";
        $ret = $ret . "  <tr>\n";
        $ret = $ret . "    <td width='100%' height='23' align='center' background='/images/fundo_barra.gif' class='textformtopo'>\n";
        $ret = $ret . "     &copy; Mundivisas 2006 \n";
        $ret = $ret . "    </td>\n";
        $ret = $ret . "  </tr>\n";
        $ret = $ret . "</table>\n";
    }
    $ret = $ret . "</body>\n";
    $ret = $ret . "</html>\n";
    return $ret;
}

function MenuNovo($opcao = 'ALL') {
    return cINTERFACE::Menu();
}

function MontaTDtopoMenu($nome, $url, $tam) {
    if ($nome == "Formularios") {
        $ret = "<td width='$tam' align=center><a href='#' onclick='javascript:window.open(\"$url\");' >$nome</a></td>\n";
    } else {
        $ret = "<td width='$tam' align=center><a href='$url'>$nome</a></td>\n";
    }
    return $ret;
}

/**
 *   CRIAÇÃO DO MENUBAR PARA OS
 *   POR FABIO
 * */
function menuBarOS($usulogado, $id_visto, $nu_servico, $estiloNC, $labelNC, $readonly) {
    // desabilita class anteriores
    $btn_menu = 'style="border:none;margin:0px;padding:0px;"';

    // BOTÃO ABRIR OS
    $btn_abrir_os = '';

    if ($readonly) { // mostrar somente se for ReadOnly
        //   if($nu_servico == 1){ // mostrar somente "ABRIR OS" se o nu_servico for "1"  equivalente ao serviço:  Aut. Tab. RN72
        //if(isset($_SESSION['cd_perfil_admin']) == 1){  // mostrar somente se for administrador
        if (Acesso::permitido(Acesso::tp_Os_Menu_Acao_Abrir)) {
            $btn_abrir_os = '
                		<form method="post" style="display:inline;">
                		    <input type="hidden" id="ID_SOLICITA_VISTO" name="ID_SOLICITA_VISTO" value="' . $id_visto . '"/>
                			<input type="hidden" id="abreOs" name="abreOs" value="1"/>
                			<input type="submit" value="Abrir OS" title="Clique para abrir a OS ' . $nu_servico . ' " ' . $btn_menu . ' />
                		</form>
                        ';
        }
        // }
        //  }
    }

    // BOTÃO EXCLUIR OS
    $btn_excluir_os = '';
    if (Acesso::permitido(Acesso::tp_Os_Menu_Acao_Excluir)) {
        //if(isset($_SESSION['cd_perfil_admin']) == 1){
        if ($id_visto <> 0) {
            $btn_excluir_os = '
      	     <input type="button" value="Excluir OS" onClick="ConfirmaAcao(this, \'Excluir"  ' . $btn_menu . '  />
             ';
        }
    }
    //}
    //DEFINO BTN_ABRIR_OS
    if ($btn_abrir_os <> '') {
        if (Acesso::permitido(Acesso::tp_Os_Menu_Acao_Abrir)) {
            $btn_abrir_os = '<li><img src="../imagens/open.png" /> ' . $btn_abrir_os . '</li>';
        } else {
            $btn_abrir_os = '';
        }
    }

    //DEFINO BTN_EXCLUIR_OS
    if ($btn_excluir_os <> '') {
        $btn_excluir_os = '<li><img src="../imagens/delete.gif" /> ' . $btn_excluir_os . '</li>';
    }


    $gerar_email = '<li><img src="../imagens/icons/email_go.png" /><a href="GeradorEmail.php?ID_SOLICITA_VISTO=' . $id_visto . '"> E-mail de confirmação</a></li>';



    if ($estiloNC == 'NC_ON') {
        $style = 'background:red;color:#fff';
        $imgNC = '';
    } else {
        $imgNC = '<img src="../imagens/action_encerrado.png">';
        $style = '';
    }


    $iten_checklist = '';
    if ($nu_servico == 1) {
        $iten_checklist = '<li>
              <a href="../LIB/OS_RELATORIOS.php?ID_SOLICITA_VISTO=' . $id_visto . '" target="_blank">
              <img src="../imagens/print.gif"/> CheckList
              </a>
          </li>';
    }


    $checklists = '';
    if (Acesso::permitido(Acesso::tp_Os_Menu_Checklist)) {
        if ($nu_servico <> '') {
            $sql = "
              SELECT C.NU_CHECKLIST, CS.NU_SERVICO, C.NO_CHECKLIST
                FROM CHECKLIST_SERVICO CS
          INNER JOIN CHECKLIST C
                  ON C.NU_CHECKLIST = CS.NU_CHECKLIST
               WHERE NU_SERVICO = " . $nu_servico . "
            ";
            $exe = mysql_query($sql);
            $count = mysql_num_rows($exe);
            $item = '';
            if ($count > 0) {
                while ($r = mysql_fetch_assoc($exe)) {
                    $item .= '
                 <li>
                    <a href="/LIB/checklist.php?ID_SOLICITA_VISTO=' . $id_visto . '&SERVICO=' . $nu_servico . '&CHECKLIST=' . $r['NU_CHECKLIST'] . '">
                     ' . $r['NO_CHECKLIST'] .
                            '</a>
                 </li>';
                }

                if ($item <> '') {

                    $checklists = '<li><img src="../imagens/print.gif"/> <u>C</u>hecklist (' . $count . ') <ul>' . $item . '</ul></li>';
                }
            }
        }
    }


    $ordemservico = '';
    if (Acesso::permitido(Acesso::tp_Os_Menu_OrdemServico)) {
        $ordemservico = '
        <li>
           <a href="../LIB/OS_rel.php?ID_SOLICITA_VISTO=' . $id_visto . '" target="_blank">
           <img src="../imagens/icons/printer.png"/> Ordem de Serviço
           </a>
        </li>
        ';
    }


    $NC = '';
    if (Acesso::permitido(Acesso::tp_Os_Menu_NaoConformidade)) {
        $NC = '
            <li>
    			<form method="post">
    				<input type="hidden" id="naoConformidade" name="naoConformidade" value="1"/>
                    ' . $imgNC . '
    				<input type="submit" value="NC" title="' . $labelNC . '" style="border:1px solid #c0c0c0;padding:0px;margin:0px;font-size:8pt;' . $style . '" />
    			</form>
            </li>';
    }


    $acao = '';
    if (Acesso::permitido(Acesso::tp_Os_Menu_Acao)) {
        $acao = '
            <li>
                <img src="/imagens/icons/application_cascade.png" />
                <u>A</u>ção
                <ul>
                    ' . $btn_abrir_os . '
                    ' . $btn_excluir_os . '
                    ' . $gerar_email . '
                </ul>
            </li>
        ';
    }





    $menu = '<ul id="menubar" class="menubar">
        <li>
            <a href="../intranet/geral/os_listar.php"> <img src="/imagens/icons/layout_content.png" /> Listar OS </a>
        </li>
         ' . $acao . '
         ' . $checklists . '
         ' . $ordemservico . '
         ' . $NC . '
    </ul>
         ';

    return $menu;
}
