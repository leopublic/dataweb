<?php

ini_set('session.gc_maxlifetime', 14400);

// each client should remember their session id for EXACTLY 1 hour
session_set_cookie_params(14400);

session_start();
include ("conecta.php");
$pag = $_SERVER['PHP_SELF'];


try {
    cSESSAO::RegistraSessao($_SESSION['myAdmin']['cd_usuario'], $_SESSION['myAdmin']['nm_nome'], $_SESSION['myAdmin']['cd_perfil'], $_SERVER['REMOTE_ADDR'], '', $_SESSION['ss_nu_candidato'], $_SESSION['ss_nu_candidato_tmp']);
} catch (exception $e) {
    cNOTIFICACAO::singleton("You don't have access to this function", cNOTIFICACAO::TM_ERROR);
    session_write_close();
    header('Location:/index.php');
}

if ($_SESSION["usuarioPerfil"] == '10') {
    if ($pag != '/index.php' 
            && $pag != '/operador/candListar.php' 
            && $pag != '/operador/detalheCandidatoCliente.php' 
            && $pag != '/operador/detalheCandidatoCliente_ARQUIVOS.php' 
            && $pag != '/operador/detalheCandidatoCliente_ID.php' 
            && $pag != '/operador/detalheCandidatoCliente_VISTO.php' 
            && $pag != '/operador/arquivoPostCliente.php' 
            && $pag != '/LIB/ARQUIVOS_LISTA.php' 
            && $pag != '/LIB/ARQUIVOS_LISTA_CLIENTE.php'
            && $pag != '/paginas/carregueCliente.php'
            && $pag != '/paginas/carregue.php'
            && $pag != '/sair.php'
    ) {
        header('Location:/index.php');
    }
}

// Restrigir o acesso do perfil cliente pelo controller sendo acessado
if ($_SESSION["usuarioPerfil"] == '10') {
    $controller = $_GET['controller'];
    if ($controller != ''){
        if ($controller != 'cCTRL_CAND_CLIE_LIST' && $controller != 'cCTRL_CANDIDATO_CLIENTE'){
            header('Location:/index.php');
        }
    }
}

// Restrigir o acesso do preenchedor convidado do operacional
if (intval($_SESSION['ss_nu_candidato_tmp']) > 0) {
    if ($controller != 'cCTRL_CONVIDADO_DR'){
        header('Location:/index.php');
    }
}

if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    $lang = '';
}
