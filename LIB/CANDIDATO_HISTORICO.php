<?php
header('Content-Type: text/html; charset=UTF-8');
include ("autenticacao.php");
include ("geral.php");
include ("../LIB/combos.php");
include ("../LIB/componentes/cCOMBO.php");

$pNU_CANDIDATO  	= cINTERFACE::GetValido($_GET,'NU_CANDIDATO');

if ($pNU_CANDIDATO==''){
	exit;
}
else{
	$sql = "     select AC.id_solicita_visto, S.ID_TIPO_ACOMPANHAMENTO, AC.NU_SOLICITACAO";
	$sql .= "      from CANDIDATO C";
	$sql .= "      join AUTORIZACAO_CANDIDATO AC	ON AC.NU_CANDIDATO = C.NU_CANDIDATO";
	$sql .= "      join solicita_visto sv			ON sv.id_solicita_visto = AC.id_solicita_visto";
	$sql .= " left join SERVICO S 					ON S.NU_SERVICO = sv.NU_SERVICO";
	$sql .= "     where C.NU_CANDIDATO = ".$pNU_CANDIDATO;
	$sql .= "  order by id_solicita_visto desc";
	$rs = mysql_query($sql);
	$encontrouMte = false;
	while(($rw = mysql_fetch_array($rs)) && !$encontrouMte) {
		if ($rw['ID_TIPO_ACOMPANHAMENTO']==1){
			$nomeTela = 'processo_mte_view';
			$titulo = "Processo MTE  (OS ".$rw['NU_SOLICITACAO'].")";
			$encontrouMTE = true;
		}
		elseif ($rw['ID_TIPO_ACOMPANHAMENTO']==3){
			$nomeTela = 'processo_prorrog_view';
			$titulo = "Prorrogação  (OS ".$rw['NU_SOLICITACAO'].")";
		}
		elseif ($rw['ID_TIPO_ACOMPANHAMENTO']==4){
			$nomeTela = 'processo_regcie_view';
			$titulo = "Registro CIE  (OS ".$rw['NU_SOLICITACAO'].")";
		}
		elseif ($rw['ID_TIPO_ACOMPANHAMENTO']==5){
			$nomeTela = 'processo_emiscie_view';
			$titulo = "Emissão CIE  (OS ".$rw['NU_SOLICITACAO'].")";
		}
		elseif ($rw['ID_TIPO_ACOMPANHAMENTO']==6){
			$nomeTela = 'processo_cancel_view';
			$titulo = "Cancelamento  (OS ".$rw['NU_SOLICITACAO'].")";
		}
		else{
			$titulo = "OS do sistema antigo  (OS ".$rw['NU_SOLICITACAO'].")";
			//$encontrouMTE = true;
		}
		if ($nomeTela!=''){
			$tela = new cEDICAO($nomeTela);
			$tela->CarregarConfiguracao();
			$tela->mCampoNome['id_solicita_visto']->mValor = $rw['id_solicita_visto'];
			$tela->RecupereRegistro();
			print '<div class="subtitulo">'.$titulo.'</div>';
			print cINTERFACE::RenderizeEdicao($tela, 2);
			print '</table>';
		}
		else{
			$tela = new cEDICAO('processo_mte_view');
			$tela->CarregarConfiguracao();
			$tela->mCampoNome['id_solicita_visto']->mValor = $rw['id_solicita_visto'];
			$tela->RecupereRegistro();
			print '<div class="subtitulo">'.$titulo.'</div>';
			print '<p>Processo MTE</p>';
			print cINTERFACE::RenderizeEdicao($tela, 2);
			print '</table>';

			$tela = new cEDICAO('processo_prorrog_view');
			$tela->CarregarConfiguracao();
			$tela->mCampoNome['id_solicita_visto']->mValor = $rw['id_solicita_visto'];
			$tela->RecupereRegistro();
			print '<p>Prorrogação</p>';
			print cINTERFACE::RenderizeEdicao($tela, 2);
			print '</table>';

			$tela = new cEDICAO('processo_regcie_view');
			$tela->CarregarConfiguracao();
			$tela->mCampoNome['id_solicita_visto']->mValor = $rw['id_solicita_visto'];
			$tela->RecupereRegistro();
			print '<p>Registro CIE</p>';
			print cINTERFACE::RenderizeEdicao($tela, 2);
			print '</table>';

			$tela = new cEDICAO('processo_cancel_view');
			$tela->CarregarConfiguracao();
			$tela->mCampoNome['id_solicita_visto']->mValor = $rw['id_solicita_visto'];
			$tela->RecupereRegistro();
			print '<p>Cancelamento</p>';
			print cINTERFACE::RenderizeEdicao($tela, 2);
			print '</table>';

		}
	}
}
