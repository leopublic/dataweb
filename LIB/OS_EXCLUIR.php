<?php
session_start();

include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$OS = new cORDEMSERVICO();
$OS->Recuperar($_GET['id_solicita_visto']);
$OS->Excluir();

$_SESSION['msg'] = 'Ordem de serviço '.$OS->mNU_SOLICITACAO.' excluída com sucesso.';
session_commit();
header("Location:/intranet/geral/os_listar.php");
