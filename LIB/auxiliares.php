<?php

function insereEscolaridade($codigo,$nome,$ingles) {
   $sql = "insert into ESCOLARIDADE (CO_ESCOLARIDADE,NO_ESCOLARIDADE,NO_ESCOLARIDADE_EM_INGLES)";
   return SQLaux($sql." values ('$codigo','$nome','$ingles')","INSERIR","ESCOLARIDADE");
}

function insereEstadoCivil($codigo,$nome,$ingles) {
   $sql = "insert into ESTADO_CIVIL (CO_ESTADO_CIVIL,NO_ESTADO_CIVIL,NO_ESTADO_CIVIL_EM_INGLES)";
   return SQLaux($sql." values ('$codigo','$nome','$ingles')","INSERIR","ESTADO_CIVIL");
}

function insereFuncaoCargo($codigo,$nome,$ingles,$cbo) {
   if( (strlen($codigo)==0) || ($codigo=="0") ) { $codigo = pegaProximo("FUNCAO_CARGO","CO_FUNCAO"); }
   $sql = "insert into FUNCAO_CARGO (CO_FUNCAO,NO_FUNCAO,NO_FUNCAO_EM_INGLES,NU_CBO)";
   return SQLaux($sql." values ($codigo,'$nome','$ingles','$cbo')","INSERIR","FUNCAO_CARGO");
}

function insereGrauParentesco($codigo,$nome,$ingles) {
   $sql = "insert into GRAU_PARENTESCO (CO_GRAU_PARENTESCO,NO_GRAU_PARENTESCO,NO_GRAU_PARENTESCO_EM_INGLES)";
   return SQLaux($sql." values ('$codigo','$nome','$ingles')","INSERIR","GRAU_PARENTESCO");
}

function insereProfissao($codigo,$nome,$ingles,$cbo) {  
   if( (strlen($codigo)==0) || ($codigo=="0") ) { $codigo = pegaProximo("PROFISSAO","CO_PROFISSAO"); }
   $sql = "insert into PROFISSAO (CO_PROFISSAO,NO_PROFISSAO,NO_PROFISSAO_EM_INGLES,NU_CBO)";
   return SQLaux($sql." values ($codigo,'$nome','$ingles','$cbo')","INSERIR","PROFISSAO");
}

function insereTipoAutorizacao($codigo,$nome,$red) {
   $sql = "insert into TIPO_AUTORIZACAO (CO_TIPO_AUTORIZACAO,NO_TIPO_AUTORIZACAO,NO_REDUZIDO_TIPO_AUTORIZACAO)";
   return SQLaux($sql." values ('$codigo','$nome','$red')","INSERIR","TIPO_AUTORIZACAO");
}

function insereTipoSolicitacao($codigo,$nome,$qtd) {
   $sql = "insert into TIPO_SOLICITACAO (CO_TIPO_SOLICITACAO,NO_TIPO_SOLICITACAO,QT_DIAS_ATENDIMENTO)";
   return SQLaux($sql." values ('$codigo','$nome','$qtd')","INSERIR","TIPO_SOLICITACAO");
}

function inserePaisNacionalidade($codigo,$nome,$nacional,$ingles,$nacingles) {
   if( (strlen($codigo)==0) || ($codigo=="0") ) { $codigo = pegaProximo("PAIS_NACIONALIDADE","CO_PAIS"); }
   $sql = "insert into PAIS_NACIONALIDADE (CO_PAIS,NO_PAIS,NO_NACIONALIDADE,NO_PAIS_EM_INGLES,NO_NACIONALIDADE_EM_INGLES)";
   return SQLaux($sql." values ($codigo,'$nome','$nacional','$ingles','$nacingles')","INSERIR","PAIS_NACIONALIDADE");
}

function insereReparticaoConsular($codigo,$nome,$tel,$ender,$cidade,$pais) {
   if( (strlen($codigo)==0) || ($codigo=="0") ) { $codigo = pegaProximo("REPARTICAO_CONSULAR","CO_REPARTICAO_CONSULAR"); }
   if(strlen($pais)==0) { $pais="NULL"; }
   $sql = "insert into REPARTICAO_CONSULAR (CO_REPARTICAO_CONSULAR,NO_REPARTICAO_CONSULAR,";
   $sql = $sql."NO_ENDERECO,NO_CIDADE,CO_PAIS,NU_TELEFONE)";
   return SQLaux($sql." values ($codigo,'$nome','$ender','$cidade',$pais,'$tel')","INSERIR","REPARTICAO_CONSULAR");
}

function insereNivelContato($codigo,$nome,$ingles) {
   global $usulogado;
   if(strlen($usulogado)==0) { $usulogado="NULL"; }
   $sql = "insert into contato_nivel (id_nivel,nm_nivel,nm_nivel_ingles,cd_admin)";
   return SQLaux($sql." values ('$codigo','$nome','$ingles',$usulogado)","INSERIR","CONTATO_NIVEL");
}


function alteraEscolaridade($codigo,$nome,$ingles) {
   $sql = "update ESCOLARIDADE set NO_ESCOLARIDADE='$nome',NO_ESCOLARIDADE_EM_INGLES='$ingles'";
   return SQLaux($sql." where CO_ESCOLARIDADE='$codigo'","ALTERAR","ESCOLARIDADE");
}

function alteraEstadoCivil($codigo,$nome,$ingles) {
   $sql = "update ESTADO_CIVIL set NO_ESTADO_CIVIL='$nome',NO_ESTADO_CIVIL_EM_INGLES='$ingles'";
   return SQLaux($sql." where CO_ESTADO_CIVIL='$codigo'","ALTERAR","ESTADO_CIVIL");
}

function alteraFuncaoCargo($codigo,$nome,$ingles,$cbo) {
   $sql = "update FUNCAO_CARGO set NO_FUNCAO='$nome',NO_FUNCAO_EM_INGLES='$ingles',NU_CBO='$cbo'";
   return SQLaux($sql." where CO_FUNCAO=$codigo","ALTERAR","FUNCAO_CARGO");
}

function alteraGrauParentesco($codigo,$nome,$ingles) {
   $sql = "update GRAU_PARENTESCO set NO_GRAU_PARENTESCO='$nome',NO_GRAU_PARENTESCO_EM_INGLES='$ingles'";
   return SQLaux($sql." where CO_GRAU_PARENTESCO='$codigo'","ALTERAR","GRAU_PARENTESCO");
}

function alteraProfissao($codigo,$nome,$ingles,$cbo) {  
   $sql = "update PROFISSAO set NO_PROFISSAO='$nome',NO_PROFISSAO_EM_INGLES='$ingles',NU_CBO='$cbo'";
   return SQLaux($sql." where CO_PROFISSAO=$codigo","ALTERAR","PROFISSAO");
}

function alteraTipoAutorizacao($codigo,$nome,$red) {
   $sql = "update TIPO_AUTORIZACAO set NO_TIPO_AUTORIZACAO='$nome',NO_REDUZIDO_TIPO_AUTORIZACAO='$red'";
   return SQLaux($sql." where CO_TIPO_AUTORIZACAO='$codigo'","ALTERAR","TIPO_AUTORIZACAO");
}

function alteraTipoSolicitacao($codigo,$nome,$qtd) {
   $sql = "update TIPO_SOLICITACAO set NO_TIPO_SOLICITACAO='$nome',QT_DIAS_ATENDIMENTO='$qtd'";
   return SQLaux($sql." where CO_TIPO_SOLICITACAO='$codigo'","ALTERAR","TIPO_SOLICITACAO");
}

function alteraPaisNacionalidade($codigo,$nome,$nacional,$ingles,$nacingles) {
   $sql = "update PAIS_NACIONALIDADE set NO_PAIS='$nome',NO_NACIONALIDADE='$nacional',";
   $sql = $sql."NO_PAIS_EM_INGLES='$ingles',NO_NACIONALIDADE_EM_INGLES='$nacingles' where CO_PAIS=$codigo ";
   return SQLaux($sql,"ALTERAR","PAIS_NACIONALIDADE");
}

function alteraReparticaoConsular($codigo,$nome,$tel,$ender,$cidade,$pais) {
   if(strlen($pais)==0) { $pais="NULL"; }
   $sql = "insert into REPARTICAO_CONSULAR set NO_REPARTICAO_CONSULAR='$nome',NO_ENDERECO='$ender',";
   $sql = $sql."NO_CIDADE='$cidade',CO_PAIS=$pais,NU_TELEFONE='$tel' where CO_REPARTICAO_CONSULAR=$codigo ";
   return SQLaux($sql,"ALTERAR","REPARTICAO_CONSULAR");
}

function alteraNivelContato($codigo,$nome,$ingles) {
   global $usulogado;
   if(strlen($usulogado)==0) { $usulogado="NULL"; }
   $sql = "update contato_nivel set nm_nivel='$nome',nm_nivel_ingles='$ingles',cd_admin=$usulogado";
   return SQLaux($sql." where id_nivel = '$codigo' ","ALTERAR","CONTATO_NIVEL");
}


function removeEscolaridade($codigo) {
   return SQLaux("delete from ESCOLARIDADE where CO_ESCOLARIDADE='$codigo'","REMOVER","ESCOLARIDADE");
}

function removeEstadoCivil($codigo) {
   return SQLaux("delete from ESTADO_CIVIL where CO_ESTADO_CIVIL='$codigo'","REMOVER","ESTADO_CIVIL");
}

function removeFuncaoCargo($codigo) {
   return SQLaux("delete from FUNCAO_CARGO where CO_FUNCAO=$codigo","REMOVER","FUNCAO_CARGO");
}

function removeGrauParentesco($codigo) {
   return SQLaux("delete from GRAU_PARENTESCO where CO_GRAU_PARENTESCO='$codigo'","REMOVER","GRAU_PARENTESCO");
}

function removeProfissao($codigo) {  
   return SQLaux("delete from PROFISSAO where CO_PROFISSAO=$codigo","REMOVER","PROFISSAO");
}

function removeTipoAutorizacao($codigo) {
   return SQLaux("delete from TIPO_AUTORIZACAO where CO_TIPO_AUTORIZACAO='$codigo'","REMOVER","TIPO_AUTORIZACAO");
}

function removeTipoSolicitacao($codigo) {
   return SQLaux("delete from TIPO_SOLICITACAO where CO_TIPO_SOLICITACAO='$codigo'","REMOVER","TIPO_SOLICITACAO");
}

function removePaisNacionalidade($codigo) {
   return SQLaux("delete from PAIS_NACIONALIDADE where CO_PAIS=$codigo","REMOVER","PAIS_NACIONALIDADE");
}

function removeReparticaoConsular($codigo) {
   return SQLaux("delete from REPARTICAO_CONSULAR where CO_REPARTICAO_CONSULAR=$codigo","REMOVER","REPARTICAO_CONSULAR");
}

function removeNivelContato($codigo) {
   global $usulogado;
   if(strlen($usulogado)==0) { $usulogado="NULL"; }
   $sql = "delete from contato_nivel where id_nivel = '$codigo' ";
   return SQLaux($sql,"ALTERAR","CONTATO_NIVEL");
}


function SQLaux($sql,$acao,$tabela) {
  $ret = "";
  mysql_query($sql);
  if(mysql_errno()>0) {
    gravaLogErro($sql,mysql_error(),$acao,$tabela);
    $ret = mysql_error()." SQL=$sql";
  } else {
    gravaLog($sql,mysql_error(),$acao,$tabela);
  }
  return $ret;   
}

?>
