<?php
header('Content-Type: text/html; charset=UTF-8');
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$pID_SOLICITA_VISTO = $_GET['ID_SOLICITA_VISTO'];
$pNU_CANDIDATO = $_GET['NU_CANDIDATO'];

$OS = new cORDEMSERVICO();
$OS->Recuperar($pID_SOLICITA_VISTO);

print cINTERFACE_CANDIDATO::ExibeSolicitacao($OS->mID_SOLICITA_VISTO, $pNU_CANDIDATO, $OS->mNU_SOLICITACAO, $OS->mID_TIPO_ACOMPANHAMENTO);
