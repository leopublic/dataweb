<?php

function ListaMotivosRS($ativos,$tipo,$order) {
  $sql = "select CD_MOTIVO,DS_MOTIVO,TP_MOTIVO,ATIVO from FINAN_MOTIVO where CD_MOTIVO>0 ";
  if($ativos=="S") {
    $sql = $sql." and ATIVO='S' ";
  }
  if(strlen($tipo)>0) {
    $sql = $sql." and TP_MOTIVO='$tipo' ";
  }
  if(strlen($order)>0) {
    $sql = $sql." order by $order ";
  }
  $rs=mysql_query($sql);
  return $rs;
}

function comboMotivos($ativos,$tipo,$order,$motivo) {
  $ret = "<option value=''>Escolha um Motivo</option>";
  $rs = ListaMotivosRS($ativos,$tipo,$order);
  while($rw=mysql_fetch_array($rs)) {
     $sel = "";
     $cod = $rw['CD_MOTIVO'];
     $desc = $rw['DS_MOTIVO'];
     if( ($cod==$motivo) && (strlen($motivo)>0) ) { $sel="selected"; }
     $ret = $ret."<option value='$cod' $sel>$desc</option>";
  }
  return $ret;
}

function DescMotivo($motivo) {
  $ret="";
  $sql = "select DS_MOTIVO from FINAN_MOTIVO where CD_MOTIVO=$motivo ";
  $rs=mysql_query($sql);
  if($rw=mysql_fetch_array($rs)) {
    $ret = $rw[0];
  }
  return $ret;
}

function InsereMotivo($descricao,$tipo) {
  $ret = "";
  $desc = addslashes($descricao);
  $cod = pegaProximo("FINAN_MOTIVO","CD_MOTIVO");
  $sql = "insert into FINAN_MOTIVO (CD_MOTIVO,DS_MOTIVO,TP_MOTIVO,ATIVO) values ";
  $sql = $sql."($cod,'$desc','$tipo','A')";
  $rs=mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
  }
  return $ret;
}

function AlteraMotivo($cod,$descricao,$tipo,$status) {
  $ret = "";
  $desc = addslashes($descricao);
  $sql = "update FINAN_MOTIVO set DS_MOTIVO='$desc',TP_MOTIVO='$tipo',ATIVO='$status' where CD_MOTIVO=$cod ";
  $rs=mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
  }
  return $ret;
}

function DescAcao($acao) {
  $ret = "Não encontrada";
  if($acao=="S") { $ret = "Solicitação"; }
  if($acao=="A") { $ret = "Autorização"; }
  if($acao=="N") { $ret = "Negação"; }
  if($acao=="P") { $ret = "Pagamento"; }
  if($acao=="R") { $ret = "Recibo"; }
  return $ret;
}

function DescTipoPedido($tipo) {
  $ret = "Não encontrado";
  if($acao=="E") { $ret = "Despesa Empresa"; }
  if($acao=="P") { $ret = "Despesa Processo"; }
  return $ret;
}

function verificaSituacao($objsol) {
  $obj = $objsol[0];
  $dtsol = dataMy2BR($obj->getDT_SOLICITACAO());
  $dtaut = dataMy2BR($obj->getDT_AUTORIZADO());
  $dtpag = dataMy2BR($obj->getDT_PAGAMENTO()); 
  $dtrec = dataMy2BR($obj->getDT_RECIBO());
  $status = "I";
  if(strlen($dtsol)>0) { $status="S"; }
  if(strlen($dtaut)>0) { $status="A"; }
  if(strlen($dtpag)>0) { $status="P"; }
  if(strlen($dtrec)>0) { $status="F"; }
  return $status;
}

function descSituacao($sit) {
  $status = "Indefinido";
  if($sit=="S") { $status="Solicitado"; }
  if($sit=="A") { $status="Autorizado"; }
  if($sit=="P") { $status="Pago"; }
  if($sit=="N") { $status="Negado"; }
  if($sit=="F") { $status="Finalizado"; }
  return $status;
}

function proxSituacao($sit) {
  $status = "Indefinido";
  if($sit=="S") { $status="Autorizar"; }
  if($sit=="A") { $status="Efetuar Pagamento"; }
  if($sit=="P") { $status="Enviar Recibo"; }
  if($sit=="N") { $status="Alterar Pedido"; }
  if($sit=="F") { $status="Sem Pendência"; }
  return $status;
}

function insereFinanLibs($cd_libs,$cd_tipo_sol,$nu_solicitacao,$cd_admin,$cd_ano) {
  $ret = "";
  if(strlen($cd_ano)==0) { $cd_ano=0; }
  $sql = "INSERT INTO finan_libs (cd_finan_libs,cd_tipo_sol,nu_solicitacao,cd_admin,dt_cadastro,cd_ano) values ";
  $sql = $sql."($cd_libs,'$cd_tipo_sol',$nu_solicitacao,$cd_admin,NOW(),$cd_ano)";
  $rs=mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
  }
  return $ret;
}

?>
