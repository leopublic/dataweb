<?php
include("finan_class.php");
include("finan_geral.php");

function BuscaSolicitacaoVale($cdadmin,$data,$empresa,$candidato,$solicitacao,$pedido,$tipo,$order) {
# tipo => S = solicitante / A = autorizador / P = pagador => tem que passar o cdadmin
# tipo => N1 = são para pedidos que ainda não foram autorizados
# tipo => N2 = são para pedidos que foram autorizados mas não dado dinheiro
# tipo => N3 = são para pedidos que foram dado dinheiro e não possui nota fiscal
# solicitacao => agrupa por solicitacao (pedido da empresa/candidato/solicitacao)
  $ok = 0;
  $sql = "select a.CD_FINANCEIRO,a.CD_SOLICITACAO,a.CD_EMPRESA,a.CD_CANDIDATO,a.CD_ADMIN_SOL,";
  $sql = $sql."a.DT_SOLICITACAO,a.VL_SOLICITADO,a.CD_ADMIN_AUTO,a.DT_AUTORIZADO,";
  $sql = $sql."a.VL_AUTORIZADO,a.CD_ADMIN_PAGO,a.DT_PAGAMENTO,a.VL_PAGAMENTO,a.CD_ADMIN_RECIBO,";
  $sql = $sql."a.DT_RECIBO,a.VL_RECIBO,a.CD_MOTIVO,a.DT_ULT_ALTERACAO,";
  $sql = $sql."b.DS_MOTIVO,b.TP_MOTIVO ";
  $sql = $sql." from FINAN_SOLICITACAO a , FINAN_MOTIVO b  ";
  $sql = $sql." where a.CD_FINANCEIRO>0 and a.CD_MOTIVO=b.CD_MOTIVO ";
  if(strlen($pedido)>0) {
    $ok++;
    $sql = $sql." and a.CD_FINANCEIRO=$pedido ";
  }
  if(strlen($cdadmin)>0) {
    $ok++;
    if($tipo=="S") {
       $sql = $sql." and a.CD_ADMIN_SOL=$cdadmin ";
    } else if($tipo=="A") {
       $sql = $sql." and a.CD_ADMIN_AUTO=$cdadmin ";
    } else if($tipo=="P") {
       $sql = $sql." and a.CD_ADMIN_PAGO=$cdadmin ";
    } else {
       $sql = $sql." and (a.CD_ADMIN_SOL=$cdadmin OR a.CD_ADMIN_AUTO=$cdadmin OR a.CD_ADMIN_PAGO=$cdadmin) ";
    }
  }
  if(strlen($empresa)>0) {
    $ok++;
    $sql = $sql." and a.CD_EMPRESA=$empresa ";
  }	 
  if(strlen($candidato)>0) {
    $ok++;
    $sql = $sql." and a.CD_CANDIDATO=$candidato ";
  }
  if(strlen($pedido)>0) {
    $ok++;
    $sql = $sql." and a.CD_SOLICITACAO=$solicitacao ";
  }
  if(strlen($data)>0) {
    $ok++;
    $dia = dataBR2My($data);
    if(strlen($tipo)==0) {
       $sql = $sql." and (a.DT_SOLICITACAO=$dia OR a.DT_AUTORIZADO=$dia OR a.DT_PAGAMENTO=$dia) ";
    } else if($tipo=="S") {
       $sql = $sql." and a.DT_SOLICITACAO=$dia ";
    } else if($tipo=="A") {
       $sql = $sql." and a.DT_AUTORIZADO=$dia ";
    } else if($tipo=="P") {
       $sql = $sql." and a.DT_PAGAMENTO=$dia ";
    }
  }
  if($tipo=="N1") {
    $ok++;
    $sql = $sql." and a.DT_SOLICITACAO is not NULL and a.DT_AUTORIZADO is NULL ";
  }
  if($tipo=="N2") {
    $ok++;
    $sql = $sql." and a.DT_AUTORIZADO is not NULL and a.DT_PAGAMENTO is NULL ";
  }
  if($tipo=="N3") {
    $ok++;
    $sql = $sql." and a.DT_PAGAMENTO is not NULL and a.DT_ENTREGA_RECIBO is NULL ";
  }
  if(strlen($order)>0) {
    $ok++;
    $sql = $sql." order by $order";
  }
  if($ok>0) {
    $x=0;
    $rs = mysql_query($sql);
    while($rw=mysql_fetch_array($rs)) {
      $obj = new objSolicitaVale();
      $obj->setCD_FINANCEIRO($rw['CD_FINANCEIRO']);
      $obj->setCD_SOLICITACAO($rw['CD_SOLICITACAO']);
      $obj->setCD_CANDIDATO($rw['CD_CANDIDATO']);
      $obj->setCD_EMPRESA($rw['CD_EMPRESA']);
      $obj->setCD_ADMIN_SOL($rw['CD_ADMIN_SOL']);
      $obj->setDT_SOLICITACAO($rw['DT_SOLICITACAO']);
      $obj->setVL_SOLICITADO($rw['VL_SOLICITADO']);
      $obj->setCD_ADMIN_AUTO($rw['CD_ADMIN_AUTO']);
      $obj->setDT_AUTORIZADO($rw['DT_AUTORIZADO']);
      $obj->setVL_AUTORIZADO($rw['VL_AUTORIZADO']);
      $obj->setCD_ADMIN_PAGO($rw['CD_ADMIN_PAGO']);
      $obj->setDT_PAGAMENTO($rw['DT_PAGAMENTO']);
      $obj->setVL_PAGAMENTO($rw['VL_PAGAMENTO']);
      $obj->setCD_ADMIN_RECIBO($rw['CD_ADMIN_RECIBO']);
      $obj->setDT_RECIBO($rw['DT_RECIBO']);
      $obj->setVL_RECIBO($rw['VL_RECIBO']);
      $obj->setCD_MOTIVO($rw['CD_MOTIVO']);
      $obj->setDS_MOTIVO($rw['DS_MOTIVO']);
      $obj->setDT_ULT_ALTERACAO($rw['DT_ULT_ALTERACAO']);
      $ret[$x]=$obj;
      $x++;
    }
  }
  return $ret;
}

function ListaHistoricoSolicitaVale($pedido) {
  $x = 0;
  $sql = "select a.CD_HISTORICO,a.CD_FINANCEIRO,a.CD_ADMIN,a.DT_HISTORICO,a.ARQUIVO,";
  $sql = $sql." a.VL_HISTORICO,a.CD_ACAO,a.DESCRICAO,b.NO_USUARIO as NM_ADMIN ";
  $sql = $sql." from FINAN_HISTORICO a , USUARIO b ";
  $sql = $sql." where CD_FINANCEIRO=$pedido and a.CD_ADMIN=b.NU_USUARIO";
  $rs = mysql_query($sql);
  while($rw=mysql_fetch_array($rs)) {
    $obj = new objHistSolicitaVale();
    $obj->setCD_HISTORICO($rw['CD_HISTORICO']);
    $obj->setCD_FINANCEIRO($rw['CD_FINANCEIRO']);
    $obj->setCD_ADMIN($rw['CD_ADMIN']);
    $obj->setDT_HISTORICO($rw['DT_HISTORICO']);
    $obj->setVL_HISTORICO($rw['VL_HISTORICO']);
    $obj->setCD_ACAO($rw['CD_ACAO']);
    $obj->setDESCRICAO($rw['DESCRICAO']);
    $obj->setNM_ADMIN($rw['NM_ADMIN']);
    $obj->setARQUIVO($rw['ARQUIVO']);
    $ret[$x]=$obj;
    $x++;
  }
  return $ret;
}

function PegaSeqHistorico($cdfinan) {
  $ret=0;
  $sql = "select max(CD_SEQ_HIST) from FINAN_HISTORICO where CD_FINANCEIRO=$cdfinan ";
  $rs=mysql_query($sql);
  if($rw=mysql_fetch_array($rs)) {
    $ret = 0+$rw[0];
  }
  $ret++;
  return $ret;
}

function InsereHistoricoSolicitacaoVale($obj,$cdEmpresa,$cdCandidato,$cdSolicitacao) {
  $ret="";
  $cod = pegaProximo("FINAN_HISTORICO","CD_HISTORICO");
  $cdfin = $obj->getCD_FINANCEIRO();
  $cdadm = $obj->getCD_ADMIN();
  $vlhis = $obj->getVL_HISTORICO();
  $cdaca = $obj->getCD_ACAO();
  $desc = $obj->getDESCRICAO();
  $cdseq = PegaSeqHistorico($cdfin);
  $sql = "insert into FINAN_HISTORICO (CD_HISTORICO,CD_FINANCEIRO,CD_SEQ_HIST,CD_ADMIN,";
  $sql = $sql."DT_HISTORICO,VL_HISTORICO,CD_ACAO,DESCRICAO) values ";
  $sql = $sql."($cod,$cdfin,$cdseq,$cdadm,now(),$vlhis,'$cdaca','$desc')";
  mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
  }
  if($ret=="") {
    enviarEmail($cdaca,$cdEmpresa,$cdSolicitacao,$cdCandidato,$cdfin,$cod,$vlhis);
  }
  if($ret == "") {
    $ret = GravaArquivoHistorico($cdEmpresa,$cdCandidato,$cdSolicitacao,$cdfin,$cdseq,$cod,$cdaca);
  }
  return $ret;
}

function GravaArquivoHistorico($empresa,$cdCandidato,$cdSolicitacao,$cdfin,$cdseq,$cdHist,$acao) {
  global $myhstdir,$myrecdir;
  $ret = "";
  $nomdir = $myhstdir;
  if($acao=="R") { $nomdir = $myrecdir; }
  if(!is_dir($nomdir)) {
    mkdir ($nomdir, 0770);
  }
  if(strlen($empresa)==0) {
    $ret = "Não foi informada a empresa.";
  } else {
    $updir = $nomdir."/".$empresa;
    if(!is_dir($updir)) {
      mkdir ($updir, 0770);
    }
    $aux1 = "000000000$cdSolicitacao";
    $aux2 = "000000000$cdfin";
    $aux3 = "000$cdseq";
    $nmarq = "HST".substr($aux1,strlen($aux1-8))."_F".substr($aux2,strlen($aux2-8))."_SEQ".substr($aux3,strlen($aux3-2));
    $noArqOriginal = $_FILES['NM_ARQUIVO_SOL']['name'];
    $ax1 = str_replace(".","#",$noArqOriginal);
    $aux = split("#",$ax1);
    $x = count($aux);
    if($x>1) {
      $extensao = $aux[$x-1];
    } else {
      $extensao = "txt";
    }
    $noArquivo = $nmarq.".".$extensao;
    $uploadfile = $updir."/".$noArquivo;
    if (!move_uploaded_file($_FILES['NM_ARQUIVO_SOL']['tmp_name'], $uploadfile)) {
      $ret = "Não foi possível gravar arquivo, mas a solicitação foi gravada.";
    } else {
      $sql = "update FINAN_HISTORICO set ARQUIVO='$noArquivo' where CD_HISTORICO=$cdHist";
      mysql_query($sql);
      if(mysql_errno!=0) {
       $ret = mysql_error();
      }
    }
  }
  return $ret;
}

function AlteraSolicitacaoVale($cod,$cdadm,$valor,$mot,$acao,$descricao,$dtrec,$cdEmpresa,$cdCandidato,$cdSolicitacao) {
  $ret="";
  $valor = str_replace(".","",$valor);
  $valor = str_replace(",",".",$valor);
  $desc = addslashes($descricao);
  if( (strlen($cod)==0) || ($cod==0) ) {
    $sql1 = "";
    $sql2 = "";
    $cod = pegaProximo("FINAN_SOLICITACAO","CD_FINANCEIRO"); 
    $sql1 = "insert into FINAN_SOLICITACAO (CD_FINANCEIRO,CD_MOTIVO";
    $sql2 = ") values ($cod,$mot";
    if(strlen($cdEmpresa)>0) { 
      $sql1 = $sql1.",CD_EMPRESA";
      $sql2 = $sql2.",$cdEmpresa";
    }
    if(strlen($cdCandidato)>0) { 
      $sql1 = $sql1.",CD_CANDIDATO";
      $sql2 = $sql2.",$cdCandidato";
    }
    if(strlen($cdSolicitacao)>0) { 
      $sql1 = $sql1.",CD_SOLICITACAO";
      $sql2 = $sql2.",$cdSolicitacao";
    }
    $sql = $sql1.$sql2.")";
    mysql_query($sql);
    if(mysql_errno()!=0) {
      $ret = mysql_error();
    }
  }
  if($ret=="") {
    if($acao=="S") { $ret = EntraFase1($cod,$cdadm,$valor,$mot,$acao,$dtrec); }
    if($acao=="A") { $ret = EntraFase2($cod,$cdadm,$valor,$mot,$acao,$dtrec); }
    if($acao=="N") { $ret = EntraFase11($cod,$cdadm,$valor,$mot,$acao,$dtrec); }
    if($acao=="P") { $ret = EntraFase3($cod,$cdadm,$valor,$mot,$acao,$dtrec); }
    if($acao=="R") { $ret = EntraFase4($cod,$cdadm,$valor,$mot,$acao,$dtrec); }
  }
  if($ret=="") {
    $obj = new objHistSolicitaVale();
    $obj->setCD_FINANCEIRO($cod);
    $obj->setCD_ADMIN($cdadm);
    $obj->setVL_HISTORICO($valor);
    $obj->setCD_ACAO($acao);
    $obj->setDESCRICAO($desc);
    $ret = InsereHistoricoSolicitacaoVale($obj,$cdEmpresa,$cdCandidato,$cdSolicitacao);
  }
  return $ret;
}

# Solicitação
function EntraFase1($cod,$cdadm,$valor,$mot,$acao,$dtrec) {
  $sql = "update FINAN_SOLICITACAO set CD_ADMIN_SOL=$cdadm,DT_SOLICITACAO=now(),VL_SOLICITADO='$valor'";
  $sql = $sql." where CD_FINANCEIRO=$cod ";
  mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
  }
  return $ret;
}

# Autorização
function EntraFase2($cod,$cdadm,$valor,$mot,$acao,$dtrec) {
  $sql = "update FINAN_SOLICITACAO set CD_ADMIN_AUTO=$cdadm,DT_AUTORIZADO=now(),VL_AUTORIZADO='$valor'";
  $sql = $sql." where CD_FINANCEIRO=$cod ";
  mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
  }
  return $ret;
}

# Pagamento
function EntraFase3($cod,$cdadm,$valor,$mot,$acao,$dtrec) {
  $sql = "update FINAN_SOLICITACAO set CD_ADMIN_PAGO=$cdadm,DT_PAGAMENTO=now(),VL_PAGAMENTO='$valor'";
  $sql = $sql." where CD_FINANCEIRO=$cod ";
  mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
  }
  return $ret;
}

# Recibo
function EntraFase4($cod,$cdadm,$valor,$mot,$acao,$dtrec) {
  $dia = dataBR2My($dtrec);
  $sql = "update FINAN_SOLICITACAO set CD_ADMIN_RECIBO=$cdadm,DT_RECIBO='$dia',VL_RECIBO='$valor'";
  $sql = $sql." where CD_FINANCEIRO=$cod ";
  mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
  }
  return $ret;
}

# Negação
function EntraFase11($cod,$cdadm,$valor,$mot,$acao,$dtrec) {
  $sql = "update FINAN_SOLICITACAO set CD_ADMIN_AUTO=$cdadm,DT_AUTORIZADO=now(),VL_AUTORIZADO=0";
  $sql = $sql." where CD_FINANCEIRO=$cod ";
  mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
  }
  return $ret;
}

function MostraSolicitacoes($idEmpresa,$idCandidato,$solicitacao,$detalha,$target,$pedido) {
  $ret = "<tr><td valign=top><b>Financeiro:</td>\n";
  $lista = BuscaSolicitacaoVale("","",$idEmpresa,$idCandidato,$solicitacao,$pedido,"","CD_FINANCEIRO");  
  $qtd = count($lista);
  if($qtd>0) {
     $ret = $ret."<td colspan=3><table border=1 align=center width=100% class='textoazulPeq' cellspacing=0><tr>\n";
     if($detalha=="E") { $ret = $ret."<td align=center><b>Sol.</td>"; }
     $ret = $ret."<td align=center><b>Administrador</td><td align=center><b>Data</td><td align=center><b>Valor</td><td align=center><b>Motivo</td><td align=center><b>Status</td></tr>";
     for($x=0; $x<$qtd;$x++) {
         $obj=$lista[$x];
         $pedido = $obj->getCD_FINANCEIRO();
         $nmadm = pegaNomeUsuario($obj->getCD_ADMIN_SOL());
         $dtsol = dataMy2BR($obj->getDT_SOLICITACAO());
         $valor = formataReais($obj->getVL_SOLICITADO());
         $motivo = $obj->getDS_MOTIVO();
         $dtaut = dataMy2BR($obj->getDT_AUTORIZADO());
         $dtpag = dataMy2BR($obj->getDT_PAGAMENTO()); 
         $dtrec = dataMy2BR($obj->getDT_RECIBO());
         $status = "Indefinido";
         if(strlen($dtsol)>0) { $status="Solicitado"; }
         if(strlen($dtaut)>0) { $status="Autorizado"; }
         if(strlen($dtpag)>0) { $status="Pago"; }
         if(strlen($dtrec)>0) { $status="Finalizado"; }
         if($detalha=="S") {
             $status = "<a href='javascript:MostraHistorico($idEmpresa,$idCandidato,$solicitacao,$pedido);' class='textoazulPeq' $target><u>$status</a>";
         }
         if($detalha=="E") { $ret = $ret."<tr><td align=center>$pedido</td>"; } else {  $ret = $ret."<tr>"; }
         $ret = $ret."<td>$nmadm</td><td align=center>$dtsol</td><td align=right>$valor</td><td>$motivo</td><td align=center>$status</td></tr>";
     }
     $ret = $ret."</table></td></tr>\n";
  } else {
     $ret = "<td colspan=3>Não há solicitação para o processo do candidato.</td></tr>\n";
  }
  return $ret;
}

function MostraHistoricoSolicitacoes($idEmpresa,$pedido,$target) {
  global $myhsturl,$myrecurl;
  $ret = "<tr><td colspan=4 align=center><b>Histórico da Solicitação &#160;&#160;&#160; $pedido</td></tr>\n";
  $lista = ListaHistoricoSolicitaVale($pedido);  
  $qtd = count($lista);
  if($qtd>0) {
     $ret = $ret."<tr><td colspan=4><table border=1 align=center width=100% class='textoazulPeq' cellspacing=0>\n";
     $ret = $ret."<tr><td align=center><b>Administrador</td><td align=center><b>Data</td><td align=center><b>Valor</td>";
     $ret = $ret."<td align=center><b>Ação</td><td align=center><b>Descrição</td><td align=center><b>Arq.</td></tr>";
     for($x=0; $x<$qtd;$x++) {
         $obj=$lista[$x];
         $acao = $obj->getCD_ACAO();
         $nmadm = $obj->getNM_ADMIN();
         $dtsol = dataMy2BR($obj->getDT_HISTORICO());
         $valor = formataReais($obj->getVL_HISTORICO());
         $dsacao = DescAcao($acao);
         $desc = $obj->getDESCRICAO();
         if(strlen($desc)==0) { $desc="&#160;"; }
         $arq1 = $obj->getARQUIVO();
         if(strlen($arq1)>0) {
           $arq2 = $arq1;
           $varq = "<a href='javascript:verArqHist($idEmpresa,\"$arq1\",\"$acao\");' class=textoazulPeq $target>VER</a>";
         } else {
           $varq = "Não";
         }
         $ret = $ret."<tr><td>$nmadm</td><td align=center>$dtsol</td><td align=right>$valor</td><td align=center>$dsacao</td><td>$desc</td><td align=center>$varq</td></tr>";
     }
     $ret = $ret."</table></td></tr>\n";
     $ret = $ret."<script language='javascript'>";
     $ret = $ret."function verArqHist(emp,nmarq,acao) {\n";
     if(acao=="R") {
       $ret = $ret."  var arq = \"$myrecurl/\"+emp+\"/\"+nmarq;\n";
     } else {
       $ret = $ret."  var arq = \"$myhsturl/\"+emp+\"/\"+nmarq;\n";
     }
     $ret = $ret."  window.open(arq);\n}\n</script>\n";
  } else {
     $ret = $ret."<td colspan=4>Não há histórico  para a  solicitação.</td></tr>\n";
  }
  return $ret;
}

function enviarEmail($acao,$empresa,$solicitacao,$candidato,$cdfin,$cdpedido,$valor) {
  global $usulogado,$myfinurl;
  if($acao=="S") { # Houve Solicitacao
    if(preAutorizado($cdfin,$valor)>0) {
      $aux = EntraFase2($cdfin,0,$valor,'Autorizacao Automatica','A','');
      $acao = "SA";
    }
  }
  $lstUsu = listaUsuariosProximaAcao($acao,$empresa,$solicitacao,$candidato,$cdfin,$cdpedido);
  $nomeEmpresa = pegaNomeEmpresa($empresa);
  $nomeCandidato = pegaNomeCandidato($candidato);
  $nomeAdmin = pegaNomeUsuario($usulogado);
  $param = "_".$empresa."_".$solicitacao."_".$candidato."_".$cdfin."_".$cdpedido."_".$acao."_";
  if($acao=="S") { # Houve Solicitacao
     $subject = "Solicitacao de Pagamento";
     $texto = "\nPrezado(a),\n\nHouve uma SOLICITAÇÃO de pagamento para o processo do candidato $nomeCandidato da empresa $nomeEmpresa pelo usuário $nomeAdmin.\n\n";
     enviaEmailPorUsuario($subject,$texto,$param,$lstUsu);
  } else if($acao=="SA") { # Houve Autorizacao Automatica
     $subject = "Autorizacao de Pagamento Automatica";
     $texto = "\nPrezado(a),\n\nHouve uma SOLICITAÇÃO de pagamento com AUTORIZAÇÃO AUTOMÁTICA para o processo do candidato $nomeCandidato da empresa $nomeEmpresa pelo usuário $nomeAdmin.\n\n";
     enviaEmailPorUsuario($subject,$texto,$param,$lstUsu);
  } else if($acao=="A") { # Houve Autorizacao
     $subject = "Autorizacao de Pagamento";
     $texto = "\nPrezado(a),\n\nHouve uma AUTORIZAÇÃO de pagamento para o processo do candidato $nomeCandidato da empresa $nomeEmpresa pelo usuário $nomeAdmin.\n\n";
     enviaEmailPorUsuario($subject,$texto,$param,$lstUsu);
  } else if($acao=="N") { # Houve Negação
     $subject = "Negacao de Pagamento";
     $texto = "\nPrezado(a),\n\nHouve uma NEGAÇÃO de pagamento para o processo do candidato $nomeCandidato da empresa $nomeEmpresa pelo usuário $nomeAdmin.\n\n";
     enviaEmailPorUsuario($subject,$texto,$param,$lstUsu);
  } else if($acao=="P") { # Houve Pagamento
     $subject = "Pagamento da Solicitacao";
     $texto = "\nPrezado(a),\n\nHouve PAGAMENTO da solicitação de pagamento para o processo do candidato $nomeCandidato da empresa $nomeEmpresa pelo usuário $nomeAdmin.\n\n";
     enviaEmailPorUsuario($subject,$texto,$param,$lstUsu);
  } else if($acao=="R") { # Houve Chegada de Recibo
     # Passar email ????? 
  }
}

function enviaEmailPorUsuario($subject,$texto,$param,$usuarios) {
  global $usulogado,$myfinurl;
  for($x=1;$x<count($usuarios);$x++) {
    $aux = $usuarios[$x];
    $cod = $aux[1];
    $nome = $aux[2];
    $para = $aux[3];
    $param = $param.$cod."_";
    $crc = crc32($param); 
    $urlwork = $myfinurl."/index.php?param=$crc&org=$param";
    $texto = $texto."Por favor, clique no link abaixo para proceguir ao workflow do processo.\n\n$urlwork\n\nObrigado.\n"; 
    gravaLogEmail($para,$subject,$texto);
    mail($para,$subject,$texto,'From: workflow-pag@mundivisasnit.com.br');
  }
}

function listaUsuariosProximaAcao($acao,$empresa,$solicitacao,$candidato,$cdfin,$cdpedido) {
  global $usulogado;
  $x = 0;
  $sql = "";
  if($acao=="S") { # Houve Solicitacao
#    $sql = "Lista usuarios que podem autorizar";
  } else if($acao=="A") { # Houve Autorizacao
#    $sql = "Lista usuarios que podem pagar";
  } else if($acao=="N") { # Houve Negação
#    $sql = "Lista usuarios que devem ser avisados que foi negado - solicitante + financeiro";
  } else if($acao=="P") { # Houve Pagamento
#    $sql = "Lista usuarios que devem ser avisados que foi pago - solicitante + coordenador";
  } else if($acao=="R") { # Houve Chegada de Recibo
# Devo avisar alguem ??????
  }
  $ret[0]=array(1=>0,2=>"Nome",3=>"Email");
  if(strlen($sql)>0) {
    $rs = mysql_query($sql);
    if(mysql_errno()==0) {
      while($rw=mysql_fetch_array($rs)) {
        $x++;
        $ret[$x]=array(1=>$rw['cod'],2=>$rw['nome'],3=>$rw['email']);
      }
    }
  } else {
    # Para teste apenas
    $ret[1]=array(1=>54,2=>"Romulo Petrobras",3=>"romulo@oceanicawebhome.com.br");
  }
  return $ret;
}

function preAutorizado($cdfin,$valor) {
  $ret = 0;
# Se ret=0 não é pré autorizado, e se ret>0 é
  return $ret;
}

function permissaoUsuario($cdadm,$acao) {
  $ret = 1;
# Se ret=0 não é permitido, e se ret>0 é
  return $ret;
}

?>
