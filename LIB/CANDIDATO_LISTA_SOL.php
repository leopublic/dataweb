<?php

header('Content-Type: text/html; charset=UTF-8');
include ("autenticacao.php");
include ("geral.php");

$id_solicita_visto = $_GET['id_solicita_visto'];
$lista = $_GET['lista'];
$alterar = $_GET['alterar'];
//
// Pega status da solicitação
$sql = "select ifnull(FL_ENCERRADA, 0) FL_ENCERRADA, ID_TIPO_ACOMPANHAMENTO "
        . " from solicita_visto "
        . " left join servico on servico.nu_servico = solicita_visto.nu_servico "
        . " left join status_solicitacao ss on ss.id_status_sol = solicita_visto.id_status_sol "
        . " where id_solicita_visto = " . $id_solicita_visto;
$linhas = conectaQuery($sql, __FILE__);
$retorno = '';
$rs = mysql_fetch_array($linhas);
$FL_ENCERRADA = $rs['FL_ENCERRADA'];
$id_tipo_acompanhamento = $rs['ID_TIPO_ACOMPANHAMENTO'];
//
// Pega os candidatos
$sql = "SELECT distinct CANDIDATO.NU_CANDIDATO
        , CANDIDATO.NOME_COMPLETO
        , CANDIDATO.NU_PASSAPORTE
        , NO_NACIONALIDADE
        , CANDIDATO.NU_RNE
        , CANDIDATO.BO_CADASTRO_MINIMO_OK
        , NO_RAZAO_SOCIAL
        , NO_EMBARCACAO_PROJETO
        , CANDIDATO.nu_candidato_parente
        , no_grau_parentesco
        , autc_fl_revisado
        , usua_tx_apelido
        , (select ifnull(count(distinct solicita_visto.id_solicita_visto) , 0)
				from solicita_visto
                    left join status_solicitacao ss on ss.id_status_sol = solicita_visto.id_status_sol
                    join autorizacao_candidato on autorizacao_candidato.id_solicita_visto = solicita_visto.id_solicita_visto
                    join servico on servico.nu_servico = solicita_visto.nu_servico
					where autorizacao_candidato.nu_candidato =  CANDIDATO.nu_candidato
					and coalesce(ss.fl_encerrada, 0) = 0
					and solicita_visto.id_solicita_visto <> " . cBANCO::ChaveOk($id_solicita_visto) . "
					and servico.id_tipo_acompanhamento = 1
					) qtd_os_aut
			  , (select ifnull(count(distinct solicita_visto.id_solicita_visto) , 0)
					from solicita_visto
                    left join status_solicitacao ss on ss.id_status_sol = solicita_visto.id_status_sol
                    join autorizacao_candidato on autorizacao_candidato.id_solicita_visto = solicita_visto.id_solicita_visto
                    join servico on servico.nu_servico = solicita_visto.nu_servico
					where autorizacao_candidato.nu_candidato =  CANDIDATO.nu_candidato
					and coalesce(ss.fl_encerrada, 0) = 0
					and solicita_visto.id_solicita_visto <> " . cBANCO::ChaveOk($id_solicita_visto) . "
					and servico.id_tipo_acompanhamento = 4
					) qtd_os_reg
			  , (select ifnull(count(distinct solicita_visto.id_solicita_visto) , 0)
					from solicita_visto
                    left join status_solicitacao ss on ss.id_status_sol = solicita_visto.id_status_sol
                    join autorizacao_candidato on autorizacao_candidato.id_solicita_visto = solicita_visto.id_solicita_visto
                    join servico on servico.nu_servico = solicita_visto.nu_servico
					where autorizacao_candidato.nu_candidato =  CANDIDATO.nu_candidato
					and coalesce(ss.fl_encerrada, 0) = 0
					and solicita_visto.id_solicita_visto <> " . cBANCO::ChaveOk($id_solicita_visto) . "
					and servico.id_tipo_acompanhamento = 3
					) qtd_os_pro
			  ";
$sql.= "  FROM CANDIDATO ";
$sql.= "  left join autorizacao_candidato ac on ac.id_solicita_visto = " . $id_solicita_visto . " and ac.nu_candidato = CANDIDATO.nu_candidato";
$sql.= "  left join CANDIDATO cp on cp.NU_CANDIDATO = CANDIDATO.nu_candidato_parente";
$sql.= "  left join PAIS_NACIONALIDADE P on P.CO_PAIS = CANDIDATO.CO_NACIONALIDADE";
$sql.= "  left join EMPRESA E on E.NU_EMPRESA = CANDIDATO.NU_EMPRESA";
$sql.= "  left join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = CANDIDATO.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = CANDIDATO.NU_EMBARCACAO_PROJETO";
$sql.= "  left join grau_parentesco gp on gp.co_grau_parentesco = CANDIDATO.co_grau_parentesco ";
$sql.= "  left join usuarios u on u.cd_usuario = ac.cd_usuario_revisao ";
$sql.= " where CANDIDATO.NU_CANDIDATO in (" . $lista . ")";
if (!$FL_ENCERRADA && ($id_tipo_acompanhamento == '' || $id_tipo_acompanhamento == 1)) {
    $sql .= "or (CANDIDATO.nu_candidato_parente in (" . $lista . ") and CANDIDATO.NU_CANDIDATO not in (" . $lista . "))";
}
$sql.= " ORDER BY concat(coalesce(cp.NOME_COMPLETO, ''), CANDIDATO.NOME_COMPLETO) asc ";
$linhas = conectaQuery($sql, __FILE__);
$retorno = '';
while ($rs = mysql_fetch_array($linhas)) {
    if (!$FL_ENCERRADA) {
        if ($rs['nu_candidato_parente'] != '') {
            if (strstr($lista, $rs['NU_CANDIDATO'])) {
                $acao = '<img src="/imagens/grey16/Trash.png" class="remove_candidato" rel="' . $rs['NU_CANDIDATO'] . '" alt="' . $id_solicita_visto . '" title="clique para remover esse candidato" style="cursor:pointer;" />';
                $estilo = "";
            } else {
                $acao = '<img src="/imagens/grey16/Plus.png" class="adicionar_candidato" rel="' . $rs['NU_CANDIDATO'] . '" alt="' . $id_solicita_visto . '" title="clique para adicionar esse dependente à OS" style="cursor:pointer;" />';
                $estilo = "cinza";
            }
        } else {
            $acao = '<img src="/imagens/grey16/Trash.png" class="remove_candidato" rel="' . $rs['NU_CANDIDATO'] . '" alt="' . $id_solicita_visto . '" title="clique para remover esse candidato" style="cursor:pointer;" />';
            $estilo = "";
        }
        $acao.= '<a href="#" onClick="javascript:enviaSenha(\''.$rs['NU_CANDIDATO'].'\');" title="clique para remover esse candidato" style="margin-left:5px;"><img src="/imagens/grey16/Key.png" /></a>';
    }


    $retorno .= '<tr class="' . $estilo . '">';
    $retorno .= '<td style="text-align:center;">' . $acao . '</td>';

    if ($rs['nu_candidato_parente'] != '') {
        if (strstr($lista, $rs['nu_candidato_parente'])) {
            $nome = '&nbsp;&nbsp;&nbsp;<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $rs['NU_CANDIDATO'] . '" target="_blank">' . $rs['no_grau_parentesco'] . ': ' . $rs['NOME_COMPLETO'] . '</a>';
        } else {
            $cand = new cCANDIDATO();
            $cand->Recuperar($rs['nu_candidato_parente']);
            $nome = '&nbsp;&nbsp;&nbsp;<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $rs['NU_CANDIDATO'] . '" target="_blank">' . $rs['NOME_COMPLETO'] . '</a>';
            $nome.= ' (' . $rs['no_grau_parentesco'] . ' de <a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $rs['nu_candidato_parente'] . '" target="_blank">' . $cand->mNOME_COMPLETO . '</a>)';
        }
        $adcDep = '--';
    } else {
        $nome = $rs['NOME_COMPLETO'];
        $nome = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $rs['NU_CANDIDATO'] . '" target="_blank">' . $nome . '</a>';
        $adcDep = '<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=' . $rs['NU_CANDIDATO'] . '&painelAtivo=6" target="_blank">Dependentes</a>';
    }
    $retorno .= '<td>' . $nome . '</td>';
    $retorno .= '<td style="text-align:center;" class="button">' . $adcDep . '</td>';

    $qtd_os = "";
    $br = "";
    if ($rs['qtd_os_aut'] > 0) {
        $qtd_os .= $br . $rs['qtd_os_aut'] . " aut.";
        $br = "<br/>";
    }
    if ($rs['qtd_os_reg'] > 0) {
        $qtd_os .= $br . $rs['qtd_os_reg'] . " reg.";
        $br = "<br/>";
    }
    if ($rs['qtd_os_pro'] > 0) {
        $qtd_os .= $br . $rs['qtd_os_pro'] . " pror.";
        $br = "<br/>";
    }
    if ($qtd_os == "") {
        $qtd_os = "--";
    }

    $retorno .= '<td style="text-align:center;" >' . $qtd_os . '</td>';
    $retorno .= '<td>' . $rs['NO_RAZAO_SOCIAL'] . '</td>';
    $retorno .= '<td>' . $rs['NO_EMBARCACAO_PROJETO'] . '</td>';
    $retorno .= '<td>' . $rs['NO_NACIONALIDADE'] . '</td>';
    $retorno .= '<td>' . $rs['NU_PASSAPORTE'] . '</td>';
    if (strstr($lista, $rs['NU_CANDIDATO'])) {
        if ($rs['autc_fl_revisado'] == 1) {
            $cadastro = '<i class="icon-ok"></i>';
            if ($rs['usua_tx_apelido'] != '') {
                $cadastro .= '<br/><span style="font-size:10px;color:#999;">(por ' . $rs['usua_tx_apelido'] . ')</span>';
            }
        } else {
            $cadastro = '<a href="/paginas/carregueComMenu.php?controller=cCTRL_CAND_REVISAR_OS&metodo=Edite&NU_CANDIDATO=' . $rs['NU_CANDIDATO'] . '&id_solicita_visto=' . $id_solicita_visto . '" target="_blank">Revisar</a>';
        }
    } else {
        $cadastro = '--';
    }
    $retorno .= '<td style="text-align:center;" class="button">' . $cadastro . '</td>';
    $retorno .= '</tr>' . "\n";
}
print '<table class="grid candidatos">
		<tr>
			<th width="50px">Ações</th>
			<th width="auto">Nome</th>
			<th width="auto">Deps</th>
			<th width="60px">Qtd OS<br/>abertas</th>
			<th width="auto">Empresa atual</th>
			<th width="auto">Emb./proj. atual</th>
			<th width="120px">Nacionalidade</th>
			<th width="100px">Passaporte</th>
			<th style="text-align:center">Cadastro</th>
		</tr>';
print $retorno;
print '</table>';
if ($id_solicita_visto > 0 && !$FL_ENCERRADA) {
    print '<p align="center" style="margin-top: 10px; margin-bottom: 10px;"><input type="button" value=" Refresh " onclick="CarregarCandidatos(\'' . $id_solicita_visto . '\', \'DadosOS_CANDIDATO_container\', \'DadosOS_CANDIDATO_chaves\',\'1\' );" style="width:auto"></p>';
}
