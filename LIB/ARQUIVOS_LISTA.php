<?php

header('Content-Type: text/html; charset=UTF-8');
// TODO: Verificar diferencas em relacao a versao de producao
include ("autenticacao.php");
include ("geral.php");
/*
 * TODO Migrar esse processo para dentro da classe cINTERFACE_CANDIDATO
 */
$NU_CANDIDATO = $_GET['NU_CANDIDATO'];
$NomeDiv = $_GET['NomeDiv'];

$cand = new cCANDIDATO();
$cand->mNU_CANDIDATO = $NU_CANDIDATO;
$rs = $cand->CursorArquivos();
$qtdArqs = 0;
$arquivos = "";
while ($rw = $rs->fetch()) {
    if ($rw['id_solicita_visto'] != 0) {
        $nmDir = $mysolurl . "/" . $rw['NU_EMPRESA'] . "/";
    } else {
        $nmDir = $mydocurl . "/" . $rw['NU_EMPRESA'] . "/" . $rw['NU_CANDIDATO'] . "/";
    }
    $qtdArqs = $qtdArqs + 1;
    $seq = $rw['NU_SEQUENCIAL'];
    if ($seq == ''){
        $seq = "(OS excluída)";
    }
    $nmArq = $rw['NO_ARQUIVO'];
    $tpArq = $rw['TP_ARQUIVO'];
    $nomeTipoArquivo = $rw['NO_TIPO_ARQUIVO'];
    $nmTpArq = $rw['CO_TIPO_ARQUIVO'];
    $nmArqOrg = $rw['NO_ARQ_ORIGINAL'];
    $dtInc = $rw['DT_INCLUSAO'];
    $numAdmin = $rw['NU_ADMIN'];
    $docpagina = $nmDir . $nmArq;
    $arq = explode(".", $nmArq);

    if (count($arq) > 1) {
        if ($rw['id_solicita_visto'] > 0) {
            $src = $mysoldir ;
            if ($rw['NU_EMPRESA'] > 0){
                $src .= DS .$rw['NU_EMPRESA'];
            }
            $src .= DS . $rw['NO_ARQUIVO'];
//			$src = $mysoldir.DS.$rw['NU_EMPRESA'].DS.$rw['NO_ARQUIVO'];
        } else {
            $src = $mydocdir ;
            $nu_empresa = 0 + $rw['NU_EMPRESA'];
            $src .= DS .$rw['NU_EMPRESA'];
            $src .= DS . $rw['NU_CANDIDATO'] . DS . $rw['NO_ARQUIVO'];
//			$src = $mydocdir.DS.$rw['NU_EMPRESA'].DS.$rw['NU_CANDIDATO'].DS.$rw['NO_ARQUIVO'];
        }
        if ($rw['id_solicita_visto'] > 0) {
            if ($rw['NU_SOLICITACAO'] > 0) {
                $sol = " - OS " . $rw['NU_SOLICITACAO'];
            } else {
                $sol = '(OS excluída)';
            }
        } else {
            $sol = '';
        }
        if ($_SESSION['myAdmin']['cd_perfil'] == '7') {
            $nomeArquivo = $nomeTipoArquivo;
        } else {
            $nomeArquivo = $nmTpArq . $sol . " (" . $nmArqOrg . ")";
        }

        if (file_exists($src)) {
            if (strtoupper($arq[1]) == "JPG" || strtoupper($arq[1]) == "TIFF" || strtoupper($arq[1]) == "BMP") {
                if ($rw['NU_SOLICITACAO'] != 0) {
                    $EhOS = 'false';
                } else {
                    $EhOS = 'true';
                }
//				$link ="<a href=\"javascript:CarregarArquivo('".$docpagina."', '".$seq."', '".$NomeDiv.'Visualizacao'.$rw['NU_CANDIDATO']."', ".$pEhOS.");\">" ;
                $link = "<a href=\"javascript:CarregarArquivo('" . $docpagina . "', '" . $seq . "', " . $EhOS . ");\">";
                //$link .= $nmTpArq.$sol." (".$nmArqOrg.")</a>";
                $link .= $nomeArquivo . "</a>";
            } else {
                $link = "<a href=\"" . $docpagina . "\" target=\"_blank\">";
                //$link .= $nmTpArq.$sol." (".$nmArqOrg.")</a>";
                $link .= $nomeArquivo . "</a>";
            }
        } else {
            $link = '<span style="text-decoration:line-through;color:#cccccc;">';
            $link .= $nomeArquivo . " </span>&nbsp;(arquivo não encontrado)";
        }
    }
    $arquivos.= '<tr><td colspan="2" style="padding-left:10px">';
    if (($usufunc == "S") || ($numAdmin == $usulogado)) {
        $arquivos.='<a class="img" href="download.php?arq=' . $src . '&nmf=' . $nmArqOrg . '"><img src="/imagens/icons/downld.gif" title="clique para baixar esse arquivo" style="border:none" /></a>';
        if ($rw['NU_SOLICITACAO'] != 0) {
            $arquivos.='&nbsp;<img src="/imagens/icons/vazio.png" />';
        } else {
            $arquivos.='&nbsp;<img src="/imagens/icons/delete.png" title="clique para remover esse arquivo" onclick="ajaxRemoverArquivo(\'' . $rw['NU_SEQUENCIAL'] . '\', \'' . $NU_CANDIDATO . '\');" style="cursor:pointer"/>';
        }
    }
    $arquivos.=$link;
    $arquivos.= "</td></tr>";
}
$tabela = '<table class="edicao dupla" style="width:auto">';
$tabela .= '<col width="50%"></col><col width="50%"></col>';
$tabela .= '<tr>' . "\n";
$tabela .= '	<td><img src="../imagens/icons/add.png" onclick="if($(\'#ARQUIVO' . $NU_CANDIDATO . '\').val()==\'\'){jAlert(\'Informe o arquivo a ser carregado\');}else{if($(\'#CMP_ID_TIPO_ARQUIVO' . $NU_CANDIDATO . '\').val()==\'\'){jAlert(\'Informe o tipo do arquivo\');}else{document.forms[\'novoArquivo' . $NU_CANDIDATO . '\'].submit();}};" style="cursor:pointer" title="Clique para adicionar o arquivo" style="vertical-align:middle"/><input type="file" id="ARQUIVO' . $NU_CANDIDATO . '" name="ARQUIVO' . $NU_CANDIDATO . '" style="width:auto"/></td>' . "\n";
$tabela .= '	<td>' . cINTERFACE::RenderizeCombo(new cCAMPO("ID_TIPO_ARQUIVO" . $NU_CANDIDATO, "Tipo", 3, "", "", "", "", 17)) . '</td>' . "\n";
$tabela .= '</tr>' . "\n";
$tabela .= '	<tr class="limpa">' . "\n";
$tabela .= '	<td colspan="2" style="text-align:center"><input type="button" value="Refresh" onclick="CarregarListaDeArquivos(\'' . $NU_CANDIDATO . '\',\'' . $NomeDiv . '\')" style="width:auto"/></td>' . "\n";
$tabela .= '</tr>' . "\n";
if ($arquivos != "") {
    $tabela .= $arquivos;
    $qtdArqs = ' (' . $qtdArqs . ' arquivos disponíveis)';
} else {
    $qtdArqs = ' (nenhum arquivo disponível)';
}
$tabela .= '</table>';
print $tabela;
