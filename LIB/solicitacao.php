<?php 
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/libVisaDetail.php");
include ("../LIB/classes/classe_proc_cancel.php");
include ("../LIB/classes/classe_proc_emiscie.php");
include ("../LIB/classes/classe_proc_mteNova.php");
include ("../LIB/classes/classe_proc_prorrog.php");
include ("../LIB/classes/classe_proc_regcie.php");

$ultimaSol = $_GET['NU_SOLICITACAO'];
$idCandidato = $_GET['NU_CANDIDATO'];
?>
<table style="width:100%">
<?php 
	$obja = new proc_mte();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoProcessoMTE("V");
?>
<?php 
    $obja = new proc_regcie();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoRegistroCIE("V");
?>
<?php 
    $obja = new proc_emiscie();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoEmissaoCIE("V");
?>
<?php 
    $obja = new proc_prorrog();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoProrrogacao("V");
?>
<?php 
    $obja = new proc_cancel();
    $obja->BuscaPorCandidato($idCandidato,$ultimaSol);
    print $obja->MontaTextoCancel("V");
?>
</table>
