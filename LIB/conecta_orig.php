<?php
include("dbs.php"); 

$hostname_mundivisas = "localhost";
$database_mundivisas = "Mundivisas";
$username_mundivisas = "root";
$password_mundivisas = "MyAdmDb%";

$mundivisas = mysql_connect($hostname_mundivisas, $username_mundivisas, $password_mundivisas) or die(mysql_error());
mysql_select_db($database_mundivisas)or die("Não foi possível conectar-se com o banco de dados");

$mydominio = "http://intranet.mundivisas.com.br";
$myinstdir = "/home/www";

$mylogdir = $myinstdir."/log";

$myfinurl = $mydominio."/intranet/solicitacoes";
$mysoldir = $myinstdir."/arquivos/solicitacoes";
$mysolurl = $mydominio."/arquivos/solicitacoes";
$myrecdir = $myinstdir."/arquivos/recibos";
$myrecurl = $mydominio."/arquivos/recibos";
$myfordir = $myinstdir."/arquivos/formularios";
$myforurl = $mydominio."/arquivos/formularios";
$mydocdir = $myinstdir."/arquivos/documentos";
$mydocurl = $mydominio."/arquivos/documentos";
$myhstdir = $myinstdir."/arquivos/histpag";
$myhsturl = $mydominio."/arquivos/histpag";

$estilo = "border: 1px solid; font-family: Verdana; background-color: rgb(247,247,247); font-size:8 pt; color:#3969A5; padding-left:4px; padding-right:4px; padding-top:1px; padding-bottom:1px";

$mydir = $myinstdir."/intranet";
$myurl = $mydominio."/intranet";
$myup = "/upload";


error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 'On');
ini_set('log_errors', 'On');
ini_set('error_log', $myinstdir . '/log/error_log' . date('Ym') . '.txt');

function conectaQuery($pSql, $pContexto)
{
	if ($res = mysql_query($pSql)){
		return $res;
	} 
	else {
		throw new Exception ("\n\n\t[Contexto]" . "\n\t" . $pContexto . "\n\t[SQL]" . "\n\t" . $pSql . "\n\t[Erro]" . "\n\t" . mysql_error());
	}
}

function executeQuery($pSql, $pContexto)
{
	if (mysql_query($pSql)){
		return true;
	} 
	else {
		throw new Exception ("\n\n\t[Contexto]" . "\n\t" . $pContexto . "\n\t[SQL]" . "\n\t" . $pSql . "\n\t[Erro]" . "\n\t" . mysql_error());
	}
}

function __autoload($className){
	if(file_exists('/LIB/classes/'.$className.'.php')){
		require_once('/LIB/classes/'.$className.'.php');
	}elseif(file_exists('../LIB/classes/'.$className.'.php')){
		require_once('../LIB/classes/'.$className.'.php');
	}elseif(file_exists('../../LIB/classes/'.$className.'.php')){
		require_once('../../LIB/classes/'.$className.'.php');
	}elseif(file_exists('/LIB/'.$className.'.php')){
		require_once('/LIB/'.$className.'.php');
	}elseif(file_exists('../LIB/'.$className.'.php')){
		require_once('../LIB/'.$className.'.php');
	}elseif(file_exists('../../LIB/'.$className.'.php')){
		require_once('../../LIB/'.$className.'.php');
	}elseif(file_exists('/LIB/componentes/'.$className.'.php')){
		require_once('/LIB/componentes/'.$className.'.php');
	}elseif(file_exists('../LIB/componentes/'.$className.'.php')){
		require_once('../LIB/componentes/'.$className.'.php');
	}
}
