<?php

function pegaDadosEmpresa($idEmpresa) {
  global $NU_EMPRESA,$NO_RAZAO_SOCIAL,$CO_ATIVIDADE_ECONOMICA,$NU_CNPJ,$NO_ENDERECO_EMP,$DT_CADASTRO_BC;
  global $NO_COMPLEMENTO_EMP,$NO_BAIRRO_EMP,$NO_MUNICIPIO_EMP,$CO_UF_EMP,$NU_CEP_EMP,$NU_DDD_EMP;
  global $NU_TELEFONE_EMP,$NO_EMAIL_EMPRESA,$NO_CONTATO_PRINCIPAL,$NO_EMAIL_CONTATO_PRINCIPAL;
  global $NO_ADMINISTRADOR,$CO_CARGO_ADMIN,$TE_OBJETO_SOCIAL,$VA_CAPITAL_ATUAL,$VA_CAPITAL_INICIAL;
  global $DT_CONSTITUICAO_EMPRESA,$DT_ALTERACAO_CONTRATUAL,$NO_EMPRESA_ESTRANGEIRA,$NM_LOGO_EMPRESA;
  global $VA_INVESTIMENTO_ESTRANGEIRO,$DT_INVESTIMENTO_ESTRANGEIRO,$QT_EMPREGADOS,$QT_EMPREGADOS_ESTRANGEIROS;
  $ret = "";
  if( $idEmpresa > 0 ){
    $sql = "SELECT * from EMPRESA where NU_EMPRESA = $idEmpresa ";
    $rs = mysql_query($sql);
    if(mysql_errno()<>0) {
      $ret = "Erro: ".mysql_error()."<!-- $sql -->";
    } else {
      if($dados = mysql_fetch_array($rs)) {
        $NU_EMPRESA = $dados['NU_EMPRESA'];
        $NO_RAZAO_SOCIAL = $dados['NO_RAZAO_SOCIAL'];
        $CO_ATIVIDADE_ECONOMICA = $dados['CO_ATIVIDADE_ECONOMICA'];
        $NU_CNPJ = $dados['NU_CNPJ'];
        $NO_ENDERECO_EMP = $dados['NO_ENDERECO'];
        $NO_COMPLEMENTO_EMP = $dados['NO_COMPLEMENTO_ENDERECO'];
        $NO_BAIRRO_EMP = $dados['NO_BAIRRO'];
        $NO_MUNICIPIO_EMP = $dados['NO_MUNICIPIO'];
        $CO_UF_EMP = $dados['CO_UF'];
        $NU_CEP_EMP = $dados['NU_CEP'];
        $NU_DDD_EMP = $dados['NU_DDD'];
        $NU_TELEFONE_EMP = $dados['NU_TELEFONE'];
        $NO_EMAIL_EMPRESA = $dados['NO_EMAIL_EMPRESA'];
        $NO_CONTATO_PRINCIPAL = $dados['NO_CONTATO_PRINCIPAL'];
        $NO_EMAIL_CONTATO_PRINCIPAL = $dados['NO_EMAIL_CONTATO_PRINCIPAL'];
        $NO_ADMINISTRADOR = $dados['NO_ADMINISTRADOR'];
        $CO_CARGO_ADMIN = $dados['CO_CARGO_ADMIN'];
        $TE_OBJETO_SOCIAL = $dados['TE_OBJETO_SOCIAL'];
        $VA_CAPITAL_ATUAL = $dados['VA_CAPITAL_ATUAL'];
        $VA_CAPITAL_INICIAL = $dados['VA_CAPITAL_INICIAL'];
        $DT_CONSTITUICAO_EMPRESA = $dados['DT_CONSTITUICAO_EMPRESA'];
        $DT_ALTERACAO_CONTRATUAL = $dados['DT_ALTERACAO_CONTRATUAL'];
        $NO_EMPRESA_ESTRANGEIRA = $dados['NO_EMPRESA_ESTRANGEIRA'];
        $VA_INVESTIMENTO_ESTRANGEIRO = $dados['VA_INVESTIMENTO_ESTRANGEIRO'];
        $DT_INVESTIMENTO_ESTRANGEIRO = $dados['DT_INVESTIMENTO_ESTRANGEIRO'];
        $QT_EMPREGADOS = $dados['QT_EMPREGADOS'];
        $QT_EMPREGADOS_ESTRANGEIROS = $dados['QT_EMPREGADOS_ESTRANGEIROS'];
        $NM_LOGO_EMPRESA = $dados['NM_LOGO_EMPRESA'];
        $DT_CONSTITUICAO_EMPRESA = dataMy2BR($DT_CONSTITUICAO_EMPRESA);
        $DT_ALTERACAO_CONTRATUAL = dataMy2BR($DT_ALTERACAO_CONTRATUAL);
        $DT_INVESTIMENTO_ESTRANGEIRO = dataMy2BR($DT_INVESTIMENTO_ESTRANGEIRO);
        $DT_CADASTRO_BC = dataMy2BR($dados['DT_CADASTRO_BC']);
        if(strlen($DT_INVESTIMENTO_ESTRANGEIRO)==0) { $DT_INVESTIMENTO_ESTRANGEIRO="N�o h�"; }
        if(strlen($DT_CADASTRO_BC)==0) { $DT_CADASTRO_BC="N�o h�"; }
        if(strlen($NM_LOGO_EMPRESA)<5) { $NM_LOGO_EMPRESA="logo_vazio.jpg"; }
      } else {
         $ret = "N�o foi encontrada a empresa";
      }
    }
  }
  return $ret;
}

function pegaDadosAssociadas($idEmpresa) {
  global $totAssoc,$NU_ASSOCIADA,$NO_ASSOCIADA,$CO_NACIONALIDADE,$NO_CAPITAL_VOTANTE,$NO_CAPITAL_TOTAL,$NU_CNPJ_ASSOC,$nmNacionalAssoc;
  $ret = "";
  $sql = "select * from ASSOCIADAS where NU_EMPRESA=$idEmpresa";
  $rs1 = mysql_query($sql);
  $x = 0;
  if(mysql_errno() <> 0) {
    $ret = "Erro: ".mysql_error()."<!-- $sql -->";
  } else {
    while($dados = mysql_fetch_array($rs1)) {
      $x++;
      $NU_ASSOCIADA[$x] = $dados['NU_ASSOCIADA'];
      $NO_ASSOCIADA[$x] = $dados['NO_ASSOCIADA'];
      $nacional = $dados['CO_NACIONALIDADE'];
      $CO_NACIONALIDADE[$x] = $nacional;
      $NO_CAPITAL_VOTANTE[$x] = $dados['NO_CAPITAL_VOTANTE'];
      $NO_CAPITAL_TOTAL[$x] = $dados['NO_CAPITAL_TOTAL'];
      $NU_CNPJ_ASSOC[$x] = $dados['NU_CNPJ'];
      $nmNacionalAssoc[$x] = pegaNomeNacionalidade($nacional);
    }
  }
  $totAssoc = $x;
  return $ret;
}

function pegaDadosAdminPetrobras($idEmpresa) {
  global $totAdmin,$CO_ADMIN1,$NM_ADMIN1,$NM_CARGO1,$TP_CARGO1;
  $sql = "select * from ADMIN_PETROBRAS where CO_EMPRESA=$idEmpresa AND TP_CARGO='A'";
  $ret = "";
  $x = 0;
  $rs2 = mysql_query($sql);
  if(mysql_errno() <> 0) {
    $ret = "Erro: ".mysql_error()."<!-- $sql -->";
  } else {
    while($dados = mysql_fetch_array($rs2)) {
      $x++;
      $CO_ADMIN1[$x] = $dados['CO_ADMIN'];
      $NM_ADMIN1[$x] = $dados['NM_ADMIN'];
      $NM_CARGO1[$x] = $dados['NM_CARGO'];
      $TP_CARGO1[$x] = "Administrador";
    }
  }
  $totAdmin = $x;
  return $ret;
}

function pegaDadosDiretorPetrobras($idEmpresa) {
  global $totDiretor,$CO_ADMIN2,$NM_ADMIN2,$NM_CARGO2,$TP_CARGO2;
  $sql = "select * from ADMIN_PETROBRAS where CO_EMPRESA=$idEmpresa AND TP_CARGO='D'";
  $ret = "";
  $x = 0;
  $rs2 = mysql_query($sql);
  if(mysql_errno() <> 0) {
    $ret = "Erro: ".mysql_error()."<!-- $sql -->";
  } else {
    while($dados = mysql_fetch_array($rs2)) {
      $x++;
      $CO_ADMIN2[$x] = $dados['CO_ADMIN'];
      $NM_ADMIN2[$x] = $dados['NM_ADMIN'];
      $NM_CARGO2[$x] = $dados['NM_CARGO'];
      $TP_CARGO2[$x] = "Administrador";
    }
  }
  $totDiretor = $x;
  return $ret;
}

function pegaDadosProjetosEmb($idEmpresa,$idEmbarcacaoProjeto) {
  global $totEmbProj,$NU_EMBARCACAO_PROJETO,$NO_EMBARCACAO_PROJETO,$ID_TIPO_EMBARCACAO_PROJETO;
  global $NO_MUNICIPIO_PROJ,$CO_UF_PROJ,$DT_PRAZO_CONTRATO_PROJ,$NO_CONTATO_PROJ;
  global $NO_EMAIL_CONTATO_PROJ,$NU_TELEFONE_PROJ,$QT_TRIPULANTES_PROJ,$QT_TRIPULANTES_EST_PROJ,$nmArmador;
  global $NO_CONTRATO_PROJ,$CO_PAIS_PROJ,$NU_ARMADOR_PROJ,$DS_JUSTIFICATIVA_PROJ,$nmPaisEmbarcacao,$bandeira, $embp_fl_ativo;
  $sql = "select * from EMBARCACAO_PROJETO where coalesce(embp_fl_ativo,0) = 1 and NU_EMPRESA = $idEmpresa ";
  if($idEmbarcacaoProjeto > 0) { $sql = $sql." AND NU_EMBARCACAO_PROJETO=$idEmbarcacaoProjeto"; }
  $sql = $sql." order by NO_EMBARCACAO_PROJETO";
  $x = 0;
  $ret = "";
  $rs4 = mysql_query($sql);
  if(mysql_errno() <> 0) {
    $ret = "Erro: ".mysql_error()."<!-- $sql -->";
  } else {
    while($dados = mysql_fetch_array($rs4)) {
      $x++;
      $NU_EMBARCACAO_PROJETO[$x] = $dados['NU_EMBARCACAO_PROJETO'];
      $NO_EMBARCACAO_PROJETO[$x] = $dados['NO_EMBARCACAO_PROJETO'];
      $ID_TIPO_EMBARCACAO_PROJETO[$x] = $dados['ID_TIPO_EMBARCACAO_PROJETO'];
      $NO_MUNICIPIO_PROJ[$x] = $dados['NO_MUNICIPIO'];
      $CO_UF_PROJ[$x] = $dados['CO_UF'];
      $DT_PRAZO_CONTRATO_PROJ[$x] = dataMy2BR($dados['DT_PRAZO_CONTRATO']);
      $NO_CONTATO_PROJ[$x] = $dados['NO_CONTATO'];
      $NO_EMAIL_CONTATO_PROJ[$x] = $dados['NO_EMAIL_CONTATO'];
      $NU_TELEFONE_PROJ[$x] = $dados['NU_TELEFONE_CONTATO'];
      $QT_TRIPULANTES_PROJ[$x] = $dados['QT_TRIPULANTES'];
      $QT_TRIPULANTES_EST_PROJ[$x] = $dados['QT_TRIPULANTES_ESTRANGEIROS'];
      $NO_CONTRATO_PROJ[$x] = $dados['NO_CONTRATO'];
      $CO_PAIS_PROJ[$x] = 0+$dados['CO_PAIS'];
      $NU_ARMADOR_PROJ[$x] = $dados['NU_ARMADOR'];
      $nmArmador[$x] = pegaNomeUsuario($NU_ARMADOR_PROJ[$x]);
      $embp_fl_ativo[$x] = $dados['embp_fl_ativo'];
      $DS_JUSTIFICATIVA_PROJ[$x] = $dados['DS_JUSTIFICATIVA'];
      $nmPaisEmbarcacao[$x] = "&#160;";
      if($CO_PAIS_PROJ[$x]>0) { $nmPaisEmbarcacao[$x]=pegaNomePais($CO_PAIS_PROJ[$x]); }
      if($CO_PAIS_PROJ[$x]>0) { $bandeira[$x]=pegaNomeNacionalidade($CO_PAIS_PROJ[$x]); }
    }
  }
  $totEmbProj = $x;
  return $ret;
}

function pegaDadosProcuradores($idEmpresa,$idProcurador) {
  global $totProcu,$NU_PROCURADOR,$NO_PROCURADOR,$CO_CARGO_PROC,$NU_CPF_PROC,$NU_IDENT_PROC;
  global $NO_EMISSOR_PROC,$DT_EMISSAO_PROC,$NO_EMAIL_PROC,$CO_NACIONAL_PROC,$CO_ESTCIV_PROC;
  global $nmCargoProc,$nmNacionalProc,$nmEstCivProc;
  $ret = "";
  $sql = "select * from PROCURADOR_EMPRESA where NU_EMPRESA = $idEmpresa ";
  if($idProcurador > 0) { $sql = $sql." AND NU_PROCURADOR = $idProcurador"; }
  $x = 0;
  $rs5 = mysql_query($sql);
  if(mysql_errno() <> 0) {
    $ret = "Erro: ".mysql_error()."<!-- $sql -->";
  } else {
    while($dados = mysql_fetch_array($rs5)) {
      $x++;
      $NU_PROCURADOR[$x] = $dados['NU_PROCURADOR'];
      $NO_PROCURADOR[$x] = $dados['NO_PROCURADOR'];
      $CO_CARGO_PROC[$x] = $dados['CO_CARGO_FUNCAO'];
      $NU_CPF_PROC[$x] = $dados['NU_CPF'];
      $NU_IDENT_PROC[$x] = $dados['NU_IDENTIDADE'];
      $NO_EMISSOR_PROC[$x] = $dados['NO_EMISSOR_IDENTIDADE'];
      $DT_EMISSAO_PROC[$x] = dataMy2BR($dados['DT_EMISSAO_IDENTIDADE']);
      $NO_EMAIL_PROC[$x] = $dados['NO_EMAIL'];
      $CO_NACIONAL_PROC[$x] = $dados['CO_NACIONALIDADE'];
      $CO_ESTCIV_PROC[$x] = $dados['CO_ESTADO_CIVIL'];
      $nmCargoProc[$x] = pegaNomeFuncao($CO_CARGO_PROC[$x]);
      $nmNacionalProc[$x] = pegaNomeNacionalidade($CO_NACIONAL_PROC[$x]);
      $nmEstCivProc[$x] = pegaNomeEstadoCivil($CO_ESTCIV_PROC[$x]);
    }
  }
  $totProcu = $x;
  return $ret;
}

function pegaDadosProcuradores2($idEmpresa,$idProcurador) {
  global $totProcu2,$NU_PROCURADOR2,$NO_PROCURADOR2,$CO_CARGO_PROC2,$NU_CPF_PROC2,$NU_IDENT_PROC2;
  global $NO_EMISSOR_PROC2,$DT_EMISSAO_PROC2,$NO_EMAIL_PROC2,$CO_NACIONAL_PROC2,$CO_ESTCIV_PROC2;
  global $nmCargoProc2,$nmNacionalProc2,$nmEstCivProc2;
  $ret = "";
  $sql = "select * from PROCURADOR_EMPRESA where NU_EMPRESA = $idEmpresa ";
  if($idProcurador > 0) { $sql = $sql." AND NU_PROCURADOR = $idProcurador"; }
  $x = 0;
  $rs5 = mysql_query($sql);
  if(mysql_errno() <> 0) {
    $ret = "Erro: ".mysql_error()."<!-- $sql -->";
  } else {
    while($dados = mysql_fetch_array($rs5)) {
      $x++;
      $NU_PROCURADOR2[$x] = $dados['NU_PROCURADOR'];
      $NO_PROCURADOR2[$x] = $dados['NO_PROCURADOR'];
      $CO_CARGO_PROC2[$x] = $dados['CO_CARGO_FUNCAO'];
      $NU_CPF_PROC2[$x] = $dados['NU_CPF'];
      $NU_IDENT_PROC2[$x] = $dados['NU_IDENTIDADE'];
      $NO_EMISSOR_PROC2[$x] = $dados['NO_EMISSOR_IDENTIDADE'];
      $DT_EMISSAO_PROC2[$x] = dataMy2BR($dados['DT_EMISSAO_IDENTIDADE']);
      $NO_EMAIL_PROC2[$x] = $dados['NO_EMAIL'];
      $CO_NACIONAL_PROC2[$x] = $dados['CO_NACIONALIDADE'];
      $CO_ESTCIV_PROC2[$x] = $dados['CO_ESTADO_CIVIL'];
      $nmCargoProc2[$x] = pegaNomeFuncao($CO_CARGO_PROC2[$x]);
      $nmNacionalProc2[$x] = pegaNomeNacionalidade($CO_NACIONAL_PROC2[$x]);
      $nmEstCivProc2[$x] = pegaNomeEstadoCivil($CO_ESTCIV_PROC2[$x]);
    }
  }
  $totProcu2 = $x;
  return $ret;
}
?>
