<?php
/**
 * Servicos pacote
 * @author Leonardo
 */
class cPACOTE extends cSERVICO{

	public function sql_Liste()
	{
		$sql = " select s.NU_SERVICO";
		$sql.= "      , s.CO_SERVICO";
		$sql.= "      , s.NO_SERVICO";
		$sql.= "      , s.NO_SERVICO_RESUMIDO";
		$sql.= "      , s.no_servico_em_ingles";
		$sql.= "      , s.no_servico_resumido_em_ingles";
		$sql.= "      , s.ID_TIPO_ACOMPANHAMENTO";
		$sql.= "      , s.CO_TIPO_AUTORIZACAO";
		$sql.= "      , s.CO_CLASSIFICACAO_VISTO";
		$sql.= "      , s.NU_TIPO_SERVICO";
		$sql.= "      , TS.NO_TIPO_SERVICO";
		$sql.= "      , TS.CO_TIPO_SERVICO";
		$sql.= "      , TAU.NO_TIPO_AUTORIZACAO";
		$sql.= "      , CV.NO_CLASSIFICACAO_VISTO";
		$sql.= "      , TAC.NO_TIPO_ACOMPANHAMENTO";
		$sql.= "      , CONCAT(CO_TIPO_SERVICO, '.', right(concat('00', CO_SERVICO), 2)) CO_SERVICO_TIPO_SERVICO";
		$sql.= "      , serv_fl_candidato_obrigatorio";
		$sql.= "      , serv_fl_form_tvs";
		$sql.= "      , serv_fl_ativo";
		$sql.= "      , serv_fl_envio_bsb";
		$sql.= "      , serv_fl_revisao_analistas";
		$sql.= "      , serv_tx_codigo_sg";
		$sql.= "      , serv_fl_timesheet";
		$sql.= "      , serv_fl_despesas_adicionais";
		$sql.= "      , serv_fl_pacote";
		$sql.= "      , coalesce(serv_fl_ativo, 0) serv_fl_ativo";
		$sql.= "      , (select count(*) from solicita_visto where NU_SERVICO = s.NU_SERVICO) qtd_os";
		$sql.= "   from SERVICO s";
		$sql.=" left join TIPO_SERVICO TS on TS.NU_TIPO_SERVICO = s.NU_TIPO_SERVICO";
		$sql.=" left join CLASSIFICACAO_VISTO CV on CV.CO_CLASSIFICACAO_VISTO = s.CO_CLASSIFICACAO_VISTO";
		$sql.=" left join TIPO_ACOMPANHAMENTO TAC on TAC.ID_TIPO_ACOMPANHAMENTO = s.ID_TIPO_ACOMPANHAMENTO";
		$sql.=" left join TIPO_AUTORIZACAO TAU on TAU.CO_TIPO_AUTORIZACAO = s.CO_TIPO_AUTORIZACAO";
		$sql.=" where 1=1 and serv_fl_pacote = 1";
		return $sql;
	}

	public function Incluir()
	{
		$this->mserv_fl_pacote = 1;
		parent::Incluir();
	}
	public function Exclua(){
		$sql = "SELECT ifnull(count(*),0) from solicita_visto where NU_SERVICO = ".$this->mNU_SERVICO;
		if($res = cAMBIENTE::$db_pdo->query($sql)){
			$rs = $res->fetch(PDO::FETCH_BOTH);
			if ($rs[0] > 0){
				throw new cERRO_CONSISTENCIA("Não é possível excluir o pacote pois existem OS cadastradas para ele.");
			}
		}
		$sql = "delete from preco where NU_SERVICO = ".$this->mNU_SERVICO;
		cAMBIENTE::$db_pdo->exec($sql);
		$sql = "delete from SERVICO where NU_SERVICO = ".$this->mNU_SERVICO;
		cAMBIENTE::$db_pdo->exec($sql);
	}

}
