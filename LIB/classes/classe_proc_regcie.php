<?php

class proc_regcie {

  public $codigo = "";
  public $cd_candidato = "";
  public $cd_solicitacao = "";
  public $nu_protocolo = "";
  public $dt_requerimento = "";
  public $dt_validade = "";
  public $dt_prazo_estada = "";
  public $observacao = "";
  public $dt_cad = "";
  public $dt_ult = "";
  public $myerr = "";

  public function proc_regcie() {  }

  public function BuscaPorCodigo($codigo) {
    $extra = "WHERE codigo=$codigo";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function BuscaPorCandidato($candidato,$solicitacao) {
    $this->cd_candidato = $candidato;
    $this->cd_solicitacao = $solicitacao;
    $extra = "WHERE cd_candidato=$candidato AND cd_solicitacao=$solicitacao";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function ExisteCandidato($candidato,$solicitacao) {
    $extra = "WHERE cd_candidato=$candidato AND cd_solicitacao=$solicitacao";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if(mysql_num_rows($rs)>0) {
        return true;
      } else {
        return false;
      }
    }
  }

  public function BuscaPorProcesso($processo) {
    $extra = "WHERE nu_protocolo=$processo";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function ListaProcessos($extra) {
    $this->myerr = "";
    $sql = "SELECT * FROM processo_regcie $extra";
    $rs = mysql_query($sql);
    if(mysql_errno() != 0) {
      $this->myerr = mysql_error();
    }
    return $rs;
  }

  function InsereProcesso() {
    if($this->Verifica() == true) {
      $cd_cand = $this->cd_candidato;
      $cd_sol = $this->cd_solicitacao;
      $dt_requerimento = $this->dt_requerimento;
      $dt_validade = $this->dt_validade;
      $dt_prazo_estada = $this->dt_prazo_estada;
      $sql1 = "INSERT INTO processo_regcie (cd_candidato,cd_solicitacao,nu_protocolo,dt_requerimento,";
      $sql2 = "dt_validade,dt_prazo_estada,observacao,dt_cad) VALUES ";
      $sql = sprintf($sql1.$sql2."($cd_cand,$cd_sol,'%s',$dt_requerimento,$dt_validade,$dt_prazo_estada,'%s',now())",
              mysql_real_escape_string($this->nu_protocolo),
              mysql_real_escape_string($this->observacao)
             );
      mysql_query($sql);
      if(mysql_errno() != 0) {
        $erro = mysql_error();
        $this->myerr = "Ocorreu um erro ao inserir registro CIE.<!-- $erro - sql=$sql -->";
        gravaLogErro($sql,$erro,"CADASTRAR","REGISTRO_CIE");
      } else {
        gravaLog($sql,"","CADASTRAR","REGISTRO_CIE");
      }
    }
  }

  function AlteraProcesso() {
    if($this->Verifica() == true) {
      $cd_cand = $this->cd_candidato;
      $cd_sol = $this->cd_solicitacao;
      $dt_requerimento = $this->dt_requerimento;
      $dt_validade = $this->dt_validade;
      $dt_prazo_estada = $this->dt_prazo_estada;
      $sql1 = "UPDATE processo_regcie SET nu_protocolo='%s',dt_requerimento=$dt_requerimento,dt_validade=$dt_validade,";
      $sql2 = "dt_prazo_estada=$dt_prazo_estada,observacao='%s' WHERE cd_candidato=$cd_cand AND cd_solicitacao=$cd_sol ";
      $sql = sprintf($sql1.$sql2,
              mysql_real_escape_string($this->nu_protocolo),
              mysql_real_escape_string($this->observacao)
             );
      mysql_query($sql);
      if(mysql_errno() != 0) {
        $erro = mysql_error();
        $this->myerr = "Ocorreu um erro ao alterar registro CIE.<!-- $erro - sql=$sql -->";
        gravaLogErro($sql,$erro,"ALTERAR","REGISTRO_CIE");
      } else {
        gravaLog($sql,"","ALTERAR","REGISTRO_CIE");
      }
    }
  }

  function AlteraVisaDetail() {
    $cd_cand = $this->cd_candidato;
    $cd_sol = $this->cd_solicitacao;
    $dt_requerimento = $this->dt_requerimento;
    $dt_validade = $this->dt_validade;
    $dt_prazo_estada = $this->dt_prazo_estada;
    $sql1 = "UPDATE AUTORIZACAO_CANDIDATO SET NU_PROTOCOLO_CIE='%s' , DT_PROTOCOLO_CIE=$dt_requerimento , DT_VALIDADE_PROTOCOLO_CIE=$dt_validade , ";
    $sql2 = "DT_PRAZO_ESTADA=$dt_prazo_estada where NU_CANDIDATO=$cd_cand and NU_SOLICITACAO=$cd_sol ";
    $sql = sprintf($sql1.$sql2,mysql_real_escape_string($this->nu_protocolo));
    mysql_query($sql);
    if(mysql_errno() != 0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao alterar registro CIE.<!-- $erro - sql=$sql -->";
      gravaLogErro($sql,$erro,"ALTERAR","REGISTRO_CIE_VISA");
    } else {
      gravaLog($sql,"","ALTERAR","REGISTRO_CIE_VISA");
    }
  }

  function RemoveProcesso($codigo,$cand,$sol) {
    $sql = "DELETE FROM processo_regcie WHERE ";
    if(strlen($codigo)>0) {  $sql = "codigo=$codigo";  } else
    if( (strlen($cand)>0) && (strlen($sol)>0) ) {  $sql = "cd_candidato=$cand AND cd_solicitacao=$sol";  }
    mysql_query($sql);
    if(mysql_errno() != 0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao remover registro CIE.<!-- $erro - sql=$sql -->";
      gravaLogErro($sql,$erro,"REMOVER","REGISTRO_CIE");
    } else {
      gravaLog($sql,"","REMOVER","REGISTRO_CIE");
    }
  }

  function Verifica() {
    $ret = true;
    $erro = "";
    if(strlen($this->cd_candidato) == 0) {
      $ret = false;
      $erro = $erro."Nao foi identificado o candidato. ";
    }
    if(strlen($this->cd_solicitacao) == 0) {
      $ret = false;
      $erro = $erro."Nao foi identificada a solicitacao. ";
    }
    if(strlen($this->dt_requerimento)>0) { $this->dt_requerimento = "'".dataBR2My($this->dt_requerimento)."'"; } else { $this->dt_requerimento="NULL"; }
    if(strlen($this->dt_validade)>0) { $this->dt_validade = "'".dataBR2My($this->dt_validade)."'"; } else { $this->dt_validade="NULL"; }
    if(strlen($this->dt_prazo_estada)>0) { $this->dt_prazo_estada = "'".dataBR2My($this->dt_prazo_estada)."'"; } else { $this->dt_prazo_estada="NULL"; }
    if($this->observacao=="NULL") { $this->observacao=""; }
    $this->myerr = $erro;
    return $ret;
  }

  private function LeituraRW($rw) {
    $this->codigo = $rw['codigo'];
    $this->cd_candidato = $rw['cd_candidato'];
    $this->cd_solicitacao = $rw['cd_solicitacao'];
    $this->nu_protocolo = $rw['nu_protocolo'];
    $this->dt_requerimento = dataMy2BR($rw['dt_requerimento']);
    $this->dt_validade = dataMy2BR($rw['dt_validade']);
    $this->dt_prazo_estada = dataMy2BR($rw['dt_prazo_estada']);
    $this->observacao = $rw['observacao'];
    if($this->observacao=="NULL") { $this->observacao=""; }
    $this->dt_cad = $rw['dt_cad'];
    $this->dt_ult = $rw['dt_ult'];
  }

  private function LimpaObjeto() {
    $this->codigo = "";
    $this->cd_candidato = "";
    $this->cd_solicitacao = "";
    $this->nu_protocolo = "";
    $this->dt_requerimento = "";
    $this->dt_validade = "";
    $this->dt_prazo_estada = "";
    $this->observacao = "";
    $this->dt_cad = "";
    $this->dt_ult = "";
    $this->myerr = "";
  }

  function insereEvento($idEmpresa,$tipo) {
    $informou = "";
    $cand = $this->cd_candidato;
    $sol = $this->cd_solicitacao;
    $processo = str_replace("'","",$this->nu_protocolo);
    $dt_req = dataMy2BR(str_replace("'","",$this->dt_requerimento));
    $dt_val = dataMy2BR(str_replace("'","",$this->dt_validade));
    $prazo = dataMy2BR(str_replace("'","",$this->dt_prazo_estada));
    if ( (strlen($processo)>0) && ($processo != "NULL") ) {
       $informou = $informou." processo ($processo) / ";
    } else {
       $informou = $informou." processo (NULL) / ";
    }
    $informou = $informou." data de requerimento ($dt_req) / ";
    $informou = $informou." data de validade ($dt_val) / ";
    $informou = $informou." prazo de estada ($prazo) ";
    $extra="no Registro CIE. <br>Informou: $informou .";
    $texto = "$tipo do cadastro do candidato $extra";
    insereEvento($idEmpresa,$cand,$sol,$texto);
  }

  public function MontaTextoRegistroCIE($acao) {
    $ret = "";
    $ret = $ret.montaCampos("cd_candidato",$this->cd_candidato,$this->cd_candidato,"H",$acao,"")."\n";
    $ret = $ret.montaCampos("cd_solicitacao",$this->cd_solicitacao,$this->cd_solicitacao,"H",$acao,"")."\n";
    $ret = $ret."<tr height=20><td class=textoazul bgcolor=#DADADA colspan=5>Registro CIE</td></tr>\n";
    $ret = $ret."<tr height=20><td><b>N&uacute;mero do Protocolo CIE:</td>\n";
    $ret = $ret."<td>".montaCampos("NU_PROTOCOLO_CIE",$this->nu_protocolo,$this->nu_protocolo,"T",$acao,"maxlength=20 size=30")."</td>\n";
    $ret = $ret."<td>&#160;</td><td><b>Data de Requerimento:</td>\n";
    $ret = $ret."<td>".montaCampos("DT_PROTOCOLO_CIE",$this->dt_requerimento,$this->dt_requerimento,"D",$acao,"")."</td>\n";
    $ret = $ret."</tr>\n";
    $ret = $ret."<tr height=20>\n<td><b>Validade do Protocolo CIE:</td>\n";
    $ret = $ret."<td>".montaCampos("DT_VALIDADE_PROTOCOLO_CIE",$this->dt_validade,$this->dt_validade,"T",$acao,"maxlength=20 size=15")."</td>\n";
    $ret = $ret."<td>&#160;</td>\n<td><b>Prazo Estada Atual:</td>\n";
    $ret = $ret."<td>".montaCampos("DT_PRAZO_ESTADA",$this->dt_prazo_estada,$this->dt_prazo_estada,"T",$acao,"maxlength=20 size=15")."</td>\n";
    $ret = $ret."</tr>\n";
    $ret = $ret."<tr height=20><td><b>Observa&ccedil;&atilde;o:</td>\n";
    $ret = $ret."<td colspan=4>".montaCampos("OBS_MTE",$this->observacao,$this->observacao,"T",$acao,"maxlength=250 size=30")."</td>\n";
    $ret = $ret."</tr>\n";
    return $ret;
  }

  public function LeituraRequest($tipo) {
    $this->cd_candidato = $tipo['cd_candidato'];
    $this->cd_solicitacao = $tipo['cd_solicitacao'];
    $this->nu_protocolo = $tipo['NU_PROTOCOLO_CIE'];
    $this->dt_requerimento = $tipo['DT_PROTOCOLO_CIE'];
    $this->dt_validade = $tipo['DT_VALIDADE_PROTOCOLO_CIE'];
    $this->dt_prazo_estada = $tipo['DT_PRAZO_ESTADA'];
    $this->observacao = $tipo['OBS_MTE'];
    if($this->observacao=="NULL") { $this->observacao=""; }
  }

}

?>

