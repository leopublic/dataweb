<?php

/**
 * Estrangeiro registrado no sistema
 * @author Leonardo Medeiros
 */
class cCANDIDATO_BASE extends cMODELO
{

    public $mNU_CANDIDATO;
    public $mNO_PRIMEIRO_NOME;
    public $mNO_NOME_MEIO;
    public $mNO_ULTIMO_NOME;
    public $mNOME_COMPLETO;
    public $mNO_EMAIL_CANDIDATO;
    public $mNO_ENDERECO_RESIDENCIA;
    public $mNO_CIDADE_RESIDENCIA;
    public $mCO_PAIS_RESIDENCIA;
    public $mNU_TELEFONE_CANDIDATO;
    public $mNU_TELEFONE_CANDIDATO_CEL;
    public $mNO_EMPRESA_ESTRANGEIRA;
    public $mNO_ENDERECO_EMPRESA;
    public $mNO_CIDADE_EMPRESA;
    public $mCO_PAIS_EMPRESA;
    public $mNU_TELEFONE_EMPRESA;
    public $mNO_PAI;
    public $mNO_MAE;
    public $mCO_NACIONALIDADE;
    public $mCO_NIVEL_ESCOLARIDADE;
    public $mCO_ESTADO_CIVIL;
    public $mCO_SEXO;
    public $mDT_NASCIMENTO;
    public $mNO_LOCAL_NASCIMENTO;
    public $mNU_PASSAPORTE;
    public $mDT_EMISSAO_PASSAPORTE;
    public $mDT_VALIDADE_PASSAPORTE;
    public $mCO_PAIS_EMISSOR_PASSAPORTE;
    public $mNU_RNE;
    public $mCO_PROFISSAO_CANDIDATO;
    public $mTE_TRABALHO_ANTERIOR_BRASIL;
    public $mNU_CPF;
    public $mDT_CADASTRAMENTO;
    public $mCO_USU_CADASTAMENTO;
    public $mDT_ULT_ALTERACAO;
    public $mCO_USU_ULT_ALTERACAO;
    public $mNO_ENDERECO_ESTRANGEIRO;
    public $mNO_CIDADE_ESTRANGEIRO;
    public $mCO_PAIS_ESTRANGEIRO;
    public $mNU_TELEFONE_ESTRANGEIRO;
    public $mBO_CADASTRO_MINIMO_OK;
    public $mNU_EMPRESA;
    public $mNU_EMBARCACAO_PROJETO;
    public $mNU_CTPS;
    public $mNU_CNH;
    public $mDT_EXPIRACAO_CNH;
    public $mDT_EXPIRACAO_CTPS;
    public $mCO_FUNCAO;
    public $mTE_DESCRICAO_ATIVIDADES;
    public $mVA_REMUNERACAO_MENSAL;
    public $mVA_REMUNERACAO_MENSAL_BRASIL;
    public $mNO_SEAMAN;
    public $mDT_EMISSAO_SEAMAN;
    public $mDT_VALIDADE_SEAMAN;
    public $mCO_PAIS_SEAMAN;
    public $mloca_id;
    public $mco_pais_nacionalidade_pai;
    public $mco_pais_nacionalidade_mae;
    public $mcand_tx_residencia_estado;
    public $mid_status_edicao;
    public $mCO_REPARTICAO_CONSULAR;
    public $mFL_ATUALIZACAO_HABILITADA;
    public $mFL_HABILITAR_CV;
    public $memail_pessoal;
    public $mNO_SENHA;
    public $mdt_envio_edicao;
    public $mdt_liberacao_edicao;

    public $mempresa;
    protected $musuario_cadastro;


    const mEscopo = 'cCANDIDATO';
    const atuBLOQUEADA = "0";
    const atuEM_ATUALIZACAO = "1";
    const atuCONCLUIDA = "2";

    public function getid() {
        return $this->mNU_CANDIDATO;
    }

    public function setId($pValor) {
        $this->mNU_CANDIDATO = $pValor;
    }

    public function QuebreNomeCompleto($pNOME_COMPLETO) {
        while (strpos($pNOME_COMPLETO, "  ") != 0) {
            $pNOME_COMPLETO = str_replace("  ", " ", $pNOME_COMPLETO);
        }

        //$arr = preg_split("[ ]",$pNO_NOME_COMPLETO);
        $arr = split(" ", trim($pNOME_COMPLETO));
        if (count($arr) == 1) {
            $this->mNO_PRIMEIRO_NOME = $arr[0];
            $this->mNO_NOME_MEIO = '';
            $this->mNO_ULTIMO_NOME = '';
        }
        if (count($arr) == 2) {
            $this->mNO_PRIMEIRO_NOME = $arr[0];
            $this->mNO_NOME_MEIO = '';
            $this->mNO_ULTIMO_NOME = $arr[1];
        }
        if (count($arr) == 3) {
            $this->mNO_PRIMEIRO_NOME = $arr[0];
            $this->mNO_NOME_MEIO = $arr[1];
            $this->mNO_ULTIMO_NOME = $arr[2];
        }
        if (count($arr) > 3) {
            $this->mNO_PRIMEIRO_NOME = $arr[0];
            $i = 1;
            $espaco = '';
            while ($i < (count($arr) - 1)) {
                $this->mNO_NOME_MEIO.= $espaco . $arr[$i];
                $espaco = ' ';
                $i++;
            }
            $this->mNO_ULTIMO_NOME = $arr[count($arr) - 1];
        }
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);
    }

    public static function CalculaNomeCompleto($pPrimeiro, $pMeio = '', $pUltimo = '') {
        $completo = '';
        $espaco = '';
        if (trim($pPrimeiro) != '') {
            $completo.= $espaco . trim($pPrimeiro);
            $espaco = ' ';
        }
        if (trim($pMeio) != '') {
            $completo.= $espaco . trim($pMeio);
            $espaco = ' ';
        }
        if (trim($pUltimo) != '') {
            $completo.= $espaco . trim($pUltimo);
            $espaco = ' ';
        }
        return $completo;
    }

    public function montaNomeCompleto() {
        $this->mNOME_COMPLETO = self::CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);
    }

    /**
     * Recupera o candidato pelo e-mail pessoal
     * @param type $email_pessoal
     * @return boolean
     */
    public function recuperesePeloEmail($email_pessoal) {
        $email_pessoal = trim($email_pessoal);
        $sql = "select * from " . $this->get_nomeTabela();
        $sql.= " where email_pessoal =" . cBANCO::StringOk($email_pessoal);
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->CarreguePropriedadesPeloRecordset($rs);
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    /**
     * Indica que o DR foi preenchido.
     * @return [type] [description]
     */
    public function colocaEdicaoPendente() {
        $chave = $this->get_nomeCampoChave();
        $sql = "update " . $this->get_nomeTabela() . " set id_status_edicao = 3" . " , dt_envio_edicao = now()" . " where " . $chave . " = " . $this->$chave;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function RecuperesePeloNome($pNomeCompleto, $pNomeMae, $pDataNasc) {
        $pNomeCompleto = trim(str_replace(' ', '%', $pNomeCompleto));
        $pNomeMae = trim(str_replace(' ', '%', $pNomeMae));
        $sql = "select * from " . $this->get_nomeTabela();
        $sql.= " where NOME_COMPLETO like " . cBANCO::StringOk($pNomeCompleto);
        $sql.= "   and DT_NASCIMENTO    = " . cBANCO::DataOk($pDataNasc);
        $sql.= "   and NO_MAE        like " . cBANCO::StringOk($pNomeMae);
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->CarreguePropriedadesPeloRecordset($rs);
            }
        }
    }

    public function RecuperesePelosNomes($pno_primeiro_nome, $pno_nome_meio, $pno_ultimo_nome, $pno_mae, $pdt_nascimento) {
        $sql = "select * from " . $this->get_nomeTabela();
        $sql.= " where no_primeiro_nome like " . cBANCO::StringOk('%' . $pno_primeiro_nome);
        $sql.= "   and no_nome_meio like " . cBANCO::StringOk('%' . $pno_nome_meio);
        $sql.= "   and no_ultimo_nome like " . cBANCO::StringOk('%' . $pno_ultimo_nome . '%');
        $sql.= "   and DT_NASCIMENTO    = " . cBANCO::DataOk($pdt_nascimento);
        $sql.= "   and NO_MAE        like " . cBANCO::StringOk('%' . $pno_mae . '%');
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->CarreguePropriedadesPeloRecordset($rs);
            }
        }
    }

    public function gerarNovaSenha() {
        $this->mNO_SENHA = $this->GeraSenhaAleatoria();
        $campo_chave = $this->get_nomeCampoChave();
        $sql = "update " . $this->get_nomeTabela() . " set no_senha =" . cBANCO::StringOk($this->mNO_SENHA) . " where " . $campo_chave . " = " . $this->$campo_chave;
        print $sql;
        cAMBIENTE::$db_pdo->exec($sql);
        return $this->mNO_SENHA;
    }

    public function GeraSenhaAleatoria() {
        $senha = '';
        for ($i = 0; $i < 10; $i++) {
            $senha.= chr(rand(97, 122));
        }
        return $senha;
    }

    public function colocaEdicaoIniciada() {
        $campo_chave = $this->get_nomeCampoChave();
        $sql = "update " . $this->get_nomeTabela() . " set id_status_edicao = 2 where " . $campo_chave . " = " . $this->$campo_chave;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function colocaEdicaoDisponivel() {
        $campo_chave = $this->get_nomeCampoChave();
        $sql = "update " . $this->get_nomeTabela() . " set id_status_edicao = 1" . " , dt_liberacao_edicao = now()" . " where " . $campo_chave . " = " . $this->$campo_chave;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function getEmpresa() {
        if (!is_object($this->mempresa)) {
            if ($this->mNU_EMPRESA > 0) {
                $empresa = new cEMPRESA;
                $empresa->RecuperePeloId($this->mNU_EMPRESA);
                $this->mempresa = $empresa;
            }
            else {
                $this->mempresa = null;
            }
        }
        return $this->mempresa;
    }

    public function getUsuarioCadastro() {
        return $this->getRelacionamento('cusuarios', 'mCO_USU_CADASTAMENTO', 'musuario_cadastro');
    }

}
