<?php

class cEMBARCACAO_PROJETO extends cMODELO {

    public $mNU_EMBARCACAO_PROJETO;
    public $mNU_EMPRESA;
    public $mNO_EMBARCACAO_PROJETO;
    public $mNO_MUNICIPIO;
    public $mDT_PRAZO_CONTRATO;
    public $mNO_CONTATO;
    public $mNO_EMAIL_CONTATO;
    public $mNU_TELEFONE_CONTATO;
    public $mQT_TRIPULANTES;
    public $mQT_TRIPULANTES_ESTRANGEIROS;
    public $mDT_CADASTRAMENTO;
    public $mCO_USUARIO_CADASTRAMENTO;
    public $mNO_CONTRATO;
    public $mCO_PAIS;
    public $mDS_JUSTIFICATIVA;
    public $mID_TIPO_EMBARCACAO_PROJETO;
    public $mcd_usuario;
    public $mTX_OBSERVACAO;
    public $mnu_estado;
    public $mNO_PREFIXO;
    public $membp_fl_ativo;
    public $embp_dt_ult_revisao;
    public $cd_usuario_ult_revisao;
    public $membp_tx_local_de_trabalho;

    public function getid() {
        return $this->mNU_EMBARCACAO_PROJETO;
    }

    public function setid($pvalor) {
        $this->mNU_EMBARCACAO_PROJETO = $pvalor;
    }

    public function sql_Liste() {
        return "select * from vEMBARCACAO_PROJETO where 1=1";
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from vEMBARCACAO_PROJETO where NU_EMPRESA = " . $this->mNU_EMPRESA . " and NU_EMBARCACAO_PROJETO =" . $this->mNU_EMBARCACAO_PROJETO;
        return $sql;
    }

    public function get_embp_tx_local_de_trabalho() {
        if (trim($this->membp_tx_local_de_trabalho) != '') {
            return $this->membp_tx_local_de_trabalho;
        } else {
            if ($this->mID_TIPO_EMBARCACAO_PROJETO == 2 || $this->mID_TIPO_EMBARCACAO_PROJETO == 3) {
                return $this->mNO_EMBARCACAO_PROJETO . " (" . $this->mNO_MUNICIPIO . ")";
            } else {
                return "A bordo da Embarcação " . $this->mNO_EMBARCACAO_PROJETO . ", por toda costa brasileira";
            }
        }
    }

    public function RecupereSe() {
        $sql = "select * from vEMBARCACAO_PROJETO where NU_EMPRESA = " . $this->mNU_EMPRESA . " and NU_EMBARCACAO_PROJETO =" . $this->mNU_EMBARCACAO_PROJETO;
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        if ($rs = mysql_fetch_array($res)) {
            $this->mNU_EMPRESA = $rs['NU_EMPRESA'];
            $this->mNU_EMBARCACAO_PROJETO = $rs['NU_EMBARCACAO_PROJETO'];
            $this->mNO_EMBARCACAO_PROJETO = $rs['NO_EMBARCACAO_PROJETO'];
            $this->mNO_MUNICIPIO = $rs['NO_MUNICIPIO'];
            $this->mCO_PAIS = $rs['CO_PAIS'];
            $this->mDT_PRAZO_CONTRATO = $rs['DT_PRAZO_CONTRATO_FMT'];
            $this->mNO_CONTATO = $rs['NO_CONTATO'];
            $this->mNO_EMAIL_CONTATO = $rs['NO_EMAIL_CONTATO'];
            $this->mNU_TELEFONE_CONTATO = $rs['NU_TELEFONE_CONTATO'];
            $this->mQT_TRIPULANTES = $rs['QT_TRIPULANTES'];
            $this->mQT_TRIPULANTES_ESTRANGEIROS = $rs['QT_TRIPULANTES_ESTRANGEIROS'];
            $this->mNO_CONTRATO = $rs['NO_CONTRATO'];
            $this->mNO_PREFIXO = $rs['NO_PREFIXO'];
            $this->mDS_JUSTIFICATIVA = str_replace('--', "-", $rs['DS_JUSTIFICATIVA']);
            ;
            $this->mTX_OBSERVACAO = str_replace('--', "-", $rs['TX_OBSERVACAO']);
            $this->mcd_usuario = $rs['cd_usuario'];
            $this->mnu_estado = $rs['nu_estado'];
            $this->mID_TIPO_EMBARCACAO_PROJETO = $rs['ID_TIPO_EMBARCACAO_PROJETO'];
            $this->membp_fl_ativo = $rs['embp_fl_ativo'];
            $this->membp_tx_local_de_trabalho = $rs['embp_tx_local_de_trabalho'];
        }
    }

    public function Salve() {
        if ($this->mNU_EMBARCACAO_PROJETO == 0) {
            $this->IncluirNovo();
        } else {
            $this->Atualize();
        }

    }

    public function Salvar(){
        parent::Salvar();
        // Atualiza SANKHYA
    }

    public function ConsistirSempre() {
        $msg = '';
        if (intval($this->mNU_EMPRESA) == 0) {
            $msg .= '- A empresa é obrigatória';
        }
        if (trim($this->mNO_EMBARCACAO_PROJETO) == '') {
            $msg .= '- O nome do projeto é obrigatório';
        }
        if ($msg != '') {
            $msg = 'Não foi possível adicionar a embarcação/projeto pois:<br/>' . $msg;
            throw new cERRO_CONSISTENCIA($msg);
        }
    }

    public function ConsistirInclusao() {
        $this->ConsistirSempre();
        $msg = '';
        if (intval($this->mNU_EMPRESA) == 0) {
            $msg .= '- A empresa é obrigatória';
        }
        if (trim($this->mNO_EMBARCACAO_PROJETO) == '') {
            $msg .= '- O nome do projeto é obrigatório';
        }

        $sql = "select NU_EMBARCACAO_PROJETO FROM embarcacao_projeto where nu_empresa = " . $this->mNU_EMPRESA . " and no_embarcacao_projeto = " . cBANCO::StringOk(trim($this->mNO_EMBARCACAO_PROJETO));
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if (intval($rs['NU_EMBARCACAO_PROJETO']) > 0) {
            $msg = "Já existe uma embarcação/projeto com esse nome nessa empresa. Por favor verifique.";
        }
        if ($msg != '') {
            $msg = 'Não foi possível adicionar a embarcação/projeto pois:<br/>' . $msg;
            throw new cERRO_CONSISTENCIA($msg);
        }
    }

    public function Incluir() {
        $this->IncluirNovo();
    }

    public function IncluirNovo() {
        $this->ConsistirInclusao();
        $sql = "select max(NU_EMBARCACAO_PROJETO) from EMBARCACAO_PROJETO where NU_EMPRESA = " . $this->mNU_EMPRESA;
        $res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        if ($rs = mysql_fetch_array($res)) {
            $this->mNU_EMBARCACAO_PROJETO = intval($rs[0]) + 1;
        }
        if ($this->membp_fl_ativo == '') {
            $this->membp_fl_ativo = '1';
        }
        $sql = "INSERT INTO EMBARCACAO_PROJETO (";
        $sql .= "  NU_EMPRESA";
        $sql .= " ,NU_EMBARCACAO_PROJETO";
        $sql .= " ,NO_EMBARCACAO_PROJETO";
        $sql .= " ,NO_MUNICIPIO";
        $sql .= " ,DT_PRAZO_CONTRATO";
        $sql .= " ,NO_CONTATO";
        $sql .= " ,NO_EMAIL_CONTATO";
        $sql .= " ,NU_TELEFONE_CONTATO";
        $sql .= " ,QT_TRIPULANTES";
        $sql .= " ,QT_TRIPULANTES_ESTRANGEIROS";
        $sql .= " ,DT_CADASTRAMENTO";
        $sql .= " ,CO_USUARIO_CADASTRAMENTO";
        $sql .= " ,NO_CONTRATO";
        $sql .= " ,CO_PAIS";
        $sql .= " ,DS_JUSTIFICATIVA";
        $sql .= " ,NO_PREFIXO";
        $sql .= " ,cd_usuario";
        $sql .= " ,TX_OBSERVACAO";
        $sql .= " ,ID_TIPO_EMBARCACAO_PROJETO";
        $sql .= " ,nu_estado";
        $sql .= " ,embp_fl_ativo";
        $sql .= ") values (";
        $sql .= "  " . cBANCO::InteiroOk($this->mNU_EMPRESA);
        $sql .= " ," . cBANCO::InteiroOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= " ," . cBANCO::StringOk($this->mNO_EMBARCACAO_PROJETO);
        $sql .= " ," . cBANCO::StringOk($this->mNO_MUNICIPIO);
        $sql .= " ," . cBANCO::DataOk($this->mDT_PRAZO_CONTRATO);
        $sql .= " ," . cBANCO::StringOk($this->mNO_CONTATO);
        $sql .= " ," . cBANCO::StringOk($this->mNO_EMAIL_CONTATO);
        $sql .= " ," . cBANCO::StringOk($this->mNU_TELEFONE_CONTATO);
        $sql .= " ," . cBANCO::InteiroOk($this->mQT_TRIPULANTES);
        $sql .= " ," . cBANCO::InteiroOk($this->mQT_TRIPULANTES_ESTRANGEIROS);
        $sql .= " ,now()";
        $sql .= " ," . cSESSAO::$mcd_usuario;
        $sql .= " ," . cBANCO::StringOk($this->mNO_CONTRATO);
        $sql .= " ," . cBANCO::ChaveOk($this->mCO_PAIS);
        $sql .= " ," . cBANCO::StringOk($this->mDS_JUSTIFICATIVA);
        $sql .= " ," . cBANCO::StringOk($this->mNO_PREFIXO);
        $sql .= " ," . cBANCO::ChaveOk($this->mcd_usuario);
        $sql .= " ," . cBANCO::StringOk($this->mTX_OBSERVACAO);
        $sql .= " ," . cBANCO::ChaveOk($this->mID_TIPO_EMBARCACAO_PROJETO);
        $sql .= " ," . cBANCO::ChaveOk($this->mnu_estado);
        $sql .= " ," . cBANCO::SimNaoOk($this->membp_fl_ativo);
        $sql .= ")";
        print $sql;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mNU_EMBARCACAO_PROJETO = mysql_insert_id();
    }

    public function Atualizar() {
        $this->Atualize();
    }

    public function Atualize() {
        $sql = "update EMBARCACAO_PROJETO set ";
        $sql .= "  NO_EMBARCACAO_PROJETO			= " . cBANCO::StringOk($this->mNO_EMBARCACAO_PROJETO);
        $sql .= " ,NO_MUNICIPIO						= " . cBANCO::StringOk($this->mNO_MUNICIPIO);
        $sql .= " ,DT_PRAZO_CONTRATO				= " . cBANCO::DataOk($this->mDT_PRAZO_CONTRATO);
        $sql .= " ,NO_CONTATO						= " . cBANCO::StringOk($this->mNO_CONTATO);
        $sql .= " ,NO_EMAIL_CONTATO					= " . cBANCO::StringOk($this->mNO_EMAIL_CONTATO);
        $sql .= " ,NU_TELEFONE_CONTATO				= " . cBANCO::StringOk($this->mNU_TELEFONE_CONTATO);
        $sql .= " ,QT_TRIPULANTES					= " . cBANCO::InteiroOk($this->mQT_TRIPULANTES);
        $sql .= " ,QT_TRIPULANTES_ESTRANGEIROS		= " . cBANCO::InteiroOk($this->mQT_TRIPULANTES_ESTRANGEIROS);
        $sql .= " ,NO_CONTRATO						= " . cBANCO::StringOk($this->mNO_CONTRATO);
        $sql .= " ,CO_PAIS							= " . cBANCO::ChaveOk($this->mCO_PAIS);
        $sql .= " ,DS_JUSTIFICATIVA					= " . cBANCO::StringOk($this->mDS_JUSTIFICATIVA);
        $sql .= " ,NO_PREFIXO						= " . cBANCO::StringOk($this->mNO_PREFIXO);
        $sql .= " ,cd_usuario						= " . cBANCO::ChaveOk($this->mcd_usuario);
        $sql .= " ,TX_OBSERVACAO					= " . cBANCO::StringOk($this->mTX_OBSERVACAO);
        $sql .= " ,ID_TIPO_EMBARCACAO_PROJETO		= " . cBANCO::ChaveOk($this->mID_TIPO_EMBARCACAO_PROJETO);
        $sql .= " ,nu_estado						= " . cBANCO::InteiroOk($this->mnu_estado);
        $sql .= " ,embp_fl_ativo					= " . cBANCO::SimNaoOk($this->membp_fl_ativo);
        $sql .= " where ";
        $sql .= "	 NU_EMPRESA				= " . $this->mNU_EMPRESA;
        $sql .= "	and NU_EMBARCACAO_PROJETO	= " . $this->mNU_EMBARCACAO_PROJETO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Atualiza a data da ultima revisao da embarcacao com a data atual
     * @param type $pcd_usuario Usuário que está atualizando a data da ultima revisao (opcional)
     */
    public function AtualizeUltimaRevisao($pcd_usuario = '') {
        if ($pcd_usuario == '') {
            $pcd_usuario = cSESSAO::$mcd_usuario;
        }
        $sql = "update embarcacao_projeto
				set embp_dt_ult_revisao = now()
				, cd_usuario_ult_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
				where NU_EMBARCACAO_PROJETO = " . $this->mNU_EMBARCACAO_PROJETO . " and NU_EMPRESA =" . $this->mNU_EMPRESA;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public static function daEmpresa($nu_empresa, $default = '') {
        $sql = "select nu_embarcacao_projeto chave, no_embarcacao_projeto descricao"
                . " from embarcacao_projeto"
                . " where embp_fl_ativo = 1"
                . " and nu_empresa = " . $nu_empresa
                . " order by no_embarcacao_projeto";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        if ($default){
            array_unshift($rs, array('valor' => '0', 'descricao' => $default, 'selected' => '0'));
        }
        return $rs;
    }

    public static function comboMustache($nu_empresa, $valor_atual = '', $default = "(selecione...)") {
        $sql = "select NU_EMBARCACAO_PROJETO valor"
                . ", NO_EMBARCACAO_PROJETO descricao"
                . ", if(NU_EMBARCACAO_PROJETO='" . $valor_atual . "' ,1 ,0 ) selected"
                . " from EMBARCACAO_PROJETO"
                . " where nu_empresa = ".$nu_empresa
                . " order by NO_EMBARCACAO_PROJETO";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor' => '0', 'descricao' => $default, 'selected' => '0'));

        if (intval($valor_atual == 0)) {
            $rs['0']['selected'] = true;
        }

        return $rs;
    }

}
