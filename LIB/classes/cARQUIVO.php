<?php

class cARQUIVO extends cMODELO {

    public $mNU_SEQUENCIAL;
    public $mID_SOLICITA_VISTO;
    public $mNU_SOLICITACAO;
    public $mNU_CANDIDATO;
    public $mNO_ARQUIVO;
    public $mNO_ARQ_ORIGINAL;
    public $mTP_ARQUIVO;
    public $mNU_EMPRESA;
    public $mDT_INCLUSAO;
    public $mid_taxa;
    public $mCO_TIPO_ARQUIVO;
    public $mNU_ADMIN;
    public $mFL_ADICIONADO_PELO_CANDIDATO;
    public $mcd_usuario_os_incompleta;
    public $mNU_TAMANHO;
    public $mNO_TIPO_ARQUIVO;
    public $mnu_candidato_tmp;

    public function getId() {
        return $this->mNU_SEQUENCIAL;
    }

    public function setId($pValor) {
        $this->mNU_SEQUENCIAL = $pValor;
    }

    public function sql_RecuperePeloId() {
        return "select arquivos.*, NU_SOLICITACAO"
                . "  from arquivos "
                . "  left join solicita_visto on solicita_visto.id_solicita_visto = arquivos.id_solicita_visto"
                . " where nu_sequencial = " . $this->mNU_SEQUENCIAL;
    }

    public function diretorio() {
        if (intval($this->mID_SOLICITA_VISTO) > 0) {
            return cAMBIENTE::getDirSol() . DIRECTORY_SEPARATOR . $this->empresaParaDiretorio();
        } else {
            if (intval($this->mNU_CANDIDATO) > 0 ){
                return cAMBIENTE::getDirDoc() . DIRECTORY_SEPARATOR . $this->empresaParaDiretorio() . DIRECTORY_SEPARATOR . $this->mNU_CANDIDATO ;
            } else {
                return cAMBIENTE::getDirDocTmp() . DIRECTORY_SEPARATOR . $this->mnu_candidato_tmp ;
            }
        }
    }

    public function empresaParaDiretorio() {
        if (intval($this->mNU_EMPRESA) > 0) {
            return $this->mNU_EMPRESA;
        } else {
            return '0';
        }
    }

    public static function CaminhoReal($pNU_CANDIDATO, $pNO_ARQUIVO, $pNU_EMPRESA, $pID_SOLICITA_VISTO, $pnu_candidato_tmp = '') {

        if (intval($pID_SOLICITA_VISTO) > 0) {
            $src = cAMBIENTE::getDirSol() . DIRECTORY_SEPARATOR . intval($pNU_EMPRESA) ;
        } else {
            if (intval($pNU_CANDIDATO) > 0 ){
                $src = cAMBIENTE::getDirDoc() . DIRECTORY_SEPARATOR . intval($pNU_EMPRESA) . DIRECTORY_SEPARATOR . $pNU_CANDIDATO ;
            } else {
                $src = cAMBIENTE::getDirDocTmp() . DIRECTORY_SEPARATOR . $pnu_candidato_tmp ;
            }
        }
        if (!is_dir($src)){
            mkdir($src, 0775, true);
        }
        return $src. DIRECTORY_SEPARATOR . $pNO_ARQUIVO;
    }

    public function caminhoRealInstancia() {
        $caminho = self::CaminhoReal($this->mNU_CANDIDATO, $this->mNO_ARQUIVO, $this->empresaParaDiretorio(), $this->mID_SOLICITA_VISTO, $this->mnu_candidato_tmp);
        return $caminho;
    }

    public static function UrlReal($pNU_CANDIDATO, $pNO_ARQUIVO, $pNU_EMPRESA, $pID_SOLICITA_VISTO) {
        if (intval($pID_SOLICITA_VISTO) > 0) {
            $src = cAMBIENTE::getUrlSol() . '/' . $pNU_EMPRESA . '/' . $pNO_ARQUIVO;
        } else {
            $src = cAMBIENTE::getUrlDoc() . '/' . $pNU_EMPRESA . '/' . $pNU_CANDIDATO . '/' . $pNO_ARQUIVO;
        }
        return $src;
    }

    public function urlRealInstancia() {
        return self::UrlReal($this->mNU_CANDIDATO, $this->mNO_ARQUIVO, $this->mNU_EMPRESA, $this->mID_SOLICITA_VISTO);
    }

    public static function Extensao($pNomeArquivo) {
        $ax1 = str_replace(".", "#", $pNomeArquivo);
        $aux = preg_split("[#]", $ax1);
        $x = count($aux);
        if ($x > 1) {
            $extensao = $aux[$x - 1];
        } else {
            $extensao = "txt";
        }
        return $extensao;
    }

    public static function CriarArquivoSolicitacao($pInputFile, $pNU_EMPRESA, $pID_SOLICITA_VISTO, $pID_TIPO_ARQUIVO, $pUsuario, $pNU_CANDIDATO = "") {
        global $mysoldir;
        if (!is_dir($mysoldir)) {
            mkdir($mysoldir, 0775);
        }
        $updir = $mysoldir . "/" . $pNU_EMPRESA;
        if (!is_dir($updir)) {
            mkdir($updir, 0775);
        }
        return self::CriarArquivo($pInputFile, $updir, $pNU_EMPRESA, 'S', $pID_SOLICITA_VISTO, $pID_TIPO_ARQUIVO, $pUsuario, $pNU_CANDIDATO);
    }

    /**
     * Cria um arquivo para um usuario específico
     */
    public static function CriarArquivo($pInputFile, $pDiretorioDestino, $pNU_EMPRESA, $pEntidade, $pIdEntidade, $pTipoArquivo, $pUsuario, $pNU_CANDIDATO = "") {
        // InputFile= $_FILES['campo']
        // NU_EMPRESA = id da empresa
        // Entidade = 'C' para candidato, 'S' para solicitacao
        // IdEntidade = id do candidato ou da solicitacao
        $ret = "";
        $noArqOriginal = $pInputFile['name'];
        $noArqOriginal = str_replace("'", "", $noArqOriginal);
        $extensao = self::Extensao($noArqOriginal);

        // Insere arquivo no banco
        if ($pEntidade == 'C') {
            $NU_CANDIDATO = $pIdEntidade;
            $ID_SOLICITA_VISTO = 'null';
        } else {
            $NU_CANDIDATO = $pNU_CANDIDATO;
            $ID_SOLICITA_VISTO = $pIdEntidade;
        }

        $sql = "insert into ARQUIVOS (NU_EMPRESA,NU_CANDIDATO,ID_SOLICITA_VISTO, ";
        $sql .= "NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NU_ADMIN, NO_ARQUIVO) ";
        $sql .= " values (";
        $sql .= "  " . cBANCO::ChaveOk($pNU_EMPRESA);
        $sql .= " ," . cBANCO::ChaveOk($NU_CANDIDATO);
        $sql .= " ," . cBANCO::ChaveOk($ID_SOLICITA_VISTO);
        $sql .= " ," . cBANCO::StringOk($noArqOriginal);
        $sql .= " ," . cBANCO::ChaveOk($pTipoArquivo);
        $sql .= " , now()";
        $sql .= " ," . cBANCO::ChaveOk($pUsuario);
        $sql .= " , '' ";
        $sql .= ")";
        cAMBIENTE::ExecuteQuery($sql);
        if (mysql_errno() > 0) {
            $ret = 'Não foi possível adicionar o arquivo ' . $noArqOriginal . "\n" . mysql_error() . "\nsql=$sql ";
        } else {
            $NU_SEQUENCIAL = cAMBIENTE::$db_pdo->lastInsertId();
            if ($pEntidade == 'C') {
                $noArquivo = "E" . $pEntidade . $pIdEntidade . "SQ" . $NU_SEQUENCIAL . "." . $extensao;
            } else {
                $aux = "000000000" . $pIdEntidade;
                $noArquivo = "SOLIC" . substr($aux, strlen($aux - 8)) . "_" . $NU_SEQUENCIAL . "." . $extensao;
            }
            $sql = "update ARQUIVOS set no_arquivo = '" . $noArquivo . "' where NU_SEQUENCIAL = " . $NU_SEQUENCIAL;
            cAMBIENTE::ExecuteQuery($sql);

            $uploadfile = $pDiretorioDestino . "/" . $noArquivo;
            // Copia arquivo para o destino
            try {
                move_uploaded_file($pInputFile['tmp_name'], $uploadfile);
                $ret = "Arquivo adicionado com sucesso";
            } catch (Exception $e) {
                $ret = 'Não foi possível mover o arquivo. Erro:' . $e->getMessage();
            }
        }
        return $ret;
    }

    /**
     * Cria um arquivo para um usuario específico
     */
    public static function CriarArquivoDoCandidato($nu_candidato, $pInputFile, $pTipoArquivo) {
        // InputFile= $_FILES['campo']
        // NU_EMPRESA = id da empresa
        // Entidade = 'C' para candidato, 'S' para solicitacao
        // IdEntidade = id do candidato ou da solicitacao
        $ret = "";
        $noArqOriginal = $pInputFile['name'];
        $noArqOriginal = str_replace("'", "", $noArqOriginal);
        $extensao = self::Extensao($noArqOriginal);

        $ID_SOLICITA_VISTO = 'null';

        $sql = "insert into ARQUIVOS (NU_EMPRESA,NU_CANDIDATO,ID_SOLICITA_VISTO, ";
        $sql .= "NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NU_ADMIN, NO_ARQUIVO, FL_ADICIONADO_PELO_CANDIDATO) ";
        $sql .= " values (";
        $sql .= " null ";
        $sql .= " ," . cBANCO::ChaveOk($nu_candidato);
        $sql .= " ," . cBANCO::ChaveOk($ID_SOLICITA_VISTO);
        $sql .= " ," . cBANCO::StringOk($noArqOriginal);
        $sql .= " ," . cBANCO::ChaveOk($pTipoArquivo);
        $sql .= " , now()";
        $sql .= " , null";
        $sql .= " , '' ";
        $sql .= " , 1 ";
        $sql .= ")";
        cAMBIENTE::ExecuteQuery($sql);
        if (mysql_errno() > 0) {
            $ret = 'Não foi possível adicionar o arquivo ' . $noArqOriginal . "\n" . mysql_error() . "\nsql=$sql ";
        } else {
            $NU_SEQUENCIAL = cAMBIENTE::$db_pdo->lastInsertId();
            $aux = "000000000" . $nu_candidato;
            $noArquivo = "CAND" . substr($aux, strlen($aux - 8)) . "_" . $NU_SEQUENCIAL . "." . $extensao;
            $sql = "update ARQUIVOS set no_arquivo = '" . $noArquivo . "' where NU_SEQUENCIAL = " . $NU_SEQUENCIAL;
            cAMBIENTE::ExecuteQuery($sql);

            $dirDestino = $uploadfile = $pDiretorioDestino . "/" . $noArquivo;
            // Copia arquivo para o destino
            try {
                move_uploaded_file($pInputFile['tmp_name'], $uploadfile);
                $ret = "Arquivo adicionado com sucesso";
            } catch (Exception $e) {
                $ret = 'Não foi possível mover o arquivo. Erro:' . $e->getMessage();
            }
        }
        return $ret;
    }

    /**
     * Cria um arquivo para um usuario específico
     */
    public static function CriarArquivoDoOperacional($nu_candidato, $pInputFile, $pTipoArquivo) {
        // InputFile= $_FILES['campo']
        // NU_EMPRESA = id da empresa
        // Entidade = 'C' para candidato, 'S' para solicitacao
        // IdEntidade = id do candidato ou da solicitacao
        $ret = "";
        $noArqOriginal = $pInputFile['name'];
        $noArqOriginal = str_replace("'", "", $noArqOriginal);
        $extensao = self::Extensao($noArqOriginal);
        $cand = new cCANDIDATO();
        $cand->Recuperar($nu_candidato);

        $ID_SOLICITA_VISTO = 'null';

        $sql = "insert into ARQUIVOS (NU_EMPRESA,NU_CANDIDATO,ID_SOLICITA_VISTO, ";
        $sql .= "NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NU_ADMIN, NO_ARQUIVO, FL_ADICIONADO_PELO_CANDIDATO) ";
        $sql .= " values (";
        $sql .= " " . cBANCO::ChaveOk($cand->mNU_EMPRESA);
        $sql .= " ," . cBANCO::ChaveOk($nu_candidato);
        $sql .= " ," . cBANCO::ChaveOk($ID_SOLICITA_VISTO);
        $sql .= " ," . cBANCO::StringOk($noArqOriginal);
        $sql .= " ," . cBANCO::ChaveOk($pTipoArquivo);
        $sql .= " , now()";
        $sql .= " , null";
        $sql .= " , '' ";
        $sql .= " , 0 ";
        $sql .= ")";
        cAMBIENTE::ExecuteQuery($sql);
        if (mysql_errno() > 0) {
            $ret = 'Não foi possível adicionar o arquivo ' . $noArqOriginal . "\n" . mysql_error() . "\nsql=$sql ";
        } else {
            $NU_SEQUENCIAL = cAMBIENTE::$db_pdo->lastInsertId();
            $aux = "000000000" . $nu_candidato;
            $noArquivo = "CAND" . substr($aux, strlen($aux - 8)) . "_" . $NU_SEQUENCIAL . "." . $extensao;
            $sql = "update ARQUIVOS set no_arquivo = '" . $noArquivo . "' where NU_SEQUENCIAL = " . $NU_SEQUENCIAL;
            cAMBIENTE::ExecuteQuery($sql);

            $uploadfile = $pDiretorioDestino . "/" . $noArquivo;
            // Copia arquivo para o destino
            try {
                move_uploaded_file($pInputFile['tmp_name'], $uploadfile);
                $ret = "Arquivo adicionado com sucesso";
            } catch (Exception $e) {
                $ret = 'Não foi possível mover o arquivo. Erro:' . $e->getMessage();
            }
        }
        return $ret;
    }

    public function CarreguePropriedadesPeloRecordset($prs) {
        $this->mNU_SEQUENCIAL = $prs['NU_SEQUENCIAL'];
        $this->mDT_INCLUSAO = $prs['DT_INCLUSAO'];
        $this->mID_SOLICITA_VISTO = $prs['ID_SOLICITA_VISTO'];
        $this->mID_SOLICITA_VISTO = $prs['id_solicita_visto'];
        $this->mNO_ARQUIVO = $prs['NO_ARQUIVO'];
        $this->mNO_ARQ_ORIGINAL = $prs['NO_ARQ_ORIGINAL'];
        $this->mNU_CANDIDATO = $prs['NU_CANDIDATO'];
        $this->mNU_EMPRESA = $prs['NU_EMPRESA'];
        $this->mNU_SOLICITACAO = $prs['NU_SOLICITACAO'];
        $this->mTP_ARQUIVO = $prs['TP_ARQUIVO'];
        $this->mCO_TIPO_ARQUIVO = $prs['CO_TIPO_ARQUIVO'];
        $this->mNU_TAMANHO = $prs['NU_TAMANHO'];
        $this->mFL_ADICIONADO_PELO_CANDIDATO = $prs['FL_ADICIONADO_PELO_CANDIDATO'];
        $this->mNO_TIPO_ARQUIVO = $prs['NO_TIPO_ARQUIVO'];
        $this->mCO_TIPO_ARQUIVO = $prs['CO_TIPO_ARQUIVO'];
        $this->mnu_candidato_tmp = $prs['nu_candidato_tmp'];
    }

    public function salvar() {
        if ($this->mNU_SEQUENCIAL > 0) {
            $sql = "update arquivos set "
                    . " id_solicita_visto = " . cBANCO::ChaveOk($this->mID_SOLICITA_VISTO)
                    . ", nu_candidato = " . cBANCO::ChaveOk($this->mNU_CANDIDATO)
                    . ", nu_candidato_tmp = " . cBANCO::ChaveOk($this->mnu_candidato_tmp)
                    . ", nu_empresa = " . cBANCO::ChaveOk($this->mNU_EMPRESA)
                    . ", tp_arquivo = " . cBANCO::ChaveOk($this->mTP_ARQUIVO)
                    . ", no_arquivo = " . cBANCO::StringOk($this->mNO_ARQUIVO)
                    . ", no_arq_original = " . cBANCO::StringOk($this->mNO_ARQ_ORIGINAL)
                    . ", fl_adicionado_pelo_candidato = " . cBANCO::ChaveOk($this->mFL_ADICIONADO_PELO_CANDIDATO)
                    . " where nu_sequencial = " . $this->mNU_SEQUENCIAL;
            cAMBIENTE::$db_pdo->exec($sql);
        } else {
            $sql = "insert into arquivos ("
                    . "  id_solicita_visto "
                    . ", nu_candidato "
                    . ", nu_candidato_tmp "
                    . ", nu_empresa "
                    . ", tp_arquivo "
                    . ", no_arquivo "
                    . ", no_arq_original "
                    . ", nu_admin "
                    . ", fl_adicionado_pelo_candidato "
                    . ", dt_inclusao"
                    . ") values ("
                    . "  " . cBANCO::ChaveOk($this->mID_SOLICITA_VISTO)
                    . ", " . cBANCO::ChaveOk($this->mNU_CANDIDATO)
                    . ", " . cBANCO::ChaveOk($this->mnu_candidato_tmp)
                    . ", " . cBANCO::ChaveOk($this->mNU_EMPRESA)
                    . ", " . cBANCO::ChaveOk($this->mTP_ARQUIVO)
                    . ", " . cBANCO::StringOk($this->mNO_ARQUIVO)
                    . ", " . cBANCO::StringOk($this->mNO_ARQ_ORIGINAL)
                    . ", " . cBANCO::ChaveOk($this->mNU_ADMIN)
                    . ", " . cBANCO::SimNaoOk($this->mFL_ADICIONADO_PELO_CANDIDATO)
                    . ", now()"
                    . ")";
            cAMBIENTE::$db_pdo->exec($sql);
            $this->mNU_SEQUENCIAL = cAMBIENTE::$db_pdo->lastInsertId();
        }
    }

    public static function JSON_ArquivosDoCandidato($pNU_CANDIDATO) {
        $sql = "select a.*
					 , date_format(DT_INCLUSAO, '%d/%m/%Y') as DT_INCLUSAO_FMT
					 , NO_TIPO_ARQUIVO
				  from arquivos a
				  left join tipo_arquivo  ta on ta.id_tipo_arquivo = a.tp_arquivo
				 where nu_candidato = " . $pNU_CANDIDATO . " order by DT_INCLUSAO desc";
        $res = cAMBIENTE::ConectaQuery($sql);
        $arquivos = array();
        while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $arquivos[] = array(
                "NU_SEQUENCIAL" => $rs['NU_SEQUENCIAL']
                , "NO_TIPO_ARQUIVO" => $rs['NO_TIPO_ARQUIVO']
                , "DT_INCLUSAO_FMT" => $rs['DT_INCLUSAO_FMT']
                , "NO_ARQ_ORIGINAL" => $rs['NO_ARQ_ORIGINAL']
                , "url" => self::CaminhoReal($rs['NU_CANDIDATO'], $rs['NO_ARQUIVO'], $rs['NU_EMPRESA'], $rs['ID_SOLICITA_VISTO'])
            );
        }
        return $arquivos;
    }

    public function descricaoDetalhada() {
        return $this->mCO_TIPO_ARQUIVO . $this->numeroOs() . " (" . $this->mNO_ARQ_ORIGINAL . ")";
    }

    public function numeroOs() {
        if ($this->mID_SOLICITA_VISTO > 0) {
            if ($this->mNU_SOLICITACAO > 0) {
                return "OS " . $this->mNU_SOLICITACAO;
            } else {
                return "(OS excluída)";
            }
        } else {
            return '';
        }
    }

    public function nomeCalculado() {
        $ext = pathinfo($this->mNO_ARQ_ORIGINAL, PATHINFO_EXTENSION);
        if ($ext != '') {
            $ext = '.' . $ext;
        }
        if ($this->mID_SOLICITA_VISTO > 0) {
            return "SOLIC" . substr("00000000" . $this->mID_SOLICITA_VISTO, -8) . "_" . $this->mNU_SEQUENCIAL . $ext;
        } else {
            if ($this->mFL_ADICIONADO_PELO_CANDIDATO) {
                $nu = $this->mNU_CANDIDATO;
                if ($nu == ''){
                    $nu = $this->mnu_candidato_tmp;
                }
                return "CAND" . substr("00000000" . $nu, -8) . "_" . $this->mNU_SEQUENCIAL . $ext;
            } else {
                return "EC" . substr("00000000" . $nu, -8) . "_" . $this->mNU_SEQUENCIAL . $ext;
            }
        }
    }

    public function existe() {
        return file_exists($this->caminhoRealInstancia());
    }

    /**
     * Indica se o arquivo é uma imagem
     */
    public function ehImagem() {
        $arq = explode(".", $this->mNO_ARQUIVO);
        if (strtoupper($arq[1]) == "JPG" || strtoupper($arq[1]) == "TIFF" || strtoupper($arq[1]) == "BMP") {
            return true;
        } else {
            return false;
        }
    }

    public function ehOS() {
        if ($this->mID_SOLICITA_VISTO > 0) {
            if ($this->mNU_SOLICITACAO > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function armazenaArquivo($inputFile) {
        if (!is_dir($this->diretorio())) {
            mkdir($this->diretorio(), 0775, true);
        }
        move_uploaded_file($inputFile['tmp_name'], $this->caminhoRealInstancia());
    }

    public function removerArquivo() {
        try{
            unlink($this->caminhoRealInstancia());
        } catch (Exception $ex) {

        }
    }

    public function excluir(){
        $sql = "delete from arquivos where nu_sequencial = ".$this->mNU_SEQUENCIAL;
        cAMBIENTE::ExecuteQuery($sql);
        if ($this->existe()){
            unlink($this->caminhoRealInstancia());
        }
    }

    public function atualizaTamanho(){
        $this->mNU_TAMANHO = filesize($this->caminhoRealInstancia());
        $sql = "update arquivos set nu_tamanho = ".cBANCO::InteiroOk($this->mNU_TAMANHO)." where nu_sequencial = ".$this->mNU_SEQUENCIAL;
        cAMBIENTE::$db_pdo->exec($sql);
    }
}
