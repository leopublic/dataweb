<?php
class cTIPO_ACOMPANHAMENTO extends cMODELO
{
	public $mID_TIPO_ACOMPANHAMENTO;
	public $mNO_TIPO_ACOMPANHAMENTO;
	public $mCO_TIPO_ACOMPANHAMENTO;
	public $mFL_MULTI_CANDIDATOS;

	
	public function campoid()
	{
		return 'ID_TIPO_ACOMPANHAMENTO';
	}
	public function getid(){
		return $this->mID_TIPO_ACOMPANHAMENTO;
	}
	public function setid($pValor){
		$this->mID_TIPO_ACOMPANHAMENTO = $pValor;
	}
	public function sql_Liste(){	
		$sql = " select * from tipo_acompanhamento where 1=1";
		return $sql;
	}
	public function sql_RecuperePeloId(){
		$sql = " select * from tipo_acompanhamento where ID_TIPO_ACOMPANHAMENTO = ".$this->mID_TIPO_ACOMPANHAMENTO;
		return $sql;
	}

	public function Atualizar(){
		$sql = " update tipo_acompanhamento set ";
		$sql.= "    NO_TIPO_ACOMPANHAMENTO  = ".cBANCO::StringOk($this->mNO_TIPO_ACOMPANHAMENTO);
		$sql.= "  , CO_TIPO_ACOMPANHAMENTO = ".cBANCO::StringOk($this->mCO_TIPO_ACOMPANHAMENTO);
		$sql.= "  , mFL_MULTI_CANDIDATOS  = ".cBANCO::SimNaoOk($this->mFL_MULTI_CANDIDATOS);
		$sql.= " where ID_TIPO_ACOMPANHAMENTO = ".$this->mID_TIPO_ACOMPANHAMENTO;
		cAMBIENTE::$db_pdo->exec($sql);
	}

	public function Incluir(){
		$sql = " insert into tipo_acompanhamento(";
		$sql.= "        NO_TIPO_ACOMPANHAMENTO";
		$sql.= "      , CO_TIPO_ACOMPANHAMENTO";
		$sql.= "      , FL_MULTI_CANDIDATOS";
		$sql.= ") values (";
		$sql.= "    ".cBANCO::StringOk($this->mNO_TIPO_ACOMPANHAMENTO);
		$sql.= "  , ".cBANCO::StringOk($this->mCO_TIPO_ACOMPANHAMENTO);
		$sql.= "  , ".cBANCO::SimNaoOk($this->mFL_MULTI_CANDIDATOS);
		$sql.= ")";
		cAMBIENTE::$db_pdo->exec($sql);
		$this->setid(cAMBIENTE::$db_pdo->lastInsertId());
	}

}
