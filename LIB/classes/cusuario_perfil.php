<?php

/**
 * @property string $mcd_perfil
 * @property string $mnm_perfil
 */
class cusuario_perfil extends cMODELO {

    protected $cd_perfil;
    protected $nm_perfil;
    protected $eh_funcionario;
    protected $eh_armador;
    protected $eh_operador;
    protected $eh_operrestrito;
    protected $eh_cliente;
    protected $eh_financeiro;
    protected $dt_cad;
    protected $nivel;

    /**
     * Get e set para a propriedade cd_perfil
     */
    public function get_cd_perfil() {
        return $this->cd_perfil;
    }

    public function set_cd_perfil($pVal) {
        $this->cd_perfil = $pVal;
    }

    /**
     * Get e set para a propriedade nm_perfil
     */
    public function get_nm_perfil() {
        return $this->nm_perfil;
    }

    public function set_nm_perfil($pVal) {
        $this->nm_perfil = $pVal;
    }

    /**
     * Get e set para a propriedade eh_funcionario
     */
    public function get_eh_funcionario() {
        return $this->eh_funcionario;
    }

    public function set_eh_funcionario($pVal) {
        $this->eh_funcionario = $pVal;
    }

    /**
     * Get e set para a propriedade eh_armador
     */
    public function get_eh_armador() {
        return $this->eh_armador;
    }

    public function set_eh_armador($pVal) {
        $this->eh_armador = $pVal;
    }

    /**
     * Get e set para a propriedade eh_operador
     */
    public function get_eh_operador() {
        return $this->eh_operador;
    }

    public function set_eh_operador($pVal) {
        $this->eh_operador = $pVal;
    }

    /**
     * Get e set para a propriedade eh_operrestrito
     */
    public function get_eh_operrestrito() {
        return $this->eh_operrestrito;
    }

    public function set_eh_operrestrito($pVal) {
        $this->eh_operrestrito = $pVal;
    }

    /**
     * Get e set para a propriedade eh_cliente
     */
    public function get_eh_cliente() {
        return $this->eh_cliente;
    }

    public function set_eh_cliente($pVal) {
        $this->eh_cliente = $pVal;
    }

    /**
     * Get e set para a propriedade eh_financeiro
     */
    public function get_eh_financeiro() {
        return $this->eh_financeiro;
    }

    public function set_eh_financeiro($pVal) {
        $this->eh_financeiro = $pVal;
    }

    /**
     * Get e set para a propriedade dt_cad
     */
    public function get_dt_cad() {
        return $this->dt_cad;
    }

    public function set_dt_cad($pVal) {
        $this->dt_cad = $pVal;
    }

    /**
     * Get e set para a propriedade nivel
     */
    public function get_nivel() {
        return $this->nivel;
    }

    public function set_nivel($pVal) {
        $this->nivel = $pVal;
    }

    public function get_eh_admin() {
        if ($this->nivel > 7) {
            return 'S';
        } else {
            return 'N';
        }
    }

    public function Salve() {
        
    }

    public function Atualize() {
        
    }

    public function Insira() {
        
    }

    public function RecupereSe($pcd_perfil = '') {
        if ($pcd_perfil != '') {
            $this->cd_perfil = $pcd_perfil;
        }
        if ($this->cd_perfil == 0) {
            throw new Exception("Chave n&atilde;o informada.");
        }
        $sql = "select * from usuario_perfil where cd_perfil = " . $this->cd_perfil;
        $res = conectaQuery($sql, __CLASS . '.' . __FUNCTION__);
        if ($rs = mysql_fetch_array($res)) {
            $this->cd_perfil = $rs['cd_perfil'];
            $this->nm_perfil = $rs['nm_perfil'];
            $this->eh_funcionario = $rs['eh_funcionario'];
            $this->eh_armador = $rs['eh_armador'];
            $this->eh_operador = $rs['eh_operador'];
            $this->eh_operrestrito = $rs['eh_operrestrito'];
            $this->eh_cliente = $rs['eh_cliente'];
            $this->nivel = $rs['nivel'];
            $this->eh_financeiro = $rs['eh_financeiro'];
        } else {
            throw new Exception("Perfil n&atilde;o encontrado (cd_perfil=" . $this->cd_perfil . ")");
        }
    }

    public function ListeTodos($pOrdenacao) {
        $sql = '';
        $sql .= "select up.cd_perfil, nm_perfil , count(*)
			       from usuario_perfil up
			  left join usuarios on usuarios.cd_perfil = up.cd_perfil';
			   group by up.cd_perfil, nm_perfil ";
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        if ($res = conectaQuery($sql, __CLASS__)) {
            return $res;
        }
    }

    public function acessosDoUsuario($pcd_usuario = 0) {
        $sql = '';
        if ($pcd_usuario == '') {
            $pcd_usuario = 0;
        }
        $sql .= "select up.cd_perfil, nm_perfil , pu.cd_perfil cd_perfil_concedido
			       from usuario_perfil up 
			  left join perfil_usuario pu on pu.cd_perfil = up.cd_perfil and cd_usuario = " . $pcd_usuario . " order by nm_perfil";
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            return $res;
        }
    }

    public function ehAdm($pcd_usuario) {
        $sql = "select cd_perfil
                  from perfil_usuario 
                 where cd_perfil = 1 
                   and cd_usuario = " . $pcd_usuario;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetch();
            if ($rs[0] == 1) {
                return 'S';
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function ehFuncionario($pcd_usuario){
        $sql .= "select up.cd_perfil "
                . " from usuario_perfil up "
                . " join perfil_usuario pu on pu.cd_perfil = up.cd_perfil and cd_usuario = " . $pcd_usuario 
                . " where up.eh_funcionario = 'S'"
                . " order by nm_perfil";
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetchAll();
            if(count($rs) > 0){
                return 'S';
            } else {
                return '';
            }
        } else {
            return '';
        }
        
    }

    public function ehCliente($pcd_usuario){
        $sql .= "select up.cd_perfil "
                . " from usuario_perfil up "
                . " join perfil_usuario pu on pu.cd_perfil = up.cd_perfil and cd_usuario = " . $pcd_usuario 
                . " where pu.cd_perfil = 10"
                . " order by nm_perfil";
        if ($res = cAMBIENTE::ConectaQuery($sql, __CLASS__)) {
            $rs = $res->fetchAll();
            if(count($rs) > 0){
                return 'S';
            } else {
                return '';
            }
        } else {
            return '';
        }
        
    }
    
    public function atualizaAcessosDoUsuario($pcd_perfil, $pcd_usuario) {
        if (is_array($pcd_perfil)) {
            $cd_perfil = implode(",", $pcd_perfil);
        } elseif (intval($cd_perfil) > 0) {
            $cd_perfil = $pcd_perfil;
        } else {
            $cd_perfil = '0';
        }
        
        $sql = "delete from perfil_usuario where cd_usuario = " . $pcd_usuario . " and cd_perfil not in (" . $cd_perfil . ")";
  //      print '<br/>'.$sql;
        cAMBIENTE::ExecuteQuery($sql);
        $sql = "insert into perfil_usuario(cd_usuario, cd_perfil, dt_concedido, cd_usuario_concessao) "
                . " select " . $pcd_usuario . ", cd_perfil, now(), " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario)
                . "   from usuario_perfil "
                . "  where cd_perfil in (" . $cd_perfil . ") "
                . "    and cd_perfil not in (select cd_perfil from perfil_usuario where cd_usuario = " . $pcd_usuario . ")";
//        print '<br/>'.$sql;
        cAMBIENTE::ExecuteQuery($sql);
    }

}
