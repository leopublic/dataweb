<?php
class cmenu{
	protected $itens;
	protected $titulo;

	public function __construct($ptitulo = ''){
		$this->titulo = $ptitulo;
	}

	public function AdicioneItem($ptitulo, $phref, $target=""){
		$this->itens[] = array(
			"href" => $phref
			, "titulo" => $ptitulo
			, "target"=> $target
        );
	}

	public function AdicioneMenu($ptitulo, $pmenu){
		$this->itens[] = array("titulo"=> $ptitulo, "menu" =>$pmenu);
	}

	public function JSON(){
		$data = array();
		$data['titulo'] = $this->titulo; 
		$data['itens'] = array();
		foreach($this->itens as $item){
			if(isset($item['menu'])){
				$x = $item['menu']->JSON();
				$x['tipo'] = 'menu';
				$data['itens'][] = $x;
			}
			else{
				$item['tipo'] = 'link';
				$data['itens'][] = $item;
			}
		}

		return $data;
	}

	public static function NovoMenuCandidato(){
		$menu = new cmenu('esquerdo');
		$menu->AdicioneItem('PIT', "/paginas/carregueCliente.php?controller=cCTRL_PIT&metodo=listarRecibosCliente");
		$menu->AdicioneItem('Personal data', "/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO_USUARIO&metodo=editarInfo");
		$menu->AdicioneItem('Receipts', "/paginas/carregueCliente.php?controller=cCTRL_PIT&metodo=recibos");
		return $menu;
	}

	public static function NovoMenuCandidatoSystem(){
		$menu = new cmenu('direito');
		$menu->AdicioneItem('Exit', '/sair.php');
		return $menu;
	}

	public static function NovoMenuCliente(){
		$menu = new cmenu('esquerdo');
		$menu->AdicioneItem('List candidates', "/paginas/carregueCliente.php?controller=cCTRL_CAND_CLIE_LIST&metodo=Liste");
		$menuData = new cmenu('Data Requests');
		$menuData->AdicioneItem('Fill new candidate', "/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO_CLIENTE&metodo=editarInfo&NU_CANDIDATO=0&nu_candidato_tmp=0");
		$menuData->AdicioneItem('Send invitation', "/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO_CLIENTE&metodo=getEnviarconvite");
		$menuData->AdicioneItem('Pending Data Requests', "/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO_CLIENTE&metodo=getListarPendentes");
		$menu->AdicioneMenu('Reports', $menuData);
		$menuReports = new cmenu('Reports');
		$menuReports->AdicioneItem('Cancelations', "/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO_CLIENTE&metodo=getCancelamentos");
		$menuReports->AdicioneItem('Prorrogations', "/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO_CLIENTE&metodo=getProrrogacoes");
		if (cSESSAO::$mcd_empresa > 0){
			$emp = new cEMPRESA;
			$emp->RecuperePeloId(cSESSAO::$mcd_empresa);
			if ($emp->mcfgr_id > 0){
				$menuReports->AdicioneItem('Excel list', "/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO_CLIENTE&metodo=getListarCandidatosExcel", "_blank");
			}
		}
		$menu->AdicioneMenu('Reports', $menuReports);
		return $menu;
	}

	public static function NovoMenuClienteSystem(){
		$menu = new cmenu('direito');
		$menu->AdicioneItem('Exit', '/sair.php');
		$menuSystem = new cmenu('System');
		$menuSystem->AdicioneItem('Change password', '/paginas/carregueCliente.php?controller=cCTRL_USUARIOS&metodo=RenoveMinhaSenha');
		$menu->AdicioneMenu('System', $menuSystem);
		return $menu;
	}

}
