<?php

class ctipo_despesa extends cMODELO {

    public $mid_tipo_despesa;
    public $mnome;
    public $mdt_importacao;

    public static function get_nomeCampoChave() {
        return 'id_tipo_despesa';
    }

    public static function get_nomeTabelaBD() {
        return 'tipo_despesa';
    }

    public function get_nomeTabela() {
        return self::get_nomeTabelaBD();
    }

    public function campoid() {
        return self::get_nomeCampoChave();
    }

    public function setid($pid) {
        $this->mid_fornecedor = $pid;
    }

    public function getid() {
        return $this->mid_tipo_despesa;
    }

    public function sql_Liste() {
        $sql = "    select *
			           from tipo_despesa";
        return $sql;
    }

    public function sql_RecuperePeloId() {
        $sql = "    select *
					   from tipo_despesa
					  where id_tipo_despesa = " . $this->mid_tipo_despesa;
        return $sql;
    }

    public function cursorTodos() {
        $sql = "select * from tipo_despesa order by nome";
        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $rs;
    }

    public static function cursorComboJson() {
        $sql = "select id_tipo_despesa chave , nome descricao from tipo_despesa order by nome";
        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $rs;
    }
}
