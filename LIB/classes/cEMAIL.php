<?php
class cEMAIL {
    private $msg;
    public $mail;

    public function __construct() {
        $this->mail = new PHPMailer(); //
        $this->mail->isSMTP();
        $this->mail->isHTML(true); //
        $this->mail->CharSet = "utf-8";
        $this->mail->Host = "smtp.gmail.com";
        $this->mail->Port = 587;
//        $this->mail->Host = "smtp.office365.com";
//        $this->mail->Port = 587;
        $this->mail->SMTPAuth = "true";
        $this->mail->SMTPSecure = "tls";
        $this->mail->Username = "dataweb@m2software.com.br";
        $this->mail->Password = 'gg0rezqn';
//        $this->mail->Username = " visa.brazil@mundivisas.com.br";
//        $this->mail->Password = 'M2dw062015';
//        $this->mail->From = " visa.brazil@mundivisas.com.br";
        $this->mail->From = "dataweb@m2software.com.br";
        $this->mail->FromName = "Dataweb";
    }

    public function setReplyTo($pValor) {
        $this->mail->AddReplyTo($pValor);
    }

    public function setMsg($pValor) {
        $this->msg = $pValor;
    }

    public function setFrom($pEmail, $pNome = 'Dataweb') {
        $this->mail->From = $pEmail;
        $this->mail->FromName = $pNome;
    }

    public function setSubject($pValor) {
        $this->mail->Subject = $pValor;
    }

    public function AdicioneDestinatario($pEmail) {
        $this->mail->AddAddress($pEmail);
    }

    public function Envie() {
        $msg = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
        $msg .= "<html>";
        $msg .= "<head></head>";
        $msg .= "<body style=\"background-color:#fff;\" >";
        $msg .= $this->msg;
        $msg .= "</body>";
        $msg .= "</html>";
        $this->mail->Body = $msg;
        if (!$this->mail->Send()) {
            print "Erro de envio: " . $this->mail->ErrorInfo;
        }
    }

    public function EnvieMsgIntegral() {
        $this->mail->Body = $this->msg;
        if (!$this->mail->Send()) {
            print "Erro de envio: " . $this->mail->ErrorInfo;
        }
    }

    public function EnvieSimplificado($email, $msg, $titulo, $reply_address = '') {
        $mail = new cEMAIL;
        $mail->AdicioneDestinatario($email);
        $mail->setSubject($titulo);
        $mail->setMsg($msg);
        if ($reply_address){
            $mail->setFrom($reply_address);
            $mail->setReplyTo($reply_address);
        }
        $mail->EnvieMsgIntegral();
    }
}
