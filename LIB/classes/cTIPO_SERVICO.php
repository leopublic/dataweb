<?php
class cTIPO_SERVICO extends cMODELO{
	protected $mNU_TIPO_SERVICO;
	protected $mNO_TIPO_SERVICO;
	protected $mCO_TIPO_SERVICO;
	
	public function getId()
	{
		return $this->mNU_TIPO_SERVICO;
	}
	
	public function setId($pValor)
	{
		$this->mNU_TIPO_SERVICO = $pValor;
	}
	
	public function campoid() 
	{
		return 'NU_TIPO_SERVICO';
	}
	
	public function sql_RecuperePeloId() {
		$sql = "select * from tipo_servico where nu_tipo_servico = ".$this->mNU_TIPO_SERVICO;
		return $sql;
	}
	
	public function Incluir()
	{
		$sql= "insert into tipo_servico(
				 no_tipo_servico 
				,co_tipo_servico 
		       ) values (
				 ".cBANCO::StringOk($this->mNO_TIPO_SERVICO)."
				,".cBANCO::StringOk($this->mCO_TIPO_SERVICO)."
				)";
		cAMBIENTE::ExecuteQuery($sql);
		$this->mNU_TIPO_SERVICO = cAMBIENTE::$db_pdo->lastInsertId();
	}
	
	public function Atualizar()
	{
		$sql= "update tipo_servico set 
				 no_tipo_servico = ".cBANCO::StringOk($this->mNO_TIPO_SERVICO)."
				,co_tipo_servico = ".cBANCO::StringOk($this->mCO_TIPO_SERVICO)."
			  where nu_tipo_servico=".$this->mNU_TIPO_SERVICO;
		cAMBIENTE::ExecuteQuery($sql);
	}
	
	public function sql_Liste()
	{
		$sql = "select * from tipo_servico where 1=1";
		return $sql;
	}
}
