<?php
class cARQUIVO_DR{
	const DIRETORIO = "arquivos_dr";
	protected $NU_SEQUENCIAL;
	protected $NU_CANDIDATO;
	protected $TP_ARQUIVO;
	protected $NO_ARQ_ORIGINAL;

    /**
     * Get e set para a propriedade NU_SEQUENCIAL
     */
    public function get_NU_SEQUENCIAL(){
        return $this->NU_SEQUENCIAL;
    }
    public function set_NU_SEQUENCIAL($pVal){
        $this->NU_SEQUENCIAL = $pVal;
    }
    /**
     * Get e set para a propriedade NU_CANDIDATO
     */
    public function get_NU_CANDIDATO(){
        return $this->NU_CANDIDATO;
    }
    public function set_NU_CANDIDATO($pVal){
        $this->NU_CANDIDATO = $pVal;
    }
    /**
     * Get e set para a propriedade TP_ARQUIVO
     */
    public function get_TP_ARQUIVO(){
        return $this->TP_ARQUIVO;
    }
    public function set_TP_ARQUIVO($pVal){
        $this->TP_ARQUIVO = $pVal;
    }
    /**
     * Get e set para a propriedade NO_ARQ_ORIGINAL
     */
    public function get_NO_ARQ_ORIGINAL(){
        return $this->NO_ARQ_ORIGINAL;
    }
    public function set_NO_ARQ_ORIGINAL($pVal){
		$val = str_replace("#","",str_replace("'","",$pVal));
        $this->NO_ARQ_ORIGINAL = $val;
    }

	public static function CaminhoReal($pNU_CANDIDATO, $pNO_ARQUIVO, $pNU_EMPRESA, $pNU_SOLICITACAO){
		if (intval($pNU_SOLICITACAO) > 0){
			$src = cAMBIENTE::getDirSol().DIRECTORY_SEPARATOR.$pNU_EMPRESA.DIRECTORY_SEPARATOR.$pNO_ARQUIVO;
		}
		else {
			$src = cAMBIENTE::getDirDoc().DIRECTORY_SEPARATOR.$pNU_EMPRESA.DIRECTORY_SEPARATOR.$pNU_CANDIDATO.DIRECTORY_SEPARATOR.$pNO_ARQUIVO;
		}
		return $src;
	}

	public function get_URL(){
		$src = cAMBIENTE::getUrlDocDr(). '/' . $this->get_NO_ARQUIVO();
		return $src;
	}

	public static function Extensao($pNomeArquivo)
	{
		$ax1 = str_replace(".","#",$pNomeArquivo);
		$aux = preg_split("[#]",$ax1);
		$x = count($aux);
		if($x>1) {
			$extensao = $aux[$x-1];
		} else {
			$extensao = "txt";
		}
		return $extensao;
	}
	
	public function Incluir(){
		$sql = "insert into dr.arquivos (
					NU_CANDIDATO 
					,NO_ARQ_ORIGINAL
					,TP_ARQUIVO
					,DT_INCLUSAO
				) values (
					".cBANCO::ChaveOk($this->NU_CANDIDATO)."
					,".cBANCO::StringOk($this->NO_ARQ_ORIGINAL)."
					,".cBANCO::ChaveOk($this->TP_ARQUIVO)."
					, now()
				)";
		cAMBIENTE::ExecuteQuery($sql);
		$this->NU_SEQUENCIAL = cAMBIENTE::$db_pdo->lastInsertId();
	}

	public function get_NO_ARQUIVO(){
		return substr('00000000'.$this->NU_SEQUENCIAL, -8);
	}

	public static function CriarArquivo($pInputFile, $pNU_CANDIDATO, $pTP_ARQUIVO){
		var_dump($pInputFile);
		$ret = "";
		$arq = new cARQUIVO_DR();
		$arq->set_NU_CANDIDATO($pNU_CANDIDATO);
		$arq->set_TP_ARQUIVO($pTP_ARQUIVO);
		$arq->set_NO_ARQ_ORIGINAL($pInputFile['name']);

		$arq->Incluir();

		$uploadfile = cAMBIENTE::getDirDocDr()."/".$arq->get_NO_ARQUIVO();
		// Copia arquivo para o destino	
		move_uploaded_file($pInputFile['tmp_name'], $uploadfile);
	}

	public function RecupereCV($pNU_CANDIDATO){
		$sql = "select * from dr.arquivos where NU_CANDIDATO = ".$pNU_CANDIDATO;
		$res = cAMBIENTE::$db_pdo->query($sql);
		$rw = $res->fetch(PDO::FETCH_ASSOC);
		if($rw['NU_SEQUENCIAL'] > 0){
			$this->TP_ARQUIVO = $rw['TP_ARQUIVO'];
			$this->NU_SEQUENCIAL = $rw['NU_SEQUENCIAL'];
			$this->NO_ARQ_ORIGINAL = $rw['NO_ARQ_ORIGINAL'];
		}

	}
}
