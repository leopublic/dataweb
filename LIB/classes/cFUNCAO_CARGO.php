<?php

class cFUNCAO_CARGO extends cMODELO {

    protected $mCO_FUNCAO;
    protected $mNO_FUNCAO;
    protected $mNO_FUNCAO_EM_INGLES;
    protected $mNU_CBO;
    protected $mTX_DESCRICAO_ATIVIDADES;
    protected $mNO_FUNCAO_ALTERNATIVO;

    public static function get_nomeCampoChave(){
        return 'co_funcao';
    }
    public static function get_nomeCampoDescricao(){
        return 'no_funcao';
    }
    public static function get_nomeTabelaBD(){
        return 'FUNCAO_CARGO';
    }

    public function get_nomeTabela() {
        return 'FUNCAO_CARGO';
    }

    public function getId() {
        return $this->mCO_FUNCAO;
    }

    public function setId($pValor) {
        $this->mCO_FUNCAO = $pValor;
    }

    public function campoid() {
        return 'CO_FUNCAO';
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from funcao_cargo where co_funcao = " . $this->mCO_FUNCAO;
        return $sql;
    }

    public function Incluir() {
        $sql = "insert into funcao_cargo(
				 no_funcao
				,no_funcao_em_ingles
				,nu_cbo
				,tx_descricao_atividades
				,no_funcao_alternativo
		       ) values (
				 " . cBANCO::StringOk($this->mNO_FUNCAO) . "
				," . cBANCO::StringOk($this->mNO_FUNCAO_EM_INGLES) . "
				," . cBANCO::StringOk($this->mNU_CBO) . "
				," . cBANCO::StringOk($this->mTX_DESCRICAO_ATIVIDADES) . "
				," . cBANCO::StringOk($this->mNO_FUNCAO_ALTERNATIVO) . "
				)";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mCO_FUNCAO = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function Atualizar() {
        $sql = "update funcao_cargo set
				 no_funcao = " . cBANCO::StringOk($this->mNO_FUNCAO) . "
				,no_funcao_em_ingles = " . cBANCO::StringOk($this->mNO_FUNCAO_EM_INGLES) . "
				,nu_cbo = " . cBANCO::StringOk($this->mNU_CBO) . "
				,tx_descricao_atividades= " . cBANCO::StringOk($this->mTX_DESCRICAO_ATIVIDADES) . "
				,no_funcao_alternativo = " . cBANCO::StringOk($this->mNO_FUNCAO_ALTERNATIVO) . "
			  where co_funcao=" . $this->mCO_FUNCAO;
    //           echo '<!DOCTYPE html>' . "\n";
    //           echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
    // echo $sql;
    // exit();
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_Liste() {
        $sql = "select * from funcao_cargo where 1=1";
        return $sql;
    }

    public function recuperePeloAlternativo($alternativo) {
        $sql = "select * from funcao_cargo where NO_FUNCAO_ALTERNATIVO like '%" . $alternativo . "%' or NO_FUNCAO_EM_INGLES like '%" . $alternativo . "%' or NO_FUNCAO like '%" . $alternativo . "%'";
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetch(PDO::FETCH_BOTH);
            cBANCO::CarreguePropriedades($rs, $this);
        }
    }

    public static function comboMustache($valor_atual = '', $default = "(selecione...)") {
        $sql = "select CO_FUNCAO valor, NO_FUNCAO descricao, if(CO_FUNCAO='" . $valor_atual . "' ,1 ,0 ) selected from FUNCAO_CARGO order by NO_FUNCAO";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor' => '0', 'descricao' => $default, 'selected' => '0'));

        if (intval($valor_atual == 0)) {
            $rs['0']['selected'] = true;
        }

        return $rs;
    }

    public static function comboMustacheIngles($valor_atual = '', $default = "(selecione...)") {
        $sql = "select CO_FUNCAO valor, NO_FUNCAO_EM_INGLES descricao, if(CO_FUNCAO='" . $valor_atual . "' ,1 ,0 ) selected from FUNCAO_CARGO order by NO_FUNCAO_EM_INGLES";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor' => '0', 'descricao' => $default, 'selected' => '0'));

        if (intval($valor_atual == 0)) {
            $rs['0']['selected'] = true;
        }

        return $rs;
    }

}
