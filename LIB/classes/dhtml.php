<?php

/**
 * @author Fabio
 * @copyright 2010
 */

    Class dhtml extends html{
        
        
        
        
        public $action = array();
        
        
        
        
        function Actions($objActions){
            
            $this->action = $objActions;
            
        }
        
        function GetAction($tipo){
            
            $res = null;
            
            if($tipo == 'find'){

                if($this->action['find']){
                  
                   if($this->action['value'] <> ''){
                       $sql = $this->action['find'].' '.$this->action['value']; 
                       $r = sql::query($sql);
                       
                      
                       if($r->count()>0){
                        $res = $r->fetch();
                       }
                   }  

                    
                }   
            }
            
            if($tipo == 'url'){
                if($this->action['url']){
                   $res = $this->action['url']; 
                      
                }
            }
            
            
            return $res;
            
 
        }
        
        
        
        
        function masterEdit($FieldName, $objActions, $value){
            

            $this->Actions($objActions);
            $this->action['value'] = $value;
            
            $val = $this->GetAction('find');
            
            if($val['descr'] == ''){
                $val['descr'] = ' --- selecione ... ---';
            }
           
           
            
            
            return '<span id="masterEdit_'.$FieldName.'" class="masterEdit">'.
                        $this->input('hidden', array('name'=>$FieldName, 'value'=>$val['pk'])).
                        '<span id="masterEditDescr_'.$FieldName.'" class="masterEditDescr">'.$val['descr'].'</span>'.
                        '<a href="'.$this->action['url'].'" rel="'.$FieldName.'" title="'.$this->action['title'].'" class="masterEditLink"> 
                        <img src="/imagens/icons/application_edit.png" />
                        </a>'. 
                   '</span>';
            
        }
        
        
        
    }

?>
