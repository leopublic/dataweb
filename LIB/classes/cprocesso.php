<?php

class cprocesso extends cENTIDADE_PERSISTENTE {

    public $mcodigo;
    public $mcd_candidato;
    public $mcodigo_processo_mte;
    public $mid_solicita_visto;
    public $mfl_vazio;
    public $mfl_processo_atual;
    public $mcd_solicitacao;
    public $mdt_cad;
    public $mdt_ult;
    public $mnomeTabela;
    public $mnu_servico;
    public $mcd_usuario;
    public $mobservacao;
    public $mNO_SERVICO_RESUMIDO;
    public $mid_status_sol;

    private $servico;
    
    public function get_nu_processo() {
        return '(n/a)';
    }
    /**
     * 
     * @return cSERVICO
     */
    public function get_servico() {
        if (is_object($this->servico)) {
            return $this->servico;
        } else {
            if ($this->mnu_servico > 0) {
                $this->servico = new cSERVICO();
                $this->servico->RecuperePeloId($this->mnu_servico);
            } else {
                $this->servico = null;
            }
        }
        return $this->servico;
    }

    /**
     * Salva os dados do processo na OS. DEVE TER IMPLEMENTACAO ESPECIFICA NO PROCESSO_MTE POR CONTA DO VISTO ATUAL.
     * @param cORDEMSERVICO $pOS
     * @param cCANDIDATO $pCand
     */
    public function SalvarDadosOs($pOS, $pCand) {
        $sql = "select codigo ";
        $sql .= " from processo_" . $this->mnomeTabela;
        $sql .= " where id_solicita_visto =" . $pOS->mID_SOLICITA_VISTO;
        $sql .= "   and cd_candidato      =" . $pCand->mNU_CANDIDATO;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $rs = mysql_fetch_array($res);
        if (intval($rs['codigo']) > 0) {
            $this->mcodigo = $rs['codigo'];
            $this->mnu_servico = $pOS->mNU_SERVICO;
            $this->AtualizeServico();
        } else {
            $this->mcodigo = 0;
            $this->mid_solicita_visto = $pOS->mID_SOLICITA_VISTO;
            $this->mnu_servico = $pOS->mNU_SERVICO;
            $this->mcd_candidato = $pCand->mNU_CANDIDATO;
            if (intval($pCand->mcodigo_processo_mte_atual) == 0) {
                $pCand->CriaVistoAtualSeNaoHouver($pOS);
            }
            $this->mcodigo_processo_mte = $pCand->mcodigo_processo_mte_atual;
            $this->IncluaNovoDadosOS();
        }
    }

    public function RecupereSe() {
        $sql = "select * from " . $this->mnomeTabela . " where codigo = " . $this->mcodigo;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        cBANCO::CarreguePropriedades($rs, $this);
    }

    public function AtualizeServico() {
        $sql = " update processo_" . $this->mnomeTabela;
        $sql .= "   set nu_servico = " . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= "     , cd_usuario = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "     , dt_ult = now()";
        $sql .= "     , no_classe = " . cBANCO::StringOk(__CLASS__);
        $sql .= "     , no_metodo = " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function IncluaNovoDadosOS() {
        $sql = "insert into processo_" . $this->mnomeTabela . "( nu_servico, cd_candidato, id_solicita_visto, codigo_processo_mte, dt_cad, cd_usuario, dt_ult, no_classe, no_metodo) ";
        $sql.= "values (";
        $sql.= " " . cBANCO::ChaveOk($this->mnu_servico);
        $sql.= "," . $this->mcd_candidato;
        $sql.= "," . cBANCO::ChaveOk($this->mid_solicita_visto);
        $sql.= "," . cBANCO::ChaveOk($this->mcodigo_processo_mte);
        $sql.= ", now()";
        $sql.= ", " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql.= ", now()";
        $sql.= "," . cBANCO::StringOk(__CLASS__);
        $sql.= "," . cBANCO::StringOk(__FUNCTION__);
        $sql.= ")";
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mcodigo = mysql_insert_id();
    }

    public function VerificarCandidato() {
        if ($this->mcd_candidato == '' || $this->mcd_candidato == 'null') {
            $sql = "select cd_candidato from processo_mte where codigo = " . $this->mcodigo_processo_mte;
            $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            if ($rs = mysql_fetch_array($res)) {
                $this->mcd_candidato = $rs['cd_candidato'];
            } else {
                throw new Exception('Não foi possível encontrar o código do candidato para adicionar o processo', 'codigo_processo_mte=' . $this->mcodigo_processo_mte);
            }
        }
    }

    public function Salve_ProcessoMte() {
        $sql = "update processo_" . $this->mnomeTabela;
        $sql.= "   set codigo_processo_mte = " . cBANCO::ChaveOk($this->mcodigo_processo_mte);
        $sql .= "     , cd_usuario = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "     , dt_ult = now()";
        $sql .= "     , no_classe = " . cBANCO::StringOk(__CLASS__);
        $sql .= "     , no_metodo = " . cBANCO::StringOk(__FUNCTION__);
        $sql.= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public static function MigraProcessoEntreCandidatos($pcodigo, $ptabela, $pcd_candidato_orig, $pcd_candidato_dest, $pMigrarArquivos) {
        $cand_orig = new cCANDIDATO();
        $cand_orig->Recuperar($pcd_candidato_orig);
        $cand_dest = new cCANDIDATO();
        $cand_dest->Recuperar($pcd_candidato_dest);

        $sql = "select id_solicita_visto from " . $ptabela . " where codigo =" . $pcodigo;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        if ($rs = mysql_fetch_array($res)) {
            $id_solicita_visto = $rs['id_solicita_visto'];
        }

        $obs = "(" . date('d/m/y') . ") Processo migrado do candidato " . $cand_orig->mNOME_COMPLETO . " (" . $cand_orig->mNU_CANDIDATO . ") para o candidato " . $cand_dest->mNOME_COMPLETO . " (" . $cand_dest->mNU_CANDIDATO . ") - por " . cSESSAO::$mnome;

        // Migra
        if ($ptabela == 'processo_mte') {
            // Tranfere o processo de uma vez
            $sql = "update processo_mte ";
            $sql .= "   set cd_candidato = " . $pcd_candidato_dest;
            $sql .= "     , cd_usuario = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
            $sql .= "     , dt_ult = now()";
            $sql .= "     , no_classe = " . cBANCO::StringOk(__CLASS__);
            $sql .= "     , no_metodo = " . cBANCO::StringOk(__FUNCTION__);
            $sql .= " where codigo=" . $pcodigo;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            // Converte também todos os processos filhos
            self::MigrarFilhosDeUmMte($pcodigo, $pcd_candidato_orig, $pcd_candidato_dest, 'processo_regcie', $pMigrarArquivos, $obs);
            self::MigrarFilhosDeUmMte($pcodigo, $pcd_candidato_orig, $pcd_candidato_dest, 'processo_coleta', $pMigrarArquivos, $obs);
            self::MigrarFilhosDeUmMte($pcodigo, $pcd_candidato_orig, $pcd_candidato_dest, 'processo_prorrog', $pMigrarArquivos, $obs);
            self::MigrarFilhosDeUmMte($pcodigo, $pcd_candidato_orig, $pcd_candidato_dest, 'processo_emiscie', $pMigrarArquivos, $obs);
            self::MigrarFilhosDeUmMte($pcodigo, $pcd_candidato_orig, $pcd_candidato_dest, 'processo_cancel', $pMigrarArquivos, $obs);
            //
            // Se o processo é o visto atual do candidato origem, então zera o visto atual do origem
            if ($cand_orig->mcodigo_processo_mte_atual == $pcodigo) {
                $cand_orig->AtualizeVistoAtual();
            }
            //
            // Se o candidato destino não tem mte, então esse será o atual
            if ($cand_dest->mcodigo_processo_mte_atual == '' || intval($pcodigo) > intval($cand_dest->mcodigo_processo_mte_atual)) {
                $cand_dest->AtualizeVistoAtual($pcodigo);
            } else {
                // Reafirma o visto atual para ressetar os outros vistos como não atuais
                $cand_dest->AtualizeVistoAtual($cand_dest->mcodigo_processo_mte_atual);
            }
            if (intval($pMigrarArquivos) > 0 && $id_solicita_visto != '') {
                $this->MigrarSolicitaVisto($id_solicita_visto, $pcd_candidato_orig, $pcd_candidato_dest, $pMigrarArquivos, $obs);
            }
        } else {
            $sql = "update " . $ptabela;
            $sql .= "   set cd_candidato = " . $pcd_candidato_dest;
            $sql .= "     , codigo_processo_mte = " . cBANCO::ChaveOk($cand_dest->mcodigo_processo_mte_atual);
            $sql .= "     , cd_usuario = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
            $sql .= "     , dt_ult = now()";
            $sql .= "     , no_classe = " . cBANCO::StringOk(__CLASS__);
            $sql .= "     , no_metodo = " . cBANCO::StringOk(__FUNCTION__);
            $sql .= "     , observacao	= concat(observacao, '<br>" . $obs . "')";
            $sql .= " where codigo=" . $pcodigo;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            self::MigrarSolicitaVisto($id_solicita_visto, $pcd_candidato_orig, $pcd_candidato_dest, $pMigrarArquivos);
        }
        self::MigrarSolicitaVisto($id_solicita_visto, $pcd_candidato_orig, $pcd_candidato_dest);
    }

    public static function MigrarSolicitaVisto($pid_solicita_visto, $pcd_candidato_orig, $pcd_candidato_dest, $pMigrarArquivos = 0, $pObs = '') {
        if ($pid_solicita_visto != '') {
            $sql = "update AUTORIZACAO_CANDIDATO ";
            $sql.= "   set NU_CANDIDATO		 = " . $pcd_candidato_dest;
            $sql.= " where ID_SOLICITA_VISTO = " . $pid_solicita_visto;
            $sql.= "   and NU_CANDIDATO		 =" . $pcd_candidato_orig;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);

            if ($pObs != '') {
                $sql = "update solicita_visto
						set de_observacao = concat(ifnull(de_observacao, ''), '<br/>" . $pObs . "')
						where id_solicita_visto = " . $pid_solicita_visto;
                executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            }

            if (intval($pMigrarArquivos) > 0) {
                $sql = "update ARQUIVOS set NU_CANDIDATO=" . $pcd_candidato_dest . " WHERE ID_SOLICITA_VISTO=" . $id_solicita_visto . " and NU_CANDIDATO=" . $pcd_candidato_orig;
                executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            }
        }
    }

    public static function MigrarFilhosDeUmMte($pcodigo_mte, $pcd_candidato_orig, $pcd_candidato_dest, $ptabela, $pMigrarArquivos = 0, $pObs = '') {
        $sql = " select id_solicita_visto, codigo ";
        $sql .= "  from " . $ptabela;
        $sql .= " where codigo_processo_mte = " . $pcodigo_mte;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        while ($rs = mysql_fetch_array($res)) {
            $sql = "update " . $ptabela;
            $sql .= "   set cd_candidato = " . $pcd_candidato_dest;
            $sql .= "     , cd_usuario	= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
            $sql .= "     , dt_ult		= now()";
            $sql .= "     , no_classe	= " . cBANCO::StringOk(__CLASS__);
            $sql .= "     , no_metodo	= " . cBANCO::StringOk(__FUNCTION__);
            if ($pObs != '') {
                $sql .= "     , observacao	= concat(observacao, '<br>" . $pObs . "')";
            }
            $sql .= " where codigo       = " . $rs['codigo'];
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            self::MigrarSolicitaVisto($rs['id_solicita_visto'], $pcd_candidato_orig, $pcd_candidato_dest, $pMigrarArquivos);
        }
    }

    public function Migre(cCANDIDATO $pcandidato_origem, cCANDIDATO $pcandidato_destino) {
        $msg = "(" . date('d/m/y') . ") Processo transferido do candidato " . $pcandidato_origem->mNOME_COMPLETO . " (" . $pcandidato_origem->mNU_CANDIDATO . ") para o candidato " . $pcandidato_destino->mNOME_COMPLETO . " (" . $pcandidato_destino->mNU_CANDIDATO . ") - por " . cSESSAO::$mnome;
        if ($this->mobservacao == '') {
            $this->mobservacao = $msg;
        } else {
            $this->mobservacao .= '<br/>' . $msg;
        }

        $sql = "update processo_" . $this->mnomeTabela;
        $sql .= "   set cd_candidato = " . $pcandidato_destino->mNU_CANDIDATO;
        $sql .= "     , codigo_processo_mte = " . cBANCO::ChaveOk($pcandidato_destino->mcodigo_processo_mte_atual);
        $sql .= "     , cd_usuario	= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "     , dt_ult		= now()";
        $sql .= "     , no_classe	= " . cBANCO::StringOk(__CLASS__);
        $sql .= "     , no_metodo	= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= "     , observacao	= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " where codigo       = " . $this->mcodigo;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function MigrePeloProcessoMte(cCANDIDATO $pcandidato_origem, cCANDIDATO $pcandidato_destino, $pcodigo_mte) {
        $msg = "(" . date('d/m/y') . ") Processo transferido do candidato " . $pcandidato_origem->mNOME_COMPLETO . " (" . $pcandidato_origem->mNU_CANDIDATO . ") para o candidato " . $pcandidato_destino->mNOME_COMPLETO . " (" . $pcandidato_destino->mNU_CANDIDATO . ") - por " . cSESSAO::$mnome;
        if ($this->mobservacao == '') {
            $this->mobservacao = $msg;
        } else {
            $this->mobservacao .= '<br/>' . $msg;
        }

        $sql = "update processo_" . $this->mnomeTabela;
        $sql .= "   set cd_candidato = " . $pcandidato_destino->mNU_CANDIDATO;
        $sql .= "     , codigo_processo_mte	= " . cBANCO::ChaveOk($pcandidato_destino->mcodigo_processo_mte_atual);
        $sql .= "     , cd_usuario	= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "     , dt_ult		= now()";
        $sql .= "     , no_classe	= " . cBANCO::StringOk(__CLASS__);
        $sql .= "     , no_metodo	= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= "     , observacao	= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " where codigo_processo_mte = " . $pcodigo_mte;
        $sql .= "   and cd_candidato        = " . $pcandidato_origem->mNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function RecupereSePelaOs($pID_SOLICITA_VISTO, $pNU_CANDIDATO) {
        $this->mid_solicita_visto = $pID_SOLICITA_VISTO;
        $this->mcd_candidato = $pNU_CANDIDATO;

        if (!intval($this->mid_solicita_visto) > 0) {
            throw new Exception("Código da OS não informado");
        }
        $sql = "select ps.*, s.NO_SERVICO_RESUMIDO from processo_" . $this->mnomeTabela . " ps";
        $sql.= " left join servico s on s.nu_servico = ps.nu_servico";
        $sql.= " where id_solicita_visto = " . $this->mid_solicita_visto;
        $sql.= "   and cd_candidato      = " . $this->mcd_candidato;
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->mnu_servico = $rs["nu_servico"];
                $this->mcodigo = $rs["codigo"];
                $this->mcodigo_processo_mte = $rs["codigo_processo_mte"];
                $this->mNO_SERVICO_RESUMIDO = $rs["NO_SERVICO_RESUMIDO"];
                cBANCO::CarreguePropriedades($rs, $this);
            }
        }
    }

    public function RecupereSePelaOsSemCand($pID_SOLICITA_VISTO) {
        $this->mid_solicita_visto = $pID_SOLICITA_VISTO;

        if (!intval($this->mid_solicita_visto) > 0) {
            throw new Exception("Código da OS não informado");
        }
        $sql = "select ps.*, s.NO_SERVICO, s.NO_SERVICO_RESUMIDO , sv.id_status_sol "
                . " from processo_" . $this->mnomeTabela . " ps";
        $sql.= " left join servico s on s.nu_servico = ps.nu_servico";
        $sql.= " join solicita_visto sv on sv.id_solicita_visto = ps.id_solicita_visto";
        $sql.= " where ps.id_solicita_visto = " . $this->mid_solicita_visto;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            if ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
                $this->mnu_servico = $rs["nu_servico"];
                $this->mcodigo = $rs["codigo"];
                $this->mcodigo_processo_mte = $rs["codigo_processo_mte"];
                $this->mNO_SERVICO_RESUMIDO = $rs["NO_SERVICO_RESUMIDO"];
                cBANCO::CarreguePropriedades($rs, $this);
            }
        }
    }

    public function JSON_AtributosProcessoOs($pID_SOLICITA_VISTO) {
        $saida = array();
        $this->RecupereSePelaOsSemCand($pID_SOLICITA_VISTO);
        $saida['processo'] = get_object_vars($this);
        $os = new cORDEMSERVICO();
        $os->Recuperar($pID_SOLICITA_VISTO);
        $saida['os'] = get_object_vars($os);
    }

    public function Exclua() {
        $sql = "delete from processo_" . $this->mnomeTabela;
        $sql.= " where codigo = " . $this->mcodigo;
        cAMBIENTE::$db_pdo->exec($sql);
        //:TODO Dar um jeito para registrar a exclusao no log.
    }

    public function ExcluaPelaOs($pID_SOLICITA_VISTO) {
        $sql = "delete from processo_" . $this->mnomeTabela;
        $sql.= " where id_solicita_visto = " . $pID_SOLICITA_VISTO;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Atualizar a observação do processo
     */
    public function AtualizeObs() {
        $sql = "update processo_" . $this->mnomeTabela;
        $sql .= "   set observacao = " . cBANCO::StringOk($this->mobservacao);
        $sql .= "     , cd_usuario = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "     , dt_ult = now()";
        $sql .= "     , no_classe = " . cBANCO::StringOk(__CLASS__);
        $sql .= "     , no_metodo = " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AdicionarObservacao($pObservacao) {
        if (intval($this->mcodigo) == 0) {
            throw new cERRO_CONSISTENCIA('Impossível atualizar processo_' . $this->mnomeTabela . ' sem a chave');
        }
        $sql = "update processo_" . $this->mnomeTabela;
        $sql .= "   set observacao = concat(observacao, '" . $pObservacao . "') ";
        $sql .= "     , cd_usuario = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "     , dt_ult = now()";
        $sql .= "     , no_classe = " . cBANCO::StringOk(__CLASS__);
        $sql .= "     , no_metodo = " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mobservacao .= $pObservacao;
    }

    public function LogaExclusao() {
        
    }

    public static function FabricaProcesso($pID_TIPO_ACOMPANHAMENTO) {
        switch ($pID_TIPO_ACOMPANHAMENTO) {
            case 1:
                $processo = new cprocesso_mte();
                break;
            case 3:
                $processo = new cprocesso_prorrog();
                break;
            case 4:
                $processo = new cprocesso_regcie();
                break;
            case 5:
                $processo = new cprocesso_emiscie();
                break;
            case 6:
                $processo = new cprocesso_cancel();
                break;
            case 8:
                $processo = new cprocesso_coleta();
                break;
            case 13:
                $processo = new cprocesso_mudanca_embarcacao();
                break;
            default:
                break;
        }
        return $processo;
    }

    public static function FabricaProcessoPeloHistorico($pcodigo, $ptipo) {
        $processo = null;
        switch ($ptipo) {
            case 'mte':
                $processo = new cprocesso_mte();
                break;
            case 'prorrog':
                $processo = new cprocesso_prorrog();
                break;
            case 'regcie':
                $processo = new cprocesso_regcie();
                break;
            case 'emiscie':
                $processo = new cprocesso_emiscie();
                break;
            case 'cancel':
                $processo = new cprocesso_cancel();
                break;
            case 'coleta':
                $processo = new cprocesso_coleta();
                break;
            case 'generico':
                $processo = new cprocesso_generico();
                break;
            default:
                break;
        }
        if (is_object($processo)) {
            if (property_exists($processo, 'mcodigo')){
                $processo->mcodigo = $pcodigo;
            $processo->RecupereSe();
            } else {
                $processo->codigo = $pcodigo;
                $processo->RecuperePeloId();
            }
        }
        return $processo;
    }

    /**
     *
     * @param cORDEMSERVICO $pOS
     * @return cprocesso Processo correspondente ao serviço da OS
     */
    public static function FabricaProcessoDaOs(cORDEMSERVICO $pOS) {
        $processo = self::FabricaProcesso($pOS->mID_TIPO_ACOMPANHAMENTO);
        if (property_exists($processo, 'mid_solicita_visto')) {
            $processo->mid_solicita_visto = $pOS->mID_SOLICITA_VISTO;
        } else {
            $processo->id_solicita_visto = $pOS->mID_SOLICITA_VISTO;
        }
        return $processo;
    }

    public function RegistraDtEnvioBSB($pid_solicita_visto, $pdt_envio_bsb) {
        throw new cERRO_CONSISTENCIA("Registro de envio para BSB não implementado para esse tipo de serviço");
    }

    public function Salve_id_solicita_visto() {
        $sql = "update processo_" . $this->mnomeTabela;
        $sql.= "   set id_solicita_visto = " . cBANCO::ChaveOk($this->mid_solicita_visto);
        $sql .= "     , cd_usuario = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "     , dt_ult = now()";
        $sql .= "     , no_classe = " . cBANCO::StringOk(__CLASS__);
        $sql .= "     , no_metodo = " . cBANCO::StringOk(__FUNCTION__);
        $sql.= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function ZeraDtEnvioBSB($pid_solicita_visto) {
        $sql = "update processo_" . $this->mnomeTabela . " set dt_envio_bsb = null where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function SituacaoVistoProcesso(&$pprazo_estada, &$ptexto) {
        throw new Exception("Tentativa de obter situacao do visto de um processo nao instanciado");
    }

    /**
     * Indica se a OS do processo pode ser fechada baseado na situação dos atributos.
     * Deve ser overridado para funcionar para cada processo
     */
    public function PodeFechar() {
        return false;
    }

    public function atualizaOrdem($ordem){
        $sql = "update processo_" . $this->mnomeTabela;
        $sql.= "   set ordem = " . $ordem;
        $sql.= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }
}
