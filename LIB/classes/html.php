<?php

/**
 * @author Fabio
 * @copyright 2010
 */


    Class html extends AppRequest{
        
        protected $attr = array();
        
        public $cache_submit = false, $name = null;
        
        protected $AttrStrEspecial = array('checked','posted');
        
        
        function str_attr($attrs){
           
           $str_attr = null;
           //$this->attr = $attrs;
           
           if( (!is_null($attrs)) AND (is_array($attrs)) ){
            
                foreach($attrs as $name=>$val){
                    
                    $str_attr .= ' '.$name.'="'.$val.'" ';    
                }
                
                $this->attr_str = $str_attr;
            }
            return $str_attr;
            
        }
        
        
        function Attr($attrs){

           if( (!is_null($attrs)) AND (is_array($attrs)) ){

                foreach($attrs as $name=>$val){
                   if($name == 'name') {
                        $this->name  = $val;
                        if($this->p_array($val)){
                            $this->attr[$this->name]['posted'] = 1;
                        }else{
                            $this->attr[$this->name]['posted'] = 0; 
                        }  
                   }
                   $this->attr[$this->name][$name] = $val;  
                } 
            }
            
            if(($this->type) AND ($this->name) ){
                $this->attr[$this->name]['type'] = $this->type;
            }
            
            //return $this->attr[$this->name];
            
        }
        
        /**
         *  Obtem TODOS os atributos do objeto HTML
         * 
         * */        
        function GetAtributos(){
             
                          
            $str_attr = null;
            foreach($this->attr[$this->name] as $p => $val){
                $str_attr .= $this->AtributoInString($p, $val);
            }
            return $str_attr;
            

        }
        
        /**
         *  Gera os atributos em formato Html. exemplo: class="green"
         * */
        function AtributoInString($name, $value){
            
            $res = null;
           
            if(in_array($name, $this->AttrStrEspecial)){
               $res = $this->getAtributoStrEspecial($name, $value);
            }else{
               $res = ' '.$name.'="'.$value.'" ';  // Forma Padrão
            }
            
           
            return $res;
        }


        /**
        *  Gera os atributos (especiais) em formato Html. exemplo: "checked"
        **/
        function getAtributoStrEspecial($name, $value){
            $res = null;
            
            // checked
            if($name == 'checked'){
                if($value){
                    
                    $res = ' checked="true" ';  //checked
                }
            }
            
              
            //posted
            if($name == 'posted'){
                    $res = '';   
            }
            
            return $res;
        }

        

        
        
        function input($type, $attrs = null, $attr_return = null){
            
            $this->type = $type;
            $this->Attr($attrs);
            
           if($this->cache_submit){ // Configuração para obter o cache (é o $_Post com o mesmo nome do campo)     
                $this->ConfiguracaoCacheInput();
            }
            
            return '<input type="'.$this->type.'" '.$this->GetAtributos().' />';
            
        }
        
        function edit($type, $attrs = null, $attr_return = null){
            return $this->input($type, $attrs = null, $attr_return = null);
        }
        
        function dbedit($field, $ds, $attrs=null){
  
            $this->type = 'text';
            $this->name = $field;
            $this->Attr($attrs);
            
            // propriedades do objeto
            $this->attr[$this->name]['type']  = $this->type;
            $this->attr[$this->name]['name']  = $field; 
             
           // if(isset($ds[$field])<>''){
           //     $this->attr[$this->name]['value'] = $ds[$field]; 
            //} 
    
    
            //novo
            $this->attr[$this->name]['value'] = $ds;
            
            return '<input '.$this->GetAtributos().' />';         

            
        }
        
        
        function dbtextarea($field, $ds, $attrs = null){
            
            $this->type = 'textarea';
            $this->Attr($attrs);
            
            // propriedades do objeto
            $this->attr[$this->name]['name']  = $field; 
             
            if(isset($ds[$field])<>''){
                $this->attr[$this->name]['value'] = $ds[$field]; 
            } 
            
            return '<textarea '.$this->GetAtributos().'></textarea>';            
        }
        
        
        function dbcombo($name, $options, $cod_checked = null, $attrs=null, $notnull=null){
            
            $res   = '';
            
            $attrs = $this->str_attr($attrs);

            if(count($options)>0){
                
                $res   = '<select name="'.$name.'" '.$attrs.'>';
                if(!$notnull){
                    $res .= '<option value="">----</option>';
                }
                
                foreach($options as $option){
                    
                    $check = '';
                    
                    if($option['COD'] == $cod_checked){
                       $check = 'selected ';
                    }
                    
                    $res .= '<option '.$check.'value="'.$option['COD'].'">'.$option['DESCR'].'</option>';
                    
                }
                
                $res .= '</select>';
            }           
            
            return $res;
        }
        
        
        
        
        
        /**
         *  Configuração dos campos input que foram enviados por POST.
         *  Pois cada um tem uma particularidade. 
         * 
         **/ 
        function ConfiguracaoCacheInput(){
            
            // checkbox
            if($this->type == 'checkbox'){
                if($this->attr[$this->name]['posted']){
                    $this->attr[$this->name]['checked'] = 1;
                }else{
                    if(!$this->attr[$this->name]['checked']){
                        $this->attr[$this->name]['checked'] = 0;
                    }
                } 
            }
            
            // text
            if($this->type == 'text'){
                if(!$this->attr[$this->name]['posted']){
                    $this->attr[$this->name]['value'] = '';
                }else{
                    $this->attr[$this->name]['value'] = $this->p($this->name);
                } 
            }            
        }

   

   
   
   
   
   
   
   
        
        
 /* 
 
         
        function input_test($type, $attrs = null, $return = null){
            
            if(!$this->cache_submit){ // não obtendo o cache (é o $_Post com o mesmo nome do campo)
                $this->str_attr($attrs);
                return $this->input_type($type);  // obtem o html input_typr
            }else{
                return $this->p_input($type, $attrs, $return);  // obtem o html inout type verificando se tem campos $_post
            }
        }
        
 
       
        
        function input_type($type){
            $addon = null;
            $str_inputtype = null;
            $res = null;
            
            if($type == 'checkbox'){
                
                if($this->name <> ''){
                    $addon = ' checked="true" ';
                }
                
                $res = '<input type="'.$type.'"'.$this->attr_str.' '.$addon.' />';
                
            }
            
            if($type == 'text'){
            
                $res = '<input type="'.$type.'"'.$this->attr_str. ' />';  
            }
            
            if($type == 'submit'){
            
                $res = '<input type="'.$type.'"'.$this->attr_str. ' />';  
            }
                       
            return $res;
            
        }
        
        
        
        function p_input($type, $attrs, $return = null){
            $this->attr = null;
            $this->attr_str =null;
            $this->name = null;
            $this->p_attr($attrs);
            
            if($this->name){ // se encontrou o POST deste objeto ...
            
                if( (!is_null($return)) AND (is_array($return)) ){
                     foreach($return as $name=>$val){
                        $this->attr_str .= ' '.$name.'="'.$val.'" ';
                    }               
                }
            }

            return $this->input_type($type);
            
        }
        
        
        
        function p_attr($attrs){
           
           $str_attr = null;
           $this->attr = $attrs;

           if( (!is_null($attrs)) AND (is_array($attrs)) ){
            
                foreach($attrs as $name=>$val){
                    if($name == 'name'){
                        if($this->p_array($val)){
                                $this->name = $val;       
                        }    
                    }
                    if($name == 'checked'){
                        if(!$this->name){
                            $name = null;
                            $val  = null;
                        }
                    }

                    
                    $str_attr .= ' '.$name.'="'.$val.'" ';
                      
                }
                
                $this->attr_str = $str_attr;
            }
            
            return $str_attr;
            
        }
        
  */      
        function p_array($val){
            
            if($_POST[$val]){
                return $_POST[$val];
            }else{
                foreach($_POST as $p=>$v){
                    if(is_array($v)){
                        $a = str_replace('[', '', str_replace(']', '',strstr($val, '[')));                     
                        if($v[$a]<>''){
                            return $v[$a];
                        }
                    }
                }    
            }
        }
        
        
        
        
    }


?>
