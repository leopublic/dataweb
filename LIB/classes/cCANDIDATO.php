<?php

/**
 * Estrangeiro registrado no sistema
 * @author Leonardo Medeiros
 */
class cCANDIDATO extends cCANDIDATO_BASE {

    // Propriedades exclusivas do candidato
    public $mcodigo_processo_mte_atual;
    public $mFL_INATIVO;
    public $mNO_LOGIN;
    public $mFL_EMBARCADO;
    public $mTE_OBSERVACAO_CREWLIST;
    public $mNU_ORDEM_CREWLIST;
    public $mcd_usuario_inativacao;
    public $mcd_usuario_sol_exclusao;
    public $mcd_usuario_revisao;
    public $mcd_usuario_conferencia;
    public $mcd_usuario_ativacao;
    public $mcand_fl_excluir;
    public $mcand_fl_revisado;
    public $mcand_fl_conferido;
    public $mcand_dt_inativacao;
    public $mcand_dt_sol_exclusao;
    public $mcand_dt_revisao;
    public $mcand_dt_conferencia;
    public $mcand_dt_ativacao;
    public $mlog_tx_controller;
    public $mlog_tx_metodo;
    public $mempt_id;
    public $mnu_candidato_parente;
    public $mco_grau_parentesco;
    public $mcand_dt_ult_crewlist;
    public $mcd_usuario_ult_crewlist;
    public $mcand_fl_pit;

    public function getid() {
        return $this->mNU_CANDIDATO;
    }

    public function setId($pValor) {
        $this->mNU_CANDIDATO = $pValor;
    }

    public function get_nomeTabela() {
        return "CANDIDATO";
    }

    public static function get_nomeCampoChave() {
        return 'NU_CANDIDATO';
    }

    public function CriarNomeCompleto($pNO_NOME_COMPLETO, $pNU_USUARIO_CAD) {
        $this->QuebreNomeCompleto($pNO_NOME_COMPLETO);
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);
        $sql = "select * from CANDIDATO where NOME_COMPLETO = '" . str_replace("'", "''", $this->mNOME_COMPLETO) . "'";
        if ($res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__)) {
            if ($rs = mysql_fetch_array($res)) {
                return 'Já existe um candidato cadastrado com esse nome. Verifique.|@@erro';
            } else {
                $x = $this->Criar($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME, $pNU_USUARIO_CAD);
                return $x;
            }
        }
    }

    /**
     * Converte dependentes de um candidato em novos candidatos
     * @param type $pNU_CANDIDATO
     */
    public function CadastraDependentes($pNU_CANDIDATO) {
        $deps = cdependente::CursorDependentesDeCandidato($pNU_CANDIDATO);
        $empresa = new cEMPRESA();
        $empresa->mNU_EMPRESA = $this->mNU_EMPRESA;
        $empresa->RecuperePeloId($pid);
        $ret = '';
        $ret .= '<br/>*=============================================';
        $ret .= '<br/>* Processando dependentes de ' . $this->mNOME_COMPLETO . ' (' . $this->mNU_CANDIDATO . ') - ' . $empresa->mNO_RAZAO_SOCIAL;
        while ($rs = $deps->fetch(PDO::FETCH_ASSOC)) {
            $cand = new cCANDIDATO();
            $cand->RecuperesePeloNomeSo($rs['no_dependente']);
            if ($cand->mNU_CANDIDATO != '') {
                $ret .= '<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dependente ' . $rs['no_dependente'] . ' de ' . $this->mNOME_COMPLETO . ' já estava cadastrado com o código ' . $cand->mNU_CANDIDATO;
            } else {
                $ret .= '<br/>&nbsp;&nbsp;&nbsp;&nbsp;Dependente ' . $rs['no_dependente'] . ' de ' . $this->mNOME_COMPLETO . ' adicionado como novo candidato';
                $cand->QuebreNomeCompleto($rs['no_dependente']);
                $cand->mNOME_COMPLETO = $rs['no_dependente'];
                $cand->mNU_CANDIDATO = 0;
            }
            $cand->mnu_candidato_parente = $this->mNU_CANDIDATO;
            $cand->mco_grau_parentesco = $rs['co_grau_parentesco'];
            $cand->mDT_NASCIMENTO = $rs['dt_nascimento'];
            $cand->mNU_PASSAPORTE = $rs['nu_passaporte'];
            $cand->mDT_VALIDADE_PASSAPORTE = $rs['dt_validade_passaporte'];
            $cand->mCO_NACIONALIDADE = $rs['co_nacionalidade'];
            $cand->mCO_PAIS_EMISSOR_PASSAPORTE = $rs['co_pais_emissor_passaporte'];
            $cand->AtualizaComoParente();
        }
        return $ret;
    }

    /**
     * Permite a criação inicial de um candidato durante a inclusão de OS.
     * @param string $pNO_PRIMEIRO_NOME
     * @param string $pNO_NOME_MEIO
     * @param string $pNO_ULTIMO_NOME
     * @param string $pNU_USUARIO_CAD
     * @throws Exception Erro na inclusao
     */
    public function Criar($pNO_PRIMEIRO_NOME, $pNO_NOME_MEIO, $pNO_ULTIMO_NOME, $pNU_USUARIO_CAD) {
        $this->mNO_PRIMEIRO_NOME = $pNO_PRIMEIRO_NOME;
        $this->mNO_NOME_MEIO = $pNO_NOME_MEIO;
        $this->mNO_ULTIMO_NOME = $pNO_ULTIMO_NOME;
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto(trim($pNO_PRIMEIRO_NOME), trim($pNO_NOME_MEIO), trim($pNO_ULTIMO_NOME));
        $sql = "INSERT INTO CANDIDATO (";
        $sql .= "  NO_PRIMEIRO_NOME";
        $sql .= " ,NO_NOME_MEIO";
        $sql .= " ,NO_ULTIMO_NOME";
        $sql .= " ,NOME_COMPLETO";
        $sql .= " ,DT_CADASTRAMENTO";
        $sql .= " ,CO_USU_CADASTAMENTO";
        $sql .= " ,CO_USU_ULT_ALTERACAO";
        $sql .= ") VALUES (";
        $sql .= cBANCO::StringOk($pNO_PRIMEIRO_NOME);
        $sql .= "," . cBANCO::StringOk($pNO_NOME_MEIO);
        $sql .= "," . cBANCO::StringOk($pNO_ULTIMO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= ", now()";
        $sql .= "," . cBANCO::StringOk($pNU_USUARIO_CAD);
        $sql .= "," . cBANCO::StringOk($pNU_USUARIO_CAD);
        $sql .= ")";
        if (mysql_query($sql)) {
            $this->mNU_CANDIDATO = mysql_insert_id();
            $this->GeraLoginParaNovoCandidato();
            return 'Candidato adicionado com sucesso.|' . $this->mNU_CANDIDATO;
        } else {
            throw new Exception(self::mEscopo . '.Criar-> Erro:' . mysql_error() . '<br/>SQL="' . $sql . '"');
        }
    }

    public function RecuperePeloId($pNU_CANDIDATO = '') {
        $this->Recuperar($pNU_CANDIDATO);
    }

    /**
     * Recupera campos do candidato
     */
    public function Recuperar($pNU_CANDIDATO = '') {
        if ($pNU_CANDIDATO != '') {
            $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        }
        if ($this->mNU_CANDIDATO == '') {
            throw new Exception("ID do candidato não informado");
        }
        $sql = "select c.*
					 , uc.nome as nome_cad
					 , ua.nome as nome_alt
				from CANDIDATO c
				left join usuarios uc on uc.cd_usuario = c.CO_USU_CADASTAMENTO
				left join usuarios ua on ua.cd_usuario = c.CO_USU_ULT_ALTERACAO
				where NU_CANDIDATO=" . $this->mNU_CANDIDATO;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            if ($rs = $res->fetch(PDO::FETCH_BOTH)) {
                cBANCO::CarreguePropriedades($rs, $this);
                $this->mDT_NASCIMENTO = cBANCO::DataFmt($rs["DT_NASCIMENTO"]);
                $this->mDT_EMISSAO_PASSAPORTE = cBANCO::DataFmt($rs["DT_EMISSAO_PASSAPORTE"]);
                $this->mDT_VALIDADE_PASSAPORTE = cBANCO::DataFmt($rs["DT_VALIDADE_PASSAPORTE"]);
                $this->mDT_EMISSAO_SEAMAN = cBANCO::DataFmt($rs["DT_EMISSAO_SEAMAN"]);
                $this->mDT_VALIDADE_SEAMAN = cBANCO::DataFmt($rs["DT_VALIDADE_SEAMAN"]);
                $this->mDT_EXPIRACAO_CNH = cBANCO::DataFmt($rs["DT_EXPIRACAO_CNH"]);
                $this->mDT_EXPIRACAO_CTPS = cBANCO::DataFmt($rs["DT_EXPIRACAO_CTPS"]);
                $this->mDT_VALIDADE_PASSAPORTE = cBANCO::DataFmt($rs["DT_VALIDADE_PASSAPORTE"]);
            }
        }
    }

    public function CursorIdentificacaoCandidato($pNU_CANDIDATO) {
        if ($pNU_CANDIDATO != '') {
            $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        }
        if ($this->mNU_CANDIDATO == '') {
            throw new Exception("ID do candidato não informado");
        }
        $sql = "select c.*
					 , uc.nome as nome_cad
					 , ua.nome as nome_alt
				from CANDIDATO c
				left join usuarios uc on uc.cd_usuario = c.CO_USU_CADASTAMENTO
				left join usuarios ua on ua.cd_usuario = c.CO_USU_ULT_ALTERACAO
				where NU_CANDIDATO=" . $this->mNU_CANDIDATO;
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    /**
     * Recupera campos do candidato
     */
    public function Recuperar_Externo($pNU_CANDIDATO = '') {
        if ($pNU_CANDIDATO != '') {
            $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        }
        if ($this->mNU_CANDIDATO == '') {
            throw new Exception("ID do candidato não informado");
        }
        $this->DisponibilizeCadastroParaCliente();

        $sql = "select * from vcandidato_tmp_cons where nu_candidato_tmp=" . $this->mnu_candidato_tmp;
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                cBANCO::CarreguePropriedades($rs, $this);
            } else {

            }
        } else {

        }
    }

    public function AtualizarDR() {
        $sql = "insert into dr.candidato(
					NO_PRIMEIRO_NOME
					,NO_NOME_MEIO
					,NO_ULTIMO_NOME
					,NOME_COMPLETO
					,NO_EMAIL_CANDIDATO
					,NO_ENDERECO_RESIDENCIA
					,NO_CIDADE_RESIDENCIA
					,CO_PAIS_RESIDENCIA
					,NU_TELEFONE_CANDIDATO
					,NO_EMPRESA_ESTRANGEIRA
					,NO_ENDERECO_EMPRESA
					,NO_CIDADE_EMPRESA
					,CO_PAIS_EMPRESA
					,NU_TELEFONE_EMPRESA
					,NO_PAI
					,NO_MAE
					,CO_NACIONALIDADE
					,CO_NIVEL_ESCOLARIDADE
					,CO_ESTADO_CIVIL
					,CO_SEXO
					,DT_NASCIMENTO
					,NO_LOCAL_NASCIMENTO
					,NU_PASSAPORTE
					,DT_EMISSAO_PASSAPORTE
					,DT_VALIDADE_PASSAPORTE
					,CO_PAIS_EMISSOR_PASSAPORTE
					,NU_RNE
					,CO_PROFISSAO_CANDIDATO
					,TE_DESCRICAO_ATIVIDADES
					,TE_TRABALHO_ANTERIOR_BRASIL
					,NU_CPF
					,DT_CADASTRAMENTO
					,NO_ENDERECO_ESTRANGEIRO
					,NO_CIDADE_ESTRANGEIRO
					,CO_PAIS_ESTRANGEIRO
					,NU_TELEFONE_ESTRANGEIRO
					,VA_REMUNERACAO_MENSAL
					,VA_REMUNERACAO_MENSAL_BRASIL
					,CO_PAIS_SEAMAN
					,DT_EMISSAO_SEAMAN
					,DT_VALIDADE_SEAMAN
					,NO_SEAMAN
					,CO_FUNCAO
					,loca_id
					,NO_PROJETO_EMBARCACAO
					,co_pais_nacionalidade_pai
					,co_pais_nacionalidade_mae
				) values (";
        $sql .= "  " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);   //ok
        $sql .= " , " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
        $sql .= " , " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);  //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok

        $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_PAI); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_MAE); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL); //ok
        $sql .= " , " . cBANCO::StringOk($this->mCO_SEXO); //ok

        $sql .= " , " . cBANCO::DataOk($this->mDT_NASCIMENTO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_PASSAPORTE); //ok
        $sql .= " , " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE); //ok
        $sql .= " , " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_RNE);
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES); //ok
        $sql .= " , " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok

        $sql .= " , " . cBANCO::StringOk($this->mNU_CPF);      //ok
        $sql .= " , now()";
        $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL); //ok
        $sql .= " , " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL_BRASIL); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_SEAMAN); //ok
        $sql .= " , " . cBANCO::DataOk($this->mDT_EMISSAO_SEAMAN); //ok

        $sql .= " , " . cBANCO::DataOk($this->mDT_VALIDADE_SEAMAN); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_SEAMAN); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_FUNCAO); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_LOCAL_EMBARCACAO_PROJETO); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mNO_EMBARCACAO_PROJETO); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae); //ok
        $sql .= ")";
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $this->mnu_candidato_tmp = mysql_insert_id();
    }

    /**
     * Atualiza atributos da instância no banco
     */
    public function Atualizar() {
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);

        $sql = "update CANDIDATO SET ";
        $sql .= "   NU_EMPRESA					= " . cBANCO::StringOk($this->mNU_EMPRESA);   //ok
        $sql .= " , NU_EMBARCACAO_PROJETO		= " . cBANCO::StringOk($this->mNU_EMBARCACAO_PROJETO);   //ok
        $sql .= " , NO_PRIMEIRO_NOME 			= " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
        $sql .= " , NO_NOME_MEIO 				= " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
        $sql .= " , NO_ULTIMO_NOME 				= " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);   //ok
        $sql .= " , NOME_COMPLETO 				= " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
        $sql .= " , NO_EMAIL_CANDIDATO 			= " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);  //ok
        $sql .= " , NO_ENDERECO_RESIDENCIA 		= " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
        $sql .= " , NO_CIDADE_RESIDENCIA 		= " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
        $sql .= " , CO_PAIS_RESIDENCIA 			= " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA);  //ok
        $sql .= " , NU_TELEFONE_CANDIDATO 		= " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
        $sql .= " , NU_TELEFONE_CANDIDATO_CEL	= " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO_CEL); //ok
        $sql .= " , NO_EMPRESA_ESTRANGEIRA		= " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok
        $sql .= " , NO_ENDERECO_EMPRESA 		= " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA);  //ok
        $sql .= " , NO_CIDADE_EMPRESA 			= " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA);  //ok
        $sql .= " , CO_PAIS_EMPRESA 			= " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA);   //ok
        $sql .= " , NU_TELEFONE_EMPRESA 		= " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA);  //ok
        $sql .= " , NO_PAI 						= " . cBANCO::StringOk($this->mNO_PAI);     //ok
        $sql .= " , NO_MAE 						= " . cBANCO::StringOk($this->mNO_MAE);     //ok
        $sql .= " , CO_NACIONALIDADE 			= " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE);   //ok
        $sql .= " , CO_NIVEL_ESCOLARIDADE 		= " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE);  //ok
        $sql .= " , CO_ESTADO_CIVIL 			= " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL);   //ok
        $sql .= " , CO_SEXO 					= " . cBANCO::StringOk($this->mCO_SEXO);     //ok
        $sql .= " , DT_NASCIMENTO 				= " . cBANCO::DataOk($this->mDT_NASCIMENTO);    //ok
        $sql .= " , NO_LOCAL_NASCIMENTO 		= " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO);  //ok
        $sql .= " , NU_PASSAPORTE 				= " . cBANCO::StringOk($this->mNU_PASSAPORTE);   //ok
        $sql .= " , DT_EMISSAO_PASSAPORTE 		= " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE);  //ok
        $sql .= " , DT_VALIDADE_PASSAPORTE 		= " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE);  //ok
        $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE 	= " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
        $sql .= " , NU_RNE 						= " . cBANCO::StringOk($this->mNU_RNE);
        $sql .= " , CO_PROFISSAO_CANDIDATO 		= " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
        $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL = " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok
        $sql .= " , NU_CPF 						= " . cBANCO::StringOk($this->mNU_CPF);     //ok
        $sql .= " , DT_ULT_ALTERACAO 			= now()";
        $sql .= " , CO_USU_ULT_ALTERACAO 		= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , NO_ENDERECO_ESTRANGEIRO 	= " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
        $sql .= " , NO_CIDADE_ESTRANGEIRO 		= " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
        $sql .= " , CO_PAIS_ESTRANGEIRO 		= " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO);  //ok
        $sql .= " , NU_TELEFONE_ESTRANGEIRO 	= " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
        $sql .= " , NU_CNH 						= " . cBANCO::StringOk($this->mNU_CNH);
        $sql .= " , DT_EXPIRACAO_CNH			= " . cBANCO::DataOk($this->mDT_EXPIRACAO_CNH);
        $sql .= " , NU_CTPS						= " . cBANCO::StringOk($this->mNU_CTPS);
        $sql .= " , DT_EXPIRACAO_CTPS			= " . cBANCO::DataOk($this->mDT_EXPIRACAO_CTPS);
        $sql .= " , empt_id						= " . cBANCO::ChaveOk($this->mempt_id);
        $sql .= " , FL_INATIVO					= " . cBANCO::SimNaoOk($this->mFL_INATIVO);
        $sql .= " , FL_EMBARCADO				= " . cBANCO::SimNaoOk($this->mFL_EMBARCADO);
        $sql .= " , co_pais_nacionalidade_pai	= " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai);
        $sql .= " , co_pais_nacionalidade_mae	= " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae);
        $sql .= " where NU_CANDIDATO 			= " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->VerifiqueCadastroCompleto();
    }


    public function salveTodosAtributos(){
        if (intval($this->mNU_CANDIDATO) > 0){
            $this->AtualizarTodosAtributos();
        } else {
            $this->InserirTodosAtributos();
        }
    }
    /**
     * Atualiza atributos da instância no banco
     */
    public function InserirTodosAtributos() {
        if ($this->mFL_EMBARCADO == ''){
            $this->mFL_EMBARCADO = '0';
        }
        if ($this->mNU_ORDEM_CREWLIST == ''){
            $this->mNU_ORDEM_CREWLIST = '0';
        }
        if ($this->mcand_fl_pit == ''){
            $this->mcand_fl_pit = '0';
        }
        $sql = "insert into candidato( ";
        $sql .= "   NO_PRIMEIRO_NOME            ";
        $sql .= " , NO_NOME_MEIO                ";
        $sql .= " , NO_ULTIMO_NOME              ";
        $sql .= " , NOME_COMPLETO               ";
        $sql .= " , NO_EMAIL_CANDIDATO          ";
        $sql .= " , NO_ENDERECO_RESIDENCIA      ";
        $sql .= " , NO_CIDADE_RESIDENCIA        ";
        $sql .= " , CO_PAIS_RESIDENCIA          ";
        $sql .= " , NU_TELEFONE_CANDIDATO       ";
        $sql .= " , NU_TELEFONE_CANDIDATO_CEL   ";
        $sql .= " , NO_EMPRESA_ESTRANGEIRA      ";
        $sql .= " , NO_ENDERECO_EMPRESA         ";
        $sql .= " , NO_CIDADE_EMPRESA           ";
        $sql .= " , CO_PAIS_EMPRESA             ";
        $sql .= " , NU_TELEFONE_EMPRESA         ";
        $sql .= " , NO_PAI                      ";
        $sql .= " , NO_MAE                      ";
        $sql .= " , CO_NACIONALIDADE            ";
        $sql .= " , CO_NIVEL_ESCOLARIDADE       ";
        $sql .= " , CO_ESTADO_CIVIL             ";
        $sql .= " , CO_SEXO                     ";
        $sql .= " , DT_NASCIMENTO               ";
        $sql .= " , NO_LOCAL_NASCIMENTO         ";
        $sql .= " , NU_PASSAPORTE               ";
        $sql .= " , DT_EMISSAO_PASSAPORTE       ";
        $sql .= " , DT_VALIDADE_PASSAPORTE      ";
        $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE  ";
        $sql .= " , NU_RNE                      ";
        $sql .= " , CO_PROFISSAO_CANDIDATO      ";
        $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL ";
        $sql .= " , NU_CPF                      ";
        $sql .= " , DT_ULT_ALTERACAO            ";
        $sql .= " , DT_CADASTRAMENTO            ";
        $sql .= " , CO_USU_ULT_ALTERACAO        ";
        $sql .= " , CO_USU_CADASTAMENTO         ";
        $sql .= " , NO_ENDERECO_ESTRANGEIRO     ";
        $sql .= " , NO_CIDADE_ESTRANGEIRO       ";
        $sql .= " , CO_PAIS_ESTRANGEIRO         ";
        $sql .= " , NU_TELEFONE_ESTRANGEIRO     ";
        $sql .= " , NU_EMPRESA                  ";
        $sql .= " , NU_EMBARCACAO_PROJETO       ";
        $sql .= " , codigo_processo_mte_atual   ";
        $sql .= " , NU_CTPS                     ";
        $sql .= " , NU_CNH                      ";
        $sql .= " , DT_EXPIRACAO_CNH            ";
        $sql .= " , DT_EXPIRACAO_CTPS           ";
        $sql .= " , FL_INATIVO                  ";
        $sql .= " , NO_LOGIN                    ";
        $sql .= " , NO_SENHA                    ";
        $sql .= " , FL_ATUALIZACAO_HABILITADA   ";
        $sql .= " , FL_HABILITAR_CV             ";
        $sql .= " , CO_FUNCAO                   ";
        $sql .= " , TE_DESCRICAO_ATIVIDADES     ";
        $sql .= " , VA_REMUNERACAO_MENSAL       ";
        $sql .= " , VA_REMUNERACAO_MENSAL_BRASIL";
        $sql .= " , NO_SEAMAN                   ";
        $sql .= " , DT_VALIDADE_SEAMAN          ";
        $sql .= " , DT_EMISSAO_SEAMAN           ";
        $sql .= " , CO_PAIS_SEAMAN              ";
        $sql .= " , loca_id                     ";
        $sql .= " , FL_EMBARCADO                ";
        $sql .= " , TE_OBSERVACAO_CREWLIST      ";
        $sql .= " , NU_ORDEM_CREWLIST           ";
        $sql .= " , cd_usuario_inativacao       ";
        $sql .= " , cd_usuario_sol_exclusao     ";
        $sql .= " , cd_usuario_revisao          ";
        $sql .= " , cd_usuario_conferencia      ";
        $sql .= " , cd_usuario_ativacao         ";
        $sql .= " , cand_fl_excluir             ";
        $sql .= " , cand_fl_revisado            ";
        $sql .= " , cand_fl_conferido           ";
        $sql .= " , cand_dt_inativacao          ";
        $sql .= " , cand_dt_sol_exclusao        ";
        $sql .= " , cand_dt_revisao             ";
        $sql .= " , cand_dt_conferencia         ";
        $sql .= " , cand_dt_ativacao            ";
        $sql .= " , log_tx_controller           ";
        $sql .= " , log_tx_metodo               ";
        $sql .= " , empt_id                     ";
        $sql .= " , co_pais_nacionalidade_pai   ";
        $sql .= " , co_pais_nacionalidade_mae   ";
        $sql .= " , cand_tx_residencia_estado   ";
        $sql .= " , nu_candidato_parente        ";
        $sql .= " , co_grau_parentesco          ";
        $sql .= " , cand_fl_pit                 ";
        $sql .= " , cand_dt_ult_crewlist        ";
        $sql .= " , CO_REPARTICAO_CONSULAR      ";
        $sql .= " , id_status_edicao            ";
        $sql .= " , dt_envio_edicao             ";
        $sql .= " , dt_liberacao_edicao         ";
        $sql .= " , email_pessoal               ";
        $sql .= " ) values (";
        $sql .= " " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);   //ok
        $sql .= " , " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
        $sql .= " , " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);  //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA);  //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO_CEL); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA);  //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA);  //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA);   //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA);  //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_PAI);     //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_MAE);     //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE);   //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE);  //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL);   //ok
        $sql .= " , " . cBANCO::StringOk($this->mCO_SEXO);     //ok
        $sql .= " , " . cBANCO::DataOk($this->mDT_NASCIMENTO);    //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO);  //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_PASSAPORTE);   //ok
        $sql .= " , " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE);  //ok
        $sql .= " , " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE);  //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_RNE);
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_CPF);     //ok
        $sql .= " , now()";
        $sql .= " , now()";
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_USU_CADASTAMENTO);
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_USU_ULT_ALTERACAO);
        $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO);  //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_EMPRESA);   //ok
        $sql .= " , " . cBANCO::StringOk($this->mNU_EMBARCACAO_PROJETO);   //ok
        $sql .= " , " . cBANCO::ChaveOk($this->mcodigo_processo_mte_atual);
        $sql .= " , " . cBANCO::StringOk($this->mNU_CTPS);
        $sql .= " , " . cBANCO::StringOk($this->mNU_CNH);
        $sql .= " , " . cBANCO::DataOk($this->mDT_EXPIRACAO_CNH);
        $sql .= " , " . cBANCO::DataOk($this->mDT_EXPIRACAO_CTPS);
        $sql .= " , " . cBANCO::SimNaoOk($this->mFL_INATIVO);
        $sql .= " , " . cBANCO::StringOk($this->mNO_LOGIN);
        $sql .= " , " . cBANCO::StringOk($this->mNO_SENHA);
        $sql .= " , " . cBANCO::SimNaoOk($this->mFL_ATUALIZACAO_HABILITADA);
        $sql .= " , " . cBANCO::SimNaoOk($this->mFL_HABILITAR_CV);
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_FUNCAO);
        $sql .= " , " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES);
        $sql .= " , " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL);
        $sql .= " , " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL_BRASIL);
        $sql .= " , " . cBANCO::StringOk($this->mNO_SEAMAN);
        $sql .= " , " . cBANCO::DataOk($this->mDT_VALIDADE_SEAMAN);
        $sql .= " , " . cBANCO::DataOk($this->mDT_EMISSAO_SEAMAN);
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_SEAMAN);
        $sql .= " , " . cBANCO::ChaveOk($this->mloca_id);
        $sql .= " , " . cBANCO::SimNaoOk($this->mFL_EMBARCADO);
        $sql .= " , " . cBANCO::StringOk($this->mTE_OBSERVACAO_CREWLIST);
        $sql .= " , " . cBANCO::InteiroOk($this->mNU_ORDEM_CREWLIST);
        $sql .= " , " . cBANCO::ChaveOk($this->mcd_usuario_inativacao);
        $sql .= " , " . cBANCO::ChaveOk($this->mcd_usuario_sol_exclusao);
        $sql .= " , " . cBANCO::ChaveOk($this->mcd_usuario_revisao);
        $sql .= " , " . cBANCO::ChaveOk($this->mcd_usuario_conferencia);
        $sql .= " , " . cBANCO::ChaveOk($this->mcd_usuario_ativacao);
        $sql .= " , " . cBANCO::SimNaoOk($this->mcand_fl_excluir);
        $sql .= " , " . cBANCO::SimNaoOk($this->mcand_fl_revisado);
        $sql .= " , " . cBANCO::SimNaoOk($this->mcand_fl_conferido);
        $sql .= " , " . cBANCO::DataOk($this->mcand_dt_inativacao);
        $sql .= " , " . cBANCO::DataOk($this->mcand_dt_sol_exclusao);
        $sql .= " , " . cBANCO::DataOk($this->mcand_dt_revisao);
        $sql .= " , " . cBANCO::DataOk($this->mcand_dt_conferencia);
        $sql .= " , " . cBANCO::DataOk($this->mcand_dt_ativacao);
        $sql .= " , " . cBANCO::StringOk($this->mlog_tx_controller);
        $sql .= " , " . cBANCO::StringOk($this->mlog_tx_metodo);
        $sql .= " , " . cBANCO::ChaveOk($this->mempt_id);
        $sql .= " , " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai);
        $sql .= " , " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae);
        $sql .= " , " . cBANCO::StringOk($this->mcand_tx_residencia_estado);
        $sql .= " , " . cBANCO::ChaveOk($this->mnu_candidato_parente);
        $sql .= " , " . cBANCO::StringOk($this->mco_grau_parentesco);
        $sql .= " , " . cBANCO::SimNaoOk($this->mcand_fl_pit);
        $sql .= " , " . cBANCO::DataOk($this->mcand_dt_ult_crewlist);
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_REPARTICAO_CONSULAR);
        $sql .= " , " . cBANCO::ChaveOk($this->mid_status_edicao);
        $sql .= " , " . cBANCO::DataOk($this->dt_envio_edicao);
        $sql .= " , " . cBANCO::DataOk($this->dt_liberacao_edicao);
        $sql .= " , " . cBANCO::StringOk($this->memail_pessoal);
        $sql .= " ) ";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->mNU_CANDIDATO = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function AtualizarTodosAtributos(){
        $sql = "update CANDIDATO SET";
        $sql .= "   NO_PRIMEIRO_NOME            = " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
        $sql .= " , NO_NOME_MEIO                = " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
        $sql .= " , NO_ULTIMO_NOME              = " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);   //ok
        $sql .= " , NOME_COMPLETO               = " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
        $sql .= " , NO_EMAIL_CANDIDATO          = " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);  //ok
        $sql .= " , NO_ENDERECO_RESIDENCIA      = " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
        $sql .= " , NO_CIDADE_RESIDENCIA        = " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
        $sql .= " , CO_PAIS_RESIDENCIA          = " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA);  //ok
        $sql .= " , NU_TELEFONE_CANDIDATO       = " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
        $sql .= " , NU_TELEFONE_CANDIDATO_CEL   = " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO_CEL); //ok
        $sql .= " , NO_EMPRESA_ESTRANGEIRA      = " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok
        $sql .= " , NO_ENDERECO_EMPRESA         = " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA);  //ok
        $sql .= " , NO_CIDADE_EMPRESA           = " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA);  //ok
        $sql .= " , CO_PAIS_EMPRESA             = " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA);   //ok
        $sql .= " , NU_TELEFONE_EMPRESA         = " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA);  //ok
        $sql .= " , NO_PAI                      = " . cBANCO::StringOk($this->mNO_PAI);     //ok
        $sql .= " , NO_MAE                      = " . cBANCO::StringOk($this->mNO_MAE);     //ok
        $sql .= " , CO_NACIONALIDADE            = " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE);   //ok
        $sql .= " , CO_NIVEL_ESCOLARIDADE       = " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE);  //ok
        $sql .= " , CO_ESTADO_CIVIL             = " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL);   //ok
        $sql .= " , CO_SEXO                     = " . cBANCO::StringOk($this->mCO_SEXO);     //ok
        $sql .= " , DT_NASCIMENTO               = " . cBANCO::DataOk($this->mDT_NASCIMENTO);    //ok
        $sql .= " , NO_LOCAL_NASCIMENTO         = " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO);  //ok
        $sql .= " , NU_PASSAPORTE               = " . cBANCO::StringOk($this->mNU_PASSAPORTE);   //ok
        $sql .= " , DT_EMISSAO_PASSAPORTE       = " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE);  //ok
        $sql .= " , DT_VALIDADE_PASSAPORTE      = " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE);  //ok
        $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE  = " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
        $sql .= " , NU_RNE                      = " . cBANCO::StringOk($this->mNU_RNE);
        $sql .= " , CO_PROFISSAO_CANDIDATO      = " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
        $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL = " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok
        $sql .= " , NU_CPF                      = " . cBANCO::StringOk($this->mNU_CPF);     //ok
        $sql .= " , DT_ULT_ALTERACAO            = now()";
        $sql .= " , CO_USU_ULT_ALTERACAO        = " . cBANCO::ChaveOk($this->mCO_USU_ULT_ALTERACAO);
        $sql .= " , NO_ENDERECO_ESTRANGEIRO     = " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
        $sql .= " , NO_CIDADE_ESTRANGEIRO       = " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
        $sql .= " , CO_PAIS_ESTRANGEIRO         = " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO);  //ok
        $sql .= " , NU_TELEFONE_ESTRANGEIRO     = " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
        $sql .= " , NU_EMPRESA                  = " . cBANCO::StringOk($this->mNU_EMPRESA);   //ok
        $sql .= " , NU_EMBARCACAO_PROJETO       = " . cBANCO::StringOk($this->mNU_EMBARCACAO_PROJETO);   //ok
        $sql .= " , codigo_processo_mte_atual   = " . cBANCO::ChaveOk($this->mcodigo_processo_mte_atual);
        $sql .= " , NU_CTPS                     = " . cBANCO::StringOk($this->mNU_CTPS);
        $sql .= " , NU_CNH                      = " . cBANCO::StringOk($this->mNU_CNH);
        $sql .= " , DT_EXPIRACAO_CNH            = " . cBANCO::DataOk($this->mDT_EXPIRACAO_CNH);
        $sql .= " , DT_EXPIRACAO_CTPS           = " . cBANCO::DataOk($this->mDT_EXPIRACAO_CTPS);
        $sql .= " , FL_INATIVO                  = " . cBANCO::SimNaoOk($this->mFL_INATIVO);
        $sql .= " , NO_LOGIN                    = " . cBANCO::StringOk($this->mNO_LOGIN);
        $sql .= " , NO_SENHA                    = " . cBANCO::StringOk($this->mNO_SENHA);
        $sql .= " , FL_ATUALIZACAO_HABILITADA   = " . cBANCO::SimNaoOk($this->mFL_ATUALIZACAO_HABILITADA);
        $sql .= " , FL_HABILITAR_CV             = " . cBANCO::SimNaoOk($this->mFL_HABILITAR_CV);
        $sql .= " , CO_FUNCAO                   = " . cBANCO::ChaveOk($this->mCO_FUNCAO);
        $sql .= " , TE_DESCRICAO_ATIVIDADES     = " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES);
        $sql .= " , VA_REMUNERACAO_MENSAL       = " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL);
        $sql .= " , VA_REMUNERACAO_MENSAL_BRASIL= " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL_BRASIL);
        $sql .= " , NO_SEAMAN                   = " . cBANCO::StringOk($this->mNO_SEAMAN);
        $sql .= " , DT_VALIDADE_SEAMAN          = " . cBANCO::DataOk($this->mDT_VALIDADE_SEAMAN);
        $sql .= " , DT_EMISSAO_SEAMAN           = " . cBANCO::DataOk($this->mDT_EMISSAO_SEAMAN);
        $sql .= " , CO_PAIS_SEAMAN              = " . cBANCO::ChaveOk($this->mCO_PAIS_SEAMAN);
        $sql .= " , loca_id                     = " . cBANCO::ChaveOk($this->mloca_id);
        $sql .= " , FL_EMBARCADO                = " . cBANCO::SimNaoOk($this->mFL_EMBARCADO);
        $sql .= " , TE_OBSERVACAO_CREWLIST      = " . cBANCO::StringOk($this->mTE_OBSERVACAO_CREWLIST);
        $sql .= " , NU_ORDEM_CREWLIST           = " . cBANCO::InteiroOk($this->mNU_ORDEM_CREWLIST);
        $sql .= " , cd_usuario_inativacao       = " . cBANCO::ChaveOk($this->mcd_usuario_inativacao);
        $sql .= " , cd_usuario_sol_exclusao     = " . cBANCO::ChaveOk($this->mcd_usuario_sol_exclusao);
        $sql .= " , cd_usuario_revisao          = " . cBANCO::ChaveOk($this->mcd_usuario_revisao);
        $sql .= " , cd_usuario_conferencia      = " . cBANCO::ChaveOk($this->mcd_usuario_conferencia);
        $sql .= " , cd_usuario_ativacao         = " . cBANCO::ChaveOk($this->mcd_usuario_ativacao);
        $sql .= " , cand_fl_excluir             = " . cBANCO::SimNaoOk($this->mcand_fl_excluir);
        $sql .= " , cand_fl_revisado            = " . cBANCO::SimNaoOk($this->mcand_fl_revisado);
        $sql .= " , cand_fl_conferido           = " . cBANCO::SimNaoOk($this->mcand_fl_conferido);
        $sql .= " , cand_dt_inativacao          = " . cBANCO::DataOk($this->mcand_dt_inativacao);
        $sql .= " , cand_dt_sol_exclusao        = " . cBANCO::DataOk($this->mcand_dt_sol_exclusao);
        $sql .= " , cand_dt_revisao             = " . cBANCO::DataOk($this->mcand_dt_revisao);
        $sql .= " , cand_dt_conferencia         = " . cBANCO::DataOk($this->mcand_dt_conferencia);
        $sql .= " , cand_dt_ativacao            = " . cBANCO::DataOk($this->mcand_dt_ativacao);
        $sql .= " , log_tx_controller           = " . cBANCO::StringOk($this->mlog_tx_controller);
        $sql .= " , log_tx_metodo               = " . cBANCO::StringOk($this->mlog_tx_metodo);
        $sql .= " , empt_id                     = " . cBANCO::ChaveOk($this->mempt_id);
        $sql .= " , co_pais_nacionalidade_pai   = " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai);
        $sql .= " , co_pais_nacionalidade_mae   = " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae);
        $sql .= " , cand_tx_residencia_estado   = " . cBANCO::StringOk($this->mcand_tx_residencia_estado);
        $sql .= " , NU_TELEFONE_CANDIDATO_CEL   = " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO_CEL);
        $sql .= " , nu_candidato_parente        = " . cBANCO::ChaveOk($this->mnu_candidato_parente);
        $sql .= " , co_grau_parentesco          = " . cBANCO::StringOk($this->mco_grau_parentesco);
        $sql .= " , cand_fl_pit                 = " . cBANCO::SimNaoOk($this->mcand_fl_pit);
        $sql .= " , cand_dt_ult_crewlist        = " . cBANCO::DataOk($this->mcand_dt_ult_crewlist);
        $sql .= " , CO_REPARTICAO_CONSULAR      = " . cBANCO::ChaveOk($this->mCO_REPARTICAO_CONSULAR);
        $sql .= " , id_status_edicao            = " . cBANCO::ChaveOk($this->mid_status_edicao);
        $sql .= " , dt_envio_edicao             = " . cBANCO::DataOk($this->dt_envio_edicao);
        $sql .= " , dt_liberacao_edicao         = " . cBANCO::DataOk($this->dt_liberacao_edicao);
        $sql .= " , email_pessoal               = " . cBANCO::StringOk($this->memail_pessoal);
        $sql .= " where nu_candidato=".$this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
    }


    public function AtualizarImportacoes() {
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);

        $sql = "update CANDIDATO SET ";
        $sql .= "   NU_EMPRESA					= " . cBANCO::StringOk($this->mNU_EMPRESA);   //ok
        $sql .= " , NU_EMBARCACAO_PROJETO		= " . cBANCO::StringOk($this->mNU_EMBARCACAO_PROJETO);   //ok
        $sql .= " , NO_PRIMEIRO_NOME 			= " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
        $sql .= " , NO_NOME_MEIO 				= " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
        $sql .= " , NO_ULTIMO_NOME 				= " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);   //ok
        $sql .= " , NOME_COMPLETO 				= " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
        $sql .= " , NO_EMAIL_CANDIDATO 			= " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);  //ok
        $sql .= " , NO_ENDERECO_RESIDENCIA 		= " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
        $sql .= " , NO_CIDADE_RESIDENCIA 		= " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
        $sql .= " , CO_PAIS_RESIDENCIA 			= " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA);  //ok
        $sql .= " , NU_TELEFONE_CANDIDATO 		= " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
        $sql .= " , NU_TELEFONE_CANDIDATO_CEL	= " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO_CEL); //ok
        $sql .= " , NO_EMPRESA_ESTRANGEIRA		= " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok
        $sql .= " , NO_ENDERECO_EMPRESA 		= " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA);  //ok
        $sql .= " , NO_CIDADE_EMPRESA 			= " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA);  //ok
        $sql .= " , CO_PAIS_EMPRESA 			= " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA);   //ok
        $sql .= " , NU_TELEFONE_EMPRESA 		= " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA);  //ok
        $sql .= " , NO_PAI 						= " . cBANCO::StringOk($this->mNO_PAI);     //ok
        $sql .= " , NO_MAE 						= " . cBANCO::StringOk($this->mNO_MAE);     //ok
        $sql .= " , CO_NACIONALIDADE 			= " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE);   //ok
        $sql .= " , CO_NIVEL_ESCOLARIDADE 		= " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE);  //ok
        $sql .= " , CO_ESTADO_CIVIL 			= " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL);   //ok
        $sql .= " , CO_SEXO 					= " . cBANCO::StringOk($this->mCO_SEXO);     //ok
        $sql .= " , DT_NASCIMENTO 				= " . cBANCO::DataOk($this->mDT_NASCIMENTO);    //ok
        $sql .= " , NO_LOCAL_NASCIMENTO 		= " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO);  //ok
        $sql .= " , NU_PASSAPORTE 				= " . cBANCO::StringOk($this->mNU_PASSAPORTE);   //ok
        $sql .= " , DT_EMISSAO_PASSAPORTE 		= " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE);  //ok
        $sql .= " , DT_VALIDADE_PASSAPORTE 		= " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE);  //ok
        $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE 	= " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
        $sql .= " , NU_RNE 						= " . cBANCO::StringOk($this->mNU_RNE);
        $sql .= " , CO_PROFISSAO_CANDIDATO 		= " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
        $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL = " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok
        $sql .= " , NU_CPF 						= " . cBANCO::StringOk($this->mNU_CPF);     //ok
        $sql .= " , DT_ULT_ALTERACAO 			= now()";
        $sql .= " , CO_USU_ULT_ALTERACAO 		= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , NO_ENDERECO_ESTRANGEIRO 	= " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
        $sql .= " , NO_CIDADE_ESTRANGEIRO 		= " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
        $sql .= " , CO_PAIS_ESTRANGEIRO 		= " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO);  //ok
        $sql .= " , NU_TELEFONE_ESTRANGEIRO 	= " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
        $sql .= " , NU_CNH 						= " . cBANCO::StringOk($this->mNU_CNH);
        $sql .= " , DT_EXPIRACAO_CNH			= " . cBANCO::DataOk($this->mDT_EXPIRACAO_CNH);
        $sql .= " , NU_CTPS						= " . cBANCO::StringOk($this->mNU_CTPS);
        $sql .= " , DT_EXPIRACAO_CTPS			= " . cBANCO::DataOk($this->mDT_EXPIRACAO_CTPS);
        $sql .= " , empt_id						= " . cBANCO::ChaveOk($this->mempt_id);
        $sql .= " , FL_INATIVO					= " . cBANCO::SimNaoOk($this->mFL_INATIVO);
        $sql .= " , FL_EMBARCADO				= " . cBANCO::SimNaoOk($this->mFL_EMBARCADO);
        $sql .= " , co_pais_nacionalidade_pai	= " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai);
        $sql .= " , co_pais_nacionalidade_mae	= " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae);
        $sql .= " , VA_REMUNERACAO_MENSAL	= " . cBANCO::ValorOk($this->mVA_REMUNERACAO_MENSAL);
        $sql .= " , VA_REMUNERACAO_MENSAL_BRASIL	= " . cBANCO::ValorOk($this->mVA_REMUNERACAO_MENSAL_BRASIL);
        $sql .= " , NO_SEAMAN	= " . cBANCO::StringOk($this->mNO_SEAMAN);
        $sql .= " , DT_EMISSAO_SEAMAN	= " . cBANCO::DataOk($this->mDT_EMISSAO_SEAMAN);
        $sql .= " , DT_VALIDADE_SEAMAN	= " . cBANCO::DataOk($this->mDT_VALIDADE_SEAMAN);
        $sql .= " , CO_PAIS_SEAMAN	= " . cBANCO::ChaveOk($this->mCO_PAIS_SEAMAN);
        $sql .= " , CO_FUNCAO	= " . cBANCO::ChaveOk($this->mCO_FUNCAO);
        $sql .= " , CO_PROFISSAO_CANDIDATO	= " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO);
        $sql .= " , NU_EMPRESA	= " . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= " , NU_EMBARCACAO_PROJETO	= " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= " , TE_DESCRICAO_ATIVIDADES	= " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES);
        $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL	= " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL);
        $sql .= " , CO_REPARTICAO_CONSULAR	= " . cBANCO::ChaveOk($this->mCO_REPARTICAO_CONSULAR);
        $sql .= " where NU_CANDIDATO 			= " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->VerifiqueCadastroCompleto();
    }

    public function AtualizarExperienciaAnterior() {
        $sql = "update CANDIDATO SET ";
        $sql .= "   TE_EXPERIENCIA_ANTERIOR		= " . self::StrBanco($this->mTE_EXPERIENCIA_ANTERIOR);
        $sql .= " where NU_CANDIDATO 			= " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Atualiza atributos da instância no banco
     */
    public function AtualizarExterno() {
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);

        $sql = "update candidato_tmp SET ";
        $sql .= "   NU_CANDIDATO 				= " . cBANCO::StringOk($this->mNU_CANDIDATO);
        $sql .= " , NO_PRIMEIRO_NOME 			= " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
        $sql .= " , NO_NOME_MEIO 				= " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
        $sql .= " , NO_ULTIMO_NOME 				= " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);   //ok
        $sql .= " , NOME_COMPLETO 				= " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
        $sql .= " , NO_EMAIL_CANDIDATO 			= " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);  //ok
        $sql .= " , NO_ENDERECO_RESIDENCIA 		= " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
        $sql .= " , NO_CIDADE_RESIDENCIA 		= " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
        $sql .= " , CO_PAIS_RESIDENCIA 			= " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA); //ok
        $sql .= " , NU_TELEFONE_CANDIDATO 		= " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
        $sql .= " , NO_EMPRESA_ESTRANGEIRA		= " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok
        $sql .= " , NO_ENDERECO_EMPRESA 		= " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA); //ok
        $sql .= " , NO_CIDADE_EMPRESA 			= " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA); //ok
        $sql .= " , CO_PAIS_EMPRESA 			= " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA); //ok
        $sql .= " , NU_TELEFONE_EMPRESA 		= " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA); //ok
        $sql .= " , NO_PAI 						= " . cBANCO::StringOk($this->mNO_PAI); //ok
        $sql .= " , NO_MAE 						= " . cBANCO::StringOk($this->mNO_MAE); //ok
        $sql .= " , CO_NACIONALIDADE 			= " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE); //ok
        $sql .= " , CO_NIVEL_ESCOLARIDADE 		= " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE); //ok
        $sql .= " , CO_ESTADO_CIVIL 			= " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL); //ok
        $sql .= " , CO_SEXO 					= " . cBANCO::StringOk($this->mCO_SEXO); //ok
        $sql .= " , DT_NASCIMENTO 				= " . cBANCO::DataOk($this->mDT_NASCIMENTO); //ok
        $sql .= " , NO_LOCAL_NASCIMENTO 		= " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO); //ok
        $sql .= " , NU_PASSAPORTE 				= " . cBANCO::StringOk($this->mNU_PASSAPORTE); //ok
        $sql .= " , DT_EMISSAO_PASSAPORTE 		= " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE); //ok
        $sql .= " , DT_VALIDADE_PASSAPORTE 		= " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE); //ok
        $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE 	= " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
        $sql .= " , NU_RNE 						= " . cBANCO::StringOk($this->mNU_RNE);
        $sql .= " , CO_PROFISSAO_CANDIDATO 		= " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
        $sql .= " , TE_DESCRICAO_ATIVIDADES		= " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES); //ok
        $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL = " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok
        $sql .= " , NU_CPF 						= " . cBANCO::StringOk($this->mNU_CPF);      //ok
        $sql .= " , DT_ULT_ALTERACAO 			= now()";
        $sql .= " , CO_USU_ULT_ALTERACAO 		= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , NO_ENDERECO_ESTRANGEIRO 	= " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
        $sql .= " , NO_CIDADE_ESTRANGEIRO 		= " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
        $sql .= " , CO_PAIS_ESTRANGEIRO 		= " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO); //ok
        $sql .= " , NU_TELEFONE_ESTRANGEIRO 	= " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
        $sql .= " , VA_REMUNERACAO_MENSAL		= " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL); //ok
        $sql .= " , VA_REMUNERACAO_MENSAL_BRASIL = " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL_BRASIL); //ok
        $sql .= " , CO_PAIS_SEAMAN				= " . cBANCO::ChaveOk($this->mCO_PAIS_SEAMAN); //ok
        $sql .= " , DT_EMISSAO_SEAMAN			= " . cBANCO::DataOk($this->mDT_EMISSAO_SEAMAN); //ok
        $sql .= " , DT_VALIDADE_SEAMAN			= " . cBANCO::DataOk($this->mDT_VALIDADE_SEAMAN); //ok
        $sql .= " , NO_SEAMAN					= " . cBANCO::StringOk($this->mNO_SEAMAN); //ok
        $sql .= " , CO_FUNCAO					= " . cBANCO::ChaveOk($this->mCO_FUNCAO); //ok
        $sql .= " , CO_PROFISSAO_CANDIDATO		= " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
        $sql .= " , CO_LOCAL_EMBARCACAO_PROJETO	= " . cBANCO::ChaveOk($this->mCO_LOCAL_EMBARCACAO_PROJETO); //ok
        $sql .= " , co_pais_nacionalidade_pai	= " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai); //ok
        $sql .= " , co_pais_nacionalidade_mae	= " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae); //ok
        $sql .= " where nu_candidato_tmp 		= " . $this->mnu_candidato_tmp;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AtualizeCadastroParaCliente() {
        $sql = "update candidato_tmp, CANDIDATO SET ";
        $sql .= "   candidato_tmp.NO_PRIMEIRO_NOME				= CANDIDATO.NO_PRIMEIRO_NOME";
        $sql .= " , candidato_tmp.NO_NOME_MEIO					= CANDIDATO.NO_NOME_MEIO";
        $sql .= " , candidato_tmp.NO_ULTIMO_NOME 				= CANDIDATO.NO_ULTIMO_NOME";
        $sql .= " , candidato_tmp.NOME_COMPLETO 				= CANDIDATO.NOME_COMPLETO";
        $sql .= " , candidato_tmp.NO_EMAIL_CANDIDATO 			= CANDIDATO.NO_EMAIL_CANDIDATO";
        $sql .= " , candidato_tmp.NO_ENDERECO_RESIDENCIA 		= CANDIDATO.NO_ENDERECO_RESIDENCIA";
        $sql .= " , candidato_tmp.NO_CIDADE_RESIDENCIA			= CANDIDATO.NO_CIDADE_RESIDENCIA";
        $sql .= " , candidato_tmp.CO_PAIS_RESIDENCIA 			= CANDIDATO.CO_PAIS_RESIDENCIA";
        $sql .= " , candidato_tmp.NU_TELEFONE_CANDIDATO 		= CANDIDATO.NU_TELEFONE_CANDIDATO";
        $sql .= " , candidato_tmp.NO_EMPRESA_ESTRANGEIRA		= CANDIDATO.NO_EMPRESA_ESTRANGEIRA";
        $sql .= " , candidato_tmp.NO_ENDERECO_EMPRESA			= CANDIDATO.NO_ENDERECO_EMPRESA";
        $sql .= " , candidato_tmp.NO_CIDADE_EMPRESA 			= CANDIDATO.NO_CIDADE_EMPRESA";
        $sql .= " , candidato_tmp.CO_PAIS_EMPRESA				= CANDIDATO.CO_PAIS_EMPRESA";
        $sql .= " , candidato_tmp.NU_TELEFONE_EMPRESA			= CANDIDATO.NU_TELEFONE_EMPRESA";
        $sql .= " , candidato_tmp.NO_PAI 						= CANDIDATO.NO_PAI";
        $sql .= " , candidato_tmp.NO_MAE 						= CANDIDATO.NO_MAE";
        $sql .= " , candidato_tmp.CO_NACIONALIDADE				= CANDIDATO.CO_NACIONALIDADE";
        $sql .= " , candidato_tmp.CO_NIVEL_ESCOLARIDADE 		= CANDIDATO.CO_NIVEL_ESCOLARIDADE";
        $sql .= " , candidato_tmp.CO_ESTADO_CIVIL				= CANDIDATO.CO_ESTADO_CIVIL";
        $sql .= " , candidato_tmp.CO_SEXO						= CANDIDATO.CO_SEXO";
        $sql .= " , candidato_tmp.DT_NASCIMENTO 				= CANDIDATO.DT_NASCIMENTO";
        $sql .= " , candidato_tmp.NO_LOCAL_NASCIMENTO			= CANDIDATO.NO_LOCAL_NASCIMENTO";
        $sql .= " , candidato_tmp.NU_PASSAPORTE 				= CANDIDATO.NU_PASSAPORTE";
        $sql .= " , candidato_tmp.DT_EMISSAO_PASSAPORTE 		= CANDIDATO.DT_EMISSAO_PASSAPORTE";
        $sql .= " , candidato_tmp.DT_VALIDADE_PASSAPORTE 		= CANDIDATO.DT_VALIDADE_PASSAPORTE";
        $sql .= " , candidato_tmp.CO_PAIS_EMISSOR_PASSAPORTE 	= CANDIDATO.CO_PAIS_EMISSOR_PASSAPORTE";
        $sql .= " , candidato_tmp.NU_RNE 						= CANDIDATO.NU_RNE";
        $sql .= " , candidato_tmp.CO_PROFISSAO_CANDIDATO 		= CANDIDATO.CO_PROFISSAO_CANDIDATO";
        $sql .= " , candidato_tmp.TE_TRABALHO_ANTERIOR_BRASIL	= CANDIDATO.TE_TRABALHO_ANTERIOR_BRASIL";
        $sql .= " , candidato_tmp.NU_CPF 						= CANDIDATO.NU_CPF";
        $sql .= " , candidato_tmp.DT_ULT_ALTERACAO				= CANDIDATO.DT_ULT_ALTERACAO";
        $sql .= " , candidato_tmp.CO_USU_ULT_ALTERACAO			= CANDIDATO.CO_USU_ULT_ALTERACAO";
        $sql .= " , candidato_tmp.NO_ENDERECO_ESTRANGEIRO		= CANDIDATO.NO_ENDERECO_ESTRANGEIRO";
        $sql .= " , candidato_tmp.NO_CIDADE_ESTRANGEIRO 		= CANDIDATO.NO_CIDADE_ESTRANGEIRO";
        $sql .= " , candidato_tmp.CO_PAIS_ESTRANGEIRO			= CANDIDATO.CO_PAIS_ESTRANGEIRO";
        $sql .= " , candidato_tmp.NU_TELEFONE_ESTRANGEIRO		= CANDIDATO.NU_TELEFONE_ESTRANGEIRO";
        $sql .= " , candidato_tmp.VA_REMUNERACAO_MENSAL		    = CANDIDATO.VA_REMUNERACAO_MENSAL";
        $sql .= " , candidato_tmp.VA_REMUNERACAO_MENSAL_BRASIL	= CANDIDATO.VA_REMUNERACAO_MENSAL_BRASIL";
        $sql .= " , candidato_tmp.NU_EMPRESA                    = CANDIDATO.NU_EMPRESA";
        $sql .= " , candidato_tmp.NU_EMBARCACAO_PROJETO         = CANDIDATO.NU_EMBARCACAO_PROJETO";
        $sql .= " , candidato_tmp.co_pais_nacionalidade_pai     = CANDIDATO.co_pais_nacionalidade_pai";
        $sql .= " , candidato_tmp.co_pais_nacionalidade_mae     = CANDIDATO.co_pais_nacionalidade_mae";
        $sql .= " where candidato_tmp.NU_CANDIDATO = CANDIDATO.NU_CANDIDATO and CANDIDATO.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Obtem a última solicitação de serviço feita para o candidato.
     * @deprecated Era utilizada para saber qual era o último visto do candidato, mas agora isso é obtido diretamente na tabela de processos.
     */
    public function UltimaSolicitacao() {
        $sql = "select max(ID_SOLICITA_VISTO) from AUTORIZACAO_CANDIDATO where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                if ($rs[0] > 0) {
                    $ac = new cAUTORIZACAO_CANDIDATO();
                    $ac->Recuperar($rs[0], $this->mNU_CANDIDATO);
                    return $ac;
                }
            }
        }
    }

    public function rsArquivos() {
        $sql = "select NU_EMPRESA, NU_CANDIDATO, NU_SEQUENCIAL,NO_ARQUIVO,NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NO_TIPO_ARQUIVO ";
        $sql .= "  from ARQUIVOS a";
        $sql .= "  left join TIPO_ARQUIVO ta on ta.ID_TIPO_ARQUIVO=a.TP_ARQUIVO ";
        $sql .= " where NU_SEQUENCIAL>0";
        $sql .= " and NU_CANDIDATO= " . $this->mNU_CANDIDATO;
        $sql .= " order by DT_INCLUSAO,TP_ARQUIVO,NO_ARQUIVO ";
        //$ret = mysql_query($sql);
        //return $ret;
        return $sql;
    }

    public function SincronizeSatelites($pid_solicita_visto) {
        $sql = " update processo_mte, AUTORIZACAO_CANDIDATO ";
        $sql .= "   set processo_mte.cd_funcao = AUTORIZACAO_CANDIDATO.CO_FUNCAO_CANDIDATO";
        $sql .= "     , processo_mte.cd_reparticao = AUTORIZACAO_CANDIDATO.CO_REPARTICAO_CONSULAR";
        $sql .= " where processo_mte.id_solicita_visto          = " . $pid_solicita_visto;
        $sql .= "   and AUTORIZACAO_CANDIDATO.id_solicita_visto = " . $pid_solicita_visto;
        $sql .= "   and AUTORIZACAO_CANDIDATO.NU_CANDIDATO      = " . $this->mNU_CANDIDATO;
        $sql .= "   and processo_mte.cd_candidato               = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     *
     * Obtem código do visto atual
     */
    public function CodigoVistoAtual() {
        $sql = "select codigo_processo_mte_atual from CANDIDATO where NU_CANDIDATO = " . $pNU_CANDIDATO;
        $rs = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__ . '->Obtendo visto atual');
        if ($rw = mysql_fetch_array($rs)) {
            return $rw['codigo_processo_mte_atual'];
        }
    }

    public function QtdHomonimos() {
        $sql = " select ifnull(count(*),0) qtd from " . DATAWEB_MORTO . ".CANDIDATO CM, " . DATAWEB_MORTO . ".AUTORIZACAO_CANDIDATO ACM  ";
        $sql .= " where CM.NU_CANDIDATO_PAI = " . $this->mNU_CANDIDATO;
        $sql .= " and ACM.NU_CANDIDATO = CM.NU_CANDIDATO";
        $sql .= " and ACM.FL_IGNORADO is null";
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                return $rs[0];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function CorrijaNomeCompleto() {
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);
        $sql = " update CANDIDATO SET NOME_COMPLETO =  " . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= " where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function LogueUltimaAtualizacao($pClass = '', $pFunction = '') {
        $sql = "update CANDIDATO ";
        $sql.= "   set DT_ULT_ALTERACAO = now() ";
        $sql.= "     , CO_USU_ULT_ALTERACAO = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $this->SqlLog($pClass, $pFunction);
        $sql.= " where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function Vistos() {
        $sql = "select codigo, dt_requerimento, prazo_solicitado from processo_mte where cd_candidato = " . $this->mNU_CANDIDATO;
        return conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function Excluir() {
        try {
            executeQuery("begin", __CLASS__ . '->' . __FUNCTION__);
            $sql = "delete from processo_mte where cd_candidato = " . $this->mNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $sql = "delete from processo_prorrog where cd_candidato = " . $this->mNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $sql = "delete from processo_regcie where cd_candidato = " . $this->mNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $sql = "delete from processo_coleta where cd_candidato = " . $this->mNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $sql = "delete from processo_cancel where cd_candidato = " . $this->mNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $sql = "delete from AUTORIZACAO_CANDIDATO where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $sql = "delete from ARQUIVOS where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $sql = "delete from ARQUIVOS_FORMULARIOS where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $sql = "delete from CANDIDATO where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            executeQuery("commit", __CLASS__ . '->' . __FUNCTION__);
        } catch (Exception $e) {
            var_dump($e);
            executeQuery("rollback", __CLASS__ . '->' . __FUNCTION__);
        }
    }

    /**
     * Verifica se os campos mínimos do candidato foram preenchidos e atualiza o status conforme
     */
    public function VerifiqueCadastroCompleto() {
        //
        // Atualiza o status do candidato se o cadastro estiver completo
        $sql = "update candidato set BO_CADASTRO_MINIMO_OK = 1
				where BO_CADASTRO_MINIMO_OK = 0
				and ifnull(NO_PRIMEIRO_NOME,'' ) != ''
				and ifnull(NO_MAE,'') != ''
				and ifnull(NO_PAI,'') != ''
				and ifnull(CO_PROFISSAO_CANDIDATO,'') != ''
				and ifnull(CO_ESTADO_CIVIL,'') != ''
				and ifnull(CO_SEXO,'') != ''
				and ifnull(DT_NASCIMENTO,'') != ''
				and ifnull(CO_NACIONALIDADE,'') != ''
				and ifnull(CO_NIVEL_ESCOLARIDADE,'') != ''
				and ifnull(NO_ENDERECO_RESIDENCIA,'') != ''
				and ifnull(NU_PASSAPORTE,'') != ''
				and ifnull(DT_EMISSAO_PASSAPORTE,'') != ''
				and ifnull(DT_VALIDADE_PASSAPORTE,'') != ''
				and ifnull(CO_PAIS_EMISSOR_PASSAPORTE,'') != ''
				and NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Salva a instância como um candidato novo (insert).
     * @throws "Já existe um candidato com esse nome e data de nascimento"
     */
    public function Salve_Novo() {
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto(trim($this->mNO_PRIMEIRO_NOME), trim($this->mNO_NOME_MEIO), trim($this->mNO_ULTIMO_NOME));

        if (trim($this->mNOME_COMPLETO) == '') {
            $msg .= '\n- Pelo menos um nome é obrigatório;';
        }
        if (trim($this->mDT_NASCIMENTO) == '') {
            $msg .= '\n- Data de nascimento obrigatória;';
        }
        if ($msg == '') {
            $sql = "select NU_CANDIDATO from CANDIDATO where NOME_COMPLETO like " . cBANCO::StringOk($this->mNOME_COMPLETO) . " and DT_NASCIMENTO = " . cBANCO::DataOk($this->mDT_NASCIMENTO);
            $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            if ($rs = mysql_fetch_array($res)) {
                throw new Exception('\n- Já existe um candidato com esse nome/data de nascimento. Por favor verifique novamente antes de tentar incluir.');
            }
        }
        $msg = '';
        if ($this->mNU_EMPRESA == 0) {
            $msg .= '\n- A empresa é obrigatória;';
        }
        if (trim($this->mNO_MAE) == '') {
            $msg .= '\n- Nome da mãe é obrigatório.';
        }
        if ($msg != '') {
            throw new Exception($msg);
        }
        $sql = "insert into CANDIDATO(";
        $sql .= "   NO_PRIMEIRO_NOME";
        $sql .= " , NO_NOME_MEIO";
        $sql .= " , NO_ULTIMO_NOME";
        $sql .= " , NOME_COMPLETO";
        $sql .= " , NO_EMAIL_CANDIDATO";
        $sql .= " , NO_ENDERECO_RESIDENCIA";
        $sql .= " , NO_CIDADE_RESIDENCIA";
        $sql .= " , CO_PAIS_RESIDENCIA";
        $sql .= " , NU_TELEFONE_CANDIDATO";
        $sql .= " , NO_EMPRESA_ESTRANGEIRA";
        $sql .= " , NO_ENDERECO_EMPRESA";
        $sql .= " , NO_CIDADE_EMPRESA";
        $sql .= " , CO_PAIS_EMPRESA";
        $sql .= " , NU_TELEFONE_EMPRESA";
        $sql .= " , NO_PAI";
        $sql .= " , NO_MAE";
        $sql .= " , CO_NACIONALIDADE";
        $sql .= " , DT_NASCIMENTO";
        $sql .= " , NO_LOCAL_NASCIMENTO";
        $sql .= " , NU_PASSAPORTE";
        $sql .= " , DT_EMISSAO_PASSAPORTE";
        $sql .= " , DT_VALIDADE_PASSAPORTE";
        $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE";
        $sql .= " , NU_RNE";
        $sql .= " , CO_PROFISSAO_CANDIDATO";
        $sql .= " , NU_CPF";
        $sql .= " , DT_CADASTRAMENTO";
        $sql .= " , CO_USU_CADASTAMENTO";
        $sql .= " , DT_ULT_ALTERACAO";
        $sql .= " , CO_USU_ULT_ALTERACAO";
        $sql .= " , NO_ENDERECO_ESTRANGEIRO";
        $sql .= " , NO_CIDADE_ESTRANGEIRO";
        $sql .= " , CO_PAIS_ESTRANGEIRO";
        $sql .= " , NU_TELEFONE_ESTRANGEIRO";
        $sql .= " , BO_CADASTRO_MINIMO_OK";
        $sql .= " , NU_EMPRESA";
        $sql .= " , NU_EMBARCACAO_PROJETO";
        $sql .= " , codigo_processo_mte_atual";
        $sql .= " , NU_CTPS";
        $sql .= " , NU_CNH";
        $sql .= " , DT_EXPIRACAO_CNH";
        $sql .= " , DT_EXPIRACAO_CTPS";
        $sql .= " , co_pais_nacionalidade_pai";
        $sql .= " , co_pais_nacionalidade_mae";
        $sql .= ") values (";
        $sql .= "   " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);
        $sql .= " , " . cBANCO::StringOk($this->mNO_NOME_MEIO);
        $sql .= " , " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);
        $sql .= " , " . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= " , " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);
        $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA);
        $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);
        $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA);
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO);
        $sql .= " , " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA);
        $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA);
        $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA);
        $sql .= " , " . cBANCO::InteiroOk($this->mCO_PAIS_EMPRESA);
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA);
        $sql .= " , " . cBANCO::StringOk($this->mNO_PAI);
        $sql .= " , " . cBANCO::StringOk($this->mNO_MAE);
        $sql .= " , " . cBANCO::InteiroOk($this->mCO_NACIONALIDADE);
        $sql .= " , " . cBANCO::DataOk($this->mDT_NASCIMENTO);
        $sql .= " , " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO);
        $sql .= " , " . cBANCO::StringOk($this->mNU_PASSAPORTE);
        $sql .= " , " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE);
        $sql .= " , " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE);
        $sql .= " , " . cBANCO::InteiroOk($this->mCO_PAIS_EMISSOR_PASSAPORTE);
        $sql .= " , " . cBANCO::StringOk($this->mNU_RNE);
        $sql .= " , " . cBANCO::InteiroOk($this->mCO_PROFISSAO_CANDIDATO);
        $sql .= " , " . cBANCO::StringOk($this->mNU_CPF);
        $sql .= " , now() ";
        $sql .= " , " . cSESSAO::$mcd_usuario;
        $sql .= " , now()";
        $sql .= " , " . cSESSAO::$mcd_usuario;
        $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO);
        $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO);
        $sql .= " , " . cBANCO::InteiroOk($this->mCO_PAIS_ESTRANGEIRO);
        $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO);
        $sql .= " , 0";
        $sql .= " , " . cBANCO::InteiroOk($this->mNU_EMPRESA);
        $sql .= " , " . cBANCO::InteiroOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= " , " . cBANCO::InteiroOk($this->mcodigo_processo_mte_atual);
        $sql .= " , " . cBANCO::StringOk($this->mNU_CTPS);
        $sql .= " , " . cBANCO::StringOk($this->mNU_CNH);
        $sql .= " , " . cBANCO::DataOk($this->mDT_EXPIRACAO_CNH);
        $sql .= " , " . cBANCO::DataOk($this->mDT_EXPIRACAO_CTPS);
        $sql .= " , " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai);
        $sql .= " , " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae);
        $sql .= ")";
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mNU_CANDIDATO = mysql_insert_id();
    }

    /**
     * Atualiza situação do candidato pela CrewList
     */
    public function AtualizeCrewList() {
        $sql = " update CANDIDATO ";
        $sql .= "   set NU_ORDEM_CREWLIST		= " . cBANCO::InteiroOk($this->mNU_ORDEM_CREWLIST);
        $sql .= "     , FL_EMBARCADO			= " . cBANCO::SimNaoOk($this->mFL_EMBARCADO);
        $sql .= "     , FL_INATIVO				= " . cBANCO::SimNaoOk($this->mFL_INATIVO);
        $sql .= "     , NU_EMBARCACAO_PROJETO	= " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= "     , TE_OBSERVACAO_CREWLIST	= " . cBANCO::StringOk($this->mTE_OBSERVACAO_CREWLIST);
        $sql .= " where NU_CANDIDATO			= " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Atualiza o CPF da instância
     */
    public function AtualizeCPF() {
        $sql = " update CANDIDATO ";
        $sql .= "   set NU_CPF       		= " . cBANCO::StringOk($this->mNU_CPF);
        $sql .= " where NU_CANDIDATO 		= " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Atualiza os campos da CNH da instância
     */
    public function AtualizeCNH() {
        $sql = " update CANDIDATO ";
        $sql .= "   set NU_CNH       		= " . cBANCO::StringOk($this->mNU_CNH);
        $sql .= "     , DT_EXPIRACAO_CNH	= " . cBANCO::DataOk($this->mDT_EXPIRACAO_CNH);
        $sql .= " where NU_CANDIDATO 		=  " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Atualiza os campos de CTPS da instância
     */
    public function AtualizeCTPS() {
        $sql = " update CANDIDATO ";
        $sql .= "   set NU_CTPS      		= " . cBANCO::StringOk($this->mNU_CTPS);
        $sql .= "     , DT_EXPIRACAO_CTPS	= " . cBANCO::DataOk($this->mDT_EXPIRACAO_CTPS);
        $sql .= " where NU_CANDIDATO 		=  " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Atualiza o RNE
     */
    public function AtualizeRNE($controller, $metodo, $cd_usuario) {
        $sql = " update CANDIDATO ";
        $sql .= "   set NU_RNE      		= " . cBANCO::StringOk($this->mNU_RNE);
        $sql .= "     , CO_USU_ULT_ALTERACAO      		= " . cBANCO::ChaveOk($cd_usuario);
        $sql .= "     , log_tx_controller      		= " . cBANCO::StringOk($controller);
        $sql .= "     , log_tx_metodo      		= " . cBANCO::StringOk($metodo);
        $sql .= " where NU_CANDIDATO 		=  " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function QtdProcessosOrfaos() {
        $sql = "select ifnull(count(*), 0) qtd ";
        $sql.= "  from vProcesso ";
        $sql.= " where cd_candidato = " . $this->mNU_CANDIDATO;
        $sql.= "   and codigo_processo_mte is null";
        $sql.= "   and codigo_processo_prorrog is null";
        $sql.= "   and (fl_vazio = 0 or fl_vazio is null)";
        $rs = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $qtd = 0;
        if ($rw = mysql_fetch_array($rs)) {
            $qtd = $rw[0];
        }
        return $qtd;
    }

    public function AtualizeEmpresaEmbarcacaoProjeto($pcd_usuario = '', $pmsg = 'Atualizou empresa e embarcação') {
        if ($pcd_usuario == '') {
            $pcd_usuario = cSESSAO::$mcd_usuario;
        }
        $sql = " update CANDIDATO ";
        $sql .= "   set NU_EMPRESA				= " . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= "     , NU_EMBARCACAO_PROJETO	= " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= " where NU_CANDIDATO 		=  " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->LogaHistorico($pcd_usuario, $pmsg);
    }

    /**
     * Atualiza o visto atual do candidato
     * @param type $pCodigo informe um codigo de um processo_mte, 'null' para forçar nenhum ou '' para atualizar para o código mais atual do candidato.
     */
    public function AtualizeVistoAtual($pCodigo = '') {
        if ($pCodigo == '') {
            // Obter o visto mais recente
            $sql = "select ifnull(max(codigo), 'null') codigo
					 from processo_mte
					where cd_candidato = " . $this->mNU_CANDIDATO . "
					  and fl_vazio=0
					  and nu_servico not in (49, 31)";
            $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            if ($rw = $rs->fetch()) {
                $pCodigo = $rw['codigo'];
            } else {
                $pCodigo = 'null';
            }
        }

        $this->mcodigo_processo_mte_atual = $pCodigo;
        $sql = "update CANDIDATO ";
        $sql.= "   set codigo_processo_mte_atual = " . $this->mcodigo_processo_mte_atual;
        $sql.= " where NU_CANDIDATO              = " . $this->mNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);

        if ($pCodigo == 'null') {
            //
            $sql = "update processo_mte ";
            $sql.= "   set fl_visto_atual = 0";
            $sql.= " where cd_candidato   = " . $this->mNU_CANDIDATO;
            cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        } else {
            $sql = "update processo_mte ";
            $sql.= "   set fl_visto_atual = 1";
            $sql.= "     , fl_vazio = 0";
            $sql.= " where cd_candidato   = " . $this->mNU_CANDIDATO;
            $sql.= "   and codigo         = " . $pCodigo;
            cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);

            $sql = "update processo_mte ";
            $sql.= "   set fl_visto_atual = 0";
            $sql.= " where cd_candidato   = " . $this->mNU_CANDIDATO;
            $sql.= "   and codigo        <> " . $pCodigo;
            cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        }
    }

    /**
     * Cria um visto atual para candidato caso ele não tenha
     * @param cORDEMSERVICO $pOS	Ordem de serviço que está solicitando o visto atual para adicionar algum processo
     */
    public function CriaVistoAtualSeNaoHouver($pOS = '') {
        if ($this->mcodigo_processo_mte_atual == '' || $this->mcodigo_processo_mte_atual == 0) {
            $xmte = new cprocesso_mte();
            if (is_object($pOS)) {
//				$xmte->mid_solicita_visto = $pOS->mID_SOLICITA_VISTO;
                $xmte->mNU_EMPRESA = $pOS->mNU_EMPRESA;
                $xmte->mNU_EMBARCACAO_PROJETO = $pOS->mNU_EMBARCACAO_PROJETO;
                $xmte->mcd_candidato = $this->mNU_CANDIDATO;
                $xmte->mobservacao = '(Processo criado automaticamente na criação da OS ' . $pOS->mNU_SOLICITACAO . ' pois o candidato não possuia um visto atual)';
                $xmte->Inclua_ProcessoOS();
            } else {
                $xmte->mNU_EMPRESA = $this->mNU_EMPRESA;
                $xmte->mNU_EMBARCACAO_PROJETO = $this->mNU_EMBARCACAO_PROJETO;
                $xmte->mcd_candidato = $this->mNU_CANDIDATO;
                $xmte->Salve();
            }
            $this->mcodigo_processo_mte_atual = $xmte->mcodigo;
        }
    }

    public function GeraLoginParaNovoCandidato() {
        if (trim($this->mNO_ULTIMO_NOME) != '') {
            $loginOrig = strtolower(substr(substr($this->mNO_PRIMEIRO_NOME, 0, 1) . $this->mNO_ULTIMO_NOME, 0, 18));
            $loginOrig = ereg_replace("[^a-zA-Z0-9]", "", $loginOrig);
        }
        $login = $loginOrig;
        $loginEncontrado = false;
        $ind = 0;
        $this->mNO_SENHA = $this->GeraSenhaAleatoria();
        while (!$loginEncontrado) {
            $sql = "select ifnull(count(*), 0) from CANDIDATO where NO_LOGIN = '" . $login . "'";
            $loginRes = conectaQuery($sql, 'inicializarLoginSenha.php->BuscaLogin');
            if ($qtdlogin = mysql_fetch_array($loginRes)) {
                if (intval($qtdlogin[0]) == 0) {
                    $loginEncontrado = true;
                    $sql = "UPDATE CANDIDATO set NO_LOGIN = '" . $login . "', NO_SENHA = '" . $this->mNO_SENHA . "' where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
                    executeQuery($sql, __CLASS__ . __FUNCTION__);
                } else {
                    $ind++;
                    $login = $loginOrig . $ind;
                }
            }
        }
    }

    public function SalvarAcesso() {
        $sql = "select ifnull(FL_ATUALIZACAO_HABILITADA, 0) FL_ATUALIZACAO_HABILITADA"
                . " from CANDIDATO "
                . " where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        if ($rw = mysql_fetch_array($res)) {
            $FL_ATUALIZACAO_HABILITADA_ANT = $rw['FL_ATUALIZACAO_HABILITADA'];
        }

        $sql = "update CANDIDATO ";
        $sql .= " set email_pessoal = " . cBANCO::StringOk($this->memail_pessoal);
        $sql .= "   , NO_SENHA = " . cBANCO::StringOk($this->mNO_SENHA);
        $sql .= "   , FL_ATUALIZACAO_HABILITADA = " . cBANCO::SimNaoOk($this->mFL_ATUALIZACAO_HABILITADA);
        $sql .= "   , FL_HABILITAR_CV = " . cBANCO::SimNaoOk($this->mFL_HABILITAR_CV);
        $sql .= "   , id_status_edicao = " . cBANCO::ChaveOk($this->mid_status_edicao);
        $sql .= " where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function CriaCandidatoCliente() {
        $sql = "insert into candidato_tmp (
					NU_CANDIDATO
					,NO_PRIMEIRO_NOME
					,NO_NOME_MEIO
					,NO_ULTIMO_NOME
					,NOME_COMPLETO
					,NO_EMAIL_CANDIDATO
					,NO_ENDERECO_RESIDENCIA
					,NO_CIDADE_RESIDENCIA
					,CO_PAIS_RESIDENCIA
					,NU_TELEFONE_CANDIDATO
					,NO_EMPRESA_ESTRANGEIRA
					,NO_ENDERECO_EMPRESA
					,NO_CIDADE_EMPRESA
					,CO_PAIS_EMPRESA
					,NU_TELEFONE_EMPRESA
					,NO_PAI
					,NO_MAE
					,CO_NACIONALIDADE
					,CO_NIVEL_ESCOLARIDADE
					,CO_ESTADO_CIVIL
					,CO_SEXO
					,DT_NASCIMENTO
					,NO_LOCAL_NASCIMENTO
					,NU_PASSAPORTE
					,DT_EMISSAO_PASSAPORTE
					,DT_VALIDADE_PASSAPORTE
					,CO_PAIS_EMISSOR_PASSAPORTE
					,NU_RNE
					,CO_PROFISSAO_CANDIDATO
					,TE_TRABALHO_ANTERIOR_BRASIL
					,NU_CPF
					,DT_CADASTRAMENTO
					,CO_USU_CADASTAMENTO
					,DT_ULT_ALTERACAO
					,CO_USU_ULT_ALTERACAO
					,NO_ENDERECO_ESTRANGEIRO
					,NO_CIDADE_ESTRANGEIRO
					,CO_PAIS_ESTRANGEIRO
					,NU_TELEFONE_ESTRANGEIRO
					,BO_CADASTRO_MINIMO_OK
					,NU_EMPRESA
					,NU_EMBARCACAO_PROJETO
					,NU_CTPS
					,NU_CNH
					,DT_EXPIRACAO_CNH
					,DT_EXPIRACAO_CTPS
					,CO_FUNCAO
					,TE_DESCRICAO_ATIVIDADES
					,VA_REMUNERACAO_MENSAL
					,VA_REMUNERACAO_MENSAL_BRASIL
					,NO_SEAMAN
					,DT_VALIDADE_SEAMAN
					,DT_EMISSAO_SEAMAN
					,CO_PAIS_SEAMAN
					,loca_id
					,co_pais_nacionalidade_pai
					,co_pais_nacionalidade_mae
					)
				select
					NU_CANDIDATO
					,NO_PRIMEIRO_NOME
					,NO_NOME_MEIO
					,NO_ULTIMO_NOME
					,NOME_COMPLETO
					,NO_EMAIL_CANDIDATO
					,NO_ENDERECO_RESIDENCIA
					,NO_CIDADE_RESIDENCIA
					,CO_PAIS_RESIDENCIA
					,NU_TELEFONE_CANDIDATO
					,NO_EMPRESA_ESTRANGEIRA
					,NO_ENDERECO_EMPRESA
					,NO_CIDADE_EMPRESA
					,CO_PAIS_EMPRESA
					,NU_TELEFONE_EMPRESA
					,NO_PAI
					,NO_MAE
					,CO_NACIONALIDADE
					,CO_NIVEL_ESCOLARIDADE
					,CO_ESTADO_CIVIL
					,CO_SEXO
					,DT_NASCIMENTO
					,NO_LOCAL_NASCIMENTO
					,NU_PASSAPORTE
					,DT_EMISSAO_PASSAPORTE
					,DT_VALIDADE_PASSAPORTE
					,CO_PAIS_EMISSOR_PASSAPORTE
					,NU_RNE
					,CO_PROFISSAO_CANDIDATO
					,TE_TRABALHO_ANTERIOR_BRASIL
					,NU_CPF
					,DT_CADASTRAMENTO
					,CO_USU_CADASTAMENTO
					,DT_ULT_ALTERACAO
					,CO_USU_ULT_ALTERACAO
					,NO_ENDERECO_ESTRANGEIRO
					,NO_CIDADE_ESTRANGEIRO
					,CO_PAIS_ESTRANGEIRO
					,NU_TELEFONE_ESTRANGEIRO
					,BO_CADASTRO_MINIMO_OK
					,NU_EMPRESA
					,NU_EMBARCACAO_PROJETO
					,NU_CTPS
					,NU_CNH
					,DT_EXPIRACAO_CNH
					,DT_EXPIRACAO_CTPS
					,CO_FUNCAO
					,TE_DESCRICAO_ATIVIDADES
					,VA_REMUNERACAO_MENSAL
					,VA_REMUNERACAO_MENSAL_BRASIL
					,NO_SEAMAN
					,DT_VALIDADE_SEAMAN
					,DT_EMISSAO_SEAMAN
					,CO_PAIS_SEAMAN
					,loca_id
					,co_pais_nacionalidade_pai
					,co_pais_nacionalidade_mae
					from CANDIDATO where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function Cursor_ExperienciaProfissional() {
        $sql = "select * from experiencia_profissional where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        return $res;
    }

    public function Cursor_Cursos() {
        $sql = "select * from curso c left join tipo_ensino tp on tp.tpen_id = c.tpen_id where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        return $res;
    }

    public function AdicionarCurso($ptpen_id, $pcurs_no_cidade, $pcurs_no_periodo, $pcurs_no_instituicao, $pcurs_no_grau) {
        $sql = "insert into curso (NU_CANDIDATO, tpen_id, curs_no_cidade, curs_no_periodo, curs_no_instituicao, curs_no_grau) ";
        $sql .= " values(";
        $sql .= " " . $this->mNU_CANDIDATO;
        $sql .= ", " . cBANCO::StringOk($ptpen_id);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_cidade);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_periodo);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_instituicao);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_grau);
        $sql .= ") ";
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function AdicionarExpProf($ptpen_id, $pcurs_no_cidade, $pcurs_no_periodo, $pcurs_no_instituicao, $pcurs_no_grau) {
        $sql = "insert into experiencia_profissional (NU_CANDIDATO, epro_no_companhia, epro_no_funcao, epro_no_periodo, epro_tx_atribuicoes) ";
        $sql .= " values(";
        $sql .= " " . $this->mNU_CANDIDATO;
        $sql .= ", " . cBANCO::StringOk($ptpen_id);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_cidade);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_periodo);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_instituicao);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_grau);
        $sql .= ") ";
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function ListeCrewList($pFiltros, $pOrdenacao) {
        $sql = '';
        $pFiltros['NU_EMPRESA']->mQualificadorFiltro = "C";
        $pFiltros['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = "C";
        if (is_object($pFiltros['cd_usuario'])) {
            $pFiltros['cd_usuario']->mQualificadorFiltro = "EMBP";
        }
        if (intval($pFiltros['TIPO_CANDIDATO']->mValor) == 1) {
            $pFiltros['TIPO_CANDIDATO']->mWhere = " MTE.codigo is not null";
        } elseif (intval($pFiltros['TIPO_CANDIDATO']->mValor) == 2) {
            $pFiltros['TIPO_CANDIDATO']->mWhere = " MTE.codigo is null";
        }
        $sql .= cBANCO::WhereFiltros($pFiltros);
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        $cursor = $this->CursorCandidatosPorEmbarcacao($pFiltros, $pOrdem);
        return $cursor;
    }

    //TODO:Refatorar para disponibilizeDrAoCliente
    //public function VerificarCandidatoExistente($pNomeCompleto, $pNomeMae, $pDataNasc){
    public function DisponibilizeSeParaCliente($pNomeCompleto, $pNomeMae, $pDataNasc, $pcd_empresa_usuario) {
        //Verifica se o candidato existe
        $this->RecuperesePeloNome($pNomeCompleto, $pNomeMae, $pDataNasc);
        if ($this->mNU_CANDIDATO > 0) {
            $embp = new cEMBARCACAO_PROJETO();
            if ($this->mNU_EMBARCACAO_PROJETO != '') {
                $embp->mNU_EMPRESA = $this->mNU_EMPRESA;
                $embp->mNU_EMBARCACAO_PROJETO = $this->mNU_EMBARCACAO_PROJETO;
                $embp->RecupereSe();
            }
            if ($this->mNU_EMPRESA == $pcd_empresa_usuario) {
                if ($this->mFL_ATUALIZACAO_HABILITADA == cCANDIDATO::atuBLOQUEADA) {
                    if (intval($this->mFL_INATIVO) == 1 || intval($embp->membp_fl_ativo) == 0) {
                        $this->DisponibilizeCadastroParaCliente();
                        $this->AtualizeCadastroParaCliente();
                        $this->LibereInativoParaClienteDaMesmaEmpresa();
                        if (intval($embp->membp_fl_ativo) == 0) {
                            $sql = "update candidato_tmp set ";
                            $sql.= "       NU_EMBARCACAO_PROJETO = null";
                            $sql.= " where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
                            cAMBIENTE::$db_pdo->exec($sql);
                        }
                    }
                }
            } else {
                if ($this->mFL_INATIVO == 1 || $embp->membp_fl_ativo == 0) {
                    if ($this->mFL_ATUALIZACAO_HABILITADA == cCANDIDATO::atuBLOQUEADA) {
                        $this->DisponibilizeCadastroParaCliente();
                        $this->AtualizeCadastroParaCliente();
                        $this->LibereInativoParaClienteDeOutraEmpresa($pcd_empresa_usuario);
                    }
                } else {
                    if (cHTTP::getLang() == 'pt_br') {
                        $msg = 'Este candidato já está cadastrado no sistema em outra empresa. Para preservar o sigilo das informações dos nossos clientes, uma empresa não pode acessar informações de candidatos que estão em outra. Para maiores informações entre em contato com a Mundivisas.';
                    } else {
                        $msg = 'This person is already in our database, filed under another company. To preseve our customers data, one company is not allowed to access candidate data from another. For further information please contact Mundivisas.';
                    }
                    throw new exception($msg);
                }
            }
        } else {
            $this->Criar_cliente($pcd_empresa_usuario, $pNomeCompleto, $pNomeMae, $pDataNasc);
        }
    }

    public function DisponibilizeDRAoCandidato($pNomeCompleto, $pNomeMae, $pDataNasc) {
        //Verifica se o candidato existe
        $this->RecuperesePeloNomeDR($pNomeCompleto, $pNomeMae, $pDataNasc);
        if ($this->mNU_CANDIDATO > 0) {

        } else {
            $this->Criar_clienteDR($pNomeCompleto, $pNomeMae, $pDataNasc);
        }
    }

    public function RecuperesePeloNomeSo($pNomeCompleto) {
        $sql = "select * from CANDIDATO";
        $sql.= " where NOME_COMPLETO like " . cBANCO::StringOk($pNomeCompleto);
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if ($rs['NU_CANDIDATO'] != '') {
                $this->CarreguePropriedadesPeloRecordset($rs);
        }
    }

    public function RecuperesePeloNomeDR($pNomeCompleto, $pNomeMae, $pDataNasc) {
        cAMBIENTE::InicializeAmbiente('DR');
        $pNomeCompleto = trim(str_replace(' ', '%', $pNomeCompleto));
        $pNomeCompleto = str_replace(';', '', $pNomeCompleto);
        $pNomeCompleto = str_replace("'", '', $pNomeCompleto);
        $pNomeCompleto = substr($pNomeCompleto, 0, 500);
        $pNomeMae = trim(str_replace(' ', '%', $pNomeMae));
        $pNomeMae = str_replace(';', '', $pNomeMae);
        $pNomeMae = str_replace("'", '', $pNomeMae);
        $pNomeMae = str_replace(';', '', $pNomeMae);
        $pNomeMae = substr($pNomeMae, 0, 500);
        $sql = "select * from dr.CANDIDATO";
        $sql.= " where NOME_COMPLETO like " . cBANCO::StringOk($pNomeCompleto);
        $sql.= "   and DT_NASCIMENTO    = " . cBANCO::DataOk($pDataNasc);
        $sql.= "   and NO_MAE        like " . cBANCO::StringOk($pNomeMae);
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                cBANCO::CarreguePropriedades($rs, $this);
                $this->mDT_EXPIRACAO_CNH = cBANCO::DataFmt($rs["DT_EXPIRACAO_CNH"]);
                $this->mDT_EXPIRACAO_CTPS = cBANCO::DataFmt($rs["DT_EXPIRACAO_CTPS"]);
            }
        }
    }


    //TODO: Adicionar a empresa e a embarcação na comparação de atualização de cadastro.
    public function LibereInativoParaClienteDeOutraEmpresa($pNU_EMPRESA) {
        $sql = "update CANDIDATO set
					FL_INATIVO = 0
				  , FL_ATUALIZACAO_HABILITADA = 1
			  where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
        $sql = "update candidato_tmp set
				    NU_EMPRESA = " . $pNU_EMPRESA . "
				  , NU_EMBARCACAO_PROJETO = null
				  , VA_REMUNERACAO_MENSAL = null
				  , VA_REMUNERACAO_MENSAL_BRASIL = null
				  , CO_FUNCAO = null
			  where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function LibereInativoParaClienteDaMesmaEmpresa() {
        $this->mFL_INATIVO = 0;
        $this->mFL_ATUALIZACAO_HABILITADA = cCANDIDATO::atuEM_ATUALIZACAO;
        $sql = "update CANDIDATO set ";
        $sql.= "       FL_INATIVO = 0";
        $sql.= "     , FL_ATUALIZACAO_HABILITADA = " . cCANDIDATO::atuEM_ATUALIZACAO;
        $sql.= " where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function Cursor_CAND_CLIE_LIST($pFiltros, $pOrdenacao) {
        $pFiltros['NU_EMPRESA']->mQualificadorFiltro = "C";
        $pFiltros['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = "C";
        $pFiltros['NOME_COMPLETO']->mQualificadorFiltro = "C";

        $where = cBANCO::WhereFiltros($pFiltros);
        $sql = "select distinct
                    null as nu_candidato_tmp,
                    C.NU_CANDIDATO,
                    C.NOME_COMPLETO,
                    C.NU_PASSAPORTE,
                    C.NU_RNE,
                    C.NU_CPF,
                    C.codigo_processo_mte_atual,
                    1 as FL_EDICAO_CONCLUIDA,
                    C.FL_ATUALIZACAO_HABILITADA,
                    C.DT_VALIDADE_PASSAPORTE DT_VALIDADE_PASSAPORTE_ORIG ,
                    DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE ,
                    DATE_FORMAT(MAX_PRAZO_ESTADA.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA,
                    MAX_PRAZO_ESTADA.dt_prazo_estada DT_PRAZO_ESTADA_ORIG,
                    datediff(MAX_PRAZO_ESTADA.dt_prazo_estada, now()) DIAS_VENCIMENTO,

                    MTE.nu_processo NU_PROCESSO_MTE,
                    MTE.prazo_solicitado PRAZO_SOLICITADO,
                    DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO ,
                    DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO,
                    MTE.dt_deferimento DT_DEFERIMENTO_ORIG,

                    PN.NO_NACIONALIDADE,
                    PN.NO_NACIONALIDADE_EM_INGLES,
                    F.NO_FUNCAO,
                    S.NO_SERVICO_RESUMIDO TIPO_VISTO,
                    RC.NO_REPARTICAO_CONSULAR CONSULADO,
                    EP.NO_EMBARCACAO_PROJETO
            from CANDIDATO C
            left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual
                            and MTE.cd_candidato = C.NU_CANDIDATO
                            and MTE.fl_vazio = 0
            left join (select cd_candidato, codigo_processo_mte, max(dt_prazo_estada) dt_prazo_estada from processo_regcie
                                    where codigo_processo_mte is not null
                                    group by cd_candidato, codigo_processo_mte) as MAX_PRAZO_ESTADA
                            on MAX_PRAZO_ESTADA.codigo_processo_mte = MTE.codigo
                            and MAX_PRAZO_ESTADA.cd_candidato = MTE.cd_candidato
            left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
            left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
            left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
            left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = MTE.cd_reparticao
            left join EMBARCACAO_PROJETO EP on EP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = C.NU_EMPRESA
            where ifnull(C.FL_INATIVO,0) = 0
              and (EP.embp_fl_ativo = 1 or C.NU_EMBARCACAO_PROJETO is null)
            " . $where;
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        return $res;
    }

    public function candidatosParaCliente($pFiltros, $pOrdenacao) {
        $pFiltros['NU_EMPRESA']->mQualificadorFiltro = "C";
        $pFiltros['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = "C";
        $pFiltros['NOME_COMPLETO']->mQualificadorFiltro = "C";

        $where = cBANCO::WhereFiltros($pFiltros);
        print '<!-- where=' . $where . '-->';
        $sql = "select distinct
                    tmp.nu_candidato_tmp,
                    C.NU_CANDIDATO,
                    coalesce(tmp.nu_candidato, C.NOME_COMPLETO,
                    C.NU_PASSAPORTE,
                    C.NU_RNE,
                    C.NU_CPF,
                    C.codigo_processo_mte_atual,
                    1 as FL_EDICAO_CONCLUIDA,
                    C.FL_ATUALIZACAO_HABILITADA,
                    C.DT_VALIDADE_PASSAPORTE DT_VALIDADE_PASSAPORTE_ORIG ,
                    DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE ,
                    DATE_FORMAT(MAX_PRAZO_ESTADA.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA,
                    MAX_PRAZO_ESTADA.dt_prazo_estada DT_PRAZO_ESTADA_ORIG,
                    datediff(MAX_PRAZO_ESTADA.dt_prazo_estada, now()) DIAS_VENCIMENTO,

                    MTE.nu_processo NU_PROCESSO_MTE,
                    MTE.prazo_solicitado PRAZO_SOLICITADO,
                    DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO ,
                    DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO,
                    MTE.dt_deferimento DT_DEFERIMENTO_ORIG,

                    PN.NO_NACIONALIDADE,
                    PN.NO_NACIONALIDADE_EM_INGLES,
                    F.NO_FUNCAO,
                    S.NO_SERVICO_RESUMIDO TIPO_VISTO,
                    RC.NO_REPARTICAO_CONSULAR CONSULADO,
                    EP.NO_EMBARCACAO_PROJETO
            from CANDIDATO C
            left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual
                            and MTE.cd_candidato = C.NU_CANDIDATO
                            and MTE.fl_vazio = 0
            left join (select cd_candidato, codigo_processo_mte, max(dt_prazo_estada) dt_prazo_estada from processo_regcie
                                    where codigo_processo_mte is not null
                                    group by cd_candidato, codigo_processo_mte) as MAX_PRAZO_ESTADA
                            on MAX_PRAZO_ESTADA.codigo_processo_mte = MTE.codigo
                            and MAX_PRAZO_ESTADA.cd_candidato = MTE.cd_candidato
            left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
            left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
            left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
            left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = MTE.cd_reparticao
            left join EMBARCACAO_PROJETO EP on EP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = C.NU_EMPRESA
            where ifnull(C.FL_INATIVO,0) = 0
              and (EP.embp_fl_ativo = 1 or C.NU_EMBARCACAO_PROJETO is null)
              and C.NU_CANDIDATO not in (select ifnull(NU_CANDIDATO,0) from candidato_tmp)
            " . $where;
        $sql .= " union
        select distinct
                C.nu_candidato_tmp,
                C.NU_CANDIDATO,
                concat(C.NOME_COMPLETO, ' (new)') NOME_COMPLETO,
                C.NU_PASSAPORTE,
                C.NU_RNE,
                C.NU_CPF,
                null as codigo_processo_mte_atual,
                C.FL_EDICAO_CONCLUIDA,
                CORIG.FL_ATUALIZACAO_HABILITADA,
                C.DT_VALIDADE_PASSAPORTE DT_VALIDADE_PASSAPORTE_ORIG ,
                DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE ,
                DATE_FORMAT(MAX_PRAZO_ESTADA.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA,
                MAX_PRAZO_ESTADA.dt_prazo_estada DT_PRAZO_ESTADA_ORIG,
                datediff(MAX_PRAZO_ESTADA.dt_prazo_estada, now()) DIAS_VENCIMENTO,

                MTE.nu_processo NU_PROCESSO_MTE,
                MTE.prazo_solicitado PRAZO_SOLICITADO,
                DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO ,
                DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO,
                MTE.dt_deferimento DT_DEFERIMENTO_ORIG,

                PN.NO_NACIONALIDADE,
                PN.NO_NACIONALIDADE_EM_INGLES,
                F.NO_FUNCAO,
                S.NO_SERVICO_RESUMIDO TIPO_VISTO,
                RC.NO_REPARTICAO_CONSULAR CONSULADO,
                EP.NO_EMBARCACAO_PROJETO
                from candidato_tmp C
                join CANDIDATO CORIG on CORIG.NU_CANDIDATO = C.NU_CANDIDATO
                left join processo_mte MTE on MTE.codigo = CORIG.codigo_processo_mte_atual
                        and MTE.cd_candidato = CORIG.NU_CANDIDATO
                        and MTE.fl_vazio = 0
                left join (select cd_candidato, codigo_processo_mte, max(dt_prazo_estada) dt_prazo_estada from processo_regcie
                                        where codigo_processo_mte is not null
                                        group by cd_candidato, codigo_processo_mte) as MAX_PRAZO_ESTADA
                        on MAX_PRAZO_ESTADA.codigo_processo_mte = MTE.codigo
                        and MAX_PRAZO_ESTADA.cd_candidato = MTE.cd_candidato
                left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
                left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
                left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
                left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = MTE.cd_reparticao
                left join EMBARCACAO_PROJETO EP on EP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = C.NU_EMPRESA
                where 1=1
                and C.NU_CANDIDATO Not in (select NU_CANDIDATO from CANDIDATO where 1=1 " . $where . ")
            " . $where;
        print '<!-- sql=' . $sql . '-->';
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        return $res;
    }

    public function CursorCandidato_cliente($pNU_CANDIDATO) {
        $sql = "select * from vcandidato_tmp_cons where NU_CANDIDATO = " . $pNU_CANDIDATO;
        if ($res = conectaQuery($sql, __CLASS__)) {
            if ($rs = mysql_fetch_array($res)) {
                return $rs;
            } else {
                $this->mNU_CANDIDATO = $pNU_CANDIDATO;
                $this->CriaCandidatoCliente();
                $sql = "select * from vcandidato_tmp_cons where NU_CANDIDATO = " . $pNU_CANDIDATO;
                if ($res = conectaQuery($sql, __CLASS__)) {
                    if ($rs = mysql_fetch_array($res)) {
                        return $rs;
                    }
                }
            }
        }
    }

    public function CursorArquivos($pIncluirArquivosOS = true, $acesso_cliente=false) {
        $sql = "select ifnull(A_CAND.NU_EMPRESA, 0) NU_EMPRESA, A_CAND.NU_CANDIDATO"
                . ", A_CAND.NU_SEQUENCIAL"
                . ", A_CAND.NO_ARQUIVO"
                . ", A_CAND.NO_ARQ_ORIGINAL"
                . ", A_CAND.TP_ARQUIVO"
                . ", A_CAND.DT_INCLUSAO"
                . ", coalesce(ta.NO_TIPO_ARQUIVO, A_CAND.NO_ARQ_ORIGINAL) NO_TIPO_ARQUIVO "
                . ", ta.CO_TIPO_ARQUIVO"
                . ", A_CAND.id_solicita_visto"
                . ", sv.NU_SOLICITACAO"
                . ", A_CAND.NO_ARQ_ORIGINAL"
                . ", A_CAND.FL_ADICIONADO_PELO_CANDIDATO";
        $sql .= "  from ARQUIVOS A_CAND";
        $sql .= "  left join TIPO_ARQUIVO ta on ta.ID_TIPO_ARQUIVO=A_CAND.TP_ARQUIVO";
        $sql .= "  left join solicita_visto sv on sv.id_solicita_visto = A_CAND.ID_SOLICITA_VISTO";
        $sql .= "  where NU_SEQUENCIAL>0";
        $sql .= "   and NU_CANDIDATO= " . $this->mNU_CANDIDATO;
        if ($acesso_cliente){
            $sql .=" and coalesce(ta.fl_disponivel_clientes, 0) = 1";
        }
        $sql .= "  union";
        $sql .= "  select ifnull(A_SOL.NU_EMPRESA, 0) NU_EMPRESA, 0 NU_CANDIDATO, A_SOL.NU_SEQUENCIAL, A_SOL.NO_ARQUIVO, A_SOL.NO_ARQ_ORIGINAL, A_SOL.TP_ARQUIVO, A_SOL.DT_INCLUSAO, TA.NO_TIPO_ARQUIVO , TA.CO_TIPO_ARQUIVO, sv.id_solicita_visto, sv.NU_SOLICITACAO, A_SOL.NO_ARQ_ORIGINAL, A_SOL.FL_ADICIONADO_PELO_CANDIDATO ";
        $sql .= "    FROM AUTORIZACAO_CANDIDATO AC";
        $sql .= "     JOIN ARQUIVOS A_SOL on A_SOL.ID_SOLICITA_VISTO = AC.id_solicita_visto";
        $sql .= "    JOIN solicita_visto sv on sv.id_solicita_visto = AC.id_solicita_visto";
        $sql .= "    LEFT JOIN TIPO_ARQUIVO TA ON TA.ID_TIPO_ARQUIVO = A_SOL.TP_ARQUIVO";
        $sql .= "  WHERE coalesce(A_SOL.NU_CANDIDATO,0) = 0";
        $sql .= "  AND AC.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        if ($acesso_cliente){
            $sql .=" and coalesce(ta.fl_disponivel_clientes, 0) = 1";
        }
        $sql .= " order by DT_INCLUSAO desc ,TP_ARQUIVO,NO_ARQUIVO ";
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    //TODO:Refatorar para InserirPeloCliente
    //TODO:Refatorar criação do tmp para a rotina de sincronização do TMP.
    public function Criar_cliente($pNU_EMPRESA, $pNOME_COMPLETO, $pNO_MAE, $pDT_NASCIMENTO) {
        $this->QuebreNomeCompleto($pNOME_COMPLETO);
        $this->mNO_MAE = $pNO_MAE;
        $this->mDT_NASCIMENTO = $pDT_NASCIMENTO;
        $this->mNU_EMPRESA = $pNU_EMPRESA;
        $sql = "insert into CANDIDATO (NU_EMPRESA, NOME_COMPLETO, NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME, NO_MAE, DT_NASCIMENTO,DT_CADASTRAMENTO,CO_USU_CADASTAMENTO,CO_USU_ULT_ALTERACAO, FL_ATUALIZACAO_HABILITADA) values(";
        $sql .= " " . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= "," . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= "," . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_NOME_MEIO);
        $sql .= "," . cBANCO::StringOk($this->mNO_ULTIMO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_MAE);
        $sql .= "," . cBANCO::DataOk($this->mDT_NASCIMENTO);
        $sql .= ", now()";
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= ",1";
        $sql .= ")";
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $this->mNU_CANDIDATO = mysql_insert_id();
        $sql = "insert into candidato_tmp (NU_CANDIDATO, NU_EMPRESA, NOME_COMPLETO, NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME, NO_MAE, DT_NASCIMENTO, FL_EDICAO_CONCLUIDA,DT_CADASTRAMENTO,CO_USU_CADASTAMENTO,CO_USU_ULT_ALTERACAO) values(";
        $sql .= " " . cBANCO::ChaveOk($this->mNU_CANDIDATO);
        $sql .= "," . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= "," . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= "," . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_NOME_MEIO);
        $sql .= "," . cBANCO::StringOk($this->mNO_ULTIMO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_MAE);
        $sql .= "," . cBANCO::DataOk($this->mDT_NASCIMENTO);
        $sql .= ",0";
        $sql .= ", now()";
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= ")";
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function Criar_clienteDR($pNOME_COMPLETO, $pNO_MAE, $pDT_NASCIMENTO) {
        cAMBIENTE::InicializeAmbiente('DR');
        $this->QuebreNomeCompleto($pNOME_COMPLETO);
        $this->mNO_MAE = $pNO_MAE;
        $this->mDT_NASCIMENTO = $pDT_NASCIMENTO;
        $sql = "insert into dr.CANDIDATO (NOME_COMPLETO, NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME, NO_MAE, DT_NASCIMENTO,DT_CADASTRAMENTO,CO_USU_CADASTAMENTO,CO_USU_ULT_ALTERACAO, FL_ATUALIZACAO_HABILITADA)
				values(";
        $sql .= "" . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= "," . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_NOME_MEIO);
        $sql .= "," . cBANCO::StringOk($this->mNO_ULTIMO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_MAE);
        $sql .= "," . cBANCO::DataOk($this->mDT_NASCIMENTO);
        $sql .= ", now()";
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= ",1";
        $sql .= ")";
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    //TODO: Refatorar para BloqueieAtualizExt
    public function FecharParaAtualizacaoDeClientes() {
        $sql = "update candidato_tmp
					set FL_EDICAO_CONCLUIDA = 1
					, DT_CONCLUSAO_CADASTRO_CLIENTE= now()
					" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO =" . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $sql = "update CANDIDATO
					set FL_ATUALIZACAO_HABILITADA = 2
					" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO =" . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        //Notifica ponto focal de que houve alteracao
        $usuario = new cusuarios();
        $usuario->RecupereSe(cSESSAO::$mcd_usuario);
        //TODO Diferenciar mensagem de inclusao de mensagem de alteracao
        $msg = $this->MensagemPadraoAlteracao($usuario->nome, $usuario->nm_email);
        try {
            $this->NotificarPontoFocal($usuario->nm_email, $msg);
        } catch (Exception $e) {

        }
    }

    public function NotificarPontoFocal($pEmailAlterador, $pMsgDescricao) {
        $email = new cEMAIL();
        $empresa = new cEMPRESA();
        $empresa->mNU_EMPRESA = $this->mNU_EMPRESA;
        $resp = $empresa->getPontosFocais();
        $adicionouAlgum = false;
        foreach ($resp as $usuario) {
            if ($usuario->nm_email != '') {
                $email->AdicioneDestinatario($usuario->nm_email);
                $adicionouAlgum = true;
            }
        }
        if ($adicionouAlgum) {
            $msgErro = '';
        } else {
            $empresa->RecuperePeloId();
            $msgErro = '<br/><br/><b><i>(ATENÇÃO: essa mensagem foi encaminhada a você porque não foi definido o responsável pela empresa ' . $empresa->mNO_RAZAO_SOCIAL . ' no sistema. Por favor, providencie que o mesmo seja notificado.)</i></b>';
            $email->AdicioneDestinatario('carlos.engel@mundivisas.com.br');
        }

        $email->setSubject('Dataweb - Alteração em DR: ' . $this->mNOME_COMPLETO);
        $msg = "Prezada(o) " . $usuario->nome . ",<br/><br/>" . $pMsgDescricao . "<br/><br/>Atenciosamente, <br/>Dataweb";
        $msg .= $msgErro;
        $email->setMsg($msg);
        $email->Envie();
    }

    public function MensagemPadraoAlteracao($pNomeAlterador, $pEmailAlterador) {
        $ret = "O cliente <b>" . $pNomeAlterador . " (" . $pEmailAlterador . ")</b> acaba de disponibilizar uma alteração no DR do candidato <b>\"" . $this->mNOME_COMPLETO . "\" (id " . $this->mNU_CANDIDATO . ")</b>.";
        return $ret;
    }

    public function MensagemPadraoInclusao($pNomeAlterador, $pEmailAlterador) {
        $ret = "O cliente <b>" . $pNomeAlterador . " (" . $pEmailAlterador . ")</b> acaba de cadastrar o candidato <b>\"" . $this->mNOME_COMPLETO . "\" (id " . $this->mNU_CANDIDATO . ")</b>.";
        return $ret;
    }

    //TODO: Refatorar para LibereAtualizExt
    public function AbrirParaAtualizacaoDeClientes() {
        $sql = "update candidato_tmp set FL_EDICAO_CONCLUIDA = 0
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO =" . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $sql = "update candidato_tmp set FL_EDICAO_CONCLUIDA = 0
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO =" . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function DisponibilizeCadastroParaCliente() {
        $sql = "select ifnull(max(nu_candidato_tmp), 0) nu_candidato_tmp from candidato_tmp where NU_CANDIDATO =" . $this->mNU_CANDIDATO;
        if ($res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__)) {
            if ($rs = mysql_fetch_array($res)) {
                if (intval($rs['nu_candidato_tmp']) > 0)
                    $this->mnu_candidato_tmp = $rs['nu_candidato_tmp'];
                else {
                    $this->CriaCandidatoCliente();
                }
            }
        }
    }

    public function AtualizeSituacaoCadastral() {
        $sql = "update CANDIDATO set FL_INATIVO = " . cBANCO::SimNaoOk($this->mFL_INATIVO);
        $sql.= $this->SqlLog(__CLASS__, __FUNCTION__);
        $sql .= " where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function AtualizeCadastroOS() {
        $sql = "update CANDIDATO set ";
        $sql.= "  VA_REMUNERACAO_MENSAL = " . cBANCO::ValorOk($this->mVA_REMUNERACAO_MENSAL);
        $sql.= " ,TE_DESCRICAO_ATIVIDADES = " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES);
        $sql.= " ,VA_REMUNERACAO_MENSAL_BRASIL = " . cBANCO::ValorOk($this->mVA_REMUNERACAO_MENSAL_BRASIL);
        $sql.= " ,CO_FUNCAO = " . cBANCO::ChaveOk($this->mCO_FUNCAO);
        $sql.= $this->SqlLog(__CLASS__, __FUNCTION__);
        $sql.= " where NU_CANDIDATO=" . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function ExcluaProcesso($pid_solicita_visto, $pid_tipo_acompanhamento_ant) {
        $processo_ant = cprocesso::FabricaProcesso($pid_tipo_acompanhamento_ant);
        if (intval($pid_solicita_visto) > 0) {
            if ($pid_tipo_acompanhamento_ant == 1) {
                $processo_ant->RecupereSePelaOs($pid_solicita_visto, $this->mNU_CANDIDATO);
                $processo_ant->ExcluaPelaOs($pid_solicita_visto, $this->mNU_CANDIDATO);
                if ($processo_ant->mfl_visto_atual == 1) {
                    $this->AtualizeVistoAtual();
                }
            } else {
                $processo_ant->ExcluaPelaOs($pid_solicita_visto);
            }
        }
    }

    public function CursorCandidatosPorEmbarcacao($pFiltros, $pOrdem) {
        if ($pOrdem == '') {
            $pOrdem = "EMBP.NO_EMBARCACAO_PROJETO, C.NOME_COMPLETO ASC";
        }
        $where = cBANCO::WhereFiltros($pFiltros);

        $sql = $this->sqlCursorRelatorio($where, $pOrdem);
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    public function sqlCursorRelatorio($where, $ordem){
        $sql = "  select distinct
                                C.NOME_COMPLETO
                                ,C.NU_CANDIDATO
                                ,C.NU_PASSAPORTE
                                ,C.NU_RNE
                                ,C.NU_CPF
                                ,C.NU_CTPS
                                ,C.NU_CNH
                                ,DATE_FORMAT(C.DT_EXPIRACAO_CNH,'%d/%m/%Y') DT_EXPIRACAO_CNH
                                ,DATE_FORMAT(C.DT_EXPIRACAO_CTPS,'%d/%m/%Y') DT_EXPIRACAO_CTPS
                                ,C.NU_TELEFONE_ESTRANGEIRO
                                ,C.NO_EMAIL_CANDIDATO
                                ,DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE
                                ,DATE_FORMAT(C.DT_NASCIMENTO,'%d/%m/%Y') DT_NASCIMENTO
                                ,DATE_FORMAT(C.cand_dt_revisao,'%d/%m/%Y') cand_dt_revisao
                                ,C.codigo_processo_mte_atual
								,concat(ifnull(C.NO_ENDERECO_ESTRANGEIRO, ''),' - ', ifnull(C.NO_CIDADE_ESTRANGEIRO, ''), ' - ', ifnull(PE.NO_PAIS, '')) NO_ENDERECO_ESTRANGEIRO_COMPLETO
								,CASE FL_INATIVO
									WHEN 2 THEN 'exclusão solicitada'
									WHEN 1 THEN 'inativo'
									WHEN 0 THEN 'ativo'
									ELSE 'ativo'
								END AS situacao_cadastral
								,C.FL_INATIVO
								,C.cand_fl_revisado
								,CASE C.cand_fl_revisado
									WHEN 2 THEN 'pendente'
									WHEN 1 THEN 'revisado'
									WHEN 0 THEN 'não revisado'
									ELSE 'não revisado'
								END AS revisado
								,C.cand_fl_conferido
								,C.FL_EMBARCADO
								,C.TE_OBSERVACAO_CREWLIST
								,C.NU_EMPRESA
								,C.NU_EMBARCACAO_PROJETO
								,C.NU_ORDEM_CREWLIST

                                ,EMBPP.NO_EMBARCACAO_PROJETO NO_EMBARCACAO_PROJETO_processo

								,empt_tx_descricao

								,MTE.codigo codigo_mte
								,MTE.nu_processo NU_PROCESSO_MTE
								,MTE.prazo_solicitado PRAZO_SOLICITADO
								,DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO
                                ,DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO
                                ,(180 - datediff(now(), MTE.dt_deferimento) ) DIAS_EXPIRACAO_DEFERIMENTO
                                ,MTE.observacao_visto OBSERVACAO_VISTO
                                ,statMTE.NO_STATUS_CONCLUSAO NO_STATUS_CONCLUSAO_MTE

                                ,DATE_FORMAT(COLETA.dt_entrada,'%d/%m/%Y')  DT_ENTRADA
                                ,DATE_FORMAT(COLETA.dt_emissao_visto,'%d/%m/%Y')  DT_EMISSAO
                                ,COLETA.*

                                ,REG.codigo codigo_REG
                                ,DATE_FORMAT(REG.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA
                                ,DATE_FORMAT(REG.dt_validade,'%d/%m/%Y')  DT_VALIDADE_REG
                                ,DATE_FORMAT(REG.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_CIE
                                ,REG.nu_protocolo NU_PROTOCOLO_REG
                                ,datediff(REG.dt_validade,now()) DT_VALIDADE_REG


                                ,PRO.codigo codigo_PRO
                                ,PRO.NU_PROTOCOLO NU_PROTOCOLO_PRO
                                ,DATE_FORMAT(PRO.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_PRO
                                ,DATE_FORMAT(PRO.dt_prazo_pret,'%d/%m/%Y')  DT_PRAZO_PRET_PRO
                                ,DATE_FORMAT(PRO.dt_publicacao_dou,'%d/%m/%Y')  dt_publicacao_dou
                                ,datediff(PRO.dt_validade,now()) DT_VALIDADE_PRO

                                ,REST.codigo codigo_rest
                                ,REST.nu_protocolo nu_protocolo_rest
                                ,date_format(REST.dt_validade, '%d/%m/%Y') dt_validade_rest
                                ,REST.dt_prazo_estada dt_prazo_estada_rest
                                ,datediff(REST.dt_prazo_estada, now()) dt_prazo_estada_rest_diff
                                ,date_format(REST.dt_prazo_estada, '%d/%m/%Y') dt_prazo_estada_rest_fmt
                                ,REST.dt_requerimento dt_requerimento_rest
                                ,date_format(REST.dt_requerimento, '%d/%m/%Y') dt_requerimento_rest_fmt
                                ,(select count(*) from processo_coleta where codigo_processo_mte = MTE.codigo and cd_candidato = C.NU_CANDIDATO ) qtd_coletas

                                ,date_format(PROREG.dt_validade, '%d/%m/%Y') dt_validade_proreg

                                ,PN.NO_NACIONALIDADE
                                ,F.NO_FUNCAO
                                ,S.NO_SERVICO_RESUMIDO TIPO_VISTO
                                ,RC.NO_REPARTICAO_CONSULAR CONSULADO
                                ,AC.VA_RENUMERACAO_MENSAL
                                ,EMBP.NO_EMBARCACAO_PROJETO
                                ,ESC.NO_ESCOLARIDADE
                                ,E.NO_RAZAO_SOCIAL
                                ,coalesce(EC.NO_RAZAO_SOCIAL, E.NO_RAZAO_SOCIAL) NO_RAZAO_SOCIAL_COBRANCA
                                ,u_rev.nome nome_revisao

                                ,concat(d.depf_no_nome,' (',d.depf_cd_uf,')') AS depf_no_nome
                                ,statOs.NO_STATUS_SOL NO_STATUS_CONCLUSAO_PRO
                                , date_format(sol.dt_solicitacao, '%d/%m/%Y') dt_solicitacao
                        from CANDIDATO C
                        left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual and  (MTE.fl_vazio = 0 or MTE.fl_vazio is null)
                        left join AUTORIZACAO_CANDIDATO AC on AC.NU_CANDIDATO = C.NU_CANDIDATO and AC.ID_SOLICITA_VISTO = MTE.id_solicita_visto
                        left join processo_prorrog PRO on PRO.dt_prazo_pret = (select max(dt_prazo_pret) from processo_prorrog  where fl_vazio = 0 and codigo_processo_mte = MTE.codigo and cd_candidato = C.NU_CANDIDATO and  dt_prazo_pret is not null)
                                  and PRO.codigo_processo_mte = MTE.codigo
                                  and PRO.cd_candidato = C.NU_CANDIDATO
                                  and (PRO.fl_vazio = 0 or PRO.fl_vazio is null)
                        left join processo_regcie REG on concat(coalesce(REG.dt_requerimento, '2000-01-01'), substr(concat('0000000000',REG.codigo),-10) ) = (select max(concat(coalesce(dt_requerimento, '2000-01-01'), substr(concat('0000000000',codigo),-10) )) from processo_regcie
                                                                                    where codigo_processo_mte = MTE.codigo
                                                                                    and cd_candidato = C.NU_CANDIDATO
                                                                                    and coalesce(fl_vazio,0) = 0
                                                                                    and coalesce(nu_servico,20) in(20, 85)
                                                                                    )
                                  and REG.codigo_processo_mte = MTE.codigo
                                  and REG.cd_candidato = C.NU_CANDIDATO
                  and coalesce(REG.nu_servico,20) in (20,85)
                                  and coalesce(REG.fl_vazio,0) = 0
                       LEFT JOIN (SELECT cd_candidato,
                                         codigo_processo_mte,
                                         max(dt_validade) dt_validade
                                    FROM processo_regcie
                                   WHERE (fl_vazio = 0 OR fl_vazio IS NULL)
                                         AND nu_servico in (18,20)
                                  GROUP BY cd_candidato, codigo_processo_mte) PROREG
                          ON PROREG.cd_candidato = C.NU_CANDIDATO
                             AND PROREG.codigo_processo_mte = MTE.codigo
                        left join processo_emiscie EMIS on EMIS.dt_emissao = (select max(dt_emissao) from processo_emiscie where codigo_processo_mte = MTE.codigo and cd_candidato = C.NU_CANDIDATO and fl_vazio =0 and dt_emissao is not null)
                                  and EMIS.cd_candidato = C.NU_CANDIDATO
                                  and EMIS.codigo_processo_mte = MTE.codigo
                                  and (EMIS.fl_vazio = 0 or EMIS.fl_vazio is null)
                        left join processo_coleta COLETA  on COLETA.cd_candidato = C.NU_CANDIDATO
                                  and COLETA.codigo_processo_mte = MTE.codigo
                        left join processo_cancel CANC on CANC.dt_processo = (select max(dt_processo) from processo_cancel where codigo_processo_mte = MTE.codigo and cd_candidato = C.NU_CANDIDATO and fl_vazio =0)
                                  and CANC.cd_candidato = C.NU_CANDIDATO
                                  and CANC.codigo_processo_mte = MTE.codigo
                                  and (CANC.fl_vazio = 0 or CANC.fl_vazio is null)
                        left join processo_regcie REST on REST.dt_requerimento = (select max(dt_requerimento) from processo_regcie
                                                                                                                        where
                                                                                                                        (codigo_processo_mte = MTE.codigo or codigo_processo_prorrog in (select codigo from processo_prorrog where codigo_processo_mte = MTE.codigo))
                                                                                                                        and cd_candidato = C.NU_CANDIDATO
                                                                                                                        and (fl_vazio = 0 or fl_vazio is null)
                                                                                                                        and nu_servico = 58
                                                                                                                        and dt_requerimento > ifnull(PRO.dt_requerimento, '1980-01-01')
                                                                                                                        )
                                                                                        and REST.dt_validade = (select max(dt_validade) from processo_regcie
                                                                                                                        where
                                                                                                                        (codigo_processo_mte = MTE.codigo or codigo_processo_prorrog in (select codigo from processo_prorrog where codigo_processo_mte = MTE.codigo))
                                                                                                                        and cd_candidato = C.NU_CANDIDATO
                                                                                                                        and (fl_vazio = 0 or fl_vazio is null)
                                                                                                                        and nu_servico = 58
                                                                                                                        and dt_requerimento =(select max(dt_requerimento) from processo_regcie
                                                                                                                                                                        where
                                                                                                                                                                        (codigo_processo_mte = MTE.codigo or codigo_processo_prorrog in (select codigo from processo_prorrog where codigo_processo_mte = MTE.codigo))
                                                                                                                                                                        and cd_candidato = C.NU_CANDIDATO
                                                                                                                                                                        and (fl_vazio = 0 or fl_vazio is null)
                                                                                                                                                                        and nu_servico = 58
                                                                                                                                                                        and dt_requerimento > ifnull(PRO.dt_requerimento, '1980-01-01')
                                                                                                                                                                        )
                                                                                                                        )
                                          and (REST.codigo_processo_mte = MTE.codigo or REST.codigo_processo_prorrog in (select codigo from processo_prorrog where codigo_processo_mte = MTE.codigo))
                                          and REST.cd_candidato = C.NU_CANDIDATO
                                          and (REST.fl_vazio = 0 or REST.fl_vazio is null)
                                          and REST.nu_servico = 58


            left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
                        left join PAIS_NACIONALIDADE PE on C.CO_PAIS_ESTRANGEIRO = PE.CO_PAIS
                        left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
                        left join solicita_visto sol on sol.id_solicita_visto = MTE.id_solicita_visto
            left join EMPRESA EC on EC.NU_EMPRESA = sol.nu_empresa_requerente
                        left join autorizacao_candidato autc on autc.id_solicita_visto = MTE.id_solicita_visto and autc.nu_candidato = MTE.cd_candidato
                        left join FUNCAO_CARGO F on F.CO_FUNCAO = coalesce(MTE.cd_funcao, autc.CO_FUNCAO_CANDIDATO)
                        left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = coalesce(MTE.cd_reparticao, autc.CO_REPARTICAO_CONSULAR)
                        left join EMPRESA E on E.NU_EMPRESA = C.NU_EMPRESA
                        left join EMBARCACAO_PROJETO EMBP on EMBP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EMBP.NU_EMPRESA = C.NU_EMPRESA
                        left join EMBARCACAO_PROJETO EMBPP on EMBPP.NU_EMBARCACAO_PROJETO = sol.NU_EMBARCACAO_PROJETO and EMBPP.NU_EMPRESA = sol.NU_EMPRESA
                        left join ESCOLARIDADE ESC on ESC.CO_ESCOLARIDADE = C.CO_NIVEL_ESCOLARIDADE
                        left join usuarios u_rev on u_rev.cd_usuario = C.cd_usuario_revisao
                        left join delegacia_pf d ON d.depf_cd_codigo = substr(trim(replace(REG.nu_protocolo, '\'', '')), 1, 5)
                        left join STATUS_CONCLUSAO statPro ON statPro.ID_STATUS_CONCLUSAO = PRO.ID_STATUS_CONCLUSAO
                        left join solicita_visto solPro on solPro.id_solicita_visto = PRO.id_solicita_visto
                        left join STATUS_SOLICITACAO statOs ON statOs.ID_STATUS_SOL= solPro.ID_STATUS_SOL
                        left join STATUS_CONCLUSAO statMte ON statMte.ID_STATUS_CONCLUSAO = MTE.ID_STATUS_CONCLUSAO
                        left join empresa_terceirizada empt ON empt.empt_id = C.empt_id
                        WHERE 1=1 " . $where . "
                        ORDER BY " . $ordem . "
                    ";
        return $sql;
    }

    public function Inative($pcd_usuario, $pmsg = "estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set FL_INATIVO = 1
					, cd_usuario_inativacao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_inativacao = now()
					, cand_dt_revisao = now()
					, cand_fl_revisado = 1
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistorico($pcd_usuario, "Alterado para inativo pelo " . $pmsg);
    }

    public function InativeLote($pcd_usuario, $pLote, $pmsg = "Alterado para ativo pelo estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set FL_INATIVO = 1
					, cd_usuario_inativacao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_inativacao = now()
					, cand_dt_revisao = now()
					, cand_fl_revisado = 1
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO in (" . $pLote . ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistoricoLote($pcd_usuario, $pLote, $pmsg);
    }

    public function Ative($pcd_usuario, $pmsg = "Alterado para ativo pelo estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set FL_INATIVO = 0
					, cd_usuario_ativacao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_ativacao = now()
					, cand_fl_revisado = 1
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_revisao = now()
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistorico($pcd_usuario, $pmsg);
    }

    public function SoliciteExclusao($pcd_usuario, $pmsg = "Solicitada exclusão pelo estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set FL_INATIVO = 2
					, cd_usuario_sol_exclusao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_sol_exclusao = now()
					, cand_fl_revisado = 1
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_revisao = now()
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistorico($pcd_usuario, $pmsg);
    }

    public function RegistreQueEstaRevisado($pcd_usuario, $pmsg = "estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set cand_fl_revisado = 1
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_revisao = now()
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistorico($pcd_usuario, "Alterado para revisado pelo " . $pmsg);
    }

    public function RegistreQueNaoEstaRevisado($pcd_usuario, $pmsg = "estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set cand_fl_revisado = 0
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_revisao = now()
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistorico($pcd_usuario, "Alterado para não revisado pelo " . $pmsg);
    }

    public function RegistreQueEstaRevisadoLote($pcd_usuario, $pLote, $pmsg = "estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set cand_fl_revisado = 1
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_revisao = now()
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO in (" . $pLote . ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistorico($pcd_usuario, $pLote, "Alterado para revisado pelo " . $pmsg);
    }

    public function ColocarRevisaoPendente($pcd_usuario, $pmsg = "estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set cand_fl_revisado = 2
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_revisao = now()
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistorico($pcd_usuario, "Alterado para não revisado pelo " . $pmsg);
    }

    public function ColocarRevisaoPendenteLote($pcd_usuario, $pLote, $pmsg = "estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set cand_fl_revisado = 2
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_revisao = now()
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO in (" . $pLote . ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistorico($pcd_usuario, $pLote, "Alterado para revisado pelo " . $pmsg);
    }

    public function SoliciteExclusaoLote($pcd_usuario, $pLote, $pmsg = "estrangeiros por projeto") {
        $sql = "update CANDIDATO
				set FL_INATIVO = 2
					, cd_usuario_sol_exclusao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_sol_exclusao = now()
					, cand_fl_revisado = 1
					, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_revisao = now()
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO in (" . $pLote . ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->LogaHistorico($pcd_usuario, $pLote, "Solicitada exclusão pelo " . $pmsg);
    }

    public function TornarEmbarcadoLote($pcd_usuario, $pLote, $pmsg = "estrangeiros por projeto") {
        $this->AtualizarAtributoEmLote($pcd_usuario, $pLote, 'FL_EMBARCADO', '1');
        $this->LogaHistorico($pcd_usuario, $pLote, "Alterado para embarcado pelo " . $pmsg);
    }

    public function TornarDesembarcadoLote($pcd_usuario, $pLote, $pmsg = "estrangeiros por projeto") {
        $this->AtualizarAtributoEmLote($pcd_usuario, $pLote, 'FL_EMBARCADO', '0');
        $this->LogaHistorico($pcd_usuario, $pLote, "Alterado para desembarcado pelo " . $pmsg);
    }

    public function AtualizarAtributoEmLote($pcd_usuario, $pLote, $pCampo, $pValor, $pRevisar = false, $pmsg = "Atualizado atributo %1 para %2 pelo estrangeiros por projeto (default)") {
        $sql = "update CANDIDATO
				set " . $pCampo . " = " . $pValor;
        if ($pRevisar) {
            $sql .= " , cand_fl_revisado = 1
				 	, cd_usuario_revisao = " . cBANCO::ChaveOk($pcd_usuario) . "
					, cand_dt_revisao = now()";
        }
        $sql .= $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where NU_CANDIDATO in (" . $pLote . ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $pmsg = str_replace('%1', $pAtributo, $pmsg);
        $pmsg = str_replace('%2', $pValor, $pmsg);
        $this->LogaHistorico(cSESSAO::$mcd_usuario, $pmsg);
    }

    /**
     * ENTIDADE_AUDITAVEL
     * @return string
     */
    public function getCampoUsuarioAlteracao() {
        return 'CO_USU_ULT_ALTERACAO';
    }

    public function QtdCandidatos($pFiltros) {
        $where = cBANCO::WhereFiltros($pFiltros);
        $sql = "select ifnull(count(*),0) qtd from CANDIDATO C
                left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual and  (MTE.fl_vazio = 0 or MTE.fl_vazio is null)
				where 1=1 " . $where;

        $res = cAMBIENTE::$db_pdo->query($sql);
        $rw = $res->fetch(PDO::FETCH_ASSOC);
        $qtd = $rw['qtd'];
        return $qtd;
    }

    public function CursorRevisoesPorUsuario() {
        $sql = "select nome, date_format(cand_dt_revisao, '%d/%m/%Y'), count(*)
				from CANDIDATO C, usuarios u
				where u.cd_usuario = C.cd_usuario_revisao
				and dt_cadastramento < '2012-01-01'
				group by nome, date_format(cand_dt_revisao, '%d/%m/%Y')";
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    public function ProcessoFormatado($pnumero) {
        return str_replace('.', "", str_replace(".", "", str_replace('/', "", str_replace('-', "", $pnumero))));
    }

    public function AtualizeAtributo($pAtributo, $pValor, $pmsg = "Atualizado atributo %1 para %2 pelo estrangeiros por projeto (default)") {
        if ($pAtributo == 'FL_EMBARCADO') {
            if ($pValor == 'Sim') {
                $pValor = '1';
            } else {
                $pValor = '0';
            }
        }

        $sql = "update candidato set " . $pAtributo . " = " . cBANCO::StringOk($pValor) . " where nu_candidato = " . $this->mNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $pmsg = str_replace('%1', $pAtributo, $pmsg);
        $pmsg = str_replace('%2', $pValor, $pmsg);

        $this->LogaHistorico(cSESSAO::$mcd_usuario, $pmsg);
    }

    public function AtualizeAtributoString($pAtributo, $pValor, $pmsg = "Atualizado atributo %1 para %2 pelo estrangeiros por projeto (default)") {
        $sql = "update candidato
				set " . $pAtributo . " = " . cBANCO::StringOk($pValor) . "
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where nu_candidato = " . $this->mNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $pmsg = str_replace('%1', $pAtributo, $pmsg);
        $pmsg = str_replace('%2', $pValor, $pmsg);
        $this->LogaHistorico(cSESSAO::$mcd_usuario, $pmsg);
    }

    public function AtualizaFL_EMBARCADO($pNU_CANDIDATO, $pValor, $pmsg = "") {
        $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        if ($pValor == 'Sim') {
            $pValor = '1';
            if ($pmsg == '') {
                $pmsg = "Alterou o status para EMBARCADO.";
            }
        } else {
            $pValor = '0';
            if ($pmsg == '') {
                $pmsg = "Alterou o status para DESEMBARCADO.";
            }
        }
        $sql = "update candidato
				set FL_EMBARCADO = " . cBANCO::SimNaoOk($pValor) . "
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where nu_candidato = " . $pNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->LogaHistorico(cSESSAO::$mcd_usuario, $pmsg);
    }

    public function AtualizaTE_OBSERVACAO_CREWLIST($pNU_CANDIDATO, $pValor, $pmsg = "Atualizada a observação da crewlist pelo estrangeiros por projeto (default)") {
        $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        $this->AtualizeAtributoString('TE_OBSERVACAO_CREWLIST', $pValor, $pmsg);
    }

    public function AtualizaNU_EMBARCACAO_PROJETO($pNU_CANDIDATO, $pValor, $pmsg = "Alterada embarcação %1 pelo estrangeiros por projeto") {
        $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        $pmsg = str_replace('%1', '(para ' . $pValor . ')', $pmsg);
        $this->AtualizeAtributo('NU_EMBARCACAO_PROJETO', $pValor, $pmsg);
    }

    public function AtualizaNU_ORDEM_CREWLIST($pNU_CANDIDATO, $pValor, $pmsg = "Alterada ordem na crewlist pelo estrangeiros por projeto") {
        $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        $this->AtualizeAtributo('NU_ORDEM_CREWLIST', $pValor, $pmsg);
    }

    public function LogaHistorico($pcd_usuario, $pobservacoes) {
        $sql = "insert into historico_candidato (
					nu_candidato
					, hica_dt_evento
					, cd_usuario
					, hica_tx_observacoes
					) values(
					" . $this->mNU_CANDIDATO . "
					, now()
					," . $pcd_usuario . "
					," . cBANCO::StringOk($pobservacoes) . "
					)";
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function LogaHistoricoLote($pcd_usuario, $pLote, $pobservacoes) {
        $sql = "insert into historico_candidato (
					nu_candidato
					, hica_dt_evento
					, cd_usuario
					, hica_tx_observacoes
					) values(
					" . $this->mNU_CANDIDATO . "
					, now()
					," . $pcd_usuario . "
					," . cBANCO::StringOk($pobservacoes) . "
					)";
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function Cursor_Historico($pFiltros, $pOrdem) {
        $sql = "select hica_dt_evento
					, date_format(hica_dt_evento, '%d/%m/%y %H:%i:%S') hica_dt_evento_fmt
					, nome
					, hica_tx_observacoes
				from historico_candidato
				left join usuarios u on u.cd_usuario = historico_candidato.cd_usuario
				where nu_candidato=" . $this->mNU_CANDIDATO . "
				order by " . $pOrdem;
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    public function get_prazo_estada() {

    }

    public function Cursor_CAND_CLIE_EDIT($pNU_CANDIDATO) {
        $sql = "select c.*
				from vcandidato_cons c
				where c.NU_CANDIDATO = " . $pNU_CANDIDATO;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        return $res;
    }

    /**
     * Retorna um array com o visto atual e seus processos
     * array
     * 		autorizacao = processo_mte atual
     * 		processos
     * 			array de processos: processo_regcie, processo_prorrog, etc.
     */
    public function VistoAtual() {
        $ret = array();
        if ($this->mcodigo_processo_mte_atual > 0) {
            // Cria o processo da autorizacao
            $autorizacao = new cprocesso_mte();
            $autorizacao->mcodigo = $this->mcodigo_processo_mte_atual;
            try {
                $autorizacao->RecupereSe();
                $ret['autorizacao'] = $autorizacao;
                // Carrega os processos desse visto
                $ret['processos'] = $autorizacao->Processos();
            } catch (Exception $e) {
                // Nada. Vai retornar o visto nulo.
            }
        }
        return $ret;
    }

    public function CursorCandidatoCompleto($pnu_candidato) {
        $sql = "select vcandidato_cons.*, candidato.dt_validade_passaporte dt_validade_passaporte_orig
				from vcandidato_cons, candidato
				where vcandidato_cons.nu_candidato = " . $pnu_candidato . "
				and candidato.nu_candidato= " . $pnu_candidato;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public function AtualizaForm1344($pNU_CANDIDATO, $pValor, $pmsg = "Informou presença de cópia do passaporte") {
        $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        if ($pValor == 'Sim') {
            cdocumento::Criar($pNU_CANDIDATO, cdocumento::dcFORM_1344, '', cSESSAO::$mcd_usuario);
            $pmsg = "Informou a presença de formulário 1344 assinado";
        } else {
            cdocumento::Remover($pNU_CANDIDATO, cdocumento::dcFORM_1344);
            $pmsg = "Informou a ausência de formulário 1344 assinado";
        }
        $this->LogaHistorico(cSESSAO::$mcd_usuario, $pmsg);
    }

    public function AtualizaCopiaPassaporte($pNU_CANDIDATO, $pValor, $pmsg = "") {
        $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        if ($pValor == 'Sim') {
            cdocumento::Criar($pNU_CANDIDATO, cdocumento::dcCOPIA_PASSAPORTE, '', cSESSAO::$mcd_usuario);
            $pmsg = "Informou a presença de cópia do passaporte";
        } else {
            cdocumento::Remover($pNU_CANDIDATO, cdocumento::dcCOPIA_PASSAPORTE);
            $pmsg = "Informou a ausência de cópia do passaporte";
        }
        $this->LogaHistorico(cSESSAO::$mcd_usuario, $pmsg);
    }

    public function Cursor_CREW_LIST($pFiltros, $pOrdem) {
        if ($pOrdem == '') {
            $pOrdem = "EMBP.NO_EMBARCACAO_PROJETO, C.NOME_COMPLETO ASC";
        }

        $where = cBANCO::WhereFiltros($pFiltros);
        $strsql_candidatos = "	select distinct
                                C.NOME_COMPLETO
								,C.NU_CANDIDATO
                                ,C.NU_PASSAPORTE
                                ,C.NU_RNE
                                ,C.NU_CPF
								,C.NU_TELEFONE_ESTRANGEIRO
								,C.NO_EMAIL_CANDIDATO
                                ,DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE
                                ,DATE_FORMAT(C.DT_NASCIMENTO,'%d/%m/%Y') DT_NASCIMENTO
                                ,DATE_FORMAT(C.cand_dt_revisao,'%d/%m/%Y') cand_dt_revisao
                                ,C.codigo_processo_mte_atual
								,concat(ifnull(C.NO_ENDERECO_ESTRANGEIRO, ''),' - ', ifnull(C.NO_CIDADE_ESTRANGEIRO, ''), ' - ', ifnull(PE.NO_PAIS, '')) NO_ENDERECO_ESTRANGEIRO_COMPLETO
								,CASE FL_INATIVO
									WHEN 2 THEN 'exclusão solicitada'
									WHEN 1 THEN 'inativo'
									WHEN 0 THEN 'ativo'
									ELSE 'ativo'
								END AS situacao_cadastral
								,C.FL_INATIVO
								,C.cand_fl_revisado
								,CASE C.cand_fl_revisado
									WHEN 2 THEN 'pendente'
									WHEN 1 THEN 'revisado'
									WHEN 0 THEN 'não revisado'
									ELSE 'não revisado'
								END AS revisado
								,C.cand_fl_conferido
								,C.FL_EMBARCADO
								,C.TE_OBSERVACAO_CREWLIST
								,C.NU_EMPRESA
								,C.NU_EMBARCACAO_PROJETO
								,C.NU_ORDEM_CREWLIST

								,PN.NO_NACIONALIDADE
								,EMBP.NO_EMBARCACAO_PROJETO
								,ESC.NO_ESCOLARIDADE
								,E.NO_RAZAO_SOCIAL
								,u_rev.nome nome_revisao

					from CANDIDATO C

						left join PAIS_NACIONALIDADE PN on PN.CO_PAIS = C.CO_NACIONALIDADE
						left join PAIS_NACIONALIDADE PE on PE.CO_PAIS = C.CO_PAIS_ESTRANGEIRO
						left join EMPRESA E on E.NU_EMPRESA = C.NU_EMPRESA
						left join EMBARCACAO_PROJETO EMBP on EMBP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EMBP.NU_EMPRESA = C.NU_EMPRESA
                        left join ESCOLARIDADE ESC on ESC.CO_ESCOLARIDADE = C.CO_NIVEL_ESCOLARIDADE
                        left join usuarios u_rev on u_rev.cd_usuario = C.cd_usuario_revisao
						left join empresa_terceirizada empt ON empt.empt_id = C.empt_id
						WHERE 1=1 " . $where . "
                        ORDER BY " . $pOrdem . "
                    ";
        $res = cAMBIENTE::$db_pdo->query($strsql_candidatos);
        return $res;
    }

    public function CursorOSPendentesDeRevisao() {
        $sql = "select id_solicita_visto, nu_solicitacao from autorizacao_candidato where nu_candidato = " . $this->mNU_CANDIDATO . " and coalesce(autc_fl_revisado, 0) = 0";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        return $res;
    }

    public function RevisarCadastroNaOS($pid_solicita_visto, $pcd_usuario) {
        $this->Atualizar();
        $sql = " update autorizacao_candidato
					set autc_fl_revisado = 1
					  , cd_usuario_revisao = " . $pcd_usuario . "
				  where id_solicita_visto=" . $pid_solicita_visto . "
					and nu_candidato = " . $this->mNU_CANDIDATO;

        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $os = new cORDEMSERVICO();
        $os->mID_SOLICITA_VISTO = $pid_solicita_visto;
        $os->RecuperarSolicitaVisto();
        $msg = "Revisou o cadastro para a OS " . $os->mNU_SOLICITACAO;
        $this->LogaHistorico(cSESSAO::$mcd_usuario, $msg);
    }

    public function get_RevisadoNaOs($pid_solicita_visto) {
        $sql = "select autc_fl_revisado
				  from autorizacao_candidato
				 where id_solicita_visto = " . $pid_solicita_visto . "
				   and nu_candidato = " . $this->mNU_CANDIDATO;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if ($rs['autc_fl_revisado'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function AtualizarCadastroNaOs($pco_reparticao_consular) {

    }

    public function CursorDependentes() {
        $sql = "select c.* , nu_candidato_parente
					 , no_grau_parentesco
					 , n.no_nacionalidade
					 , e.no_pais
				  from vcandidato_cons c
				  join candidato on candidato.nu_candidato = c.nu_candidato
			 left join grau_parentesco gp on gp.co_grau_parentesco = candidato.co_grau_parentesco
			 left join pais_nacionalidade  n on n.co_pais = candidato.co_nacionalidade
			 left join pais_nacionalidade  e on e.co_pais = candidato.co_pais_emissor_passaporte
			     where nu_candidato_parente = " . $this->mNU_CANDIDATO;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public function CursorDependentesNaOs($pid_solicita_visto) {
        $sql = "select c.* , nu_candidato_parente
					 , no_grau_parentesco
					 , n.no_nacionalidade
					 , e.no_pais
				  from vcandidato_cons c
				  join candidato on candidato.nu_candidato = c.nu_candidato
				  join autorizacao_candidato ac on ac.nu_candidato = c.nu_candidato and ac.id_solicita_visto = " . $pid_solicita_visto . "
			 left join grau_parentesco gp on gp.co_grau_parentesco = candidato.co_grau_parentesco
			 left join pais_nacionalidade  n on n.co_pais = candidato.co_nacionalidade
			 left join pais_nacionalidade  e on e.co_pais = candidato.co_pais_emissor_passaporte
			     where nu_candidato_parente = " . $this->mNU_CANDIDATO;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public function ArrayDependentes() {
        $res = $this->CursorDependentes();
        $arr = $res->fetchAll(PDO::FETCH_ASSOC);
        return $arr;
    }

    public function ArrayDependentesNaOs($pid_solicita_visto) {
        $res = $this->CursorDependentesNaOs($pid_solicita_visto);
        $arr = array();
        while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $arr[] = $rs;
        }
        return $arr;
    }

    public function CriarDependente($pcd_usuario) {
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto(trim($this->mNO_PRIMEIRO_NOME), trim($this->mNO_NOME_MEIO), trim($this->mNO_ULTIMO_NOME));
        $sql = "INSERT INTO CANDIDATO (";
        $sql .= "  NO_PRIMEIRO_NOME";
        $sql .= " ,NO_NOME_MEIO";
        $sql .= " ,NO_ULTIMO_NOME";
        $sql .= " ,NOME_COMPLETO";
        $sql .= " ,DT_CADASTRAMENTO";
        $sql .= " ,CO_USU_CADASTAMENTO";
        $sql .= " ,CO_USU_ULT_ALTERACAO";
        $sql .= " ,CO_GRAU_PARENTESCO";
        $sql .= " ,nu_candidato_parente";
        $sql .= " ,DT_NASCIMENTO";
        $sql .= " ,CO_NACIONALIDADE";
        $sql .= " ,NU_PASSAPORTE";
        $sql .= " ,DT_VALIDADE_PASSAPORTE";
        $sql .= " ,CO_PAIS_EMISSOR_PASSAPORTE";
        $sql .= ") VALUES (";
        $sql .= cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_NOME_MEIO);
        $sql .= "," . cBANCO::StringOk($this->mNO_ULTIMO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= ", now()";
        $sql .= "," . cBANCO::StringOk($pcd_usuario);
        $sql .= "," . cBANCO::StringOk($pcd_usuario);
        $sql .= "," . cBANCO::ChaveOk($this->mco_grau_parentesco);
        $sql .= "," . cBANCO::ChaveOk($this->mnu_candidato_parente);
        $sql .= "," . cBANCO::DataOk($this->mDT_NASCIMENTO);
        $sql .= "," . cBANCO::ChaveOk($this->mCO_NACIONALIDADE);
        $sql .= "," . cBANCO::StringOk($this->mNU_PASSAPORTE);
        $sql .= "," . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE);
        $sql .= "," . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE);
        $sql .= ")";
        if (mysql_query($sql)) {
            $this->mNU_CANDIDATO = mysql_insert_id();
        } else {
            throw new Exception(self::mEscopo . '.CriarDependente-> Erro:' . mysql_error() . '<br/>SQL="' . $sql . '"');
        }
    }

    public function AtualizaComoParente() {
        if ($this->mNU_CANDIDATO > 0) {
            $sql = "update candidato set
						nu_candidato_parente = " . cBANCO::ChaveOk($this->mnu_candidato_parente) . "
						,co_grau_parentesco = " . cBANCO::ChaveOk($this->mco_grau_parentesco) . "
						,dt_nascimento = " . cBANCO::DataOk($this->mDT_NASCIMENTO) . "
						,nu_passaporte = " . cBANCO::ChaveOk($this->mNU_PASSAPORTE) . "
						,co_pais_emissor_passaporte = " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE) . "
						,dt_validade_passaporte = " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE) . "
						,co_nacionalidade= " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE) . "
						,dt_ult_alteracao= now()
						,co_usu_ult_alteracao = " . cSESSAO::$mcd_usuario . "
					where nu_candidato =" . $this->mNU_CANDIDATO;
        } else {
            $sql = "insert into candidato (
						nu_candidato_parente
						,no_primeiro_nome
						,no_nome_meio
						,no_ultimo_nome
						,nome_completo
						,co_grau_parentesco
						,dt_nascimento
						,nu_passaporte
						,co_pais_emissor_passaporte
						,dt_validade_passaporte
						,co_nacionalidade
						,dt_cadastramento
						,co_usu_cadastamento
						,dt_ult_alteracao
						,co_usu_ult_alteracao
					) values (
						" . cBANCO::ChaveOk($this->mnu_candidato_parente) . "
						," . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME) . "
						," . cBANCO::StringOk($this->mNO_NOME_MEIO) . "
						," . cBANCO::StringOk($this->mNO_ULTIMO_NOME) . "
						," . cBANCO::StringOk($this->mNOME_COMPLETO) . "
						," . cBANCO::ChaveOk($this->mco_grau_parentesco) . "
						," . cBANCO::DataOk($this->mDT_NASCIMENTO) . "
						," . cBANCO::ChaveOk($this->mNU_PASSAPORTE) . "
						," . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE) . "
						," . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE) . "
						," . cBANCO::ChaveOk($this->mCO_NACIONALIDADE) . "
						, now()
						, " . cSESSAO::$mcd_usuario . "
						, now()
						, " . cSESSAO::$mcd_usuario . "
					)";
        }
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_Liste() {
        $sql = "select * from candidato where 1=1";
        return $sql;
    }

    public static function CursorPit($pFiltros, $pOrdem) {
        $where = cBANCO::WhereFiltros($pFiltros);
        $sql = "select nu_candidato
					, nome_completo
					, coalesce(cand_fl_pit, 0) cand_fl_pit
					, c.nu_empresa
					, no_razao_social
					, c.nu_embarcacao_projeto
					, no_embarcacao_projeto
					from candidato c
					left join empresa e on e.nu_empresa = c.nu_empresa
					left join embarcacao_projeto ep on ep.nu_empresa = c.nu_empresa and ep.nu_embarcacao_projeto = c.nu_embarcacao_projeto
					where coalesce(fl_inativo, 0) = 0 " . $where;
        if ($pOrdem != '') {
            $sql .= " order by " . $pOrdem;
        }
        $sql .= " limit 1000";

        return cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function IncluirPit($pcd_usuario) {
        $sql = "update candidato set cand_fl_pit = 1 where nu_candidato = " . $this->mNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function RemoverPit($pcd_usuario) {
        $sql = "update candidato set cand_fl_pit = 0 where nu_candidato = " . $this->mNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function AdicionadoAOS($pos) {
        // Desembarca o candidato caso ele esteja sendo adicionado a uma OS de autorizacao
        if ($pos->mID_TIPO_ACOMPANHAMENTO == 1) {
            $msg = "Alterado para desembarcado pela inclusão na OS " . $pos->mNU_SOLICITACAO;
            $this->AtualizaFL_EMBARCADO($this->mNU_CANDIDATO, false, $msg);
        }
        // Ativa caso esteja inativo
        if ($this->mFL_INATIVO == 1) {
            $this->Ative(cSESSAO::$mcd_usuario, "Ativado automaticamente pela inclusão na OS " . $pos->mNU_SOLICITACAO);
        }
    }

    public function salvaImportado() {
        $sql = "insert into candidato ("
                . "nome_completo"
                . ", no_primeiro_nome"
                . ", no_ultimo_nome"
                . ", no_nome_meio"
                . ", NU_PASSAPORTE"
                . ", DT_VALIDADE_PASSAPORTE"
                . ", CO_NACIONALIDADE"
                . ", CO_FUNCAO"
                . ", NU_EMPRESA"
                . ", NU_EMBARCACAO_PROJETO"
                . ", DT_CADASTRAMENTO"
                . ", CO_USU_ULT_ALTERACAO"
                . ") values ("
                . cBANCO::StringOk($this->mNOME_COMPLETO)
                . ", " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME)
                . ", " . cBANCO::StringOk($this->mNO_ULTIMO_NOME)
                . ", " . cBANCO::StringOk($this->mNO_NOME_MEIO)
                . ", " . cBANCO::StringOk($this->mNU_PASSAPORTE)
                . ", " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE)
                . ", " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE)
                . ", " . cBANCO::ChaveOk($this->mCO_FUNCAO)
                . ", " . cBANCO::ChaveOk($this->mNU_EMPRESA)
                . ", " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO)
                . ", now()"
                . ", 38"
                . ") "
        ;
        if (cAMBIENTE::ExecuteQuery($sql)) {
            $this->mNU_CANDIDATO = cAMBIENTE::$db_pdo->lastInsertId();
        } else {
            throw new Exception(self::mEscopo . '.Criar-> Erro:' . mysql_error() . '<br/>SQL="' . $sql . '"');
        }
    }

    public function salvaCodigoMteAtual() {
        $sql = "update candidato set codigo_processo_mte_atual = " . $this->mcodigo_processo_mte_atual . " where nu_candidato = " . $this->mNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql);
    }

//    /**
//     * Recupera o candidato pelo e-mail pessoal
//     * @param type $email_pessoal
//     * @return boolean
//     */
//    public function recuperesePeloEmail($email_pessoal) {
//        $email_pessoal = trim($email_pessoal);
//        $sql = "select * from CANDIDATO";
//        $sql.= " where email_pessoal =" . cBANCO::StringOk($email_pessoal);
//        if ($res = mysql_query($sql)) {
//            if ($rs = mysql_fetch_array($res)) {
//                $this->CarreguePropriedadesPeloRecordset($rs);
//                return true;
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }
//    }
    public function cursorHomonimos($no_primeiro_nome, $no_nome_meio, $no_ultimo_nome) {
        $sql = "select * from candidato "
                . " where no_primeiro_nome = " . cBANCO::StringOk($no_primeiro_nome)
                . " and no_nome_meio = " . cBANCO::StringOk($no_nome_meio)
                . " and no_ultimo_nome = " . cBANCO::StringOk($no_ultimo_nome)
        ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll();
        return $rs;
    }

    public function CarreguePropriedadesPeloRecordset($prs, $prefixo = "m"){
        parent::CarreguePropriedadesPeloRecordset($prs, $prefixo);
        $this->mDT_EXPIRACAO_CNH = cBANCO::DataFmt($prs["DT_EXPIRACAO_CNH"]);
        $this->mDT_EXPIRACAO_CTPS = cBANCO::DataFmt($prs["DT_EXPIRACAO_CTPS"]);
        $this->mDT_VALIDADE_PASSAPORTE = cBANCO::DataFmt($prs["DT_VALIDADE_PASSAPORTE"]);
        $this->mDT_VALIDADE_SEAMAN = cBANCO::DataFmt($prs["DT_VALIDADE_SEAMAN"]);
    }
}
