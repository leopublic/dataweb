<?php

Class AppRequest{

    public $ajax = false;


   function __construct(){

    }

	public function inject($t){
		$t = preg_replace("/(from|select|insert|delete|where|drop table|show tables|£|¢|¹|!|¨|# OR| # AND|' OR |\" OR|--|=|´|`|~|^|\\\\)/i",'',$t);
	    $t = preg_replace(sql_regcase("/(\n|\r|%0a|%0d|Content-Type:|bcc:|to:|cc:|Autoreply:|%|\\\\)/"), '', $t);
        $t = addslashes($t);
            return $this ->trate_str($t);
	}

   public function trate_str($t){
	    $t = trim($t);
        $t = strip_tags($t);
            return $t;
	}

	/** função para obter post inteiro */
   public function p_int($t) {
		if(is_numeric($this ->p($t))){
			return $this -> inject($this ->p($t)) ? $this -> inject($this ->p($t)) : '';
		}
	}

	/** função para obter post string */
   public function p_str($t) {
		if(is_string($this ->p($t))){
			return $this ->inject($this ->p($t)) ? $this -> inject($this ->p($t)) : '';
		}
	}



	/** função para obter get inteiro */
	public function g_int($t){
		if(is_numeric($this ->g($t))){
			return $this ->inject($this ->g($t)) ? $this ->inject($this ->g($t)) : '';
		}
	}

	/** função para obter get string */
	public function g_str($t){
		if(is_string($this ->g($t))){
			return $this ->inject($this ->g($t)) ? $this ->inject($this ->g($t)) : '';
		}
	}


    public function g_urldecode($t){


        		if(is_string($this ->g($t))){
			return $this ->inject($this ->g($t)) ? $this ->inject($this ->g($t)) : '';
		}

    }

    public function p_ajax($t){
        if(is_string($this ->__post($t))){
			return $this ->inject($this ->__post($t)) ? utf8_decode($this ->__post($t)) : '';
		}
    }


	public function g_ajax($t){
	  if(is_string($this ->__get($t))){
			return $this ->inject($this ->__get($t)) ? utf8_decode($this ->__get($t)) : '';
		}
	}



	/** função que obtém requisição $_POST */
	public function p($t){

      $res = null;

      if($this->ajax){ // verifico se é codificação ajax

         $res = $this->p_ajax($t);

      }else{

    	  if(is_string($this ->__post($t))){
    			$res = $this ->inject($this ->__post($t)) ? $this ->inject($this ->__post($t)) : '';
    	  }
      }

        return $res;
	}

	/** função que obtém requisição $_GET, aliás de __get() */
	public function g($t){

       $res = null;

       if($this->ajax){

            $res = $this->g_ajax($t);

       }else{

    	  if(is_string($this ->__get($t))){
    			$res = $this ->inject($this ->__get($t)) ? $this ->inject($this ->__get($t)) : '';
    	  }
       }

        return $res;
	}

	public function f($t){
		return $this -> __file($t);
	}

	function __file($t){
		if ( (isset($_FILES[$t]))  and  (!empty($_FILES[$t])) )
    	{
		    return $_FILES[$t];
		}
	}



	function __get($t)
	{
		if ( (isset($_GET[$t]))  and  (!empty($_GET[$t])) and ($_GET[$t]<>'') )
    	{
		    return (isset($_GET[$t]) ) ? $_GET[$t] : '';
		}
	}


	function __post($t)
	{
		if ( (isset($_POST[$t]))  and  (!empty($_POST[$t])) and ($_POST[$t]<>'') )
    	{
		   	return (isset($_POST[$t]) ) ? $_POST[$t] : '';
		}
	}


}
?>
