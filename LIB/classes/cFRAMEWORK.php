<?php
class cFRAMEWORK{
	public static function ConverteEdicaoEmTemplate($pid_edicao, $pnome_template){
		$tipo_combo = array(1 => 'cmbTIPO_AUTORIZACAO',
					2 => 'cmbESTADO_CIVIL',
					3 => 'cmbSERVICO',
					4 => 'cmbSTATUS_SOLICITACAO',
					5 => 'cmbNACIONALIDADE',
					6 => 'cmbPAIS',
					7 => 'cmbESCOLARIDADE',
					8 => 'cmbREPARTICAO_CONSULAR',
					9 => 'cmbPROFISSAO',
					10 => 'cmbEMPRESA',
					11 => 'cmbEMBARCACAO_PROJETO',
					12 => 'cmbFUNCAO',
					13 => 'cmbCLASSIFICA_VISTO',
					14 => 'cmbTRANSPORTE_ENTRADA',
					15 => 'cmbTIPO_SERVICO',
					16 => 'cmbSEXO',
					17 => 'cmbTIPO_ARQUIVO',
					18 => 'cmbUF',
					19 => 'cmbRESPONSAVEL',
					20 => 'cmbSTATUS_ANDAMENTO',
					21 => 'cmbSTATUS_CONCLUSAO',
					22 => 'cmbEMPRESA_ATIVA',
					23 => 'cmbTIPO_COMBO',
					24 => 'cmbTIPO_CAMPO',
					25 => 'cmbPROCESSO_MTE',
					26 => 'cmbCANDIDATO_PROCESSO',
					27 => 'cmbNACIONALIDADE_EN',
					28 => 'cmbESCOLARIDADE_EN',
					29 => 'cmbESTADO_CIVIL_EN',
					30 => 'cmbSEXO_EN',
					31 => 'cmbPAIS_EN',
					32 => 'cmbLOCAL_PROJETO',
					33 => 'cmbSERVICO_SIMPLES',
					34 => 'cmbTipoEnsino',
					35 => 'cmbEDICAO'
			);
		$arquivo = "../templates/".$pnome_template.'.php';
		if (file_exists($arquivo)){
			throw new Exception ("Já existe um arquivo com esse nome.");
		}
		print $arquivo;
		$tmpl = '' ;
		$sql = "select * from Campo where id_edicao = ".$pid_edicao." order by ifnull(nu_ordem_painel, 0), nu_ordem";
		$res = conectaQuery($sql, __CLASS__.'.'.__FUNCTION__);
		$tmpl .= '<?php';
		$tmpl .= "\n".'class '.$pnome_template. ' extends cTEMPLATE{';
		$tmpl .= "\n\t".'public function __construct() {';
		$tmpl .= "\n\t\t".'parent::__construct(__CLASS__);';
		while($rs = mysql_fetch_array($res)){
			switch ($rs['id_tipo_campo']) {
				case cCAMPO::cpCHAVE:
				case cCAMPO::cpHIDDEN:
					$tmpl.= "\n\t\t".'$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, \''.$rs['no_campo_bd'].'\'));';
					break;
				case cCAMPO::cpCHAVE_ESTR:
					$tmpl.= "\n\t\t".'$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpCHAVE_ESTR,\''.$rs['no_campo_bd'].'\', \''.$rs['no_label'].'\', cCOMBO::'.$tipo_combo[intval($rs['id_tipo_combo'])].'));';
					break;
				case cCAMPO::cpDATA:
					$tmpl.= "\n\t\t".'$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA,\''.$rs['no_campo_bd'].'\', \''.$rs['no_label'].'\'));';
					break;
				case cCAMPO::cpSIMNAO:
					$tmpl.= "\n\t\t".'$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpSIMNAO, \''.$rs['no_campo_bd'].'\', \''.$rs['no_label'].'\'));';
					break;
				case cCAMPO::cpTEXTO:
					$tmpl.= "\n\t\t".'$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, \''.$rs['no_campo_bd'].'\', \''.$rs['no_label'].'\'));';
					if (intval($rs['bo_exibir']) == 0 ){
						$tmpl.= "\n\t\t".'$this->mCampo[\''.$rs['no_campo_bd'].'\']->mVisivel = false;';
					}
					if (intval($rs['fl_largura_dupla']) == 1 ){
						$tmpl.= "\n\t\t".'$this->mCampo[\''.$rs['no_campo_bd'].'\']->mfl_largura_dupla = true;';
					}
					break;
				case cCAMPO::cpMEMO:
					$tmpl.= "\n\t\t".'$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpMEMO, \''.$rs['no_campo_bd'].'\', \''.$rs['no_label'].'\'));';
					if (intval($rs['bo_exibir']) == 0 ){
						$tmpl.= "\n\t\t".'$this->mCampo[\''.$rs['no_campo_bd'].'\']->mVisivel = false;';
					}
					if (intval($rs['fl_largura_dupla']) == 1 ){
						$tmpl.= "\n\t\t".'$this->mCampo[\''.$rs['no_campo_bd'].'\']->mfl_largura_dupla = true;';
					}
					break;
				default:
					$tmpl.= "\n\t\t".'$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, \''.$rs['no_campo_bd'].'\', \''.$rs['no_label'].'\'));';
					if (intval($rs['bo_exibir']) == 0 ){
						$tmpl.= "\n\t\t".'$this->mCampo[\''.$rs['no_campo_bd'].'\']->mVisivel = false;';
					}
					if (intval($rs['fl_largura_dupla']) == 1 ){
						$tmpl.= "\n\t\t".'$this->mCampo[\''.$rs['no_campo_bd'].'\']->mfl_largura_dupla = true;';
					}
					break;
			}
		}
		$tmpl .= "\n\t".'}';
		$tmpl .= "\n".'}';

		$arq = fopen($arquivo, "w" );
		fwrite($arq, $tmpl);
		fclose($arq);

	}

	public function GerarClasse($pTabela, $psobrescrever_arquivos = false){
		$ret = '';
		$sql = "show columns from ".$pTabela;
		$primeiro = '' ;

		if($cursor = cAMBIENTE::$db_pdo->query($sql)){
			$props = '' ;
			$insert = '	public function Incluir(){';
			$insert1 = '' ;
			$insert2 = '' ;
			$update1 = '';
			$update = '	public function Atualizar(){';
			$select = '	public function RecupereSe($pid = ""){';
			$virgula = ' ';
			$tmpl_listar = '' ;
			$tmpl_editar = '' ;
			$cols = '"20px"';
			while ($rs = $cursor->fetch(PDO::FETCH_BOTH)){
				$props .= "\n".'	public $'.$rs['Field'].';';
				$rotina = '';
				$tipo = substr($rs['Type'],0,4);
				$cols .= ', "20px"';
				$nomes = explode("_", $rs['Field']);
				$nome = '';
				$espaco = '';
				for ($index = 2; $index < count($nomes); $index++) {
					$nome .=  $espaco.$nomes[$index];
					$espaco = ' ';
				}
				$nome = ucfirst($nome) ;
				switch ($tipo){
					case 'int(':
						$rotina = 'InteiroOk';
						$cmp_listar = "\n".'		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, \''.$rs['Field'].'\', \''.$nome."'));";
						$cmp_editar = "\n".'		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, \''.$rs['Field'].'\', \''.$nome."'));";
						break;
					case 'char':
					case 'varc':
						$rotina = 'StringOk';
						$cmp_listar = "\n".'		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, \''.$rs['Field'].'\', \''.$nome."'));";
						$cmp_editar = "\n".'		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, \''.$rs['Field'].'\', \''.$nome."'));";
						break;
					case 'date':
						$rotina = 'DataOk';
						$cmp_listar = "\n".'		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, \''.$rs['Field'].'\', \''.$nome."'));";
						$cmp_editar = "\n".'		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpDATA, \''.$rs['Field'].'\', \''.$nome."'));";
						break;
					default:
						$rotina = 'StringOk';
						$cmp_listar = "\n".'		$this->AdicioneColuna(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, \''.$rs['Field'].'\', \''.$nome."'));";
						$cmp_editar = "\n".'		$this->AdicioneCampo(cFABRICA_CAMPO::Novo(cCAMPO::cpTEXTO, \''.$rs['Field'].'\', \''.$nome."'));";
				}
				if ($primeiro != '' ){
					$insert1 .= "\n".'		$sql .= "	'.$virgula.$rs['Field'].'";';
					$insert2 .= "\n".'		$sql .= "	'.$virgula.'".cBANCO::'.$rotina.'($this->'.$rs['Field'].');';
					$update1 .= "\n".'		$sql .= "  '.$virgula.$rs['Field'].'			= ".cBANCO::'.$rotina.'($this->'.$rs['Field'].');';
					$virgula = ',';
				}
				else{
					$primeiro = $rs['Field'];
					$cmp_listar = "\n".'		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, \''.$rs['Field'].'\'));';
					$cmp_editar = "\n".'		$this->AdicioneCampoChave(cFABRICA_CAMPO::Novo(cCAMPO::cpHIDDEN, \''.$rs['Field'].'\'));';
				}
				$tmpl_editar .= $cmp_editar;
				$tmpl_listar .= $cmp_listar;
			}

			$insert .= "\n".'		$sql = "INSERT INTO '.$pTabela.' (";'.$insert1."\n".'		$sql .= ") values (";'.$insert2."\n".'		$sql .= ")";';
			$insert .= "\n".'		cAMBIENTE::$db_pdo->exec($sql);';
			$insert .= "\n".'		$this->setid(cAMBIENTE::$db_pdo->lastInsertId());';
			$insert .= "\n".'	}';

			$update .= "\n".'		$sql = "update '.$pTabela.' set ";'.$update1."\n".'		$sql .= " where '.$primeiro.' = ".$this->'.$primeiro.';';
			$update .= "\n".'		cAMBIENTE::$db_pdo->exec($sql);';
			$update .= "\n".'	}';

			$select .= "\n".'		if ($pid != \'\' ){';
			$select .= "\n".'			$this->setid($pid);';
			$select .= "\n".'		}';
			$select .= "\n".'';
			$select .= "\n".'		if ($this->setid() == \'\' ){';
			$select .= "\n".'			throw new Exception("Id do objeto n&atilde;o informado");';
			$select .= "\n".'		}';
			$select .= "\n".'		$sql = "select * from '.$pTabela.' where '.$primeiro.' = $this->'.$primeiro.'";';
			$select .= "\n".'		if($res = cAMBIENTE::$db_pdo->query($sql)){';
			$select .= "\n".'			$rs = $res->fetch(PDO::FETCH_BOTH);';
			$select .= "\n".'			cBANCO::CarreguePropriedades($rs, $this);';
			$select .= "\n".'		}';
			$select .= "\n".'		else{';
			$select .= "\n".'			throw new Exception("N&atilde;o foi poss&iacute;vel obter a inst&acirc;ncia com o id informado", $sql);';
			$select .= "\n".'		}';
			$select .= "\n".'	}';

			$tmpl_editar ='<?php
class cTMPL_'.strtoupper($pTabela).'_EDITAR extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "INCLUIR/ALTERAR '.strtoupper($pTabela).'";
		$this->mlocalAcoes = cTEMPLATE::lca_RODAPE;
			'.$tmpl_editar.'
		$this->AdicioneAcao(new cACAO_SUBMIT_BUTTON("SALVAR", "Salvar", "Clique para salvar"));
	}
}';
			$tmpl_listar ='<?php
class cTMPL_'.strtoupper($pTabela).'_LISTAR extends cTEMPLATE{
	public function __construct() {
		parent::__construct(__CLASS__);
		$this->mTitulo = "'.strtoupper($pTabela).'";
		$this->mlocalAcoes = cTEMPLATE::lca_TOPO;
		$this->mCols = array('.$cols.');
			'.$tmpl_listar.'
		$this->AdicioneAcao(new cACAO_LINK_INTERNO("ADICIONAR", cACAO::ACAO_IMG_ADD, "Clique para adicionar um novo", "cCTRL_'.strtoupper($pTabela).'", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_INTERNO("ALTERAR",  cACAO::ACAO_IMG_EDIT, "Clique para alterar esse item", "cCTRL_'.strtoupper($pTabela).'", "Edite"));
		$this->AdicioneAcaoLinha(new cACAO_LINK_INTERNO("EXCLUIR",  cACAO::ACAO_IMG_DEL, "Clique para excluir esse item", "cCTRL_'.strtoupper($pTabela).'", "Exclua"));
	}
}';

			$ret = '';
			$ret .='<?php';
			$ret .= "\n".'class c'.strtoupper($pTabela).' extends cMODELO{';
			$ret .= $props;
			$ret .= "\n".'	public function setid($pid){';
			$ret .= "\n".'		$this->'.$primeiro.' = $pid;';
			$ret .= "\n".'	}';
			$ret .= "\n".'	';
			$ret .= "\n".'	public function getid(){';
			$ret .= "\n".'		return $this->'.$primeiro.';';
			$ret .= "\n".'	}';
			$ret .= "\n".'	';
			$ret .= "\n".'	public function sql_Liste(){	';
			$ret .= "\n".'		$sql = "select * from '.$pTabela.' where 1=1 ";';
			$ret .= "\n".'		return $sql;';
			$ret .= "\n".'	}';
			$ret .= "\n".'	';
			$ret .= "\n".'	public function sql_RecuperePeloId(){';
			$ret .= "\n".'		$sql = "select * from '.$pTabela.' where '.$primeiro.' = ".$this->'.$primeiro.';';
			$ret .= "\n".'		return $sql;';
			$ret .= "\n".'	}';
			$ret .= "\n".'	';
			//$ret .= "\n".$select;
			$ret .= "\n".$insert;
			$ret .= "\n"."\n".$update;
			$ret .= "\n".'}';
			$pClasse = $ret;
			$ptmpl_editar = $tmpl_editar;
			$ptmpl_listar = $tmpl_listar;

			$controller = '<?php
class cCTRL_'.strtoupper($pTabela).' extends cCTRL_MODELO{
	public static function ComplementarLinhaListagem(&$pTmpl) {

	}
	public static function Liste(){
		return parent::Liste(new cTMPL_'.strtoupper($pTabela).'_LISTAR(), new c'.strtoupper($pTabela).'());
	}

	public static function Edite() {
		return parent::Edite(new cTMPL_'.strtoupper($pTabela).'_EDITAR(), new c'.strtoupper($pTabela).'(), "cCTRL_'.strtoupper($pTabela).'", "Liste");
	}
}
			';
			// Escreve os arquivos
			$caminho = $_SERVER['DOCUMENT_ROOT'];
			var_dump($caminho);
			if ($psobrescrever_arquivos || !file_exists($caminho.'/controller/cCTRL_'.strtoupper($pTabela).'.php')){
				$arq = fopen($caminho.'/controles/cCTRL_'.strtoupper($pTabela).'.php', 'w');
				fwrite($arq, $controller);
				fclose($arq);
			}

			if ($psobrescrever_arquivos || !file_exists($caminho.'/templates/cTMPL_'.strtoupper($pTabela).'_LISTAR.php')){
				$arq = fopen($caminho.'/templates/cTMPL_'.strtoupper($pTabela).'_LISTAR.php', 'w');
				fwrite($arq, $ptmpl_listar);
				fclose($arq);
			}

			if ($psobrescrever_arquivos || !file_exists($caminho.'/templates/cTMPL_'.strtoupper($pTabela).'_EDITAR.php')){
				$arq = fopen($caminho.'/templates/cTMPL_'.strtoupper($pTabela).'_EDITAR.php', 'w');
				fwrite($arq, $ptmpl_editar);
				fclose($arq);
			}

			if ($psobrescrever_arquivos || !file_exists($caminho.'/model/c'.$pTabela.'.php')){
				$arq = fopen($caminho.'/LIB/classes/c'.$pTabela.'.php', 'w');
				fwrite($arq, $pClasse);
				fclose($arq);
			}

		}
	}
}
