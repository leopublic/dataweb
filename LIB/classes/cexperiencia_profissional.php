<?php
class cexperiencia_profissional extends cMODELO
{
    public $mNU_CANDIDATO;
    public $mnu_candidato_tmp;
    public $mepro_id;
    public $mepro_no_companhia;
    public $mepro_no_funcao;
    public $mepro_no_periodo;
    public $mepro_dt_inicio;
    public $mepro_dt_fim;
    public $mepro_dt_inicio_fmt;
    public $mepro_dt_fim_fmt;
    public $mepro_tx_atribuicoes;

    public static function get_nomeCampoChave() {
        return 'epro_id';
    }
    public static function get_nomeCampoDescricao() {
        return "epro_no_companhia";
    }
    public static function get_nomeTabelaBD() {
        return 'experiencia_profissional';
    }

    public function get_nomeTabela() {
        return 'experiencia_profissional';
    }

    public function getId() {
        return $this->mepro_id;
    }

    public function setId($pValor) {
        $this->mepro_id = $pValor;
    }

    public function campoid() {
        return 'epro_id';
    }

    public function sql_RecuperePeloId() {
        $sql = "select * , date_format(epro_dt_inicio, 'dd/mm/yyyy') epro_dt_inicio_fmt, date_format(epro_dt_fim, 'dd/mm/yyyy') epro_dt_fim_fmt
        		from experiencia_profissional where epro_id = " . $this->mepro_id;
        return $sql;
    }

    public function Incluir() {
        $sql = "insert into experiencia_profissional(
				 NU_CANDIDATO
				,nu_candidato_tmp
				,epro_no_companhia
				,epro_no_periodo
				,epro_dt_inicio
				,epro_dt_fim
				,epro_no_funcao
				,epro_tx_atribuicoes
		       ) values (
				 " . cBANCO::ChaveOk($this->mNU_CANDIDATO) . "
				," . cBANCO::ChaveOk($this->mnu_candidato_tmp) . "
				," . cBANCO::StringOk($this->mepro_no_companhia) . "
				," . cBANCO::StringOk($this->mepro_no_periodo) . "
				," . cBANCO::DataOk($this->mepro_dt_inicio) . "
				," . cBANCO::DataOk($this->mepro_dt_fim) . "
				," . cBANCO::StringOk($this->mepro_no_funcao) . "
				," . cBANCO::StringOk($this->mepro_tx_atribuicoes) . "
				)";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mepro_id = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function Atualizar() {
        $sql = "update experiencia_profissional set
				 epro_no_companhia = " . cBANCO::StringOk($this->mepro_no_companhia) . "
				,epro_no_funcao = " . cBANCO::StringOk($this->mepro_no_funcao) . "
				,epro_no_periodo = " . cBANCO::StringOk($this->mepro_no_periodo) . "
				,epro_dt_inicio = " . cBANCO::DataOk($this->mepro_dt_inicio) . "
				,epro_dt_fim = " . cBANCO::DataOk($this->mepro_dt_fim) . "
				,epro_tx_atribuicao = " . cBANCO::StringOk($this->mepro_tx_atribuicao) . "
				,NU_CANDIDATO = " . cBANCO::ChaveOk($this->mNU_CANDIDATO) . "
				,nu_candidato_tmp = " . cBANCO::ChaveOk($this->mnu_candidato_tmp) . "
			  where epro_id=" . $this->mepro_id;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public static function cursorDoCandidatoTmp($nu_candidato_tmp) {
        $sql = "select * , date_format(epro_dt_inicio, '%d/%m/%Y') epro_dt_inicio_fmt, date_format(epro_dt_fim, '%d/%m/%Y') epro_dt_fim_fmt"
        . " from experiencia_profissional"
        . " where nu_candidato_tmp = " . $nu_candidato_tmp 
        . " order by epro_dt_inicio desc";
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res->fetchAll(PDO::FETCH_ASSOC);
    }

    public function Excluir() {
        $sql = "delete from experiencia_profissional where epro_id = " . $this->epro_id;
        $res = cAMBIENTE::$db_pdo->exec($sql);
    }
}
