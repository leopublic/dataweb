<?php

 /**
 *   Classe Acesso
 *   Estipula, Busca e verifica permissões do usuário no sistema.
 *
 *   M2Software: 08/10/2010
 **/

  Class Acesso{


    /**
    * Tipos de acesso:
    *  Ordenado de acordo com o menu superior do sistema.
    **/

    // 000.000 - Grupo Gerais
    const tp_Geral                                 = 000.000;
    const tp_Geral_Menu_Cadastro                   = 000.001;
    const tp_Geral_Menu_Edicao                     = 000.002;

    // 001.000 - Grupo Sistema
    const tp_Sistema                               = 001.000;
    const tp_Sistema_Controle                      = 001.001;
    const tp_Sistema_Controle_Usuario              = 001.002;
    const tp_Sistema_Controle_Usuario_Cadastrar    = 001.003;
    const tp_Sistema_Controle_Usuario_Editar       = 001.004;
    const tp_Sistema_Controle_Usuario_Excluir      = 001.005;
    const tp_Sistema_Controle_Usuario_Pesquisar    = 001.006;
    const tp_Sistema_Tabelas                       = 001.007;
    const tp_Sistema_Tabelas_Escola                = 001.008;
    const tp_Sistema_Tabelas_Escola_Cadastrar      = 001.009;
    const tp_Sistema_Tabelas_Escola_Editar         = 001.010;
    const tp_Sistema_Tabelas_Escola_Visualizar     = 001.011;
    const tp_Sistema_Tabelas_Escola_Excluir        = 001.012;
    const tp_Sistema_Tabelas_Civil                 = 001.013;
    const tp_Sistema_Tabelas_Civil_Cadastrar       = 001.014;
    const tp_Sistema_Tabelas_Civil_Editar          = 001.015;
    const tp_Sistema_Tabelas_Civil_Excluir         = 001.016;
    const tp_Sistema_Tabelas_Cargo                 = 001.017;
    const tp_Sistema_Tabelas_Cargo_Cadastrar       = 001.018;
    const tp_Sistema_Tabelas_Cargo_Editar          = 001.019;
    const tp_Sistema_Tabelas_Cargo_Excluir         = 001.020;
    const tp_Sistema_Tabelas_Parente               = 001.021;
    const tp_Sistema_Tabelas_Parente_Cadastrar     = 001.022;
    const tp_Sistema_Tabelas_Parente_Editar        = 001.023;
    const tp_Sistema_Tabelas_Parente_Excluir       = 001.024;
    const tp_Sistema_Tabelas_Pais                  = 001.025;
    const tp_Sistema_Tabelas_Pais_Cadastrar        = 001.026;
    const tp_Sistema_Tabelas_Pais_Editar           = 001.027;
    const tp_Sistema_Tabelas_Pais_Excluir          = 001.028;
    const tp_Sistema_Tabelas_Profissao             = 001.029;
    const tp_Sistema_Tabelas_Profissao_Cadastrar   = 001.030;
    const tp_Sistema_Tabelas_Profissao_Editar      = 001.031;
    const tp_Sistema_Tabelas_Profissao_Excluir     = 001.032;
    const tp_Sistema_Tabelas_Reparticao            = 001.033;
    const tp_Sistema_Tabelas_Reparticao_Cadastrar  = 001.034;
    const tp_Sistema_Tabelas_Reparticao_Editar     = 001.035;
    const tp_Sistema_Tabelas_Reparticao_Excluir    = 001.036;
    const tp_Sistema_Tabelas_Autorizacao_Cadastrar = 001.037;
    const tp_Sistema_Tabelas_Autorizacao_Editar    = 001.038;
    const tp_Sistema_Tabelas_Autorizacao_Excluir   = 001.039;
    const tp_Sistema_Tabelas_Servico               = 001.040;
    const tp_Sistema_Tabelas_Servico_Cadastrar     = 001.041;
    const tp_Sistema_Tabelas_Servico_Editar        = 001.042;
    const tp_Sistema_Tabelas_Servico_Excluir       = 001.043;
    const tp_Sistema_Tabelas_NivelContato_Cadastrar= 001.044;
    const tp_Sistema_Tabelas_NivelContato_Editar   = 001.045;
    const tp_Sistema_Tabelas_NivelContato_Excluir  = 001.046;

    // 002.000 - Grupo Gerencial
    const tp_Gerencial                             = 002.000;
    const tp_Gerencial_Eventos                     = 002.001;
    const tp_Gerencial_RelatoriosMTE               = 002.002;
    const tp_Gerencial_Autorizacao                 = 002.003;

    // 003.000 - Grupo OS (Ordem de serviço)
    const tp_Os                                    = 003.000;
    const tp_Os_Listar                             = 003.001;
    const tp_Os_Aba_Dados                          = 003.002;
    const tp_Os_Aba_DadosPessoais                  = 003.003;
    const tp_Os_Aba_Cadastro                       = 003.004;
    const tp_Os_Aba_Cadastro_Salvar                = 003.005;
    const tp_Os_Aba_Cadastro_Dependentes           = 003.006;
    const tp_Os_Aba_Cadastro_Dependentes_Cadastrar = 003.007;
    const tp_Os_Aba_Cadastro_Dependentes_Editar    = 003.008;
    const tp_Os_Aba_Cadastro_Dependentes_Excluir   = 003.009;
    const tp_Os_Menu                               = 003.010;
    const tp_Os_Menu_Acao                          = 003.011;
    const tp_Os_Menu_Acao_Abrir                    = 003.012;
    const tp_Os_Menu_Acao_Excluir                  = 003.013;
    const tp_Os_Menu_Checklist                     = 003.014;
    const tp_Os_Menu_OrdemServico                  = 003.015;
    const tp_Os_Menu_NaoConformidade               = 003.016;
    const tp_Os_Nova                               = 003.017;
    const tp_Os_Aba_Processo                       = 003.018;
    const tp_Os_Aba_Formulario                     = 003.019; 
    const tp_Os_Aba_Liberacao 					   = 003.020;

    // 004.000 - Grupo Cadastros
    const tp_Cadastro                              = 004.000;
    const tp_Cadastro_Candidato                    = 004.001;
    const tp_Cadastro_EmpresaAntigo                = 004.002;
    const tp_Cadastro_EmpresaAntigo_Cadastrar      = 004.003;
    const tp_Cadastro_EmpresaAntigo_Editar         = 004.004;
    const tp_Cadastro_EmpresaAntigo_Excluir        = 004.005;
    const tp_Cadastro_EmpresaNova                  = 004.006;
    const tp_Cadastro_Candidato_Incluir            = 004.007;
    const tp_Cadastro_Candidato_Alterar_Processos  = 004.008;
    const tp_Cadastro_EmpresaAlterar               = 004.009;
    const tp_Cadastro_EmpresaAtivar                = 004.010;
    

    // 005.000 - Grupo Relatórios
    const tp_Relatorio                             = 005.001;
    const tp_Relatorio_Requisicoes                 = 005.002;
    const tp_Relatorio_Requisicoes_CrewList        = 005.003;
    const tp_Relatorio_Relatorio                   = 005.004;
    const tp_Relatorio_Relatorio_GerenciaDePrazo   = 005.005;
    const tp_Relatorio_Relatorio_Estrangeiros      = 005.006;
    const tp_Relatorio_Relatorio_Empresa           = 005.007;
    const tp_Relatorio_Relatorio_Sintetico         = 005.008;
    const tp_Relatorio_Relatorio_Periodo           = 005.009;
    const tp_Relatorio_Processo                    = 005.010;
    const tp_Relatorio_Processo_MTE                = 005.011;

	// 006.000 - Grupo Cobranca
    const tp_Cobranca							= 006.001;
    const tp_Cobranca_1							= 006.010;
    const tp_Cobranca_2							= 006.020;
    const tp_Cobranca_3							= 006.030;
    const tp_Cobranca_4							= 006.040;
    const tp_Cobranca_5							= 006.050;
    const tp_Cobranca_6							= 006.060;
    const tp_Cobranca_7							= 006.070;
    const tp_Cobranca_8							= 006.080;
    const tp_Cobranca_9							= 006.090;
    const tp_Cobranca_10		                = 006.100;

    /**
    *  GetAcesso - Obtem os acessos do usuário
    *  @return Session['ACESSO'] = Array
    **/
    function GetAcesso($cod_usuario){
      $strs   = array();
      $strsql =
        '     SELECT TIPO_ACESSO
                FROM usuarios U
          INNER JOIN perfil_usuario PU	ON PU.cd_usuario  = U.cd_usuario
          INNER JOIN usuario_perfil P	ON P.cd_perfil  = PU.cd_perfil
          INNER JOIN ACESSO_PERFIL AP	ON AP.CD_PERFIL = PU.cd_perfil
          INNER JOIN ACESSO A			ON A.CD_ACESSO  = AP.CD_ACESSO
               WHERE U.cd_usuario = '.$cod_usuario;

      $sql    = new sql;
      $res    = $sql-> query($strsql);

       while($r = $res->fetch()){
        $strs[] = $r['TIPO_ACESSO'];
       }

       $_SESSION['ACESSO'] = $strs;

    }

   /**
    * Verifica se o usuário tem acesso ao elemento passado por parametro
	* @param int $tpElemento Constante do elemento que está sendo testado
    * @return bool True se tiver acesso, False se não tiver acesso
    **/
    public function permitido($tpElemento){
       return in_array($tpElemento, $_SESSION['ACESSO']);
    }
    
    
    function SetBloqueio($tpElemento){
        
        if(!Acesso::permitido($tpElemento)){
            $acesso = sql::query("SELECT * FROM ACESSO WHERE TIPO_ACESSO = ".$tpElemento);
            if($acesso->count()==1){
                $r = $acesso->fetch();
                header('location:http://'.$_SERVER['HTTP_HOST'].'/index.php?bloqueado='.$r['CD_ACESSO']);
            } 
        }  
    }
    
    function GetBloqueio($cd_acesso){
        $acesso = sql::query("SELECT * FROM ACESSO WHERE CD_ACESSO = ".$cd_acesso);
        if($acesso->count()==1){
            $r = $acesso->fetch();
            $msg = 'De acordo com o seu PERFIL, você não tem acesso ao recurso: " '.$r['NOME_ACESSO'].'". Entre em contato com o seu administrador ';   
        }else{
            $msg = '';
        }
        
        return $msg;
    }
    
	function DaAcessoAoPerfil(){
		
	}
	
  }
