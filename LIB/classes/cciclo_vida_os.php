<?php
/**
 * Controla o ciclo de vida da ordem de serviço
 */
class cciclo_vida_os{
    /**
     * Define o status inicial a partir das caracteristicas da OS
     * @param cORDEMSERVICO $ordemservico
     */
    public static function statusInicial(cORDEMSERVICO $ordemservico){

    }

    /**
     * Retorna um array de status disponiveis, no formato array(nome_estado, código) a partir do status atual de uma OS
     * @param type $ordemservico
     */
    public static function statusDisponiveis(cORDEMSERVICO $ordemservico){
        $statusAtual = $ordemservico->mID_STATUS_SOL;
        $disponiveis = array();

        switch($statusAtual){
            case cSTATUS_SOLICITACAO::ssNOVA:
                $disponiveis[] = array('pendente', cSTATUS_SOLICITACAO::ssPENDENTE);
                $disponiveis[] = array('devolver', cSTATUS_SOLICITACAO::ssDEVOLVIDA);
                break;
            case cSTATUS_SOLICITACAO::ssPENDENTE:
                $disponiveis[] = array('devolver', cSTATUS_SOLICITACAO::ssDEVOLVIDA);
                break;
            case cSTATUS_SOLICITACAO::ssDEVOLVIDA:
                $disponiveis[] = array('enviada bsb', cSTATUS_SOLICITACAO::ssENVIADA_BSB);
                break;
            case cSTATUS_SOLICITACAO::ssENVIADA_BSB:
                $disponiveis[] = array('pendência BSB', cSTATUS_SOLICITACAO::ssPENDENCIA_BSB);
                $disponiveis[] = array('aceitar', cSTATUS_SOLICITACAO::ssACEITA_BSB);
                $disponiveis[] = array('protocolada', cSTATUS_SOLICITACAO::ssPROTOCOLADA);
                break;
            case cSTATUS_SOLICITACAO::ssPENDENCIA_BSB:
                $disponiveis[] = array('liberar pendência', cSTATUS_SOLICITACAO::ssPENDENCIA_LIBERADA);
                $disponiveis[] = array('cancelar', cSTATUS_SOLICITACAO::ssCANCELADA);
                break;
            case cSTATUS_SOLICITACAO::ssPENDENCIA_LIBERADA:
                $disponiveis[] = array('aceitar', cSTATUS_SOLICITACAO::ssACEITA_BSB);
                $disponiveis[] = array('protocolada', cSTATUS_SOLICITACAO::ssPROTOCOLADA);
                break;
            case cSTATUS_SOLICITACAO::ssACEITA_BSB:
                $disponiveis[] = array('protocolada', cSTATUS_SOLICITACAO::ssPROTOCOLADA);
                $disponiveis[] = array('cancelar', cSTATUS_SOLICITACAO::ssCANCELADA);
                break;
            case cSTATUS_SOLICITACAO::ssPROTOCOLADA:
                $disponiveis[] = array('não cadastrada', cSTATUS_SOLICITACAO::ssNAO_CADASTRADA);
                $disponiveis[] = array('em análise', cSTATUS_SOLICITACAO::ssEM_ANALISE);
                $disponiveis[] = array('em exigência', cSTATUS_SOLICITACAO::ssEM_EXIGENCIA);
                $disponiveis[] = array('juntada', cSTATUS_SOLICITACAO::ssJUNTADA);
                $disponiveis[] = array('prop. def.', cSTATUS_SOLICITACAO::ssPROP_DEFERIMENTO);
                $disponiveis[] = array('prop. indef.', cSTATUS_SOLICITACAO::ssPROP_INDEFERIMENTO);
                $disponiveis[] = array('ped. arquiv.', cSTATUS_SOLICITACAO::ssPED_ARQUIVAMENTO);
                $disponiveis[] = array('cancelar', cSTATUS_SOLICITACAO::ssCANCELADA);
                break;
            case cSTATUS_SOLICITACAO::ssNAO_CADASTRADA:
                $disponiveis[] = array('em análise', cSTATUS_SOLICITACAO::ssEM_ANALISE);
                $disponiveis[] = array('em exigência', cSTATUS_SOLICITACAO::ssEM_EXIGENCIA);
                $disponiveis[] = array('juntada', cSTATUS_SOLICITACAO::ssJUNTADA);
                $disponiveis[] = array('prop. def.', cSTATUS_SOLICITACAO::ssPROP_DEFERIMENTO);
                $disponiveis[] = array('prop. indef.', cSTATUS_SOLICITACAO::ssPROP_INDEFERIMENTO);
                $disponiveis[] = array('ped. arquiv.', cSTATUS_SOLICITACAO::ssPED_ARQUIVAMENTO);
                $disponiveis[] = array('cancelar', cSTATUS_SOLICITACAO::ssCANCELADA);
                break;
            case cSTATUS_SOLICITACAO::ssEM_ANALISE:
                $disponiveis[] = array('em exigência', cSTATUS_SOLICITACAO::ssEM_EXIGENCIA);
                $disponiveis[] = array('juntada', cSTATUS_SOLICITACAO::ssJUNTADA);
                $disponiveis[] = array('prop. def.', cSTATUS_SOLICITACAO::ssPROP_DEFERIMENTO);
                $disponiveis[] = array('prop. indef.', cSTATUS_SOLICITACAO::ssPROP_INDEFERIMENTO);
                $disponiveis[] = array('ped. arquiv.', cSTATUS_SOLICITACAO::ssPED_ARQUIVAMENTO);
                $disponiveis[] = array('cancelar', cSTATUS_SOLICITACAO::ssCANCELADA);
                break;
            case cSTATUS_SOLICITACAO::ssEM_EXIGENCIA:
                $disponiveis[] = array('em análise', cSTATUS_SOLICITACAO::ssEM_ANALISE);
                $disponiveis[] = array('juntada', cSTATUS_SOLICITACAO::ssJUNTADA);
                $disponiveis[] = array('prop. def.', cSTATUS_SOLICITACAO::ssPROP_DEFERIMENTO);
                $disponiveis[] = array('prop. indef.', cSTATUS_SOLICITACAO::ssPROP_INDEFERIMENTO);
                $disponiveis[] = array('ped. arquiv.', cSTATUS_SOLICITACAO::ssPED_ARQUIVAMENTO);
                $disponiveis[] = array('cancelar', cSTATUS_SOLICITACAO::ssCANCELADA);
                break;
            case cSTATUS_SOLICITACAO::ssJUNTADA:
                $disponiveis[] = array('em análise', cSTATUS_SOLICITACAO::ssEM_ANALISE);
                $disponiveis[] = array('em exigência', cSTATUS_SOLICITACAO::ssEM_EXIGENCIA);
                $disponiveis[] = array('prop. def.', cSTATUS_SOLICITACAO::ssPROP_DEFERIMENTO);
                $disponiveis[] = array('prop. indef.', cSTATUS_SOLICITACAO::ssPROP_INDEFERIMENTO);
                $disponiveis[] = array('ped. arquiv.', cSTATUS_SOLICITACAO::ssPED_ARQUIVAMENTO);
                $disponiveis[] = array('cancelar', cSTATUS_SOLICITACAO::ssCANCELADA);
                break;
            case cSTATUS_SOLICITACAO::ssPROP_DEFERIMENTO:
            case cSTATUS_SOLICITACAO::ssPROP_INDEFERIMENTO:
            case cSTATUS_SOLICITACAO::ssPED_ARQUIVAMENTO:
                $disponiveis[] = array('deferida', cSTATUS_SOLICITACAO::ssDEFERIDA);
                $disponiveis[] = array('indeferida', cSTATUS_SOLICITACAO::ssINDEFERIDA);
                $disponiveis[] = array('arquivada', cSTATUS_SOLICITACAO::ssARQUIVADA);
                $disponiveis[] = array('cancelar', cSTATUS_SOLICITACAO::ssCANCELADA);
                break;
        }
        return $disponiveis;
    }

    public static function acoesDisponiveis(cORDEMSERVICO $os){
        $statusDisponiveis = self::statusDisponiveis($os);
        $acoes = array();
        foreach($statusDisponiveis as $status){
            $acao = new cACAO_AJAX('cCTRL_UPDT_SOLV', 'AlterarStatus', $status[0]);
            $acao->adicionaCampo('id_solicita_visto', $os->mID_SOLICITA_VISTO);
            $acao->adicionaCampo('id_status_sol', $status[1]);
            $acoes[] = $acao;
        }
        return $acoes;
    }

    public function alteraStatusPara($ordemservico, $id_status_sol){

    }
}
