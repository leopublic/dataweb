<?php

// Ã¡
class cORDEMSERVICO extends cMODELO {

    public $mNU_SOLICITACAO;
    public $mNU_EMPRESA;
    public $mNU_EMBARCACAO_PROJETO;
    public $mNU_USUARIO_CAD;
    public $mcd_tecnico;
    public $mcd_admin_cad;
    public $mcd_admin_fim;
    public $mdt_solicitacao;
    public $mde_observacao;
    public $mno_solicitador;
    public $mID_SOLICITA_VISTO;
    public $mcd_libs_finan;
    public $mdt_finalizacao;
    public $mnm_arquivo_sol;
    public $mdt_envio_bsb;
    public $mdt_recebimento_bsb;
    public $mdt_liberacao;
    public $mcd_usuario_qualidade;
    public $mfl_conclusao_conforme;
    public $mfl_liberado;
    public $mde_comentarios_liberacao;
    public $mcd_usuario_liberacao;
    public $msoli_dt_alteracao;
    public $mhios_id_atual;
    public $msoli_qt_autenticacoes;
    public $msoli_qt_autenticacoes_bsb;
    public $msoli_tx_justificativa;
    public $mcontroller;
    public $mmetodo;
    public $msoli_fl_incompleta;
    public $mcd_usuario_faturador_temp;
    public $mCandidatos;
    public $mnome_cadastro;
    public $mdt_cadastro;
    public $mID_TIPO_ACOMPANHAMENTO;
    public $mID_TIPO_ACOMPANHAMENTO_ANT;
    public $mFL_MULTI_CANDIDATOS;
    public $mID_STATUS_SOL;
    public $mNO_STATUS_SOL;
    public $mReadonly;
    public $mfl_nao_conformidade;
    public $mDT_PRAZO_ESTADA_SOLICITADO;
    public $mnu_empresa_requerente;
    public $mfl_cobrado;
    public $mNU_SERVICO;
    public $mNU_FORMULARIOS;
    public $mNU_FORMULARIOS_OS;
    public $mNU_FORMULARIOS_CAND;
    public $mNO_SERVICO;
    public $mNO_SERVICO_RESUMIDO;
    public $mfl_liberacao_fechada;
    public $mfl_qualidade_fechada;
    public $mNO_RAZAO_SOCIAL;
    public $mNO_EMBARCACAO_PROJETO;
    public $mNO_RAZAO_SOCIAL_REQUERENTE;
    public $mnome_criada_por;
    public $mnome_responsavel;
    public $mnome_liberacao_cobranca;
    public $mNU_EMPRESA_COBRANCA;
    public $mNU_EMBARCACAO_PROJETO_COBRANCA;
    public $mNO_EMBARCACAO_PROJETO_COBRANCA;
    public $mresc_id;
    public $mtbpc_id;
    public $mtbpc_id_empresa;
    public $mstco_id;
    public $mstco_tx_nome;
    public $msoli_vl_cobrado;
    public $msoli_tx_obs_cobranca;
    public $msoli_dt_liberacao_cobranca;
    public $msoli_dt_cobranca;
    public $mcd_usuario_liberacao_cobranca;
    public $mcd_usuario_cobranca;
    public $mcd_usuario_alteracao;
    public $mserv_fl_candidato_obrigatorio;
    public $mtppr_id;
    public $mtppr_tx_nome;
    public $mfl_sistema_antigo;
    public $mcd_usuario_analista;
    public $msoli_tx_solicitante_cobranca;
    public $msoli_tx_codigo_servico;
    public $mtbpc_tx_nome;
    public $msoli_tx_descricao_servico;
    public $msoli_dt_devolucao;
    public $mcd_usuario_devolucao;
    public $nova;
    public $mde_observacao_bsb;
    public $soli_qt_autenticacoes;
    public $mnu_servico_pacote;
    public $mid_solicita_visto_pacote;          // Indica o número da OS que abriu o pacote onde essa OS está
    public $mFL_ENCERRADA;
    public $mid_tipo_envio;
    public $msoli_fl_disponivel;                // Indica que essa OS é de um pacote mas ainda não foi utilizada
    public $msoli_fl_pacote_incompleto;         // Indica que o pacote dessa OS ainda possui serviços não executados
    protected $servico;
    private $res;
    private $rs_eof;

    const mEscopo = 'cORDEMSERVICO';
    const op_InverterNaoConformidade = 'OS_inverterNaoConformidade';
    const op_ConcluirOperacao = 'OS_';
    const op_Cobrar = 'OS_Cobrar';
    const STATUS_INICIAL = "1";

    /*
     * @param sem parametros
     */

    public function __construct() {
        $this->mID_STATUS_SOL = 0;
    }

    public static function get_nomeCampoChave() {
        return 'id_solicita_visto';
    }

    public static function get_nomeTabelaBD() {
        return 'solicita_visto';
    }

    public function get_nomeTabela() {
        return self::get_nomeTabelaBD();
    }

    public function campoid() {
        return self::get_nomeCampoChave();
    }

    public function getid() {
        return $this->mID_SOLICITA_VISTO;
    }

    public function sql_RecuperePeloId() {
        $sql = "     SELECT sv.nu_solicitacao, sv.nu_empresa NU_EMPRESA, sv.NU_SERVICO";
        $sql .= "         , date_format(sv.dt_cadastro, '%d/%m/%Y') dt_cadastro";
        $sql .= "         , sv.ID_STATUS_SOL, sv.fl_nao_conformidade";
        $sql .= "         , ac.NU_CANDIDATO";
        $sql .= "         , sv.NU_EMBARCACAO_PROJETO";
        $sql .= "         , sv.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "         , sv.nu_empresa_requerente";
        $sql .= "         , sv.fl_liberacao_fechada";
        $sql .= "         , sv.fl_qualidade_fechada";
        $sql .= "         , sv.tppr_id";
        $sql .= "         , no_solicitador";
        $sql .= "         , de_observacao";
        $sql .= "         , date_format(dt_solicitacao, '%d/%m/%Y') dt_solicitacao";
        $sql .= "         , sv.cd_tecnico";
        $sql .= "         , u.nome";
        $sql .= "         , S.ID_TIPO_ACOMPANHAMENTO";
        $sql .= "         , S.serv_fl_candidato_obrigatorio";
        $sql .= "         , TA.FL_MULTI_CANDIDATOS";
        $sql .= "         , SS.NO_STATUS_SOL";
        $sql .= "         , ac.DT_PRAZO_ESTADA_SOLICITADO";
        $sql .= "         , fl_cobrado";
        $sql .= "         , NO_SERVICO_RESUMIDO";
        $sql .= "         , NO_SERVICO";
        $sql .= "         , (SELECT ifnull(COUNT(*),0) FROM TIPO_FORMULARIO TF, FORMULARIO_ACOMPANHAMENTO FA WHERE FA.ID_TIPO_FORMULARIO =TF.ID_TIPO_FORMULARIO AND TF.FL_OS = 1 AND FA.ID_TIPO_ACOMPANHAMENTO = S.ID_TIPO_ACOMPANHAMENTO) NU_FORMULARIOS_OS";
        $sql .= "         , (SELECT ifnull(COUNT(*),0) FROM TIPO_FORMULARIO TF, FORMULARIO_ACOMPANHAMENTO FA WHERE FA.ID_TIPO_FORMULARIO =TF.ID_TIPO_FORMULARIO AND TF.FL_CANDIDATO = 1 AND ID_TIPO_ACOMPANHAMENTO = S.ID_TIPO_ACOMPANHAMENTO) NU_FORMULARIOS_CAND";
        $sql .= "         , E.NO_RAZAO_SOCIAL";
        $sql .= "         , E2.NO_RAZAO_SOCIAL NO_RAZAO_SOCIAL_REQUERENTE";
        $sql .= "         , EP.NO_EMBARCACAO_PROJETO";
        $sql .= "         , EP2.NO_EMBARCACAO_PROJETO NO_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "         , tppr_tx_nome";
        $sql .= "         , stco_tx_nome";
        $sql .= "         , tbpc_tx_nome";
        $sql .= "         , FL_ENCERRADA";
        $sql .= "         , id_tipo_envio";
        $sql .= "      FROM solicita_visto sv        ";
        $sql .= "      JOIN EMPRESA                E on E.NU_EMPRESA = sv.NU_EMPRESA";
        $sql .= "      JOIN EMBARCACAO_PROJETO    EP on EP.NU_EMPRESA = sv.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO";
        $sql .= " LEFT JOIN EMPRESA				  E2 on E2.NU_EMPRESA = sv.nu_empresa_requerente ";
        $sql .= " LEFT JOIN EMBARCACAO_PROJETO   EP2 on EP2.NU_EMPRESA = sv.nu_empresa_requerente and EP2.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= " LEFT JOIN AUTORIZACAO_CANDIDATO ac on ac.id_solicita_visto = sv.id_solicita_visto";
        $sql .= " LEFT JOIN SERVICO                S on S.NU_SERVICO = sv.NU_SERVICO";
        $sql .= " LEFT JOIN TIPO_ACOMPANHAMENTO   TA on TA.ID_TIPO_ACOMPANHAMENTO = S.ID_TIPO_ACOMPANHAMENTO";
        $sql .= " LEFT JOIN usuarios               u on u.cd_usuario = sv.cd_admin_cad";
        $sql .= " LEFT JOIN STATUS_SOLICITACAO    SS on SS.ID_STATUS_SOL = sv.ID_STATUS_SOL ";
        $sql .= " LEFT JOIN tipo_preco			  tp on tp.tppr_id = sv.tppr_id";
        $sql .= " LEFT JOIN status_cobranca_os	  sc on sc.stco_id = sv.stco_id";
        $sql .= " LEFT JOIN tabela_precos 		 tbp on tbp.tbpc_id = E2.tbpc_id";
        $sql .= "     WHERE sv.id_solicita_visto =" . $this->mID_SOLICITA_VISTO;
        return $sql;
    }

    public function isPosted() {
        if ($this->g('ID_SOLICITA_VISTO')) {
            $this->mID_SOLICITA_VISTO = $this->g('ID_SOLICITA_VISTO');

            return true;
        } else {
            return false;
        }
    }

    public function Loaded() {
        $res = false;
        if ($this->isPosted()) {
            $this->RecuperarSolicitaVisto();
            $res = true;
        }
        return $res;
    }

    public function get_fechada() {
        if ($this->mFL_ENCERRADA) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return cSERVICO
     */
    public function get_servico() {
        if (is_object($this->servico)) {
            return $this->servico;
        } else {
            if ($this->mNU_SERVICO > 0) {
                $this->servico = new cSERVICO();
                $this->servico->RecuperePeloId($this->mNU_SERVICO);
            } else {
                $this->servico = null;
            }
        }
        return $this->servico;
    }

    public function Recuperar($pID_SOLICITA_VISTO = '') {
        $this->mCandidatos = '';
        $virg = '';
        if ($pID_SOLICITA_VISTO != '') {
            $this->mID_SOLICITA_VISTO = $pID_SOLICITA_VISTO;
        }
        $this->RecuperarSolicitaVisto();
    }

    public function RecuperarSolicitaVisto() {
        $sql = "     SELECT sv.nu_solicitacao, sv.nu_empresa, sv.NU_SERVICO";
        $sql .= "         , date_format(sv.dt_cadastro, '%d/%m/%Y') dt_cadastro";
        $sql .= "         , sv.ID_STATUS_SOL, sv.fl_nao_conformidade";
        $sql .= "         , ac.NU_CANDIDATO";
        $sql .= "         , sv.NU_EMBARCACAO_PROJETO";
        $sql .= "         , sv.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "         , sv.nu_empresa_requerente";
        $sql .= "         , sv.fl_liberacao_fechada";
        $sql .= "         , sv.fl_qualidade_fechada";
        $sql .= "         , sv.tppr_id";
        $sql .= "         , sv.soli_vl_cobrado";
        $sql .= "         , sv.fl_sistema_antigo";
        $sql .= "         , sv.stco_id";
        $sql .= "         , sv.soli_tx_obs_cobranca";
        $sql .= "         , sv.de_observacao_bsb";
        $sql .= "         , sv.id_solicita_visto_pacote";
        $sql .= "         , sv.nu_servico_pacote";
        $sql .= "         , no_solicitador";
        $sql .= "         , de_observacao";
        $sql .= "         , date_format(dt_solicitacao, '%d/%m/%Y') dt_solicitacao";
        $sql .= "         , sv.cd_tecnico";
        $sql .= "         , u.nome";
        $sql .= "         , S.ID_TIPO_ACOMPANHAMENTO";
        $sql .= "         , S.serv_fl_candidato_obrigatorio";
        $sql .= "         , TA.FL_MULTI_CANDIDATOS";
        $sql .= "         , SS.NO_STATUS_SOL";
        $sql .= "         , ac.DT_PRAZO_ESTADA_SOLICITADO";
        $sql .= "         , fl_cobrado";
        $sql .= "         , NO_SERVICO_RESUMIDO";
        $sql .= "         , NO_SERVICO";
        $sql .= "         , (SELECT ifnull(COUNT(*),0) FROM TIPO_FORMULARIO TF, FORMULARIO_ACOMPANHAMENTO FA WHERE FA.ID_TIPO_FORMULARIO =TF.ID_TIPO_FORMULARIO AND TF.FL_OS = 1 AND FA.ID_TIPO_ACOMPANHAMENTO = S.ID_TIPO_ACOMPANHAMENTO) NU_FORMULARIOS_OS";
        $sql .= "         , (SELECT ifnull(COUNT(*),0) FROM TIPO_FORMULARIO TF, FORMULARIO_ACOMPANHAMENTO FA WHERE FA.ID_TIPO_FORMULARIO =TF.ID_TIPO_FORMULARIO AND TF.FL_CANDIDATO = 1 AND ID_TIPO_ACOMPANHAMENTO = S.ID_TIPO_ACOMPANHAMENTO) NU_FORMULARIOS_CAND";
        $sql .= "         , E.NO_RAZAO_SOCIAL";
        $sql .= "         , E2.NO_RAZAO_SOCIAL NO_RAZAO_SOCIAL_REQUERENTE";
        $sql .= "         , EP.NO_EMBARCACAO_PROJETO";
        $sql .= "         , EP2.NO_EMBARCACAO_PROJETO NO_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "         , tppr_tx_nome";
        $sql .= "         , tbpc_tx_nome";
        $sql .= "         , FL_ENCERRADA";
        $sql .= "         , id_tipo_envio";
        $sql .= "      FROM solicita_visto sv        ";
        $sql .= "      JOIN EMPRESA E on E.NU_EMPRESA = sv.NU_EMPRESA";
        $sql .= " LEFT JOIN EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = sv.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO";
        $sql .= " LEFT JOIN EMPRESA E2 on E2.NU_EMPRESA = sv.nu_empresa_requerente ";
        $sql .= " LEFT JOIN EMBARCACAO_PROJETO EP2 on EP2.NU_EMPRESA = sv.nu_empresa_requerente and EP2.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= " LEFT JOIN AUTORIZACAO_CANDIDATO ac on ac.id_solicita_visto = sv.id_solicita_visto";
        $sql .= " LEFT JOIN SERVICO                S on S.NU_SERVICO = sv.NU_SERVICO";
        $sql .= " LEFT JOIN TIPO_ACOMPANHAMENTO   TA on TA.ID_TIPO_ACOMPANHAMENTO = S.ID_TIPO_ACOMPANHAMENTO";
        $sql .= " LEFT JOIN usuarios               u on u.cd_usuario = sv.cd_admin_cad";
        $sql .= " LEFT JOIN STATUS_SOLICITACAO    SS on SS.ID_STATUS_SOL = sv.ID_STATUS_SOL ";
        $sql .= " LEFT JOIN tipo_preco			  tp on tp.tppr_id = sv.tppr_id";
        $sql .= " LEFT JOIN tabela_precos 		 tbp on tbp.tbpc_id = E2.tbpc_id";
        $sql .= "     WHERE sv.id_solicita_visto =" . $this->mID_SOLICITA_VISTO;
        // $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $res = cAMBIENTE::$db_pdo->query($sql);
        if ($rs = $res->fetch()) {
            cBANCO::CarreguePropriedades($rs, $this);
            $this->mNU_EMPRESA = $rs['nu_empresa'];
            $this->mNU_SOLICITACAO = $rs['nu_solicitacao'];
            $this->mNU_FORMULARIOS = $this->mNU_FORMULARIOS_OS + $this->mNU_FORMULARIOS_CAND;
            if ($this->mFL_ENCERRADA) {
                $this->mReadonly = true;
            } else {
                $this->mReadonly = false;
            }
        } else {
            throw new Exception("Solicitação não encontrada (" . $this->mID_SOLICITA_VISTO . ")");
        }
    }

    public function RecuperarSolicitaVistoCobranca() {
        $sql = "     SELECT sv.nu_solicitacao, sv.nu_empresa, sv.NU_SERVICO";
        $sql .= "         , date_format(sv.dt_cadastro, '%d/%m/%Y') dt_cadastro";
        $sql .= "         , sv.NU_EMBARCACAO_PROJETO";
        $sql .= "         , sv.nu_empresa_requerente";
        $sql .= "         , sv.NU_EMPRESA_COBRANCA";
        $sql .= "         , sv.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "         , sv.soli_vl_cobrado";
        $sql .= "         , sv.soli_tx_obs_cobranca";
        $sql .= "         , sv.soli_tx_solicitante_cobranca";
        $sql .= "         , sv.soli_tx_codigo_servico";
        $sql .= "         , sv.soli_tx_descricao_servico";
        $sql .= "         , sv.soli_dt_liberacao_cobranca";
        $sql .= "         , sv.soli_dt_cobranca";
        $sql .= "         , sv.cd_usuario_liberacao_cobranca";
        $sql .= "         , sv.cd_usuario_cobranca";
        $sql .= "         , sv.tbpc_id";
        $sql .= "         , sv.stco_id";
        $sql .= "         , sv.resc_id";
        $sql .= "         , sv.ID_STATUS_SOL";
        $sql .= "         , sv.fl_nao_conformidade";
        $sql .= "         , sv.fl_liberacao_fechada";
        $sql .= "         , sv.fl_qualidade_fechada";
        $sql .= "         , sv.tppr_id";
        $sql .= "         , sv.id_solicita_visto_pacote";
        $sql .= "         , sv.nu_servico_pacote";
        $sql .= "         , ac.NU_CANDIDATO";
        $sql .= "         , NO_EMBARCACAO_PROJETO";
        $sql .= "         , E.tbpc_id tbpc_id_empresa";
        $sql .= "         , no_solicitador";
        $sql .= "         , de_observacao";
        $sql .= "         , date_format(dt_solicitacao, '%d/%m/%Y') dt_solicitacao";
        $sql .= "         , sv.cd_tecnico";
        $sql .= "         , u.nome nome_criada_por";
        $sql .= "         , ut.nome nome_responsavel";
        $sql .= "         , S.ID_TIPO_ACOMPANHAMENTO";
        $sql .= "         , SS.NO_STATUS_SOL";
        $sql .= "         , ac.DT_PRAZO_ESTADA_SOLICITADO";
        $sql .= "         , fl_cobrado";
        $sql .= "         , NO_SERVICO_RESUMIDO";
        $sql .= "         , NO_SERVICO";
        $sql .= "         , E.NO_RAZAO_SOCIAL";
        $sql .= "         , EREQ.NO_RAZAO_SOCIAL NO_RAZAO_SOCIAL_REQUERENTE";
        $sql .= "         , E.tbpc_id";
        $sql .= "         , p.prec_vl_conceito";
        $sql .= "         , p.prec_vl_pacote";
        $sql .= "         , ul.nome nome_liberacao_cobranca";
        $sql .= "         , tp.tbpc_tx_nome";
        $sql .= "         , tpr.tppr_tx_nome";
        $sql .= "         , sco.stco_tx_nome";
        $sql .= "         , SS.FL_ENCERRADA";
        $sql .= "         , sv.id_tipo_envio";
        $sql .= "      FROM solicita_visto sv        ";
        $sql .= "      JOIN EMPRESA                E on E.NU_EMPRESA = sv.NU_EMPRESA";
        $sql .= " LEFT JOIN EMBARCACAO_PROJETO    EP on EP.NU_EMPRESA = sv.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO";
        $sql .= " LEFT JOIN EMPRESA			    EREQ on EREQ.NU_EMPRESA = sv.nu_empresa_requerente";
        $sql .= " LEFT JOIN AUTORIZACAO_CANDIDATO ac on ac.id_solicita_visto = sv.id_solicita_visto";
        $sql .= " LEFT JOIN SERVICO                S on S.NU_SERVICO = sv.NU_SERVICO";
        $sql .= " LEFT JOIN usuarios               u on u.cd_usuario = sv.cd_admin_cad";
        $sql .= " LEFT JOIN usuarios              ut on ut.cd_usuario = sv.cd_tecnico";
        $sql .= " LEFT JOIN usuarios              ul on ul.cd_usuario = sv.cd_usuario_liberacao_cobranca";
        $sql .= " LEFT JOIN STATUS_SOLICITACAO    SS on SS.ID_STATUS_SOL = sv.ID_STATUS_SOL ";
        $sql .= " LEFT JOIN tabela_precos         tp on tp.tbpc_id = coalesce(sv.tbpc_id,E.tbpc_id)";
        $sql .= " LEFT JOIN tipo_preco			 tpr on tpr.tppr_id = sv.tppr_id";
        $sql .= " LEFT JOIN status_cobranca_os	 sco on sco.stco_id = sv.stco_id";
        $sql .= " LEFT JOIN preco		           p on p.tbpc_id = tp.tbpc_id and p.NU_SERVICO = sv.NU_SERVICO";
        $sql .= " LEFT JOIN resumo_cobranca		  rc on rc.resc_id = sv.resc_id";
        $sql .= "     WHERE sv.id_solicita_visto =" . $this->mID_SOLICITA_VISTO;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        if ($rs = mysql_fetch_array($res)) {
            cBANCO::CarreguePropriedades($rs, $this);
            $this->mNU_EMPRESA = $rs['nu_empresa'];
            $this->mNU_SOLICITACAO = $rs['nu_solicitacao'];
            if (intval($this->msoli_vl_cobrado) == 0) {
                if ($this->mtppr_id == 1) {
                    $this->msoli_vl_cobrado = 0;
                } elseif ($this->mtppr_id == 2) {
                    $this->msoli_vl_cobrado = $rs['prec_vl_conceito'];
                } else {
                    $this->msoli_vl_cobrado = $rs['prec_vl_pacote'];
                }
            }
            if ($this->mFL_ENCERRADA) {
                $this->mReadonly = true;
            } else {
                $this->mReadonly = false;
            }
        } else {
            throw new Exception("Solicitação não encontrada (" . $pID_SOLICITA_VISTO . ")");
        }
    }

    /**
     * Alimenta a propriedade mCandidatos com uma lista dos ids dos candidatos separados por virgula.
     * @param type $pID_SOLICITA_VISTO
     */
    public function RecuperarCandidatos($pID_SOLICITA_VISTO = '') {
        if ($pID_SOLICITA_VISTO != '') {
            $this->mID_SOLICITA_VISTO = $pID_SOLICITA_VISTO;
        }
        $this->mCandidatos = '';
        $virg = '';

        $sql = "SELECT ac.NU_CANDIDATO
		             , ac.NU_EMBARCACAO_PROJETO
		             , c.NOME_COMPLETO";
        $sql.= "  FROM AUTORIZACAO_CANDIDATO ac";
        $sql.= "  JOIN CANDIDATO             c on c.NU_CANDIDATO = ac.NU_CANDIDATO";
        $sql.= "  left join CANDIDATO cp    on cp.NU_CANDIDATO = c.nu_candidato_parente";
        $sql.= "  left join grau_parentesco gp on gp.co_grau_parentesco = c.co_grau_parentesco ";
        $sql.= " WHERE ac.ID_SOLICITA_VISTO =" . $this->mID_SOLICITA_VISTO;
        $sql.= " order by concat(coalesce(cp.NOME_COMPLETO, ''),c.NOME_COMPLETO) ";
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        if ($rs = mysql_fetch_array($res)) {
            $this->mCandidatos = $rs['NU_CANDIDATO'];
            while ($rs = mysql_fetch_array($res)) {
                $this->mCandidatos .= ',' . $rs['NU_CANDIDATO'];
            }
        }
    }

    public function SalvarInstancia() {
        //
        // Consistencias
        $msg = '';
        //if ($this->mID_SOLICITA_VISTO==0){
        if ($this->mNU_EMPRESA == '') {
            $msg .= '<br/>- Empresa não informada.';
        }

        //}
        if ($this->mNU_EMBARCACAO_PROJETO == '') {
            $msg .= '<br/>- Embarcação/projeto não informado.';
        }

        if (intval($this->mnu_empresa_requerente) == 0) {
            $this->mnu_empresa_requerente = $this->mNU_EMPRESA;
        }
        $tabela_precos = ctabela_precos::InstanciePelaEmpresa($this->mnu_empresa_requerente);
        if ($tabela_precos->mtbpc_id > 0 && !$tabela_precos->mtbpc_fl_tem_pacote && $this->mtppr_id == 3) {
            $msg .= '<br/>- A tabela de preços dessa empresa não admite cobrança por pacote. Verifique o tipo de cobrança informada.';
        }

        if ($this->mID_SOLICITA_VISTO == 0) {
            if (trim($this->mNU_SERVICO) == '' || $this->mNU_SERVICO == 0) {
                $msg .= '<br/>- Serviço não informado.';
            }
        }
        if (intval($this->mNU_SERVICO) > 0) {
            $this->RecuperaAtributosDoServico();
            if ($this->mserv_fl_candidato_obrigatorio) {
                if ($this->mCandidatos == '') {
                    $msg .= '<br/>- Inclua pelo menos um candidato na OS.(' . $this->mserv_fl_candidato_obrigatorio . ')';
                }

                if ($this->mFL_MULTI_CANDIDATOS == 0 || $this->mFL_MULTI_CANDIDATOS == '') {
                    $candidatosArray = explode(",", $this->mCandidatos);
                    if (count($candidatosArray) > 1) {
                        $msg .= "<br/>- Esse tipo de serviço só permite um candidato. Altere o tipo de serviço ou remova os candidatos excedentes.";
                    }
                }
            }
            if ($this->mID_TIPO_ACOMPANHAMENTO == 3) {  // Se for prorrogacao
                // Obriga o perÃ­odo a ser uma data vÃ¡lida.
                if ($this->mDT_PRAZO_ESTADA_SOLICITADO) {
                    $data = split("/", $this->mDT_PRAZO_ESTADA_SOLICITADO);
                    if (!checkdate($data[1], $data[0], $data[2])) {
                        $msg .= "<br/>- Para serviços de prorrogação, o prazo solicitado deve ser uma data válida (informe somente dia/mês/ano, ano com o século).";
                    }
                }
            }
            //Obtem tipo de acompanhamento anterior
        }
        if ($this->mNU_EMBARCACAO_PROJETO_COBRANCA == '') {
            $this->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->mNU_EMBARCACAO_PROJETO;
        }

        if ($msg != '') {
            throw new cERRO_CONSISTENCIA($msg);
        }

        $osAnt = new cORDEMSERVICO();
        if ($this->mID_SOLICITA_VISTO > 0) {
            $osAnt->Recuperar($this->mID_SOLICITA_VISTO);
            $osAnt->RecuperarSolicitaVistoCobranca();
        }

        $this->mID_TIPO_ACOMPANHAMENTO_ANT = "";
        if ($this->mID_SOLICITA_VISTO > 0) {
            $sql = "select sv.nu_servico, ID_TIPO_ACOMPANHAMENTO from solicita_visto sv, SERVICO S where S.NU_SERVICO = sv.NU_SERVICO and sv.id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
            $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            if ($rs = mysql_fetch_array($res)) {
                $servicoAnt = new cSERVICO();
                $servicoAnt->RecuperePeloId($rs['nu_servico']);
                $this->mID_TIPO_ACOMPANHAMENTO_ANT = $rs['ID_TIPO_ACOMPANHAMENTO'];
//				$processo_ant = cprocesso::FabricaProcesso($this->mID_TIPO_ACOMPANHAMENTO_ANT);
            }
        }

        //
        // Grava solicita_visto
        $this->nova = true;
        if ($this->mID_SOLICITA_VISTO > 0) {
            $this->nova = false;
        }

        // Se mudou o servico, recalcula o status
        if ($this->mID_SOLICITA_VISTO > 0
                && $this->mNU_SERVICO != $servicoAnt->mNU_SERVICO
                && $this->mID_TIPO_ACOMPANHAMENTO != $this->mID_TIPO_ACOMPANHAMENTO_ANT) {
            $this->CalculaStatusInicial($this->get_servico());
            $sql = "update solicita_visto "
                    . " set id_status_sol = " . $this->mID_STATUS_SOL ;
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__."-Recaulcua status" )
                   . " where id_solicita_visto =" . $this->mID_SOLICITA_VISTO;
            cAMBIENTE::ExecuteQuery($sql);
        }

        $this->SalvarSolicitaVisto();
        //
        // Loop pelos candidatos
        //error_log('Candidatos='.$this->mCandidatos);
        if ($this->mCandidatos != '') {
            $cand = preg_split("[,]", $this->mCandidatos);
            $i = 0;
            while ($i < count($cand)) {
                $this->AdicionarCandidatos($cand[$i]);  // Atualiza AUTORIZACAO_CANDIDATO
                $i++;
            }
        }

        if ($this->mID_TIPO_ACOMPANHAMENTO == 1) {
            $sql = "update AUTORIZACAO_CANDIDATO
					   set DT_PRAZO_AUTORIZACAO_MTE = DT_PRAZO_ESTADA_SOLICITADO
					 where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        }

        //
        // Atualizar link do processo com a autorizaÃ§ao correspondente
        //$this->RelacionarProcessoAutorizacao();
        //
		// Atualiza acompanhamento
        $this->InsereAcomp();
        //
        // Insere agenda e
        //  finan (sÃ³ na inclusÃ£o)
        if ($this->mID_SOLICITA_VISTO == 0) {
            $this->InsereAgendaExterna();
            $this->CriarFinanLibs();
            $sql = "update solicita_visto set cd_libs_finam = " . $this->mcd_libs_finam . " where id_solicita_visto=" . $this->mID_SOLICITA_VISTO;
        }
        //
        // Atualiza status
        /* Suspenso por solicitacao da Patricia 22/06/2011
          if ($this->mID_STATUS_SOL <= 1 ){
          if ($this->mID_TIPO_ACOMPANHAMENTO == 1 ){
          $sql = " select ifnull(count(COK.NU_CANDIDATO), 0) totalOK, ifnull(count(CNOK.NU_CANDIDATO), 0) totalNOK";
          $sql .= "  from AUTORIZACAO_CANDIDATO AC";
          $sql .= "  left join CANDIDATO COK on COK.NU_CANDIDATO      = AC.NU_CANDIDATO and COK.BO_CADASTRO_MINIMO_OK  = 1";
          $sql .= "  left join CANDIDATO CNOK on CNOK.NU_CANDIDATO    = AC.NU_CANDIDATO and CNOK.BO_CADASTRO_MINIMO_OK <> 1";
          $sql .= " where AC.id_solicita_visto = ".$this->mID_SOLICITA_VISTO;
          $res=mysql_query($sql);
          while ($rs = mysql_fetch_array($res)){
          if ($rs['totalNOK']==0){
          $sql = "update solicita_visto set ID_STATUS_SOL = 4 where id_solicita_visto = ".$this->mID_SOLICITA_VISTO;
          executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
          $this->mID_STATUS_SOL = 4;
          }
          else{
          $sql = "update solicita_visto set ID_STATUS_SOL = 1 where id_solicita_visto = ".$this->mID_SOLICITA_VISTO;
          executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
          $this->mID_STATUS_SOL = 1;
          }
          }
          }
          }
          else{
          $sql = "update solicita_visto set ID_STATUS_SOL = 4 where id_solicita_visto = ".$this->mID_SOLICITA_VISTO;
          executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
          $this->mID_STATUS_SOL = 4;
          }
         *
         */
        $this->RecuperarSolicitaVisto();
    }

    public function AvancarStatus() {
        $novoStatus = "";
        switch ($this->mID_STATUS_SOL) {
            case 1:
            case 2:
            case 3:
                $novoStatus = 4;
                break;
            case 6:
                break;
            default:
                $novoStatus = $this->mID_STATUS_SOL + 1;
        }
        if ($novoStatus > 0) {
            $sql = 'update solicita_visto '
                    . ' set ID_STATUS_SOL = ' . $novoStatus ;
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ )
                    . ' where id_solicita_visto = ' . $this->mID_SOLICITA_VISTO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $this->Recuperar($this->mID_SOLICITA_VISTO);
        }
    }

    public function RecuperaAtributosDoServico() {
        $sql = " select S.ID_TIPO_ACOMPANHAMENTO, FL_MULTI_CANDIDATOS, serv_fl_candidato_obrigatorio ";
        $sql .= "  from SERVICO S left join TIPO_ACOMPANHAMENTO TA on TA.ID_TIPO_ACOMPANHAMENTO = S.ID_TIPO_ACOMPANHAMENTO";
        $sql .= " where NU_SERVICO = " . $this->mNU_SERVICO;
        if ($res = mysql_query($sql)) {
            if ($row = mysql_fetch_array($res)) {
                $this->mID_TIPO_ACOMPANHAMENTO = $row['ID_TIPO_ACOMPANHAMENTO'];
                $this->mFL_MULTI_CANDIDATOS = $row['FL_MULTI_CANDIDATOS'];
                $this->mserv_fl_candidato_obrigatorio = $row['serv_fl_candidato_obrigatorio'];
            } else {
                throw new Exception(self::mEscopo . '.' . __CLASS__ . '->' . __FUNCTION__ . '-> SERVICO nÃ£o encontrado');
            }
        } else {
            throw new Exception(self::mEscopo . '.' . __CLASS__ . '->' . __FUNCTION__ . '-> Erro:' . mysql_error() . '<br/>SQL="' . $sql . '"');
        }
    }

    public function SalvarSolicitaVisto() {
        if ($this->dt_solicitacao == '') {
            $this->dt_solicitacao = date('d/m/Y');
        }

        if ($this->mNU_USUARIO_CAD == '') {
            $this->mNU_USUARIO_CAD = cSESSAO::$mcd_usuario;
        }

        if (intval($this->mID_SOLICITA_VISTO) == 0) {
//            $this->mNU_SOLICITACAO = cORDEMSERVICO_NOVA::proximoNumero($this);
            $this->mfl_nao_conformidade = 0;

            $ospacote = cORDEMSERVICO_PACOTE::pacoteAplicavel($this);
            $this->mnu_servico_pacote = $ospacote->mnu_servico_pacote;
            $this->mid_solicita_visto_pacote = $ospacote->mid_solicita_visto_pacote;
            $this->mtppr_id = $ospacote->mtppr_id;
            //
            // Cálculo do status inicial... (migrar para um controle de fluxo predefinido ou status inicial por serviço
            $this->CalculaStatusInicial($this->get_servico());

            $this->mstco_id = 1;
            //
            // Inclui solicita visto
            $sql = "INSERT INTO solicita_visto (";
            $sql .= "	  nu_solicitacao";
            $sql .= "	, nu_empresa";
            $sql .= "   , NU_EMBARCACAO_PROJETO";
            $sql .= "	, no_solicitador";
            $sql .= "	, dt_solicitacao";
            $sql .= "	, cd_admin_cad";
            $sql .= "	, dt_cadastro";
            $sql .= "	, de_observacao";
            $sql .= "   , NU_SERVICO ";
            $sql .= "   , ID_STATUS_SOL ";
            $sql .= "   , fl_nao_conformidade";
            $sql .= "   , nu_empresa_requerente";
            $sql .= "   , NU_EMBARCACAO_PROJETO_COBRANCA";
            $sql .= "   , tppr_id";
            $sql .= "   , stco_id";
            $sql .= "	, cd_usuario_analista";
            $sql .= "	, nu_servico_pacote";
            $sql .= "	, id_solicita_visto_pacote";
            $sql .= "	, cd_tecnico";
            $sql .= "	, cd_usuario_alteracao";
            $sql .= ") VALUES (";
            $sql .= "	  0";
            $sql .= "	, " . cBANCO::ChaveOk($this->mNU_EMPRESA);
            $sql .= "	, " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
            $sql .= "	, " . cBANCO::StringOk($this->mno_solicitador);
            $sql .= "	, " . cBanco::DataOk($this->mdt_solicitacao);
            $sql .= "	, " . cBanco::ChaveOk($this->mNU_USUARIO_CAD);
            $sql .= "	, now()";
            $sql .= "	, " . cBANCO::StringOk($this->mde_observacao);
            $sql .= "	, " . cBanco::ChaveOk($this->mNU_SERVICO);
            $sql .= "   , " . cBANCO::ChaveOk($this->mID_STATUS_SOL);
            $sql .= "   , " . cBANCO::SimNaoOk($this->mfl_nao_conformidade);
            $sql .= "	, " . cBANCO::ChaveOk($this->mnu_empresa_requerente);
            $sql .= "	, " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO_COBRANCA);
            $sql .= "	, " . cBANCO::ChaveOk($this->mtppr_id);
            $sql .= "	, " . cBANCO::ChaveOk($this->mstco_id);
            $sql .= "	, " . cBANCO::ChaveOk($this->mcd_usuario_analista);
            $sql .= "	, " . cBANCO::ChaveOk($this->mnu_servico_pacote);
            $sql .= "	, " . cBANCO::ChaveOk($this->mid_solicita_visto_pacote);
            $sql .= "	, " . cBANCO::ChaveOk($this->mcd_tecnico);
            $sql .= "	, " . cBANCO::ChaveOk($this->mcd_usuario_alteracao);
            $sql .= ");";
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $this->mID_SOLICITA_VISTO = mysql_insert_id();
            // Numera a OS
            $stmt = cAMBIENTE::$db_pdo->prepare("CALL os_gera_numero(?)");
            $stmt->bindParam(1, $this->mID_SOLICITA_VISTO, PDO::PARAM_INT);
            $stmt->execute();
            $rs = $stmt->fetchAll();
            $this->mNU_SOLICITACAO = $rs[0]['nu_solicitacao'];
            return $this->mID_SOLICITA_VISTO;

            $emb = new cEMBARCACAO_PROJETO();
            $emb->mNU_EMPRESA = $this->mNU_EMPRESA;
            $emb->mNU_EMBARCACAO_PROJETO = $this->mNU_EMBARCACAO_PROJETO;
            $emb->RecupereSe();
            if ($emb->mcd_usuario == '') {
                $emb->mcd_usuario = $this->mNU_USUARIO_CAD;
                $emb->AtualizeResponsavel();
            }
        } else {
            $sql = " update solicita_visto ";
            $sql .= "   set nu_empresa      = " . cBANCO::ChaveOk($this->mNU_EMPRESA);
            $sql .= "     , NU_EMBARCACAO_PROJETO      = " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
            $sql .= "     , no_solicitador  = " . cBANCO::StringOk($this->mno_solicitador);
            $sql .= "     , dt_solicitacao  = " . cBanco::DataOk($this->mdt_solicitacao);
            $sql .= "     , de_observacao   = " . cBANCO::StringOk($this->mde_observacao);
//			$sql .= "	  , NU_SERVICO      = ".cBanco::ChaveOk($this->mNU_SERVICO);
            $sql .= "	  , fl_nao_conformidade = " . cBANCO::SimNaoOk($this->mfl_nao_conformidade);
            $sql .= "     , nu_empresa_requerente= " . cBANCO::ChaveOk($this->mnu_empresa_requerente);
            $sql .= "     , NU_EMBARCACAO_PROJETO_COBRANCA = " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO_COBRANCA);
            $sql .= "     , tppr_id				= " . cBANCO::ChaveOk($this->mtppr_id);
            $sql .= "	  , cd_usuario_analista  = " . cBanco::ChaveOk($this->mcd_usuario_analista);
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
            $sql .= " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        }

        $this->RecuperaAtributosDoServico();
    }

    public function CalculaStatusInicial($servico) {
        if ($servico->mserv_fl_revisao_analistas == 1) {
            $this->mID_STATUS_SOL = cSTATUS_SOLICITACAO::ssNOVA;  //Nova
        } else {
            if ($servico->mserv_fl_envio_bsb == 1) {
                $this->mID_STATUS_SOL = cSTATUS_SOLICITACAO::ssDEVOLVIDA; // Devolvida
            } else {
                $this->mID_STATUS_SOL = cSTATUS_SOLICITACAO::ssPROTOCOLADA; // Protocolada
            }
        }
    }

    /**
     * Atualiza dados de cobranca: preco, descricao do servico, codigo, a partir dos valores
     * da tabela de precos da empresa da OS e da OS, enquanto a OS nao estiver faturada
     */
    public function AtualizaCobrancaPelaTabela() {
        //
        // Recupera status da cobranca
        $this->RecuperarSolicitaVistoCobranca();
        if ($this->mstco_id != 3) {  // Se nÃ£o estiver faturada...
            //
			// Atualiza solicitante de cobranca
            $sql = "update solicita_visto
					set soli_tx_solicitante_cobranca = no_solicitador";
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
					where stco_id <> 3
					and id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
            cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            //
            // Atualiza preÃ§o e descriÃ§Ã£o do serviÃ§o se houver tabela de preÃ§o associada a empresa
            $updateValorPreco = "";
            if ($this->mtppr_id == 2) {
                $updateValorPreco = ", solicita_visto.soli_vl_cobrado = preco.prec_vl_conceito";
            } elseif ($this->mtppr_id == 3) {
                $updateValorPreco = ", solicita_visto.soli_vl_cobrado = preco.prec_vl_pacote";
            }
            if ($this->mtppr_id == 2 || $this->mtppr_id == 3) {
                $sql = "update solicita_visto, empresa, tabela_precos, preco
						set solicita_visto.soli_tx_descricao_servico = preco.prec_tx_descricao_tabela_precos
						  , solicita_visto.soli_tx_codigo_servico = preco.prec_tx_item
						" . $updateValorPreco;
                $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
                $sql .= "    where solicita_visto.id_solicita_visto = " . $this->mID_SOLICITA_VISTO . "
						and solicita_visto.stco_id = 1
						and empresa.nu_empresa = solicita_visto.nu_empresa
						and tabela_precos.tbpc_id = empresa.tbpc_id
						and preco.tbpc_id = tabela_precos.tbpc_id
						and preco.nu_servico = solicita_visto.nu_servico
						";
                cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            }
            //
            // Atualiza a descriÃ§Ã£o conforme serviÃ§o da OS caso nÃ£o haja tabela associada.
            $sql = "update solicita_visto, servico
					set solicita_visto.soli_tx_descricao_servico = servico.NO_SERVICO_RESUMIDO";
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
					where solicita_visto.id_solicita_visto = " . $this->mID_SOLICITA_VISTO . "
					and solicita_visto.stco_id = 1
					and servico.NU_SERVICO = solicita_visto.NU_SERVICO
					and soli_tx_descricao_servico is null;
					";
            cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        }
    }

    public function CriarFinanLibs() {
        $sql = "INSERT INTO finan_libs (";
        $sql .= "  cd_tipo_sol";
        $sql .= ", nu_solicitacao";
        $sql .= ", cd_admin";
        $sql .= ", dt_cadastro";
        $sql .= ", cd_ano";
        $sql .= ") VALUES (";
        $sql .= "  'V'";
        $sql .= ", " . $this->mNU_SOLICITACAO;
        $sql .= ", " . cBanco::ChaveOk($this->mNU_USUARIO_CAD);
        $sql .= ", now()";
        $sql .= ", year(now())";
        $sql .= ")";
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mcd_libs_finam = mysql_insert_id();
        return $this->mcd_libs_finam;
    }

    public function InsereAcomp() {
        $msg = "";
        $sql = "select cd_solicitacao from solicita_visto_acomp where cd_solicitacao = " . $this->mNU_SOLICITACAO;
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $sql = " update solicita_visto_acomp ";
                $sql .= "   set cd_tecnico = " . cBANCO::ChaveOk($this->mcd_tecnico);
                $sql .= "     , nu_projeto = " . $this->mNU_EMBARCACAO_PROJETO;
                $sql .= " where cd_solicitacao = " . $this->mNU_SOLICITACAO;
            } else {
                $sql = "INSERT INTO solicita_visto_acomp (cd_solicitacao,nu_empresa,cd_coordenador,cd_tecnico,nu_projeto,dt_sol) ";
                $sql.=" VALUES (";
                $sql.=" " . $this->mNU_SOLICITACAO;
                $sql.=" ," . cBANCO::ChaveOk($this->mNU_EMPRESA);
                $sql.=" ," . cBANCO::ChaveOk($this->mNU_USUARIO_CAD);
                $sql.=" ," . cBANCO::ChaveOk($this->mcd_tecnico);
                $sql.=" ," . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
                $sql.=" , now() ";
                $sql.=" ) ";
            }
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        } else {
            //gravaLogErro($sql,$ret,"ACOMPANHA","SOLICITACAO");
            throw new Exception(self::mEscopo . '.InsereAcomp(' . $this->mcd_tecnico . ')-> Erro:' . mysql_error() . '<br/>SQL="' . $sql . '"');
        }
    }

    function InsereAgendaExterna($cd_status) {
        $sql = "select NO_RAZAO_SOCIAL from EMPRESA where NU_EMPRESA = " . $this->mNU_EMPRESA;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $rs = mysql_fetch_array($res);
        $NO_RAZAO_SOCIAL = $rw[0];
        $texto = "Solicitação número $this->mNU_SOLICITACAO da empresa $. Favor proceder com o cadastro.";

        $codigo = GeraCodigo('agenda_usuario', 'cd_agenda');
        $sql = "insert into agenda_usuario (cd_agenda,cd_usuario,cd_adm_cad,cd_adm_ult,dia,hora,cd_status,ds_agenda,dt_cadastro,dt_ult) ";
        $sql = $sql . " values ($codigo,$this->mcd_tecnico,$this->mNU_USUARIO_CAD,$this->mNU_USUARIO_CAD,now(),'$hora','C','$texto',now(),now())";
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        return $ret;
    }

    public function AdicionarCandidatos($pNU_CANDIDATO) {
        $cand = new cCANDIDATO();
        $cand->mNU_CANDIDATO = $pNU_CANDIDATO;
        $cand->Recuperar();

        $sql = "select NU_CANDIDATO from AUTORIZACAO_CANDIDATO where NU_CANDIDATO = " . $pNU_CANDIDATO . " and ID_SOLICITA_VISTO=" . $this->mID_SOLICITA_VISTO;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        if ($rs = mysql_fetch_array($res)) {
            $sql = "update AUTORIZACAO_CANDIDATO ";
            $sql .= "   set NU_EMPRESA            = " . $this->mNU_EMPRESA;
            $sql .= "     , NU_EMBARCACAO_PROJETO = " . $this->mNU_EMBARCACAO_PROJETO;
            $sql .= "     , DT_ULT_ALTERACAO      = now()";
            $sql .= "     , DT_PRAZO_ESTADA_SOLICITADO		  = " . cBanco::StringOk($this->mDT_PRAZO_ESTADA_SOLICITADO);
            $sql .= " where id_solicita_visto     = " . $this->mID_SOLICITA_VISTO;
            $sql .= "   and NU_CANDIDATO          = " . $pNU_CANDIDATO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        } else {
            $sql = "insert into AUTORIZACAO_CANDIDATO (";
            $sql .= "   ID_SOLICITA_VISTO";
            $sql .= " , NU_EMPRESA";
            $sql .= " , NU_CANDIDATO";
            $sql .= " , NU_SOLICITACAO";
            $sql .= " , NU_EMBARCACAO_PROJETO";
            $sql .= " , NU_EMBARCACAO_INICIAL";
            $sql .= " , DT_CADASTRAMENTO";
            $sql .= " , NU_USUARIO_CAD";
            $sql .= " , DT_PRAZO_ESTADA_SOLICITADO) ";
            $sql .= " VALUES (";
            $sql .= "  " . $this->mID_SOLICITA_VISTO;
            $sql .= " ," . $this->mNU_EMPRESA;
            $sql .= " ," . $pNU_CANDIDATO;
            $sql .= " ," . $this->mNU_SOLICITACAO;
            $sql .= " ," . $this->mNU_EMBARCACAO_PROJETO;
            $sql .= " ," . $this->mNU_EMBARCACAO_PROJETO;
            $sql .= " , now()";
            $sql .= " ," . $this->mNU_USUARIO_CAD;
            $sql .= " ," . cBanco::StringOk($this->mDT_PRAZO_ESTADA_SOLICITADO);
            $sql .= ")";
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            $cand->AdicionadoAOS($this);
            $msg = 'Adicionado o candidato "' . $cand->mNOME_COMPLETO . '"';
            cHISTORICO_OS::registre($this, $msg, cSESSAO::$mcd_usuario, $pNU_CANDIDATO);
        }

        $processo = $this->AdicionaProcessoNoCandidato($cand);

        if ($this->get_AfetaEmpresaProjeto($processo)) {
            $msg = 'Inclusão na OS ' . $this->mNU_SOLICITACAO . ' alterou';
            if ($cand->mNU_EMPRESA != $this->mNU_EMPRESA) {
                $msg .= ' empresa para ' . $this->mNU_EMPRESA;
            }
            if ($cand->mNU_EMBARCACAO_PROJETO != $this->get_NU_EMBARCACAO_PROJETO_Real()) {
                $msg .= ' embarcação para ' . $this->get_NU_EMBARCACAO_PROJETO_Real();
            }
            $cand->mNU_EMPRESA = $this->mNU_EMPRESA;
            $cand->mNU_EMBARCACAO_PROJETO = $this->get_NU_EMBARCACAO_PROJETO_Real();
            $cand->AtualizeEmpresaEmbarcacaoProjeto(cSESSAO::$mcd_usuario, $msg);
        }
    }

    public function AdicionaProcessoNoCandidato($cand) {
        // Atualiza o processo correto pelo servico da OS
        $processo = cprocesso::FabricaProcesso($this->mID_TIPO_ACOMPANHAMENTO);

        if (is_object($processo)) {
            if ($this->mID_TIPO_ACOMPANHAMENTO_ANT != '' && $this->mID_TIPO_ACOMPANHAMENTO_ANT != $this->mID_TIPO_ACOMPANHAMENTO && intval($this->mID_SOLICITA_VISTO) > 5688) {
                $cand->ExcluaProcesso($this->mID_SOLICITA_VISTO, $this->mID_TIPO_ACOMPANHAMENTO_ANT);
            }
            $processo->SalvarDadosOs($this, $cand);
        }
        return $processo;
    }

    public function AdicionaProcessoATodosCandidatos() {
        $this->RecuperarCandidatos();

        $cands = explode(",", trim($this->mCandidatos));
        if (count($cands) > 0){
            foreach ($cands as $nu_candidato) {
                if (intval($nu_candidato) > 0 ){
                    $cand = new cCANDIDATO();
                    $cand->Recuperar($nu_candidato);
                    $processo = $this->AdicionaProcessoNoCandidato($cand);
                }
            }
        }
    }

    public function get_nu_empresa_requerente() {
        $ret = '';
        if ($this->mnu_empresa_requerente != '') {
            $ret = $this->mnu_empresa_requerente;
        } else {
            $ret = $this->mNU_EMPRESA;
        }
        return $ret;
    }

    /**
     * Retorna a embarcacao/projeto real caso informada
     * @return int Chave da embarcação real
     */
    public function get_NU_EMBARCACAO_PROJETO_Real() {
        $ret;
        if ($this->mNU_EMBARCACAO_PROJETO_COBRANCA != '') {
            $ret = $this->mNU_EMBARCACAO_PROJETO_COBRANCA;
        } else {
            $ret = $this->mNU_EMBARCACAO_PROJETO;
        }
        return $ret;
    }

    /**
     * Indica se a OS afeta a empresa e a embarcacao do candidato
     * @return boolean true/false
     */
    public function get_AfetaEmpresaProjeto($pprocesso) {
        $ret = false;
        if ($this->mID_TIPO_ACOMPANHAMENTO == 1) {
            if ($this->nova || $pprocesso->mfl_visto_atual) {
                $ret = true;
            }
        } elseif ($this->mID_TIPO_ACOMPANHAMENTO == 3 || $this->mID_TIPO_ACOMPANHAMENTO == 4 || $this->mID_TIPO_ACOMPANHAMENTO == 5) {
            $ret = true;
        }
        return $ret;
    }

    public function ExcluirCandidato($pNU_CANDIDATO) {
        // $sql = "select ifnull(count(*), 0) from AUTORIZACAO_CANDIDATO where id_solicita_visto = ".$this->mID_SOLICITA_VISTO;
        // $res = cAMBIENTE::ConectaQuery($sql, __CLASS__."->".__FUNCTION__);
        // $rs = $res->fetch(PDO::FETCH_BOTH);
        // if($rs[0] < 2){
        // 	return "NÃ£o Ã© possÃ­vel excluir o Ãºltimo candidato da OS. Adicione outro primeiro ou entÃ£o exclua a OS.";
        // }
        // else{
        $sql = "delete from AUTORIZACAO_CANDIDATO where id_solicita_visto = " . $this->mID_SOLICITA_VISTO . " and NU_CANDIDATO = " . $pNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . "->" . __FUNCTION__);
        $this->Recuperar();
        $cand = new cCANDIDATO();
        $cand->mNU_CANDIDATO = $pNU_CANDIDATO;
        $cand->Recuperar();
        $cand->ExcluaProcesso($this->mID_SOLICITA_VISTO, $this->mID_TIPO_ACOMPANHAMENTO);
        $msg = 'Candidato "' . $cand->mNOME_COMPLETO . '" retirado';
        cHISTORICO_OS::registre($this, $msg, cSESSAO::$mcd_usuario, $pNU_CANDIDATO);
        return "Candidato removido com sucesso";
        // }
    }

    public function InvertaNaoConformidade() {
        if ($this->mfl_nao_conformidade) {
            $fl_nao_conformidade = 0;
        } else {
            $fl_nao_conformidade = 1;
        }
        $sql = "update solicita_visto
				set fl_nao_conformidade = " . $fl_nao_conformidade;
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        $sql .= " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mfl_nao_conformidade = $fl_nao_conformidade;
    }

    public function Cobre() {
        $sql = "update solicita_visto
				set fl_cobrado = 1";
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        $sql .= " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mfl_cobrado = 1;
    }

    public function AtualizarStatus($pStatus, $cd_usuario = '') {
        if ($cd_usuario == '') {
            $cd_usuario = cSESSAO::$mcd_usuario;
        }
        $sql = "update solicita_visto
				set ID_STATUS_SOL = " . $pStatus;
        $sql .= $this->SqlAuditagem($cd_usuario, __CLASS__, __FUNCTION__) . "
				where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mID_STATUS_SOL = $pStatus;
        if ($pStatus == 6) {
            $this->mReadonly = true;
        }
    }

    public function AtualizarTipoEnvio($pid_tipo_envio, $cd_usuario = '') {
        if ($cd_usuario == '') {
            $cd_usuario = cSESSAO::$mcd_usuario;
        }
        $sql = "update solicita_visto
				set id_tipo_envio = " . cBANCO::ChaveOk($pid_tipo_envio);
        $sql .= $this->SqlAuditagem($cd_usuario, __CLASS__, __FUNCTION__) . "
				where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mid_tipo_envio = $pid_tipo_envio;
    }
    /**
     * @deprecated A associaÃ§Ã£o agora estÃ¡ sendo feita de outra forma.
     */
    public function RelacionarProcessoAutorizacao() {
        // Verifica se o serviÃ§o da OS Ã© diferente de autorizaÃ§Ã£o.
        if ($this->mID_TIPO_ACOMPANHAMENTO == 3 || $this->mID_TIPO_ACOMPANHAMENTO == 4 || $this->mID_TIPO_ACOMPANHAMENTO == 5 || $this->mID_TIPO_ACOMPANHAMENTO == 6) {
            $tabela = '';
            if ($this->mID_TIPO_ACOMPANHAMENTO == 1) {
                $tabela = "processo_mte";
            } elseif ($this->mID_TIPO_ACOMPANHAMENTO == 3) {
                $tabela = "processo_prorrog";
            } elseif ($this->mID_TIPO_ACOMPANHAMENTO == 4) {
                $tabela = "processo_regcie";
            } elseif ($this->mID_TIPO_ACOMPANHAMENTO == 5) {
                $tabela = "processo_emiscie";
            } elseif ($this->mID_TIPO_ACOMPANHAMENTO == 6) {
                $tabela = "processo_cancel";
            }

            // Verifica se o codigo_processo_mte atual Ã© nulo entÃ£o atualiza
            $sql = "select codigo_processo_mte from " . $tabela . " where id_solicita_visto =" . $this->mID_SOLICITA_VISTO;
            $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            if ($linha = mysql_fetch_array($res)) {
                //gravaLog("Entrou","","debug","RelacionarProcessoAutorizacao");
                if ($linha['codigo_processo_mte'] == '') {
                    // Obtem o processo atual do candidato
                    $sql = "select codigo from vCANDIDATO_MTE where NU_CANDIDATO = " . $this->mCandidatos;
                    $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
                    if ($processoAtual = mysql_fetch_array($res)) {
                        if ($processoAtual['codigo'] != '') {
                            // Atualiza o processo
                            $sql = "update " . $tabela . " set codigo_processo_mte = " . $processoAtual['codigo'] . " where id_solicita_visto =" . $this->mID_SOLICITA_VISTO;
                            sql::query($sql);
                        }
                    }
                }
            }
        }
    }

    public function NomeTabelaSatelite() {
        $tabela = '';
        if ($this->mID_TIPO_ACOMPANHAMENTO == 1) {
            $tabela = "processo_mte";
        } elseif ($this->mID_TIPO_ACOMPANHAMENTO == 3) {
            $tabela = "processo_prorrog";
        } elseif ($this->mID_TIPO_ACOMPANHAMENTO == 4) {
            $tabela = "processo_regcie";
        } elseif ($this->mID_TIPO_ACOMPANHAMENTO == 5) {
            $tabela = "processo_emiscie";
        } elseif ($this->mID_TIPO_ACOMPANHAMENTO == 6) {
            $tabela = "processo_cancel";
        }
        return $tabela;
    }

    /**
     * Transfere um processo de um candidato para outro, usando o banco de arquivo morto, e exclui o candidato do arquivo morto.
     * @param int $pNU_CANDIDATO_MORTO Candidato origem
     * @param int $pNU_CANDIDATO_PAI Candidato destino
     */
    public function MigrarServico($pNU_CANDIDATO_MORTO, $pNU_CANDIDATO_PAI) {
        if ($this->mID_SOLICITA_VISTO == '') {
            throw new ErrorException("Processo inválido (sem id) para migração");
        } else {
            if ($pNU_CANDIDATO_MORTO == '' || $pNU_CANDIDATO_PAI == '') {
                throw new ErrorException("Processo inválido (id candidato inválido de " . $$pNU_CANDIDATO_MORTO . " para " . $pNU_CANDIDATO_PAI . " INICIANDO...", "", "", "", "");
            } else {
                $this->MigrarServicoProcesso($pNU_CANDIDATO_MORTO, $pNU_CANDIDATO_PAI, 'processo_mte');
                $this->MigrarServicoProcesso($pNU_CANDIDATO_MORTO, $pNU_CANDIDATO_PAI, 'processo_emiscie');
                $this->MigrarServicoProcesso($pNU_CANDIDATO_MORTO, $pNU_CANDIDATO_PAI, 'processo_regcie');
                $this->MigrarServicoProcesso($pNU_CANDIDATO_MORTO, $pNU_CANDIDATO_PAI, 'processo_prorrog');
                $this->MigrarServicoProcesso($pNU_CANDIDATO_MORTO, $pNU_CANDIDATO_PAI, 'processo_cancel');
                $sql = "update AUTORIZACAO_CANDIDATO
					   set NU_CANDIDATO = " . $pNU_CANDIDATO_PAI . "
						 where id_solicita_visto = " . $this->mID_SOLICITA_VISTO . "
						   and NU_CANDIDATO = " . $pNU_CANDIDATO_MORTO;
                executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
                gravaLog($sql, mysql_error(), "MIGRACAO_OS", "TABELA=" . $tabela . " NU_CANDIDATO_atual=" . $pNU_CANDIDATO_MORTO . " NU_CANDIDATO_novo=" . $pNU_CANDIDATO_PAI . " id_solicita_visto=" . $this->mID_SOLICITA_VISTO . " SUCESSO!!!");
                $sql = "delete from " . DATAWEB_MORTO . ".CANDIDATO where NU_CANDIDATO = " . $pNU_CANDIDATO_MORTO;
                executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            }
        }
    }

    public function MigrarServicoProcesso($pNU_CANDIDATO_atual, $pNU_CANDIDATO_novo, $ptabela) {
        $sql = "update " . $ptabela . "
				   set cd_candidato = " . $pNU_CANDIDATO_novo . "
				 where id_solicita_visto = " . $this->mID_SOLICITA_VISTO . "
				   and cd_candidato = " . $pNU_CANDIDATO_atual;
        gravaLog($sql, mysql_error(), "MIGRACAO_OS", "TABELA=" . $ptabela . " NU_CANDIDATO_atual=" . $pNU_CANDIDATO_atual . " NU_CANDIDATO_novo=" . $pNU_CANDIDATO_novo . " id_solicita_visto=" . $this->mID_SOLICITA_VISTO);

        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function Migre(cCANDIDATO $pcandidato_origem, cCANDIDATO $pcandidato_destino, $pMigrarArquivos, $pMigrarAssociados, $pTabela) {
        $this->Recuperar();
        if (intval($this->mfl_sistema_antigo) == 0) {
            $sql = "update AUTORIZACAO_CANDIDATO ";
            $sql.= "   set NU_CANDIDATO		 = " . $pcandidato_destino->mNU_CANDIDATO;
            $sql.= " where ID_SOLICITA_VISTO = " . $this->mID_SOLICITA_VISTO;
            $sql.= "   and NU_CANDIDATO		 =" . $pcandidato_origem->mNU_CANDIDATO;
            cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);

            if ($this->mde_observacao == '') {
                $br = '';
            } else {
                $br = '<br/>';
            }
            $obs = $br . "(" . date('d/m/y') . ") Candidato " . $pcandidato_origem->mNOME_COMPLETO . " (" . $pcandidato_origem->mNU_CANDIDATO . ") trocado pelo candidato " . $pcandidato_destino->mNOME_COMPLETO . " (" . $pcandidato_destino->mNU_CANDIDATO . ") - por " . cSESSAO::$mnome;
            $sql = "update solicita_visto
					set de_observacao = concat(ifnull(de_observacao, ''), '" . $obs . "') ";
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
					where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
            cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);

            if (intval($pMigrarArquivos) > 0) {
                $sql = "update ARQUIVOS
						set   NU_CANDIDATO		=" . $pcandidato_destino->mNU_CANDIDATO . "
						where ID_SOLICITA_VISTO	=" . $this->mID_SOLICITA_VISTO . "
						and   NU_CANDIDATO		=" . $pcandidato_origem->mNU_CANDIDATO;
                cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            }
            // Migra o processo associado
            $processo = cprocesso::FabricaProcesso($this->mID_TIPO_ACOMPANHAMENTO);
            $processo->RecupereSePelaOs($this->mID_SOLICITA_VISTO, $pcandidato_origem->mNU_CANDIDATO);
            if ($this->mID_TIPO_ACOMPANHAMENTO == 1) {
                $processo->Migre($pcandidato_origem, $pcandidato_destino, $pMigrarAssociados);
            } else {
                $processo->Migre($pcandidato_origem, $pcandidato_destino);
            }
        } else {
            if (intval($pMigrarArquivos) > 0) {
                $sql = "update ARQUIVOS
						set   NU_CANDIDATO		=" . $pcandidato_destino->mNU_CANDIDATO . "
						where ID_SOLICITA_VISTO	=" . $this->mID_SOLICITA_VISTO . "
						and   NU_CANDIDATO		=" . $pcandidato_origem->mNU_CANDIDATO;
                cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            }
            // Migra o processo associado
            if ($pTabela == 'processo_mte') {
                $id_tipo_acompanhamento = 1;
            } elseif ($pTabela == 'processo_prorrog') {
                $id_tipo_acompanhamento = 3;
            } elseif ($pTabela == 'processo_regcie') {
                $id_tipo_acompanhamento = 4;
            } elseif ($pTabela == 'processo_emiscie') {
                $id_tipo_acompanhamento = 5;
            } elseif ($pTabela == 'processo_cancel') {
                $id_tipo_acompanhamento = 6;
            } elseif ($pTabela == 'processo_coleta') {
                $id_tipo_acompanhamento = 8;
            }
            $processo = cprocesso::FabricaProcesso($id_tipo_acompanhamento);
            $processo->RecupereSePelaOs($this->mID_SOLICITA_VISTO, $pcandidato_origem->mNU_CANDIDATO);
            $processo->mid_solicita_visto = '';
            $processo->Salve_id_solicita_visto();
            if ($id_tipo_acompanhamento == 1) {
                $processo->Migre($pcandidato_origem, $pcandidato_destino, $pMigrarAssociados);
            } else {
                $processo->Migre($pcandidato_origem, $pcandidato_destino);
            }
        }
    }

    /**
     * Suspende todos os processos nÃ£o migrados do duplicado.
     */
    public static function SuspendeProcessosNaoMigrados($pNU_CANDIDATO) {
        $sql = "update " . DATAWEB_MORTO . ".AUTORIZACAO_CANDIDATO
				   set FL_IGNORADO = 1
				 where NU_CANDIDATO in (select NU_CANDIDATO FROM " . DATAWEB_MORTO . ".CANDIDATO WHERE NU_CANDIDATO_PAI = " . $pNU_CANDIDATO . ")";
        gravaLog($sql, mysql_error(), "MIGRACAO_OS (SUSPENSO)", "NU_CANDIDATO=" . $pNU_CANDIDATO);

        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public static function RecupereOsProtocolo($pData) {
        /*
         * Gera um recordset com as OS de um dia.
         */
        $sql = "select * from vORDEM_SERVICO_PROTOCOLO where date_format(dt_cadastro,'%d/%m/%Y')='" . $pData . "' order by ID_TIPO_ACOMPANHAMENTO, NO_RAZAO_SOCIAL, nu_solicitacao, NOME_COMPLETO";
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        return $res;
    }

    public static function RecupereOsProtocoloTodos($pDataProtocolo, $pdt_cadastro_ini = '', $pdt_cadastro_fim = '') {
        /*
         * Gera um recordset com as OS de um dia.
         */
        $sql = " select * from vORDEM_SERVICO_PROTOCOLO ";
        $sql .= " where (dt_envio_bsb=" . cBANCO::DataOk($pDataProtocolo) . ')';
        $sql .= " or ( dt_envio_bsb is null  ";
        if ($pdt_cadastro_ini != '') {
            $sql .= " and dt_cadastro >= " . cBANCO::DataOk($pdt_cadastro_ini);
        }
        if ($pdt_cadastro_fim != '') {
            $sql .= " and dt_cadastro <= " . cBANCO::DataOk($pdt_cadastro_fim);
        }
        $sql .= ")";
        $sql .= " order by dt_cadastro desc, ID_TIPO_ACOMPANHAMENTO, NO_RAZAO_SOCIAL, nu_solicitacao, NOME_COMPLETO";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        return $res;
    }

    public static function RecupereOsProtocoloProtocolo() {
        /*
         * Gera um recordset com as OS de um dia.
         */
        $sql = " select * from vORDEM_SERVICO_PROTOCOLO ";
        $sql .= " where dt_requerimento is null";
        $sql .= "   and dt_envio_bsb is not null";
        $sql .= " order by dt_envio_bsb desc, ID_TIPO_ACOMPANHAMENTO, NO_RAZAO_SOCIAL, nu_solicitacao, NOME_COMPLETO";
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        return $res;
    }

    public static function RecupereOsProtocoloEnviados($pData) {
        /*
         * Gera um recordset com as OS de um dia.
         */
        $sql = " select * from vORDEM_SERVICO_PROTOCOLO ";
        $sql .= " where dt_envio_bsb =" . cBANCO::DataOk($pData);
        $sql .= " order by  dt_cadastro desc, ID_TIPO_ACOMPANHAMENTO, NO_RAZAO_SOCIAL, nu_solicitacao, NOME_COMPLETO";
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        return $res;
    }

    public static function RecupereOsProtocoloColeta($pdt_solicitacao_ini,$pdt_solicitacao_fim, $pNU_EMPRESA, $pNU_EMBARCACAO_PROJETO) {
        /*
         * Gera um recordset com as OS de um dia.
         */
        $sql = " select * from vORDEM_SERVICO_PROTOCOLO_COLETA ";
        $sql .= " where 1=1 ";
        if ($pdt_solicitacao_ini != ''){
            $sql.= " and dt_solicitacao >= '".$pdt_solicitacao_ini."'";
        }
        if ($pdt_solicitacao_fim != ''){
            $sql.= " and dt_solicitacao <= '".$pdt_solicitacao_fim."'";
        }

        if (intval($pNU_EMPRESA) > 0) {
            $sql .= " and nu_empresa = " . $pNU_EMPRESA;
        }
        if (intval($pNU_EMBARCACAO_PROJETO) > 0) {
            $sql .= " and NU_EMBARCACAO_PROJETO = " . $pNU_EMBARCACAO_PROJETO;
        }
        $sql .= " order by ID_TIPO_ACOMPANHAMENTO, NO_RAZAO_SOCIAL, nu_solicitacao, NOME_COMPLETO";
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        return $res;
    }

    public function Abrir() {
        $sql = "update solicita_visto ";
        $sql .= " set ID_STATUS_SOL = 4";
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        $sql .= " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mID_STATUS_SOL = self::STATUS_INICIAL;
        return 'OS aberta com sucesso.';
    }

    public function AbrirQualidade() {
        $sql = "update solicita_visto ";
        $sql .= " set fl_liberacao_fechada = 0";
        $sql .= "   , fl_qualidade_fechada = 0 ";
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        $sql .= " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mfl_liberacao_fechada = 0;
        $this->mfl_qualidade_fechada = 0;
        return 'Aba de qualidade da OS aberta com sucesso.';
    }

    public function Fechar() {
        $this->mReadonly = true;
        $sql = "update solicita_visto ";
        $sql .= " set ID_STATUS_SOL = 6";
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        $sql .= " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mID_STATUS_SOL = 6;
    }

    public function SalvarObservacao() {
        $sql = "update solicita_visto ";
        $sql .= " set de_observacao =" . cBANCO::StringOk($this->mde_observacao);
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        $sql .= " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function SalvarObservacaoBsb($pcd_usuario) {
        $sql = "update solicita_visto ";
        $sql .= " set de_observacao_bsb =" . cBANCO::StringOk($this->mde_observacao_bsb);
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        $sql .= " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function ProtocoloRegistro_Registro($pData, $pNU_EMPRESA, $pCO_REPARTICAO_CONSULAR) {
        $sql = "select * ";
        $sql.= "  from solicita_visto	sv ";
        $sql.= "     , SERVICO			S ";
        $sql.= " where date_format(dt_cadastro,'%d/%m/%Y') = '" . $pData . "' ";
        $sql.= "   and sv.NU_SERVICO = S.NU_SERVICO";
        $sql.= "   and sv.NU_EMPRESA = " . $pNU_EMPRESA;
        $sql.= " order by NOME_COMPLETO";
        return conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function IncluirNoProtocoloBSB($pData) {
        $sql = "update processo_mte set ";
    }

    /**
     * Retorna um recordset de usuarios
     * @param int $pPag
     * @return PDOstatement
     */
    public function ListeCobranca($pPag = '', $pFiltros = '', $pOrdenacao = '') {
        $sql = '';
        $sql .= " select s.*, E.NO_RAZAO_SOCIAL, EP.NO_EMBARCACAO_PROJETO, SV.NO_SERVICO_RESUMIDO, stco_tx_nome ";
        $sql .= "   , EC.NO_RAZAO_SOCIAL NO_RAZAO_SOCIAL_REQUERENTE";
        $sql .= "   , EPC.NO_EMBARCACAO_PROJETO NO_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "   , E.tbpc_id";
        $sql .= "   , p.prec_vl_conceito";
        $sql .= "   , p.prec_vl_pacote";
        $sql .= "   , tpc.tppr_tx_nome";
        $sql .= "   , SS.no_status_sol";
        $sql .= "   , rc.resc_dt_faturamento";
        $sql .= "   from solicita_visto s ";
        $sql .= " left join STATUS_SOLICITACAO    SS on SS.ID_STATUS_SOL = s.ID_STATUS_SOL";
        $sql .= " left join EMPRESA                E on E.NU_EMPRESA = s.nu_empresa";
        $sql .= " left join EMBARCACAO_PROJETO    EP on EP.NU_EMPRESA = s.nu_empresa and EP.NU_EMBARCACAO_PROJETO = s.NU_EMBARCACAO_PROJETO";
        $sql .= " left join EMPRESA               EC on EC.NU_EMPRESA = s.nu_empresa_requerente";
        $sql .= " left join EMBARCACAO_PROJETO   EPC on EPC.NU_EMPRESA = s.nu_empresa_requerente and EPC.NU_EMBARCACAO_PROJETO = s.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= " left join SERVICO               SV on SV.NU_SERVICO = s.NU_SERVICO";
        $sql .= " left join status_cobranca_os   sco on sco.stco_id = s.stco_id";
        $sql .= " left join tabela_precos         tp on tp.tbpc_id = E.tbpc_id";
        $sql .= " left join preco		           p on p.tbpc_id = tp.tbpc_id and p.NU_SERVICO = s.NU_SERVICO";
        $sql .= " left join tipo_preco		     tpc on tpc.tppr_id = s.tppr_id";
        $sql .= " left join resumo_cobranca		  rc on rc.resc_id = s.resc_id";
        $sql .= "  where 1=1";
        $filtros = cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql.= $filtros;
        if ($pOrdenacao != '') {
            if ($pOrdenacao != 'NOME_COMPLETO'){
                $sql .= " order by " . $pOrdenacao;
            }
        }
        print "<!-- ".$sql."-->";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        return $res;
    }

    /**
     * Retorna um recordset de usuarios
     * @param int $pPag
     * @return PDOstatement
     */
    public function ListeLiberadasParaFatPorCC($pPag = '', $pFiltros = '', $pOrdenacao = '') {
        $sql = '';
        $sql .= " select s.NU_EMPRESA_COBRANCA, s.NU_EMBARCACAO_PROJETO_COBRANCA, E.NO_RAZAO_SOCIAL, EP.NO_EMBARCACAO_PROJETO, count(*) qtd, sum(soli_vl_cobrado) soli_vl_cobrado";
        $sql .= "   from solicita_visto s ";
        $sql .= "   join EMPRESA                E on E.NU_EMPRESA = s.NU_EMPRESA_COBRANCA";
        $sql .= "   join EMBARCACAO_PROJETO    EP on EP.NU_EMPRESA = s.NU_EMPRESA_COBRANCA and EP.NU_EMBARCACAO_PROJETO = s.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "  where stco_id = 2 ";
        $filtros = cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql.= $filtros;
        $sql .= " group by s.NU_EMPRESA_COBRANCA, s.NU_EMBARCACAO_PROJETO_COBRANCA,E.NO_RAZAO_SOCIAL, EP.NO_EMBARCACAO_PROJETO";
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        if ($res = conectaQuery($sql, __CLASS__)) {
            return $res;
        }
    }

    public function ListeLiberadasParaFatPorSolicitante($pPag = '', $pFiltros = '', $pOrdenacao = '') {
        $offset = 40;
        $sql = '';
        $sql .= " select coalesce(s.nu_empresa_requerente, s.nu_empresa) nu_empresa_requerente";
        $sql .= "	, s.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "   , s.id_solicita_visto";
        $sql .= "   , case s.tppr_id when 1 then 0 when 2 then pr.prec_vl_conceito when 3 then pr.prec_vl_pacote else 0 end soli_vl_cobrado_tabela";
//		$sql .= "   , coalesce(s.soli_vl_cobrado, (case s.tppr_id when 1 then 0 when 2 then pr.prec_vl_conceito when 3 then pr.prec_vl_pacote else 0 end))  soli_vl_cobrado";
        $sql .= "   , case s.tppr_id when 1 then coalesce(s.soli_vl_cobrado, 0) when 2 then coalesce(s.soli_vl_cobrado, pr.prec_vl_conceito) when 3 then coalesce(s.soli_vl_cobrado, pr.prec_vl_pacote) else coalesce(s.soli_vl_cobrado, 0) end  soli_vl_cobrado";
        $sql .= "   , s.nu_solicitacao";
        $sql .= "   , s.tppr_id";
        $sql .= "   , s.soli_tx_solicitante_cobranca";
        $sql .= "   , s.no_solicitador";
        $sql .= "   , SV.NO_SERVICO_RESUMIDO";
        $sql .= "   , E.NO_RAZAO_SOCIAL NO_RAZAO_SOCIAL_REQUERENTE";
        $sql .= "   , EP.NO_EMBARCACAO_PROJETO NO_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "   from solicita_visto s ";
        $sql .= "   left join EMPRESA             E on E.NU_EMPRESA = s.nu_empresa_requerente";
        $sql .= "   left join tabela_precos       tp on tp.tbpc_id = E.tbpc_id";
        $sql .= "   left join EMBARCACAO_PROJETO  EP on EP.NU_EMPRESA = s.nu_empresa_requerente and EP.NU_EMBARCACAO_PROJETO = s.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "   left join SERVICO             SV on SV.NU_SERVICO = s.NU_SERVICO";
        $sql .= "   left join preco               pr on pr.tbpc_id = E.tbpc_id and pr.nu_servico = s.nu_servico";
        $sql .= " where stco_id = 2 and resc_id is null";
        $filtros = cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql.= $filtros;
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        if ($res = cAMBIENTE::ConectaQuery($sql, __CLASS__)) {
            return $res;
        }
    }

    public function CursorFaturadasLegado($pFiltros, $pOrdenacao, $pPag = 1, $pOffset = 30) {
        $sql = $this->QueryFaturadasLegado($pFiltros, $pOrdenacao, $pPag, $pOffset, false);
        $cursor = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $cursor;
    }

    public function QtdFaturadasLegado($pFiltros, $pOrdenacao) {
        $sql = $this->QueryFaturadasLegado($pFiltros, $pOrdenacao, null, null, true);
        $cursor = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $cursor->fetch(PDO::FETCH_BOTH);
        return $rs[0];
    }

    public function QueryFaturadasLegado($pFiltros, $pOrdenacao, $pPag = 1, $pOffset = 30, $pCount = false) {
        if ($pCount) {
            $sql .= " select count(*) ";
        } else {
            $sql .= " select distinct coalesce(s.nu_empresa_requerente, s.nu_empresa) nu_empresa_requerente";
            $sql .= "	, s.NU_EMBARCACAO_PROJETO_COBRANCA";
            $sql .= "   , s.id_solicita_visto";
            $sql .= "   , case s.tppr_id when 1 then 0 when 2 then pr.prec_vl_conceito when 3 then pr.prec_vl_pacote else 0 end soli_vl_cobrado_tabela";
            //		$sql .= "   , coalesce(s.soli_vl_cobrado, (case s.tppr_id when 1 then 0 when 2 then pr.prec_vl_conceito when 3 then pr.prec_vl_pacote else 0 end))  soli_vl_cobrado";
            $sql .= "   , case s.tppr_id when 1 then coalesce(s.soli_vl_cobrado, 0) when 2 then coalesce(s.soli_vl_cobrado, pr.prec_vl_conceito) when 3 then coalesce(s.soli_vl_cobrado, pr.prec_vl_pacote) else coalesce(s.soli_vl_cobrado, 0) end  soli_vl_cobrado";
            $sql .= "   , s.nu_solicitacao";
            $sql .= "   , s.tppr_id";
            $sql .= "   , s.soli_tx_solicitante_cobranca";
            $sql .= "   , s.no_solicitador";
            $sql .= "   , SV.NO_SERVICO_RESUMIDO";
            $sql .= "   , E.NO_RAZAO_SOCIAL NO_RAZAO_SOCIAL_REQUERENTE";
            $sql .= "   , EP.NO_EMBARCACAO_PROJETO NO_EMBARCACAO_PROJETO_COBRANCA";
            $sql .= "   , resc_nu_numero, resc_nu_numero_mv, s.resc_id";
            $sql .= "   , tppr_tx_nome";
        }
        $sql .= "   from solicita_visto s ";
        $pFiltros['nome_completo']->mWhere = '1=1';
        if ($pFiltros['nome_completo']->mValor != '') {
            $sql .= " join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto";
            $sql .= " join candidato c on c.nu_candidato = ac.nu_candidato and c.nome_completo like '%" . $pFiltros['nome_completo']->mValor . "%'";
        }
        $sql .= "   left join resumo_cobranca            rc on rc.resc_id = s.resc_id";
        $sql .= "   left join EMPRESA                E on E.NU_EMPRESA = coalesce(s.nu_empresa_requerente, s.NU_EMPRESA)";
        $sql .= "   left join tabela_precos         tp on tp.tbpc_id = E.tbpc_id";
        $sql .= "   left join EMBARCACAO_PROJETO    EP on EP.NU_EMPRESA = s.nu_empresa_requerente and EP.NU_EMBARCACAO_PROJETO = s.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= "   left join SERVICO		   SV on SV.NU_SERVICO = s.NU_SERVICO";
        $sql .= "   left join preco					pr on pr.tbpc_id = E.tbpc_id and pr.nu_servico = s.nu_servico";
        $sql .= "   left join tipo_preco		   tpr on tpr.tppr_id = s.tppr_id";
        $sql .= "  where 1=1";
        $filtros = cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql.= $filtros;
        if ($pOrdenacao != '') {
            if (strtok($pOrdenacao) == 'nu_solicitacao') {
                $pOrdenacao = 's.' . $pOrdenacao;
            }
            $sql .= " order by " . $pOrdenacao;
        }
        if (!$pCount) {
            $sql .= " LIMIT " . (($pPag - 1) * $pOffset) . " , " . $pOffset;
        }
        return $sql;
    }

    public function ObtenhaNomesCandidatos() {
        $cand = array();
        $sql = "select AC.NU_CANDIDATO, NOME_COMPLETO from CANDIDATO C, AUTORIZACAO_CANDIDATO AC where C.NU_CANDIDATO = AC.NU_CANDIDATO and AC.ID_SOLICITA_VISTO = " . $this->mID_SOLICITA_VISTO;
        $res = cAMBIENTE::$db_pdo->query($sql);
        while ($row = $res->fetch(PDO::FETCH_BOTH)) {
            $cand[$row['NU_CANDIDATO']] = $row['NOME_COMPLETO'];
        }
        return $cand;
    }

    public function SalvarCobranca($justificativa) {
        // Verifica se as informações relativas a preço foram alteradas sem justificativa.
        $os = new cORDEMSERVICO();
        $os->mID_SOLICITA_VISTO = $this->mID_SOLICITA_VISTO;
        $os->Recuperar();
        $os->RecuperarSolicitaVistoCobranca();
        $msg = '';
        $alteracaoCritica = false;

        if ($os->mtppr_id != $this->mtppr_id) {
            $msg .= "<br/>- Tipo de preço alterado de " . cTIPO_PRECO::descricao($os->mtppr_id) . " para " . cTIPO_PRECO::descricao($this->mtppr_id);
        }

        $valor = cBANCO::ValorOk($this->msoli_vl_cobrado) * 1;
        if ($os->msoli_vl_cobrado != $valor) {
            $msg .= "<br/>- Valor cobrado alterado de " . number_format($os->msoli_vl_cobrado, 2, ",", ".") . " para " . $this->msoli_vl_cobrado;
        }

        if ($msg != '' && trim($justificativa) == '') {
            throw new cERRO_CONSISTENCIA('Para alterar essas informações a justificativa é obrigatória:' . $msg);
            exit;
        }

        $sql = " update solicita_visto set";
        $sql .= "   nu_empresa_requerente			= " . cBANCO::ChaveOk($this->mnu_empresa_requerente);
        $sql .= "  , NU_EMBARCACAO_PROJETO_COBRANCA	= " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO_COBRANCA);
        $sql .= "  , soli_vl_cobrado				= " . cBANCO::ValorOk($this->msoli_vl_cobrado);
        $sql .= "  , soli_tx_solicitante_cobranca	= " . cBANCO::StringOk($this->msoli_tx_solicitante_cobranca);
        $sql .= "  , soli_tx_codigo_servico			= " . cBANCO::StringOk($this->msoli_tx_codigo_servico);
        $sql .= "  , soli_tx_descricao_servico		= " . cBANCO::StringOk($this->msoli_tx_descricao_servico);
        $sql .= "  , soli_tx_obs_cobranca			= " . cBANCO::StringOk($this->msoli_tx_obs_cobranca);
        $sql .= "  , tbpc_id						= " . cBANCO::ChaveOk($this->mtbpc_id);
        $sql .= "  , tppr_id						= " . cBANCO::ChaveOk($this->mtppr_id);
        $sql .= "  , soli_tx_justificativa			= " . cBANCO::StringOk($justificativa);
        if ($this->mtppr_id == '1'){
            $this->mstco_id = cstatus_cobranca_os::NAO_GERA_COBRANCA;
            $sql .= " , stco_id = ".$this->mstco_id;
        }
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        $sql .= " where id_solicita_visto			= " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function SalvarELiberarCobranca() {
        $sql = " update solicita_visto set";
        $sql .= "   nu_empresa_requerente			= " . cBANCO::ChaveOk($this->mnu_empresa_requerente);
        $sql .= "  , NU_EMBARCACAO_PROJETO_COBRANCA = " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO_COBRANCA);
        $sql .= "  , soli_vl_cobrado				= " . cBANCO::ValorOk($this->msoli_vl_cobrado);
        $sql .= "  , soli_tx_obs_cobranca			= " . cBANCO::StringOk($this->msoli_tx_obs_cobranca);
        $sql .= "  , soli_tx_solicitante_cobranca	= " . cBANCO::StringOk($this->msoli_tx_solicitante_cobranca);
        $sql .= "  , soli_tx_codigo_servico			= " . cBANCO::StringOk($this->msoli_tx_codigo_servico);
        $sql .= "  , soli_tx_descricao_servico		= " . cBANCO::StringOk($this->msoli_tx_descricao_servico);
        $sql .= "  , tbpc_id						= " . cBANCO::ChaveOk($this->mtbpc_id);
        $sql .= "  , tppr_id						= " . cBANCO::ChaveOk($this->mtppr_id);
        $sql .= "  , soli_dt_liberacao_cobranca		= now()";
        $sql .= "  , cd_usuario_liberacao_cobranca	= " . cSESSAO::$mcd_usuario;
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        if ($this->mstco_id == 1) {
            $sql .= "  , stco_id		= 2";
        }
        $sql .= " where id_solicita_visto   = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function Desfaturar() {
        $sql = " update solicita_visto set";
        $sql .= "    soli_vl_cobrado				= " . cBANCO::ValorOk($this->msoli_vl_cobrado);
        $sql .= "  , soli_tx_obs_cobranca = coalesce(concat(soli_tx_obs_cobranca,'<br/>'),'')," . cBANCO::StringOk('Faturamento cancelado e liberado novamente para cobrança em ' . date('d/m/y')) . ")";
        $sql .= "  , soli_dt_liberacao_cobranca		= now()";
        $sql .= "  , cd_usuario_liberacao_cobranca	= " . cSESSAO::$mcd_usuario;
        $sql .= "  , resc_id		= null";
        $sql .= "  , stco_id		= 2";  // Liberado para cobranca
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ );
        $sql .= " where id_solicita_visto   = " . $this->mID_SOLICITA_VISTO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function ListeArquivos() {
        $sql = "SELECT A.NU_SEQUENCIAL, A.NO_ARQ_ORIGINAL, A.NO_ARQUIVO, A.NU_EMPRESA, TA.NO_TIPO_ARQUIVO, TA.CO_TIPO_ARQUIVO, A.NU_CANDIDATO, sv.nu_solicitacao";
        $sql.= "  FROM ARQUIVOS A ";
        $sql.= "  left join TIPO_ARQUIVO TA on TA.ID_TIPO_ARQUIVO = A.TP_ARQUIVO";
        $sql.= "  join solicita_visto sv on sv.id_solicita_visto = A.ID_SOLICITA_VISTO";
        $sql.= " where A.ID_SOLICITA_VISTO = " . $this->mID_SOLICITA_VISTO;
        $sql.= " ORDER BY NO_ARQ_ORIGINAL asc ";
        if ($res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__)) {
            return $res;
        }
    }

    public function ExcluaArquivo($pNU_SEQUENCIAL) {
        $sql = "delete from ARQUIVOS where NU_SEQUENCIAL = " . $pNU_SEQUENCIAL;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function AtualizarCandidatoDeArquivo($pNU_SEQUENCIAL, $pNU_CANDIDATO) {
        $sql = "update ARQUIVOS set NU_CANDIDATO = " . cBANCO::ChaveOk($pNU_CANDIDATO) . " where NU_SEQUENCIAL = " . $pNU_SEQUENCIAL;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function CursorDeCandidatos() {
        $sql = "select AC.NU_CANDIDATO, NOME_COMPLETO from CANDIDATO C, AUTORIZACAO_CANDIDATO AC where C.NU_CANDIDATO = AC.NU_CANDIDATO and AC.ID_SOLICITA_VISTO = " . $this->mID_SOLICITA_VISTO;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            return $res;
        } else {
            throw new exception('Não foi possível obter os candidatos');
        }
    }

    /**
     *
     * @param type $pDT_PRAZO_ESTADA_SOLICITADO
     */
    public function AtualizePrazoSolicitadoPeloProcesso($pDT_PRAZO_ESTADA_SOLICITADO) {
        if ($this->mDT_PRAZO_ESTADA_SOLICITADO != $pDT_PRAZO_ESTADA_SOLICITADO) {
            $sql = "update AUTORIZACAO_CANDIDATO
					   set DT_PRAZO_ESTADA_SOLICITADO = " . cBANCO::StringOk($pDT_PRAZO_ESTADA_SOLICITADO) . "
					 where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            if ($this->mde_observacao == '') {
                $nl = "";
            } else {
                $nl = "\n";
            }
            $this->mde_observacao .= $nl . "(Prazo pretendido alterado para ficar igual ao processo. De " . $this->mDT_PRAZO_ESTADA_SOLICITADO . " para " . $pDT_PRAZO_ESTADA_SOLICITADO . " em " . date('d/m/y') . " por " . cSESSAO::$mnome . ").";
            $sql = "update solicita_visto
					   set de_observacao = " . cBANCO::StringOk($this->mde_observacao);
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
					 where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        }
    }

    public function CursorDescricoesAtividade($pFiltros = '', $pOrdenacao = '') {
        $sql = "select NO_RAZAO_SOCIAL, date_format(sv.dt_solicitacao, '%d/%m/%y') dt_solicitacao, sv.NU_SOLICITACAO, NO_SERVICO_RESUMIDO, NO_FUNCAO, TE_DESCRICAO_ATIVIDADES
				from solicita_visto sv
				join AUTORIZACAO_CANDIDATO AC on AC.id_solicita_visto = sv.id_solicita_visto
				left join EMPRESA E on E.NU_EMPRESA = sv.NU_EMPRESA
				left join FUNCAO_CARGO FC on FC.CO_FUNCAO = AC.CO_FUNCAO_CANDIDATO
				left join SERVICO S on S.NU_SERVICO = sv.NU_SERVICO
				where TE_DESCRICAO_ATIVIDADES IS NOT NULL
				";
        if (is_array($pFiltros)) {
            $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        }

        $sql .= " order by AC.id_solicita_visto desc limit 100";
        if ($res = conectaQuery($sql, __CLASS__ . '.' . __FUNCTION__)) {
            return $res;
        } else {
            throw new exception('Não foi possível obter as descrições de atividades');
        }
    }

    public function CursorOSAbertasPorMes() {
        $sql = "select month(dt_cadastro), year(dt_cadastroselect concat(month(dt_cadastro), '/', year(dt_cadastro)), count(*)
				from solicita_visto
				group by concat(month(dt_cadastro), '/', year(dt_cadastro))
				order by year(dt_cadastro),month(dt_cadastro)";

        $cursor = cAMBIENTE::$db_pdo->query($sql);
        return $cursor;
    }

    /**
     * Registra a data de envio para brasilia de uma lista de OS
     * @param type $pListaId
     * @param type $pdt_envio_bsb
     */
    public static function RegistrarEnvioProcessoBSB_emlote($pListaId, $pdt_envio_bsb) {
        $id = explode(",", $pListaId);
        foreach ($id as $id_solicita_visto) {
            $os = new cORDEMSERVICO();
            $os->Recuperar($id_solicita_visto);
            $proc = cprocesso::FabricaProcessoDaOs($os);
            $proc->RegistraDtEnvioBSB($id_solicita_visto, $pdt_envio_bsb);
        }
        // Retira do protocolo as os que nao foram informadas
        $sql = "select id_solicita_visto from vORDEM_SERVICO_PROTOCOLO where dt_envio_bsb = " . cBANCO::DataOk($pdt_envio_bsb) . " and id_solicita_visto not in (" . $pListaId . ")";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        while ($rs = $res->fetch(PDO::FETCH_BOTH)) {
            $os = new cORDEMSERVICO();
            $os->Recuperar($rs['id_solicita_visto']);
            $proc = cprocesso::FabricaProcessoDaOs($os);
            $proc->ZeraDtEnvioBSB($rs['id_solicita_visto']);
        }
    }

    public static function UltimoProtocoloBsb() {
        $sql = "select date_format(max(dt_envio_bsb), '%d/%m/%Y') dt_envio_bsb from vORDEM_SERVICO_PROTOCOLO";
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $row = mysql_fetch_array($res);
        return $row['dt_envio_bsb'];
    }

    public static function LibereParaCobrancaEmLote($pcd_usuario, $plote) {
        $sql = "update solicita_visto
				   set stco_id = 2
				     , soli_dt_liberacao_cobranca = now()
					 , cd_usuario_liberacao_cobranca = " . cBANCO::ChaveOk($pcd_usuario);
        $sql .= self::SqlAuditagemS(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
				 where id_solicita_visto in (" . $plote . ")
				   ";
        cAMBIENTE::$db_pdo->exec($sql);
        // Atualiza a empresa requerente caso nao tenha sido preenchido
        $sql = "update solicita_visto
				   set nu_empresa_requerente = nu_empresa ";
        $sql .= self::SqlAuditagemS(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
				 where id_solicita_visto in (" . $plote . ")
				   and nu_empresa_requerente is null
				";
        cAMBIENTE::$db_pdo->exec($sql);
        // Atualiza o projeto embarcacao de cobranca caso nao tenha sido preenchido
        $sql = "update solicita_visto
				   set nu_embarcacao_projeto_cobranca = nu_embarcacao_projeto ";
        $sql .= self::SqlAuditagemS(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
				 where id_solicita_visto in (" . $plote . ")
				   and nu_embarcacao_projeto_cobranca is null
				";
        cAMBIENTE::$db_pdo->exec($sql);
    }

    protected static function AtualizaTipoCobrancaEmLote($ptppr_id, $pcd_usuario, $plote) {
        $sql = "update solicita_visto
				   set tppr_id = " . $ptppr_id;
        $sql .= self::SqlAuditagemS($pcd_usuario, __CLASS__, __FUNCTION__ ) . "
				 where id_solicita_visto in (" . $plote . ")
				   ";
        cAMBIENTE::$db_pdo->exec($sql);
    }

    protected static function AtualizaStatusCobrancaEmLote($ptppr_id, $pcd_usuario, $plote) {
        $sql = "update solicita_visto
				   set stco_id = " . $ptppr_id;
        $sql .= self::SqlAuditagemS($pcd_usuario, __CLASS__, __FUNCTION__ ) . "
				 where id_solicita_visto in (" . $plote . ")
				   ";
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function CobrancaConceitoEmLote($pcd_usuario, $plote) {
        self::AtualizaTipoCobrancaEmLote('2', $pcd_usuario, $plote);
    }

    public static function CobrancaPacoteEmLote($pcd_usuario, $plote) {
        self::AtualizaTipoCobrancaEmLote('3', $pcd_usuario, $plote);
    }

    public static function NaoCobreEmLote($pcd_usuario, $plote) {
        self::AtualizaTipoCobrancaEmLote('1', $pcd_usuario, $plote);
        self::AtualizaStatusCobrancaEmLote(cstatus_cobranca_os::NAO_GERA_COBRANCA, $pcd_usuario, $plote);
    }

    public static function PendentesEmLote($pcd_usuario, $plote) {
        self::AtualizaStatusCobrancaEmLote(cstatus_cobranca_os::PENDENTE, $pcd_usuario, $plote);
    }

    public function LibereParaCobranca($pcd_usuario) {
        if ($this->mtppr_id == '1'){
            $this->mstco_id = cstatus_cobranca_os::NAO_GERA_COBRANCA;
        } else {
            $this->mstco_id = cstatus_cobranca_os::LIBERADA_PARA_COBRANCA;
        }
        $sql = "update solicita_visto
                   set stco_id = ".$this->mstco_id."
                    , soli_dt_liberacao_cobranca = now()
                    , cd_usuario_liberacao_cobranca = " . cBANCO::ChaveOk($pcd_usuario) . "
                    , soli_tx_obs_cobranca = " . cBANCO::StringOk($this->msoli_tx_obs_cobranca) . "
                    , tppr_id = " . cBANCO::ChaveOk($this->mtppr_id) ."
                    , soli_tx_solicitante_cobranca = coalesce(soli_tx_solicitante_cobranca, no_solicitador)" ;
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
                    where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        cAMBIENTE::$db_pdo->exec($sql);
        $this->AssumeEmpEmbCobrancaDefault($this->mID_SOLICITA_VISTO);
    }

    private function AssumeEmpEmbCobrancaDefault($pid_solicita_visto) {
        $sql = "update solicita_visto
                    set nu_empresa_requerente = nu_empresa ";
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
                    where id_solicita_visto in (" . $pid_solicita_visto . ")
                      and nu_empresa_requerente is null
			";
        cAMBIENTE::$db_pdo->exec($sql);
        $sql = "update solicita_visto
                    set nu_embarcacao_projeto_cobranca = nu_embarcacao_projeto";
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
                    where id_solicita_visto in (" . $pid_solicita_visto . ")
                      and nu_embarcacao_projeto_cobranca is null
			";
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function AtualizeInfoCobranca() {
        $sql = "update solicita_visto
				   set tppr_id = " . cBANCO::ChaveOk($this->mtppr_id) . "
					 , soli_tx_obs_cobranca = " . cBANCO::StringOk($this->msoli_tx_obs_cobranca) ;
            $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__ ) . "
				 where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function JSON_ParaChecklist() {
        $saida = array();
        $this->Recuperar();
        $os = array('nu_solicitacao' => $this->mNU_SOLICITACAO
            , 'dt_solicitacao' => $this->mdt_solicitacao
            , 'dt_prazo_estada_solicitado' => $this->mDT_PRAZO_ESTADA_SOLICITADO);
        $this->RecuperarCandidatos();
        $candidatos_os = explode(",", $this->mCandidatos);
        $os['qtd_candidatos'] = count($candidatos_os);
        $empresa = array('no_razao_social' => $this->mNO_RAZAO_SOCIAL);
        $os['empresa'] = $empresa;
        $embarcacao_projeto = array('no_embarcacao_projeto' => $this->mNO_EMBARCACAO_PROJETO);
        $os['embarcacao_projeto'] = $embarcacao_projeto;
        $embarcacao_projeto_cobranca = array('no_embarcacao_projeto' => $this->mNO_EMBARCACAO_PROJETO_COBRANCA);
        $os['embarcacao_projeto_cobranca'] = $embarcacao_projeto_cobranca;
        $candidatos = array();
        foreach ($candidatos_os as $candidato) {
            if (isset($cand)) {
                $candidatos[] = $cand;
            }
            $c = new cCANDIDATO();
            $c->Recuperar($candidato);
            $cand = array('nome_completo' => $c->mNOME_COMPLETO);
        }
        $cand['ultimo'] = true;
        $candidatos[] = $cand;
        $os['candidatos'] = $candidatos;
        $saida['os'] = $os;
        return $saida;
    }

    public function CursorAcompanhamento($pFiltros, $pOrdem) {
        $sql = "select v.*, SI_STATUS_SOL
			from vordemservico v
			left join status_solicitacao ss on ss.id_status_sol = v.id_status_sol
			where serv_fl_revisao_analistas = 1

			";
        if (is_array($pFiltros)) {
            $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        }
        $sql .= " order by id_solicita_visto desc , nome_completo";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        return $res;
    }

    public function SqlLog($pClasse, $pMetodo) {
        return "";
    }

    public function SqlAuditagem($pcd_usuario, $controller = '', $metodo = '') {
        $sql = " , soli_dt_alteracao = now()";
        $sql .= ", controller = " . cBANCO::StringOk($controller);
        $sql .= ", metodo= " . cBANCO::StringOk($metodo);
        $sql .= ", cd_usuario_alteracao = " . cBANCO::ChaveOk($pcd_usuario);
        return $sql;
    }

    public static function SqlAuditagemS($pcd_usuario, $controller = '', $metodo = '') {
        $sql = " , soli_dt_alteracao = now()";
        $sql .= ", controller = " . cBANCO::StringOk($controller);
        $sql .= ", metodo= " . cBANCO::StringOk($metodo);
        $sql .= ", cd_usuario_alteracao = " . cBANCO::ChaveOk($pcd_usuario);
        return $sql;
    }

    public function AtualizeAtributo($pAtributo, $pValor, $pTipo, $pChave, $pcd_usuario) {
        parent::AtualizeAtributo($pAtributo, $pValor, $pTipo, $pChave, $pcd_usuario);
    }

    public function CursorOSCompleta($pid_solicita_visto) {
        $sql = "select * from vordemservico where id_solicita_visto = " . $pid_solicita_visto;

        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    public function CursorProrrogacoesPreCadastro($pFiltros, $pOrderBy) {
        $sql = "
			select s.dt_solicitacao
				  ,s.nu_solicitacao
				  ,s.de_observacao
				  ,s.id_solicita_visto
				  ,c.nu_candidato
				  ,c.nome_completo
				  ,u.usua_tx_apelido
				  ,p.*
				  ,up.usua_tx_apelido usua_tx_apelido_pre_cadastro
				  ,st.*
			 from solicita_visto s
			 join processo_prorrog p on p.id_solicita_visto = s.id_solicita_visto
			 join candidato c on c.nu_candidato = p.cd_candidato and coalesce(c.fl_inativo, 0) = 0
			 left join status_solicitacao st on st.id_status_sol = s.id_status_sol
			 left join usuarios u on u.cd_usuario = s.cd_admin_cad
			 left join usuarios up on up.cd_usuario = p.cd_usuario_pre_cadastro
			 left join status_solicitacao ss on ss.id_status_sol = s.id_status_sol
			 where s.dt_solicitacao > '2012-01-01'
			   and coalesce(ss.FL_ENCERRADA, 0) = 0 ";
        // Adicionar filtro para sÃ³ trazer prorrogacoes
        if (is_array($pFiltros)) {
            $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        }
        $sql .= "	 order by s.nu_solicitacao desc";
        error_log($sql);
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    public function Devolver($pcd_usuario) {
        $this->AtualizarStatus('3');
        $sql = "update solicita_visto "
                . " set soli_dt_devolucao = now()"
                . ", id_tipo_envio = 1"
                . ", cd_usuario_devolucao = " . $pcd_usuario ;
        $sql .= $this->SqlAuditagem($pcd_usuario, __CLASS__, __FUNCTION__)
                . " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . "." . __FUNCTION__);
    }

    public function EnviadoDigital($pcd_usuario) {
        $this->AtualizarStatus(cSTATUS_SOLICITACAO::ssNAO_CADASTRADA);
        $sql = "update solicita_visto "
                . " set soli_dt_devolucao = now()"
                . ", id_tipo_envio = 2"
                . ", cd_usuario_devolucao = " . $pcd_usuario ;
        $sql .= $this->SqlAuditagem($pcd_usuario, __CLASS__, __FUNCTION__)
                . " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . "." . __FUNCTION__);

//        cprocesso_mte::Atualiza_dt_envio_bsb($this->mID_SOLICITA_VISTO, date('Y-m-d'), $pcd_usuario);
//        cprocesso_mte::Atualiza_dt_requerimento($this->mID_SOLICITA_VISTO, date('Y-m-d'), $pcd_usuario);
//        cprocesso_mte::Atualiza_nu_processoDigital($pnu_processo, $pid_solicita_visto, $pcd_usuario)
    }

    public function IndicaPendenciaBSB($pcd_usuario) {
        $this->AtualizarStatus('11');
    }

    public function IndicaAceitaBSB($pcd_usuario) {
        $this->AtualizarStatus('7');
    }

    public function LiberaPendenciaBSB($pcd_usuario) {
        $this->AtualizarStatus('12');
    }

//
//	public static function IndicaProtocoloRealizadoAutomaticamente($pid_solicita_visto, $pcd_usuario)
//	{
//		$os = new cORDEMSERVICO();
//		$os->mID_SOLICITA_VISTO = $pid_solicita_visto;
//
//		$os->RecuperarSolicitaVisto();
//		if($os->mID_STATUS_SOL == 4 || $os->mID_STATUS_SOL == 11){
//			$os->AtualizarStatus(8);
//			$os->AcrescentaObservacaoBSB("Atualizada para não cadastrada pela atualização do processo");
//		}
//
//	}

    public function qtdProtocoloDoDia($pFiltros) {
        $filtro = strval(cBANCO::WhereFiltros($pFiltros));
        $sql = $this->sqlProtocoloAutorizacao($filtro, false);
        $sql.= " union ";
        $sql = $this->sqlProtocoloGenerico($filtro, false);
        $sql.= " union ";
        $sql.= $this->sqlProtocoloCancelamento($filtro, false);
        $sql.= " union ";
        $sql.= $this->sqlProtocoloProrrogacao($filtro, false);
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $tot = 0;
        while ($rs = $res->fetch(PDO::FETCH_BOTH)) {
            $tot = $tot + intval($rs[1]);
        }
        return $tot;
    }

    public function CursorProtocoloDoDia($pFiltros, $pOrderBy, $pPag = 1, $pOffset = 30) {
        $filtro = strval(cBANCO::WhereFiltros($pFiltros));
        if ($pOrderBy != '') {
            $orderby = " order by " . $pOrderBy;
        }

        $sql_mte = $this->sqlProtocoloAutorizacao($filtro);
        $sql_mte.= " union ";
        $sql_mte.= $this->sqlProtocoloCancelamento($filtro);
        $sql_mte.= " union ";
        $sql_mte.= $this->sqlProtocoloGenerico($filtro);

        $sql_mj = $this->sqlProtocoloProrrogacao($filtro);

        $pFiltros['dt_envio_bsb']->mWhere = " dt_envio_bsb_pre_cadastro = " . cBANCO::DataOk($pFiltros['dt_envio_bsb']->mValor);
        $filtro = cBANCO::WhereFiltros($pFiltros);
        $pFiltros['dt_envio_bsb']->mWhere = "";

        $sql_pre = $this->sqlProtocoloPrecadastro($filtro);

        if ($pFiltros['orgao_protocolo']->mValor == 'MTE') {
            $sql = $sql_mte . " union " . $sql_pre;
        } elseif ($pFiltros['orgao_protocolo']->mValor == 'MJ') {
            $sql = $sql_mj;
        } elseif ($pFiltros['orgao_protocolo']->mValor == 'MENOS_PRE') {
            $sql = $sql_mte . " union " . $sql_mj;
        } else {
            $sql = $sql_mte . " union " . $sql_mj . " union " . $sql_pre;
        }
        $sql .= $orderby;
        if ($pOffset > 0) {
            $sql .= " LIMIT " . (($pPag - 1) * $pOffset) . " , " . $pOffset;
        }
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public function sqlProtocoloAutorizacao($filtro, $colunas = true) {
        $sql_mte = "select 'mte' as tabela";
        if ($colunas) {
            $sql_mte .= "
                    , p.id_solicita_visto
                    , p.dt_envio_bsb
                    , p.dt_requerimento
                    , p.nu_processo nu_protocolo
                    , s.dt_solicitacao
                    , s.nu_solicitacao
                    , s.de_observacao_bsb
                    , s.soli_qt_autenticacoes
                    , s.soli_qt_autenticacoes_bsb
                        , s.stco_id
                    , sv.no_servico_resumido
                    , sv.id_tipo_acompanhamento
                    , ac.nu_candidato
                    , c.nome_completo
                    , c.nu_passaporte
                    , no_razao_social
                    , dt_prazo_estada_solicitado
                    , no_reparticao_consular
                    , eb.no_embarcacao_projeto
                    , coalesce(ebc.no_embarcacao_projeto, eb.no_embarcacao_projeto) no_embarcacao_projeto_cobranca
                    , pn.no_nacionalidade
                    , ta.no_reduzido_tipo_autorizacao
                    , s.id_status_sol
                    , ss.no_status_sol_res
                    , sc.stco_tx_nome";
        } else {
            $sql_mte .= " , count(*)";
        }
        $sql_mte .=" from processo_mte p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
                left join status_cobranca_os sc on sc.stco_id = s.stco_id
				join candidato c on c.nu_candidato = p.cd_candidato
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				left join embarcacao_projeto ebc on ebc.nu_empresa = s.nu_empresa and ebc.nu_embarcacao_projeto = s.nu_embarcacao_projeto_cobranca
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				left join pais_nacionalidade pn on pn.co_pais = c.co_nacionalidade
				left join tipo_autorizacao ta on ta.co_tipo_autorizacao = sv.co_tipo_autorizacao
				left join status_solicitacao ss on ss.id_status_sol = s.id_status_sol
				where 1=1 " . $filtro;
        if (!$colunas) {
            $sql_mte .= " group by 1";
        }
        return $sql_mte;
    }

    public function sqlProtocoloGenerico($filtro, $colunas = true) {
        $filtro = str_replace('p.dt_envio_bsb', 'p.date_2', $filtro);
        $sql_mte = "select 'generico' as tabela";
        if ($colunas) {
            $sql_mte .= "
                    , p.id_solicita_visto
                    , p.date_2 dt_envio_bsb
                    , p.date_1 dt_requerimento
                    , '--' nu_protocolo
                    , s.dt_solicitacao
                    , s.nu_solicitacao
                    , s.de_observacao_bsb
                    , s.soli_qt_autenticacoes
                    , s.soli_qt_autenticacoes_bsb
                        , s.stco_id
                    , sv.no_servico_resumido
                    , sv.id_tipo_acompanhamento
                    , ac.nu_candidato
                    , c.nome_completo
                    , c.nu_passaporte
                    , no_razao_social
                    , dt_prazo_estada_solicitado
                    , no_reparticao_consular
                    , eb.no_embarcacao_projeto
                    , coalesce(ebc.no_embarcacao_projeto, eb.no_embarcacao_projeto) no_embarcacao_projeto_cobranca
                    , pn.no_nacionalidade
                    , ta.no_reduzido_tipo_autorizacao
                    , s.id_status_sol
                    , ss.no_status_sol_res
                    , sc.stco_tx_nome";
        } else {
            $sql_mte .= " , count(*)";
        }
        $sql_mte .=" from processo_generico p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
                left join status_cobranca_os sc on sc.stco_id = s.stco_id
				join candidato c on c.nu_candidato = p.cd_candidato
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				left join embarcacao_projeto ebc on ebc.nu_empresa = s.nu_empresa and ebc.nu_embarcacao_projeto = s.nu_embarcacao_projeto_cobranca
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				left join pais_nacionalidade pn on pn.co_pais = c.co_nacionalidade
				left join tipo_autorizacao ta on ta.co_tipo_autorizacao = sv.co_tipo_autorizacao
				left join status_solicitacao ss on ss.id_status_sol = s.id_status_sol
				where id_tipoprocesso = 13
                                " . $filtro;
        if (!$colunas) {
            $sql_mte .= " group by 1";
        }
        return $sql_mte;
    }

    public function sqlProtocoloCancelamento($filtro, $colunas = true) {
        $sql = "select 'cancel' as tabela";
        if ($colunas) {
            $sql .= ", p.id_solicita_visto
                    , p.dt_envio_bsb
                    , p.dt_processo dt_requerimento
                    , p.nu_processo nu_protocolo
                    , s.dt_solicitacao
                    , s.nu_solicitacao
                    , s.de_observacao_bsb
                    , s.soli_qt_autenticacoes
                    , s.soli_qt_autenticacoes_bsb
                        , s.stco_id
                    , no_servico_resumido
                    , id_tipo_acompanhamento
                    , ac.nu_candidato
                    , nome_completo
                    , nu_passaporte
                    , no_razao_social
                    , dt_prazo_estada_solicitado
                    , '' no_reparticao_consular
                    , eb.no_embarcacao_projeto
                    , coalesce(ebc.no_embarcacao_projeto, eb.no_embarcacao_projeto) no_embarcacao_projeto_cobranca
                    , pn.no_nacionalidade
                    , ta.no_reduzido_tipo_autorizacao
                    , s.id_status_sol
                    , ss.no_status_sol_res
                    , sc.stco_tx_nome";
        } else {
            $sql .= " , count(*)";
        }
        $sql .=" from processo_cancel p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
                left join status_cobranca_os sc on sc.stco_id = s.stco_id
				join candidato c on c.nu_candidato = p.cd_candidato
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				left join embarcacao_projeto ebc on ebc.nu_empresa = s.nu_empresa and ebc.nu_embarcacao_projeto = s.nu_embarcacao_projeto_cobranca
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join pais_nacionalidade pn on pn.co_pais = c.co_nacionalidade
				left join tipo_autorizacao ta on ta.co_tipo_autorizacao = sv.co_tipo_autorizacao
				left join status_solicitacao ss on ss.id_status_sol = s.id_status_sol
				where 1=1
				" . $filtro;
        if (!$colunas) {
            $sql .= " group by 1";
        }
        return $sql;
    }

    public function sqlProtocoloProrrogacao($filtro, $colunas = true) {
        $sql = "select 'prorrog' as tabela";
        if ($colunas) {
            $sql .= ", p.id_solicita_visto
                    , p.dt_envio_bsb
                    , p.dt_requerimento
                    , p.nu_protocolo
                    , s.dt_solicitacao
                    , s.nu_solicitacao
                    , s.de_observacao_bsb
                    , s.soli_qt_autenticacoes
                    , s.soli_qt_autenticacoes_bsb
                        , s.stco_id
                    , no_servico_resumido
                    , id_tipo_acompanhamento
                    , ac.nu_candidato
                    , nome_completo
                    , nu_passaporte
                    , no_razao_social
                    , dt_prazo_estada_solicitado
                    , no_reparticao_consular
                    , eb.no_embarcacao_projeto
                    , coalesce(ebc.no_embarcacao_projeto, eb.no_embarcacao_projeto) no_embarcacao_projeto_cobranca
                    , pn.no_nacionalidade
                    , ta.no_reduzido_tipo_autorizacao
                    , s.id_status_sol
                    , ss.no_status_sol_res
                    , sc.stco_tx_nome";
        } else {
            $sql .= " , count(*)";
        }
        $sql .=" from processo_prorrog p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
                left join status_cobranca_os sc on sc.stco_id = s.stco_id
				join candidato c on c.nu_candidato = p.cd_candidato
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				left join embarcacao_projeto ebc on ebc.nu_empresa = s.nu_empresa and ebc.nu_embarcacao_projeto = s.nu_embarcacao_projeto_cobranca
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				left join pais_nacionalidade pn on pn.co_pais = c.co_nacionalidade
				left join tipo_autorizacao ta on ta.co_tipo_autorizacao = sv.co_tipo_autorizacao
				left join status_solicitacao ss on ss.id_status_sol = s.id_status_sol
				where 1=1
				" . $filtro;
        if (!$colunas) {
            $sql .= " group by 1";
        }
        return $sql;
    }

    public function sqlProtocoloPrecadastro($filtro, $colunas = true) {
        $sql = "
				select 'precadastro' as tabela";
        if ($colunas) {
            $sql .= ", p.id_solicita_visto
						, p.dt_envio_bsb_pre_cadastro dt_envio_bsb
						, p.dt_requerimento_pre_cadastro dt_requerimento
						, p.nu_pre_cadastro nu_protocolo
						, s.dt_solicitacao
						, s.nu_solicitacao
						, s.de_observacao_bsb
						, s.soli_qt_autenticacoes
                        , s.soli_qt_autenticacoes_bsb
                        , s.stco_id
    					, concat('Pré-cadastro ', no_servico_resumido) no_servico_resumido
						, id_tipo_acompanhamento
						, ac.nu_candidato
						, nome_completo
						, nu_passaporte
						, no_razao_social
						, dt_prazo_estada_solicitado
						, no_reparticao_consular
						, eb.no_embarcacao_projeto
						, coalesce(ebc.no_embarcacao_projeto, eb.no_embarcacao_projeto) no_embarcacao_projeto_cobranca
						, pn.no_nacionalidade
						, ta.no_reduzido_tipo_autorizacao
						, s.id_status_sol
                        , ss.no_status_sol_res
                        , sc.stco_tx_nome";
        } else {
            $sql .= " , count(*)";
        }
        $sql .=" from processo_prorrog p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
                left join status_cobranca_os sc on sc.stco_id = s.stco_id
				join candidato c on c.nu_candidato = p.cd_candidato
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				join embarcacao_projeto ebc on ebc.nu_empresa = s.nu_empresa and ebc.nu_embarcacao_projeto = s.nu_embarcacao_projeto_cobranca
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				left join pais_nacionalidade pn on pn.co_pais = c.co_nacionalidade
				left join tipo_autorizacao ta on ta.co_tipo_autorizacao = sv.co_tipo_autorizacao
				left join status_solicitacao ss on ss.id_status_sol = s.id_status_sol
				where 1=1
				" . $filtro;
        if (!$colunas) {
            $sql .= " group by 1";
        }
        return $sql;
    }

    public function CursorProtocoloAtualizar($pFiltros, $pOrderBy) {
        $filtro = cBANCO::WhereFiltros($pFiltros);
        if ($pOrderBy != '') {
            $orderby = " order by " . $pOrderBy;
        }
        $sql = "		select 'mte' as tabela
						, p.id_solicita_visto
						, p.dt_envio_bsb
						, p.dt_requerimento
						, p.nu_processo nu_protocolo
						, s.dt_solicitacao
						, s.nu_solicitacao
						, s.de_observacao_bsb
						, no_servico_resumido
						, id_tipo_acompanhamento
						, ac.nu_candidato
						, nome_completo
						, nu_passaporte
						, no_razao_social
						, dt_prazo_estada_solicitado
						, no_reparticao_consular
						, no_embarcacao_projeto
				from processo_mte p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				join candidato c on c.nu_candidato = ac.nu_candidato
				where s.id_status_sol >= 4 and (dt_requerimento is null or nu_processo is null)
				" . $filtro . "
			union
				select 'prorrog' as tabela
						, p.id_solicita_visto
						, p.dt_envio_bsb
						, p.dt_requerimento
						, p.nu_protocolo
						, s.dt_solicitacao
						, s.nu_solicitacao
						, s.de_observacao_bsb
						, no_servico_resumido
						, id_tipo_acompanhamento
						, ac.nu_candidato
						, nome_completo
						, nu_passaporte
						, no_razao_social
						, dt_prazo_estada_solicitado
						, no_reparticao_consular
						, no_embarcacao_projeto
				from processo_prorrog p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				join candidato c on c.nu_candidato = ac.nu_candidato
				where s.id_status_sol >= 4 and (dt_requerimento is null or nu_processo is null)
				" . $filtro . "
			union
				select 'cancel' as tabela
						, p.id_solicita_visto
						, p.dt_envio_bsb
						, p.dt_processo dt_requerimento
						, p.nu_processo nu_protocolo
						, s.dt_solicitacao
						, s.nu_solicitacao
						, s.de_observacao_bsb
						, no_servico_resumido
						, id_tipo_acompanhamento
						, ac.nu_candidato
						, nome_completo
						, nu_passaporte
						, no_razao_social
						, dt_prazo_estada_solicitado
						, '' no_reparticao_consular
						, no_embarcacao_projeto
				from processo_cancel p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				join candidato c on c.nu_candidato = ac.nu_candidato
				where s.id_status_sol >= 4 and (dt_requerimento is null or nu_processo is null)
				" . $filtro . "
				" . $orderby . "
			";
//		print $sql;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public function CursorNaoEnviadasBsb($pFiltros, $pOrderBy) {
        $filtro = cBANCO::WhereFiltros($pFiltros);
        if ($pOrderBy != '') {
            $orderby = " order by " . $pOrderBy;
        }
        $pFiltros['dt_envio_bsb']->mWhere = '1=1';

        $sql_mte = "
				select 'mte' as tabela
						, p.id_solicita_visto
						, p.dt_envio_bsb
						, s.dt_solicitacao
						, s.nu_solicitacao
						, s.de_observacao_bsb
						, no_servico_resumido
						, id_tipo_acompanhamento
						, ac.nu_candidato
						, nome_completo
						, nu_passaporte
						, no_razao_social
						, dt_prazo_estada_solicitado
						, no_reparticao_consular
						, no_embarcacao_projeto
						, soli_qt_autenticacoes
						, soli_qt_autenticacoes_bsb
				from processo_mte p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				join candidato c on c.nu_candidato = ac.nu_candidato
				where ((s.id_status_sol = 3 and p.dt_envio_bsb is null) or p.dt_envio_bsb=" . cBANCO::DataOk($pFiltros['dt_envio_bsb']->mValor) . ")
				" . $filtro . ""
                . " union select 'mudemb' as tabela
						, p.id_solicita_visto
						, p.date_2 dt_envio_bsb
						, s.dt_solicitacao
						, s.nu_solicitacao
						, s.de_observacao_bsb
						, no_servico_resumido
						, id_tipo_acompanhamento
						, ac.nu_candidato
						, nome_completo
						, nu_passaporte
						, no_razao_social
						, dt_prazo_estada_solicitado
						, no_reparticao_consular
						, no_embarcacao_projeto
						, soli_qt_autenticacoes
						, soli_qt_autenticacoes_bsb
				from processo_generico p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				join candidato c on c.nu_candidato = ac.nu_candidato
				where id_tipoprocesso = 13
                                and ((s.id_status_sol = 3 and p.date_2 is null) or p.date_2 =" . cBANCO::DataOk($pFiltros['dt_envio_bsb']->mValor) . ")
                    ";
        $sql_mj = "
				select 'prorrog' as tabela
						, p.id_solicita_visto
						, p.dt_envio_bsb
						, s.dt_solicitacao
						, s.nu_solicitacao
						, s.de_observacao_bsb
						, no_servico_resumido
						, id_tipo_acompanhamento
						, ac.nu_candidato
						, nome_completo
						, nu_passaporte
						, no_razao_social
						, dt_prazo_estada_solicitado
						, no_reparticao_consular
						, no_embarcacao_projeto
						, soli_qt_autenticacoes
						, soli_qt_autenticacoes_bsb
				from processo_prorrog p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				join candidato c on c.nu_candidato = ac.nu_candidato
				where ((s.id_status_sol = 3 and p.dt_envio_bsb is null) or p.dt_envio_bsb=" . cBANCO::DataOk($pFiltros['dt_envio_bsb']->mValor) . ")
				" . $filtro . "
			union
				select 'cancel' as tabela
						, p.id_solicita_visto
						, p.dt_envio_bsb
						, s.dt_solicitacao
						, s.nu_solicitacao
						, s.de_observacao_bsb
						, no_servico_resumido
						, id_tipo_acompanhamento
						, ac.nu_candidato
						, nome_completo
						, nu_passaporte
						, no_razao_social
						, dt_prazo_estada_solicitado
						, '' no_reparticao_consular
						, no_embarcacao_projeto
						, soli_qt_autenticacoes
						, soli_qt_autenticacoes_bsb
				from processo_cancel p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				join candidato c on c.nu_candidato = ac.nu_candidato
				where ((s.id_status_sol = 3 and p.dt_envio_bsb is null) or p.dt_envio_bsb=" . cBANCO::DataOk($pFiltros['dt_envio_bsb']->mValor) . ")
				" . $filtro;

        $sql_pre = "select 'precadastro' as tabela
						, p.id_solicita_visto
						, p.dt_envio_bsb_pre_cadastro dt_envio_bsb
						, s.dt_solicitacao
						, s.nu_solicitacao
						, s.de_observacao_bsb
						, concat('Pré-cadastro ', no_servico_resumido) no_servico_resumido
						, id_tipo_acompanhamento
						, ac.nu_candidato
						, nome_completo
						, nu_passaporte
						, no_razao_social
						, dt_prazo_estada_solicitado
						, no_reparticao_consular
						, no_embarcacao_projeto
						, soli_qt_autenticacoes
						, soli_qt_autenticacoes_bsb
				from processo_prorrog p
				join solicita_visto s on s.id_solicita_visto = p.id_solicita_visto
				join servico sv on sv.nu_servico = s.nu_servico and sv.serv_fl_envio_bsb=1
				join empresa e on e.nu_empresa = s.nu_empresa
				join embarcacao_projeto eb on eb.nu_empresa = s.nu_empresa and eb.nu_embarcacao_projeto = s.nu_embarcacao_projeto
				join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto and ac.nu_candidato = p.cd_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				join candidato c on c.nu_candidato = ac.nu_candidato
				where (p.nu_protocolo is not null and (p.dt_envio_bsb_pre_cadastro is null or p.dt_envio_bsb_pre_cadastro=" . cBANCO::DataOk($pFiltros['dt_envio_bsb']->mValor) . "))
				" . $filtro;

        if ($pFiltros['orgao_protocolo']->mValor == 'MTE') {
            $sql = $sql_mte;
        } elseif ($pFiltros['orgao_protocolo']->mValor == 'MJ') {
            $sql = $sql_mj;
        } elseif ($pFiltros['orgao_protocolo']->mValor == 'PRE') {
            $sql = $sql_pre;
        } else {
            $sql = $sql_mte . " union " . $sql_mj . " union " . $sql_pre;
        }
        $sql .= $orderby;
        // print $sql;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public static function TotalAutenticacoesProtocolo($pdt_envio_bsb) {
        $data = cBANCO::DataOk($pdt_envio_bsb);
        $sql = "select sum(soli_qt_autenticacoes) soli_qt_autenticacoes from solicita_visto sv
				where id_solicita_visto in (select id_solicita_visto from processo_mte where dt_envio_bsb = " . $data . ")
				or id_solicita_visto in (select id_solicita_visto from processo_prorrog where dt_envio_bsb = " . $data . ")
				or id_solicita_visto in (select id_solicita_visto from processo_cancel where dt_envio_bsb = " . $data . ")
				or id_solicita_visto in (select id_solicita_visto from processo_generico where id_tipoprocesso = 13 and date_2 =" . $data . ")
			";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        return $rs['soli_qt_autenticacoes'];
    }

    public static function TotalAutenticacoesBsbProtocolo($pdt_envio_bsb) {
        $data = cBANCO::DataOk($pdt_envio_bsb);
        $sql = "select sum(ifnull(soli_qt_autenticacoes_bsb, 0)) soli_qt_autenticacoes from solicita_visto sv
				where id_solicita_visto in (select id_solicita_visto from processo_mte where dt_envio_bsb = " . $data . ")
				or id_solicita_visto in (select id_solicita_visto from processo_prorrog where dt_envio_bsb = " . $data . ")
				or id_solicita_visto in (select id_solicita_visto from processo_cancel where dt_envio_bsb = " . $data . ")
				or id_solicita_visto in (select id_solicita_visto from processo_generico where id_tipoprocesso = 13 and date_2 =" . $data . ")
			";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        return $rs['soli_qt_autenticacoes'];
    }

    public static function Faturar($pid_solicita_visto, $presc_id) {
        $os = new cORDEMSERVICO();
        $os->mID_SOLICITA_VISTO = $pid_solicita_visto;
        $os->Fature($presc_id);
    }

    public function Fature($presc_id, $pobsFaturamento = '') {
        $sql = " update solicita_visto ";
        $sql .= "   set resc_id = " . $presc_id;
        $sql .= "     , stco_id = 3";
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__) ;
        $sql .= " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;

        cAMBIENTE::$db_pdo->exec($sql);
        $this->AtribuaValorDeTabela();

        if ($pobsFaturamento != '') {
            $this->AcrescentaObservacaoCobranca($pobsFaturamento);
        }
    }

    public function AtribuaValorDeTabela() {
        $sql = " select soli_vl_cobrado
				from solicita_visto
				where id_solicita_visto= " . $this->mID_SOLICITA_VISTO;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);

        if ($rs['soli_vl_cobrado'] == '') {
            $sql = " select
                        case s.tppr_id
                                when 1 then 0
                                when 2 then pr.prec_vl_conceito
                                when 3 then pr.prec_vl_pacote
                                else 0
                        end soli_vl_cobrado_tabela
                from solicita_visto s
                left join EMPRESA                E on E.NU_EMPRESA = s.nu_empresa_requerente
                left join tabela_precos         tp on tp.tbpc_id = E.tbpc_id
                left join SERVICO		   SV on SV.NU_SERVICO = s.NU_SERVICO
                left join preco					pr on pr.tbpc_id = E.tbpc_id and pr.nu_servico = s.nu_servico
                where id_solicita_visto= " . $this->mID_SOLICITA_VISTO;
            $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
            $rs = $res->fetch(PDO::FETCH_ASSOC);

            $sql = "update solicita_visto set soli_vl_cobrado = " . cBANCO::ValorOk($rs['soli_vl_cobrado_tabela']) . " where id_solicita_visto =" . $this->mID_SOLICITA_VISTO;
            cAMBIENTE::ExecuteQuery($sql);
        }
    }

    public function CursorOsRegistro($pFiltros, $pOrderBy) {

        $sql = "select sv.id_solicita_visto
					, E.no_razao_social
					, C.nome_completo
					, C.nu_candidato
					, EP.no_embarcacao_projeto
					, date_format(p.dt_requerimento, '%d/%m/%y') dt_requerimento
					, p.nu_protocolo
					, C.nu_passaporte
					, sv.nu_solicitacao
					, concat(depf_no_nome, ' (', depf_cd_uf, ')') depf_no_nome
				from solicita_visto sv
				JOIN AUTORIZACAO_CANDIDATO AC on AC.id_solicita_visto = sv.id_solicita_visto
				JOIN CANDIDATO C			on C.NU_CANDIDATO = AC.NU_CANDIDATO
				JOIN processo_regcie p		on p.id_solicita_visto = sv.id_solicita_visto and p.cd_candidato = C.nu_candidato
				JOIN EMPRESA E				on E.NU_EMPRESA = sv.NU_EMPRESA
				JOIN EMBARCACAO_PROJETO EP	on EP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = C.NU_EMPRESA
				LEFT JOIN delegacia_pf d	on d.depf_cd_codigo = substr(trim(replace(p.nu_protocolo, '''', '')),1,5)
				where sv.NU_SERVICO in (58, 20) ";
        $filtro = cBANCO::WhereFiltros($pFiltros);

        $orderby = '';
        if ($pOrderBy != '') {
            $orderby = " order by " . $pOrderBy;
        }
        $sql .= $filtro . $orderby;
//		print $sql;
        return cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function AcrescentaObservacaoBSB($ptexto) {
        $sql = "update solicita_visto "
                . " set de_observacao_bsb = concat(coalesce(concat(de_observacao_bsb, '<br/>'), ''), " . cBANCO::StringOk($ptexto) . ") ";
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__)
                . " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function AcrescentaObservacaoCobranca($ptexto) {
        $sql = "update solicita_visto "
                . " set soli_tx_obs_cobranca = concat(coalesce(concat(soli_tx_obs_cobranca, '<br/>'), ''), " . cBANCO::StringOk($ptexto) . ") ";
        $sql .= $this->SqlAuditagem(cSESSAO::$mcd_usuario, __CLASS__, __FUNCTION__)
                . " where id_solicita_visto = " . $this->mID_SOLICITA_VISTO;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function StatusPossiveis($pid_status_sol = null) {
        if ($pid_status_sol != '') {
            $this->mID_STATUS_SOL = $pid_status_sol;
        }
        $ret = array();
        switch ($this->mID_STATUS_SOL) {
            // NOVA
            case 1:
                $ret[] = array('id_status_sol' => 2, "no_status_sol" => 'pendente');
                $ret[] = array('id_status_sol' => 3, "no_status_sol" => 'devolvida');
                break;
            // PENDENTE
            case 2:
                $ret[] = array('id_status_sol' => 3, "no_status_sol" => 'devolvida');
                break;
            // DEVOLVIDA
            case 3:
                $ret[] = array('id_status_sol' => 4, "no_status_sol" => 'enviada BSB');
                break;
            // ENVIADA BSB
            case 4:
                $ret[] = array('id_status_sol' => 7, "no_status_sol" => 'pendência BSB');
                $ret[] = array('id_status_sol' => 8, "no_status_sol" => 'protocoloada');
                break;
            // ENCERRADA
            case 6:
                break;
            // PENDENCIA BSB
            case 7:
                $ret[] = array('id_status_sol' => 11, "no_status_sol" => 'autorizada');
                break;
            // NÃƒO CADASTRADA
            case 8:
                $ret[] = array('id_status_sol' => 9, "no_status_sol" => 'em análise');
                $ret[] = array('id_status_sol' => 10, "no_status_sol" => 'em exigência');
                break;
            // NÃƒO CADASTRADA
            case 8:
                $ret[] = array('id_status_sol' => 9, "no_status_sol" => 'em análise');
                $ret[] = array('id_status_sol' => 10, "no_status_sol" => 'em exigência');
                break;
        }
    }

    public static function CriarServicosPIT($pnu_servico, $pcandidatos, $pmes, $pano) {
        $chaves = explode(",", $pcandidatos);
        foreach ($chaves as $nu_candidato) {
            if (intval($nu_candidato) > 0) {
                $cand = new cCANDIDATO();
                $cand->Recuperar($nu_candidato);
                $os = new cORDEMSERVICO();
                $os->mID_SOLICITA_VISTO = 0;
                $os->mNU_EMPRESA = $cand->mNU_EMPRESA;
                $os->mNU_EMBARCACAO_PROJETO = $cand->mNU_EMBARCACAO_PROJETO;
                $os->mnu_empresa_requerente = $cand->mNU_EMPRESA;
                $os->mNU_EMBARCACAO_PROJETO_COBRANCA = $cand->mNU_EMBARCACAO_PROJETO;
                $os->mNU_SERVICO = $pnu_servico;
                $os->mdt_solicitacao = '01/' . $pmes . '/' . $pano;
                $os->mno_solicitador = cSESSAO::$mnome;

                $os->mcd_tecnico = cSESSAO::$mcd_usuario;
                $os->mde_observacao = "Criação automática pela tela de controle de PIT";
                $os->mNU_USUARIO_CAD = cSESSAO::$mcd_usuario;
                $os->mCandidatos = $nu_candidato;
                $os->mcd_usuario_alteracao = cSESSAO::$mcd_usuario;


                $os->SalvarInstancia();
                $os->mtppr_id = 1; // sempre conceito
                $os->msoli_tx_obs_cobranca = "Liberada para cobrança automaticamente depois de criada";
                $os->LibereParaCobranca(cSESSAO::$mcd_usuario);
            }
        }
    }

    public static function CursorOSPIT($pFiltros, $pOrderBy) {
        $sql = "select sv.id_solicita_visto
					, sv.dt_solicitacao
					, E.no_razao_social
					, EP.no_embarcacao_projeto
					, C.nome_completo
					, C.nu_candidato
					, C.nu_passaporte
					, C.nu_cpf
					, sv.nu_solicitacao
				from solicita_visto sv
				left JOIN EMPRESA E				on E.NU_EMPRESA = sv.NU_EMPRESA
				left JOIN EMBARCACAO_PROJETO EP	on EP.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = sv.NU_EMPRESA
				JOIN AUTORIZACAO_CANDIDATO AC on AC.id_solicita_visto = sv.id_solicita_visto
				JOIN CANDIDATO C			on C.NU_CANDIDATO = AC.NU_CANDIDATO
				where sv.NU_SERVICO in (71) ";
        $filtro = cBANCO::WhereFiltros($pFiltros);
        $sql .= $filtro;
        $orderby = '';
        if ($pOrderBy != '') {
            $orderby = " order by " . $pOrderBy;
        }
        $sql .= $filtro . $orderby;
//		print $sql;
        return cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public static function FabriquePeloId($pid_solicita_visto) {
        $os = new cORDEMSERVICO();
        $os->Recuperar($pid_solicita_visto);
        $os->RecuperarSolicitaVistoCobranca();
        $os->RecuperarCandidatos();
        return $os;
    }

    public function CobrancaPacoteHabilitada() {
        $rs = cSERVICO::pacoteQueAbre($this->mNU_SERVICO, $this->mNU_EMPRESA);
        if (isset($rs) && $rs['NU_SERVICO'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function AtualizaNumeroProcesso($nu_processo, $cd_usuario) {
        switch ($this->mID_TIPO_ACOMPANHAMENTO) {
            case 1:  // Autorizacao
                $proc = new cprocesso_mte();
                $proc->RecupereSePelaOsSemCand($this->mID_SOLICITA_VISTO);
                if ($proc->mdt_requerimento != '' || $this->mid_tipo_envio > 0) {
                    cprocesso_mte::Atualiza_nu_processo($nu_processo, $this->mID_SOLICITA_VISTO, $cd_usuario);
                } else {
                    throw new cERRO_CONSISTENCIA('Para informar o número do processo é necessário que a data do requerimento esteja preenchida, ou então que seja um processo enviado por certificado digital. Por favor verifique.');
                }

                break;
            case 3:  // Prorrogacao
                $proc = new cprocesso_prorrog();
                $proc->RecupereSePelaOsSemCand($this->mID_SOLICITA_VISTO);
                if ($proc->mdt_requerimento != '' || $this->mid_tipo_envio > 0) {
                    cprocesso_prorrog::AtualizaPelaOs_nu_protocolo($nu_processo, $this->mID_SOLICITA_VISTO, $cd_usuario);
                } else {
                    throw new cERRO_CONSISTENCIA('Para informar o número do processo é necessário que a data do requerimento esteja preenchida, ou então que seja um processo enviado por certificado digital. Por favor verifique.');
                }
                break;
            case 6:  // Cancelamento
                cprocesso_cancel::AtualizaPelaOs_nu_processo($nu_processo, $this->mID_SOLICITA_VISTO, $cd_usuario);
                break;
        }
        $this->Protocola($cd_usuario);
    }

    public function AtualizaNumeroProcessoDigital($nu_processo, $cd_usuario) {
        switch ($this->mID_TIPO_ACOMPANHAMENTO) {
            case 1:  // Autorizacao
                $proc = new cprocesso_mte();
                $proc->RecupereSePelaOsSemCand($this->mID_SOLICITA_VISTO);
                if ($proc->mdt_requerimento != '' || $this->mid_tipo_envio > 0) {
                    cprocesso_mte::Atualiza_nu_processoDigital($nu_processo, $this->mID_SOLICITA_VISTO, $cd_usuario);
                } else {
                    throw new cERRO_CONSISTENCIA('Para informar o número do processo é necessário que a data do requerimento esteja preenchida, ou então que seja um processo enviado por certificado digital. Por favor verifique.');
                }
                break;
            case 3:  // Prorrogacao
                $proc = new cprocesso_prorrog();
                $proc->RecupereSePelaOsSemCand($this->mID_SOLICITA_VISTO);
                if ($proc->mdt_requerimento != '' || $this->mid_tipo_envio > 0) {
                    cprocesso_prorrog::AtualizaPelaOs_nu_protocoloDigital($nu_processo, $this->mID_SOLICITA_VISTO, $cd_usuario);
                } else {
                    throw new cERRO_CONSISTENCIA('Para informar o número do processo é necessário que a data do requerimento esteja preenchida, ou então que seja um processo enviado por certificado digital. Por favor verifique.');
                }
                break;
            case 6:  // Cancelamento
                cprocesso_cancel::AtualizaPelaOs_nu_processo($nu_processo, $this->mID_SOLICITA_VISTO, $cd_usuario);
                break;
        }
        $this->Protocola($cd_usuario);
    }

    public function Protocola($cd_usuario) {
        if ($this->mID_STATUS_SOL == cSTATUS_SOLICITACAO::ssACEITA_BSB || $this->mID_STATUS_SOL == cSTATUS_SOLICITACAO::ssDEVOLVIDA || $this->mID_STATUS_SOL == cSTATUS_SOLICITACAO::ssNOVA || $this->mID_STATUS_SOL == cSTATUS_SOLICITACAO::ssENVIADA_BSB || $this->mID_STATUS_SOL == cSTATUS_SOLICITACAO::ssPENDENCIA_BSB || $this->mID_STATUS_SOL == cSTATUS_SOLICITACAO::ssPENDENCIA_LIBERADA || $this->mID_STATUS_SOL == cSTATUS_SOLICITACAO::ssPENDENTE
        ) {
            $this->AtualizarStatus(cSTATUS_SOLICITACAO::ssPROTOCOLADA, $cd_usuario);
        }
    }

    public function AtualizaDtRequerimento($dt_requerimento, $cd_usuario) {
        switch ($this->mID_TIPO_ACOMPANHAMENTO) {
            case 1:  // Autorizacao
                cprocesso_mte::Atualiza_dt_requerimento($dt_requerimento, $this->mID_SOLICITA_VISTO, $cd_usuario);
                break;
            case 3:  // Prorrogacao
                cprocesso_prorrog::Atualiza_dt_requerimento($dt_requerimento, $this->mID_SOLICITA_VISTO, $cd_usuario);
                break;
            case 6:  // Cancelamento
                cprocesso_cancel::Atualiza_dt_processo($dt_requerimento, $this->mID_SOLICITA_VISTO, $cd_usuario);
                break;
        }
    }

    public function comboCandidatosMustache($default = "(geral)") {
        $sql = "select autorizacao_candidato.nu_candidato valor, concat(nome_completo, ' (', autorizacao_candidato.nu_candidato, ')') descricao"
                . " from autorizacao_candidato"
                . " join candidato on candidato.nu_candidato = autorizacao_candidato.nu_candidato "
                . " where autorizacao_candidato.id_solicita_visto = ".$this->mID_SOLICITA_VISTO
                . " order by nome_completo";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));

        if (intval($valor_atual == 0)){
            $rs['0']['selected'] = true;
        }

        return  $rs ;
    }


    public function cursorCandidatosComboJson() {
        $sql = "select ac.nu_candidato chave , nome_completo descricao"
                . " from autorizacao_candidato ac "
                . " join candidato c on c.nu_candidato = ac.nu_candidato"
                . " where ac.id_solicita_visto =".$this->mID_SOLICITA_VISTO
                . " order by nome_completo";
        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rx = $rs->fetchAll(PDO::FETCH_ASSOC);
        return $rx;
    }
    /**
     * Retorna as taxas pagas associadas à OS
     * @return [type] [description]
     */
    public function taxaspagas(){
        $sql = "select * from taxapaga where id_solicita_visto = ".$this->mID_SOLICITA_VISTO;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        var_dump($rs);
        return $rs;
    }

    public function salveTodosAtributos(){
        if ($this->msoli_qt_autenticacoes == ''){
            $this->msoli_qt_autenticacoes = 0;
        }
        if ($this->msoli_qt_autenticacoes_bsb == ''){
            $this->msoli_qt_autenticacoes_bsb = 0;
        }
        if ($this->mID_SOLICITA_VISTO > 0){
            $sql = "update solicita_visto set ";
            $sql .= "    nu_solicitacao= " . cBANCO::ChaveOk($this->mNU_SOLICITACAO);
            $sql .= "  , cd_libs_finan= " . cBANCO::ChaveOk($this->mcd_libs_finan);
            $sql .= "  , nu_empresa= " . cBANCO::ChaveOk($this->mNU_EMPRESA);
            $sql .= "  , no_solicitador= " . cBANCO::StringOk($this->mno_solicitador);
            $sql .= "  , dt_solicitacao= " . cBANCO::DataOk($this->mdt_solicitacao);
            $sql .= "  , cd_admin_fim= " . cBANCO::ChaveOk($this->mcd_admin_fim);
            $sql .= "  , dt_finalizacao= " . cBANCO::DataOk($this->mdt_finalizacao);
            $sql .= "  , nm_arquivo_sol= " . cBANCO::StringOk($this->mnm_arquivo_sol);
            $sql .= "  , de_observacao= " . cBANCO::StringOk($this->mde_observacao);
            $sql .= "  , NU_SERVICO= " . cBANCO::ChaveOk($this->mNU_SERVICO);
            $sql .= "  , nu_empresa_requerente= " . cBANCO::ChaveOk($this->mnu_empresa_requerente);
            $sql .= "  , dt_envio_bsb= " . cBANCO::DataOk($this->mdt_envio_bsb);
            $sql .= "  , dt_recebimento_bsb= " . cBANCO::DataOk($this->mdt_recebimento_bsb);
            $sql .= "  , fl_cobrado= " . cBANCO::SimNaoOk($this->mfl_cobrado);
            $sql .= "  , fl_nao_conformidade= " . cBANCO::SimNaoOk($this->mfl_nao_conformidade);
            $sql .= "  , ID_STATUS_SOL= " . cBANCO::ChaveOk($this->mID_STATUS_SOL);
            $sql .= "  , fl_sistema_antigo= " . cBANCO::SimNaoOk($this->mfl_sistema_antigo);
            $sql .= "  , dt_liberacao= " . cBANCO::DataOk($this->mdt_liberacao);
            $sql .= "  , cd_usuario_qualidade= " . cBANCO::ChaveOk($this->mcd_usuario_qualidade);
            $sql .= "  , fl_conclusao_conforme= " . cBANCO::SimNaoOk($this->mfl_conclusao_conforme);
            $sql .= "  , fl_liberado= " . cBANCO::SimNaoOk($this->mfl_liberado);
            $sql .= "  , de_comentarios_liberacao= " . cBANCO::StringOk($this->mde_comentarios_liberacao);
            $sql .= "  , cd_usuario_liberacao= " . cBANCO::ChaveOk($this->mcd_usuario_liberacao);
            $sql .= "  , fl_liberacao_fechada= " . cBANCO::SimNaoOk($this->mfl_liberacao_fechada);
            $sql .= "  , fl_qualidade_fechada= " . cBANCO::SimNaoOk($this->mfl_qualidade_fechada);
            $sql .= "  , NU_EMBARCACAO_PROJETO= " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
            $sql .= "  , stco_id= " . cBANCO::ChaveOk($this->mstco_id);
            $sql .= "  , resc_id= " . cBANCO::ChaveOk($this->mresc_id);
            $sql .= "  , soli_vl_cobrado= " . cBANCO::ValorOk($this->msoli_vl_cobrado);
            $sql .= "  , soli_tx_obs_cobranca= " . cBANCO::StringOk($this->msoli_tx_obs_cobranca);
            $sql .= "  , soli_dt_liberacao_cobranca= " . cBANCO::DataOk($this->msoli_dt_liberacao_cobranca);
            $sql .= "  , soli_dt_cobranca= " . cBANCO::DataOk($this->msoli_dt_cobranca);
            $sql .= "  , cd_usuario_liberacao_cobranca= " . cBANCO::ChaveOk($this->mcd_usuario_liberacao_cobranca);
            $sql .= "  , cd_usuario_cobranca= " . cBANCO::ChaveOk($this->mcd_usuario_cobranca);
            $sql .= "  , NU_EMPRESA_COBRANCA= " . cBANCO::ChaveOk($this->mNU_EMPRESA_COBRANCA);
            $sql .= "  , NU_EMBARCACAO_PROJETO_COBRANCA= " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO_COBRANCA);
            $sql .= "  , tbpc_id= " . cBANCO::ChaveOk($this->mtbpc_id);
            $sql .= "  , tppr_id= " . cBANCO::ChaveOk($this->mtppr_id);
            $sql .= "  , cd_usuario_analista= " . cBANCO::ChaveOk($this->mcd_usuario_analista);
            $sql .= "  , soli_tx_codigo_servico= " . cBANCO::StringOk($this->msoli_tx_codigo_servico);
            $sql .= "  , soli_tx_solicitante_cobranca= " . cBANCO::StringOk($this->msoli_tx_solicitante_cobranca);
            $sql .= "  , cd_usuario_alteracao= " . cBANCO::ChaveOk($this->mcd_usuario_alteracao);
            $sql .= "  , soli_dt_alteracao= " . cBANCO::DataOk($this->msoli_dt_alteracao);
            $sql .= "  , soli_tx_descricao_servico= " . cBANCO::StringOk($this->msoli_tx_descricao_servico);
            $sql .= "  , hios_id_atual= " . cBANCO::ChaveOk($this->mhios_id_atual);
            $sql .= "  , soli_dt_devolucao= " . cBANCO::DataOk($this->msoli_dt_devolucao);
            $sql .= "  , cd_usuario_devolucao= " . cBANCO::ChaveOk($this->mcd_usuario_devolucao);
            $sql .= "  , de_observacao_bsb= " . cBANCO::StringOk($this->mde_observacao_bsb);
            $sql .= "  , soli_qt_autenticacoes= " . cBANCO::ChaveOk($this->msoli_qt_autenticacoes);
            $sql .= "  , id_solicita_visto_pacote= " . cBANCO::ChaveOk($this->mid_solicita_visto_pacote);
            $sql .= "  , nu_servico_pacote= " . cBANCO::ChaveOk($this->mnu_servico_pacote);
            $sql .= "  , soli_tx_justificativa= " . cBANCO::StringOk($this->msoli_tx_justificativa);
            $sql .= "  , id_tipo_envio= " . cBANCO::ChaveOk($this->mid_tipo_envio);
            $sql .= "  , soli_qt_autenticacoes_bsb= " . cBANCO::InteiroOk($this->msoli_qt_autenticacoes_bsb);
            $sql .= "  , cd_tecnico= " . cBANCO::ChaveOk($this->mcd_tecnico);
            $sql .= "  , controller= " . cBANCO::StringOk($this->mcontroller);
            $sql .= "  , metodo= " . cBANCO::StringOk($this->mmetodo);
            $sql .= "  , soli_fl_incompleta= " . cBANCO::SimNaoOk($this->msoli_fl_incompleta);
            $sql .= "  , soli_fl_disponivel= " . cBANCO::SimNaoOk($this->msoli_fl_disponivel);
            $sql .= "  , soli_fl_pacote_incompleto= " . cBANCO::SimNaoOk($this->msoli_fl_pacote_incompleto);
            $sql .= "  , cd_usuario_faturador_temp= " . cBANCO::ChaveOk($this->mcd_usuario_faturador_temp);
            $sql .= " where id_solicita_visto = ".$this->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);
        } else {
            $sql = "insert into solicita_visto(";
            $sql .= "    NU_SOLICITACAO";
            $sql .= "   , cd_libs_finan";
            $sql .= "   , nu_empresa";
            $sql .= "   , no_solicitador";
            $sql .= "   , dt_solicitacao";
            $sql .= "   , cd_admin_cad";
            $sql .= "   , dt_cadastro";
            $sql .= "   , cd_admin_fim";
            $sql .= "   , dt_finalizacao";
            $sql .= "   , nm_arquivo_sol";
            $sql .= "   , de_observacao";
            $sql .= "   , NU_SERVICO";
            $sql .= "   , nu_empresa_requerente";
            $sql .= "   , dt_envio_bsb";
            $sql .= "   , dt_recebimento_bsb";
            $sql .= "   , fl_cobrado";
            $sql .= "   , fl_nao_conformidade";
            $sql .= "   , ID_STATUS_SOL";
            $sql .= "   , fl_sistema_antigo";
            $sql .= "   , dt_liberacao";
            $sql .= "   , cd_usuario_qualidade";
            $sql .= "   , fl_conclusao_conforme";
            $sql .= "   , fl_liberado";
            $sql .= "   , de_comentarios_liberacao";
            $sql .= "   , cd_usuario_liberacao";
            $sql .= "   , fl_liberacao_fechada";
            $sql .= "   , fl_qualidade_fechada";
            $sql .= "   , NU_EMBARCACAO_PROJETO";
            $sql .= "   , stco_id";
            $sql .= "   , resc_id";
            $sql .= "   , soli_vl_cobrado";
            $sql .= "   , soli_tx_obs_cobranca";
            $sql .= "   , soli_dt_liberacao_cobranca";
            $sql .= "   , soli_dt_cobranca";
            $sql .= "   , cd_usuario_liberacao_cobranca";
            $sql .= "   , cd_usuario_cobranca";
            $sql .= "   , NU_EMPRESA_COBRANCA";
            $sql .= "   , NU_EMBARCACAO_PROJETO_COBRANCA";
            $sql .= "   , tbpc_id";
            $sql .= "   , tppr_id";
            $sql .= "   , cd_usuario_analista";
            $sql .= "   , soli_tx_codigo_servico";
            $sql .= "   , soli_tx_solicitante_cobranca";
            $sql .= "   , cd_usuario_alteracao";
            $sql .= "   , soli_dt_alteracao";
            $sql .= "   , soli_tx_descricao_servico";
            $sql .= "   , hios_id_atual";
            $sql .= "   , soli_dt_devolucao";
            $sql .= "   , cd_usuario_devolucao";
            $sql .= "   , de_observacao_bsb";
            $sql .= "   , soli_qt_autenticacoes";
            $sql .= "   , id_solicita_visto_pacote";
            $sql .= "   , nu_servico_pacote";
            $sql .= "   , soli_tx_justificativa";
            $sql .= "   , id_tipo_envio";
            $sql .= "   , soli_qt_autenticacoes_bsb";
            $sql .= "   , cd_tecnico";
            $sql .= "   , controller";
            $sql .= "   , metodo";
            $sql .= "   , soli_fl_incompleta";
            $sql .= "   , soli_fl_disponivel";
            $sql .= "   , soli_fl_pacote_incompleto";
            $sql .= "   , cd_usuario_faturador_temp";
            $sql .= ") values (";
            $sql .= "   0" ;
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_libs_finan);
            $sql .= "  , " . cBANCO::ChaveOk($this->mNU_EMPRESA);
            $sql .= "  , " . cBANCO::StringOk($this->mno_solicitador);
            $sql .= "  , " . cBANCO::DataOk($this->mdt_solicitacao);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_admin_cad);
            $sql .= "  , " . cBANCO::DataOk($this->mdt_cadastro);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_admin_fim);
            $sql .= "  , " . cBANCO::DataOk($this->mdt_finalizacao);
            $sql .= "  , " . cBANCO::StringOk($this->mnm_arquivo_sol);
            $sql .= "  , " . cBANCO::StringOk($this->mde_observacao);
            $sql .= "  , " . cBANCO::ChaveOk($this->mNU_SERVICO);
            $sql .= "  , " . cBANCO::ChaveOk($this->mnu_empresa_requerente);
            $sql .= "  , " . cBANCO::DataOk($this->mdt_envio_bsb);
            $sql .= "  , " . cBANCO::DataOk($this->mdt_recebimento_bsb);
            $sql .= "  , " . cBANCO::SimNaoOk($this->mfl_cobrado);
            $sql .= "  , " . cBANCO::SimNaoOk($this->mfl_nao_conformidade);
            $sql .= "  , " . cBANCO::ChaveOk($this->mID_STATUS_SOL);
            $sql .= "  , " . cBANCO::SimNaoOk($this->mfl_sistema_antigo);
            $sql .= "  , " . cBANCO::DataOk($this->mdt_liberacao);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_usuario_qualidade);
            $sql .= "  , " . cBANCO::SimNaoOk($this->mfl_conclusao_conforme);
            $sql .= "  , " . cBANCO::SimNaoOk($this->mfl_liberado);
            $sql .= "  , " . cBANCO::StringOk($this->mde_comentarios_liberacao);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_usuario_liberacao);
            $sql .= "  , " . cBANCO::SimNaoOk($this->mfl_liberacao_fechada);
            $sql .= "  , " . cBANCO::SimNaoOk($this->mfl_qualidade_fechada);
            $sql .= "  , " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
            $sql .= "  , " . cBANCO::ChaveOk($this->mstco_id);
            $sql .= "  , " . cBANCO::ChaveOk($this->mresc_id);
            $sql .= "  , " . cBANCO::ValorOk($this->msoli_vl_cobrado);
            $sql .= "  , " . cBANCO::StringOk($this->msoli_tx_obs_cobranca);
            $sql .= "  , " . cBANCO::DataOk($this->msoli_dt_liberacao_cobranca);
            $sql .= "  , " . cBANCO::DataOk($this->msoli_dt_cobranca);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_usuario_liberacao_cobranca);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_usuario_cobranca);
            $sql .= "  , " . cBANCO::ChaveOk($this->mNU_EMPRESA_COBRANCA);
            $sql .= "  , " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO_COBRANCA);
            $sql .= "  , " . cBANCO::ChaveOk($this->mtbpc_id);
            $sql .= "  , " . cBANCO::ChaveOk($this->mtppr_id);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_usuario_analista);
            $sql .= "  , " . cBANCO::StringOk($this->msoli_tx_codigo_servico);
            $sql .= "  , " . cBANCO::StringOk($this->msoli_tx_solicitante_cobranca);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_usuario_alteracao);
            $sql .= "  , " . cBANCO::DataOk($this->msoli_dt_alteracao);
            $sql .= "  , " . cBANCO::StringOk($this->msoli_tx_descricao_servico);
            $sql .= "  , " . cBANCO::ChaveOk($this->mhios_id_atual);
            $sql .= "  , " . cBANCO::DataOk($this->msoli_dt_devolucao);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_usuario_devolucao);
            $sql .= "  , " . cBANCO::StringOk($this->mde_observacao_bsb);
            $sql .= "  , " . cBANCO::InteiroOk($this->msoli_qt_autenticacoes);
            $sql .= "  , " . cBANCO::ChaveOk($this->mid_solicita_visto_pacote);
            $sql .= "  , " . cBANCO::ChaveOk($this->mnu_servico_pacote);
            $sql .= "  , " . cBANCO::StringOk($this->msoli_tx_justificativa);
            $sql .= "  , " . cBANCO::ChaveOk($this->mid_tipo_envio);
            $sql .= "  , " . cBANCO::InteiroOk($this->msoli_qt_autenticacoes_bsb);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_tecnico);
            $sql .= "  , " . cBANCO::StringOk($this->mcontroller);
            $sql .= "  , " . cBANCO::StringOk($this->mmetodo);
            $sql .= "  , " . cBANCO::SimNaoOk($this->msoli_fl_incompleta);
            $sql .= "  , " . cBANCO::SimNaoOk($this->msoli_fl_disponivel);
            $sql .= "  , " . cBANCO::SimNaoOk($this->msoli_fl_pacote_incompleto);
            $sql .= "  , " . cBANCO::ChaveOk($this->mcd_usuario_faturador_temp);
            $sql .= ")";
            cAMBIENTE::$db_pdo->exec($sql);
            $this->mID_SOLICITA_VISTO = cAMBIENTE::$db_pdo->lastInsertId();
            $x = $this->mID_SOLICITA_VISTO ;
            // Numera a OS
            $stmt = cAMBIENTE::$db_pdo->prepare("CALL os_gera_numero(?)");
            $stmt->bindParam(1, $x, PDO::PARAM_INT);
            $stmt->execute();
            $rs = $stmt->fetchAll();
            $this->mNU_SOLICITACAO = $rs[0]['nu_solicitacao'];
        }
    }
}
