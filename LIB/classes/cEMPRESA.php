<?php

class cEMPRESA extends cMODELO {

    public $mNU_EMPRESA;
    public $mNO_RAZAO_SOCIAL;
    public $mCO_ATIVIDADE_ECONOMICA;
    public $mNU_CNPJ;
    public $mNO_ENDERECO;
    public $mNO_COMPLEMENTO_ENDERECO;
    public $mNO_BAIRRO;
    public $mNO_MUNICIPIO;
    public $mCO_UF;
    public $mNU_CEP;
    public $mNU_DDD;
    public $mNU_TELEFONE;
    public $mNO_EMAIL_EMPRESA;
    public $mNO_CONTATO_PRINCIPAL;
    public $mNO_EMAIL_CONTATO_PRINCIPAL;
    public $mTE_OBJETO_SOCIAL;
    public $mVA_CAPITAL_ATUAL;
    public $mDT_CONSTITUICAO_EMPRESA;
    public $mDT_ALTERACAO_CONTRATUAL;
    public $mNO_EMPRESA_ESTRANGEIRA;
    public $mVA_INVESTIMENTO_ESTRANGEIRO;
    public $mDT_INVESTIMENTO_ESTRANGEIRO;
    public $mQT_EMPREGADOS;
    public $mQT_EMPREGADOS_ESTRANGEIROS;
    public $mDT_CADASTRAMENTO;
    public $mCO_USUARIO_CADASTRAMENTO;
    public $mVA_CAPITAL_INICIAL;
    public $mNO_ADMINISTRADOR;
    public $mCO_CARGO_ADMIN;
    public $mEH_PETROBRAS;
    public $mNM_LOGO_EMPRESA;
    public $mDT_CADASTRO_BC;
    public $mNM_SENHA;
    public $mFL_ATIVA;
    public $mtbpc_id;
    public $musua_id_responsavel;
    public $musua_id_responsavel2;
    public $musua_id_responsavel3;
    public $musua_id_responsavel4;
    public $musua_id_responsavel5;
    public $mfl_exclusiva_cobranca;
    public $mempr_fl_fatura_em_ingles;
    public $mempr_fl_faturavel;
    public $mempr_fl_tem_embarcacao;
    // Endereco de cobranca
    public $mempr_tx_razao_social_cobranca;
    public $mempr_tx_cnpj_cobranca;
    public $mempr_tx_endereco_cobranca;
    public $mempr_tx_complemento_endereco_cobranca;
    public $mempr_tx_bairro_cobranca;
    public $mempr_tx_municipio_cobranca;
    public $mempr_tx_cep_cobranca;
    public $mco_uf_cobranca;
    public $mempr_tx_ddd_cobranca;
    public $mempr_tx_telefone_cobranca;
    public $mempr_tx_email_cobranca;
    public $mempr_tx_contato_cobranca;
    public $mempr_tx_log_alteracoes;
    public $mcfgr_id;

    public $mnu_empresa_prestadora;

    private $mpontosfocais;

    protected $mponto_focal;
    protected $mgestor;

    public function getPontoFocal(){
        if (!is_object($this->mponto_focal)){
            if ($this->musua_id_responsavel > 0){
                $this->mponto_focal = new cusuarios;
                $this->mponto_focal->RecuperePeloId($this->musua_id_responsavel);
            } else {
                $this->mponto_focal = null;
            }
        }
        return $this->mponto_focal;

    }

    public function getGestor(){
        if (!is_object($this->mgestor)){
            if ($this->musua_id_responsavel2 > 0){
                $this->mgestor = new cusuarios;
                $this->mgestor->RecuperePeloId($this->musua_id_responsavel2);
            } else {
                $this->mgestor = null;
            }
        }
        return $this->mgestor;

    }

    public static function get_nomeCampoChave(){
        return 'nu_empresa';
    }
    public static function get_nomeCampoDescricao(){
        return 'no_razao_social';
    }
    public static function get_nomeTabelaBD(){
        return 'empresa';
    }

    public function __construct() {
        $this->mEH_PETROBRAS = '0';
    }

    public function setid($pid) {
        $this->mNU_EMPRESA = $pid;
    }

    public function getid() {
        return $this->mNU_EMPRESA;
    }

    public function sql_Liste() {
        $sql = "select * from EMPRESA where 1=1 ";
        return $sql;
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from EMPRESA where NU_EMPRESA = " . $this->mNU_EMPRESA;
        return $sql;
    }

    public function ConsistirSempre() {
        if ($this->mEH_PETROBRAS == '') {
            $this->mEH_PETROBRAS = 0;
        }
    }

    public function Incluir() {
        if ($this->mEH_PETROBRAS == '') {
            $this->mEH_PETROBRAS = 0;
        }
        if ($this->mNM_SENHA == '') {
            $this->mNM_SENHA = 'mundivisas';
        }
        $sql = "INSERT INTO EMPRESA (";
        $sql .= "	 NO_RAZAO_SOCIAL";
        $sql .= "	,CO_ATIVIDADE_ECONOMICA";
        $sql .= "	,NU_CNPJ";
        $sql .= "	,NO_ENDERECO";
        $sql .= "	,NO_COMPLEMENTO_ENDERECO";
        $sql .= "	,NO_BAIRRO";
        $sql .= "	,NO_MUNICIPIO";
        $sql .= "	,CO_UF";
        $sql .= "	,NU_CEP";
        $sql .= "	,NU_DDD";
        $sql .= "	,NU_TELEFONE";
        $sql .= "	,NO_EMAIL_EMPRESA";
        $sql .= "	,NO_CONTATO_PRINCIPAL";
        $sql .= "	,NO_EMAIL_CONTATO_PRINCIPAL";
        $sql .= "	,TE_OBJETO_SOCIAL";
        $sql .= "	,VA_CAPITAL_ATUAL";
        $sql .= "	,DT_CONSTITUICAO_EMPRESA";
        $sql .= "	,DT_ALTERACAO_CONTRATUAL";
        $sql .= "	,NO_EMPRESA_ESTRANGEIRA";
        $sql .= "	,VA_INVESTIMENTO_ESTRANGEIRO";
        $sql .= "	,DT_INVESTIMENTO_ESTRANGEIRO";
        $sql .= "	,QT_EMPREGADOS";
        $sql .= "	,QT_EMPREGADOS_ESTRANGEIROS";
        $sql .= "	,DT_CADASTRAMENTO";
        $sql .= "	,CO_USUARIO_CADASTRAMENTO";
        $sql .= "	,VA_CAPITAL_INICIAL";
        $sql .= "	,NO_ADMINISTRADOR";
        $sql .= "	,CO_CARGO_ADMIN";
        $sql .= "	,EH_PETROBRAS";
        $sql .= "	,NM_LOGO_EMPRESA";
        $sql .= "	,DT_CADASTRO_BC";
        $sql .= "	,NM_SENHA";
        $sql .= "	,FL_ATIVA";
        $sql .= "	,tbpc_id";
        $sql .= "	,usua_id_responsavel";
        $sql .= "	,usua_id_responsavel2";
        $sql .= "	,usua_id_responsavel3";
        $sql .= "	,usua_id_responsavel4";
        $sql .= "	,usua_id_responsavel5";
        $sql .= "	,fl_exclusiva_cobranca";
        $sql .= "	,empr_fl_fatura_em_ingles";
        $sql .= "	,empr_fl_faturavel";
        $sql .= "	,nu_empresa_prestadora";
        $sql .= "   ,cfgr_id";
        $sql .= ") values (";
        $sql .= "	 " . cBANCO::StringOk($this->mNO_RAZAO_SOCIAL);
        $sql .= "	," . cBANCO::StringOk($this->mCO_ATIVIDADE_ECONOMICA);
        $sql .= "	," . cBANCO::StringOk($this->mNU_CNPJ);
        $sql .= "	," . cBANCO::StringOk($this->mNO_ENDERECO);
        $sql .= "	," . cBANCO::StringOk($this->mNO_COMPLEMENTO_ENDERECO);
        $sql .= "	," . cBANCO::StringOk($this->mNO_BAIRRO);
        $sql .= "	," . cBANCO::StringOk($this->mNO_MUNICIPIO);
        $sql .= "	," . cBANCO::StringOk($this->mCO_UF);
        $sql .= "	," . cBANCO::StringOk($this->mNU_CEP);
        $sql .= "	," . cBANCO::StringOk($this->mNU_DDD);
        $sql .= "	," . cBANCO::StringOk($this->mNU_TELEFONE);
        $sql .= "	," . cBANCO::StringOk($this->mNO_EMAIL_EMPRESA);
        $sql .= "	," . cBANCO::StringOk($this->mNO_CONTATO_PRINCIPAL);
        $sql .= "	," . cBANCO::StringOk($this->mNO_EMAIL_CONTATO_PRINCIPAL);
        $sql .= "	," . cBANCO::StringOk($this->mTE_OBJETO_SOCIAL);
        $sql .= "	," . cBANCO::StringOk($this->mVA_CAPITAL_ATUAL);
        $sql .= "	," . cBANCO::DataOk($this->mDT_CONSTITUICAO_EMPRESA);
        $sql .= "	," . cBANCO::DataOk($this->mDT_ALTERACAO_CONTRATUAL);
        $sql .= "	," . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA);
        $sql .= "	," . cBANCO::StringOk($this->mVA_INVESTIMENTO_ESTRANGEIRO);
        $sql .= "	," . cBANCO::DataOk($this->mDT_INVESTIMENTO_ESTRANGEIRO);
        $sql .= "	," . cBANCO::InteiroOk($this->mQT_EMPREGADOS);
        $sql .= "	," . cBANCO::InteiroOk($this->mQT_EMPREGADOS_ESTRANGEIROS);
        $sql .= "	," . cBANCO::DataOk($this->mDT_CADASTRAMENTO);
        $sql .= "	," . cBANCO::StringOk($this->mCO_USUARIO_CADASTRAMENTO);
        $sql .= "	," . cBANCO::StringOk($this->mVA_CAPITAL_INICIAL);
        $sql .= "	," . cBANCO::StringOk($this->mNO_ADMINISTRADOR);
        $sql .= "	," . cBANCO::StringOk($this->mCO_CARGO_ADMIN);
        $sql .= "	," . cBANCO::SimNaoOk($this->mEH_PETROBRAS);
        $sql .= "	," . cBANCO::StringOk($this->mNM_LOGO_EMPRESA);
        $sql .= "	," . cBANCO::DataOk($this->mDT_CADASTRO_BC);
        $sql .= "	," . cBANCO::StringOk($this->mNM_SENHA);
        $sql .= "	," . cBANCO::StringOk($this->mFL_ATIVA);
        $sql .= "	," . cBANCO::ChaveOk($this->mtbpc_id);
        $sql .= "	," . cBANCO::ChaveOk($this->musua_id_responsavel);
        $sql .= "	," . cBANCO::ChaveOk($this->musua_id_responsavel2);
        $sql .= "	," . cBANCO::ChaveOk($this->musua_id_responsavel3);
        $sql .= "	," . cBANCO::ChaveOk($this->musua_id_responsavel4);
        $sql .= "	," . cBANCO::ChaveOk($this->musua_id_responsavel5);
        $sql .= "	," . cBANCO::SimNaoOk($this->mfl_exclusiva_cobranca);
        $sql .= "	," . cBANCO::SimNaoOk($this->mempr_fl_fatura_em_ingles);
        $sql .= "	," . cBANCO::SimNaoOk($this->mempr_fl_faturavel);
        $sql .= "	," . cBANCO::ChaveOk($this->mnu_empresa_prestadora);
        $sql .= "   ," . cBANCO::ChaveOk($this->mcfgr_id);
        $sql .= ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->setid(cAMBIENTE::$db_pdo->lastInsertId());
    }

    public function Atualizar() {
        $sql = "update EMPRESA set ";
        $sql .= "   NO_RAZAO_SOCIAL			= " . cBANCO::StringOk($this->mNO_RAZAO_SOCIAL);
        $sql .= "  ,CO_ATIVIDADE_ECONOMICA			= " . cBANCO::ChaveOk($this->mCO_ATIVIDADE_ECONOMICA);
        $sql .= "  ,NU_CNPJ			= " . cBANCO::StringOk($this->mNU_CNPJ);
        $sql .= "  ,NO_ENDERECO			= " . cBANCO::StringOk($this->mNO_ENDERECO);
        $sql .= "  ,NO_COMPLEMENTO_ENDERECO			= " . cBANCO::StringOk($this->mNO_COMPLEMENTO_ENDERECO);
        $sql .= "  ,NO_BAIRRO			= " . cBANCO::StringOk($this->mNO_BAIRRO);
        $sql .= "  ,NO_MUNICIPIO			= " . cBANCO::StringOk($this->mNO_MUNICIPIO);
        $sql .= "  ,CO_UF			= " . cBANCO::StringOk($this->mCO_UF);
        $sql .= "  ,NU_CEP			= " . cBANCO::StringOk($this->mNU_CEP);
        $sql .= "  ,NU_DDD			= " . cBANCO::StringOk($this->mNU_DDD);
        $sql .= "  ,NU_TELEFONE			= " . cBANCO::StringOk($this->mNU_TELEFONE);
        $sql .= "  ,NO_EMAIL_EMPRESA			= " . cBANCO::StringOk($this->mNO_EMAIL_EMPRESA);
        $sql .= "  ,NO_CONTATO_PRINCIPAL			= " . cBANCO::StringOk($this->mNO_CONTATO_PRINCIPAL);
        $sql .= "  ,NO_EMAIL_CONTATO_PRINCIPAL			= " . cBANCO::StringOk($this->mNO_EMAIL_CONTATO_PRINCIPAL);
        $sql .= "  ,TE_OBJETO_SOCIAL			= " . cBANCO::StringOk($this->mTE_OBJETO_SOCIAL);
        $sql .= "  ,VA_CAPITAL_ATUAL			= " . cBANCO::StringOk($this->mVA_CAPITAL_ATUAL);
        $sql .= "  ,DT_CONSTITUICAO_EMPRESA			= " . cBANCO::DataOk($this->mDT_CONSTITUICAO_EMPRESA);
        $sql .= "  ,DT_ALTERACAO_CONTRATUAL			= " . cBANCO::DataOk($this->mDT_ALTERACAO_CONTRATUAL);
        $sql .= "  ,NO_EMPRESA_ESTRANGEIRA			= " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA);
        $sql .= "  ,VA_INVESTIMENTO_ESTRANGEIRO			= " . cBANCO::StringOk($this->mVA_INVESTIMENTO_ESTRANGEIRO);
        $sql .= "  ,DT_INVESTIMENTO_ESTRANGEIRO			= " . cBANCO::DataOk($this->mDT_INVESTIMENTO_ESTRANGEIRO);
        $sql .= "  ,QT_EMPREGADOS			= " . cBANCO::InteiroOk($this->mQT_EMPREGADOS);
        $sql .= "  ,QT_EMPREGADOS_ESTRANGEIROS			= " . cBANCO::InteiroOk($this->mQT_EMPREGADOS_ESTRANGEIROS);
        $sql .= "  ,DT_CADASTRAMENTO			= " . cBANCO::DataOk($this->mDT_CADASTRAMENTO);
        $sql .= "  ,CO_USUARIO_CADASTRAMENTO			= " . cBANCO::StringOk($this->mCO_USUARIO_CADASTRAMENTO);
        $sql .= "  ,VA_CAPITAL_INICIAL			= " . cBANCO::StringOk($this->mVA_CAPITAL_INICIAL);
        $sql .= "  ,NO_ADMINISTRADOR			= " . cBANCO::StringOk($this->mNO_ADMINISTRADOR);
        $sql .= "  ,CO_CARGO_ADMIN			= " . cBANCO::StringOk($this->mCO_CARGO_ADMIN);
        $sql .= "  ,EH_PETROBRAS			= " . cBANCO::SimNaoOk($this->mEH_PETROBRAS);
        $sql .= "  ,NM_LOGO_EMPRESA			= " . cBANCO::StringOk($this->mNM_LOGO_EMPRESA);
        $sql .= "  ,DT_CADASTRO_BC			= " . cBANCO::DataOk($this->mDT_CADASTRO_BC);
        $sql .= "  ,NM_SENHA			= " . cBANCO::StringOk($this->mNM_SENHA);
        $sql .= "  ,FL_ATIVA			= " . cBANCO::SimNaoOk($this->mFL_ATIVA);
        $sql .= "  ,tbpc_id			= " . cBANCO::ChaveOk($this->mtbpc_id);
        $sql .= "  ,usua_id_responsavel	= " . cBANCO::ChaveOk($this->musua_id_responsavel);
        $sql .= "  ,usua_id_responsavel2	= " . cBANCO::ChaveOk($this->musua_id_responsavel2);
        $sql .= "  ,usua_id_responsavel3	= " . cBANCO::ChaveOk($this->musua_id_responsavel3);
        $sql .= "  ,usua_id_responsavel4	= " . cBANCO::ChaveOk($this->musua_id_responsavel4);
        $sql .= "  ,usua_id_responsavel5	= " . cBANCO::ChaveOk($this->musua_id_responsavel5);
        $sql .= "  ,fl_exclusiva_cobranca   = " . cBANCO::SimNaoOk($this->mfl_exclusiva_cobranca);
        $sql .= "  ,empr_fl_fatura_em_ingles   = " . cBANCO::SimNaoOk($this->mempr_fl_fatura_em_ingles);
        $sql .= "  ,empr_fl_faturavel   = " . cBANCO::SimNaoOk($this->mempr_fl_faturavel);
        $sql .= "  ,nu_empresa_prestadora  = " . cBANCO::ChaveOk($this->mnu_empresa_prestadora);
        $sql .= "  ,cfgr_id  = " . cBANCO::ChaveOk($this->mcfgr_id);
        $sql .= " where NU_EMPRESA = " . $this->mNU_EMPRESA;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function AtualizarEnderecoCobranca() {
        $sql = "update EMPRESA set ";
        $sql .= "   empr_tx_razao_social_cobranca			= " . cBANCO::StringOk($this->mempr_tx_razao_social_cobranca);
        $sql .= "  ,empr_tx_cnpj_cobranca					= " . cBANCO::StringOk($this->mempr_tx_cnpj_cobranca);
        $sql .= "  ,empr_tx_endereco_cobranca 				= " . cBANCO::StringOk($this->mempr_tx_endereco_cobranca);
        $sql .= "  ,empr_tx_complemento_endereco_cobranca	= " . cBANCO::StringOk($this->mempr_tx_complemento_endereco_cobranca);
        $sql .= "  ,empr_tx_bairro_cobranca					= " . cBANCO::StringOk($this->mempr_tx_bairro_cobranca);
        $sql .= "  ,empr_tx_municipio_cobranca				= " . cBANCO::StringOk($this->mempr_tx_municipio_cobranca);
        $sql .= "  ,empr_tx_cep_cobranca					= " . cBANCO::StringOk($this->mempr_tx_cep_cobranca);
        $sql .= "  ,co_uf_cobranca							= " . cBANCO::StringOk($this->mco_uf_cobranca);
        $sql .= "  ,empr_tx_ddd_cobranca 					= " . cBANCO::StringOk($this->mempr_tx_ddd_cobranca);
        $sql .= "  ,empr_tx_telefone_cobranca 				= " . cBANCO::StringOk($this->mempr_tx_telefone_cobranca);
        $sql .= "  ,empr_tx_email_cobranca					= " . cBANCO::StringOk($this->mempr_tx_email_cobranca);
        $sql .= "  ,empr_tx_contato_cobranca				= " . cBANCO::StringOk($this->mempr_tx_contato_cobranca);
        $sql .= "  ,tbpc_id									= " . cBANCO::ChaveOk($this->mtbpc_id);
        $sql .= " where NU_EMPRESA = " . $this->mNU_EMPRESA;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function RecuperarEnderecoCobranca() {
        if ($this->mNU_EMPRESA == '') {
            throw new cERRO_CONSISTENCIA('Chave da empresa não informada');
        }
        $sql = "select
					 empr_tx_razao_social_cobranca
					,empr_tx_cnpj_cobranca
					,empr_tx_endereco_cobranca
					,empr_tx_complemento_endereco_cobranca
					,empr_tx_bairro_cobranca
					,empr_tx_municipio_cobranca
					,empr_tx_cep_cobranca
					,co_uf_cobranca
					,empr_tx_ddd_cobranca
					,empr_tx_telefone_cobranca
					,empr_tx_email_cobranca
					,empr_tx_contato_cobranca
					,tbpc_id
				from empresa
				where NU_EMPRESA = " . $this->mNU_EMPRESA;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        cBANCO::CarreguePropriedades($rs, $this);
    }

    public function getPontosFocais() {
        if (!is_array($this->mpontosfocais)) {
            $this->mpontosfocais = array();
            $usuario = new cusuarios();
            $usuario->RecuperePontoFocalEmpresa($this->mNU_EMPRESA, "");
            if ($usuario->cd_usuario != '') {
                $this->mpontosfocais[] = $usuario;
            }
            $usuario = new cusuarios();
            $usuario->RecuperePontoFocalEmpresa($this->mNU_EMPRESA, "2");
            if ($usuario->cd_usuario != '') {
                $this->mpontosfocais[] = $usuario;
            }
            $usuario = new cusuarios();
            $usuario->RecuperePontoFocalEmpresa($this->mNU_EMPRESA, "3");
            if ($usuario->cd_usuario != '') {
                $this->mpontosfocais[] = $usuario;
            }
            $usuario = new cusuarios();
            $usuario->RecuperePontoFocalEmpresa($this->mNU_EMPRESA, "4");
            if ($usuario->cd_usuario != '') {
                $this->mpontosfocais[] = $usuario;
            }
            $usuario = new cusuarios();
            $usuario->RecuperePontoFocalEmpresa($this->mNU_EMPRESA, "5");
            if ($usuario->cd_usuario != '') {
                $this->mpontosfocais[] = $usuario;
            }
        }
        return $this->mpontosfocais;
    }


    public static function CursorCompleto($pnu_empresa) {
        $sql = "select e.*
				     , fc.no_funcao
				from empresa e
				left join funcao_cargo fc on fc.co_funcao = e.co_cargo_admin
			   where e.nu_empresa = " . $pnu_empresa;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public function Cursor_Associadas() {
        $sql = "select a.* , NO_NACIONALIDADE
				from associadas a
				left join pais_nacionalidade p on p.co_pais = a.co_nacionalidade
				where nu_empresa = " . $this->mNU_EMPRESA;
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    public function qtd_Associadas() {
        $sql = "select ifnull(count(*),0) qtd
				from associadas a
				where nu_empresa = " . $this->mNU_EMPRESA;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        return $rs['qtd'];
    }

    public function Cursor_Administradores() {
        $sql = "select NM_ADMIN, NM_CARGO
				from admin_petrobras
				where co_empresa =" . $this->mNU_EMPRESA . "
				  and TP_CARGO = 'A'
				  order by TP_CARGO, CO_ADMIN";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public function qtd_Administradores() {
        $sql = "select ifnull(count(*),0) qtd
				from admin_petrobras
				where co_empresa =" . $this->mNU_EMPRESA . "
				  and TP_CARGO = 'A'";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        return $rs['qtd'];
    }

    public function Cursor_Diretores() {
        $sql = "select NM_ADMIN, NM_CARGO
				from admin_petrobras
				where co_empresa =" . $this->mNU_EMPRESA . "
				  and TP_CARGO = 'D'
				  order by TP_CARGO, CO_ADMIN";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public function qtd_Diretores() {
        $sql = "select ifnull(count(*),0) qtd
				from admin_petrobras
				where co_empresa =" . $this->mNU_EMPRESA . "
				  and TP_CARGO = 'D'";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        return $rs['qtd'];
    }

    public function Cursor_RepresentantesFuncionarios() {
        $sql = "select NM_ADMIN, NM_CARGO
				from admin_petrobras
				where co_empresa =" . $this->mNU_EMPRESA . "
				  and TP_CARGO = 'R'
				  order by TP_CARGO, CO_ADMIN";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public function qtd_RepresentantesFuncionarios() {
        $sql = "select ifnull(count(*),0) qtd
				from admin_petrobras
				where co_empresa =" . $this->mNU_EMPRESA . "
				  and TP_CARGO = 'R'";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        return $rs['qtd'];
    }

    public static function ativasComPreco($default = '(selecione...)', $nu_empresa = ''){
        $sql = "select nu_empresa chave, no_razao_social descricao "
                . " from empresa "
                . " where (FL_ATIVA = 1"
                . " and coalesce(fl_exclusiva_cobranca,0) = 0 "
                . " and tbpc_id > 0 )";
        if ($nu_empresa != ''){
            $sql .= " or nu_empresa = ".$nu_empresa;
        }
        $sql .= " order by no_razao_social";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        if ($default != ''){
            $rs = array(array('chave' => 0, 'descricao' => $default)) + $rs;
        }
        return $rs;
    }

    public static function deCobranca($default = "(selecione...)", $nu_empresa = ''){
        $sql = "select nu_empresa chave, no_razao_social descricao "
                . " from empresa "
                . " where FL_ATIVA = 1";
        if ($nu_empresa != ''){
            $sql .= " or nu_empresa = ".$nu_empresa;
        }
        $sql .= " order by no_razao_social";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        if ($default != ''){
            $rs = array(array('chave' => 0, 'descricao' => $default)) + $rs;
        }
        return $rs;
    }
}
