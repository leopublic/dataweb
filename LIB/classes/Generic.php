<?php

class Generic extends html{
    

    public $filtro = null;

        function strDs($str){
           return str_replace(DS, DS.DS, $str);
        }
     
     
        function dataMysql($strdata){
            
           $d = explode('/',$strdata);
            
            return $d[2].'-'.$d[1].'-'.$d[0];
            
        }
        
        function dataBr($strdata){
           $res = $strdata;
           if(strpos($strdata,'-')>0){
            $d = explode('-',$strdata);
            $res = $d[2].'/'.$d[1].'/'.$d[0];
           }
            return $res;
        }
        
        function datetimeBr($datetime){
               
              $res = ''; 
               
               if(strpos($datetime,' ')>0){
                
                    $dt = explode(' ', $datetime);
                    
                    $data = $dt[0];
                    $hora = $dt[1];
                    
                    $data = $this->dataBr($data);
                    
                    
                    $res = $data.' '.$hora;
                    
                    
                
                
               }
               

                return $res;            
        }
        
        

 
        function combo_tiposervico($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
                        
            $sql    = new sql;
            $h      = $sql->execute("SELECT * FROM TIPO_SERVICO ORDER BY NU_TIPO_SERVICO DESC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="NU_TIPO_SERVICO" id="NU_TIPO_SERVICO">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['NU_TIPO_SERVICO']){
                    $opt .= '<option selected value="'.$r['NU_TIPO_SERVICO'].'">'.$r['NO_TIPO_SERVICO'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['NU_TIPO_SERVICO'].'">'.$r['NO_TIPO_SERVICO'].'</option>';
                }
              }
              $se_dois = '</select>';
              
              $combo = $se_um.$opt.$se_dois;
            }
            
            return $combo;
        }

        function combo_tipoacompanhamento($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
            $sql    = new sql;
            $h      = $sql->execute("SELECT * FROM TIPO_ACOMPANHAMENTO ORDER BY ID_TIPO_ACOMPANHAMENTO DESC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="ID_TIPO_ACOMPANHAMENTO" id="ID_TIPO_ACOMPANHAMENTO">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['ID_TIPO_ACOMPANHAMENTO']){
                    $opt .= '<option selected value="'.$r['ID_TIPO_ACOMPANHAMENTO'].'">'.$r['NO_TIPO_ACOMPANHAMENTO'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['ID_TIPO_ACOMPANHAMENTO'].'">'.$r['NO_TIPO_ACOMPANHAMENTO'].'</option>';
                }
              }
              $se_dois = '</select>';
              
              $combo = $se_um.$opt.$se_dois;
            }
            
            return $combo;
        }

        function combo_tipoautorizacao($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
            $sql    = new sql;
            $h      = $sql->execute("SELECT * FROM TIPO_AUTORIZACAO ORDER BY NO_TIPO_AUTORIZACAO DESC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="CO_TIPO_AUTORIZACAO" id="CO_TIPO_AUTORIZACAO">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['CO_TIPO_AUTORIZACAO']){
                    $opt .= '<option selected value="'.$r['CO_TIPO_AUTORIZACAO'].'">'.$r['NO_TIPO_AUTORIZACAO'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['CO_TIPO_AUTORIZACAO'].'">'.$r['NO_TIPO_AUTORIZACAO'].'</option>';
                }
              }
              $se_dois = '</select>';
              
              $combo = $se_um.$opt.$se_dois;
            }
            
            return $combo;
        }

        

        
        function combo_grauparentesco($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
            $sql    = new sql;
            $h      = $sql->execute("SELECT * FROM GRAU_PARENTESCO ORDER BY CO_GRAU_PARENTESCO DESC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="CO_GRAU_PARENTESCO" id="CO_GRAU_PARENTESCO">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['CO_GRAU_PARENTESCO']){
                    $opt .= '<option selected value="'.$r['CO_GRAU_PARENTESCO'].'">'.$r['NO_GRAU_PARENTESCO'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['CO_GRAU_PARENTESCO'].'">'.$r['NO_GRAU_PARENTESCO'].'</option>';
                }
              }
              $se_dois = '</select>';

              $combo = $se_um.$opt.$se_dois;
            }

            return $combo;
        }


        function combo_nacionalidade($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
            $sql    = new sql;
            $h      = $sql->execute("SELECT * FROM PAIS_NACIONALIDADE ORDER BY NO_NACIONALIDADE ASC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="CO_NACIONALIDADE" id="CO_NACIONALIDADE">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['CO_PAIS']){
                    $opt .= '<option selected value="'.$r['CO_PAIS'].'">'.$r['NO_NACIONALIDADE'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['CO_PAIS'].'">'.$r['NO_NACIONALIDADE'].'</option>';
                }
              }
              $se_dois = '</select>';

              $combo = $se_um.$opt.$se_dois;
            }

            return $combo;
        }


        function combo_pais($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
            $sql    = new sql;
            $h      = $sql->execute("SELECT * FROM PAIS_NACIONALIDADE ORDER BY NO_NACIONALIDADE ASC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="CO_PAIS" id="CO_PAIS">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['CO_PAIS']){
                    $opt .= '<option selected value="'.$r['CO_PAIS'].'">'.$r['NO_PAIS'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['CO_PAIS'].'">'.$r['NO_PAIS'].'</option>';
                }
              }
              $se_dois = '</select>';

              $combo = $se_um.$opt.$se_dois;
            }

            return $combo;
        }
        
         function combo_empresa($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
            $sql    = new sql;
            $h      = $sql->execute("SELECT NU_EMPRESA, NO_RAZAO_SOCIAL FROM EMPRESA ORDER BY NO_RAZAO_SOCIAL ASC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="NU_EMPRESA" id="NU_EMPRESA" style="width:300px">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['NU_EMPRESA']){
                    $opt .= '<option selected value="'.$r['NU_EMPRESA'].'">'.$r['NO_RAZAO_SOCIAL'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['NU_EMPRESA'].'">'.$r['NO_RAZAO_SOCIAL'].'</option>';
                }
              }
              $se_dois = '</select>';

              $combo = $se_um.$opt.$se_dois;
            }

            return $combo;
        } 
        
        
       
        
              
        
        function combo_perfil($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
            $sql    = new sql;
            $h      = $sql->execute("SELECT cd_perfil, nm_perfil FROM usuario_perfil ORDER BY nm_perfil ASC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="cd_perfil" id="cd_perfil" style="width:300px">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['cd_perfil']){
                    $opt .= '<option selected value="'.$r['cd_perfil'].'">'.$r['nm_perfil'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['cd_perfil'].'">'.$r['nm_perfil'].'</option>';
                }
              }
              $se_dois = '</select>';

              $combo = $se_um.$opt.$se_dois;
            }

            return $combo;
        } 
        
        
        function combo_superior($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
            $sql    = new sql;
            $h      = $sql->execute("SELECT cd_usuario, nome FROM usuarios ORDER BY nome ASC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="cd_superior" id="cd_superior" style="width:300px">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['cd_usuario']){
                    $opt .= '<option selected value="'.$r['cd_usuario'].'">'.$r['nome'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['cd_usuario'].'">'.$r['nome'].'</option>';
                }
              }
              $se_dois = '</select>';

              $combo = $se_um.$opt.$se_dois;
            }

            return $combo;
        }
        
        function combo_funcao($checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked;
            }
            $sql    = new sql;
            $h      = $sql->execute("SELECT CO_FUNCAO, NO_FUNCAO FROM FUNCAO_CARGO ORDER BY NO_FUNCAO ASC ");
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="CO_FUNCAO" id="CO_FUNCAO" style="width:300px">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['CO_FUNCAO']){
                    $opt .= '<option selected value="'.$r['CO_FUNCAO'].'">'.$r['NO_FUNCAO'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['CO_FUNCAO'].'">'.$r['NO_FUNCAO'].'</option>';
                }
              }
              $se_dois = '</select>';

              $combo = $se_um.$opt.$se_dois;
            }

            return $combo;
        }        
        
        
         
        
        
         function combo_embarcacaoempresa($NU_EMPRESA, $checked = null){
            $codgrupochecked = '';
            if($checked<>''){
                $codgrupochecked = $checked; 
            }
            
            $h      = sql::query("select NU_EMBARCACAO_PROJETO,NO_EMBARCACAO_PROJETO from EMBARCACAO_PROJETO where NU_EMPRESA= ".$NU_EMPRESA);
            $combo  = '';
            if($h->count()>0){
              $se_um = '<select NAME="NU_EMBARCACAO" id="NU_EMBARCACAO" style="width:300px">'; $opt ='';
              $opt .= '<option selected value="">----</option>';
              while($r=$h->fetch()){
                if($codgrupochecked == $r['NU_EMBARCACAO_PROJETO']){
                    $opt .= '<option selected value="'.$r['NU_EMBARCACAO_PROJETO'].'">'.$r['NO_EMBARCACAO_PROJETO'].'</option>';
                }else{
                    $opt .= '<option value="'.$r['NU_EMBARCACAO_PROJETO'].'">'.$r['NO_EMBARCACAO_PROJETO'].'</option>';
                }
              }
              $se_dois = '</select>';

              $combo = $se_um.$opt.$se_dois;
            }

            return $combo;
        }        
        
        
        
        function getCampo($tabela, $nomecampo, $campochave, $chave){
            
           $r = sql::query ("SELECT $nomecampo from $tabela where $campochave = $chave")->fetch();
           return $r[$nomecampo];
        }
                       
        

        function strxml($str){

          $str  = str_replace('&','',$str);
          $str  = str_replace('"','',$str);
          $str  = str_replace("'","",$str);
          $str  = str_replace('<','',$str);
          $str  = str_replace('>','',$str);

          return $str;
        }


        function fk($t){
            if($t == ''){
                $t = 'null';
            }
            return $t;
        }


     function SetFiltro($post){

        $this->filtro = $post;
        
    }
    
    
    function TitulosDinamicos(){
        
        $rows = null;
        $post = $this->filtro;
        
        foreach($post as $p => $k){
            $rows .= '<th>'.$k.'</th>';

        }     
        
        return $rows;    
    }
    
    function ColunasDinamicas($row){
        
        $rows = null;
        $post = $this->filtro;

        foreach($post as $p => $k){
        	if ($p == 'NU_PROCESSO_MTE' || $p == 'NU_PASSAPORTE' || $p == 'NU_PROTOCOLO_REG' || $p == 'NU_PROTOCOLO_PRO' ){
				$style = ' style="mso-number-format:\@;"';    		
        	}
        	else{
        		$style = '';
        	}
        	
               $rows .= '<td'.$style.'>'.$row[$p].'</td>';
        } 
        
       return $rows; 
    }
    
    
    function str_basename($str){
        
        $str = str_replace('/','', $str);
        $str = str_replace(' ','_', $str);
        
        return $str;
    }
           


    function dolarToReal($dolar){
        
        $dolar   = $this->moedaToDecimal($dolar);
        
        $cotacao = $this->getCampo('configuracao', 'valor', 'codconfiguracao', 2);
        
        $res = $dolar * $cotacao;
        
        $res = round($res * 100) / 100;     //arredondamento ..
        
        return $this->decimalToReal($res);
        
    }
    
    
    function decimalToReal($decimal){
        
        return number_format($decimal, 2, ',', '.');
    }
    
    function moeda($d){
       return number_format($d, 2, ',', '.'); 
    }
    
    
    function moedaAmericana($d){
           
       return number_format($d, 2, '.', ','); 
        
    }  
    
    
    function realToDecimal($real){
        
       $real = str_replace('.','', $real);
       $real = str_replace(',','.', $real);
       
       return $real;
    }
    
    function moedaToDecimal($real){
        
       $real = str_replace('.','', $real);
       $real = str_replace(',','.', $real);
       

       
       return $real;
    }
    
    function decimalToNumeric($decimal){
        
        return  str_replace('.','', $decimal);        
    }





}
