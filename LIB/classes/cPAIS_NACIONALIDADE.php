<?php

class cPAIS_NACIONALIDADE extends cMODELO {

    public $mCO_PAIS;
    public $mNO_PAIS;
    public $mNO_PAIS_EM_INGLES;
    public $mNO_NACIONALIDADE;
    public $mNO_NACIONALIDADE_EM_INGLES;
    public $mCO_PAIS_PF;
    public $mNO_NACIONALIDADE_ALTERNATIVO;

    public static function get_nomeCampoChave(){
        return 'co_pais';
    }
    public static function get_nomeCampoDescricao(){
        return "concat(no_nacionalidade, ' (', no_pais, ')')";
    }
    public static function get_nomeTabelaBD(){
        return 'PAIS_NACIONALIDADE';
    }

    public function get_nomeTabela() {
        return 'pais_nacionalidade';
    }

    public function getId() {
        return $this->mCO_PAIS;
    }

    public function setId($pValor) {
        $this->mCO_PAIS = $pValor;
    }

    public function campoid() {
        return 'CO_PAIS';
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from PAIS_NACIONALIDADE where CO_PAIS = " . $this->mCO_PAIS;
        return $sql;
    }

    public function Incluir() {
        $sql = "insert into PAIS_NACIONALIDADE(
				 NO_PAIS
				,NO_PAIS_EM_INGLES
				,NO_NACIONALIDADE
				,NO_NACIONALIDADE_EM_INGLES
				,NO_NACIONALIDADE_ALTERNATIVO
				,CO_PAIS_PF
		       ) values (
				 " . cBANCO::StringOk($this->mNO_PAIS) . "
				," . cBANCO::StringOk($this->mNO_PAIS_EM_INGLES) . "
				," . cBANCO::StringOk($this->mNO_NACIONALIDADE) . "
				," . cBANCO::StringOk($this->mNO_NACIONALIDADE_EM_INGLES) . "
				," . cBANCO::StringOk($this->mNO_NACIONALIDADE_ALTERNATIVO) . "
				," . cBANCO::StringOk($this->mCO_PAIS_PF) . "
				)";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mCO_PAIS = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function Atualizar() {
        $sql = "update PAIS_NACIONALIDADE set
				 NO_PAIS = " . cBANCO::StringOk($this->mNO_PAIS) . "
				,NO_PAIS_EM_INGLES = " . cBANCO::StringOk($this->mNO_PAIS_EM_INGLES) . "
				,NO_NACIONALIDADE = " . cBANCO::StringOk($this->mNO_NACIONALIDADE) . "
				,NO_NACIONALIDADE_EM_INGLES = " . cBANCO::StringOk($this->mNO_NACIONALIDADE_EM_INGLES) . "
				,NO_NACIONALIDADE_ALTERNATIVO = " . cBANCO::StringOk($this->mNO_NACIONALIDADE_ALTERNATIVO) . "
				,CO_PAIS_PF = " . cBANCO::StringOk($this->mCO_PAIS_PF) . "
			  where CO_PAIS=" . $this->mCO_PAIS;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_Liste() {
        $sql = "select * from PAIS_NACIONALIDADE where 1=1";
        return $sql;
    }

    public function recuperePeloAlternativo($alternativo) {
        $sql = "select * "
                . " from pais_nacionalidade "
                . " where NO_NACIONALIDADE_ALTERNATIVO like '%" . $alternativo . "%' ";
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetch(PDO::FETCH_BOTH);
            cBANCO::CarreguePropriedades($rs, $this);
        }
    }

    public static function comboMustache($valor_atual = '', $default = "(selecione...)") {
        $sql = "select CO_PAIS_NACIONALIDADE valor, NO_NACIONALIDADE descricao, if(CO_PAIS_NACIONALIDADE='".$valor_atual."' ,1 ,0 ) selected from PAIS_NACIONALIDADE order by NO_NACIONALIDADE";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));

        if (intval($valor_atual == 0)){
            $rs['0']['selected'] = true;
        }

        return  $rs ;
    }

    public static function comboMustacheIngles($valor_atual = '', $default = "(selecione...)") {
        $sql = "select CO_PAIS valor, concat(NO_NACIONALIDADE_EM_INGLES, ' (', NO_PAIS_EM_INGLES, ')') descricao, if(CO_PAIS='".$valor_atual."' ,1 ,0 ) selected from PAIS_NACIONALIDADE order by NO_NACIONALIDADE_EM_INGLES";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));

        if (intval($valor_atual == 0)){
            $rs['0']['selected'] = true;
        }

        return  $rs ;
    }

    public static function comboMustachePaisIngles($valor_atual = '', $default = "(selecione...)") {
        $sql = "select CO_PAIS valor, coalesce(NO_PAIS_EM_INGLES, NO_PAIS) descricao, if(CO_PAIS='".$valor_atual."' ,1 ,0 ) selected from PAIS_NACIONALIDADE order by NO_PAIS_EM_INGLES";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));

        if (intval($valor_atual == 0)){
            $rs['0']['selected'] = true;
        }

        return  $rs ;
    }

}
