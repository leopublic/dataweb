<?php

class cdespesa extends cMODELO {

    public $mdesp_id;
    public $mnu_servico;
    public $mid_solicita_visto;
    public $mcd_usuario_cad;
    public $mdesp_tx_funcionario;
    public $mdesp_vl_valor;
    public $mdesp_vl_qtd;
    public $mdesp_dt_inicio;
    public $mdesp_dt_fim;
    public $mdesp_tx_tarefa;
    public $mdesp_tx_observacao;

    public static function get_nomeCampoChave() {
        return 'desp_id';
    }

    public static function get_nomeTabelaBD() {
        return 'despesa';
    }

    public function get_nomeTabela() {
        return self::get_nomeTabelaBD();
    }

    public function campoid() {
        return self::get_nomeCampoChave();
    }

    public function setid($pid) {
        $this->mdesp_id = $pid;
    }

    public function getid() {
        return $this->mdesp_id;
    }

    public function sql_Liste() {
        $sql = "    select d.*, timediff(desp_dt_fim, desp_dt_inicio) desp_dt_diff
			           from despesa d";
        $sql .= "    left join servico s on s.nu_servico = d.nu_servico where 1=1";
        return $sql;
    }

    public function Incluir() {
        $sql = " insert into despesa(
					nu_servico 
					,id_solicita_visto 
					,cd_usuario_cad 
					,desp_tx_funcionario 
					,desp_vl_valor
					,desp_vl_qtd
					,desp_dt_inicio
					,desp_dt_fim
					,desp_tx_tarefa
					,desp_tx_observacao
				) values (
					" . cBANCO::ChaveOk($this->mnu_servico) . "
					," . cBANCO::ChaveOk($this->mid_solicita_visto) . "
					," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario) . "
					," . cBANCO::StringOk($this->mdesp_tx_funcionario) . "
					," . cBANCO::ValorOk($this->mdesp_vl_valor) . "
					," . cBANCO::ValorOk($this->mdesp_vl_qtd) . "
					," . cBANCO::DataOk($this->mdesp_dt_inicio) . "
					," . cBANCO::DataOk($this->mdesp_dt_fim) . "
					," . cBANCO::StringOk($this->mdesp_tx_tarefa) . "
					," . cBANCO::StringOk($this->mdesp_tx_observacao) . "
					)";
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function Atualizar() {
        $sql = "update despesa set 
					nu_servico				= " . cBANCO::ChaveOk($this->mnu_servico) . "
					,desp_tx_funcionario	= " . cBANCO::StringOk($this->mdesp_tx_funcionario) . "
					,desp_vl_valor			= " . cBANCO::ValorOk($this->mdesp_vl_valor) . "
					,desp_vl_qtd			= " . cBANCO::ValorOk($this->mdesp_vl_qtd) . "
					,desp_dt_inicio			= " . cBANCO::DataOk($this->mdesp_dt_inicio) . "
					,desp_dt_fim			= " . cBANCO::DataOk($this->mdesp_dt_fim) . "
					,desp_tx_tarefa			= " . cBANCO::StringOk($this->mdesp_tx_tarefa) . "
					,desp_tx_observacao		= " . cBANCO::StringOk($this->mdesp_tx_observacao) . "
				where desp_id = " . $this->mdesp_id;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_RecuperePeloId() {
        $sql = "    select *
					   from despesa 
					  where desp_id = " . $this->mdesp_id;
        return $sql;
    }

    public function Excluir() {
        $sql = "delete from despesa where desp_id = " . $this->mdesp_id;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public static function cursorDaOS($id_solicita_visto) {
        $sql = "select despesa.*"
                . ", date_format(desp_dt_inicio, '%d/%m/%Y %H:%i:%s') desp_dt_inicio_fmt"
                . ", date_format(desp_dt_fim, '%d/%m/%Y %H:%i:%s') desp_dt_fim_fmt"
                . ", tp.nome nome_tipo "
                . " from despesa "
                . " left join tipo_despesa tp on tp.id_tipo_despesa = despesa.id_tipo_despesa"
                . " where id_solicita_visto = " . $id_solicita_visto;
        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $rs;
    }
    
    public static function novaDaOs($id_solicita_visto){
        $desp = new cdespesa();
        $desp->mid_solicita_visto = $id_solicita_visto;
        $desp->mcd_usuario_cad = cSESSAO::$mcd_usuario;
        $desp->Salvar();
        return $desp;
    }

}
