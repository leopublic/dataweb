<?php
/**
 * Entidade pai de todas as entidades que gravam no banco.
 * Centraliza todas as operações comuns a persistencia em banco
 * @author Leonardo Medeiros
 * @since 29/05/2011
 */
class cENTIDADE_PERSISTENTE {
	/**
	 * Preserva os valores originais de um registro, para efeito de gravação de log.
	 */
	public $mOriginal;
	public $mIgnorar;
	public $mChaves;

	public static function get_nomeCampoChave() {
		throw new cERRO_CONSISTENCIA("Nome do campo chave não definido para essa entidade");
	}
	public static function get_nomeTabelaBD() {
		throw new cERRO_CONSISTENCIA("Nome da tabela não definido para essa entidade");
	}


	public function ValidaCampoObrigatorio($pCampo, $pTitulo){
		if (trim($pCampo) == ''){
			return '<br>- o preenchimento d'.$pTitulo.' é obrigatório';
		}
		else{
			return '';
		}
	}
	public function getid(){

	}

	public static function AtualizeCampoPadrao($pid, $pnome_campo, $pvalor, $ptipo, $pCampoChave = '')
	{
		if($pCampoChave == ''){
			$pCampoChave = static::get_nomeCampoChave();
		}
		$sql = "update ".static::get_nomeTabelaBD()." set ";
		switch($ptipo){
			case cCAMPO::cpCHAVE_ESTR:
			case cCAMPO::cpCOMBO_GERAL:
				$sql .= $pnome_campo." = ".cBANCO::ChaveOk($pvalor);
				break;
			case cCAMPO::cpTEXTO:
			case cCAMPO::cpCODIGO:
			case cCAMPO::cpMEMO:
				$sql .= $pnome_campo." = ".cBANCO::StringOk($pvalor);
				break;
			case cCAMPO::cpDATA:
				$sql .= $pnome_campo." = ".cBANCO::DataOk($pvalor);
				break;
			case cCAMPO::cpSIMNAO:
				$sql .= $pnome_campo." = ".cBANCO::SimNaoOk($pvalor);
				break;
			case cCAMPO::cpVALOR:
			case cCAMPO::cpVALOR_DIN_DOLAR:
			case cCAMPO::cpVALOR_DIN_REAL:
				$sql .= $pnome_campo." = ".cBANCO::ValorOk($pvalor);
				break;
			default:
				throw new cERRO_CONSISTENCIA("Atualização para o tipo de campo informado (".$ptipo.") não prevista.");
		}
		$sql .= " where ".$pCampoChave." = ".$pid;
		cAMBIENTE::$db_pdo->exec($sql);
	}


    public function CarreguePropriedadesPeloRecordset($prs, $prefixo = "") {
        foreach ($prs as $campo => $valor) {
            if (property_exists($this, strtolower($campo))) {
                $this->$campo = $valor;
            } elseif (property_exists($this, $prefixo.$campo)) {
                $nome = $prefixo.$campo;
                $this->$nome = $valor;
            }
        }
    }

    public function CarreguePropriedadesDoPost($post, $prefixo = "m") {
        foreach ($post as $campo_tela => $valor) {
            $campo = substr($campo_tela, 4 );
            $campo_pref = $prefixo.$campo;
            if (property_exists($this, strtolower($campo))) {
                $this->$campo = $valor;
            } elseif (property_exists($this, $prefixo.$campo)) {
                $this->$campo_pref = $valor;
            }
        }
    }

}
