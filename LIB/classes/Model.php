<?php

/**
 * @author Fabio Pereira
 * @copyright M2Software - 2010
 */

  Class Model extends sql{
    
    public $relacao = array(), $use = array();
    
    
    
    
       function find(){
        
            $rec = array();
            while($r = $this -> fetch()){
                $rec[] = $r;
            }  
            
            return $rec;
       }
       
       
       function relacaoField($_FieldName, $id_Field, $DataSource){
        
           $field_relacional = array();
            
           if(is_array($DataSource)){
                foreach($DataSource as $Field){
                    if($Field[$_FieldName] == $id_Field){
                        $field_relacional[] = $Field;
                    }
                }     
            }

            return $field_relacional;
            
      }
    
        
  }  

?>
