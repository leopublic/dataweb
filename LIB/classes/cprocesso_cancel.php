<?php
class cprocesso_cancel
	extends cprocesso
{
	public $mnu_processo;
	public $mdt_processo;
	public $mdt_cancel;
	public $mdt_envio_bsb;
	public $mcodigo_processo_prorrog;


	public function __construct($pCodigo = 0){
		$this->mnomeTabela = 'cancel';
		if ($pCodigo > 0){
			$this->RecupereSe($pCodigo);
		}
		else{
			$this->mcodigo = 0;
		}
	}


	public static function get_nomeCampoChave() {
		return 'codigo';
	}
	public static function get_nomeTabelaBD() {
		return 'processo_cancel';
	}

	public function get_nu_processo() {
		return $this->mnu_processo;
	}

	public function RecupereSe($pCodigo = 0){
		if ($pCodigo > 0){
			$this->mcodigo = $pCodigo;
		}

		if (! $this->mcodigo > 0){
			throw new cERRO_CONSISTENCIA("Código do processo não informado");
		}
		$sql = "select processo_cancel.*, s.NO_SERVICO_RESUMIDO "
                        . " from processo_cancel"
                        . " left join servico s on s.nu_servico = processo_cancel.nu_servico"
                        . " where codigo = ".$this->mcodigo;
		if($res = mysql_query($sql)){
			if($rs = mysql_fetch_array($res)){
				$this->mcd_candidato		=$rs["cd_candidato"];
				$this->mcd_solicitacao		=$rs["cd_solicitacao"];
				$this->mnu_processo			=$rs["nu_processo"];
				$this->mdt_processo			=cBANCO::DataFmt($rs["dt_processo"]);
				$this->mdt_cancel			=cBANCO::DataFmt($rs["dt_cancel"]);
				$this->mobservacao			=$rs["observacao"];
				$this->mdt_cad				=cBANCO::DataFmt($rs["dt_cad"]);
				$this->mdt_ult				=cBANCO::DataFmt($rs["dt_ult"]);
				$this->mid_solicita_visto	=$rs["id_solicita_visto"];
				$this->mcodigo_processo_mte	=$rs["codigo_processo_mte"];
				$this->mfl_vazio			=cBANCO::SimNaoOk($rs["fl_vazio"]);
				$this->mfl_processo_atual	=cBANCO::SimNaoOk($rs["fl_processo_atual"]);
				$this->mnu_servico			=$rs['nu_servico'];
				$this->mdt_envio_bsb		=cBANCO::DataFmt($rs['dt_envio_bsb']);
                $this->mNO_SERVICO_RESUMIDO = $rs['NO_SERVICO_RESUMIDO'];
                $this->mcodigo_processo_prorrog = $rs['codigo_processo_prorrog'];
			}
			else{
				throw new Exception("Processo (".$this->mcodigo.") não encontrado");
			}
		}
	}

	public function Salve(){
		if ($this->mcodigo == 0){
			$this->IncluaNovo();
		}
		else{
			$this->Atualize();
		}
	}

	public function Salve_ProcessoEdit(){
		if ($this->mcodigo == 0){
			$this->IncluaNovo();
		}
		else{
			$this->Atualize_ProcessoEdit();
		}
	}

	private function IncluaNovo(){
		$sql = "insert into processo_cancel (";
		$sql .= "	cd_candidato";
		$sql .= "	,cd_solicitacao";
		$sql .= "	,nu_processo";
		$sql .= "	,dt_processo";
		$sql .= "	,dt_cancel";
		$sql .= "	,observacao";
		$sql .= "	,dt_cad";
		$sql .= "	,id_solicita_visto";
		$sql .= "	,codigo_processo_mte";
		$sql .= "	,fl_vazio";
		$sql .= "	,fl_processo_atual";
		$sql .= "	,nu_servico";
		$sql .= "   ,cd_usuario";
		$sql .= "	,dt_ult";
		$sql .= "   ,no_classe";
		$sql .= "   ,no_metodo";
		$sql .= "	,dt_envio_bsb";
		$sql .= "	,codigo_processo_prorrog";

		$sql .= "	) values (";
		$sql .= "	 ".cBANCO::ChaveOk($this->mcd_candidato);
		$sql .= "	,".cBANCO::ChaveOk($this->mcd_solicitacao);
		$sql .= "	,".cBANCO::StringOk($this->mnu_processo);
		$sql .= "	,".cBANCO::DataOk($this->mdt_processo);
		$sql .= "	,".cBANCO::DataOk($this->mdt_cancel);
		$sql .= "	,".cBANCO::StringOk($this->mobservacao);
		$sql .= "	,now() ";
		$sql .= "	,".cBANCO::ChaveOk($this->mid_solicita_visto);
		$sql .= "	,".cBANCO::ChaveOk($this->mcodigo_processo_mte);
		$sql .= "	,".cBANCO::InteiroOk($this->mfl_vazio);
		$sql .= "	,".cBANCO::InteiroOk($this->mfl_processo_atual);
		$sql .= "	,".cBANCO::ChaveOk($this->mnu_servico);
		$sql .= "	,".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= "	,now()";
		$sql.= "	,".cBANCO::StringOk(__CLASS__);
		$sql.= "	,".cBANCO::StringOk(__FUNCTION__);
		$sql .= "	,".cBANCO::DataOk($this->mdt_envio_bsb);
		$sql .= "	,".cBANCO::ChaveOk($this->mcodigo_processo_prorrog);
		$sql .= "	)";
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
		$this->mcodigo = mysql_insert_id();
		// Ao incluir uma nova OS de cancelamento, se for para o visto atual, inativar o candidato.
		if ($this->mid_solicita_visto != ''){
			$cand = new cCANDIDATO();
			$cand->mNU_CANDIDATO = $this->mcd_candidato;
			$cand->Inative(cSESSAO::$mcd_usuario);
		}
	}

	public function IncluaNovoFilho(){
		$this->VerificarCandidato();
		$this->mfl_vazio = '0';
		$this->IncluaNovo();
	}

	private function Atualize(){
		$sql = "update processo_cancel set ";
		$sql .= "   cd_candidato			= ".cBANCO::ChaveOk($this->mcd_candidato);
		$sql .= " , cd_solicitacao		= ".cBANCO::StringOk($this->mcd_solicitacao);
		$sql .= " , nu_processo			= ".cBANCO::StringOk($this->mnu_processo);
		$sql .= " , dt_processo			= ".cBANCO::DataOk($this->mdt_processo);
		$sql .= " , dt_cancel			= ".cBANCO::DataOk($this->mdt_cancel);
		$sql .= " , observacao			= ".cBANCO::StringOk($this->mobservacao);
		$sql .= " , id_solicita_visto	= ".cBANCO::ChaveOk($this->mid_solicita_visto);
		$sql .= " , codigo_processo_mte	= ".cBANCO::ChaveOk($this->mcodigo_processo_mte);
		$sql .= " , fl_vazio 			= ".cBANCO::InteiroOk($this->mfl_vazio);
		$sql .= " , fl_processo_atual	= ".cBANCO::InteiroOk($this->mfl_processo_atual);
		$sql .= " , nu_servico			= ".cBANCO::ChaveOk($this->mnu_servico);
		$sql .= " , dt_ult 				= now()";
		$sql .= " , cd_usuario			= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= " , no_classe			= ".cBANCO::StringOk(__CLASS__);
		$sql .= " , no_metodo			= ".cBANCO::StringOk(__FUNCTION__);
		$sql .= " , codigo_processo_prorrog	= ".cBANCO::ChaveOk($this->mcodigo_processo_prorrog);
		$sql .= " where codigo 			= ".$this->mcodigo;
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);

	}

	private function Atualize_ProcessoEdit(){
		$msg = '';

		if ($this->mcodigo_processo_mte == ''){
			$msg .= '<br>- O visto que está sendo cancelado/arquivado é obrigatório.';
		}
		if ($msg != ''){
			throw new cERRO_CONSISTENCIA('Não foi possível completar a operação pois:'.$msg);
		}

		$sql = "update processo_cancel set ";
		$sql .= "   nu_processo			= ".cBANCO::StringOk($this->mnu_processo);
		$sql .= " , dt_processo			= ".cBANCO::DataOk($this->mdt_processo);
		$sql .= " , dt_cancel			= ".cBANCO::DataOk($this->mdt_cancel);
		$sql .= " , codigo_processo_mte	= ".cBANCO::ChaveOk($this->mcodigo_processo_mte);
		$sql .= " , observacao			= ".cBANCO::StringOk($this->mobservacao);
		$sql .= " , dt_ult 				= now()";
		$sql .= " , cd_usuario			= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= " , no_classe			= ".cBANCO::StringOk(__CLASS__);
		$sql .= " , no_metodo			= ".cBANCO::StringOk(__FUNCTION__);
		$sql .= " , dt_envio_bsb		= ".cBANCO::DataOk($this->mdt_envio_bsb);
		$sql .= " , codigo_processo_prorrog	= ".cBANCO::ChaveOk($this->mcodigo_processo_prorrog);
		$sql .= " where codigo 			= ".$this->mcodigo;
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
	}
	/**
	 * Indica se a OS do processo pode ser fechada baseado na situação dos atributos.
	 * Deve ser overridado para funcionar para cada processo
	 */
	public function PodeFechar()
	{
		if($this->mdt_processo != '' && $this->mnu_processo != ''){
			return true;
		}
		else{
			return false;
		}
	}

	public static function AcoesDisponiveis(){
		$acoes = array();
		$acao = new cACAO();
		$acao->mCodigo = 'EXCLUIR';
		$acao->mLabel = 'Excluir';
		$acao->mMsgConfirmacao = 'Deseja mesmo excluir esse processo?';
		$acao->mMsgConfirmacaoTitulo = 'Exclusão de processo';
		$acoes[] = $acao;
	}

	public function RegistraDtEnvioBSB($pid_solicita_visto, $pdt_envio_bsb) {
		$sql = "update processo_cancel
				set dt_envio_bsb = ".cBANCO::DataOk($pdt_envio_bsb)."
				, cd_usuario	= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario)."
				, dt_ult		= now()
				where id_solicita_visto = ".$pid_solicita_visto;
		cAMBIENTE::ExecuteQuery($sql, __CLASS__.'->'.__FUNCTION__);
	}

	public static function Atualiza_dt_envio_bsb($pid_solicita_visto, $pdt_envio_bsb, $pcd_usuario)
	{
		$sql = "update processo_cancel
			       set dt_envio_bsb = ".cBANCO::DataOk($pdt_envio_bsb)."
					 , cd_usuario = ".cBANCO::ChaveOk($pcd_usuario)."
			     where id_solicita_visto = ".$pid_solicita_visto;
		cAMBIENTE::$db_pdo->exec($sql);
		if($pdt_envio_bsb != ''){
			$status = 4;
		}
		else{
			$status = 3;
		}
		$sql = "update solicita_visto
			       set id_status_sol = ".$status."
			     where id_solicita_visto = ".$pid_solicita_visto;
		cAMBIENTE::$db_pdo->exec($sql);
	}

	public function SituacaoVistoProcesso(&$pprazo_estada, &$ptexto, &$porigem){
		$pprazo_estada = '--';
		$ptexto = "visto cancelado";
		$porigem = "Processo mais recente é um cancelamento";
		return true;
		break;
	}

	public static function RetirarDoProtocoloBsb($pid_solicita_visto, $pcd_usuario)
	{
		$sql = "update processo_cancel
			       set dt_envio_bsb = null
					 , cd_usuario = ".cBANCO::ChaveOk($pcd_usuario)."
			     where id_solicita_visto = ".cBANCO::ChaveOk($pid_solicita_visto);
		cAMBIENTE::$db_pdo->exec($sql);

		$sql = "update solicita_visto
			       set id_status_sol = 3
			     where solicita_visto.id_solicita_visto = ".$pid_solicita_visto;
		cAMBIENTE::$db_pdo->exec($sql);
	}

	public static function Atualiza_dt_processo($pdt_processo, $pid_solicita_visto, $pcd_usuario)
	{
		$sql = "update processo_cancel
			       set dt_processo = ".cBANCO::DataOk($pdt_processo)."
					 , cd_usuario = ".cBANCO::ChaveOk($pcd_usuario)."
			     where id_solicita_visto = ".$pid_solicita_visto;
		cAMBIENTE::$db_pdo->exec($sql);
	}

	public static function AtualizaPelaOs_nu_processo($pnu_processo, $pid_solicita_visto, $pcd_usuario)
	{
		$sql = "update processo_cancel
			       set nu_processo = ".cBANCO::StringOk($pnu_processo)."
					 , cd_usuario = ".cBANCO::ChaveOk($pcd_usuario)."
			     where id_solicita_visto = ".$pid_solicita_visto;
		cAMBIENTE::$db_pdo->exec($sql);

		if($pnu_processo != ''){
			cORDEMSERVICO::IndicaProtocoloRealizadoAutomaticamente($pid_solicita_visto ,$pcd_usuario);
		}
	}

	/**
	 * Retorna um recordset com os registros do log do processo
	 * @param int $codigo Chave do processo
	 * @return PDOStatement
	 */
	public static function cursorLog($codigo){
		$sql = "select historico_processo_cancel.* "
				."     , date_format(hcan_dt_evento, '%d/%m/%Y %H:%i:%s') hcan_dt_evento_fmt"
				."     , usuarios.nome"
				."  from historico_processo_cancel"
				."  left join usuarios on usuarios.cd_usuario = historico_processo_cancel.cd_usuario "
				." where codigo = ".$codigo
				." order by historico_processo_cancel.hcan_dt_evento desc ";
		$rs = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		return $rs;
	}

}
