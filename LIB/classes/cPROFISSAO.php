<?php

// á
class cPROFISSAO extends cMODELO {

    protected $mCO_PROFISSAO;
    protected $mNO_PROFISSAO;
    protected $mNO_PROFISSAO_EM_INGLES;
    protected $mNU_CBO;

    public static function get_nomeCampoChave(){
        return 'co_profissao';
    }
    public static function get_nomeCampoDescricao(){
        return 'no_profissao';
    }
    public static function get_nomeTabelaBD(){
        return 'PROFISSAO';
    }

    public function get_nomeTabela() {
        return 'profissao';
    }

    public function getId() {
        return $this->mCO_PROFISSAO;
    }

    public function setId($pValor) {
        $this->mCO_PROFISSAO = $pValor;
    }

    public function campoid() {
        return 'CO_PROFISSAO';
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from PROFISSAO where CO_PROFISSAO = " . $this->mCO_PROFISSAO;
        return $sql;
    }

    public function Incluir() {
        $sql = "insert into PROFISSAO(
				 NO_PROFISSAO 
				,NO_PROFISSAO_EM_INGLES 
				,NU_CBO 
		       ) values (
				 " . cBANCO::StringOk($this->mNO_PROFISSAO) . "
				," . cBANCO::StringOk($this->mNO_PROFISSAO_EM_INGLES) . "
				," . cBANCO::StringOk($this->mNU_CBO) . "
				)";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mCO_PROFISSAO = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function Atualizar() {
        $sql = "update PROFISSAO set 
				 NO_PROFISSAO = " . cBANCO::StringOk($this->mNO_PROFISSAO) . "
				,NO_PROFISSAO_EM_INGLES = " . cBANCO::StringOk($this->mNO_PROFISSAO_EM_INGLES) . "
				,NU_CBO = " . cBANCO::StringOk($this->mNU_CBO) . "
			  where CO_PROFISSAO=" . $this->mCO_PROFISSAO;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_Liste() {
        $sql = "select * from PROFISSAO where 1=1";
        return $sql;
    }

    public static function comboMustache($valor_atual = '', $default = "(selecione...)") {
        $sql = "select CO_PROFISSAO valor, NO_PROFISSAO descricao, if(CO_PROFISSAO='" . $valor_atual . "' ,1 ,0 ) selected from PROFISSAO order by NO_PROFISSAO";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor' => '0', 'descricao' => $default, 'selected' => '0'));

        if (intval($valor_atual == 0)) {
            $rs['0']['selected'] = true;
        }

        return $rs;
    }

    public static function comboMustacheIngles($valor_atual = '', $default = "(selecione...)") {
        $sql = "select CO_PROFISSAO valor, NO_PROFISSAO_EM_INGLES descricao, if(CO_PROFISSAO='" . $valor_atual . "' ,1 ,0 ) selected from PROFISSAO order by NO_PROFISSAO_EM_INGLES";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor' => '0', 'descricao' => $default, 'selected' => '0'));

        if (intval($valor_atual == 0)) {
            $rs['0']['selected'] = true;
        }

        return $rs;
    }

}
