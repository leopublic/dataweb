<?php

class cprocesso_prorrog extends cprocesso {

    public $mnu_protocolo;
    public $mdt_requerimento;
    public $mdt_validade;
    public $mdt_prazo_pret;
    public $mdt_publicacao_dou;
    public $mID_STATUS_CONCLUSAO;
    public $mdt_envio_bsb;
    public $mnu_pre_cadastro;
    public $mdt_pre_cadastro;
    public $mcd_usuario_pre_cadastro;
    public $mdt_envio_bsb_pre_cadastro;
    public $mdt_requerimento_pre_cadastro;
    public $mnu_pagina_dou;
    public $mID_STATUS_SOL;
    public $mid_status_sol;

    public function __construct($pCodigo = 0) {
        $this->mnomeTabela = 'prorrog';
        if ($pCodigo > 0) {
            $this->RecupereSe($pCodigo);
        } else {
            $this->mcodigo = 0;
            $this->mfl_vazio = 0;
            $this->mfl_processo_atual = 0;
        }
    }

    public static function get_nomeCampoChave() {
        return 'codigo';
    }

    public static function get_nomeTabelaBD() {
        return 'processo_prorrog';
    }

    public function getid() {
        return $this->mcodigo;
    }

    public function get_nu_processo() {
        return $this->mnu_protocolo;
    }

    public function RecupereSe($pCodigo = 0) {
        if ($pCodigo > 0) {
            $this->mcodigo = $pCodigo;
        }

        if (!$this->mcodigo > 0) {
            throw new cERRO_CONSISTENCIA("Código do processo não informado");
        }
        $sql = "select processo_prorrog.*, s.NO_SERVICO_RESUMIDO"
                . " from processo_prorrog"
                . " left join servico s on s.nu_servico = processo_prorrog.nu_servico"
                . " where codigo = " . $this->mcodigo;
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->mcd_candidato = $rs["cd_candidato"];
                $this->mcd_solicitacao = $rs["cd_solicitacao"];
                $this->mnu_protocolo = $rs["nu_protocolo"];
                $this->mdt_requerimento = cBANCO::DataFmt($rs["dt_requerimento"]);
                $this->mdt_validade = cBANCO::DataFmt($rs["dt_validade"]);
                $this->mdt_prazo_pret = cBANCO::DataFmt($rs["dt_prazo_pret"]);
                $this->mobservacao = $rs["observacao"];
                $this->mdt_cad = cBANCO::DataFmt($rs["dt_cad"]);
                $this->mdt_ult = cBANCO::DataFmt($rs["dt_ult"]);
                $this->mid_solicita_visto = $rs["id_solicita_visto"];
                $this->mcodigo_processo_mte = $rs["codigo_processo_mte"];
                $this->mfl_vazio = $rs["fl_vazio"];
                $this->mfl_processo_atual = $rs["fl_processo_atual"];
                $this->mID_STATUS_CONCLUSAO = $rs['ID_STATUS_CONCLUSAO'];
                $this->mdt_publicacao_dou = cBANCO::DataFmt($rs['dt_publicacao_dou']);
                $this->mnu_servico = $rs['nu_servico'];
                $this->mdt_envio_bsb = cBANCO::DataFmt($rs['dt_envio_bsb']);
                $this->mnu_pre_cadastro = $rs['nu_pre_cadastro'];
                $this->mdt_pre_cadastro = cBANCO::DataFmt($rs['dt_pre_cadastro']);
                $this->mdt_envio_bsb_pre_cadastro = cBANCO::DataFmt($rs['dt_envio_bsb_pre_cadastro']);
                $this->mcd_usuario_pre_cadastro = cBANCO::DataFmt($rs['cd_usuario_pre_cadastro']);
                $this->mdt_requerimento_pre_cadastro = cBANCO::DataFmt($rs["dt_requerimento_pre_cadastro"]);
                $this->mNO_SERVICO_RESUMIDO = cBANCO::DataFmt($rs["NO_SERVICO_RESUMIDO"]);
            } else {
                throw new Exception("Processo (" . $this->mcodigo . ") não encontrado");
            }
        }
    }

    public function Salve() {
        if ($this->mcodigo == 0) {
            $this->IncluaNovo();
        } else {
            $this->Atualize();
        }
    }

    public function Salve_ProcessoEdit() {
        if ($this->mcodigo == 0) {
            $this->IncluaNovo();
        } else {
            $this->Atualize_ProcessoEdit();
        }
    }

    private function IncluaNovo() {
        $sql = "insert into processo_prorrog (";
        $sql .= "	cd_candidato";
        $sql .= "	,cd_solicitacao";
        $sql .= "	,nu_protocolo";
        $sql .= "	,dt_requerimento";
        $sql .= "	,dt_validade";
        $sql .= "	,dt_prazo_pret";
        $sql .= "	,observacao";
        $sql .= "	,dt_cad";
        $sql .= "	,dt_ult";
        $sql .= "	,id_solicita_visto";
        $sql .= "	,codigo_processo_mte";
        $sql .= "	,fl_vazio";
        $sql .= "	,fl_processo_atual";
        $sql .= "	,dt_publicacao_dou";
        $sql .= "	,ID_STATUS_CONCLUSAO";
        $sql .= "	,nu_servico";
        $sql .= "	,cd_usuario";
        $sql .= "	,no_classe";
        $sql .= "	,no_metodo";
        $sql .= "	,dt_envio_bsb";
        $sql .= "	,nu_pre_cadastro";
        $sql .= "	,dt_pre_cadastro";
        $sql .= "	,dt_envio_bsb_pre_cadastro";
        $sql .= "	,dt_requerimento_pre_cadastro";
        $sql .= "	) values (";
        $sql .= "	 " . cBANCO::ChaveOk($this->mcd_candidato);
        $sql .= "	," . cBANCO::ChaveOk($this->mcd_solicitacao);
        $sql .= "	," . cBANCO::StringOk($this->mnu_protocolo);
        $sql .= "	," . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= "	," . cBANCO::DataOk($this->mdt_validade);
        $sql .= "	," . cBANCO::DataOk($this->mdt_prazo_pret);
        $sql .= "	," . cBANCO::StringOk($this->mobservacao);
        $sql .= "	,now()";
        $sql .= "	,now()";
        $sql .= "	," . cBANCO::ChaveOk($this->mid_solicita_visto);
        $sql .= "	," . cBANCO::ChaveOk($this->mcodigo_processo_mte);
        $sql .= "	," . cBANCO::InteiroOk($this->mfl_vazio);
        $sql .= "	," . cBANCO::InteiroOk($this->mfl_processo_atual);
        $sql .= "	," . cBANCO::DataOk($this->mdt_publicacao_dou);
        $sql .= "	," . cBANCO::ChaveOk($this->mID_STATUS_CONCLUSAO);
        $sql .= "	," . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= "   ," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "	," . cBANCO::StringOk(__CLASS__);
        $sql .= "	," . cBANCO::StringOk(__FUNCTION__);
        $sql .= "	," . cBANCO::DataOk($this->mdt_envio_bsb);
        $sql .= "	," . cBANCO::StringOk($this->mnu_pre_cadastro);
        $sql .= "	," . cBANCO::DataOk($this->mdt_pre_cadastro);
        $sql .= "	," . cBANCO::DataOk($this->mdt_envio_bsb_pre_cadastro);
        $sql .= "	," . cBANCO::DataOk($this->mdt_requerimento_pre_cadastro);
        $sql .= "	)";
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mcodigo = mysql_insert_id();
    }

    public function IncluaNovoFilho() {
        $this->VerificarCandidato();
        $this->mfl_vazio = '0';
        $this->IncluaNovo();
    }

    private function Atualize() {
        $sql = "update processo_prorrog set ";
        $sql .= "   cd_candidato 		= " . cBANCO::ChaveOk($this->mcd_candidato);
        $sql .= " , cd_solicitacao 		= " . cBANCO::ChaveOk($this->mcd_solicitacao);
        $sql .= " , nu_protocolo 		= " . cBANCO::StringOk($this->mnu_protocolo);
        $sql .= " , dt_requerimento 	= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , dt_validade 		= " . cBANCO::DataOk($this->mdt_validade);
        $sql .= " , dt_prazo_pret 		= " . cBANCO::DataOk($this->mdt_prazo_pret);
        $sql .= " , observacao 			= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , id_solicita_visto 	= " . cBANCO::ChaveOk($this->mid_solicita_visto);
        $sql .= " , codigo_processo_mte = " . cBANCO::ChaveOk($this->mcodigo_processo_mte);
        $sql .= " , fl_vazio 			= " . cBANCO::InteiroOk($this->mfl_vazio);
        $sql .= " , fl_processo_atual 	= " . cBANCO::InteiroOk($this->mfl_processo_atual);
        $sql .= " , dt_publicacao_dou	= " . cBANCO::DataOk($this->mdt_publicacao_dou);
//		$sql .= " , ID_STATUS_CONCLUSAO	= ".cBANCO::ChaveOk($this->mID_STATUS_CONCLUSAO);
        $sql .= " , nu_servico			= " . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " , nu_pre_cadastro		= " . cBANCO::StringOk($this->mnu_pre_cadastro);
        $sql .= " , dt_pre_cadastro		= " . cBANCO::DataOk($this->mdt_pre_cadastro);
        $sql .= " , dt_requerimento_pre_cadastro= " . cBANCO::DataOk($this->mdt_requerimento_pre_cadastro);
        $sql .= " where codigo 			= " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    private function Atualize_ProcessoEdit() {
        $this->ConsisteCampos();
        if ($this->mnu_pre_cadastro != '' && $this->mdt_pre_cadastro == '') {
            $this->mdt_pre_cadastro = date('Y-m-d');
        }
        $sql = "update processo_prorrog set ";
        $sql .= "   nu_protocolo 		= " . cBANCO::StringOk($this->mnu_protocolo);
        $sql .= " , dt_requerimento 	= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , dt_validade 		= " . cBANCO::DataOk($this->mdt_validade);
        $sql .= " , dt_prazo_pret 		= " . cBANCO::DataOk($this->mdt_prazo_pret);
        $sql .= " , observacao 			= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , id_solicita_visto 	= " . cBANCO::ChaveOk($this->mid_solicita_visto);
        $sql .= " , dt_publicacao_dou	= " . cBANCO::DataOk($this->mdt_publicacao_dou);
        $sql .= " , dt_publicacao_dou	= " . cBANCO::DataOk($this->mdt_publicacao_dou);
		$sql .= " , ID_STATUS_CONCLUSAO	= ".cBANCO::ChaveOk($this->mID_STATUS_CONCLUSAO);
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " , dt_envio_bsb		= " . cBANCO::DataOk($this->mdt_envio_bsb);
        $sql .= " , nu_pre_cadastro		= " . cBANCO::StringOk($this->mnu_pre_cadastro);
        $sql .= " , dt_pre_cadastro		= " . cBANCO::DataOk($this->mdt_pre_cadastro);
        $sql .= " , dt_envio_bsb_pre_cadastro		= " . cBANCO::DataOk($this->mdt_envio_bsb_pre_cadastro);
        $sql .= " , dt_requerimento_pre_cadastro		= " . cBANCO::DataOk($this->mdt_requerimento_pre_cadastro);
        $sql .= " , nu_pagina_dou		= " . cBANCO::StringOk($this->mnu_pagina_dou);
        $sql .= " where codigo 			= " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);

        if (intval($this->mid_solicita_visto) > 0) {
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $this->mid_solicita_visto;
            $os->Recuperar();
            $os->AtualizePrazoSolicitadoPeloProcesso($this->mdt_prazo_pret);
            if ($this->mID_STATUS_SOL > 0) {
                $os->AtualizarStatus($this->mID_STATUS_SOL, cSESSAO::$mcd_usuario);
            }
            if ($this->mid_status_sol > 0) {
                $os->AtualizarStatus($this->mid_status_sol, cSESSAO::$mcd_usuario);
            }
        }
    }

    /**
     * Indica se a OS do processo pode ser fechada baseado na situação dos atributos.
     * Deve ser overridado para funcionar para cada processo
     */
    public function PodeFechar() {
        if ($this->mnu_protocolo != '' && $this->mdt_requerimento != '') {
            return true;
        } else {
            return false;
        }
    }

    public function ConsisteCampos() {
        $msg = '';
        if ($this->mdt_requerimento != '' && !cBANCO::DataValida($this->mdt_requerimento)) {
            $msg .= '<br/>- A data do requerimento é inválida';
        }
        if ($this->mdt_validade != '' && !cBANCO::DataValida($this->mdt_validade)) {
            $msg .= '<br/>- A validade do protocolo é inválida';
        }
        if ($this->mdt_prazo_pret != '' && !cBANCO::DataValida($this->mdt_prazo_pret)) {
            $msg .= '<br/>- O prazo pretendido é inválido';
        }
        if ($this->mdt_publicacao_dou != '' && !cBANCO::DataValida($this->mdt_publicacao_dou)) {
            $msg .= '<br/>- A data de publicação DOU é inválida';
        }
        if ($msg != '') {
            $msg = 'Não foi possível executar a operação pois:' . $msg;
            throw new cERRO_CONSISTENCIA($msg);
        }
    }

    public function Exclua() {
        // Verifica se há algum restabelecimento associada a ela e transfere para o processo mte...
        $sql = "update processo_regcie , processo_prorrog";
        $sql.= "   set processo_regcie.codigo_processo_mte = processo_prorrog.codigo_processo_mte";
        $sql.= "     , processo_regcie.codigo_processo_prorrog = null";
        $sql.= " where processo_regcie.codigo_processo_prorrog = processo_prorrog.codigo";
        $sql.= "   and processo_prorrog.codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__ . '=> Tranferindo registros da prorrogação para o visto');
        parent::Exclua();
    }

    public function SalvarDadosOs($pOS, $pCand) {
        parent::SalvarDadosOs($pOS, $pCand);
        $this->RecupereSePelaOs($pOS->mID_SOLICITA_VISTO, $pCand->mNU_CANDIDATO);
        $this->mdt_prazo_pret = cBANCO::DataFmt($this->mdt_prazo_pret);
        if ($pOS->mDT_PRAZO_ESTADA_SOLICITADO != '') {
            if ($pOS->mDT_PRAZO_ESTADA_SOLICITADO != $this->mdt_prazo_pret && $this->mdt_prazo_pret != '') {
                $nl = '';
                if ($this->mobservacao != '') {
                    $nl = "\n";
                }
                $this->mobservacao .= $nl . "(Prazo pretendido alterado pelo prazo informado na OS. De " . $this->mdt_prazo_pret . " para " . $pOS->mDT_PRAZO_ESTADA_SOLICITADO . " por " . cSESSAO::$mnome . " em " . date('d/m/y') . ").";
            }
            $data = split("/", $pOS->mDT_PRAZO_ESTADA_SOLICITADO);
            if (checkdate($data[1], $data[0], $data[2])) {
//				$sql = "select dt_prazo_pret from processo_prorrog where id_solicita_visto = ".$pOS->mID_SOLICITA_VISTO;
//				if($res=mysql_query($sql)){
//					if ($rs=mysql_fetch_array($res)){
//						if ($rs['dt_prazo_pret']=='' ){
                $sql = " update processo_prorrog ";
                $sql .= "   set dt_prazo_pret = '" . $data[2] . '-' . $data[1] . '-' . $data[0] . "'";
                $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
                $sql .= " , dt_ult 				= now()";
                $sql .= " , observacao			= " . cBANCO::StringOk($this->mobservacao);
                $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
                $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
                $sql .= " where processo_prorrog.id_solicita_visto = " . $pOS->mID_SOLICITA_VISTO;
                executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
//						}
//					}
//				}
            }
        }
    }

    public function RegistraDtEnvioBSB($pid_solicita_visto, $pdt_envio_bsb) {
        $sql = "update processo_prorrog
				set dt_envio_bsb = " . cBANCO::DataOk($pdt_envio_bsb) . "
				, cd_usuario	= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario) . "
				, dt_ult		= now()
				where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function RecupereMaisRecenteDoCandidato($pcd_candidato, $pcodigo_processo_mte) {
        $sql = "select * from processo_prorrog
			    where  dt_prazo_pret = (select max(dt_prazo_pret) from processo_prorrog
										     where fl_vazio = 0
											 and codigo_processo_mte = " . $pcodigo_processo_mte . "
											 and cd_candidato = " . $pcd_candidato . "
											 and  dt_prazo_pret is not null)
				and codigo_processo_mte = " . $pcodigo_processo_mte . "
				and cd_candidato = " . $pcd_candidato . "
		        and (fl_vazio = 0 or fl_vazio is null)";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        cBANCO::CarreguePropriedades($rs, $this);
    }

    public function SituacaoVistoProcesso(&$pprazo_estada, &$ptexto, &$porigem) {

        if ($this->mnu_servico == 17) {  //Prorrogação Vitem II
            return false;
        } elseif (trim($this->mdt_requerimento) == '') { //
            return false;
        } else {
            if (trim($this->mnu_protocolo) == '') {
                return false;
            } else {
                switch ($this->mID_STATUS_CONCLUSAO) {
                    case "2":  // (deferido)
                        $pprazo_estada = $this->mdt_prazo_pret;
                        $ptexto = "Precisa ser registrado até data publicacao dou + 89 dias";
                        $porigem = "Prorrogação deferida sem registro";
                        return true;
                        break;
                    case "3":  // (indeferido)
                        $pprazo_estada = "--";
                        $ptexto = "Visto expirado";
                        $porigem = "Prorrogação indeferida";
                        return true;
                        break;
                    case "4":  // (arquivado)
                        $pprazo_estada = "--";
                        $ptexto = "Visto arquivado";
                        $porigem = "Prorrogação arquivada";
                        return true;
                        break;
                    case "1":  // (n/a)
                    default:
                        $pprazo_estada = $this->mdt_prazo_pret;
                        $ptexto = "Prorrogação em análise";
                        $porigem = "Prorrogação em andamento";
                        return true;
                        break;
                }
            }
        }
    }

    public static function Atualiza_nu_pre_cadastro($pcodigo, $pnu_pre_cadastro, $pcd_usuario_pre_cadastro) {
        $sql = "update processo_prorrog
			       set nu_pre_cadastro = " . cBANCO::StringOk($pnu_pre_cadastro) . "
					 , cd_usuario_pre_cadastro = " . cBANCO::ChaveOk($pcd_usuario_pre_cadastro) . "
			     where codigo = " . cBANCO::ChaveOk($pcodigo);
        cAMBIENTE::$db_pdo->exec($sql);
        // Coloca a data como data atual
        $sql = "update processo_prorrog
			       set dt_pre_cadastro = now()
				 where codigo = " . cBANCO::ChaveOk($pcodigo) . "
				   and dt_pre_cadastro is null";
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_nu_protocolo($pcodigo, $pnu_protocolo, $pcd_usuario) {
        $sql = "update processo_prorrog
			       set nu_protocolo = " . cBANCO::StringOk($pnu_protocolo) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
					 , dt_ult = now()
			     where codigo = " . cBANCO::ChaveOk($pcodigo);
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_dt_publicacao_dou($pcodigo, $pvalor, $pcd_usuario) {
        $sql = "update processo_prorrog
			       set dt_publicacao_dou = " . cBANCO::DataOk($pvalor) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
					 , dt_ult = now()
			     where codigo = " . cBANCO::ChaveOk($pcodigo);
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_nu_pagina_dou($pcodigo, $pvalor, $pcd_usuario) {
        $sql = "update processo_prorrog
			       set nu_pagina_dou = " . cBANCO::StringOk($pvalor) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
					 , dt_ult = now()
			     where codigo = " . cBANCO::ChaveOk($pcodigo);
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_dt_pre_cadastro($pcodigo, $pdt_pre_cadastro, $pcd_usuario_pre_cadastro) {
        $sql = "update processo_prorrog
			       set dt_pre_cadastro = " . cBANCO::DataOk($pdt_pre_cadastro) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario_pre_cadastro) . "
			     where codigo = " . cBANCO::ChaveOk($pcodigo);
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_dt_envio_bsb($pid_solicita_visto, $pdt_envio_bsb, $pcd_usuario) {
        $sql = "update processo_prorrog
			       set dt_envio_bsb = " . cBANCO::DataOk($pdt_envio_bsb) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);
        if ($pdt_envio_bsb != '') {
            $status = 4;
        } else {
            $status = 3;
        }
        $sql = "update solicita_visto
			       set id_status_sol = " . $status . "
			     where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_dt_envio_bsb_pre_cadastro($pid_solicita_visto, $pdt_envio_bsb, $pcd_usuario) {
        $sql = "update processo_prorrog
			       set dt_envio_bsb_pre_cadastro = " . cBANCO::DataOk($pdt_envio_bsb) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_observacao($pcodigo, $pobservacao, $pcd_usuario) {
        $sql = "update processo_prorrog
			       set observacao = " . cBANCO::StringOk($pobservacao) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where codigo = " . cBANCO::ChaveOk($pcodigo);
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_dt_requerimento($pdt_requerimento, $pid_solicita_visto, $pcd_usuario) {
        $sql = "update processo_prorrog
			       set dt_requerimento = " . cBANCO::DataOk($pdt_requerimento) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function AtualizaPelaOs_nu_protocolo($pnu_protocolo, $pid_solicita_visto, $pcd_usuario) {
        //TODO: Recuperar a data de requerimento e se estiver vazia atualizar com a data de hoje.
        $sql = "update processo_prorrog
			       set nu_protocolo = " . cBANCO::StringOk($pnu_protocolo) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);

        if ($pnu_processo != '') {
            cORDEMSERVICO::IndicaProtocoloRealizadoAutomaticamente($pid_solicita_visto, $pcd_usuario);
        }
    }

    public static function AtualizaPelaOs_nu_protocoloDigital($pnu_protocolo, $pid_solicita_visto, $pcd_usuario) {
        //TODO: Recuperar a data de requerimento e se estiver vazia atualizar com a data de hoje.
        $sql = "update processo_prorrog
			       set nu_protocolo = " . cBANCO::StringOk($pnu_protocolo) . "
					 , dt_envio_bsb = now()
					 , dt_requerimento = now()
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);

        if ($pnu_processo != '') {
            cORDEMSERVICO::IndicaProtocoloRealizadoAutomaticamente($pid_solicita_visto, $pcd_usuario);
        }
    }

    public static function RetirarDoProtocoloBsb($pid_solicita_visto, $pcd_usuario) {
        $sql = "update processo_prorrog
			       set dt_envio_bsb = null
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . cBANCO::ChaveOk($pid_solicita_visto);
        cAMBIENTE::$db_pdo->exec($sql);

        $sql = "update solicita_visto
			       set id_status_sol = 3
			     where solicita_visto.id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function RetirarDoProtocoloBsbPrecadastro($pid_solicita_visto, $pcd_usuario) {
        $sql = "update processo_prorrog
			       set dt_envio_bsb_pre_cadastro = null
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . cBANCO::ChaveOk($pid_solicita_visto);
        cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Retorna um recordset com os registros do log do processo
     * @param int $codigo Chave do processo
     * @return PDOStatement
     */
    public static function cursorLog($codigo) {
        $sql = "select historico_processo_prorrog.* "
                . "     , date_format(hpro_dt_evento, '%d/%m/%Y %H:%i:%s') hpro_dt_evento_fmt"
                . "     , usuarios.nome"
                . "  from historico_processo_prorrog"
                . "  left join usuarios on usuarios.cd_usuario = historico_processo_prorrog.cd_usuario "
                . " where codigo = " . $codigo
                . " order by historico_processo_prorrog.hpro_dt_evento desc ";
        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $rs;
    }


    public function nu_protocolo_fmt(){
        return formatarMascara(preg_replace("/[^0-9]/", "", $this->mnu_protocolo), '#####.######/####-##');
    }
}
