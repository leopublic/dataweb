<?php
class cESTADO_CIVIL extends cMODELO{
	protected $mCO_ESTADO_CIVIL;
	protected $mNO_ESTADO_CIVIL;
	protected $mNO_ESTADO_CIVIL_EM_INGLES;


    public static function get_nomeCampoChave(){
        return 'co_estado_civil';
    }
    public static function get_nomeCampoDescricao(){
        return "no_estado_civil";
    }
    public static function get_nomeTabelaBD(){
        return 'estado_civil';
    }


	public function get_nomeTabela()
	{
		return 'estado_civil';
	}

	public function getId()
	{
		return $this->mCO_ESTADO_CIVIL;
	}

	public function setId($pValor)
	{
		$this->mCO_ESTADO_CIVIL = $pValor;
	}

	public function campoid()
	{
		return 'CO_ESTADO_CIVIL';
	}

	public function sql_RecuperePeloId() {
		$sql = "select * from ESTADO_CIVIL where CO_ESTADO_CIVIL = ".$this->mCO_ESTADO_CIVIL;
		return $sql;
	}

	public function Incluir()
	{
		$sql= "insert into ESTADO_CIVIL(
				 NO_ESTADO_CIVIL
				,NO_ESTADO_CIVIL_EM_INGLES
		       ) values (
				 ".cBANCO::StringOk($this->mNO_ESTADO_CIVIL)."
				,".cBANCO::StringOk($this->mNO_ESTADO_CIVIL_EM_INGLES)."
				)";
		cAMBIENTE::ExecuteQuery($sql);
		$this->mCO_ESTADO_CIVIL = cAMBIENTE::$db_pdo->lastInsertId();
	}

	public function Atualizar()
	{
		$sql= "update ESTADO_CIVIL set
				 NO_ESTADO_CIVIL = ".cBANCO::StringOk($this->mNO_ESTADO_CIVIL)."
				,NO_ESTADO_CIVIL_EM_INGLES = ".cBANCO::StringOk($this->mNO_ESTADO_CIVIL_EM_INGLES)."
			  where CO_ESTADO_CIVIL=".$this->mCO_ESTADO_CIVIL;
		cAMBIENTE::ExecuteQuery($sql);
	}

	public function sql_Liste()
	{
		$sql = "select * from ESTADO_CIVIL where 1=1";
		return $sql;
	}
}
