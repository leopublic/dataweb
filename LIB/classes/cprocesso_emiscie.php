<?php
class cprocesso_emiscie
	extends cprocesso
{
	public $mnu_cie;
	public $mdt_emissao;
	public $mdt_validade;
	public $mCO_CLASSIFICACAO_VISTO;

	public function __construct($pCodigo = 0){
		$this->mnomeTabela = 'emiscie';
		if ($pCodigo > 0){
			$this->RecupereSe($pCodigo);
		}
		else{
			$this->mcodigo = 0;
		}
	}
	
	public function RecupereSe($pCodigo = 0){
		if ($pCodigo > 0){
			$this->mcodigo = $pCodigo;
		}
		
		if (! $this->mcodigo > 0){
			throw new cERRO_CONSISTENCIA("Código do processo não informado");
		}
		$sql = "select pe.* 
				, NO_CLASSIFICACAO_VISTO
                                , s.NO_SERVICO_RESUMIDO
				from processo_emiscie pe
				left join classificacao_visto cv on cv.CO_CLASSIFICACAO_VISTO = pe.CO_CLASSIFICACAO_VISTO
				left join servico s on s.nu_servico = pe.nu_servico
				where codigo = ".$this->mcodigo;
		if($res = mysql_query($sql)){
			if($rs = mysql_fetch_array($res)){
				$this->mcodigo					= $rs["codigo"];
				$this->mcd_candidato			= $rs["cd_candidato"];
				$this->mcd_solicitacao			= $rs["cd_solicitacao"];
				$this->mnu_cie					= $rs["nu_cie"];
				$this->mdt_emissao				= cBANCO::DataFmt($rs["dt_emissao"]);
				$this->mdt_validade				= cBANCO::DataFmt($rs["dt_validade"]);
				$this->mobservacao				= $rs["observacao"];
				$this->mdt_cad					= cBANCO::DataFmt($rs["dt_cad"]);
				$this->mdt_ult					= cBANCO::DataFmt($rs["dt_ult"]);
				$this->mid_solicita_visto		= $rs["id_solicita_visto"];
				$this->mcodigo_processo_mte		= $rs["codigo_processo_mte"];
				$this->mfl_vazio				= $rs["fl_vazio"];
				$this->mfl_processo_atual		= $rs["fl_processo_atual"];
				$this->mnu_servico				= $rs['nu_servico'];
				$this->mCO_CLASSIFICACAO_VISTO  = $rs['CO_CLASSIFICACAO_VISTO'];
                                $this->mNO_SERVICO_RESUMIDO = $rs['NO_SERVICO_RESUMIDO'];
			}
			else{
				throw new Exception("Processo (".$this->mcodigo.") não encontrado");
			}
		}		
	}
	
	public function Salve(){
		if ($this->mcodigo == 0){
			$this->IncluaNovo();
		}
		else{
			$this->Atualize();
		}
	}
	
	public function Salve_ProcessoEdit(){
		if ($this->mcodigo == 0){
			$this->IncluaNovo();
		}
		else{
			$this->Atualize_ProcessoEdit();
		}
	}
	
	private function IncluaNovo(){
		$sql = "insert into processo_emiscie(";
		$sql .= "   cd_candidato";
		$sql .= " , cd_solicitacao";
		$sql .= " , nu_cie";
		$sql .= " , dt_emissao";
		$sql .= " , dt_validade";
		$sql .= " , observacao";
		$sql .= " , dt_cad";
		$sql .= " , dt_ult";
		$sql .= " , id_solicita_visto";
		$sql .= " , codigo_processo_mte";
		$sql .= " , fl_vazio";
		$sql .= " , fl_processo_atual";
		$sql .= "	,nu_servico";
		$sql .= "	,cd_usuario";
		$sql .= "	,no_classe";
		$sql .= "	,no_metodo";
		$sql .= "	,CO_CLASSIFICACAO_VISTO";
		$sql .= ") values (";
		$sql .= "   ".cBANCO::ChaveOk($this->mcd_candidato);
		$sql .= " , ".cBANCO::ChaveOk($this->mcd_solicitacao);
		$sql .= " , ".cBANCO::StringOk($this->mnu_cie);
		$sql .= " , ".cBANCO::DataOk($this->mdt_emissao);
		$sql .= " , ".cBANCO::DataOk($this->mdt_validade);
		$sql .= " , ".cBANCO::StringOk($this->mobservacao);
		$sql .= " , now()";
		$sql .= " , now()";
		$sql .= " , ".cBANCO::ChaveOk($this->mid_solicita_visto);
		$sql .= " , ".cBANCO::ChaveOk($this->mcodigo_processo_mte);
		$sql .= " , ".cBANCO::InteiroOk($this->mfl_vazio);
		$sql .= " , ".cBANCO::InteiroOk($this->mfl_processo_atual);
		$sql .= "	,".cBANCO::ChaveOk($this->mnu_servico);
		$sql .= "   ,".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= "	,".cBANCO::StringOk(__CLASS__);
		$sql .= "	,".cBANCO::StringOk(__FUNCTION__);
		$sql .= " , ".cBANCO::ChaveOk($this->mCO_CLASSIFICACAO_VISTO);
		$sql .= ")";
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
		$this->mcodigo = mysql_insert_id();
	}
	
	public function IncluaNovoFilho(){
		$this->VerificarCandidato();
		$this->mfl_vazio = '0';
		$this->IncluaNovo();
	}
	
	private function Atualize(){
		$sql = "update processo_emiscie";
		$sql .= " set";
		$sql .= "   cd_candidato = ".cBANCO::ChaveOk($this->mcd_candidato);
		$sql .= " , cd_solicitacao = ".cBANCO::ChaveOk($this->mcd_solicitacao);
		$sql .= " , nu_cie = ".cBANCO::StringOk($this->mnu_cie);
		$sql .= " , dt_emissao = ".cBANCO::DataOk($this->mdt_emissao);
		$sql .= " , dt_validade = ".cBANCO::DataOk($this->mdt_validade);
		$sql .= " , observacao = ".cBANCO::StringOk($this->mobservacao);
		$sql .= " , id_solicita_visto = ".cBANCO::ChaveOk($this->mid_solicita_visto);
		$sql .= " , codigo_processo_mte = ".cBANCO::ChaveOk($this->mcodigo_processo_mte);
		$sql .= " , fl_vazio = ".cBANCO::InteiroOk($this->mfl_vazio);
		$sql .= " , fl_processo_atual = ".cBANCO::InteiroOk($this->mfl_processo_atual);
		$sql .= " , nu_servico			= ".cBANCO::ChaveOk($this->mnu_servico);
		$sql .= " , dt_ult 				= now()";
		$sql .= " , cd_usuario			= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= " , CO_CLASSIFICACAO_VISTO = ".cBANCO::ChaveOk($this->mCO_CLASSIFICACAO_VISTO);
		$sql .= " , no_classe			= ".cBANCO::StringOk(__CLASS__);
		$sql .= " , no_metodo			= ".cBANCO::StringOk(__FUNCTION__);
		$sql .= " where codigo = ".$this->mcodigo;
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
	}

	private function Atualize_ProcessoEdit(){
		$sql = "update processo_emiscie";
		$sql .= " set";
		$sql .= "   nu_cie = ".cBANCO::StringOk($this->mnu_cie);
		$sql .= " , dt_emissao = ".cBANCO::DataOk($this->mdt_emissao);
		$sql .= " , dt_validade = ".cBANCO::DataOk($this->mdt_validade);
		$sql .= " , observacao = ".cBANCO::StringOk($this->mobservacao);
		$sql .= " , CO_CLASSIFICACAO_VISTO = ".cBANCO::ChaveOk($this->mCO_CLASSIFICACAO_VISTO);
		$sql .= " , dt_ult = now()";
		$sql .= " , cd_usuario			= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= " , no_classe			= ".cBANCO::StringOk(__CLASS__);
		$sql .= " , no_metodo			= ".cBANCO::StringOk(__FUNCTION__);
		$sql .= " where codigo = ".$this->mcodigo;
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
	}

	public function SituacaoVistoProcesso(&$pprazo_estada, &$ptexto, &$porigem)
	{
		if(trim($this->mdt_validade) == ''){
			return false;
		}
		else{
			$pprazo_estada = $this->mdt_validade;
			$ptexto = "Visto válido";
			$porigem = "Processo mais recente é uma coleta de cie";
			return true;
			break;
		}

	}
	
	public function RecupereSePelaOsSemCand($pID_SOLICITA_VISTO){
		$this->mid_solicita_visto = $pID_SOLICITA_VISTO;

		if (! intval($this->mid_solicita_visto) > 0){
			throw new Exception("Código da OS não informado");
		}
		$sql = "select ps.*, s.NO_SERVICO, s.NO_SERVICO_RESUMIDO , cv.NO_CLASSIFICACAO_VISTO
				from processo_emiscie ps";
		$sql.= " left join servico s on s.nu_servico = ps.nu_servico";
		$sql.= " left join classificacao_visto cv on cv.co_classificacao_visto = ps.co_classificacao_visto";
		$sql.= " where id_solicita_visto = ".$this->mid_solicita_visto;
		if($res = cAMBIENTE::$db_pdo->query($sql)){
			if($rs = $res->fetch(PDO::FETCH_ASSOC)){
				$this->mnu_servico				= $rs["nu_servico"];
				$this->mcodigo					= $rs["codigo"];
				$this->mcodigo_processo_mte		= $rs["codigo_processo_mte"];
				$this->mNO_SERVICO_RESUMIDO     = $rs["NO_SERVICO_RESUMIDO"]; 
				$this->mNO_CLASSIFICACAO_VISTO  = $rs['NO_CLASSIFICACAO_VISTO'];
				cBANCO::CarreguePropriedades($rs, $this);
			}
		}
	}

	/**
	 * Retorna um recordset com os registros do log do processo
	 * @param int $codigo Chave do processo
	 * @return PDOStatement
	 */
	public static function cursorLog($codigo){
		$sql = "select historico_processo_emiscie.* "
				."     , date_format(hemi_dt_evento, '%d/%m/%Y %H:%i:%s') hemi_dt_evento_fmt"
				."     , usuarios.nome"
				."  from historico_processo_emiscie"
				."  left join usuarios on usuarios.cd_usuario = historico_processo_emiscie.cd_usuario "
				." where codigo = ".$codigo
				." order by historico_processo_emiscie.hemi_dt_evento desc ";
		$rs = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		return $rs;
	}
	
}
