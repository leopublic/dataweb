<?php
class ctabela_precos extends cMODELO{
	public $mtbpc_id;
	public $mtbpc_tx_nome;
	public $mtbpc_nu_ano;
	public $mtbpc_tx_observacao;
	public $mtbpc_fl_tem_pacote;

	public function campoid(){
		return 'tbpc_id';
	}
	public function setId($pid){
		$this->mtbpc_id = $pid;
	}

	public function getId(){
		return $this->mtbpc_id;
	}

	public function sql_Liste(){
		$sql = "select *, (select count(*) from empresa where tbpc_id = tabela_precos.tbpc_id) qtdEmpresas from tabela_precos where 1=1 ";
		return $sql;
	}

	public function sql_RecuperePeloId(){
		$sql = "select * from tabela_precos where tbpc_id = ".$this->mtbpc_id;
		return $sql;
	}

	public function Incluir(){
		$sql = "INSERT INTO tabela_precos (";
		$sql .= "	 tbpc_tx_nome";
		$sql .= "	,tbpc_nu_ano";
		$sql .= "	,tbpc_tx_observacao";
		$sql .= "	,tbpc_fl_tem_pacote";
		$sql .= ") values (";
		$sql .= "	 ".cBANCO::StringOk($this->mtbpc_tx_nome);
		$sql .= "	,".cBANCO::InteiroOk($this->mtbpc_nu_ano);
		$sql .= "	,".cBANCO::StringOk($this->mtbpc_tx_observacao);
		$sql .= "	,".cBANCO::SimNaoOk($this->mtbpc_fl_tem_pacote);
		$sql .= ")";
		cAMBIENTE::$db_pdo->exec($sql);
		$this->setid(cAMBIENTE::$db_pdo->lastInsertId());
	}

	public function Atualizar(){
		$sql = "update tabela_precos set ";
		$sql .= "   tbpc_tx_nome			= ".cBANCO::StringOk($this->mtbpc_tx_nome);
		$sql .= "  ,tbpc_nu_ano				= ".cBANCO::InteiroOk($this->mtbpc_nu_ano);
		$sql .= "  ,tbpc_tx_observacao		= ".cBANCO::StringOk($this->mtbpc_tx_observacao);
		$sql .= "  ,tbpc_fl_tem_pacote		= ".cBANCO::StringOk($this->mtbpc_fl_tem_pacote);
		$sql .= " where tbpc_id = ".$this->mtbpc_id;
		cAMBIENTE::$db_pdo->exec($sql);
	}

	public function Clonar(){
		$tbpc_id_ant = $this->mtbpc_id;
		$this->RecuperePeloId($this->mtbpc_id);
		$this->mtbpc_tx_nome .= ' (clone)';
		$this->mtbpc_id = 0;
		$this->Salvar();
		cpreco::ClonarParaOutraTabela($tbpc_id_ant, $this->mtbpc_id);
	}

	public function Exclua(){
		cpreco::ExcluaPrecosDeTabela($this->mtbpc_id);
		$sql = "update solicita_visto set tbpc_id = null where tbpc_id = ".$this->mtbpc_id;
		cAMBIENTE::$db_pdo->exec($sql);
		$sql = "delete from tabela_precos where tbpc_id = ".$this->mtbpc_id;
		cAMBIENTE::$db_pdo->exec($sql);
	}

	public static function InstanciePelaEmpresa($pnu_empresa){
		$tabela = new ctabela_precos();
		if($pnu_empresa != ""){
			$sql = "select tp.* from tabela_precos tp join empresa e on e.tbpc_id = tp.tbpc_id and e.nu_empresa = ".cBANCO::ChaveOk($pnu_empresa);
			if($res = cAMBIENTE::$db_pdo->query($sql)){
				$rs = $res->fetch(PDO::FETCH_BOTH);
				cBANCO::CarreguePropriedades($rs, $tabela);
			}
		}
		return $tabela;
	}

	public static function AdicioneServico($ptbpc_id, $pnu_servico){
		$servico = new cSERVICO;
		$servico->RecuperePeloId($pnu_servico);
		$preco = new cpreco();
		$preco->mtbpc_id = $ptbpc_id;
		$preco->mNU_SERVICO = $pnu_servico;
		$preco->mprec_tx_descricao_tabela_precos = $servico->mNO_SERVICO;
		$preco->mprec_tx_descricao_tabela_precos_em_ingles = $servico->mno_servico_em_ingles;
		$preco->Salvar();
		return $preco;
	}

	public static function ExcluaServico($ptbpc_id, $pnu_servico)
	{
		$sql = "delete from preco where tbpc_id =".$ptbpc_id." and nu_servico=".$pnu_servico;
		cAMBIENTE::ExecuteQuery($sql);
	}
}
