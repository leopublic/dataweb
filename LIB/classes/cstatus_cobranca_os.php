<?php
class cstatus_cobranca_os extends cMODELO
{

    public $mstco_id;
    public $mstco_tx_nome;
    public $mstco_tx_sigla;

    const PENDENTE = 1;
    const LIBERADA_PARA_COBRANCA = 2;
    const FATURADA = 3;
    const NAO_GERA_COBRANCA = 4;
    const ANTIGA = 5;

    public function getid() {
        return $this->mstco_id;
    }

    public function setid($valor) {
        $this->mstco_id = $valor;
    }
    public function sql_RecuperePeloId() {
        $sql = "select * from status_cobranca_os where stco_id = " . $this->mstco_id;
        return $sql;
    }

    // Retorna um array com os status possiveis indicando o progresso
    public static function situacaoProgresso($stco_id) {
        $ret = array();

        if ($stco_id == 5) {
            $ret[] = array('ok' => true, 'stco_tx_nome' => 'Antiga');
        }
        elseif ($stco_id == 4) {
            $ret[] = array('ok' => true, 'stco_tx_nome' => 'Não gera cobrança');
        }
        else {
            $ok = true;
            $ret[] = array('ok' => $ok, 'stco_tx_nome' => 'Pendente');
            if ($stco_id == 1) {
                $ok = false;
            }
            $ret[] = array('ok' => $ok, 'stco_tx_nome' => 'Liberada');
            if ($stco_id == 2) {
                $ok = false;
            }
            $ret[] = array('ok' => $ok, 'stco_tx_nome' => 'Faturada');
        }

        return $ret;
    }
}
