<?php
class cprocesso_coleta
	extends cprocesso
{
	public $mno_validade;
	public $mnu_visto;
	public $mdt_emissao_visto;
	public $mno_local_emissao;
	public $mco_pais_nacionalidade_visto;
	public $mco_classificacao;
	public $mno_local_entrada;
	public $mco_uf;
	public $mdt_entrada;
	public $mnu_transporte_entrada;

	public function __construct($pCodigo = 0){
		$this->mnomeTabela = 'coleta';
		if ($pCodigo > 0){
			$this->RecupereSe($pCodigo);
		}
		else{
			$this->mcodigo = 0;
			$this->mfl_vazio = 0;
		}
	}
	
	public function RecupereSe($pCodigo = 0){
		if ($pCodigo > 0){
			$this->mcodigo = $pCodigo;
		}
		
		if (! $this->mcodigo > 0){
			throw new cERRO_CONSISTENCIA("Código do processo não informado");
		}
		$sql = "select processo_coleta.*, s.NO_SERVICO_RESUMIDO "
                        . " from processo_coleta"
                        . " left join servico s on s.nu_servico = processo_coleta.nu_servico"
                        . " where codigo = ".$this->mcodigo;
		if($res = mysql_query($sql)){
			if($rs = mysql_fetch_array($res)){
				$this->mcodigo						= $rs["codigo"];
				$this->mid_solicita_visto			= $rs["id_solicita_visto"];
				$this->mcd_candidato				= $rs["cd_candidato"];
				$this->mno_validade					= $rs["no_validade"];
				$this->mnu_visto					= $rs["nu_visto"];
				$this->mdt_emissao_visto			= cBANCO::DataFmt($rs["dt_emissao_visto"]);
				$this->mno_local_emissao			= $rs["no_local_emissao"];
				$this->mco_pais_nacionalidade_visto	= $rs["co_pais_nacionalidade_visto"];
				$this->mco_classificacao			= $rs["co_classificacao"];
				$this->mno_local_entrada			= $rs["no_local_entrada"];
				$this->mco_uf						= $rs["co_uf"];
				$this->mdt_entrada					= cBANCO::DataFmt($rs["dt_entrada"]);
				$this->mnu_transporte_entrada		= $rs["nu_transporte_entrada"];
				$this->mcodigo_processo_mte			= $rs["codigo_processo_mte"];
				$this->mnu_servico			=$rs['nu_servico'];
                                $this->mNO_SERVICO_RESUMIDO = $rs['NO_SERVICO_RESUMIDO'];
			}
			else{
				throw new Exception("Processo (".$this->mcodigo.") não encontrado");
			}
		}		
	}
	
	public function Salve(){
		if ($this->mcodigo == 0){
			$this->IncluaNovo();
		}
		else{
			$this->Atualize();
		}
	}
	
	public function Salve_ProcessoEdit(){
		if ($this->mcodigo == 0){
			$this->IncluaNovo();
		}
		else{
			$this->Atualize_ProcessoEdit();
		}
	}
	
	private function IncluaNovo(){
		$sql = "insert into processo_coleta (";
		$sql .= "	 id_solicita_visto";
		$sql .= "	,cd_candidato";
		$sql .= "	,no_validade";
		$sql .= "	,nu_visto";
		$sql .= "	,dt_emissao_visto";
		$sql .= "	,no_local_emissao";
		$sql .= "	,co_pais_nacionalidade_visto";
		$sql .= "	,co_classificacao";
		$sql .= "	,no_local_entrada";
		$sql .= "	,co_uf";
		$sql .= "	,dt_entrada";
		$sql .= "	,nu_transporte_entrada";
		$sql .= "	,codigo_processo_mte";
		$sql .= "   ,cd_usuario_cad";
		$sql .= "   ,cd_usuario";
		$sql .= "   ,dt_cad";
		$sql .= "   ,dt_ult";
		$sql .= "	,nu_servico";
		$sql .= "   ,no_classe";
		$sql .= "   ,no_metodo";
		$sql .= "	) values (";	
		$sql .= "	 ".cBANCO::ChaveOk($this->mid_solicita_visto);
		$sql .= "	,".cBANCO::ChaveOk($this->mcd_candidato);
		$sql .= "	,".cBANCO::StringOk($this->mno_validade);
		$sql .= "	,".cBANCO::StringOk($this->mnu_visto);
		$sql .= "	,".cBANCO::DataOk($this->mdt_emissao_visto);
		$sql .= "	,".cBANCO::StringOk($this->mno_local_emissao);
		$sql .= "	,".cBANCO::ChaveOk($this->mco_pais_nacionalidade_visto);
		$sql .= "	,".cBANCO::ChaveOk($this->mco_classificacao);
		$sql .= "	,".cBANCO::StringOk($this->mno_local_entrada);
		$sql .= "	,".cBANCO::ChaveOk($this->mco_uf);
		$sql .= "	,".cBANCO::DataOk($this->mdt_entrada);
		$sql .= "	,".cBANCO::ChaveOk($this->mnu_transporte_entrada);
		$sql .= "	,".cBANCO::ChaveOk($this->mcodigo_processo_mte);
		$sql .= "	,".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= "	,".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= "	, now()";
		$sql .= "	, now()";
		$sql .= "	,".cBANCO::ChaveOk($this->mnu_servico);
		$sql.= "	,".cBANCO::StringOk(__CLASS__);
		$sql.= "	,".cBANCO::StringOk(__FUNCTION__);
		$sql .= "	)";		
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
		$this->mcodigo = mysql_insert_id();
	}
	
	public function IncluaNovoFilho(){
		$this->VerificarCandidato();
		$this->mfl_vazio = '0';
		$this->IncluaNovo();
	}
	
	private function Atualize(){
		$sql = "update processo_coleta set ";
		$sql .= "   id_solicita_visto 			= ".cBANCO::ChaveOk($this->mid_solicita_visto);
		$sql .= " , cd_candidato 				= ".cBANCO::ChaveOk($this->mcd_candidato);
		$sql .= " , no_validade 				= ".cBANCO::StringOk($this->mno_validade);
		$sql .= " , nu_visto 					= ".cBANCO::StringOk($this->mnu_visto);
		$sql .= " , dt_emissao_visto 			= ".cBANCO::DataOk($this->mdt_emissao_visto);
		$sql .= " , no_local_emissao 			= ".cBANCO::StringOk($this->mno_local_emissao);
		$sql .= " , co_pais_nacionalidade_visto = ".cBANCO::ChaveOk($this->mco_pais_nacionalidade_visto);
		$sql .= " , co_classificacao 			= ".cBANCO::ChaveOk($this->mco_classificacao);
		$sql .= " , no_local_entrada 			= ".cBANCO::StringOk($this->mno_local_entrada);
		$sql .= " , co_uf 						= ".cBANCO::ChaveOk($this->mco_uf);
		$sql .= " , dt_entrada 					= ".cBANCO::DataOk($this->mdt_entrada);
		$sql .= " , nu_transporte_entrada 		= ".cBANCO::ChaveOk($this->mnu_transporte_entrada);
		$sql .= " , codigo_processo_mte 		= ".cBANCO::ChaveOk($this->mcodigo_processo_mte);
		$sql .= " , nu_servico					= ".cBANCO::ChaveOk($this->mnu_servico);
		$sql .= " , cd_usuario 				= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= " , dt_ult			 			= now() ";
		$sql .= " , no_classe			= ".cBANCO::StringOk(__CLASS__);
		$sql .= " , no_metodo			= ".cBANCO::StringOk(__FUNCTION__);
		$sql .= " where codigo = ".$this->mcodigo;
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);	
	}

	private function Atualize_ProcessoEdit(){
		$sql = "update processo_coleta set ";
		$sql .= "   no_validade 				= ".cBANCO::StringOk($this->mno_validade);
		$sql .= " , nu_visto 					= ".cBANCO::StringOk($this->mnu_visto);
		$sql .= " , dt_emissao_visto 			= ".cBANCO::DataOk($this->mdt_emissao_visto);
		$sql .= " , no_local_emissao 			= ".cBANCO::StringOk($this->mno_local_emissao);
		$sql .= " , co_pais_nacionalidade_visto = ".cBANCO::ChaveOk($this->mco_pais_nacionalidade_visto);
		$sql .= " , co_classificacao 			= ".cBANCO::ChaveOk($this->mco_classificacao);
		$sql .= " , no_local_entrada 			= ".cBANCO::StringOk($this->mno_local_entrada);
		$sql .= " , co_uf 						= ".cBANCO::ChaveOk($this->mco_uf);
		$sql .= " , dt_entrada 					= ".cBANCO::DataOk($this->mdt_entrada);
		$sql .= " , nu_transporte_entrada 		= ".cBANCO::ChaveOk($this->mnu_transporte_entrada);
		$sql .= " , cd_usuario 				= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= " , dt_ult 				= now()";
		$sql .= " , no_classe			= ".cBANCO::StringOk(__CLASS__);
		$sql .= " , no_metodo			= ".cBANCO::StringOk(__FUNCTION__);
		$sql .= " where codigo = ".$this->mcodigo;
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);	
	}

	public function SituacaoVistoProcesso(&$pprazo_estada, &$ptexto, &$porigem){
	    
		if(trim($this->mnu_servico) == '79'){	//Coleta VITEM II em Houston
			return false;
		}
		else{
			$pprazo_estada = '--';
			$ptexto = "Precisa registrar";
			$porigem = "Processo mais recente é uma coleta de visto";
			return true;
			break;
		}

	}
}
