<?php
// á
class cTIPO_AUTORIZACAO extends cMODELO{
	protected $mCO_TIPO_AUTORIZACAO;
	protected $mNO_TIPO_AUTORIZACAO;
	protected $mNO_REDUZIDO_TIPO_AUTORIZACAO;
	
	
	public function get_nomeTabela()
	{
		return 'TIPO_AUTORIZACAO';
	}

	public function getId()
	{
		return $this->mCO_TIPO_AUTORIZACAO;
	}
	
	public function setId($pValor)
	{
		$this->mCO_TIPO_AUTORIZACAO = $pValor;
	}
	
	public function campoid() 
	{
		return 'CO_TIPO_AUTORIZACAO';
	}
	
	public function sql_RecuperePeloId() {
		$sql = "select * from tipo_autorizacao where co_tipo_autorizacao = ".$this->mCO_TIPO_AUTORIZACAO;
		return $sql;
	}
	
	public function Incluir()
	{
		$sql= "insert into TIPO_AUTORIZACAO(
				 no_tipo_autorizacao 
				,no_reduzido_tipo_autorizacao 
		       ) values (
				 ".cBANCO::StringOk($this->mNO_TIPO_AUTORIZACAO)."
				,".cBANCO::StringOk($this->mNO_REDUZIDO_TIPO_AUTORIZACAO)."
				)";
		cAMBIENTE::ExecuteQuery($sql);
		$this->mCO_TIPO_AUTORIZACAO = cAMBIENTE::$db_pdo->lastInsertId();
	}
	
	public function Atualizar()
	{
		$sql= "update tipo_autorizacao set 
				 no_tipo_autorizacao = ".cBANCO::StringOk($this->mNO_TIPO_AUTORIZACAO)."
				,no_reduzido_tipo_autorizacao = ".cBANCO::StringOk($this->mNO_REDUZIDO_TIPO_AUTORIZACAO)."
			  where co_tipo_autorizacao=".$this->mCO_TIPO_AUTORIZACAO;
		cAMBIENTE::ExecuteQuery($sql);
	}
	
	public function sql_Liste()
	{
		$sql = "select * from tipo_autorizacao where 1=1";
		return $sql;
	}
}
