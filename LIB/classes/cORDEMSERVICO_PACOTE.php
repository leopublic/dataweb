<?php

/**
 * Essa Ã© uma ordem de serviÃ§o que abre um pacote
 * nu_servico_pacote != 0
 *
 * Foi criada para concentrar os metodos relacionados aa administracao do pacote numa classe a parte
 */
class cORDEMSERVICO_PACOTE extends cORDEMSERVICO {

    /**
     * Identifica o pacote que se aplica a OS que estÃ¡ sendo criada
     * @param  cORDEMSERVICO $os [description]
     * @return cORDEMSERVICO retorna a OS do pacote
     */
    public static function pacoteAplicavel(cORDEMSERVICO $os) {
        $servico_pacote = cSERVICO::pacoteQueAbre($os->mNU_SERVICO, $os->mNU_EMPRESA);
        $ospacote = new cORDEMSERVICO();
        if (isset($servico_pacote) && $servico_pacote['NU_SERVICO'] > 0) {
            // Esse servico abre um pacote nessa empresa
            $ospacote->mnu_servico_pacote = $servico_pacote['NU_SERVICO'];
            $ospacote->mid_solicita_visto_pacote = '';
            $ospacote->mtppr_id = cTIPO_PRECO::tpPACOTE;
        } else {
            $ospacote->mnu_servico_pacote = '';
            $ospacote->mtppr_id = cTIPO_PRECO::tpCONCEITO;
            // Verifica se esse servico pertence a algum pacote dessa empresa
            $nu_servico_pacote = self::PacoteQueContemEsseServico($os->mNU_EMPRESA, $os->mNU_SERVICO);
            if ($nu_servico_pacote > 0) {
                // Verifica se existe alguma OS desse candidato de um pacote sem o servico dessa OS
                $id_solicita_visto = self::OsPacoteLivre($nu_servico_pacote, $os->mNU_EMPRESA, $os->mCandidatos, self::RecuperaOsDoPacoteQueJaTemOServico($nu_servico_pacote, $os->mNU_SERVICO, $os->mNU_EMPRESA, $os->mCandidatos));
                $ospacote->mid_solicita_visto_pacote = $id_solicita_visto;
                $ospacote->mtppr_id = cTIPO_PRECO::tpPACOTE;
            }
            // Retorna esse pacote
        }
        return $ospacote;
    }

    public static function OsPacoteLivre($nu_servico_pacote, $nu_empresa, $nu_candidato, $id_solicita_visto_ocupadas) {
        // Verifica se existe alguma OS desse candidato de um pacote sem o servico dessa OS
        $sql = "select sv.id_solicita_visto
                from solicita_visto sv
                join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto and NU_CANDIDATO = " . $nu_candidato . "
                where sv.nu_servico_pacote = " . $nu_servico_pacote . "
                and sv.nu_empresa = " . $nu_empresa;
        if (count($id_solicita_visto_ocupadas) > 0) {
            $sql .= " and sv.id_solicita_visto not in (" . implode(",", $id_solicita_visto_ocupadas) . ")";
        }
        $sql .= " order by id_solicita_visto desc 
                limit 1";
        $res = cAMBIENTE::ConectaQuery($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        return $rs['id_solicita_visto'];
    }

    public static function PacoteQueContemEsseServico($nu_empresa, $nu_servico) {
        $sql = "select nu_servico_pacote 
				from pacote_servico
				join preco on preco.NU_SERVICO = pacote_servico.nu_servico_pacote
				join empresa on empresa.tbpc_id = preco.tbpc_id and nu_empresa = " . $nu_empresa . "
				where nu_servico_incluido = " . $nu_servico . " and pacs_fl_principal = 0";
        $res = cAMBIENTE::ConectaQuery($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if (isset($rs['nu_servico_pacote'])) {
            return $rs['nu_servico_pacote'];
        } else {
            return "";
        }
    }

    // Recupera as ordens de servico de um determinado pacote que jÃ¡ tem OS de um determinado servico
    public static function RecuperaOsDoPacoteQueJaTemOServico($nu_servico_pacote, $nu_servico, $nu_empresa, $nu_candidato) {
        $sql = "select svPacote.id_solicita_visto
			    from solicita_visto svServico
			    join solicita_visto svPacote on svPacote.id_solicita_visto = svServico.id_solicita_visto_pacote
			    join autorizacao_candidato ac on ac.id_solicita_visto = svServico.id_solicita_visto  and ac.NU_CANDIDATO = " . $nu_candidato . "
				join pacote_servico ps on ps.nu_servico_pacote = " . $nu_servico_pacote . " 
						and nu_servico_incluido = svPacote.nu_servico and pacs_fl_principal = 1
			    where svServico.nu_servico = " . $nu_servico . "
			      and svServico.nu_empresa = " . $nu_empresa;

        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $ret = array();
        while ($rs = $res->fetch(PDO::FETCH_NUM)) {
            $ret[] = $rs[0];
        }
        return $ret;
    }

    public static function resolvePacote(cORDEMSERVICO $os) {
        
    }

}
