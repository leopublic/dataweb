<?php

/**
 * Estrangeiro registrado no sistema
 * @author Leonardo Medeiros
 */
class cCANDIDATO_TMP extends cCANDIDATO_BASE {

    public $mnu_candidato_tmp;
    public $mFL_EDICAO_CONCLUIDA;
    public $mDT_CONCLUSAO_CADASTRO_CLIENTE;
    public $mTE_EXPERIENCIA_ANTERIOR;
    public $mCO_LOCAL_EMBARCACAO_PROJETO;
    public $mNO_ESCOLARIDADE_EM_INGLES;
    public $mNO_REPARTICAO_CONSULAR;
    public $mNO_ESTADO_CIVIL_EM_INGLES;
    public $mNO_PROFISSAO_EM_INGLES;
    public $mNO_FUNCAO_EM_INGLES;
    public $mNO_EMBARCACAO_PROJETO;
    public $mconv_id;
    public $mnu_servico;
    public $mFL_CADASTRO_REVISADO;
    public $mcand_fl_liberado;
    public $mid_solicita_visto;
    public $mdt_envio_aprovacao;
    public $mcd_usuario_envio_aprovacao;
    public $mhash_string;
    public $mhash_dt_cadastro;

    protected $mconvidado;
    protected $mservico;
    protected $musuario_cadastro;

    const mEscopo = 'cCANDIDATO';
    const atuBLOQUEADA = "0";
    const atuEM_ATUALIZACAO = "1";
    const atuCONCLUIDA = "2";

    public function getid() {
        return $this->mnu_candidato_tmp;
    }

    public function setId($pValor) {
        $this->mNU_CANDIDATO = $pValor;
    }

    public function get_nomeTabela() {
        return "CANDIDATO_TMP";
    }

    public static function get_nomeCampoChave() {
        return 'nu_candidato_tmp';
    }


    public function RecuperePeloId($pNU_CANDIDATO = '') {
        $this->Recuperar($pNU_CANDIDATO);
    }

    /**
     * Recupera campos do candidato
     */
    public function Recuperar($pNU_CANDIDATO = '') {
        if ($pNU_CANDIDATO != '') {
            $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        }
        if ($this->mNU_CANDIDATO == '') {
            throw new Exception("ID do candidato não informado");
        }
        $sql = "select c.* 
                    , uc.nome as nome_cad
                    , ua.nome as nome_alt
                from CANDIDATO_TMP c
                left join usuarios uc on uc.cd_usuario = c.CO_USU_CADASTAMENTO
                left join usuarios ua on ua.cd_usuario = c.CO_USU_ULT_ALTERACAO
                where NU_CANDIDATO=" . $this->mNU_CANDIDATO;
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->carreguePropriedades($rs);
            }
        }
    }

    public function RecuperarPeloTmp($pnu_candidato_tmp = '') {
        if ($pnu_candidato_tmp > 0) {
            $this->mnu_candidato_tmp = $pnu_candidato_tmp;
        }
        if ($this->mnu_candidato_tmp == '') {
            throw new Exception("ID do candidato não informado");
        }
        $sql = "select c.* 
                from CANDIDATO_TMP c
                where nu_candidato_tmp=" . $this->mnu_candidato_tmp;
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->carreguePropriedades($rs);
            }
        }
    }

    public function carreguePropriedades($rs){
        cBANCO::CarreguePropriedades($rs, $this);
        $this->mDT_NASCIMENTO = cBANCO::DataFmt($rs["DT_NASCIMENTO"]);
        $this->mDT_EXPIRACAO_CNH = cBANCO::DataFmt($rs["DT_EXPIRACAO_CNH"]);
        $this->mDT_EXPIRACAO_CTPS = cBANCO::DataFmt($rs["DT_EXPIRACAO_CTPS"]);
        $this->mDT_VALIDADE_PASSAPORTE = cBANCO::DataFmt($rs["DT_VALIDADE_PASSAPORTE"]);
        $this->mDT_EMISSAO_PASSAPORTE = cBANCO::DataFmt($rs["DT_EMISSAO_PASSAPORTE"]);
        $this->mDT_VALIDADE_SEAMAN = cBANCO::DataFmt($rs["DT_VALIDADE_SEAMAN"]);
        $this->mDT_EMISSAO_SEAMAN = cBANCO::DataFmt($rs["DT_EMISSAO_SEAMAN"]);
    }

    public function CursorIdentificacaoCandidato($pNU_CANDIDATO) {
        if ($pNU_CANDIDATO != '') {
            $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        }
        if ($this->mNU_CANDIDATO == '') {
            throw new Exception("ID do candidato não informado");
        }
        $sql = "select c.* 
					 , uc.nome as nome_cad
					 , ua.nome as nome_alt
				from CANDIDATO c
				left join usuarios uc on uc.cd_usuario = c.CO_USU_CADASTAMENTO
				left join usuarios ua on ua.cd_usuario = c.CO_USU_ULT_ALTERACAO
				where NU_CANDIDATO=" . $this->mNU_CANDIDATO;
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    /**
     * Recupera campos do candidato
     */
    public function Recuperar_Externo($pNU_CANDIDATO = '') {
        if ($pNU_CANDIDATO != '') {
            $this->mNU_CANDIDATO = $pNU_CANDIDATO;
        }
        if ($this->mNU_CANDIDATO == '') {
            throw new Exception("ID do candidato não informado");
        }
        $this->DisponibilizeCadastroParaCliente();

        $sql = "select * from vcandidato_tmp_cons where nu_candidato_tmp=" . $this->mnu_candidato_tmp;
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                cBANCO::CarreguePropriedades($rs, $this);
            } else {
                
            }
        } else {
            
        }
    }

    public function CriaCandidatoCliente() {
        $sql = "insert into candidato_tmp (
					NU_CANDIDATO
					,NO_PRIMEIRO_NOME
					,NO_NOME_MEIO
					,NO_ULTIMO_NOME
					,NOME_COMPLETO
					,NO_EMAIL_CANDIDATO
					,email_pessoal
                                        , NO_SENHA
					,NO_ENDERECO_RESIDENCIA
					,NO_CIDADE_RESIDENCIA
					,CO_PAIS_RESIDENCIA
					,NU_TELEFONE_CANDIDATO
					,NO_EMPRESA_ESTRANGEIRA
					,NO_ENDERECO_EMPRESA
					,NO_CIDADE_EMPRESA
					,CO_PAIS_EMPRESA
					,NU_TELEFONE_EMPRESA
					,NO_PAI
					,NO_MAE
					,CO_NACIONALIDADE
					,CO_NIVEL_ESCOLARIDADE
					,CO_ESTADO_CIVIL
					,CO_SEXO
					,DT_NASCIMENTO
					,NO_LOCAL_NASCIMENTO
					,NU_PASSAPORTE
					,DT_EMISSAO_PASSAPORTE
					,DT_VALIDADE_PASSAPORTE
					,CO_PAIS_EMISSOR_PASSAPORTE
					,NU_RNE
					,CO_PROFISSAO_CANDIDATO
					,TE_TRABALHO_ANTERIOR_BRASIL
					,NU_CPF
					,DT_CADASTRAMENTO
					,CO_USU_CADASTAMENTO
					,DT_ULT_ALTERACAO
					,CO_USU_ULT_ALTERACAO
					,NO_ENDERECO_ESTRANGEIRO
					,NO_CIDADE_ESTRANGEIRO
					,CO_PAIS_ESTRANGEIRO
					,NU_TELEFONE_ESTRANGEIRO
					,BO_CADASTRO_MINIMO_OK
					,NU_EMPRESA
					,NU_EMBARCACAO_PROJETO
					,NU_CTPS
					,NU_CNH
					,DT_EXPIRACAO_CNH
					,DT_EXPIRACAO_CTPS
					,CO_FUNCAO
					,TE_DESCRICAO_ATIVIDADES
					,VA_REMUNERACAO_MENSAL
					,VA_REMUNERACAO_MENSAL_BRASIL
					,NO_SEAMAN
					,DT_VALIDADE_SEAMAN
					,DT_EMISSAO_SEAMAN
					,CO_PAIS_SEAMAN
					,loca_id
					,co_pais_nacionalidade_pai
					,co_pais_nacionalidade_mae
					)
				select
					NU_CANDIDATO
					,NO_PRIMEIRO_NOME
					,NO_NOME_MEIO
					,NO_ULTIMO_NOME
					,NOME_COMPLETO
					,NO_EMAIL_CANDIDATO
					,email_pessoal
                                        ,NO_SENHA
					,NO_ENDERECO_RESIDENCIA
					,NO_CIDADE_RESIDENCIA
					,CO_PAIS_RESIDENCIA
					,NU_TELEFONE_CANDIDATO
					,NO_EMPRESA_ESTRANGEIRA
					,NO_ENDERECO_EMPRESA
					,NO_CIDADE_EMPRESA
					,CO_PAIS_EMPRESA
					,NU_TELEFONE_EMPRESA
					,NO_PAI
					,NO_MAE
					,CO_NACIONALIDADE
					,CO_NIVEL_ESCOLARIDADE
					,CO_ESTADO_CIVIL
					,CO_SEXO
					,DT_NASCIMENTO
					,NO_LOCAL_NASCIMENTO
					,NU_PASSAPORTE
					,DT_EMISSAO_PASSAPORTE
					,DT_VALIDADE_PASSAPORTE
					,CO_PAIS_EMISSOR_PASSAPORTE
					,NU_RNE
					,CO_PROFISSAO_CANDIDATO
					,TE_TRABALHO_ANTERIOR_BRASIL
					,NU_CPF
					,DT_CADASTRAMENTO
					,CO_USU_CADASTAMENTO
					,DT_ULT_ALTERACAO
					,CO_USU_ULT_ALTERACAO
					,NO_ENDERECO_ESTRANGEIRO
					,NO_CIDADE_ESTRANGEIRO
					,CO_PAIS_ESTRANGEIRO
					,NU_TELEFONE_ESTRANGEIRO
					,BO_CADASTRO_MINIMO_OK
					,NU_EMPRESA
					,NU_EMBARCACAO_PROJETO
					,NU_CTPS
					,NU_CNH
					,DT_EXPIRACAO_CNH
					,DT_EXPIRACAO_CTPS
					,CO_FUNCAO
					,TE_DESCRICAO_ATIVIDADES
					,VA_REMUNERACAO_MENSAL
					,VA_REMUNERACAO_MENSAL_BRASIL
					,NO_SEAMAN
					,DT_VALIDADE_SEAMAN
					,DT_EMISSAO_SEAMAN
					,CO_PAIS_SEAMAN
					,loca_id
					,co_pais_nacionalidade_pai
					,co_pais_nacionalidade_mae
					from CANDIDATO where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $this->mnu_candidato_tmp = mysql_insert_id();
    }

    /**
     * Atualiza atributos da instância no banco
     */
    public function AtualizarExterno() {
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);

        if ($this->mnu_candidato_tmp > 0) {
            $sql = "update candidato_tmp SET ";
            $sql .= "   NO_PRIMEIRO_NOME 			= " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
            $sql .= " , NO_NOME_MEIO 				= " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
            $sql .= " , NO_ULTIMO_NOME 				= " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);   //ok
            $sql .= " , NOME_COMPLETO 				= " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
            $sql .= " , NO_EMAIL_CANDIDATO           = " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);  //ok
            $sql .= " , NO_ENDERECO_RESIDENCIA 		= " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
            $sql .= " , NO_CIDADE_RESIDENCIA 		= " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
            $sql .= " , CO_PAIS_RESIDENCIA 			= " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA); //ok
            $sql .= " , NU_TELEFONE_CANDIDATO 		= " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
            $sql .= " , NO_EMPRESA_ESTRANGEIRA		= " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok
            $sql .= " , NO_ENDERECO_EMPRESA 		= " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA); //ok
            $sql .= " , NO_CIDADE_EMPRESA 			= " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA); //ok
            $sql .= " , CO_PAIS_EMPRESA 			= " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA); //ok
            $sql .= " , NU_TELEFONE_EMPRESA 		= " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA); //ok
            $sql .= " , NO_PAI 						= " . cBANCO::StringOk($this->mNO_PAI); //ok
            $sql .= " , NO_MAE 						= " . cBANCO::StringOk($this->mNO_MAE); //ok
            $sql .= " , CO_NACIONALIDADE 			= " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE); //ok
            $sql .= " , CO_NIVEL_ESCOLARIDADE 		= " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE); //ok
            $sql .= " , CO_ESTADO_CIVIL 			= " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL); //ok
            $sql .= " , CO_SEXO 					= " . cBANCO::StringOk($this->mCO_SEXO); //ok
            $sql .= " , DT_NASCIMENTO 				= " . cBANCO::DataOk($this->mDT_NASCIMENTO); //ok
            $sql .= " , NO_LOCAL_NASCIMENTO 		= " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO); //ok
            $sql .= " , NU_PASSAPORTE 				= " . cBANCO::StringOk($this->mNU_PASSAPORTE); //ok
            $sql .= " , DT_EMISSAO_PASSAPORTE 		= " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE); //ok
            $sql .= " , DT_VALIDADE_PASSAPORTE 		= " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE); //ok
            $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE 	= " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
            $sql .= " , NU_RNE 						= " . cBANCO::StringOk($this->mNU_RNE);
            $sql .= " , CO_PROFISSAO_CANDIDATO      = " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
            $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL = " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok
            $sql .= " , NU_CPF 						= " . cBANCO::StringOk($this->mNU_CPF);      //ok
            $sql .= " , DT_ULT_ALTERACAO 			= now()";
            $sql .= " , CO_USU_ULT_ALTERACAO 		= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
            $sql .= " , NO_ENDERECO_ESTRANGEIRO 	= " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
            $sql .= " , NO_CIDADE_ESTRANGEIRO 		= " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
            $sql .= " , CO_PAIS_ESTRANGEIRO 		= " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO); //ok
            $sql .= " , NU_TELEFONE_ESTRANGEIRO 	= " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
            $sql .= " , VA_REMUNERACAO_MENSAL		= " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL); //ok
            $sql .= " , VA_REMUNERACAO_MENSAL_BRASIL = " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL_BRASIL); //ok
            $sql .= " , CO_PAIS_SEAMAN				= " . cBANCO::ChaveOk($this->mCO_PAIS_SEAMAN); //ok
            $sql .= " , DT_EMISSAO_SEAMAN			= " . cBANCO::DataOk($this->mDT_EMISSAO_SEAMAN); //ok
            $sql .= " , DT_VALIDADE_SEAMAN			= " . cBANCO::DataOk($this->mDT_VALIDADE_SEAMAN); //ok
            $sql .= " , NO_SEAMAN					= " . cBANCO::StringOk($this->mNO_SEAMAN); //ok
            $sql .= " , CO_FUNCAO					= " . cBANCO::ChaveOk($this->mCO_FUNCAO); //ok
            $sql .= " , CO_LOCAL_EMBARCACAO_PROJETO	= " . cBANCO::ChaveOk($this->mCO_LOCAL_EMBARCACAO_PROJETO); //ok
            $sql .= " , co_pais_nacionalidade_pai	= " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai); //ok
            $sql .= " , co_pais_nacionalidade_mae	= " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae); //ok
            $sql .= " , NO_ESCOLARIDADE_EM_INGLES	= " . cBANCO::StringOk($this->mNO_ESCOLARIDADE_EM_INGLES); //ok
            $sql .= " , NO_REPARTICAO_CONSULAR	= " . cBANCO::StringOk($this->mNO_REPARTICAO_CONSULAR); //ok
            $sql .= " , NO_ESTADO_CIVIL_EM_INGLES	= " . cBANCO::StringOk($this->mNO_ESTADO_CIVIL_EM_INGLES); //ok
            $sql .= " , NO_FUNCAO_EM_INGLES	        = " . cBANCO::StringOk($this->mNO_FUNCAO_EM_INGLES); //ok
            $sql .= " , NO_PROFISSAO_EM_INGLES	= " . cBANCO::StringOk($this->mNO_PROFISSAO_EM_INGLES); //ok
            $sql .= " , NO_EMBARCACAO_PROJETO	= " . cBANCO::StringOk($this->mNO_EMBARCACAO_PROJETO); //ok
            $sql .= " , NU_EMBARCACAO_PROJETO	= " . cBANCO::StringOk($this->mNU_EMBARCACAO_PROJETO); //ok
            $sql .= " , TE_DESCRICAO_ATIVIDADES     = " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES); //ok
            $sql .= " , nu_servico	= " . cBANCO::StringOk($this->mnu_servico); //ok
            $sql .= " , email_pessoal           = " . cBANCO::StringOk($this->memail_pessoal);  //ok
            $sql .= " , loca_id	= " . cBANCO::ChaveOk($this->mloca_id); //ok
            if ($this->mNU_CANDIDATO > 0) {
                $sql .= " where nu_candidato 		= " . $this->mNU_CANDIDATO;
            } else {
                $sql .= " where nu_candidato_tmp 		= " . $this->mnu_candidato_tmp;
            }
            executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        } else {
            $sql = "insert into candidato_tmp (";
            $sql .= "   NU_EMPRESA 			";
            $sql .= " , NO_PRIMEIRO_NOME 			";
            $sql .= " , NO_NOME_MEIO 				";
            $sql .= " , NO_ULTIMO_NOME 				";
            $sql .= " , NOME_COMPLETO 				";
            $sql .= " , email_pessoal			";
            $sql .= " , NO_ENDERECO_RESIDENCIA 	";
            $sql .= " , NO_CIDADE_RESIDENCIA 		";
            $sql .= " , CO_PAIS_RESIDENCIA 			";
            $sql .= " , NU_TELEFONE_CANDIDATO 	";
            $sql .= " , NO_EMPRESA_ESTRANGEIRA	";
            $sql .= " , NO_ENDERECO_EMPRESA 		";
            $sql .= " , NO_CIDADE_EMPRESA 			";
            $sql .= " , CO_PAIS_EMPRESA 			";
            $sql .= " , NU_TELEFONE_EMPRESA 	";
            $sql .= " , NO_PAI 					";
            $sql .= " , NO_MAE 					";
            $sql .= " , CO_NACIONALIDADE 	";
            $sql .= " , CO_NIVEL_ESCOLARIDADE 		";
            $sql .= " , CO_ESTADO_CIVIL 			";
            $sql .= " , CO_SEXO 				";
            $sql .= " , DT_NASCIMENTO 			";
            $sql .= " , NO_LOCAL_NASCIMENTO 		";
            $sql .= " , NU_PASSAPORTE 				";
            $sql .= " , DT_EMISSAO_PASSAPORTE 		";
            $sql .= " , DT_VALIDADE_PASSAPORTE 		";
            $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE 	";
            $sql .= " , NU_RNE 						";
            $sql .= " , TE_DESCRICAO_ATIVIDADES		";
            $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL ";
            $sql .= " , NU_CPF 				";
            $sql .= " , DT_CADASTRAMENTO    ";
            $sql .= " , CO_USU_CADASTAMENTO    ";
            $sql .= " , DT_ULT_ALTERACAO 	";
            $sql .= " , CO_USU_ULT_ALTERACAO 	";
            $sql .= " , NO_ENDERECO_ESTRANGEIRO 	";
            $sql .= " , NO_CIDADE_ESTRANGEIRO 		";
            $sql .= " , CO_PAIS_ESTRANGEIRO 		";
            $sql .= " , NU_TELEFONE_ESTRANGEIRO 	";
            $sql .= " , VA_REMUNERACAO_MENSAL		";
            $sql .= " , VA_REMUNERACAO_MENSAL_BRASIL ";
            $sql .= " , CO_PAIS_SEAMAN		";
            $sql .= " , DT_EMISSAO_SEAMAN	";
            $sql .= " , DT_VALIDADE_SEAMAN	";
            $sql .= " , NO_SEAMAN		";
            $sql .= " , CO_FUNCAO		";
            $sql .= " , CO_PROFISSAO_CANDIDATO	";
            $sql .= " , CO_LOCAL_EMBARCACAO_PROJETO";
            $sql .= " , co_pais_nacionalidade_pai	";
            $sql .= " , co_pais_nacionalidade_mae	";
            $sql .= " , NO_ESCOLARIDADE_EM_INGLES	";
            $sql .= " , NO_REPARTICAO_CONSULAR	";
            $sql .= " , NO_ESTADO_CIVIL_EM_INGLES	";
            $sql .= " , NO_FUNCAO_EM_INGLES	        ";
            $sql .= " , NO_PROFISSAO_EM_INGLES	";
            $sql .= " , NO_EMBARCACAO_PROJETO	";
            $sql .= " , loca_id	";
            $sql .= " , nu_servico ";
            $sql .= " ) values (";
            $sql .= "  " . cBANCO::ChaveOk($this->mNU_EMPRESA);   //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);    //ok
            $sql .= ", " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
            $sql .= ", " . cBANCO::StringOk($this->memail_pessoal);   //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_PAI); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_MAE); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL); //ok
            $sql .= ", " . cBANCO::StringOk($this->mCO_SEXO); //ok
            $sql .= ", " . cBANCO::DataOk($this->mDT_NASCIMENTO); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNU_PASSAPORTE); //ok
            $sql .= ", " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE); //ok
            $sql .= ", " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNU_RNE);
            $sql .= ", " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES); //ok
            $sql .= ", " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNU_CPF);      //ok
            $sql .= ",  now()";
            $sql .= ", " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
            $sql .= ",  now()";
            $sql .= ", " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
            $sql .= ", " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
            $sql .= ", " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL); //ok
            $sql .= ", " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL_BRASIL); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_PAIS_SEAMAN); //ok
            $sql .= ", " . cBANCO::DataOk($this->mDT_EMISSAO_SEAMAN); //ok
            $sql .= ", " . cBANCO::DataOk($this->mDT_VALIDADE_SEAMAN); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_SEAMAN); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_FUNCAO); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mCO_LOCAL_EMBARCACAO_PROJETO); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_ESCOLARIDADE_EM_INGLES); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_REPARTICAO_CONSULAR); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_ESTADO_CIVIL_EM_INGLES); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_FUNCAO_EM_INGLES); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_PROFISSAO_EM_INGLES); //ok
            $sql .= ", " . cBANCO::StringOk($this->mNO_EMBARCACAO_PROJETO); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mloca_id); //ok
            $sql .= ", " . cBANCO::ChaveOk($this->mnu_servico); //ok
            $sql .= ")";
            cAMBIENTE::$db_pdo->exec($sql);
            $this->mnu_candidato_tmp = cAMBIENTE::$db_pdo->lastInsertId();
        }
    }

    /**
     * Atualiza todos os atributos do objeto no banco.
     * Só deve ser usado se todos os atributos tiverem sido recuperados antes.
     */
    public function salvar() {
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);

        if ($this->mnu_candidato_tmp > 0) {
            $sql = "update candidato_tmp SET ";
            $sql .= "   NU_CANDIDATO                = " . cBANCO::ChaveOk($this->mNU_CANDIDATO);   //ok
            $sql .= " , NO_PRIMEIRO_NOME            = " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
            $sql .= " , NO_NOME_MEIO                = " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
            $sql .= " , NO_ULTIMO_NOME              = " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);   //ok
            $sql .= " , NOME_COMPLETO               = " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
            $sql .= " , NO_EMAIL_CANDIDATO          = " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);  //ok
            $sql .= " , NO_ENDERECO_RESIDENCIA      = " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
            $sql .= " , NO_CIDADE_RESIDENCIA        = " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
            $sql .= " , CO_PAIS_RESIDENCIA          = " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA); //ok
            $sql .= " , NU_TELEFONE_CANDIDATO       = " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
            $sql .= " , NO_EMPRESA_ESTRANGEIRA      = " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok
            $sql .= " , NO_ENDERECO_EMPRESA         = " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA); //ok
            $sql .= " , NO_CIDADE_EMPRESA           = " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA); //ok
            $sql .= " , CO_PAIS_EMPRESA             = " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA); //ok
            $sql .= " , NU_TELEFONE_EMPRESA         = " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA); //ok
            $sql .= " , NO_PAI                      = " . cBANCO::StringOk($this->mNO_PAI); //ok
            $sql .= " , NO_MAE                      = " . cBANCO::StringOk($this->mNO_MAE); //ok
            $sql .= " , CO_NACIONALIDADE            = " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE); //ok
            $sql .= " , CO_NIVEL_ESCOLARIDADE       = " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE); //ok
            $sql .= " , CO_ESTADO_CIVIL             = " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL); //ok
            $sql .= " , CO_SEXO                     = " . cBANCO::StringOk($this->mCO_SEXO); //ok
            $sql .= " , DT_NASCIMENTO               = " . cBANCO::DataOk($this->mDT_NASCIMENTO); //ok
            $sql .= " , NO_LOCAL_NASCIMENTO         = " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO); //ok
            $sql .= " , NU_PASSAPORTE               = " . cBANCO::StringOk($this->mNU_PASSAPORTE); //ok
            $sql .= " , DT_EMISSAO_PASSAPORTE       = " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE); //ok
            $sql .= " , DT_VALIDADE_PASSAPORTE      = " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE); //ok
            $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE  = " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
            $sql .= " , NU_RNE                      = " . cBANCO::StringOk($this->mNU_RNE);
            $sql .= " , CO_PROFISSAO_CANDIDATO      = " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
            $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL = " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok
            $sql .= " , NU_CPF                      = " . cBANCO::StringOk($this->mNU_CPF);      //ok
            $sql .= " , DT_ULT_ALTERACAO            = now()";
            $sql .= " , CO_USU_ULT_ALTERACAO        = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
            $sql .= " , NO_ENDERECO_ESTRANGEIRO     = " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
            $sql .= " , NO_CIDADE_ESTRANGEIRO       = " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
            $sql .= " , CO_PAIS_ESTRANGEIRO         = " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO); //ok
            $sql .= " , NU_TELEFONE_ESTRANGEIRO     = " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
            $sql .= " , BO_CADASTRO_MINIMO_OK       = " . cBANCO::SimNaoOk($this->mBO_CADASTRO_MINIMO_OK); //ok
            $sql .= " , NU_EMPRESA                  = " . cBANCO::ChaveOk($this->mNU_EMPRESA); //ok
            $sql .= " , NU_EMBARCACAO_PROJETO       = " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO); //ok
            $sql .= " , NU_CTPS                     = " . cBANCO::StringOk($this->mNU_CTPS); //ok
            $sql .= " , NU_CNH                      = " . cBANCO::StringOk($this->mNU_CNH); //ok
            $sql .= " , DT_EXPIRACAO_CNH            = " . cBANCO::DataOk($this->mDT_EXPIRACAO_CNH); //ok
            $sql .= " , DT_EXPIRACAO_CTPS           = " . cBANCO::DataOk($this->mDT_EXPIRACAO_CTPS); //ok
            $sql .= " , FL_CADASTRO_REVISADO        = " . cBANCO::SimNaoOk($this->mFL_CADASTRO_REVISADO); //ok
            $sql .= " , CO_LOCAL_EMBARCACAO_PROJETO = " . cBANCO::ChaveOk($this->mCO_LOCAL_EMBARCACAO_PROJETO); //ok
            $sql .= " , TE_DESCRICAO_ATIVIDADES     = " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES); //ok
            $sql .= " , CO_FUNCAO                   = " . cBANCO::ChaveOk($this->mCO_FUNCAO); //ok
            $sql .= " , VA_REMUNERACAO_MENSAL       = " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL); //ok
            $sql .= " , VA_REMUNERACAO_MENSAL_BRASIL = " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL_BRASIL); //ok
            $sql .= " , NO_SEAMAN                   = " . cBANCO::StringOk($this->mNO_SEAMAN); //ok
            $sql .= " , DT_EMISSAO_SEAMAN           = " . cBANCO::DataOk($this->mDT_EMISSAO_SEAMAN); //ok
            $sql .= " , DT_VALIDADE_SEAMAN          = " . cBANCO::DataOk($this->mDT_VALIDADE_SEAMAN); //ok
            $sql .= " , CO_PAIS_SEAMAN              = " . cBANCO::ChaveOk($this->mCO_PAIS_SEAMAN); //ok
            $sql .= " , loca_id                     = " . cBANCO::ChaveOk($this->mloca_id); //ok
            $sql .= " , FL_EDICAO_CONCLUIDA         = " . cBANCO::SimNaoOk($this->mFL_EDICAO_CONCLUIDA); //ok
            $sql .= " , DT_CONCLUSAO_CADASTRO_CLIENTE = " . cBANCO::DataOk($this->mDT_CONCLUSAO_CADASTRO_CLIENTE); //ok
            $sql .= " , co_pais_nacionalidade_pai   = " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai); //ok
            $sql .= " , co_pais_nacionalidade_mae   = " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae); //ok
            $sql .= " , cand_tx_residencia_estado   = " . cBANCO::StringOk($this->mcand_tx_residencia_estado); //ok
            $sql .= " , cand_fl_liberado            = " . cBANCO::SimNaoOk($this->mcand_fl_liberado); //ok
            $sql .= " , NO_ESCOLARIDADE_EM_INGLES   = " . cBANCO::StringOk($this->mNO_ESCOLARIDADE_EM_INGLES); //ok
            $sql .= " , NO_REPARTICAO_CONSULAR      = " . cBANCO::StringOk($this->mNO_REPARTICAO_CONSULAR); //ok
            $sql .= " , NO_ESTADO_CIVIL_EM_INGLES   = " . cBANCO::StringOk($this->mNO_ESTADO_CIVIL_EM_INGLES); //ok
            $sql .= " , NO_FUNCAO_EM_INGLES         = " . cBANCO::StringOk($this->mNO_FUNCAO_EM_INGLES); //ok
            $sql .= " , NO_PROFISSAO_EM_INGLES      = " . cBANCO::StringOk($this->mNO_PROFISSAO_EM_INGLES); //ok
            $sql .= " , NO_EMBARCACAO_PROJETO       = " . cBANCO::StringOk($this->mNO_EMBARCACAO_PROJETO); //ok
            $sql .= " , email_pessoal               = " . cBANCO::StringOk($this->memail_pessoal);  //ok
            $sql .= " , NO_SENHA                    = " . cBANCO::StringOk($this->mNO_SENHA);  //ok
            $sql .= " , dt_envio_edicao             = " . cBANCO::DataOk($this->dt_envio_edicao); //ok
            $sql .= " , id_status_edicao            = " . cBANCO::ChaveOk($this->mid_status_edicao); //ok
            $sql .= " , conv_id                     = " . cBANCO::ChaveOk($this->mconv_id); //ok
            $sql .= " , nu_servico                  = " . cBANCO::ChaveOk($this->mnu_servico); //ok
            $sql .= " , id_solicita_visto           = " . cBANCO::ChaveOk($this->mid_solicita_visto); //ok
            $sql .= " , dt_envio_aprovacao          = " . cBANCO::DataOk($this->mdt_envio_aprovacao); //ok
            $sql .= " , cd_usuario_envio_aprovacao  = " . cBANCO::ChaveOk($this->mcd_usuario_envio_aprovacao); //ok
            $sql .= " , hash_string  = '" . $this->mhash_string."'"; //ok
            $sql .= " , hash_dt_cadastro  = " . cBANCO::DataOk($this->mhash_dt_cadastro); //ok
            $sql .= " where nu_candidato_tmp        = " . $this->mnu_candidato_tmp;
            cAMBIENTE::$db_pdo->exec($sql);
        } else {

            if ($this->mBO_CADASTRO_MINIMO_OK == ''){
                $this->mBO_CADASTRO_MINIMO_OK = '0';
            }

            if ($this->mid_status_edicao == ''){
                $this->mid_status_edicao = 1;
            }
            $sql = "insert into candidato_tmp (";
            $sql .= "   NU_CANDIDATO                ";
            $sql .= " , NO_PRIMEIRO_NOME            ";
            $sql .= " , NO_NOME_MEIO                ";
            $sql .= " , NO_ULTIMO_NOME              ";
            $sql .= " , NOME_COMPLETO               ";
            $sql .= " , NO_EMAIL_CANDIDATO          ";
            $sql .= " , NO_ENDERECO_RESIDENCIA      ";
            $sql .= " , NO_CIDADE_RESIDENCIA        ";
            $sql .= " , CO_PAIS_RESIDENCIA          ";
            $sql .= " , NU_TELEFONE_CANDIDATO       ";
            $sql .= " , NO_EMPRESA_ESTRANGEIRA      ";
            $sql .= " , NO_ENDERECO_EMPRESA         ";
            $sql .= " , NO_CIDADE_EMPRESA           ";
            $sql .= " , CO_PAIS_EMPRESA             ";
            $sql .= " , NU_TELEFONE_EMPRESA         ";
            $sql .= " , NO_PAI                      ";
            $sql .= " , NO_MAE                      ";
            $sql .= " , CO_NACIONALIDADE            ";
            $sql .= " , CO_NIVEL_ESCOLARIDADE       ";
            $sql .= " , CO_ESTADO_CIVIL             ";
            $sql .= " , CO_SEXO                     ";
            $sql .= " , DT_NASCIMENTO               ";
            $sql .= " , NO_LOCAL_NASCIMENTO         ";
            $sql .= " , NU_PASSAPORTE               ";
            $sql .= " , DT_EMISSAO_PASSAPORTE       ";
            $sql .= " , DT_VALIDADE_PASSAPORTE      ";
            $sql .= " , CO_PAIS_EMISSOR_PASSAPORTE  ";
            $sql .= " , NU_RNE                      ";
            $sql .= " , CO_PROFISSAO_CANDIDATO      ";
            $sql .= " , TE_TRABALHO_ANTERIOR_BRASIL ";
            $sql .= " , NU_CPF                      ";
            $sql .= " , DT_CADASTRAMENTO            ";
            $sql .= " , CO_USU_CADASTAMENTO         ";
            $sql .= " , DT_ULT_ALTERACAO            ";
            $sql .= " , CO_USU_ULT_ALTERACAO        ";
            $sql .= " , NO_ENDERECO_ESTRANGEIRO     ";
            $sql .= " , NO_CIDADE_ESTRANGEIRO       ";
            $sql .= " , CO_PAIS_ESTRANGEIRO         ";
            $sql .= " , NU_TELEFONE_ESTRANGEIRO     ";
            $sql .= " , BO_CADASTRO_MINIMO_OK       ";
            $sql .= " , NU_EMPRESA                  ";
            $sql .= " , NU_EMBARCACAO_PROJETO       ";
            $sql .= " , NU_CTPS                     ";
            $sql .= " , NU_CNH                      ";
            $sql .= " , DT_EXPIRACAO_CNH            ";
            $sql .= " , DT_EXPIRACAO_CTPS           ";
            $sql .= " , FL_CADASTRO_REVISADO        ";
            $sql .= " , CO_LOCAL_EMBARCACAO_PROJETO ";
            $sql .= " , TE_DESCRICAO_ATIVIDADES     ";
            $sql .= " , CO_FUNCAO                   ";
            $sql .= " , VA_REMUNERACAO_MENSAL       ";
            $sql .= " , VA_REMUNERACAO_MENSAL_BRASIL ";
            $sql .= " , NO_SEAMAN                   ";
            $sql .= " , DT_EMISSAO_SEAMAN           ";
            $sql .= " , DT_VALIDADE_SEAMAN          ";
            $sql .= " , CO_PAIS_SEAMAN              ";
            $sql .= " , loca_id                     ";
            $sql .= " , FL_EDICAO_CONCLUIDA         ";
            $sql .= " , DT_CONCLUSAO_CADASTRO_CLIENTE ";
            $sql .= " , co_pais_nacionalidade_pai   ";
            $sql .= " , co_pais_nacionalidade_mae   ";
            $sql .= " , cand_tx_residencia_estado   ";
            $sql .= " , cand_fl_liberado            ";
            $sql .= " , NO_ESCOLARIDADE_EM_INGLES   ";
            $sql .= " , NO_REPARTICAO_CONSULAR      ";
            $sql .= " , NO_ESTADO_CIVIL_EM_INGLES   ";
            $sql .= " , NO_FUNCAO_EM_INGLES         ";
            $sql .= " , NO_PROFISSAO_EM_INGLES      ";
            $sql .= " , NO_EMBARCACAO_PROJETO       ";
            $sql .= " , email_pessoal               ";
            $sql .= " , NO_SENHA                    ";
            $sql .= " , dt_envio_edicao             ";
            $sql .= " , id_status_edicao            ";
            $sql .= " , conv_id                     ";
            $sql .= " , nu_servico                  ";
            $sql .= " , id_solicita_visto           ";
            $sql .= " , dt_envio_aprovacao           ";
            $sql .= " , cd_usuario_envio_aprovacao           ";
            $sql .= " , hash_string "; //ok
            $sql .= " , hash_dt_cadastro "; //ok
            $sql .= " ) values (";

            $sql .= "   " . cBANCO::ChaveOk($this->mNU_CANDIDATO);   //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);   //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_NOME_MEIO);    //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_ULTIMO_NOME);   //ok
            $sql .= " , " . cBANCO::StringOk($this->mNOME_COMPLETO);   // Concatenar
            $sql .= " , " . cBANCO::StringOk($this->mNO_EMAIL_CANDIDATO);  //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_RESIDENCIA); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_RESIDENCIA);  //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_RESIDENCIA); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_CANDIDATO); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_EMPRESA_ESTRANGEIRA); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_EMPRESA); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_EMPRESA); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_EMPRESA); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_EMPRESA); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_PAI); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_MAE); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_NACIONALIDADE); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_NIVEL_ESCOLARIDADE); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_ESTADO_CIVIL); //ok
            $sql .= " , " . cBANCO::StringOk($this->mCO_SEXO); //ok
            $sql .= " , " . cBANCO::DataOk($this->mDT_NASCIMENTO); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_LOCAL_NASCIMENTO); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNU_PASSAPORTE); //ok
            $sql .= " , " . cBANCO::DataOk($this->mDT_EMISSAO_PASSAPORTE); //ok
            $sql .= " , " . cBANCO::DataOk($this->mDT_VALIDADE_PASSAPORTE); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_EMISSOR_PASSAPORTE); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNU_RNE);
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_PROFISSAO_CANDIDATO); //ok
            $sql .= " , " . cBANCO::StringOk($this->mTE_TRABALHO_ANTERIOR_BRASIL); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNU_CPF);      //ok
            $sql .= " , now()";
            $sql .= " , " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
            $sql .= " , now()";
            $sql .= " , " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
            $sql .= " , " . cBANCO::StringOk($this->mNO_ENDERECO_ESTRANGEIRO); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_CIDADE_ESTRANGEIRO); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_ESTRANGEIRO); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNU_TELEFONE_ESTRANGEIRO); //ok
            $sql .= " , " . cBANCO::SimNaoOk($this->mBO_CADASTRO_MINIMO_OK); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mNU_EMPRESA); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNU_CTPS); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNU_CNH); //ok
            $sql .= " , " . cBANCO::DataOk($this->mDT_EXPIRACAO_CNH); //ok
            $sql .= " , " . cBANCO::DataOk($this->mDT_EXPIRACAO_CTPS); //ok
            $sql .= " , " . cBANCO::SimNaoOk($this->mFL_CADASTRO_REVISADO); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_LOCAL_EMBARCACAO_PROJETO); //ok
            $sql .= " , " . cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_FUNCAO); //ok
            $sql .= " , " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL); //ok
            $sql .= " , " . cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL_BRASIL); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_SEAMAN); //ok
            $sql .= " , " . cBANCO::DataOk($this->mDT_EMISSAO_SEAMAN); //ok
            $sql .= " , " . cBANCO::DataOk($this->mDT_VALIDADE_SEAMAN); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mCO_PAIS_SEAMAN); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mloca_id); //ok
            $sql .= " , " . cBANCO::SimNaoOk($this->mFL_EDICAO_CONCLUIDA); //ok
            $sql .= " , " . cBANCO::DataOk($this->mDT_CONCLUSAO_CADASTRO_CLIENTE); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_pai); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mco_pais_nacionalidade_mae); //ok
            $sql .= " , " . cBANCO::StringOk($this->mcand_tx_residencia_estado); //ok
            $sql .= " , " . cBANCO::SimNaoOk($this->mcand_fl_liberado); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_ESCOLARIDADE_EM_INGLES); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_REPARTICAO_CONSULAR); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_ESTADO_CIVIL_EM_INGLES); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_FUNCAO_EM_INGLES); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_PROFISSAO_EM_INGLES); //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_EMBARCACAO_PROJETO); //ok
            $sql .= " , " . cBANCO::StringOk($this->memail_pessoal);  //ok
            $sql .= " , " . cBANCO::StringOk($this->mNO_SENHA);  //ok
            $sql .= " , " . cBANCO::DataOk($this->dt_envio_edicao); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mid_status_edicao); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mconv_id); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mnu_servico); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mid_solicita_visto); //ok
            $sql .= " , " . cBANCO::DataOk($this->mdt_envio_aprovacao); //ok
            $sql .= " , " . cBANCO::ChaveOk($this->mcd_usuario_envio_aprovacao); //ok
            $sql .= " , '" . $this->mhash_string."'"; //ok
            $sql .= " , " . cBANCO::DataOk($this->mhash_dt_cadastro); //ok
            $sql .= ")";

            cAMBIENTE::$db_pdo->exec($sql);
            $this->mnu_candidato_tmp = cAMBIENTE::$db_pdo->lastInsertId();
        }
    }

    public function AtualizeCadastroParaCliente() {
        $sql = "update candidato_tmp, CANDIDATO SET ";
        $sql .= "   candidato_tmp.NO_PRIMEIRO_NOME				= CANDIDATO.NO_PRIMEIRO_NOME";
        $sql .= " , candidato_tmp.NO_NOME_MEIO					= CANDIDATO.NO_NOME_MEIO";
        $sql .= " , candidato_tmp.NO_ULTIMO_NOME 				= CANDIDATO.NO_ULTIMO_NOME";
        $sql .= " , candidato_tmp.NOME_COMPLETO 				= CANDIDATO.NOME_COMPLETO";
        $sql .= " , candidato_tmp.NO_EMAIL_CANDIDATO 			= CANDIDATO.NO_EMAIL_CANDIDATO";
        $sql .= " , candidato_tmp.email_pessoal			= CANDIDATO.email_pessoal";
        $sql .= " , candidato_tmp.NO_SENHA			= CANDIDATO.NO_SENHA";
        $sql .= " , candidato_tmp.NO_ENDERECO_RESIDENCIA 		= CANDIDATO.NO_ENDERECO_RESIDENCIA";
        $sql .= " , candidato_tmp.NO_CIDADE_RESIDENCIA			= CANDIDATO.NO_CIDADE_RESIDENCIA";
        $sql .= " , candidato_tmp.CO_PAIS_RESIDENCIA 			= CANDIDATO.CO_PAIS_RESIDENCIA";
        $sql .= " , candidato_tmp.NU_TELEFONE_CANDIDATO 		= CANDIDATO.NU_TELEFONE_CANDIDATO";
        $sql .= " , candidato_tmp.NO_EMPRESA_ESTRANGEIRA		= CANDIDATO.NO_EMPRESA_ESTRANGEIRA";
        $sql .= " , candidato_tmp.NO_ENDERECO_EMPRESA			= CANDIDATO.NO_ENDERECO_EMPRESA";
        $sql .= " , candidato_tmp.NO_CIDADE_EMPRESA 			= CANDIDATO.NO_CIDADE_EMPRESA";
        $sql .= " , candidato_tmp.CO_PAIS_EMPRESA				= CANDIDATO.CO_PAIS_EMPRESA";
        $sql .= " , candidato_tmp.NU_TELEFONE_EMPRESA			= CANDIDATO.NU_TELEFONE_EMPRESA";
        $sql .= " , candidato_tmp.NO_PAI 						= CANDIDATO.NO_PAI";
        $sql .= " , candidato_tmp.NO_MAE 						= CANDIDATO.NO_MAE";
        $sql .= " , candidato_tmp.CO_NACIONALIDADE				= CANDIDATO.CO_NACIONALIDADE";
        $sql .= " , candidato_tmp.CO_NIVEL_ESCOLARIDADE 		= CANDIDATO.CO_NIVEL_ESCOLARIDADE";
        $sql .= " , candidato_tmp.CO_ESTADO_CIVIL				= CANDIDATO.CO_ESTADO_CIVIL";
        $sql .= " , candidato_tmp.CO_SEXO						= CANDIDATO.CO_SEXO";
        $sql .= " , candidato_tmp.DT_NASCIMENTO 				= CANDIDATO.DT_NASCIMENTO";
        $sql .= " , candidato_tmp.NO_LOCAL_NASCIMENTO			= CANDIDATO.NO_LOCAL_NASCIMENTO";
        $sql .= " , candidato_tmp.NU_PASSAPORTE 				= CANDIDATO.NU_PASSAPORTE";
        $sql .= " , candidato_tmp.DT_EMISSAO_PASSAPORTE 		= CANDIDATO.DT_EMISSAO_PASSAPORTE";
        $sql .= " , candidato_tmp.DT_VALIDADE_PASSAPORTE 		= CANDIDATO.DT_VALIDADE_PASSAPORTE";
        $sql .= " , candidato_tmp.CO_PAIS_EMISSOR_PASSAPORTE 	= CANDIDATO.CO_PAIS_EMISSOR_PASSAPORTE";
        $sql .= " , candidato_tmp.NU_RNE 						= CANDIDATO.NU_RNE";
        $sql .= " , candidato_tmp.CO_PROFISSAO_CANDIDATO 		= CANDIDATO.CO_PROFISSAO_CANDIDATO";
        $sql .= " , candidato_tmp.TE_TRABALHO_ANTERIOR_BRASIL	= CANDIDATO.TE_TRABALHO_ANTERIOR_BRASIL";
        $sql .= " , candidato_tmp.NU_CPF 						= CANDIDATO.NU_CPF";
        $sql .= " , candidato_tmp.DT_ULT_ALTERACAO				= CANDIDATO.DT_ULT_ALTERACAO";
        $sql .= " , candidato_tmp.CO_USU_ULT_ALTERACAO			= CANDIDATO.CO_USU_ULT_ALTERACAO";
        $sql .= " , candidato_tmp.NO_ENDERECO_ESTRANGEIRO		= CANDIDATO.NO_ENDERECO_ESTRANGEIRO";
        $sql .= " , candidato_tmp.NO_CIDADE_ESTRANGEIRO 		= CANDIDATO.NO_CIDADE_ESTRANGEIRO";
        $sql .= " , candidato_tmp.CO_PAIS_ESTRANGEIRO			= CANDIDATO.CO_PAIS_ESTRANGEIRO";
        $sql .= " , candidato_tmp.NU_TELEFONE_ESTRANGEIRO		= CANDIDATO.NU_TELEFONE_ESTRANGEIRO";
        $sql .= " , candidato_tmp.VA_REMUNERACAO_MENSAL		    = CANDIDATO.VA_REMUNERACAO_MENSAL";
        $sql .= " , candidato_tmp.VA_REMUNERACAO_MENSAL_BRASIL	= CANDIDATO.VA_REMUNERACAO_MENSAL_BRASIL";
        $sql .= " , candidato_tmp.NU_EMPRESA                    = CANDIDATO.NU_EMPRESA";
        $sql .= " , candidato_tmp.NU_EMBARCACAO_PROJETO         = CANDIDATO.NU_EMBARCACAO_PROJETO";
        $sql .= " , candidato_tmp.co_pais_nacionalidade_pai     = CANDIDATO.co_pais_nacionalidade_pai";
        $sql .= " , candidato_tmp.co_pais_nacionalidade_mae     = CANDIDATO.co_pais_nacionalidade_mae";
        $sql .= " where candidato_tmp.NU_CANDIDATO = CANDIDATO.NU_CANDIDATO"
                . " and CANDIDATO.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);

        $sql = "update candidato_tmp, CANDIDATO, estado_civil SET ";
        $sql .= "   candidato_tmp.NO_ESTADO_CIVIL_EM_INGLES = estado_civil.NO_ESTADO_CIVIL_EM_INGLES";
        $sql .= " where candidato_tmp.NU_CANDIDATO = CANDIDATO.NU_CANDIDATO"
                . " and estado_civil.CO_ESTADO_CIVIL = CANDIDATO.CO_ESTADO_CIVIL"
                . " and CANDIDATO.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);

        $sql = "update candidato_tmp, CANDIDATO, escolaridade SET ";
        $sql .= "   candidato_tmp.NO_ESCOLARIDADE_EM_INGLES = coalesce(escolaridade.NO_ESCOLARIDADE_EM_INGLES, escolaridade.NO_ESCOLARIDADE)";
        $sql .= " where candidato_tmp.NU_CANDIDATO = CANDIDATO.NU_CANDIDATO"
                . " and escolaridade.CO_ESCOLARIDADE= CANDIDATO.CO_NIVEL_ESCOLARIDADE"
                . " and CANDIDATO.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);

        $sql = "update candidato_tmp, CANDIDATO, funcao_cargo SET ";
        $sql .= "   candidato_tmp.NO_FUNCAO_EM_INGLES = coalesce(funcao_cargo.NO_FUNCAO_EM_INGLES, funcao_cargo.NO_FUNCAO)";
        $sql .= " where candidato_tmp.NU_CANDIDATO = CANDIDATO.NU_CANDIDATO"
                . " and funcao_cargo.CO_FUNCAO= CANDIDATO.CO_FUNCAO"
                . " and CANDIDATO.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);

        $sql = "update candidato_tmp, CANDIDATO, profissao SET ";
        $sql .= "   candidato_tmp.NO_PROFISSAO_EM_INGLES = coalesce(profissao.NO_PROFISSAO_EM_INGLES, profissao.NO_PROFISSAO)";
        $sql .= " where candidato_tmp.NU_CANDIDATO = CANDIDATO.NU_CANDIDATO"
                . " and profissao.CO_PROFISSAO= CANDIDATO.CO_PROFISSAO_CANDIDATO"
                . " and CANDIDATO.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);

        $sql = "update candidato_tmp, CANDIDATO, embarcacao_projeto SET ";
        $sql .= "   candidato_tmp.NO_EMBARCACAO_PROJETO = embarcacao_projeto.NO_EMBARCACAO_PROJETO";
        $sql .= " where candidato_tmp.NU_CANDIDATO = CANDIDATO.NU_CANDIDATO"
                . " and embarcacao_projeto.nu_empresa = CANDIDATO.nu_empresa"
                . " and embarcacao_projeto.nu_embarcacao_projeto = CANDIDATO.nu_embarcacao_projeto"
                . " and CANDIDATO.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);

        $sql = "update candidato_tmp, CANDIDATO, empresa SET ";
        $sql .= "   candidato_tmp.NO_EMPRESA_ESTRANGEIRA = empresa.NO_RAZAO_SOCIAL";
        $sql .= " where candidato_tmp.NU_CANDIDATO = CANDIDATO.NU_CANDIDATO"
                . " and empresa.nu_empresa = CANDIDATO.nu_empresa"
                . " and CANDIDATO.NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function CorrijaNomeCompleto() {
        $this->mNOME_COMPLETO = $this->CalculaNomeCompleto($this->mNO_PRIMEIRO_NOME, $this->mNO_NOME_MEIO, $this->mNO_ULTIMO_NOME);
        $sql = " update CANDIDATO SET NOME_COMPLETO =  " . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= " where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function Cursor_ExperienciaProfissional() {
        $sql = "select * from experiencia_profissional where nu_candidato_tmp = " . $this->mnu_candidato_tmp;
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    public function Cursor_Cursos() {
        $sql = "select * from curso c left join tipo_ensino tp on tp.tpen_id = c.tpen_id where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        return $res;
    }

    public function AdicionarCurso($ptpen_id, $pcurs_no_cidade, $pcurs_no_periodo, $pcurs_no_instituicao, $pcurs_no_grau) {
        $sql = "insert into curso (NU_CANDIDATO, tpen_id, curs_no_cidade, curs_no_periodo, curs_no_instituicao, curs_no_grau) ";
        $sql .= " values(";
        $sql .= " " . $this->mNU_CANDIDATO;
        $sql .= ", " . cBANCO::StringOk($ptpen_id);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_cidade);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_periodo);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_instituicao);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_grau);
        $sql .= ") ";
        print 'Vai adicionar o curso ' . $sql;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function AdicionarExpProf($ptpen_id, $pcurs_no_cidade, $pcurs_no_periodo, $pcurs_no_instituicao, $pcurs_no_grau) {
        $sql = "insert into experiencia_profissional (NU_CANDIDATO, epro_no_companhia, epro_no_funcao, epro_no_periodo, epro_tx_atribuicoes) ";
        $sql .= " values(";
        $sql .= " " . $this->mNU_CANDIDATO;
        $sql .= ", " . cBANCO::StringOk($ptpen_id);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_cidade);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_periodo);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_instituicao);
        $sql .= ", " . cBANCO::StringOk($pcurs_no_grau);
        $sql .= ") ";
        print 'Vai adicionar o curso ' . $sql;
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }


    //TODO:Refatorar para disponibilizeDrAoCliente
    //public function VerificarCandidatoExistente($pNomeCompleto, $pNomeMae, $pDataNasc){
    public function DisponibilizeSeParaCliente($pNomeCompleto, $pNomeMae, $pDataNasc, $pcd_empresa_usuario) {
        //Verifica se o candidato existe
        $this->RecuperesePeloNome($pNomeCompleto, $pNomeMae, $pDataNasc);
        if ($this->mNU_CANDIDATO > 0) {
            $embp = new cEMBARCACAO_PROJETO();
            if ($this->mNU_EMBARCACAO_PROJETO != '') {
                $embp->mNU_EMPRESA = $this->mNU_EMPRESA;
                $embp->mNU_EMBARCACAO_PROJETO = $this->mNU_EMBARCACAO_PROJETO;
                $embp->RecupereSe();
            }
            if ($this->mNU_EMPRESA == $pcd_empresa_usuario) {
                if ($this->mFL_ATUALIZACAO_HABILITADA == cCANDIDATO::atuBLOQUEADA) {
                    if (intval($this->mFL_INATIVO) == 1 || intval($embp->membp_fl_ativo) == 0) {
                        $this->DisponibilizeCadastroParaCliente();
                        $this->AtualizeCadastroParaCliente();
                        $this->LibereInativoParaClienteDaMesmaEmpresa();
                        if (intval($embp->membp_fl_ativo) == 0) {
                            $sql = "update candidato_tmp set ";
                            $sql.= "       NU_EMBARCACAO_PROJETO = null";
                            $sql.= " where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
                            cAMBIENTE::$db_pdo->exec($sql);
                        }
                    }
                }
            } else {
                if ($this->mFL_INATIVO == 1 || $embp->membp_fl_ativo == 0) {
                    if ($this->mFL_ATUALIZACAO_HABILITADA == cCANDIDATO::atuBLOQUEADA) {
                        $this->DisponibilizeCadastroParaCliente();
                        $this->AtualizeCadastroParaCliente();
                        $this->LibereInativoParaClienteDeOutraEmpresa($pcd_empresa_usuario);
                    }
                } else {
                    if (cHTTP::getLang() == 'pt_br') {
                        $msg = 'Este candidato já está cadastrado no sistema em outra empresa. Para preservar o sigilo das informações dos nossos clientes, uma empresa não pode acessar informações de candidatos que estão em outra. Para maiores informações entre em contato com a Mundivisas.';
                    } else {
                        $msg = 'This person is already in our database, filed under another company. To preseve our customers data, one company is not allowed to access candidate data from another. For further information please contact Mundivisas.';
                    }
                    throw new exception($msg);
                }
            }
        } else {
            $this->Criar_cliente($pcd_empresa_usuario, $pNomeCompleto, $pNomeMae, $pDataNasc);
        }
    }

    public function CursorCandidatosCliente($pFiltros, $pOrdenacao) {
        $pFiltros['NU_EMPRESA']->mQualificadorFiltro = "C";
        $pFiltros['NU_EMBARCACAO_PROJETO']->mQualificadorFiltro = "C";
        $pFiltros['NOME_COMPLETO']->mQualificadorFiltro = "C";

        $where = cBANCO::WhereFiltros($pFiltros);
        $sql = "select distinct
					C.NU_CANDIDATO,
					C.NOME_COMPLETO,
					C.NU_PASSAPORTE,
					C.NU_RNE,
					C.NU_CPF,
					C.FL_ATUALIZACAO_HABILITADA,
					C.DT_VALIDADE_PASSAPORTE DT_VALIDADE_PASSAPORTE_ORIG ,
					DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE ,
					DATE_FORMAT(MAX_PRAZO_ESTADA.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA,
					MAX_PRAZO_ESTADA.dt_prazo_estada DT_PRAZO_ESTADA_ORIG,
					datediff(MAX_PRAZO_ESTADA.dt_prazo_estada, now()) DIAS_VENCIMENTO,

					MTE.nu_processo NU_PROCESSO_MTE,
					MTE.prazo_solicitado PRAZO_SOLICITADO,
					DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO ,
					DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO,
					MTE.dt_deferimento DT_DEFERIMENTO_ORIG,

					PN.NO_NACIONALIDADE,
					PN.NO_NACIONALIDADE_EM_INGLES,
					F.NO_FUNCAO,
					S.NO_SERVICO_RESUMIDO TIPO_VISTO,
					RC.NO_REPARTICAO_CONSULAR CONSULADO,
					EP.NO_EMBARCACAO_PROJETO
				from CANDIDATO C
				left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual
						and MTE.cd_candidato = C.NU_CANDIDATO
						and MTE.fl_vazio = 0
				left join (select cd_candidato, codigo_processo_mte, max(dt_prazo_estada) dt_prazo_estada from processo_regcie
							where codigo_processo_mte is not null
							group by cd_candidato, codigo_processo_mte) as MAX_PRAZO_ESTADA
						on MAX_PRAZO_ESTADA.codigo_processo_mte = MTE.codigo
						and MAX_PRAZO_ESTADA.cd_candidato = MTE.cd_candidato
				left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
				left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
				left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
				left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = MTE.cd_reparticao
				left join EMBARCACAO_PROJETO EP on EP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = C.NU_EMPRESA
				where ifnull(C.FL_INATIVO,0) = 0 
				  and (EP.embp_fl_ativo = 1 or C.NU_EMBARCACAO_PROJETO is null)
				  and ifnull(FL_ATUALIZACAO_HABILITADA,0) = 0
				" . $where;
        $sql .= " union
				select distinct
					C.NU_CANDIDATO,
					C.NOME_COMPLETO,
					C.NU_PASSAPORTE,
					C.NU_RNE,
					C.NU_CPF,
					CORIG.FL_ATUALIZACAO_HABILITADA,
					C.DT_VALIDADE_PASSAPORTE DT_VALIDADE_PASSAPORTE_ORIG ,
					DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE ,
					DATE_FORMAT(MAX_PRAZO_ESTADA.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA,
					MAX_PRAZO_ESTADA.dt_prazo_estada DT_PRAZO_ESTADA_ORIG,
					datediff(MAX_PRAZO_ESTADA.dt_prazo_estada, now()) DIAS_VENCIMENTO,

					MTE.nu_processo NU_PROCESSO_MTE,
					MTE.prazo_solicitado PRAZO_SOLICITADO,
					DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO ,
					DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO,
					MTE.dt_deferimento DT_DEFERIMENTO_ORIG,

					PN.NO_NACIONALIDADE,
					PN.NO_NACIONALIDADE_EM_INGLES,
					F.NO_FUNCAO,
					S.NO_SERVICO_RESUMIDO TIPO_VISTO,
					RC.NO_REPARTICAO_CONSULAR CONSULADO,
					EP.NO_EMBARCACAO_PROJETO
					from candidato_tmp C
					join CANDIDATO CORIG on CORIG.NU_CANDIDATO = C.NU_CANDIDATO
					left join processo_mte MTE on MTE.codigo = CORIG.codigo_processo_mte_atual
						and MTE.cd_candidato = CORIG.NU_CANDIDATO
						and MTE.fl_vazio = 0
					left join (select cd_candidato, codigo_processo_mte, max(dt_prazo_estada) dt_prazo_estada from processo_regcie
								where codigo_processo_mte is not null
								group by cd_candidato, codigo_processo_mte) as MAX_PRAZO_ESTADA
						on MAX_PRAZO_ESTADA.codigo_processo_mte = MTE.codigo
						and MAX_PRAZO_ESTADA.cd_candidato = MTE.cd_candidato
					left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
					left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
					left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
					left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = MTE.cd_reparticao
					left join EMBARCACAO_PROJETO EP on EP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO and EP.NU_EMPRESA = C.NU_EMPRESA
					where ifnull(CORIG.FL_INATIVO,0) = 0
					  and (EP.embp_fl_ativo = 1 or C.NU_EMBARCACAO_PROJETO is null)
					  and ifnull(CORIG.FL_ATUALIZACAO_HABILITADA,0) <> 0
					" . $where;
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        if ($res = conectaQuery($sql, __CLASS__)) {
            return $res;
        }
    }

    public function CursorCandidato_cliente($pNU_CANDIDATO) {
        $sql = "select * from vcandidato_tmp_cons where NU_CANDIDATO = " . $pNU_CANDIDATO;
        if ($res = conectaQuery($sql, __CLASS__)) {
            if ($rs = mysql_fetch_array($res)) {
                return $rs;
            } else {
                $this->mNU_CANDIDATO = $pNU_CANDIDATO;
                $this->CriaCandidatoCliente();
                $sql = "select * from vcandidato_tmp_cons where NU_CANDIDATO = " . $pNU_CANDIDATO;
                if ($res = conectaQuery($sql, __CLASS__)) {
                    if ($rs = mysql_fetch_array($res)) {
                        return $rs;
                    }
                }
            }
        }
    }

    public function CursorArquivos($acesso_cliente) {
        $sql = "select ifnull(A_CAND.NU_EMPRESA, 0) NU_EMPRESA"
                . ", A_CAND.NU_CANDIDATO"
                . ", A_CAND.NU_SEQUENCIAL"
                . ", A_CAND.NO_ARQUIVO"
                . ", A_CAND.NO_ARQ_ORIGINAL"
                . ", A_CAND.TP_ARQUIVO"
                . ", coalesce(ta.NO_TIPO_ARQUIVO, A_CAND.NO_ARQ_ORIGINAL) NO_TIPO_ARQUIVO "
                . ", A_CAND.DT_INCLUSAO"
                . ", ta.CO_TIPO_ARQUIVO";
        $sql .= "  from ARQUIVOS A_CAND";
        $sql .= "  left join TIPO_ARQUIVO ta on ta.ID_TIPO_ARQUIVO=A_CAND.TP_ARQUIVO";
        $sql .= "  where NU_SEQUENCIAL>0";
        $sql .= "   and nu_candidato_tmp= " . $this->mnu_candidato_tmp;
        if ($acesso_cliente){
            $sql .=" and coalesce(ta.fl_disponivel_clientes, 0) = 1";
        }
        $sql .= " order by DT_INCLUSAO desc ,TP_ARQUIVO,NO_ARQUIVO ";
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    //TODO:Refatorar para InserirPeloCliente
    //TODO:Refatorar criação do tmp para a rotina de sincronização do TMP.
    public function Criar_cliente($pNU_EMPRESA, $pNOME_COMPLETO, $pNO_MAE, $pDT_NASCIMENTO) {
        $this->QuebreNomeCompleto($pNOME_COMPLETO);
        $this->mNO_MAE = $pNO_MAE;
        $this->mDT_NASCIMENTO = $pDT_NASCIMENTO;
        $this->mNU_EMPRESA = $pNU_EMPRESA;
        $sql = "insert into CANDIDATO (NU_EMPRESA, NOME_COMPLETO, NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME, NO_MAE, DT_NASCIMENTO,DT_CADASTRAMENTO,CO_USU_CADASTAMENTO,CO_USU_ULT_ALTERACAO, FL_ATUALIZACAO_HABILITADA) values(";
        $sql .= " " . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= "," . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= "," . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_NOME_MEIO);
        $sql .= "," . cBANCO::StringOk($this->mNO_ULTIMO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_MAE);
        $sql .= "," . cBANCO::DataOk($this->mDT_NASCIMENTO);
        $sql .= ", now()";
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= ",1";
        $sql .= ")";
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $this->mNU_CANDIDATO = mysql_insert_id();
        $sql = "insert into candidato_tmp (NU_CANDIDATO, NU_EMPRESA, NOME_COMPLETO, NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME, NO_MAE, DT_NASCIMENTO, FL_EDICAO_CONCLUIDA,DT_CADASTRAMENTO,CO_USU_CADASTAMENTO,CO_USU_ULT_ALTERACAO) values(";
        $sql .= " " . cBANCO::ChaveOk($this->mNU_CANDIDATO);
        $sql .= "," . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= "," . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= "," . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_NOME_MEIO);
        $sql .= "," . cBANCO::StringOk($this->mNO_ULTIMO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_MAE);
        $sql .= "," . cBANCO::DataOk($this->mDT_NASCIMENTO);
        $sql .= ",0";
        $sql .= ", now()";
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= ")";
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function Criar_clienteDR($pNOME_COMPLETO, $pNO_MAE, $pDT_NASCIMENTO) {
        cAMBIENTE::InicializeAmbiente('DR');
        $this->QuebreNomeCompleto($pNOME_COMPLETO);
        $this->mNO_MAE = $pNO_MAE;
        $this->mDT_NASCIMENTO = $pDT_NASCIMENTO;
        $sql = "insert into dr.CANDIDATO (NOME_COMPLETO, NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME, NO_MAE, DT_NASCIMENTO,DT_CADASTRAMENTO,CO_USU_CADASTAMENTO,CO_USU_ULT_ALTERACAO, FL_ATUALIZACAO_HABILITADA) 
				values(";
        $sql .= "" . cBANCO::StringOk($this->mNOME_COMPLETO);
        $sql .= "," . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_NOME_MEIO);
        $sql .= "," . cBANCO::StringOk($this->mNO_ULTIMO_NOME);
        $sql .= "," . cBANCO::StringOk($this->mNO_MAE);
        $sql .= "," . cBANCO::DataOk($this->mDT_NASCIMENTO);
        $sql .= ", now()";
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= ",1";
        $sql .= ")";
        executeQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function NotificarPontoFocal($pEmailAlterador, $pMsgDescricao) {
        $email = new cEMAIL();
        $empresa = new cEMPRESA();
        $empresa->mNU_EMPRESA = $this->mNU_EMPRESA;
        $resp = $empresa->getPontosFocais();
        $adicionouAlgum = false;
        foreach ($resp as $usuario) {
            if ($usuario->nm_email != '') {
                $email->AdicioneDestinatario($usuario->nm_email);
                $adicionouAlgum = true;
            }
        }
        if ($adicionouAlgum) {
            $msgErro = '';
        } else {
            $empresa->RecuperePeloId();
            $msgErro = '<br/><br/><b><i>(ATENÇÃO: essa mensagem foi encaminhada a você porque não foi definido o responsável pela empresa ' . $empresa->mNO_RAZAO_SOCIAL . ' no sistema. Por favor, providencie que o mesmo seja notificado.)</i></b>';
            $email->AdicioneDestinatario('daniela@mundivisas.com.br');
            $email->AdicioneDestinatario('leonardo.mattos@mundivisas.com.br');
        }

        $email->setSubject('Dataweb - Alteração em DR: ' . $this->mNOME_COMPLETO);
        $msg = "Prezada(o) " . $usuario->nome . ",<br/><br/>" . $pMsgDescricao . "<br/><br/>Atenciosamente, <br/>Dataweb";
        $msg .= $msgErro;
        $email->setMsg($msg);
        $email->Envie();
    }



    /**
     * ENTIDADE_AUDITAVEL
     * @return string
     */
    public function getCampoUsuarioAlteracao() {
        return 'CO_USU_ULT_ALTERACAO';
    }

    public function QtdCandidatos($pFiltros) {
        $where = cBANCO::WhereFiltros($pFiltros);
        $sql = "select ifnull(count(*),0) qtd from CANDIDATO C 
                left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual and  (MTE.fl_vazio = 0 or MTE.fl_vazio is null)
				where 1=1 " . $where;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rw = $res->fetch(PDO::FETCH_ASSOC);
        $qtd = $rw['qtd'];
        return $qtd;
    }

    public function CursorRevisoesPorUsuario() {
        $sql = "select nome, date_format(cand_dt_revisao, '%d/%m/%Y'), count(*) 
				from CANDIDATO C, usuarios u 
				where u.cd_usuario = C.cd_usuario_revisao
				and dt_cadastramento < '2012-01-01'
				group by nome, date_format(cand_dt_revisao, '%d/%m/%Y')";
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res;
    }

    public function ProcessoFormatado($pnumero) {
        return str_replace('.', "", str_replace(".", "", str_replace('/', "", str_replace('-', "", $pnumero))));
    }

    public function AtualizeAtributo($pAtributo, $pValor) {
        $sql = "update candidato_tmp set " . $pAtributo . " = " . cBANCO::StringOk($pValor) . " where nu_candidato_tmp = " . $this->mnu_candidato_tmp;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AtualizeAtributoString($pAtributo, $pValor) {
        $sql = "update candidato_tmp
				set " . $pAtributo . " = " . cBANCO::StringOk($pValor) . " 
				" . $this->SqlLog(__CLASS__, __FUNCTION__) . "
				where nu_candidato = " . $this->mNU_CANDIDATO;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function get_cvCarregado() {
        if ($this->mNU_CANDIDATO > 0) {
            $sql = "select * from dr.arquivos where NU_CANDIDATO = " . $this->mNU_CANDIDATO;
            $res = cAMBIENTE::$db_pdo->query($sql);
            $rw = $res->fetch(PDO::FETCH_ASSOC);
            if ($rw['NU_SEQUENCIAL'] > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function CarreguePropriedadesPeloRecordset($prs, $prefixo = "m") {
        parent::CarreguePropriedadesPeloRecordset($prs, $prefixo);
        $this->mDT_EXPIRACAO_CNH = cBANCO::DataFmt($prs["DT_EXPIRACAO_CNH"]);
        $this->mDT_EXPIRACAO_CTPS = cBANCO::DataFmt($prs["DT_EXPIRACAO_CTPS"]);
        $this->mDT_VALIDADE_PASSAPORTE = cBANCO::DataFmt($prs["DT_VALIDADE_PASSAPORTE"]);
        $this->mDT_VALIDADE_SEAMAN = cBANCO::DataFmt($prs["DT_VALIDADE_SEAMAN"]);
        $this->mDT_EMISSAO_PASSAPORTE = cBANCO::DataFmt($prs["DT_EMISSAO_PASSAPORTE"]);
        $this->mDT_EMISSAO_SEAMAN = cBANCO::DataFmt($prs["DT_EMISSAO_SEAMAN"]);
    }

    public function adicionaPeloConvidado(cconvidado $conv, $nome_completo) {
        if (trim($nome_completo) != '') {
            $this->mNOME_COMPLETO = $nome_completo;
            $this->QuebreNomeCompleto($nome_completo);
            $this->mconv_id = $conv->mconv_id;
            $sql = "insert into candidato_tmp (conv_id, id_status_edicao, nome_completo, no_primeiro_nome, no_nome_meio, no_ultimo_nome, nu_empresa, nu_embarcacao_projeto, nu_servico) values("
                    . $conv->mconv_id
                    . ", 1"
                    . ", " . cBANCO::StringOk($this->mNOME_COMPLETO)
                    . ", " . cBANCO::StringOk($this->mNO_PRIMEIRO_NOME)
                    . ", " . cBANCO::StringOk($this->mNO_NOME_MEIO)
                    . ", " . cBANCO::StringOk($this->mNO_ULTIMO_NOME)
                    . ", " . cBANCO::ChaveOk($conv->mnu_empresa)
                    . ", " . cBANCO::ChaveOk($conv->mnu_embarcacao_projeto)
                    . ", " . cBANCO::ChaveOk($conv->mnu_servico)
                    . ")";

            cAMBIENTE::$db_pdo->exec($sql);
        }
    }

    /**
     * Indica que o candidato foi enviado para aprovação
     * @param  [type] $cd_usuario [description]
     * @return [type]             [description]
     */
    public function enviadoParaAprovacao($cd_usuario){
        $agora = new DateTime('now', new DateTimeZone('America/Sao_Paulo'));
        $sql = "update candidato_tmp set
                    dt_envio_edicao = '".$agora->format('Y-m-d H:i:s')."'
                  , id_status_edicao = ".cstatus_edicao::AGUARDANDO_CONFIRMACAO."
                where nu_candidato_tmp = ".$this->mnu_candidato_tmp;
        cAMBIENTE::$db_pdo->exec($sql);
        $this->mdt_envio_aprovacao = date('d/m/Y H:i:s');
        $this->mcd_usuario_envio_aprovacao = $cd_usuario;
    }

    public function arquivos(){
        $sql = "select NU_SEQUENCIAL, NO_ARQ_ORIGINAL from arquivos where nu_candidato_tmp = ".$this->mnu_candidato_tmp;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        return $rs;
    }

    public function getConvidado() {
        return $this->getRelacionamento('cconvidado', 'mconv_id', 'mconvidado');
    }

    /**
     * Retorna nome e email do preenchedor do DR
     * @return [type] [description]
     */
    public function nome_email_preenchedor(){
        if (intval($this->mconv_id) > 0){
            $conv = $this->getConvidado();
            return array('nome' => $conv->mnome, 'email' => $conv->memail);
        } else {
            $usuario = $this->getUsuarioCadastro();
            return array('nome' => $usuario->nome, 'email' => $usuario->nm_email);
        }
    }

    public function nome_solicitador(){
        $preenchedor = $this->nome_email_preenchedor();
        return $preenchedor['nome'];
    }

    public function getServico(){
        return $this->getRelacionamento('cSERVICO', 'mnu_servico', 'mservico');
    }

    public function atualizaHash(){
        $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $date = new DateTime(date('Y-m-d'));
        $date->sub(new DateInterval('P10D'));
        $validade = $date->format('Y-m-d') ;

        if (strstr($this->mhash_dt_cadastro, "/")){
            $data = DateTime::createFromFormat('d/m/Y', $this->mhash_dt_cadastro);
        } else {
            $data = DateTime::createFromFormat('Y-m-d', $this->mhash_dt_cadastro);
        }

        if (!$data || $data->format('Y-m-d') < $validade ){
            $this->mhash_string = md5(substr(str_shuffle($letters), 0, 8));
            $this->mhash_dt_cadastro = date('Y-m-d');
            $this->salvar();
        }
    }
}
