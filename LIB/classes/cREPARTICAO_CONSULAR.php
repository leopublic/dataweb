<?php

// á
class cREPARTICAO_CONSULAR extends cMODELO {

    protected $mCO_REPARTICAO_CONSULAR;
    protected $mNO_REPARTICAO_CONSULAR;
    protected $mNO_REPARTICAO_CONSULAR_ALTERNATIVO;
    protected $mNO_ENDERECO;
    protected $mNO_CIDADE;
    protected $mCO_PAIS;
    protected $mNU_TELEFONE;


    public static function get_nomeCampoChave(){
        return 'co_reparticao_consular';
    }
    public static function get_nomeCampoDescricao(){
        return 'no_reparticao_consular';
    }
    public static function get_nomeTabelaBD(){
        return 'REPARTICAO_CONSULAR';
    }



    public function get_nomeTabela() {
        return 'REPARTICAO_CONSULAR';
    }

    public function getId() {
        return $this->mCO_REPARTICAO_CONSULAR;
    }

    public function setId($pValor) {
        $this->mCO_REPARTICAO_CONSULAR = $pValor;
    }

    public function campoid() {
        return 'CO_REPARTICAO_CONSULAR';
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from REPARTICAO_CONSULAR where CO_REPARTICAO_CONSULAR = " . $this->mCO_REPARTICAO_CONSULAR;
        return $sql;
    }

    public function Incluir() {
        $sql = "insert into REPARTICAO_CONSULAR(
				 NO_REPARTICAO_CONSULAR 
				,NO_ENDERECO 
				,NO_CIDADE
				,CO_PAIS 
				,NU_TELEFONE 
				,NO_REPARTICAO_CONSULAR_ALTERNATIVO
		       ) values (
				 " . cBANCO::StringOk($this->mNO_REPARTICAO_CONSULAR) . "
				," . cBANCO::StringOk($this->mNO_ENDERECO) . "
				," . cBANCO::StringOk($this->mNO_CIDADE) . "
				," . cBANCO::ChaveOk($this->mCO_PAIS) . "
				," . cBANCO::StringOk($this->mNU_TELEFONE) . "
				," . cBANCO::StringOk($this->mNO_REPARTICAO_CONSULAR_ALTERNATIVO) . "
				)";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mCO_REPARTICAO_CONSULAR = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function Atualizar() {
        $sql = "update REPARTICAO_CONSULAR set 
				 NO_REPARTICAO_CONSULAR = " . cBANCO::StringOk($this->mNO_REPARTICAO_CONSULAR) . "
				,NO_ENDERECO = " . cBANCO::StringOk($this->mNO_ENDERECO) . "
				,NO_CIDADE = " . cBANCO::StringOk($this->mNO_CIDADE) . "
				,CO_PAIS= " . cBANCO::ChaveOk($this->CO_PAIS) . "
				,NU_TELEFONE = " . cBANCO::StringOk($this->mNU_TELEFONE) . "
				,NO_REPARTICAO_CONSULAR_ALTERNATIVO = " . cBANCO::StringOk($this->mNO_REPARTICAO_CONSULAR_ALTERNATIVO) . "
			  where CO_REPARTICAO_CONSULAR=" . $this->mCO_REPARTICAO_CONSULAR;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_Liste() {
        $sql = "select r.*, NO_PAIS from REPARTICAO_CONSULAR r left join pais_nacionalidade p on p.co_pais = r.co_pais where CO_REPARTICAO_CONSULAR>999 ";
        return $sql;
    }

    public function recuperePeloAlternativo($alternativo) {
        $sql = "select * from REPARTICAO_CONSULAR where NO_REPARTICAO_CONSULAR_ALTERNATIVO like '%" . $alternativo . "%' or NO_CIDADE like '%" . $alternativo . "%'";
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetch(PDO::FETCH_BOTH);
            cBANCO::CarreguePropriedades($rs, $this);
        }
    }

    public static function comboNacionalidades($default = '(não informado)'){
        $sql = "select co_pais chave, no_nacionalidade descricao from pais_nacionalidade order by no_nacionalidade";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));
        return $rs;

    }

}
