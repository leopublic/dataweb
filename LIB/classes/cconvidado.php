<?php
class cconvidado extends cMODELO{
    public $mconv_id;
    public $mnome;
    public $memail;
    public $mdt_inclusao;
    public $mdias;
    public $mchave;
    public $mnu_empresa;
    public $mnu_embarcacao_projeto;
    public $mnu_servico;
    public $mnu_candidato_tmp;
    public $mempresa;
    public $mservico;

    public function getEmpresa(){
      if (!is_object($this->mempresa)){
        if ($this->mnu_empresa > 0){
          $empresa = new cEMPRESA;
          $empresa->RecuperePeloId($this->mnu_empresa);
          $this->mempresa = $empresa;
        } else {
          $this->mempresa= null;
        }
      }
      return $this->mempresa;
    }

    public function getServico(){
      if (!is_object($this->mservico)){
        if ($this->mnu_servico > 0){
          $servico = new cSERVICO;
          $servico->RecuperePeloId($this->mnu_servico);
          $this->mservico = $servico;
        } else {
          $this->mservico= null;
        }
      }
      return $this->mservico;
    }

    public function get_nomeTabela() {
        return 'convidado';
    }

    public function getId() {
        return $this->mconv_id;
    }

    public function setId($pValor) {
        $this->mconv_id = $pValor;
    }

    public function campoid() {
        return 'conv_id';
    }

    public function sql_RecuperePeloId() {
        $sql = "select *, datediff(now(), dt_inclusao) dias from convidado where conv_id = " . $this->mconv_id;
        return $sql;
    }

    public function Incluir() {
        $this->mchave = md5(date('Y-m-d H:i:s'));
        $sql = "insert into convidado(
				 nome
				,email
				,dt_inclusao
				,chave
				,nu_empresa
				,nu_embarcacao_projeto
				,nu_servico
                ,nu_candidato_tmp
		       ) values (
				 " . cBANCO::StringOk($this->mnome) . "
				," . cBANCO::StringOk($this->memail) . "
				, now()
				," . cBANCO::StringOk($this->mchave) . "
				," . cBANCO::ChaveOk($this->mnu_empresa) . "
				," . cBANCO::ChaveOk($this->mnu_embarcacao_projeto) . "
				," . cBANCO::ChaveOk($this->mnu_servico) . "
                ," . cBANCO::ChaveOk($this->mnu_candidato_tmp) . "
				)";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mconv_id = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function Atualizar(){
        $sql = "update convidado set"
             . " nome = ".cBANCO::StringOk($this->mnome)
             . " ,email = ".cBANCO::StringOk($this->memail)
             . " ,nu_empresa = ".cBANCO::ChaveOk($this->mnu_empresa)
             . " ,nu_embarcacao_projeto = ".cBANCO::ChaveOk($this->mnu_embarcacao_projeto)
             . " ,nu_servico = ".cBANCO::ChaveOk($this->mnu_servico)
             . " ,nu_candidato_tmp = ".cBANCO::ChaveOk($this->mnu_candidato_tmp)
             . " ,dt_inclusao = ".cBANCO::DataOk($this->mdt_inclusao)
             . " where conv_id = ".$this->mconv_id;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function recuperesePeloEmail($email) {
        $email = trim($email);
        $sql = "select * from convidado"
             . " where email =" . cBANCO::StringOk($email);
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->CarreguePropriedadesPeloRecordset($rs, "m");
                $this->mdt_inclusao = cBANCO::DataFmt($rs['dt_inclusao']);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Retorna os DRs do convidado
     * @return [type] [description]
     */
    public function drs(){
        $sql = "select * from candidato_tmp where conv_id = ".$this->mconv_id;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        return $rs;
    }
    /**
     * Retorna os DRs do convidado que jÃ¡ foram importados e nÃ£o tem OS ainda
     * @return [type] [description]
     */
    public function drsImportadosSemOs(){
        $sql = "select * from candidato_tmp "
              ." where conv_id = ".$this->mconv_id
              ."   and nu_candidato is not null"
              ."   and id_solicita_visto is null"
              ."   and id_status_edicao = 5";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        return $rs;
    }
    /**
     * Retorna os DRs do convidado que jÃ¡ foram importados e nÃ£o tem OS ainda
     * @return [type] [description]
     */
    public function qtdDrsImportadosSemOs(){
        $sql = "select count(*) qtd from candidato_tmp "
              ." where conv_id = ".$this->mconv_id
              ."   and nu_candidato is not null"
              ."   and id_solicita_visto is null"
              ."   and id_status_edicao = 5";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        return $rs['qtd'];
    }

    public function recuperesePeloCandidato($nu_candidato_tmp){
      $sql = "select * from convidado where nu_candidato_tmp = ".$nu_candidato_tmp;
      $res = cAMBIENTE::$db_pdo->query($sql);
      $rs = $res->fetch(PDO::FETCH_ASSOC);
      $this->CarreguePropriedadesPeloRecordset($rs, "m");
      $this->mdt_inclusao = cBANCO::DataFmt($rs['dt_inclusao']);
   }

   public function excluir(){
      $sql = "delete from convidado where conv_id = ".$this->mconv_id;
      cAMBIENTE::$db_pdo->exec($sql);

   }

   public static function excluirPeloCandidato($nu_candidato_tmp){
      $sql = "delete from convidado where nu_candidato_tmp = ".$nu_candidato_tmp;
      cAMBIENTE::$db_pdo->exec($sql);

   }

}
