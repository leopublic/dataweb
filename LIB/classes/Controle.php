<?php

/**
 * @author Fabio - M2Software
 * @copyright 2010
 */

 Class Controle extends Generic{
    
    public $ctrl_set = array(), $classname , $erro = false, $sucesso = true, $msgErro, $msgSucesso, $set = array();
    
    protected $metodos_internos = array('onLoad', 'save', 'isPost', 'layout') ;
    
    
     function __construct($file = null){
        if($file){
            $this->create($file);
        }else{
            $this->create(basename($_SERVER["PHP_SELF"]));
        }        
     }
    
     function create($file){
        
        
        $filename  = 'controle_'. basename($file);
        $this->classname =  str_replace('.php', '', basename($file));
        
        $file = realpath('../LIB/controles/'. $filename);
        //echo '../LIB/controles/'. $filename;
        
        if(file_exists($file)){
           require_once $file;
        }else{
            trigger_error('O controle desta View não foi encontrado: '.$file);
        }
        
        
      
        
        
        
        //require_once realpath('..'.DS.'LIB'.DS). DS .'controles' . DS . $filename;
  
     }
     
     function getEventos(){
        
           $win = new $this->classname;
           $win->isPost();   
           $win->save();
           $win->onLoad();
           $win->layout();
           
           $this->ctrl_set = $win->set; 
        
     }     
     
     function set($name, $valor){
        $this->set[$name] = $valor;
        $this->get = $this->set;
        

     }
     
    function layout(){
         if($this->layout <> ''){
            $t = realpath('../LIB/templates/'. $this->layout.'.php');
            if(file_exists($t)){
                $title = $this->title;
                require_once($t);
            }else{
                trigger_error('O Layout definido ('.$this->layout.') não foi encontrado: '.$t);
            }
        }
    }
     
    function __call ($metodo, $param){
        
        if(!in_array($metodo, $this->metodos_internos)){
           trigger_error("Método não definido: $metodo "); 
        }
       
       
   
    }
    
    
    function relacaoField($_FieldName, $id_Field, $DataSource){
        
           $field_relacional = array();
            
           if(is_array($DataSource)){
                foreach($DataSource as $Field){
                    if($Field[$_FieldName] == $id_Field){
                        $field_relacional[] = $Field;
                    }
                }     
            }

            return $field_relacional;
            
    }

   

     

    
 }

?>
