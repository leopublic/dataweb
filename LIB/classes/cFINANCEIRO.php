<?php
class cFINANCEIRO{
	/**
	 * Cancela o faturamento de uma OS
	 */
	public static function DesfaturarOs(cORDEMSERVICO $os, $pcd_usuario)
	{
		
	}

	public static function CorrigirResumoPara(cresumo_cobranca $resumoNovo, cORDEMSERVICO $os)
	{
		$resc_id_anterior = $os->mresc_id;
	
		if(intval($resc_id_anterior) > 0 && $resc_id_anterior != $resumoNovo->mresc_id){
			$resumo_anterior = new cresumo_cobranca();
			$resumo_anterior->RecuperePeloId($resc_id_anterior);
			$msg = "Transferida do resumo ".$resumo_anterior->mresc_nu_numero_mv." para o resumo ".$resumoNovo->mresc_nu_numero_mv." pela tela de auditoria de cobrança em ".date('d/m/Y');
		}
		else{
			$msg = "Faturada pela auditoria de cobrança em ".date('d/m/Y');
		}
		$os->Fature($resumoNovo->mresc_id, $msg);
	}
}
