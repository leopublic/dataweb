<?php

/**
 * Serviços
 * @author Leonardo
 */
class cSERVICO extends cMODELO {

    const RESTABELECIMENTO = '58';

    public $mNU_SERVICO;
    public $mCO_SERVICO;
    public $mNO_SERVICO;
    public $mNO_SERVICO_RESUMIDO;
    public $mID_TIPO_ACOMPANHAMENTO;
    public $mCO_TIPO_AUTORIZACAO;
    public $mCO_CLASSIFICACAO_VISTO;
    public $mNU_TIPO_SERVICO;
    public $mNO_TIPO_SERVICO;
    public $mCO_TIPO_SERVICO;
    public $mNO_TIPO_AUTORIZACAO;
    public $mNO_CLASSIFICACAO_VISTO;
    public $mNO_TIPO_ACOMPANHAMENTO;
    public $mCO_SERVICO_TIPO_SERVICO;
    public $mserv_fl_candidato_obrigatorio;
    public $mserv_fl_form_tvs;
    public $mserv_fl_ativo;
    public $mserv_fl_envio_bsb;
    public $mserv_tx_codigo_sg;
    public $mserv_fl_revisao_analistas;
    public $mserv_fl_timesheet;
    public $mserv_fl_despesas_adicionais;
    public $mserv_fl_pacote;
    public $mno_servico_em_ingles;
    public $mno_servico_resumido_em_ingles;
    public $mserv_fl_acompanhamento_unico;
    public $mserv_fl_cria_visto;
    public $mid_perfiltaxa;
    public $mfl_disponivel_clientes;
    protected $tipo_acompanhamento;
    public $mserv_fl_coleta;

    public function __construct($pNU_SERVICO = '') {
        if ($pNU_SERVICO != '') {
            $this->mNU_SERVICO = $pNU_SERVICO;
            $this->RecupereSe();
        }
    }


    public static function get_nomeCampoChave(){
        return 'nu_servico';
    }
    public static function get_nomeCampoDescricao(){
        return 'no_servico_resumido';
    }
    public static function get_nomeTabelaBD(){
        return 'servico';
    }


    public function getTipoAcompanhamento() {
        if (isset($this->tipo_acompanhamento)) {
            return $this->tipo_acompanhamento;
        } else {
            $this->tipo_acompanhamento = new cTIPO_ACOMPANHAMENTO();
            $this->tipo_acompanhamento->mID_TIPO_ACOMPANHAMENTO = $this->mID_TIPO_ACOMPANHAMENTO;
            $this->tipo_acompanhamento->RecuperePeloId();
            return $this->tipo_acompanhamento;
        }
    }

    public static function Instancie($pnu_servico) {
        $sql = "select * from SERVICO where NU_SERVICO = " . $pnu_servico;
        $res = conectaQuery($sql, __CLASS . '.' . __FUNCTION__);
        $rs = mysql_fetch_array($res);
        if ($rs['NU_SERVICO'] != '') {
            if ($rs['serv_fl_pacote'] == 1) {
                $serv = new cpacote_servico();
            } else {
                $serv = new cSERVICO();
            }
            cBANCO::CarreguePropriedades($rs, $serv);
        } else {
            throw new Exception("Serviço não encontrado (NU_SERVICO=" . $this->mNU_SERVICO . ")");
        }
        return $serv;
    }

    public function get_nomeTabela() {
        return 'servico';
    }

    public function campoid() {
        return 'NU_SERVICO';
    }

    public function getid() {
        return $this->mNU_SERVICO;
    }

    public function setid($pValor) {
        $this->mNU_SERVICO = $pValor;
    }

    public function sql_Liste() {
        $sql = " select s.NU_SERVICO";
        $sql.= "      , s.CO_SERVICO";
        $sql.= "      , s.NO_SERVICO";
        $sql.= "      , s.NO_SERVICO_RESUMIDO";
        $sql.= "      , s.no_servico_em_ingles";
        $sql.= "      , s.no_servico_resumido_em_ingles";
        $sql.= "      , s.ID_TIPO_ACOMPANHAMENTO";
        $sql.= "      , s.CO_TIPO_AUTORIZACAO";
        $sql.= "      , s.CO_CLASSIFICACAO_VISTO";
        $sql.= "      , s.NU_TIPO_SERVICO";
        $sql.= "      , TS.NO_TIPO_SERVICO";
        $sql.= "      , TS.CO_TIPO_SERVICO";
        $sql.= "      , TAU.NO_TIPO_AUTORIZACAO";
        $sql.= "      , CV.NO_CLASSIFICACAO_VISTO";
        $sql.= "      , TAC.NO_TIPO_ACOMPANHAMENTO";
        $sql.= "      , CONCAT(CO_TIPO_SERVICO, '.', right(concat('00', CO_SERVICO), 2)) CO_SERVICO_TIPO_SERVICO";
        $sql.= "      , serv_fl_candidato_obrigatorio";
        $sql.= "      , serv_fl_form_tvs";
        $sql.= "      , serv_fl_ativo";
        $sql.= "      , serv_fl_envio_bsb";
        $sql.= "      , serv_fl_revisao_analistas";
        $sql.= "      , serv_tx_codigo_sg";
        $sql.= "      , serv_fl_timesheet";
        $sql.= "      , serv_fl_despesas_adicionais";
        $sql.= "      , serv_fl_pacote";
        $sql.= "      , serv_fl_coleta";
        $sql.= "      , fl_disponivel_clientes";
        $sql.= "      , pt.descricao descricao_perfiltaxa";
        $sql.= "      , coalesce(serv_fl_ativo, 0) serv_fl_ativo";
        $sql.= "      , (select count(*) from solicita_visto where NU_SERVICO = s.NU_SERVICO) qtd_os";
        $sql.= "   from SERVICO s";
        $sql.=" left join TIPO_SERVICO TS on TS.NU_TIPO_SERVICO = s.NU_TIPO_SERVICO";
        $sql.=" left join CLASSIFICACAO_VISTO CV on CV.CO_CLASSIFICACAO_VISTO = s.CO_CLASSIFICACAO_VISTO";
        $sql.=" left join TIPO_ACOMPANHAMENTO TAC on TAC.ID_TIPO_ACOMPANHAMENTO = s.ID_TIPO_ACOMPANHAMENTO";
        $sql.=" left join TIPO_AUTORIZACAO TAU on TAU.CO_TIPO_AUTORIZACAO = s.CO_TIPO_AUTORIZACAO";
        $sql.=" left join perfiltaxa pt on pt.id_perfiltaxa = s.id_perfiltaxa";
        $sql.=" where 1=1";
        $sql.="  and coalesce(serv_fl_pacote, 0) =0";
        return $sql;
    }

    public function sql_RecuperePeloId() {
        $sql = " select s.NU_SERVICO";
        $sql.= "      , s.CO_SERVICO";
        $sql.= "      , s.NO_SERVICO";
        $sql.= "      , s.NO_SERVICO_RESUMIDO";
        $sql.= "      , s.no_servico_em_ingles";
        $sql.= "      , s.no_servico_resumido_em_ingles";
        $sql.= "      , s.ID_TIPO_ACOMPANHAMENTO";
        $sql.= "      , s.CO_TIPO_AUTORIZACAO";
        $sql.= "      , s.CO_CLASSIFICACAO_VISTO";
        $sql.= "      , s.NU_TIPO_SERVICO";
        $sql.= "      , s.id_perfiltaxa";
        $sql.= "      , s.fl_disponivel_clientes";
        $sql.= "      , TS.NO_TIPO_SERVICO";
        $sql.= "      , TS.CO_TIPO_SERVICO";
        $sql.= "      , TAU.NO_TIPO_AUTORIZACAO";
        $sql.= "      , CV.NO_CLASSIFICACAO_VISTO";
        $sql.= "      , TAC.NO_TIPO_ACOMPANHAMENTO";
        $sql.= "      , serv_fl_candidato_obrigatorio";
        $sql.= "      , serv_fl_form_tvs";
        $sql.= "      , serv_fl_ativo";
        $sql.= "      , serv_fl_envio_bsb";
        $sql.= "      , serv_fl_revisao_analistas";
        $sql.= "      , serv_tx_codigo_sg";
        $sql.= "      , serv_fl_timesheet";
        $sql.= "      , serv_fl_despesas_adicionais";
        $sql.= "      , serv_fl_pacote";
        $sql.= "      , serv_fl_cria_visto";
        $sql.= "      , serv_fl_acompanhamento_unico";
        $sql.= "      , serv_fl_coleta";
        $sql.= "   from SERVICO s";
        $sql.=" left join TIPO_SERVICO TS on TS.NU_TIPO_SERVICO = s.NU_TIPO_SERVICO";
        $sql.=" left join CLASSIFICACAO_VISTO CV on CV.CO_CLASSIFICACAO_VISTO = s.CO_CLASSIFICACAO_VISTO";
        $sql.=" left join TIPO_ACOMPANHAMENTO TAC on TAC.ID_TIPO_ACOMPANHAMENTO = s.ID_TIPO_ACOMPANHAMENTO";
        $sql.=" left join TIPO_AUTORIZACAO TAU on TAU.CO_TIPO_AUTORIZACAO = s.CO_TIPO_AUTORIZACAO";
        $sql.=" where s.NU_SERVICO = " . $this->mNU_SERVICO;
        return $sql;
    }

    public function RecupereSe($pNU_SERVICO = '') {
        if ($pNU_SERVICO != '') {
            $this->mNU_SERVICO = $pNU_SERVICO;
        }
        if ($this->mNU_SERVICO == 0) {
            throw new Exception("Chave não informada. NU_SERVICO='' .");
        }
        $sql = "select * from SERVICO where NU_SERVICO = " . $this->mNU_SERVICO;
        $res = conectaQuery($sql, __CLASS . '.' . __FUNCTION__);
        if ($rs = mysql_fetch_array($res)) {
            cBANCO::CarreguePropriedades($rs, $this);
        } else {
            throw new Exception("Serviço não encontrado (NU_SERVICO=" . $this->mNU_SERVICO . ")");
        }
    }

    public function Atualizar() {
        if ($this->mserv_fl_acompanhamento_unico == '') {
            $this->mserv_fl_acompanhamento_unico = 0;
        }
        if ($this->mserv_fl_coleta == ''){
            $this->mserv_fl_coleta = false;
        }
        if ($this->serv_fl_envio_bsb == ''){
            $this->serv_fl_envio_bsb = false;
        }

        $sql = " update SERVICO set ";
        $sql.= "    CO_SERVICO  = " . cBANCO::StringOk($this->mCO_SERVICO);
        $sql.= "  , NO_SERVICO  = " . cBANCO::StringOk($this->mNO_SERVICO);
        $sql.= "  , NO_SERVICO_RESUMIDO  = " . cBANCO::StringOk($this->mNO_SERVICO_RESUMIDO);
        $sql.= "  , ID_TIPO_ACOMPANHAMENTO  = " . cBANCO::ChaveOk($this->mID_TIPO_ACOMPANHAMENTO);
        $sql.= "  , CO_TIPO_AUTORIZACAO  = " . cBANCO::ChaveOk($this->mCO_TIPO_AUTORIZACAO);
        $sql.= "  , CO_CLASSIFICACAO_VISTO  = " . cBANCO::ChaveOk($this->mCO_CLASSIFICACAO_VISTO);
        $sql.= "  , NU_TIPO_SERVICO  = " . cBANCO::ChaveOk($this->mNU_TIPO_SERVICO);
        $sql.= "  , serv_fl_candidato_obrigatorio  = " . cBANCO::SimNaoOk($this->mserv_fl_candidato_obrigatorio);
        $sql.= "  , serv_fl_form_tvs  = " . cBANCO::SimNaoOk($this->mserv_fl_form_tvs);
        $sql.= "  , serv_fl_ativo  = " . cBANCO::SimNaoOk($this->mserv_fl_ativo);
        $sql.= "  , serv_fl_envio_bsb  = " . cBANCO::SimNaoOk($this->mserv_fl_envio_bsb);
        $sql.= "  , serv_tx_codigo_sg = " . cBANCO::StringOk($this->mserv_tx_codigo_sg);
        $sql.= "  , serv_fl_revisao_analistas  = " . cBANCO::SimNaoOk($this->mserv_fl_revisao_analistas);
        $sql.= "  , serv_fl_timesheet  = " . cBANCO::SimNaoOk($this->mserv_fl_timesheet);
        $sql.= "  , serv_fl_despesas_adicionais  = " . cBANCO::SimNaoOk($this->mserv_fl_despesas_adicionais);
        $sql.= "  , no_servico_em_ingles  = " . cBANCO::StringOk($this->mno_servico_em_ingles);
        $sql.= "  , no_servico_resumido_em_ingles  = " . cBANCO::StringOk($this->mno_servico_resumido_em_ingles);
        $sql.= "  , id_perfiltaxa  = " . cBANCO::ChaveOk($this->mid_perfiltaxa);
        $sql.= "  , serv_fl_cria_visto  = " . cBANCO::SimNaoOk($this->mserv_fl_cria_visto);
        $sql.= "  , serv_fl_acompanhamento_unico  = " . cBANCO::SimNaoOk($this->mserv_fl_acompanhamento_unico);
        $sql.= "  , fl_disponivel_clientes  = " . cBANCO::SimNaoOk($this->mfl_disponivel_clientes);
        $sql.= "  , serv_fl_coleta  = " . cBANCO::SimNaoOk($this->mserv_fl_coleta);
        $sql.= " where NU_SERVICO = " . $this->mNU_SERVICO;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function Incluir() {
        if ($this->mserv_fl_acompanhamento_unico == '') {
            $this->mserv_fl_acompanhamento_unico = 0;
        }
        if ($this->mserv_fl_coleta == ''){
            $this->mserv_fl_coleta = "0";
        }
        $sql = " insert into SERVICO(";
        $sql.= "        CO_SERVICO";
        $sql.= "      , NO_SERVICO";
        $sql.= "      , NO_SERVICO_RESUMIDO";
        $sql.= "      , ID_TIPO_ACOMPANHAMENTO";
        $sql.= "      , CO_TIPO_AUTORIZACAO";
        $sql.= "      , CO_CLASSIFICACAO_VISTO";
        $sql.= "      , NU_TIPO_SERVICO";
        $sql.= "      , serv_fl_candidato_obrigatorio";
        $sql.= "      , serv_fl_form_tvs";
        $sql.= "      , serv_fl_ativo";
        $sql.= "      , serv_fl_envio_bsb";
        $sql.= "      , serv_tx_codigo_sg";
        $sql.= "      , serv_fl_revisao_analistas";
        $sql.= "      , serv_fl_timesheet";
        $sql.= "      , serv_fl_despesas_adicionais";
        $sql.= "      , serv_fl_pacote";
        $sql.= "      , no_servico_em_ingles";
        $sql.= "      , no_servico_resumido_em_ingles";
        $sql.= "      , id_perfiltaxa";
        $sql.= "      , serv_fl_cria_visto";
        $sql.= "      , serv_fl_acompanhamento_unico";
        $sql.= "      , fl_disponivel_clientes";
        $sql.= "      , serv_fl_coleta";
        $sql.= ") values (";
        $sql.= "    " . cBANCO::StringOk($this->mCO_SERVICO);
        $sql.= "  , " . cBANCO::StringOk($this->mNO_SERVICO);
        $sql.= "  , " . cBANCO::StringOk($this->mNO_SERVICO_RESUMIDO);
        $sql.= "  , " . cBANCO::ChaveOk($this->mID_TIPO_ACOMPANHAMENTO);
        $sql.= "  , " . cBANCO::ChaveOk($this->mCO_TIPO_AUTORIZACAO);
        $sql.= "  , " . cBANCO::ChaveOk($this->mCO_CLASSIFICACAO_VISTO);
        $sql.= "  , " . cBANCO::ChaveOk($this->mNU_TIPO_SERVICO);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_candidato_obrigatorio);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_form_tvs);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_ativo);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_envio_bsb);
        $sql.= "  , " . cBANCO::StringOk($this->mserv_tx_codigo_sg);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_revisao_analistas);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_timesheet);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_despesas_adicionais);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_pacote);
        $sql.= "  , " . cBANCO::StringOk($this->mno_servico_em_ingles);
        $sql.= "  , " . cBANCO::StringOk($this->mno_servico_resumido_em_ingles);
        $sql.= "  , " . cBANCO::ChaveOk($this->mid_perfiltaxa);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_cria_visto);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_acompanhamento_unico);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mfl_disponivel_clientes);
        $sql.= "  , " . cBANCO::SimNaoOk($this->mserv_fl_coleta);
        $sql.= ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->setid(cAMBIENTE::$db_pdo->lastInsertId());
    }

    public function Exclua() {
        $sql = "SELECT ifnull(count(*),0) from solicita_visto where NU_SERVICO = " . $this->mNU_SERVICO;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetch(PDO::FETCH_BOTH);
            if ($rs[0] > 0) {
                throw new cERRO_CONSISTENCIA("Não é possível excluir o serviço pois existem OS cadastradas para ele.");
            }
        }
        $sql = "delete from preco where NU_SERVICO = " . $this->mNU_SERVICO;
        cAMBIENTE::$db_pdo->exec($sql);
        $sql = "delete from SERVICO where NU_SERVICO = " . $this->mNU_SERVICO;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function CursorServicoCompleto($pnu_servico) {
        $sql = " select s.NU_SERVICO";
        $sql.= "      , s.CO_SERVICO";
        $sql.= "      , s.NO_SERVICO";
        $sql.= "      , s.NO_SERVICO_RESUMIDO";
        $sql.= "      , s.ID_TIPO_ACOMPANHAMENTO";
        $sql.= "      , s.CO_TIPO_AUTORIZACAO";
        $sql.= "      , s.CO_CLASSIFICACAO_VISTO";
        $sql.= "      , s.NU_TIPO_SERVICO";
        $sql.= "      , s.no_servico_em_ingles";
        $sql.= "      , s.no_servico_resumido_em_ingles";
        $sql.= "      , TS.NO_TIPO_SERVICO";
        $sql.= "      , TS.CO_TIPO_SERVICO";
        $sql.= "      , TAU.NO_TIPO_AUTORIZACAO";
        $sql.= "      , CV.NO_CLASSIFICACAO_VISTO";
        $sql.= "      , TAC.NO_TIPO_ACOMPANHAMENTO";
        $sql.= "      , CONCAT(CO_TIPO_SERVICO, '.', right(concat('00', CO_SERVICO), 2)) CO_SERVICO_TIPO_SERVICO";
        $sql.= "      , serv_fl_candidato_obrigatorio";
        $sql.= "      , serv_fl_form_tvs";
        $sql.= "      , serv_fl_ativo";
        $sql.= "      , serv_fl_envio_bsb";
        $sql.= "      , serv_fl_revisao_analistas";
        $sql.= "      , serv_tx_codigo_sg";
        $sql.= "      , serv_fl_timesheet";
        $sql.= "      , serv_fl_despesas_adicionais";
        $sql.= "      , serv_fl_pacote";
        $sql.= "      , serv_fl_coleta";
        $sql.= "   from SERVICO s";
        $sql.=" left join TIPO_SERVICO TS on TS.NU_TIPO_SERVICO = s.NU_TIPO_SERVICO";
        $sql.=" left join CLASSIFICACAO_VISTO CV on CV.CO_CLASSIFICACAO_VISTO = s.CO_CLASSIFICACAO_VISTO";
        $sql.=" left join TIPO_ACOMPANHAMENTO TAC on TAC.ID_TIPO_ACOMPANHAMENTO = s.ID_TIPO_ACOMPANHAMENTO";
        $sql.=" left join TIPO_AUTORIZACAO TAU on TAU.CO_TIPO_AUTORIZACAO = s.CO_TIPO_AUTORIZACAO";
        $sql.=" where 1=1";
        $sql.=" and s.nu_servico =" . $pnu_servico;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        return $res;
    }

    public static function Atualize_no_servico_resumido($pnu_servico, $pno_servico_resumido, $pcd_candidato) {
        $sql = " update SERVICO set ";
        $sql.= "    no_servico_resumido  = " . cBANCO::StringOk($pno_servico_resumido);
        $sql.= " where NU_SERVICO = " . $pnu_servico;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualize_no_servico($pnu_servico, $pvalor, $pcd_candidato) {
        $sql = " update SERVICO set ";
        $sql.= "    no_servico  = " . cBANCO::StringOk($pvalor);
        $sql.= " where NU_SERVICO = " . $pnu_servico;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualize_serv_tx_codigo_sg($pnu_servico, $pvalor, $pcd_candidato) {
        $sql = " update SERVICO set ";
        $sql.= "    serv_tx_codigo_sg  = " . cBANCO::StringOk($pvalor);
        $sql.= " where NU_SERVICO = " . $pnu_servico;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualize_co_servico($pnu_servico, $pvalor, $pcd_candidato) {
        $sql = " update SERVICO set ";
        $sql.= "    co_servico  = " . cBANCO::StringOk($pvalor);
        $sql.= " where NU_SERVICO = " . $pnu_servico;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualize_nu_tipo_servico($pnu_servico, $pvalor, $pcd_candidato) {
        $sql = " update SERVICO set ";
        $sql.= "    nu_tipo_servico  = " . cBANCO::ChaveOk($pvalor);
        $sql.= " where NU_SERVICO = " . $pnu_servico;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualize_serv_fl_ativo($pnu_servico, $pvalor, $pcd_candidato) {
        $sql = " update SERVICO set ";
        $sql.= "    serv_fl_ativo  = " . cBANCO::SimNaoOk($pvalor);
        $sql.= " where NU_SERVICO = " . $pnu_servico;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function pacoteQueAbre($pnu_servico, $pnu_empresa) {
        $sql = "select distinct s.*
				from servico s, pacote_servico ps
				where s.nu_servico = ps.nu_servico_pacote
				and ps.nu_servico_incluido = " . $pnu_servico . "
				and pacs_fl_principal = 1
				and s.nu_servico in (select p.nu_servico from empresa e
														join preco p on p.tbpc_id = e.tbpc_id
														where e.nu_empresa= " . $pnu_empresa . " )
				order by no_servico_resumido
				limit 1";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        return $rs;
    }

    public static function cursorPacotesQueEstaIncluido($pnu_servico) {
        $sql = "select s.*
				from servico s, pacote_servico ps
				where s.nu_servico = ps.nu_servico_incluido
				order by no_servico_resumido";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        return $res;
    }

    public function podeEssaQtdDeCandidtos($qtd) {
        if ($this->mserv_fl_candidato_obrigatorio) {
            if ($qtd == 0) {
                throw new cERRO_CONSISTENCIA('Nesse tipo de serviço é obrigatório pelo menos um candidato');
            }
        }
        if ($this->getTipoAcompanhamento()->mFL_MULTI_CANDIDATOS == 0) {
            if ($qtd > 1) {
                throw new cERRO_CONSISTENCIA('Esse tipo de serviço só permite 1 candidato por ordem de serviço');
            }
        }
        return true;
    }

    public function criaVistoNovo() {
        if ($this->mserv_fl_cria_visto) {
            return true;
        } else {
            return false;
        }
    }

    public function ativos($nu_servico = '') {
        $sql = "select nu_servico chave, no_servico_resumido descricao "
                . " from servico"
                . " where serv_fl_ativo = 1";
        if ($nu_servico != '') {
            $sql .= " or nu_servico = " . $nu_servico;
        }
        $sql .= " order by no_servico_resumido";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        return $rs;
    }

    public static function comboDisponiveisClientes($valor_atual = '', $default = "(selecione...)") {
        $sql = "select nu_servico valor"
                . ", no_servico_em_ingles descricao"
                . ", if(nu_servico='" . $valor_atual . "' ,1 ,0 ) selected"
                . " from servico"
                . " where fl_disponivel_clientes = 1"
                . " order by no_servico_em_ingles";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor' => '0', 'descricao' => $default, 'selected' => '0'));

        if (intval($valor_atual == 0)) {
            $rs['0']['selected'] = true;
        }

        return $rs;
    }

    public static function comboDisponiveisEmpresa($nu_empresa, $exibeDefault = true, $default = "(selecione...)") {
        $filtro = " where nu_servico in"
                . " (select nu_servico from preco , tabela_precos, empresa"
                . "   where empresa.nu_empresa = ".$nu_empresa
                . "     and tabela_precos.tbpc_id = empresa.tbpc_id"
                . "     and preco.tbpc_id = tabela_precos.tbpc_id)";
        $servicos = cSERVICO::combo($exibeDefault , $default , 'nu_servico', 'no_servico_resumido', $filtro);
        return $servicos;
    }

}
