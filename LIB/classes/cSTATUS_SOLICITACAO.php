<?php

/**
 * Controla o status das OS
 */
class cSTATUS_SOLICITACAO extends cMODELO {

    public $mID_STATUS_SOL;
    public $mSI_STATUS_SOL;
    public $mNO_STATUS_SOL;
    public $mNO_STATUS_SOL_RES;
    public $mFL_ENCERRADA;
    public $mordem;
    /**
     * Retorna os status disponiveis para uma OS
     * @param type $ordemservico
     */
    const ssNOVA = 1;
    const ssPENDENTE = 2;
    const ssDEVOLVIDA = 3;
    const ssENVIADA_BSB = 4;
    const ssPROTOCOLADA = 5;
    const ssENCERRADA = 6;
    const ssACEITA_BSB = 7;
    const ssPENDENCIA_LIBERADA = 8;
    const ssCANCELADA = 9;
    const ssPENDENCIA_BSB = 11;
    const ssNAO_CADASTRADA = 12;
    const ssEM_ANALISE= 13;
    const ssEM_EXIGENCIA = 14;
    const ssEXIGENCIA_CUMPRIDA = 15;
    const ssJUNTADA = 16;
    const ssPROP_DEFERIMENTO = 17;
    const ssPROP_INDEFERIMENTO = 18;
    const ssPED_ARQUIVAMENTO = 19;
    const ssDEFERIDA = 20;
    const ssINDEFERIDA = 21;
    const ssARQUIVADA = 22;
    const ssNO_SHOW = 23;


    public function campoid() {
        return 'ID_STATUS_SOL';
    }

    public function getid() {
        return $this->mID_STATUS_SOL;
    }

    public function setid($pValor) {
        $this->mID_STATUS_SOL = $pValor;
    }

    public function sql_Liste() {
        $sql = " select * from STATUS_SOLICITACAO where 1=1";
        return $sql;
    }

    public function sql_RecuperePeloId() {
        $sql = " select * from STATUS_SOLICITACAO where ID_STATUS_SOL = " . $this->mID_STATUS_SOL;
        return $sql;
    }

    public static function get_nomeCampoChave(){
        return 'id_status_sol';
    }

    public static function get_nomeCampoDescricao(){
        return 'no_status_sol';
    }

    public static function get_nomeTabelaBD(){
        return 'STATUS_SOLICITACAO';
    }

    public function Atualizar() {
        $sql = " update STATUS_SOLICITACAO set ";
        $sql.= "    NO_STATUS_SOL  = " . cBANCO::StringOk($this->mNO_STATUS_SOL);
        $sql.= "  , SI_STATUS_SOL = " . cBANCO::StringOk($this->mCO_STATUS_SOL);
        $sql.= "  , NO_STATUS_SOL_RES  = " . cBANCO::StringOk($this->mNO_STATUS_SOL_RES);
        $sql.= "  , ordem = " . cBANCO::InteiroOk($this->mordem);
        $sql.= " where ID_STATUS_SOL = " . $this->mID_STATUS_SOL;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function Incluir() {
        $sql = " insert into STATUS_SOLICITACAO(";
        $sql.= "        NO_STATUS_SOL";
        $sql.= "      , SI_STATUS_SOL";
        $sql.= "      , NO_STATUS_SOL_RES";
        $sql.= "      , ordem";
        $sql.= ") values (";
        $sql.= "    " . cBANCO::StringOk($this->mNO_STATUS_SOL);
        $sql.= "  , " . cBANCO::StringOk($this->mCO_STATUS_SOL);
        $sql.= "  , " . cBANCO::StringOk($this->mNO_STATUS_SOL_RES);
        $sql.= "  , " . cBANCO::InteiroOk($this->mordem);
        $sql.= ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->setid(cAMBIENTE::$db_pdo->lastInsertId());
    }

    public static function statusDoAndamento(){
        return '4, 5, 12, 13, 14, 15, 16, 17, 18, 19, 20 ,21, 22';
    }

    public static function statusDisponiveis($ordemservico) {

    }

}
