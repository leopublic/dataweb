<?php
class cTIPO_PRECO
{
	const tpCONCEITO = 2;
	const tpPACOTE  = 3;
	const tpNAOCOBRAVEL = 1;

    public static function descricao($id)
    {
        switch($id){
            case 1:
                return "não cobrável";
                break;
            case 2:
                return "conceito";
                break;
            case 3:
                return "pacote";
                break;
        }
    }
}
