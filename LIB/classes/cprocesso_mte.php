<?php

class cprocesso_mte extends cprocesso {

    public $mnu_processo;
    public $mdt_requerimento;
    public $mnu_oficio;
    public $mdt_deferimento;
    public $mprazo_solicitado;
    public $mcd_funcao;
    public $mcd_reparticao;
    public $mobservacao_visto;
    public $mdt_ult_atu_andamento;
    public $mdt_publicacao_dou;
    public $mdt_envio_bsb;
    public $mdt_recebimento_bsb;
    public $mID_STATUS_ANDAMENTO;
    public $mID_STATUS_CONCLUSAO;
    public $mNO_STATUS_ANDAMENTO;
    public $mNO_STATUS_CONCLUSAO;
    public $mfl_visto_atual;
    public $mfl_ignorar_vencimento;
    public $mNU_EMPRESA;
    public $mNU_EMBARCACAO_PROJETO;
    public $mcodigo_ultimo_processo;
    public $mid_tipo_acompanhamento_ultimo_processo;
    public $mNO_RAZAO_SOCIAL;
    public $mNO_EMBARCACAO_PROJETO;
    public $mNO_SERVICO_AUTORIZACAO;
    public $mNO_FUNCAO;
    public $mNO_REPARTICAO_CONSULAR;
    public $mID_STATUS_SOL;
    public $mfl_cp_passaporte;
    public $mfl_form_144;
    protected $processos;
    protected $processos_carregados;
    protected $situacao_visto_atualizada;  // Indica se a situacao esta atualizada
    protected $processo_regcie;    // Processo de registro mais recente
    protected $processo_prorrog;    // Prorrogacao mais recente
    protected $processo_coleta;     // Processo coleta do visto
    protected $processo_emiscie;    // Processo de coleta de cie mais recente

    public function __construct($pCodigo = 0) {
        $this->mnomeTabela = "mte";

        if ($pCodigo > 0) {
            $this->RecupereSe($pCodigo);
        } else {
            $this->mcodigo = 0;
        }
        $this->processos_carregados = false;
        $this->situacao_visto_atualizada = false;
    }

    public static function get_nomeCampoChave() {
        return 'codigo';
    }

    public static function get_nomeTabelaBD() {
        return 'processo_mte';
    }

    public function setId($pValor) {
        $this->mcodigo = $pValor;
    }

    public function getId() {
        return $this->mcodigo;
    }

    public function nu_processo_fmt(){
        return formatarMascara(preg_replace("/[^0-9]/", "", $this->mnu_processo), '#####.######/####-##');
    }

    public function get_nu_processo() {
        return $this->mnu_processo;
    }

    public function RecupereSe($pCodigo = 0) {
        if ($pCodigo > 0) {
            $this->mcodigo = $pCodigo;
        }

        if (!$this->mcodigo > 0) {
            throw new cERRO_CONSISTENCIA("Código do processo não informado");
        }
        $sql = "select * from vprocesso_mte where codigo = " . $this->mcodigo;
        $res = cAMBIENTE::$db_pdo->query($sql);
        if ($rs = $res->fetch(PDO::FETCH_BOTH)) {
            $this->mcd_candidato = $rs["cd_candidato"];
            $this->mcd_solicitacao = $rs["cd_solicitacao"];
            $this->mnu_processo = $rs["nu_processo"];
            $this->mdt_requerimento = $rs["dt_requerimento"];
            $this->mnu_oficio = $rs["nu_oficio"];
            $this->mdt_deferimento = $rs["dt_deferimento"];
            $this->mprazo_solicitado = $rs["prazo_solicitado"];
            $this->mcd_funcao = $rs["cd_funcao"];
            $this->mcd_reparticao = $rs["cd_reparticao"];
            $this->mobservacao = $rs["observacao"];
            $this->mobservacao_visto = $rs["observacao_visto"];
            $this->mdt_cad = $rs["dt_cad"];
            $this->mdt_ult = $rs["dt_ult"];
            $this->mid_solicita_visto = $rs["id_solicita_visto"];
            $this->mdt_ult_atu_andamento = $rs["dt_ult_atu_andamento"];
            $this->mdt_publicacao_dou = $rs["dt_publicacao_dou"];
            $this->mdt_envio_bsb = $rs["dt_envio_bsb"];
            $this->mdt_recebimento_bsb = $rs["dt_recebimento_bsb"];
            $this->mID_STATUS_ANDAMENTO = $rs["ID_STATUS_ANDAMENTO"];
            $this->mID_STATUS_CONCLUSAO = $rs["ID_STATUS_CONCLUSAO"];
            $this->mfl_vazio = $rs["fl_vazio"];
            $this->mnu_servico = $rs["nu_servico"];
            $this->mfl_visto_atual = $rs["fl_visto_atual"];
            $this->mfl_ignorar_vencimento = $rs["fl_ignorar_vencimento"];
            $this->mNU_EMPRESA = $rs["NU_EMPRESA"];
            $this->mNU_EMBARCACAO_PROJETO = $rs["NU_EMBARCACAO_PROJETO"];
            $this->mNU_EMPRESA = $rs["NU_EMPRESA"];
            $this->mNU_EMBARCACAO_PROJETO = $rs["NU_EMBARCACAO_PROJETO"];
            $this->mNO_RAZAO_SOCIAL = $rs["NO_RAZAO_SOCIAL"];
            $this->mNO_EMBARCACAO_PROJETO = $rs["NO_EMBARCACAO_PROJETO"];
            $this->mNO_SERVICO_AUTORIZACAO = $rs['NO_SERVICO_AUTORIZACAO'];
            $this->mNO_FUNCAO = $rs['NO_FUNCAO'];
            $this->mNO_REPARTICAO_CONSULAR = $rs['NO_REPARTICAO_CONSULAR'];
            $this->mNO_SERVICO_RESUMIDO = $rs['NO_SERVICO_RESUMIDO'];
            $this->mID_STATUS_SOL = $rs['ID_STATUS_SOL'];
            $this->mNO_STATUS_ANDAMENTO = $rs['NO_STATUS_ANDAMENTO'];
            $this->mNO_STATUS_CONCLUSAO = $rs['NO_STATUS_CONCLUSAO'];
            //$this->mcodigo_ultimo_processo 	=$rs["codigo_ultimo_processo"];
            //$this->mid_tipo_acompanhamento_ultimo_processo=$rs["id_tipo_acompanhamento_ultimo_processo"];
        } else {
            throw new Exception("Processo (" . $this->mcodigo . ") não encontrado");
        }
    }

    public function Salve() {
        if (intval($this->mcodigo) == 0) {
            $this->IncluaNovo();
        } else {
            $this->Atualize();
        }
    }

    public function Salve_ProcessoEdit() {
        if (intval($this->mcodigo) == 0) {
            $this->IncluaNovo();
        } else {
            $this->Atualize_ProcessoEdit();
        }
    }

    public function IncluaNovo() {
        $sql = "INSERT INTO processo_mte (";
        $sql .= "	cd_candidato";
        $sql .= "	, cd_solicitacao";
        $sql .= "	, nu_processo";
        $sql .= "	, dt_requerimento";
        $sql .= "	, nu_oficio";
        $sql .= "	, dt_deferimento";
        $sql .= "	, prazo_solicitado";
        $sql .= "	, cd_funcao";
        $sql .= "	, cd_reparticao";
        $sql .= "	, observacao";
        $sql .= "	, observacao_visto";
        $sql .= "	, dt_cad";
        $sql .= "	, dt_ult";
        $sql .= "	, id_solicita_visto";
        $sql .= "	, dt_ult_atu_andamento";
        $sql .= "	, dt_publicacao_dou";
        $sql .= "	, dt_envio_bsb";
        $sql .= "	, dt_recebimento_bsb";
        $sql .= "	, ID_STATUS_ANDAMENTO";
        $sql .= "	, ID_STATUS_CONCLUSAO";
        $sql .= "	, nu_servico";
        $sql .= "	, fl_visto_atual";
        $sql .= "	, fl_ignorar_vencimento";
        $sql .= "	, NU_EMPRESA";
        $sql .= "	, NU_EMBARCACAO_PROJETO";
        $sql .= "	, cd_usuario";
        $sql .= "	,no_classe";
        $sql .= "	,no_metodo";
        $sql .= "	) values (";
        $sql .= "	" . cBANCO::ChaveOk($this->mcd_candidato);
        $sql .= "	," . cBANCO::ChaveOk($this->mcd_solicitacao);
        $sql .= "	," . cBANCO::StringOk($this->mnu_processo);
        $sql .= "	," . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= "	," . cBANCO::StringOk($this->mnu_oficio);
        $sql .= "	," . cBANCO::DataOk($this->mdt_deferimento);
        $sql .= "	," . cBANCO::StringOk($this->mprazo_solicitado);
        $sql .= "	," . cBANCO::ChaveOk($this->mcd_funcao);
        $sql .= "	," . cBANCO::ChaveOk($this->mcd_reparticao);
        $sql .= "	," . cBANCO::StringOk($this->mobservacao);
        $sql .= "	," . cBANCO::StringOk($this->mobservacao_visto);
        $sql .= "	, now()";
        $sql .= "	, now()";
        $sql .= "	," . cBANCO::ChaveOk($this->mid_solicita_visto);
        $sql .= "	," . cBANCO::DataOk($this->mdt_ult_atu_andamento);
        $sql .= "	," . cBANCO::DataOk($this->mdt_publicacao_dou);
        $sql .= "	," . cBANCO::DataOk($this->mdt_envio_bsb);
        $sql .= "	," . cBANCO::DataOk($this->mdt_recebimento_bsb);
        $sql .= "	," . cBANCO::ChaveOk($this->mID_STATUS_ANDAMENTO);
        $sql .= "	," . cBANCO::ChaveOk($this->mID_STATUS_CONCLUSAO);
        $sql .= "	," . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= "	," . cBANCO::InteiroOk($this->mfl_visto_atual);
        $sql .= "	," . cBANCO::InteiroOk($this->mfl_ignorar_vencimento);
        $sql .= "	," . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= "	," . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= "	," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "	," . cBANCO::StringOk(__CLASS__);
        $sql .= "	," . cBANCO::StringOk(__FUNCTION__);
        $sql .= "	)";
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mcodigo = mysql_insert_id();
    }

    public function Salve_ProcessoADICIONAR() {
        $sql = "INSERT INTO processo_mte (";
        $sql .= "	cd_candidato";
        $sql .= "	, cd_solicitacao";
        $sql .= "	, nu_processo";
        $sql .= "	, dt_requerimento";
        $sql .= "	, nu_oficio";
        $sql .= "	, dt_deferimento";
        $sql .= "	, prazo_solicitado";
        $sql .= "	, cd_funcao";
        $sql .= "	, cd_reparticao";
        $sql .= "	, observacao";
        $sql .= "	, dt_cad";
        $sql .= "	, dt_ult";
        $sql .= "	, nu_servico";
        $sql .= "	, fl_visto_atual";
        $sql .= "	, NU_EMPRESA";
        $sql .= "	, NU_EMBARCACAO_PROJETO";
        $sql .= "	, cd_usuario";
        $sql .= "	,no_classe";
        $sql .= "	,no_metodo";
        $sql .= "	) values (";
        $sql .= "	" . cBANCO::ChaveOk($this->mcd_candidato);
        $sql .= "	," . cBANCO::ChaveOk($this->mcd_solicitacao);
        $sql .= "	," . cBANCO::StringOk($this->mnu_processo);
        $sql .= "	," . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= "	," . cBANCO::StringOk($this->mnu_oficio);
        $sql .= "	," . cBANCO::DataOk($this->mdt_deferimento);
        $sql .= "	," . cBANCO::StringOk($this->mprazo_solicitado);
        $sql .= "	," . cBANCO::ChaveOk($this->mcd_funcao);
        $sql .= "	," . cBANCO::ChaveOk($this->mcd_reparticao);
        $sql .= "	," . cBANCO::StringOk($this->mobservacao);
        $sql .= "	, now()";
        $sql .= "	, now()";
        $sql .= "	," . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= "	," . cBANCO::InteiroOk($this->mfl_visto_atual);
        $sql .= "	," . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= "	," . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= "	," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "	," . cBANCO::StringOk(__CLASS__);
        $sql .= "	," . cBANCO::StringOk(__FUNCTION__);
        $sql .= "	)";
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mcodigo = mysql_insert_id();
        $servico = $this->get_servico();
        if (isset($servico)){
            if ($this->mcodigo > 0 && $servico->criaVistoNovo()) {
                $this->TorneSeVistoAtual();
            }
        }
    }

    private function Atualize() {

        $sql = "update processo_mte set ";
        $sql .= "   nu_processo 			= " . cBANCO::StringOk($this->mnu_processo);
        $sql .= " , dt_requerimento 		= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , nu_oficio 				= " . cBANCO::StringOk($this->mnu_oficio);
        $sql .= " , dt_deferimento 			= " . cBANCO::DataOk($this->mdt_deferimento);
        $sql .= " , prazo_solicitado 		= " . cBANCO::StringOk($this->mprazo_solicitado);
        $sql .= " , cd_funcao 				= " . cBANCO::ChaveOk($this->mcd_funcao);
        $sql .= " , cd_reparticao 			= " . cBANCO::ChaveOk($this->mcd_reparticao);
        $sql .= " , observacao 				= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , observacao_visto 		= " . cBANCO::StringOk($this->mobservacao_visto);
        $sql .= " , dt_publicacao_dou 		= " . cBANCO::DataOk($this->mdt_publicacao_dou);
        $sql .= " , dt_envio_bsb 			= " . cBANCO::DataOk($this->mdt_envio_bsb);
        $sql .= " , dt_recebimento_bsb 		= " . cBANCO::DataOk($this->mdt_recebimento_bsb);
        $sql .= " , ID_STATUS_ANDAMENTO 	= " . cBANCO::ChaveOk($this->mID_STATUS_ANDAMENTO);
        $sql .= " , ID_STATUS_CONCLUSAO 	= " . cBANCO::ChaveOk($this->mID_STATUS_CONCLUSAO);
        $sql .= " , fl_vazio 				= " . cBANCO::InteiroOk($this->mfl_vazio);
        $sql .= " , nu_servico 				= " . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= " , fl_visto_atual 			= " . cBANCO::InteiroOk($this->mfl_visto_atual);
        $sql .= " , fl_ignorar_vencimento	= " . cBANCO::InteiroOk($this->mfl_ignorar_vencimento);
        $sql .= " , NU_EMPRESA				= " . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= " , NU_EMBARCACAO_PROJETO	= " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= " , codigo_ultimo_processo	= " . cBANCO::ChaveOk($this->mcodigo_ultimo_processo);
        $sql .= " , id_tipo_acompanhamento_ultimo_processo= " . cBANCO::ChaveOk($this->mid_tipo_acompanhamento_ultimo_processo);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    private function Atualize_ProcessoEdit() {
        $sql = "update processo_mte set ";
        $sql .= "   nu_processo 			= " . cBANCO::StringOk($this->mnu_processo);
        $sql .= " , dt_requerimento 		= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , nu_oficio 				= " . cBANCO::StringOk($this->mnu_oficio);
        $sql .= " , dt_deferimento 			= " . cBANCO::DataOk($this->mdt_deferimento);
        $sql .= " , prazo_solicitado 		= " . cBANCO::StringOk($this->mprazo_solicitado);
        $sql .= " , cd_funcao 				= " . cBANCO::ChaveOk($this->mcd_funcao);
        $sql .= " , cd_reparticao 			= " . cBANCO::ChaveOk($this->mcd_reparticao);
        $sql .= " , observacao 				= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , NU_EMPRESA				= " . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= " , NU_EMBARCACAO_PROJETO	= " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= " , nu_servico				= " . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function Atualize_Revisao() {
        $sql = "update processo_mte set ";
        $sql .= "  cd_funcao 				= " . cBANCO::ChaveOk($this->mcd_funcao);
        $sql .= " , cd_reparticao 			= " . cBANCO::ChaveOk($this->mcd_reparticao);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    
    public function ConsisteCampos() {
        $msg = '';
        if ($this->mdt_requerimento != '' && !cBANCO::DataValida($this->mdt_requerimento)) {
            $msg .= '<br/>- A data do requerimento é inválida';
        }
        if ($this->mdt_deferimento != '' && !cBANCO::DataValida($this->mdt_deferimento)) {
            $msg .= '<br/>- A data do deferimento é inválida';
        }
        if ($this->mdt_validade != '' && !cBANCO::DataValida($this->mdt_validade)) {
            $msg .= '<br/>- A validade do protocolo é inválida';
        }
        if ($this->mdt_prazo_pret != '' && !cBANCO::DataValida($this->mdt_prazo_pret)) {
            $msg .= '<br/>- O prazo pretendido é inválido';
        }
        if ($this->mdt_publicacao_dou != '' && !cBANCO::DataValida($this->mdt_publicacao_dou)) {
            $msg .= '<br/>- A data de publicação DOU é inválida';
        }
        if ($msg != '') {
            $msg = 'Não foi possível executar a operação pois:' . $msg;
            throw new cERRO_CONSISTENCIA($msg);
        }
    }

    private function Atualize_DadosOS() {
        $sql = "update processo_mte set ";
        $sql .= "   nu_servico 			    = " . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= " , NU_EMPRESA 			    = " . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= " , NU_EMBARCACAO_PROJETO	= " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
        $sql .= " , prazo_solicitado 		= " . cBANCO::StringOk($this->mprazo_solicitado);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /*
     * Rotina sem uso
      public function AtualizeAndamento(){
      $sql = "update processo_mte set ";
      $sql .= "   nu_processo 			= ".cBANCO::StringOk($this->mnu_processo);
      $sql .= " , dt_requerimento 		= ".cBANCO::DataOk($this->mdt_requerimento);
      $sql .= " , nu_oficio 				= ".cBANCO::StringOk($this->mnu_oficio);
      $sql .= " , dt_deferimento 			= ".cBANCO::DataOk($this->mdt_deferimento);
      $sql .= " , prazo_solicitado 		= ".cBANCO::StringOk($this->mprazo_solicitado);
      $sql .= " , observacao 				= ".cBANCO::StringOk($this->mobservacao);
      $sql .= " , dt_ult 				= now()";
      $sql .= " , cd_usuario			= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
      $sql .= " , no_classe			= ".cBANCO::StringOk(__CLASS__);
      $sql .= " , no_metodo			= ".cBANCO::StringOk(__FUNCTION__);
      $sql .= " where codigo = ".$this->mcodigo;
      executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
      }
     */

    /**
     * Atualiza a observacao_visto
     */
    public function AtualizeObservacaoVisto() {
        $sql = "update processo_mte set ";
        $sql .= "  observacao_visto		= " . cBANCO::StringOk($this->mobservacao_visto);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Faz esse visto
     */
    public function IgnoreVencimento() {
        $this->mfl_ignorar_vencimento = true;
        $sql = "update processo_mte set ";
        $sql .= "   fl_ignorar_vencimento	= " . cBANCO::SimNaoOk($this->mfl_ignorar_vencimento);
        $sql .= " , observacao 				= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     *
     */
    public function NotifiqueVencimento() {
        $sql = "update processo_mte set ";
        $sql .= "   fl_ignorar_vencimento	= 0";
        $sql .= " , observacao 				= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Torna esse processo o visto atual
     */
    public function TorneSeVistoAtual() {
        if ($this->mcd_candidato == '' || $this->mcd_candidato == 0) {
            $this->RecupereSe();
        }

        $sql = "update processo_mte set ";
        $sql .= "  fl_visto_atual	= 1";
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mfl_visto_atual = 1;

        $sql = "update processo_mte set ";
        $sql .= "  fl_visto_atual	 = 0";
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo      <> " . $this->mcodigo;
        $sql .= "   and cd_candidato = " . $this->mcd_candidato;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);

        $sql = "update CANDIDATO set ";
        $sql .= "  codigo_processo_mte_atual	= " . cBANCO::ChaveOk($this->mcodigo);
        $sql .= " where NU_CANDIDATO = " . $this->mcd_candidato;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    /**
     * Excluir o processo mte
     */
    public function Exclua() {
        if ($this->mcd_candidato == '') {
            $this->RecupereSe();
        }
        parent::Exclua();
        $cand = new cCANDIDATO();
        $cand->Recuperar($this->mcd_candidato);
        if ($cand->mcodigo_processo_mte_atual == $this->mcodigo) {
            $cand->AtualizeVistoAtual();
        }
        //
        // Resseta referencias a esse processo.
        $sql = "update processo_prorrog ";
        $sql.= "   set codigo_processo_mte = null ";
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql.= " where codigo_processo_mte = " . $this->mcodigo;
        cAMBIENTE::$db_pdo->exec($sql);

        $sql = "update processo_regcie ";
        $sql.= "   set codigo_processo_mte = null ";
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql.= " where codigo_processo_mte = " . $this->mcodigo;
        cAMBIENTE::$db_pdo->exec($sql);

        $sql = "update processo_emiscie ";
        $sql.= "   set codigo_processo_mte = null ";
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql.= " where codigo_processo_mte = " . $this->mcodigo;
        cAMBIENTE::$db_pdo->exec($sql);

        $sql = "update processo_coleta ";
        $sql.= "   set codigo_processo_mte = null ";
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql.= " where codigo_processo_mte = " . $this->mcodigo;
        cAMBIENTE::$db_pdo->exec($sql);

        $sql = "update processo_cancel ";
        $sql.= "   set codigo_processo_mte = null ";
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql.= " where codigo_processo_mte = " . $this->mcodigo;
        cAMBIENTE::$db_pdo->exec($sql);

        $sql = "update processo_generico ";
        $sql.= "   set codigo_processo_mte = null ";
        $sql .= " , dt_ult              = now()";
        $sql .= " , cd_usuario_ult          = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql.= " where codigo_processo_mte = " . $this->mcodigo;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Cria um processo apontando para solicita-visto
     */
    public function Inclua_ProcessoOS() {
        $sql = "insert into processo_mte (";
        $sql.= "   NU_EMPRESA";
        $sql.= " , NU_EMBARCACAO_PROJETO";
        $sql.= " , nu_servico";
        $sql.= " , cd_solicitacao";
        $sql.= " , cd_candidato";
        $sql.= " , id_solicita_visto";
        $sql.= " , dt_cad";
        $sql.= " , fl_vazio";
        $sql.= " , dt_ult";
        $sql.= " , cd_usuario";
        $sql.= " , no_classe";
        $sql.= " , no_metodo";
        $sql.= " )";
        $sql.= "values (";
        $sql.= "  " . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql.= ", " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO);
        $sql.= ", " . cBANCO::ChaveOk($this->mnu_servico);
        $sql.= ", null";
        $sql.= ", " . cBANCO::ChaveOk($this->mcd_candidato);
        $sql.= ", " . cBANCO::ChaveOk($this->mid_solicita_visto);
        $sql.= ", now()";
        $sql.= ", 0";
        $sql.= ", now()";
        $sql.= "," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql.= "," . cBANCO::StringOk(__CLASS__);
        $sql.= "," . cBANCO::StringOk(__FUNCTION__);
        $sql.= ")";
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mcodigo = mysql_insert_id();

        $servico = $this->get_servico();
        if (isset($servico)){
            if ($this->mcodigo > 0 && $servico->criaVistoNovo()) {
                $this->TorneSeVistoAtual();
            }
        }
    }

    /**
     * Salva os dados da OS no processo e cria o processo caso ele não exista
     * @param cORDEMSERVICO $pOS
     * @param cCANDIDATO $pCand
     */
    public function SalvarDadosOs($pOS, $pCand) {
        // Verifica se já existe um processo para essa OS
        $sql = "select codigo ";
        $sql .= " from processo_mte";
        $sql .= " where id_solicita_visto =" . $pOS->mID_SOLICITA_VISTO;
        $sql .= "   and cd_candidato      =" . $pCand->mNU_CANDIDATO;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $rs = mysql_fetch_array($res);
        if (intval($rs['codigo']) > 0) {
            $this->RecupereSe($rs['codigo']);
            $this->mNU_EMPRESA = $pOS->mNU_EMPRESA;
            $this->mNU_EMBARCACAO_PROJETO = $pOS->mNU_EMBARCACAO_PROJETO;
            $this->mnu_servico = $pOS->mNU_SERVICO;
            $this->mprazo_solicitado = $pOS->mDT_PRAZO_ESTADA_SOLICITADO;
            $this->Atualize_DadosOS();
        } else {
            $this->mcodigo = 0;
            $this->mNU_EMPRESA = $pOS->mNU_EMPRESA;
            $this->mNU_EMBARCACAO_PROJETO = $pOS->mNU_EMBARCACAO_PROJETO;
            $this->mnu_servico = $pOS->mNU_SERVICO;
            $this->mid_solicita_visto = $pOS->mID_SOLICITA_VISTO;
            $this->mcd_candidato = $pCand->mNU_CANDIDATO;
            $this->mprazo_solicitado = $pOS->mDT_PRAZO_ESTADA_SOLICITADO;
            $this->mcd_usuario = cSESSAO::$mcd_usuario;
            $this->Salve();
            // Se não for troca de repartição consular nem mudança de embarcação, altera o visto atual.
            //if ($this->AlteraVistoAtual()) {
            if ($pOS->get_servico()->criaVistoNovo()){
                $this->TorneSeVistoAtual();
            }
            $pCand->Ative(cSESSAO::$mcd_usuario);
        }
    }

    /**
     * Informa se esse tipo de processo afeta o visto atual ou nao
     * @ret boolean
     * 
     * 01/08/2014 -> Ficou obsoleto porque agora o controle de novo visto é feito por atributo no servico
     */
//    public function AlteraVistoAtual() {
//        if ($this->mnu_servico != "31" && $this->mnu_servico != "49") {
//            return true;
//        } else {
//            return false;
//        }
//    }

    /**
     * Calcula quantos processos de coleta estão associados a esse visto
     */
    public function QtdColetas() {
        $sql = "select count(*)";
        $sql .= " from processo_coleta";
        $sql .= " where codigo_processo_mte =" . $this->mcodigo;
        if ($res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__)) {
            $rs = mysql_fetch_array($res);
            return intval($rs[0]);
        } else {
            return 0;
        }
    }

    public function ExcluaPelaOs($pID_SOLICITA_VISTO, $pNU_CANDIDATO) {
        // Exclui, verificando em quais candidatos esse processo é o atual
//		$sql = "delete from processo_mte";
//		$sql.= " where id_solicita_visto = ".$pID_SOLICITA_VISTO;
//		$sql.= " and cd_candidato = ".$pNU_CANDIDATO;
//		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
        if (intval($this->mcodigo) == 0) {
            throw new exception("Não foi possível excluir pois a chave do processo não foi informada.(cprocesso_mte->ExcluaPelaOs)");
        }
        $this->Exclua();
        // Em seguida atribui o novo processo atual
    }

    public function RecupereSePelaOs($pID_SOLICITA_VISTO, $pNU_CANDIDATO) {
        $this->mid_solicita_visto = $pID_SOLICITA_VISTO;
        $this->mcd_candidato = $pNU_CANDIDATO;
        if (!intval($this->mid_solicita_visto) > 0) {
            throw new Exception("Código da OS não informado");
        }
        $sql = "select pm.*, NO_STATUS_ANDAMENTO, NO_STATUS_CONCLUSAO, sv.id_status_sol "
              . " from processo_mte pm";
        $sql.= " left join status_andamento sa on sa.id_status_andamento = pm.id_status_andamento";
        $sql.= " left join status_conclusao sc on sc.id_status_conclusao = pm.id_status_conclusao";
        $sql.= " join solicita_visto sv on sv.id_solicita_visto = pm.id_solicita_visto";
        $sql.= " where pm.id_solicita_visto = " . $this->mid_solicita_visto;
        $sql.= "   and cd_candidato      = " . $this->mcd_candidato;
        $res = cAMBIENTE::$db_pdo->query($sql);
        if ($rs = $res->fetch(PDO::FETCH_BOTH)) {
            cBANCO::CarreguePropriedades($rs, $this);
        }
    }

    public function RecupereSePelaOsSemCand($pID_SOLICITA_VISTO) {
        $this->mid_solicita_visto = $pID_SOLICITA_VISTO;

        if (!intval($this->mid_solicita_visto) > 0) {
            throw new Exception("Código da OS não informado");
        }
        $sql = "select pm.*, NO_SERVICO_RESUMIDO, NO_STATUS_ANDAMENTO, NO_STATUS_CONCLUSAO, sv.id_status_sol
				  from processo_mte pm";
        $sql.= " left join status_andamento sa on sa.id_status_andamento = pm.id_status_andamento";
        $sql.= " left join status_conclusao sc on sc.id_status_conclusao = pm.id_status_conclusao";
        $sql.= " left join servico s on s.nu_servico = pm.nu_servico";
        $sql.= " join solicita_visto sv on sv.id_solicita_visto = pm.id_solicita_visto";
        $sql.= " where pm.id_solicita_visto = " . $this->mid_solicita_visto;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        $this->mnu_servico = $rs["nu_servico"];
        $this->mcodigo = $rs["codigo"];
        $this->mcodigo_processo_mte = $rs["codigo_processo_mte"];
        $this->mNO_SERVICO_RESUMIDO = $rs["NO_SERVICO_RESUMIDO"];
        cBANCO::CarreguePropriedades($rs, $this);
    }

    public function AtualizeProtocolo() {
        $sql = "update processo_mte set ";
        $sql .= "   dt_envio_bsb		= " . cBANCO::DataOk($this->mdt_envio_bsb);
        $sql .= " , dt_recebimento_bsb	= " . cBANCO::DataOk($this->mdt_recebimento_bsb);
        $sql .= " , nu_processo			= " . cBANCO::StringOk($this->mnu_processo);
        $sql .= " , dt_requerimento		= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , observacao			= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AtualizeProtocoloPelaOs($pid_solicita_visto) {
        $sql = "update processo_mte set ";
        $sql .= "   dt_envio_bsb		= " . cBANCO::DataOk($this->mdt_envio_bsb);
        $sql .= " , dt_recebimento_bsb	= " . cBANCO::DataOk($this->mdt_recebimento_bsb);
        $sql .= " , nu_processo			= " . cBANCO::StringOk($this->mnu_processo);
        $sql .= " , dt_requerimento		= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , observacao			= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where id_solicita_visto = " . $pid_solicita_visto;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AtualizeAndamento() {
        $sql = "update processo_mte set ";
        $sql .= "   dt_envio_bsb		= " . cBANCO::DataOk($this->mdt_envio_bsb);
        $sql .= " , dt_recebimento_bsb	= " . cBANCO::DataOk($this->mdt_recebimento_bsb);
        $sql .= " , nu_processo			= " . cBANCO::StringOk($this->mnu_processo);
        $sql .= " , dt_requerimento		= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , observacao			= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , dt_ult_atu_andamento= " . cBANCO::DataOk($this->mdt_ult_atu_andamento);
//        $sql .= " , ID_STATUS_ANDAMENTO = " . cBANCO::ChaveOk($this->mID_STATUS_ANDAMENTO);
//        $sql .= " , ID_STATUS_CONCLUSAO = " . cBANCO::ChaveOk($this->mID_STATUS_CONCLUSAO);
        $sql .= " , dt_deferimento		= " . cBANCO::DataOk($this->mdt_deferimento);
        $sql .= " , dt_publicacao_dou	= " . cBANCO::DataOk($this->mdt_publicacao_dou);
        $sql .= " , nu_oficio			= " . cBANCO::StringOk($this->mnu_oficio);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AtualizeAndamentoPelaOs($pid_solicita_visto) {
        $sql = "update processo_mte set ";
        $sql .= "   dt_envio_bsb		= " . cBANCO::DataOk($this->mdt_envio_bsb);
        $sql .= " , dt_recebimento_bsb	= " . cBANCO::DataOk($this->mdt_recebimento_bsb);
        $sql .= " , nu_processo			= " . cBANCO::StringOk($this->mnu_processo);
        $sql .= " , dt_requerimento		= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , observacao			= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , dt_ult_atu_andamento= " . cBANCO::DataOk($this->mdt_ult_atu_andamento);
//        $sql .= " , ID_STATUS_ANDAMENTO = " . cBANCO::ChaveOk($this->mID_STATUS_ANDAMENTO);
//        $sql .= " , ID_STATUS_CONCLUSAO = " . cBANCO::ChaveOk($this->mID_STATUS_CONCLUSAO);
        $sql .= " , dt_deferimento		= " . cBANCO::DataOk($this->mdt_deferimento);
        $sql .= " , dt_publicacao_dou	= " . cBANCO::DataOk($this->mdt_publicacao_dou);
        $sql .= " , nu_oficio			= " . cBANCO::StringOk($this->mnu_oficio);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where id_solicita_visto = " . $pid_solicita_visto;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AtualizeConclusao() {
        $sql = "update processo_mte set ";
        $sql .= "   dt_publicacao_dou	= " . cBANCO::DataOk($this->mdt_publicacao_dou);
        $sql .= " , nu_oficio			= " . cBANCO::StringOk($this->mnu_oficio);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo = " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AtualizeConclusaoPelaOs($pid_solicita_visto) {
        $sql = "update processo_mte set ";
        $sql .= "   dt_publicacao_dou	= " . cBANCO::DataOk($this->mdt_publicacao_dou);
        $sql .= " , nu_oficio			= " . cBANCO::StringOk($this->mnu_oficio);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where id_solicita_visto = " . $pid_solicita_visto;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function Migre(cCANDIDATO $pcandidato_origem, cCANDIDATO $pcandidato_destino, $pMigreProcessosAssociados) {
        if ($pMigreProcessosAssociados) {
            $processo = new cprocesso_prorrog();
            $processo->MigrePeloProcessoMte($pcandidato_origem, $pcandidato_destino, $this->mcodigo);
            $processo = new cprocesso_regcie();
            $processo->MigrePeloProcessoMte($pcandidato_origem, $pcandidato_destino, $this->mcodigo);
            $processo = new cprocesso_coleta();
            $processo->MigrePeloProcessoMte($pcandidato_origem, $pcandidato_destino, $this->mcodigo);
            $processo = new cprocesso_emiscie();
            $processo->MigrePeloProcessoMte($pcandidato_origem, $pcandidato_destino, $this->mcodigo);
            $processo = new cprocesso_cancel();
            $processo->MigrePeloProcessoMte($pcandidato_origem, $pcandidato_destino, $this->mcodigo);
        }
        $msg = "(" . date('d/m/y') . ") Processo transferido do candidato " . $pcandidato_origem->mNOME_COMPLETO . " (" . $pcandidato_origem->mNU_CANDIDATO . ") para o candidato " . $pcandidato_destino->mNOME_COMPLETO . " (" . $pcandidato_destino->mNU_CANDIDATO . ") - por " . cSESSAO::$mnome;

        if ($this->mobservacao == '') {
            $this->mobservacao = $msg;
        } else {
            $this->mobservacao .= '<br/>' . $msg;
        }
        $this->mcd_usuario = $pcandidato_destino->mNU_CANDIDATO;

        $sql = "update processo_mte";
        $sql .= "   set cd_candidato = " . $pcandidato_destino->mNU_CANDIDATO;
        $sql .= "     , cd_usuario	= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "     , dt_ult		= now()";
        $sql .= "     , no_classe	= " . cBANCO::StringOk(__CLASS__);
        $sql .= "     , no_metodo	= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= "     , observacao	= " . cBANCO::StringOk($this->mobservacao);
        $sql .= "     , fl_visto_atual = 0";
        $sql .= " where codigo       = " . $this->mcodigo;

        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $pcandidato_origem->AtualizeVistoAtual('null');
        $pcandidato_origem->AtualizeVistoAtual();

        if (intval($pcandidato_destino->mcodigo_processo_mte_atual) == 0) {
            $pcandidato_destino->AtualizeVistoAtual($this->mcodigo);
        }
    }

    public function AtualizeAtributo($pAtributo, $pValor) {
        $sql = "update processo_mte set " . $pAtributo . " = " . cBANCO::StringOk($pValor) . " where codigo = " . $this->mcodigo;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AtualizeAtributoString($pAtributo, $pValor) {
        $sql = "update processo_mte
				set " . $pAtributo . " = " . cBANCO::StringOk($pValor) . "
				, cd_usuario	= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario) . "
				, dt_ult		= now()
				where codigo = " . $this->mcodigo;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function AtualizeAtributoSimNao($pAtributo, $pValor) {
        $sql = "update processo_mte
				set " . $pAtributo . " = " . cBANCO::SimNaoOk($pValor) . "
				, cd_usuario	= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario) . "
				, dt_ult		= now()
				where codigo = " . $this->mcodigo;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function Atualizaobservacao_visto($pcodigo, $pValor) {
        $this->mcodigo = $pcodigo;
        $this->AtualizeAtributoString('observacao_visto', $pValor);
    }

    public function Atualizafl_cp_passaporte($pcodigo, $pValor) {
        $this->mcodigo = $pcodigo;
        $this->AtualizeAtributoSimNao('fl_cp_passaporte', $pValor);
    }

    public function Atualizafl_form_144($pcodigo, $pValor) {
        $this->mcodigo = $pcodigo;
        $this->AtualizeAtributoSimNao('fl_form_144', $pValor);
    }

    public function RegistraDtEnvioBSB($pid_solicita_visto, $pdt_envio_bsb) {
        $sql = "update processo_mte
				set dt_envio_bsb = " . cBANCO::DataOk($pdt_envio_bsb) . "
				, cd_usuario	= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario) . "
				, dt_ult		= now()
				where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function CursorProcessos() {
        $sql = "  select 'mte' AS tipo
					    ,mte.cd_candidato AS cd_candidato
					    ,mte.codigo AS codigo
					    ,null as codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,mte.id_solicita_visto AS id_solicita_visto
					    ,mte.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,1 AS ordem_temp
					    , dt_cad
					    , mte.nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_mte mte
					where codigo_processo_mte = " . $this->mcodigo . " and fl_vazio = 0 and mte.nu_servico = 31
					union
					select 'coleta' AS tipo
					    ,col.cd_candidato AS cd_candidato
					    ,col.codigo AS codigo
					    ,col.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,col.id_solicita_visto AS id_solicita_visto
					    ,0 AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,2 AS ordem_temp
					    , dt_cad
					    , col.nu_servico
					    , null as dt_ordem
					  from processo_coleta col
					where codigo_processo_mte = " . $this->mcodigo . "
					union
					  select 'regcie' AS tipo
					    ,reg.cd_candidato AS cd_candidato
					    ,reg.codigo AS codigo
					    ,reg.codigo_processo_mte AS codigo_processo_mte
					    ,reg.codigo_processo_prorrog AS codigo_processo_prorrog
					    ,reg.id_solicita_visto AS id_solicita_visto
					    ,reg.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,(case reg.nu_servico when 58 then 6 else 3 end) AS ordem_temp
					    , dt_cad
					    , reg.nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_regcie reg
					where codigo_processo_mte = " . $this->mcodigo . " and fl_vazio = 0
					union
					  select 'regcie' AS tipo
					    ,reg.cd_candidato AS cd_candidato
					    ,reg.codigo AS codigo
					    ,reg.codigo_processo_mte AS codigo_processo_mte
					    ,reg.codigo_processo_prorrog AS codigo_processo_prorrog
					    ,reg.id_solicita_visto AS id_solicita_visto
					    ,reg.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,(case reg.nu_servico when 58 then 6 else 3 end) AS ordem_temp
					    , dt_cad
					    , reg.nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_regcie reg
					where codigo_processo_mte is null
					and codigo_processo_prorrog in (select codigo from processo_prorrog where codigo_processo_mte = " . $this->mcodigo . ")
					and fl_vazio = 0
					union
					  select 'emiscie' AS tipo
					    ,emi.cd_candidato AS cd_candidato
					    ,emi.codigo AS codigo
					    ,emi.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,emi.id_solicita_visto AS id_solicita_visto
					    ,emi.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,4 AS ordem_temp
					    , dt_cad
					    , emi.nu_servico
					    , coalesce(dt_emissao, dt_cad) as dt_ordem
					  from processo_emiscie emi
					where codigo_processo_mte = " . $this->mcodigo . " and fl_vazio = 0
					union
					  select 'prorrog' AS tipo
					    ,pro.cd_candidato AS cd_candidato
					    ,pro.codigo AS codigo
					    ,pro.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,pro.id_solicita_visto AS id_solicita_visto
					    ,pro.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,5 AS ordem_temp
					    , dt_cad
					    , pro.nu_servico
					    , coalesce(dt_requerimento, dt_cad) as dt_ordem
					  from processo_prorrog pro
					where codigo_processo_mte = " . $this->mcodigo . " and fl_vazio = 0
					union
					  select 'cancel' AS tipo
					    ,can.cd_candidato AS cd_candidato
					    ,can.codigo AS codigo
					    ,can.codigo_processo_mte AS codigo_processo_mte
					    ,NULL AS codigo_processo_prorrog
					    ,can.id_solicita_visto AS id_solicita_visto
					    ,can.fl_vazio AS fl_vazio
                                            , coalesce(ordem, 0) ordem
					    ,7 AS ordem_temp
					    , dt_cad
					    , can.nu_servico
					    , coalesce(dt_processo, dt_cad) as dt_ordem
					from processo_cancel can
					where codigo_processo_mte = " . $this->mcodigo . " and fl_vazio = 0
					";
        $sql .= "  order by ordem, dt_ordem, ordem_temp asc";
        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo processos satelites');
        return $rs;
    }

    /**
     * Retorna um array com os objetos dos processos do visto, os mais recentes primeiro.
     */
    public function Processos() {
        $cursor = $this->CursorProcessos();
        $ret = array();
        if (is_object($cursor)) {
            while ($rs = $cursor->fetch(PDO::FETCH_ASSOC)) {
                $proc = cprocesso::FabricaProcessoPeloHistorico($rs['codigo'], $rs['tipo']);
                array_unshift($ret, $proc);
            }
        }
        $this->processos = $ret;
        $this->processos_carregados = true;
        return $ret;
    }

    /**
     * Controla e informa a situacao do visto
     */
    protected $situacao_prazo_estada;
    protected $situacao_texto;
    protected $situacao_origem;

    public function get_situacao_prazo_estada() {
        if (!$this->situacao_visto_atualizada) {
            $this->AtualizaSituacaoVisto();
        }
        return $this->situacao_prazo_estada;
    }

    public function get_situacao_texto() {
        if (!$this->situacao_visto_atualizada) {
            $this->AtualizaSituacaoVisto();
        }
        return $this->situacao_texto;
    }

    public function get_situacao_origem() {
        if (!$this->situacao_visto_atualizada) {
            $this->AtualizaSituacaoVisto();
        }
        return $this->situacao_origem;
    }

    public function get_processo_regcie() {
        return $this->processo_regcie;
    }

    public function get_processo_prorrog() {
        return $this->processo_prorrog;
    }

    public function get_processo_coleta() {
        return $this->processo_coleta;
    }

    public function get_processo_emiscie() {
        return $this->processo_emiscie;
    }

    public function AtualizaSituacaoVisto() {
        $analise_concluida = false;
        $i = 0;
        $prazo_estada = '';
        $texto = '';
        $origem = '';
        if (!$this->processos_carregados) {
            $this->Processos();
        }
        while ($i < count($this->processos) && !$analise_concluida) {
            $processo = $this->processos[$i];
            if ($processo->SituacaoVistoProcesso($prazo_estada, $texto, $origem)) {
                $this->situacao_prazo_estada = $prazo_estada;
                $this->situacao_texto = $texto;
                $this->situacao_origem = $origem;
                $analise_concluida = true;
                $this->situacao_visto_atualizada = true;
                return true;
            } else {
                $i++;
            }
        }
        if ($this->SituacaoVistoProcesso($prazo_estada, $texto, $origem)) {
            $this->situacao_prazo_estada = $prazo_estada;
            $this->situacao_texto = $texto;
            $this->situacao_origem = $origem;
            $analise_concluida = true;
            $this->situacao_visto_atualizada = true;
            return true;
        }
    }

    public function SituacaoVistoProcesso(&$pprazo_estada, &$ptexto, &$porigem) {
        if ($this->mnu_servico == 31  //Mudança de embarcação
                || $this->mnu_servico == 49  //Troca repartição consular
                || $this->mnu_servico == 7  //Visto de dependentes
                || $this->mnu_servico == 6  //Visto de Emergencia
                || $this->mnu_servico == 92  //VITEM II
        ) {
            return false;
        } else {
            if (trim($this->mnu_processo) == '') {
                $pprazo_estada = "--";
                $ptexto = "(not available)";
                $porigem = "Autorização sem número do processo!";
                return true;
            } else {
                if ($this->mid_solicita_visto > 0){
                    $sql = "select sv.id_status_sol, ss.fl_encerrada, ss.no_status_sol_res , ss.no_status_sol from status_solicitacao ss, solicita_visto sv where ss.id_status_sol = sv.id_status_sol and sv.id_solicita_visto = ".$this->mid_solicita_visto;
                    $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__ . '->Obtendo processos satelites');
                    $rs = $res->fetch(PDO::FETCH_BOTH);
                    if ($rs['id_status_sol'] == 6){
                        switch ($this->mID_STATUS_CONCLUSAO) {
                            case "2":  // (deferido)
                                $pprazo_estada = "--";
                                $ptexto = "Aguardando coleta de visto";
                                $porigem = "Autorização deferida, sem registro";
                                return true;
                                break;
                            case "3":  // (indeferido)
                                $pprazo_estada = "--";
                                $ptexto = "Processo indeferido";
                                $porigem = "Autorização indeferida";
                                return true;
                                break;
                            case "4":  // (arquivado)
                                $pprazo_estada = "--";
                                $ptexto = "Processo arquivado";
                                $porigem = "Autorização arquivada";
                                return true;
                                break;
                            default:
                                $pprazo_estada = "--";
                                $ptexto = "(not available)";
                                $porigem = "Autorização encerrada mas sem status de conclusão";
                                return true;
                                break;
                        }
                    } else {
                        switch ($rs['id_status_sol']) {
                            case "20":  // (deferido)
                                $pprazo_estada = "--";
                                $ptexto = "Aguardando coleta de visto";
                                $porigem = "Autorização deferida, sem registro";
                                return true;
                                break;
                            case "21":  // (indeferido)
                                $pprazo_estada = "--";
                                $ptexto = "Processo indeferido";
                                $porigem = "Autorização indeferida";
                                return true;
                                break;
                            case "22":  // (arquivado)
                                $pprazo_estada = "--";
                                $ptexto = "Processo arquivado";
                                $porigem = "Autorização arquivada";
                                return true;
                                break;
                            default:
                                $pprazo_estada = "--";
                                $ptexto = "(not available)";
                                $porigem = "Autorização em andamento. Status: ".$rs['no_status_sol'];
                                return true;
                                break;
                        }
                    }
                } else {
                    switch ($this->mID_STATUS_CONCLUSAO) {
                        case "1":  // (n/a)
                            $pprazo_estada = "--";
                            $ptexto = "(not available)";
                            $porigem = "Autorização sem status de conclusão";
                            return true;
                            break;
                        case "2":  // (deferido)
                            $pprazo_estada = "--";
                            $ptexto = "Aguardando coleta de visto";
                            $porigem = "Autorização deferida, sem registro";
                            return true;
                            break;
                        case "3":  // (indeferido)
                            $pprazo_estada = "--";
                            $ptexto = "Processo indeferido";
                            $porigem = "Autorização indeferida";
                            return true;
                            break;
                        case "4":  // (arquivado)
                            $pprazo_estada = "--";
                            $ptexto = "Processo arquivado";
                            $porigem = "Autorização arquivada";
                            return true;
                            break;
                        default:
                            $pprazo_estada = "--";
                            $ptexto = "(not available)";
                            $porigem = "Autorização sem status de conclusão";
                            return true;
                            break;
                    }

                }
            }
        }
    }

    public static function Atualiza_dt_envio_bsb($pid_solicita_visto, $pdt_envio_bsb, $pcd_usuario) {
        $sql = "update processo_mte
			       set dt_envio_bsb = " . cBANCO::DataOk($pdt_envio_bsb) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . cBANCO::ChaveOk($pid_solicita_visto);
        cAMBIENTE::$db_pdo->exec($sql);
        if ($pdt_envio_bsb != '') {
            $status = 4;
        } else {
            $status = 3;
        }
        $sql = "update solicita_visto
		   set id_status_sol = " . $status . "
                     , dt_envio_bsb = " . cBANCO::DataOk($pdt_envio_bsb) . "
                     , cd_usuario_alteracao = " . $pcd_usuario . "
                     , soli_dt_alteracao = now()
		 where solicita_visto.id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_dt_requerimento($pdt_requerimento, $pid_solicita_visto, $pcd_usuario) {
        $sql = "update processo_mte
			       set dt_requerimento = " . cBANCO::DataOk($pdt_requerimento) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . cBANCO::ChaveOk($pid_solicita_visto);
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function Atualiza_nu_processo($pnu_processo, $pid_solicita_visto, $pcd_usuario) {
        $sql = "update processo_mte
			       set nu_processo = " . cBANCO::StringOk($pnu_processo) . "
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . cBANCO::ChaveOk($pid_solicita_visto);
        cAMBIENTE::$db_pdo->exec($sql);

        //TODO: Atualizar o status da OS para não cadastrada
//		if($pnu_processo != ''){
//			cORDEMSERVICO::IndicaProtocoloRealizadoAutomaticamente($pid_solicita_visto, $pcd_usuario);
//		}
    }

    public static function Atualiza_nu_processoDigital($pnu_processo, $pid_solicita_visto, $pcd_usuario) {
        $sql = "update processo_mte
			       set nu_processo = " . cBANCO::StringOk($pnu_processo) . "
					 , dt_envio_bsb = now()
					 , dt_requerimento = now()
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . cBANCO::ChaveOk($pid_solicita_visto);
        cAMBIENTE::$db_pdo->exec($sql);

        //TODO: Atualizar o status da OS para não cadastrada
//		if($pnu_processo != ''){
//			cORDEMSERVICO::IndicaProtocoloRealizadoAutomaticamente($pid_solicita_visto, $pcd_usuario);
//		}
    }
    public static function RetirarDoProtocoloBsb($pid_solicita_visto, $pcd_usuario) {
        $sql = "update processo_mte
			       set dt_envio_bsb = null
					 , cd_usuario = " . cBANCO::ChaveOk($pcd_usuario) . "
			     where id_solicita_visto = " . cBANCO::ChaveOk($pid_solicita_visto);
        cAMBIENTE::$db_pdo->exec($sql);

        $sql = "update solicita_visto
			       set id_status_sol = 3
			     where solicita_visto.id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Retorna um recordset com os registros do log do processo
     * @param int $codigo Chave do processo
     * @return PDOStatement
     */
    public static function cursorLog($codigo){
    	$sql = "select historico_processo_mte.* "
    			."     , date_format(hmte_dt_evento, '%d/%m/%Y %H:%i:%s') hmte_dt_evento_fmt"
    			."     , usuarios.nome"
    			."  from historico_processo_mte"
    			."  left join usuarios on usuarios.cd_usuario = historico_processo_mte.cd_usuario "
    			." where codigo = ".$codigo
    			." order by historico_processo_mte.hmte_dt_evento desc ";
    	$rs = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
    	return $rs;
    }
    
}
