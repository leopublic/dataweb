<?php

class cprocurador_empresa extends cMODELO {

    public $mnu_empresa;
    public $mnu_procurador;
    public $mno_procurador;
    public $mco_cargo_funcao;
    public $mnu_cpf;
    public $mnu_identidade;
    public $mno_emissor_identidade;
    public $mdt_emissao_identidade;
    public $mno_email;
    public $mdt_cadastramento;
    public $mco_usuario_cadastramento;
    public $mco_nacionalidade;
    public $mco_estado_civil;
    public $mnu_ordem;
    public $mimagem;
    public function get_nu_empresa() {
        return $this->mnu_empresa;
    }

    public function set_nu_empresa($nu_empresa) {
        $this->mnu_empresa = $nu_empresa;
    }

    public function get_nu_procurador() {
        return $this->mnu_procurador;
    }

    public function set_nu_procurador($nu_procurador) {
        $this->mnu_procurador = $nu_procurador;
    }

    public function get_no_procurador() {
        return $this->mno_procurador;
    }

    public function set_no_procurador($no_procurador) {
        $this->mno_procurador = $no_procurador;
    }

    public function get_co_cargo_funcao() {
        return $this->mco_cargo_funcao;
    }

    public function set_co_cargo_funcao($co_cargo_funcao) {
        $this->mco_cargo_funcao = $co_cargo_funcao;
    }

    public function get_nu_cpf() {
        return $this->mnu_cpf;
    }

    public function set_nu_cpf($nu_cpf) {
        $this->mnu_cpf = $nu_cpf;
    }

    public function get_nu_identidade() {
        return $this->mnu_identidade;
    }

    public function set_nu_identidade($nu_identidade) {
        $this->mnu_identidade = $nu_identidade;
    }

    public function get_no_emissor_identidade() {
        return $this->mno_emissor_identidade;
    }

    public function set_no_emissor_identidade($no_emissor_identidade) {
        $this->mno_emissor_identidade = $no_emissor_identidade;
    }

    public function get_dt_emissao_identidade() {
        return $this->mdt_emissao_identidade;
    }

    public function set_dt_emissao_identidade($dt_emissao_identidade) {
        $this->mdt_emissao_identidade = $dt_emissao_identidade;
    }

    public function get_no_email() {
        return $this->mno_email;
    }

    public function set_no_email($no_email) {
        $this->mno_email = $no_email;
    }

    public function get_dt_cadastramento() {
        return $this->mdt_cadastramento;
    }

    public function set_dt_cadastramento($dt_cadastramento) {
        $this->mdt_cadastramento = $dt_cadastramento;
    }

    public function get_co_usuario_cadastramento() {
        return $this->mco_usuario_cadastramento;
    }

    public function set_co_usuario_cadastramento($co_usuario_cadastramento) {
        $this->mco_usuario_cadastramento = $co_usuario_cadastramento;
    }

    public function get_co_nacionalidade() {
        return $this->mco_nacionalidade;
    }

    public function set_co_nacionalidade($co_nacionalidade) {
        $this->mco_nacionalidade = $co_nacionalidade;
    }

    public function get_co_estado_civil() {
        return $this->mco_estado_civil;
    }

    public function set_co_estado_civil($co_estado_civil) {
        $this->mco_estado_civil = $co_estado_civil;
    }

    public function get_nu_ordem() {
        return $this->mnu_ordem;
    }

    public function set_nu_ordem($nu_ordem) {
        $this->mnu_ordem = $nu_ordem;
    }

    public function get_imagem(){
        error_log('Entrou no imagem');
        $ximagem = '';
        if (substr($this->mno_procurador, 0, 12) == 'Leonardo dos') {
            $ximagem = '1.jpg';
        } elseif (substr($this->mno_procurador, 0, 7) == 'Daniela') {
            $ximagem = '2.jpg';
        } elseif (substr($this->mno_procurador, 0, 7) == 'Daniel ') {
            $ximagem = '4.jpg';
        } elseif (substr($this->mno_procurador, 0, 5) == 'Vilma') {
            $ximagem = '5.jpg';
        } elseif (substr($this->mno_procurador, 0, 4) == 'Mari') {
            $ximagem = '3.jpg';
        }
        error_log("mimagem=".$ximagem. " nome =".$this->mno_procurador);
        return $ximagem;
    }

    public function get_caminho_imagem(){
        return cAMBIENTE::$m_raiz.'/imagens/assinaturas/'.$this->get_imagem();
    }

    public function get_assinatura_encoded(){
        return base64_encode(file_get_contents($this->get_caminho_imagem()));
    }

    public function sql_RecuperePeloId() {

    }

    public function RecupereSe() {
        $sql = "select nu_empresa
					 , nu_procurador
					 , no_procurador
					 , co_cargo_funcao
					 , co_nacionalidade
					 , nu_cpf
					 , nu_identidade
					 , no_emissor_identidade
					 , dt_emissao_identidade
					 , no_email
					 , dt_cadastramento
					 , co_usuario_cadastramento
					 , nu_ordem
					 , fc.no_funcao
				  from procurador_empresa
				  left join funcao_cargo fc on fc.co_funcao = co_cargo_funcao
				 where nu_empresa    = " . $this->mnu_empresa . "
				   and nu_procurador = " . $this->mnu_procurador;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if ($rs['nu_empresa'] != '') {
            $this->CarreguePropriedadesPeloRecordset($rs, 'm');
        }
    }

    public function sql_Liste() {
        
    }

    public function Atualizar() {
        
    }

    public function Incluir() {
        
    }

    public static function RecordsetCompleto($pnu_empresa, $pnu_procurador) {
        $sql = "select nu_empresa
					 , nu_procurador
					 , no_procurador
					 , co_cargo_funcao
					 , co_nacionalidade
					 , nu_cpf
					 , nu_identidade
					 , no_emissor_identidade
					 , dt_emissao_identidade
					 , no_email
					 , dt_cadastramento
					 , co_usuario_cadastramento
					 , nu_ordem
					 , fc.no_funcao
				  from procurador_empresa
				  left join funcao_cargo fc on fc.co_funcao = co_cargo_funcao
				 where nu_empresa    = " . $pnu_empresa . "
				   and nu_procurador = " . $pnu_procurador;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if (substr($rs['no_procurador'], 0, 12) == 'Leonardo dos') {
            $rs['imagem'] = '1.jpg';
        } elseif (substr($rs['no_procurador'], 0, 7) == 'Daniela') {
            $rs['imagem'] = '2.jpg';
        } elseif (substr($rs['no_procurador'], 0, 7) == 'Daniel ') {
            $rs['imagem'] = '4.jpg';
        } elseif (substr($rs['no_procurador'], 0, 5) == 'Vilma') {
            $rs['imagem'] = '5.jpg';
        } elseif (substr($rs['no_procurador'], 0, 4) == 'Mari') {
            $rs['imagem'] = '3.jpg';
        }
        return $rs;
    }

}
