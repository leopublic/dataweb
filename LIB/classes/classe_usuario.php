<?php

class objUsuario {

 var $cd_usuario=0;
 var $cd_perfil=0;
 var $cd_empresa=0;
 var $nm_login="";
 var $nm_senha="";
 var $nm_nome="";
 var $nm_email="";
 var $cd_superior=0;
 var $dt_cadastro = "";
 var $lista_empresas="";

 function objUsuario() { }

 function GETcd_usuario() { return $this->cd_usuario; }
 function GETcd_perfil() { return $this->cd_perfil; }
 function GETcd_empresa() { return $this->cd_empresa; }
 function GETnm_login() { return $this->nm_login; }
 function GETnm_senha() { return $this->nm_senha; }
 function GETnm_nome() { return $this->nm_nome; }
 function GETnm_email() { return $this->nm_email; }
 function GETcd_superior() { return $this->cd_superior; }
 function GETdt_cadastro() { return $this->dt_cadastro; }
 function GETlista_empresas() { return $this->lista_empresas; }

 function SETcd_usuario($aux) { $this->cd_usuario = $aux; }
 function SETcd_perfil($aux) { $this->cd_perfil = $aux; }
 function SETcd_empresa($aux) { $this->cd_empresa = $aux; }
 function SETnm_login($aux) { $this->nm_login = $aux; }
 function SETnm_senha($aux) { $this->nm_senha = $aux; }
 function SETnm_nome($aux) { $this->nm_nome = $aux; }
 function SETnm_email($aux) { $this->nm_email = $aux; }
 function SETcd_superior($aux) { $this->cd_superior = $aux; }
 function SETdt_cadastro($aux) { $this->dt_cadastro = $aux; }
 function SETlista_empresas($aux) { $this->lista_empresas = $aux; }
}

class objPerfil {

 var $cd_perfil=0;
 var $nm_perfil="";
 var $eh_funcionario="";
 var $eh_armador="";
 var $eh_operador="";
 var $eh_operrestrito="";
 var $eh_cliente="";
 var $cd_nivel=0;
 var $eh_financeiro="";

 function objPerfil() { }

 function GETcd_perfil() { return $this->cd_perfil; }
 function GETnm_perfil() { return $this->nm_perfil; }
 function GETeh_funcionario() { return $this->eh_funcionario; }
 function GETeh_armador() { return $this->eh_armador; }
 function GETeh_operador() { return $this->eh_operador; }
 function GETeh_operrestrito() { return $this->eh_operrestrito; }
 function GETeh_cliente() { return $this->eh_cliente; }
 function GETcd_nivel() { return $this->cd_nivel; }
 function GETeh_financeiro() { return $this->eh_financeiro; }

 function SETcd_perfil($aux) { $this->cd_perfil = $aux; }
 function SETnm_perfil($aux) { $this->nm_perfil = $aux; }
 function SETeh_funcionario($aux) { $this->eh_funcionario = $aux; }
 function SETeh_armador($aux) { $this->eh_armador = $aux; }
 function SETeh_operador($aux) { $this->eh_operador = $aux; }
 function SETeh_operrestrito($aux) { $this->eh_operrestrito = $aux; }
 function SETeh_cliente($aux) { $this->eh_cliente = $aux; }
 function SETcd_nivel($aux) { $this->cd_nivel = $aux; }
 function SETeh_financeiro($aux) { $this->eh_financeiro = $aux; }
}

class objUsuarioEmpresa {

 var $cd_usuario=0;
 var $cd_empresa=0;
 var $nm_empresa="";
 var $dt_cadastro="";

 function objUsuarioEmpresa() { }

 function GETcd_usuario() { return $this->cd_usuario; }
 function GETcd_empresa() { return $this->cd_empresa; }
 function GETnm_empresa() { return $this->nm_empresa; }
 function GETdt_cadastro() { return $this->dt_cadastro; }

 function SETcd_usuario($aux) { $this->cd_usuario = $aux; }
 function SETcd_empresa($aux) { $this->cd_empresa = $aux; }
 function SETnm_empresa($aux) { $this->nm_empresa = $aux; }
 function SETdt_cadastro($aux) { $this->dt_cadastro = $aux; }
}


function buscaUsuarios($cd_usuario,$cd_nivel,$cd_empresa,$nm_login,$nm_nome,$orderby) {
  global $myerror;
  $sql = "SELECT * FROM usuarios WHERE cd_usuario>0 ";
  if( (strlen($cd_usuario)>0) && ($cd_usuario>0) ) {
    $sql = $sql." AND cd_usuario=$cd_usuario ";
  }
  if(strlen($nm_login)>0)  {
    $sql = $sql." AND login='$nm_login' and flagativo='Y' ";
  }
  if(strlen($nm_nome)>0)  {
    $sql = $sql." AND nome LIKE '%$nm_nome%' ";
  }
  if( (strlen($cd_nivel)>0) && ($cd_nivel>0) ) {
    $sql = $sql." AND cd_nivel=$cd_nivel ";
  }
  if( (strlen($cd_empresa)>0) && ($cd_empresa>0) ) {
    $sql = $sql." AND cd_usuario in (SELECT cd_usuario FROM usuario_empresa WHERE cd_empresa=$cd_empresa) ";
  }
  $sql = $sql.$orderby;
  $rs = mysql_query($sql);
  if(mysql_error()!=0) {
    $myerror = mysql_error()." SQL=$sql";
  } else {
    $x = 0;
    while($rw=mysql_fetch_array($rs)) {
      $obj = new objUsuario();
      $cd_usuario = $rw['cd_usuario'];
      $obj->SETcd_usuario($rw['cd_usuario']);
      $obj->SETcd_perfil(buscaPerfil($rw['cd_perfil']));
      $obj->SETcd_empresa($rw['cd_empresa']);
      $obj->SETnm_login($rw['login']);
      $obj->SETnm_senha($rw['nm_senha']);
      $obj->SETnm_nome($rw['nome']);
      $obj->SETnm_email($rw['nm_email']);
      $obj->SETcd_superior($rw['cd_superior']);
      $obj->SETdt_cadastro(dataMy2BR($rw['dt_cadastro']));
      if( (strlen($cd_usuario)>0) && ($cd_usuario>0) ) {
        $obj->SETlista_empresas(buscaEmpresas($cd_usuario));
      }
      $ret[$x] = $obj;
      $x++;
    }
  }
  return $ret;
}


function buscaEmpresas($cd_usuario) {
  global $adm_empresas;
  $sql = "SELECT a.cd_empresa as cod,b.NO_RAZAO_SOCIAL as nome,a.dt_cadastro FROM usuario_empresa a,EMPRESA b WHERE a.cd_usuario=$cd_usuario AND a.cd_empresa=b.NU_EMPRESA ";
  $sql = $sql." order by b.NO_RAZAO_SOCIAL";
  $x = 0;
  $lstEmp = "";
  $rs1 = mysql_query($sql);
  while($rw=mysql_fetch_array($rs1)) {
    $obj = new objUsuarioEmpresa();
    $obj->SETcd_usuario($cd_usuario);
    $obj->SETcd_empresa($rw['cod']);
    $obj->SETnm_empresa($rw['nome']);
    $obj->SETdt_cadastro(dataMy2BR($rw['dt_cadastro']));
    $ret[$x] = $obj;
    if($x>0) { $lstEmp=$lstEmp.","; }
    $lstEmp = $lstEmp.$rw['cod'];
    $x++;
  }
  $adm_empresas = $lstEmp;
  if(strlen($lstEmp)==0) { $lstEmp = "0"; }
  $sql = "SELECT NU_EMPRESA as cod,NO_RAZAO_SOCIAL as nome FROM EMPRESA ";
  $sql = $sql." where NU_EMPRESA not in ($lstEmp) ";
  $sql = $sql." order by NO_RAZAO_SOCIAL";
  $lstEmp = "";
  $rs2 = mysql_query($sql);
  while($rw=mysql_fetch_array($rs2)) {
    $obj = new objUsuarioEmpresa();
    $obj->SETcd_usuario(0);
    $obj->SETcd_empresa($rw['cod']);
    $obj->SETnm_empresa($rw['nome']);
    $ret[$x] = $obj;
    $x++;
  }
  return $ret;
}

function buscaPerfil($cd_perfil) {
  $sql = "SELECT * FROM usuario_perfil ";
  if( (strlen($cd_perfil)>0) && ($cd_perfil>0) ) {
    $sql = $sql." WHERE cd_perfil=$cd_perfil ";
  }
  $sql = $sql." order by cd_perfil ";
  $x = 0;
  $lstEmp = "";
  $rs = mysql_query($sql);
  while($rw=mysql_fetch_array($rs)) {
    $obj = new objPerfil();
    $obj->SETcd_perfil($rw['cd_perfil']);
    $obj->SETnm_perfil($rw['nm_perfil']);
    $obj->SETeh_funcionario($rw['eh_funcionario']);
    $obj->SETeh_armador($rw['eh_armador']);
    $obj->SETeh_operador($rw['eh_operador']);
    $obj->SETeh_operrestrito($rw['eh_operrestrito']);
    $obj->SETeh_cliente($rw['eh_cliente']);
    $obj->SETcd_nivel($rw['nivel']);
    $obj->SETeh_financeiro($rw['eh_financeiro']);
    $ret[$x] = $obj;
    $x++;
  }
  return $ret;
}

function alteraUsuario($usu) {
  $ret = "";
  $tipo = "";
  $cd_usuario = $usu->GETcd_usuario();
  $cd_perfil = $usu->GETcd_perfil();
  $cd_empresa = $usu->GETcd_empresa();
  $nm_login = $usu->GETnm_login();
  $nm_senha = $usu->GETnm_senha();
  $nm_nome = $usu->GETnm_nome();
  $nm_email = $usu->GETnm_email();
  $cd_superior = $usu->GETcd_superior();
  $lista = $usu->GETlista_empresas();
  if( (strlen($cd_usuario)==0) || ($cd_usuario==0) ) {
    # Buscar novo codigo de usuario
    $cd_usuario = pegaProximo("usuarios","cd_usuario");
    $sql = "INSERT INTO usuarios (cd_usuario,cd_perfil,cd_empresa,login,nm_senha,nome,nm_email,cd_superior,dt_cadastro) ";
    $sql = $sql."VALUES ($cd_usuario,$cd_perfil,$cd_empresa,'$nm_login','$nm_senha','$nm_nome','$nm_email',$cd_superior,now())";
    $tipo = "INCLUSAO";
  } else {
    $sql = "UPDATE usuarios SET cd_perfil=$cd_perfil,cd_empresa=$cd_empresa,login='$nm_login',nm_senha='$nm_senha', ";
    $sql = $sql."nome='$nm_nome',nm_email='$nm_email',cd_superior=$cd_superior WHERE cd_usuario=$cd_usuario";
    $tipo = "ALTERACAO";
  }
  mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error()." SQL=$sql";
    gravaLogErro($sql,$ret,$tipo,"USUARIO");
  } else {
    gravaLog($sql,"",$tipo,"USUARIO");
  }
  if(strlen($ret)==0) {
    $ret = alteraUsuarioEmpresas($cd_usuario,$lista);
  }
  return $ret;
}

function alteraUsuarioEmpresas($cdusuario,$lista) {
  $ret = "";
  mysql_query("DELETE FROM usuario_empresa WHERE cd_usuario=$cdusuario");
  if(mysql_errno()!=0) {
    $ret = mysql_error();
    gravaLogErro("DELETE FROM usuario_empresa WHERE cd_usuario=$cdusuario","ALTERACAO",$tipo,"USUARIO_EMPRESA");
  } else {
    $total = count($lista);
    for($x=0;$x<$total;$x++) {
      $obj = $lista[$x];
      $cd_usuario = $obj->GETcd_usuario();
      $cd_empresa = $obj->GETcd_empresa();
      if( ($cd_usuario!=0) && ($cd_empresa>0) ) {
        $sql = "INSERT INTO usuario_empresa (cd_usuario,cd_empresa,dt_cadastro) VALUES ($cdusuario,$cd_empresa,now())";
        mysql_query($sql);
        if(mysql_errno() != 0) {
          $ret = $ret.mysql_error();
          gravaLogErro($sql,mysql_error(),"ALTERACAO","USUARIO_EMPRESA");
        }
      }
    }
  }
  return $ret;
}

function pegaPostUsuario() {
  $cd_usuario = 0+$_POST['cd_usuario'];
  $obj = new objUsuario();
  $obj->SETcd_usuario(0+$_POST['cd_usuario']);
  $obj->SETcd_perfil(0+$_POST['cd_perfil']);
  $obj->SETcd_empresa(0+$_POST['cd_empresa']);
  $obj->SETnm_login(trim($_POST['nm_login']));
  $obj->SETnm_senha(trim($_POST['nm_senha']));
  $obj->SETnm_nome(trim($_POST['nm_nome']));
  $obj->SETnm_email(trim($_POST['nm_email']));
  $obj->SETcd_superior(0+$_POST['cd_superior']);
  $lista = pegaPostUsuarioEmpresa($cd_usuario);
  $obj->SETlista_empresas($lista);
  return $obj;
}

function pegaPostUsuarioEmpresa($cd_usuario) {
  if(!($cd_usuario>0)) { $cd_usuario = -1; }
  $total = 0+$_POST['tot_emp'];
  $x = 0;
  for($x=0;$x<$total;$x++) {
    $aux = "cd_empresa".$x;
    $cd_empresa = 0+$_POST[$aux];
    $obj = new objUsuarioEmpresa();
    if($cd_empresa>0) {
      $obj->SETcd_usuario($cd_usuario);
      $obj->SETcd_empresa($cd_empresa);
    } else {
      $obj->SETcd_usuario(0);
      $obj->SETcd_empresa(0);
    }
    $ret[$x] = $obj;
  }
  return $ret;
}

function montaChkEmpresas($lista) {
  $ret = "";
  $total = count($lista);
  $x = 0;
  while($x<$total) {
    $chk="";
    $obj = $lista[$x];
    $cdemp = $obj->GETcd_empresa();
    $nmemp = $obj->GETnm_empresa();
    $cdusu = $obj->GETcd_usuario();
    if($cdusu>0) {
      $chk="checked";
    }
    $ret = $ret."<br><input type=checkbox value='$cdemp' $chk> $nmemp \n";
  }
  return $ret;
}
