<?php

class cprocesso_generico extends cMODELO {

    public $codigo;
    public $cd_candidato;
    public $id_solicita_visto;
    public $id_tipoprocesso;
    public $nu_empresa;
    public $nu_embarcacao_projeto;
    public $codigo_processo_mte;
    public $nu_servico;
    public $dt_cad;
    public $dt_ult;
    public $cd_usuario_cad;
    public $cd_usuario_ult;
    public $ordem;
    public $observacao;
    public $date_1;
    public $date_2;
    public $date_3;
    public $date_4;
    public $date_5;
    public $date_6;
    public $string_1;
    public $string_2;
    public $string_3;
    public $string_4;
    public $string_5;
    public $string_6;
    public $integer_1;
    public $integer_2;
    public $integer_3;
    public $integer_4;
    public $integer_5;
    public $integer_6;

    public function __construct() {
        
    }

    public function campoid() {
        return 'codigo';
    }

    public function setId($pid) {
        $this->codigo = $pid;
    }

    public function getId() {
        return $this->codigo;
    }

    public function sql_Liste() {
        $sql = "select * from processo_generico where 1=1 ";
        return $sql;
    }

    public function get_tipo_processo() {
        throw new Exception("Tipo de processo não definido");
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from processo_generico where codigo = " . $this->codigo;
        return $sql;
    }

    public function Incluir() {
        $sql = "INSERT INTO processo_generico (";
        $sql .= "	 cd_candidato";
        $sql .= "	,nu_empresa";
        $sql .= "	,nu_embarcacao_projeto";
        $sql .= "	,nu_servico";
        $sql .= "	,id_solicita_visto";
        $sql .= "	,id_tipoprocesso";
        $sql .= "	,codigo_processo_mte";
        $sql .= "	,ordem";
        $sql .= "	,observacao";
        $sql .= "	,date_1";
        $sql .= "	,date_2";
        $sql .= "	,date_3";
        $sql .= "	,date_4";
        $sql .= "	,date_5";
        $sql .= "	,date_6";
        $sql .= "	,string_1";
        $sql .= "	,string_2";
        $sql .= "	,string_3";
        $sql .= "	,string_4";
        $sql .= "	,string_5";
        $sql .= "	,string_6";
        $sql .= "	,integer_1";
        $sql .= "	,integer_2";
        $sql .= "	,integer_3";
        $sql .= "	,integer_4";
        $sql .= "	,integer_5";
        $sql .= "	,integer_6";
        $sql .= "	,dt_cad";
        $sql .= "	,cd_usuario_cad";
        $sql .= ") values (";
        $sql .= "	 " . cBANCO::ChaveOk($this->cd_candidato);
        $sql .= "	," . cBANCO::ChaveOk($this->nu_empresa);
        $sql .= "	," . cBANCO::ChaveOk($this->nu_embarcacao_projeto);
        $sql .= "	," . cBANCO::ChaveOk($this->nu_servico);
        $sql .= "	," . cBANCO::ChaveOk($this->id_solicita_visto);
        $sql .= "	," . cBANCO::ChaveOk($this->id_tipoprocesso);
        $sql .= "	," . cBANCO::ChaveOk($this->codigo_processo_mte);
        $sql .= "	," . cBANCO::InteiroOk($this->ordem);
        $sql .= "	," . cBANCO::StringOk($this->observacao);
        $sql .= "	," . cBANCO::DataOk($this->date_1);
        $sql .= "	," . cBANCO::DataOk($this->date_2);
        $sql .= "	," . cBANCO::DataOk($this->date_3);
        $sql .= "	," . cBANCO::DataOk($this->date_4);
        $sql .= "	," . cBANCO::DataOk($this->date_5);
        $sql .= "	," . cBANCO::DataOk($this->date_6);
        $sql .= "	," . cBANCO::StringOk($this->string_1);
        $sql .= "	," . cBANCO::StringOk($this->string_2);
        $sql .= "	," . cBANCO::StringOk($this->string_3);
        $sql .= "	," . cBANCO::StringOk($this->string_4);
        $sql .= "	," . cBANCO::StringOk($this->string_5);
        $sql .= "	," . cBANCO::StringOk($this->string_6);
        $sql .= "	," . cBANCO::InteiroOk($this->integer_1);
        $sql .= "	," . cBANCO::InteiroOk($this->integer_2);
        $sql .= "	," . cBANCO::InteiroOk($this->integer_3);
        $sql .= "	," . cBANCO::InteiroOk($this->integer_4);
        $sql .= "	," . cBANCO::InteiroOk($this->integer_5);
        $sql .= "	," . cBANCO::InteiroOk($this->integer_6);
        $sql .= "	, now()";
        $sql .= "	," . cBANCO::InteiroOk($this->cd_usuario_cad);
        $sql .= ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->setid(cAMBIENTE::$db_pdo->lastInsertId());
    }

    public function Atualizar() {
        $sql = "update processo_generico set ";
        $sql .= "	 cd_candidato = " . cBANCO::ChaveOk($this->cd_candidato);
        $sql .= "	,nu_empresa = " . cBANCO::ChaveOk($this->nu_empresa);
        $sql .= "	,nu_embarcacao_projeto = " . cBANCO::ChaveOk($this->nu_embarcacao_projeto);
        $sql .= "	,nu_servico = " . cBANCO::ChaveOk($this->nu_servico);
        $sql .= "	,id_solicita_visto = " . cBANCO::ChaveOk($this->id_solicita_visto);
        $sql .= "	,id_tipoprocesso = " . cBANCO::ChaveOk($this->id_tipoprocesso);
        $sql .= "	,codigo_processo_mte = " . cBANCO::ChaveOk($this->codigo_processo_mte);
        $sql .= "	,ordem = " . cBANCO::InteiroOk($this->ordem);
        $sql .= "	,observacao = " . cBANCO::StringOk($this->observacao);
        $sql .= "	,date_1 = " . cBANCO::DataOk($this->date_1);
        $sql .= "	,date_2 = " . cBANCO::DataOk($this->date_2);
        $sql .= "	,date_3 = " . cBANCO::DataOk($this->date_3);
        $sql .= "	,date_4 = " . cBANCO::DataOk($this->date_4);
        $sql .= "	,date_5 = " . cBANCO::DataOk($this->date_5);
        $sql .= "	,date_6 = " . cBANCO::DataOk($this->date_6);
        $sql .= "	,string_1 = " . cBANCO::StringOk($this->string_1);
        $sql .= "	,string_2 = " . cBANCO::StringOk($this->string_2);
        $sql .= "	,string_3 = " . cBANCO::StringOk($this->string_3);
        $sql .= "	,string_4 = " . cBANCO::StringOk($this->string_4);
        $sql .= "	,string_5 = " . cBANCO::StringOk($this->string_5);
        $sql .= "	,string_6 = " . cBANCO::StringOk($this->string_6);
        $sql .= "	,integer_1 = " . cBANCO::InteiroOk($this->integer_1);
        $sql .= "	,integer_2 = " . cBANCO::InteiroOk($this->integer_2);
        $sql .= "	,integer_3 = " . cBANCO::InteiroOk($this->integer_3);
        $sql .= "	,integer_4 = " . cBANCO::InteiroOk($this->integer_4);
        $sql .= "	,integer_5 = " . cBANCO::InteiroOk($this->integer_5);
        $sql .= "	,integer_6 = " . cBANCO::InteiroOk($this->integer_6);
        $sql .= "	,cd_usuario_ult = " . cSESSAO::$mcd_usuario;
        $sql .= "	,dt_ult =  now()";
        $sql .= " where codigo = " . $this->codigo;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function AtualizeServico() {
        $sql = " update processo_generico";
        $sql .= "   set nu_servico = " . cBANCO::ChaveOk($this->nu_servico);
        $sql .= "     , cd_usuario_ult = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= "     , dt_ult = now()";
        $sql .= " where codigo = " . $this->codigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function SalvarDadosOs($pOS, $pCand) {
        $sql = "select codigo ";
        $sql .= " from processo_generico";
        $sql .= " where id_solicita_visto =" . $pOS->mID_SOLICITA_VISTO;
        $sql .= "   and id_tipoprocesso   =" . $this->get_tipo_processo();
        $sql .= "   and cd_candidato      =" . $pCand->mNU_CANDIDATO;
        $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $rs = mysql_fetch_array($res);
        if (intval($rs['codigo']) > 0) {
            $this->codigo = $rs['codigo'];
            $this->nu_servico = $pOS->mNU_SERVICO;
            $this->AtualizeServico();
        } else {
            $this->inicializa($pOS, $pCand);
            $this->Salvar();
        }
    }

    public function inicializa($os, $cand) {
        $this->codigo = 0;
        $this->id_solicita_visto = $os->mID_SOLICITA_VISTO;
        $this->nu_servico = $os->mNU_SERVICO;
        $this->cd_candidato = $cand->mNU_CANDIDATO;
        $this->nu_empresa = $os->mNU_EMPRESA;
        $this->nu_embarcacao_projeto = $os->mNU_EMBARCACAO_PROJETO;
        if (intval($cand->mcodigo_processo_mte_atual) == 0) {
            $cand->CriaVistoAtualSeNaoHouver($os);
        }
        $this->codigo_processo_mte = $cand->mcodigo_processo_mte_atual;
        $this->cd_usuario_cad = cSESSAO::$mcd_usuario;
    }

    public function RecupereSePelaOsSemCand($pID_SOLICITA_VISTO) {
        $this->id_solicita_visto = $pID_SOLICITA_VISTO;

        if (!intval($this->id_solicita_visto) > 0) {
            throw new Exception("Código da OS não informado");
        }
        $sql = "select ps.*"
                . " from processo_generico  ps";
        $sql.= " where ps.id_solicita_visto = " . $this->id_solicita_visto;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            if ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
                $this->nu_servico = $rs["nu_servico"];
                $this->codigo = $rs["codigo"];
                $this->codigo_processo_mte = $rs["codigo_processo_mte"];
                cBANCO::CarreguePropriedades($rs, $this, "");
            }
        }
    }

    public function RecupereSePelaOs($pID_SOLICITA_VISTO, $pnu_candidato) {
        $this->id_solicita_visto = $pID_SOLICITA_VISTO;

        if (!intval($this->id_solicita_visto) > 0) {
            throw new Exception("Código da OS não informado");
        }
        $sql = "select ps.*"
                . " from processo_generico  ps";
        $sql.= " where ps.id_solicita_visto = " . $this->id_solicita_visto . ""
                . " and ps.cd_candidato = " . $pnu_candidato;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            if ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
                $this->nu_servico = $rs["nu_servico"];
                $this->codigo = $rs["codigo"];
                $this->codigo_processo_mte = $rs["codigo_processo_mte"];
                cBANCO::CarreguePropriedades($rs, $this, "");
            }
        }
    }

    public function atualizaOrdem($ordem){
        $sql = "update processo_generico";
        $sql.= "   set ordem = " . $ordem;
        $sql.= " where codigo = " . $this->codigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

}
