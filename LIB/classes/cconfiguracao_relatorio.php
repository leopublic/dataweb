<?php

class cconfiguracao_relatorio extends cMODELO{
    public $mcfgr_id;
    public $mcfgr_nome;

    public function get_nomeTabela() {
        return 'configuracao_relatorio';
    }

    public function getId() {
        return $this->mcfgr_id;
    }

    public function setId($pValor) {
        $this->mcfgr_id = $pValor;
    }

    public function campoid() {
        return 'cfgr_id';
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from configuracao_relatorio where cfgr_id = ".$this->mcfgr_id;
        return $sql;
    }

    public function Incluir()
    {
        $sql= "insert into configuracao_relatorio(
                 cfgr_nome
               ) values (
                 ".cBANCO::StringOk($this->mcfgr_nome)."
                )";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mcfgr_id = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function Atualizar()
    {
        $sql= "update configuracao_relatorio set
                 cfgr_nome = ".cBANCO::StringOk($this->mcfgr_nome)."
              where cfgr_id=".$this->mcfgr_id;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_Liste()
    {
        $sql = "select * from configuracao_relatorio where 1=1";
        return $sql;
    }


    public static function comboMustache($cfgr_id = '', $default = "(selecione...)") {
        $sql = "select cfgr_id valor, cfgr_nome descricao, if(cfgr_Id='".$cfgr_id."' ,1 ,0 ) selected from configuracao_relatorio order by cfgr_nome";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));

        if (intval($cfgr_id) == 0){
            $rs['0']['selected'] = true;
        }

        return  $rs ;
    }

    public function colunas(){
        $sql = "select cfgc_nome_coluna from configuracao_relatorio_coluna where cfgr_id = ".$this->mcfgr_id." order by cfgc_ordem";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_COLUMN, 0);
        return $rs;
    }
}
