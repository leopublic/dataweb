<?php
class cpreco extends cMODELO{
	public $mprec_id;
	public $mNU_SERVICO;
	public $mtbpc_id;
	public $mprec_vl_conceito;
	public $mprec_vl_pacote;
	public $mprec_tx_item;
	public $mprec_tx_descricao_tabela_precos;
	public $mprec_tx_descricao_tabela_precos_em_ingles;
	public $mNO_SERVICO_RESUMIDO;

	public static function get_nomeCampoChave() {
		return 'prec_id';
	}
	public static function get_nomeTabelaBD() {
		return 'preco';
	}


	public function get_nomeTabela() {
		return self::get_nomeTabelaBD();
	}

	public function campoid(){
		return self::get_nomeCampoChave();
	}

	public function setid($pid){
		$this->mprec_id = $pid;
	}

	public function getid(){
		return $this->mprec_id;
	}

	public function sql_Liste(){
		$sql  = "    select prec_id";
		$sql .= "         , tbpc_id";
		$sql .= "         , S.NU_SERVICO";
		$sql .= "         , S.NO_SERVICO_RESUMIDO";
		$sql .= "         , S.serv_fl_ativo";
		$sql .= "         , S.serv_fl_pacote";
		$sql .= "         , prec_vl_conceito";
		$sql .= "         , prec_vl_pacote";
		$sql .= "         , prec_tx_item";
		$sql .= "         , prec_tx_descricao_tabela_precos";
		$sql .= "         , prec_tx_descricao_tabela_precos_em_ingles";
		$sql .= "      from SERVICO S";
		$sql .= " left join preco p on p.NU_SERVICO = S.NU_SERVICO where 1=1";
		return $sql;
	}

	public function sql_RecuperePeloId(){
		$sql  = "    select prec_id";
		$sql .= "         , S.NU_SERVICO";
		$sql .= "         , S.NO_SERVICO_RESUMIDO";
		$sql .= "         , prec_vl_conceito";
		$sql .= "         , prec_vl_pacote ";
		$sql .= "         , prec_tx_item";
		$sql .= "         , prec_tx_descricao_tabela_precos";
		$sql .= "         , prec_tx_descricao_tabela_precos_em_ingles";
		$sql .= "      from SERVICO S";
		$sql .= " left join preco p on p.NU_SERVICO = S.NU_SERVICO and p.tbpc_id = ".$this->mtbpc_id;
		$sql .= " where S.NU_SERVICO = ".$this->mNU_SERVICO;
		return $sql;
	}

	public function RecuperePeloId($pid = ''){
		if ($pid != '' ){
			$this->setid($pid);
		}
		$sql = $this->sql_RecuperePeloId();
		if($res = cAMBIENTE::$db_pdo->query($sql)){
			$rs = $res->fetch(PDO::FETCH_BOTH);
			cBANCO::CarreguePropriedades($rs, $this);
		}
		else{
			throw new Exception("N&atilde;o foi poss&iacute;vel obter o registro", $sql);
		}
	}

	public function RecuperePelaTabelaServico($ptbpc_id, $pnu_servico){
		if ($ptbpc_id == '' ){
			throw new Exception("Código da tabela de preços não informado");
		}
		if ($pnu_servico == '' ){
			throw new Exception("Código do serviço não informado");
		}

		$sql = "select * from preco
				where tbpc_id=".$ptbpc_id."
				and nu_servico=".$pnu_servico;
		print $sql;
		if($res = cAMBIENTE::$db_pdo->query($sql)){
			$rs = $res->fetch(PDO::FETCH_BOTH);
			cBANCO::CarreguePropriedades($rs, $this);
		}
		else{
			throw new cERRO_SQL("N&atilde;o foi poss&iacute;vel obter o registro", __CLASS__.'.'.__FUNCTION__, $sql);
		}
	}

	public function RecuperePelaEmpresaServico($pnu_empresa, $pnu_servico){
		if ($pnu_empresa == '' ){
			throw new Exception("Código da empresa não informado");
		}
		if ($pnu_servico != '' ){
			$this->mNU_SERVICO = $pnu_servico;
		}
		else{
			throw new Exception("Código do serviço não informado");
		}

		$sql = "select * from preco , empresa
				where empresa.nu_empresa =".$pnu_empresa."
				and preco.tbpc_id = empresa.tbpc_id
				and preco.nu_servico=".$pnu_servico;
		if($res = cAMBIENTE::$db_pdo->query($sql)){
			$rs = $res->fetch(PDO::FETCH_BOTH);
			cBANCO::CarreguePropriedades($rs, $this);
		}
		else{
			throw new cERRO_SQL("N&atilde;o foi poss&iacute;vel obter o registro", __CLASS__.'.'.__FUNCTION__, $sql);
		}
	}

	public function Incluir(){
		$sql = "INSERT INTO preco (";
		$sql .= "	 NU_SERVICO";
		$sql .= "	,tbpc_id";
		$sql .= "	,prec_vl_conceito";
		$sql .= "	,prec_vl_pacote";
		$sql .= "   ,prec_tx_item";
		$sql .= "	,prec_tx_descricao_tabela_precos";
		$sql .= "	,prec_tx_descricao_tabela_precos_em_ingles";
		$sql .= ") values (";
		$sql .= "	 ".cBANCO::InteiroOk($this->mNU_SERVICO);
		$sql .= "	,".cBANCO::InteiroOk($this->mtbpc_id);
		$sql .= "	,".cBANCO::ValorOk($this->mprec_vl_conceito);
		$sql .= "	,".cBANCO::ValorOk($this->mprec_vl_pacote);
		$sql .= "	,".cBANCO::StringOk($this->mprec_tx_item);
		$sql .= "	,".cBANCO::StringOk($this->mprec_tx_descricao_tabela_precos);
		$sql .= "	,".cBANCO::StringOk($this->mprec_tx_descricao_tabela_precos_em_ingles);
		$sql .= ")";
		cAMBIENTE::$db_pdo->exec($sql);
		$this->setid(cAMBIENTE::$db_pdo->lastInsertId());
	}

	public function Atualizar(){
		$sql = "update preco set ";
		$sql .= "   NU_SERVICO		  = ".cBANCO::InteiroOk($this->mNU_SERVICO);
		$sql .= "  ,tbpc_id			  = ".cBANCO::InteiroOk($this->mtbpc_id);
		$sql .= "  ,prec_vl_conceito  = ".cBANCO::ValorOk($this->mprec_vl_conceito);
		$sql .= "  ,prec_vl_pacote	  = ".cBANCO::ValorOk($this->mprec_vl_pacote);
		$sql .= "  ,prec_tx_item     = ".cBANCO::StringOk($this->mprec_tx_item);
		$sql .= "  ,prec_tx_descricao_tabela_precos = ".cBANCO::StringOk($this->mprec_tx_descricao_tabela_precos);
		$sql .= "  ,prec_tx_descricao_tabela_precos_em_ingles = ".cBANCO::StringOk($this->mprec_tx_descricao_tabela_precos_em_ingles);
		$sql .= " where prec_id = ".$this->mprec_id;
		cAMBIENTE::$db_pdo->exec($sql);
	}

	public static function ClonarParaOutraTabela($ptbpc_id_orig, $ptbpc_id_dest){
		$sql = "INSERT INTO preco (";
		$sql .= "	 NU_SERVICO";
		$sql .= "	,tbpc_id";
		$sql .= "	,prec_vl_conceito";
		$sql .= "	,prec_vl_pacote";
		$sql .= "   ,prec_tx_item";
		$sql .= "	,prec_tx_descricao_tabela_precos";
		$sql .= "	,prec_tx_descricao_tabela_precos_em_ingles";
		$sql .= ") select";
		$sql .= "	 NU_SERVICO";
		$sql .= "	," . $ptbpc_id_dest;
		$sql .= "	,prec_vl_conceito";
		$sql .= "	,prec_vl_pacote";
		$sql .= "   ,prec_tx_item";
		$sql .= "	,prec_tx_descricao_tabela_precos";
		$sql .= "	,prec_tx_descricao_tabela_precos_em_ingles";
		$sql .= "  from preco where tbpc_id = " . $ptbpc_id_orig ;
		cAMBIENTE::$db_pdo->exec($sql);
	}

	public static function ExcluaPrecosDeTabela($ptbpc_id){
		$sql = "delete from preco where tbpc_id = ".$ptbpc_id;
		cAMBIENTE::$db_pdo->exec($sql);
	}

	public function ExcluirPeloId()
	{
		$sql = "delete from preco where prec_id = ".$this->mprec_id;
		cAMBIENTE::ExecuteQuery($sql);
	}
}
