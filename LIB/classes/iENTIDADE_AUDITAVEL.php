<?php
/**
 * Define os métodos necessários para logar todas as alterações feitas
 * @author leonardo
 */
interface iENTIDADE_AUDITAVEL {
	public function getCampoUsuarioAlteracao();
	public function SqlLog($pClass, $pFunction);
}
