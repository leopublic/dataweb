<?php
// á
class cESCOLARIDADE extends cMODELO{
	protected $mCO_ESCOLARIDADE;
	protected $mNO_ESCOLARIDADE;
	protected $mNO_ESCOLARIDADE_EM_INGLES;

    public static function get_nomeCampoChave(){
        return 'co_escolaridade';
    }
    public static function get_nomeCampoDescricao(){
        return "no_escolaridade";
    }
    public static function get_nomeTabelaBD(){
        return 'escolaridade';
    }

	
	public function get_nomeTabela() {
        return 'escolaridade';
    }

    public function getId() {
        return $this->mCO_ESCOLARIDADE;
    }

    public function setId($pValor) {
        $this->mCO_ESCOLARIDADE = $pValor;
    }

    public function campoid() {
        return 'CO_ESCOLARIDADE';
    }

    public function sql_RecuperePeloId() {
		$sql = "select * from ESCOLARIDADE where CO_ESCOLARIDADE = ".$this->mCO_ESCOLARIDADE;
		return $sql;
	}
	
	public function Incluir()
	{
		$sql= "insert into ESCOLARIDADE(
				 NO_ESCOLARIDADE 
				,NO_ESCOLARIDADE_EM_INGLES 
		       ) values (
				 ".cBANCO::StringOk($this->mNO_ESCOLARIDADE)."
				,".cBANCO::StringOk($this->mNO_ESCOLARIDADE_EM_INGLES)."
				)";
		cAMBIENTE::ExecuteQuery($sql);
		$this->mCO_ESCOLARIDADE = cAMBIENTE::$db_pdo->lastInsertId();
	}
	
	public function Atualizar()
	{
		$sql= "update ESCOLARIDADE set 
				 NO_ESCOLARIDADE = ".cBANCO::StringOk($this->mNO_ESCOLARIDADE)."
				,NO_ESCOLARIDADE_EM_INGLES = ".cBANCO::StringOk($this->mNO_ESCOLARIDADE_EM_INGLES)."
			  where CO_ESCOLARIDADE=".$this->mCO_ESCOLARIDADE;
		cAMBIENTE::ExecuteQuery($sql);
	}
	
	public function sql_Liste()
	{
		$sql = "select * from ESCOLARIDADE where 1=1";
		return $sql;
	}
}
