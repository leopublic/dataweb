<?php
class cARQUIVOS_FORMULARIOS{
	public static $mCD_ARQUIVO;
	
	public static function CaminhoReal($pNU_CANDIDATO, $pNO_ARQUIVO, $pNU_EMPRESA, $pNU_SOLICITACAO){
		$src = cAMBIENTE::getDirForm().DIRECTORY_SEPARATOR.$pNU_EMPRESA.DIRECTORY_SEPARATOR.$pNO_ARQUIVO;
		return $src;
	}

	public static function UrlReal($pNU_CANDIDATO, $pNO_ARQUIVO, $pNU_EMPRESA, $pNU_SOLICITACAO){
		$src = cAMBIENTE::getUrlForm().'/'.$pNU_EMPRESA.'/'.$pNO_ARQUIVO;
		return $src;
	}

	public static function Extensao($pNomeArquivo)
	{
		$ax1 = str_replace(".","#",$pNomeArquivo);
		$aux = preg_split("[#]",$ax1);
		$x = count($aux);
		if($x>1) {
			$extensao = $aux[$x-1];
		} else {
			$extensao = "txt";
		}
		return $extensao;
	}

	public static function CriarFormularioSolicitacao($pInputFile, $pNU_EMPRESA, $pID_SOLICITA_VISTO, $pID_TIPO_ARQUIVO, $pUsuario, $pNU_CANDIDATO )
	{
		$updir = cAMBIENTE::getDirForm(). DIRECTORY_SEPARATOR .$pNU_EMPRESA;
		if(!is_dir($updir)) {
			mkdir ($updir, 0770);
		}
		return self::CriarForm($pInputFile, $updir, $pNU_EMPRESA, 'S', $pID_SOLICITA_VISTO, $pID_TIPO_ARQUIVO, $pUsuario, $pNU_CANDIDATO );
	}

	public static function CriarArquivoSolicitacao($pInputFile, $pNU_EMPRESA, $pID_SOLICITA_VISTO, $pID_TIPO_ARQUIVO, $pUsuario, $pNU_CANDIDATO = ""  )
	{
		global $mysoldir;
		if(!is_dir($mysoldir)) {
			mkdir ($mysoldir, 0770);
		}
		$updir = $mysoldir."/".$pNU_EMPRESA;
		if(!is_dir($updir)) {
			mkdir ($updir, 0770);
		}
		return self::CriarArquivo($pInputFile, $updir, $pNU_EMPRESA, 'S', $pID_SOLICITA_VISTO, $pID_TIPO_ARQUIVO, $pUsuario, $pNU_CANDIDATO );
	}
	/**
	 * Cria um arquivo para um usuario específico
	 */
	public static function CriarArquivo($pInputFile, $pDiretorioDestino, $pNU_EMPRESA, $pEntidade, $pIdEntidade, $pTipoArquivo, $pUsuario, $pNU_CANDIDATO = "" ){
		// InputFile= $_FILES['campo']
		// NU_EMPRESA = id da empresa
		// Entidade = 'C' para candidato, 'S' para solicitacao
		// IdEntidade = id do candidato ou da solicitacao
		$ret = "";
		$noArqOriginal = $pInputFile['name'];
		$noArqOriginal = str_replace("'","",$noArqOriginal);
		$extensao = self::Extensao($noArqOriginal);
		
		// Insere arquivo no banco
		if ($pEntidade=='C'){
			$NU_CANDIDATO = $pIdEntidade;
			$ID_SOLICITA_VISTO= 'null';
		}
		else{
			$NU_CANDIDATO = $pNU_CANDIDATO;
			$ID_SOLICITA_VISTO= $pIdEntidade;
		}

//		$sql = "insert into ARQUIVOS_FORMULARIOS (
//					 NU_EMPRESA
//					,id_solicita_visto
//					,NU_CANDIDATO
//					,NM_FORMULARIO
//					,CD_ADMIN
//					,DT_INCLUSAO
//					,ID_TIPO_FORMULARIO
//				) values ($idEmpresa,$id_solicita_visto, $pNU_CANDIDATO, '$noArquivo',$usulogado,now(),$ID_TIPO_FORMULARIO)";
		$sql = "insert into ARQUIVOS (NU_EMPRESA,NU_CANDIDATO,ID_SOLICITA_VISTO, ";
		$sql .= "NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NU_ADMIN, NO_ARQUIVO) ";
		$sql .= " values (";
		$sql .= "  ".cBANCO::ChaveOk($pNU_EMPRESA);
		$sql .= " ,".cBANCO::ChaveOk($NU_CANDIDATO);
		$sql .= " ,".cBANCO::ChaveOk($ID_SOLICITA_VISTO);
		$sql .= " ,".cBANCO::StringOk($noArqOriginal);
		$sql .= " ,".cBANCO::ChaveOk($pTipoArquivo);
		$sql .= " , now()";
		$sql .= " ,".cBANCO::ChaveOk($pUsuario);
		$sql .= " , '' ";
		$sql .= ")";
		cAMBIENTE::ExecuteQuery($sql);
		if(mysql_errno()>0) {
			$ret = 'Não foi possível adicionar o arquivo '.$noArqOriginal."\n".mysql_error()."\nsql=$sql ";
		}
		else{
			self::$mNU_SEQUENCIAL = cAMBIENTE::$db_pdo->lastInsertId();
			if ($pEntidade=='C'){
				$noArquivo = "E".$pEntidade.$pIdEntidade."SQ".self::$mNU_SEQUENCIAL.".".$extensao;
			}
			else{
			    $aux = "000000000".$pIdEntidade;
			    $noArquivo = "SOLIC".substr($aux,strlen($aux-8))."_".self::$mNU_SEQUENCIAL.".".$extensao;
			}
			$sql = "update ARQUIVOS set no_arquivo = '".$noArquivo."' where NU_SEQUENCIAL = ".self::$mNU_SEQUENCIAL;
			cAMBIENTE::ExecuteQuery($sql);
			
			$uploadfile = $pDiretorioDestino."/".$noArquivo;
			// Copia arquivo para o destino	
			try{
				move_uploaded_file($pInputFile['tmp_name'], $uploadfile);
				$ret = "Arquivo adicionado com sucesso";
			}
			catch(Exception $e){
				$ret = 'Não foi possível mover o arquivo. Erro:'.$e->getMessage();
			}
		}
		return $ret;		
	}

	public static function DiretorioUploadOk($pNU_EMPRESA, $pNU_CANDIDATO){
		$mydocdir = cCONFIGURACAO::$configuracoes[AMBIENTE]['raiz'].DIRECTORY_SEPARATOR.'arquivos'.DIRECTORY_SEPARATOR.'documentos';
		if(!is_dir($mydocdir)) {
			mkdir ($mydocdir, 0770);
		}
		$updir = $mydocdir.DIRECTORY_SEPARATOR.$pNU_EMPRESA;
		if(!is_dir($updir)) {
			mkdir ($updir, 0770);
		}
		$updir = $updir.DIRECTORY_SEPARATOR.$pNU_CANDIDATO;

		if(!is_dir($updir)) {
			mkdir ($updir, 0770);
		}
		return $updir;

	}

	public static function JSON_ArquivosDoCandidato($pNU_CANDIDATO){
		$sql = "select a.*
					 , date_format(DT_INCLUSAO, '%d/%m/%Y') as DT_INCLUSAO_FMT 
					 , NO_TIPO_ARQUIVO
				  from arquivos a
				  left join tipo_arquivo  ta on ta.id_tipo_arquivo = a.tp_arquivo 
				 where nu_candidato = ".$pNU_CANDIDATO." order by DT_INCLUSAO desc";
		$res = cAMBIENTE::ConectaQuery($sql);
		$arquivos = array();
		while($rs = $res->fetch(PDO::FETCH_ASSOC)){
			$arquivos[] = array(
					"NU_SEQUENCIAL" => $rs['NU_SEQUENCIAL']
					,"NO_TIPO_ARQUIVO" => $rs['NO_TIPO_ARQUIVO']
					,"DT_INCLUSAO_FMT" => $rs['DT_INCLUSAO_FMT']
					,"NO_ARQ_ORIGINAL" => $rs['NO_ARQ_ORIGINAL']
					,"url"			   => self::UrlReal($rs['NU_CANDIDATO'], $rs['NO_ARQUIVO'], $rs['NU_EMPRESA'], $rs['ID_SOLICITA_VISTO'])
				);
		}		
		return $arquivos;
	}

	public static function AdicionaFormulario($idEmpresa, $id_solicita_visto, $pNU_CANDIDATO, $noArquivo, $ID_TIPO_FORMULARIO) {
		$ret = "";
		// Deleta versões anteriores do mesmo form para a mesma OS
		$sql = " select CD_ARQUIVO, NM_FORMULARIO ";
		$sql .= "  from ARQUIVOS_FORMULARIOS ";
		$sql .= " where id_solicita_visto	= " . $id_solicita_visto;
		$sql .= "   and NU_CANDIDATO		= " . $pNU_CANDIDATO;
		$sql .= "   and ID_TIPO_FORMULARIO	= " . $ID_TIPO_FORMULARIO;

		$res = cAMBIENTE::ConectaQuery($sql, __FILE__ . '->' . __CLASS__ . '->' . __FUNCTION__);
		if ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
			if($rs['CD_ARQUIVO']!= ''){
				$arq = cAMBIENTE::getDirForm(). DIRECTORY_SEPARATOR . 'OS'.$id_solicita_visto . DIRECTORY_SEPARATOR . $pNU_CANDIDATO . DIRECTORY_SEPARATOR . $rs['NM_FORMULARIO'];
				if (file_exists($arq)) {
					unlink($arq);
				}
				$sql = " delete from ARQUIVOS_FORMULARIOS ";
				$sql .= " where CD_ARQUIVO =" . $rs['CD_ARQUIVO'];
				cAMBIENTE::ExecuteQuery($sql, __FILE__ . '->' . __CLASS__ . '->' . __FUNCTION__);			
			}
		}
		
		$sql = "insert into ARQUIVOS_FORMULARIOS (
				     NU_EMPRESA
				   , id_solicita_visto
				   , NU_CANDIDATO
				   , NM_FORMULARIO
				   , CD_ADMIN
				   , DT_INCLUSAO
				   , ID_TIPO_FORMULARIO
				   ) ";
		$sql .= " values (
				" . $idEmpresa . "
			   ," . $id_solicita_visto . "
			   ," . $pNU_CANDIDATO . "
			   ,'" . $noArquivo . "'
			   ," . cSESSAO::$mcd_usuario . "
			   ,  now() 
			   ," . $ID_TIPO_FORMULARIO . "
			    )";
//		print $sql ;
		cAMBIENTE::ExecuteQuery($sql, __FILE__ . '->' . __CLASS__ . '->' . __FUNCTION__);
		return true;
	}
	
}
