<?php
class cCURSO{
	public $mcurs_id;
	public $mtpen_id;
	public $mNU_CANDIDATO;
	public $mcurs_no_cidade;
	public $mcurs_no_periodo;
	public $mcurs_no_instituicao;
	public $mcurs_no_grau;
	public $mcurs_no_nome;

	public function Salvar(){
		$sql = "insert into curso (";
		$sql .= "   tpen_id";
		$sql .= " , NU_CANDIDATO";
		$sql .= " , curs_no_cidade";
		$sql .= " , curs_no_periodo";
		$sql .= " , curs_no_instituicao";
		$sql .= " , curs_no_grau";
		$sql .= " , curs_no_nome";
		$sql .= ") values (";
		$sql .= " ".cBANCO::ChaveOk($this->mtpen_id);
		$sql .= " ,".cBANCO::ChaveOk($this->mNU_CANDIDATO);
		$sql .= " ,".cBANCO::StringOk($this->mcurs_no_cidade);
		$sql .= " ,".cBANCO::StringOk($this->mcurs_no_periodo);
		$sql .= " ,".cBANCO::StringOk($this->mcurs_no_instituicao);
		$sql .= " ,".cBANCO::StringOk($this->mcurs_no_grau);
		$sql .= " ,".cBANCO::StringOk($this->mcurs_no_nome);
		$sql .= ")";
		executeQuery($sql, __CLASS__.'.'.__FUNCTION__);
	}

	public function Excluir(){
		$sql = "delete from curso where curs_id = ".$this->mcurs_id;
		executeQuery($sql, __CLASS__.'.'.__FUNCTION__);
	}
}
