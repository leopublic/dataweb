<?php

class proc_emiscie {

  public $codigo = "";
  public $cd_candidato = "";
  public $cd_solicitacao = "";
  public $nu_cie = "";
  public $dt_emissao = "";
  public $dt_validade = "";
  public $observacao = "";
  public $dt_cad = "";
  public $dt_ult = "";
  public $myerr = "";

  public function proc_regcie() {  }

  public function BuscaPorCodigo($codigo) {
    $extra = "WHERE codigo=$codigo";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function BuscaPorCandidato($candidato,$solicitacao) {
    $this->cd_candidato = $candidato;
    $this->cd_solicitacao = $solicitacao;
    $extra = "WHERE cd_candidato=$candidato AND cd_solicitacao=$solicitacao";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function ExisteCandidato($candidato,$solicitacao) {
    $extra = "WHERE cd_candidato=$candidato AND cd_solicitacao=$solicitacao";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if(mysql_num_rows($rs)>0) {
        return true;
      } else {
        return false;
      }
    }
  }

  public function BuscaPorProcesso($processo) {
    $extra = "WHERE nu_cie=$processo";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function ListaProcessos($extra) {
    $this->myerr = "";
    $sql = "SELECT * FROM processo_emiscie $extra";
    $rs = mysql_query($sql);
    if(mysql_errno() != 0) {
      $this->myerr = mysql_error();
    }
    return $rs;
  }

  function InsereProcesso() {
    if($this->Verifica() == true) {
      $cd_cand = $this->cd_candidato;
      $cd_sol = $this->cd_solicitacao;
      $dt_validade = $this->dt_validade;
      $dt_emissao = $this->dt_emissao;
      $sql1 = "INSERT INTO processo_emiscie (cd_candidato,cd_solicitacao,nu_cie,dt_validade,dt_emissao,observacao,dt_cad) VALUES ";
      $sql = sprintf($sql1.$sql2."($cd_cand,$cd_sol,'%s',$dt_validade,$dt_emissao,'%s',now())",
              mysql_real_escape_string($this->nu_cie),
              mysql_real_escape_string($this->observacao)
             );
      mysql_query($sql);
      if(mysql_errno() != 0) {
        $erro = mysql_error();
        $this->myerr = "Ocorreu um erro ao inserir emissao CIE.<!-- $erro - sql=$sql -->";
        gravaLogErro($sql,$erro,"CADASTRAR","EMISSAO_CIE");
      } else {
        gravaLog($sql,"","CADASTRAR","EMISSAO_CIE");
      }
    }
  }

  function AlteraProcesso() {
    if($this->Verifica() == true) {
      $cd_cand = $this->cd_candidato;
      $cd_sol = $this->cd_solicitacao;
      $dt_validade = $this->dt_validade;
      $dt_emissao = $this->dt_emissao;
      $sql1 = "UPDATE processo_emiscie SET nu_cie='%s',dt_validade=$dt_validade,dt_emissao=$dt_emissao,observacao='%s' ";
      $sql2 = " WHERE cd_candidato=$cd_cand AND cd_solicitacao=$cd_sol ";
      $sql = sprintf($sql1.$sql2,
              mysql_real_escape_string($this->nu_cie),
              mysql_real_escape_string($this->observacao)
             );
      mysql_query($sql);
      if(mysql_errno() != 0) {
        $erro = mysql_error();
        $this->myerr = "Ocorreu um erro ao alterar emissao CIE.<!-- $erro - sql=$sql -->";
        gravaLogErro($sql,$erro,"ALTERAR","EMISSAO_CIE");
      } else {
        gravaLog($sql,"","ALTERAR","EMISSAO_CIE");
      }
    }
  }

  function AlteraVisaDetail() {
    $cd_cand = $this->cd_candidato;
    $cd_sol = $this->cd_solicitacao;
    $dt_validade = $this->dt_validade;
    $dt_emissao = $this->dt_emissao;
    $sql1 = "UPDATE AUTORIZACAO_CANDIDATO SET NU_EMISSAO_CIE='%s',DT_VALIDADE_CIE=$dt_validade,";
    $sql2 = "DT_EMISSAO_CIE=$dt_emissao where NU_CANDIDATO=$cd_cand and NU_SOLICITACAO=$cd_sol ";
    $sql = sprintf($sql1.$sql2, mysql_real_escape_string($this->nu_cie));
    mysql_query($sql);
    if(mysql_errno() != 0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao alterar emissao CIE.<!-- $erro - sql=$sql -->";
      gravaLogErro($sql,$erro,"ALTERAR","EMISSAO_CIE_VISA");
    } else {
      gravaLog($sql,"","ALTERAR","EMISSAO_CIE_VISA");
    }
  }

  function RemoveProcesso($codigo,$cand,$sol) {
    $sql = "DELETE FROM processo_emiscie WHERE ";
    if(strlen($codigo)>0) {  $sql = "codigo=$codigo";  } else
    if( (strlen($cand)>0) && (strlen($sol)>0) ) {  $sql = "cd_candidato=$cand AND cd_solicitacao=$sol";  }
    mysql_query($sql);
    if(mysql_errno() != 0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao remover emissao CIE.<!-- $erro - sql=$sql -->";
      gravaLogErro($sql,$erro,"REMOVER","EMISSAO_CIE");
    } else {
      gravaLog($sql,"","REMOVER","EMISSAO_CIE");
    }
  }

  function Verifica() {
    $ret = true;
    $erro = "";
    if(strlen($this->cd_candidato) == 0) {
      $ret = false;
      $erro = $erro."Nao foi identificado o candidato. ";
    }
    if(strlen($this->cd_solicitacao) == 0) {
      $ret = false;
      $erro = $erro."Nao foi identificada a solicitacao. ";
    }
    if(strlen($this->dt_validade)>0) { $this->dt_validade = "'".dataBR2My($this->dt_validade)."'"; } else { $this->dt_validade = "NULL"; }
    if(strlen($this->dt_emissao)>0) { $this->dt_emissao = "'".dataBR2My($this->dt_emissao)."'"; } else { $this->dt_emissao = "NULL"; }
    if($this->observacao=="NULL") { $this->observacao=""; }
    $this->myerr = $erro;
    return $ret;
  }

  private function LeituraRW($rw) {
    $this->codigo = $rw['codigo'];
    $this->cd_candidato = $rw['cd_candidato'];
    $this->cd_solicitacao = $rw['cd_solicitacao'];
    $this->nu_cie = $rw['nu_cie'];
    $this->dt_validade = dataMy2BR($rw['dt_validade']);
    $this->dt_emissao = dataMy2BR($rw['dt_emissao']);
    $this->observacao = $rw['observacao'];
    if($this->observacao=="NULL") { $this->observacao=""; }
    $this->dt_cad = $rw['dt_cad'];
    $this->dt_ult = $rw['dt_ult'];
  }

  private function LimpaObjeto() {
    $this->codigo = "";
    $this->cd_candidato = "";
    $this->cd_solicitacao = "";
    $this->nu_cie = "";
    $this->dt_validade = "";
    $this->dt_emissao = "";
    $this->observacao = "";
    $this->dt_cad = "";
    $this->dt_ult = "";
    $this->myerr = "";
  }

  function insereEvento($idEmpresa,$tipo) {
    $informou = "";
    $cand = $this->cd_candidato;
    $sol = $this->cd_solicitacao;
    $nu_cie = str_replace("'","",$this->nu_cie);
    $dt_emissao = dataMy2BR(str_replace("'","",$this->dt_emissao));
    $dt_validade = dataMy2BR(str_replace("'","",$this->dt_validade));
    $informou = $informou." n&uacute;mero CIE ($nu_cie) / ";
    $informou = $informou." data de emiss&atilde;o ($dt_emissao) / ";
    $informou = $informou." data de validade ($dt_validade) ";
    $extra="na Emissão CIE. <br>Informou: $informou .";
    $texto = "$tipo do cadastro do candidato $extra";
    insereEvento($idEmpresa,$cand,$sol,$texto);
  }

  public function MontaTextoEmissaoCIE($acao) {
    $ret = "";
    $ret = $ret.montaCampos("cd_candidato",$this->cd_candidato,$this->cd_candidato,"H",$acao,"")."\n";
    $ret = $ret.montaCampos("cd_solicitacao",$this->cd_solicitacao,$this->cd_solicitacao,"H",$acao,"")."\n";
    $ret = $ret."<tr height=20><td class=textoazul bgcolor=#DADADA colspan=5>Emiss&atilde;o CIE</td></tr>\n";
    $ret = $ret."<tr height=20><td><b>N&uacute;mero da CIE:</td>\n";
    $ret = $ret."<td>".montaCampos("NU_PROTOCOLO_CIE",$this->nu_cie,$this->nu_cie,"T",$acao,"maxlength=20 size=30")."</td>\n";
    $ret = $ret."<td>&#160;</td><td>&#160;</td>\n<td>&#160;</td>\n</tr>\n";
    $ret = $ret."<tr height=20>\n<td><b>Data Emiss&atilde;o CIE:</td>\n";
    $ret = $ret."<td>".montaCampos("DT_EMISSAO_CIE",$this->dt_emissao,$this->dt_emissao,"D",$acao,"")."</td>\n";
    $ret = $ret."<td>&#160;</td>\n<td><b>Data de Validade CIE:</td>\n";
    $ret = $ret."<td>".montaCampos("DT_VALIDADE_CIE",$this->dt_validade,$this->dt_validade,"D",$acao,"")."</td>\n";
    $ret = $ret."</tr>\n";
    $ret = $ret."<tr height=20><td><b>Observa&ccedil;&atilde;o:</td>\n";
    $ret = $ret."<td colspan=4>".montaCampos("OBS_MTE",$this->observacao,$this->observacao,"T",$acao,"maxlength=250 size=30")."</td>\n";
    $ret = $ret."</tr>\n";
    return $ret;
  }

  public function LeituraRequest($tipo) {
    $this->cd_candidato = $tipo['cd_candidato'];
    $this->cd_solicitacao = $tipo['cd_solicitacao'];
    $this->nu_cie = $tipo['NU_PROTOCOLO_CIE'];
    $this->dt_validade = $tipo['DT_VALIDADE_CIE'];
    $this->dt_emissao = $tipo['DT_EMISSAO_CIE'];
    $this->observacao = $tipo['OBS_MTE'];
    if($this->observacao=="NULL") { $this->observacao=""; }
  }

}

?>

