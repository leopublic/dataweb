<?php
// TODO: Verificar diferencas em relacao a versao de producao
class cAUTORIZACAO_CANDIDATO
{
	public $mNU_EMPRESA;
	public $mNU_CANDIDATO;
	public $mNU_SOLICITACAO;
	public $mNU_EMBARCACAO_PROJETO;

	public $mCO_TIPO_AUTORIZACAO;
	public $mNU_EMBARCACAO_INICIAL;
	public $mDT_PRAZO_ESTADA_SOLICITADO;
	public $mNU_ARMADOR;
	public $mCD_EMBARCADO;
	public $mCO_REPARTICAO_CONSULAR;
		
	public $mVA_RENUMERACAO_MENSAL;
	public $mNO_MOEDA_REMUNERACAO_MENSAL;
	public $mVA_REMUNERACAO_MENSAL_BRASIL;
	public $mCO_FUNCAO_CANDIDATO;
	public $mTE_DESCRICAO_ATIVIDADES;
	public $mDT_ABERTURA_PROCESSO_MTE;
	public $mNU_PROCESSO_MTE;
	public $mNU_AUTORIZACAO_MTE;
	public $mDT_AUTORIZACAO_MTE;
	public $mDT_PRAZO_AUTORIZACAO_MTE;
	public $mDT_VALIDADE_VISTO;
	public $mNU_VISTO;
	public $mDT_EMISSAO_VISTO;
	public $mNO_LOCAL_EMISSAO_VISTO;
	public $mCO_NACIONALIDADE_VISTO;
	public $mNU_PROTOCOLO_CIE;
	public $mDT_PROTOCOLO_CIE;
	public $mDT_VALIDADE_PROTOCOLO_CIE;
	public $mDT_PRAZO_ESTADA;
	public $mDT_VALIDADE_CIE;
	public $mDT_EMISSAO_CIE;
	public $mNU_PROTOCOLO_PRORROGACAO;
	public $mDT_PROTOCOLO_PRORROGACAO;
	public $mDT_VALIDADE_PROTOCOLO_PROR;
	public $mDT_PRETENDIDA_PRORROGACAO;
	public $mDT_CANCELAMENTO;
	public $mNU_PROCESSO_CANCELAMENTO;
	public $mDT_PROCESSO_CANCELAMENTO;
	public $mTE_OBSERVACOES_PROCESSOS;
	public $mNU_EMISSAO_CIE;
	public $mNO_LOCAL_ENTRADA;
	public $mNO_UF_ENTRADA;
	public $mDT_ENTRADA;
	public $mNU_TRANSPORTE_ENTRADA;
	public $mCO_CLASSIFICACAO_VISTO;
	public $mDS_JUSTIFICATIVA;
	public $mDS_LOCAL_TRABALHO;
	public $mDS_SALARIO_EXTERIOR;
	public $mDS_BENEFICIOS_SALARIO;
	public $mDT_SITUACAO_SOL;
	public $mNU_SERVICO;
	public $mDT_CADASTRAMENTO;
	public $mDT_ULT_ALTERACAO;
	public $mNU_USUARIO_CAD;
	public $mID_AUTORIZACAO_CANDIDATO;
	public $mid_solicita_visto;
	public $mautc_fl_revisado;
	public $mcd_usuario_revisao;
	public $mNO_JUSTIFICATIVA_REP_CONS;
	
	public function Recuperar($pID_SOLICITA_VISTO, $pNU_CANDIDATO)
	{
		$sql = "select * from AUTORIZACAO_CANDIDATO where id_solicita_visto = ".$pID_SOLICITA_VISTO." and NU_CANDIDATO=".$pNU_CANDIDATO;
		$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		$rs = $res->fetch(PDO::FETCH_ASSOC);
		cBANCO::CarreguePropriedades($rs, $this);
		
	}
	
	public function SalvarProcesso_Antigo($post){
		//$this->mid_solicita_visto = 
	}

	public function Revisar($pNU_CANDIDATO, $pid_solicita_visto){
		
	}
	
	public function CursorCompleto($pid_solicita_visto, $pNU_CANDIDATO)
	{
		$sql = "select ac.* 
					 , fc.no_funcao
					 , fc.nu_cbo
					 , rc.no_reparticao_consular
				from autorizacao_candidato ac
				left join funcao_cargo fc on fc.co_funcao = ac.co_funcao_candidato
				left join reparticao_consular rc on rc.co_reparticao_consular = ac.co_reparticao_consular
				where ac.nu_candidato = ".$pNU_CANDIDATO."
				  and ac.id_solicita_visto=".$pid_solicita_visto."
				";
		$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		return $res;
	}
	
	public function AtualizarCadastroOs($pcd_usuario){
		$sql = "update autorizacao_candidato 
				   set CO_REPARTICAO_CONSULAR = ".cBANCO::ChaveOk($this->mCO_REPARTICAO_CONSULAR)."
					 , NO_JUSTIFICATIVA_REP_CONS = ".cBANCO::StringOk($this->mNO_JUSTIFICATIVA_REP_CONS)."
					 , CO_FUNCAO_CANDIDATO = ".cBANCO::ChaveOk($this->mCO_FUNCAO_CANDIDATO)."
					 , VA_RENUMERACAO_MENSAL = ".cBANCO::StringOk($this->mVA_RENUMERACAO_MENSAL)."
					 , VA_REMUNERACAO_MENSAL_BRASIL = ".cBANCO::StringOk($this->mVA_REMUNERACAO_MENSAL_BRASIL)."
					 , DS_LOCAL_TRABALHO = ".cBANCO::StringOk($this->mDS_LOCAL_TRABALHO)."
					 , TE_DESCRICAO_ATIVIDADES = ".cBANCO::StringOk($this->mTE_DESCRICAO_ATIVIDADES)."
					 , DS_JUSTIFICATIVA = ".cBANCO::StringOk($this->mDS_JUSTIFICATIVA)."
					 , DS_SALARIO_EXTERIOR = ".cBANCO::StringOk($this->mDS_SALARIO_EXTERIOR)."
					 , NU_USUARIO_CAD = ".$pcd_usuario."
				 where NU_CANDIDATO = ".$this->mNU_CANDIDATO."
				   and id_solicita_visto = ".$this->mid_solicita_visto;
		cAMBIENTE::ExecuteQuery($sql, __CLASS__.".".__FUNCTION__);
	}
}
