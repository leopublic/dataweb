<?php
class cprocesso_mudanca_embarcacao extends cprocesso_generico{

    public function __construct(){
        parent::__construct();
        $this->id_tipoprocesso = 13;
    }
    public function get_tipo_processo(){
        return $this->id_tipoprocesso;
    }

    public function get_nu_processo(){
        return '';
    }
    
    public function get_dt_requerimento(){
        return $this->date_1;
    }
    public function set_dt_requerimento($valor){
        $this->date_1 = $valor;
    }
    public function get_dt_envio_bsb(){
        return $this->date_2;
    }
    public function set_dt_envio_bsb($valor){
        $this->date_2 = $valor;
    }
    public function get_nu_embarcacao_projeto_anterior(){
        return $this->integer_1;
    }
    public function set_nu_embarcacao_projeto_anterior($valor){
        $this->integer_1 = $valor;
    }
    public function get_nu_embarcacao_projeto_novo(){
        return $this->integer_2;
    }
    public function set_nu_embarcacao_projeto_novo($valor){
        $this->integer_2 = $valor;
    }

    public function inicializa($os, $cand) {
        parent::inicializa($os, $cand);
        $this->set_nu_embarcacao_projeto_anterior($cand->mNU_EMBARCACAO_PROJETO);
        $this->set_nu_embarcacao_projeto_novo($os->mNU_EMBARCACAO_PROJETO);
    }

    public static function Atualiza_dt_envio_bsb($pid_solicita_visto, $pdt_envio_bsb, $pcd_usuario) {
        $sql = "update processo_generico
                   set date_2 = " . cBANCO::DataOk($pdt_envio_bsb) . "
                     , cd_usuario_ult = " . cBANCO::ChaveOk($pcd_usuario) . "
                 where id_solicita_visto = " . cBANCO::ChaveOk($pid_solicita_visto);
        cAMBIENTE::$db_pdo->exec($sql);
        if ($pdt_envio_bsb != '') {
            $status = cSTATUS_SOLICITACAO::ssENVIADA_BSB;
        } else {
            $status = cSTATUS_SOLICITACAO::ssDEVOLVIDA;
        }
        $sql = "update solicita_visto
		   set id_status_sol = " . $status . "
                     , dt_envio_bsb = " . cBANCO::DataOk($pdt_envio_bsb) . "
                     , cd_usuario_alteracao = " . $pcd_usuario . "
                     , soli_dt_alteracao = now()
                 where id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function RetirarDoProtocoloBsb($pid_solicita_visto, $pcd_usuario) {
        $sql = "update processo_generico
		   set date_2 = null
		     , cd_usuario_ult = " . cBANCO::ChaveOk($pcd_usuario) . "
	        where id_solicita_visto = " . cBANCO::ChaveOk($pid_solicita_visto);
        cAMBIENTE::$db_pdo->exec($sql);

        $sql = "update solicita_visto
		   set id_status_sol = 3
                     , dt_envio_bsb = null
                     , cd_usuario_alteracao = " . $pcd_usuario . "
                     , soli_dt_alteracao = now()
		 where solicita_visto.id_solicita_visto = " . $pid_solicita_visto;
        cAMBIENTE::$db_pdo->exec($sql);
    }

}
