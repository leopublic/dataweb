<?php
class cdocumento
{
	protected $docu_id;
	protected $cd_usuario_inclusao;
	protected $docu_dt_inclusao;
	protected $docu_tx_localizacao ;
	protected $tdoc_id;
	protected $nu_candidato;
	protected $codigo;
	protected $docu_dt_utilizado;
	protected $cd_usuario_utilizou;
	protected $docu_tx_tipo_processo;

	const dcCOPIA_PASSAPORTE	= 1;
	const dcFORM_1344			= 2;
	
	public function get_docu_id() {
		return $this->docu_id;
	}

	public function set_docu_id($docu_id) {
		$this->docu_id = $docu_id;
	}

	public function get_cd_usuario_inclusao() {
		return $this->cd_usuario_inclusao;
	}

	public function set_cd_usuario_inclusao($cd_usuario_inclusao) {
		$this->cd_usuario_inclusao = $cd_usuario_inclusao;
	}

	public function get_docu_dt_inclusao() {
		return $this->docu_dt_inclusao;
	}

	public function set_docu_dt_inclusao($docu_dt_inclusao) {
		$this->docu_dt_inclusao = $docu_dt_inclusao;
	}

	public function get_docu_tx_localizacao() {
		return $this->docu_tx_localizacao;
	}

	public function set_docu_tx_localizacao($docu_tx_localizacao) {
		$this->docu_tx_localizacao = $docu_tx_localizacao;
	}

	public function get_tdoc_id() {
		return $this->tdoc_id;
	}

	public function set_tdoc_id($tdoc_id) {
		$this->tdoc_id = $tdoc_id;
	}

	public function get_nu_candidato() {
		return $this->nu_candidato;
	}

	public function set_nu_candidato($nu_candidato) {
		$this->nu_candidato = $nu_candidato;
	}

	public function get_codigo() {
		return $this->codigo;
	}

	public function set_codigo($codigo) {
		$this->codigo = $codigo;
	}

	public function get_docu_dt_utilizado() {
		return $this->docu_dt_utilizado;
	}

	public function set_docu_dt_utilizado($docu_dt_utilizado) {
		$this->docu_dt_utilizado = $docu_dt_utilizado;
	}

	public function get_cd_usuario_utilizou() {
		return $this->cd_usuario_utilizou;
	}

	public function set_cd_usuario_utilizou($cd_usuario_utilizou) {
		$this->cd_usuario_utilizou = $cd_usuario_utilizou;
	}

	public function get_docu_tx_tipo_processo() {
		return $this->docu_tx_tipo_processo;
	}

	public function set_docu_tx_tipo_processo($docu_tx_tipo_processo) {
		$this->docu_tx_tipo_processo = $docu_tx_tipo_processo;
	}

		
	public function Incluir()
	{
		$sql = " insert into documento(
					cd_usuario_inclusao
				   ,docu_dt_inclusao
				   ,docu_tx_localizacao
				   ,tdoc_id
				   ,nu_candidato
				) values (
				    ".cBANCO::ChaveOk($this->cd_usuario_inclusao)."
				   , now()
				   ,".cBANCO::StringOk($this->docu_tx_localizacao)."
				   ,".cBANCO::ChaveOk($this->tdoc_id)."
				   ,".cBANCO::ChaveOk($this->nu_candidato)."
				)";
		cAMBIENTE::ExecuteQuery($sql, __CLASS__.".".__FUNCTION__);
		$this->docu_id = cAMBIENTE::$db_pdo->lastInsertId();
	}
	/**
	 * Adiciona um documento físico de um determinado tipo ao candidato
	 * @param int $pnu_candidato Candidato onde o documento está sendo adicionado
	 * @param int $ptdoc_id	Tipo do documento
	 */
	public static function Criar($pnu_candidato, $ptdoc_id, $pdocu_tx_localizacao, $pcd_usuario)
	{
		$doc = new cdocumento();
		$doc->set_cd_usuario_inclusao($pcd_usuario);
		$doc->set_docu_tx_localizacao($pdocu_tx_localizacao);
		$doc->set_nu_candidato($pnu_candidato);
		$doc->set_tdoc_id($ptdoc_id);
		$doc->Incluir();
	}
	/**
	 * Adiciona um documento físico de um determinado tipo ao candidato
	 * @param int $pnu_candidato Candidato onde o documento está sendo adicionado
	 * @param int $ptdoc_id	Tipo do documento
	 */
	public static function Remover($pnu_candidato, $ptdoc_id)
	{
		$sql = "delete from documento 
				where nu_candidato = ".$pnu_candidato."
				  and tdoc_id = ".$ptdoc_id."
				  and codigo is null";
		cAMBIENTE::ExecuteQuery($sql, __CLASS__.".".__FUNCTION__);		
	}
	/**
	 * Indica que o documento foi utilizado por um determinado processo
	 * @param int $pcodigo
	 * @param string $pdocu_tx_tipo_processo
	 * @param int $pcd_usuario
	 */
	public function Utilizar($pcodigo, $pdocu_tx_tipo_processo, $pcd_usuario)
	{
		$sql = "update documento 
				set codigo = ".cBANCO::ChaveOk($pcodigo)."
				  , docu_tx_tipo_processo = ".cBANCO::StringOk($pdocu_tx_tipo_processo)."
				  , cd_usuario_utilizou  = ".cBANCO::ChaveOk($pcd_usuario)."
				  , docu_dt_utilizado = now()
				where docu_id = ".$this->docu_id;
		cAMBIENTE::ExecuteQuery($sql, __CLASS__.".".__FUNCTION__);		
	}
	/**
	 * Recupera os documentos de um determinado candidato em um determinado processo
	 * @param int $pnu_candidato Candidato do processo
	 * @param int $pcodigo Chave do processo
	 * @param string $pdocu_tx_tipo_processo Tabela do processo
	 * @param boolean $precuperarDisponiveis Indica se os documentos disponiveis devem ser recuperados tambem ou nao
	 */
	public static function Cursor_DocumentosProcesso($pnu_candidato, $pcodigo, $pdocu_tx_tipo_processo, $precuperarDisponiveis = true)
	{
		$sql = "select ";
	}
	
	public static function TemDisponivel($pnu_candidato, $ptdoc_id)
	{
		$sql = "select docu_id 
				  from documento
				 where nu_candidato = ".$pnu_candidato."
				   and tdoc_id = ".$ptdoc_id."
				   and codigo is null";
		$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		$rs = $res->fetch(PDO::FETCH_ASSOC);
		if(intval($rs['docu_id']) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
