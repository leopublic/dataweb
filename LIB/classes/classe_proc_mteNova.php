<?php

class proc_mte {

  public $codigo = "";
  public $cd_candidato = "";
  public $cd_solicitacao = "";
  public $nu_processo = "";
  public $dt_requerimento = "";
  public $nu_oficio = "";
  public $dt_deferimento = "";
  public $prazo_solicitado = "";
  public $cd_funcao = "";
  public $cd_reparticao = "";
  public $observacao = "";
  public $dt_cad = "";
  public $dt_ult = "";
  public $myerr = "";

  public function proc_mte() {  }

  public function BuscaPorCodigo($codigo) {
    $extra = "WHERE codigo=$codigo";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function BuscaPorCandidato($candidato,$solicitacao) {
    $this->cd_candidato = $candidato;
    $this->cd_solicitacao = $solicitacao;
    $extra = "WHERE cd_candidato=$candidato AND cd_solicitacao=$solicitacao";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function ExisteCandidato($candidato,$solicitacao) {
    $extra = "WHERE cd_candidato=$candidato AND cd_solicitacao=$solicitacao";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if(mysql_num_rows($rs)>0) {
        return true;
      } else {
        return false;
      }
    }
  }

  public function BuscaPorProcesso($processo) {
    $extra = "WHERE nu_processo=$processo";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function ListaProcessos($extra) {
    $this->myerr = "";
    $sql = "SELECT * FROM processo_mte $extra";
    $rs = mysql_query($sql);
    if(mysql_errno() != 0) {
      $this->myerr = mysql_error();
    }
    return $rs;
  }

  function InsereProcesso() {
    if($this->Verifica() == true) {
      $cd_cand = $this->cd_candidato;
      $cd_sol = $this->cd_solicitacao;
      $sql1 = "INSERT INTO processo_mte (cd_candidato,cd_solicitacao,nu_processo,dt_requerimento,nu_oficio,";
      $sql2 = "dt_deferimento,prazo_solicitado,observacao,dt_cad) VALUES ";
      $sql = sprintf($sql1.$sql2."($cd_cand,$cd_sol,%s,%s,%s,%s,%s,'%s',now())",
              $this->nu_processo,
              $this->dt_requerimento,
              $this->nu_oficio,
              $this->dt_deferimento,
              $this->prazo_solicitado,
              mysql_real_escape_string($this->observacao)
             );
      mysql_query($sql);
      if(mysql_errno() != 0) {
        $erro = mysql_error();
        $this->myerr = "Ocorreu um erro ao inserir processo MTE. - $erro - sql=$sql ";
        gravaLogErro($sql,$erro,"CADASTRAR","PROCESSO_MTE");
      } else {
        gravaLog($sql,"","CADASTRAR","PROCESSO_MTE");
      }
    }
  }

  function AlteraProcesso() {
    if($this->Verifica() == true) {
      $cd_cand = $this->cd_candidato;
      $cd_sol = $this->cd_solicitacao;
      $sql1 = "UPDATE processo_mte SET nu_processo=%s,dt_requerimento=%s,nu_oficio=%s,";
      $sql2 = "dt_deferimento=%s,prazo_solicitado=%s,observacao='%s' ";
      $sql3 = " WHERE cd_candidato=$cd_cand AND cd_solicitacao=$cd_sol ";
      $sql = sprintf($sql1.$sql2.$sql3,
              $this->nu_processo,
              $this->dt_requerimento,
              $this->nu_oficio,
              $this->dt_deferimento,
              $this->prazo_solicitado,
              mysql_real_escape_string($this->observacao)
             );
      mysql_query($sql);
      if(mysql_errno() != 0) {
        $erro = mysql_error();
        $this->myerr = "Ocorreu um erro ao alterar processo MTE. - sql=$sql - ".$erro;
        gravaLogErro($sql,$erro,"ALTERAR","PROCESSO_MTE");
      } else {
        gravaLog($sql,"","ALTERAR","PROCESSO_MTE");
      }
    }
  }

  function AlteraVisaDetail() {
    $cd_cand = $this->cd_candidato;
    $cd_sol = $this->cd_solicitacao;
    $cd_func = $this->cd_funcao;
    $cd_rep = $this->cd_reparticao;
    $sql1 = "UPDATE AUTORIZACAO_CANDIDATO SET NU_PROCESSO_MTE=%s,DT_ABERTURA_PROCESSO_MTE=%s,NU_AUTORIZACAO_MTE=%s,";
    $sql2 = "DT_AUTORIZACAO_MTE=%s,DT_PRAZO_AUTORIZACAO_MTE=%s ";
    $sql3 = " where NU_CANDIDATO=$cd_cand and NU_SOLICITACAO=$cd_sol ";
    $sql = sprintf($sql1.$sql2.$sql3,
              $this->nu_processo,
              $this->dt_requerimento,
              $this->nu_oficio,
              $this->dt_deferimento,
              $this->prazo_solicitado
             );
    mysql_query($sql);
    if(mysql_errno() != 0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao alterar Visa Detail MTE. - $erro - sql=$sql ";
      gravaLogErro($sql,$erro,"ALTERAR","PROCESSO_MTE_VISA");
    } else {
      gravaLog($sql,"","ALTERAR","PROCESSO_MTE_VISA");
    }
  }

  function RemoveProcesso($codigo,$cand,$sol) {
    $sql = "DELETE FROM processo_mte WHERE ";
    if(strlen($codigo)>0) {  $sql = "codigo=$codigo";  } else
    if( (strlen($cand)>0) && (strlen($sol)>0) ) {  $sql = "cd_candidato=$cand AND cd_solicitacao=$sol";  }
    mysql_query($sql);
    if(mysql_errno() != 0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao remover processo MTE.<!-- $erro - sql=$sql -->";
      gravaLogErro($sql,$erro,"REMOVER","PROCESSO_MTE");
    } else {
      gravaLog($sql,"","REMOVER","PROCESSO_MTE");
    }
  }

  function Verifica() {
    $ret = true;
    $erro = "";
    if(strlen($this->cd_candidato) == 0) {
      $ret = false;
      $erro = $erro."Nao foi identificado o candidato. ";
    }
    if(strlen($this->cd_solicitacao) == 0) {
      $ret = false;
      $erro = $erro."Nao foi identificada a solicitacao. ";
    }
    if(strlen($this->dt_requerimento)>0) { $this->dt_requerimento = dataBR2My($this->dt_requerimento); }
    if(strlen($this->dt_deferimento)>0) { $this->dt_deferimento = dataBR2My($this->dt_deferimento); }
    if(strlen($this->nu_processo)>0) { $this->nu_processo = "'".limpaTudo($this->nu_processo)."'"; } else {  $this->nu_processo = "NULL"; }
    if(strlen($this->dt_requerimento)>0) { $this->dt_requerimento = "'".$this->dt_requerimento."'"; } else {  $this->dt_requerimento = "NULL"; }
    if(strlen($this->nu_oficio)>0) { $this->nu_oficio = "'".$this->nu_oficio."'"; } else {  $this->nu_oficio = "NULL"; }
    if(strlen($this->dt_deferimento)>0) { $this->dt_deferimento = "'".$this->dt_deferimento."'"; } else {  $this->dt_deferimento = "NULL"; }
    if(strlen($this->prazo_solicitado)>0) { $this->prazo_solicitado = "'".$this->prazo_solicitado."'"; } else {  $this->prazo_solicitado = "NULL"; }
    if($this->observacao=="NULL") { $this->observacao=""; }
    $this->myerr = $erro;
    return $ret;
  }

  private function LeituraRW($rw) {
    $this->codigo = $rw['codigo'];
    $this->cd_candidato = $rw['cd_candidato'];
    $this->cd_solicitacao = $rw['cd_solicitacao'];
    $this->nu_processo = $rw['nu_processo'];
    $this->dt_requerimento = dataMy2BR($rw['dt_requerimento']);
    $this->nu_oficio = $rw['nu_oficio'];
    $this->dt_deferimento = dataMy2BR($rw['dt_deferimento']);
    $this->prazo_solicitado = $rw['prazo_solicitado'];
    $this->cd_funcao = $rw['cd_funcao'];
    $this->cd_reparticao = $rw['cd_reparticao'];
    $this->observacao = $rw['observacao'];
    if($this->observacao=="NULL") { $this->observacao=""; }
    $this->dt_cad = $rw['dt_cad'];
    $this->dt_ult = $rw['dt_ult'];
  }

  private function LimpaObjeto() {
    $this->codigo = "";
    $this->cd_candidato = "";
    $this->cd_solicitacao = "";
    $this->nu_processo = "";
    $this->dt_requerimento = "";
    $this->nu_oficio = "";
    $this->dt_deferimento = "";
    $this->prazo_solicitado = "";
    $this->cd_funcao = "";
    $this->cd_reparticao = "";
    $this->observacao = "";
    $this->dt_cad = "";
    $this->dt_ult = "";
    $this->myerr = "";
  }

  public function MontaTextoProcessoMTE($acao) {
    $this->BuscaReparticaoFuncao($this->cd_candidato,$this->cd_solicitacao);
    $aux_esc = "<option value=''>Escolha ";
    $nmReparticao = pegaNomeReparticao($this->cd_reparticao);
    $nmFuncao = pegaNomeFuncao($this->cd_funcao);
    $cmbReparticao = "$aux_esc uma reparticao".montaComboReparticoes($this->cd_reparticao,"");
    $cmbFuncao = "$aux_esc uma funcao".montaComboFuncoes($this->cd_funcao,"ORDER BY NO_FUNCAO");
    $ret = "";
    $ret = $ret.montaCampos("cd_candidato",$this->cd_candidato,$this->cd_candidato,"H",$acao,"")."\n";
    $ret = $ret.montaCampos("cd_solicitacao",$this->cd_solicitacao,$this->cd_solicitacao,"H",$acao,"")."\n";
    $ret = $ret."<tr height=20><td class=textoazul bgcolor=#DADADA colspan=5>Processo MTE</td></tr>\n";
    $ret = $ret."<tr height=20><td><b>N&uacute;mero Processo MTE:</td>\n";
    if ($acao=="V")
    {
    	$ret = $ret."<td><a href=\"javascript:veracomp(".preg_replace("/[^0-9]/","", $this->codigo)." ,'MTE','".preg_replace("/[^0-9]/","", $this->nu_processo)."');\" class='textoazulpeq linkVisivel' title=\"Clique para ver o processo no Minist&eacute;rio do Trabalho\">".$this->nu_processo."</a></td>\n";
    }
    else
    {
    	$ret = $ret."<td>".montaCampos("NU_PROCESSO_MTE",$this->nu_processo,$this->nu_processo,"T",$acao,"maxlength=20 size=20")."&nbsp;<input type=\"button\" value=\"MTE\" onClick=\"javascript:veracomp($this->codigo ,'MTE','".preg_replace("/[^0-9]/","", $this->nu_processo)."');\" style=\"font-size:8pt;font-weight:bold;padding-left:1px;padding-right:1px;\" title=\"Clique para ver o processo no Minist&eacute;rio do Trabalho\" /></td>\n";
    }
    $ret = $ret."<td>&#160;</td><td><b>Data de Requerimento no MTE:</td>\n";
    $ret = $ret."<td>".montaCampos("DT_ABERTURA_PROCESSO_MTE",$this->dt_requerimento,$this->dt_requerimento,"D",$acao,"")."</td>\n";
    $ret = $ret."</tr>";
    $ret = $ret."<tr height=20><td><b>Fun&ccedil;&atilde;o do Candidato:</td>\n";
    $ret = $ret."<td>".montaCampos("CO_FUNCAO_CANDIDATO",$cmbFuncao,$nmFuncao,"S","V","")."</td>\n";
    $ret = $ret."<td>&#160;</td><td><b>Reparti&ccedil;&atilde;o Consular:</td>\n";
    $ret = $ret."<td>".montaCampos("CO_REPARTICAO_CONSULAR",$cmbReparticao,$nmReparticao,"S","V","")."</td>\n";
    $ret = $ret."</tr>\n";
    $ret = $ret."<tr height=20><td><b>N&uacute;mero  Oficio MRE:</td>\n";
    $ret = $ret."<td>".montaCampos("NU_AUTORIZACAO_MTE",$this->nu_oficio,$this->nu_oficio,"T",$acao,"maxlength=20 size=30")."</td>\n";
    $ret = $ret."<td>&#160;</td>\n<td><b>Prazo Solicitado no MTE:</td>\n";
    $ret = $ret."<td>".montaCampos("DT_PRAZO_AUTORIZACAO_MTE",$this->prazo_solicitado,$this->prazo_solicitado,"T",$acao,"maxlength=10 size=20")."</td>\n";
    $ret = $ret."</tr>\n";
    $ret = $ret."<tr height=20><td><b>Observa&ccedil;&atilde;o:</td>\n";
    $ret = $ret."<td colspan=4>".montaCampos("OBS_MTE",$this->observacao,$this->observacao,"T",$acao,"maxlength=250 size=30")."</td>\n";
    $ret = $ret."</tr>\n";
    if( ($this->cd_solicitacao > 0) && ($acao=="A") ) {
      $ret = $ret."<tr height=20><td><input type=checkbox name='todossol' value='y'> Sim,</td>\n";
      $ret = $ret."<td colspan=4>Aplicar a todos os candidatos da mesma solicita&ccedil;&atilde;o.</td>\n";
      $ret = $ret."</tr>\n";
    }
    return $ret;
  }

  public function LeituraRequest($tipo) {
    $this->cd_candidato = $tipo['cd_candidato'];
    $this->cd_solicitacao = $tipo['cd_solicitacao'];
    $this->nu_processo = $tipo['NU_PROCESSO_MTE'];
    $this->dt_requerimento = $tipo['DT_ABERTURA_PROCESSO_MTE'];
    $this->nu_oficio = $tipo['NU_AUTORIZACAO_MTE'];
    $this->dt_deferimento = $tipo['DT_AUTORIZACAO_MTE'];
    $this->prazo_solicitado = $tipo['DT_PRAZO_AUTORIZACAO_MTE'];
    $this->observacao = $tipo['OBS_MTE'];
    if($this->observacao=="NULL") { $this->observacao=""; }
  }

  public function BuscaReparticaoFuncao($cd_cand,$cd_sol) {
    $sql = "SELECT CO_FUNCAO_CANDIDATO,CO_REPARTICAO_CONSULAR from AUTORIZACAO_CANDIDATO ";
    $sql = $sql." where NU_CANDIDATO=$cd_cand and NU_SOLICITACAO=$cd_sol ";
    $rs = mysql_query($sql);
    if($rw=mysql_fetch_array($rs)) {
      $this->cd_funcao = $rw['CO_FUNCAO_CANDIDATO'];
      $this->cd_reparticao = $rw['CO_REPARTICAO_CONSULAR'];
    }
  }

  public function AcompanhaProcesso($empresa) {
    $cand = $this->cd_candidato;
    $sol = $this->cd_solicitacao;
    $proc = $this->nu_processo;
    $nuofc = $this->nu_oficio;
    $dtdef = $this->dt_deferimento;
    if( (strlen($nuofc)>3) || (strlen($dtdef)==10) ) {
      $status = "F";
    } else {
      $status = "C";
    }
    if( (strlen($cand)>0) && (strlen($sol)>0) ) {
      $rs = $this->BuscaProcessoMTE($cand,$sol);
      if($this->myerr == "") {
        if(mysql_num_rows($rs)==0) {
          $this->InsereAcompanhamentoMTE($empresa,$cand,$sol,$proc,$status);
        } else {
          $rw = mysql_fetch_array($rs);
          $codigo = $rw['codigo'];
          $this->AlteraAcompanhamentoMTE($codigo,$proc,$status);
        }
      }
    } else {
      $this->myerr = "Nao ha informacao de candidato/solicitacao/processo";
    }
  }

  public function BuscaProcessoMTE($candidato,$solicitacao) {
    $sql = "SELECT * FROM acompanhamento_processo WHERE tipo='MTE' AND candidato=$candidato AND solicitacao=$solicitacao";
    $rs = mysql_query($sql);
    if(mysql_errno()!=0) {
      $this->myerr = mysql_error();
    }
    return $rs;
  }

  public function InsereAcompanhamentoMTE($empresa,$candidato,$solicitacao,$processo,$status) {
    $sql = "INSERT INTO acompanhamento_processo (empresa,candidato,solicitacao,processo,tipo,status,dt_cad) ";
    $sql = $sql."VALUES ($empresa,$candidato,$solicitacao,$processo,'MTE','$status',now()) ";
    mysql_query($sql);
    if(mysql_errno()!=0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao alterar acompanhamento MTE. - $erro - sql=$sql ";
    }
    return $rs;
  }

  public function AlteraAcompanhamentoMTE($codigo,$processo,$status) {
    $sql = "UPDATE acompanhamento_processo SET processo=$processo,status='$status' WHERE codigo=$codigo";
    mysql_query($sql);
    if(mysql_errno()!=0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao alterar acompanhamento MTE. - $erro - sql=$sql ";
    }
    return $rs;
  }

  function insereEvento($idEmpresa,$tipo) {
    $informou = "";
    $naoinfor = "";
    $cand = $this->cd_candidato;
    $sol = $this->cd_solicitacao;
    $processo = str_replace("'","",$this->nu_processo);
    $dt_req = dataMy2BR(str_replace("'","",$this->dt_requerimento));
    $oficio = str_replace("'","",$this->nu_oficio);
    $dt_defer = dataMy2BR(str_replace("'","",$this->dt_deferimento));
    $prazo = dataMy2BR(str_replace("'","",$this->prazo_solicitado));
    if ( (strlen($processo)>0) && ($processo != "NULL") ) {
       $pagina = "http://www.mte.gov.br/Empregador/TrabEstrang/Pesquisa/ConsultaProcesso.asp?Parametro=$processo&Opcao=1";
       $informou = $informou." processo (<a href='$pagina' target=newwin>$processo</a>) / ";
    } else {
       $informou = $informou." processo (NULL) / ";
    }
    $informou = $informou." data de requerimento ($dt_req) / ";
    $informou = $informou." oficio ($oficio) / ";
    $informou = $informou." data de deferimento ($dt_defer) / ";
    $informou = $informou." prazo solicitado ($prazo) ";
    $extra="no MTE. <br>Informou: $informou .";
    $texto = "$tipo do cadastro do candidato $extra";
    insereEvento($idEmpresa,$cand,$sol,$texto);
  }

  public function RelatorioRS($tipo,$extra,$grupo) { # tipo = "C" = contar apenas
    $sql = "";
    $this->myerr = "";
    if($tipo=="C") {
      $sql = "SELECT count(*),$grupo ";
    } else {
      $sql = $sql."SELECT a.cd_candidato,a.cd_solicitacao,a.nu_processo,a.dt_requerimento,a.nu_oficio,a.dt_deferimento,a.prazo_solicitado, ";
      $sql = $sql."a.observacao,a.dt_cad,b.NU_EMPRESA,c.NO_RAZAO_SOCIAL,b.NU_EMBARCACAO_PROJETO,b.CO_FUNCAO_CANDIDATO,b.CO_REPARTICAO_CONSULAR ";
      $sql = $sql.",CONCAT(e.NO_PRIMEIRO_NOME,' ',e.NO_NOME_MEIO,' ',e.NO_ULTIMO_NOME) as nome_candidato ";
    }
    $onde = "FROM processo_mte a, AUTORIZACAO_CANDIDATO b, EMPRESA c, acompanhamento_processo d, CANDIDATO e ";
    $oque = "WHERE a.cd_candidato=b.NU_CANDIDATO AND a.cd_solicitacao=b.NU_SOLICITACAO AND b.NU_EMPRESA=c.NU_EMPRESA ";
    $mais = "AND a.cd_candidato=d.candidato AND a.cd_solicitacao=d.solicitacao AND d.tipo='MTE' AND a.cd_candidato=e.NU_CANDIDATO ";
    $query = $sql.$onde.$oque.$mais.$extra.$grupo;
    $rs = mysql_query($query);
    if(mysql_errno() != 0) {
      $this->myerr = mysql_error();
    }
    return $rs;
  }

}
?>
