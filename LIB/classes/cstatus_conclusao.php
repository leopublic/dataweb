<?php

/**
 * Controla o status das OS
 */
class cstatus_conclusao extends cMODELO {

    public $mid_status_conclusao;
    public $mno_status_conclusao;

    const scNA = 1;
    const scDEFERIDO = 2;
    const scINDEFERIDO = 3;
    const scARQUIVADO = 4;

    public function campoid() {
        return 'id_status_conclusao';
    }

    public function getid() {
        return $this->mid_status_conclusao;
    }

    public function setid($pValor) {
        $this->mid_status_conclusao = $pValor;
    }

    public function sql_Liste() {
        $sql = " select * from status_conclusao where 1=1";
        return $sql;
    }

    public function sql_RecuperePeloId() {
        $sql = " select * from status_conclusao where id_status_conclusao = " . $this->mid_status_conclusao;
        return $sql;
    }

    public static function get_nomeCampoChave(){
        return 'id_status_conclusao';
    }

    public static function get_nomeCampoDescricao(){
        return 'no_status_conclusao';
    }

    public static function get_nomeTabelaBD(){
        return 'status_conclusao';
    }

    public function Atualizar() {
        $sql = " update status_conclusao set ";
        $sql.= "    no_status_conclusao  = " . cBANCO::StringOk($this->mno_status_conclusao);
        $sql.= " where id_status_conclusao = " . $this->mid_status_conclusao;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function Incluir() {
        $sql = " insert into status_conclusao(";
        $sql.= "        no_status_conclusao";
        $sql.= ") values (";
        $sql.= "    " . cBANCO::StringOk($this->mno_status_conclusao);
        $sql.= ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->setid(cAMBIENTE::$db_pdo->lastInsertId());
    }

}
