<?php

class ctaxa extends cMODELO {

    public $mid_taxa;
    public $mid_perfiltaxa;
    public $mdescricao;
    public $mvalor_fixo;

    public static function get_nomeCampoChave() {
        return 'id_taxa';
    }

    public static function get_nomeTabelaBD() {
        return 'taxa';
    }

    public function get_nomeTabela() {
        return self::get_nomeTabelaBD();
    }

    public function campoid() {
        return self::get_nomeCampoChave();
    }

    public function setid($pid) {
        $this->mid_taxa= $pid;
    }

    public function getid() {
        return $this->mid_taxa;
    }

    public function sql_Liste() {
        $sql = "select t.* from taxa t";
        $sql .= " where 1=1 ";
        return $sql;
    }

    public function Incluir() {
        $sql = " insert into taxa(
					id_perfiltaxa
					,descricao
					,valor_fixo
				) values (
					" . cBANCO::ChaveOk($this->mid_perfiltaxa) . "
					," . cBANCO::StringOk($this->mdescricao) . "
					," . cBANCO::ValorOk($this->mvalor_fixo) . "
					)";
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function Atualizar() {
        $sql = "update taxa set
					descricao				= " . cBANCO::StringOk($this->mdescricao) . "
					,valor_fixo	= " . cBANCO::ValorOk($this->mvalor_fixo) . "
				where id_taxa = " . $this->mid_taxa;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from taxa where id_taxa = " . $this->mid_taxa;
        return $sql;
    }

    public function Excluir() {
        $sql = "delete from taxa where id_taxa = " . $this->mid_taxa;
        cAMBIENTE::ExecuteQuery($sql);
    }
}
