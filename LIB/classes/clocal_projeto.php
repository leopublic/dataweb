<?php

class clocal_projeto extends cMODELO {

    public $mloca_id;
    public $mloca_no_nome;


    public static function get_nomeCampoChave() {
        return 'local_id';
    }
    public static function get_nomeCampoDescricao(){
        return 'loca_no_nome';
    }

    public static function get_nomeTabelaBD() {
        return 'local_projeto';
    }

    public function get_nomeTabela() {
        return self::get_nomeTabelaBD();
    }

    public function campoid() {
        return self::get_nomeCampoChave();
    }

    public function setid($pid) {
        $this->mloca_id = $pid;
    }

    public function getid() {
        return $this->mloca_id;
    }

    public function sql_Liste() {
        $sql = "    select * from local_projeto order by loca_no_nome";
        return $sql;
    }

    public function Incluir() {
        $sql = " insert into local_projeto(
                        loca_no_nome
                ) values (
                        " . cBANCO::StringOk($this->mloca_no_nome) . "
                        )";
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function Atualizar() {
        $sql = "update local_projeto set
                    loca_no_nome = " . cBANCO::StringOk($this->mloca_no_nome) . "
            where loca_id = " . $this->mloca_id;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_RecuperePeloId() {
        $sql = "    select *
                    from local_projet
                   where loca_id = " . $this->mloca_id;
        return $sql;
    }

    public static function comboMustache($valor_atual = '', $default = "(selecione...)") {
        $sql = "select local_id valor, loca_no_nome descricao"
                . ", if(local_id='".$valor_atual."' ,1 ,0 ) selected"
                . " from local_projeto"
                . " order by loca_no_nome";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));

        if (intval($valor_atual == 0)){
            $rs['0']['selected'] = true;
        }

        return  $rs ;
    }
}
