<?php

class cperfiltaxa extends cMODELO {

    public $mid_perfiltaxa;
    public $mdescricao;

    public static function get_nomeCampoChave() {
        return 'id_perfiltaxa';
    }

    public static function get_nomeTabelaBD() {
        return 'perfiltaxa';
    }

    public function get_nomeTabela() {
        return self::get_nomeTabelaBD();
    }

    public function campoid() {
        return self::get_nomeCampoChave();
    }

    public function setid($pid) {
        $this->mid_perfiltaxa= $pid;
    }

    public function getid() {
        return $this->mid_perfiltaxa;
    }

    public function sql_Liste() {
        $sql = "    select *
			           from perfiltaxa t"
                . "  where 1=1";
        return $sql;
    }

    public function Incluir() {
        $sql = " insert into perfiltaxa(
					descricao
				) values (
					" . cBANCO::StringOk($this->mdescricao) . "
					)";
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function Atualizar() {
        $sql = "update perfiltaxa set
					descricao				= " . cBANCO::DataOk($this->mdescricao) . "
				where id_perfiltaxa = " . $this->mid_perfiltaxa;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_RecuperePeloId() {
        $sql = "    select *
					   from perfiltaxa
					  where id_perfiltaxa = " . $this->mid_perfiltaxa;
        return $sql;
    }

    public function Excluir() {
        $sql = "delete from perfiltaxa where id_perfiltaxa = " . $this->mid_perfiltaxa;
        cAMBIENTE::ExecuteQuery($sql);
    }
}
