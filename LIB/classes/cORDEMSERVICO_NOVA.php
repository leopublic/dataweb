<?php

class cORDEMSERVICO_NOVA {

    public $nu_empresa;
    protected $empresa;
    public $nu_servico;
    protected $servico;

    public function setNuEmpresa($nu_empresa) {
        $this->nu_empresa = $nu_empresa;
        unset($this->empresa);
    }

    public function getNuEmpresa() {
        return $this->nu_empresa;
    }

    public function getEmpresa() {
        if (isset($this->empresa)) {
            return $this->empresa;
        } else {
            $this->empresa = new cEMPRESA();
            $this->empresa->mNU_EMPRESA = $this->nu_empresa;
            $this->empresa->RecuperePeloId();
            return $this->empresa;
        }
    }

    public function setNuServico($nu_servico) {
        $this->nu_servico = $nu_servico;
        unset($this->servico);
    }

    public function getServico() {
        if (isset($this->servico)) {
            return $this->servico;
        } else {
            $this->servico = new cSERVICO();
            $this->servico->mNU_SERVICO = $this->nu_servico;
            $this->servico->RecuperePeloId();
        }
    }

    public static function osAberta($nu_candidato, $nu_empresa, $nu_servico) {
        $sql = "select sv.*,  c.codigo_processo_mte_atual, s.id_tipo_acompanhamento
                from candidato c
		join autorizacao_candidato ac on ac.nu_candidato = c.nu_candidato
                join solicita_visto sv on sv.id_solicita_visto = ac.id_solicita_visto
                join servico s on s.nu_servico = sv.nu_servico
                join status_solicitacao ss on ss.id_status_sol = sv.id_status_sol
		where sv.nu_empresa = " . $nu_empresa . "
		and sv.nu_servico = " . $nu_servico . "
		and ss.FL_ENCERRADA <> 1
                and c.nu_candidato = " . $nu_candidato . "
		order by sv.id_solicita_visto desc limit 1";

        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        if (isset($rs['id_solicita_visto']) && $rs['id_solicita_visto'] > 0) {
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $rs['id_solicita_visto'];
            $os->mNU_SOLICITACAO = $rs['nu_solicitacao'];
            $os->mID_TIPO_ACOMPANHAMENTO = $rs['id_tipo_acompanhamento'];
            $os->CarreguePropriedadesPeloRecordset($rs);
            $processo = cprocesso::FabricaProcessoDaOs($os);
            if (method_exists($processo, 'RecupereSePelaOs')) {
                $processo->RecupereSePelaOs($os->mID_SOLICITA_VISTO, $nu_candidato);
                if ($processo->codigo_processo_mte == $rs['codigo_processo_mte_atual']) {
                    return $os;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function proximoNumero(cORDEMSERVICO $os) {
        if ($os->mid_solicita_visto_pacote > 0) {
            $ospacote = new cORDEMSERVICO();
            $ospacote->Recuperar($os->mid_solicita_visto_pacote);
            $numero = $ospacote->mNU_SOLICITACAO;
        } else {
            $sql = "select max(nu_solicitacao) from solicita_visto";
            if ($res = mysql_query($sql)) {
                $rs = mysql_fetch_array($res);
                $numero = $rs[0];
                mysql_free_result($res);
            }
            $sql = "select max(nu_solicitacao) from dataweb_log.solicita_visto";
            if ($res = mysql_query($sql)) {
                $rs = mysql_fetch_array($res);
                if ($rs[0] > $numero) {
                    $numero = $rs[0];
                }
                mysql_free_result($res);
            }
            $numero++;
        }
        return $numero;
    }

}
