<?php
/**
 * @property int $depe_id ID
 * @property int $nu_dependente ordem do dependente
 * @property int $nu_candidato Candidato de quem o dependente Ã©
 * @property string $no_dependente Nome do dependente
 * @property int $co_grau_parentesco Grau de parentesco
 * @property date $dt_nascimento Data de nascimento
 * @property int $co_nacionalidade Nacionalidade
 * @property string $nu_passaporte NÃºmero do passaporte
 * @property int $co_pais_emissor_passaporte PaÃ­s emissor do passaporte
 */
class cdependente extends cMODELO{
	protected $depe_id;
	protected $nu_dependente;
	protected $nu_candidato;
	protected $no_dependente;
	protected $co_grau_parentesco;
	protected $dt_nascimento;
	protected $co_nacionalidade;
	protected $nu_passaporte;
	protected $co_pais_emissor_passaporte;

    /**
     * Get e set para a propriedade depe_id
     */
    public function get_depe_id(){
        return $this->depe_id;
    }
    public function set_depe_id($pVal){
        $this->depe_id = $pVal;
    }
    /**
     * Get e set para a propriedade nu_dependente
     */
    public function get_nu_dependente(){
        return $this->nu_dependente;
    }
    public function set_nu_dependente($pVal){
        $this->nu_dependente = $pVal;
    }
    /**
     * Get e set para a propriedade nu_candidato
     */
    public function get_nu_candidato(){
        return $this->nu_candidato;
    }
    public function set_nu_candidato($pVal){
        $this->nu_candidato = $pVal;
    }
    /**
     * Get e set para a propriedade no_dependente
     */
    public function get_no_dependente(){
        return $this->no_dependente;
    }
    public function set_no_dependente($pVal){
        $this->no_dependente = $pVal;
    }
    /**
     * Get e set para a propriedade co_grau_parentesco
     */
    public function get_co_grau_parentesco(){
        return $this->co_grau_parentesco;
    }
    public function set_co_grau_parentesco($pVal){
        $this->co_grau_parentesco = $pVal;
    }
    /**
     * Get e set para a propriedade dt_nascimento
     */
    public function get_dt_nascimento(){
        return $this->dt_nascimento;
    }
    public function set_dt_nascimento($pVal){
        $this->dt_nascimento = $pVal;
    }
    /**
     * Get e set para a propriedade co_nacionalidade
     */
    public function get_co_nacionalidade(){
        return $this->co_nacionalidade;
    }
    public function set_co_nacionalidade($pVal){
        $this->co_nacionalidade = $pVal;
    }
    /**
     * Get e set para a propriedade nu_passaporte
     */
    public function get_nu_passaporte(){
        return $this->nu_passaporte;
    }
    public function set_nu_passaporte($pVal){
        $this->nu_passaporte = $pVal;
    }
    /**
     * Get e set para a propriedade co_pais_emissor_passaporte
     */
    public function get_co_pais_emissor_passaporte(){
        return $this->co_pais_emissor_passaporte;
    }
    public function set_co_pais_emissor_passaporte($pVal){
        $this->co_pais_emissor_passaporte = $pVal;
    }

	public function getid(){
		return $this->depe_id;
	}

	public function setid($pValor){
		$this->depe_id = $pValor;
	}
	public function get_nomeTabela(){
		return "dependentes";
	}
	
	public function sql_Liste() {
		$sql = "select d.depe_id
					 , d.nu_dependente
					 , d.nu_candidato
					 , d.no_dependente
					 , d.co_grau_parentesco
					 , d.dt_nascimento
					 , d.co_nacionalidade
					 , d.nu_passaporte
					 , d.co_pais_emissor_passaporte
					 , gp.no_grau_parentesco
					 , p.no_nacionalidade
					 , pp.no_pais
			    from dependentes d
				left join grau_parentesco gp on gp.co_grau_parentesco = d.co_grau_parentesco
				left join pais_nacionalidade p on p.co_pais = d.co_nacionalidade
				left join pais_nacionalidade pp on pp.co_pais = d.co_pais_emissor_passaporte
				where 1=1 ";
		return $sql;
	}

	public function sql_RecuperePeloId() {
		$sql = "select depe_id
					 , nu_dependente
					 , nu_candidato
					 , no_dependente
					 , co_grau_parentesco
					 , dt_nascimento
					 , co_nacionalidade
					 , nu_passaporte
					 , co_pais_emissor_passaporte
			    from dependentes d
				where depe_id =  ".$this->depe_id;
		return $sql;
		
	}

	public function ConsistirSempre() {
		$msg = "";
		$msg .= $this->ConsisteTextoObrigatorio($this->no_dependente, '- nome do dependente Ã© obrigatÃ³rio');
		$msg .= $this->ConsisteChaveEstrObrigatoria($this->co_grau_parentesco, '- grau de parentesco Ã© obrigatÃ³rio');
		$msg .= $this->ConsisteChaveEstrObrigatoria($this->co_nacionalidade, '- nacionalidade Ã© obrigatÃ³ria');
		$msg .= $this->ConsisteTextoObrigatorio($this->nu_passaporte, '- nÃºmero do passaporte Ã© obrigatÃ³rio');
		$msg .= $this->ConsisteChaveEstrObrigatoria($this->co_pais_emissor_passaporte, '- paÃ­s emissor do passaporte Ã© obrigatÃ³rio');
		$msg .= $this->ConsisteTextoObrigatorio($this->dt_nascimento, '- data de nascimento Ã© obrigatÃ³ria');
		if($msg != ''){
			throw new cERRO_CONSISTENCIA("NÃ£o foi possÃ­vel atualizar pois:".$msg);
		}
	}

	public function ConsistirInclusao() {
		$this->ConsistirSempre();
	}
	
	public function ConsistirAtualizacao() {
		$this->ConsistirSempre();
	}
	
	public function Atualizar() {
		$this->ConsistirAtualizacao();
		$sql = "update dependentes set ";
//		$sql.= "   nu_dependente              = ".cBANCO::ValorOk($this->nu_dependente);
		$sql.= "  no_dependente              = ".cBANCO::StringOk($this->no_dependente);
		$sql.= " , co_grau_parentesco         = ".cBANCO::ChaveOk($this->co_grau_parentesco);
		$sql.= " , dt_nascimento              = ".cBANCO::DataOk($this->dt_nascimento);
		$sql.= " , co_nacionalidade           = ".cBANCO::ChaveOk($this->co_nacionalidade);
		$sql.= " , nu_passaporte   			  = ".cBANCO::StringOk($this->nu_passaporte);
		$sql.= " , co_pais_emissor_passaporte = ".cBANCO::ChaveOk($this->co_pais_emissor_passaporte);
		$sql.= " where depe_id = ".$this->depe_id;
		cAMBIENTE::$db_pdo->exec($sql);
	}
	
	public function Incluir() {
		$this->ConsistirInclusao();
		$sql = "insert into dependentes (";
		$sql.= "   nu_candidato ";
		$sql.= " , nu_dependente ";
		$sql.= " , no_dependente ";
		$sql.= " , co_grau_parentesco ";
		$sql.= " , dt_nascimento  ";
		$sql.= " , co_nacionalidade ";
		$sql.= " , nu_passaporte ";
		$sql.= " , co_pais_emissor_passaporte  ";
		$sql.= " )values(";
		$sql.= "   ".cBANCO::ChaveOk($this->nu_candidato);
		$sql.= " , ".cBANCO::ValorOk($this->nu_dependente);
		$sql.= " , ".cBANCO::StringOk($this->no_dependente);
		$sql.= " , ".cBANCO::ChaveOk($this->co_grau_parentesco);
		$sql.= " , ".cBANCO::DataOk($this->dt_nascimento);
		$sql.= " , ".cBANCO::ChaveOk($this->co_nacionalidade);
		$sql.= " , ".cBANCO::StringOk($this->nu_passaporte);
		$sql.= " , ".cBANCO::ChaveOk($this->co_pais_emissor_passaporte);
		$sql.= " )";
		cAMBIENTE::$db_pdo->exec($sql);
		$this->setid(cAMBIENTE::$db_pdo->lastInsertId());
	}
	
	public static function CursorDependentesDeCandidato($pnu_candidato)
	{
		$sql = "select d.nu_dependente
					 , d.nu_candidato
					 , d.no_dependente
					 , d.co_grau_parentesco
					 , d.dt_nascimento
					 , d.co_nacionalidade
					 , d.nu_passaporte
					 , d.co_pais_emissor_passaporte
					 , gp.no_grau_parentesco
					 , n.no_nacionalidade
					 , e.no_pais
				  from dependentes         d
			 left join grau_parentesco    gp on gp.co_grau_parentesco = d.co_grau_parentesco
			 left join pais_nacionalidade  n on n.co_pais = d.co_nacionalidade
			 left join pais_nacionalidade  e on e.co_pais = d.co_pais_emissor_passaporte
				 where nu_candidato = ".$pnu_candidato;
		$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		return $res;
	}
	
	public static function ArrayDependentesDeCandidato($pnu_candidato)
	{
		$cursor = self::CursorDependentesDeCandidato($pnu_candidato);
		$deps = array();
		while($rs = $cursor->fetch(PDO::FETCH_ASSOC))
		{
			$deps[] = $rs;
		}
		return $deps;
	}

}
