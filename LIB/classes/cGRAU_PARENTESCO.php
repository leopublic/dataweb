<?php
class cGRAU_PARENTESCO extends cMODELO{
	protected $mCO_GRAU_PARENTESCO;
	protected $mNO_GRAU_PARENTESCO;
	protected $mNO_GRAU_PARENTESCO_EM_INGLES;
	
	public function getId()
	{
		return $this->mCO_GRAU_PARENTESCO;
	}
	
	public function setId($pValor)
	{
		$this->mCO_GRAU_PARENTESCO = $pValor;
	}
	
	public function campoid() 
	{
		return 'CO_GRAU_PARENTESCO';
	}
	
	public function sql_RecuperePeloId() {
		$sql = "select * from GRAU_PARENTESCO where CO_GRAU_PARENTESCO = ".$this->mCO_GRAU_PARENTESCO;
		return $sql;
	}
	
	public function Incluir()
	{
		$sql= "insert into GRAU_PARENTESCO(
				 NO_GRAU_PARENTESCO 
				,NO_GRAU_PARENTESCO_EM_INGLES 
		       ) values (
				 ".cBANCO::StringOk($this->mNO_GRAU_PARENTESCO)."
				,".cBANCO::StringOk($this->mNO_GRAU_PARENTESCO_EM_INGLES)."
				)";
		cAMBIENTE::ExecuteQuery($sql);
		$this->mCO_GRAU_PARENTESCO = cAMBIENTE::$db_pdo->lastInsertId();
	}
	
	public function Atualizar()
	{
		$sql= "update GRAU_PARENTESCO set 
				 NO_GRAU_PARENTESCO = ".cBANCO::StringOk($this->mNO_GRAU_PARENTESCO)."
				,NO_GRAU_PARENTESCO_EM_INGLES = ".cBANCO::StringOk($this->mNO_GRAU_PARENTESCO_EM_INGLES)."
			  where CO_GRAU_PARENTESCO=".$this->mCO_GRAU_PARENTESCO;
		cAMBIENTE::ExecuteQuery($sql);
	}
	
	public function sql_Liste()
	{
		$sql = "select * from GRAU_PARENTESCO where 1=1";
		return $sql;
	}
}
