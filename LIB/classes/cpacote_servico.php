<?php
/**
 * Seriços
 * @author Leonardo
 */
class cpacote_servico extends cSERVICO{

	public static function CrieNovo()
	{
		$serv = new cSERVICO();
		$serv->mNU_SERVICO = 0;
		$serv->mserv_fl_pacote = 1;
		$serv->mNO_SERVICO = '(novo pacote)';
		$serv->mNO_SERVICO_RESUMIDO = '(novo pacote)';
		$serv->mserv_fl_despesas_adicionais= '0';
		$serv->mserv_fl_timesheet = '0';
		$serv->mNU_TIPO_SERVICO = '0';
		$serv->mserv_fl_form_tvs = '0'; 
		$serv->mserv_fl_cria_visto = '0';
		$serv->mfl_disponivel_clientes = '0';
		return $serv;
	}
	
	public static function IncluaServicoNoPacote($pnu_servico_pacote, $pnu_servico){
		$sql = "select nu_servico_incluido from pacote_servico 
				 where nu_servico_pacote=".$pnu_servico_pacote." 
				   and nu_servico_incluido=".$pnu_servico;
		$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		$rs = $res->fetch(PDO::FETCH_BOTH);
		if($rs[0] == ''){
			$sql = "insert into pacote_servico (nu_servico_pacote, nu_servico_incluido) values(".$pnu_servico_pacote.", ".$pnu_servico.")";
			cAMBIENTE::ExecuteQuery($sql);
		}		
	}

	public static function RetireServicoDoPacote($pnu_servico_pacote, $pnu_servico){
		$sql = "delete from pacote_servico 
				 where nu_servico_pacote = ".$pnu_servico_pacote."
				   and nu_servico_incluido = ".$pnu_servico;
		cAMBIENTE::ExecuteQuery($sql);
	}

	public static function ServicoPrincipal($pnu_servico_pacote, $pnu_servico){
		$sql = "update pacote_servico 
			       set pacs_fl_principal = 1 
				 where nu_servico_pacote = ".$pnu_servico_pacote."
				   and nu_servico_incluido = ".$pnu_servico;
		cAMBIENTE::ExecuteQuery($sql);
		$sql = "update pacote_servico 
			       set pacs_fl_principal = 0 
				 where nu_servico_pacote = ".$pnu_servico_pacote."
				   and nu_servico_incluido <> ".$pnu_servico;
		cAMBIENTE::ExecuteQuery($sql);
	}
	
	public function Exclua()
	{
		$sql = "delete from pacote_servico where nu_servico_pacote = ".$this->mNU_SERVICO;
		cAMBIENTE::ExecuteQuery($sql);
		parent::Exclua();
	}
	
	public static function CursorServicosDoPacote($pnu_servico_pacote)
	{
		$sql = "select s.*
					 , ps.* 
				  from pacote_servico ps 
				  join servico s on s.nu_servico = ps.nu_servico_incluido 
				 where nu_servico_pacote = ".$pnu_servico_pacote." order by pacs_fl_principal desc, s.no_servico_resumido";
		$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		return $res;
	}
}
