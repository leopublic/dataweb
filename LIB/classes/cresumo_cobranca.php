<?php

class cresumo_cobranca extends cMODELO {

    public $mresc_id;
    public $mNU_EMPRESA;
    public $mstrc_id;
    public $mNU_EMBARCACAO_PROJETO;
    public $mNU_EMBARCACAO_PROJETO_COBRANCA;
    public $mnu_empresa_requerente;
    public $mresc_dt_criacao;
    public $mresc_dt_faturamento;
    public $mresc_dt_recebimento;
    public $mresc_tx_nota_fiscal;
    public $mresc_nu_numero;
    public $mNO_RAZAO_SOCIAL;
    public $mNO_EMBARCACAO_PROJETO;
    public $mid_solicita_visto;
    public $mresc_tx_observacoes;
    public $mresc_tx_solicitante;
    public $mempf_id;
    public $mresc_nu_numero_mv;
    public $mnu_empresa_prestadora;

    /**
     * Chave
     *
     * @return integer Chave do resumo
     */
    public function get_resc_id() {
        return $this->mresc_id;
    }

    /**
     * Chave
     *
     * @param int $mresc_id Valor da chave
     */
    public function set_resc_id($mresc_id) {
        $this->mresc_id = $mresc_id;
        return $this;
    }

    public function get_nomeTabela() {
        return 'resumo_cobranca';
    }

    public function campoid() {
        return 'resc_id';
    }

    public function setid($pid) {
        $this->mresc_id = $pid;
    }

    public function getid() {
        return $this->mresc_id;
    }

    public function sql_Liste($tipo = 'registros') {
        $sql = "select r.resc_id
					,r.nu_empresa_requerente
					,r.strc_id
					,r.NU_EMBARCACAO_PROJETO_COBRANCA
					,r.resc_dt_criacao
					,r.resc_dt_faturamento
					,r.resc_dt_recebimento
					,r.resc_tx_nota_fiscal
					,r.resc_nu_numero
					,r.resc_nu_numero_mv
					,r.resc_tx_observacoes
					,r.resc_tx_solicitante
					,r.nu_empresa_prestadora
                    ,p.nome nome_prestadora
					,ef.empf_tx_descricao
					,s.resc_id resc_id_solicitacao
					, NO_RAZAO_SOCIAL
					, NO_EMBARCACAO_PROJETO
					, ef.empf_tx_descricao
					, ifnull(count(*),0) qtd
					, ifnull(sum(soli_vl_cobrado),0) soli_vl_cobrado
                                        ";
        $sql .= "from resumo_cobranca r
                left join EMPRESA E on E.NU_EMPRESA = r.nu_empresa_requerente
                left join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = r.nu_empresa_requerente and EP.NU_EMBARCACAO_PROJETO = r.NU_EMBARCACAO_PROJETO_COBRANCA
                left join solicita_visto s on s.resc_id = r.resc_id
                left join empresa_faturamento ef on ef.empf_id = r.empf_id
                left join empresa_prestadora p on p.nu_empresa_prestadora = r.nu_empresa_prestadora
                where 1 = 1 ";
        return $sql;
    }

    public function sql_RecuperePeloId() {
        $sql = "select r.*, NO_RAZAO_SOCIAL, NO_EMBARCACAO_PROJETO
            from resumo_cobranca r
            left join EMPRESA E on E.NU_EMPRESA = r.nu_empresa_requerente
            left join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = r.nu_empresa_requerente and EP.NU_EMBARCACAO_PROJETO = r.NU_EMBARCACAO_PROJETO_COBRANCA
            where resc_id = " . $this->mresc_id;
        return $sql;
    }

    public function ConsistirSempre() {
        $msg = '';
        if ($this->mresc_dt_faturamento == '') {
            $msg .= '<br/>- A data de faturamento é obrigatória';
        }
        if ($msg != '') {
            $msg = 'Não foi possível realizar a operação pois:' . $msg;
            throw new cERRO_CONSISTENCIA($msg);
        }
    }

    public function ConsistirInclusao() {
        $msg = '';
        if ($this->mnu_empresa_requerente == '') {
            $msg .= '<br/>- A empresa é obrigatória';
        }
        if ($msg != '') {
            $msg = 'Não foi possível realizar a operação pois:' . $msg;
            throw new cERRO_CONSISTENCIA($msg);
        }
    }

    public function Incluir() {
//		$sql = "select ifnull(max(resc_nu_numero), 0) resc_nu_numero from resumo_cobranca where nu_empresa_requerente = ".$this->mnu_empresa_requerente;
//		if($cursor = cAMBIENTE::$db_pdo->query($sql)){
//			$rs = $cursor->fetch(PDO::FETCH_BOTH);
//			$this->mresc_nu_numero = $rs['resc_nu_numero'] + 1;
//		}

        if ($this->mresc_nu_numero_mv != '') {
            $sql = "select resc_id from resumo_cobranca where resc_nu_numero_mv = " . $this->mresc_nu_numero_mv;
            $cursor = cAMBIENTE::$db_pdo->query($sql);
            $rs = $cursor->fetch(PDO::FETCH_BOTH);
            if ($rs['resc_id'] != '') {
                $msg = 'O número do relatório é único e não pode ser repetido. Já existe um relatório cadastrado com o número ' . $this->mresc_nu_numero_mv . ". Por favor revise.";
            }
        }
        if ($msg != '') {
            $msg = 'Não foi possível realizar a operação pois:' . $msg;
            throw new cERRO_CONSISTENCIA($msg);
        }

        $sql = "INSERT INTO resumo_cobranca (";
        $sql .= " nu_empresa_requerente";
        $sql .= ", resc_nu_numero_mv";
        $sql .= ", resc_nu_numero";
        $sql .= ", strc_id";
        $sql .= ", NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= ", resc_dt_criacao";
        $sql .= ", resc_dt_faturamento";
        $sql .= ", resc_dt_recebimento";
        $sql .= ", resc_tx_nota_fiscal";
        $sql .= ", resc_tx_solicitante";
        $sql .= ", resc_tx_observacoes";
        $sql .= ", empf_id";
        $sql .= ", nu_empresa_prestadora";
        $sql .= ") values (";
        $sql .= " " . cBANCO::ChaveOk($this->mnu_empresa_requerente);
        $sql .= ", " . cBANCO::InteiroOk($this->mresc_nu_numero_mv);
        $sql .= ", " . cBANCO::InteiroOk($this->mresc_nu_numero);
        $sql .= ", " . cBANCO::ChaveOk($this->mstrc_id);
        $sql .= ", " . cBANCO::ChaveOk($this->mNU_EMBARCACAO_PROJETO_COBRANCA);
        $sql .= ", now()";
        $sql .= ", " . cBANCO::DataOk($this->mresc_dt_faturamento);
        $sql .= ", " . cBANCO::DataOk($this->mresc_dt_recebimento);
        $sql .= ", " . cBANCO::StringOk($this->mresc_tx_nota_fiscal);
        $sql .= ", " . cBANCO::StringOk($this->mresc_tx_solicitante);
        $sql .= ", " . cBANCO::StringOk($this->mresc_tx_observacoes);
        $sql .= ", " . cBANCO::ChaveOk($this->mempf_id);
        $sql .= ", " . cBANCO::ChaveOk($this->mnu_empresa_prestadora);
        $sql .= ")";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->setid(cAMBIENTE::$db_pdo->lastInsertId());
    }

    /**
     * Adiciona todas as OS não faturadas da empresa/centro de custos ao resumo de cobranca
     */
    public function AdicioneTodasOsNaoFaturadas() {
        $sql = " update solicita_visto ";
        $sql .= " set resc_id = " . $this->mresc_id;
        $sql .= ", stco_id = 3";
        $sql .= ", soli_tx_solicitante_cobranca = coalesce(soli_tx_solicitante_cobranca, no_solicitador)";
        $sql .= " where resc_id is null ";  // Não faturadas
        $sql .= " and stco_id = 2 ";      // liberadas para faturamento
        $sql .= " and nu_empresa_requerente = " . $this->mnu_empresa_requerente;
        $sql .= " and NU_EMBARCACAO_PROJETO_COBRANCA = " . $this->mNU_EMBARCACAO_PROJETO_COBRANCA;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Adiciona uma OS não faturadas da empresa/centro de custos ao resumo de cobranca
     */
    public function AdicioneOs($pid_solicita_visto, $controller = 'cresumo_cobranca', $metodo='RemovaOs') {
        $sql = " update solicita_visto ";
        $sql .= " set resc_id = " . $this->mresc_id;
        $sql .= ", stco_id = 3";
        $sql .= ", soli_tx_solicitante_cobranca = coalesce(soli_tx_solicitante_cobranca, no_solicitador)";
        $sql .= ", cd_usuario_alteracao = ".cSESSAO::$mcd_usuario;
        $sql .= ", controller = ".cBANCO::StringOk($controller);
        $sql .= ", metodo = ".cBANCO::StringOk($metodo);
        $sql .= " where id_solicita_visto = " . $pid_solicita_visto;  // Não faturadas
        cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Remova uma OS do resumo de cobranca
     */
    public function RemovaOs($pid_solicita_visto, $controller = 'cresumo_cobranca', $metodo='RemovaOs') {
        $sql = " update solicita_visto ";
        $sql .= " set resc_id = null";
        $sql .= ", stco_id = 2";
        $sql .= ", cd_usuario_alteracao = ".cSESSAO::$mcd_usuario;
        $sql .= ", controller = ".cBANCO::StringOk($controller);
        $sql .= ", metodo = ".cBANCO::StringOk($metodo);
        $sql .= " where id_solicita_visto = " . $pid_solicita_visto;  // Não faturadas
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function Atualizar() {
        $sql = "update resumo_cobranca set ";
        $sql .= " strc_id = " . cBANCO::ChaveOk($this->mstrc_id);
        $sql .= ", resc_dt_faturamento = " . cBANCO::DataOk($this->mresc_dt_faturamento);
        $sql .= ", resc_dt_recebimento = " . cBANCO::DataOk($this->mresc_dt_recebimento);
        $sql .= ", resc_tx_nota_fiscal = " . cBANCO::StringOk($this->mresc_tx_nota_fiscal);
        $sql .= ", resc_nu_numero = " . cBANCO::InteiroOk($this->mresc_nu_numero);
        $sql .= ", resc_nu_numero_mv = " . cBANCO::InteiroOk($this->mresc_nu_numero_mv);
        $sql .= ", resc_tx_solicitante = " . cBANCO::StringOk($this->mresc_tx_solicitante);
        $sql .= ", resc_tx_observacoes = " . cBANCO::StringOk($this->mresc_tx_observacoes);
        $sql .= ", NU_EMBARCACAO_PROJETO_COBRANCA = " . cBANCO::StringOk($this->mNU_EMBARCACAO_PROJETO_COBRANCA);
        $sql .= ", empf_id = " . cBANCO::ChaveOk($this->mempf_id);
        $sql .= ", nu_empresa_prestadora = " . cBANCO::ChaveOk($this->mnu_empresa_prestadora);
        $sql .= " where resc_id = " . $this->mresc_id;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function IndiqueNotaFiscal() {
        $sql = "update resumo_cobranca set ";
        $sql .= " resc_tx_nota_fiscal = " . cBANCO::StringOk($this->mresc_tx_nota_fiscal);
        $sql .= " where resc_id = " . $this->mresc_id;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function InformeRecebimento() {
        $sql = "update resumo_cobranca set ";
        $sql .= " resc_dt_recebimento = " . cBANCO::DataOk($this->mresc_dt_recebimento);
        $sql .= " where resc_id = " . $this->mresc_id;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Retorna um recordset de resumos de cobr
     * @param int $pPag
     * @return PDOstatement
     */
    public function Listar($pFiltros = '', $pOrdenacao = '', $pPag = 1, $pOffset = 30) {
        $sql = '';
        $sql .= $this->sql_Liste();
        $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql .= " group by r.resc_id
                    , r.nu_empresa_requerente
                    , r.strc_id
                    , r.NU_EMBARCACAO_PROJETO_COBRANCA
                    , r.resc_dt_criacao
                    , r.resc_dt_faturamento
                    , r.resc_dt_recebimento
                    , r.resc_tx_nota_fiscal
                    , r.resc_nu_numero
                    , r.resc_nu_numero_mv
                    , r.resc_tx_observacoes
                    , r.resc_tx_solicitante
                    , s.resc_id
                    , ef.empf_tx_descricao
                    , NO_RAZAO_SOCIAL
                    , NO_EMBARCACAO_PROJETO
                    , ef.empf_tx_descricao ";
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        if ($pOffset > 0) {
            $sql .= " LIMIT " . (($pPag - 1) * $pOffset) . ", " . $pOffset;
        }
        if ($res = cAMBIENTE::ConectaQuery($sql, __CLASS__)) {
            return $res;
        }
    }

    /**
     * Retorna um recordset de resumos de cobr
     * @param int $pPag
     * @return PDOstatement
     */
    public function ListarQtd($pFiltros = '') {
        $sql = '';
        $sql .= "select count(*) from resumo_cobranca r where 1=1 ";
        $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        if ($res = cAMBIENTE::ConectaQuery($sql, __CLASS__)) {
            $rs = $res->fetch(PDO::FETCH_BOTH);
            return $rs[0];
        } else {
            return 0;
        }
    }

    public function ListeOs($pPag = '', $pFiltros = '', $pOrdenacao = '') {
        $sql = "";
        $sql .= " select " . $this->mresc_id . " resc_id";
        $sql .= ", s.resc_id resc_id_real";
        $sql .= ", s.nu_solicitacao";
        $sql .= ", s.id_solicita_visto";
        $sql .= ", s.dt_solicitacao";
        $sql .= ", s.soli_vl_cobrado";
        $sql .= ", s.soli_tx_codigo_servico";
        $sql .= ", s.soli_tx_solicitante_cobranca";
        $sql .= ", s.resc_id resc_id_solicitacao";
        $sql .= ", E.NO_RAZAO_SOCIAL";
        $sql .= ", E.empr_fl_fatura_em_ingles";
        $sql .= ", EP.NO_EMBARCACAO_PROJETO";
        $sql .= ", SV.NO_SERVICO_RESUMIDO";
        $sql .= ", SV.serv_tx_codigo_sg";
        $sql .= ", coalesce(prec_tx_item, co_servico) item";
        $sql .= ", stco_tx_nome ";
        $sql .= " from solicita_visto s ";
        $sql .= " left join STATUS_SOLICITACAO SS on SS.ID_STATUS_SOL = s.ID_STATUS_SOL";
        $sql .= " left join EMPRESA E on E.NU_EMPRESA = s.nu_empresa_requerente";
        $sql .= " left join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = s.nu_empresa_requerente and EP.NU_EMBARCACAO_PROJETO = s.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= " left join SERVICO SV on SV.NU_SERVICO = s.NU_SERVICO";
        $sql .= " left join preco p on p.tbpc_id = E.tbpc_id and p.nu_servico = sv.nu_servico";
        $sql .= " left join status_cobranca_os sco on sco.stco_id = s.stco_id";
        $sql .= " where resc_id = " . $this->mresc_id;
        if ($this->mresc_tx_nota_fiscal == '') {
            $sql .= " or (resc_id is null
                        and s.nu_empresa_requerente = " . $this->mnu_empresa_requerente;
            $sql .= " and s.stco_id = 2)";
        }
        $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql .= " order by s.resc_id desc";
        if ($pOrdenacao != '') {
            $sql .= ", " . $pOrdenacao;
        }
        if ($res = cAMBIENTE::ConectaQuery($sql, __CLASS__)) {
            return $res;
        }
    }

    public function CursorDemonstrativoDiario($pPag = '', $pFiltros = '', $pOrdenacao = '') {
        $colunas = " s.resc_id
                    , s.nu_solicitacao
                    , s.id_solicita_visto
                    , SV.ID_TIPO_ACOMPANHAMENTO
                    , date_format(s.dt_solicitacao, '%d/%m/%Y') dt_solicitacao
                    , s.soli_vl_cobrado
                    , s.soli_tx_codigo_servico
                    , s.resc_id resc_id_solicitacao
                    , s.no_solicitador
                    , coalesce(s.soli_tx_solicitante_cobranca, s.no_solicitador) soli_tx_solicitante_cobranca
                    , E.NO_RAZAO_SOCIAL
                    , EP.NO_EMBARCACAO_PROJETO
                    , SV.NO_SERVICO_RESUMIDO
                    , c.NOME_COMPLETO
                    , stco_tx_nome
                    , u.nome nome_responsavel
                    , date_format(rc.resc_dt_faturamento, '%d/%m/%Y') resc_dt_faturamento
                    , rc.resc_nu_numero_mv
                    , ef.empf_tx_descricao";
        $sql .= " from solicita_visto s ";
        $sql .= " join resumo_cobranca rc on rc.resc_id = s.resc_id";
        $sql .= " left join autorizacao_candidato ac on ac.id_solicita_visto = s.id_solicita_visto";
        $sql .= " left join candidato c on c.nu_candidato = ac.nu_candidato";
        $sql .= " left join STATUS_SOLICITACAO SS on SS.ID_STATUS_SOL = s.ID_STATUS_SOL";
        $sql .= " left join EMPRESA E on E.NU_EMPRESA = rc.nu_empresa_requerente";
        $sql .= " left join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = rc.nu_empresa_requerente and EP.NU_EMBARCACAO_PROJETO = rc.NU_EMBARCACAO_PROJETO_COBRANCA";
        $sql .= " left join SERVICO SV on SV.NU_SERVICO = s.NU_SERVICO";
        $sql .= " left join status_cobranca_os sco on sco.stco_id = s.stco_id";
        $sql .= " left join usuarios u on u.cd_usuario = s.cd_tecnico";
        $sql .= " left join empresa_faturamento ef on ef.empf_id = rc.empf_id";
        $sql .= " left join empresa_prestadora p on p.nu_empresa_prestadora = rc.nu_empresa_prestadora";
        $sql .= " where 1 = 1 ";
        $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros

        $xsql = "select count(*) " . $sql;
        $res = cAMBIENTE::ConectaQuery($xsql, __CLASS__);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        if ($rs[0] > 1000) {
            cHTTP::$SESSION['msg'] = 'Mais de 1.000 linhas selecionadas. Por favor faça uma consulta mais específica.';
        } else {
            if ($pOrdenacao != '') {
                $sql .= " order by " . $pOrdenacao;
            }
            $xsql = "select " . $colunas . $sql;
            if ($res = cAMBIENTE::ConectaQuery($xsql, __CLASS__)) {
                return $res;
            }
        }
    }

    public function Excluir() {
        $sql = "update solicita_visto set resc_id = null, stco_id = 2 where resc_id = " . $this->mresc_id;
        cAMBIENTE::$db_pdo->exec($sql);
        $sql = "delete from resumo_cobranca where resc_id = " . $this->mresc_id;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public static function get_proximoNumero() {
        $sql = "select ifnull(max(resc_nu_numero_mv), 0) from resumo_cobranca";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        return $rs[0] + 1;
    }

    public function SqlLog($pClasse, $pMetodo) {
        return "";
    }

    public function SqlAuditagem($pcd_usuario) {
        $sql = ", resc_dt_alteracao = now()";
        $sql .= ", cd_usuario_alteracao = " . cBANCO::ChaveOk($pcd_usuario);
        return $sql;
    }

    public function JSON_OsParaRelatorio(&$total_cand, &$total_geral_fmt) {
        $ret = array();
        $sql = "select c.NU_CANDIDATO, NOME_COMPLETO, NO_SERVICO_RESUMIDO
                    , NO_SERVICO_RESUMIDO as soli_tx_descricao_servico
                    , soli_vl_cobrado, coalesce(prec_tx_item, co_servico) item
                    from solicita_visto sv 
                    join servico s on s.nu_servico = sv.nu_servico
                    join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto
                    join candidato c on c.NU_CANDIDATO = ac.NU_CANDIDATO
                    join empresa e on e.nu_empresa = sv.nu_empresa
                    join preco p on p.tbpc_id = e.tbpc_id and p.nu_servico = sv.nu_servico
                    where sv.resc_id = " . $this->mresc_id . "
                    order by c.NOME_COMPLETO, sv.dt_solicitacao";
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        $nu_candidato_ant = "";
        $qtd_cand = 0;
        $total_geral = 0;
        while ($rs = $res->fetch(PDO::FETCH_BOTH)) {
            $rs['NOME_COMPLETO'] = htmlentities($rs['NOME_COMPLETO']);
            $rs['NO_SERVICO_RESUMIDO'] = htmlentities($rs['NO_SERVICO_RESUMIDO']);
            $rs['soli_tx_descricao_servico'] = htmlentities($rs['soli_tx_descricao_servico']);
            if ($nu_candidato_ant != $rs['NU_CANDIDATO']) {
                $qtd_cand++;
                if ($nu_candidato_ant != '') {
                    $cand['solicitacoes'] = $solicitacoes;
                    $cand['subtotal_fmt'] = number_format($subtotal, 2, ", ", ".");
                    $ret[] = $cand;
                }
// Salva candidato anterior
                $nu_candidato_ant = $rs['NU_CANDIDATO'];
                $cand = array();
                $cand = $rs;
                $subtotal = 0;
                $solicitacoes = array();
            }
            $rs['soli_vl_cobrado_fmt'] = number_format($rs['soli_vl_cobrado'], 2, ", ", ".");
            $subtotal += $rs['soli_vl_cobrado'];
            $solicitacoes[] = $rs;
            $total_geral += $rs['soli_vl_cobrado'];
        }
        if ($nu_candidato_ant != $rs['NU_CANDIDATO']) {
            $cand['solicitacoes'] = $solicitacoes;
            $cand['subtotal_fmt'] = number_format($subtotal, 2, ", ", ".");
            $ret[] = $cand;
        }
        $total_cand = $qtd_cand;
        $total_geral_fmt = number_format($total_geral, 2, ", ", ".");
        return $ret;
    }

    public static function AtualizeCampoPadrao($pid, $pnome_campo, $pvalor, $ptipo) {
        if ($pnome_campo != 'resc_nu_numero_mv') {
            parent::AtualizeCampoPadrao($pid, $pnome_campo, $pvalor, $ptipo);
        } else {
            if ($pvalor != '') {
                $sql = "select resc_id, no_razao_social
                        from resumo_cobranca
                        left join empresa e on e.nu_empresa = resumo_cobranca.nu_empresa_requerente
                        where resc_nu_numero_mv = " . $pvalor . "
                        and resc_id <> " . $pid;
                $cursor = cAMBIENTE::$db_pdo->query($sql);
                $rs = $cursor->fetch(PDO::FETCH_BOTH);
                if ($rs['resc_id'] != '') {
                    $msg = 'O número do relatório é único e não pode ser repetido. Já existe um resumo cadastrado com o número ' . $pvalor . " para a empresa " . $rs['no_razao_social'] . ". Por favor revise.";
//					$msg = 'O n&uacute;mero do relat&oacute;rio &eacute; &uacute;nico e n&atilde;o pode ser repetido. J&aacute; existe um resumo cadastrado com o n&uacute;mero '.$pvalor." para a empresa ".$rs['no_razao_social'].". Por favor revise.";
//					$msg = "O n&uacute;mero do relat&oacute;rio &eacute;&uacute;nico e n&atilde;o pode ser repetido. J&aacute;existe um resumo cadastrado com o n&uacute;mero ".$pvalor." para a empresa Por favor revise.";

                    throw new cERRO_CONSISTENCIA($msg);
                } else {
                    parent::AtualizeCampoPadrao($pid, $pnome_campo, $pvalor, $ptipo);
                }
            }
        }
    }

    public static function FabriquePeloNumero($presc_nu_numero, $pnu_empresa) {
        if (!is_numeric(trim($presc_nu_numero))) {
            throw new cERRO_CONSISTENCIA('Número do resumo inválido');
        } else {
            $resumo = new cresumo_cobranca();
            $sql = "select resc_id, nu_empresa_requerente
                    from resumo_cobranca
                    where resc_nu_numero_mv = " . $presc_nu_numero;
            $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
            $rs = $res->fetch(PDO::FETCH_BOTH);
            if ($rs['resc_id'] != '') {
                if ($rs['nu_empresa_requerente'] == $pnu_empresa) {
                    $resumo->RecuperePeloId($rs['resc_id']);
                    return $resumo;
                } else {
                    $empresa = new cEMPRESA();
                    $empresa->RecuperePeloId($pnu_empresa);
                    throw new cERRO_CONSISTENCIA('O resumo ' . $presc_nu_numero . ' é da empresa ' . $empresa->mNO_RAZAO_SOCIAL);
                }
            } else {
                $resumo->mnu_empresa_requerente = $pnu_empresa;
                $resumo->mresc_nu_numero_mv = $presc_nu_numero;
                $resumo->mresc_tx_observacoes = 'Resumo criado pela auditoria do faturamento em ' . date('d/m/Y');
                $resumo->Salvar();
                return $resumo;
            }
        }
    }

}
