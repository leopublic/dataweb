<?php

class ctaxapaga extends cMODELO {

    public $mid_taxapaga;
    public $mid_taxa;
    public $mid_solicita_visto;
    public $mnu_candidato;
    public $mdata_taxa;
    public $mvalor;
    public $mid_usuario;

    public static function get_nomeCampoChave() {
        return 'id_taxapaga';
    }

    public static function get_nomeTabelaBD() {
        return 'taxapaga';
    }

    public function get_nomeTabela() {
        return self::get_nomeTabelaBD();
    }

    public function campoid() {
        return self::get_nomeCampoChave();
    }

    public function setid($pid) {
        $this->mid_taxapaga = $pid;
    }

    public function getid() {
        return $this->mid_taxapaga;
    }

    public function sql_Liste() {
        $sql = "    select tp.*, t.descricao, coalesce(tp.valor, t.valorfixo) valor_taxa
			           from taxapaga tp";
        $sql .= "    join taxa t on t.id_taxa = tp.id_taxa"
                . "  where 1=1";
        return $sql;
    }

    public function Incluir() {
        if ($this->mid_usuario == ''){
            $this->mid_usuario = cSESSAO::$mcd_usuario;
        }

        $sql = " insert into taxapaga(
					id_taxa
					,id_solicita_visto
					,nu_candidato
					,data_taxa
					,valor
					,id_usuario
				) values (
					" . cBANCO::ChaveOk($this->mid_taxa) . "
					," . cBANCO::ChaveOk($this->mid_solicita_visto) . "
					," . cBANCO::ChaveOk($this->mnu_candidato) . "
					," . cBANCO::DataOk($this->mdata_taxa) . "
					," . cBANCO::ValorOk($this->mvalor) . "
					," . cBANCO::ChaveOk($this->mid_usuario) . "
					)";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mid_taxapaga = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function Atualizar() {
        $sql = "update taxapaga set
					data_taxa				= " . cBANCO::DataOk($this->mdata_taxa) . "
					,valor	= " . cBANCO::ValorOk($this->mvalor) . "
				where id_taxapaga = " . $this->mid_taxapaga;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_RecuperePeloId() {
        $sql = "    select *
					   from taxapaga
					  where id_taxapaga = " . $this->mid_taxapaga;
        return $sql;
    }

    public function Excluir() {
        $sql = "delete from taxapaga where id_taxapaga = " . $this->mid_taxapaga;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function Criar(){
        if ($this->mvalor == ''){
            $taxa = new ctaxa();
            $taxa->id_taxa = $this->id_taxa;
            $taxa->RecuperePeloId();
            if ($taxa->mvalor_fixo > 0){
                $this->mvalor = number_format($taxa->mvalor_fixo, 2, ",", ".");
            } else {
                $this->mvalor = 0;
            }
        }
        if ($this->mdata_taxa == ''){
            $this->mdata_taxa = date('d/m/Y');
        }
        $this->Salvar();

    }
}
