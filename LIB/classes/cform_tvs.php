<?php

class cform_tvs extends cMODELO{

	public $ftvs_id;

	public $entt_id;

	public $past_id;

	public $accc_id;

	public $trrs_id;

	public $nu_candidato;

	public $id_solicita_visto;

	public $ftvs_tx_job_title;

	public $ftvs_tx_employee_id;

	public $ftvs_fl_us_citizen;

	public $ftvs_tx_us_status;

	public $ftvs_fl_offshore;

	public $ftvs_tx_visatype;

	public $ftvs_tx_focalpoint;

	public $ftvs_tx_focalpoint_tel;

	public $ftvs_tx_focalpoint_cel;

	public $ftvs_tx_focalpoint_email;

	public $ftvs_tx_foreign_employer;

	public $ftvs_tx_foreign_address;

	public $ftvs_tx_foreign_tel;

	public $ftvs_tx_brazil_employer;

	public $ftvs_tx_brazil_address;

	public $ftvs_tx_brazil_tel;

	public $ftvs_fl_request;

	public $ftvs_tx_countries;

	public $ftvs_dt_travel;

	public $ftvs_dt_passport_needed;

	public $cd_usuario_ult_atu;

	public $ftvs_dt_ult_atu;

	public $ftvs_tx_other_nationalities;

	public $ftvs_fl_fedex;

	public $ftvs_tx_return_address;

	public $ftvs_tx_return_city;

	public $ftvs_tx_return_country;

	public $ftvs_tx_return_zip;

	public $ftvs_tx_other_request;



	public function campoid(){

		return "ftvs_id";

	}

	public function setid($pid){

		$this->ftvs_id = $pid;

	}

	

	public function getid(){

		return $this->ftvs_id;

	}

	

	public function sql_Liste(){	

		$sql = "select * from form_tvs where 1=1 ";

		return $sql;

	}

	

	public function sql_RecuperePeloId(){

		$sql = "select * from form_tvs where ftvs_id = ".$this->ftvs_id;

		return $sql;

	}

	

	public function Incluir(){

		$sql = "INSERT INTO form_tvs (";

		$sql .= "	 entt_id";

		$sql .= "	,past_id";

		$sql .= "	,accc_id";

		$sql .= "	,trrs_id";

		$sql .= "	,nu_candidato";

		$sql .= "	,id_solicita_visto";

		$sql .= "	,ftvs_tx_job_title";

		$sql .= "	,ftvs_tx_employee_id";

		$sql .= "	,ftvs_fl_us_citizen";

		$sql .= "	,ftvs_tx_us_status";

		$sql .= "	,ftvs_fl_offshore";

		$sql .= "	,ftvs_tx_visatype";

		$sql .= "	,ftvs_tx_focalpoint";

		$sql .= "	,ftvs_tx_focalpoint_tel";

		$sql .= "	,ftvs_tx_focalpoint_cel";

		$sql .= "	,ftvs_tx_focalpoint_email";

		$sql .= "	,ftvs_tx_foreign_employer";

		$sql .= "	,ftvs_tx_foreign_address";

		$sql .= "	,ftvs_tx_foreign_tel";

		$sql .= "	,ftvs_tx_brazil_employer";

		$sql .= "	,ftvs_tx_brazil_address";

		$sql .= "	,ftvs_tx_brazil_tel";

		$sql .= "	,ftvs_fl_request";

		$sql .= "	,ftvs_tx_countries";

		$sql .= "	,ftvs_dt_travel";

		$sql .= "	,ftvs_dt_passport_needed";

		$sql .= "	,cd_usuario_ult_atu";

		$sql .= "	,ftvs_dt_ult_atu";

		$sql .= "	,ftvs_tx_other_nationalities";

		$sql .= "	,ftvs_tx_other_request";

		$sql .= "	,ftvs_fl_fedex";

		$sql .= "	,ftvs_tx_return_address";

		$sql .= "	,ftvs_tx_return_city";

		$sql .= "	,ftvs_tx_return_country";

		$sql .= "	,ftvs_tx_return_zip";

		$sql .= ") values (";

		$sql .= "	 ".cBANCO::ChaveOk($this->entt_id);

		$sql .= "	,".cBANCO::ChaveOk($this->past_id);

		$sql .= "	,".cBANCO::ChaveOk($this->accc_id);

		$sql .= "	,".cBANCO::ChaveOk($this->trrs_id);

		$sql .= "	,".cBANCO::ChaveOk($this->nu_candidato);

		$sql .= "	,".cBANCO::ChaveOk($this->id_solicita_visto);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_job_title);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_employee_id);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_fl_us_citizen);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_us_status);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_fl_offshore);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_visatype);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_focalpoint);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_focalpoint_tel);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_focalpoint_cel);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_focalpoint_email);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_foreign_employer);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_foreign_address);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_foreign_tel);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_brazil_employer);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_brazil_address);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_brazil_tel);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_fl_request);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_countries);

		$sql .= "	,".cBANCO::DataOk($this->ftvs_dt_travel);

		$sql .= "	,".cBANCO::DataOk($this->ftvs_dt_passport_needed);

		$sql .= "	,".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);

		$sql .= "	, now()";

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_other_nationalities);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_other_request);

		$sql .= "	,".cBANCO::SimNaoOk($this->ftvs_fl_fedex);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_return_address);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_return_city);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_return_country);

		$sql .= "	,".cBANCO::StringOk($this->ftvs_tx_return_zip);

		$sql .= ")";

		cAMBIENTE::$db_pdo->exec($sql);

		$this->setid(cAMBIENTE::$db_pdo->lastInsertId());

	}



	public function Atualizar(){

		$sql = "update form_tvs set ";

		$sql .= "   entt_id			= ".cBANCO::InteiroOk($this->entt_id);

		$sql .= "  ,past_id			= ".cBANCO::InteiroOk($this->past_id);

		$sql .= "  ,accc_id			= ".cBANCO::InteiroOk($this->accc_id);

		$sql .= "  ,trrs_id			= ".cBANCO::InteiroOk($this->trrs_id);

		$sql .= "  ,ftvs_tx_job_title			= ".cBANCO::StringOk($this->ftvs_tx_job_title);

		$sql .= "  ,ftvs_tx_employee_id			= ".cBANCO::StringOk($this->ftvs_tx_employee_id);

		$sql .= "  ,ftvs_fl_us_citizen			= ".cBANCO::StringOk($this->ftvs_fl_us_citizen);

		$sql .= "  ,ftvs_tx_us_status			= ".cBANCO::StringOk($this->ftvs_tx_us_status);

		$sql .= "  ,ftvs_fl_offshore			= ".cBANCO::StringOk($this->ftvs_fl_offshore);

		$sql .= "  ,ftvs_tx_visatype			= ".cBANCO::StringOk($this->ftvs_tx_visatype);

		$sql .= "  ,ftvs_tx_focalpoint			= ".cBANCO::StringOk($this->ftvs_tx_focalpoint);

		$sql .= "  ,ftvs_tx_focalpoint_tel			= ".cBANCO::StringOk($this->ftvs_tx_focalpoint_tel);

		$sql .= "  ,ftvs_tx_focalpoint_cel			= ".cBANCO::StringOk($this->ftvs_tx_focalpoint_cel);

		$sql .= "  ,ftvs_tx_focalpoint_email			= ".cBANCO::StringOk($this->ftvs_tx_focalpoint_email);

		$sql .= "  ,ftvs_tx_foreign_employer			= ".cBANCO::StringOk($this->ftvs_tx_foreign_employer);

		$sql .= "  ,ftvs_tx_foreign_address			= ".cBANCO::StringOk($this->ftvs_tx_foreign_address);

		$sql .= "  ,ftvs_tx_foreign_tel			= ".cBANCO::StringOk($this->ftvs_tx_foreign_tel);

		$sql .= "  ,ftvs_tx_brazil_employer			= ".cBANCO::StringOk($this->ftvs_tx_brazil_employer);

		$sql .= "  ,ftvs_tx_brazil_address			= ".cBANCO::StringOk($this->ftvs_tx_brazil_address);

		$sql .= "  ,ftvs_tx_brazil_tel			= ".cBANCO::StringOk($this->ftvs_tx_brazil_tel);

		$sql .= "  ,ftvs_fl_request			= ".cBANCO::StringOk($this->ftvs_fl_request);

		$sql .= "  ,ftvs_tx_countries			= ".cBANCO::StringOk($this->ftvs_tx_countries);

		$sql .= "  ,ftvs_dt_travel			= ".cBANCO::DataOk($this->ftvs_dt_travel);

		$sql .= "  ,ftvs_dt_passport_needed			= ".cBANCO::DataOk($this->ftvs_dt_passport_needed);

		$sql .= "  ,cd_usuario_ult_atu			= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);

		$sql .= "  ,ftvs_dt_ult_atu			= now()";

		$sql .= "	,ftvs_tx_other_request = ".cBANCO::StringOk($this->ftvs_tx_other_request);

		$sql .= "	,ftvs_tx_other_nationalities = ".cBANCO::StringOk($this->ftvs_tx_other_nationalities);

		$sql .= "	,ftvs_fl_fedex = ".cBANCO::SimNaoOk($this->ftvs_fl_fedex);

		$sql .= "	,ftvs_tx_return_address = ".cBANCO::StringOk($this->ftvs_tx_return_address);

		$sql .= "	,ftvs_tx_return_city = ".cBANCO::StringOk($this->ftvs_tx_return_city);

		$sql .= "	,ftvs_tx_return_country = ".cBANCO::StringOk($this->ftvs_tx_return_country);

		$sql .= "	,ftvs_tx_return_zip = ".cBANCO::StringOk($this->ftvs_tx_return_zip);

		$sql .= " where ftvs_id = ".$this->ftvs_id;

		cAMBIENTE::$db_pdo->exec($sql);

	}



	public function RecuperePeloCandidatoOs($pNU_CANDIDATO, $pid_solicita_visto){

		$sql = " select * ";

		$sql.= "   from CANDIDATO C ";

		$sql.= "   left join form_tvs f on f.NU_CANDIDATO = C.NU_CANDIDATO";

		$sql.= "   left join solicita_visto sv on sv.id_solicita_visto = f.id_solicita_visto and sv.id_solicita_visto=".$pid_solicita_visto;

		$sql.= "  where C.NU_CANDIDATO = ".$pNU_CANDIDATO;

		if($res = cAMBIENTE::$db_pdo->query($sql)){

			$rs = $res->fetch(PDO::FETCH_BOTH);

			cBANCO::CarreguePropriedades($rs, $this, "");

			if ($rs['ftvs_id'] == '' ){

				$this->nu_candidato = $pNU_CANDIDATO;

				$this->id_solicita_visto = $pid_solicita_visto;

				$this->InicializaPeloCandidato($pNU_CANDIDATO);

				$this->InicializaPeloFocalPointDaOS($pid_solicita_visto);

			}

			return $rs;

		}

		else{

			throw new exception("Não foi possível recuperar os dados.");

		}

	}



	public function RecuperePeloId($pftvs_id = '' ){

		if ($pftvs_id != '' ){

			$this->ftvs_id = $pftvs_id;

		}

		if ($this->ftvs_id != '' ){

			$sql = " select * ";

			$sql.= "   from form_tvs f ";

			$sql.= "   left join CANDIDATO C on C.NU_CANDIDATO = f.NU_CANDIDATO";

			$sql.= "   left join solicita_visto sv on sv.id_solicita_visto = f.id_solicita_visto";

			$sql.= "   left join PAIS_NACIONALIDADE p on p.CO_PAIS = C.CO_NACIONALIDADE";

			$sql.= "  where f.ftvs_id = ".$this->ftvs_id;

			if($res = cAMBIENTE::$db_pdo->query($sql)){

				$rs = $res->fetch(PDO::FETCH_BOTH);

				cBANCO::CarreguePropriedades($rs, $this, "");

				return $rs;

			}

			else{

				throw new exception("Não foi possível recuperar os dados.");

			}

		}

		else{

			throw new exception ("Id não informado");

		}

	}

	

	public function InicializaPeloCandidato($pNU_CANDIDATO){

		$cand = new cCANDIDATO();

		$cand->Recuperar($pNU_CANDIDATO);

		$this->ftvs_tx_foreign_employer = $cand->mNO_EMPRESA_ESTRANGEIRA;

		$this->ftvs_tx_foreign_address = $cand->mNO_ENDERECO_EMPRESA;

		$this->ftvs_tx_foreign_tel = $cand->mNU_TELEFONE_EMPRESA;

		$this->ftvs_tx_brazil_employer =  $cand->mNO_EMPRESA_ESTRANGEIRA;

		$this->ftvs_tx_brazil_address =  $cand->mNO_ENDERECO_RESIDENCIA;

		$this->ftvs_tx_brazil_tel =  $cand->mNU_TELEFONE_CANDIDATO;

	}



	public function InicializaPeloFocalPointDaOS($pID_SOLICITA_VISTO){

		$usuario = new cusuarios();

		$usuario->RecupereResponsavelOs($pID_SOLICITA_VISTO);

		$this->ftvs_tx_focalpoint = $usuario->nome;

		$this->ftvs_tx_focalpoint_email = $usuario->nm_email;

	}

}
