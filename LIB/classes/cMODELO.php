<?php

class cMODELO {

    protected $nomeTabela;

    public static function get_nomeCampoChave() {
        throw new cERRO_CONSISTENCIA("Nome do campo chave não definido para essa entidade");
    }

    public static function get_nomeTabelaBD() {
        throw new cERRO_CONSISTENCIA("Nome da tabela não definido para essa entidade");
    }

    public static function get_nomeCampoDescricao() {
        throw new cERRO_CONSISTENCIA("Nome do campo chave não definido para essa entidade");
    }

    public function setId($pValor) {
        
    }

    public function getId() {
        
    }

    public function sql_Liste() {
        
    }

    public function sql_RecuperePeloId() {
        
    }

    public function msg_InstanciaNaoEncontrada() {
        return "Não foi possível encontrar o registro informado";
    }

    public function msg_IdNaoInformado() {
        return "Id não informado";
    }

    public function get_nomeTabela() {
        throw new cERRO_CONSISTENCIA("Nome da tabela não definido para essa entidade");
    }

    public function __get($pnome) {
        if (property_exists($this, $pnome)) {
            return $this->$pnome;
        } elseif (property_exists($this, 'm' . $pnome)) {
            $pnome = 'm' . $pnome;
            return $this->$pnome;
        } else {
            throw new Exception("Propriedade " . $pnome . " não encontrada na classe " . get_class($this));
        }
    }

    public function __set($pnome, $value) {
        if (property_exists($this, $pnome)) {
            $this->$pnome = $value;
        } elseif (property_exists($this, 'm' . $pnome)) {
            $pnome = 'm' . $pnome;
            $this->$pnome = $value;
        } else {
            throw new Exception("Propriedade " . $pnome . " não encontrada na classe " . get_class($this));
        }
    }

    public function __isset($pnome) {
        if (property_exists($this, $pnome)) {
            return true;
        } else {
            $pnome = 'm' . $pnome;
            if (property_exists($this, $pnome)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function ConsisteTextoObrigatorio($pCampo, $pMsg, $pQuebra = '<br/>') {
        if (trim($pCampo) == '') {
            return $pQuebra . $pMsg;
        }
    }

    public function ConsisteChaveEstrObrigatoria($pCampo, $pMsg, $pQuebra = '<br/>') {
        if (intval($pCampo) == 0) {
            return $pQuebra . $pMsg;
        }
    }

    public function ConsistirInclusao() {
        return $this->ConsistirSempre();
    }

    public function ConsistirAtualizacao() {
        return $this->ConsistirSempre();
    }

    public function ConsistirSempre() {
        
    }

    public function Atualizar() {
        
    }

    public function Incluir() {
        
    }

    public function Salvar() {
        if (intval($this->getid()) == 0) {
            $msg = $this->ConsistirInclusao();
            if ($msg != '') {
                throw new Exception('N&atilde;o foi poss&iacute;vel incluir pois:' . $msg);
            }
            $this->Incluir();
        } else {
            $msg = $this->ConsistirAtualizacao();
            if ($msg != '') {
                throw new Exception('N&atilde;o foi poss&iacute;vel incluir pois:' . $msg);
            }
            $this->Atualizar();
        }
    }

    /**
     * Retorna um recordset de usuarios
     * @param int $pPag
     * @return PDOstatement
     */
    public function Liste($pPag = '', $pFiltros = '', $pOrdenacao = '') {
        $sql = '';
        $sql .= $this->sql_Liste();
        $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        if ($res = conectaQuery($sql, __CLASS__)) {
            return $res;
        }
    }

    /**
     * Retorna um recordset de usuarios
     * @param int $pPag
     * @return PDOstatement
     */
    public function Listar($pFiltros = '', $pOrdenacao = '') {
        $sql = '';
        $sql .= $this->sql_Liste();
        $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        if ($res = cAMBIENTE::ConectaQuery($sql, __CLASS__)) {
            return $res;
        }
    }

    public static function StrBanco($pValor) {
        return cBANCO::StringOk($pValor);
    }

    public static function ChvBanco($pValor) {
        return cBANCO::ChaveOk($pValor);
    }

    public function RecuperePeloId($pid = '') {
        if ($pid != '') {
            $this->setid($pid);
        }
        if ($this->getid() == '') {
            throw new cERRO_CONSISTENCIA("Id n&atilde;o informado" );
        }
        $sql = $this->sql_RecuperePeloId();
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetch(PDO::FETCH_BOTH);
            cBANCO::CarreguePropriedades($rs, $this);
        } else {
            throw new Exception("N&atilde;o foi poss&iacute;vel obter o registro", $sql);
        }
    }

    /**
     * ENTIDADE_AUDITAVEL
     * @return string
     */
    public function getCampoUsuarioAlteracao() {
        return '';
    }

    /**
     * ENTIDADE_AUDITAVEL
     * @return string
     */
    public function SqlLog($pClass, $pFunction) {
        $sql = ", log_tx_controller = " . cBANCO::StringOk($pClass);
        $sql.= ", log_tx_metodo     = " . cBANCO::StringOk($pFunction);
        $campo_cd_usuario = $this->getCampoUsuarioAlteracao();
        if ($campo_cd_usuario != '') {
            $sql .= ", " . $campo_cd_usuario . " = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        }
        return $sql;
    }

    public function SqlAuditagem($pcd_usuario) {
        return "";
    }

    /**
     * Gera o SQL para atualizacao de um atributo (para o EDIT IN PLACE)
     * @param  string $pAtributo Nome do atributo sendo atualizado
     * @param  string $pValor    Novo valor do atributo
     * @param  string $pChave    Array com pares campo/valor da chave em formato JSON
     */
    public function AtualizeAtributo($pAtributo, $pValor, $pTipo, $pChave, $pcd_usuario) {
        $sql = $this->sqlAtualizaAtributo($pAtributo, $pValor, $pTipo, $pChave, $pcd_usuario);
        cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Gera o SQL para atualizacao de um atributo (para o EDIT IN PLACE)
     * @param  string $pAtributo Nome do atributo sendo atualizado
     * @param  string $pValor    Novo valor do atributo
     * @param  string $pChave    Array com pares campo/valor da chave em formato JSON
     * @return string            SQL que faz a atualizacao
     */
    public function sqlAtualizaAtributo($pAtributo, $pValor, $pTipo, $pChave, $pcd_usuario) {
        $where = '';
        $jsonArray = json_decode($pChave);
        $and = '';
        foreach ($jsonArray as $jsonChave) {
            $where .= $and . $jsonChave->{"campo"} . " = " . $jsonChave->{"valor"};
            $and = ' and ';
        }
        $campo = new cCAMPO("", "", $pTipo);
        $campo->mValor = $pValor;
        $valor = cBANCO::valorParaBanco($campo);

        $sql = "update " . $this->get_nomeTabela();
        $sql .= " set " . $pAtributo . " = " . $valor;
        $sql .= $this->SqlLog(__CLASS__, "AtualizarAtributo_" . $pAtributo);
        $sql .= $this->SqlAuditagem($pcd_usuario);
        $sql .= " where " . $where;
        return $sql;
    }

    public function CarreguePropriedadesPeloRecordset($prs, $prefixo = "") {
        foreach ($prs as $campo => $valor) {
            if (property_exists($this, strtolower($campo))) {                
                $this->$campo = $valor;
            } elseif (property_exists($this, $prefixo.$campo)) {
                $nome = $prefixo.$campo;
                $this->$nome = $valor;
            }
        }
    }

    public function CarreguePropriedadesDoPost($post, $prefixo = "m") {
        foreach ($post as $campo_tela => $valor) {
            $campo = substr($campo_tela, 4 );
            $campo_pref = $prefixo.$campo;
            if (property_exists($this, strtolower($campo))) {
                $this->$campo = $valor;
            } elseif (property_exists($this, $prefixo.$campo)) {
                $this->$campo_pref = $valor;
            }
        }
    }

    public static function AtualizeCampoPadrao($pid, $pnome_campo, $pvalor, $ptipo) {
        $sql = "update " . static::get_nomeTabela() . " set ";
        switch ($ptipo) {
            case cCAMPO::cpCHAVE_ESTR:
            case cCAMPO::cpCOMBO_GERAL:
                $sql .= $pnome_campo . " = " . cBANCO::ChaveOk($pvalor);
                break;
            case cCAMPO::cpTEXTO:
            case cCAMPO::cpCODIGO:
            case cCAMPO::cpMEMO:
                $sql .= $pnome_campo . " = " . cBANCO::StringOk($pvalor);
                break;
            case cCAMPO::cpDATA:
            case cCAMPO::cpDATAHORA:
                $sql .= $pnome_campo . " = " . cBANCO::DataOk($pvalor);
                break;
            case cCAMPO::cpSIMNAO:
                $sql .= $pnome_campo . " = " . cBANCO::SimNaoOk($pvalor);
                break;
            case cCAMPO::cpVALOR:
            case cCAMPO::cpVALOR_DIN_DOLAR:
            case cCAMPO::cpVALOR_DIN_REAL:
                $sql .= $pnome_campo . " = " . cBANCO::ValorOk($pvalor);
                break;
            default:
                throw new cERRO_CONSISTENCIA("Atualização para o tipo de campo informado (" . $ptipo . ") não prevista.");
        }
        $sql .= " where " . static::campoid() . " = " . $pid;
        cAMBIENTE::$db_pdo->exec($sql);
    }


    public static function combo($exibeDefault = true, $default = '(não informado)', $campoChave = '', $campoDescricao='', $filtro ='' ){
        if ($campoChave == ''){
            $campoChave = static::get_nomeCampoChave();
        }
        if ($campoDescricao == ''){
            $campoDescricao = static::get_nomeCampoDescricao();
        }
        $sql = "select ".$campoChave." chave, ".$campoDescricao." descricao from ".static::get_nomeTabelaBD()." ".$filtro." order by ".$campoDescricao;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        if ($exibeDefault){
            array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));
        }
        return $rs;

    }


    public function getRelacionamento($nomeClasse, $campo, $nomeEntidade){
        $entidade = $this->$nomeEntidade;
        if (!is_object($entidade)) {
            $chave = $this->$campo;
            if ($chave > 0) {
                $entidade = new $nomeClasse;
                $entidade->RecuperePeloId($chave);
                $this->$nomeEntidade = $entidade;
            }
            else {
                $this->$nomeEntidade = null;
            }
        }
        return $this->$nomeEntidade;
    }

}
