<?php

    /**
     * @author Fabio - M2 Software
     * @copyright 2010
     */

    Class AcessoHtml extends Acesso{
        
        
        function Os_Aba_Cadastro_Dependentes($id, $nu_candidato, $id_visto, $action =null){
            if(Acesso::permitido(Acesso::tp_Os_Aba_Cadastro_Dependentes)){
                
                if(!$action){
                    $action ='true';
                }
                
                $script = " <script>
                $(document).ready(function() {
                    var nu_candidato = '&NU_CANDIDATO=".$nu_candidato."';
                    var id_visto = '".$id_visto."';
                     $('#".$id."').Grid({
                        request        : '../ajax/dependente.php',
                        title          : 'Dependente',
                        qs_load_list   : '?action=list'+nu_candidato,
                        qs_add         : '?action=add'+nu_candidato,
                        qs_edit        : '?action=edit'+nu_candidato,
                        qs_delete      : '?action=remove'+nu_candidato,
                        qs_detail      : '?action=detailedit'+nu_candidato,
                        qs_refresh_row : '?action=refreshrow'+nu_candidato,
                        msgCfmDelete   : 'Tem certeza que deseja excluir?',
                        bodyHeight     : '100px',
                        BoxEditWidth   : '1000px',
                        BoxAddWidth    : '1000px',
                        actions        : ".$action."
                     });
                }); </script>"; 
         
                $TDependentesHtml = $script;               
                $TDependentesHtml .='
                    <table id="'.$id.'">
                    <thead>
                        <tr colspan="3">
                            <th>Nome</th>
                            <th>Grau de parentesco</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>';
                
                if(Acesso::permitido(Acesso::tp_Os_Aba_Cadastro_Dependentes_Cadastrar)){
                    $TDependentesHtml .= '
                    <tfoot>
                      <tr>
                         <th colspan="3" style="text-align:right">
                          <button id="add"> <img src="/imagens/icons/add.png"> Adicionar Dependente </button>
                         </th>
                      </tr>
                    </tfoot>';
                }
                
                $TDependentesHtml .='</table>';
            
            }else{
                $TDependentesHtml = '';
            }
            return $TDependentesHtml;
        } 
    }
