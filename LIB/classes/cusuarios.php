<?php

/**
 * @property int $cd_usuario ID
 * @property int $cd_perfil ID do perfil
 * @property int $cd_empresa ID da empresa
 * @property string $login Login
 * @property string $nm_senha Senha
 * @property string $nome Nome do usuario
 * @property mixed $nm_email Email
 * @property int $cd_superior ID do superiod
 * @property date$dt_cadastro Data do cadastramento
 * @property date $dt_ult Data da última atualização
 * @property mixed $flagativo Indica se está ativo
 */
class cusuarios extends cMODELO {

    protected $cd_usuario;
    protected $cd_perfil;
    protected $cd_empresa;
    protected $login;
    protected $nm_senha;
    protected $nome;
    protected $nm_email;
    protected $cd_superior;
    protected $dt_cadastro;
    protected $dt_ult;
    protected $flagativo;
    protected $usua_tx_apelido;
    protected $usua_qtd_acesso_negado;
    protected $hash_string;
    protected $hash_dt_cadastro;

    public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function getid() {
        return $this->cd_usuario;
    }

    public function setid($pValor) {
        $this->cd_usuario = $pValor;
    }

    public function get_flagativo() {
        switch ($this->flagativo) {
            case '':
            case '0':
            case 0:
                return 'N';
                break;
            case 1:
            case '1':
            case 'Y':
                return 'Y';
                break;
            default:
                return 'N';
                break;
        }
    }

    public function ConsistirSempre() {

        $msg = '';
        if (trim($this->nome) == '') {
            $msg .= '<br/>- o nome é obrigatório';
        }
        if (trim($this->login) == '') {
            $msg .= '<br/>- o login é obrigatório';
        }
//		if(trim($this->cd_perfil) == ''){
//			$msg .= '<br/>- o perfil é obrigatório';
//		}
        if ($msg == '') {
            $sql = "select * from usuarios where nome = " . cBANCO::StringOk($this->nome) . " and cd_usuario <>" . intval($this->cd_usuario);
            $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
            $rs = $res->fetch(PDO::FETCH_BOTH);
            if ($rs['cd_usuario'] != '') {
                $msg .= '<br/>- já existe outro usuário cadastrado com esse nome.';
            }
            $sql = "select * from usuarios where login= " . cBANCO::StringOk($this->login) . " and cd_usuario <>" . intval($this->cd_usuario);
            $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
            $rs = $res->fetch(PDO::FETCH_BOTH);
            if ($rs['cd_usuario'] != '') {
                $msg .= '<br/>- já existe outro usuário cadastrado com esse login.';
            }
        }
        if ($msg != '') {
            $msg = 'Não foi possível realizar a operação pois' . $msg;
            throw new cERRO_CONSISTENCIA($msg);
        }
    }

    public function sql_Liste() {
        $sql = "select u.cd_usuario
					, u.nome
					, u.cd_perfil
					, u.cd_empresa
					, u.login
					, u.nm_email
					, u.cd_superior
					, case u.flagativo when 'Y' then 1 else 0 end flagativo
					, u.dt_ult
					, u.dt_cadastro
					, u.usua_dt_ult_acesso
                                        , u.usua_qtd_acesso_negado
					, date_format(u.usua_dt_ult_acesso, '%d/%m/%Y %H:%i:%s') usua_dt_ult_acesso_fmt
					, p.nm_perfil
					, E.NO_RAZAO_SOCIAL
			    from usuarios u
				left join usuario_perfil p on p.cd_perfil = u.cd_perfil
				left join EMPRESA E on E.NU_EMPRESA = u.cd_empresa
				where 1=1 ";
        return $sql;
    }

    public function sql_RecuperePeloId() {
        $sql = "select u.cd_usuario
					, u.nome
					, u.cd_perfil
					, u.cd_empresa
					, u.login
					, u.nm_email
					, u.nm_senha
					, u.cd_superior
					, case u.flagativo when 'Y' then 1 else 0 end flagativo
					, u.dt_ult
					, u.dt_cadastro
					, u.usua_tx_apelido
					, p.nm_perfil
					, E.NO_RAZAO_SOCIAL
			    from usuarios u
				left join usuario_perfil p on p.cd_perfil = u.cd_perfil
				left join EMPRESA E on E.NU_EMPRESA = u.cd_empresa
				where cd_usuario = " . $this->cd_usuario;
        return $sql;
    }

    public function ConsistirInclusao() {
        $this->ConsistirSempre();
    }

    public function ConsistirAtualizacao() {
        $this->ConsistirSempre();
    }

    public function Atualizar($plog_tx_evento = "") {
        $this->ConsistirAtualizacao();
        $sql = "update usuarios set ";
        $sql.= "   cd_perfil = " . cBANCO::ChaveOk($this->cd_perfil);
        $sql.= " , cd_empresa = " . cBANCO::ChaveOk($this->cd_empresa);
        $sql.= " , nome = " . cBANCO::StringOk($this->nome);
        $sql.= " , login = " . cBANCO::StringOk($this->login);
        $sql.= " , nm_email = " . cBANCO::StringOk($this->nm_email);
        $sql.= " , cd_superior = " . cBANCO::ChaveOk($this->cd_superior);
        $sql.= " , flagativo = " . cBANCO::StringOk($this->get_flagativo());
        $sql.= " , dt_ult = now()";
        $sql.= " , usua_tx_apelido = " . cBANCO::StringOk($this->usua_tx_apelido);
        $sql.= " , cd_usuario_log = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql.= " , log_tx_controller = " . cBANCO::StringOk(cHTTP::get_audit_controller());
        $sql.= " , log_tx_metodo = " . cBANCO::StringOk(cHTTP::get_audit_metodo());
        $sql.= " , log_tx_evento = " . cBANCO::StringOk($plog_tx_evento);
        $sql.= " , log_dt_evento = now()";
        $sql.= " where cd_usuario = " . $this->cd_usuario;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function Incluir() {
        $this->flagativo = 1;
        $this->ConsistirInclusao();
        $sql = "insert into usuarios (";
        $sql.= "   nome ";
        $sql.= " , cd_perfil ";
        $sql.= " , cd_empresa  ";
        $sql.= " , login ";
        $sql.= " , nm_email  ";
        $sql.= " , cd_superior  ";
        $sql.= " , flagativo  ";
        $sql.= " , dt_ult ";
        $sql.= " , dt_cadastro ";
        $sql.= " , cd_usuario_log";
        $sql.= " , log_tx_controller";
        $sql.= " , log_tx_metodo";
        $sql.= " , log_tx_evento";
        $sql.= " , log_dt_evento";
        $sql.= " )values(";
        $sql.= "   " . cBANCO::StringOk($this->nome);
        $sql.= " , " . cBANCO::ChaveOk($this->cd_perfil);
        $sql.= " , " . cBANCO::ChaveOk($this->cd_empresa);
        $sql.= " , " . cBANCO::StringOk($this->login);
        $sql.= " , " . cBANCO::StringOk($this->nm_email);
        $sql.= " , " . cBANCO::ChaveOk($this->cd_superior);
        $sql.= " , " . cBANCO::StringOk($this->get_flagativo());
        $sql.= " , now()";
        $sql.= " , now()";
        $sql.= " , " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql.= " , " . cBANCO::StringOk(cHTTP::get_audit_controller());
        $sql.= " , " . cBANCO::StringOk(cHTTP::get_audit_metodo());
        $sql.= " , 'Inclusão'";
        $sql.= " , now()";
        $sql.= " )";
        cAMBIENTE::$db_pdo->exec($sql);
        $this->cd_usuario = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function AltereSenha($pSenha, $pSenha_rep = '__na__') {
        $this->NovaSenhaValida($pSenha, $pSenha_rep);

        $sql = "update usuarios
				 set nm_senha = '" . md5($pSenha) . "'
				 , cd_usuario_log = " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario) . "
				 , log_tx_controller = " . cBANCO::StringOk(cHTTP::get_audit_controller()) . "
				 , log_tx_metodo = " . cBANCO::StringOk(cHTTP::get_audit_metodo()) . "
				 , log_tx_evento = 'Alteração de senha'
				 where cd_usuario=" . $this->cd_usuario;
        cAMBIENTE::ExecuteQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public function NovaSenhaValida($pSenha, $pSenha_rep = '__na__') {
        $msg = '';
        if ($pSenha_rep != '__na__') {
            if ($pSenha != $pSenha_rep) {
                if (cHTTP::getLang() == 'pt_br') {
                    $msg = 'Não foi possível alterar a senha pois os valores informados não são idênticos';
                } else {
                    $msg = "The new passwords don't match. Please verify and try again.";
                }
            }
        }
        if ($msg == '') {
            if (strlen($pSenha) < 8) {
                if (cHTTP::getLang() == 'pt_br') {
                    $msg = 'Não foi possível alterar a senha pois o tamanho da nova senha é muito pequeno. A senha deve ter no mínimo 8 caracteres.';
                } else {
                    $msg = 'The new password needs to be at least 8 characters long. Please verify and try again.';
                }
            }
            if (strlen($pSenha) > 10) {
                if (cHTTP::getLang() == 'pt_br') {
                    $msg = 'Não foi possível alterar a senha pois o tamanho da nova senha é muito grande. A senha deve ter no máximo 10 caracteres.';
                } else {
                    $msg = 'The new password needs to be at max 10 characters long. Please verify and try again.';
                }
            }
        }
        if ($msg != '') {
            throw new cERRO_CONSISTENCIA($msg);
        }
    }

    public function EnvieNovaSenha() {
        $senha = $this->GereNovaSenha();
        $this->comuniqueNovaSenha($senha);
    }

    public function comuniqueNovaSenha($senha) {
        $email = new cEMAIL();
        $email->AdicioneDestinatario($this->nm_email);
        $email->setSubject("Dataweb - Envio de senha");
        $msg = "Prezado " . $this->nome . "<br/><br/><br/>Foi solicitado uma renovação de senha para o seu acesso ao sistema Dataweb. Sua nova senha é <b>" . $senha . "</b><br/><br/><br/><b>Dataweb</b>";
        $email->setMsg($msg);
        $email->Envie();
    }

    public function RecupereSePeloEmail($pEmail) {
        if ($pEmail == '') {
            throw new exception("Email não informado");
        }
        $sql = "select * from usuarios where nm_email = " . cBANCO::StringOk($pEmail);
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        if ($rs) {
            cBANCO::CarreguePropriedades($rs, $this, "");
        } else {
            throw new exception("Usuário não encontrado");
        }
    }

    public function GereNovaSenha() {
        $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $novasenha = substr(str_shuffle($letters), 0, 8);

        $sql = "update usuarios set usua_qtd_acesso_negado = 0, nm_senha ='" . md5($novasenha) . "' where cd_usuario =" . $this->cd_usuario;
        $res = cAMBIENTE::$db_pdo->exec($sql);
        return $novasenha;
    }

    public function RecupereSe($pcd_usuario = '') {
        if ($pcd_usuario != '') {
            $this->cd_usuario = $pcd_usuario;
        }
        if ($this->cd_usuario == '') {
            throw new Exception('ID do usuário nao informado');
        }
        $sql = "select * from usuarios where cd_usuario = " . $this->cd_usuario;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetch(PDO::FETCH_BOTH);
            cBANCO::CarreguePropriedades($rs, $this, "");
        } else {
            throw new exception("Não foi possível recuperar o usuário (cd_usuario=" . $pcd_usuario . ".");
        }
    }

    public function RecuperePontoFocalEmpresa($pNU_EMPRESA, $pOrdemPontoFocal) {
        $sql = "select u.* from usuarios u , EMPRESA e where u.cd_usuario = e.usua_id_responsavel" . $pOrdemPontoFocal . " and e.NU_EMPRESA = " . $pNU_EMPRESA;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetch(PDO::FETCH_BOTH);
            cBANCO::CarreguePropriedades($rs, $this, "");
        }
    }

    public function RecupereResponsavelOs($pid_solicita_visto) {
        $sql = "select u.*
				  from usuarios u
					  , solicita_visto sv
				  where cd_usuario = sv.cd_tecnico
				    and sv.id_solicita_visto = " . $pid_solicita_visto;
        if ($res = cAMBIENTE::$db_pdo->query($sql)) {
            $rs = $res->fetch(PDO::FETCH_BOTH);
            cBANCO::CarreguePropriedades($rs, $this, "");
        } else {
            throw new exception("Não foi possível recuperar o usuário (cd_usuario=" . $pcd_usuario . ".");
        }
    }

    public function RegistraUltimoLogin($pIp) {
        $sql = "update usuarios set usua_dt_ult_acesso = now(), usua_tx_ip_ult_acesso = " . cBANCO::StringOk($pIp) . " where cd_usuario=" . $this->cd_usuario;
        try {
            cAMBIENTE::ExecuteQuery($sql);
        } catch (Exception $e) {

        }
    }

    public function CursorRevisoes($pFiltros, $pOrdem) {
        if ($pOrdem == '') {
            $pOrdem = "nome";
        } else {
            $pOrdem .= ", nome";
        }
        $pOrdem = str_replace('total', 'count(nu_candidato) desc', $pOrdem);
        $sql = "select nm_perfil, u.cd_usuario, u.nome, count(nu_candidato) total
				  from usuarios u
				  left join candidato c on
				  			c.cd_usuario_revisao = u.cd_usuario
				  			and date_format(cand_dt_revisao, '%Y-%m-%d') <> '2012-10-11'
				  			and (c.DT_CADASTRAMENTO < '2012-01-01'
				  			or date_format(dt_cadastramento, '%y-%m-%d') <> date_format(cand_dt_revisao, '%y-%m-%d') )
							and cand_fl_revisado = 1
				  left join usuario_perfil p on p.cd_perfil = u.cd_perfil
				  where u.cd_perfil in (1,2,3,4,5,6)
				  and flagativo = 'Y'
				  group by u.cd_usuario, u.nome
				  order by " . $pOrdem;
        $res = conectaQuery($sql, __CLASS . '.' . __FUNCTION__);
        return $res;
    }

    public function Listar($pFiltros = '', $pOrdenacao = '') {
        $sql = '';
        $sql .= $this->sql_Liste();
        $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        if ($pOrdenacao != '') {
            $sql .= " order by " . $pOrdenacao;
        }
        if ($res = cAMBIENTE::ConectaQuery($sql, __CLASS__)) {
            return $res;
        }
    }

    public function SenhaValida($pSenha) {
        if ($this->nm_senha == md5($pSenha)) {
            return true;
        } else {
            return false;
        }
    }

    public function recuperePeloLogin() {
        $sql = "select * from usuarios where login = " . cBANCO::StringOk($this->login);
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if ($rs['cd_usuario'] > 0) {
            cBANCO::CarreguePropriedades($rs, $this);
            return true;
        } else {
            return false;
        }
    }

    public function LoginValido() {
        $sql = "select * from usuarios where login = " . cBANCO::StringOk($this->login);
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if ($rs['cd_usuario'] > 0) {
            if ($rs['nm_senha'] == $this->nm_senha) {
                cBANCO::CarreguePropriedades($rs, $this);
                return true;
            } else {
                $msg = "Login ou senha inválidos <br/>Invalid username or password";
                throw new cERRO_CONSISTENCIA($msg);
                return false;
            }
        } else {
            $msg = "Login ou senha inválidos <br/>Username or password invalid";
            throw new cERRO_CONSISTENCIA($msg);
            return false;
        }
    }

    public function LoginValidoCrypt($login, $senha) {
        $this->login = $login;
        if ($this->recuperePeloLogin()) {
            if ($this->usua_qtd_acesso_negado < 5) {
                if (md5($senha) == $this->nm_senha) {
                    $this->zeraAcessoNegado();
                    return true;
                } else {
                    $this->incrementaAcessoNegado();
                    $restantes = 5 - $this->usua_qtd_acesso_negado;
                    $msg = "Login ou senha inválidos (" . $restantes . " tentativas restantes)<br/>Username or password invalid (" . $restantes . " tries left)";
                    throw new cERRO_CONSISTENCIA($msg);
                    return false;
                }
            } else {
                $msg = "Quantidade de tentativas inválidas excedida. Usuário bloqueado <br/>Password attempts exceeded. User blocked.";
                throw new cERRO_CONSISTENCIA($msg);
                return false;
            }
        } else {
            $msg = "Login ou senha inválidos <br/>Username or password invalid";
            throw new cERRO_CONSISTENCIA($msg);
            return false;
        }
    }

    public function incrementaAcessoNegado() {
        if ($this->usua_qtd_acesso_negado == '') {
            $this->usua_qtd_acesso_negado = 1;
        } else {
            $this->usua_qtd_acesso_negado++;
        }
        $sql = "update usuarios set usua_qtd_acesso_negado = " . $this->usua_qtd_acesso_negado . " where cd_usuario=" . $this->cd_usuario;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function zeraAcessoNegado() {
        $sql = "update usuarios set usua_qtd_acesso_negado = 0 where cd_usuario=" . $this->cd_usuario;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function get_ArrayEmpresas() {

    }

    public function get_ehPetro() {
        if ($this->cd_empresa == 21) {
            return 'S';
        } else {
            return 'N';
        }
    }

    public function get_ehAdmin() {
        if ($this->cd_empresa == 21) {
            return 'S';
        } else {
            return 'N';
        }
    }

    public static function responsaveis($default = '(selecione...)', $cd_usuario= 0){
        $sql = "select cd_usuario chave"
                . ", nome descricao"
                . " from usuarios"
                . " where cd_perfil < 10"
                . " and (flagativo = 'Y' or cd_usuario =".$cd_usuario.")"
                . " order by nome";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $regs = $res->fetchAll(PDO::FETCH_ASSOC);
        if ($default != ''){
            $regs = array(array('chave' => 0, 'descricao' => $default)) + $regs;
        }
        return $regs;

    }
}
