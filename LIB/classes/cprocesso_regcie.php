<?php

class cprocesso_regcie extends cprocesso {

    public $mnu_protocolo;
    public $mdt_requerimento;
    public $mdt_validade;
    public $mdt_prazo_estada;
    public $mdt_prazo_estada_orig;
    public $mcodigo_processo_prorrog;
    public $mID_STATUS_SOL;
    public $mdepf_no_nome;

    public function __construct($pCodigo = 0) {
        $this->mnomeTabela = 'regcie';
        if ($pCodigo > 0) {
            $this->RecupereSe($pCodigo);
        } else {
            $this->mcodigo = 0;
        }
    }

    public function get_nu_processo() {
        return $this->mnu_protocolo;
    }

    public function RecupereSe($pCodigo = 0) {
        if ($pCodigo > 0) {
            $this->mcodigo = $pCodigo;
        }

        if (!$this->mcodigo > 0) {
            throw new cERRO_CONSISTENCIA("Código do processo não informado");
        }
        $sql = "select * from vprocesso_regcie where codigo = " . $this->mcodigo;
        if ($res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->mcd_candidato = $rs["cd_candidato"];
                $this->mcd_solicitacao = $rs["cd_solicitacao"];
                $this->mnu_protocolo = $rs["nu_protocolo"];
                $this->mdt_requerimento = cBANCO::DataFmt($rs["dt_requerimento"]);
                $this->mdt_validade = cBANCO::DataFmt($rs["dt_validade"]);
                $this->mdt_prazo_estada = $rs["dt_prazo_estada"];
                $this->mobservacao = $rs["observacao"];
                $this->mdt_cad = cBANCO::DataFmt($rs["dt_cad"]);
                $this->mdt_ult = cBANCO::DataFmt($rs["dt_ult"]);
                $this->mid_solicita_visto = $rs["id_solicita_visto"];
                $this->mcodigo_processo_mte = $rs["codigo_processo_mte"];
                $this->mfl_vazio = $rs["fl_vazio"];
                $this->mfl_processo_atual = $rs["fl_processo_atual"];
                $this->mcodigo_processo_prorrog = $rs["codigo_processo_prorrog"];
                $this->mnu_servico = $rs['nu_servico'];
                $this->mID_STATUS_SOL = $rs['ID_STATUS_SOL'];
                $this->mNO_SERVICO_RESUMIDO = $rs['NO_SERVICO_RESUMIDO'];
                $this->mdepf_no_nome = $rs['depf_no_nome'];
            } else {
                throw new Exception("Processo (" . $this->mcodigo . ") não encontrado");
            }
        }
    }

    public function Salve() {
        if ($this->mcodigo == 0) {
            $this->IncluaNovo();
        } else {
            $this->Atualize();
        }
    }

    public function Salve_ProcessoEdit() {
        if ($this->mcodigo == 0) {
            $this->IncluaNovo();
        } else {
            $this->Atualize_ProcessoEdit();
        }
    }

    private function IncluaNovo() {
        if ($this->mfl_vazio == '') {
            $this->mfl_vazio = '0';
        }

        $sql = "insert into processo_regcie (";
        $sql .= "	cd_candidato";
        $sql .= "	,cd_solicitacao";
        $sql .= "	,nu_protocolo";
        $sql .= "	,dt_requerimento";
        $sql .= "	,dt_validade";
        $sql .= "	,dt_prazo_estada";
        $sql .= "	,observacao";
        $sql .= "	,dt_cad";
        $sql .= "	,dt_ult";
        $sql .= "	,id_solicita_visto";
        $sql .= "	,codigo_processo_mte";
        $sql .= "	,fl_vazio";
        $sql .= "	,fl_processo_atual";
        $sql .= "	,codigo_processo_prorrog";
        $sql .= "	,nu_servico";
        $sql .= "   ,cd_usuario";
        $sql .= "   ,no_classe";
        $sql .= "   ,no_metodo";
        $sql .= "	) values (";
        $sql .= "	" . cBANCO::ChaveOk($this->mcd_candidato);
        $sql .= "	," . cBANCO::StringOk($this->mcd_solicitacao);
        $sql .= "	," . cBANCO::StringOk($this->mnu_protocolo);
        $sql .= "	," . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= "	," . cBANCO::DataOk($this->mdt_validade);
        $sql .= "	," . cBANCO::DataOk($this->mdt_prazo_estada);
        $sql .= "	," . cBANCO::StringOk($this->mobservacao);
        $sql .= "	,now()";
        $sql .= "	,now()";
        $sql .= "	," . cBANCO::ChaveOk($this->mid_solicita_visto);
        $sql .= "	," . cBANCO::ChaveOk($this->mcodigo_processo_mte);
        $sql .= "	," . cBANCO::InteiroOk($this->mfl_vazio);
        $sql .= "	," . cBANCO::InteiroOk($this->mfl_processo_atual);
        $sql .= "	," . cBANCO::ChaveOk($this->mcodigo_processo_prorrog);
        $sql .= "	," . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= "	," . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql.= "	," . cBANCO::StringOk(__CLASS__);
        $sql.= "	," . cBANCO::StringOk(__FUNCTION__);
        $sql .= "	)";
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $this->mcodigo = mysql_insert_id();
    }

    public function IncluaNovoFilho() {
        $this->VerificarCandidato();
        $this->mfl_vazio = '0';
        $this->IncluaNovo();
    }

    public function VerificarCandidato() {
        if ($this->mcd_candidato == '' || $this->mcd_candidato == 'null') {
            if ($this->mcodigo_processo_mte == '') {
                $sql = "select cd_candidato from processo_prorrog where codigo = " . $this->mcodigo_processo_prorrog;
            } else {
                $sql = "select cd_candidato from processo_mte where codigo = " . $this->mcodigo_processo_mte;
            }
            $res = conectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
            if ($rs = mysql_fetch_array($res)) {
                $this->mcd_candidato = $rs['cd_candidato'];
            } else {
                throw new Exception('Não foi possível encontrar o código do candidato para adicionar o processo', 'codigo_processo_mte=' . $this->mcodigo_processo_mte);
            }
        }
    }

    private function Atualize() {
        $sql = "update processo_regcie set ";
        $sql .= "   cd_candidato 		= " . cBANCO::ChaveOk($this->mcd_candidato);
        $sql .= " , cd_solicitacao 		= " . cBANCO::ChaveOk($this->mcd_solicitacao);
        $sql .= " , nu_protocolo 		= " . cBANCO::StringOk($this->mnu_protocolo);
        $sql .= " , dt_requerimento 	= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , dt_validade 		= " . cBANCO::DataOk($this->mdt_validade);
        $sql .= " , dt_prazo_estada 	= " . cBANCO::DataOk($this->mdt_prazo_estada);
        $sql .= " , observacao 			= " . cBANCO::StringOk($this->mobservacao);
        $sql .= " , id_solicita_visto 	= " . cBANCO::ChaveOk($this->mid_solicita_visto);
        $sql .= " , codigo_processo_mte = " . cBANCO::ChaveOk($this->mcodigo_processo_mte);
        $sql .= " , codigo_processo_prorrog = " . cBANCO::ChaveOk($this->mcodigo_processo_prorrog);
        $sql .= " , fl_vazio 			= " . cBANCO::InteiroOk($this->mfl_vazio);
        $sql .= " , fl_processo_atual 	= " . cBANCO::InteiroOk($this->mfl_processo_atual);
        $sql .= " , nu_servico			= " . cBANCO::ChaveOk($this->mnu_servico);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo 			= " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function Atualize_ProcessoEdit() {
        $msg = '';
        if (trim($this->mnu_protocolo) != '' || trim($this->mdt_requerimento) != '' || trim($this->mdt_validade) != '' || trim($this->mdt_prazo_estada) != '') {
            $msg .= $this->ValidaCampoObrigatorio($this->mnu_protocolo, 'o número do protocolo');
            $msg .= $this->ValidaCampoObrigatorio($this->mdt_requerimento, 'a data do requerimento');
            $msg .= $this->ValidaCampoObrigatorio($this->mdt_validade, 'a validade do protocolo');
            $msg .= $this->ValidaCampoObrigatorio($this->mdt_prazo_estada, 'o prazo de estada atual');
        }
        //$msg .= $this->ValidaCampoObrigatorio($this->mnu_servico, 'o serviço');
        if ($msg != '') {
            throw new cERRO_CONSISTENCIA('Não foi possível completar a operação pois:' . $msg);
        }

        $this->ConsisteCampos();
        $sql = "update processo_regcie set ";
        $sql .= "   nu_protocolo 		= " . cBANCO::StringOk($this->mnu_protocolo);
        $sql .= " , dt_requerimento 	= " . cBANCO::DataOk($this->mdt_requerimento);
        $sql .= " , dt_validade 		= " . cBANCO::DataOk($this->mdt_validade);
        $sql .= " , dt_prazo_estada 	= " . cBANCO::DataOk($this->mdt_prazo_estada);
        $sql .= " , observacao 			= " . cBANCO::StringOk($this->mobservacao);
//		$sql .= " , nu_servico			= ".cBANCO::ChaveOk($this->mnu_servico);
        $sql .= " , cd_usuario			= " . cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
        $sql .= " , dt_ult 				= now()";
        $sql .= " , no_classe			= " . cBANCO::StringOk(__CLASS__);
        $sql .= " , no_metodo			= " . cBANCO::StringOk(__FUNCTION__);
        $sql .= " where codigo 			= " . $this->mcodigo;
        executeQuery($sql, __CLASS__ . '->' . __FUNCTION__);
    }

    public function ConsisteCampos() {
        $msg = '';
        if ($this->mdt_requerimento != '' && !cBANCO::DataValida($this->mdt_requerimento)) {
            $msg .= '<br/>- A data do requerimento é inválida';
        }
        if ($this->mdt_validade != '' && !cBANCO::DataValida($this->mdt_validade)) {
            $msg .= '<br/>- A validade do protocolo CIE é inválida';
        }
        if ($this->mdt_prazo_estada != '' && !cBANCO::DataValida($this->mdt_prazo_estada)) {
            $msg .= '<br/>- O prazo de estada é inválido';
        }
        if ($msg != '') {
            $msg = 'Não foi possível executar a operação pois:' . $msg;
            throw new cERRO_CONSISTENCIA($msg);
        }
    }

    public function RecupereSePelaOsSemCand($pID_SOLICITA_VISTO) {
        $this->mid_solicita_visto = $pID_SOLICITA_VISTO;

        if (!intval($this->mid_solicita_visto) > 0) {
            throw new Exception("Código da OS não informado");
        }
        $sql = "select * from vprocesso_" . $this->mnomeTabela . " ps";
        $sql.= " where id_solicita_visto = " . $this->mid_solicita_visto;
        if ($res = mysql_query($sql)) {
            if ($rs = mysql_fetch_array($res)) {
                $this->mnu_servico = $rs["nu_servico"];
                $this->mcodigo = $rs["codigo"];
                $this->mcodigo_processo_mte = $rs["codigo_processo_mte"];
                $this->mNO_SERVICO_RESUMIDO = $rs["NO_SERVICO_RESUMIDO"];
                $this->mdepf_no_nome = $rs["depf_no_nome"];
                cBANCO::CarreguePropriedades($rs, $this);
            }
        }
    }

    public function SituacaoVistoProcesso(&$pprazo_estada, &$ptexto, &$porigem) {
        if ($porigem != '') {
            $br = '<br/>';
        } else {
            $br = '';
        }
        if ($this->mnu_servico == 61  //Retificação de nome
                || $this->mnu_servico == 23  //Segunda via CIE
                || $this->mnu_servico == 21) { //Transcrição do registro
            return false;
        } elseif ($this->mnu_servico == 18 || $this->mnu_servico == 93) { //Prorrogação protocolo
            $ptexto = "";
            $msg = "(mas verifique a validade do protocolo)";
            if (!strstr($msg, $porigem)) {
                $porigem = $msg;
            }
            return false;
        } elseif ($this->mnu_servico == 60) { //Reconsideração
            $pprazo_estada = "--";
            $ptexto = "Aguardando análise do recurso";
            $porigem = "Processo mais recente é uma Reconsideração" . $br . $porigem;
            return true;
        } else {
            if (trim($this->mdt_prazo_estada) == '') {
                $pprazo_estada = "--";
                $ptexto = "Precisa ser registrado";
                $porigem = "Registro mas com prazo de estada em branco" . $br . $porigem;
                return true;
            } else {
                $dt_prazo_estada = substr($this->mdt_prazo_estada, 6, 4) . '-' . substr($this->mdt_prazo_estada, 3, 2) . '-' . substr($this->mdt_prazo_estada, 0, 2);
                if ($dt_prazo_estada > date('Y-m-d')) {
                    $pprazo_estada = $this->mdt_prazo_estada;
                    $ptexto = "Visto válido";
                    $porigem = "Registro com prazo de estada > hoje" . $br . $porigem;
                    return true;
                } else {
                    $pprazo_estada = "--";
                    $ptexto = "Visto vencido";
                    $porigem = "Registro com prazo de estada < hoje" . $br . $porigem;
                    return true;
                }
            }
        }
    }

    /**
     * Retorna um recordset com os registros do log do processo
     * @param int $codigo Chave do processo
     * @return PDOStatement
     */
    public static function cursorLog($codigo){
    	$sql = "select * "
    		  ."     , concat(no_classe, '->', no_metodo) classe_metodo"
    		  ."     , date_format(processo_regcie_log.dt_prazo_estada, '%d/%m/%Y') dt_prazo_estada_fmt"
    		  ."     , date_format(processo_regcie_log.dt_ult, '%d/%m/%Y %h:%i:%s') dt_ult_fmt"
    		  ."     , date_format(processo_regcie_log.dt_requerimento, '%d/%m/%Y') dt_requerimento_fmt"
    		  ."     , date_format(processo_regcie_log.dt_validade, '%d/%m/%Y') dt_validade_fmt"
    		  ."     , usuarios.nome"
    		  ."  from processo_regcie_log "
    		  ."  left join usuarios on usuarios.cd_usuario = processo_regcie_log.cd_usuario "
    		  ." where codigo = ".$codigo
    	 	  ." order by processo_regcie_log.dt_ult desc ";
    	$rs = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
    	return $rs;
    }
}
