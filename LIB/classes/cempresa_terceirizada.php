<?php
/**
 * Empresa terceirizada. Criado para atender Ã s necessidades da Odebrecht
 */
class cempresa_terceirizada extends cMODELO{
	protected $empt_id;
	protected $empt_tx_descricao;
    /**
     * Gets the value of empt_id.
     *
     * @return mixed
     */
    public function get_empt_id()
    {
        return $this->empt_id;
    }

    /**
     * Sets the value of empt_id.
     *
     * @param mixed $empt_id the empt_id
     */
    public function set_empt_id($empt_id)
    {
        $this->empt_id = $empt_id;
    }

    /**
     * Gets the value of empt_tx_descricao.
     *
     * @return mixed
     */
    public function get_empt_tx_descricao()
    {
        return $this->empt_tx_descricao;
    }

    /**
     * Sets the value of empt_tx_descricao.
     *
     * @param mixed $empt_tx_descricao the empt_tx_descricao
     */
    public function set_empt_tx_descricao($empt_tx_descricao)
    {
        $this->empt_tx_descricao = $empt_tx_descricao;
    }

 	public function campoid(){
		return 'empt_id';
	}
	public function setId($pValor){
		$this->empt_id = $pValor;
	}
	public function getId(){
		return $this->empt_id;
	}
	public function sql_Liste(){
		return "select * from empresa_terceirizada where 1=1 ";
	}
	public function sql_RecuperePeloId(){
		return "select * from empresa_terceirizada where empt_id=".$this->empt_id;
	}
	public function sql_RecuperePelaDescricao(){
		return "select * from empresa_terceirizada where empt_tx_descricao like ".cBANCO::StringOk($this->empt_tx_descricao);
	}
	public function Atualizar(){
        $sql = "update empresa_terceirizada set
                    empt_tx_descricao = ".cBANCO::StringOk($this->empt_tx_descricao)."
                where empt_id = ".$this->empt_id;
        cAMBIENTE::$db_pdo->exec($sql);
		
	}
	
	public function Incluir(){
		$sql = "select * from empresa_terceirizada where empt_tx_descricao = ".cBANCO::StringOk($this->empt_tx_descricao);
		$res = cAMBIENTE::$db_pdo->query($sql);
		$rs = $res->fetch(PDO::FETCH_ASSOC);
		if($rs['empt_id'] != ''){
			$this->empt_id = $rs['empt_id'];
		}
		else{
			$sql = "insert into empresa_terceirizada (
						empt_tx_descricao
					) values (
						".cBANCO::StringOk($this->empt_tx_descricao)."
					)";
			cAMBIENTE::$db_pdo->exec($sql);
			$this->setid(cAMBIENTE::$db_pdo->lastInsertId());			
		}
		
	}

    public function JSON_ComboDinamico($pempt_tx_descricao){
        $sql = "select * from empresa_terceirizada where empt_tx_descricao like ".cBANCO::StringOk($pempt_tx_descricao.'%')." order by empt_tx_descricao";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $json = array();
        while($rs = $res->fetch(PDO::FETCH_ASSOC)){
            $json[] = array("label" => $rs['empt_tx_descricao'], "empt_id" => $rs['empt_id']);
        }
        return json_encode($json);
    }
}
