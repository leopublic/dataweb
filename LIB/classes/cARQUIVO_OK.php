<?php

class cARQUIVO_OK {

    public $mNU_SEQUENCIAL;
    public $mNU_EMPRESA;
    public $mNU_CANDIDATO;
    public $mNO_ARQUIVO;
    public $mNO_ARQ_ORIGINAL;
    public $mTP_ARQUIVO;
    public $mDT_INCLUSAO;
    public $mNU_ADMIN;
    public $mNO_DIRETORIO;
    public $mID_SOLICITA_VISTO;
    public $mid_taxa;
    public $extensao;
    public $mNU_TAMANHO;
    protected $phpFile;

    /**
     * Instancia um novo arquivo usando o file recebido do PHP
     * @param type $file
     */
    public static function novoPeloFile($file) {
        $arq = new cARQUIVO_OK;
        $arq->phpFile = $file;
        $arq->mNO_ARQ_ORIGINAL = str_replace("'", "", $file['name']);
        $arq->mNU_TAMANHO = $file['size'];
        return $arq;
    }

    public function extensao() {
        $ax1 = str_replace(".", "#", $this->mNO_ARQ_ORIGINAL);
        $aux = preg_split("[#]", $ax1);
        $x = count($aux);
        if ($x > 1) {
            $extensao = $aux[$x - 1];
        } else {
            $extensao = "txt";
        }
        return $extensao;
    }

    public function caminhoArmazenado() {
        global $mysoldir;
        global $mydocdir;
        if ($this->mID_SOLICITA_VISTO > 0) {
            $ret = $mysoldir . DS . $this->mNU_EMPRESA;
        } else {
            $ret = $mydocdir . DS . $this->mNU_EMPRESA . DS . $this->mNU_CANDIDATO;
        }
        self::criaDiretorioSeNaoExistir($ret);
        return $ret;
    }

    public function caminhoArmazenadoCompleto() {
        return $this->caminhoArmazenado() . DS . $this->nomeArmazenado();
    }

    public function nomeArmazenado() {
        if ($this->mID_SOLICITA_VISTO == '') {
            $ret = "EC" . $this->mNU_CANDIDATO . "SQ" . $this->mNU_SEQUENCIAL . "." . $this->extensao();
        } else {
            $ret = "SOLIC" . substr('00000000' . $this->mID_SOLICITA_VISTO, - 8) . "_" . $this->mNU_SEQUENCIAL . "." . $this->extensao();
        }
        return $ret;
    }

    public static function criaDiretorioSeNaoExistir($caminho) {
        if (!is_dir($caminho)) {
            mkdir($caminho, 0770, true);
        }
    }

    public function insere() {
        $sql = "insert into ARQUIVOS (NU_EMPRESA,NU_CANDIDATO,ID_SOLICITA_VISTO, ";
        $sql .= "NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NU_ADMIN, NO_ARQUIVO, id_taxa, NU_TAMANHO) ";
        $sql .= " values (";
        $sql .= "  " . cBANCO::ChaveOk($this->mNU_EMPRESA);
        $sql .= " ," . cBANCO::ChaveOk($this->mNU_CANDIDATO);
        $sql .= " ," . cBANCO::ChaveOk($this->mID_SOLICITA_VISTO);
        $sql .= " ," . cBANCO::StringOk($this->mNO_ARQ_ORIGINAL);
        $sql .= " ," . cBANCO::ChaveOk($this->mTP_ARQUIVO);
        $sql .= " , now()";
        $sql .= " ," . cBANCO::ChaveOk($this->mNU_ADMIN);
        $sql .= " , '' ";
        $sql .= " ," . cBANCO::ChaveOk($this->mid_taxa);
        $sql .= " ," . cBANCO::InteiroOk($this->mNU_TAMANHO);
        $sql .= ")";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mNU_SEQUENCIAL = cAMBIENTE::$db_pdo->lastInsertId();

        $sql = "update ARQUIVOS set NO_ARQUIVO = " . cBANCO::StringOk($this->nomeArmazenado()) . " where NU_SEQUENCIAL = " . $this->mNU_SEQUENCIAL;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function armazenaArquivoFisico() {
        move_uploaded_file($this->phpFile['tmp_name'], $this->caminhoArmazenadoCompleto());
    }

    public function salvaEArmazena(){
        $this->insere();
        $this->armazenaArquivoFisico();
    }
    
    public static function CaminhoReal($pNU_CANDIDATO, $pNO_ARQUIVO, $pNU_EMPRESA, $pNU_SOLICITACAO) {
        if (intval($pNU_SOLICITACAO) > 0) {
            $src = cAMBIENTE::getDirSol() . DIRECTORY_SEPARATOR . $pNU_EMPRESA . DIRECTORY_SEPARATOR . $pNO_ARQUIVO;
        } else {
            $src = cAMBIENTE::getDirDoc() . DIRECTORY_SEPARATOR . $pNU_EMPRESA . DIRECTORY_SEPARATOR . $pNU_CANDIDATO . DIRECTORY_SEPARATOR . $pNO_ARQUIVO;
        }
        return $src;
    }

    public function url() {
        global $mysolurl;
        global $mydocurl;
        if ($this->mID_SOLICITA_VISTO > 0) {
            $ret = $mysolurl . '/' . $this->mNU_EMPRESA . '/' . $this->nomeArmazenado();
        } else {
            $ret = $mydocurl . '/' . $this->mNU_EMPRESA . '/' . $this->mNU_CANDIDATO . '/' . $this->nomeArmazenado();
        }
        return $ret;
    }

    public static function JSON_ArquivosDoCandidato($pNU_CANDIDATO) {
        $sql = "select a.*
                        , date_format(DT_INCLUSAO, '%d/%m/%Y') as DT_INCLUSAO_FMT 
                        , NO_TIPO_ARQUIVO
                 from arquivos a
                 left join tipo_arquivo  ta on ta.id_tipo_arquivo = a.tp_arquivo 
                where nu_candidato = " . $pNU_CANDIDATO . " order by DT_INCLUSAO desc";
        $res = cAMBIENTE::ConectaQuery($sql);
        $arquivos = array();
        while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $arquivos[] = array(
                "NU_SEQUENCIAL" => $rs['NU_SEQUENCIAL']
                , "NO_TIPO_ARQUIVO" => $rs['NO_TIPO_ARQUIVO']
                , "DT_INCLUSAO_FMT" => $rs['DT_INCLUSAO_FMT']
                , "NO_ARQ_ORIGINAL" => $rs['NO_ARQ_ORIGINAL']
                , "url" => self::CaminhoReal($rs['NU_CANDIDATO'], $rs['NO_ARQUIVO'], $rs['NU_EMPRESA'], $rs['ID_SOLICITA_VISTO'])
            );
        }
        return $arquivos;
    }
}
