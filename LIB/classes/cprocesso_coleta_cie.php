<?php
class cprocesso_coleta_cie
	extends cprocesso
{
	public $mnu_rne;
	public $mdt_expedicao;
	public $mdt_validade;
	public $mdt_atendimento;
	
	public function __construct($pCodigo = 0){
		$this->mnomeTabela = 'coleta_cie';
		if ($pCodigo > 0){
			$this->RecupereSe($pCodigo);
		}
		else{
			$this->mcodigo = 0;
			$this->mfl_vazio = 0;
		}
	}

	public function RecupereSe($pCodigo = 0){
		if ($pCodigo > 0){
			$this->mcodigo = $pCodigo;
		}

		if (! $this->mcodigo > 0){
			throw new Exception("Código do processo não informado");
		}
		$sql = "select processo_coleta_cie.*, s.NO_SERVICO_RESUMIDO "
                        . " from processo_coleta_cie"
                        . " left join servico s on s.nu_servico = processo_coleta_cie.nu_servico"
                        . " where codigo = ".$this->mcodigo;
		if($res = mysql_query($sql)){
			if($rs = mysql_fetch_array($res)){
				cBANCO::CarreguePropriedades($rs, $this);
                                $this->mNO_SERVICO_RESUMIDO = $rs['NO_SERVICO_RESUMIDO'];
			}
			else{
				throw new Exception("Processo (".$this->mcodigo.") não encontrado");
			}
		}
	}

	public function RecupereSePelaOs($pid_solicita_visto){
		if ($pid_solicita_visto == ''){
			throw new Exception("ID da OS não informado");
		}
		$sql = "select * from processo_coleta_cie where id_solicita_visto = ".$pid_solicita_visto;
		if($res = mysql_query($sql)){
			if($rs = mysql_fetch_array($res)){
				cBANCO::CarreguePropriedades($rs, $this);
			}
			else{
				throw new Exception("Processo relacionado à OS (".$pid_solicita_visto.") não encontrado");
			}
		}
	}

	public function Salve(){
		if ($this->mcodigo == 0){
			$this->IncluaNovo();
		}
		else{
			$this->Atualize();
		}
	}

	public function Salve_ProcessoEdit(){
		if ($this->mcodigo == 0){
			$this->IncluaNovo();
		}
		else{
			$this->Atualize_ProcessoEdit();
		}
	}

	private function IncluaNovo(){
		$sql = "insert into processo_coleta_cie (";
		$sql .= "	 id_solicita_visto";
		$sql .= "	,cd_candidato";
		$sql .= "	,nu_rne";
		$sql .= "	,dt_atendimento";
		$sql .= "	,dt_expedicao";
		$sql .= "	,dt_validade";
		$sql .= "	,codigo_processo_mte";
		$sql .= "   ,cd_usuario_cad";
		$sql .= "   ,dt_cad";
		$sql .= "	,nu_servico";
		$sql .= "	) values (";
		$sql .= "	 ".cBANCO::ChaveOk($this->mid_solicita_visto);
		$sql .= "	,".cBANCO::ChaveOk($this->mcd_candidato);
		$sql .= "	,".cBANCO::StringOk($this->mnu_rne);
		$sql .= "	,".cBANCO::DataOk($this->mdt_atendimento);
		$sql .= "	,".cBANCO::DataOk($this->mdt_expedicao);
		$sql .= "	,".cBANCO::DataOk($this->mdt_validade);
		$sql .= "	,".cBANCO::ChaveOk($this->mcodigo_processo_mte);
		$sql .= "	,".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= "	, now()";
		$sql .= "	,".cBANCO::ChaveOk($this->mnu_servico);
		$sql .= "	)";
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
		$this->mcodigo = mysql_insert_id();
	}

	public function IncluaNovoFilho(){
		$this->VerificarCandidato();
		$this->mfl_vazio = '0';
		$this->IncluaNovo();
	}

	private function Atualize($pClasse = '', $pMetodo = ''){
		if($pClasse == ''){
			$pClasse = __CLASS__;
		}
		if($pMetodo== ''){
			$pMetodo = __FUNCTION__;
		}
		
		$sql = "update processo_coleta_cie set ";
		$sql .= "   nu_rne					= ".cBANCO::StringOk($this->mnu_rne);
		$sql .= " , dt_expedicao 			= ".cBANCO::DataOk($this->mdt_expedicao);
		$sql .= " , dt_validade 			= ".cBANCO::DataOk($this->mdt_validade);
		$sql .= " , dt_ult 				= now()";
		$sql .= " , cd_usuario_ult		= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= " , no_classe			= ".cBANCO::StringOk(__CLASS__);
		$sql .= " , no_metodo			= ".cBANCO::StringOk(__FUNCTION__);		
		$sql .= " , observacao			= ".cBANCO::StringOk($this->mobservacao);		
		$sql .= " where codigo = ".$this->mcodigo;
		executeQuery($sql, $pClasse.'->'.$pMetodo);
	}
	
	public function SalvarPelaOs($pnu_rne, $pdt_atendimento, $pdt_expedicao, $pdt_validade, $pobservacao){
		$this->mnu_rne = $pnu_rne;
		$this->mdt_expedicao = $pdt_expedicao;
		$this->mdt_validade = $pdt_validade;
		$this->mdt_atendimento = $pdt_atendimento;
		$this->mobservacao = $pobservacao;
		$sql = "update processo_coleta_cie set ";
		$sql .= "   nu_rne					= ".cBANCO::StringOk($this->mnu_rne);
		$sql .= " , dt_atendimento 			= ".cBANCO::DataOk($this->mdt_atendimento);
		$sql .= " , dt_expedicao 			= ".cBANCO::DataOk($this->mdt_expedicao);
		$sql .= " , dt_validade 			= ".cBANCO::DataOk($this->mdt_validade);
		$sql .= " , observacao				= ".cBANCO::StringOk($this->mobservacao);		
		$sql .= " , dt_ult					= now()";
		$sql .= " , cd_usuario_ult			= ".cBANCO::ChaveOk(cSESSAO::$mcd_usuario);
		$sql .= " , log_tx_controler		= ".cBANCO::StringOk(__CLASS__);
		$sql .= " , log_tx_metodo			= ".cBANCO::StringOk(__FUNCTION__);		
		$sql .= " where codigo = ".$this->mcodigo;
		executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
	}
	
	public function SalvarPelaOSeFechar($pnu_rne, $pdt_atendimento, $pdt_expedicao, $pdt_validade, $pobservacao){
		$this->SalvarPelaOs($pnu_rne, $pdt_atendimento, $pdt_expedicao, $pdt_validade, $pobservacao);
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO = $this->mid_solicita_visto;
		$os->Fechar();
	}
}
