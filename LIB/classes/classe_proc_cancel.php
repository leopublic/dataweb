<?php

class proc_cancel {

  public $codigo = "";
  public $cd_candidato = "";
  public $cd_solicitacao = "";
  public $nu_processo = "";
  public $dt_processo = "";
  public $dt_cancel = "";
  public $observacao = "";
  public $dt_cad = "";
  public $dt_ult = "";
  public $myerr = "";

  public function proc_cancel() {  }

  public function BuscaPorCodigo($codigo) {
    $extra = "WHERE codigo=$codigo";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function BuscaPorCandidato($candidato,$solicitacao) {
    $this->cd_candidato = $candidato;
    $this->cd_solicitacao = $solicitacao;
    $extra = "WHERE cd_candidato=$candidato AND cd_solicitacao=$solicitacao";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function ExisteCandidato($candidato,$solicitacao) {
    $extra = "WHERE cd_candidato=$candidato AND cd_solicitacao=$solicitacao";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if(mysql_num_rows($rs)>0) {
        return true;
      } else {
        return false;
      }
    }
  }

  public function BuscaPorProcesso($processo) {
    $extra = "WHERE nu_processo=$processo";
    $rs = $this->ListaProcessos($extra);
    if($this->myerr == "") {
      if($rw=mysql_fetch_array($rs)) {
        $this->LeituraRW($rw);
      }
    }
  }

  public function ListaProcessos($extra) {
    $this->myerr = "";
    $sql = "SELECT * FROM processo_cancel $extra";
    $rs = mysql_query($sql);
    if(mysql_errno() != 0) {
      $this->myerr = mysql_error();
    }
    return $rs;
  }

  function InsereProcesso() {
    if($this->Verifica() == true) {
      $cd_cand = $this->cd_candidato;
      $cd_sol = $this->cd_solicitacao;
      $dt_cancel = $this->dt_cancel;
      $dt_processo = $this->dt_processo;
      $sql1 = "INSERT INTO processo_cancel (cd_candidato,cd_solicitacao,nu_processo,dt_cancel,dt_processo,observacao,dt_cad) VALUES ";
      $sql = sprintf($sql1.$sql2."($cd_cand,$cd_sol,'%s',$dt_cancel,$dt_processo,'%s',now())",
              mysql_real_escape_string($this->nu_processo),
              mysql_real_escape_string($this->observacao)
             );
      mysql_query($sql);
      if(mysql_errno() != 0) {
        $erro = mysql_error();
        $this->myerr = "Ocorreu um erro ao inserir cancelamento.<!-- $erro - sql=$sql -->";
        gravaLogErro($sql,$erro,"CADASTRAR","CANCELAMENTO");
      } else {
        gravaLog($sql,"","CADASTRAR","CANCELAMENTO");
      }
    }
  }

  function AlteraProcesso() {
    if($this->Verifica() == true) {
      $cd_cand = $this->cd_candidato;
      $cd_sol = $this->cd_solicitacao;
      $dt_cancel = $this->dt_cancel;
      $dt_processo = $this->dt_processo;
      $sql1 = "UPDATE processo_cancel SET nu_processo='%s',dt_cancel=$dt_cancel,dt_processo=$dt_processo,observacao='%s' ";
      $sql2 = " WHERE cd_candidato=$cd_cand AND cd_solicitacao=$cd_sol ";
      $sql = sprintf($sql1.$sql2,
              mysql_real_escape_string($this->nu_processo),
              mysql_real_escape_string($this->observacao)
             );
      mysql_query($sql);
      if(mysql_errno() != 0) {
        $erro = mysql_error();
        $this->myerr = "Ocorreu um erro ao alterar cancelamento.<!-- $erro - sql=$sql -->";
        gravaLogErro($sql,$erro,"ALTERAR","CANCELAMENTO");
      } else {
        gravaLog($sql,"","ALTERAR","CANCELAMENTO");
      }
    }
  }

  function AlteraVisaDetail() {
    $cd_cand = $this->cd_candidato;
    $cd_sol = $this->cd_solicitacao;
    $dt_cancel = $this->dt_cancel;
    $dt_processo = $this->dt_processo;
    $sql1 = "UPDATE AUTORIZACAO_CANDIDATO SET NU_PROCESSO_CANCELAMENTO='%s',DT_CANCELAMENTO=$dt_cancel,";
    $sql2 = "DT_PROCESSO_CANCELAMENTO=$dt_processo where NU_CANDIDATO=$cd_cand and NU_SOLICITACAO=$cd_sol ";
    $sql = sprintf($sql1.$sql2,
            mysql_real_escape_string($this->nu_processo)
           );
    mysql_query($sql);
    if(mysql_errno() != 0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao alterar cancelamento.<!-- $erro - sql=$sql -->";
      gravaLogErro($sql,$erro,"ALTERAR","CANCELAMENTO_VISA");
    } else {
      gravaLog($sql,"","ALTERAR","CANCELAMENTO_VISA");
    }
  }

  function RemoveProcesso($codigo,$cand,$sol) {
    $sql = "DELETE FROM processo_cancel WHERE ";
    if(strlen($codigo)>0) {  $sql = "codigo=$codigo";  } else
    if( (strlen($cand)>0) && (strlen($sol)>0) ) {  $sql = "cd_candidato=$cand AND cd_solicitacao=$sol";  }
    mysql_query($sql);
    if(mysql_errno() != 0) {
      $erro = mysql_error();
      $this->myerr = "Ocorreu um erro ao remover cancelamento.<!-- $erro - sql=$sql -->";
      gravaLogErro($sql,$erro,"REMOVER","CANCELAMENTO");
    } else {
      gravaLog($sql,"","REMOVER","CANCELAMENTO");
    }
  }

  function Verifica() {
    $ret = true;
    $erro = "";
    if(strlen($this->cd_candidato) == 0) {
      $ret = false;
      $erro = $erro."Nao foi identificado o candidato. ";
    }
    if(strlen($this->cd_solicitacao) == 0) {
      $ret = false;
      $erro = $erro."Nao foi identificada a solicitacao. ";
    }
    if(strlen($this->dt_cancel)>0) { $this->dt_cancel = "'".dataBR2My($this->dt_cancel)."'"; } else { $this->dt_cancel="NULL"; }
    if(strlen($this->dt_processo)>0) { $this->dt_processo = "'".dataBR2My($this->dt_processo)."'"; } else { $this->dt_processo="NULL"; }
    if($this->observacao=="NULL") { $this->observacao=""; }
    $this->myerr = $erro;
    return $ret;
  }

  private function LeituraRW($rw) {
    $this->codigo = $rw['codigo'];
    $this->cd_candidato = $rw['cd_candidato'];
    $this->cd_solicitacao = $rw['cd_solicitacao'];
    $this->nu_processo = $rw['nu_processo'];
    $this->dt_cancel = dataMy2BR($rw['dt_cancel']);
    $this->dt_processo = dataMy2BR($rw['dt_processo']);
    $this->observacao = $rw['observacao'];
    if($this->observacao=="NULL") { $this->observacao=""; }
    $this->dt_cad = $rw['dt_cad'];
    $this->dt_ult = $rw['dt_ult'];
  }

  private function LimpaObjeto() {
    $this->codigo = "";
    $this->cd_candidato = "";
    $this->cd_solicitacao = "";
    $this->nu_processo = "";
    $this->dt_cancel = "";
    $this->dt_processo = "";
    $this->observacao = "";
    $this->dt_cad = "";
    $this->dt_ult = "";
    $this->myerr = "";
  }

  function insereEvento($idEmpresa,$tipo) {
    $informou = "";
    $cand = $this->cd_candidato;
    $sol = $this->cd_solicitacao;
    $nu_processo = str_replace("'","",$this->nu_processo);
    $dt_cancel = dataMy2BR(str_replace("'","",$this->dt_cancel));
    $dt_processo = dataMy2BR(str_replace("'","",$this->dt_processo));
    $informou = $informou." n&uacute;mero processo ($nu_processo) / ";
    $informou = $informou." data de cancelamento ($dt_cancel) / ";
    $informou = $informou." data do processo ($dt_processo) ";
    $extra="no Cancelamento. <br>Informou: $informou .";
    $texto = "$tipo do cadastro do candidato $extra";
    insereEvento($idEmpresa,$cand,$sol,$texto);
  }

  public function MontaTextoCancel($acao) {
    $ret = "";
    $ret = $ret.montaCampos("cd_candidato",$this->cd_candidato,$this->cd_candidato,"H",$acao,"")."\n";
    $ret = $ret.montaCampos("cd_solicitacao",$this->cd_solicitacao,$this->cd_solicitacao,"H",$acao,"")."\n";
    $ret = $ret."<tr height=20><td class=textoazul bgcolor=#DADADA colspan=5>Cancelamento</td></tr>\n";
    $ret = $ret."<tr height=20>\n<td><b>Data de Cancelamento:</td>\n";
    $ret = $ret."<td>".montaCampos("DT_CANCELAMENTO",$this->dt_cancel,$this->dt_cancel,"D",$acao,"")."</td>\n";
    $ret = $ret."<td>&#160;</td>\n<td><b>N&uacute;mero do Processo:</td>\n";
    $ret = $ret."<td>".montaCampos("NU_PROCESSO_CANCELAMENTO",$this->nu_processo,$this->nu_processo,"T",$acao,"maxlength=20 size=30")."</td>\n";
    $ret = $ret."</tr>\n";
    $ret = $ret."<tr height=20><td><b>Data de Processo:</td>\n";
    $ret = $ret."<td>".montaCampos("DT_PROCESSO_CANCELAMENTO",$this->dt_processo,$this->dt_processo,"D",$acao,"")."</td>\n";
    $ret = $ret."<td>&#160;</td><td>&#160;</td>\n<td>&#160;</td>\n</tr>\n";
    $ret = $ret."<tr height=20><td><b>Observa&ccedil;&atilde;o:</td>\n";
    $ret = $ret."<td colspan=4>".montaCampos("OBS_MTE",$this->observacao,$this->observacao,"T",$acao,"maxlength=250 size=30")."</td>\n";
    $ret = $ret."</tr>\n";
    return $ret;
  }

  public function LeituraRequest($tipo) {
    $this->cd_candidato = $tipo['cd_candidato'];
    $this->cd_solicitacao = $tipo['cd_solicitacao'];
    $this->nu_processo = $tipo['NU_PROCESSO_CANCELAMENTO'];
    $this->dt_cancel = $tipo['DT_CANCELAMENTO'];
    $this->dt_processo = $tipo['DT_PROCESSO_CANCELAMENTO'];
    $this->observacao = $tipo['OBS_MTE'];
    if($this->observacao=="NULL") { $this->observacao=""; }
  }

}

?>

