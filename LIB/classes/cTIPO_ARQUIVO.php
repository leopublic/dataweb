<?php

class cTIPO_ARQUIVO extends cMODELO {

    protected $mID_TIPO_ARQUIVO;
    protected $mNO_TIPO_ARQUIVO;
    protected $mNO_TIPO_ARQUIVO_EM_INGLES;
    protected $mCO_TIPO_ARQUIVO;
    protected $mfl_disponivel_clientes;

    const COPIA_DR_PREENCHIDO_PELO_CLIENTE  = 84;
    const DR_ENVIADO_PARA_APROVACAO = 85;

    public function getId() {
        return $this->mID_TIPO_ARQUIVO;
    }

    public function setId($pValor) {
        $this->mID_TIPO_ARQUIVO = $pValor;
    }

    public function campoid() {
        return 'ID_TIPO_ARQUIVO';
    }

    public function sql_RecuperePeloId() {
        $sql = "select * from tipo_arquivo where id_tipo_arquivo = " . $this->mID_TIPO_ARQUIVO;
        return $sql;
    }

    public function Incluir() {
        $sql = "insert into tipo_arquivo(
				 no_tipo_arquivo
				,no_tipo_arquivo_em_ingles
				,co_tipo_arquivo
                ,fl_disponivel_clientes
		       ) values (
				 " . cBANCO::StringOk($this->mNO_TIPO_ARQUIVO) . "
				," . cBANCO::StringOk($this->mNO_TIPO_ARQUIVO_EM_INGLES) . "
				," . cBANCO::StringOk($this->mCO_TIPO_ARQUIVO) . "
                ," . cBANCO::SimNaoOk($this->mfl_disponivel_clientes) . "
				)";
        cAMBIENTE::ExecuteQuery($sql);
        $this->mID_TIPO_ARQUIVO = cAMBIENTE::$db_pdo->lastInsertId();
    }

    public function Atualizar() {
        $sql = "update tipo_arquivo set
				 no_tipo_arquivo = " . cBANCO::StringOk($this->mNO_TIPO_ARQUIVO) . "
				,no_tipo_arquivo_em_ingles = " . cBANCO::StringOk($this->mNO_TIPO_ARQUIVO_EM_INGLES) . "
				,co_tipo_arquivo = " . cBANCO::StringOk($this->mCO_TIPO_ARQUIVO) . "
                ,fl_disponivel_clientes = " . cBANCO::SimNaoOk($this->mfl_disponivel_clientes) . "
			  where id_tipo_arquivo=" . $this->mID_TIPO_ARQUIVO;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_Liste() {
        $sql = "select * from tipo_arquivo where 1=1";
        return $sql;
    }

    public static function comboMustache($valor_atual = '', $default = "(selecione...)") {
        $sql = "select ID_TIPO_ARQUIVO valor, NO_TIPO_ARQUIVO descricao, if(ID_TIPO_ARQUIVO='".$valor_atual."' ,1 ,0 ) selected from TIPO_ARQUIVO order by NO_TIPO_ARQUIVO";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));

        if (intval($valor_atual == 0)){
            $rs['0']['selected'] = true;
        }

        return  $rs ;
    }

    public static function comboMustacheIngles($valor_atual = '', $default = "(selecione...)") {
        $sql = "select ID_TIPO_ARQUIVO valor, NO_TIPO_ARQUIVO_EM_INGLES descricao, if(ID_TIPO_ARQUIVO='".$valor_atual."' ,1 ,0 ) selected from TIPO_ARQUIVO order by NO_TIPO_ARQUIVO_EM_INGLES";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        array_unshift($rs, array('valor'=> '0', 'descricao'=> $default, 'selected' => '0'));

        if (intval($valor_atual == 0)){
            $rs['0']['selected'] = true;
        }

        return  $rs ;
    }

}
