<?php
class cfornecedor extends cMODELO{
	public $mid_fornecedor;
	public $mnome ;
	public $mdt_importacao ;
	public static function get_nomeCampoChave() {
		return 'id_fornecedor';
	}
	public static function get_nomeTabelaBD() {
		return 'fornecedor';
	}
	
	public function get_nomeTabela() {
		return self::get_nomeTabelaBD();
	}
	
	public function campoid(){
		return self::get_nomeCampoChave();
	}
	
	public function setid($pid){
		$this->mid_fornecedor = $pid;
	}
	
	public function getid(){
		return $this->mid_fornecedor;
	}
	
	public function sql_Liste(){
		$sql  = "    select *
			           from fornecedor";
		return $sql;
	}
			
	public function sql_RecuperePeloId(){
		$sql  = "    select *
					   from fornecedor
					  where id_fornecedor = ".$this->mid_fornecedor;
		return $sql;
	}
	
	public function cursorTodos(){
		$sql = "select * from fornecedor order by nome";
		$rs = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__);
		return $rs;
	}
}
