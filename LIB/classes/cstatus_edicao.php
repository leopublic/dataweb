<?php

class cstatus_edicao extends cMODELO {

    public $mid_status_edicao;
    public $mdescricao;

    const NAO_INICIADO = 1;                     // O usuário não iniciou a alteraão
    const AGUARDANDO_ENVIO = 2;                 // O usuário iniciou a alteraçao
    const AGUARDANDO_CONFIRMACAO = 3;           // Foi alterado e liberado para confirmação pelo cliente
    const CONFIRMADO = 4;                       // Foi confirmado pelo cliente
    const IMPORTADO = 5;                        // Foi importado para um candidato real e encerrado

    public static function get_nomeCampoChave() {
        return 'id_status_edicao';
    }

    public static function get_nomeTabelaBD() {
        return 'status_edicao';
    }

    public function get_nomeTabela() {
        return self::get_nomeTabelaBD();
    }

    public function campoid() {
        return self::get_nomeCampoChave();
    }

    public function setid($pid) {
        $this->mid_status_edicao = $pid;
    }

    public function getid() {
        return $this->mid_status_edicao;
    }

    public function sql_Liste() {
        $sql = "select * from status_edicao order by descricao";
        return $sql;
    }

    public function Incluir() {
        $sql = "insert into status_edicao(
                    descricao
                ) values (
                    " . cBANCO::StringOk($this->mdescricao) . "
                )";
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function Atualizar() {
        $sql = "update status_edicao
                   set descricao		= " . cBANCO::StringOk($this->mdescricao) . "
                 where id_status_edicao = " . $this->mid_status_edicao;
        cAMBIENTE::ExecuteQuery($sql);
    }

    public function sql_RecuperePeloId() {
        $sql = "    select *
                    from status_edicao
                   where id_status_edicao = " . $this->mid_status_edicao;
        return $sql;
    }

    public static function descricao($id) {
        switch ($id) {
            case '':
            case self::NAO_INICIADO:
                return 'Não iniciado';
                break;
            case self::AGUARDANDO_ENVIO:
                return 'Aguardando envio';
                break;
            case self::CONFIRMADO:
                return 'Pronto para importaçao!';
                break;
            case self::IMPORTADO:
                return 'Importado';
                break;
            case self::AGUARDANDO_CONFIRMACAO:
                return 'Aguardando confirmação';
                break;
        }
    }

    public static function comboFiltro(){
        return array(
            array("id" => 1, "descricao" => 'Não iniciado')
            ,array("id" => 2, "descricao" => 'Aguardando envio')
            ,array("id" => 3, "descricao" => 'Aguardando confirmação')
            ,array("id" => 4, "descricao" => 'Confirmado')
            ,array("id" => 5, "descricao" => 'Importado')
            );
    }

    public static function statusLiberadosParaEdicaoCliente(){
        return "1,2";
    }

    public static function statusLiberadosParaEdicaoOperacional(){
        return self::CONFIRMADO;
    }

    public static function statusLiberadosParaImportacao(){
        return self::CONFIRMADO;
    }
}
