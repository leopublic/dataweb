<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$id_solicita_visto = $_GET['id_solicita_visto'];
$FL_OS = $_GET['FL_OS'];
$FL_CANDIDATO = $_GET['FL_CANDIDATO'];
$NU_CANDIDATO = $_GET['NU_CANDIDATO'];

$OS = new cORDEMSERVICO();
$OS->Recuperar($id_solicita_visto);
if ($FL_CANDIDATO!=1){
	$OS->RecuperarCandidatos($id_solicita_visto);
	$arrayIds = explode(",",$OS->mCandidatos);
	$NU_CANDIDATO = $arrayIds[0];
}

if ($FL_CANDIDATO==1){
	$sql = "select NOME_COMPLETO from CANDIDATO where NU_CANDIDATO = ".$NU_CANDIDATO ;
	$res=conectaQuery($sql, __FILE__);
	if($rs = mysql_fetch_array($res)){
		$NOME_COMPLETO = $rs['NOME_COMPLETO'];
	}
	$titulo = ' ('.$NOME_COMPLETO.')';
}
else{
	$titulo = ' (OS)';
}

?>
<table class="edicao dupla">
				<col width="24px"></col>
				<col width="250px"></col>
				<col width="auto"></col>
				<tr class="subTitulo">
				    <td colspan="3">Formul&aacute;rios <?=$titulo;?></td>
				</tr>
<?
if($OS->mID_TIPO_ACOMPANHAMENTO!= ''){
	$sql = "select TF.ID_TIPO_FORMULARIO, NO_TIPO_FORMULARIO, CD_ARQUIVO, NU_CANDIDATO, NM_FORMULARIO, NO_PAGINA_GERACAO, NO_PEDIDO ";
	$sql .= " from TIPO_FORMULARIO TF";
	$sql .= "      join FORMULARIO_ACOMPANHAMENTO FA ON FA.ID_TIPO_FORMULARIO = TF.ID_TIPO_FORMULARIO AND FA.ID_TIPO_ACOMPANHAMENTO = ".$OS->mID_TIPO_ACOMPANHAMENTO;
	$sql .= " left join ARQUIVOS_FORMULARIOS      AF ON AF.ID_TIPO_FORMULARIO = TF.ID_TIPO_FORMULARIO AND AF.id_solicita_visto = ".$OS->mID_SOLICITA_VISTO;
	if ($FL_CANDIDATO==1){
		$sql .= ' AND AF.NU_CANDIDATO = '.$NU_CANDIDATO;
	}
	$sql .= " where FL_OS = ".$FL_OS;
	$sql .= "   and FL_CANDIDATO = ".$FL_CANDIDATO;
}
else{
	$sql = "select TF.ID_TIPO_FORMULARIO, NO_TIPO_FORMULARIO, CD_ARQUIVO, NU_CANDIDATO, NM_FORMULARIO, NO_PAGINA_GERACAO, NO_PEDIDO ";
	$sql .= " from TIPO_FORMULARIO TF";
	$sql .= " left join ARQUIVOS_FORMULARIOS      AF ON AF.ID_TIPO_FORMULARIO = TF.ID_TIPO_FORMULARIO AND AF.id_solicita_visto = ".$OS->mID_SOLICITA_VISTO;
}
$res=conectaQuery($sql, __FILE__);
while($rs = mysql_fetch_array($res)){
	if($rs['NM_FORMULARIO']!=''){
		if ($FL_CANDIDATO==1){
			$urlArquivo =$myforurl.'/OS'.$OS->mID_SOLICITA_VISTO.'/'.$rs['NU_CANDIDATO'].'/'.$rs['NM_FORMULARIO'];
		}
		else{
			$urlArquivo =$myforurl.'/OS'.$OS->mID_SOLICITA_VISTO.'/'.$rs['NM_FORMULARIO'];
		}
		$urlArquivo ='<a href="#" onclick="window.open(\''.$urlArquivo.'\');var x = setTimeout(\'window.location.reload()\',5000 );">'.$rs['NO_TIPO_FORMULARIO'].'</a>';
		$botao = '<input type="submit" value="Regerar" style="width:auto"/>';
		$imagem = "/imagens/icons/accept.png";
	}
	else{
		$urlArquivo = $rs['NO_TIPO_FORMULARIO'];
		$imagem = "/imagens/icons/cross.png";
		$botao = '<input type="submit" value="Gerar" style="width:auto"/>';
	}

	echo '<tr><td style="vertical-align:middle"><img src="'.$imagem.'" /></td><td style="vertical-align:middle">'.$urlArquivo.'</td>';
	echo '<td style="vertical-align:middle">';
	if(substr($rs['NO_PAGINA_GERACAO'],0,5) == 'cCTRL'){
		echo '<form method="post" action="/paginas/carregueDownload.php?controller=' . $rs['NO_PAGINA_GERACAO'] .'&metodo=Gerar" target="_blank">'."\n";
		if ($NU_CANDIDATO!=''){
			echo '<input type="hidden" name="nu_candidato" value="'.$NU_CANDIDATO.'">'."\n";
		}
	}
	else{
		echo '<form method="post" action="/formularios/' . $rs['NO_PAGINA_GERACAO'] .'" target="_blank">'."\n";
		if ($NU_CANDIDATO!=''){
			echo '<input type="hidden" name="idCandidato" value="'.$NU_CANDIDATO.'">'."\n";
		}
	}
	echo '<input type="hidden" name="idEmpresa" value="'.$OS->mNU_EMPRESA.'">'."\n";
	echo '<input type="hidden" name="pedido" value="'.$rs['NO_PEDIDO'].'">'."\n";
	echo '<input type="hidden" name="ID_TIPO_FORMULARIO" value="'.$rs['ID_TIPO_FORMULARIO'].'">'."\n";
	echo '<input type="hidden" name="tpSolicitacao" value="novo">'."\n";
	echo cINTERFACE::inputHidden('id_solicita_visto',$OS->mID_SOLICITA_VISTO)."\n";
	if(!$OS->mReadonly){
		echo 'Tamanho letra:<select name="font-size" style="width:auto"><option value="12px">12</option><option value="10px">10</option><option value="8px">8</option></select>&nbsp;';
		echo 'Procurador:<select name="idProcurador" style="width:auto">'.montaComboProcurador("",$OS->mNU_EMPRESA," order by NU_ORDEM").'</select> e <select name="idProcurador2" style="width:auto"><option value="">(opcional</option>'.montaComboProcurador("",$OS->mNU_EMPRESA,"").'</select>';
		echo '<select name="assinatura" style="width:auto"><option value="0">sem assinatura</option><option value="1">com assinatura</option></select>&nbsp;';
		echo $botao;
	}
	echo '</form></td></tr>'."\n";
}
?>
</table>
