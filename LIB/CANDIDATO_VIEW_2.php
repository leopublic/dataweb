<?php
header('Content-Type: text/html; charset=UTF-8');
include ("autenticacao.php");
include ("geral.php");
include ("../LIB/combos.php");
include ("../LIB/componentes/cCOMBO.php");
include ("../LIB/classes/cBANCO.php");
include ("../LIB/classes/cCANDIDATO.php");
include ("../LIB/classes/cORDEMSERVICO.php");
include ("../LIB/classes/cINTERFACE.php");
include ("../LIB/classes/cAUTORIZACAO_CANDIDATO.php");

$editaCandidato = new cEDICAO("vCANDIDATO");
$editaCandidato->CarregarConfiguracao();


$pNU_CANDIDATO  	= cINTERFACE::GetValido($_GET,'NU_CANDIDATO');
$pID_SOLICITA_VISTO = cINTERFACE::GetValido($_GET,'ID_SOLICITA_VISTO');

if ($pNU_CANDIDATO=='')
{

}
else
{
	$ac = new cAUTORIZACAO_CANDIDATO();
	$ac->Recuperar($pID_SOLICITA_VISTO, $pNU_CANDIDATO);
	$cand = new cCANDIDATO();
	$cand->Recuperar($pNU_CANDIDATO);
	?>
<div id="accordionCand" class="accordion">

<h3 class="interno">Dados da solicitação</h3>
<div>
<table class="edicao dupla">
	<tr>
		<th width="18%">N&uacute;mero da Solicita&ccedil;&atilde;o:</th>
		<td width="31%"><?=$ac->mNU_SOLICITACAO;?></td>
		<td>&#160;</td>
		<th width="18%"><font color="Red">*</font>Prazo Solicitado:</th>
		<td width="31%"><?=cINTERFACE::inputData("DT_PRAZO_ESTADA_SOLICITADO",$ac->mDT_PRAZO_ESTADA_SOLICITADO);?></td>
	</tr>

	<tr>
		<th><font color="Red">*</font>Tipo de Visto:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbTIPO_AUTORIZACAO, $ac->mCO_TIPO_AUTORIZACAO);?></td>
		<td>&#160;</td>
		<th>RNE:</th>
		<td><?=cINTERFACE::inputTexto("NU_RNE",$cand->mNU_RNE,"10", "30");?></td>
	</tr>

	<tr>
		<th><font color="Red">*</font>Projeto/Embarca&ccedil;&atilde;o Atual:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbEMBARCACAO_PROJETO, $ac->mNU_EMBARCACAO_PROJETO, "where NU_EMPRESA = ".$ac->mNU_EMPRESA);?></td>
		<td>&#160;</td>
		<th>Embarcado:</th>
		<td><?=cINTERFACE::inputSimNao('CD_EMBARCADO', $ac->mCD_EMBARCADO);?></td>
	</tr>

	<tr>
		<th><font color="Red">*</font>Projeto/Embarca&ccedil;&atilde;o Inical:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbEMBARCACAO_PROJETO, $ac->mNU_EMBARCACAO_INICIAL);?></td>
		<td>&#160;</td>
		<th>Armador:</th>
		<td></td>
	</tr>

	<tr>
		<th><font color="Red">*</font>Reparti&ccedil;&atilde;o Consular:</th>
		<td colspan=4><?=cCOMBO::campoHTML(cCOMBO::cmbREPARTICAO_CONSULAR, $ac->mCO_REPARTICAO_CONSULAR);?></td>
	</tr>
	</table>
</div>
<h3  class="interno">Identificação do Candidato</h3>
<div>
<table class="edicao dupla">
	<tr>
		<th><font color="Red">*</font>Primeiro nome:</th>
		<td><?=cINTERFACE::inputTexto("NO_PRIMEIRO_NOME", $cand->mNO_PRIMEIRO_NOME);?></td>
		<td>&#160;</td>
		<th>Nome do meio:</th>
		<td><?=cINTERFACE::inputTexto("NO_NOME_MEIO", $cand->mNO_NOME_MEIO);?></td>
	</tr>

	<tr>
		<th>&Uacute;ltimo nome:</th>
		<td><?=cINTERFACE::inputTexto("NO_ULTIMO_NOME", $cand->mNO_ULTIMO_NOME);?></td>
		<td>&#160;</td>
		<th>CPF:</th>
		<td><?=montaCampos("NU_CPF",formatarCPF_CNPJ($cand->mNU_CPF, true),$cand->mNU_CPF,"T",$acao,"maxlength=14 size=30  onkeyup=\"javascript:criaMascara(this,'###.###.###-##');\"")?></td>
	</tr>

	<tr>
		<th><font color="Red">*</font>Email:</th>
		<td colspan="4"><?=cINTERFACE::inputTexto("NO_EMAIL_CANDIDATO",$cand->mNO_EMAIL_CANDIDATO,'60');?></td>
	</tr>
	<tr>
		<th><font color="Red">*</font>Nome do Pai:</th>
		<td><?=cINTERFACE::inputTexto("NO_PAI",$cand->mNO_PAI,"60");?></td>
		<td>&#160;</td>
		<th><font color="Red">*</font>Nome da M&atilde;e:</th>
		<td><?=cINTERFACE::inputTexto("NO_MAE",$cand->mNO_MAE,"60");?></td>
	</tr>

	<tr>
		<th><font color="Red">*</font>Estado civil:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbESTADO_CIVIL,$cand->mCO_ESTADO_CIVIL);?></td>
		<td>&#160;</td>
		<th><font color="Red">*</font>Sexo:</th>
		<td><?php
		if($acao=="V") {
			if($cand->mCO_SEXO =="F") { $nmSexo="Feminino"; } else { $nmSexo="Masculino"; }
			echo montaCampos("sexo",$sexo,$nmSexo,"V",$acao,"");
		} else {
			if($cand->mCO_SEXO=="F") { $nmSexoF="checked"; } else { $nmSexoF=""; }
			if($cand->mCO_SEXO=="M") { $nmSexoM="checked"; } else { $nmSexoM=""; }
			echo montaCampos("sexo","F","Feminino","R",$acao,$nmSexoF);
			echo "&nbsp;&nbsp;".montaCampos("sexo","M","Masculino","R",$acao,$nmSexoM);
		}
		?></td>
	</tr>
	<tr>
		<th>Data de nascimento:</th>
		<td><?=cINTERFACE::inputData("DT_NASCIMENTO", $cand->mDT_NASCIMENTO);?></td>
		<td>&#160;</td>
		<th>Local de nascimento:</th>
		<td><?=cINTERFACE::inputTexto('NO_LOCAL_NASCIMENTO', $cand->mNO_LOCAL_NASCIMENTO,'40');?></td>
	</tr>
	<tr>
		<th><font color="Red">*</font>Nacionalidade:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbNACIONALIDADE, $cand->mCO_NACIONALIDADE);?></td>
		<td>&#160;</td>
		<th><font color="Red">*</font>N&iacute;vel de escolaridade:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbESCOLARIDADE, $cand->mCO_NIVEL_ESCOLARIDADE)?></td>
	</tr>

	<tr>
		<th><font color="Red">*</font>Profiss&atilde;o:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbPROFISSAO, $cand->mCO_PROFISSAO_CANDIDATO);?></td>
		<td>&#160;</td>
		<th><font color="Red">*</font>Fun&ccedil;&atilde;o:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbFUNCAO, $ac->mCO_FUNCAO_CANDIDATO);?></td>
	</tr>

	<tr>
		<th><font color="Red">*</font>Sal&aacute;rio Total em Reais:</th>
		<td><?=cINTERFACE::inputTexto("VA_REMUNERACAO_MENSAL_BRASIL", $ac->mVA_REMUNERACAO_MENSAL_BRASIL);?></td>
		<td>&#160;</td>
		<th><font color="Red">*</font>Sal&aacute;rio no Brasil:</th>
		<td><?=cINTERFACE::inputTexto("VA_REMUNERACAO_MENSAL", $ac->mVA_RENUMERACAO_MENSAL);?></td>
	</tr>

	<tr>
		<th>Benef&iacute;cios Sal&aacute;rio:</th>
		<td colspan="4"><?=montaCampos("DS_BENEFICIOS_SALARIO",$ac->mDS_BENEFICIOS_SALARIO,$ac->mDS_BENEFICIOS_SALARIO,"A",$acao,"cols=60 rows=3")?></td>
	</tr>

	<tr class="subTitulo">
		<td colspan="5">Endereço do Candidato no Brasil</td>
	</tr>
	<tr>
		<th><font color="Red">*</font>Endere&ccedil;o:</th>
		<td colspan="4"><?=cINTERFACE::inputTexto("NO_ENDERECO_RESIDENCIA",$cand->mNO_ENDERECO_RESIDENCIA);?></td>
	</tr>
	<tr>
		<th><font color="Red">*</font>Cidade</th>
		<td><?=cINTERFACE::inputTexto("NO_CIDADE_RESIDENCIA ",$cand->mNO_CIDADE_RESIDENCIA)?></td>
		<td>&#160;</td>
		<th><font color="Red">*</font>Pa&iacute;s:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbPAIS, $cand->mCO_PAIS_RESIDENCIA, "", "CO_PAIS_RESIDENCIA");?></td>
	</tr>
	<tr>
		<th><font color="Red">*</font>Telefone:</th>
		<td><?=cINTERFACE::inputTexto("NU_TELEFONE_CANDIDATO",$cand->mNU_TELEFONE_CANDIDATO ,$telefone,"T",$acao,"maxlength=60 size=30")?></td>
		<td>&#160;</td>
		<td>&#160;</td>
		<td>&#160;</td>
	</tr>

	<tr class="subTitulo">
		<td colspan="5">Endereços no Exterior</td>
	</tr>
	<tr>
		<th>Nome da Empresa no Exterior:</th>
		<td><?=cINTERFACE::inputTexto("NO_EMPRESA_ESTRANGEIRA",$cand->mNO_EMPRESA_ESTRANGEIRA)?></td>
		<td>&#160;</td>
		<th>Tel. da empresa no exterior:</th>
		<td><?=cINTERFACE::inputTexto("NU_TELEFONE_EMPRESA",$cand->mNU_TELEFONE_EMPRESA)?></td>
	</tr>

	<tr>
		<th>Endere&ccedil;o da Empresa:</th>
		<td colspan="4"><?=cINTERFACE::inputTexto("NO_ENDERECO_EMPRESA",$cand->mNO_ENDERECO_EMPRESA);?></td>
	</tr>

	<tr>
		<th>Cidade da Empresa:</th>
		<td><?=cINTERFACE::inputTexto("NO_CIDADE_EMPRESA",$cand->mNO_CIDADE_EMPRESA);?></td>
		<td>&#160;</td>
		<th>Pa&iacute;s da Empresa:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbPAIS, $cand->mCO_PAIS_EMPRESA, "", "CO_PAIS_EMPRESA");?></td>
	</tr>

	<tr>
		<th>Endere&ccedil;o do Candidato:</th>
		<td colspan="4"><?=cINTERFACE::inputTexto("NO_ENDERECO_ESTRANGEIRO ",$cand->mNO_ENDERECO_ESTRANGEIRO);?></td>
	</tr>

	<tr>
		<th>Cidade do Candidato:</th>
		<td><?=cINTERFACE::inputTexto("NO_CIDADE_ESTRANGEIRO",$cand->mNO_CIDADE_ESTRANGEIRO);?></td>
		<td>&#160;</td>
		<th><font color="Red">*</font>Pa&iacute;s de Resid&ecirc;ncia:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbPAIS,$cand->mCO_PAIS_ESTRANGEIRO, "","CO_PAIS_ESTRANGEIRO");?></td>
	</tr>

	<tr>
		<th>Telefone do Candidato:</th>
		<td><?=cINTERFACE::inputTexto("NU_TELEFONE_ESTRANGEIRO",$cand->mNU_TELEFONE_ESTRANGEIRO);?></td>
		<td>&#160;</td>
		<td>&#160;</td>
		<td>&#160;</td>
	</tr>

	</table>

</div>
<h3  class="interno">Dados do Passaporte</h3>
<div>
<table class="edicao dupla">
	<tr>
		<th>N&uacute;mero do passaporte:</th>
		<td><?=montaCampos("NU_PASSAPORTE",$cand->mNU_PASSAPORTE,$cand->mNU_PASSAPORTE,"T",$acao,"maxlength=30 size=30")?></td>
		<td>&#160;</td>
		<th><font color="Red">*</font>Pa&iacute;s emissor do passaporte:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbPAIS, $cand->mCO_PAIS_EMISSOR_PASSAPORTE, "","CO_PAIS_EMISSOR_PASSAPORTE" );?></td>
	</tr>

	<tr>
		<th>Data de emiss&atilde;o do passaporte:</th>
		<td><?=cINTERFACE::inputData("DT_EMISSAO_PASSAPORTE",$cand->mDT_EMISSAO_PASSAPORTE);?></td>
		<td>&#160;</td>
		<th>Data validade passaporte:</th>
		<td><?=cINTERFACE::inputData("DT_VALIDADE_PASSAPORTE",$cand->mDT_VALIDADE_PASSAPORTE);?></td>
	</tr>

	</table>
</div>
<h3  class="interno">Outras Informações do Candidato</h3>
<div>
<table class="edicao dupla">
	<tr>
		<th>Experi&ecirc;ncia Profissional:</th>
		<td colspan="4"><?=montaCampos("TE_TRABALHO_ANTERIOR_BRASIL",$cand->mTE_TRABALHO_ANTERIOR_BRASIL ,$cand->mTE_TRABALHO_ANTERIOR_BRASIL,"A",$acao,"rows=3 cols=60")?></td>
	</tr>

	<tr>
		<th><font color="Red">*</font>Descri&ccedil;&atilde;o das
		Atividades:</th>
		<td colspan="4"><?=montaCampos("TE_DESCRICAO_ATIVIDADES",$ac->mTE_DESCRICAO_ATIVIDADES,$ac->mTE_DESCRICAO_ATIVIDADES,"A",$acao,"cols=60 rows=5")?></td>
	</tr>

	<tr>
		<th>Justificativa:</th>
		<td colspan="4"><?=montaCampos("DS_JUSTIFICATIVA",$ac->mDS_JUSTIFICATIVA,$ac->mDS_JUSTIFICATIVA,"A",$acao,"rows=4 cols=60")?></td>
	</tr>

	<?php   if($ac->mNU_EMPRESA != 21)  {  ?>

	<tr>
		<th>Local de Trabalho:</th>
		<td colspan="4"><?=montaCampos("DS_LOCAL_TRABALHO",$ac->mDS_LOCAL_TRABALHO,$ac->mDS_LOCAL_TRABALHO,"A",$acao,"rows=2 cols=60")?></td>
	</tr>

	<tr>
		<th>Declara&ccedil;&atilde;o de remunera&ccedil;&atilde;o:</th>
		<td colspan="4"><?=montaCampos("DS_SALARIO_EXTERIOR",$ac->mDS_SALARIO_EXTERIOR,$ac->mDS_SALARIO_EXTERIOR,"A",$acao,"rows=4 cols=60")?></td>
	</tr>
	<tr class="subTitulo">
		<td colspan="5">Dados do Visto</td>
 </tr>
 <tr>
  <th>Valido Por:</th>
  <td><?=montaCampos("DT_VALIDADE_VISTO",$ac->mDT_VALIDADE_VISTO,$ac->mDT_VALIDADE_VISTO,"T",$acao,"maxlength=15 size=10")?></td>
  <td>&#160;</td>
  <th>N&uacute;mero do Visto:</th>
  <td><?=montaCampos("NU_VISTO",$ac->mNU_VISTO,$ac->mNU_VISTO,"T",$acao,"maxlength=20 size=30")?></td>
 </tr>
 <tr>
  <th>Data de Emiss&atilde;o:</th>
  <td><?=montaCampos("DT_EMISSAO_VISTO",$ac->mDT_EMISSAO_VISTO,$ac->mDT_EMISSAO_VISTO,"D",$acao,"")?></td>
  <td>&#160;</td>
  <th>Local de Emiss&atilde;o:</th>
  <td><?=montaCampos("NO_LOCAL_EMISSAO_VISTO",$ac->mNO_LOCAL_EMISSAO_VISTO,$ac->mNO_LOCAL_EMISSAO_VISTO,"T",$acao,"maxlength=60 size=30")?></td>
 </tr>
 <tr>
  <th>Pais de Emiss&atilde;o do Visto:</th>
	<td><?=cCOMBO::campoHTML(cCOMBO::cmbNACIONALIDADE, $ac->mCO_NACIONALIDADE_VISTO, "CO_NACIONALIDADE_VISTO");?></td>
  <td>&#160;</td>
  <th>Classifica&ccedil;&atilde;o Visto:</th>
  <td><?=cCOMBO::campoHTML(cCOMBO::cmbCLASSIFICA_VISTO, $ac->mCO_CLASSIFICACAO_VISTO)?></td>
 </tr>
 <tr>
  <th>Local de Entrada: (cidade)</th>
  <td><?=montaCampos("NO_LOCAL_ENTRADA",$ac->mNO_LOCAL_ENTRADA,$ac->mNO_LOCAL_ENTRADA,"T",$acao,"maxlength=60 size=30")?></td>
  <td>&#160;</td>
  <th>UF de Entrada:</th>
  <td><?=montaCampos("NO_UF_ENTRADA",$ac->mNO_UF_ENTRADA,$ac->mNO_UF_ENTRADA,"S",$acao,"")?></td>
 </tr>
 <tr>
  <th>Data de Entrada:</th>
  <td><?=montaCampos("DT_ENTRADA",$ac->mDT_ENTRADA,$ac->mDT_ENTRADA,"D",$acao,"")?></td>
		<td>&#160;</td>
		<th>Meio de Transporte:</th>
		<td><?=cCOMBO::campoHTML(cCOMBO::cmbTRANSPORTE_ENTRADA, $ac->mNU_TRANSPORTE_ENTRADA);?></td>
	</tr>
	<?php   }    ?>
	</table>
</div>
<h3  class="interno">Alterações nos Dados do Candidato</h3>
<div>
<table class="edicao">
	<tr>
		<th>Data do Cadastro:</th>
		<td><?=$ac->mDT_CADASTRAMENTO;?></td>
		<td>&#160;</td>
		<th>Admin Cadastro:</th>
		<td><?=$ac->mNU_USUARIO_CAD;?></td>
	</tr>
	<tr>
		<th>&Uacute;ltima Altera&ccedil;&atilde;o:</th>
		<td><?=$dtUsuAlt?></td>
		<td>&#160;</td>
		<th>Admin Altera&ccedil;&atilde;o:</th>
		<td><?=$nmUsuAlt?></td>
	</tr>
</table>
</div>
<?php
	$sql = $cand->rsArquivos();
	$rs = mysql_query($sql);
	$qtdArqs = 0;
	$arquivos = "";
	while($rw = mysql_fetch_array($rs)) {
		$nmDir = $mydocurl."/".$rw['NU_EMPRESA']."/".$rw['NU_CANDIDATO']."/";
		$qtdArqs = $qtdArqs + 1;
		$seq = $rw['NU_SEQUENCIAL'];
		$nmArq = $rw['NO_ARQUIVO'];
		$tpArq = $rw['TP_ARQUIVO'];
		$nmTpArq = $rw['NO_TIPO_ARQUIVO'];
		$nmArqOrg = $rw['NO_ARQ_ORIGINAL'];
		$dtInc = $rw['DT_INCLUSAO'];
		$numAdmin = $rw['NU_ADMIN'];
		$docpagina = $nmDir.$nmArq;
		$arq = explode(".",$nmArq );

		if (count($arq)>1)
		{
			$src = $mydocdir."/".$rw['NU_EMPRESA']."/".$rw['NU_CANDIDATO']."/".$rw['NO_ARQUIVO'];
			if (file_exists($src))
			{
				if (strtoupper($arq[1])=="JPG"||strtoupper($arq[1])=="TIFF"||strtoupper($arq[1])=="BMP")
				{
					$link ="<a href=\"javascript:CarregarArquivo('".$docpagina."', '".$seq."');\">" ;
					$link .= $nmTpArq."(".$nmArqOrg.")</a>";
				}
				else
				{
					$link = "<a href=\"".$docpagina."\" target=\"_blank\">";
					$link .= $nmTpArq."(".$nmArqOrg.")</a>";
				}
			}
			else
			{
				$link = '<span style="text-decoration:line-through;color:#cccccc;">';
				$link .= $nmTpArq."(".$nmArqOrg.")</span>&nbsp;(arquivo não encontrado)";
			}
		}
		$arquivos.= "<li>";
		if( ($usufunc=="S") || ($numAdmin==$usulogado) ) {
			$arquivos.='<img src="/images/bullet_delete.png" title="clique para remover esse arquivo" onclick="ajaxRemover(\'ARQUIVO\', \''.$rs['NU_SEQUENCIAL'].'\');" />';
	    }
		$arquivos.=$link;
		$arquivos.= "</li>";
	}
	if ($arquivos!="")
	{
		$arquivos = '<ul style="list-style-type:none;padding:0;margin:0;">'.$arquivos.'</ul>';
		$qtdArqs = ' ('.$qtdArqs.' arquivos disponíveis)';
	}
	else
	{
		$qtdArqs = ' (nenhum arquivo disponível)';
	}
?>
<h3 class="interno">Arquivos<?=$qtdArqs;?></h3>
<div>
	<div id="containerVisualizacao" style="float:right;border:solid 1px #999999;padding:3px;text-align:center;">
	</div>
	<?=$arquivos;?>
	Adicionar arquivos

	<script>
		function CarregarArquivo(pCaminho,pNU_SEQUENCIAL)
		{
		  	var dimensoes = new Array();
			$.ajax({
				  url: '/LIB/imagemTam.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL,
				  async:false,
				  success: function(data) {
					dimensoes = data.split("#");

				  }
				});
			var pWidth = +dimensoes[0];
			var pHeight = +dimensoes[1];
			//alert('pWidth='+pWidth+';pHeight='+pHeight);
			var maxW = 400;
			var maxH = 300;
			var w = maxW;
			var h = maxH;
			if (pWidth>pHeight)
			{
				h = Math.round(pHeight/pWidth * maxW);
			}
			else
			{
				w = Math.round(pWidth/pHeight * maxH);
			}
			$('#containerVisualizacao').html('<img src="'+pCaminho+'"/><br/><input type="button" onclick="abreJanelaxxx(\'/LIB/editaImagem.php?NU_SEQUENCIAL='+pNU_SEQUENCIAL+'\',\'wteste\');" value="Editar"/><input type="button" value="Atualizar" onclick="CarregarArquivo(\''+pCaminho+'\',\''+pNU_SEQUENCIAL+'\', '+pWidth+', '+pHeight+')"/>');
			$("#containerVisualizacao img").imagetool({
			    viewportWidth: w
			   ,viewportHeight: h
			  });
		}
	</script>
</div>

<h3 class="interno">Solicitações anteriores</h3>
<div>
<?php
// Montagem do histórico das solicitações anteriores
	$tabs = '';
	$sql = " select NU_SOLICITACAO, ID_SOLICITA_VISTO ";
	$sql .= "  from AUTORIZACAO_CANDIDATO ";
	$sql .= " WHERE NU_CANDIDATO  = ".$cand->mNU_CANDIDATO;
	$sql .= "   and NU_SOLICITACAO <> ".$ac->mNU_SOLICITACAO;
	$sql .= "   and NU_SOLICITACAO <> 0";
	$rs = mysql_query($sql);
	while($rw = mysql_fetch_array($rs))
	{
		$tabs .= '<li><a href="/LIB/solicitacao.php?NU_CANDIDATO='.$cand->mNU_CANDIDATO.'&NU_SOLICITACAO='.$rw['NU_SOLICITACAO'].'"><span>'.$rw['NU_SOLICITACAO'].'</span></a></li>';
	}
	if($tabs!='')
	{
		$tabs = '<div id="tabsInt"><ul>'.$tabs.'</ul></div>';
	}
	else
	{
		$tabs = '(nenhuma solicitação encontrada)';
	}
	print $tabs;
?>
</div>

<?php
}
?>
