<?php

  

    Class PrazoVersusProrrogacao extends Controle{
        
       var $layout = 'mundivisas';
       var $title  = 'Relatório - Prazo de estada x Prorrogação';


        function onLoad(){
            if($this->isPost()){  //Só executar query se o formulário for enviado			
				$this->set('razaosocial', $this->getCampo('EMPRESA', 'NO_RAZAO_SOCIAL', 'NU_EMPRESA', $this->p('NU_EMPRESA')));
				$this->set('combo_EmbarcacaoEmpresa', $this->combo_embarcacaoempresa($this->p('NU_EMPRESA'), $_POST['NU_EMBARCACAO'] ));

				if (($_POST['relat']) && (is_array($_POST['relat']))){
					if($_POST['NU_EMBARCACAO']){ $embarcacao = ' and C.NU_EMBARCACAO_PROJETO = '.$_POST['NU_EMBARCACAO'].' ';}else{ $embarcacao = '';}
					if($this->p('DIAS_VENCIMENTO')){ $diasvencimento = ' and  ifnull(datediff(MAX_PRAZO_ESTADA.dt_prazo_estada, now()),-9999999) < '.$this->p('DIAS_VENCIMENTO').' ';}else{ $diasvencimento = '';}
					if($this->p('FL_INATIVO') && $this->p('FL_INATIVO') != '-1' ){$fl_inativo = ' and  C.FL_INATIVO = '.($this->p('FL_INATIVO') - 1).' ';}else{ $fl_inativo= '';}

					$strsql_candidatos ="
							select distinct
							  CONCAT('<a target=\"_blank\" href=/operador/detalheCandidatoAuto.php?NU_CANDIDATO=',C.NU_CANDIDATO, '>', C.NOME_COMPLETO ,' (', C.NU_CANDIDATO, ')</a>')  NOME_COMPLETO_LINK,
							  C.NOME_COMPLETO,
							  C.NU_PASSAPORTE,
							  C.NU_RNE,
							  C.FL_INATIVO,
							  C.NU_EMPRESA,
							  C.NU_CPF,
							  C.NU_EMBARCACAO_PROJETO,
							  DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,'%d/%m/%Y') DT_VALIDADE_PASSAPORTE ,
							  DATE_FORMAT(MAX_PRAZO_ESTADA.dt_prazo_estada,'%d/%m/%Y')  DT_PRAZO_ESTADA,
							  datediff(MAX_PRAZO_ESTADA.dt_prazo_estada, now()) DIAS_VENCIMENTO,
							  ifnull(QTD_PRORROG.qtd_prorrog, 0) QTD_PRORROG,

							  DATE_FORMAT(REG.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_CIE,
							  DATE_FORMAT(REG.dt_requerimento,'%Y/%m/%d')  DT_REQUERIMENTO_CIE_ORIG,
							  REG.nu_protocolo NU_PROTOCOLO_REG,

							  PRO.codigo codigo_prorrog,
							  DATE_FORMAT(PRO.dt_requerimento,'%d/%m/%Y')  DT_REQUERIMENTO_PRO,
							  DATE_FORMAT(PRO.dt_requerimento,'%Y/%m/%d') DT_REQUERIMENTO_PRO_ORIG,
							  PRO.nu_protocolo NU_PROTOCOLO_PRO,
							  DATE_FORMAT(PRO.dt_prazo_pret,'%d/%m/%Y')  DT_PRAZO_PRET_PRO,
							  datediff(PRO.dt_prazo_pret, now()) DIAS_VENCIMENTO_PRO,
							  PRO.ID_STATUS_CONCLUSAO ID_STATUS_CONCLUSAO_PRO, 
							  
							  sv.ID_STATUS_SOL,
							  sv.nu_solicitacao NU_SOLICITACAO_PRO,
							  SS.NO_STATUS_SOL,

							  MTE.nu_processo NU_PROCESSO_MTE,
							  MTE.prazo_solicitado PRAZO_SOLICITADO,
							  DATE_FORMAT(MTE.dt_requerimento,'%d/%m/%Y') DT_REQUERIMENTO ,
							  DATE_FORMAT(MTE.dt_deferimento,'%d/%m/%Y') DT_DEFERIMENTO,

							  DATE_FORMAT(COLETA.dt_entrada,'%d/%m/%Y')  DT_ENTRADA,
							  PN.NO_NACIONALIDADE,
							  F.NO_FUNCAO,
							  S.NO_SERVICO_RESUMIDO TIPO_VISTO,
							  RC.NO_REPARTICAO_CONSULAR CONSULADO,
							  EP.NO_EMBARCACAO_PROJETO,
							  SCP.NO_STATUS_CONCLUSAO
							from CANDIDATO C
							  left join processo_mte MTE on MTE.codigo = C.codigo_processo_mte_atual
								  and MTE.cd_candidato = C.NU_CANDIDATO
								  and MTE.fl_vazio = 0
							  left join (select cd_candidato, codigo_processo_mte, count(*) qtd_prorrog from processo_prorrog where codigo_processo_mte is not null group by cd_candidato, codigo_processo_mte) as QTD_PRORROG
								  on QTD_PRORROG.codigo_processo_mte = MTE.codigo
								  and QTD_PRORROG.cd_candidato = MTE.cd_candidato
							  left join (select cd_candidato, codigo_processo_mte, max(dt_prazo_estada) dt_prazo_estada from processo_regcie
										  where codigo_processo_mte is not null
										  group by cd_candidato, codigo_processo_mte) as MAX_PRAZO_ESTADA
								  on MAX_PRAZO_ESTADA.codigo_processo_mte = MTE.codigo
								  and MAX_PRAZO_ESTADA.cd_candidato = MTE.cd_candidato
							  left join processo_regcie REG
								  on REG.codigo_processo_mte = MTE.codigo
								  and REG.cd_candidato = MTE.cd_candidato
								  and REG.dt_prazo_estada = MAX_PRAZO_ESTADA.dt_prazo_estada
							  left join (select cd_candidato, codigo_processo_mte, max(dt_requerimento) dt_requerimento from processo_prorrog where codigo_processo_mte is not null group by cd_candidato, codigo_processo_mte) as MAX_PRORROG
								  on MAX_PRORROG.codigo_processo_mte = MTE.codigo
								  and MAX_PRORROG.cd_candidato = MTE.cd_candidato
								  and MAX_PRORROG.dt_requerimento >= REG.dt_requerimento
							  left join processo_prorrog PRO
								  on PRO.codigo_processo_mte = MTE.codigo
								  and PRO.cd_candidato = MTE.cd_candidato
								  and PRO.dt_requerimento = MAX_PRORROG.dt_requerimento
							  left join solicita_visto sv on sv.id_solicita_visto = PRO.id_solicita_visto
							  left join (select cd_candidato, codigo_processo_mte, max(codigo) codigo from processo_coleta where processo_coleta.codigo_processo_mte is not null group by cd_candidato, codigo_processo_mte) as MAX_COLETA
								  on MAX_COLETA.codigo_processo_mte = MTE.codigo
								  and MAX_COLETA.cd_candidato = MTE.cd_candidato
							  left join processo_coleta COLETA  on COLETA.codigo = MAX_COLETA.codigo
							  left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS
							  left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
							  left join SERVICO S on S.NU_SERVICO= MTE.NU_SERVICO
							  left join REPARTICAO_CONSULAR RC on RC.CO_REPARTICAO_CONSULAR = MTE.cd_reparticao
							  left join STATUS_SOLICITACAO SS ON SS.ID_STATUS_SOL = sv.ID_STATUS_SOL
							  left join EMBARCACAO_PROJETO EP ON EP.NU_EMPRESA = C.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = C.NU_EMBARCACAO_PROJETO
							  left join STATUS_CONCLUSAO SCP ON SCP.ID_STATUS_CONCLUSAO = PRO.ID_STATUS_CONCLUSAO
							 WHERE C.NU_EMPRESA = ".$this->p('NU_EMPRESA')." ".$embarcacao.$diasvencimento.$fl_inativo."
							 ORDER BY EP.NO_EMBARCACAO_PROJETO,  C.NOME_COMPLETO ASC
						";
					$candidatos = sql::query($strsql_candidatos)->getArray();

					//var_dump($candidatos);

					$this->set('candidatos', $candidatos);
					$this->set('SEQ', 0);
					$this->set('excel', $this->p('excel'));

					if($this->p('excel')){ // se for gerar excel não colocar o layout na pagina
						$this->layout = '';
					}
				}
            }else{  
                $this->set('combo_EmbarcacaoEmpresa', '<select id="NU_EMBARCACAO" name="NU_EMBARCACAO"><option >xxx</option></select>');  //mostrar combo vazio para preenchimento
                $this->set('excel', false);
            }
               
        }
        
        


        function isPost(){
             
             //$isPost = (bool) ( ($_POST['relat']) AND (is_array($_POST['relat'])) AND ($this->p('NU_EMPRESA')) ) ? 1 : 0;
             $isPost = (bool) ( ($this->p('NU_EMPRESA')) ) ? 1 : 0;
             $this->set('isPost', $isPost);
  
             return $isPost;
        }
        


        function save(){
            
            if($this->isPost()){
                
                // Codigo para realizar algum função ex: DELETAR / ALTERAR / INSERIR

            }  
        }
        
        
    }
