<?php

    Class usuario_acesso_edit extends Controle{
        

        /**
        * onLoad() - Método util para carregar os dados da pagina. e para carregar apos atualização
        * @return Array
        **/
        function onLoad(){

            $perfil = sql::query("SELECT * FROM usuarios WHERE flagativo = 'Y'  AND cd_usuario = ".$this->g('cod') );
           
            return $perfil->fetch();
    
        }
        
        

        
        /**
         *  isPost() - Verifica se foi passado os campos corretamnte para o formulário
         *  @return bool
         **/
        function isPost(){
            
           $this->sucesso = (bool)($this->g('cod') AND ($this->g('action')=='save') AND ($this->p('nome')) ) ? true : false;
           return $this->sucesso;  
        }
        
        
        
        /**
         *  save() - Salva os dados. se $this->sucesso estiver TRUE em todo momento ! 
         *  @return bool
         **/
        function save($login){
            
                        
            // Se enviou os dados ..
            if($this->isPost()){
                // verifico se o campo login é diferente do passado por paremetro 
                if($login <> $this->p('login')){
                    //(Para poder cadastrar/alterar nao pode ter outro LOGIN igual)
                    $existe = sql::query("SELECT cd_usuario FROM usuarios where login = '".$this->p('login')."' ")->count();
                    if($existe > 0){
                        // erro - pois nao pode cadastrar
                        $this->sucesso = false;
                        // msg do erro - p/ exibir msgHtml
                        $this->msgErro =  '
                                Não foi possível gravar pois já existe uma pessoa com este mesmo login. <br />
                                Por favor verifique ! <a href="usuarios_acesso_edit.php?cod='.$this->g('cod').'"> Clique aqui para remover este aviso. </a>
                        ';  
                    }
                }    
                
                // Se estiver tudo certo ..
                if($this->sucesso){
                    // iremos alterar o registro do banco via string SQL (UPDATE)                    
                    $strsql =
                    "
                        UPDATE usuarios SET
                            cd_perfil   = ".$this->p('cd_perfil').",
                            login       = '".$this->p('login')."',
                            nm_senha    = '".$this->p('nm_senha')."',
                            nome        = '".$this->p('nome')."',
                            nm_email    = '".$this->p('nm_email')."',
                            cd_empresa    = '".$this->p('NU_EMPRESA')."',
                            cd_superior = ".$this->fk($this->p('cd_superior'))."                   
                        WHERE cd_usuario  = ".$this->g('cod')."    
                    ";
                    error_log ("usuarios->alterar usuario=".cSESSAO::$mcd_usuario." alterado=".$this->g('cod')." sql=".$strsql );

                    $usuario = sql::query($strsql);
                    
                    // Se executou com sucesso. mostramos a msg  ..
                    if($usuario->saved()){
                        
                        $this->msgSucesso = '
                               Os dados do usuário <b>'.$this->p('nome').'</b> foram salvas com sucesso ! 
                               <a href="usuarios_acesso.php">Voltar para a listagem de usuários</a> ou
                               <a href="usuarios_acesso_edit.php?cod='.$this->g('cod').'"> remover este aviso. </a>
                        '; 
                    }
               
                }   
                
            }
            
            return $this->sucesso;
            
        }
        
        
        
        function msgHtml(){
            if($this->sucesso){
                return ' <div class="success">'.$this->msgSucesso.'</div>';
                
            }else{
                return '<div class="error">'.$this->msgErro.'</div>';
            }
        }
        
        
    }
