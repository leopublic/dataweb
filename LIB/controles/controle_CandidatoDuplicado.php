<?php

  

    Class CandidatoDuplicado extends Controle{
        
        var $layout = 'mundivisas';
        var $title  = '';
        
        

        function onLoad(){
            
           
            if($this->isPost()){  //Só executar query se o formulário for enviado
            
            
                if($this->p('NU_EMBARCACAO')){ $embarcacao = ' and C.NU_EMBARCACAO_PROJETO = '.$this->p('NU_EMBARCACAO');}else{ $embarcacao = '';}
            
                    //                                    (select count(*) from CANDIDATO where NOME_COMPLETO like replace(replace(replace(replace(replace(C.NOME_COMPLETO, \'  \', \' \'),\'  \', \' \'),\'  \', \' \'),\'  \', \' \'),\' \', \'%\') and C.NU_CANDIDATO <> CANDIDATO.NU_CANDIDATO ) CADASTRO_DUPLICADO,
            
                    $strsql_candidatos = 
                    '   select      concat(concat(concat(\'<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=\',C.NU_CANDIDATO),\'">\',C.NOME_COMPLETO),\'</a>\') NOME_COMPLETO, 

                                    PN.NO_NACIONALIDADE, 
                                    F.NO_FUNCAO,
                                    C.NU_PASSAPORTE,
                                    DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE,"%d/%m/%Y") DT_VALIDADE_PASSAPORTE ,
                                    C.NU_RNE,
                                    DATE_FORMAT(MTE.dt_deferimento,"%d/%m/%Y") DT_DEFERIMENTO,
                                    PRO.NU_PROTOCOLO NU_PROTOCOLO_PRO,
                                    REG.NU_PROTOCOLO NU_PROTOCOLO_REG,
                                    DATE_FORMAT(REG.dt_prazo_estada,"%d/%m/%Y")  DT_PRAZO_ESTADA,
                                    datediff(REG.dt_validade,now()) DT_VALIDADE_REG,
                                    datediff(PRO.dt_validade,now()) DT_VALIDADE_PRO,
                                    DATE_FORMAT(AC.DT_ENTRADA ,"%d/%m/%Y") DT_ENTRADA,
                                    DATE_FORMAT(PRO.dt_requerimento ,"%d/%m/%Y") DT_REQUERIMENTO_PRO,
                                    DATE_FORMAT(REG.dt_requerimento ,"%d/%m/%Y") DT_REQUERIMENTO_CIE,
                                    DATE_FORMAT(PRO.dt_prazo_pret  ,"%d/%m/%Y") DT_PRAZO_PRET_PRO,
                                    MTE.nu_processo NU_PROCESSO,
                                    DATE_FORMAT(MTE.dt_requerimento  ,"%d/%m/%Y") DT_REQUERIMENTO,
                                    RC.NO_REPARTICAO_CONSULAR CONSULADO,
                                    MTE.prazo_solicitado PRAZO_SOLICITADO,
                                    AC.VA_RENUMERACAO_MENSAL SALARIO_MENSAL,
                                    AC.VA_REMUNERACAO_MENSAL_BRASIL SALARIO_BRASIL,
                                    TA.NO_REDUZIDO_TIPO_AUTORIZACAO TIPO_VISTO      
                             from CANDIDATO C    
                        left join processo_mte MTE on MTE.cd_candidato = C.NU_CANDIDATO and MTE.codigo = (select max(codigo) from processo_mte where cd_candidato = C.NU_CANDIDATO)
                        left join processo_prorrog PRO on PRO.cd_candidato = C.NU_CANDIDATO and PRO.codigo_processo_mte = MTE.codigo
                        left join processo_regcie REG on REG.cd_candidato = C.NU_CANDIDATO and REG.codigo_processo_mte = MTE.codigo
                        left join processo_emiscie EMIS on EMIS.cd_candidato = C.NU_CANDIDATO and EMIS.codigo_processo_mte = MTE.codigo
                        left join processo_cancel CANC on CANC.cd_candidato = C.NU_CANDIDATO and CANC.codigo_processo_mte = MTE.codigo
                        left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS 
                        left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao
                        left join AUTORIZACAO_CANDIDATO AC 
                               ON AC.NU_CANDIDATO = C.NU_CANDIDATO
                        LEFT JOIN SERVICO S
                               ON AC.CO_TIPO_AUTORIZACAO = S.CO_TIPO_AUTORIZACAO
                        LEFT JOIN TIPO_AUTORIZACAO TA
                               ON S.CO_TIPO_AUTORIZACAO = TA.CO_TIPO_AUTORIZACAO
                        LEFT JOIN REPARTICAO_CONSULAR RC
                               ON MTE.cd_reparticao = RC.CO_REPARTICAO_CONSULAR 
                            WHERE AC.NU_SOLICITACAO IN (SELECT MAX(NU_SOLICITACAO) FROM AUTORIZACAO_CANDIDATO where NU_CANDIDATO = AC.NU_CANDIDATO)  
                              AND C.NU_EMPRESA = '.$this->p('NU_EMPRESA').' '.$embarcacao.' 
                         ORDER BY C.NOME_COMPLETO ASC            
                    
                    ';
                    
                $candidatos = sql::query($strsql_candidatos)->getArray();

                $this->set('razaosocial', $this->getCampo('EMPRESA', 'NO_RAZAO_SOCIAL', 'NU_EMPRESA', $this->p('NU_EMPRESA')));
                $this->set('projeto', $this->getCampo('EMBARCACAO_PROJETO', 'NO_EMBARCACAO_PROJETO'  ,'NU_EMBARCACAO_PROJETO',  $this->p('NU_EMBARCACAO') ));
                $this->set('combo_EmbarcacaoEmpresa', $this->combo_embarcacaoempresa($this->p('NU_EMPRESA'), $this->p('NU_EMBARCACAO') ));
                $this->set('candidatos', $candidatos);
                $this->set('SEQ', 0);
                
            }else{  
                $this->set('combo_EmbarcacaoEmpresa', '<select id="NU_EMBARCACAO" name="NU_EMBARCACAO"></select>');  //mostrar combo vazio para preenchimento
            }
            
            
            
            
            
            
        }
        
        


        function isPost(){
             
             $isPost = (bool) ( ($_POST['relat']) AND (is_array($_POST['relat'])) AND ($this->p('NU_EMPRESA')) ) ? 1 : 0;
             $this->set('isPost', $isPost);
  
             return $isPost;
        }
        


        function save(){
            
            if($this->isPost()){
                
                // Codigo para realizar algum função ex: DELETAR / ALTERAR / INSERIR

            }  
        }
        
        
    }

?>
