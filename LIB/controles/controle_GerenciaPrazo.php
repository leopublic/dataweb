<?php

  

    Class GerenciaPrazo extends Controle{
        
        var $layout = 'mundivisas';
        var $title  = 'Relatório - Gerência de Prazo'; 
        
        
        function onLoad(){
            
           
            if($this->isPost()){  //Só executar query se o formulário for enviado
            
            
                if($this->p('NU_EMBARCACAO')){ $embarcacao = ' and C.NU_EMBARCACAO_PROJETO = '.$this->p('NU_EMBARCACAO');}else{ $embarcacao = '';}
            
            
                $strsql = 
                '   select 
                            concat(concat(concat(\'<a href="/operador/detalheCandidatoAuto.php?NU_CANDIDATO=\',C.NU_CANDIDATO),\'">\',C.NOME_COMPLETO),\'</a>\') NOME_COMPLETO 
                            ,PN.NO_NACIONALIDADE 
                            ,F.NO_FUNCAO
                            ,C.NU_PASSAPORTE
                            ,DATE_FORMAT(C.DT_VALIDADE_PASSAPORTE, "%d/%m/%Y") DT_VALIDADE_PASSAPORTE
                            ,DATEDIFF(C.DT_VALIDADE_PASSAPORTE,NOW()) AS DIAS_PASS
                            ,MTE.nu_processo NU_PROCESSO_MTE
                            ,REG.nu_protocolo NU_PROTOCOLO_REG
                            ,datediff(REG.dt_validade,now()) NU_DIAS_VALIDADE_REG
                            ,DATE_FORMAT(REG.dt_validade, "%d/%m/%Y") DT_VALIDADE_REG
                            ,PRO.NU_PROTOCOLO NU_PROTOCOLO_PRO
                            ,DATE_FORMAT(PRO.dt_validade, "%d/%m/%Y") DT_VALIDADE_PROR                           
                            ,datediff(PRO.dt_validade,now()) NU_DIAS_VALIDADE_PRO                           
                    from CANDIDATO C 
                    left join processo_mte MTE on MTE.cd_candidato = C.NU_CANDIDATO and MTE.codigo = (select max(codigo) from processo_mte where cd_candidato = C.NU_CANDIDATO)
                    left join processo_prorrog PRO on PRO.cd_candidato = C.NU_CANDIDATO and PRO.codigo_processo_mte = MTE.codigo
                    left join processo_regcie REG on REG.cd_candidato = C.NU_CANDIDATO and REG.codigo_processo_mte = MTE.codigo
                    left join processo_emiscie EMIS on EMIS.cd_candidato = C.NU_CANDIDATO and EMIS.codigo_processo_mte = MTE.codigo
                    left join processo_cancel CANC on CANC.cd_candidato = C.NU_CANDIDATO and CANC.codigo_processo_mte = MTE.codigo
                    left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS 
                    left join FUNCAO_CARGO F on F.CO_FUNCAO = MTE.cd_funcao  
                    where C.NU_EMPRESA = '.$this->p('NU_EMPRESA').' '.$embarcacao.' 
                    order by C.NOME_COMPLETO ASC            
                
                ';
                
                $candidatos = sql::query($strsql)->getArray();
                $projeto    = sql::query("select NO_EMBARCACAO_PROJETO from EMBARCACAO_PROJETO where NU_EMPRESA =".$this->p('NU_EMPRESA')." and NU_EMBARCACAO_PROJETO = ".$this->p('NU_EMBARCACAO'))->fetch();
                
                $this->set('razaosocial', $this->getCampo('EMPRESA', 'NO_RAZAO_SOCIAL', 'NU_EMPRESA', $this->p('NU_EMPRESA')));
                $this->set('projeto', $projeto['NO_EMBARCACAO_PROJETO']);
                $this->set('combo_EmbarcacaoEmpresa', $this->combo_embarcacaoempresa($this->p('NU_EMPRESA'), $this->p('NU_EMBARCACAO') ));
                $this->set('candidatos', $candidatos);
                $this->set('SEQ', 0);
                $this->set('excel', $this->p('excel'));
                
                if($this->p('excel')){ // se for gerar excel não colocar o layout na pagina 
                    $this->layout = '';
                }
                
            }else{  
                $this->set('combo_EmbarcacaoEmpresa', '<select id="NU_EMBARCACAO" name="NU_EMBARCACAO" ></select>');  //mostrar combo vazio para preenchimento
            }
            
            
            
            
            
            
        }
        
        


        function isPost(){
             
             $isPost = (bool) ( ($_POST['relat']) AND (is_array($_POST['relat'])) AND ($this->p('NU_EMPRESA')) ) ? 1 : 0;
             $this->set('isPost', $isPost);
  
             return $isPost;
        }
        


        function save(){
            
            if($this->isPost()){
                
                // Codigo para realizar algum função ex: DELETAR / ALTERAR / INSERIR

            }  
        }
        
        
    }

?>
