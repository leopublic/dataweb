<?php

  

    Class usuarios_acesso_add extends Controle{
        


        function onLoad(){

            $perfil = sql::query("SELECT * FROM usuarios WHERE flagativo = 'Y' " )->getArray();
            $this->set('perfil', $perfil);
        }
        
        


        function isPost(){
            
             $isPost = (bool) (($this->g('action')=='save') AND ($this->p('nome'))  AND ($this->p('login')) ) ? true : false;
             
             $this->set('isPost', $isPost);
             
             return $isPost;
        }
        


        function save(){
            
            if($this->isPost()){
                
                if(!$this->p('cd_perfil')){
                    $this->sucesso = false;
                    $this->msgErro = ' Para cadastrar um novo usuário é necessário que indique o campo "Perfil".';
                }
                
                $existe = sql::query("SELECT cd_usuario FROM usuarios where login = '".$this->p('login')."' ")->count();
                
                if($existe > 0){
                    $this->sucesso = false;
                    $this->msgErro =  '
                            Não foi possível cadastrar pois já existe uma pessoa com este mesmo login. <br />
                            Por favor verifique ! <a href="usuarios_acesso_add.php?"> Clique aqui para remover este aviso. </a>
                    ';  
                }
                    
                if($this->sucesso){
                    
                    $strsql =                   
                    "
                        INSERT INTO usuarios 
                            (cd_perfil, login, nm_senha, nome, nm_email, cd_superior, cd_empresa, flagativo, dt_cadastro)
                        VALUES 
                            (".$this->p('cd_perfil').",
                             '".$this->p('login')."',
                             '".$this->p('nm_senha')."',
                             '".$this->p('nome')."',
                             '".$this->p('nm_email')."',
                             ".$this->fk($this->p('cd_superior')).",
                             ".$this->fk($this->p('NU_EMPRESA')).",
                             'Y',
                             NOW())
                    ";
                                        
                    $usuario = sql::query($strsql);
                    
                    if($usuario->saved()){
                        $this->msgSucesso = '
                               Os dados do usuário <b>'.$this->p('nome').'</b> foram salvas com sucesso ! 
                               <a href="usuarios_acesso.php">Voltar para a listagem de usuários</a> ou
                               <a href="usuarios_acesso_edit.php?cod='.$usuario->id().'"> para editar este usuário. </a>
                        '; 
                    }else{
                        $this->sucesso = false;
                        $this->msgErro = '
                            Não Foi possivel cadastrar devido um erro inesperado. por favor verificar
                        ';
                    }
                }   
                
            }
            
            
            $this->set('sucesso', $this->sucesso);
            $this->set('msgSucesso', $this->msgSucesso);
            $this->set('msgErro', $this->msgErro);
            
            return $this->sucesso;
            
        }
        
        
    }

?>
