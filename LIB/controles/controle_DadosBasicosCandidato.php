<?php

  

    Class DadosBasicosCandidato extends cINTERFACE_OS{
        
       var $layout = 'mundivisas';
       var $title  = 'OS - Dados Básicos do Candidato'; 
       
       
       function onLoad(){
           
            if($this->Loaded()){

                $sql_candidatos = "
                    SELECT C.NU_CANDIDATO, C.NOME_COMPLETO, AC.CO_FUNCAO_CANDIDATO, AC.VA_REMUNERACAO_MENSAL_BRASIL, AC.VA_RENUMERACAO_MENSAL,  AC.CO_REPARTICAO_CONSULAR, AC.SALARIO_DOLAR
                      FROM solicita_visto os 
                INNER JOIN AUTORIZACAO_CANDIDATO AC 
                        ON AC.id_solicita_visto = os.id_solicita_visto
                INNER JOIN CANDIDATO C
                        ON AC.NU_CANDIDATO = C.NU_CANDIDATO 
                     WHERE os.id_solicita_visto = ".$this->g('ID_SOLICITA_VISTO');
                      

                // Configuração: masterEdit
                $action['title'] = 'Escolha a função';
                $action['url']   = '/ajax/masterEdit.funcao.php';
                $action['find']  = 'SELECT CO_FUNCAO pk, NO_FUNCAO descr FROM FUNCAO_CARGO where CO_FUNCAO =';
                
                $action_consulado['title'] = 'Escolha o Consulado';
                $action_consulado['url']   = '/ajax/masterEdit.consulado.php';
                $action_consulado['find']  = 'SELECT RC.CO_REPARTICAO_CONSULAR pk, RC.NO_REPARTICAO_CONSULAR descr FROM REPARTICAO_CONSULAR RC WHERE RC.CO_REPARTICAO_CONSULAR =';
                


                //cotação
                $this->set('cotacao', $this->moeda($this->getCampo('configuracao', 'valor', 'codconfiguracao', 2)));
                $this->set('cotacao_usuario', $this->getCampo('usuarios', 'nome', 'cd_usuario',  $this->getCampo('configuracao', 'cd_usuario', 'codconfiguracao', 2)));
                $this->set('cotacao_data',$this->datetimeBr($this->getCampo('configuracao', 'data', 'codconfiguracao', 2)));

                // os
                $this->set('os', $this);
                $this->set('candidatos', sql::query($sql_candidatos)->getArray());
                $this->set('action', $action);
                $this->set('action_consulado', $action_consulado);
                $this->set('html', new dhtml);
              
                
            }
 
        }
        
        


        function isPost(){
            
             $isPost = ( 
                            ($this->g('ID_SOLICITA_VISTO')) AND
                            ($_POST['CO_FUNCAO_CANDIDATO']) AND (is_array($_POST['CO_FUNCAO_CANDIDATO'])) AND
                            ($_POST['SALARIO_DOLAR']) AND (is_array($_POST['SALARIO_DOLAR'])) AND
                            ($_POST['candidato']) AND (is_array($_POST['candidato'])) AND
                            ($_POST['CO_REPARTICAO_CONSULAR']) AND (is_array($_POST['CO_REPARTICAO_CONSULAR']))
                                    
                        )? 1 : 0;
            
             $this->set('isPost', $isPost);
  
             return $isPost;
        }
        


        function save(){
            
            if($this->isPost()){
                
                $candidatos = $_POST['candidato'];
                foreach($candidatos as $nu_candidato=>$valor){
                    
                    
                    $dolar = $this->fk($this->moedaToDecimal($_POST['SALARIO_DOLAR'][$nu_candidato]));  // se zero retorna "null"
                    $real  = '';
                    
                    if($dolar <> 'null'){
                       $real = " VA_RENUMERACAO_MENSAL  = '".$this->dolarToReal($_POST['SALARIO_DOLAR'][$nu_candidato])."', "; 
                    }
                  
                  
                    
                    $sql = "UPDATE AUTORIZACAO_CANDIDATO
                               SET CO_FUNCAO_CANDIDATO    = " .$this->fk($_POST['CO_FUNCAO_CANDIDATO'][$nu_candidato]).",
                                   SALARIO_DOLAR          = " .$this->fk($this->moedaToDecimal($_POST['SALARIO_DOLAR'][$nu_candidato])).",
                                   ".$real."
                                   CO_REPARTICAO_CONSULAR = " .$this->fk($_POST['CO_REPARTICAO_CONSULAR'][$nu_candidato])."
                             WHERE NU_CANDIDATO      = ".$nu_candidato."
                               AND id_solicita_visto = ".$this->g('ID_SOLICITA_VISTO');
                     
                                              
                     sql::query($sql); 
                     
                
                }
            }  
        }
        
        
    }

?>
