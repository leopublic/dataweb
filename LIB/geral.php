<?php

function pegaNome($tabela,$retorno,$pesquisa,$codigo,$extra) {
  $ret = "";
  $sql = "SELECT $retorno from $tabela where $pesquisa=$codigo";
  if(strlen($extra)>0) { $sql = $sql." $extra "; }
  $rs = mysql_query($sql);
  if(mysql_errno()>0) { print "ERRO: ".mysql_error()."\nSQL=$sql"; }
  if($rw=mysql_fetch_array($rs)) {
    $ret = $rw[0];
  }
  return $ret;
}

function pegaNomeUsuario($cod) {
  $cod = 0 + $cod;
  return pegaNome("usuarios","nome","cd_usuario",$cod,"");
}

function pegaNomeEmpresa($cod) {
  $cod = 0 + $cod;
  return pegaNome("EMPRESA","NO_RAZAO_SOCIAL","NU_EMPRESA",$cod,"");
}

function pegaNomeCandidato($cod) {
  $cod = 0 + $cod;
  return pegaNome("CANDIDATO","concat(NO_PRIMEIRO_NOME,' ',NO_NOME_MEIO,' ',NO_ULTIMO_NOME) as NOME","NU_CANDIDATO",$cod,"");
}

function pegaNomeProjeto($cod,$empresa) {
  $cod = 0 + $cod;
  if(strlen($empresa)>0) { $extra = " AND NU_EMPRESA=$empresa "; }
  return pegaNome("EMBARCACAO_PROJETO","NO_EMBARCACAO_PROJETO","NU_EMBARCACAO_PROJETO",$cod,$extra);
}

function pegaNomePais($cod) {
  $cod = 0 + $cod;
  return pegaNome("PAIS_NACIONALIDADE","NO_PAIS","CO_PAIS",$cod,"");
}

function pegaNomeNacionalidade($cod) {
  $cod = 0 + $cod;
  return pegaNome("PAIS_NACIONALIDADE","NO_NACIONALIDADE","CO_PAIS",$cod,"");
}

function pegaNomeReparticao($cod) {
  $cod = 0 + $cod;
  return pegaNome("REPARTICAO_CONSULAR","NO_REPARTICAO_CONSULAR","CO_REPARTICAO_CONSULAR",$cod,"");
}

function pegaNomePerfil($cod) {
  $cod = 0 + $cod;
  return pegaNome("usuario_perfil","nm_perfil","cd_perfil",$cod,"");
}

function pegaNomeFuncao($cod) {
  $cod = 0 + $cod;
  return pegaNome("FUNCAO_CARGO","NO_FUNCAO","CO_FUNCAO",$cod,"");
}

function pegaCBOFuncao($cod) {
  $cod = 0 + $cod;
  return pegaNome("FUNCAO_CARGO","NU_CBO","CO_FUNCAO",$cod,"");
}

function pegaNomeProfissao($cod) {
  $cod = 0 + $cod;
  return pegaNome("PROFISSAO","NO_PROFISSAO","CO_PROFISSAO",$cod,"");
}

function pegaNomeEstadoCivil($cod) {
  $cod = "'$cod'";
  return pegaNome("ESTADO_CIVIL","NO_ESTADO_CIVIL","CO_ESTADO_CIVIL",$cod,"");
}

function pegaNomeEscola($cod) {
  $cod = "'$cod'";
  return pegaNome("ESCOLARIDADE","NO_ESCOLARIDADE","CO_ESCOLARIDADE",$cod,"");
}

function pegaNomeGrauParentesco($cod) {
  $cod = "'$cod'";
  return pegaNome("GRAU_PARENTESCO","NO_GRAU_PARENTESCO","CO_GRAU_PARENTESCO",$cod,"");
}

function pegaNomeTipoAutorizacao1($cod) {
  $cod = "'$cod'";
  return pegaNome("TIPO_AUTORIZACAO","NO_TIPO_AUTORIZACAO","CO_TIPO_AUTORIZACAO",$cod,"");
}

function pegaNomeTipoAutorizacao2($cod) {
  $cod = "'$cod'";
  return pegaNome("TIPO_AUTORIZACAO","NO_REDUZIDO_TIPO_AUTORIZACAO","CO_TIPO_AUTORIZACAO",$cod,"");
}

function pegaNomeClassVisto($cod) {
  $ret = "";
  if($cod=="1") {   $ret = "Permanente"; }
  elseif($cod=="2") {   $ret = "Temporário"; }
  return $ret;
}

function pegaNomeTransEntrada($cod) {
  $ret = "";
  if($cod=="1") {   $ret = "Navio"; }
  elseif($cod=="2") {   $ret = "Aviao"; }
  elseif($cod=="3") {   $ret = "Onibus"; }
  elseif($cod=="4") {   $ret = "Outros"; }
  return $ret;
}

function limpaValor($valor) {
  $valor = trim($valor);
  $valor = str_replace(" ","",$valor);
  $valor = str_replace(",","",$valor);
  $valor = str_replace(".","",$valor);
  $valor = str_replace("-","",$valor);
  $valor = str_replace("/","",$valor);
  return $valor;
}

function formataReais($valor) {
  $tam = strlen($valor);
  $prin = substr($valor,0,$tam-3);
  $cent = substr($valor,$tam-2,$tam-1);
  $ret = $prin.",".$cent;
  return $ret;
}

function formataCNPJ($valor) {
  $ret = "";
  $valor = limpaValor($valor);
  if(strlen($valor)>1) { 
    if(strlen($valor)>14) {
      $valor = substr($valor, strlen($valor)-14 , 14);
    }
    while(strlen($valor)<14) {
      $valor = "0".$valor;
    }
    $ret = substr($valor,0,2).".".substr($valor,2,3).".".substr($valor,5,3)."/".substr($valor,8,4)."-".substr($valor,12,2);
  }
  return $ret;
}

function formataCPF($valor) {
  $ret = "";
  $valor = limpaValor($valor);
  if(strlen($valor)>1) { 
    if(strlen($valor)>11) {
      $valor = substr($valor, strlen($valor)-11 , 11);
    }
    while(strlen($valor)<11) {
      $valor = "0".$valor;
    }
    $ret = substr($valor,0,3).".".substr($valor,3,3).".".substr($valor,6,3)."-".substr($valor,9,2);
  }
  return $ret;
}

function formataCEP($valor) {
  $ret = "";
  $valor = limpaValor($valor);
  if(strlen($valor)>1) { 
    if(strlen($valor)>8) {
      $valor = substr($valor, strlen($valor)-8 , 8);
    }
    while(strlen($valor)<8) {
      $valor = "0".$valor;
    }
    $ret = substr($valor,0,5)."-".substr($valor,5,3);
  }
  return $ret;
}

function pegaUltimaSolicitacao($candidato) {
  $ret = 0;
  $sql = "select NU_SOLICITACAO from AUTORIZACAO_CANDIDATO where NU_CANDIDATO=$candidato";
#  print "SQL=$sql";
  $rs = mysql_query($sql);
  while($rw=mysql_fetch_array($rs)) {
    $ret = $rw[0];
  }
  return $ret;
}

function pegaMeses() {
  $mes[1]="Janeiro";
  $mes[2]="Fevereiro";
  $mes[3]="Março";
  $mes[4]="Abril";
  $mes[5]="Maio";
  $mes[6]="Junho";
  $mes[7]="Julho";
  $mes[8]="Agosto";
  $mes[9]="Setembro";
  $mes[10]="Outubro";
  $mes[11]="Novembro";
  $mes[12]="Dezembro";
  return $mes;
} 

function montaCampos($nome,$valor,$descricao,$tipo,$acao,$extra) {
   $ret = "";
   if(($acao=="V") && ($tipo!="H") && ($tipo!="C")) {
   	 $tipo="V"; 
   }
   if($tipo=="H") {  # Colocar so hidden
     $ret = "<input type=hidden name='$nome' value='$valor'>";
   } else if($tipo=="T") { # colocar text
     $ret = "<input type=text name='$nome' value='$valor' $extra>";
   } else if($tipo=="M") { # MOEDA
     $ret = '<input type=text name="'.$nome.'" value="'.$valor.'" $extra onKeyUp="this.value=formatar(this.value);" style="text-align:right;">';
   } else if($tipo=="D") { # campo data
     $ret = "<input type=\"text\" name=\"$nome\" id=\"$nome\" value=\"$valor\" onKeyUp=\"this.value=formatarData(this.value);\" maxlenght=\"10\" size=\"10\" $extra class=\"data\"/>";
   } else if($tipo=="P") { # colocar text
     $ret = "<input type=password name='$nome' value='$valor' $extra>";
   } else if($tipo=="R") { # colocar radio
     $ret = "<input type=radio name='$nome' value='$valor' $extra class='radio'> $descricao";
   } else if($tipo=="S") { # colocar select (chega o option no valor
     $ret = "<select name='$nome' $extra>$valor</select>";
   } else if($tipo=="A") { # colocar textarea
     $ret = "<textarea name='$nome' $extra>$valor</textarea>";
   } else if($tipo=="C") { # colocar textarea
   		if($acao=="V"){
   			$extra = "readonly disabled";
   		}
   		if ($valor>0){
   			$checked = 'checked';
   		}
		$ret = '<input type="checkbox" name="'.$nome.'" '.$checked.' '.$extra.'/>';
   } else { # colocar so a descricao
     $ret = $descricao;
   }
   if($acao=="H") {
     $ret = "<input type=hidden name='$nome' value='$valor'> $valor";
   }
   return $ret;
}
function formatarCPF_CNPJ($campo, $formatado = true){
    //retira formato
    $codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
    // pega o tamanho da string menos os digitos verificadores
    $tamanho = (strlen($codigoLimpo) -2);
    //verifica se o tamanho do código informado é válido
    if ($tamanho != 9 && $tamanho != 12){
        return false;
    }
 
    if ($formatado){
        // seleciona a máscara para cpf ou cnpj
        $mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##';
 
        $indice = -1;
        for ($i=0; $i < strlen($mascara); $i++) {
            if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
        }
        //retorna o campo formatado
        $retorno = $mascara;
 
    }else{
        //se não quer formatado, retorna o campo limpo
        $retorno = $codigoLimpo;
    }
 
    return $retorno;
 
}
function formatarMascara($campo, $mascara){
    //retira formato
    $codigoLimpo = preg_replace("[' '-./ t]","",$campo);
    $mascaraLimpa = preg_replace("/[^#]/","",$mascara);
    $indice = -1;
    if (strlen($mascaraLimpa)> strlen($codigoLimpo))
    {	$tam = strlen($codigoLimpo);	}
    else
    {	$tam = strlen($mascara);	}
    $ret = '';
    
    for ($i=0; $i < $tam; $i++) {
        if ($mascara[$i]=='#') 
        {	$ret .= $codigoLimpo[++$indice];}
        else
        {	$ret .= $mascara[$i];	}
    }
        //retorna o campo formatado
    $retorno = $ret;
 
    return $retorno;
}
function ValorParametro(&$pSession, $pPost, $pNomeParametro, &$pVariavel, $pNomeTela = ''){
	if (isset($pPost[$pNomeParametro])){
		$pVariavel = $pPost[$pNomeParametro];
		$pSession['cFILTRO'.$pNomeTela.$pNomeParametro]=$pVariavel;
	}
	else{
		if (isset($pSession['cFILTRO'.$pNomeTela.$pNomeParametro])){
			$pVariavel = $pSession['cFILTRO'.$pNomeTela.$pNomeParametro];
		}
	}
}

?>
