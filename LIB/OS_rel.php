<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

$pID_SOLICITA_VISTO = $_GET['ID_SOLICITA_VISTO'];

/*
$tela = new cEDICAO('SOLICITA_VISTO_view');
$tela->CarregarConfiguracao();
$tela->id_solicita_visto=$pID_SOLICITA_VISTO ;
$tela->RecupereRegistro();
*/
$OS = new cORDEMSERVICO();
$OS->Recuperar($pID_SOLICITA_VISTO);
$OS->RecuperarCandidatos($pID_SOLICITA_VISTO);

$tppr_tx_nome = $OS->mtppr_tx_nome;
if ($tppr_tx_nome == ''){
	$tppr_tx_nome = '(não informado)';
}

$rs = mysql_query("SELECT DISTINCT v.*, e.NO_ENDERECO, e.NU_CNPJ FROM vSOLICITA_VISTO v LEFT JOIN EMPRESA e ON e.NU_EMPRESA = v.NU_EMPRESA WHERE v.ID_SOLICITA_VISTO = $pID_SOLICITA_VISTO") or die( mysql_error() );
$tela = mysql_fetch_object($rs);

if($OS->mid_solicita_visto_pacote > 0){
    $OSpacote = new cORDEMSERVICO();
    $OSpacote->mID_SOLICITA_VISTO = $OS->mid_solicita_visto_pacote;
    $OSpacote->Recuperar();
    $tela->NU_SOLICITACAO .= '<br/><span style="font-size:14px">Pertence ao pacote da OS '.$OSpacote->mNU_SOLICITACAO.'</span>';
}

if($tela->NO_RAZAO_SOCIAL_REQUERENTE == ''){
	$tela->NO_RAZAO_SOCIAL_REQUERENTE = $tela->NO_RAZAO_SOCIAL;
}

if($OS->mNO_EMBARCACAO_PROJETO_COBRANCA == ''){
	$OS->mNO_EMBARCACAO_PROJETO_COBRANCA = $OS->mNO_EMBARCACAO_PROJETO;
}

include('../MPDF56/mpdf.php');

$html = '
<style>
	table {width:100%;border-collapse:collapse;margin-left:20px;}
	th {vertical-align:top;text-align:left;margin-left:10px;margin-right:10px;border-bottom:solid 1px #cccccc;}
	div.sep {font-size:12pt;color:#999999;font-weight:bold;border-bottom:dotted 1px #999999;vertical-align:bottom;width:100%;margin-top:20px;margin-left:20px;margin-bottom:10px;}
	td {border-bottom:solid 1px #cccccc;}
	table.grid {border-bottom:solid 3px #cccccc;}
	table.grid th{text-align:left;border-bottom:solid 3px #cccccc;}
	</style>
<body style="font-family: sans; font-size: 10pt;">
<table style="border-bottom:solid 6px #999999;margin-left:0;">
<tr>
	<td style="font-size:42pt;color:#999999;font-weight:bold">OS '.$tela->NU_SOLICITACAO.'</td>
	<td style="text-align:right"><img align="right" src="/imagens/logo_mundivisas_PANTONE.jpg" style="width:90mm"/></td>
</tr>
</table>
<div style="text-align:right;vertical-align:bottom;font-weight:bold;">
    Cliente : '.htmlentities($tela->NO_RAZAO_SOCIAL).'<br/>
    CNPJ : '.htmlentities($tela->NU_CNPJ).'<br/>
    Endere&ccedil;o : '.htmlentities($tela->NO_ENDERECO).'<br/>
    Servi&ccedil;o : &quot;'.htmlentities($tela->NO_SERVICO_RESUMIDO).'&quot;
    </div>
<div class="sep">Detalhes</div>
<table>
    <tr><th width="180px">Empresa COBRAN&Ccedil;A</th><td>'.htmlentities($tela->NO_RAZAO_SOCIAL_REQUERENTE).'</td></tr>
    <tr><th>Emb./Proj. PROCESSO</th><td>'.htmlentities($OS->mNO_EMBARCACAO_PROJETO).'&nbsp;</td></tr>
    <tr><th>Emb./Proj. REAL</th><td>'.htmlentities($OS->mNO_EMBARCACAO_PROJETO_COBRANCA).'&nbsp;</td></tr>
    <tr><th>Tipo de cobran&ccedil;a</th><td>'.htmlentities($tppr_tx_nome).'&nbsp;</td></tr>
    <tr><th>Prazo solicitado</th><td>'.htmlentities($tela->DT_PRAZO_ESTADA_SOLICITADO).'&nbsp;</td></tr>
    <tr><th>Solicitante</th><td>'.htmlentities($tela->no_solicitador).'&nbsp;</td></tr>
    <tr><th>Data da solicita&ccedil;&atilde;o</th><td>'.htmlentities($tela->dt_solicitacao).'&nbsp;</td></tr>
    <tr><th>Respons&aacute;vel</th><td>'.htmlentities($tela->NO_USUARIO_TECNICO).'&nbsp;</td></tr>
    <tr><th>Observa&ccedil;&otilde;es</th><td>'.str_replace("\n", "<br/>", htmlentities($tela->de_observacao)).'&nbsp;</td></tr>
</table>';
if ($OS->mCandidatos != '' ){
	$html .= '
<div class="sep">Candidatos</div><table class="grid">
    <tr><th colspan="2">Nome completo</th><th>Nacionalidade</th><th>Passaporte</th><th>RNE</th></tr>';

	$sql = "SELECT distinct CANDIDATO.NU_CANDIDATO
				  , CANDIDATO.NOME_COMPLETO
				  , CANDIDATO.NU_PASSAPORTE
				  , NO_NACIONALIDADE
				  , CANDIDATO.NU_RNE
				  , CANDIDATO.BO_CADASTRO_MINIMO_OK
				  , CANDIDATO.nu_candidato_parente
				  , no_grau_parentesco
				  , autc_fl_revisado
				  ";
	$sql.= "  FROM CANDIDATO ";
	$sql.= "  join autorizacao_candidato ac on ac.id_solicita_visto = ".$pID_SOLICITA_VISTO." and ac.nu_candidato = CANDIDATO.nu_candidato" ;
	$sql.= "  left join CANDIDATO cp on cp.NU_CANDIDATO = CANDIDATO.nu_candidato_parente" ;
	$sql.= "  left join PAIS_NACIONALIDADE P on P.CO_PAIS = CANDIDATO.CO_NACIONALIDADE" ;
	$sql.= "  left join grau_parentesco gp on gp.co_grau_parentesco = CANDIDATO.co_grau_parentesco " ;
	$sql.= " where CANDIDATO.NU_CANDIDATO in (".$OS->mCandidatos.") or (CANDIDATO.nu_candidato_parente in (".$OS->mCandidatos.") and CANDIDATO.NU_CANDIDATO not in (".$OS->mCandidatos."))";
	$sql.= " ORDER BY concat(coalesce(cp.NOME_COMPLETO, ''), CANDIDATO.NOME_COMPLETO) asc ";

	$res = conectaQuery($sql, __FILE__);
    while($rs = mysql_fetch_array($res)){
		if($rs['nu_candidato_parente']!= ''){
			$nome = '<td>&nbsp;</td><td>'.htmlentities($rs['no_grau_parentesco']).': '.$rs['NOME_COMPLETO'].'</td>';
		}
		else{
			$nome = '<td colspan="2">'.htmlentities($rs['NOME_COMPLETO']).'</td>';
		}
    	$html.= '<tr>'.$nome.'</td><td>'.htmlentities($rs['NO_NACIONALIDADE']).'</td><td>'.htmlentities($rs['NU_PASSAPORTE']).'</td><td>'.htmlentities($rs['NU_RNE']).'</td></tr>';
    }
    $html.= '</table>';
}
$html.=  '
	<div class="sep">Cadastro</div>
    <table>
    <tr><th width="150px">Criada por</th><td>'.htmlentities($tela->NO_USUARIO_CADASTRO).'</td></tr>
    <tr><th>Em</th><td>'.htmlentities($tela->dt_cadastro).'</td></tr>
</table>

</body>
';
// $html = cBANCO::SemCaracteresEspeciaisMS($html);
$mpdf=new mPDF();
$mpdf->img_dpi = 200;
$mpdf->WriteHTML($html);
$mpdf->Output();
