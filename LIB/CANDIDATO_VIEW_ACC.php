<?php
header('Content-Type: text/html; charset=UTF-8');
include ("autenticacao.php");
include ("geral.php");
//include ("../LIB/combos.php");
//include ("../LIB/componentes/cCOMBO.php");

$pNU_CANDIDATO  	= cINTERFACE::GetValido($_GET,'NU_CANDIDATO');
$pID_SOLICITA_VISTO = cINTERFACE::GetValido($_GET,'ID_SOLICITA_VISTO');

if ($pNU_CANDIDATO=='')
{

}
else
{
	$ac = new cAUTORIZACAO_CANDIDATO();
	$ac->Recuperar($pID_SOLICITA_VISTO, $pNU_CANDIDATO);
	$cand = new cCANDIDATO();
	$cand->Recuperar($pNU_CANDIDATO);
	?>
<div id="accordionCand" class="accordion">

<h3  class="interno">Identificação do Candidato</h3>
<div>
	<table class="edicao dupla">
		<col width="18%"></col>
		<col width="28%"></col>
		<col width="5%"></col>
		<col width="18%"></col>
		<col width="28%"></col>
<?php
$editaCandidato = new cEDICAO("vCANDIDATO_CONS");
$editaCandidato->CarregarConfiguracao();
$editaCandidato->mCampoNome["NU_CANDIDATO"]->mValor =$pNU_CANDIDATO ;
$editaCandidato->RecupereRegistro();
$btr = "";
$par = false;
$painelAnt = '';
foreach($editaCandidato->mCampo as $campo)
{
	if ($painelAnt!=$campo->mPainel)
	{
		if ($par&&$linha!='')
		{
			$linha .= '<td>&#160;</td><td>&#160;</td><td>&#160;</td>';
			$linha .='</tr>';
			print $linha."\n";
		}
		print '<tr class="subTitulo"><td colspan="5">'.$campo->mPainel.'</td></tr>';
		$painelAnt = $campo->mPainel;
		$par = false;
	}
	if (!$campo->mChave && $campo->mVisivel)
	{
		if (!$par)
		{
			$linha = '<tr>';
		}
		else
		{
			$linha .= '<td>&#160;</td>';
		}

		$linha .= cINTERFACE::RenderizeLinhaEdicao($campo, true);

		if ($par)
		{
			$linha .='</tr>';
			print $linha."\n";
		}

		$par = !$par;
	}
}
if ($par)
{
	print $linha.'<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>'."\n";
}

?>
	</table>
</div>
	<h3 class="interno">Visa Details</h3>
	<div>
		<table class="edicao dupla">
		<col width="18%"></col>
		<col width="28%"></col>
		<col width="5%"></col>
		<col width="18%"></col>
		<col width="28%"></col>

<?php
$editaCandidato = new cEDICAO("VISA_DETAILS_CONS");
$editaCandidato->CarregarConfiguracao();
$editaCandidato->mCampoNome["id_solicita_visto"]->mValor =$pID_SOLICITA_VISTO;
$editaCandidato->mCampoNome["NU_CANDIDATO"]->mValor =$pNU_CANDIDATO ;
$editaCandidato->RecupereRegistro();
$btr = "";
$par = false;
$painelAnt = '';
foreach($editaCandidato->mCampo as $campo)
{
	if ($painelAnt!=$campo->mPainel)
	{
		if ($par&&$linha!='')
		{
			$linha .= '<td>&#160;</td>';
			$linha .='</tr>';
			print $linha."\n";
		}
		print '<tr class="subTitulo"><td colspan="5">'.$campo->mPainel.'</td></tr>';
		$painelAnt = $campo->mPainel;
		$par = false;
	}
	if (!$campo->mChave && $campo->mVisivel)
	{
		if (!$par)
		{
			$linha = '<tr>';
		}
		else
		{
			$linha .= '<td>&#160;</td>';
		}
		$linha .= cINTERFACE::RenderizeLinhaEdicao($campo);

		if ($par)
		{
			$linha .='</tr>';
			print $linha."\n";
		}
		$par = !$par;
	}
}
if ($par)
{
	print $linha.'<td>&#160;</td><td>&#160;</td><td>&#160;</td></tr>'."\n";
}
?>
		</table>
	</div>

<h3 class="interno">Solicitações anteriores</h3>
<div>
<?php
// Montagem do histórico das solicitações anteriores
	$tabs = '';
	$sql = " select NU_SOLICITACAO, ID_SOLICITA_VISTO ";
	$sql .= "  from AUTORIZACAO_CANDIDATO ";
	$sql .= " WHERE NU_CANDIDATO  = ".$cand->mNU_CANDIDATO;
	$sql .= "   and NU_SOLICITACAO <> ".$ac->mNU_SOLICITACAO;
	$sql .= "   and NU_SOLICITACAO <> 0";
	$rs = mysql_query($sql);
	while($rw = mysql_fetch_array($rs))
	{
		$tabs .= '<li><a href="/LIB/solicitacao.php?NU_CANDIDATO='.$cand->mNU_CANDIDATO.'&NU_SOLICITACAO='.$rw['NU_SOLICITACAO'].'"><span>'.$rw['NU_SOLICITACAO'].'</span></a></li>';
	}
	if($tabs!='')
	{
		$tabs = '<div id="tabsInt"><ul>'.$tabs.'</ul></div>';
	}
	else
	{
		$tabs = '(nenhuma solicitação encontrada)';
	}
	print $tabs;
?>
</div>
<?php
}
?>
