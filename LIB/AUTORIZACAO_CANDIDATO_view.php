<?php
header('Content-Type: text/html; charset=UTF-8');
include ("autenticacao.php");
include ("geral.php");
include ("../LIB/combos.php");
include ("../LIB/componentes/cCOMBO.php");
$pNU_CANDIDATO  	= cINTERFACE::GetValido($_GET,'NU_CANDIDATO');
$pID_SOLICITA_VISTO = cINTERFACE::GetValido($_GET,'ID_SOLICITA_VISTO');

if ($pNU_CANDIDATO=='')
{

}
else
{
	$acCand = new cEDICAO("AUTORIZACAO_CANDIDATO_OS");
	$acCand->CarregarConfiguracao();
	$acCand->mCampoNome['id_solicita_visto']->mValor =$pID_SOLICITA_VISTO ;
	$acCand->mCampoNome['NU_CANDIDATO']->mValor =$pNU_CANDIDATO ;
	$acCand->RecupereRegistro();
?>
	<form method="post">
	<?=cINTERFACE::RenderizeCampo($acCand->mCampoCtrlEnvio);?>
	<?=cINTERFACE::RenderizeCampo($acCand->mCampoNome['id_solicita_visto']);?>
	<?=cINTERFACE::RenderizeCampo($acCand->mCampoNome['NU_CANDIDATO']);?>
	<table class="edicao dupla">
		<col width="18%"></col>
		<col width="28%"></col>
		<col width="5%"></col>
		<col width="18%"></col>
		<col width="28%"></col>
	<?php
	$btr = "";
	$par = false;
	$painelAnt = '';

	foreach($acCand->mCampo as $campo)
	{
		if (!$campo->mChave && $campo->mVisivel)
		{
			if (!$par)
			{
				$linha = '<tr>';
			}
			else
			{
				$linha .= '<td>&#160;</td>';
			}

			$linha .= cINTERFACE::RenderizeLinhaEdicao($campo, false);

			if ($par)
			{
				$linha .='</tr>';
				print $linha."\n";
			}

			$par = !$par;
		}
	}
	if ($par)
	{
		$linha .= '<td>&#160;</td>';
		$linha .= '<td>&#160;</td>';
		$linha .= '<td>&#160;</td>';
		$linha .='</tr>';
		print $linha."\n";
	}
	?>
	</table>
	<center><input type="submit" value="Alterar" style="width:auto"/></center>
	</form>
<?php
}
?>
