<?php

function execSQL($sql) {
  $ret = "";
  mysql_query($sql);
  if(mysql_errno()>0) { $ret = mysql_error()." SQL=$sql"; }
  return $ret;
}

function pegaProximo($tabela,$coluna) {
  $ret = 0;
  $sql = "select max($coluna) from $tabela";
  $rs = mysql_query($sql);
  if($rw=mysql_fetch_array($rs)) {
    $ret = $rw[0];
  }
  $ret++;
  return $ret;
}

function listaGeral($tabela,$condicao) {
   $sql = "SELECT *, if (coalesce(fl_ativa, 0) = 1 ,'ativa', 'inativa' ) status FROM $tabela $condicao";
# print "SQL=$sql";
   $rs = mysql_query($sql);
   return $rs;
}

function dataBR2My($data) {
  $ret = $data;
  if(strlen($data)==10) {
    $aux = split("/",$data);
    if(count($aux)>1) {
      $ret = $aux[2]."-".$aux[1]."-".$aux[0];
      if($aux[2]=="0000") { $ret = ""; }
    }
  }
  return $ret;
}

function dataMy2BR($data) {
  $ret = $data;
  if(strlen($data)==10) {
    $aux = preg_split("[-]",$data);
    if(count($aux)>1) {
      $ret = $aux[2]."/".$aux[1]."/".$aux[0];
      if($aux[0]=="0000") { $ret = ""; }
    }
  }
  return $ret;
}
function dataMy2BRRed($data) {
  $ret = $data;
  if(strlen($data)==10) {
    $aux = preg_split("[-]",$data);
    if(count($aux)>1) {
		$ano = substr($aux[0], 2,2);
		$ano = $aux[0];
    	$ret = $aux[2]."/".$aux[1]."/".$ano;
      if($aux[0]=="0000") { $ret = ""; }
    }
  }
  return $ret;
}

function limpaDataZeroMy($data) {
  if(strlen(dataMy2BR($data))==0) {
    $ret = "";
  }
  return $ret;
}

function limpaDataZeroBR($data) {
  if(strlen(dataBR2My($data))==0) {
    $ret = "";
  }
  return $ret;
}

function limpaTudo($data) {
  $ret = str_replace("-","",$data);
  $ret = str_replace("/","",$ret);
  $ret = str_replace(".","",$ret);
  $ret = str_replace(",","",$ret);
  return $ret;
}

function frmtDataBR($data) {
  if(strpos($data,"000")===true) {
     $ret = "";
  } else {
     $ret = dataMy2BR($data);
  }
  return $ret;
}

function frmtDataDB($data) {
  if(strlen($data)!=10) {
     $ret = "NULL";
  } else {
     $ret = "'".dataBR2My($data)."'";
  }
  return $ret;
}

function frmtValorDB($valor,$tipo) {
  if($tipo=="N") {
     if(strlen($valor)>0) {
       $ret = 0 + str_replace(",",".",$valor);
     } else {
       $ret = "NULL";
     }
  } else if($tipo=="D") {
     $ret = frmtDataDB($valor);
  } else {
     $ret = "'".addslashes($valor)."'";
  }
  return ",".$ret;
}

function frmtValorIns($valor,$tipo) {
  if($tipo=="N") {
     $ret = 0 + str_replace(",",".",$valor);
  } else if($tipo=="D") {
     $ret = frmtDataDB($valor);
  } else {
     $ret = "'".addslashes($valor)."'";
  }
  return ",".$ret;
}

function frmtValorAlt($valor,$tipo) {
  if($tipo=="N") {
     $ret = 0 + str_replace(",",".",$valor);
  } else if($tipo=="D") {
     $ret = frmtDataDB($valor);
  } else {
     $ret = "'".addslashes($valor)."'";
  }
  return $ret;
}

function gravaLog($sql,$erro,$tipo,$local) {
  global $mylogdir,$usulogado;
  $sql = str_replace(chr(10), " ", $sql);
  $sql = str_replace(chr(13), " ", $sql);
  $nome = pegaNomeUsuario($usulogado);
  $data = date("d/m/Y H:i:s");
  $texto = "------------------- $data --------------------\n";
  $texto = $texto."$data -> Usuario: $usulogado ($nome) executou $tipo em $local -> $sql \n";
  $arquivo = $mylogdir."/log_".date("Ymd").".txt";
  $handle = fopen($arquivo, 'a+');
  fwrite($handle, $texto);
  fclose($handle);
#  if(substr($tipo,0,4)=="REMO") {
#    $para = "mundivisas@mundivisas.com.br,romulo@oceanicawebhome.com.br,barrie@mundivisas.com.br";
#    $titulo = "Site Mundivisas: $tipo na $local";
#    $corpo = "\nPrezados,\n\nO usuário $nome removeu da tabela $local com o seguinte LOG:\n\n$texto\n\n";
#    mail($para,$titulo,$corpo);
#  }
}

function gravaLogErro($sql,$erro,$tipo,$local) {
  global $mylogdir,$usulogado;
  $sql = str_replace(chr(10), " ", $sql);
  $sql = str_replace(chr(13), " ", $sql);
  $nome = pegaNomeUsuario($usulogado);
  $data = date("d/m/Y H:i:s");
  $texto = "------------------- $data --------------------\n";
  $texto = $texto."$data -> Usuario: $usulogado ($nome) executou $tipo em $local -> $sql => ERRO: $erro\n";
  $arquivo = $mylogdir."/erro_".date("Ymd").".txt";
  $handle = fopen($arquivo, 'a+');
  fwrite($handle, $texto);
  fclose($handle);
}

function gravaLogEmail($para,$subject,$email) {
  global $mylogdir,$usulogado;
  $data = date("d/m/Y H:i:s");
  $nome = pegaNomeUsuario($usulogado);
  $texto = "------------------- $data --------------------\n";
  $texto = $texto."$data -> Usuario: $usulogado ($nome) - Para: $para - Subject: $subject\nCorpo: $email";
  $arquivo = $mylogdir."/email_".date("Ymd").".txt";
  $handle = fopen($arquivo, 'a+');
  fwrite($handle, $texto);
  fclose($handle);
}

function insereEvento($cd_emp,$cd_cand,$cd_sol,$texto) {
  global $usulogado;
  if(strlen($usulogado)==0) { $usulogado = "NULL"; }
  if(strlen($cd_emp)==0) { $cd_emp = "NULL"; }
  if(strlen($cd_cand)==0) { $cd_cand = "NULL"; }
  if(strlen($cd_sol)==0) { $cd_sol = "NULL"; }
  $auxsql = "INSERT INTO acompanhamento_eventos (cd_empresa,cd_candidato,cd_solicitacao,cd_admin,texto) ";
  $sql = sprintf($auxsql." VALUES ($cd_emp,$cd_cand,$cd_sol,$usulogado,'%s') ", mysql_real_escape_string($texto));
  mysql_query($sql);
  $ret = mysql_error();
#print "<br>sql=$sql e erro=$ret .<br>";
  if(mysql_errno()==0) {
    gravaLogErro($sql,mysql_error(),"INSERE","EVENTO");
  }
}

?>
