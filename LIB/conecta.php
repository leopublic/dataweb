<?php

ini_set('display_errors', 1);
ini_set('log_errors', 'On');

if (file_exists('vendor/autoload.php')) {
    require_once('vendor/autoload.php');
} elseif (file_exists('../vendor/autoload.php')) {
    require_once('../vendor/autoload.php');
} elseif (file_exists('../../vendor/autoload.php')) {
    require_once('../../vendor/autoload.php');
}

if (file_exists('framework/cAMBIENTE.php')) {
    require_once('framework/cAMBIENTE.php');
} elseif (file_exists('../framework/cAMBIENTE.php')) {
    require_once('../framework/cAMBIENTE.php');
} elseif (file_exists('../../framework/cAMBIENTE.php')) {
    require_once('../../framework/cAMBIENTE.php');
} elseif (file_exists('../../../framework/cAMBIENTE.php')) {
    require_once('../../../framework/cAMBIENTE.php');
}

$ambiente = new cAMBIENTE();

define('DS', DIRECTORY_SEPARATOR);
if (!isset($_SERVER['SERVER_NAME']) || $_SERVER['SERVER_NAME'] == 'dataweb.dev') {
    define('AMBIENTE', cCONFIGURACAO::cfgDESENVOLVIMENTO);
    define('DEBUG', true);
} elseif ($_SERVER['SERVER_NAME'] == 'datawebdev.m2software.com.br') {
    define('AMBIENTE', cCONFIGURACAO::cfgM2);
    define('DEBUG', true);
} else {
    define('DEBUG', false);
    define('AMBIENTE', cCONFIGURACAO::cfgPRODUCAO);
}


define('DATAWEB_MORTO', 'dataweb_morto');

cTWIG::DefineDefaults(false);

cHTTP::$FILES = $_FILES;
cHTTP::$GET = $_GET;
cHTTP::$POST = $_POST;
cHTTP::$SESSION = &$_SESSION;
if (isset($_GET['controller'])) {
    cHTTP::$controller = $_GET['controller'];
} elseif (isset($_POST['controller'])) {
    cHTTP::$controller = $_POST['controller'];
} else {
    cHTTP::$controller = '';
}
if (isset($_GET['MIME'])) {
    cHTTP::$MIME = $_GET['MIME'];
} else {
    cHTTP::$MIME = 'HTM';
}
if (isset($_GET['metodo'])) {
    cHTTP::$metodo = $_GET['metodo'];
} elseif (isset($_POST['metodo'])) {
    cHTTP::$metodo = $_POST['metodo'];
} else {
    cHTTP::$metodo = '';
}

include("dbs.php");

$hostname_mundivisas = cCONFIGURACAO::$configuracoes[AMBIENTE]['servidor'];
$username_mundivisas = cCONFIGURACAO::$configuracoes[AMBIENTE]['loginDb'];
$password_mundivisas = cCONFIGURACAO::$configuracoes[AMBIENTE]['senhaDb'];
$database_mundivisas = cCONFIGURACAO::$configuracoes[AMBIENTE]['database'];
$mydominio = cCONFIGURACAO::$configuracoes[AMBIENTE]['dominio'];
$myinstdir = cCONFIGURACAO::$configuracoes[AMBIENTE]['raiz'];

$arquivo_log = $myinstdir . '/log/error_log' . date('Ym') . '.txt';

ini_set('error_log', $arquivo_log);

$mundivisas = mysql_connect($hostname_mundivisas, $username_mundivisas, $password_mundivisas) or die(mysql_error());
mysql_set_charset('utf8',$mundivisas);
mysql_select_db($database_mundivisas)or die("Não foi possível conectar com o banco de dados");
cAMBIENTE::InicializeAmbiente(AMBIENTE);


$mylogdir = $myinstdir . "/log";

$myfinurl = $mydominio . "/intranet/solicitacoes";
$mysoldir = $myinstdir . "/arquivos/solicitacoes";
$mysolurl = $mydominio . "/arquivos/solicitacoes";
$myrecdir = $myinstdir . "/arquivos/recibos";
$myrecurl = $mydominio . "/arquivos/recibos";
$myfordir = $myinstdir . "/arquivos/formularios";
$myforurl = $mydominio . "/arquivos/formularios";
$mydocdir = $myinstdir . "/arquivos/documentos";
$mydocurl = $mydominio . "/arquivos/documentos";
$myhstdir = $myinstdir . "/arquivos/histpag";
$myhsturl = $mydominio . "/arquivos/histpag";

$estilo = "border: 1px solid; font-family: Verdana; background-color: rgb(247,247,247); font-size:8 pt; color:#3969A5; padding-left:4px; padding-right:4px; padding-top:1px; padding-bottom:1px";

$mydir = $myinstdir . "/intranet";
$myurl = $mydominio . "/intranet";
$myup = "/upload";

error_reporting(E_ALL & ~E_NOTICE);
if (defined('E_STRICT')) {
    error_reporting(E_ALL & ~E_WARNING & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE);
} else {
    if (defined('E_DEPRECATED')) {
        error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    }
}

function conectaQuery($pSql, $pContexto) {
    if (DEBUG) {
        error_log(str_replace("\t", " ", str_replace("     ", " ", str_replace("    ", " ", str_replace("    ", " ", str_replace("  ", " ", $pContexto . " sql=" . $pSql))))));
    }
    if ($res = mysql_query($pSql)) {
        return $res;
    } else {
        throw new Exception("\n\n\t[Contexto]" . "\n\t" . $pContexto . "\n\t[SQL]" . "\n\t" . $pSql . "\n\t[Erro]" . "\n\t" . mysql_error());
    }
}

function executeQuery($pSql, $pContexto) {
//    error_log(str_replace("\t", " ", str_replace("     ", " ", str_replace("    ", " ", str_replace("    ", " ", str_replace("  ", " ", $pContexto . " cd_usuario=" . cSESSAO::$mcd_usuario . " sql=" . $pSql))))));
    if (mysql_query($pSql)) {
        return true;
    } else {
        throw new Exception("\n\n\t[Contexto]" . "\n\t" . $pContexto . "\n\t[SQL]" . "\n\t" . $pSql . "\n\t[Erro]" . "\n\t" . mysql_error());
    }
}
