<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

$resc_id = $_GET['resc_id'];
$rc = new cresumo_cobranca();
$rc->RecuperePeloId($resc_id);
$empresa = new cEMPRESA();
$empresa->RecuperePeloId($rc->mnu_empresa_requerente);

$rs = mysql_query("SELECT DISTINCT v.*, e.NO_ENDERECO, e.NU_CNPJ FROM vSOLICITA_VISTO v LEFT JOIN EMPRESA e ON e.NU_EMPRESA = v.NU_EMPRESA WHERE v.ID_SOLICITA_VISTO = $pID_SOLICITA_VISTO") or die( mysql_error() );
$tela = mysql_fetch_object($rs);

include('../MPDF45/mpdf.php');

$html = '
<style>
	table {width:100%;border-collapse:collapse;margin-left:20px;}
	th {vertical-align:top;text-align:left;margin-left:10px;margin-right:10px;border-bottom:solid 1px #cccccc;}
	div.sep {font-size:12pt;color:#999999;font-weight:bold;border-bottom:dotted 1px #999999;vertical-align:bottom;width:100%;margin-top:20px;margin-left:20px;margin-bottom:10px;}
	td {border-bottom:solid 1px #cccccc;}
	table.grid {border-bottom:solid 3px #cccccc;}
	table.grid th{text-align:left;border-bottom:solid 3px #cccccc;}
	</style>
<body style="font-family: sans; font-size: 10pt;">
<table style="width:100%">
	<col width="200px"></col><col width="auto"></col><col width="100px"></col>
	<tr><td rowspan="2">(logo)</td><td>MUNDIVISAS LEGALIZATION OF FOREIGNERS</td><td>Data : '.$rc->mresc_dt_faturamento.'</td> </tr>
	<tr><td>Endere&ccedil;o: Rua S&atilde;o Pedro 154, salas 1005 a 1008<br/>Centro - Niter&oacute;i - RJ - CEP 24020-058<br/>Telefone: (21) 2719-1709<br/>Email:<br/>CNPJ:04343644/0001-33</td><td>Hora: 15:01</td> </tr>
	<tr><td colspan="2">RELAT&Oacute;RIO DE COBRAN&Ccedil;A</td><td>'.$rc->mresc_dt_faturamento.'</td> </tr>
</table>
<br/>COBRAR
<br/>'.$empresa->mNO_RAZAO_SOCIAL.'
<br/>'.$empresa->mNO_ENDERECO.'
<br/>'.$empresa->mNO_BAIRRO.' - '.$empresa->mNO_MUNICIPIO.' - '.$empresa->mCO_UF.' - '.$empresa->mNU_CEP.'

<table>
	<tr>
		<td>&nbsp;</td>
		<td><span style="font-weight:bold;">M&ecirc;s Ref.</span><br/>10/2011</td>
		<td><span style="font-weight:bold;">Centro de custo</span><br/>'.$rc->mNO_EMBARCACAO_PROJETO.'</td>
		<td><span style="font-weight:bold;">Autorizado por:</span><br/>'.$rc->mno_solicitador.'</td>
	</tr>
</table>
<table class="candidatos">
	<col width="80px"></col><col width="200px"></col><col width="auto"></col><col width="100px"></col>';
//Loop de candidatos
while ($rs = mysql_fetch_array($query)) {
	$html .= '
	<tr class="cand">
		<td colspan="4">'.$rs['NOME_COMPLETO'].'</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><span style="font-weight:bold;">Item</span><br/>'.$rs['prec_tx_item'].'</td>
		<td><span style="font-weight:bold;">Descri&ccedil;&atilde;o</span><br/>'.$rs['prec_tx_descricao_tabela_precos'].'</td>
		<td><span style="font-weight:bold;">Valor</span><br/>'.$rs['soli_vl_cobrado'].'</td>
	</tr>
	<tr>
		<td colspan="4" style="text-align:right;">Sub-total: '.$rs['soli_vl_cobrado'].'</td>
	</tr>';
}
$html.= '</table>
<table class="totalgeral">
	<col width="50%"></col><col width="50%"></col>
	<tr>
		<td>Total de candidatos: '.$totalCand.'</td><td>TOTAL GERAL R$ '.$totalValor.'</td>
	</tr>
</table>
Observa&ccedil;&otilde;es:<br/>'.$rc->mresc_tx_observacoes.'

</body>
';

$html = cBANCO::SemCaracteresEspeciaisMS($html);

$mpdf=new mPDF();
$mpdf->img_dpi = 200;
$mpdf->WriteHTML($html);
$mpdf->Output();
