<?php

function ListaDocumentos($idEmpresa,$idCandidato,$idAdmin,$dtInc) {
  global $usulogado;
  $ret = "";
  $sql = "select NU_SEQUENCIAL,NU_EMPRESA,NU_CANDIDATO,NO_ARQUIVO,NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NU_ADMIN ";
  $sql = $sql." from ARQUIVOS where NU_SEQUENCIAL>0";
  if($idEmpresa>0) { $sql=$sql." and NU_EMPRESA=$idEmpresa "; }
  if($idCandidato>0) { $sql=$sql." and NU_CANDIDATO=$idCandidato "; }
  if($idAdmin>0) { $sql=$sql." and NU_ADMIN=$idAdmin "; }
  if(strlen($dtInc)>0) { $sql=$sql." and DT_INCLUSAO='$dtInc' "; }
  $sql = $sql." order by NU_EMPRESA,NU_CANDIDATO,DT_INCLUSAO,TP_ARQUIVO,NO_ARQUIVO ";
  $ret = mysql_query($sql);
  return $ret;
}

function ListaTipoDocumentos() {
  $ret[1] = "Pagina de identificacao do passaporte";
  $ret[2] = "Pagina do visto";
  $ret[3] = "Pagina do registro";
  $ret[4] = "Protocolo de Registro (Frente/verso)";
  $ret[5] = "Carteira de Identidade para estrangeiros";
  $ret[6] = "CPF";
  $ret[7] = "Carteira Motorista";
  $ret[8] = "Protocolo de prorrogacao de prazo de estada ";
  $ret[9] = "Dados Completos";
  $ret[10] = "C. V.";
  $ret[11] = "P. V.";
  $ret[12] = "P. T.";
  $ret[13] = "D. R.";
  $ret[14] = "Cert. Tramite";
  $ret[15] = "Pag. Auto";
  $ret[16] = "Pag. DE";
  $ret[17] = "DINCRE";
  return $ret;
}

function GravaDocumentos() {
  global $mydocdir,$mydocurl;
  $idEmpresa = $_POST['idEmpresa'];
  $idCandidato = $_POST['idCandidato'];
  if(!is_dir($mydocdir)) {
    mkdir ($mydocdir, 0770);
  }
  if($idEmpresa!='') {
    $updir = $mydocdir."/".$idEmpresa;
    if(!is_dir($updir)) {
      mkdir ($updir, 0770);
    }
    if($idCandidato!='') {
      $updir = $updir."/".$idCandidato;
    } else {
      $updir = $updir."/geral";
    }
  } else {
    $updir = $mydocdir."/geral";
  }
  if(!is_dir($updir)) {
    mkdir ($updir, 0770);
  }
  $id = pegaProximo("ARQUIVOS","NU_SEQUENCIAL");
  $ret = GravaDocumentoDir($updir,$id,$idEmpresa,$idCandidato);
  return $ret;
}

function GravaDocumentoDir($mydir,$id,$idEmpresa,$idCandidato) {
  $ret = "";
  $tpArquivo = $_POST['tipo'];
  $noArqOriginal = $_FILES['userfile']['name'];
  $noArqOriginal = str_replace("'","",$noArqOriginal);
  $ax1 = str_replace(".","#",$noArqOriginal);
  $aux = split("#",$ax1);
  $x = count($aux);
  if($x>1) {
    $extensao = $aux[$x-1];
  } else {
    $extensao = "txt";
  }
  $noArquivo = "E".$idEmpresa."C".$idCandidato."SQ".$id.".".$extensao;
  $uploadfile = $mydir."/".$noArquivo;
  try
  {
  	move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
  	$ret = GravaDocumentoDB($id,$idEmpresa,$idCandidato,$noArquivo,$noArqOriginal,$tpArquivo);
  }
  catch(Exception $e)
  {
  	$ret = 'Não foi possível mover o arquivo. Erro:'.$e->getMessage();
  }
  return $ret;
}

function GravaDocumentoDB($id,$idEmpresa,$idCandidato,$noArquivo,$noArqOriginal,$tpArquivo) {
  global $usulogado,$noOriginal,$tipoArquivo;
  $ret = "";
  $noOriginal=$noArqOriginal;
  $tipoArquivo=$tpArquivo;
  $sql = "insert into ARQUIVOS (NU_SEQUENCIAL,NU_EMPRESA,NU_CANDIDATO,NO_ARQUIVO,";
  $sql = $sql."NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NU_ADMIN) values ($id,$idEmpresa,";
  $sql = $sql."$idCandidato,'$noArquivo','$noArqOriginal','$tpArquivo',now(),$usulogado)";
  mysql_query($sql);
  if(mysql_errno()>0) {
    $ret = mysql_error()." - sql=$sql ";
  }
  return $ret;
}

function PegaEmbarcacaoCandidato($idEmpresa,$idCandidato,$idSolicitacao) {
  $ret = "";
  $sql = "select NU_EMBARCACAO_PROJETO from AUTORIZACAO_CANDIDATO where NU_CANDIDATO=$idCandidato ";
  if($idEmpresa>0) {
    $sql = $sql." and NU_EMPRESA=$idEmpresa";
  }
  if($idSolicitacao>0) {
    $sql = $sql." and NU_SOLICITACAO=$idSolicitacao";
  } else {
    $sql = $sql." and NU_SOLICITACAO in (select max(NU_SOLICITACAO) where NU_CANDIDATO=$idCandidato ) ";
  }
  if ($rs = mysql_query($sql))
  {
	  if($rw = mysql_fetch_array($rs)) {
	     $ret = $rw[0];
	  }
  }
  return $ret;
}

function RemoveDocumento($idArquivo) {
  global $mydocdir,$mydocurl;
  $sql = "select NU_EMPRESA,NU_CANDIDATO,NO_ARQUIVO from ARQUIVOS where NU_SEQUENCIAL=$idArquivo";
  $rs = mysql_query($sql);
  if($rw = mysql_fetch_array($rs)) {
     $idEmpresa = $rw['NU_EMPRESA'];
     $idCandidato = $rw['NU_CANDIDATO'];
     $nmArquivo = $rw['NO_ARQUIVO'];
     $sql = "delete from ARQUIVOS where NU_SEQUENCIAL=$idArquivo";
     mysql_query($sql);
     $myArq = $mydocdir."/".$idEmpresa."/".$idCandidato."/".$nmArquivo;
     if (file_exists($myArq)) {
       unlink($myArq);
     } 
  }
}

?>
