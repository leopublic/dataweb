<?php

function ListaEmpresas($empresa,$lstCand) {
  $ret = null;
  $x = 0;
  $sql = "select NU_EMPRESA,NO_RAZAO_SOCIAL as NO_EMPRESA from EMPRESA ";
  if( (strlen($empresa)>0) && ($empresa>0) ) {
    $sql = $sql."where NU_EMPRESA=$empresa";
  }
  $rs = mysql_query($sql);
  if(mysql_errno()>0) {
     $ret = mysql_error()."<!-- SQL=$sql -->";
  } else {
     while($rw=mysql_fetch_array($rs)) {
        $aux = new objEmpresa();
        $aux->setNU_EMPRESA($rw[0]);
        $aux->setNO_EMPRESA($rw[1]);
#        $projetos = ListaProjetos($rw[0],$projeto);
#        $aux->setPROJETOS($projetos);
        $candidatos = ListaCandidatos($rw[0],$lstCand);
        $aux->setCANDIDATOS($candidatos);
        $ret[$x]=$aux;
        $x++;
     }
  }
  return $ret;
}

function ListaProjetos($empresa,$projeto) {
  $ret = "";
  $x = 0;
  $sql = "select NU_EMPRESA,NU_EMBARCACAO_PROJETO,NO_EMBARCACAO_PROJETO ";
  $sql = $sql."from EMBARCACAO_PROJETO where NU_EMPRESA=$empresa order by NO_EMBARCACAO_PROJETO ";
  if( (strlen($projeto)>0) && ($projeto>0) ) {
    $sql = $sql."and NU_EMBARCACAO_PROJETO=$projeto";
  }
  $rs = mysql_query($sql);
  if(mysql_errno()>0) {
     $ret = mysql_error()."<!-- SQL=$sql -->";
  } else {
     while($rw=mysql_fetch_array($rs)) {
        $aux = new objProjeto();
        $aux->setNU_EMPRESA($rw[0]);
        $aux->setNU_EMBARCACAO_PROJETO($rw[1]);
        $aux->setNO_EMBARCACAO_PROJETO($rw[2]);
        $candidatos = ListaCandidatos($rw[0],$rw[1]);
        $aux->setCANDIDATOS($candidatos);
        $ret[$x]=$aux;
        $x++;
     }
  }
  return $ret;
}

function ListaCandidatos($empresa,$lstCand) {
  $ret = "";
  $x=0;
  $sql = "select concat(C.NO_PRIMEIRO_NOME,' ',ifnull(NO_NOME_MEIO,''),' ',NO_ULTIMO_NOME) as NOME_CANDIDATO,";
  $sql = $sql." C.NU_CANDIDATO as CODIGO , ";
  $sql = $sql."C.NU_EMPRESA,C.NU_EMBARCACAO_PROJETO,CO_NACIONALIDADE,AC.CO_FUNCAO_CANDIDATO,";
  $sql = $sql."C.NU_PASSAPORTE,C.DT_VALIDADE_PASSAPORTE,C.NU_RNE,AC.DT_PRAZO_ESTADA,";
  $sql = $sql."AC.NU_PROTOCOLO_CIE,AC.DT_VALIDADE_PROTOCOLO_CIE,AC.NU_PROTOCOLO_PRORROGACAO,";
  $sql = $sql."AC.DT_VALIDADE_PROTOCOLO_PROR,AC.DT_PRETENDIDA_PRORROGACAO,";
  $sql = $sql."DATEDIFF(AC.DT_PRAZO_ESTADA,NOW()) AS DIAS_ESTADA,";
  $sql = $sql."DATEDIFF(AC.DT_VALIDADE_PROTOCOLO_CIE,NOW()) AS DIAS_CIE ";
  $sql = $sql."from CANDIDATO C, AUTORIZACAO_CANDIDATO AC ";
  $sql = $sql."where C.NU_CANDIDATO = AC.NU_CANDIDATO ";
  $sql = $sql."and C.NU_EMPRESA=$empresa ";
  $sql = $sql."AND AC.NU_SOLICITACAO in (SELECT max(NU_SOLICITACAO) FROM AUTORIZACAO_CANDIDATO c where c.NU_CANDIDATO = AC.NU_CANDIDATO)";
  if(strlen($lstCand)>0) { 
     $sql = $sql."and C.NU_CANDIDATO in ($lstCand) ";
  }
  $sql = $sql."order by  NOME_CANDIDATO";
  $rs=mysql_query($sql);
  if(mysql_errno()>0) {
  } else {
    while($rw1=mysql_fetch_array($rs)) {
      $aux = new objCandidato();
      $aux->setNU_CANDIDATO($rw1['CODIGO']);
      $aux->setNU_EMPRESA($rw1['NU_EMPRESA']);
      $aux->setNU_EMBARCACAO_PROJETO($rw1['NU_EMBARCACAO_PROJETO']);
      $aux->setNOME_CANDIDATO($rw1['NOME_CANDIDATO']);
      $aux->setCO_NACIONALIDADE(0+$rw1['CO_NACIONALIDADE']);
      $aux->setCO_FUNCAO_CANDIDATO(0+$rw1['CO_FUNCAO_CANDIDATO']);
      $aux->setNU_PASSAPORTE($rw1['NU_PASSAPORTE']);
      $aux->setDT_VALIDADE_PASSAPORTE($rw1['DT_VALIDADE_PASSAPORTE']);
      $aux->setNU_RNE($rw1['NU_RNE']);
      $aux->setDT_PRAZO_ESTADA($rw1['DT_PRAZO_ESTADA']);
      $aux->setNU_PROTOCOLO_CIE($rw1['NU_PROTOCOLO_CIE']);
      $aux->setDT_VALIDADE_PROTOCOLO_CIE($rw1['DT_VALIDADE_PROTOCOLO_CIE']);
      $aux->setNU_PROTOCOLO_PRORROGACAO($rw1['NU_PROTOCOLO_PRORROGACAO']);
      $aux->setDT_VALIDADE_PROTOCOLO_PROR($rw1['DT_VALIDADE_PROTOCOLO_PROR']);
      $aux->setDT_PRETENDIDA_PRORROGACAO($rw1['DT_PRETENDIDA_PRORROGACAO']);
      $aux->setDIAS_ESTADA($rw1['DIAS_ESTADA']);
      $aux->setDIAS_CIE($rw1['DIAS_CIE']);
      $ret[$x]=$aux;
      $x++;
    }
  }
  return $ret;
}

class objEmpresa {
 var $NU_EMPRESA = 0;
 var $NO_EMPRESA = "";
 var $PROJETOS="";
 var $CANDIDATOS="";
 var $QTD_ESTR = 0;
 var $QTD_ESTA = 0;
 var $QTD_PRR = 0;
 var $QTD_CIE = 0;
 function objEmpresa() { }
 function getNU_EMPRESA() { return $this->NU_EMPRESA; }
 function getNO_EMPRESA() { return $this->NO_EMPRESA; }
 function getQTD_ESTR() { return $this->QTD_ESTR; }
 function getQTD_ESTA() { return $this->QTD_ESTA; }
 function getQTD_PRR() { return $this->QTD_PRR; }
 function getQTD_CIE() { return $this->QTD_CIE; }
 function getPROJETOS() { return $this->PROJETOS; }
 function getCANDIDATOS() { return $this->CANDIDATOS; }
 function setNU_EMPRESA($aux) { $this->NU_EMPRESA=$aux; }
 function setNO_EMPRESA($aux) { $this->NO_EMPRESA=$aux; }
 function setQTD_ESTR($aux) { $this->QTD_ESTR=$aux; }
 function setQTD_ESTA($aux) { $this->QTD_ESTA=$aux; }
 function setQTD_PRR($aux) { $this->QTD_PRR=$aux; }
 function setQTD_CIE($aux) { $this->QTD_CIE=$aux; }
 function setPROJETOS($aux) { $this->PROJETOS=$aux; }
 function setCANDIDATOS($aux) { $this->CANDIDATOS=$aux; }
}

class objProjeto {
 var $NU_EMPRESA=0;
 var $NU_EMBARCACAO_PROJETO=0;
 var $NO_EMBARCACAO_PROJETO="";
 var $CANDIDATOS="";
 var $QTD_ESTR = 0;
 var $QTD_ESTA = 0;
 var $QTD_PRR = 0;
 var $QTD_CIE = 0;
 function objProjeto() { }
 function getNU_EMPRESA() { return $this->NU_EMPRESA; }
 function getNU_EMBARCACAO_PROJETO() { return $this->NU_EMBARCACAO_PROJETO; }
 function getNO_EMBARCACAO_PROJETO() { return $this->NO_EMBARCACAO_PROJETO; }
 function getCANDIDATOS() { return $this->CANDIDATOS; }
 function getQTD_ESTR() { return $this->QTD_ESTR; }
 function getQTD_ESTA() { return $this->QTD_ESTA; }
 function getQTD_PRR() { return $this->QTD_PRR; }
 function getQTD_CIE() { return $this->QTD_CIE; }
 function setNU_EMPRESA($aux) { $this->NU_EMPRESA = $aux; }
 function setNU_EMBARCACAO_PROJETO($aux) { $this->NU_EMBARCACAO_PROJETO = $aux; }
 function setNO_EMBARCACAO_PROJETO($aux) { $this->NO_EMBARCACAO_PROJETO = $aux; }
 function setCANDIDATOS($aux) { $this->CANDIDATOS = $aux; }
 function setQTD_ESTR($aux) { $this->QTD_ESTR=$aux; }
 function setQTD_ESTA($aux) { $this->QTD_ESTA=$aux; }
 function setQTD_PRR($aux) { $this->QTD_PRR=$aux; }
 function setQTD_CIE($aux) { $this->QTD_CIE=$aux; }
}

class objCandidato {
 var $NU_EMPRESA=0;
 var $NU_EMBARCACAO_PROJETO=0;
 var $NOME_CANDIDATO = "";
 var $NU_CANDIDATO = 0;
 var $CO_NACIONALIDADE = 0;
 var $CO_FUNCAO_CANDIDATO = 0;
 var $NU_PASSAPORTE = "";
 var $DT_VALIDADE_PASSAPORTE = "";
 var $NU_RNE = "";
 var $DT_PRAZO_ESTADA = "";
 var $NU_PROTOCOLO_CIE = "";
 var $DT_VALIDADE_PROTOCOLO_CIE = "";
 var $NU_PROTOCOLO_PRORROGACAO = "";
 var $DT_VALIDADE_PROTOCOLO_PROR = "";
 var $DT_PRETENDIDA_PRORROGACAO = "";
 var $DIAS_ESTADA = 0;
 var $DIAS_CIE = 0;
 function objCandidato() { }
 function getNU_EMPRESA() { return $this->NU_EMPRESA; }
 function getNU_EMBARCACAO_PROJETO() { return $this->NU_EMBARCACAO_PROJETO; }
 function getNU_CANDIDATO() { return $this->NU_CANDIDATO; }
 function getNOME_CANDIDATO() { return $this->NOME_CANDIDATO; }
 function getCO_NACIONALIDADE() { return $this->CO_NACIONALIDADE; }
 function getCO_FUNCAO_CANDIDATO() { return $this->CO_FUNCAO_CANDIDATO; }
 function getNU_PASSAPORTE() { return $this->NU_PASSAPORTE; }
 function getDT_VALIDADE_PASSAPORTE() { return $this->DT_VALIDADE_PASSAPORTE; }
 function getNU_RNE() { return $this->NU_RNE; }
 function getDT_PRAZO_ESTADA() { return $this->DT_PRAZO_ESTADA; }
 function getNU_PROTOCOLO_CIE() { return $this->NU_PROTOCOLO_CIE; }
 function getDT_VALIDADE_PROTOCOLO_CIE() { return $this->DT_VALIDADE_PROTOCOLO_CIE; }
 function getNU_PROTOCOLO_PRORROGACAO() { return $this->NU_PROTOCOLO_PRORROGACAO; }
 function getDT_VALIDADE_PROTOCOLO_PROR() { return $this->DT_VALIDADE_PROTOCOLO_PROR; }
 function getDT_PRETENDIDA_PRORROGACAO() { return $this->DT_PRETENDIDA_PRORROGACAO; }
 function getDIAS_ESTADA() { return $this->DIAS_ESTADA; }
 function getDIAS_CIE() { return $this->DIAS_CIE; }
 function setNU_EMPRESA($aux) { $this->NU_EMPRESA = $aux; }
 function setNU_EMBARCACAO_PROJETO($aux) { $this->NU_EMBARCACAO_PROJETO = $aux; }
 function setNU_CANDIDATO($aux) { $this->NU_CANDIDATO = $aux; }
 function setNOME_CANDIDATO($aux) { $this->NOME_CANDIDATO = $aux; }
 function setCO_NACIONALIDADE($aux) { $this->CO_NACIONALIDADE = $aux; }
 function setCO_FUNCAO_CANDIDATO($aux) { $this->CO_FUNCAO_CANDIDATO = $aux; }
 function setNU_PASSAPORTE($aux) { $this->NU_PASSAPORTE = $aux; }
 function setDT_VALIDADE_PASSAPORTE($aux) { $this->DT_VALIDADE_PASSAPORTE = $aux; }
 function setNU_RNE($aux) { $this->NU_RNE = $aux; }
 function setDT_PRAZO_ESTADA($aux) { $this->DT_PRAZO_ESTADA = $aux; }
 function setNU_PROTOCOLO_CIE($aux) { $this->NU_PROTOCOLO_CIE = $aux; }
 function setDT_VALIDADE_PROTOCOLO_CIE($aux) { $this->DT_VALIDADE_PROTOCOLO_CIE = $aux; }
 function setNU_PROTOCOLO_PRORROGACAO($aux) { $this->NU_PROTOCOLO_PRORROGACAO = $aux; }
 function setDT_VALIDADE_PROTOCOLO_PROR($aux) { $this->DT_VALIDADE_PROTOCOLO_PROR = $aux; }
 function setDT_PRETENDIDA_PRORROGACAO($aux) { $this->DT_PRETENDIDA_PRORROGACAO = $aux; }
 function setDIAS_ESTADA($aux) { $this->DIAS_ESTADA = $aux; }
 function setDIAS_CIE($aux) { $this->DIAS_CIE = $aux; }
}

function MostraCabecaCand() {
  global $nome,$nacional,$funcao,$passnum,$passdata,$rne,$estada,$cieprot,$cieval,$prorrnum,$prorrval,$prorrpre;
  global $titNome,$titNacionalidade,$titFuncao,$titPassaport,$titDatPassap,$titRNE,$titEstada;
  global $titProtocoloCIE,$titValidadeCIE,$titNumProrr,$titValProrr,$titPretProrr;
  $ret = $ret."<tr><td align=center width=30><b>Seq</td>\n";
  if($nome==true) { $ret = $ret."<td align=center width=140><nobr><b>$titNome</td>\n"; }
  if($nacional==true) { $ret = $ret."<td align=center width=140><nobr><b>$titNacionalidade</td>\n"; }
  if($funcao==true) { $ret = $ret."<td align=center width=140><nobr><b>$titFuncao</td>\n"; }
  if($passnum==true) { $ret = $ret."<td align=center width=80><nobr><b>$titPassaport</td>\n"; }
  if($passdata==true) { $ret = $ret."<td align=center width=80><nobr><b>$titDatPassap</td>\n"; }
  if($rne==true) { $ret = $ret."<td align=center width=80><nobr><b>$titRNE</td>\n"; }
  if($estada==true) { $ret = $ret."<td align=center width=80><nobr><b>$titEstada</td>\n"; }
  if($cieprot==true) { $ret = $ret."<td align=center width=80><nobr><b>$titProtocoloCIE</td>\n"; }
  if($cieval==true) { $ret = $ret."<td align=center width=80><nobr><b>$titValidadeCIE</td>\n"; }
  if($prorrnum==true) { $ret = $ret."<td align=center width=80><nobr><b>$titNumProrr</td>\n"; }
  if($prorrval==true) { $ret = $ret."<td align=center width=80><nobr><b>$titValProrr</td>\n"; }
  if($prorrpre==true) { $ret = $ret."<td align=center width=80><nobr><b>$titPretProrr</td>\n"; }
  $ret = $ret."</tr>\n";
  return $ret;
}

function MostraCandidato($candidato,$seq) {
   global $nome,$nacional,$funcao,$passnum,$passdata,$rne,$estada,$cieprot,$cieval,$prorrnum,$prorrval,$prorrpre;
   $NOME_CANDIDATO = $candidato->getNOME_CANDIDATO();
   $CO_NACIONALIDADE = $candidato->getCO_NACIONALIDADE();
   $CO_FUNCAO_CANDIDATO = $candidato->getCO_FUNCAO_CANDIDATO();
   $NU_PASSAPORTE = $candidato->getNU_PASSAPORTE();
   $DT_VALIDADE_PASSAPORTE = $candidato->getDT_VALIDADE_PASSAPORTE();
   $NU_RNE = $candidato->getNU_RNE();
   $DT_PRAZO_ESTADA = $candidato->getDT_PRAZO_ESTADA();
   $NU_PROTOCOLO_CIE = $candidato->getNU_PROTOCOLO_CIE();
   $DT_VALIDADE_PROTOCOLO_CIE = $candidato->getDT_VALIDADE_PROTOCOLO_CIE();
   $NU_PROTOCOLO_PRORROGACAO = $candidato->getNU_PROTOCOLO_PRORROGACAO();
   $DT_VALIDADE_PROTOCOLO_PROR = $candidato->getDT_VALIDADE_PROTOCOLO_PROR();
   $DT_PRETENDIDA_PRORROGACAO = $candidato->getDT_PRETENDIDA_PRORROGACAO();
   $DIAS_ESTADA = $candidato->getDIAS_ESTADA();
   $DIAS_CIE = $candidato->getDIAS_CIE();
   $ret = "<tr><td align=center>$seq</td>\n";
   if($nome==true) { $ret = $ret."<td><nobr>&#160;$NOME_CANDIDATO</td>\n"; }
   if($nacional==true) { $ret = $ret."<td><nobr>&#160;".pegaNomeNacionalidade($CO_NACIONALIDADE)."</td>\n"; }
   if($funcao==true) { $ret = $ret."<td><nobr>&#160;".pegaNomeFuncao($CO_FUNCAO_CANDIDATO)."</td>\n"; }
   if($passnum==true) { $ret = $ret."<td align=center><nobr>&#160;$NU_PASSAPORTE</td>\n"; }
   if($passdata==true) { $ret = $ret."<td align=center><nobr>&#160;".dataMy2BR($DT_VALIDADE_PASSAPORTE)."</td>\n"; }
   if($rne==true) { $ret = $ret."<td align=center><nobr>&#160;$NU_RNE</td>\n"; }
   if($estada==true) { $ret = $ret."<td align=center><nobr>&#160;".dataMy2BR($DT_PRAZO_ESTADA)."</td>\n"; }
   if($cieprot==true) { $ret = $ret."<td align=center><nobr>&#160;$NU_PROTOCOLO_CIE</td>\n"; }
   if($cieval==true) { $ret = $ret."<td align=center><nobr>&#160;".dataMy2BR($DT_VALIDADE_PROTOCOLO_CIE)."</td>\n"; }
   if($prorrnum==true) { $ret = $ret."<td align=center><nobr>&#160;$NU_PROTOCOLO_PRORROGACAO</td>\n"; }
   if($prorrval==true) { $ret = $ret."<td align=center><nobr>&#160;".dataMy2BR($DT_VALIDADE_PROTOCOLO_PROR)."</td>\n"; }
   if($prorrpre==true) { $ret = $ret."<td align=center><nobr>&#160;".dataMy2BR($DT_PRETENDIDA_PRORROGACAO)."</td>\n"; }
   $ret = $ret."</tr>\n";
   return $ret;
}

function MostraCabecaRel() {
  global $lang;
  if($lang=="E") {
    $ret = "<tr><td align=center><b>Foreigners<b></td>\n<td align=center><b>Visa Validity &lt; 6 month<b></td>\n";
    $ret = $ret."<td align=center><b>With Extension<b></td>\n<td align=center><b>Extension Validity Date &lt; 60 days<b></td></tr>\n";
  } else {
    $ret = "<tr><td align=center><b>Estrangeiros<b></td>\n<td align=center><b>Estada &lt; 6 meses<b></td>\n";
    $ret = $ret."<td align=center><b>Com Prorrogação<b></td>\n<td align=center><b>Prot. vencem em 60 dias<b></td></tr>\n";
  }
  return $ret;
}

function MostraRelatorio($rel) {
   $col1 = $rel->getQTD_ESTR();
   $col2 = $rel->getQTD_ESTA();
   $col3 = $rel->getQTD_PRR();
   $col4 = $rel->getQTD_CIE();
   $ret = "<tr><td align=center>$col1</td>\n";
   $ret = $ret."<td align=center>$col2</td>\n";
   $ret = $ret."<td align=center>$col3</td>\n";
   $ret = $ret."<td align=center>$col4</td>\n";
   $ret = $ret."</tr>\n";
   return $ret;
}

function MostraCabecaRelOpt() {
  global $lang;
  if($lang=="E") {
    $ret = "<tr bgcolor=#eeeeee><td align=center><b>Project<b></td>\n<td align=center><b>Foreigners<b></td>\n<td align=center><b>Visa Validity &lt; 6 month<b></td>\n";
    $ret = $ret."<td align=center><b>With Extension<b></td>\n<td align=center><b>Extension Validity Date &lt; 60 days<b></td></tr>\n";
  } else {
    $ret = "<tr bgcolor=#eeeeee><td align=center><b>Projeto<b></td>\n<td align=center><b>Estrangeiros<b></td>\n<td align=center><b>Estada &lt; 6 meses<b></td>\n";
    $ret = $ret."<td align=center><b>Com Prorrogação<b></td>\n<td align=center><b>Prot. vencem em 60 dias<b></td></tr>\n";
  }
  return $ret;
}

function MostraRelatorioOpt($rel,$nome) {
   $bg="";
   if($nome=="Total") { $bg="bgcolor='#eeeeee'"; }
   $col1 = $rel->getQTD_ESTR();
   $col2 = $rel->getQTD_ESTA();
   $col3 = $rel->getQTD_PRR();
   $col4 = $rel->getQTD_CIE();
   $ret = "<tr $bg><td align=center><b>$nome</td>\n";
   $ret = $ret."<td align=center>$col1</td>\n";
   $ret = $ret."<td align=center>$col2</td>\n";
   $ret = $ret."<td align=center>$col3</td>\n";
   $ret = $ret."<td align=center>$col4</td>\n";
   $ret = $ret."</tr>\n";
   return $ret;
}

?>
