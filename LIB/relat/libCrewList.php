<?php

function listaCandidatos($idEmpresa,$idEmbarcacao,$idCandidato) {
  global $totallista,$NU_CANDIDATO,$NO_PRIMEIRO_NOME,$NO_NOME_MEIO,$NO_ULTIMO_NOME,$NU_PASSAPORTE,$DT_VALIDADE_PASSAPORTE;
  global $DT_ABERTURA_PROCESSO_MTE,$NU_PROCESSO_MTE,$NU_AUTORIZACAO_MTE,$DT_AUTORIZACAO_MTE,$DT_PRAZO_AUTORIZACAO_MTE;
  global $NU_PROTOCOLO_CIE,$DT_PROTOCOLO_CIE,$DT_VALIDADE_PROTOCOLO_CIE,$DT_PRAZO_ESTADA,$DT_VALIDADE_CIE,$DT_EMISSAO_CIE;
  global $NU_PROTOCOLO_PRORROGACAO,$DT_PROTOCOLO_PRORROGACAO,$DT_VALIDADE_PROTOCOLO_PROR,$DT_PRETENDIDA_PRORROGACAO;
  global $DT_CANCELAMENTO,$NU_PROCESSO_CANCELAMENTO,$DT_PROCESSO_CANCELAMENTO,$DT_PRAZO_ESTADA_SOLICITADO;

  $sql = "select a.NU_CANDIDATO,a.NO_PRIMEIRO_NOME,a.NO_NOME_MEIO,a.NO_ULTIMO_NOME,a.NU_PASSAPORTE,a.DT_VALIDADE_PASSAPORTE,b.DT_PRAZO_ESTADA,";
  $sql = $sql."b.DT_ABERTURA_PROCESSO_MTE,b.NU_PROCESSO_MTE,b.NU_AUTORIZACAO_MTE,b.DT_AUTORIZACAO_MTE,b.DT_PRAZO_AUTORIZACAO_MTE,";
  $sql = $sql."b.NU_PROTOCOLO_CIE,b.DT_PROTOCOLO_CIE,b.DT_VALIDADE_PROTOCOLO_CIE,b.DT_VALIDADE_CIE,b.DT_EMISSAO_CIE,b.NU_EMISSAO_CIE,";
  $sql = $sql."b.NU_PROTOCOLO_PRORROGACAO,b.DT_PROTOCOLO_PRORROGACAO,b.DT_VALIDADE_PROTOCOLO_PROR,b.DT_PRETENDIDA_PRORROGACAO,";
  $sql = $sql."b.DT_CANCELAMENTO,b.NU_PROCESSO_CANCELAMENTO,b.DT_PROCESSO_CANCELAMENTO,b.DT_PRAZO_ESTADA_SOLICITADO ";
  $sql = $sql."from CANDIDATO a,AUTORIZACAO_CANDIDATO b where a.NU_CANDIDATO=b.NU_CANDIDATO and b.NU_EMPRESA=$idEmpresa and b.CD_EMBARCADO=1 ";
  $sql = $sql."AND b.NU_SOLICITACAO in (SELECT max(NU_SOLICITACAO) FROM AUTORIZACAO_CANDIDATO c where c.NU_CANDIDATO = b.NU_CANDIDATO)";
  if($idEmbarcacao>0) {  $sql=$sql." and NU_EMBARCACAO_PROJETO=$idEmbarcacao ";  }
  if($idCandidato>0) {  $sql=$sql." and NU_CANDIDATO=$idCandidato ";  }
  $sql = $sql." order by a.NO_PRIMEIRO_NOME,a.NO_NOME_MEIO,a.NO_ULTIMO_NOME";
  $totallista = 0;
  $rs = mysql_query($sql);
  while($rw=mysql_fetch_array($rs)) {
    $totallista++;
    $NU_CANDIDATO[$totallista] = $rw['NU_CANDIDATO'];
    $NO_PRIMEIRO_NOME[$totallista] = $rw['NO_PRIMEIRO_NOME'];
    $NO_NOME_MEIO[$totallista] = $rw['NO_NOME_MEIO'];
    $NO_ULTIMO_NOME[$totallista] = $rw['NO_ULTIMO_NOME'];
    $DT_VALIDADE_PASSAPORTE[$totallista] = $rw['DT_VALIDADE_PASSAPORTE'];
    $NU_PASSAPORTE[$totallista] = $rw['NU_PASSAPORTE'];
    $DT_PRAZO_ESTADA[$totallista] = $rw['DT_PRAZO_ESTADA'];
    $DT_ABERTURA_PROCESSO_MTE[$totallista] = frmtDataBR($rw['DT_ABERTURA_PROCESSO_MTE']);
    $NU_PROCESSO_MTE[$totallista] = $rw['NU_PROCESSO_MTE'];
    $NU_AUTORIZACAO_MTE[$totallista] = $rw['NU_AUTORIZACAO_MTE'];
    $DT_AUTORIZACAO_MTE[$totallista] = frmtDataBR($rw['DT_AUTORIZACAO_MTE']);
    $DT_PRAZO_AUTORIZACAO_MTE[$totallista] = $rw['DT_PRAZO_AUTORIZACAO_MTE'];
    $NU_PROTOCOLO_CIE[$totallista] = $rw['NU_PROTOCOLO_CIE'];
    $DT_PROTOCOLO_CIE[$totallista] = frmtDataBR($rw['DT_PROTOCOLO_CIE']);
    $DT_VALIDADE_PROTOCOLO_CIE[$totallista] = frmtDataBR($rw['DT_VALIDADE_PROTOCOLO_CIE']);
    $DT_PRAZO_ESTADA[$totallista] = frmtDataBR($rw['DT_PRAZO_ESTADA']);
    $DT_VALIDADE_CIE[$totallista] = frmtDataBR($rw['DT_VALIDADE_CIE']);
    $DT_EMISSAO_CIE[$totallista] = frmtDataBR($rw['DT_EMISSAO_CIE']);
    $NU_EMISSAO_CIE[$totallista] = $rw['NU_EMISSAO_CIE'];
    $NU_PROTOCOLO_PRORROGACAO[$totallista] = $rw['NU_PROTOCOLO_PRORROGACAO'];
    $DT_PROTOCOLO_PRORROGACAO[$totallista] = frmtDataBR($rw['DT_PROTOCOLO_PRORROGACAO']);
    $DT_VALIDADE_PROTOCOLO_PROR[$totallista] = frmtDataBR($rw['DT_VALIDADE_PROTOCOLO_PROR']);
    $DT_PRETENDIDA_PRORROGACAO[$totallista] = frmtDataBR($rw['DT_PRETENDIDA_PRORROGACAO']);
    $DT_CANCELAMENTO[$totallista] = frmtDataBR($rw['DT_CANCELAMENTO']);
    $NU_PROCESSO_CANCELAMENTO[$totallista] = $rw['NU_PROCESSO_CANCELAMENTO'];
    $DT_PROCESSO_CANCELAMENTO[$totallista] = frmtDataBR($rw['DT_PROCESSO_CANCELAMENTO']);
    $DT_PRAZO_ESTADA_SOLICITADO[$totallista] = $rw['DT_PRAZO_ESTADA_SOLICITADO'];
  }
}




?>

 
