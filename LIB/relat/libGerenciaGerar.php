<?php

function ListaEmpresas($empresa,$projeto) {
  $ret = null;
  $x = 0;
  $sql = "select NU_EMPRESA,NO_RAZAO_SOCIAL as NO_EMPRESA from EMPRESA ";
  if( (strlen($empresa)>0) && ($empresa>0) ) {
    $sql = $sql."where NU_EMPRESA=$empresa";
  }
  //print $sql;
  $rs = mysql_query($sql);
  if(mysql_errno()>0) {
     $ret = mysql_error()."<!-- SQL=$sql -->";
  } else {
     while($rw=mysql_fetch_array($rs)) {
        $aux = new objEmpresa();
        $aux->setNU_EMPRESA($rw[0]);
        $aux->setNO_EMPRESA($rw[1]);
        $projetos = ListaProjetos($rw[0],$projeto);
        $aux->setPROJETOS($projetos);
        $ret[$x]=$aux;
        $x++;
     }
  }
  return $ret;
}

function ListaProjetos($empresa,$projeto) {
  $ret = "";
  $x = 0;
  $sql = "select NU_EMPRESA,NU_EMBARCACAO_PROJETO,NO_EMBARCACAO_PROJETO ";
  $sql = $sql."from EMBARCACAO_PROJETO where NU_EMPRESA=$empresa ";
  if( (strlen($projeto)>0) && ($projeto>0) ) {
    $sql = $sql."and NU_EMBARCACAO_PROJETO=$projeto";
  }
  $sql = $sql." order by NO_EMBARCACAO_PROJETO ";
  //print $sql;
  $rs = mysql_query($sql);
  if(mysql_errno()>0) {
     $ret = mysql_error()."<!-- SQL=$sql -->";
  } else {
     while($rw=mysql_fetch_array($rs)) {
        $aux = new objProjeto();
        $aux->setNU_EMPRESA($rw[0]);
        $aux->setNU_EMBARCACAO_PROJETO($rw[1]);
        $aux->setNO_EMBARCACAO_PROJETO($rw[2]);
        $candidatos = ListaCandidatos($rw[0],$rw[1]);
        $aux->setCANDIDATOS($candidatos);
        $ret[$x]=$aux;
        $x++;
     }
  }
  return $ret;
}

function ListaCandidatos($empresa,$projeto) {
  $ret = "";
  $x=0;
  $orderby = $_POST['orderby'];
  $filtro = $_POST['filtro'];
  $sql = "select concat(C.NO_PRIMEIRO_NOME,' ',ifnull(NO_NOME_MEIO,''),' ',NO_ULTIMO_NOME) as NOME_CANDIDATO, ";
  $sql = $sql." C.NU_EMPRESA, C.NU_EMBARCACAO_PROJETO, CO_NACIONALIDADE, ";
  $sql = $sql."C.DT_VALIDADE_PASSAPORTE ,AC.DT_PRAZO_ESTADA, ";
  $sql = $sql."AC.DT_VALIDADE_PROTOCOLO_CIE, AC.DT_VALIDADE_PROTOCOLO_PROR, ";
  $sql = $sql."DATEDIFF(AC.DT_PRAZO_ESTADA,NOW()) AS DIAS_ESTADA,";
  $sql = $sql."DATEDIFF(AC.DT_VALIDADE_PROTOCOLO_CIE,NOW()) AS DIAS_CIE, ";
  $sql = $sql."DATEDIFF(AC.DT_VALIDADE_PROTOCOLO_PROR,NOW()) AS DIAS_PROR, ";
  $sql = $sql."DATEDIFF(C.DT_VALIDADE_PASSAPORTE,NOW()) AS DIAS_PASS ";
  $sql = $sql."from CANDIDATO C, AUTORIZACAO_CANDIDATO AC ";
  $sql = $sql."where C.NU_CANDIDATO = AC.NU_CANDIDATO ";
  $sql = $sql."and AC.NU_EMPRESA=$empresa ";
  $sql = $sql."and AC.NU_EMBARCACAO_PROJETO=$projeto ";
  $sql = $sql."AND AC.NU_SOLICITACAO in (SELECT max(NU_SOLICITACAO) FROM AUTORIZACAO_CANDIDATO c where c.NU_CANDIDATO = AC.NU_CANDIDATO) ";
  $maisfiltros = "";
  if( ($filtro == "alerta") || ($filtro == "estada") ) {
     if(strlen($maisfiltros)>0) { $maisfiltros=$maisfiltros." OR "; }
     $maisfiltros = $maisfiltros." DATEDIFF(AC.DT_PRAZO_ESTADA,NOW()) < 180 ";
  }
  if( ($filtro == "alerta") || ($filtro == "estada60") ) {
     if(strlen($maisfiltros)>0) { $maisfiltros=$maisfiltros." OR "; }
     $maisfiltros = $maisfiltros." DATEDIFF(AC.DT_PRAZO_ESTADA,NOW()) < 60 ";
  }
  if( ($filtro == "alerta") || ($filtro == "pass") ) {
     if(strlen($maisfiltros)>0) { $maisfiltros=$maisfiltros." OR "; }
     $maisfiltros = $maisfiltros." DATEDIFF(C.DT_VALIDADE_PASSAPORTE,NOW()) < 180 ";
  }
  if( ($filtro == "alerta") || ($filtro == "cie") ) {
     if(strlen($maisfiltros)>0) { $maisfiltros=$maisfiltros." OR "; }
     $maisfiltros = $maisfiltros." DATEDIFF(AC.DT_VALIDADE_PROTOCOLO_CIE,NOW()) < 45 ";
  }
  if( ($filtro == "alerta") || ($filtro == "prot") ) {
     if(strlen($maisfiltros)>0) { $maisfiltros=$maisfiltros." OR "; }
     $maisfiltros = $maisfiltros." DATEDIFF(AC.DT_VALIDADE_PROTOCOLO_PROR,NOW()) < 45 ";
  }
  if( ! ($filtro == "all") && $maisfiltros != '') {
     $sql = $sql." and ( $maisfiltros )";
  }
  if(strlen($orderby)>0) { $sql=$sql." order by $orderby  "; }
//  print "SQL=$sql ";
  $rs=mysql_query($sql);
  if(mysql_errno()>0) {
# print "\nERRO: ".mysql_error()."\nSQL=$sql";
  } else {
    while($rw1=mysql_fetch_array($rs)) {
      $aux = new objCandidato();
      $aux->setNU_EMPRESA($rw['NU_EMPRESA']);
      $aux->setNU_EMBARCACAO_PROJETO('NU_EMBARCACAO_PROJETO');
      $aux->setNOME_CANDIDATO($rw1['NOME_CANDIDATO']);
      $aux->setCO_NACIONALIDADE(0+$rw1['CO_NACIONALIDADE']);
      $aux->setDT_VALIDADE_PASSAPORTE($rw1['DT_VALIDADE_PASSAPORTE']);
      $aux->setDT_PRAZO_ESTADA($rw1['DT_PRAZO_ESTADA']);
      $aux->setDT_VALIDADE_PROTOCOLO_CIE($rw1['DT_VALIDADE_PROTOCOLO_CIE']);
      $aux->setDT_VALIDADE_PROTOCOLO_PROR($rw1['DT_VALIDADE_PROTOCOLO_PROR']);
      $aux->setDIAS_ESTADA($rw1['DIAS_ESTADA']);
      $aux->setDIAS_CIE($rw1['DIAS_CIE']);
      $aux->setDIAS_PROR($rw1['DIAS_PROR']);
      $aux->setDIAS_PASS($rw1['DIAS_PASS']);
      $ret[$x]=$aux;
      $x++;
    }
  }
  return $ret;
}

class objEmpresa {
 var $NU_EMPRESA = 0;
 var $NO_EMPRESA = "";
 var $PROJETOS="";
 var $QTD_ESTA = 0;
 var $QTD_PRR = 0;
 var $QTD_CIE = 0;
 var $QTD_PASS = 0;
 var $QTD_CAND = 0;
 function objEmpresa() { }
 function getNU_EMPRESA() { return $this->NU_EMPRESA; }
 function getNO_EMPRESA() { return $this->NO_EMPRESA; }
 function getQTD_PASS() { return $this->QTD_PASS; }
 function getQTD_ESTA() { return $this->QTD_ESTA; }
 function getQTD_PRR() { return $this->QTD_PRR; }
 function getQTD_CIE() { return $this->QTD_CIE; }
 function getQTD_CAND() { return $this->QTD_CAND; }
 function getPROJETOS() { return $this->PROJETOS; }
 function setNU_EMPRESA($aux) { $this->NU_EMPRESA=$aux; }
 function setNO_EMPRESA($aux) { $this->NO_EMPRESA=$aux; }
 function setQTD_PASS($aux) { $this->QTD_PASS=$aux; }
 function setQTD_ESTA($aux) { $this->QTD_ESTA=$aux; }
 function setQTD_PRR($aux) { $this->QTD_PRR=$aux; }
 function setQTD_CIE($aux) { $this->QTD_CIE=$aux; }
 function setQTD_CAND($aux) { $this->QTD_CAND=$aux; }
 function setPROJETOS($aux) { $this->PROJETOS=$aux; }
}

class objProjeto {
 var $NU_EMPRESA=0;
 var $NU_EMBARCACAO_PROJETO=0;
 var $NO_EMBARCACAO_PROJETO="";
 var $CANDIDATOS="";
 var $QTD_ESTR = 0;
 var $QTD_ESTA = 0;
 var $QTD_PRR = 0;
 var $QTD_CIE = 0;
 var $QTD_CAND = 0;
 function objProjeto() { }
 function getNU_EMPRESA() { return $this->NU_EMPRESA; }
 function getNU_EMBARCACAO_PROJETO() { return $this->NU_EMBARCACAO_PROJETO; }
 function getNO_EMBARCACAO_PROJETO() { return $this->NO_EMBARCACAO_PROJETO; }
 function getCANDIDATOS() { return $this->CANDIDATOS; }
 function getQTD_PASS() { return $this->QTD_PASS; }
 function getQTD_ESTA() { return $this->QTD_ESTA; }
 function getQTD_PRR() { return $this->QTD_PRR; }
 function getQTD_CIE() { return $this->QTD_CIE; }
 function getQTD_CAND() { return $this->QTD_CAND; }
 function setNU_EMPRESA($aux) { $this->NU_EMPRESA = $aux; }
 function setNU_EMBARCACAO_PROJETO($aux) { $this->NU_EMBARCACAO_PROJETO = $aux; }
 function setNO_EMBARCACAO_PROJETO($aux) { $this->NO_EMBARCACAO_PROJETO = $aux; }
 function setCANDIDATOS($aux) { $this->CANDIDATOS = $aux; }
 function setQTD_PASS($aux) { $this->QTD_PASS=$aux; }
 function setQTD_ESTA($aux) { $this->QTD_ESTA=$aux; }
 function setQTD_PRR($aux) { $this->QTD_PRR=$aux; }
 function setQTD_CIE($aux) { $this->QTD_CIE=$aux; }
 function setQTD_CAND($aux) { $this->QTD_CAND=$aux; }
}

class objCandidato {
 var $NU_EMPRESA=0;
 var $NU_EMBARCACAO_PROJETO=0;
 var $NOME_CANDIDATO = "";
 var $CO_NACIONALIDADE = 0;
 var $DT_VALIDADE_PASSAPORTE = "";
 var $DT_PRAZO_ESTADA = "";
 var $DT_VALIDADE_PROTOCOLO_CIE = "";
 var $DT_VALIDADE_PROTOCOLO_PROR = "";
 var $DIAS_ESTADA = 0;
 var $DIAS_CIE = 0;
 var $DIAS_PROR = 0;
 var $DIAS_PASS = 0;
 function objCandidato() { }
 function getNU_EMPRESA() { return $this->NU_EMPRESA; }
 function getNU_EMBARCACAO_PROJETO() { return $this->NU_EMBARCACAO_PROJETO; }
 function getNOME_CANDIDATO() { return $this->NOME_CANDIDATO; }
 function getCO_NACIONALIDADE() { return $this->CO_NACIONALIDADE; }
 function getDT_VALIDADE_PASSAPORTE() { return $this->DT_VALIDADE_PASSAPORTE; }
 function getDT_PRAZO_ESTADA() { return $this->DT_PRAZO_ESTADA; }
 function getDT_VALIDADE_PROTOCOLO_CIE() { return $this->DT_VALIDADE_PROTOCOLO_CIE; }
 function getDT_VALIDADE_PROTOCOLO_PROR() { return $this->DT_VALIDADE_PROTOCOLO_PROR; }
 function getDIAS_ESTADA() { return $this->DIAS_ESTADA; }
 function getDIAS_CIE() { return $this->DIAS_CIE; }
 function getDIAS_PROR() { return $this->DIAS_PROR; }
 function getDIAS_PASS() { return $this->DIAS_PASS; }
 function setNU_EMPRESA($aux) { $this->NU_EMPRESA = $aux; }
 function setNU_EMBARCACAO_PROJETO($aux) { $this->NU_EMBARCACAO_PROJETO = $aux; }
 function setNOME_CANDIDATO($aux) { $this->NOME_CANDIDATO = $aux; }
 function setCO_NACIONALIDADE($aux) { $this->CO_NACIONALIDADE = $aux; }
 function setDT_VALIDADE_PASSAPORTE($aux) { $this->DT_VALIDADE_PASSAPORTE = $aux; }
 function setDT_PRAZO_ESTADA($aux) { $this->DT_PRAZO_ESTADA = $aux; }
 function setDT_VALIDADE_PROTOCOLO_CIE($aux) { $this->DT_VALIDADE_PROTOCOLO_CIE = $aux; }
 function setDT_VALIDADE_PROTOCOLO_PROR($aux) { $this->DT_VALIDADE_PROTOCOLO_PROR = $aux; }
 function setDIAS_ESTADA($aux) { $this->DIAS_ESTADA = $aux; }
 function setDIAS_CIE($aux) { $this->DIAS_CIE = $aux; }
 function setDIAS_PROR($aux) { $this->DIAS_PROR = $aux; }
 function setDIAS_PASS($aux) { $this->DIAS_PASS = $aux; }
}

function MostraCabecaCand() {  
  global $nome,$nacional,$passdata,$estada,$protval,$lang;
  $ret = $ret."<tr><td align=center width=30><b>Seq</td>\n";
  if($lang=="E") {
    if($nome==true) { $ret = $ret."<td align=center width=140><nobr><b>Candidates</td>\n"; }
    if($nacional==true) { $ret = $ret."<td align=center width=140><nobr><b>Nationality</td>\n"; }
    if($estada==true) { $ret = $ret."<td align=center width=80><nobr><b>Visa Validity</td>\n"; }
    if($passdata==true) { $ret = $ret."<td align=center width=80><nobr><b>Passport  Validity</td>\n"; }
    if($protval==true) { $ret = $ret."<td align=center width=80><nobr><b>Protocol Validity</td>\n"; }
    if($estada==true) { $ret = $ret."<td align=center width=80><nobr><b>Visa Validity</td>\n"; }
  } else {
    if($nome==true) { $ret = $ret."<td align=center width=140><nobr><b>Candidato</td>\n"; }
    if($nacional==true) { $ret = $ret."<td align=center width=140><nobr><b>Nacionalidade</td>\n"; }
    if($estada==true) { $ret = $ret."<td align=center width=80><nobr><b>Dias Estada</td>\n"; }
    if($passdata==true) { $ret = $ret."<td align=center width=80><nobr><b>Data Val. Passaporte</td>\n"; }
    if($protval==true) { $ret = $ret."<td align=center width=80><nobr><b>Data Val. Protocolo</td>\n"; }
    if($estada==true) { $ret = $ret."<td align=center width=80><nobr><b>Prazo Estada</td>\n"; }
  }
  $ret = $ret."</tr>\n";
  return $ret;
}

function MostraCandidato($candidato,$seq) {  
  global $nome,$nacional,$passdata,$estada,$protval;
  $NOME_CANDIDATO = $candidato->getNOME_CANDIDATO();
  $CO_NACIONALIDADE = $candidato->getCO_NACIONALIDADE();
  $DT_VALIDADE_PASSAPORTE = $candidato->getDT_VALIDADE_PASSAPORTE();
  $DT_PRAZO_ESTADA = $candidato->getDT_PRAZO_ESTADA();
  $DT_VALIDADE_PROTOCOLO_CIE = $candidato->getDT_VALIDADE_PROTOCOLO_CIE();
  $DT_VALIDADE_PROTOCOLO_PROR = $candidato->getDT_VALIDADE_PROTOCOLO_PROR();
  $DIAS_ESTADA = $candidato->getDIAS_ESTADA();
  $DIAS_CIE = $candidato->getDIAS_CIE();
  $DIAS_PROR = $candidato->getDIAS_PROR();
  $DIAS_PASS = $candidato->getDIAS_PASS();
  $fgdias = "";
  $fgpass = "";
  $fgprot = "";
  $fgest = "";
  $protdt = "";

  $ret = "<tr><td align=center>$seq</td>\n";
  if($nome==true) { $ret = $ret."<td><nobr>&#160;$NOME_CANDIDATO</td>\n"; }
  if($nacional==true) { $ret = $ret."<td><nobr>&#160;".pegaNomeNacionalidade($CO_NACIONALIDADE)."</td>\n"; }
  if($estada==true) { 
     if( ($DIAS_ESTADA <= 180) && (strlen($DIAS_ESTADA)>0) ) { $fgdias="<font color=red><b>"; }
     $ret = $ret."<td align=center><nobr>$fgdias&#160;$DIAS_ESTADA</td>\n"; 
  }
  if($passdata==true) { 
     if( ($DIAS_PASS <= 180) && (strlen($DIAS_PASS)>0) ) { $fgpass="<font color=red><b>"; }
     $ret = $ret."<td align=center><nobr>$fgpass&#160;".dataMy2BR($DT_VALIDADE_PASSAPORTE)."</td>\n"; 
  }
  if($protval==true) {
     if( strlen($DT_VALIDADE_PROTOCOLO_PROR)>0 ) {
        if( ($DIAS_PROR <= 45) && (strlen($DIAS_PROR)>0) ) { $fgprot="<font color=red><b>"; }
        $protdt = dataMy2BR($DT_VALIDADE_PROTOCOLO_PROR);
     } else if( strlen($DT_VALIDADE_PROTOCOLO_CIE)>0 ) {
        if( ($DIAS_CIE < 45) && (strlen($DIAS_CIE)>0) ) { $fgprot="<font color=red><b>"; }
        $protdt = dataMy2BR($DT_VALIDADE_PROTOCOLO_CIE);
     } 
     $ret = $ret."<td align=center><nobr>$fgprot&#160;$protdt</td>\n"; 
  }
  if($estada==true) { 
     $ret = $ret."<td align=center><nobr>$fgdias&#160;".dataMy2BR($DT_PRAZO_ESTADA)."</td>\n"; 
  }
  $ret = $ret."</tr>\n";
  return $ret;
}
