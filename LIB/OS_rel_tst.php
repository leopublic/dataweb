<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

$pID_SOLICITA_VISTO = $_GET['ID_SOLICITA_VISTO'];

/*
$tela = new cEDICAO('SOLICITA_VISTO_view');
$tela->CarregarConfiguracao();
$tela->id_solicita_visto=$pID_SOLICITA_VISTO ;
$tela->RecupereRegistro();
*/
$OS = new cORDEMSERVICO();
$OS->Recuperar($pID_SOLICITA_VISTO);
$OS->RecuperarCandidatos($pID_SOLICITA_VISTO);

$tppr_tx_nome = $OS->mtppr_tx_nome;
if ($tppr_tx_nome == ''){
	$tppr_tx_nome = '(não informado)';
}

$rs = mysql_query("SELECT DISTINCT v.*, e.NO_ENDERECO, e.NU_CNPJ FROM vSOLICITA_VISTO v LEFT JOIN EMPRESA e ON e.NU_EMPRESA = v.NU_EMPRESA WHERE v.ID_SOLICITA_VISTO = $pID_SOLICITA_VISTO") or die( mysql_error() );
$tela = mysql_fetch_object($rs);

include('../MPDF45/mpdf.php');

$html = '
<style>
	table {width:100%;border-collapse:collapse;margin-left:20px;}
	th {vertical-align:top;text-align:left;margin-left:10px;margin-right:10px;border-bottom:solid 1px #cccccc;}
	div.sep {font-size:12pt;color:#999999;font-weight:bold;border-bottom:dotted 1px #999999;vertical-align:bottom;width:100%;margin-top:20px;margin-left:20px;margin-bottom:10px;}
	td {border-bottom:solid 1px #cccccc;}
	table.grid {border-bottom:solid 3px #cccccc;}
	table.grid th{text-align:left;border-bottom:solid 3px #cccccc;}
	</style>
<body style="font-family: sans; font-size: 10pt;">
<table style="border-bottom:solid 6px #999999;margin-left:0;">
<tr>
	<td style="font-size:42pt;color:#999999;font-weight:bold">OS '.$tela->NU_SOLICITACAO.'</td>
	<td style="text-align:right"><img align="right" src="/imagens/logo_mundivisas_PANTONE.jpg" style="width:90mm"/></td>
</tr>
</table>
<div style="text-align:right;vertical-align:bottom;font-weight:bold;">
    Cliente : '.htmlentities($tela->NO_RAZAO_SOCIAL).'<br/>
    CNPJ : '.htmlentities($tela->NU_CNPJ).'<br/>
    Endere&ccedil;o : '.htmlentities($tela->NO_ENDERECO).'<br/>
    Servi&ccedil;o : &quot;'.htmlentities($tela->NO_SERVICO_RESUMIDO).'&quot;<br/>
    </div>
<div class="sep">Detalhes</div>
<table>
    <tr><th width="180px">Empresa COBRAN&Ccedil;A</th><td>'.htmlentities($tela->NO_RAZAO_SOCIAL_REQUERENTE).'</td></tr>
    <tr><th>Emb./Proj. REAL</th><td>'.htmlentities($OS->mNO_EMBARCACAO_PROJETO_COBRANCA).'&nbsp;</td></tr>
    <tr><th>Tipo de cobran&ccedil;a</th><td>'.htmlentities($tppr_tx_nome).'&nbsp;</td></tr>
    <tr><th>Prazo solicitado</th><td>'.htmlentities($tela->DT_PRAZO_ESTADA_SOLICITADO).'&nbsp;</td></tr>
    <tr><th>Solicitante</th><td>'.htmlentities($tela->no_solicitador).'&nbsp;</td></tr>
    <tr><th>Data da solicita&ccedil;&atilde;o</th><td>'.htmlentities($tela->dt_solicitacao).'&nbsp;</td></tr>
    <tr><th>Respons&aacute;vel</th><td>'.htmlentities($tela->NO_USUARIO_TECNICO).'&nbsp;</td></tr>
    <tr><th>Observa&ccedil;&otilde;es</th><td>'.str_replace("\n", "<br/>", htmlentities($tela->de_observacao)).'&nbsp;</td></tr>
</table>';
if ($OS->mCandidatos != '' ){
	$html .= '
<div class="sep">Candidatos</div><table class="grid">
    <tr><th colspan="2">Nome completo</th><th>Nacionalidade</th><th>Passaporte</th><th>RNE</th></tr>';
    $sql = "select NU_CANDIDATO, NOME_COMPLETO, NO_NACIONALIDADE, NU_PASSAPORTE, NU_RNE";
    $sql .= " from CANDIDATO C left join PAIS_NACIONALIDADE P on P.CO_PAIS = C.CO_NACIONALIDADE";
    $sql .= " where NU_CANDIDATO in (".$OS->mCandidatos.")";
    $sql .= " order by NOME_COMPLETO";
    $res = conectaQuery($sql, __FILE__);
    while($rs = mysql_fetch_array($res)){
    	$html.= '<tr><td colspan="2">'.$rs['NU_CANDIDATO'].'-'.htmlentities($rs['NOME_COMPLETO']).'</td><td>'.htmlentities($rs['NO_NACIONALIDADE']).'</td><td>'.htmlentities($rs['NU_PASSAPORTE']).'</td><td>'.htmlentities($rs['NU_RNE']).'</td></tr>';
        $sql_Dep = "select NO_DEPENDENTE, NO_NACIONALIDADE, NU_PASSAPORTE, NO_GRAU_PARENTESCO";
        $sql_Dep .= " from DEPENDENTES D left join PAIS_NACIONALIDADE P on P.CO_PAIS = D.CO_NACIONALIDADE";
        $sql_Dep .= " left join GRAU_PARENTESCO G on P.CO_PAIS = G.CO_GRAU_PARENTESCO ";
        $sql_Dep .= " where NU_CANDIDATO = {$rs['NU_CANDIDATO']}";
        $sql_Dep .= " order by NU_DEPENDENTE";
        
        $res_Dep = conectaQuery($sql_Dep, __FILE__);
        while($rsDep = mysql_fetch_array($res_Dep)){
        	$html.= '<tr><td>&nbsp;&nbsp;&nbsp;</td><td>'.htmlentities($rsDep['NO_DEPENDENTE']).'</td><td>'.htmlentities($rsDep['NO_NACIONALIDADE']).'</td><td>'.htmlentities($rsDep['NU_PASSAPORTE']).'</td><td>'.htmlentities($rsDep['NO_GRAU_PARENTESCO']).'</td></tr>';
        }
        mysql_free_result($res_Dep);
    }
    $html.= '</table>';
}
$html.=  '
	<div class="sep">Cadastro</div>
    <table>
    <tr><th width="150px">Criada por</th><td>'.htmlentities($tela->NO_USUARIO_CADASTRO).'</td></tr>
    <tr><th>Em</th><td>'.htmlentities($tela->dt_cadastro).'</td></tr>
</table>

</body>
';

$html = cBANCO::SemCaracteresEspeciaisMS($html);

$mpdf=new mPDF();
$mpdf->img_dpi = 200;
$mpdf->WriteHTML($html);
$mpdf->Output();
