<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");

$pID_SOLICITA_VISTO = $_GET['ID_SOLICITA_VISTO'];
$tela = new cEDICAO('SOLICITA_VISTO_view');
$tela->CarregarConfiguracao();
$tela->mCampoNome['id_solicita_visto']->mValor=$pID_SOLICITA_VISTO ;
$tela->RecupereRegistro();
$OS = new cORDEMSERVICO();
$OS->Recuperar($pID_SOLICITA_VISTO);
$OS->RecuperarCandidatos($pID_SOLICITA_VISTO);

include('../MPDF45/mpdf.php');

$html = '
<style>
	table {width:100%;border-collapse:collapse;margin-left:20px;}
	th {vertical-align:top;text-align:left;margin-left:10px;margin-right:10px;border-bottom:solid 1px #cccccc;}
	div.sep {font-size:12pt;color:#999999;font-weight:bold;border-bottom:dotted 1px #999999;vertical-align:bottom;width:100%;margin-top:20px;margin-left:20px;margin-bottom:10px;}
	td {border-bottom:solid 1px #cccccc;}
	table.grid {border-bottom:solid 3px #cccccc;}
	table.grid th{text-align:left;border-bottom:solid 3px #cccccc;}
	</style>
<body style="font-family: sans; font-size: 10pt;">
<table style="border-bottom:solid 6px #999999;margin-left:0;">
<tr>
	<td style="font-size:42pt;color:#999999;font-weight:bold">OS '.$tela->mCampoNome['NU_SOLICITACAO']->mValor.'</td>
	<td style="text-align:right"><img align="right" src="/imagens/logo_mundivisas_PANTONE.jpg" style="width:90mm"/></td>
</tr>
</table>
<div style="text-align:right;vertical-align:bottom;font-weight:bold;">Cliente:'.htmlentities($tela->mCampoNome['NO_RAZAO_SOCIAL']->mValor).'<br/>'.htmlentities('Serviço').':&quot;'.htmlentities($tela->mCampoNome['NO_SERVICO_RESUMIDO']->mValor).'&quot;</div>
<div class="sep">Detalhes</div>
<table>
<tr><th width="150px">Embarca&ccedil;&atilde;o/Projeto</th><td>'.htmlentities($tela->mCampoNome['NO_EMBARCACAO_PROJETO']->mValor).'</td></tr>
<tr><th>Solicitante</th><td>'.htmlentities($tela->mCampoNome['no_solicitador']->mValor).'</td></tr>
<tr><th>Data da solicita&ccedil;&atilde;o</th><td>'.htmlentities($tela->mCampoNome['dt_solicitacao']->mValor).'</td></tr>
<tr><th>Requerente</th><td>'.htmlentities($tela->mCampoNome['NO_RAZAO_SOCIAL_REQUERENTE']->mValor).'</td></tr>
<tr><th>Prazo solicitado</th><td>'.htmlentities($tela->mCampoNome['DT_PRAZO_ESTADA_SOLICITADO']->mValor).'</td></tr>
<tr><th>Respons&aacute;vel</th><td>'.htmlentities($tela->mCampoNome['NO_USUARIO_TECNICO']->mValor).'</td></tr>
<tr><th>Observa&ccedil;&otilde;es</th><td>'.str_replace("\n", "<br/>", htmlentities($tela->mCampoNome['de_observacao']->mValor)).'</td></tr>
</table>
<div class="sep">Candidatos</div>
<table class="grid">
<tr><th>Nome completo</th><th>Nacionalidade</th><th>Passaporte</th><th>RNE</th></tr>';
$sql = "select NOME_COMPLETO, NO_NACIONALIDADE, NU_PASSAPORTE, NU_RNE";
$sql .= " from CANDIDATO C left join PAIS_NACIONALIDADE P on P.CO_PAIS = C.CO_NACIONALIDADE";
$sql .= " where NU_CANDIDATO in (".$OS->mCandidatos.")";
$sql .= " order by NOME_COMPLETO";
$res = conectaQuery($sql, __FILE__);
while($rs = mysql_fetch_array($res)){
	$html.= '<tr><td>'.htmlentities($rs['NOME_COMPLETO']).'</td><td>'.htmlentities($rs['NO_NACIONALIDADE']).'</td><td>'.htmlentities($rs['NU_PASSAPORTE']).'</td><td>'.htmlentities($rs['NU_RNE']).'</td></tr>';
}
$html.= '</table>
<div class="sep">Cadastro</div>
<table>
<tr><th width="150px">Criada por</th><td>'.htmlentities($tela->mCampoNome['NO_USUARIO_CADASTRO']->mValor).'</td></tr>
<tr><th>Em</th><td>'.htmlentities($tela->mCampoNome['dt_cadastro']->mValor).'</td></tr>
</table>

</body>
';
print $html;

/*
$mpdf=new mPDF();

$mpdf->img_dpi = 200;
$mpdf->WriteHTML($html);
$mpdf->Output();
*/
?>
