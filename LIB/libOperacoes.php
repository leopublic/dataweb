<?php

function GravaVisto() {
   $msgErro = "";
   $idEmpresa = 0+$_POST['idEmpresa'];
   $idCandidato = 0+$_POST['idCandidato'];
   $NU_SOLICITACAO = 0+$_POST['NU_SOLICITACAO'];
   $DT_VALIDADE_VISTO = dataBR2My($_POST['DT_VALIDADE_VISTO']);
   $NU_VISTO = $_POST['NU_VISTO'];
   $DT_EMISSAO_VISTO = dataBR2My($_POST['DT_EMISSAO_VISTO']);
   $NO_LOCAL_EMISSAO_VISTO = $_POST['NO_LOCAL_EMISSAO_VISTO'];
   $NO_LOCAL_ENTRADA = $_POST['NO_LOCAL_ENTRADA'];
   $NO_UF_ENTRADA = $_POST['NO_UF_ENTRADA'];
   $DT_ENTRADA = dataBR2My($_POST['DT_ENTRADA']);
   $NU_TRANSPORTE_ENTRADA = $_POST['NU_TRANSPORTE_ENTRADA'];
   $CO_CLASSIFICACAO_VISTO = $_POST['CO_CLASSIFICACAO_VISTO'];
   $CO_NACIONALIDADE_VISTO = $_POST['CO_NACIONALIDADE_VISTO'];
   if(strlen($DT_ENTRADA)==0) { $DT_ENTRADA="NULL"; } else { $DT_ENTRADA="'$DT_ENTRADA'"; }
   if(strlen($DT_VALIDADE_VISTO)==0) { $DT_VALIDADE_VISTO="NULL"; } else { $DT_VALIDADE_VISTO="'$DT_VALIDADE_VISTO'"; }
   if(strlen($DT_EMISSAO_VISTO)==0) { $DT_EMISSAO_VISTO="NULL"; } else { $DT_EMISSAO_VISTO="'$DT_EMISSAO_VISTO'"; }
   if(strlen($CO_NACIONALIDADE_VISTO)==0) { $CO_NACIONALIDADE_VISTO="NULL"; }
   if(strlen($CO_CLASSIFICACAO_VISTO)==0) { $CO_CLASSIFICACAO_VISTO="NULL"; }
   $sql = $sql."update AUTORIZACAO_CANDIDATO set DT_VALIDADE_VISTO=$DT_VALIDADE_VISTO, NU_VISTO='$NU_VISTO', ";
   $sql = $sql."DT_EMISSAO_VISTO=$DT_EMISSAO_VISTO,NO_LOCAL_EMISSAO_VISTO='$NO_LOCAL_EMISSAO_VISTO', ";
   $sql = $sql."NO_LOCAL_ENTRADA='$NO_LOCAL_ENTRADA',NO_UF_ENTRADA='$NO_UF_ENTRADA',DT_ENTRADA=$DT_ENTRADA, ";
   $sql = $sql."NU_TRANSPORTE_ENTRADA='$NU_TRANSPORTE_ENTRADA',CO_CLASSIFICACAO_VISTO=$CO_CLASSIFICACAO_VISTO, ";
   $sql = $sql."CO_NACIONALIDADE_VISTO=$CO_NACIONALIDADE_VISTO where  NU_CANDIDATO=$idCandidato and NU_EMPRESA=$idEmpresa AND NU_SOLICITACAO=$NU_SOLICITACAO ";
   mysql_query($sql);
   if(mysql_errno()>0) {
     $msgErro= mysql_error();
     gravaLogErro($sql,$msgErro,"ALTERACAO OPERACIONAL","AUTORIZACAO_CANDIDATO - Visto");
   } else {
     gravaLog($sql,mysql_error(),"ALTERACAO OPERACIONAL","AUTORIZACAO_CANDIDATO - Visto");
   }
   return $msgErro;
}

function GravaRegistroCIE() {
   $msgErro = "";
   $idEmpresa = 0+$_POST['idEmpresa'];
   $idCandidato = 0+$_POST['idCandidato'];
   $NU_PROTOCOLO_CIE = $_POST['NU_PROTOCOLO_CIE'];
   $DT_PROTOCOLO_CIE = dataBR2My($_POST['DT_PROTOCOLO_CIE']);
   $DT_VALIDADE_PROTOCOLO_CIE = dataBR2My($_POST['DT_VALIDADE_PROTOCOLO_CIE']);
   $DT_PRAZO_ESTADA = dataBR2My($_POST['DT_PRAZO_ESTADA']);
   $sql = $sql."update AUTORIZACAO_CANDIDATO set NU_PROTOCOLO_CIE='$NU_PROTOCOLO_CIE', DT_PROTOCOLO_CIE='$DT_PROTOCOLO_CIE', ";
   $sql = $sql."DT_VALIDADE_PROTOCOLO_CIE='$DT_VALIDADE_PROTOCOLO_CIE',DT_PRAZO_ESTADA='$DT_PRAZO_ESTADA' ";
   $sql = $sql."where  NU_CANDIDATO=$idCandidato and NU_EMPRESA=$idEmpresa";
   mysql_query($sql);
   if(mysql_errno()>0) {
     $msgErro= mysql_error();
     gravaLogErro($sql,$msgErro,"ALTERACAO OPERACIONAL","AUTORIZACAO_CANDIDATO - Registro CIE");
   } else {
     gravaLog($sql,mysql_error(),"ALTERACAO OPERACIONAL","AUTORIZACAO_CANDIDATO - Registro CIE");
   }
   return $msgErro;
}

function GravaEmissaoCIE() {
   $msgErro = "";
   $idEmpresa = 0+$_POST['idEmpresa'];
   $idCandidato = 0+$_POST['idCandidato'];
   $NU_PROTOCOLO_CIE = $_POST['NU_PROTOCOLO_CIE'];
   $DT_EMISSAO_CIE = dataBR2My($_POST['DT_EMISSAO_CIE']);
   $DT_VALIDADE_CIE = dataBR2My($_POST['DT_VALIDADE_CIE']);
   $sql = $sql."update AUTORIZACAO_CANDIDATO set NU_PROTOCOLO_CIE='$NU_PROTOCOLO_CIE', DT_EMISSAO_CIE='$DT_EMISSAO_CIE', ";
   $sql = $sql."DT_VALIDADE_CIE='$DT_VALIDADE_CIE' ";
   $sql = $sql."where  NU_CANDIDATO=$idCandidato and NU_EMPRESA=$idEmpresa";
   mysql_query($sql);
   if(mysql_errno()>0) {
     $msgErro= mysql_error();
     gravaLogErro($sql,$msgErro,"ALTERACAO OPERACIONAL","AUTORIZACAO_CANDIDATO - Emissao CIE");
   } else {
     gravaLog($sql,mysql_error(),"ALTERACAO OPERACIONAL","AUTORIZACAO_CANDIDATO - Emissao CIE");
   }
   return $msgErro;
}

function GravaProrrogacao() {
   $msgErro = "";
   $idEmpresa = 0+$_POST['idEmpresa'];
   $idCandidato = 0+$_POST['idCandidato'];
   $NU_PROTOCOLO_PRORROGACAO = $_POST['NU_PROTOCOLO_PRORROGACAO'];
   $DT_PROTOCOLO_PRORROGACAO = dataBR2My($_POST['DT_PROTOCOLO_PRORROGACAO']);
   $DT_VALIDADE_PROTOCOLO_PROR = dataBR2My($_POST['DT_VALIDADE_PROTOCOLO_PROR']);
   $DT_PRETENDIDA_PRORROGACAO = dataBR2My($_POST['DT_PRETENDIDA_PRORROGACAO']);
   $sql = $sql."update AUTORIZACAO_CANDIDATO set NU_PROTOCOLO_PRORROGACAO='$NU_PROTOCOLO_PRORROGACAO', ";
   $sql = $sql."DT_PROTOCOLO_PRORROGACAO='$DT_PROTOCOLO_PRORROGACAO', ";
   $sql = $sql."DT_VALIDADE_PROTOCOLO_PROR='$DT_VALIDADE_PROTOCOLO_PROR', ";
   $sql = $sql."DT_PRETENDIDA_PRORROGACAO='$DT_PRETENDIDA_PRORROGACAO' ";
   $sql = $sql."where  NU_CANDIDATO=$idCandidato and NU_EMPRESA=$idEmpresa";
   mysql_query($sql);
   if(mysql_errno()>0) {
     $msgErro= mysql_error();
     gravaLogErro($sql,$msgErro,"ALTERACAO OPERACIONAL","AUTORIZACAO_CANDIDATO - Prorrogacao");
   } else {
     gravaLog($sql,mysql_error(),"ALTERACAO OPERACIONAL","AUTORIZACAO_CANDIDATO - Prorrogacao");
   }
   return $msgErro;
}

function ListaArquivos($idEmpresa,$idCandidato,$idAdmin,$dtInc) {
  global $usulogado;
  $ret = "";
  $sql = "select NU_SEQUENCIAL,NU_EMPRESA,NU_CANDIDATO,NO_ARQUIVO,NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NU_ADMIN,NO_DIRETORIO ";
  $sql = $sql." from ARQUIVOS where NU_SEQUENCIAL>0";
  if($idEmpresa>0) { $sql=$sql." and NU_EMPRESA=$idEmpresa "; }
  if($idCandidato>0) { $sql=$sql." and NU_CANDIDATO=$idCandidato "; }
  if($idAdmin>0) { $sql=$sql." and NU_ADMIN=$idAdmin "; }
  if(strlen($dtInc)>0) { $sql=$sql." and DT_INCLUSAO='$dtInc' "; }
  $sql = $sql." order by NU_EMPRESA,NU_CANDIDATO,DT_INCLUSAO,TP_ARQUIVO,NO_ARQUIVO ";
  $ret = mysql_query($sql);
  return $ret;
}

function ListaTipoArquivo() {
  $ret[1] = "Pagina de identificacao do passaporte";
  $ret[2] = "Pagina do visto";
  $ret[3] = "Pagina do registro";
  $ret[4] = "Protocolo de Registro (Frente/verso)";
  $ret[5] = "Carteira de Identidade para estrangeiros";
  $ret[6] = "CPF";
  $ret[7] = "Carteira Motorista";
  $ret[8] = "Protocolo de prorrogacao de prazo de estada ";
  $ret[9] = "Dados Completos";
  $ret[10] = "C. V.";
  $ret[11] = "P. V.";
  $ret[12] = "P. T.";
  $ret[13] = "D. R.";
  $ret[14] = "Cert. Tramite";
  $ret[15] = "Pag. Auto";
  $ret[16] = "Pag. DE";
  $ret[17] = "DINCRE";
  return $ret;
}

function GravaArquivo($diretorio) {
  $idEmpresa = $_POST['idEmpresa'];
  $idCandidato = $_POST['idCandidato'];
  $mydir = "/upload";
  if(!is_dir($mydir)) {
    mkdir ($diretorio.$mydir, 0770);
  }
  if($idEmpresa!='') {
    $updir = $mydir."/".$idEmpresa;
    if(!is_dir($diretorio.$updir)) {
      mkdir ($diretorio.$updir, 0770);
    }
    if($idCandidato!='') {
      $updir = $updir."/".$idCandidato;
    } else {
      $updir = $updir."/geral";
    }
  } else {
    $updir = $mydir."/geral";
  }
  if(!is_dir($diretorio.$updir)) {
    mkdir ($diretorio.$updir, 0770);
  }
  $id = pegaProximo("ARQUIVOS","NU_SEQUENCIAL");
  $ret = GravaArquivoDir($diretorio,$updir,$id,$idEmpresa,$idCandidato);
  return $ret;
}

function GravaArquivoDir($diretorio,$mydir,$id,$idEmpresa,$idCandidato) {
  $ret = "";
  $tpArquivo = $_POST['tipo'];
  $noArqOriginal = $_FILES['userfile']['name'];
  $ax1 = str_replace(".","#",$noArqOriginal);
  $aux = split("#",$ax1);
  $x = count($aux);
  if($x>1) {
    $extensao = $aux[$x-1];
  } else {
    $extensao = "txt";
  }
  $noArquivo = "E".$idEmpresa."C".$idCandidato."SQ".$id.".".$extensao;
  $uploadfile = $diretorio.$mydir."/".$noArquivo;
  if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    $ret = "Possivel ataque de upload! Aqui esta alguma informação:\n".print_r($_FILES);
  } else {
    $ret = GravaArquivoDB($id,$idEmpresa,$idCandidato,$noArquivo,$noArqOriginal,$tpArquivo,$mydir);
  }
  return $ret;
}

function GravaArquivoDB($id,$idEmpresa,$idCandidato,$noArquivo,$noArqOriginal,$tpArquivo,$mydir) {
  global $usulogado,$noOriginal,$tipoArquivo;
  $ret = "";
  $noOriginal=$noArqOriginal;
  $tipoArquivo=$tpArquivo;
  $sql = "insert into ARQUIVOS (NU_SEQUENCIAL,NU_EMPRESA,NU_CANDIDATO,NO_ARQUIVO,";
  $sql = $sql."NO_ARQ_ORIGINAL,TP_ARQUIVO,DT_INCLUSAO,NU_ADMIN,NO_DIRETORIO) values ($id,$idEmpresa,";
  $sql = $sql."$idCandidato,'$noArquivo','$noArqOriginal','$tpArquivo',now(),$usulogado,'$mydir')";
  mysql_query($sql);
  if(mysql_errno()>0) {
    $ret = mysql_error()." - sql=$sql ";
  }
  return $ret;
}


?>
