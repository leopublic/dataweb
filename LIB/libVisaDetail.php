<?php

class objVisaDetail {
 var $NU_EMPRESA = 0;
 var $NU_CANDIDATO = 0;
 var $NM_CANDIDATO = "";
 var $NU_SOLICITACAO = 0;
 var $NU_EMBARCACAO_PROJETO = 0;
 var $CO_TIPO_AUTORIZACAO = "";
 var $VA_RENUMERACAO_MENSAL = 0;
 var $NO_MOEDA_REMUNERACAO_MENSAL = "";
 var $VA_REMUNERACAO_MENSAL_BRASIL = 0; 
 var $CO_REPARTICAO_CONSULAR = 0;       
 var $CO_FUNCAO_CANDIDATO = 0;          
 var $DT_ABERTURA_PROCESSO_MTE = "";
 var $NU_PROCESSO_MTE = "";
 var $NU_AUTORIZACAO_MTE = "";
 var $DT_AUTORIZACAO_MTE = "";
 var $DT_PRAZO_AUTORIZACAO_MTE = "";
 var $DT_VALIDADE_VISTO  = "";
 var $NU_VISTO = "";      
 var $DT_EMISSAO_VISTO = "";
 var $NO_LOCAL_EMISSAO_VISTO = "";
 var $CO_NACIONALIDADE_VISTO = 0;
 var $NU_PROTOCOLO_CIE = "";
 var $DT_PROTOCOLO_CIE = "";
 var $DT_VALIDADE_PROTOCOLO_CIE = "";
 var $DT_PRAZO_ESTADA = "";
 var $DT_VALIDADE_CIE = "";
 var $DT_EMISSAO_CIE = "";
 var $NU_EMISSAO_CIE = "";
 var $NU_PROTOCOLO_PRORROGACAO = "";
 var $DT_PROTOCOLO_PRORROGACAO = "";
 var $DT_VALIDADE_PROTOCOLO_PROR = "";
 var $DT_PRETENDIDA_PRORROGACAO = "";
 var $DT_CANCELAMENTO = "";
 var $NU_PROCESSO_CANCELAMENTO = "";
 var $DT_PROCESSO_CANCELAMENTO = "";
 var $TE_OBSERVACOES_PROCESSOS = "";
 var $DT_PRAZO_ESTADA_SOLICITADO = "";
 var $NU_ARMADOR = ""; # novo
 var $NO_LOCAL_ENTRADA =""; # Novo
 var $NO_UF_ENTRADA =""; # Novo
 var $DT_ENTRADA =""; # Novo
 var $NU_TRANSPORTE_ENTRADA =""; # Novo
 var $CO_CLASSIFICACAO_VISTO =""; # Novo
 var $DS_JUSTIFICATIVA = ""; #novo
 var $TE_DESCRICAO_ATIVIDADES = ""; #novo
 var $DS_BENEFICIOS_SALARIO = "";
 var $NU_EMBARCACAO_INICIAL = "";
 var $CD_EMBARCADO = "";
 var $DT_SITUACAO_SOL = "";

 function objVisaDetail() { }
 
 function getNU_EMPRESA() { return $this->NU_EMPRESA; }
 function getNU_CANDIDATO() { return $this->NU_CANDIDATO; }
 function getNM_CANDIDATO() { return $this->NM_CANDIDATO; }
 function getNU_SOLICITACAO() { return $this->NU_SOLICITACAO; }
 function getNU_EMBARCACAO_PROJETO() { return $this->NU_EMBARCACAO_PROJETO; }
 function getCO_TIPO_AUTORIZACAO() { return $this->CO_TIPO_AUTORIZACAO; }
 function getVA_RENUMERACAO_MENSAL() { return $this->VA_RENUMERACAO_MENSAL; }
 function getNO_MOEDA_REMUNERACAO_MENSAL() { return $this->NO_MOEDA_REMUNERACAO_MENSAL; }
 function getVA_REMUNERACAO_MENSAL_BRASIL() { return $this->VA_REMUNERACAO_MENSAL_BRASIL; }
 function getCO_REPARTICAO_CONSULAR() { return $this->CO_REPARTICAO_CONSULAR; }
 function getCO_FUNCAO_CANDIDATO() { return $this->CO_FUNCAO_CANDIDATO; }
 function getDT_ABERTURA_PROCESSO_MTE() { return $this->DT_ABERTURA_PROCESSO_MTE; }
 function getNU_PROCESSO_MTE() { return $this->NU_PROCESSO_MTE; }
 function getNU_AUTORIZACAO_MTE() { return $this->NU_AUTORIZACAO_MTE; }
 function getDT_AUTORIZACAO_MTE() { return $this->DT_AUTORIZACAO_MTE; }
 function getDT_PRAZO_AUTORIZACAO_MTE() { return $this->DT_PRAZO_AUTORIZACAO_MTE; }
 function getDT_VALIDADE_VISTO() { return $this->DT_VALIDADE_VISTO; }
 function getNU_VISTO() { return $this->NU_VISTO; }
 function getDT_EMISSAO_VISTO() { return $this->DT_EMISSAO_VISTO; }
 function getNO_LOCAL_EMISSAO_VISTO() { return $this->NO_LOCAL_EMISSAO_VISTO; }
 function getCO_NACIONALIDADE_VISTO() { return $this->CO_NACIONALIDADE_VISTO; }
 function getNU_PROTOCOLO_CIE() { return $this->NU_PROTOCOLO_CIE; }
 function getDT_PROTOCOLO_CIE() { return $this->DT_PROTOCOLO_CIE; }
 function getDT_VALIDADE_PROTOCOLO_CIE() { return $this->DT_VALIDADE_PROTOCOLO_CIE; }
 function getDT_PRAZO_ESTADA() { return $this->DT_PRAZO_ESTADA; }
 function getDT_VALIDADE_CIE() { return $this->DT_VALIDADE_CIE; }
 function getDT_EMISSAO_CIE() { return $this->DT_EMISSAO_CIE; }
 function getNU_EMISSAO_CIE() { return $this->NU_EMISSAO_CIE; }
 function getNU_PROTOCOLO_PRORROGACAO() { return $this->NU_PROTOCOLO_PRORROGACAO; }
 function getDT_PROTOCOLO_PRORROGACAO() { return $this->DT_PROTOCOLO_PRORROGACAO; }
 function getDT_VALIDADE_PROTOCOLO_PROR() { return $this->DT_VALIDADE_PROTOCOLO_PROR; }
 function getDT_PRETENDIDA_PRORROGACAO() { return $this->DT_PRETENDIDA_PRORROGACAO; }
 function getDT_CANCELAMENTO() { return $this->DT_CANCELAMENTO; }
 function getNU_PROCESSO_CANCELAMENTO() { return $this->NU_PROCESSO_CANCELAMENTO; }
 function getDT_PROCESSO_CANCELAMENTO() { return $this->DT_PROCESSO_CANCELAMENTO; }
 function getTE_OBSERVACOES_PROCESSOS() { return $this->TE_OBSERVACOES_PROCESSOS; }
 function getDT_PRAZO_ESTADA_SOLICITADO() { return $this->DT_PRAZO_ESTADA_SOLICITADO; }
 function getNU_ARMADOR() { return $this->NU_ARMADOR; }
 function getNO_LOCAL_ENTRADA() { return $this->NO_LOCAL_ENTRADA; }
 function getNO_UF_ENTRADA() { return $this->NO_UF_ENTRADA; }
 function getDT_ENTRADA() { return $this->DT_ENTRADA; }
 function getNU_TRANSPORTE_ENTRADA() { return $this->NU_TRANSPORTE_ENTRADA; }
 function getCO_CLASSIFICACAO_VISTO() { return $this->CO_CLASSIFICACAO_VISTO; }
 function getDS_JUSTIFICATIVA() { return $this->DS_JUSTIFICATIVA; }
 function getTE_DESCRICAO_ATIVIDADES() { return $this->TE_DESCRICAO_ATIVIDADES; }

 function getDS_BENEFICIOS_SALARIO() { return $this->DS_BENEFICIOS_SALARIO; }
 function getNU_EMBARCACAO_INICIAL() { return $this->NU_EMBARCACAO_INICIAL; }
 function getCD_EMBARCADO() { return $this->CD_EMBARCADO; }
 function getDT_SITUACAO_SOL() { return $this->DT_SITUACAO_SOL; }

 function setNU_EMPRESA($aux) { $this->NU_EMPRESA = $aux; }
 function setNU_CANDIDATO($aux) { $this->NU_CANDIDATO = $aux; }
 function setNM_CANDIDATO($aux) { $this->NM_CANDIDATO = $aux; }
 function setNU_SOLICITACAO($aux) { $this->NU_SOLICITACAO = $aux; }
 function setNU_EMBARCACAO_PROJETO($aux) { $this->NU_EMBARCACAO_PROJETO = $aux; }
 function setCO_TIPO_AUTORIZACAO($aux) { $this->CO_TIPO_AUTORIZACAO = $aux; }
 function setVA_RENUMERACAO_MENSAL($aux) { $this->VA_RENUMERACAO_MENSAL = $aux; }
 function setNO_MOEDA_REMUNERACAO_MENSAL($aux) { $this->NO_MOEDA_REMUNERACAO_MENSAL = $aux; }
 function setVA_REMUNERACAO_MENSAL_BRASIL($aux) { $this->VA_REMUNERACAO_MENSAL_BRASIL = $aux; }
 function setCO_REPARTICAO_CONSULAR($aux) { $this->CO_REPARTICAO_CONSULAR = $aux; }
 function setCO_FUNCAO_CANDIDATO($aux) { $this->CO_FUNCAO_CANDIDATO = $aux; }
 function setDT_ABERTURA_PROCESSO_MTE($aux) { $this->DT_ABERTURA_PROCESSO_MTE = $aux; }
 function setNU_PROCESSO_MTE($aux) { $this->NU_PROCESSO_MTE = $aux; }
 function setNU_AUTORIZACAO_MTE($aux) { $this->NU_AUTORIZACAO_MTE = $aux; }
 function setDT_AUTORIZACAO_MTE($aux) { $this->DT_AUTORIZACAO_MTE = $aux; }
 function setDT_PRAZO_AUTORIZACAO_MTE($aux) { $this->DT_PRAZO_AUTORIZACAO_MTE = $aux; }
 function setDT_VALIDADE_VISTO($aux) { $this->DT_VALIDADE_VISTO = $aux; }
 function setNU_VISTO($aux) { $this->NU_VISTO = $aux; }
 function setDT_EMISSAO_VISTO($aux) { $this->DT_EMISSAO_VISTO = $aux; }
 function setNO_LOCAL_EMISSAO_VISTO($aux) { $this->NO_LOCAL_EMISSAO_VISTO = $aux; }
 function setCO_NACIONALIDADE_VISTO($aux) { $this->CO_NACIONALIDADE_VISTO = $aux; }
 function setNU_PROTOCOLO_CIE($aux) { $this->NU_PROTOCOLO_CIE = $aux; }
 function setDT_PROTOCOLO_CIE($aux) { $this->DT_PROTOCOLO_CIE = $aux; }
 function setDT_VALIDADE_PROTOCOLO_CIE($aux) { $this->DT_VALIDADE_PROTOCOLO_CIE = $aux; }
 function setDT_PRAZO_ESTADA($aux) { $this->DT_PRAZO_ESTADA = $aux; }
 function setDT_VALIDADE_CIE($aux) { $this->DT_VALIDADE_CIE = $aux; }
 function setDT_EMISSAO_CIE($aux) { $this->DT_EMISSAO_CIE = $aux; }
 function setNU_EMISSAO_CIE($aux) { $this->NU_EMISSAO_CIE = $aux; }
 function setNU_PROTOCOLO_PRORROGACAO($aux) { $this->NU_PROTOCOLO_PRORROGACAO = $aux; }
 function setDT_PROTOCOLO_PRORROGACAO($aux) { $this->DT_PROTOCOLO_PRORROGACAO = $aux; }
 function setDT_VALIDADE_PROTOCOLO_PROR($aux) { $this->DT_VALIDADE_PROTOCOLO_PROR = $aux; }
 function setDT_PRETENDIDA_PRORROGACAO($aux) { $this->DT_PRETENDIDA_PRORROGACAO = $aux; }
 function setDT_CANCELAMENTO($aux) { $this->DT_CANCELAMENTO = $aux; }
 function setNU_PROCESSO_CANCELAMENTO($aux) { $this->NU_PROCESSO_CANCELAMENTO = $aux; }
 function setDT_PROCESSO_CANCELAMENTO($aux) { $this->DT_PROCESSO_CANCELAMENTO = $aux; }
 function setTE_OBSERVACOES_PROCESSOS($aux) { $this->TE_OBSERVACOES_PROCESSOS = $aux; }
 function setDT_PRAZO_ESTADA_SOLICITADO($aux) { $this->DT_PRAZO_ESTADA_SOLICITADO = $aux; }
 function setNU_ARMADOR($aux) { $this->NU_ARMADOR = $aux; }
 function setNO_LOCAL_ENTRADA($aux) { $this->NO_LOCAL_ENTRADA = $aux; }
 function setNO_UF_ENTRADA($aux) { $this->NO_UF_ENTRADA = $aux; }
 function setDT_ENTRADA($aux) { $this->DT_ENTRADA = $aux; }
 function setNU_TRANSPORTE_ENTRADA($aux) { $this->NU_TRANSPORTE_ENTRADA = $aux; }
 function setCO_CLASSIFICACAO_VISTO($aux) { $this->CO_CLASSIFICACAO_VISTO = $aux; }
 function setDS_JUSTIFICATIVA($aux) { $this->DS_JUSTIFICATIVA = $aux; }
 function setTE_DESCRICAO_ATIVIDADES($aux) { $this->TE_DESCRICAO_ATIVIDADES = $aux; }

 function setDS_BENEFICIOS_SALARIO($aux) { $this->DS_BENEFICIOS_SALARIO = $aux; }
 function setNU_EMBARCACAO_INICIAL($aux) { $this->NU_EMBARCACAO_INICIAL = $aux; }
 function setCD_EMBARCADO($aux) { $this->CD_EMBARCADO = $aux; }
 function setDT_SITUACAO_SOL($aux) { $this->DT_SITUACAO_SOL = $aux; }

 public  function RetornaArray() {
   foreach($this as $key => $value) {
     $chave = strtoupper($key);
     $ret[$chave] = $value;
   }
   return $ret;
 }

}

function lerVisaDetail($empresa,$candidato,$nuSolicitacao) {
   $ret = new objVisaDetail();
   $sql = "select * from AUTORIZACAO_CANDIDATO where NU_EMPRESA=$empresa ";
   if( (strlen($candidato)>0) && ($candidato>0) ) {
      $sql = $sql." and NU_CANDIDATO=$candidato ";
   }
   if( (strlen($nuSolicitacao)>0) && ($nuSolicitacao>0) ) {
      $sql = $sql." and NU_SOLICITACAO=$nuSolicitacao ";
   }
   $rs = mysql_query($sql);
   $rows = mysql_num_rows($rs);
   while($rw = mysql_fetch_array($rs)) {
      $ret->setNU_EMPRESA($rw['NU_EMPRESA']);
      $ret->setNU_CANDIDATO($rw['NU_CANDIDATO']);
      $ret->setNU_SOLICITACAO($rw['NU_SOLICITACAO']);
      $ret->setNU_EMBARCACAO_PROJETO($rw['NU_EMBARCACAO_PROJETO']);
      $ret->setCO_TIPO_AUTORIZACAO($rw['CO_TIPO_AUTORIZACAO']);
      $ret->setVA_RENUMERACAO_MENSAL($rw['VA_RENUMERACAO_MENSAL']);
      $ret->setNO_MOEDA_REMUNERACAO_MENSAL($rw['NO_MOEDA_REMUNERACAO_MENSAL']);
      $ret->setVA_REMUNERACAO_MENSAL_BRASIL($rw['VA_REMUNERACAO_MENSAL_BRASIL']);
      $ret->setCO_REPARTICAO_CONSULAR($rw['CO_REPARTICAO_CONSULAR']);
      $ret->setCO_FUNCAO_CANDIDATO($rw['CO_FUNCAO_CANDIDATO']);
      $ret->setTE_DESCRICAO_ATIVIDADES($rw['TE_DESCRICAO_ATIVIDADES']);
      $ret->setDT_ABERTURA_PROCESSO_MTE( frmtDataBR( $rw['DT_ABERTURA_PROCESSO_MTE'] ) );
      $ret->setNU_PROCESSO_MTE($rw['NU_PROCESSO_MTE']);
      $ret->setNU_AUTORIZACAO_MTE($rw['NU_AUTORIZACAO_MTE']);
      $ret->setDT_AUTORIZACAO_MTE( frmtDataBR( $rw['DT_AUTORIZACAO_MTE'] ) );
      $ret->setDT_PRAZO_AUTORIZACAO_MTE( $rw['DT_PRAZO_AUTORIZACAO_MTE'] );
      $ret->setDT_VALIDADE_VISTO( frmtDataBR( $rw['DT_VALIDADE_VISTO'] ) );
      $ret->setNU_VISTO($rw['NU_VISTO']);
      $ret->setDT_EMISSAO_VISTO( frmtDataBR( $rw['DT_EMISSAO_VISTO'] ) );
      $ret->setNO_LOCAL_EMISSAO_VISTO($rw['NO_LOCAL_EMISSAO_VISTO']);
      $ret->setCO_NACIONALIDADE_VISTO($rw['CO_NACIONALIDADE_VISTO']);
      $ret->setNU_PROTOCOLO_CIE($rw['NU_PROTOCOLO_CIE']);
      $ret->setDT_PROTOCOLO_CIE( frmtDataBR( $rw['DT_PROTOCOLO_CIE'] ) );
      $ret->setDT_VALIDADE_PROTOCOLO_CIE( frmtDataBR( $rw['DT_VALIDADE_PROTOCOLO_CIE'] ) );
      $ret->setDT_PRAZO_ESTADA( frmtDataBR( $rw['DT_PRAZO_ESTADA'] ) );
      $ret->setDT_VALIDADE_CIE( frmtDataBR( $rw['DT_VALIDADE_CIE'] ) );
      $ret->setDT_EMISSAO_CIE( frmtDataBR( $rw['DT_EMISSAO_CIE'] ) );
      $ret->setNU_EMISSAO_CIE( $rw['NU_EMISSAO_CIE'] );
      $ret->setNU_PROTOCOLO_PRORROGACAO($rw['NU_PROTOCOLO_PRORROGACAO']);
      $ret->setDT_PROTOCOLO_PRORROGACAO( frmtDataBR( $rw['DT_PROTOCOLO_PRORROGACAO'] ) );
      $ret->setDT_VALIDADE_PROTOCOLO_PROR( frmtDataBR( $rw['DT_VALIDADE_PROTOCOLO_PROR'] ) );
      $ret->setDT_PRETENDIDA_PRORROGACAO( frmtDataBR( $rw['DT_PRETENDIDA_PRORROGACAO'] ) );
      $ret->setDT_CANCELAMENTO( frmtDataBR( $rw['DT_CANCELAMENTO'] ) );
      $ret->setNU_PROCESSO_CANCELAMENTO($rw['NU_PROCESSO_CANCELAMENTO']);
      $ret->setDT_PROCESSO_CANCELAMENTO( frmtDataBR( $rw['DT_PROCESSO_CANCELAMENTO'] ) );
      $ret->setTE_OBSERVACOES_PROCESSOS($rw['TE_OBSERVACOES_PROCESSOS']);
      $ret->setDT_PRAZO_ESTADA_SOLICITADO( $rw['DT_PRAZO_ESTADA_SOLICITADO'] );
      $ret->setNU_ARMADOR($rw['NU_ARMADOR']);
      $ret->setNO_LOCAL_ENTRADA($rw['NO_LOCAL_ENTRADA']);
      $ret->setNO_UF_ENTRADA($rw['NO_UF_ENTRADA']);
      $ret->setDT_ENTRADA( frmtDataBR( $rw['DT_ENTRADA'] ) );
      $ret->setNU_TRANSPORTE_ENTRADA($rw['NU_TRANSPORTE_ENTRADA']);
      $ret->setCO_CLASSIFICACAO_VISTO($rw['CO_CLASSIFICACAO_VISTO']);
      $ret->setDS_JUSTIFICATIVA($rw['DS_JUSTIFICATIVA']);
      $ret->setTE_DESCRICAO_ATIVIDADES($rw['TE_DESCRICAO_ATIVIDADES']);
      $ret->setNM_CANDIDATO(pegaNomeCandidato($rw['NU_CANDIDATO']));
      $ret->setDS_BENEFICIOS_SALARIO($rw['DS_BENEFICIOS_SALARIO']);
      $ret->setNU_EMBARCACAO_INICIAL($rw['NU_EMBARCACAO_INICIAL']);
      $ret->setCD_EMBARCADO($rw['CD_EMBARCADO']);
#      $ret->setDT_SITUACAO_SOL(frmtDataBR($rw['DT_SITUACAO_SOL']));
   }
   return $ret;   
}

function insereVisaDetail($autorizacao) {
   $ret = "";
   $sql1 = "NU_EMPRESA"; $sql2 = $autorizacao->getNU_EMPRESA();
   $sql1 = $sql1.",NU_CANDIDATO"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_CANDIDATO(),"N");
   $sql1 = $sql1.",NU_SOLICITACAO"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_SOLICITACAO(),"N");
#   $sql1 = $sql1.",NU_EMBARCACAO_PROJETO"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_EMBARCACAO_PROJETO(),"N");
#   $sql1 = $sql1.",CO_TIPO_AUTORIZACAO"; $sql2 = $sql2.frmtValorIns($autorizacao->getCO_TIPO_AUTORIZACAO(),"S");
#   $sql1 = $sql1.",VA_RENUMERACAO_MENSAL"; $sql2 = $sql2.frmtValorIns($autorizacao->getVA_RENUMERACAO_MENSAL(),"S");
#   $sql1 = $sql1.",NO_MOEDA_REMUNERACAO_MENSAL"; $sql2 = $sql2.frmtValorIns($autorizacao->getNO_MOEDA_REMUNERACAO_MENSAL(),"S");
#   $sql1 = $sql1.",VA_REMUNERACAO_MENSAL_BRASIL"; $sql2 = $sql2.frmtValorIns($autorizacao->getVA_REMUNERACAO_MENSAL_BRASIL(),"S");
   $sql1 = $sql1.",CO_REPARTICAO_CONSULAR"; $sql2 = $sql2.frmtValorIns($autorizacao->getCO_REPARTICAO_CONSULAR(),"N");
   $sql1 = $sql1.",CO_FUNCAO_CANDIDATO"; $sql2 = $sql2.frmtValorIns($autorizacao->getCO_FUNCAO_CANDIDATO(),"N");
   $sql1 = $sql1.",DT_ABERTURA_PROCESSO_MTE"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_ABERTURA_PROCESSO_MTE(),"D");
   $sql1 = $sql1.",NU_PROCESSO_MTE"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_PROCESSO_MTE(),"S");
   $sql1 = $sql1.",NU_AUTORIZACAO_MTE"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_AUTORIZACAO_MTE(),"S");
   $sql1 = $sql1.",DT_AUTORIZACAO_MTE"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_AUTORIZACAO_MTE(),"D");
   $sql1 = $sql1.",DT_PRAZO_AUTORIZACAO_MTE"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_PRAZO_AUTORIZACAO_MTE(),"S");
   $sql1 = $sql1.",DT_VALIDADE_VISTO"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_VALIDADE_VISTO(),"D");
   $sql1 = $sql1.",NU_VISTO"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_VISTO(),"S");
   $sql1 = $sql1.",DT_EMISSAO_VISTO"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_EMISSAO_VISTO(),"D");
   $sql1 = $sql1.",NO_LOCAL_EMISSAO_VISTO"; $sql2 = $sql2.frmtValorIns($autorizacao->getNO_LOCAL_EMISSAO_VISTO(),"S");
   $sql1 = $sql1.",CO_NACIONALIDADE_VISTO"; $sql2 = $sql2.frmtValorIns($autorizacao->getCO_NACIONALIDADE_VISTO(),"N");
   $sql1 = $sql1.",NU_PROTOCOLO_CIE"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_PROTOCOLO_CIE(),"S");
   $sql1 = $sql1.",DT_PROTOCOLO_CIE"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_PROTOCOLO_CIE(),"D");
   $sql1 = $sql1.",DT_VALIDADE_PROTOCOLO_CIE"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_VALIDADE_PROTOCOLO_CIE(),"D");
   $sql1 = $sql1.",DT_PRAZO_ESTADA"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_PRAZO_ESTADA(),"D");
   $sql1 = $sql1.",DT_VALIDADE_CIE"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_VALIDADE_CIE(),"D");
   $sql1 = $sql1.",DT_EMISSAO_CIE"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_EMISSAO_CIE(),"D");
   $sql1 = $sql1.",NU_EMISSAO_CIE"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_EMISSAO_CIE(),"S");
   $sql1 = $sql1.",NU_PROTOCOLO_PRORROGACAO"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_PROTOCOLO_PRORROGACAO(),"S");
   $sql1 = $sql1.",DT_PROTOCOLO_PRORROGACAO"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_PROTOCOLO_PRORROGACAO(),"D");
   $sql1 = $sql1.",DT_VALIDADE_PROTOCOLO_PROR"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_VALIDADE_PROTOCOLO_PROR(),"D");
   $sql1 = $sql1.",DT_PRETENDIDA_PRORROGACAO"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_PRETENDIDA_PRORROGACAO(),"D");
   $sql1 = $sql1.",DT_CANCELAMENTO"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_CANCELAMENTO(),"D");
   $sql1 = $sql1.",NU_PROCESSO_CANCELAMENTO"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_PROCESSO_CANCELAMENTO(),"S");
   $sql1 = $sql1.",DT_PROCESSO_CANCELAMENTO"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_PROCESSO_CANCELAMENTO(),"D");
   $sql1 = $sql1.",TE_OBSERVACOES_PROCESSOS"; $sql2 = $sql2.frmtValorIns($autorizacao->getTE_OBSERVACOES_PROCESSOS(),"S");
   $sql1 = $sql1.",DT_PRAZO_ESTADA_SOLICITADO"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_PRAZO_ESTADA_SOLICITADO(),"S");
#   $sql1 = $sql1.",NU_ARMADOR"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_ARMADOR(),"S");
   $sql1 = $sql1.",NO_LOCAL_ENTRADA"; $sql2 = $sql2.frmtValorIns($autorizacao->getNO_LOCAL_ENTRADA(),"S");
   $sql1 = $sql1.",NO_UF_ENTRADA"; $sql2 = $sql2.frmtValorIns($autorizacao->getNO_UF_ENTRADA(),"S");
   $sql1 = $sql1.",DT_ENTRADA"; $sql2 = $sql2.frmtValorIns($autorizacao->getDT_ENTRADA(),"D");
   $sql1 = $sql1.",NU_TRANSPORTE_ENTRADA"; $sql2 = $sql2.frmtValorIns($autorizacao->getNU_TRANSPORTE_ENTRADA(),"S");
   $sql1 = $sql1.",CO_CLASSIFICACAO_VISTO"; $sql2 = $sql2.frmtValorIns($autorizacao->getCO_CLASSIFICACAO_VISTO(),"S");
#   $sql1 = $sql1.",DS_JUSTIFICATIVA"; $sql2 = $sql2.frmtValorIns($autorizacao->getDS_JUSTIFICATIVA(),"S");
#   $sql1 = $sql1.",TE_DESCRICAO_ATIVIDADES"; $sql2 = $sql2.frmtValorIns($autorizacao->getTE_DESCRICAO_ATIVIDADES(),"S");
   $sql = "insert into AUTORIZACAO_CANDIDATO ($sql1) VALUES ($sql2)";
   mysql_query($sql);
   gravaLog($sql,mysql_error(),"INCLUSAO","VISA_DETAIL");
   if(mysql_errno()>0) {
     $ret = mysql_error()."SQL=$sql";
   }
   return $ret;
}

function alteraVisaDetailObs($autorizacao) {
   $ret = "";
   $empresa = frmtValorAlt($autorizacao->getNU_EMPRESA(),"N");
   $candidato = frmtValorAlt($autorizacao->getNU_CANDIDATO(),"N");
   $solicitacao = frmtValorAlt($autorizacao->getNU_SOLICITACAO(),"N");
   $sqla = $sqla."TE_OBSERVACOES_PROCESSOS=".frmtValorAlt($autorizacao->getTE_OBSERVACOES_PROCESSOS(),"S");
   $sql = "update AUTORIZACAO_CANDIDATO set $sqla where NU_EMPRESA=$empresa and NU_CANDIDATO=$candidato and NU_SOLICITACAO=$solicitacao";
   mysql_query($sql);
#print "SQL=$sql";
   gravaLog($sql,mysql_error(),"ALTERACAO","VISA_DETAIL");
   if(mysql_errno()>0) {
     $ret = mysql_error()." SQL=$sql";
#print "ERRO: ".mysql_error();
   }
}

function alteraVisaDetail($autorizacao) {
   $ret = "";
   $empresa = frmtValorAlt($autorizacao->getNU_EMPRESA(),"N");
   $candidato = frmtValorAlt($autorizacao->getNU_CANDIDATO(),"N");
   $solicitacao = frmtValorAlt($autorizacao->getNU_SOLICITACAO(),"N");
#   $sqla = $sqla.",NU_EMBARCACAO_PROJETO=".frmtValorAlt($autorizacao->getNU_EMBARCACAO_PROJETO(),"N");
#   $sqla = $sqla.",CO_TIPO_AUTORIZACAO=".frmtValorAlt($autorizacao->getCO_TIPO_AUTORIZACAO(),"S");
#   $sqla = $sqla.",VA_RENUMERACAO_MENSAL=".frmtValorAlt($autorizacao->getVA_RENUMERACAO_MENSAL(),"S");
#   $sqla = $sqla.",NO_MOEDA_REMUNERACAO_MENSAL=".frmtValorAlt($autorizacao->getNO_MOEDA_REMUNERACAO_MENSAL(),"S");
#   $sqla = $sqla.",VA_REMUNERACAO_MENSAL_BRASIL=".frmtValorAlt($autorizacao->getVA_REMUNERACAO_MENSAL_BRASIL(),"S");
   $sqla = $sqla." CO_REPARTICAO_CONSULAR=".frmtValorAlt($autorizacao->getCO_REPARTICAO_CONSULAR(),"N");
   $sqla = $sqla.",CO_FUNCAO_CANDIDATO=".frmtValorAlt($autorizacao->getCO_FUNCAO_CANDIDATO(),"N");
   $sqla = $sqla.",DT_ABERTURA_PROCESSO_MTE=".frmtValorAlt($autorizacao->getDT_ABERTURA_PROCESSO_MTE(),"D");
   $sqla = $sqla.",NU_PROCESSO_MTE=".frmtValorAlt($autorizacao->getNU_PROCESSO_MTE(),"S");
   $sqla = $sqla.",NU_AUTORIZACAO_MTE=".frmtValorAlt($autorizacao->getNU_AUTORIZACAO_MTE(),"S");
   $sqla = $sqla.",DT_AUTORIZACAO_MTE=".frmtValorAlt($autorizacao->getDT_AUTORIZACAO_MTE(),"D");
   $sqla = $sqla.",DT_PRAZO_AUTORIZACAO_MTE=".frmtValorAlt($autorizacao->getDT_PRAZO_AUTORIZACAO_MTE(),"S");
   $sqla = $sqla.",DT_VALIDADE_VISTO=".frmtValorAlt($autorizacao->getDT_VALIDADE_VISTO(),"D");
   $sqla = $sqla.",NU_VISTO=".frmtValorAlt($autorizacao->getNU_VISTO(),"S");
   $sqla = $sqla.",DT_EMISSAO_VISTO=".frmtValorAlt($autorizacao->getDT_EMISSAO_VISTO(),"D");
   $sqla = $sqla.",NO_LOCAL_EMISSAO_VISTO=".frmtValorAlt($autorizacao->getNO_LOCAL_EMISSAO_VISTO(),"S");
   $sqla = $sqla.",CO_NACIONALIDADE_VISTO=".frmtValorAlt($autorizacao->getCO_NACIONALIDADE_VISTO(),"N");
   $sqla = $sqla.",NU_PROTOCOLO_CIE=".frmtValorAlt($autorizacao->getNU_PROTOCOLO_CIE(),"S");
   $sqla = $sqla.",DT_PROTOCOLO_CIE=".frmtValorAlt($autorizacao->getDT_PROTOCOLO_CIE(),"D");
   $sqla = $sqla.",DT_VALIDADE_PROTOCOLO_CIE=".frmtValorAlt($autorizacao->getDT_VALIDADE_PROTOCOLO_CIE(),"D");
   $sqla = $sqla.",DT_PRAZO_ESTADA=".frmtValorAlt($autorizacao->getDT_PRAZO_ESTADA(),"D");
   $sqla = $sqla.",DT_VALIDADE_CIE=".frmtValorAlt($autorizacao->getDT_VALIDADE_CIE(),"D");
   $sqla = $sqla.",DT_EMISSAO_CIE=".frmtValorAlt($autorizacao->getDT_EMISSAO_CIE(),"D");
   $sqla = $sqla.",NU_EMISSAO_CIE=".frmtValorAlt($autorizacao->getNU_EMISSAO_CIE(),"S");
   $sqla = $sqla.",NU_PROTOCOLO_PRORROGACAO=".frmtValorAlt($autorizacao->getNU_PROTOCOLO_PRORROGACAO(),"S");
   $sqla = $sqla.",DT_PROTOCOLO_PRORROGACAO=".frmtValorAlt($autorizacao->getDT_PROTOCOLO_PRORROGACAO(),"D");
   $sqla = $sqla.",DT_VALIDADE_PROTOCOLO_PROR=".frmtValorAlt($autorizacao->getDT_VALIDADE_PROTOCOLO_PROR(),"D");
   $sqla = $sqla.",DT_PRETENDIDA_PRORROGACAO=".frmtValorAlt($autorizacao->getDT_PRETENDIDA_PRORROGACAO(),"D");
   $sqla = $sqla.",DT_CANCELAMENTO=".frmtValorAlt($autorizacao->getDT_CANCELAMENTO(),"D");
   $sqla = $sqla.",NU_PROCESSO_CANCELAMENTO=".frmtValorAlt($autorizacao->getNU_PROCESSO_CANCELAMENTO(),"S");
   $sqla = $sqla.",DT_PROCESSO_CANCELAMENTO=".frmtValorAlt($autorizacao->getDT_PROCESSO_CANCELAMENTO(),"D");
   $sqla = $sqla.",TE_OBSERVACOES_PROCESSOS=".frmtValorAlt($autorizacao->getTE_OBSERVACOES_PROCESSOS(),"S");
   $sqla = $sqla.",DT_PRAZO_ESTADA_SOLICITADO=".frmtValorAlt($autorizacao->getDT_PRAZO_ESTADA_SOLICITADO(),"S");
#   $sqla = $sqla.",NU_ARMADOR=".frmtValorAlt($autorizacao->getNU_ARMADOR(),"S");
   $sqla = $sqla.",NO_LOCAL_ENTRADA=".frmtValorAlt($autorizacao->getNO_LOCAL_ENTRADA(),"S");
   $sqla = $sqla.",NO_UF_ENTRADA=".frmtValorAlt($autorizacao->getNO_UF_ENTRADA(),"S");
   $sqla = $sqla.",DT_ENTRADA=".frmtValorAlt($autorizacao->getDT_ENTRADA(),"D");
   $sqla = $sqla.",NU_TRANSPORTE_ENTRADA=".frmtValorAlt($autorizacao->getNU_TRANSPORTE_ENTRADA(),"S");
   $sqla = $sqla.",CO_CLASSIFICACAO_VISTO=".frmtValorAlt($autorizacao->getCO_CLASSIFICACAO_VISTO(),"S");
#   $sqla = $sqla.",DS_JUSTIFICATIVA=".frmtValorAlt($autorizacao->getDS_JUSTIFICATIVA(),"S");
#   $sqla = $sqla.",TE_DESCRICAO_ATIVIDADES=".frmtValorAlt($autorizacao->getTE_DESCRICAO_ATIVIDADES(),"S");
   $sql = "update AUTORIZACAO_CANDIDATO set $sqla where NU_EMPRESA=$empresa and NU_CANDIDATO=$candidato and NU_SOLICITACAO=$solicitacao";
   mysql_query($sql);
#print "SQL=$sql";
   gravaLog($sql,mysql_error(),"ALTERACAO","VISA_DETAIL");
   if(mysql_errno()>0) {
     $ret = mysql_error()." SQL=$sql";
#print "ERRO: ".mysql_error();
   }
   return $ret;
}

function removeVisaDetail($empresa,$candidato,$solicitacao) {
   $ret = "";
   $sql = "delete from AUTORIZACAO_CANDIDATO where NU_EMPRESA=$empresa and NU_CANDIDATO=$candidato and NU_SOLICITACAO=$solicitacao";
   mysql_query($sql);
   if(mysql_errno()>0) {
     $ret = mysql_error();
   }
   return $ret;
}

function postVisaDetail() {
   $ret = new objVisaDetail();
   $ret->setNU_EMPRESA($_POST['NU_EMPRESA']);
   $ret->setNU_CANDIDATO($_POST['NU_CANDIDATO']);
   $ret->setNU_SOLICITACAO($_POST['NU_SOLICITACAO']);
   $ret->setNU_EMBARCACAO_PROJETO($_POST['NU_EMBARCACAO_PROJETO']);
   $ret->setCO_TIPO_AUTORIZACAO($_POST['CO_TIPO_AUTORIZACAO']);
   $ret->setVA_RENUMERACAO_MENSAL($_POST['VA_RENUMERACAO_MENSAL']);
   $ret->setNO_MOEDA_REMUNERACAO_MENSAL($_POST['NO_MOEDA_REMUNERACAO_MENSAL']);
   $ret->setVA_REMUNERACAO_MENSAL_BRASIL($_POST['VA_REMUNERACAO_MENSAL_BRASIL']);
   $ret->setCO_REPARTICAO_CONSULAR($_POST['CO_REPARTICAO_CONSULAR']);
   $ret->setCO_FUNCAO_CANDIDATO($_POST['CO_FUNCAO_CANDIDATO']);
   $ret->setTE_DESCRICAO_ATIVIDADES($_POST['TE_DESCRICAO_ATIVIDADES']);
   $ret->setDT_ABERTURA_PROCESSO_MTE($_POST['DT_ABERTURA_PROCESSO_MTE']);
   $ret->setNU_PROCESSO_MTE($_POST['NU_PROCESSO_MTE']);
   $ret->setNU_AUTORIZACAO_MTE($_POST['NU_AUTORIZACAO_MTE']);
   $ret->setDT_AUTORIZACAO_MTE($_POST['DT_AUTORIZACAO_MTE']);
   $ret->setDT_PRAZO_AUTORIZACAO_MTE($_POST['DT_PRAZO_AUTORIZACAO_MTE']);
   $ret->setDT_VALIDADE_VISTO($_POST['DT_VALIDADE_VISTO']);
   $ret->setNU_VISTO($_POST['NU_VISTO']);
   $ret->setDT_EMISSAO_VISTO($_POST['DT_EMISSAO_VISTO']);
   $ret->setNO_LOCAL_EMISSAO_VISTO($_POST['NO_LOCAL_EMISSAO_VISTO']);
   $ret->setCO_NACIONALIDADE_VISTO($_POST['CO_NACIONALIDADE_VISTO']);
   $ret->setNU_PROTOCOLO_CIE($_POST['NU_PROTOCOLO_CIE']);
   $ret->setDT_PROTOCOLO_CIE($_POST['DT_PROTOCOLO_CIE']);
   $ret->setDT_VALIDADE_PROTOCOLO_CIE($_POST['DT_VALIDADE_PROTOCOLO_CIE']);
   $ret->setDT_PRAZO_ESTADA($_POST['DT_PRAZO_ESTADA']);
   $ret->setDT_VALIDADE_CIE($_POST['DT_VALIDADE_CIE']);
   $ret->setDT_EMISSAO_CIE($_POST['DT_EMISSAO_CIE']);
   $ret->setNU_EMISSAO_CIE($_POST['NU_EMISSAO_CIE']);
   $ret->setNU_PROTOCOLO_PRORROGACAO($_POST['NU_PROTOCOLO_PRORROGACAO']);
   $ret->setDT_PROTOCOLO_PRORROGACAO($_POST['DT_PROTOCOLO_PRORROGACAO']);
   $ret->setDT_VALIDADE_PROTOCOLO_PROR($_POST['DT_VALIDADE_PROTOCOLO_PROR']);
   $ret->setDT_PRETENDIDA_PRORROGACAO($_POST['DT_PRETENDIDA_PRORROGACAO']);
   $ret->setDT_CANCELAMENTO($_POST['DT_CANCELAMENTO']);
   $ret->setNU_PROCESSO_CANCELAMENTO($_POST['NU_PROCESSO_CANCELAMENTO']);
   $ret->setDT_PROCESSO_CANCELAMENTO($_POST['DT_PROCESSO_CANCELAMENTO']);
   $ret->setTE_OBSERVACOES_PROCESSOS($_POST['TE_OBSERVACOES_PROCESSOS']);
   $ret->setDT_PRAZO_ESTADA_SOLICITADO($_POST['DT_PRAZO_ESTADA_SOLICITADO']);
   $ret->setNU_ARMADOR($_POST['NU_ARMADOR']);
   $ret->setNO_LOCAL_ENTRADA($_POST['NO_LOCAL_ENTRADA']);
   $ret->setNO_UF_ENTRADA($_POST['NO_UF_ENTRADA']);
   $ret->setDT_ENTRADA($_POST['DT_ENTRADA']);
   $ret->setNU_TRANSPORTE_ENTRADA($_POST['NU_TRANSPORTE_ENTRADA']);
   $ret->setCO_CLASSIFICACAO_VISTO($_POST['CO_CLASSIFICACAO_VISTO']);
   $ret->setDS_JUSTIFICATIVA($_POST['DS_JUSTIFICATIVA']);
   $ret->setTE_DESCRICAO_ATIVIDADES($_POST['TE_DESCRICAO_ATIVIDADES']);
# Verificar quais datas alteram o prazo de estada
   return $ret;
}
