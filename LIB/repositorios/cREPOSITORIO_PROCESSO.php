<?php

class cREPOSITORIO_PROCESSO {

    public function migre($nomeTabela, $nu_candidato_destino ) {
        $nome_completo = str_replace("'", "%", str_replace(" ", "%", str_replace('.', '%', trim($nome_completo))));
        $query = "select count(*) FROM CANDIDATO C";
        $query .= " WHERE NOME_COMPLETO like '%" . $nome_completo . "%'";
        if (intval($nu_empresa) > 0) {
            $query .= " AND C.NU_EMPRESA = " . $nu_empresa;
        }
        $res = cAMBIENTE::$db_pdo->query($query);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $qtd = $rs[0];

        $query = " SELECT C.nu_candidato"
                . " ,C.nome_completo"
                . " ,C.no_pai"
                . " ,C.no_mae"
                . " ,date_format(C.DT_NASCIMENTO, '%d/%m/%Y') dt_nascimento_fmt"
                . " ,C.nu_passaporte"
                . " ,date_format(C.DT_CADASTRAMENTO, '%d/%m/%Y') as dt_cadastramento_fmt"
                . " ,date_format(C.DT_ULT_ALTERACAO, '%d/%m/%Y') as dt_ult_alteracao_fmt"
                . " ,e.no_razao_social"
                . " FROM CANDIDATO C";
        $query .= "  LEFT JOIN EMPRESA e ON e.NU_EMPRESA = C.NU_EMPRESA";
        $query .= " WHERE NOME_COMPLETO like '%" . $nome_completo . "%'";
        if (intval($nu_empresa) > 0) {
            $query .= " AND C.NU_EMPRESA = " . $nu_empresa;
        }
        $query .= " ORDER BY nome_completo"
                . " limit 20";
        $res = cAMBIENTE::$db_pdo->query($query);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        $ret = array('total' => $qtd, 'registros' => $rs);
        return $ret;
    }

    public function osNaoConcluida($nu_empresa, $nu_servico, $nu_candidato) {
// Verifica se existe OS desse serviço aberta para esse candidato.
        $sql = "select sv.id_solicita_visto, sv.nu_solicitacao, c.codigo_processo_mte_atual"
                . " from candidato c"
                . " join autorizacao_candidato ac on ac.nu_candidato = c.nu_candidato"
                . " join solicita_visto sv on sv.id_solicita_visto = ac.id_solicita_visto"
                . " join status_solicitacao ss on ss.id_status_sol = sv.id_status_sol"
                . " where sv.nu_empresa = " . $nu_empresa
                . "     and sv.nu_servico = " . $nu_servico
                . "	and coalesce(ss.FL_ENCERRADA, 0) = 0 "
                . "     and c.nu_candidato = " . $nu_candidato
                . " order by sv.id_solicita_visto desc limit 1";

        $res = cAMBIENTE::$db_pdo->query($sql);
        if ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $ret = array('id_solicita_visto' => $rs['id_solicita_visto'], 'nu_solicitacao' => $rs['nu_solicitacao']);
        } else {
            $ret = null;
        }
        return $ret;
    }

    /**
     * Recupera as OS de pacote que não estão sendo utilizadas
     */
    public function osDePacotePendentes($nu_empresa, $nu_servico, $nu_candidato) {
        $sql = "select sv.id_solicita_visto, sv.nu_solicitacao, sv.id_solicita_visto_pacote, svp.nu_solicitacao nu_solicitacao_pacote"
                . " from candidato c "
                . " join autorizacao_candidato ac on ac.nu_candidato = c.nu_candidato"
                . " join solicita_visto sv on sv.id_solicita_visto = ac.id_solicita_visto"
                . " join solicita_visto svp on svp.id_solicita_visto = sv.id_solicita_visto_pacote"
                . " where sv.nu_empresa = " . $nu_empresa
                . " and sv.nu_servico = " . $nu_servico
                . " and sv.soli_fl_disponivel = 1"
                . " and c.nu_candidato = " . $nu_candidato
                . " order by sv.id_solicita_visto desc limit 1";
        $res = cAMBIENTE::$db_pdo->query($sql);
        if ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $ret = array(
                'id_solicita_visto' => $rs['id_solicita_visto']
                , 'nu_solicitacao' => $rs['nu_solicitacao']
                , 'id_solicita_visto_pacote' => $rs['id_solicita_visto_pacote']
                , 'nu_solicitacao_pacote' => $rs['nu_solicitacao_pacote']
            );
        } else {
            $ret =  null;
        }
        return $ret;
    }

    public function usarOsPendente($nu_candidato, $nu_servico, $nu_empresa) {
// Remove os candidatos anteriores dessa OS
// 
    }

    public function LoginValidoCrypt($login, $senha) {
        $cand = new cCANDIDATO();
        if ($cand->recuperesePeloLogin($login)) {
                if ($senha == $cand->mNO_SENHA) {
                    cSESSAO::CriaSessaoCandidato($cand);
                    return true;
                } else {
//                    $this->incrementaAcessoNegado();
  //                  $restantes = 5 - $this->usua_qtd_acesso_negado;
    //                $msg = "Login ou senha inválidos (" . $restantes . " tentativas restantes)<br/>Username or password invalid (" . $restantes . " tries left)";
                    $msg = "Login ou senha inválidos<br/>Username or password invalid";
                    return false;
                }
        } else {
            $msg = "Login ou senha inválidos <br/>Username or password invalid";
            return false;
        }
    }

    public function migreProcesso($nu_candidato_destino){
        
    }
}
