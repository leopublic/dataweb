<?php
class cREPOSITORIO_DR extends cREPOSITORIO
{

    public function pendentes($filtros = '') {
        $sql.= "select c.nu_candidato_tmp" . ", c.nu_candidato" . ", c.NOME_COMPLETO" . ", c.id_solicita_visto" . ", c.id_status_edicao" . ", c.conv_id" . ", c.dt_envio_edicao" . ", date_format(c.dt_cadastramento, '%d/%m/%Y %H:%i:%s') dt_cadastramento_fmt" . ", date_format(c.dt_envio_edicao, '%d/%m/%Y %H:%i:%s') dt_envio_edicao_fmt" . ", date_format(c.dt_ult_alteracao, '%d/%m/%Y %H:%i:%s') dt_ult_alteracao_fmt" . ", coalesce(ep.no_embarcacao_projeto, c.no_embarcacao_projeto) no_embarcacao_projeto" . ", no_razao_social" . ", u1.nome nome1, u2.nome nome2, u3.nome nome3, u4.nome nome4, u5.nome nome5" . ", coalesce(conv.nome, ualt.nome) nome_alterador" . ", conv.email" . ", s.no_servico_resumido" . ", cc.nome_completo nome_completo_definitivo" . ", sv.nu_solicitacao" . " from candidato_tmp c" . " join empresa e on e.nu_empresa = c.nu_empresa" . " left join convidado conv on conv.conv_id = c.conv_id" . " left join embarcacao_projeto ep on ep.nu_empresa = c.nu_empresa and ep.nu_embarcacao_projeto = c.nu_embarcacao_projeto" . " left join servico s on s.nu_servico = c.nu_servico" . " left join usuarios u1 on u1.cd_usuario = e.usua_id_responsavel" . " left join usuarios u2 on u2.cd_usuario = e.usua_id_responsavel2" . " left join usuarios u3 on u3.cd_usuario = e.usua_id_responsavel3" . " left join usuarios u4 on u4.cd_usuario = e.usua_id_responsavel4" . " left join usuarios u5 on u5.cd_usuario = e.usua_id_responsavel5" . " left join usuarios ualt on ualt.cd_usuario = c.co_usu_ult_alteracao" . " left join candidato cc on cc.nu_candidato = c.nu_candidato" . " left join solicita_visto sv on sv.id_solicita_visto = c.id_solicita_visto" . " where 1=1" . $filtros . " order by conv_id, dt_envio_edicao desc, c.nome_completo";
        error_log($sql);
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll();
        return $rs;
    }

    public function qtdPendentes($where) {
        $sql.= "select count(*) qtd" . " from candidato_tmp c" . " join empresa e on e.nu_empresa = c.nu_empresa" . " left join convidado conv on conv.conv_id = c.conv_id" . " left join embarcacao_projeto ep on ep.nu_empresa = c.nu_empresa and ep.nu_embarcacao_projeto = c.nu_embarcacao_projeto" . " left join servico s on s.nu_servico = c.nu_servico" . " left join usuarios u1 on u1.cd_usuario = e.usua_id_responsavel" . " left join usuarios u2 on u2.cd_usuario = e.usua_id_responsavel2" . " left join usuarios u3 on u3.cd_usuario = e.usua_id_responsavel3" . " left join usuarios u4 on u4.cd_usuario = e.usua_id_responsavel4" . " left join usuarios u5 on u5.cd_usuario = e.usua_id_responsavel5" . " left join usuarios ualt on ualt.cd_usuario = c.co_usu_ult_alteracao" . " left join candidato cc on cc.nu_candidato = c.nu_candidato" . " left join solicita_visto sv on sv.id_solicita_visto = c.id_solicita_visto" . " where 1=1" . $filtros;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        return $rs['qtd'];
    }

    public function pendentesDoUsuario($cd_usuario) {
        $sql.= "select c.nu_candidato_tmp, c.NU_CANDIDATO, c.NOME_COMPLETO
                    , c.NO_MAE
                    , no_razao_social
                    , coalesce(ep.no_embarcacao_projeto, c.no_embarcacao_projeto) no_embarcacao_projeto
                    , c.dt_envio_edicao
                    , date_format(c.dt_envio_edicao, '%d/%m/%Y %H:%i:%s') dt_envio_edicao_fmt" . ", u1.nome nome1
                    , u2.nome nome2
                    , u3.nome nome3
                    , u4.nome nome4
                    , u5.nome nome5" . " from candidato_tmp c" . " join empresa e on e.nu_empresa = c.nu_empresa" . " left join embarcacao_projeto ep on ep.nu_empresa = c.nu_empresa and ep.nu_embarcacao_projeto = c.nu_embarcacao_projeto" . " left join usuarios u1 on u1.cd_usuario = e.usua_id_responsavel" . " left join usuarios u2 on u2.cd_usuario = e.usua_id_responsavel2" . " left join usuarios u3 on u3.cd_usuario = e.usua_id_responsavel3" . " left join usuarios u4 on u4.cd_usuario = e.usua_id_responsavel4" . " left join usuarios u5 on u5.cd_usuario = e.usua_id_responsavel5" . " where coalesce(id_status_edicao, 0) < 5" . " and c.nu_empresa in (select nu_empresa from empresa where usua_id_responsavel = " . $cd_usuario . " or usua_id_responsavel2 = " . $cd_usuario . " or usua_id_responsavel3 = " . $cd_usuario . " or usua_id_responsavel4 = " . $cd_usuario . " or usua_id_responsavel5 = " . $cd_usuario . " )" . " order by dt_envio_edicao desc";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll();
        return $rs;
    }

    public function excluir($nu_candidato_tmp) {
        $sql = "delete from candidato_tmp where nu_candidato_tmp = " . $nu_candidato_tmp;
        cAMBIENTE::$db_pdo->exec($sql);
        $sql = "delete from convidado where nu_candidato_tmp = " . $nu_candidato_tmp;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function desvinculeCandidato($nu_candidato_tmp) {
        $sql = "update candidato_tmp set nu_candidato = null, id_status_edicao = " . cstatus_edicao::CONFIRMADO . " where nu_candidato_tmp = " . $nu_candidato_tmp;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Associa o DR a um candidato existente, para fazer o aproveitamento dos dados
     * @param  [type] $nu_candidato     [description]
     * @param  [type] $nu_candidato_tmp [description]
     * @return [type]                   [description]
     */
    public function associeCandidato($nu_candidato, $nu_candidato_tmp) {
        $sql = "update candidato_tmp set nu_candidato = " . $nu_candidato . " where nu_candidato_tmp = " . $nu_candidato_tmp;
        $res = cAMBIENTE::$db_pdo->exec($sql);
    }

    /**
     * Adiciona uma experiencia ao DR
     * @param  [type] $nu_candidato_tmp [description]
     * @param  [type] $no_companhia     [description]
     * @param  [type] $no_funcao        [description]
     * @param  [type] $no_periodo       [description]
     * @param  [type] $tx_atribuicoes   [description]
     * @return [type]                   [description]
     */
    public function adicionarExperiencia($nu_candidato_tmp, $no_companhia, $no_funcao, $dt_inicio, $dt_fim, $tx_atribuicoes) {
        $exp = new cexperiencia_profissional;
        $exp->mnu_candidato_tmp = $nu_candidato_tmp;
        $exp->mepro_no_companhia = $no_companhia;
        $exp->mepro_no_funcao = $no_funcao;
        $exp->mepro_dt_inicio = $dt_inicio;
        $exp->mepro_dt_fim = $dt_fim;
        $exp->mepro_tx_atribuicoes = $tx_atribuicoes;
        $exp->Salvar();
        return $exp;
    }

    /**
     * Excluir uma experiencia
     * @return [type]                   [description]
     */
    public function excluirExperiencia($epro_id) {
        $exp = new cexperiencia_profissional;
        $exp->epro_id = $epro_id;
        $exp->Excluir();
        return true;
    }

    public function criar($nu_empresa, $nu_embarcacao_projeto, $nu_servico, $nome, $email, $id_usuario) {
        $msg = '';
        $conv = new cconvidado;

        if ($nu_empresa == '') {
            $msg.= '<br/>- a empresa é obrigatória';
        }
        else {
            $conv->mnu_empresa = $nu_empresa;
            $empresa = $conv->getEmpresa();
            if (intval($empresa->musua_id_responsavel) == 0) {
                $msg.= '<br/>- complete o cadastro da empresa: ponto focal não cadastrado';
                $ponto_focal = $empresa->getPontoFocal();
                if ($ponto_focal->cd_usuario > 0) {
                    if (trim($ponto_focal->nm_email) == '') {
                        $msg.= '<br/>- complete o cadastro do ponto focal: email não informado';
                    }
                }
                else {
                    $msg.= '<br/>- complete o cadastro da empresa: ponto focal não cadastrado';
                }
            }
            if (intval($empresa->musua_id_responsavel2) == 0) {
                $msg.= '<br/>- complete o cadastro da empresa: gestor não cadastrado';
            }
        }
        if ($nu_servico == '') {
            $msg.= '<br/>- o serviço é obrigatório';
        }
        if ($nome == '') {
            $msg.= '<br/>- o nome do candidato é obrigatório';
        }
        if ($email == '') {
            $msg.= '<br/>- o email do candidato é obrigatório';
        }
        else {
            $convx = new cconvidado;
            if ($convx->recuperesePeloEmail($email)) {
                $msg.= '<br/>- já existe um DR associado a esse e-mail. Não é possível criar outro.';
            }
        }
        if ($msg != '') {
            throw new Exception('Não foi possível criar o DR pois:' . $msg);
        }

        // Ok, então grava
        $cand = new cCANDIDATO_TMP;
        $cand->QuebreNomeCompleto($nome);
        $cand->mNO_EMAIL_CANDIDATO = $email;
        $cand->memail_pessoal = $email;
        $cand->mNU_EMPRESA = $nu_empresa;
        $cand->mNU_EMBARCACAO_PROJETO = $nu_embarcacao_projeto;
        $cand->mnu_servico = $nu_servico;
        $cand->mid_status_edicao = cstatus_edicao::NAO_INICIADO;
        $cand->mCO_USU_ULT_ALTERACAO = $id_usuario;
        $cand->mCO_USU_CADASTAMENTO = $id_usuario;
        $cand->salvar();

        $conv->mnome = $nome;
        $conv->memail = $email;
        $conv->mnu_empresa = $nu_empresa;
        $conv->mnu_embarcacao_projeto = $nu_embarcacao_projeto;
        $conv->mnu_servico = $nu_servico;
        $conv->mnu_candidato_tmp = $cand->mnu_candidato_tmp;
        $conv->Salvar();
        $this->enviaConvitePreenchimento($cand->mNOME_COMPLETO, $conv, $id_usuario);

        $cand->mconv_id = $conv->mconv_id;
        $cand->salvar();
    }

    public function enviaConvitePreenchimento($destinatario, $conv, $cd_usuario) {
        $gestor_cliente = new cusuarios;
        $gestor_cliente->RecuperePeloId($cd_usuario);
        $servico = new cSERVICO;
        $servico->RecuperePeloId($conv->mnu_servico);
        $empresa = $conv->getEmpresa();
        $ponto_focal = $empresa->getPontoFocal();
        $gestor = $empresa->getGestor();

        $data = array('destinatario' => $destinatario, 'conv' => $conv, 'empresa' => $empresa, 'servico' => $servico, 'ponto_focal' => $empresa->getPontoFocal(), 'gestor' => $empresa->getGestor(), 'gestor_cliente' => $gestor_cliente, 'dominio' => cAMBIENTE::$m_dominio);

        $msg = cTWIG::Render('intranet/candidato/email_convite.twig', $data);
        $mail = new cEMAIL;
        $mail->EnvieSimplificado($conv->email, $msg, "MUNDIVISAS - BRAZILIAN WORK VISA APPLICATION", $ponto_focal->nm_email);
    }

    /**
     * Suspende o acesso externo a um DR
     * @param  [type] $nu_candidato_tmp [description]
     * @return [type]                   [description]
     */
    public function suspenderAcesso($nu_candidato_tmp) {
        cconvidado::excluirPeloCandidato($nu_candidato_tmp);
    }

    public function cursorArquivos(\cCANDIDATO_TMP $candidato) {
        $res = $candidato->CursorArquivos();
        return $res->fetchAll(PDO::FETCH_ASSOC);
    }

    public function enviaAlteracoes($nu_candidato, $nu_candidato_tmp) {
        if (intval($nu_candidato) > 0) {
            $cand = new \cCANDIDATO();
            $cand->Recuperar($nu_candidato);
            $cand->colocaEdicaoPendente();
        }
        else {
            $cand = new \cCANDIDATO_TMP();
            $cand->RecuperarPeloTmp($nu_candidato_tmp);

            $this->enviaParaAprovacao($cand, $cand->mCO_USU_CADASTAMENTO);
            $this->geraCopiaDrEnviado($cand);
        }
    }

    public function geraCopiaDrEnviado(cCANDIDATO_TMP $cand) {
        $reparq = new cREPOSITORIO_ARQUIVO;

        $NO_ARQUIVO = 'CopiaDR.pdf';

        $reparq->excluiArquivoTemporarioExistente($cand->mnu_candidato_tmp, cTIPO_ARQUIVO::COPIA_DR_PREENCHIDO_PELO_CLIENTE);
        $arq = $reparq->salvaArquivo('', $cand->mnu_candidato_tmp, $NO_ARQUIVO, 0, cTIPO_ARQUIVO::COPIA_DR_PREENCHIDO_PELO_CLIENTE);
        $caminho = cARQUIVO::CaminhoReal("", $arq->mNO_ARQUIVO, "", "", $cand->mnu_candidato_tmp);
        try {
            $cand_completo = $this->candidatoComEntidades($cand->mnu_candidato_tmp);
            $res = $cand->Cursor_ExperienciaProfissional();
            $data['experiencias'] = $res->fetchAll();
            $data['candidato'] = $cand_completo;
            $html = cTWIG::Render('cliente/candidato/copia_dr.twig', $data);
            $mpdf = new mPDF();
            $mpdf->img_dpi = 200;
            $mpdf->WriteHTML($html);
            $mpdf->Output($caminho);
        }
        catch(Exception $ex) {
            if ($arq->mNU_SEQUENCIAL > 0) {
                $arq->excluir();
            }
            throw $ex;
        }
    }

    public function candidatoComEntidades($nu_candidato_tmp) {
        $sql = "select c.*" . ", coalesce(pn.no_nacionalidade_em_ingles, pn.no_pais_em_ingles, pn.no_nacionalidade) no_nacionalidade " . ", date_format(dt_nascimento, '%d/%m/%Y') DT_NASCIMENTO_FMT" . ", date_format(dt_emissao_seaman, '%d/%m/%Y') DT_EMISSAO_SEAMAN_FMT" . ", date_format(dt_validade_seaman, '%d/%m/%Y') DT_VALIDADE_SEAMAN_FMT" . ", date_format(dt_emissao_passaporte, '%d/%m/%Y') DT_EMISSAO_PASSAPORTE_FMT" . ", date_format(dt_validade_passaporte, '%d/%m/%Y') DT_VALIDADE_PASSAPORTE_FMT" . ", date_format(dt_envio_edicao, '%d/%m/%Y %H:%i:%s') dt_envio_edicao_FMT" . ", coalesce(pnp.no_pais_em_ingles,pnp.no_pais) pais_pai" . ", coalesce(pnm.no_pais_em_ingles,pnm.no_pais) pais_mae" . ", coalesce(pnpass.no_pais_em_ingles, pnpass.no_pais) no_pais_passaporte" . ", coalesce(pnestr.no_pais_em_ingles,pnestr.no_pais) no_pais_estrangeiro" . ", coalesce(pnseam.no_pais_em_ingles, pnseam.no_pais) no_pais_seaman" . ", coalesce(pnempr.no_pais_em_ingles, pnempr.no_pais) no_pais_empresa" . ", coalesce(pnempr.no_pais_em_ingles, pnempr.no_pais) no_pais_empresa" . ", ep.NO_EMBARCACAO_PROJETO" . ", e.NO_RAZAO_SOCIAL" . ", l.loca_no_nome" . " from candidato_tmp c" . " left join pais_nacionalidade pn on pn.co_pais = c.co_nacionalidade" . " left join pais_nacionalidade pnp on pnp.co_pais = c.co_pais_nacionalidade_pai" . " left join pais_nacionalidade pnm on pnm.co_pais = c.co_pais_nacionalidade_mae" . " left join pais_nacionalidade pnpass on pnpass.co_pais = c.co_pais_emissor_passaporte" . " left join pais_nacionalidade pnestr on pnestr.co_pais = c.co_pais_residencia" . " left join pais_nacionalidade pnseam on pnseam.co_pais = c.co_pais_seaman" . " left join pais_nacionalidade pnempr on pnempr.co_pais = c.co_pais_empresa" . " left join local_projeto l on l.local_id = c.loca_id" . " left join empresa e on e.nu_empresa = c.nu_empresa" . " left join embarcacao_projeto ep on ep.nu_empresa = c.nu_empresa and ep.nu_embarcacao_projeto = c.nu_embarcacao_projeto" . " where nu_candidato_tmp=" . $nu_candidato_tmp;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        foreach ($rs as & $item) {
            $item = $item;
        }
        if ($rs['CO_SEXO'] == 'M') {
            $rs['NO_SEXO'] = 'Masculine';
        }
        elseif ($rs['CO_SEXO'] == 'F') {
            $rs['NO_SEXO'] = "Feminine";
        }
        else {
            $rs['NO_SEXO'] = '(n/a)';
        }
        $rs['VA_REMUNERACAO_MENSAL'] = number_format($rs['VA_REMUNERACAO_MENSAL'], 2, ",", ".");
        $rs['VA_REMUNERACAO_MENSAL_BRASIL'] = number_format($rs['VA_REMUNERACAO_MENSAL_BRASIL'], 2, ",", ".");
        $rs['TE_DESCRICAO_ATIVIDADES'] = str_replace("\n", "<br/>", $rs['TE_DESCRICAO_ATIVIDADES']);
        $rs['TE_TRABALHO_ANTERIOR_BRASIL'] = str_replace("\n", "<br/>", $rs['TE_TRABALHO_ANTERIOR_BRASIL']);
        return $rs;
    }

    /**
     * Importa os dados de um DR para um candidato real.
     *
     * Se houver um NU_CANDIDATO informado, é importado para esse candidato, senão é importado para um novo.
     *
     * Gera exception caso haja problema na consistencia
     *
     * @param  [type] $post [description]
     * @return [type]       [description]
     */
    public function importaDr($post, $cd_usuario) {
        $cand = new cCANDIDATO;
        if ($post['CMP_NU_CANDIDATO'] > 0) {
            $cand->Recuperar($post['CMP_NU_CANDIDATO']);
        }
        else {
            $cand->mCO_USU_CADASTAMENTO = $cd_usuario;
        }
        $cand->mCO_USU_ULT_ALTERACAO = $cd_usuario;
        $cand->CarreguePropriedadesDoPost($post);
        $this->consisteCandidatoImportado($cand);
        if (array_key_exists('fechar', $post) && intval($post['CMP_NU_CANDIDATO']) == 0) {
            throw new Exception("O DR só pode ser fechado depois de associado a um candidato");
        }
        $cand->salveTodosAtributos();

        $dr = new cCANDIDATO_TMP();
        $dr->RecuperarPeloTmp($post['CMP_nu_candidato_tmp']);
        if (array_key_exists('fechar', $post)) {
            $dr->mid_status_edicao = cstatus_edicao::IMPORTADO;
        }
        $dr->mNU_CANDIDATO = $cand->mNU_CANDIDATO;
        $dr->salvar();

        if (array_key_exists('fechar', $post)) {
            $reparq = new cREPOSITORIO_ARQUIVO;
            $reparq->migraArquivos($post['CMP_nu_candidato_tmp'], $cand->mNU_CANDIDATO);
        }
    }

    public function consisteCandidatoImportado($cand) {
        $cand->montaNomeCompleto();
        $msg = '';
        if (trim($cand->mNOME_COMPLETO) == '') {
            $msg.= "- O nome do candidato não foi informado.";
        }
        if (trim($cand->mNO_MAE) == '') {
            $msg.= "- O nome da mãe do candidato é obrigatório.";
        }
        if (trim($cand->mDT_NASCIMENTO) == '') {
            $msg.= "- A data de nascimento é obrigatória.";
        }
        if ($msg != '') {
            throw new Exception($msg);
        }
    }

    /**
     * Envia o DR para aprovação do cliente
     * @param  [type] $id_candidato_tmp [description]
     * @return [type]                   [description]
     */
    public function enviaParaAprovacao($dr, $cd_usuario) {
        // Envia e-mail
        $this->enviaSolicitacaoDeAprovacao($dr, $cd_usuario);

        // Altera o status
        $dr->enviadoParaAprovacao($cd_usuario);
    }

    public function enviaSolicitacaoDeAprovacao($dr, $cd_usuario) {
        $empresa = $dr->getEmpresa();

        $preenchedor = $dr->nome_email_preenchedor();

        $gestor_cliente = new cusuarios;
        $gestor_cliente->RecuperePeloId($cd_usuario);

        //$servico->RecuperePeloId($nu_servico);
        $gestor = $empresa->getGestor();
        $ponto_focal = $empresa->getPontoFocal();

        if ($dr->mconv_id > 0) {
            $conv = $dr->getConvidado();
            $linkDR = cAMBIENTE::$m_dominio . '/paginas/carregueConvidado.php?metodo=getLogin&conv_id=' . $conv->mconv_id . '&chave=' . $conv->mchave . '&nu_candidato_tmp=' . $dr->mnu_candidato_tmp . '&redirect=getCopiaDr';
            $linkCONFIRMA = cAMBIENTE::$m_dominio . '/paginas/carregueConvidado.php?metodo=getLogin&conv_id=' . $conv->mconv_id . '&chave=' . $conv->mchave . '&nu_candidato_tmp=' . $dr->mnu_candidato_tmp . '&redirect=getConfirmaEnvio';
            $linkCANCELA = cAMBIENTE::$m_dominio . '/paginas/carregueConvidado.php?metodo=getLogin&conv_id=' . $conv->mconv_id . '&chave=' . $conv->mchave . '&nu_candidato_tmp=' . $dr->mnu_candidato_tmp . '&redirect=getCancelaEnvio';
            $servico = $conv->getServico();
        } else {
            $dr->atualizaHash();
            $usuario = $dr->getUsuarioCadastro();
            $linkDR = cAMBIENTE::$m_dominio . '/paginas/carregueClienteExpresso.php?metodo=getCopiaDr&usuario='.$usuario->cd_usuario.'&hash='.$dr->mhash_string.'&nu_candidato_tmp=' . $dr->mnu_candidato_tmp ;
            $linkCONFIRMA = cAMBIENTE::$m_dominio . '/paginas/carregueClienteExpresso.php?metodo=getConfirmaEnvio&usuario='.$usuario->cd_usuario.'&hash='.$dr->mhash_string.'&nu_candidato_tmp='.$dr->mnu_candidato_tmp ;
            $linkCANCELA  = cAMBIENTE::$m_dominio   . '/paginas/carregueClienteExpresso.php?metodo=getCancelaEnvio&usuario='.$usuario->cd_usuario.'&hash='.$dr->mhash_string.'&nu_candidato_tmp='.$dr->mnu_candidato_tmp ;
            $servico = $dr->getServico();
        }

        $data = array(
                  'destinatario' => $destinatario
                , 'servico' => $servico
                , 'ponto_focal' => $ponto_focal
                , 'gestor' => $gestor
                , 'gestor_cliente' => $gestor_cliente
                , 'linkDR' => $linkDR
                , 'linkCONFIRMA' => $linkCONFIRMA
                , 'linkCANCELA' => $linkCANCELA
            );

        $msg = cTWIG::Render('intranet/candidato/email_solicitacao_aprovacao.twig', $data);

        $mail = new cEMAIL;
        $mail->EnvieSimplificado($preenchedor['email'], $msg, "MUNDIVISAS - BRAZILIAN WORK VISA APPLICATION", $ponto_focal->nm_email);
    }

    public function confirmaEnvio() {
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        $cand = new \cCANDIDATO_TMP();
        $cand->RecuperarPeloTmp($nu_candidato_tmp);
        $cand->mid_status_edicao = cstatus_edicao::CONFIRMADO;
        $cand->mdt_envio_aprovacao = date('d/m/Y H:i:s');
        $cand->mhash_string = '';
        $cand->mhash_dt_cadastro = '';
        $cand->salvar();
    }

    public function cancelaEnvio() {
        $nu_candidato_tmp = $_GET['nu_candidato_tmp'];
        $cand = new \cCANDIDATO_TMP();
        $cand->RecuperarPeloTmp($nu_candidato_tmp);
        $cand->mid_status_edicao = cstatus_edicao::AGUARDANDO_ENVIO;
        $cand->mdt_envio_edicao = '';
        $cand->mhash_string = '';
        $cand->mhash_dt_cadastro = '';
        $cand->salvar();
    }

    public function usuario_pode_acessar_dr($cd_usuario, $nu_candidato_tmp) {
        $this->cand = new cCANDIDATO_TMP;
        $this->cand->RecuperarPeloTmp($nu_candidato_tmp);
        if ($cd_usuario > 0) {
            $usuario = new cusuarios;
            $usuario->RecuperePeloId($cd_usuario);
            if ($usuario->mcd_empresa == $this->cand->mNU_EMPRESA){
                return true;
            } else {
                throw new Exception("You don't have access to this information");
            }
        }
        else {
            throw new Exception("Your session has expired. Please inform your credentials again.");
        }
    }

    public function convidado_pode_acessar_dr($conv_id, $nu_candidato_tmp) {
        $this->cand = new cCANDIDATO_TMP;
        $this->cand->RecuperarPeloTmp($nu_candidato_tmp);
        if ($conv_id > 0) {
            if ($conv_id != $this->cand->mconv_id) {
                throw new Exception("You don't have access to this information");
            } else {
                return true;
            }
        }
        else {
            throw new Exception("Your session has expired. Please access the original link again.");
        }
    }
}
