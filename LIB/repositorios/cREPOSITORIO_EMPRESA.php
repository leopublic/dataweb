<?
class cREPOSITORIO_EMPRESA extends cREPOSITORIO{
	    /**
     * Popula uma empresa com candidatos fictÃ­Â­cios de outra
     *
     *
     */
    public static function PopularEmpresa($pNU_EMPRESA_orig, $pNU_EMBARCACAO_PROJETO_orig, $pNU_EMPRESA_dest, $pNU_EMBARCACAO_PROJETO_dest) {
        //Abre cursor de candidatos da embarcacao origem
        $sql = "select NU_CANDIDATO from candidato where nu_empresa = " . $pNU_EMPRESA_orig . " and NU_EMBARCACAO_PROJETO = " . $pNU_EMBARCACAO_PROJETO_orig;
        $res = cAMBIENTE::$db_pdo->query($sql);
        while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            //Salva novo candidato
            $cand = new cCANDIDATO();
            $cand->Recuperar($rs['NU_CANDIDATO']);
            $cand->mNU_CANDIDATO = '';
            $cand->mNU_EMPRESA = $pNU_EMPRESA_dest;
            $cand->mNU_EMBARCACAO_PROJETO = $pNU_EMBARCACAO_PROJETO_dest;
            if ($cand->mNO_MAE == '') {
                $cand->mNO_MAE = '(incomplete)';
            }
            if ($cand->mDT_NASCIMENTO == '') {
                $cand->mDT_NASCIMENTO = '1968-09-15';
            }
            $sexo = "";
            //Seleciona 3 nomes aleatorios
            $x = rand(10000, 20000);
            $sql = "select NO_PRIMEIRO_NOME from candidato where NO_PRIMEIRO_NOME is not null and NO_PRIMEIRO_NOME <> '' " . $sexo . " limit " . $x . ",1";
            $resx = cAMBIENTE::$db_pdo->query($sql);
            $rsx = $resx->fetch(PDO::FETCH_ASSOC);
            $cand->mNO_PRIMEIRO_NOME = $rsx['NO_PRIMEIRO_NOME'];
            $x = rand(10000, 20000);
            $sql = "select NO_NOME_MEIO from candidato where NO_NOME_MEIO is not null and NO_NOME_MEIO <> '' " . $sexo . " limit " . $x . ",1";
            $resx = cAMBIENTE::$db_pdo->query($sql);
            $rsx = $resx->fetch(PDO::FETCH_ASSOC);
            $cand->mNO_NOME_MEIO = $rsx['NO_NOME_MEIO'];
            $x = rand(10000, 20000);
            $sql = "select NO_ULTIMO_NOME from candidato where NO_ULTIMO_NOME is not null and NO_ULTIMO_NOME <> '' " . $sexo . " limit " . $x . ",1";
            $resx = cAMBIENTE::$db_pdo->query($sql);
            $rsx = $resx->fetch(PDO::FETCH_ASSOC);
            $cand->mNO_ULTIMO_NOME = $rsx['NO_ULTIMO_NOME'];
            $cand->mNOME_COMPLETO = '';
            $cand->Salve_Novo();
            //Seleciona processo mte atual
            if (intval($cand->mcodigo_processo_mte_atual) > 0) {
                $codigoAtual = $cand->mcodigo_processo_mte_atual;
                $mte = new cprocesso_mte();
                $mte->RecupereSe($cand->mcodigo_processo_mte_atual);
                $mte->mcodigo = "";
                $mte->mcd_candidato = $cand->mNU_CANDIDATO;
                $mte->mid_solicita_visto = '';
                $mte->mNU_EMPRESA = $pNU_EMPRESA_dest;
                $mte->mNU_EMBARCACAO_PROJETO = $pNU_EMBARCACAO_PROJETO_dest;
                //Salva com o novo candidato
                $mte->IncluaNovo();
                //Atualiza processo atual
                $cand->AtualizeVistoAtual($mte->mcodigo);
                //Recupera prorrogaÃƒÂ§oes do visto atual
                $sql = "select codigo from processo_prorrog where codigo_processo_mte = " . $codigoAtual;
                $resx = cAMBIENTE::$db_pdo->query($sql);
                while ($rsx = $resx->fetch(PDO::FETCH_ASSOC)) {
                    $prorrog = new cprocesso_prorrog();
                    $prorrog->RecupereSe($rsx['codigo']);
                    //Altera o processo atual, o candidato e salva
                    $prorrog->mcodigo = '';
                    $prorrog->mcodigo_processo_mte = $mte->mcodigo;
                    $prorrog->mcd_candidato = $cand->mNU_CANDIDATO;
                    $prorrog->Salve_ProcessoEdit();
                }
                //Recupera cancelamentos do visto atual
                $sql = "select codigo from processo_cancel where codigo_processo_mte = " . $codigoAtual;
                $resx = cAMBIENTE::$db_pdo->query($sql);
                while ($rsx = $resx->fetch(PDO::FETCH_ASSOC)) {
                    $processo = new cprocesso_cancel();
                    $processo->RecupereSe($rsx['codigo']);
                    //Altera o processo atual, o candidato e salva
                    $processo->mcodigo = '';
                    $processo->mcodigo_processo_mte = $mte->mcodigo;
                    $processo->mcd_candidato = $cand->mNU_CANDIDATO;
                    $processo->Salve_ProcessoEdit();
                }
                //Recupera registros do visto atual
                $sql = "select codigo from processo_regcie where codigo_processo_mte = " . $codigoAtual;
                $resx = cAMBIENTE::$db_pdo->query($sql);
                while ($rsx = $resx->fetch(PDO::FETCH_ASSOC)) {
                    $processo = new cprocesso_regcie();
                    $processo->RecupereSe($rsx['codigo']);
                    //Altera o processo atual, o candidato e salva
                    $processo->mcodigo = '';
                    $processo->mcodigo_processo_mte = $mte->mcodigo;
                    $processo->mcd_candidato = $cand->mNU_CANDIDATO;
                    $processo->Salve_ProcessoEdit();
                }
                //Recupera coletas do visto atual
                $sql = "select codigo from processo_coleta where codigo_processo_mte = " . $codigoAtual;
                $resx = cAMBIENTE::$db_pdo->query($sql);
                while ($rsx = $resx->fetch(PDO::FETCH_ASSOC)) {
                    $processo = new cprocesso_coleta();
                    $processo->RecupereSe($rsx['codigo']);
                    //Altera o processo atual, o candidato e salva
                    $processo->mcodigo = '';
                    $processo->mcodigo_processo_mte = $mte->mcodigo;
                    $processo->mcd_candidato = $cand->mNU_CANDIDATO;
                    $processo->Salve_ProcessoEdit();
                }
                //Recupera emissoes cie do visto atual
                $sql = "select codigo from processo_emiscie where codigo_processo_mte = " . $codigoAtual;
                $resx = cAMBIENTE::$db_pdo->query($sql);
                while ($rsx = $resx->fetch(PDO::FETCH_ASSOC)) {
                    $processo = new cprocesso_emiscie();
                    $processo->RecupereSe($rsx['codigo']);
                    //Altera o processo atual, o candidato e salva
                    $processo->mcodigo = '';
                    $processo->mcodigo_processo_mte = $mte->mcodigo;
                    $processo->mcd_candidato = $cand->mNU_CANDIDATO;
                    $processo->Salve_ProcessoEdit();
                }
            }
        }
    }

    public function CursorEnderecoCobranca($pFiltros, $pOrdem) {
        $sql = "select * from empresa where 1=1 ";
        if (is_array($pFiltros)) {
            $sql .= cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        }
        if ($pOrdem != '') {
            $sql .= " order by " . $pOrdem;
        }
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
        return $res;
    }

}
