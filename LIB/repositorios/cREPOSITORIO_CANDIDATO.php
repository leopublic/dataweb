<?php

class cREPOSITORIO_CANDIDATO {

    public function pesquisaParaOs($nome_completo, $nu_empresa = '', $nu_servico = '') {
        $nome_completo = str_replace("'", "%", str_replace(" ", "%", str_replace('.', '%', trim($nome_completo))));
        $query = "select count(*) FROM CANDIDATO C";
        $query .= " WHERE NOME_COMPLETO like '%" . $nome_completo . "%'";
        if (intval($nu_empresa) > 0) {
            $query .= " AND C.NU_EMPRESA = " . $nu_empresa;
        }
        $res = cAMBIENTE::$db_pdo->query($query);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $qtd = $rs[0];

        $query = " SELECT C.nu_candidato"
                . " ,C.nome_completo"
                . " ,C.no_pai"
                . " ,C.no_mae"
                . " ,date_format(C.DT_NASCIMENTO, '%d/%m/%Y') dt_nascimento_fmt"
                . " ,C.nu_passaporte"
                . " ,date_format(C.DT_CADASTRAMENTO, '%d/%m/%Y') as dt_cadastramento_fmt"
                . " ,date_format(C.DT_ULT_ALTERACAO, '%d/%m/%Y') as dt_ult_alteracao_fmt"
                . " ,e.no_razao_social"
                . " FROM CANDIDATO C";
        $query .= "  LEFT JOIN EMPRESA e ON e.NU_EMPRESA = C.NU_EMPRESA";
        $query .= " WHERE NOME_COMPLETO like '%" . $nome_completo . "%'";
        if (intval($nu_empresa) > 0) {
            $query .= " AND C.NU_EMPRESA = " . $nu_empresa;
        }
        $query .= " ORDER BY nome_completo"
                . " limit 20";
        $res = cAMBIENTE::$db_pdo->query($query);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        $ret = array('total' => $qtd, 'registros' => $rs);
        return $ret;
    }

    public function osNaoConcluida($nu_empresa, $nu_servico, $nu_candidato) {
// Verifica se existe OS desse serviÃ§o aberta para esse candidato.
        $sql = "select sv.id_solicita_visto, sv.nu_solicitacao, c.codigo_processo_mte_atual"
                . " from candidato c"
                . " join autorizacao_candidato ac on ac.nu_candidato = c.nu_candidato"
                . " join solicita_visto sv on sv.id_solicita_visto = ac.id_solicita_visto"
                . " join status_solicitacao ss on ss.id_status_sol = sv.id_status_sol"
                . " where sv.nu_empresa = " . $nu_empresa
                . "     and sv.nu_servico = " . $nu_servico
                . "	and coalesce(ss.FL_ENCERRADA, 0) = 0 "
                . "     and c.nu_candidato = " . $nu_candidato
                . " order by sv.id_solicita_visto desc limit 1";

        $res = cAMBIENTE::$db_pdo->query($sql);
        if ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $ret = array('id_solicita_visto' => $rs['id_solicita_visto'], 'nu_solicitacao' => $rs['nu_solicitacao']);
        } else {
            $ret = null;
        }
        return $ret;
    }

    /**
     * Recupera as OS de pacote que nÃ£o estÃ£o sendo utilizadas
     */
    public function osDePacotePendentes($nu_empresa, $nu_servico, $nu_candidato) {
        $sql = "select sv.id_solicita_visto, sv.nu_solicitacao, sv.id_solicita_visto_pacote, svp.nu_solicitacao nu_solicitacao_pacote"
                . " from candidato c "
                . " join autorizacao_candidato ac on ac.nu_candidato = c.nu_candidato"
                . " join solicita_visto sv on sv.id_solicita_visto = ac.id_solicita_visto"
                . " join solicita_visto svp on svp.id_solicita_visto = sv.id_solicita_visto_pacote"
                . " where sv.nu_empresa = " . $nu_empresa
                . " and sv.nu_servico = " . $nu_servico
                . " and sv.soli_fl_disponivel = 1"
                . " and c.nu_candidato = " . $nu_candidato
                . " order by sv.id_solicita_visto desc limit 1";
        $res = cAMBIENTE::$db_pdo->query($sql);
        if ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
            $ret = array(
                'id_solicita_visto' => $rs['id_solicita_visto']
                , 'nu_solicitacao' => $rs['nu_solicitacao']
                , 'id_solicita_visto_pacote' => $rs['id_solicita_visto_pacote']
                , 'nu_solicitacao_pacote' => $rs['nu_solicitacao_pacote']
            );
        } else {
            $ret = null;
        }
        return $ret;
    }

    public function usarOsPendente($nu_candidato, $nu_servico, $nu_empresa) {
// Remove os candidatos anteriores dessa OS
//
    }

    public function LoginValidoCrypt($login, $senha) {
        $cand = new cCANDIDATO();
        if ($cand->recuperesePeloEmail($login)) {
            if ($senha == $cand->mNO_SENHA) {
                cSESSAO::CriaSessaoCandidato($cand);
                return true;
            } else {
//                    $this->incrementaAcessoNegado();
//                  $restantes = 5 - $this->usua_qtd_acesso_negado;
//                $msg = "Login ou senha invÃ¡lidos (" . $restantes . " tentativas restantes)<br/>Username or password invalid (" . $restantes . " tries left)";
                $msg = "Login ou senha invÃ¡lidos<br/>Username or password invalid";
                return false;
            }
        } else {
            $cand = new cCANDIDATO_TMP();
            if ($cand->recuperesePeloEmail($login)) {
                if ($senha == $cand->mNO_SENHA) {
                    cSESSAO::CriaSessaoCandidatoTmp($cand);
                    return true;
                } else {
//                    $this->incrementaAcessoNegado();
//                  $restantes = 5 - $this->usua_qtd_acesso_negado;
//                $msg = "Login ou senha invÃ¡lidos (" . $restantes . " tentativas restantes)<br/>Username or password invalid (" . $restantes . " tries left)";
                    $msg = "Login ou senha invÃ¡lidos<br/>Username or password invalid";
                    return false;
                }
            }
        }
    }

    public function migreProcesso($nu_candidato_destino) {

    }

    /**
     * Retorna o proprio candidato quando ele esta acessando
     */
    public function candidatoProprio($nu_candidato, $nu_candidato_tmp) {
        $cand = new \cCANDIDATO();
        $cand_tmp = new cCANDIDATO_TMP();
        if ($nu_candidato > 0) {
            $cand->Recuperar($nu_candidato);
            $cand_tmp->mNU_CANDIDATO = $nu_candidato;
            $cand_tmp->Recuperar();
            if ($cand_tmp->mnu_candidato_tmp == '') {
                $cand_tmp->CriaCandidatoCliente();
            }
// Se a ediÃ§Ã£o estÃ¡ habilitada
            if ($cand->mid_status_edicao == cstatus_edicao::NAO_INICIADO) {
// Atualiza temporÃ¡rio com dados do oficial
                $cand_tmp->AtualizeCadastroParaCliente();
            }
            $cand_tmp->Recuperar();
            $cand_tmp->mid_status_edicao = $cand->mid_status_edicao;
            $cand_tmp->mFL_HABILITAR_CV = $cand->mFL_HABILITAR_CV;
            $cand_tmp->mFL_ATUALIZACAO_HABILITADA = $cand->mFL_ATUALIZACAO_HABILITADA;
        } elseif ($nu_candidato_tmp > 0) {
            $cand_tmp->mnu_candidato_tmp = $nu_candidato_tmp;
            $cand_tmp->RecuperarPeloTmp();
            $cand_tmp->mid_status_edicao = 2;
        }
        return $cand_tmp;
    }

    public function candidatoParaCliente($nu_candidato, $nu_candidato_tmp = '') {
        $cand = new \cCANDIDATO();
        $cand_tmp = new cCANDIDATO_TMP();
        if ($nu_candidato > 0) {
            $cand->Recuperar($nu_candidato);
            if ($cand->mNU_EMPRESA == cSESSAO::$mcd_empresa ) {
                return $cand;
            } else {
                throw new Exception("You don't have access to this person profile.");
            }
        } else{
            if ($nu_candidato_tmp > 0) {
                $cand_tmp->mnu_candidato_tmp = $nu_candidato_tmp;
                $cand_tmp->RecuperarPeloTmp();
//                $cand_tmp->mid_status_edicao = 2;
            } else {
                $cand_tmp->mNOME_COMPLETO = '(new)';
                $cand_tmp->mid_status_edicao = 1;
                $cand_tmp->mFL_HABILITAR_CV = 1;
                $cand_tmp->mFL_ATUALIZACAO_HABILITADA = 1;
            }
            if ($cand_tmp->mNU_EMPRESA == cSESSAO::$mcd_empresa || intval($cand_tmp->mnu_candidato_tmp) == 0) {
                return $cand_tmp;
            } else {
                throw new Exception("You don't have access to this person profile.");
            }

        }
    }

    public function salvaAlteracoesTmp($nu_candidato_tmp, $post) {
        if ($nu_candidato_tmp > 0) {
            $cand_tmp = new cCANDIDATO_TMP();
            $cand_tmp->RecuperarPeloTmp($nu_candidato_tmp);
            $cand_tmp->CarreguePropriedadesDoPost($post, 'm');
            $cand_tmp->AtualizarExterno();
            $cand_tmp->colocaEdicaoIniciada();
        }
    }

    public function salvaAlteracoesCliente($nu_candidato, $nu_candidato_tmp, $post) {
        if ($nu_candidato > 0) {
            $cand = new \cCANDIDATO();
            $cand->Recuperar($nu_candidato);
            // Se ediÃ§Ã£o habilitada
            if ($cand->mid_status_edicao == cstatus_edicao::NAO_INICIADO) {
                $cand_tmp = new cCANDIDATO_TMP();
                $cand_tmp->mNU_CANDIDATO = $nu_candidato;
                // Atualiza temporÃ¡rio com dados do oficial
                $cand_tmp->Recuperar($nu_candidato);
                // Salva alteraÃ§Ãµes
                $cand_tmp->CarreguePropriedadesDoPost($post, 'm');
                $cand_tmp->AtualizarExterno();
                $cand->colocaEdicaoIniciada();
                return $cand_tmp;
            } else {
                // Recupera temporÃ¡rio
                $cand_tmp = new cCANDIDATO_TMP();
                $cand_tmp->mNU_CANDIDATO = $nu_candidato;
                $cand_tmp->Recuperar($nu_candidato);
                // Salva alteraÃ§Ãµes
                $cand_tmp->CarreguePropriedadesDoPost($post, 'm');
                $cand_tmp->AtualizarExterno();
                return $cand_tmp;
            }
        } elseif (intval($nu_candidato_tmp) > 0) {
            $cand_tmp = new cCANDIDATO_TMP();
            $cand_tmp->RecuperarPeloTmp($nu_candidato_tmp);
            // Salva alteraÃ§Ãµes
            $cand_tmp->CarreguePropriedadesDoPost($post, 'm');
            $cand_tmp->AtualizarExterno();
            return $cand_tmp;
        } else {
            // Cria um novo DR
            $cand_tmp = new cCANDIDATO_TMP();
            // Salva alteraÃ§Ãµes
            $cand_tmp->CarreguePropriedadesDoPost($post, 'm');
            $cand_tmp->mNU_EMPRESA = cSESSAO::$mcd_empresa;
            $cand_tmp->mid_status_edicao = cstatus_edicao::AGUARDANDO_ENVIO;
            $cand_tmp->salvar();
            return $cand_tmp;
        }
    }

    public function candidatoComEntidadesFinal($nu_candidato) {
        $sql = "select c.*"
                . ", coalesce(pn.no_nacionalidade_em_ingles, pn.no_pais_em_ingles, pn.no_nacionalidade) no_nacionalidade "
                . ", date_format(dt_nascimento, '%d/%m/%Y') DT_NASCIMENTO_FMT"
                . ", date_format(dt_emissao_seaman, '%d/%m/%Y') DT_EMISSAO_SEAMAN_FMT"
                . ", date_format(dt_validade_seaman, '%d/%m/%Y') DT_VALIDADE_SEAMAN_FMT"
                . ", date_format(dt_emissao_passaporte, '%d/%m/%Y') DT_EMISSAO_PASSAPORTE_FMT"
                . ", date_format(dt_validade_passaporte, '%d/%m/%Y') DT_VALIDADE_PASSAPORTE_FMT"
                . ", date_format(dt_envio_edicao, '%d/%m/%Y %H:%i:%s') dt_envio_edicao_FMT"
                . ", coalesce(pnp.no_pais_em_ingles,pnp.no_pais) pais_pai"
                . ", coalesce(pnm.no_pais_em_ingles,pnm.no_pais) pais_mae"
                . ", coalesce(pnpass.no_pais_em_ingles, pnpass.no_pais) no_pais_passaporte"
                . ", coalesce(pnestr.no_pais_em_ingles,pnestr.no_pais) no_pais_estrangeiro"
                . ", coalesce(pnseam.no_pais_em_ingles, pnseam.no_pais) no_pais_seaman"
                . ", coalesce(pnempr.no_pais_em_ingles, pnempr.no_pais) no_pais_empresa"
                . ", coalesce(ec.no_estado_civil_em_ingles, '(n/a)') no_estado_civil_em_ingles"
                . ", coalesce(e.no_escolaridade_em_ingles, '(n/a)') no_escolaridade_em_ingles"
                . ", coalesce(p.no_profissao_em_ingles, '(n/a)') no_profissao_em_ingles"
                . ", coalesce(fc.no_funcao_em_ingles, '(n/a)') no_funcao_em_ingles"
                . ", coalesce(rc.no_reparticao_consular, '(n/a)') no_reparticao_consular"
                . ", ep.NO_EMBARCACAO_PROJETO"
                . ", e.NO_RAZAO_SOCIAL"
                . ", l.loca_no_nome"
                . " from candidato c"
                . " left join pais_nacionalidade pn on pn.co_pais = c.co_nacionalidade"
                . " left join pais_nacionalidade pnp on pnp.co_pais = c.co_pais_nacionalidade_pai"
                . " left join pais_nacionalidade pnm on pnm.co_pais = c.co_pais_nacionalidade_mae"
                . " left join pais_nacionalidade pnpass on pnpass.co_pais = c.co_pais_emissor_passaporte"
                . " left join pais_nacionalidade pnestr on pnestr.co_pais = c.co_pais_residencia"
                . " left join pais_nacionalidade pnseam on pnseam.co_pais = c.co_pais_seaman"
                . " left join pais_nacionalidade pnempr on pnempr.co_pais = c.co_pais_empresa"
                . " left join estado_civil ec on ec.co_estado_civil = c.co_estado_civil "
                . " left join escolaridade e on e.co_escolaridade = c.co_nivel_escolaridade "
                . " left join profissao p on p.co_profissao = c.co_profissao_candidato "
                . " left join funcao_cargo fc on fc.co_funcao = c.co_funcao "
                . " left join local_projeto l on l.local_id = c.loca_id"
                . " left join reparticao_consular rc on rc.co_reparticao_consular = c.co_reparticao_consular "
                . " left join empresa e on e.nu_empresa = c.nu_empresa"
                . " left join embarcacao_projeto ep on ep.nu_empresa = c.nu_empresa and ep.nu_embarcacao_projeto = c.nu_embarcacao_projeto"
                . " where nu_candidato=" . $nu_candidato;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        $rs['VA_REMUNERACAO_MENSAL'] = number_format($rs['VA_REMUNERACAO_MENSAL'], 2, ",", ".");
        $rs['VA_REMUNERACAO_MENSAL_BRASIL'] = number_format($rs['VA_REMUNERACAO_MENSAL_BRASIL'], 2, ",", ".");
        $rs['TE_DESCRICAO_ATIVIDADES'] = str_replace("\n", "<br/>", $rs['TE_DESCRICAO_ATIVIDADES']);
        $rs['TE_TRABALHO_ANTERIOR_BRASIL'] = str_replace("\n", "<br/>", $rs['TE_TRABALHO_ANTERIOR_BRASIL']);
        return $rs;
    }

    public function edicaoHabilitada($candidato, $cd_usuario) {
        if ($candidato->mNU_CANDIDATO > 0) {
            if ($candidato->mFL_HABILITAR_CV) {
                if ($candidato->mid_status_edicao == cstatus_edicao::EM_REVISAO) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            if ($candidato->mid_status_edicao == cstatus_edicao::NAO_INICIADO || $candidato->mid_status_edicao == cstatus_edicao::AGUARDANDO_ENVIO) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function atualizaDoInputExterno($post) {
        $cand = new cCANDIDATO;
        $cand->mNU_CANDIDATO = $post['CMP_NU_CANDIDATO'];
        $cand->Recuperar();
        $cand->CarreguePropriedadesDoPost($post);
        $cand->AtualizarImportacoes();

        $sql = "update candidato_tmp set nu_candidato = " . $post['CMP_NU_CANDIDATO'] . " id_status_edicao = " . cstatus_edicao::AGUARDANDO_CONFIRMACAO . " where nu_candidato_tmp=" . $post['CMP_nu_candidato_tmp'];
        print $sql;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function cursorArquivos(\cCANDIDATO_BASE $candidato) {
        if ($candidato->mNU_CANDIDATO > 0) {
            $cand_real = new cCANDIDATO();
            $cand_real->Recuperar($_GET['NU_CANDIDATO']);
            $res = $cand_real->CursorArquivos(true, true);
            return $res->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $res = $candidato->CursorArquivos(true);
            return $res->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function convidaCandidato($no_primeiro_nome, $no_nome_meio, $no_ultimo_nome, $email_pessoal, $no_mae, $dt_nascimento, $nu_embarcacao_projeto) {
// Se informou o nu_candidato
// Verifica se existe alguem usando o e-mail informado
        $candidato = null;
        $cand = new cCANDIDATO;
        $cand->recuperesePeloEmail($email_pessoal);
        if ($cand->mNU_CANDIDATO > 0) {
            if ($cand->mNO_PRIMEIRO_NOME != $no_primeiro_nome || $cand->mNO_NOME_MEIO != $no_nome_meio || $cand->mNO_ULTIMO_NOME != $no_ultimo_nome) {
                throw new exception('The e-mail informed is being used by another candidate. Please review.');
            } else {
                $candidato = $cand;
            }
        }
        $cand_tmp = new cCANDIDATO_TMP;
        $cand_tmp->recuperesePeloEmail($email_pessoal);
        if ($cand_tmp->mnu_candidato_tmp > 0) {
            if ($cand_tmp->mNO_PRIMEIRO_NOME != $no_primeiro_nome || $cand_tmp->mNO_NOME_MEIO != $no_nome_meio || $cand_tmp->mNO_ULTIMO_NOME != $no_ultimo_nome) {
                throw new exception('The e-mail informed is being used by another candidate. Please review.');
            } else {
                $candidato = $cand_tmp;
            }
        }

        if (!isset($candidato)) {
            $cand = new cCANDIDATO;
            $cand->RecuperesePelosNomes($no_primeiro_nome, $no_nome_meio, $no_ultimo_nome, $no_mae, $dt_nascimento);
            if ($cand->mNU_CANDIDATO > 0) {
                $candidato = $cand;
                $candidato->memail_pessoal = $email_pessoal;
                $candidato->AtualizeAtributo('email_pessoal', $email_pessoal);
            } else {
                $cand = new cCANDIDATO_TMP;
                $cand->RecuperesePelosNomes($no_primeiro_nome, $no_nome_meio, $no_ultimo_nome, $no_mae, $dt_nascimento);
                if ($cand->mnu_candidato_tmp > 0) {
                    $candidato = $cand;
                    $candidato->memail_pessoal = $email_pessoal;
                    $candidato->AtualizarExterno();
                } else {
                    $candidato = new cCANDIDATO_TMP;
                    $candidato->mNOME_COMPLETO = cCANDIDATO::CalculaNomeCompleto($no_primeiro_nome, $no_nome_meio, $no_ultimo_nome);
                    $candidato->mNU_EMPRESA = cSESSAO::$mcd_empresa;
                    $candidato->mNO_PRIMEIRO_NOME = $no_primeiro_nome;
                    $candidato->mNO_NOME_MEIO = $no_nome_meio;
                    $candidato->mNO_ULTIMO_NOME = $no_ultimo_nome;
                    $candidato->memail_pessoal = $email_pessoal;
                    $candidato->mDT_NASCIMENTO = $dt_nascimento;
                    $candidato->mNU_EMBARCACAO_PROJETO = $nu_embarcacao_projeto;
                    $candidato->mNO_MAE = $no_mae;
                    $candidato->AtualizarExterno();
                }
            }
        }
        if (isset($candidato)) {
            $candidato->gerarNovaSenha();
            $this->enviarEmailConvite($candidato);
            return $candidato;
        }
    }

    public function cursorHomonimos($nome_completo, $nu_candidato = 0) {
        if ($nome_completo != '') {
            $sql = "select c.*"
                    . ", date_format(dt_nascimento, '%d/%m/%Y') dt_nascimento_fmt"
                    . ", e.no_razao_social"
                    . ", PN.no_nacionalidade"
                    . ", PN.no_pais"
                    . " from candidato c"
                    . " left join empresa e on e.nu_empresa = c.nu_empresa "
                    . " left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS"
                    . " where c.nome_completo like " . cBANCO::StringOk('%' . str_replace(' ', '%', $nome_completo) . '%')
                    . " and nu_candidato <> ".$nu_candidato
                    . " order by nome_completo, no_razao_social "
            ;
            $res = cAMBIENTE::$db_pdo->query($sql);
        } else {
            return array();
        }
        return $res->fetchAll(PDO::FETCH_ASSOC);
    }

    public function cursorHomonimosQtd($nome_completo, $nu_candidato = 0) {
        if ($nome_completo != '') {
            $sql = "select count(*) qtd"
                    . " from candidato c"
                    . " left join empresa e on e.nu_empresa = c.nu_empresa "
                    . " left join PAIS_NACIONALIDADE PN on C.CO_NACIONALIDADE = PN.CO_PAIS"
                    . " where c.nome_completo like " . cBANCO::StringOk('%' . str_replace(' ', '%', $nome_completo) . '%')
                    . " and nu_candidato <> ".$nu_candidato
            ;
            $res = cAMBIENTE::$db_pdo->query($sql);
            $rs = $res->fetch(PDO::FETCH_ASSOC);
            return $rs['qtd'];
        } else {
            return -1;
        }
    }

    public function associeTmpNovo($nu_candidato_tmp) {
        $sql = "insert into CANDIDATO (NOME_COMPLETO, NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME, NO_MAE, DT_NASCIMENTO,DT_CADASTRAMENTO,CO_USU_CADASTAMENTO,CO_USU_ULT_ALTERACAO, email_pessoal)"
                . " select NOME_COMPLETO, NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME, NO_MAE, DT_NASCIMENTO,now()," . cSESSAO::$mcd_usuario . "," . cSESSAO::$mcd_usuario . ", email_pessoal"
                . " from candidato_tmp where nu_candidato_tmp = " . $nu_candidato_tmp;
        $res = cAMBIENTE::$db_pdo->exec($sql);
        $nu_candidato = cAMBIENTE::$db_pdo->lastInsertId();
        $this->associeTmpExistente($nu_candidato, $nu_candidato_tmp);
        $rep = new cREPOSITORIO_ARQUIVO;
        $rep->associaTmpAoExistente($nu_candidato_tmp, $nu_candidato);
        $cand = new cCANDIDATO;
        $cand->mNU_CANDIDATO = $nu_candidato;
        return $cand;
    }

    public function envieNovaSenhaId($nu_candidato) {
        $cand = new cCANDIDATO;
        $ret = array();
        try {
            $cand->mNU_CANDIDATO = $nu_candidato;
            $cand->Recuperar();
            if ($cand->memail_pessoal != '') {
                $senha = $cand->gerarNovaSenha();
                $this->enviarEmailComSenha($cand);
                $ret['msg'] = "Nova senha enviada com sucesso";
            } else {
                $ret['msg'] = "Nao foi possivel enviar pois o candidato nao tem e-mail";
            }
        } catch (Exception $ex) {
            $ret['msg'] = $ex->getMessage();
        }
        return json_encode($ret);
    }

    public function envieNovaSenha($email_pessoal) {
        $cand = new cCANDIDATO;
        $cand->recuperesePeloEmail($email_pessoal);
        if ($cand->mNU_CANDIDATO > 0) {
            $senha = $cand->gerarNovaSenha();
            $this->enviarEmailComSenha($cand);
        } else {
            $cand = new cCANDIDATO_TMP;
            $cand->recuperesePeloEmail($email_pessoal);
            if ($cand->mnu_candidato_tmp > 0) {
                $senha = $cand->gerarNovaSenha();
                $this->enviarEmailComSenha($cand);
            } else {
                throw new exception("You don't have access to Dataweb.");
            }
        }
    }

    public function enviarEmailConvite(\cCANDIDATO_BASE $cand) {
        $msg = "<html>"
                . "<body>"
                . "A new password has been generated for your account on Mundivisas Dataweb"
                . "<br/>Login: " . $cand->memail_pessoal
                . "<br/>Password: " . $cand->mNO_SENHA
                . "<br/><br/>You are invited to log into Mundivisas site to fill in your personal information"
                . '<br/><a href="http://dataweb.mundivisas.com.br/">Access now</a>'
                . "</body>"
                . "</html>";
        $this->enviaEmailCandidato($cand, $msg, "Mundivisas-Dataweb: personal data request");
    }

    public function enviarEmailComSenha(\cCANDIDATO_BASE $cand) {
        $msg = "<html>"
                . "<body>"
                . "A new password has been generated for your account on Mundivisas Dataweb"
                . "<br/>Login: " . $cand->memail_pessoal
                . "<br/>Password: " . $cand->mNO_SENHA
                . '<br/><a href="http://dataweb.mundivisas.com.br/">Access now</a>'
                . "</body>"
                . "</html>";
        $this->enviaEmailCandidato($cand, $msg, "Mundivisas-Dataweb: new password");
    }

    public function enviaEmailCandidato(\cCANDIDATO_BASE $cand, $msg, $titulo) {
        $mail = new cEMAIL;
        $mail->AdicioneDestinatario($cand->memail_pessoal);
        $mail->setSubject($titulo);
        $mail->setMsg($msg);
        $mail->Envie();
    }

    public function responsaveisConcatenados($nome1, $nome2, $nome3, $nome4, $nome5) {
        $responsaveis = '';
        $virg = '';

        if ($nome1 != '') {
            $responsaveis .= $virg . $nome1;
            $virg = ", ";
        }
        if ($nome2 != '') {
            $responsaveis .= $virg . $nome2;
            $virg = ", ";
        }
        if ($nome3 != '') {
            $responsaveis .= $virg . $nome3;
            $virg = ", ";
        }
        if ($nome4 != '') {
            $responsaveis .= $virg . $nome4;
            $virg = ", ";
        }
        if ($nome5 != '') {
            $responsaveis .= $virg . $nome5;
            $virg = ", ";
        }
        return $responsaveis;
    }

    public function excluir($nu_candidato){
        $msg = '';

        $sql = "select count(*) qtd from taxapaga where nu_candidato = ".$nu_candidato;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if ($rs['qtd'] > 0){
            $msg .= "- existem ".$rs['qtd']." taxas pagas para esse candidato.";
        }

        $sql = "select solicita_visto.id_solicita_visto, nu_solicitacao"
            . "   from solicita_visto "
            . "   join autorizacao_candidato on autorizacao_candidato.id_solicita_visto = solicita_visto.id_solicita_visto"
            . "   where autorizacao_candidato.nu_candidato = ".$nu_candidato
            . "     and solicita_visto.stco_id = 3" ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        if (count($rs) > 0){
            $oss = '';
            $virg= '';
            foreach($rs as $reg){
                $oss.= $virg.$reg['nu_solicitacao'];
                $virg = ", ";
            }
            $msg .= "- existem ".count($rs)." (".$oss.") ordem(ns) de serviÃ§o faturadas para esse candidato. Remova o candidato dessas OSs antes de tentar excluÃ­-lo.";
        }
        if ($msg != ''){
            throw new Exception ($msg);
        } else {
            $sql = "select solicita_visto.id_solicita_visto, nu_solicitacao"
                . "      , (select count(*) from autorizacao_candidato ac where ac.id_solicita_visto = solicita_visto.id_solicita_visto and ac.nu_candidato <> autorizacao_candidato.nu_candidato) qtd_outros"
                . "   from solicita_visto "
                . "   join autorizacao_candidato on autorizacao_candidato.id_solicita_visto = solicita_visto.id_solicita_visto"
                . "   where autorizacao_candidato.nu_candidato = ".$nu_candidato
                . "     and solicita_visto.stco_id = 3" ;
            $res = cAMBIENTE::$db_pdo->query($sql);
            $oss = $res->fetchAll(PDO::FETCH_ASSOC);

            foreach($oss as $regos ){
                if ($regos['qtd_outros'] == 0 ){
                    $os = new cORDEMSERVICO;
                    $os->mID_SOLICITA_VISTO = $regos['id_solicita_visto'];
                    $os->Excluir();
                }
            }

            $sql = "select * from arquivos where nu_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->query($sql);
            $arquivos = $res->fetchAll(PDO::FETCH_ASSOC);
            foreach($arquivos as $arquivo){
                $arq = new cARQUIVO;
                $arq->CarreguePropriedadesPeloRecordset($arquivo);
                $arq->excluir();
            }

            $sql = "delete from processo_regcie where cd_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_prorrog where cd_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_emiscie where cd_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_cancel where cd_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_mte where cd_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from autorizacao_candidato where nu_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from taxapaga where nu_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from documento where nu_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from form_tvs where nu_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from candidato where nu_candidato = ".$nu_candidato;
            $res = cAMBIENTE::$db_pdo->exec($sql);
       }
    }

}
