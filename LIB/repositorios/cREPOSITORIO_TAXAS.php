<?php
class cREPOSITORIO_TAXAS{
    public function taxaPaga($id_solicita_visto, ctaxa $taxa, $data, $valor = null, $id_usuario = null){
        $taxapaga = new ctaxapaga();
        $taxapaga->mid_solicita_visto = $id_solicita_visto;
        $taxapaga->mid_taxa = $taxa->mid_taxa;
        $taxapaga->mdata_taxa = $data;
        if ($id_usuario == ''){
            $taxapaga->mid_usuario = cSESSAO::$mcd_usuario;
        } else {
            $taxapaga->mid_usuario = $id_usuario;
        }
        if ($valor != ''){
            $taxapaga->mvalor = $valor;
        } else {
            $taxapaga->mvalor = null;
        }
        $taxapaga->Salvar();
        return $taxapaga;
    }

    public function taxasDaOs($id_solicita_visto, $id_perfiltaxa){
        $sql = "select tp.id_taxapaga, t.id_taxa, t.descricao, tp.data_taxa, coalesce(tp.valor, t.valor_fixo) valor_taxa, u.nome"
             . " from perfiltaxa pt"
             . " join taxa t on t.id_perfiltaxa = pt.id_perfiltaxa"
             . " left join taxapaga tp on tp.id_taxa = t.id_taxa and tp.id_solicita_visto = ".$id_solicita_visto
             . " where pt.id_perfiltaxa = ".$id_perfiltaxa;
        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__.'.'.__FUNCTION__);
        return $rs;
    }
}
