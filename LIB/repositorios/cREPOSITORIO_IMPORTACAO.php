<?php

class cREPOSITORIO_IMPORTACAO
{
    /**
     * Retorna uma empresa a partir do nome informado
     * @param  [type] $nome_empresa [description]
     * @return [type]               [description]
     */
    public function recupera_empresa($nome_empresa){
        $sql = "select * from empresa where no_razao_social = '".$nome_empresa."'";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_ASSOC);
        if ($rs){
            if ($rs['NU_EMPRESA'] > 0){
                $emp = new cEMPRESA;
                $emp->CarreguePropriedadesPeloRecordset($rs, 'm');
                return $emp;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function recupera_embarcacao($nu_empresa, $nome_embarcacao, $criar = true){
        $sql = "select * from embarcacao_projeto where no_embarcacao_projeto like '%".trim($nome_embarcacao)."%' and nu_empresa = ".$nu_empresa;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $emp = new cEMBARCACAO_PROJETO;
        if ($rs){
            if ($rs['NU_EMBARCACAO_PROJETO'] > 0){
                $emp->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        } else {
            if ($criar){
                $emb = new cEMBARCACAO_PROJETO;
                $emb->mNU_EMPRESA = $nu_empresa;
                $emb->mNO_EMBARCACAO_PROJETO = $nome_embarcacao;
                $emb->Salve();
            }
        }
        return $emp;

    }

    public function recupera_pais_nacionalidade($nome_pais_nacionalidade){
        $sql = "select * from pais_nacionalidade where
                    no_nacionalidade_alternativo like '%".$nome_pais_nacionalidade."%'
                    or no_pais = '".$nome_pais_nacionalidade."'
                    or no_pais_em_ingles = '".$nome_pais_nacionalidade."'
                    or no_nacionalidade = '".$nome_pais_nacionalidade."'
                    or NO_NACIONALIDADE_EM_INGLES = '".$nome_pais_nacionalidade."'"
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $emp = new cPAIS_NACIONALIDADE;
        if ($rs){
            if ($rs['CO_PAIS'] > 0){
                $emp->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        return $emp;
    }

    public function recupera_funcao($nome){
        $sql = "select * from funcao_cargo where
                    no_funcao_alternativo like '%".$nome."%'
                    or no_funcao_em_ingles like '%".$nome."%'
                    or no_funcao = '".$nome."'"
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $emp = new cFUNCAO_CARGO;
        if ($rs){
            if ($rs['CO_FUNCAO'] > 0){
                $emp->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        return $emp;
    }

    public function recupera_servico($nome){
        $sql = "select * from servico where
                    no_servico_alternativo = '".$nome."'
                    or no_servico = '".$nome."'
                    or NO_SERVICO_RESUMIDO = '".$nome."'
                    or no_servico_em_ingles = '".$nome."'
                    or no_servico_resumido_em_ingles = '".$nome."'"
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $emp = new cSERVICO;
        if ($rs){
            if ($rs['NU_SERVICO'] > 0){
                $emp->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        return $emp;
    }


    public function recupera_profissao($nome){
        $sql = "select * from profissao where
                    no_profissao_alternativo like '%".$nome."%'
                    or no_profissao_em_ingles = '".$nome."'
                    or no_profissao = '".$nome."'"
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $emp = new cPROFISSAO;
        if ($rs){
            if ($rs['CO_PROFISSAO'] > 0){
                $emp->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        return $emp;
    }

    public function recupera_reparticao_consular($nome){
        $sql = "select * from reparticao_consular where
                    no_reparticao_consular_alternativo like '%".$nome."%'
                    or no_reparticao_consular_em_ingles like '%".$nome."%'
                    or no_reparticao_consular = '%".$nome."%'"
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $emp = new cREPARTICAO_CONSULAR;
        if ($rs){
            if ($rs['CO_REPARTICAO_CONSULAR'] > 0){
                $emp->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        return $emp;
    }

    public function recupera_status($nome){
        $sql = "select * from status_conclusao where
                    no_status_conclusao = '".$nome."'"
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $emp = new cstatus_conclusao;
        if ($rs){
            if ($rs['ID_STATUS_CONCLUSAO'] > 0){
                $emp->mid_status_conclusao = $rs['ID_STATUS_CONCLUSAO'];
            }
        }
        return $emp;
    }


    public function recuperar_candidato($nome, $nu_empresa, $criar = true){
        $sql = "select * from candidato where
                    nome_completo = '".$nome."'
                    and nu_empresa = '".$nu_empresa."'"
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $cand = false;
        if ($rs){
            if ($rs['NU_CANDIDATO'] > 0){
                $cand = new cCANDIDATO;
                $cand->CarreguePropriedadesPeloRecordset($rs, 'm');
            } else {
                $cand = false;
            }
        } else {
            if ($criar){
                $cand = new cCANDIDATO();
                $cand->QuebreNomeCompleto($nome);
                $cand->Criar($cand->mNO_PRIMEIRO_NOME, $cand->mNO_NOME_MEIO, $cand->mNO_ULTIMO_NOME, cSESSAO::$mcd_usuario);
                $cand->mNU_EMPRESA = $nu_empresa;
                $sql = "update candidato set nu_empresa = ".$nu_empresa." where nu_candidato = ".$cand->mNU_CANDIDATO;
                cAMBIENTE::$db_pdo->exec($sql);
            } else {
                $cand = false;
            }
        }
        return $cand;
    }

    public function atualiza_candidato($cand, $valores){
        foreach($valores as $propriedade => $valor){
            $cand->$propriedade = $valor;
        }
        $cand->salveTodosAtributos();
    }

    public function cria_processo_mte($cand, $valores){
        $sql = "select * from processo_mte where
                    cd_candidato = ".$cand->mNU_CANDIDATO."
                    and fl_visto_atual = 1"
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $mte = new cprocesso_mte;
        if ($rs){
            if ($rs['codigo'] > 0){
                $mte->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        foreach($valores as $propriedade => $valor){
            $mte->$propriedade = $valor;
        }

        $mte->mcd_candidato = $cand->mNU_CANDIDATO;
        $mte->mNU_EMPRESA = $cand->mNU_EMPRESA;
        $mte->mNU_EMBARCACAO_PROJETO = $cand->mNU_EMBARCACAO_PROJETO;
        $mte->mfl_visto_atual = 1;
        $mte->Salve();
        return $mte;
    }

    public function cria_registro($cand, $valores){
        $sql = "select * from processo_regcie where
                    cd_candidato = ".$cand->mNU_CANDIDATO."
                    and codigo_processo_mte = ".$cand->mcodigo_processo_mte_atual
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $processo = new cprocesso_regcie;
        if ($rs){
            if ($rs['codigo'] > 0){
                $processo->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        foreach($valores as $propriedade => $valor){
            $processo->$propriedade = $valor;
        }

        $processo->mcd_candidato = $cand->mNU_CANDIDATO;
        $processo->mcodigo_processo_mte = $cand->mcodigo_processo_mte_atual;
        $processo->Salve();
        return $processo;
    }

    public function cria_coleta_visto($cand, $valores){
        $sql = "select * from processo_coleta where
                    cd_candidato = ".$cand->mNU_CANDIDATO."
                    and codigo_processo_mte = ".$cand->mcodigo_processo_mte_atual
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $processo = new cprocesso_coleta;
        if ($rs){
            if ($rs['codigo'] > 0){
                $processo->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        foreach($valores as $propriedade => $valor){
            $processo->$propriedade = $valor;
        }

        $processo->mcd_candidato = $cand->mNU_CANDIDATO;
        $processo->mcodigo_processo_mte = $cand->mcodigo_processo_mte_atual;
        $processo->Salve();
        return $processo;
    }

    public function cria_coleta_cie($cand, $valores){
        $sql = "select * from processo_emiscie where
                    cd_candidato = ".$cand->mNU_CANDIDATO."
                    and codigo_processo_mte = ".$cand->mcodigo_processo_mte_atual
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $processo = new cprocesso_emiscie;
        if ($rs){
            if ($rs['codigo'] > 0){
                $processo->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        foreach($valores as $propriedade => $valor){
            $processo->$propriedade = $valor;
        }

        $processo->mcd_candidato = $cand->mNU_CANDIDATO;
        $processo->mcodigo_processo_mte = $cand->mcodigo_processo_mte_atual;
        $processo->Salve();
        return $processo;
    }

    public function cria_prorrogacao($cand, $valores){
        $sql = "select * from processo_prorrog where
                    cd_candidato = ".$cand->mNU_CANDIDATO."
                    and codigo_processo_mte = ".$cand->mcodigo_processo_mte_atual
                    ;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);
        $processo = new cprocesso_prorrog;
        if ($rs){
            if ($rs['codigo'] > 0){
                $processo->CarreguePropriedadesPeloRecordset($rs, 'm');
            }
        }
        foreach($valores as $propriedade => $valor){
            $processo->$propriedade = $valor;
        }

        $processo->mcd_candidato = $cand->mNU_CANDIDATO;
        $processo->mcodigo_processo_mte = $cand->mcodigo_processo_mte_atual;
        $processo->Salve();
        return $processo;

    }

}
