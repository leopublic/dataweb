<?php
class cREPOSITORIO_FATURAMENTO{

    public function criarFaturamentoTemporario($cd_usuario, $array_ids){
        $sql = "update solicita_visto set cd_usuario_faturador_temp = null where cd_usuario_faturador_temp = ".$cd_usuario;
        cAMBIENTE::$db_pdo->exec($sql);

        $ids = implode(",", $array_ids);

        $sql = "update solicita_visto set cd_usuario_faturador_temp = ".$cd_usuario." where id_solicita_visto in (".$ids.")";
        cAMBIENTE::$db_pdo->exec($sql);
    }
    /**
     * Fatura associa as OS temporariamente faturadas a um determinado resumod de cobrança
     * @param type $cd_usuario
     * @param type $resc_id
     * @param type $obsFaturamento
     */
    public function faturarTemporarias($cd_usuario, $resc_id, $obsFaturamento = '', $controller = 'cREPOSITORIO_FATURAMENTO', $metodo= 'faturarTemporarias'){
        $sql = " update solicita_visto "
                . " set resc_id = ".$resc_id.""
                . "   , stco_id = 3"
                . "   , soli_dt_alteracao = now()"
                . "   , controller = 'cREPOSITORIO_FATURAMENTO'"
                . "   , metodo= 'faturarTemporarias'"
                . "   , cd_usuario_alteracao = '".$cd_usuario."'"
                . "   , cd_usuario_faturador_temp = null"
                . "   , controller = ".cBANCO::StringOk($controller)
                . "   , metodo = ".cBANCO::StringOk($metodo)
                . " where cd_usuario_faturador_temp =" . $cd_usuario;
        cAMBIENTE::$db_pdo->exec($sql);

        $sql = " update solicita_visto "
                . "   set soli_vl_cobrado = 0"
                . "   , cd_usuario_alteracao = '".$cd_usuario."'"
                . "   , controller = ".cBANCO::StringOk($controller)
                . "   , metodo = ".cBANCO::StringOk($metodo)
                . "   where tppr_id = 1"
                . "   and solicita_visto.resc_id = ".$resc_id;
        cAMBIENTE::$db_pdo->exec($sql);
        $sql = " update solicita_visto, empresa, tabela_precos, preco "
                . "   set soli_vl_cobrado = preco.prec_vl_conceito"
                . "   , cd_usuario_alteracao = '".$cd_usuario."'"
                . "   , controller = ".cBANCO::StringOk($controller)
                . "   , metodo = ".cBANCO::StringOk($metodo)
                . "   where solicita_visto.tppr_id = 2"
                . "   and empresa.nu_empresa = solicita_visto.nu_empresa"
                . "   and tabela_precos.tbpc_id= empresa.tbpc_id"
                . "   and preco.nu_servico = solicita_visto.NU_SERVICO"
                . "   and preco.tbpc_id = tabela_precos.tbpc_id"
                . "   and solicita_visto.resc_id = ".$resc_id;
        cAMBIENTE::$db_pdo->exec($sql);
        $sql = " update solicita_visto, empresa, tabela_precos, preco "
                . "   set soli_vl_cobrado = preco.prec_vl_pacote"
                . "   , cd_usuario_alteracao = '".$cd_usuario."'"
                . "   , controller = ".cBANCO::StringOk($controller)
                . "   , metodo = ".cBANCO::StringOk($metodo)
                . "   where solicita_visto.tppr_id = 3"
                . "   and empresa.nu_empresa = solicita_visto.nu_empresa"
                . "   and tabela_precos.tbpc_id= empresa.tbpc_id"
                . "   and preco.nu_servico = solicita_visto.NU_SERVICO"
                . "   and preco.tbpc_id = tabela_precos.tbpc_id"
                . "   and solicita_visto.resc_id = ".$resc_id;
        cAMBIENTE::$db_pdo->exec($sql);

        if ($obsFaturamento != '') {
            $sql = "update solicita_visto"
                    ." set soli_tx_obs_cobranca = concat(coalesce(concat(soli_tx_obs_cobranca, '<br/>'), ''), " . cBANCO::StringOk($obsFaturamento) . ") "
                    . "   , cd_usuario_alteracao = '".$cd_usuario."'"
                    . "   , controller = ".cBANCO::StringOk($controller)
                    . "   , metodo = ".cBANCO::StringOk($metodo)
                    . " where resc_id = ".$resc_id;
        }
    }
}
