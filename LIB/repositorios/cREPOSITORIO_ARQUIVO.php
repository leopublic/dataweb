<?php

class cREPOSITORIO_ARQUIVO {

    public function consisteArquivoEnviado($files, $nomeCampoArquivo) {
        if (!isset($files[$nomeCampoArquivo])) {
            throw new Exception("Arquivo não informado");
        }
        switch ($files[$nomeCampoArquivo]['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new Exception("O arquivo informado excede o tamanho máximo permitido");
                break;
            case UPLOAD_ERR_PARTIAL:
                throw new Exception("Somente uma parte do arquivo foi enviada ao servidor. Por favor tente novamente.");
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new Exception("Arquivo não informado");
                break;
            case UPLOAD_ERR_CANT_WRITE:
                throw new Exception("Ocorreu um erro no recebimento do arquivo (ERR_CANT_WRITE). Por favor tente novamente.");
                break;
            case UPLOAD_ERR_EXTENSION:
                throw new Exception("A extensão do arquivo informado não é permitida no sistema (ERR_EXTENSION).");
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                throw new Exception("Ocorreu um erro no recebimento do arquivo (NO_TMP_DIR). Por favor tente novamente.");
                break;
        }
    }

    public function novoArquivoDoCandidatoPeloCandidato($nu_candidato, $pInputFile, $nu_candidato_tmp = '') {
        $arq = new cARQUIVO();
        $arq->mNO_ARQ_ORIGINAL = str_replace("'", "", $pInputFile['name']);
        if (intval($nu_candidato) > 0) {
            $cand = new cCANDIDATO();
            $cand->Recuperar($nu_candidato);
            $arq->mNU_CANDIDATO = $nu_candidato;
        } else {
            $cand = new cCANDIDATO_TMP;
            $cand->RecuperarPeloTmp($nu_candidato_tmp);
            $arq->mnu_candidato_tmp = $nu_candidato_tmp;
        }
        $arq->mNU_EMPRESA = $cand->mNU_EMPRESA;
        $arq->mFL_ADICIONADO_PELO_CANDIDATO = "1";
        $arq->mNO_ARQUIVO = '(temp)';
        $arq->mNU_TAMANHO = $pInputFile['size'];
        $arq->salvar();
        $arq->mNO_ARQUIVO = $arq->nomeCalculado();
        $arq->salvar();

        $arq->armazenaArquivo($pInputFile);
    }

    public function salvaArquivo($nu_candidato, $nu_candidato_tmp, $no_arq_original, $tamanho, $tp_arquivo){
        $arq = new cARQUIVO();
        $arq->mNO_ARQ_ORIGINAL = str_replace("'", "", $no_arq_original);
        if (intval($nu_candidato) > 0) {
            $cand = new cCANDIDATO();
            $cand->Recuperar($nu_candidato);
            $arq->mNU_CANDIDATO = $nu_candidato;
        } else {
            $cand = new cCANDIDATO_TMP;
            $cand->RecuperarPeloTmp($nu_candidato_tmp);
            $arq->mnu_candidato_tmp = $nu_candidato_tmp;
        }
        $arq->mNU_EMPRESA = $cand->mNU_EMPRESA;
        $arq->mFL_ADICIONADO_PELO_CANDIDATO = "1";
        $arq->mNO_ARQUIVO = '(temp)';
        $arq->mNU_TAMANHO = $tamanho;
        $arq->mTP_ARQUIVO = $tp_arquivo;
        $arq->salvar();
        $arq->mNO_ARQUIVO = $arq->nomeCalculado();
        $arq->salvar();
        return $arq;
    }

    public function novoArquivoDoCandidatoPeloOperacional($nu_candidato, $pInputFile, $pTipoArquivo) {
        $arq = new cARQUIVO();
        $arq->mNO_ARQ_ORIGINAL = preg_replace("/[^.a-zA-Z0-9]/", "", $pInputFile['name']);
        $cand = new cCANDIDATO();
        $cand->Recuperar($nu_candidato);
        $arq->mNU_EMPRESA = $cand->mNU_EMPRESA;
        $arq->mNU_CANDIDATO = $nu_candidato;
        $arq->mTP_ARQUIVO = $pTipoArquivo;
        $arq->mFL_ADICIONADO_PELO_CANDIDATO = "0";
        $arq->mNO_ARQUIVO = '(temp)';
        $arq->mNU_ADMIN = cSESSAO::$mcd_usuario;
        $arq->mNU_TAMANHO = $pInputFile['size'];
        $arq->salvar();
        $arq->mNO_ARQUIVO = $arq->nomeCalculado();
        $arq->salvar();
        $arq->armazenaArquivo($pInputFile);
    }

    public function armazenaDr(cCANDIDATO_TMP $cand, $pInputFile) {
        $arq = new cARQUIVO();
        $arq->mNO_ARQ_ORIGINAL = 'dr.pdf';
        $arq->mNU_EMPRESA = $cand->mNU_EMPRESA;
        $arq->mnu_candidato_tmp = $cand->mnu_candidato_tmp;
        $arq->mTP_ARQUIVO = 18;
        $arq->mFL_ADICIONADO_PELO_CANDIDATO = "0";
        $arq->mNO_ARQUIVO = '(temp)';
        $arq->mNU_ADMIN = cSESSAO::$mcd_usuario;
        $arq->mNU_TAMANHO = $pInputFile['size'];
        $arq->salvar();
        $arq->mNO_ARQUIVO = $arq->nomeCalculado();
        $arq->salvar();

        $arq->armazenaArquivo($pInputFile);
    }

    public function novoArquivoDaOs($nu_candidato, $pInputFile, $pTipoArquivo, $id_solicita_visto, $cd_usuario) {
        if ($pTipoArquivo == '' || $pTipoArquivo == '0') {
            throw new Exception("Informe o tipo do arquivo");
            exit();
        }
        $arq = new cARQUIVO();

        if ($id_solicita_visto > 0) {
            $arq->mID_SOLICITA_VISTO = $id_solicita_visto;
            $os = new cORDEMSERVICO;
            $os->Recuperar($id_solicita_visto);
            $arq->mNU_EMPRESA = $os->mNU_EMPRESA;
        } else {
            $arq->mID_SOLICITA_VISTO = 0;
            $arq->mcd_usuario_os_incompleta = $cd_usuario;
            $arq->mNU_EMPRESA = 0;
        }

        $arq->mNO_ARQ_ORIGINAL = str_replace("'", "", $pInputFile['name']);
        $arq->mNU_CANDIDATO = $nu_candidato;
        $arq->mTP_ARQUIVO = $pTipoArquivo;
        $arq->mFL_ADICIONADO_PELO_CANDIDATO = "0";
        $arq->mNO_ARQUIVO = '(temp)';
        $arq->mNU_ADMIN = $cd_usuario;
        $arq->salvar();
        $arq->mNO_ARQUIVO = $arq->nomeCalculado();
        $arq->salvar();
        $arq->armazenaArquivo($pInputFile);
    }

    public function remover($nu_sequencial) {
        $arq = new cARQUIVO();
        $arq->RecuperePeloId($nu_sequencial);
        $arq->removerArquivo();
        $arq->excluir();
    }

    public function verificaDiretorio($caminho, &$i, &$erro, &$tamanho_total) {
        $dir = opendir($caminho);

        while (($file = readdir($dir)) !== false) {
            if ($file[0] == '.') {
                continue;
            }
            $fullpath = $caminho . '/' . $file;
            if (is_dir($fullpath)) {
                $this->verificaDiretorio($fullpath, $i, $erro, $tamanho_total);
            } else {
                $i++;
                if (strpos($file, '_') > 0) {
                    $tokens = explode("_", $file);
                    $nome = $tokens[1];
                } else {
                    $nome = $file;
                }
                if (strpos($nome, '.')) {
                    $tokens = explode(".", $nome);
                    $nome = $tokens[0];
                }
                $posSQ = strpos($nome, 'SQ');
                if ($posSQ > 0) {
                    $nomex = substr($nome, (strlen($nome) - $posSQ - 2 ) * -1);
                } else {
                    $nomex = $nome;
                }
                if (is_numeric($nomex)) {
                    $sql = "select * from arquivos where nu_sequencial=" . $nomex;
                    $res = cAMBIENTE::$db_pdo->query($sql);
                    $rs = $res->fetch();
                    if ($rs['NU_SEQUENCIAL'] > 0) {

                    } else {
                        $erro++;
                        print '<br/> Arquivo inexistente ' . $caminho . '/' . $file . " ID considerado: " . $nomex . " tamanho:" . filesize($fullpath);
                        $tamanho_total = $tamanho_total + filesize($fullpath);
                    }
                } else {
                    print '<br/> ID do arquivo não recuperado ' . $caminho . '/' . $file . " nome=>" . $nome . " nomex=>" . $nomex . " tamanho=" . strlen($nome) . " pos=" . $posSQ;
                }
            }
        }
    }

    public function corrigeTamanho() {
        $sql = "select * from arquivos";
        $res = cAMBIENTE::$db_pdo->query($sql);
        while ($rs = $res->fetch()) {
            $arq = new cARQUIVO();
            $arq->CarreguePropriedadesPeloRecordset($rs);
            if ($arq->mNU_TAMANHO == '') {
                if ($arq->existe()) {
                    $arq->atualizaTamanho();
                }
            }
        }
    }

    public function todosArquivosPorEmpresa() {
        $sql = "select a.nu_empresa, no_razao_social"
                . ", count(*) qtd_total"
                . ",  sum((nu_tamanho/1024)) tamanho_total"
                . ", sum((nu_tamanho/1024)) / count(*) media_total from arquivos a"
                . " left join empresa e on e.nu_empresa = a.nu_empresa"
                . " where nu_tamanho > 0"
                . " group by a.nu_empresa, no_razao_social"
                . " order by sum(nu_tamanho)";
        $res = cAMBIENTE::$db_pdo->query($sql);
        return $res->fetchAll(PDO::FETCH_ASSOC);
    }

    public function arquivosNovos() {
        $sql = "select a.*, no_razao_social, nu_embarcacao_projeto, nome_completo"
                . " from arquivos a"
                . " join candidato c on c.nu_candidato = a.nu_candidato"
                . " left join empresa e on e.nu_empresa = c.nu_empresa "
                . " left join embarcacao_projeto ep on ep.nu_empresa = c.nu_empresa and ep.nu_embarcacao_projeto = c.nu_embarcacao_projeto"
                . " where tp_arquivo is null";
    }

    public function downloadParaCliente($nu_sequencial) {
        $arq = new cARQUIVO;
        $arq->mNU_SEQUENCIAL = $nu_sequencial;
        $arq->RecuperePeloId();
        $rep = new cREPOSITORIO_CANDIDATO;
        $cand = $rep->candidatoParaCliente($arq->mNU_CANDIDATO, $arq->mnu_candidato_tmp);
        $nomeArq = str_replace(",", "", str_replace(" ", "_", $arq->mNO_ARQ_ORIGINAL));

        if ($cand->mNU_EMPRESA == cSESSAO::$mcd_empresa) {
            if ($arq->mNU_TAMANHO > 0) {
                header('Content-Length: ' . $arq->mNU_TAMANHO);
            }

            header('Content-Disposition: attachment; filename="' . $nomeArq . '"');
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Description: File Transfer');
            header('Content-Type: application/force-download');
            readfile($arq->caminhoRealInstancia());
            exit;
        } else {
            print '<script>alert("You are trying to access a file that you don\'t have permission to");window.close();</script>';
        }
    }

    public function download($nu_sequencial) {
        $arq = new cARQUIVO;
        $arq->mNU_SEQUENCIAL = $nu_sequencial;
        $arq->RecuperePeloId();
        $rep = new cREPOSITORIO_CANDIDATO;
        $nomeArq = str_replace(",", "", str_replace(" ", "_", $arq->mNO_ARQ_ORIGINAL));

        if ($arq->mNU_TAMANHO > 0) {
            header('Content-Length: ' . $arq->mNU_TAMANHO);
        }

        header('Content-Disposition: attachment; filename="' . $nomeArq . '"');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header('Content-Type: application/force-download');
        readfile($arq->caminhoRealInstancia());
        exit;
    }

    public function associaTmpAoExistente($nu_candidato_tmp, $nu_candidato) {
        $sql = "update arquivos set nu_candidato = " . $nu_candidato . " where nu_candidato_tmp = " . $nu_candidato_tmp;
        cAMBIENTE::$db_pdo->exec($sql);
    }

    public function migraArquivos($nu_candidato_tmp, $nu_candidato){
        $sql = "select * from arquivos where nu_candidato_tmp =".$nu_candidato_tmp;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        foreach($rs as $arq){
            $caminhoAtual = cARQUIVO::CaminhoReal('', $arq['NO_ARQUIVO'], '', '', $arq['nu_candidato_tmp']);
            $arqNovo = new cARQUIVO;
            $arqNovo->mNU_SEQUENCIAL = $arq['NU_SEQUENCIAL'];
            $arqNovo->mFL_ADICIONADO_PELO_CANDIDATO = 1;
            $arqNovo->mNO_ARQ_ORIGINAL = $arq['NO_ARQ_ORIGINAL'];
            $arqNovo->mNU_CANDIDATO = $nu_candidato;
            $NO_ARQUIVO = $arqNovo->nomeCalculado();
            $caminhoNovo = cARQUIVO::CaminhoReal($nu_candidato, $NO_ARQUIVO, $arq['NU_EMPRESA'], '');
            $sql = "update arquivos set nu_candidato = ".$nu_candidato.", no_arquivo = '".$NO_ARQUIVO."', nu_candidato_tmp = null where nu_sequencial=".$arq['NU_SEQUENCIAL'];
            error_log($sql);
            cAMBIENTE::$db_pdo->exec($sql);
            if (file_exists($caminhoAtual)){
                rename($caminhoAtual, $caminhoNovo);
            }
        }
    }

    public function migraArquivosEntreCandidatos($nu_candidato_origem, $nu_candidato_destino){
        $sql = "select * from arquivos where nu_candidato =".$nu_candidato_origem;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        foreach($rs as $arq){
            $caminhoAtual = cARQUIVO::CaminhoReal($arq['NU_CANDIDATO'], $arq['NO_ARQUIVO'], $arq['NU_EMPRESA'], $arq['ID_SOLICITA_VISTO']);
            $arqNovo = new cARQUIVO;
            $arqNovo->mNU_SEQUENCIAL = $arq['NU_SEQUENCIAL'];
            $arqNovo->mNO_ARQ_ORIGINAL = $arq['NO_ARQ_ORIGINAL'];
            $arqNovo->mNU_CANDIDATO = $nu_candidato_destino;
            $NO_ARQUIVO = $arqNovo->nomeCalculado();
            $caminhoNovo = cARQUIVO::CaminhoReal($nu_candidato_destino, $NO_ARQUIVO, $arq['NU_EMPRESA'], '');
            $sql = "update arquivos set nu_candidato = ".$nu_candidato_destino.", id_solicita_visto = null, no_arquivo = '".$NO_ARQUIVO."', nu_candidato_tmp = null where nu_sequencial=".$arq['NU_SEQUENCIAL'];
            cAMBIENTE::$db_pdo->exec($sql);
            if (file_exists($caminhoAtual)){
                rename($caminhoAtual, $caminhoNovo);
            }
        }
    }
    /**
     * Exclui todos os arquivos de um determinado tipo
     * @param  [type] $nu_candidato_tmp [description]
     * @param  [type] $id_tipo_arquivo  [description]
     * @return [type]                   [description]
     */
    public function excluiArquivoTemporarioExistente($nu_candidato_tmp, $id_tipo_arquivo){
        $sql = "select * from arquivos where nu_candidato_tmp = ".$nu_candidato_tmp." and tp_arquivo = ".$id_tipo_arquivo;
        $res = cAMBIENTE::ConectaQuery($sql);
        $regs = $res->fetchAll();
        if (count($regs) > 0){
            foreach($regs as $rs){
                $arq = new cARQUIVO;
                $arq->CarreguePropriedadesPeloRecordset($rs, "m");
                $arq->excluir();
            }
        }

    }
    /**
     * Retorna o arquivo mais recente do tipo informado
     * @param  [type] $nu_candidato_tmp [description]
     * @param  [type] $id_tipo_arquivo  [description]
     * @return [type]                   [description]
     */
    public static function recuperaArquivoTemporarioDoTipo($nu_candidato_tmp, $id_tipo_arquivo){
        $sql = "select * from arquivos where nu_candidato_tmp = ".$nu_candidato_tmp." and tp_arquivo = ".$id_tipo_arquivo. " order by nu_sequencial desc";
        $res = cAMBIENTE::$db_pdo->query($sql);
        if ($rs = $res->fetch(PDO::FETCH_ASSOC)){
            $arq = new cARQUIVO;
            $arq->CarreguePropriedadesPeloRecordset($rs, "m");
        } else {
            $arq = null;
        }
        return $arq;
    }

    public function arquivos_da_os($id_solicita_visto){
        $sql = "select arquivos.*, coalesce(NO_TIPO_ARQUIVO, '(indefinido)') NO_TIPO_ARQUIVO from arquivos, tipo_arquivo where tipo_arquivo.ID_TIPO_ARQUIVO = arquivos.TP_ARQUIVO and id_solicita_visto = ".$id_solicita_visto;
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        return $rs;
    }
}
