<?php

class cREPOSITORIO_USUARIO {

    /** 
     * Retorna todos os usuários do sistema
     * @return array
     */
    public static function listaFuncionarios() {
        $sql = "select cd_usuario, nome "
                . " from usuarios, usuario_perfil "
                . " where eh_funcionario = 'S' "
                . " and flagativo = 'Y'"
                . " and usuario_perfil.cd_perfil = usuarios.cd_perfil"
                . " order by nome ";
        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $ret = $rs->fetchAll();
        return $ret;
    }

    public function alterarSenha($cd_usuario, $senhaAtual, $novaSenha, $novaSenhaRep){
        if ($senhaAtual == '') {
            if (cHTTP::getLang() == 'pt_br') {
                $msg = 'Por favor informe a senha atual.';
            } else {
                $msg = 'Please inform your current password.';
            }
            throw new Exception($msg);
        } else {
            $usuario = new cusuarios();
            $usuario->cd_usuario = cSESSAO::$mcd_usuario;
            $usuario->RecuperePeloId();
            if ($usuario->SenhaValida($senhaAtual)) {
                $usuario->AltereSenha($novaSenha, $novaSenhaRep);
                if (cHTTP::getLang() == 'pt_br') {
                    $msg = 'Senha alterada com sucesso!';
                } else {
                    $msg = 'Password changed sucessfully!';
                }
                return $msg;
            } else {
                if (cHTTP::getLang() == 'pt_br') {
                    $msg = 'Senha atual não confere.';
                } else {
                    $msg = "Current password don't match. Please verify and try again.";
                }
                throw new Exception ($msg);
            }
        }

    }

    public function novaSenhaAleatoria($tamanho = 8){
        $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $novasenha = substr(str_shuffle($letters), 0, $tamanho);
    }
}
