<?php

class cREPOSITORIO_CONVIDADO {

    public function login($conv_id, $chave) {
        $conv = new cconvidado();
        $conv->mconv_id = $conv_id;
        $conv->RecuperePeloId();
        if ($conv->mchave == $chave) {
            if ($conv->mdias > 3){
                cNOTIFICACAO::singleton('Your access expired.', cNOTIFICACAO::TM_ERROR, "Attention");
                return false;
            } else {
                return $conv;
            }
        } else {
            cNOTIFICACAO::singleton('Login failed.', cNOTIFICACAO::TM_ERROR, "Attention");
            return false;
        }
    }

    public function preenchimentos($conv_id) {
        $sql = "select nu_candidato_tmp, nome_completo, id_status_edicao"
                . " from candidato_tmp "
                . " where conv_id = " . $conv_id . ""
                . " order by nome_completo";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        return $rs;
    }

    public function preenchimentosPendentes($nu_empresa) {
        $sql = "select ctmp.nu_candidato_tmp
                     , coalesce(cnvu.nome, cnv.nome, '(you)') nome
                     , coalesce(cnvu.email, cnv.email, '--') email
                     , ep.no_embarcacao_projeto
                     , coalesce(su.no_servico_resumido_em_ingles,s.no_servico_resumido_em_ingles, ssu.no_servico_resumido_em_ingles, '--') no_servico_resumido_em_ingles
                     , nome_completo"
                . " from candidato_tmp ctmp"
                . " join empresa e on e.nu_empresa = ctmp.NU_EMPRESA"
                . " left join convidado cnv on cnv.conv_id = ctmp.conv_id"
                . " left join convidado cnvu on cnvu.nu_candidato_tmp = ctmp.nu_candidato_tmp"
                . " left join usuarios u on u.cd_usuario = ctmp.CO_USU_CADASTAMENTO"
                . " left join servico s on s.nu_servico = cnv.nu_servico"
                . " left join servico su on su.nu_servico = cnvu.nu_servico"
                . " left join servico ssu on ssu.nu_servico = ctmp.nu_servico"
                . " left join embarcacao_projeto ep on ep.nu_empresa = ctmp.NU_EMPRESA and ep.NU_EMBARCACAO_PROJETO = ctmp.NU_EMBARCACAO_PROJETO"
                . " where ctmp.nu_empresa = " . $nu_empresa
                . "   and coalesce(ctmp.id_status_edicao, 0) <> 7"
                . " order by ctmp.nome_completo";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        error_log(count($rs));
        return $rs;
    }

    public function prorrogacoes($nu_empresa, $where) {
        $sql = "select c.nu_candidato
                     , c.nu_passaporte
                     , ep.no_embarcacao_projeto
                     , pp.nu_servico nu_servico_pp
                     , coalesce(s.no_servico_resumido_em_ingles, '--') no_servico_resumido_em_ingles
                     , nome_completo
                     , pp.codigo
                     , pp.nu_protocolo
                     , pn.no_nacionalidade_em_ingles
                     , date_format(pp.dt_requerimento, '%d/%m/%Y') dt_requerimento_fmt"
                . " from processo_prorrog pp"
                . " join candidato c on c.nu_candidato = pp.cd_candidato"
                . " join processo_mte pm on pm.codigo = pp.codigo_processo_mte "
                . " join servico spp on spp.nu_servico = pp.nu_servico"
                . " left join embarcacao_projeto ep on ep.nu_empresa = c.NU_EMPRESA and ep.NU_EMBARCACAO_PROJETO = c.NU_EMBARCACAO_PROJETO"
                . " left join pais_nacionalidade pn on pn.co_pais = c.co_nacionalidade"
                . " left join servico s on s.nu_servico = pm.nu_servico"
                . " where c.nu_empresa = " . $nu_empresa
                . " and coalesce(c.fl_inativo, 0) = 0"
                . " and spp.fl_certidao_tramite = 1 "
                . " and pp.codigo in (select max(codigo) from processo_prorrog, servico where servico.nu_servico = processo_prorrog.nu_servico and servico.fl_certidao_tramite = 1 and cd_candidato = pp.cd_candidato) "
                . $where
                . " order by c.nome_completo";
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        foreach($rs as &$reg){
            $reg['nu_protocolo'] = preg_replace('/\D/', '', $reg['nu_protocolo']);
            $nu = $reg['nu_protocolo'];
            $reg['nu_protocolo'] = substr($nu, 0,5).'.'.substr($nu,5,6).'/'.substr($nu,11,4).'-'.substr($nu,15,2);
        }
        return $rs;
    }

    public function cancelamentos($nu_empresa, $where) {
        $sql = "select c.nu_candidato
                     , c.nu_passaporte
                     , ep.no_embarcacao_projeto
                     , pc.nu_servico nu_servico_pc
                     , coalesce(s.no_servico_resumido_em_ingles, '--') no_servico_resumido_em_ingles
                     , nome_completo
                     , pc.codigo
                     , pc.nu_processo
                     , pn.no_nacionalidade_em_ingles
                     , date_format(pc.dt_processo, '%d/%m/%Y') dt_processo_fmt"
                . " from processo_cancel pc"
                . " join candidato c on c.nu_candidato = pc.cd_candidato"
                . " join processo_mte pm on pm.codigo = pc.codigo_processo_mte "
                . " join servico spc on spc.nu_servico = pc.nu_servico"
                . " left join embarcacao_projeto ep on ep.nu_empresa = c.NU_EMPRESA and ep.NU_EMBARCACAO_PROJETO = c.NU_EMBARCACAO_PROJETO"
                . " left join pais_nacionalidade pn on pn.co_pais = c.co_nacionalidade"
                . " left join servico s on s.nu_servico = pm.nu_servico"
                . " where c.nu_empresa = " . $nu_empresa
                . " and coalesce(c.fl_inativo, 0) = 0"
                . " and pc.codigo in (select max(codigo) from processo_cancel, servico where servico.nu_servico = processo_cancel.nu_servico and  cd_candidato = pc.cd_candidato) "
                . $where
                . " order by c.nome_completo";
        error_log($sql);
        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);
        foreach($rs as &$reg){
            $reg['nu_protocolo'] = preg_replace('/\D/', '', $reg['nu_protocolo']);
            $nu = $reg['nu_protocolo'];
            $reg['nu_protocolo'] = substr($nu, 0,5).'.'.substr($nu,5,6).'/'.substr($nu,11,4).'-'.substr($nu,15,2);
        }
        return $rs;
    }

    public function adicionarPreenchimentos(cconvidado $conv, $nome_completos) {
        $cands = array();
        foreach ($nome_completos as $nome_completo) {
            $cand = new cCANDIDATO_TMP;
            $cand->adicionaPeloConvidado($conv, $nome_completo);
            $cands[] = $cand;
        }
        return $cands;
    }

    public function criar($email, $nome, $nu_empresa, $nu_embarcacao_projeto, $nu_servico) {
        $msg = '';
        $sep = '';
        if ($nu_embarcacao_projeto == '') {
            $msg .= $sep . 'Please inform the Vessel/project';
            $sep = '<br/>';
        }
        if ($nu_embarcacao_projeto == '') {
            $msg .= 'Please inform the service requested';
            $sep = '<br/>';
        }
        if ($msg != '') {
            throw new Exception($msg);
        }
        $conv = new cconvidado();
        if (!$conv->recuperesePeloEmail($email)) {
            $conv->memail = $email;
        } else {
            if ($conv->mnu_candidato_tmp !=  ''){
                throw new Exception("We can't send an invitation for this user because it already has a Data Request pending" );
            }
        }
        $conv->mnome = $nome;
        $conv->mnu_empresa = $nu_empresa;
        $conv->mnu_embarcacao_projeto = $nu_embarcacao_projeto;
        $conv->mnu_servico = $nu_servico;
        $conv->mdt_inclusao = date('d/m/Y H:i:s');
        $conv->Salvar();
        return $conv;
    }

    public function envieNovaSenhaId(cconvidado $conv) {
        $this->enviarEmailComSenha($conv);
    }

    public function envieNovaSenha($email_pessoal) {
        $cand = new cCANDIDATO;
        $cand->recuperesePeloEmail($email_pessoal);
        if ($cand->mNU_CANDIDATO > 0) {
            $senha = $cand->gerarNovaSenha();
            $this->enviarEmailComSenha($cand);
        } else {
            $cand = new cCANDIDATO_TMP;
            $cand->recuperesePeloEmail($email_pessoal);
            if ($cand->mnu_candidato_tmp > 0) {
                $senha = $cand->gerarNovaSenha();
                $this->enviarEmailComSenha($cand);
            } else {
                throw new exception("You don't have access to Dataweb.");
            }
        }
    }

    public function enviarEmailConvite(\cconvidado $conv) {
        $msg = "<html>"
                . "<body>"
                . "You are invited to log into Mundivisas site to fill in personal information for some VISA procedures"
                . '<br/><a href="'.cAMBIENTE::$m_dominio.'/paginas/carregueConvidado.php?metodo=getLogin&conv_id=' . $conv->mconv_id . '&chave=' . $conv->mchave .'">Access now</a>'
                . "</body>"
                . "</html>";
        $this->enviaEmailCandidato($conv, $msg, "Mundivisas-Dataweb: personal data request");
    }

    public function enviarEmailComSenha(\cCANDIDATO_BASE $cand) {
        $msg = "<html>"
                . "<body>"
                . "A new password has been generated for your account on Mundivisas Dataweb"
                . "<br/>Login: " . $cand->memail_pessoal
                . "<br/>Password: " . $cand->mNO_SENHA
                . '<br/><a href="'.cAMBIENTE::$m_dominio.'/">Access now</a>'
                . "</body>"
                . "</html>";
        $this->enviaEmailCandidato($cand, $msg, "Mundivisas-Dataweb: new password");
    }

    public function enviaEmailCandidato(\cconvidado $conv, $msg, $titulo) {
        $mail = new cEMAIL;
        $mail->AdicioneDestinatario($conv->memail);
        $mail->setSubject($titulo);
        $mail->setMsg($msg);
        $mail->Envie();
    }

}
