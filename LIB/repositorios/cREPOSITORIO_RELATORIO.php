<?php
class cREPOSITORIO_RELATORIO 
{
	public function campos(){

		$campos= array();
		$grupo = array(
		          array('campo'=> 'NO_RAZAO_SOCIAL', 'label' =>'Empresa')
		        , array('campo' => 'NO_EMBARCACAO_PROJETO', 'label' => 'Embarcação/Projeto')
		        , array('campo' => 'empt_tx_descricao', 'label' => 'Empresa terceirizada')
		        , array('campo' => 'NU_ORDEM_CREWLIST', 'label' => 'Ordem crewlist')
		        , array('campo' => 'FL_EMBARCADO', 'label' => 'Embarcado', 'class' => 'cen')
		        , array('campo' => 'NOME_COMPLETO', 'label' => 'Nome', 'obrigatorio'=> true)
		        , array('campo' => 'situacao_cadastral', 'label' => 'Situação', 'class' => 'cen')
		        , array('campo' => 'revisado', 'label' => 'Revisado?', 'class' => 'cen')
		        , array('campo' => 'NO_NACIONALIDADE', 'label' => 'Nacionalidade')
		        , array('campo' => 'NU_CPF', 'label' => 'CPF', 'class' => 'cen')
		        , array('campo' => 'NU_RNE', 'label' => 'RNE', 'class' => 'cen')
		        , array('campo' => 'NU_CTPS', 'label' => 'CTPS', 'class' => 'cen')
		        , array('campo' => 'DT_EXPIRACAO_CTPS', 'label' => 'Expiração CTPS', 'class' => 'cen')
		        , array('campo' => 'NU_CNH', 'label' => 'CNH', 'class' => 'cen')
		        , array('campo' => 'DT_EXPIRACAO_CNH', 'label' => 'Expiração CNH', 'class' => 'cen')
		        , array('campo' => 'NO_ENDERECO_ESTRANGEIRO_COMPLETO', 'label' => 'Endereço')
		        , array('campo' => 'NO_FUNCAO', 'label' => 'Função')
		        , array('campo' => 'NU_PASSAPORTE', 'label' => 'No passaporte', 'class' => 'cen')
		        , array('campo' => 'DT_VALIDADE_PASSAPORTE', 'label' => 'Val. Passaporte', 'class' => 'cen')
		        , array('campo' => 'TIPO_VISTO', 'label' => 'Tipo de visto')
		        , array('campo' => 'VA_RENUMERACAO_MENSAL', 'label' => 'Salário', 'class' => 'dir')
		        , array('campo' => 'NO_EMAIL_CANDIDATO', 'label' => 'Email')
		        , array('campo' => 'NU_TELEFONE_ESTRANGEIRO', 'label' => 'Telefone', 'class' => 'cen')
		        , array('campo' => 'DT_NASCIMENTO', 'label' => 'Dt. nasc.', 'class' => 'cen')
		        , array('campo' => 'NO_ESCOLARIDADE', 'label' => 'Escolaridade')
		        , array('campo' => 'TE_OBSERVACAO_CREWLIST', 'label' => 'Observação Crewlist')
		        , array('campo' => 'fl_tem_copia_passaporte', 'label' => 'Tem cópia passaporte?', 'class' => 'cen')
		        , array('campo' => 'fl_tem_form1344', 'label' => 'Tem form. 1344?', 'class' => 'cen')
		        , array('campo' => 'situacao_prazo_estada', 'label' => 'Prazo de estada calculado')
		        , array('campo' => 'situacao_texto', 'label' => 'Situação visto')
		        , array('campo' => 'situacao_origem', 'label' => 'Porque?')
			);
	    $campos[] = array(
	    	'painel' => 'cand',
	    	'titulo' => 'Dados candidato',
	    	'campos' => $grupo
	    );

		$grupo = array(
	         array('campo' => 'NU_PROCESSO_MTE', 'label' => 'Nº Processo MTE.', 'class' => 'cen')
	        ,array('campo' => 'dt_solicitacao', 'label' => 'Data solicitação', 'class' => 'cen')
	        ,array('campo' => 'DT_REQUERIMENTO', 'label' => 'Data Req. MTE', 'class' => 'cen')
	        ,array('campo' => 'CONSULADO', 'label' => 'Consulado')
	        ,array('campo' => 'PRAZO_SOLICITADO', 'label' => 'Prazo Solicitado', 'class' => 'cen')
	        ,array('campo' => 'DT_DEFERIMENTO', 'label' => 'Deferimento', 'class' => 'cen')
	        ,array('campo' => 'DIAS_EXPIRACAO_DEFERIMENTO', 'label' => 'Exp. do deferimento (180-dias)', 'class' => 'cen')
	        ,array('campo' => 'OBSERVACAO_VISTO', 'label' => 'Obs')
	        ,array('campo' => 'NO_RAZAO_SOCIAL_COBRANCA', 'label' => 'Empresa Cobr.')
		);
	    $campos[] = array(
	    	'painel' => 'mte',
	    	'titulo' => 'Autorização',
	    	'campos' => $grupo
	    );

		$grupo = array(
	         array('campo' => 'DT_EMISSAO', 'label' => 'Dt emissão', 'class' => 'cen')
	        , array('campo' => 'DT_ENTRADA', 'label' => 'Dt entrada', 'class' => 'cen')
	        , array('campo' => 'no_validade', 'label' => 'Validade', 'class' => 'cen')
	        , array('campo' => 'no_local_emissao', 'label' => 'Local emissão')
	        , array('campo' => 'nu_visto', 'label' => 'Núm. visto', 'class' => 'cen')
	    );
	    $campos[] = array(
	    	'painel' => 'coleta',
	    	'titulo' => 'Coleta',
	    	'campos' => $grupo
	    );

	    $grupo = array(
	         array('campo' => 'NU_PROTOCOLO_REG', 'label' => 'Nº Protocolo reg.', 'class' => 'cen')
	        , array('campo' => 'DT_PRAZO_ESTADA', 'label' => 'Prazo Estada', 'class' => 'destaque cen')
	        , array('campo' => 'DT_REQUERIMENTO_CIE', 'label' => 'Dt Req. Reg.', 'class' => 'cen')
	        , array('campo' => 'dt_validade_proreg', 'label' => 'Validade prot.', 'class' => 'cen')
	        , array('campo' => 'depf_no_nome', 'label' => 'Delegacia PF')
	    );
	    $campos[] = array(
	    	'painel' => 'registro',
	    	'titulo' => 'Registro',
	    	'campos' => $grupo
	    );


	    $grupo = array(
	         array('campo' => 'NU_PROTOCOLO_PRO', 'label' => 'Nº Prot. Prorrog.', 'class' => 'cen')
	        , array('campo' => 'DT_REQUERIMENTO_PRO', 'label' => 'Dt. Req. Prorrog.', 'class' => 'cen')
	        , array('campo' => 'DT_PRAZO_PRET_PRO', 'label' => 'Prazo Prorrog.', 'class' => 'destaque cen')
	        , array('campo' => 'dt_publicacao_dou', 'label' => 'Dt. Def. Prorrog.', 'class' => 'cen')
	        , array('campo' => 'NO_STATUS_CONCLUSAO_PRO', 'label' => 'Status', 'class' => 'cen')
	    );
	    $campos[] = array(
	    	'painel' => 'prorrog',
	    	'titulo' => 'Prorrogação',
	    	'campos' => $grupo
	    );

	    $grupo = array(
	         array('campo' => 'nu_protocolo_rest', 'label' => 'Nº Prot. Restab.', 'class' => 'cen')
	        , array('campo' => 'dt_requerimento_rest_fmt', 'label' => 'Dt. Req. Restab.', 'class' => 'cen')
	        , array('campo' => 'dt_prazo_estada_rest_fmt', 'label' => 'Prazo estada Restab.', 'class'=> 'destaque cen')
	        , array('campo' => 'dt_validade_rest', 'label' => 'Dt. val. prot. CIE.', 'class' => 'cen')
	    );
	    $campos[] = array(
	    	'painel' => 'restab',
	    	'titulo' => 'Restabelecimento',
	    	'campos' => $grupo
	    );
	    return $campos;
	}
	
}
