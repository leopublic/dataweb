<?php

class cREPOSITORIO_OS {

    public static function CursorProdutividade() {
        $sql = "
            select distinct up.cd_usuario , nome, meses.mesano, meses.anomes
                , ifnull(totalAut.qtdProcessos, 0) qtdProcessosAut
                , ifnull(totalPro.qtdProcessos, 0) qtdProcessosPro
                , ifnull(totalPre.qtdProcessos, 0) qtdProcessosPre
            from vusuarios_produtividade up
            join    (
                    select distinct date_format(soli_dt_devolucao, '%m/%Y') mesano, date_format(soli_dt_devolucao, '%Y-%m') anomes
                        from solicita_visto
                        where soli_dt_devolucao is not null
                        and cd_usuario_devolucao is not null
                        and soli_dt_devolucao  > '2013-01-01'
                    union distinct
                    select distinct date_format(dt_pre_cadastro, '%m/%Y') mesano, date_format(dt_pre_cadastro, '%Y-%m') anomes
                        from processo_prorrog
                        where dt_pre_cadastro is not null
                        and id_solicita_visto is not null
                        and dt_pre_cadastro > '2013-01-01'
                    ) meses
            join usuarios on usuarios.cd_usuario = up.cd_usuario
            left join
                    (
                    select date_format(soli_dt_devolucao, '%m/%Y') mesano, s.cd_usuario_devolucao, id_tipo_acompanhamento, count(*) qtdProcessos
                    from solicita_visto s
                    join servico sv on sv.nu_servico = s.nu_servico
                    where cd_usuario_devolucao is not null
                    and id_tipo_acompanhamento = 1
                    group by date_format(soli_dt_devolucao, '%m/%Y') , s.cd_usuario_devolucao, id_tipo_acompanhamento
                    )totalAut
                        on totalAut.mesano = meses.mesano
                        and totalAut.cd_usuario_devolucao = up.cd_usuario
            left join
                    (
                    select date_format(soli_dt_devolucao, '%m/%Y') mesano, s.cd_usuario_devolucao, id_tipo_acompanhamento, count(*) qtdProcessos
                    from solicita_visto s
                    join servico sv on sv.nu_servico = s.nu_servico
                    where cd_usuario_devolucao is not null
                    and id_tipo_acompanhamento = 3
                    group by date_format(soli_dt_devolucao, '%m/%Y') , s.cd_usuario_devolucao, id_tipo_acompanhamento
                    )totalPro
                        on totalPro.mesano = meses.mesano
                        and totalPro.cd_usuario_devolucao = up.cd_usuario
            left join
                    (
                    select date_format(dt_pre_cadastro, '%m/%Y') mesano, cd_usuario_pre_cadastro, 99 id_tipo_acompanhamento, count(*) qtdProcessos
                    from processo_prorrog pp
                    join solicita_visto sv on sv.id_solicita_visto = pp.id_solicita_visto
                    where cd_usuario_pre_cadastro is not null
                    and convert(nu_pre_cadastro USING utf8) <> CONVERT('(já realizado)' USING utf8)
                    group by date_format(dt_pre_cadastro, '%m/%Y') , cd_usuario_pre_cadastro, id_tipo_acompanhamento
                    )totalPre
                        on totalPre.mesano = meses.mesano
                        and totalPre.cd_usuario_pre_cadastro = up.cd_usuario
            order by nome, anomes
            ";
        return cAMBIENTE::ConectaQuery($sql, __CLASS__ . '.' . __FUNCTION__);
    }

    public static function CursorHistoricoOs($pFiltros) {
        $sql = "select sv.id_solicita_visto
                , sv.nu_solicitacao
                , sv.dt_cadastro
                , s.no_servico_resumido
                , h1.hios_dt_evento hios_dt_evento1
                , h2.hios_dt_evento hios_dt_evento2
                , h3.hios_dt_evento hios_dt_evento3
                , h4.hios_dt_evento hios_dt_evento4
                , h5.hios_dt_evento hios_dt_evento5
                , h6.hios_dt_evento hios_dt_evento6
                from solicita_visto sv
                join servico s on s.nu_servico = sv.nu_servico
                left join vhistorico_os_status h1 on h1.id_solicita_visto = sv.id_solicita_visto and h1.id_status_sol_depois = 1
                left join vhistorico_os_status h2 on h2.id_solicita_visto = sv.id_solicita_visto and h2.id_status_sol_depois = 2
                left join vhistorico_os_status h3 on h3.id_solicita_visto = sv.id_solicita_visto and h3.id_status_sol_depois = 3
                left join vhistorico_os_status h4 on h4.id_solicita_visto = sv.id_solicita_visto and h4.id_status_sol_depois = 4
                left join vhistorico_os_status h5 on h5.id_solicita_visto = sv.id_solicita_visto and h5.id_status_sol_depois = 5
                left join vhistorico_os_status h6 on h6.id_solicita_visto = sv.id_solicita_visto and h6.id_status_sol_depois = 6
                where s.serv_fl_envio_bsb = 1";
        $filtros = cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql.= $filtros;
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        return $res;
    }

    public static function sqlCursorTaxas($pFiltros, $pOrderby, $pPag = 1, $pOffset = 30, $pCount = false) {
        if ($pCount) {
            $sql .= " select count(*) ";
        } else {
            $sql = " select sv.id_solicita_visto, sv.nu_solicitacao, sv.dt_solicitacao, date_format(dt_solicitacao, '%d/%m/%Y') dt_solicitacao_fmt"
                    . " ,c.nome_completo"
                    . " , ac.nu_candidato"
                    . " ,e.no_razao_social"
                    . " ,ep.no_embarcacao_projeto"
                    . " ,s.no_servico_resumido"
                    . " ,t.id_taxa"
                    . " ,tp.id_taxapaga"
                    . " ,t.descricao descricao_taxa"
                    . " ,ifnull(date_format(tp.data_taxa, '%d/%m/%Y'), '--') data_taxa"
                    . " ,tp.valor"
                    . " ,tp.observacao"
                    . " ,ss.no_status_sol"
                    . " ,ifnull(ua.nome, '--') nome_cad";
        }

        $sql .= " from solicita_visto sv"
                . " join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto"
                . " left join empresa e on e.nu_empresa = sv.nu_empresa"
                . " left join status_solicitacao ss on ss.id_status_sol = sv.id_status_sol"
                . " left join usuarios ua on ua.cd_usuario = sv.cd_admin_cad"
                . " left join embarcacao_projeto ep on ep.nu_empresa = sv.nu_empresa and ep.nu_embarcacao_projeto = sv.nu_embarcacao_projeto"
                . " join candidato c on c.nu_candidato = ac.nu_candidato"
                . " join servico s on s.nu_servico = sv.nu_servico"
                . " join perfiltaxa pt on pt.id_perfiltaxa = s.id_perfiltaxa"
                . " join taxa t on t.id_perfiltaxa = pt.id_perfiltaxa"
                . " left join taxapaga tp on tp.id_taxa = t.id_taxa and tp.id_solicita_visto = sv.id_solicita_visto and tp.nu_candidato = ac.nu_candidato"
                . " where 1=1 ";
        $filtros = cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql.= $filtros;
        if ($pOrderby != '') {
            $sql .= " order by " . $pOrderby;
        }
        if (!$pCount) {
            $sql .= " LIMIT " . (($pPag - 1) * $pOffset) . " , " . $pOffset;
        }
        return $sql;
    }



    public static function cursorTaxasNaoPagas($pFiltros, $pOrderBy){
        $sql = self::sqlCursorTaxasNaoPagas($pFiltros, $pOrderBy);
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        return $res;        
    }
    public static function sqlCursorTaxasNaoPagas($pFiltros, $pOrderby) {
            $sql = " select sv.id_solicita_visto, sv.nu_solicitacao, sv.dt_solicitacao, date_format(dt_solicitacao, '%d/%m/%Y') dt_solicitacao_fmt"
                    . " , sv.de_observacao"
                    . " ,c.nome_completo"
                    . " , ac.nu_candidato"
                    . " ,e.no_razao_social"
                    . " ,ep.no_embarcacao_projeto"
                    . " ,s.no_servico_resumido"
                    . " ,ss.no_status_sol"
                    . " ,ifnull(ua.nome, '--') nome_cad";

        $sql .= " from solicita_visto sv"
                . " join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto"
                . " join status_solicitacao ss on ss.id_status_sol = sv.id_status_sol"
                . " left join empresa e on e.nu_empresa = sv.nu_empresa"
                . " left join usuarios ua on ua.cd_usuario = sv.cd_admin_cad"
                . " left join embarcacao_projeto ep on ep.nu_empresa = sv.nu_empresa and ep.nu_embarcacao_projeto = sv.nu_embarcacao_projeto"
                . " join candidato c on c.nu_candidato = ac.nu_candidato"
                . " join servico s on s.nu_servico = sv.nu_servico"
                . " where ss.FL_ENCERRADA = 1"
                . " and s.id_perfiltaxa is not null"
                . " and sv.id_solicita_visto not in (select id_solicita_visto from taxapaga) ";
        $filtros = cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql.= $filtros;
        if ($pOrderby != '') {
            $sql .= " order by " . $pOrderby;
        }
        return $sql;
    }

    public static function cursorTaxasPagas($pFiltros, $pOrderBy){
        $sql = self::sqlCursorTaxasPagas($pFiltros, $pOrderBy);
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        return $res;        
    }
    
    public static function sqlCursorTaxasPagas($pFiltros, $pOrderby) {
        $sql = " select sv.id_solicita_visto, sv.nu_solicitacao, sv.dt_solicitacao, date_format(dt_solicitacao, '%d/%m/%Y') dt_solicitacao_fmt"
                . " ,c.nome_completo"
                . " , tp.nu_candidato"
                . " ,e.no_razao_social"
                . " ,ep.no_embarcacao_projeto"
                . " ,s.no_servico_resumido"
                . " ,t.id_taxa"
                . " ,tp.id_taxapaga"
                . " ,t.descricao descricao_taxa"
                . " ,ifnull(date_format(tp.data_taxa, '%d/%m/%Y'), '--') data_taxa_fmt"
                . " ,tp.valor"
                . " ,tp.observacao"
                . " ,ss.no_status_sol"
                . " ,sc.stco_tx_nome"
                . " ,ifnull(u.nome, '--') nome_cad";

        $sql .= " from taxapaga tp "
                . " join solicita_visto sv on sv.id_solicita_visto = tp.id_solicita_visto"
                . " join candidato c on c.nu_candidato = tp.nu_candidato"
                . " join taxa t on t.id_taxa = tp.id_taxa"
                . " left join status_solicitacao ss on ss.ID_STATUS_SOL = sv.ID_STATUS_SOL"
                . " left join status_cobranca_os sc on sc.stco_id = sv.stco_id"
                . " join empresa e on e.nu_empresa = sv.nu_empresa"
                . " left join embarcacao_projeto ep on ep.nu_empresa = sv.nu_empresa and ep.NU_EMBARCACAO_PROJETO = sv.NU_EMBARCACAO_PROJETO"
                . " left join usuarios u on u.cd_usuario = sv.cd_admin_cad"
                . " join servico s on s.NU_SERVICO = sv.NU_SERVICO"
                . " where 1=1 ";
        $filtros = cBANCO::WhereFiltros($pFiltros);    // Monta filtros
        $sql.= $filtros;
        if ($pOrderby != '') {
            $sql .= " order by " . $pOrderby;
        }
        return $sql;
    }

    public static function QtdCursorTaxas($pFiltros, $pOrdenacao) {
        $sql = self::sqlCursorTaxas($pFiltros, $pOrdenacao, null, null, true);
        $cursor = cAMBIENTE::ConectaQuery($sql, __CLASS__ . "." . __FUNCTION__);
        $rs = $cursor->fetch(PDO::FETCH_BOTH);
        return $rs[0];
    }

    public static function cursorTaxas($pFiltros, $pOrderby, $pPag = 1, $pOffset = 30) {
        $sql = self::sqlCursorTaxas($pFiltros, $pOrderby, $pPag, $pOffset, false);
        $res = cAMBIENTE::ConectaQuery($sql, __CLASS__);
        return $res;
    }

    public static function cursorAgenda($cd_usuario, $filtros) {
        $sql = "select sv.id_solicita_visto"
                . ", sv.nu_solicitacao"
                . ", nu_protocolo"
                . ", sv.dt_solicitacao"
                . ", date_format(sv.dt_solicitacao, '%d/%m/%Y') dt_solicitacao_fmt"
                . ", date_format(pp.dt_envio_bsb, '%d/%m/%Y') dt_envio_bsb_fmt"
                . ", ac.nu_candidato"
                . ", c.nome_completo"
                . ", s.no_servico_resumido"
                . ", ac.nu_candidato"
                . ", e.no_razao_social"
                . ", ep.no_embarcacao_projeto"
                . ", sv.id_tipo_envio"
                . ", te.descricao"
                . ", ss.no_status_sol"
                . ", u.nome"
                . " from solicita_visto sv"
                . " left join empresa e on e.nu_empresa = sv.nu_empresa"
                . " left join embarcacao_projeto ep on ep.nu_empresa = sv.nu_empresa and ep.nu_embarcacao_projeto = sv.nu_embarcacao_projeto"
                . " join servico s on s.nu_servico = sv.nu_servico"
                . " join processo_prorrog pp on pp.id_solicita_visto = sv.id_solicita_visto"
                . " join autorizacao_candidato ac on ac.id_solicita_visto = sv.id_solicita_visto"
                . " join candidato c on c.nu_candidato = ac.nu_candidato"
                . " left join status_solicitacao ss on ss.id_status_sol = sv.id_status_sol"
                . " left join tipo_envio te on te.id_tipo_envio = sv.id_tipo_envio"
                . " left join usuarios u on u.cd_usuario = sv.cd_tecnico"
                . " where coalesce(nu_protocolo , '')  = '' "
                . " and s.nu_tipo_servico = 3";
        foreach ($filtros as $campo => $valor) {
            if ($valor != '') {
                $sql .= ' and ' . $campo . '=' . $valor;
            }
        }
//        $sql   .= " and cd_admin_cad = ".$cd_usuario
        $sql .= " order by sv.dt_solicitacao desc";
//                . " limit 50";

        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $ret = $rs->fetchAll();

        // Monta o prazo de estada atual
        foreach ($ret as &$reg) {
            $cand = new cCANDIDATO();
            $cand->Recuperar($reg['nu_candidato']);
            $visto = $cand->VistoAtual();
            $data = '';
            $reg['dias'] = '--';
            $reg['atrasado'] = '';
            if (isset($visto['autorizacao'])) {
                $data = $visto['autorizacao']->get_situacao_prazo_estada();
                $reg['situacao_prazo_estada'] = $data;
                if (strlen($data) == 10) {
                    if (checkdate(substr($data, 3, 2), substr($data, 0, 2), substr($data, 6, 4))) {
                        $hoje = new DateTime();
                        $validade = DateTime::createFromFormat('d/m/Y', $data);
                        $dias = $hoje->diff($validade)->format('%R%a');
                        if (intval($dias) > 90) {
                            $reg['estilo'] = '';
                        } else {
                            $reg['atrasado'] = true;
                        }
                        $reg['dias'] = $dias;
                    }
                }
                $reg['situacao_texto'] = $visto['autorizacao']->get_situacao_texto() . '<br/>Porque?: ' . $visto['autorizacao']->get_situacao_origem();
            } else {
                $reg['situacao_prazo_estada'] = '--';
                $reg['situacao_texto'] = '--';
            }
            if (is)
                $envio = '';
            if ($reg['dt_envio_bsb_fmt'] != '') {
                $envio = $reg['dt_envio_bsb_fmt'];
            }
            if ($envio != '') {
                if ($reg['id_tipo_envio'] != '') {
                    $envio.= "<br/>(via " . $reg['descricao'] . ')';
                } else {
                    $envio.= "<br/>(via Brasília)";
                }
            }
            $reg['envio'] = $envio;
        }
        return $ret;
    }

    public function listaStatusSolicitacao() {
        $sql = "select id_status_sol , no_status_sol from status_solicitacao order by id_status_sol";
        $rs = cAMBIENTE::ConectaQuery($sql, __CLASS__ . '->' . __FUNCTION__);
        $ret = $rs->fetchAll();
        return $ret;
    }

    public function candidatos($id_solicita_visto, $lista = '0') {
        if ($id_solicita_visto == '') {
            $id_solicita_visto = 0;
        }
        $sql = "SELECT distinct CANDIDATO.nu_candidato
                , CANDIDATO.nome_completo
                , CANDIDATO.nu_passaporte
                , no_nacionalidade
                , CANDIDATO.NU_RNE
                , CANDIDATO.BO_CADASTRO_MINIMO_OK
                , no_razao_social
                , no_embarcacao_projeto
                , CANDIDATO.nu_candidato_parente
                , no_grau_parentesco
                , autc_fl_revisado
                , usua_tx_apelido
                , (select ifnull(count(distinct solicita_visto.id_solicita_visto) , 0)
                                        from solicita_visto
                            left join status_solicitacao ss on ss.id_status_sol = solicita_visto.id_status_sol
                            join autorizacao_candidato on autorizacao_candidato.id_solicita_visto = solicita_visto.id_solicita_visto
                            join servico on servico.nu_servico = solicita_visto.nu_servico
                                                where autorizacao_candidato.nu_candidato =  CANDIDATO.nu_candidato
                                                and coalesce(ss.fl_encerrada, 0) = 0
                                                and solicita_visto.id_solicita_visto <> " . cBANCO::ChaveOk($id_solicita_visto) . "
                                                and servico.id_tipo_acompanhamento = 1
                                                ) qtd_os_aut
                                  , (select ifnull(count(distinct solicita_visto.id_solicita_visto) , 0)
                                                from solicita_visto
                            left join status_solicitacao ss on ss.id_status_sol = solicita_visto.id_status_sol
                            join autorizacao_candidato on autorizacao_candidato.id_solicita_visto = solicita_visto.id_solicita_visto
                            join servico on servico.nu_servico = solicita_visto.nu_servico
                                                where autorizacao_candidato.nu_candidato =  CANDIDATO.nu_candidato
                                                and coalesce(ss.fl_encerrada, 0) = 0
                                                and solicita_visto.id_solicita_visto <> " . cBANCO::ChaveOk($id_solicita_visto) . "
                                                and servico.id_tipo_acompanhamento = 4
                                                ) qtd_os_reg
                                  , (select ifnull(count(distinct solicita_visto.id_solicita_visto) , 0)
                                                from solicita_visto
                            left join status_solicitacao ss on ss.id_status_sol = solicita_visto.id_status_sol
                            join autorizacao_candidato on autorizacao_candidato.id_solicita_visto = solicita_visto.id_solicita_visto
                            join servico on servico.nu_servico = solicita_visto.nu_servico
                                                where autorizacao_candidato.nu_candidato =  CANDIDATO.nu_candidato
                                                and coalesce(ss.fl_encerrada, 0) = 0
                                                and solicita_visto.id_solicita_visto <> " . cBANCO::ChaveOk($id_solicita_visto) . "
                                                and servico.id_tipo_acompanhamento = 3
                                                ) qtd_os_pro
                                  ";
        $sql.= "  FROM CANDIDATO ";
        $sql.= "  left join autorizacao_candidato ac on ac.id_solicita_visto = " . $id_solicita_visto . " and ac.nu_candidato = CANDIDATO.nu_candidato";
        $sql.= "  left join CANDIDATO cp on cp.NU_CANDIDATO = CANDIDATO.nu_candidato_parente";
        $sql.= "  left join PAIS_NACIONALIDADE P on P.CO_PAIS = CANDIDATO.CO_NACIONALIDADE";
        $sql.= "  left join EMPRESA E on E.NU_EMPRESA = CANDIDATO.NU_EMPRESA";
        $sql.= "  left join EMBARCACAO_PROJETO EP on EP.NU_EMPRESA = CANDIDATO.NU_EMPRESA and EP.NU_EMBARCACAO_PROJETO = CANDIDATO.NU_EMBARCACAO_PROJETO";
        $sql.= "  left join grau_parentesco gp on gp.co_grau_parentesco = CANDIDATO.co_grau_parentesco ";
        $sql.= "  left join usuarios u on u.cd_usuario = ac.cd_usuario_revisao ";
        $sql.= " where (CANDIDATO.NU_CANDIDATO in (select nu_candidato from autorizacao_candidato where id_solicita_visto = " . $id_solicita_visto . ")"
                . " or  CANDIDATO.NU_CANDIDATO in (" . $lista . "))";
        $sql .= "   or (CANDIDATO.nu_candidato_parente in (" . $lista . ")"
                . " and CANDIDATO.NU_CANDIDATO not in (" . $lista . "))";
        $sql.= " ORDER BY concat(coalesce(cp.NOME_COMPLETO, ''), CANDIDATO.NOME_COMPLETO) asc ";

        $res = cAMBIENTE::$db_pdo->query($sql);
        $rs = $res->fetchAll(PDO::FETCH_ASSOC);

        return $rs;
    }
    /**
     * Exclui a OS, se for possível
     * @param  \cORDEMSERVICO $os [description]
     * @return [type]             [description]
     */
    public function excluir(\cORDEMSERVICO $os){
        $taxaspagas = $os->taxaspagas();
        if (count($taxaspagas) > 0){
            throw new Exception("essa OS possui taxas que foram pagas. Entre em contato com a Rafaela para regularizar as taxas pagas.");
        } else {
            $sql = "call salva_os_excluida(:id_solicita_visto, :cd_usuario)";
            $sth = cAMBIENTE::$db_pdo->prepare($sql);
            $sth->bindParam(':id_solicita_visto', $p_id_solicita_visto, PDO::PARAM_INT);
            $sth->bindParam(':cd_usuario', $p_cd_usuario, PDO::PARAM_INT);
            $p_id_solicita_visto = $os->mID_SOLICITA_VISTO;
            $p_cd_usuario = cSESSAO::$mcd_usuario;
            $sth->execute();

            $sql = "delete from AUTORIZACAO_CANDIDATO where id_solicita_visto = " . $os->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);
            $candidatos = split(",", $os->mCandidatos);
            if (count($candidatos) > 0) {
                foreach ($candidatos as $NU_CANDIDATO) {
                    if ($NU_CANDIDATO > 0) {
                        $mte = new cprocesso_mte();
                        $mte->RecupereSePelaOs($os->mID_SOLICITA_VISTO, $NU_CANDIDATO);
                        if ($mte->mcodigo > 0) {
                            $mte->Exclua();
                        }
                    }
                }
            }
            cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_prorrog where id_solicita_visto = " . $os->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_emiscie where id_solicita_visto = " . $os->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_regcie where id_solicita_visto = " . $os->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_cancel where id_solicita_visto = " . $os->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_coleta where id_solicita_visto = " . $os->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from processo_generico where id_solicita_visto = " . $os->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);
            $sql = "delete from solicita_visto where id_solicita_visto = " . $os->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);

            $sql = "update candidato_tmp set id_solicita_visto = null where id_solicita_visto = ".$os->mID_SOLICITA_VISTO;
            cAMBIENTE::$db_pdo->exec($sql);
            //TODO: Se for principal de pacote
            // retirar o pacote das filhas
            // renumerar as filhas
            // comentar na obs
            //
            // Se for filha de pacote
            //  liberar a caga no pacote
            //


        }
    }
    /**
     * Cria uma OS a partir de um DR com serviço
     * @param  \cCANDIDATO_TMP $dr [description]
     * @return [type]              [description]
     */
    public function criaOsDeDr(\cCANDIDATO_TMP $dr){
        $observacao = 'Criada a partir do DR ('.$dr->mnu_candidato_tmp.')';
        $os = $this->nova($dr->mNU_EMPRESA, $dr->mNU_EMBARCACAO_PROJETO, $dr->nome_solicitador(), cSESSAO::$mcd_usuario, $dr->mnu_servico, $observacao );

        $os->AdicionarCandidatos($dr->mNU_CANDIDATO);
        $dr->mid_solicita_visto = $os->mID_SOLICITA_VISTO;
        $dr->salvar();
        return $os;
    }
    /**
     * Cria uma OS a partir de um convite com vários candidatos
     * @param  int $conv_id
     * @return [type]              [description]
     */
    public function criaOsDeConvite($conv_id){
        $conv = new cconvidado;
        $conv->RecuperePeloId($conv_id);
        $drs = $conv->drsImportadosSemOs();
        if (count($drs) > 0){
            $observacao = 'Criada a partir do convidado '.$conv->mnome.' ('.$conv->mconv_id.')';

            $os = $this->nova($conv->mnu_empresa, $conv->mnu_embarcacao_projeto, $conv->mnome, cSESSAO::$mcd_usuario, $conv->mnu_servico, $observacao );

            foreach($drs as $dr){
                $cand = new cCANDIDATO_TMP;
                $cand->carreguePropriedades($dr);
                $os->AdicionarCandidatos($cand->mNU_CANDIDATO);
                $cand->mid_solicita_visto = $os->mID_SOLICITA_VISTO;
                $cand->salvar();
            }
            $_SESSION['msg'] = 'OS criada com sucesso!';
        } else {
            $os = new cORDEMSERVICO();
            $os->mID_SOLICITA_VISTO = $conv->mID_SOLICITA_VISTO;
            $os->RecuperarSolicitaVisto();
            $_SESSION['msg'] = 'Não há nenhum candidato pronto para abrir OS desse convite.';
        }
        return $os;
    }

    public function nova($nu_empresa, $nu_embarcacao_projeto, $no_solicitador, $cd_usuario_cadastro, $nu_servico, $observacao){
        $os = new cORDEMSERVICO;
        $os->mNU_EMPRESA = $nu_empresa;
        $os->mNU_EMBARCACAO_PROJETO = $nu_embarcacao_projeto;
        $os->mnu_empresa_requerente = $nu_empresa;
        $os->mNU_EMBARCACAO_PROJETO_COBRANCA = $nu_embarcacao_projeto;
        $os->mno_solicitador = $no_solicitador;
        $os->mdt_solicitacao = date('d/m/Y H:i:s');
        $os->mcd_admin_cad = $cd_usuario_cadastro;
        $os->mNU_USUARIO_CAD = $cd_usuario_cadastro;
        $os->mdt_cadastro = date('d/m/Y H:i:s');
        $os->mID_STATUS_SOL = cSTATUS_SOLICITACAO::ssNOVA;
        $os->mNU_SERVICO = $nu_servico;
        $os->mcd_tecnico = $cd_usuario_cadastro;
        $os->mde_observacao = $observacao;
        $servico = new cSERVICO;
        $servico->RecuperePeloId($nu_servico);
        $os->mID_TIPO_ACOMPANHAMENTO = $servico->mID_TIPO_ACOMPANHAMENTO;
        $os->salveTodosAtributos();
        return $os;
    }

}
