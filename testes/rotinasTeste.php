<?php
class rotinasComuns{
	public static function randValor($pMenor = 10000, $pMaior = 999999){
		return rand($pMenor, $pMaior);
	}

	public static function randData(){
		return substr("0".rand(1,28),-2)."/".substr("0".rand(1,12),-2)."/2010";
	}

	public static function randTexto($pTam = 10){
		$ret = '';
		for ($i=0;$i<$pTam;$i++){
			$ret .= chr(rand(97,122));
		}
		return $ret;
	}

	public static function MontaPostDeTela(&$pPOST, $pTela){
		foreach($pTela->mCampo as $campo){
			$pPOST['CMP_'.$campo->mCampoBD] = '' ;
		}
	}

	public static function MontaValoresEmPost(&$pPOST){
		foreach($pPOST as $campo => $valor){
			$pPOST[$campo] = rand(100000,999999) ;
		}
	}

	public static function MontaValoresEmTela(&$pTela){
		foreach($pTela->mCampo as &$campo ){
			$campo->mValor = rand(100000,999999) ;
		}
	}

	public static function ObterServico($pID_TIPO_ACOMPANHAMENTO = 1){
		$sql = 'select * from SERVICO where ID_TIPO_ACOMPANHAMENTO = '.$pID_TIPO_ACOMPANHAMENTO;
		$res = conectaQuery($sql, __CLASS__.'->'.__FUNCTION__);
		$rs = mysql_fetch_array($res);
		return $rs['NU_SERVICO'];
	}

}
