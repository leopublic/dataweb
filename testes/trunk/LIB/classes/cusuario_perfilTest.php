<?php

/**
 * Description of cusuario_perfilTest
 *
 * @author leonardo
 */
class cusuario_perfilTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var cusuario_perfil
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new cusuario_perfil;
	}

	public static function Stub(){
		$sql = "select * from usuario_perfil ";
		$res = cAMBIENTE::ConectaQuery($sql,__CLASS__.".".__FUNCTION__);
		$rs = $res->fetch(PDO::FETCH_ASSOC );
		$obj = new cusuario_perfil();
		$obj->RecupereSe($rs['cd_perfil']);
		return $obj;
	}
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {

	}
			
	public function testCriacaoStub(){
		$this->object = self::Stub();
		$this->assertNotEmpty($this->object->mcd_perfil);
		$this->assertNotEmpty($this->object->mnm_perfil);
	}


}
