<?php
include 'cEMPRESATest.php';
/**
 * Description of cEMBARCACAO_PROJETOTest
 *
 * @author leonardo
 */
class cEMBARCACAO_PROJETOTest extends PHPUnit_Framework_TestCase {	
	/**
	 * @var cEMBARCACAO_PROJETO
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new cEMBARCACAO_PROJETO;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {

	}
	/**
	 * @expectedException cERRO_CONSISTENCIA
	 */
	public function testConsistirInclusao(){
		$this->object->mNU_EMPRESA = 21;
		$nome = rotinasComuns::randTexto();
		$this->object->mNO_EMBARCACAO_PROJETO = $nome;
		$this->object->Salve();
		$this->assertNotEmpty($this->object->mNU_EMBARCACAO_PROJETO);
		$this->object->mNU_EMBARCACAO_PROJETO = '';
		$this->object->Salve();
	}
	
	public function testIncluir()
	{
		$mNU_EMPRESA = cEMPRESATest::Stub()->mNU_EMPRESA;
		$mNO_EMBARCACAO_PROJETO = rotinasComuns::randTexto();
		$mNO_MUNICIPIO = rotinasComuns::randTexto();
		$mDT_PRAZO_CONTRATO = rotinasComuns::randData();
		$mNO_CONTATO = rotinasComuns::randTexto();
		$mNO_EMAIL_CONTATO = rotinasComuns::randTexto();
		$mNU_TELEFONE_CONTATO = rotinasComuns::randTexto();
		$mQT_TRIPULANTES = rotinasComuns::randValor();
		$mQT_TRIPULANTES_ESTRANGEIROS = rotinasComuns::randValor();
		$mNO_CONTRATO = rotinasComuns::randTexto();
		$mCO_PAIS = 10;
		$mDS_JUSTIFICATIVA = rotinasComuns::randTexto();
		$mID_TIPO_EMBARCACAO_PROJETO= 1;
		$mTX_OBSERVACAO = rotinasComuns::randTexto();
		$mnu_estado = 13;
		$mNO_PREFIXO = rotinasComuns::randTexto();
		$membp_fl_ativo = 1;
		$this->object->mNU_EMPRESA = $mNU_EMPRESA;
		$this->object->mNO_EMBARCACAO_PROJETO = $mNO_EMBARCACAO_PROJETO;
		$this->object->mNO_MUNICIPIO = $mNO_MUNICIPIO;
		$this->object->mDT_PRAZO_CONTRATO = $mDT_PRAZO_CONTRATO;
		$this->object->mNO_CONTATO = $mNO_CONTATO;
		$this->object->mNO_EMAIL_CONTATO = $mNO_EMAIL_CONTATO;
		$this->object->mNU_TELEFONE_CONTATO = $mNU_TELEFONE_CONTATO;
		$this->object->mQT_TRIPULANTES = $mQT_TRIPULANTES;
		$this->object->mQT_TRIPULANTES_ESTRANGEIROS = $mQT_TRIPULANTES_ESTRANGEIROS;
		$this->object->mNO_CONTRATO = $mNO_CONTRATO;
		$this->object->mCO_PAIS = $mCO_PAIS;
		$this->object->mDS_JUSTIFICATIVA = $mDS_JUSTIFICATIVA;
		$this->object->mID_TIPO_EMBARCACAO_PROJETO = $mID_TIPO_EMBARCACAO_PROJETO;
		$this->object->mTX_OBSERVACAO = $mTX_OBSERVACAO;
		$this->object->mnu_estado= $mnu_estado;
		$this->object->mNO_PREFIXO = $mNO_PREFIXO;
		$this->object->membp_fl_ativo = $membp_fl_ativo;
		$this->object->Salvar();
		$NU_EMBARCACAO_PROJETO = $this->object->mNU_EMBARCACAO_PROJETO;
		
		$this->object = new cEMBARCACAO_PROJETO();
		$this->object->mNU_EMPRESA = $mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO = $NU_EMBARCACAO_PROJETO;
		$this->object->RecuperePeloId();

		$this->assertEquals($this->object->mNU_EMPRESA, $mNU_EMPRESA);
		$this->assertEquals($this->object->mNO_EMBARCACAO_PROJETO, $mNO_EMBARCACAO_PROJETO);
		$this->assertEquals($this->object->mNO_MUNICIPIO, $mNO_MUNICIPIO);
		$this->assertEquals(cBANCO::DataFmt($this->object->mDT_PRAZO_CONTRATO), $mDT_PRAZO_CONTRATO);
		$this->assertEquals($this->object->mNO_CONTATO, $mNO_CONTATO);
		$this->assertEquals($this->object->mNO_EMAIL_CONTATO, $mNO_EMAIL_CONTATO);
		$this->assertEquals($this->object->mNU_TELEFONE_CONTATO, $mNU_TELEFONE_CONTATO);
		$this->assertEquals($this->object->mQT_TRIPULANTES, $mQT_TRIPULANTES);
		$this->assertEquals($this->object->mQT_TRIPULANTES_ESTRANGEIROS, $mQT_TRIPULANTES_ESTRANGEIROS);
		$this->assertEquals($this->object->mNO_CONTRATO, $mNO_CONTRATO);
		$this->assertEquals($this->object->mCO_PAIS, $mCO_PAIS);
		$this->assertEquals($this->object->mDS_JUSTIFICATIVA, $mDS_JUSTIFICATIVA);
		$this->assertEquals($this->object->mID_TIPO_EMBARCACAO_PROJETO, $mID_TIPO_EMBARCACAO_PROJETO);
		$this->assertEquals($this->object->mTX_OBSERVACAO, $mTX_OBSERVACAO);
		$this->assertEquals($this->object->mnu_estado, $mnu_estado);
		$this->assertEquals($this->object->mNO_PREFIXO, $mNO_PREFIXO);
		$this->assertEquals($this->object->membp_fl_ativo, $membp_fl_ativo);
		
	}
	/**
	 * @depends testIncluir
	 */
	public function testAtualizar(){
		$mNU_EMPRESA = cEMPRESATest::Stub()->mNU_EMPRESA;
		$mNO_EMBARCACAO_PROJETO = rotinasComuns::randTexto();
		$mNO_MUNICIPIO = rotinasComuns::randTexto();
		$mDT_PRAZO_CONTRATO = rotinasComuns::randData();
		$mNO_CONTATO = rotinasComuns::randTexto();
		$mNO_EMAIL_CONTATO = rotinasComuns::randTexto();
		$mNU_TELEFONE_CONTATO = rotinasComuns::randTexto();
		$mQT_TRIPULANTES = rotinasComuns::randValor();
		$mQT_TRIPULANTES_ESTRANGEIROS = rotinasComuns::randValor();
		$mNO_CONTRATO = rotinasComuns::randTexto();
		$mCO_PAIS = 10;
		$mDS_JUSTIFICATIVA = rotinasComuns::randTexto();
		$mID_TIPO_EMBARCACAO_PROJETO= 1;
		$mTX_OBSERVACAO = rotinasComuns::randTexto();
		$mnu_estado = 13;
		$mNO_PREFIXO = rotinasComuns::randTexto();
		$membp_fl_ativo = 1;
		$this->object->mNU_EMPRESA = $mNU_EMPRESA;
		$this->object->mNO_EMBARCACAO_PROJETO = $mNO_EMBARCACAO_PROJETO;
		$this->object->mNO_MUNICIPIO = $mNO_MUNICIPIO;
		$this->object->mDT_PRAZO_CONTRATO = $mDT_PRAZO_CONTRATO;
		$this->object->mNO_CONTATO = $mNO_CONTATO;
		$this->object->mNO_EMAIL_CONTATO = $mNO_EMAIL_CONTATO;
		$this->object->mNU_TELEFONE_CONTATO = $mNU_TELEFONE_CONTATO;
		$this->object->mQT_TRIPULANTES = $mQT_TRIPULANTES;
		$this->object->mQT_TRIPULANTES_ESTRANGEIROS = $mQT_TRIPULANTES_ESTRANGEIROS;
		$this->object->mNO_CONTRATO = $mNO_CONTRATO;
		$this->object->mCO_PAIS = $mCO_PAIS;
		$this->object->mDS_JUSTIFICATIVA = $mDS_JUSTIFICATIVA;
		$this->object->mID_TIPO_EMBARCACAO_PROJETO = $mID_TIPO_EMBARCACAO_PROJETO;
		$this->object->mTX_OBSERVACAO = $mTX_OBSERVACAO;
		$this->object->mnu_estado= $mnu_estado;
		$this->object->mNO_PREFIXO = $mNO_PREFIXO;
		$this->object->membp_fl_ativo = $membp_fl_ativo;
		$this->object->Salvar();
		$mNU_EMPRESA = $this->object->mNU_EMPRESA;
		$NU_EMBARCACAO_PROJETO = $this->object->mNU_EMBARCACAO_PROJETO;
		//
		$mNO_EMBARCACAO_PROJETO = rotinasComuns::randTexto();
		$mNO_MUNICIPIO = rotinasComuns::randTexto();
		$mDT_PRAZO_CONTRATO = rotinasComuns::randData();
		$mNO_CONTATO = rotinasComuns::randTexto();
		$mNO_EMAIL_CONTATO = rotinasComuns::randTexto();
		$mNU_TELEFONE_CONTATO = rotinasComuns::randTexto();
		$mQT_TRIPULANTES = rotinasComuns::randValor();
		$mQT_TRIPULANTES_ESTRANGEIROS = rotinasComuns::randValor();
		$mNO_CONTRATO = rotinasComuns::randTexto();
		$mCO_PAIS = 15;
		$mDS_JUSTIFICATIVA = rotinasComuns::randTexto();
		$mID_TIPO_EMBARCACAO_PROJETO= 1;
		$mTX_OBSERVACAO = rotinasComuns::randTexto();
		$mnu_estado = 26;
		$mNO_PREFIXO = rotinasComuns::randTexto();
		$membp_fl_ativo = 0;
		$this->object->mNO_EMBARCACAO_PROJETO = $mNO_EMBARCACAO_PROJETO;
		$this->object->mNO_MUNICIPIO = $mNO_MUNICIPIO;
		$this->object->mDT_PRAZO_CONTRATO = $mDT_PRAZO_CONTRATO;
		$this->object->mNO_CONTATO = $mNO_CONTATO;
		$this->object->mNO_EMAIL_CONTATO = $mNO_EMAIL_CONTATO;
		$this->object->mNU_TELEFONE_CONTATO = $mNU_TELEFONE_CONTATO;
		$this->object->mQT_TRIPULANTES = $mQT_TRIPULANTES;
		$this->object->mQT_TRIPULANTES_ESTRANGEIROS = $mQT_TRIPULANTES_ESTRANGEIROS;
		$this->object->mNO_CONTRATO = $mNO_CONTRATO;
		$this->object->mCO_PAIS = $mCO_PAIS;
		$this->object->mDS_JUSTIFICATIVA = $mDS_JUSTIFICATIVA;
		$this->object->mID_TIPO_EMBARCACAO_PROJETO = $mID_TIPO_EMBARCACAO_PROJETO;
		$this->object->mTX_OBSERVACAO = $mTX_OBSERVACAO;
		$this->object->mnu_estado= $mnu_estado;
		$this->object->mNO_PREFIXO = $mNO_PREFIXO;
		$this->object->membp_fl_ativo = $membp_fl_ativo;
		$this->object->Salvar();
		
		$this->object = new cEMBARCACAO_PROJETO();
		$this->object->mNU_EMPRESA = $mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO = $NU_EMBARCACAO_PROJETO;
		$this->object->RecuperePeloId();

		$this->assertEquals($this->object->mNU_EMPRESA, $mNU_EMPRESA);
		$this->assertEquals($this->object->mNO_EMBARCACAO_PROJETO, $mNO_EMBARCACAO_PROJETO);
		$this->assertEquals($this->object->mNO_MUNICIPIO, $mNO_MUNICIPIO);
		$this->assertEquals(cBANCO::DataFmt($this->object->mDT_PRAZO_CONTRATO), $mDT_PRAZO_CONTRATO);
		$this->assertEquals($this->object->mNO_CONTATO, $mNO_CONTATO);
		$this->assertEquals($this->object->mNO_EMAIL_CONTATO, $mNO_EMAIL_CONTATO);
		$this->assertEquals($this->object->mNU_TELEFONE_CONTATO, $mNU_TELEFONE_CONTATO);
		$this->assertEquals($this->object->mQT_TRIPULANTES, $mQT_TRIPULANTES);
		$this->assertEquals($this->object->mQT_TRIPULANTES_ESTRANGEIROS, $mQT_TRIPULANTES_ESTRANGEIROS);
		$this->assertEquals($this->object->mNO_CONTRATO, $mNO_CONTRATO);
		$this->assertEquals($this->object->mCO_PAIS, $mCO_PAIS);
		$this->assertEquals($this->object->mDS_JUSTIFICATIVA, $mDS_JUSTIFICATIVA);
		$this->assertEquals($this->object->mID_TIPO_EMBARCACAO_PROJETO, $mID_TIPO_EMBARCACAO_PROJETO);
		$this->assertEquals($this->object->mTX_OBSERVACAO, $mTX_OBSERVACAO);
		$this->assertEquals($this->object->mnu_estado, $mnu_estado);
		$this->assertEquals($this->object->mNO_PREFIXO, $mNO_PREFIXO);
		$this->assertEquals($this->object->membp_fl_ativo, $membp_fl_ativo);
	}
}
