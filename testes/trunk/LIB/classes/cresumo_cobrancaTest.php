<?php

/**
 * Description of cresumo_cobrancaTest
 *
 * @author leonardo
 */
class cresumo_cobrancaTest extends PHPUnit_Framework_TestCase{
	/**
	 * @var cresumo_cobranca
	 */
	public $object;
	/**
	 * @var cEMPRESA
	 */
	public $emp;
	/**
	 * @var cEMBARCACAO_PROJETO
	 */
	public $proj;
	
	protected function setUp() {
		unset($this->object);
		$this->object = new cresumo_cobranca();
		$this->emp = new cEMPRESA();
		$this->emp->mNO_RAZAO_SOCIAL = rotinasComuns::randTexto();
		$this->emp->Salvar();
		$this->proj = new cEMBARCACAO_PROJETO();
		$this->proj->mNU_EMPRESA = $this->emp->mNU_EMPRESA;
		$this->proj->mNO_EMBARCACAO_PROJETO = rotinasComuns::randTexto();
		$this->proj->Salve();

	}
	
	public function testIncluir(){
		$this->setUp();
		$this->object->mnu_empresa_requerente = $this->emp->mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->proj->mNU_EMBARCACAO_PROJETO;
		$this->object->Salvar();
		$this->assertNotEmpty($this->object->resc_nu_numero_mv);
		$numero_mv_ant = $this->object->mresc_nu_numero_mv;
		$numero_emp_ant = $this->object->mresc_nu_numero;
		$emp = $this->object->mnu_empresa_requerente;
		$proj = $this->object->mNU_EMBARCACAO_PROJETO_COBRANCA;
		
		$this->setUp();
		$this->object->mnu_empresa_requerente = $emp;
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $proj;		
		$this->object->Salvar();
		$this->assertEquals($numero_mv_ant + 1, $this->object->mresc_nu_numero_mv);
		$this->assertEquals($numero_emp_ant + 1, $this->object->mresc_nu_numero);
		
		
	}
	/**
	 * @expectedException cERRO_CONSISTENCIA
	 */
	public function testIncluir_numeroIgual(){
		$this->setUp();
		$this->object->mnu_empresa_requerente = $this->emp->mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->proj->mNU_EMBARCACAO_PROJETO;
		$this->object->Salvar();
		$this->assertNotEmpty($this->object->resc_nu_numero_mv);
		$numero = $this->object->mresc_nu_numero_mv;
		
		$this->setUp();
		$this->object->mnu_empresa_requerente = $this->emp->mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->proj->mNU_EMBARCACAO_PROJETO;
		$this->object->mresc_nu_numero_mv = $numero;
		$this->object->Salvar();
	}
	
	public function testget_proximoNumero(){
		$this->setUp();
		$this->object->mnu_empresa_requerente = $this->emp->mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->proj->mNU_EMBARCACAO_PROJETO;
		$this->object->Salvar();
		$this->assertNotEmpty($this->object->resc_nu_numero_mv);
		$numero = $this->object->mresc_nu_numero_mv;
		$sql = "delete from resumo_cobranca where resc_nu_numero_mv > ".$numero;
		cAMBIENTE::$db_pdo->exec($sql);
		
		$this->assertEquals($numero +1 , cresumo_cobranca::get_proximoNumero());
	}

	public function testAtualizarEmbarcacaoProjeto(){
		$this->setUp();
		$this->object->mNU_EMPRESA = $this->emp->mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO = $this->proj->mNU_EMBARCACAO_PROJETO;

		$this->emp = new cEMPRESA();
		$this->emp->mNO_RAZAO_SOCIAL = rotinasComuns::randTexto();
		$this->emp->Salvar();

		$this->proj = new cEMBARCACAO_PROJETO();
		$this->proj->mNU_EMPRESA = $this->emp->mNU_EMPRESA;
		$this->proj->mNO_EMBARCACAO_PROJETO = rotinasComuns::randTexto();
		$this->proj->Salve();
		$this->object->mnu_empresa_requerente = $this->emp->mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->proj->mNU_EMBARCACAO_PROJETO;

		$this->object->mresc_dt_faturamento = rotinasComuns::randData();
		$this->object->Salvar();
		$id = $this->object->mresc_id;

		$this->proj = new cEMBARCACAO_PROJETO();
		$this->proj->mNU_EMPRESA = $this->emp->mNU_EMPRESA;
		$this->proj->mNO_EMBARCACAO_PROJETO = rotinasComuns::randTexto();
		$this->proj->Salve();
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->proj->mNU_EMBARCACAO_PROJETO;
		$this->object->Salvar();

		$this->object = new cresumo_cobranca();
		$this->object->mresc_id = $id;
		$this->object->RecuperePeloId();
		$this->assertEquals($this->proj->mNU_EMBARCACAO_PROJETO, $this->object->mNU_EMBARCACAO_PROJETO_COBRANCA);
	}

	public function testAtualizarNumeroMv(){
		$this->setUp();
		$this->object->mnu_empresa_requerente = $this->emp->mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->proj->mNU_EMBARCACAO_PROJETO;
		$this->object->Salvar();
		$this->assertNotEmpty($numero = $this->object->mresc_id);
		$numero = $this->object->mresc_nu_numero_mv;
		$this->object->mresc_id = "";
		$this->object->mresc_nu_numero_mv = "";
		$this->object->Salvar();
		$this->assertEquals($numero + 1, $this->object->mresc_nu_numero_mv);
	}

	public function testAtualizarNumeroMvUnico(){
		$this->setUp();
		$this->object->mnu_empresa_requerente = $this->emp->mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->proj->mNU_EMBARCACAO_PROJETO;
		$this->object->Salvar();
		$this->assertNotEmpty($numero = $this->object->mresc_id);
		$numero = $this->object->mresc_nu_numero_mv;
		$this->object->mresc_id = "";
		try{
			$this->object->Salvar();
		}
		catch(cERRO_CONSISTENCIA $e){
			$msgEsperada = "Não foi possível realizar a operação pois:O número do relatório é único e não pode ser repetido.";
			$msgReal = substr($e->getMessage(), 0, 96);
			$this->assertEquals($msgEsperada, $msgReal);
		}
	}

	public function testAtualizarNumero(){
		$this->setUp();
		$this->object->mnu_empresa_requerente = $this->emp->mNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO_COBRANCA = $this->proj->mNU_EMBARCACAO_PROJETO;
		$this->object->Salvar();
		$this->assertNotEmpty($numero = $this->object->mresc_id);
		$numero = $this->object->mresc_nu_numero;
		$this->object->mresc_id = "";
		$this->object->mresc_nu_numero_mv = "";
		$this->object->mresc_nu_numero = "";
		$this->object->Salvar();
		$this->assertEquals($numero + 1, $this->object->mresc_nu_numero);
	}

	public function testFabriquePelaEmpresaNumero()
	{
		$sql = "delete from resumo_cobranca where empr_id = 21 and resc_nu_numero_mv = 12345678";
		cAMBIENTE::ExecuteQuery($sql);

		$resumo = cresumo_cobranca::FabriquePelaEmpresaENumero(21, 12345678);
		$this->assertNotEmpty($resumo->mresc_id);

		$resc_id = $resumo->mresc_id;

		unset($resumo);
		$resumo = cresumo_cobranca::FabriquePelaEmpresaENumero(21, 12345678);
		$this->assertEquals($resumo->mresc_id, $resc_id);

	}
}
