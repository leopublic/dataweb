<?php
class cCANDIDATO_TMPTest extends PHPUnit_Framework_TestCase
{
	public function testPreenchedor(){
		$dr = new cCANDIDATO_TMP;
		$dr->mCO_USU_CADASTAMENTO = 38;
		$dr->salvar();
		$preenchedor = $dr->nome_email_preenchedor();
		$usuario = new cusuarios;
		$usuario->RecuperePeloId(38);
		$this->assertEquals($usuario->nm_email, $preenchedor['email']);
		$this->assertEquals($usuario->nome, $preenchedor['nome']);

		$conv = new cconvidado();
		$conv->memail = 'teste@testeemail.com.br';
		$conv->mnome = 'teste nome';
		$conv->Salvar();
		$dr->mconv_id = $conv->mconv_id;
		$dr->salvar();
		$preenchedor = $dr->nome_email_preenchedor();
		$this->assertEquals($conv->memail, $preenchedor['email']);
		$this->assertEquals($conv->mnome, $preenchedor['nome']);
	}

	public function testAtualizaHash(){
		$dr = new cCANDIDATO_TMP;
		$dr->salvar();
		$id = $dr->mnu_candidato_tmp;
		$dr->atualizaHash();

		unset($dr);

		$dr = new cCANDIDATO_TMP;
		$dr->RecuperarPeloTmp($id);
		$this->assertNotEmpty($dr->mhash_string);
		$this->assertEquals(date('d/m/Y'), $dr->mhash_dt_cadastro);

		$hash = $dr->mhash_string;
		$dr->atualizaHash();
		$this->assertEquals($hash, $dr->mhash_string);

		$dr->mhash_dt_cadastro = '2015-09-01';
		$dr->salvar();

		$dr->atualizaHash();
		$this->assertNotEquals($hash, $dr->mhash_string);
	}
	public function testGeraDr(){
		$cand = new cCANDIDATO_TMP;
		$cand->mnu_candidato_tmp = 9;
		$cand->RecuperarPeloTmp();
		$rep = new cREPOSITORIO_DR;
		$rep->geraCopiaDrEnviado($cand);

	}

}
