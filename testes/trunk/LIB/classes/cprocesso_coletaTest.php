<?php
class cprocesso_coletaTest extends PHPUnit_Framework_TestCase
{
	// Propriedades do cprocesso
	public	$xcodigo;
	public	$xcd_candidato;
	public	$xcodigo_processo_mte ;
	public	$xid_solicita_visto ;
	public	$xdt_cad;
	public	$xdt_ult;
	public	$xnu_servico;
	public	$xobservacao ;

	// Propriedades do cprocesso_cancel
	public $xno_validade;
	public $xnu_visto;
	public $xdt_emissao_visto;
	public $xno_local_emissao;
	public $xco_pais_nacionalidade_visto;
	public $xco_classificacao;
	public $xno_local_entrada;
	public $xco_uf;
	public $xdt_entrada;
	public $xnu_transporte_entrada;

	// Propriedades de rastreamento
	public $cd_usuario;
	public $no_classe;
	public $no_metodo;
	public $dt_ult;

	public $agora;

	/**
	 * @var cprocesso_coleta
	 */
	public $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		cSESSAO::$mcd_usuario = rand(1000000,9999999);
		$this->AtualizaTimeStamp();
		$this->InicializaObjeto();
		$this->InicializaValores();
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {

	}

	public function InicializaValores(){
		$this->xcd_candidato = rand(10,99);
		$this->xcodigo_processo_mte = rand(100000000,999999999);
		$this->xid_solicita_visto = rand(100000000,999999999);
		$this->xnu_servico = rand(1,50);

		$this->xno_validade = rand(100000000,999999999);
		$this->xnu_visto = rand(100000000,999999999);
		$this->xdt_emissao_visto = rotinasComuns::randData();
		$this->xno_local_emissao = rand(100000000,999999999);
		$this->xco_pais_nacionalidade_visto = rand(100000000,999999999);
		$this->xco_classificacao = rand(0,9);
		$this->xno_local_entrada = rand(100000000,999999999);
		$this->xco_uf = rand(1,99);
		$this->xdt_entrada= rotinasComuns::randData();
		$this->xnu_transporte_entrada = rand(100000000,999999999);
	}

	public function CarregaValores(){
		$this->object->mcd_candidato = $this->xcd_candidato;
		$this->object->mcodigo_processo_mte = $this->xcodigo_processo_mte;
		$this->object->mid_solicita_visto = $this->xid_solicita_visto;
		$this->object->mnu_servico = $this->xnu_servico;

		$this->object->mno_validade = $this->xno_validade;
		$this->object->mnu_visto = $this->xnu_visto;
		$this->object->mdt_emissao_visto = $this->xdt_emissao_visto;
		$this->object->mno_local_emissao = $this->xno_local_emissao;
		$this->object->mco_pais_nacionalidade_visto = $this->xco_pais_nacionalidade_visto;
		$this->object->mco_classificacao = $this->xco_classificacao;
		$this->object->mno_local_entrada= $this->xno_local_entrada;
		$this->object->mco_uf = $this->xco_uf;
		$this->object->mdt_entrada = $this->xdt_entrada;
		$this->object->mnu_transporte_entrada = $this->xnu_transporte_entrada;

	}

	public function TestaValores(){
		$this->assertEquals($this->xcodigo, $this->object->mcodigo);
		$this->assertEquals($this->xcd_candidato, $this->object->mcd_candidato);
		$this->assertEquals($this->xcodigo_processo_mte, $this->object->mcodigo_processo_mte, "Campo codigo_processo_mte");
		$this->assertEquals($this->xid_solicita_visto, $this->object->mid_solicita_visto, "Campo id_solicita_visto");
		$this->assertEquals($this->xnu_servico, $this->object->mnu_servico, "Campo nu_servico");

		$this->assertEquals($this->xno_validade, $this->object->mno_validade);
		$this->assertEquals($this->xnu_visto, $this->object->mnu_visto);
		$this->assertEquals($this->xdt_emissao_visto, $this->object->mdt_emissao_visto);
		$this->assertEquals($this->xno_local_emissao, $this->object->mno_local_emissao);
		$this->assertEquals($this->xco_pais_nacionalidade_visto, $this->object->mco_pais_nacionalidade_visto);
		$this->assertEquals($this->xco_classificacao, $this->object->mco_classificacao);
		$this->assertEquals($this->xno_local_entrada, $this->object->mno_local_entrada);
		$this->assertEquals($this->xco_uf, $this->object->mco_uf);
		$this->assertEquals($this->xdt_entrada, $this->object->mdt_entrada);
		$this->assertEquals($this->xnu_transporte_entrada, $this->object->mnu_transporte_entrada);
	}

	public function AtualizaTimeStamp(){
		$sql = "select date_format( now(), '%Y%m%d%T')";
		$res = conectaQuery($sql, __CLASS__.'->'.__FUNCTION__);
		if($rs = mysql_fetch_array($res)){
			$this->agora = $rs[0];
		}
	}
	public function InicializaObjeto($pCodigo = '' ){
		unset($this->object);
		$this->object = new cprocesso_coleta($pCodigo);
	}
	public function ObterDadosRastreamento($pCodigo){
		$sql = "select date_format( dt_ult, '%Y%m%d%T') dt_ult, cd_usuario,  no_classe, no_metodo from processo_coleta where codigo = ".$pCodigo;
		$res = conectaQuery($sql, __CLASS__.'->'.__FUNCTION__);
		$this->cd_usuario = 'nill';
		$this->no_classe = 'nill';
		$this->no_metodo = 'nill';
		$this->dt_ult = 'nill';
		if($rs = mysql_fetch_array($res)){
			$this->dt_ult = $rs['dt_ult'];
			$this->cd_usuario = $rs['cd_usuario'];
			$this->no_classe = $rs['no_classe'];
			$this->no_metodo = $rs['no_metodo'];
		}
	}



    public function testConstructComCodigo(){
		$this->CarregaValores();
		$this->object->Salve();
		$this->assertNotEmpty($this->object->mcodigo, "Chave vazia:".$this->object->mcodigo);
		$this->xcodigo = $this->object->mcodigo;

		$this->InicializaObjeto($this->xcodigo);
		$this->TestaValores();
    }

    public function testRecupereSe_ComParametro(){
		$this->CarregaValores();
		$this->object->Salve();
		$this->assertNotEmpty($this->object->mcodigo, "Chave vazia:".$this->object->mcodigo);
		$this->xcodigo = $this->object->mcodigo;

		$this->InicializaObjeto();
		$this->object->RecupereSe($this->xcodigo);
		$this->TestaValores();
    }

    public function testRecupereSe_SemParametro(){
		$this->CarregaValores();
		$this->object->Salve();
		$this->assertNotEmpty($this->object->mcodigo, "Chave vazia:".$this->object->mcodigo);
		$this->xcodigo = $this->object->mcodigo;

		$this->InicializaObjeto();
		$this->object->mcodigo = $this->xcodigo;
		$this->object->RecupereSe();
		$this->TestaValores();
    }
	/**
     * @expectedException        cERRO_CONSISTENCIA
	 */
    public function testRecupereSe_semCodigo(){
		$this->object->RecupereSe();
	}

	public function testRecupereSePelaOs(){
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO  =  rand(1000000,9999999);
		$os->mNU_SERVICO =  rand(1,100);
		$cand = new cCANDIDATO();
		$cand->Criar(rotinasComuns::randTexto(), rotinasComuns::randTexto(), rotinasComuns::randTexto(), cSESSAO::$mcd_usuario);
		$cand->AtualizeVistoAtual(rand(1000000,9999999));

		$this->InicializaObjeto();
		$this->object->SalvarDadosOs($os, $cand);
		$codigo = $this->object->mcodigo;

		$this->InicializaObjeto();
		$this->object->RecupereSePelaOs($os->mID_SOLICITA_VISTO, $cand->mNU_CANDIDATO);
		$this->assertEquals($codigo, $this->object->mcodigo);
		$this->assertEquals($os->mID_SOLICITA_VISTO, $this->object->mid_solicita_visto);
		$this->assertEquals($os->mNU_SERVICO, $this->object->mnu_servico);
		$this->assertEquals($cand->mNU_CANDIDATO, $this->object->mcd_candidato);
	}

	public function testSalve_IncluaNovo(){
		$this->CarregaValores();
		$this->object->Salve();
		$this->assertNotEmpty($this->object->mcodigo, "Chave vazia:".$this->object->mcodigo);

		$this->xcodigo = $this->object->mcodigo;
		$this->InicializaObjeto($this->xcodigo);

		$this->TestaValores();
		//
		// Testa rastreamento
		$this->ObterDadosRastreamento($this->xcodigo);
		$this->assertGreaterThanOrEqual($this->agora, $this->dt_ult, "Campo dt_ult");
		$this->assertEquals(cSESSAO::$mcd_usuario, $this->cd_usuario, "Campo cd_usuario");
		$this->assertEquals('cprocesso_coleta', $this->no_classe, "Campo no_classe");
		$this->assertEquals('IncluaNovo', $this->no_metodo, "Campo no_metodo");
	}

	public function testSalvarDadosOs_CriarNovo(){
		$this->AtualizaTimeStamp();
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO  =  rand(1000000,9999999);
		$os->mNU_SERVICO =  rand(1,100);
		$cand = new cCANDIDATO();
		$cand->Criar(rotinasComuns::randTexto(), rotinasComuns::randTexto(), rotinasComuns::randTexto(), cSESSAO::$mcd_usuario);

		$proc = new cprocesso_coleta();
		$proc->SalvarDadosOs($os, $cand);
		$codigo = $proc->mcodigo;
		unset($proc);
		$proc = new cprocesso_coleta($codigo);
		$this->assertEquals($os->mID_SOLICITA_VISTO, $proc->mid_solicita_visto);
		$this->assertEquals($os->mNU_SERVICO, $proc->mnu_servico);
		$this->assertEquals($cand->mNU_CANDIDATO, $proc->mcd_candidato);

		$this->ObterDadosRastreamento($codigo);
		$this->assertGreaterThanOrEqual($this->agora, $this->dt_ult, "Campo dt_ult");
		$this->assertEquals(cSESSAO::$mcd_usuario, $this->cd_usuario, "Campo cd_usuario");
		$this->assertEquals('cprocesso', $this->no_classe, "Campo no_classe");
		$this->assertEquals('IncluaNovoDadosOS', $this->no_metodo, "Campo no_metodo");
	}

	public function testSalvarDadosOs_AtualizeServico(){
		$this->AtualizaTimeStamp();
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO  =  rand(1000000,9999999);
		$os->mNU_SERVICO =  rand(1,100);
		$cand = new cCANDIDATO();
		$cand->Criar(rotinasComuns::randTexto(), rotinasComuns::randTexto(), rotinasComuns::randTexto(), cSESSAO::$mcd_usuario);

		$proc = new cprocesso_coleta();
		$proc->SalvarDadosOs($os, $cand);
		$codigo = $proc->mcodigo;

		$os->mNU_SERVICO = rand(1,100);
		$proc->SalvarDadosOs($os, $cand);

		unset($proc);
		$proc = new cprocesso_coleta($codigo);

		$this->assertEquals($os->mID_SOLICITA_VISTO, $proc->mid_solicita_visto);
		$this->assertEquals($os->mNU_SERVICO, $proc->mnu_servico);
		$this->assertEquals($cand->mNU_CANDIDATO, $proc->mcd_candidato);

		$this->ObterDadosRastreamento($codigo);
		$this->assertGreaterThanOrEqual($this->agora, $this->dt_ult, "Campo dt_ult");
		$this->assertEquals(cSESSAO::$mcd_usuario, $this->cd_usuario, "Campo cd_usuario");
		$this->assertEquals('cprocesso', $this->no_classe, "Campo no_classe");
		$this->assertEquals('AtualizeServico', $this->no_metodo, "Campo no_metodo");
	}

	public function testSalve_Atualize(){
		$this->CarregaValores();
		$this->object->Salve();

		$this->xcodigo = $this->object->mcodigo;

		// Testando a atualização de dados existentes
		$this->AtualizaTimeStamp();
		cSESSAO::$mcd_usuario = rand(1000000,9999999);
		$this->InicializaValores();
		$this->CarregaValores();

		$this->object->Salve();
		$this->InicializaObjeto($this->xcodigo);
		$this->TestaValores();
		//
		// Testa rastreamento
		$this->ObterDadosRastreamento($this->xcodigo);

		$this->assertGreaterThanOrEqual($this->agora, $this->dt_ult, "Campo dt_ult");
		$this->assertEquals(cSESSAO::$mcd_usuario, $this->cd_usuario, "Campo cd_usuario");
		$this->assertEquals('cprocesso_coleta', $this->no_classe, "Campo no_classe");
		$this->assertEquals('Atualize', $this->no_metodo, "Campo no_metodo");

	}

	public function testSalve_ProcessoEdit(){
		$this->CarregaValores();
		$this->object->Salve();
		$this->assertNotEmpty($this->object->mcodigo, "Chave vazia:".$this->object->mcodigo);

		$this->xcodigo = $this->object->mcodigo;

		// Testando a atualização de dados existentes via processo_edit

		$this->AtualizaTimeStamp();
		cSESSAO::$mcd_usuario = rand(1000000,9999999);

		$this->xno_validade = rand(100000000,999999999);
		$this->xnu_visto = rand(100000000,999999999);
		$this->xdt_emissao_visto = rotinasComuns::randData();
		$this->xno_local_emissao = rand(100000000,999999999);
		$this->xco_pais_nacionalidade_visto = rand(100000000,999999999);
		$this->xno_local_entrada = rand(100000000,999999999);
		$this->xco_uf = rand(100000000,999999999);
		$this->xdt_entrada= rotinasComuns::randData();
		$this->xnu_transporte_entrada = rand(100000000,999999999);

		$this->object->mno_validade = $this->xno_validade;
		$this->object->mnu_visto = $this->xnu_visto;
		$this->object->mdt_emissao_visto = $this->xdt_emissao_visto;
		$this->object->mno_local_emissao = $this->xno_local_emissao;
		$this->object->mco_pais_nacionalidade_visto = $this->xco_pais_nacionalidade_visto;
		$this->object->mno_local_entrada= $this->xno_local_entrada;
		$this->object->mco_uf = $this->xco_uf;
		$this->object->mdt_entrada = $this->xdt_entrada;
		$this->object->mnu_transporte_entrada = $this->xnu_transporte_entrada;

		$this->object->Salve_ProcessoEdit();
		$this->InicializaObjeto($this->xcodigo);

		$this->TestaValores();
		//
		// Testa rastreamento
		$this->ObterDadosRastreamento($this->xcodigo);

		$this->assertGreaterThanOrEqual($this->agora, $this->dt_ult, "Campo dt_ult");
		$this->assertEquals(cSESSAO::$mcd_usuario, $this->cd_usuario, "Campo cd_usuario");
		$this->assertEquals('cprocesso_coleta', $this->no_classe, "Campo no_classe");
		$this->assertEquals('Atualize_ProcessoEdit', $this->no_metodo, "Campo no_metodo");
	}

	public function testSalve_ProcessoMte(){
		$this->CarregaValores();
		$this->object->Salve();

		$this->xcodigo = $this->object->mcodigo;

		// Testando a atualização de dados existentes
		$this->xcodigo_processo_mte = rand(1000000,9999999);
		$this->object->mcodigo_processo_mte = $this->xcodigo_processo_mte;
		$this->AtualizaTimeStamp();
		cSESSAO::$mcd_usuario = rand(1000000,9999999);
		$this->object->Salve_ProcessoMte();

		$this->InicializaObjeto($this->xcodigo);
		$this->TestaValores();
		//
		// Testa rastreamento
		$this->ObterDadosRastreamento($this->xcodigo);

		$this->assertGreaterThanOrEqual($this->agora, $this->dt_ult, "Campo dt_ult");
		$this->assertEquals(cSESSAO::$mcd_usuario, $this->cd_usuario, "Campo cd_usuario");
		$this->assertEquals('cprocesso', $this->no_classe, "Campo no_classe");
		$this->assertEquals('Salve_ProcessoMte', $this->no_metodo, "Campo no_metodo");
	}


	public function testAtualizeServico(){
		$this->CarregaValores();
		$this->object->Salve();
		$this->assertNotEmpty($this->object->mcodigo, "Chave vazia:".$this->object->mcodigo);

		$this->xcodigo = $this->object->mcodigo;

		$this->AtualizaTimeStamp();
		cSESSAO::$mcd_usuario = rand(1000000,9999999);
		$this->xnu_servico = rand(1,99);
		$this->object->mnu_servico = $this->xnu_servico;
		$this->object->AtualizeServico();

		$this->InicializaObjeto($this->xcodigo);
		$this->TestaValores();
		//
		// Testa rastreamento
		$this->ObterDadosRastreamento($this->xcodigo);

		$this->assertGreaterThanOrEqual($this->agora, $this->dt_ult, "Campo dt_ult");
		$this->assertEquals(cSESSAO::$mcd_usuario, $this->cd_usuario, "Campo cd_usuario");
		$this->assertEquals('cprocesso', $this->no_classe, "Campo no_classe");
		$this->assertEquals('AtualizeServico', $this->no_metodo, "Campo no_metodo");
	}

}
