<?php

/**
 * Test class for cORDEMSERVICO_PACOTE.
 * Generated by PHPUnit on 2012-03-23 at 17:43:13.
 */
class cORDEMSERVICO_PACOTETest extends PHPUnit_Framework_TestCase {

	/**
	 * @var cORDEMSERVICO_PACOTE
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new cORDEMSERVICO_PACOTE;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {

	}

	public function testPacoteQueAbre()
	{
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO = 0;
		$os->mNU_EMPRESA = 12;
		$os->mNU_SERVICO = 1;
		$ospacote = cORDEMSERVICO_PACOTE::pacoteAplicavel($os);
		$this->assertEquals(143, $ospacote->mnu_servico_pacote);
	}

	public function testPacoteOndeEsseServicoEstaIncluido()
	{
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO = 0;
		$os->mNU_EMPRESA = 12;
		$os->mNU_SERVICO = 1;

		$nu_servico_pacote = cORDEMSERVICO_PACOTE::PacoteQueContemEsseServico(12, 1);
		$this->assertEquals(null, $nu_servico_pacote, "Servico 1 na empresa 12");

		$nu_servico_pacote = cORDEMSERVICO_PACOTE::PacoteQueContemEsseServico(12, 22);
		$this->assertEquals(143, $nu_servico_pacote, "Servico 22 na empresa 12");

		$nu_servico_pacote = cORDEMSERVICO_PACOTE::PacoteQueContemEsseServico(217, 1);
		$this->assertEquals(null, $nu_servico_pacote, "Servico 1 na empresa 217");

		$nu_servico_pacote = cORDEMSERVICO_PACOTE::PacoteQueContemEsseServico(217, 22);
		$this->assertEquals(null, $nu_servico_pacote, "Servico 22 na empresa 217");
	}

	public function testRecuperaOsDoPacoteQueJaTemOServico()
	{
		$os = new cORDEMSERVICO();
		$os->mID_SOLICITA_VISTO = 0;
		$os->mNU_EMPRESA = 12;
		$os->mNU_SERVICO = 1;

		$id_solicita_visto = cORDEMSERVICO_PACOTE::RecuperaOsDoPacoteQueJaTemOServico( 143, 20, 12, 29627);
		$this->assertContains(66233, $id_solicita_visto);
		$this->assertContains(66236, $id_solicita_visto);
		$this->assertContains(66240, $id_solicita_visto);

	}

	public function testOsPacoteLivre()
	{
		$id_solicita_visto = cORDEMSERVICO_PACOTE::OsPacoteLivre(143, 12, 29627, array(66233, 66240, 66236));
		$this->assertEquals(66241, $id_solicita_visto);

		$id_solicita_visto = cORDEMSERVICO_PACOTE::OsPacoteLivre(143, 12, 29627, array(66233, 66241, 66236));
		$this->assertEquals(66240, $id_solicita_visto);
	}
}
