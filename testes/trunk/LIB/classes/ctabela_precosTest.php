<?php
/**
 * Test class for ctabela_precos.
 */
class ctabela_precosTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var ctabela_precos
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new ctabela_precos();
	}
	
	/**
	 * @return ctabela_precos
	 */
	public static function Stub(){
		$sql = "select * from tabela_precos limit ".  rotinasComuns::randValor(1,5).",1";
		$res = cAMBIENTE::ConectaQuery($sql, __CLASS__.".".__FUNCTION__ );
		$rs = $res->fetch(PDO::FETCH_ASSOC);
		$obj = new ctabela_precos();
		$obj->RecuperePeloId($rs['tbpc_id']);
		
		return $obj;
	}
	
	public function testCriacaoStub(){
		$emp = $this->Stub();
		$this->assertNotEmpty($emp->mtbpc_id);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {

	}


	/**
	 * @covers ctabela_precos::campoid
	 */
	public function testcampoid() {
		$this->object = $this::Stub();
		$this->assertEquals('tbpc_id', $this->object->campoid());
	}

	/**
	 * @covers ctabela_precos::setId
	 */
	public function testSetid() {
		$id = rotinasComuns::randValor();
		$this->object = $this::Stub();
		$this->object->setId($id);
		$this->assertEquals($id, $this->object->mtbpc_id);
	}

	/**
	 * @covers ctabela_precos::getId
	 */
	public function testGetid() {
		$id = rotinasComuns::randValor();
		$this->object = $this::Stub();
		$this->object->mtbpc_id = $id;
		$this->assertEquals($id, $this->object->getId());
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testSql_Liste().
	 */
	public function testSql_Liste() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testSql_RecuperePeloId().
	 */
	public function testSql_RecuperePeloId() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testIncluir().
	 */
	public function testIncluir() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testAtualizar().
	 */
	public function testAtualizar() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	public function testInstanciePelaEmpresa(){
		$this->object = $this::Stub();
		$emp = cEMPRESATest::Stub();
		$tbpc_id = $this->object->mtbpc_id;
		$emp->mtbpc_id = $tbpc_id;
		$emp->Atualizar();
		$this->object = ctabela_precos::InstanciePelaEmpresa($emp->mNU_EMPRESA);
		$this->assertEquals($tbpc_id, $this->object->mtbpc_id);

		$emp->mNU_EMPRESA = "";
		$this->object = ctabela_precos::InstanciePelaEmpresa($emp->mNU_EMPRESA);
		$this->assertEquals("", $this->object->mtbpc_id);
	}
}
