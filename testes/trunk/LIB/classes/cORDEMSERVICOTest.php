<?php

class cORDEMSERVICOTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var cORDEMSERVICO
	 */
	public $object;

	/**
	 * @var cEMPRESA
	 */
	public $emp;

	/**
	 * @var cEMBARCACAO_PROJETO
	 */
	public $proj;
	public $xNU_SOLICITACAO;
	public $xNU_EMPRESA;
	public $xNU_EMBARCACAO_PROJETO;
	public $xcd_tecnico;
	public $xdt_solicitacao;
	public $xde_observacao;
	public $xno_solicitador;
	public $xcd_libs_finam;
	public $xCandidatos;
	public $xnome_cadastro;
	public $xID_STATUS_SOL;
	public $xReadonly;
	public $xfl_nao_conformidade;
	public $xDT_PRAZO_ESTADA_SOLICITADO;
	public $xnu_empresa_requerente;
	public $xfl_cobrado;
	public $xNU_SERVICO;
	public $xNU_FORMULARIOS;
	public $xNU_FORMULARIOS_OS;
	public $xNU_FORMULARIOS_CAND;
	public $xfl_liberacao_fechada;
	public $xfl_qualidade_fechada;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new cORDEMSERVICO();
		$this->emp = new cEMPRESA();
		$this->emp->mNO_RAZAO_SOCIAL = rotinasComuns::randTexto();
		$this->emp->Salvar();
		$this->proj = new cEMBARCACAO_PROJETO();
		$this->proj->mNU_EMPRESA = $this->emp->mNU_EMPRESA;
		$this->proj->mNO_EMBARCACAO_PROJETO = rotinasComuns::randTexto();
		$this->proj->Salve();
	}

	public static function Stub() {
		$obj = new cORDEMSERVICO();
		$emp = new cEMPRESA();
		$emp->mNO_RAZAO_SOCIAL = rotinasComuns::randTexto();
		$emp->Salvar();
		$proj = new cEMBARCACAO_PROJETO();
		$proj->mNU_EMPRESA = $emp->mNU_EMPRESA;
		$proj->mNO_EMBARCACAO_PROJETO = rotinasComuns::randTexto();
		$proj->Salve();
		$obj->mNU_EMPRESA = $emp->mNU_EMPRESA;
		$obj->mNU_EMBARCACAO_PROJETO = $proj->mNU_EMBARCACAO_PROJETO;
		$serv = cSERVICOTest::Stub();
		$obj->mNU_SERVICO = $serv->mNU_SERVICO;
		$obj->SalvarSolicitaVisto();

		return $obj;
	}

	public function testCriacaoStub() {
		$this->object = self::Stub();
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	public function InicializaValores() {
		$this->xNU_SOLICITACAO = rand(1000000, 9999999);
		$this->xNU_EMPRESA = $this->emp->mNU_EMPRESA;
		$this->xNU_EMBARCACAO_PROJETO = $this->proj->mNU_EMBARCACAO_PROJETO;
		$this->xcd_tecnico = rand(1000000, 9999999);
		$this->xdt_solicitacao = rotinasComuns::randData();
		$this->xde_observacao = rotinasComuns::randTexto(20);
		$this->xno_solicitador = rotinasComuns::randTexto(20);
		$this->xcd_libs_finam = rand(1000000, 9999999);
		$this->xnome_cadastro = rotinasComuns::randTexto(20);
		$this->xID_STATUS_SOL = rand(1000000, 9999999);
		$this->xReadonly = rand(10, 99);
		$this->xfl_nao_conformidade = rand(10, 99);
		$this->xDT_PRAZO_ESTADA_SOLICITADO = rotinasComuns::randData();
		$this->xnu_empresa_requerente = rand(1000000, 9999999);
		$this->xfl_cobrado = rand(10, 99);
		$this->xNU_SERVICO = rand(1000000, 9999999);
		$this->xNU_FORMULARIOS = rand(1000000, 9999999);
		$this->xNU_FORMULARIOS_OS = rand(1000000, 9999999);
		$this->xNU_FORMULARIOS_CAND = rand(1000000, 9999999);
		$this->xfl_liberacao_fechada = rand(10, 99);
		$this->xfl_qualidade_fechada = rand(10, 99);
	}

	public function CarregaValores() {
		$this->object->mNU_SOLICITACAO = $this->xNU_SOLICITACAO;
		$this->object->mNU_EMPRESA = $this->xNU_EMPRESA;
		$this->object->mNU_EMBARCACAO_PROJETO = $this->xNU_EMBARCACAO_PROJETO;
		$this->object->mcd_tecnico = $this->xcd_tecnico;
		$this->object->mdt_solicitacao = $this->xdt_solicitacao;
		$this->object->mde_observacao = $this->xde_observacao;
		$this->object->mno_solicitador = $this->xno_solicitador;
		$this->object->mcd_libs_finam = $this->xcd_libs_finam;
		$this->object->mCandidatos = $this->xCandidatos;
		$this->object->mnome_cadastro = $this->xnome_cadastro;
		$this->object->mID_STATUS_SOL = $this->xID_STATUS_SOL;
		$this->object->mReadonly = $this->xReadonly;
		$this->object->mfl_nao_conformidade = $this->xfl_nao_conformidade;
		$this->object->mDT_PRAZO_ESTADA_SOLICITADO = $this->xDT_PRAZO_ESTADA_SOLICITADO;
		$this->object->mnu_empresa_requerente = $this->xnu_empresa_requerente;
		$this->object->mfl_cobrado = $this->xfl_cobrado;
		$this->object->mNU_SERVICO = $this->xNU_SERVICO;
		$this->object->mNU_FORMULARIOS = $this->xNU_FORMULARIOS;
		$this->object->mNU_FORMULARIOS_OS = $this->xNU_FORMULARIOS_OS;
		$this->object->mNU_FORMULARIOS_CAND = $this->xNU_FORMULARIOS_CAND;
		$this->object->mfl_liberacao_fechada = $this->xfl_liberacao_fechada;
		$this->object->mfl_qualidade_fechada = $this->xfl_qualidade_fechada;
	}

	public function TestaValores() {
		$this->assertEquals($this->xID_SOLICITA_VISTO, $this->object->mID_SOLICITA_VISTO);
		$this->assertEquals($this->xNU_SOLICITACAO, $this->object->mNU_SOLICITACAO);
		$this->assertEquals($this->xNU_EMPRESA, $this->object->mNU_EMPRESA);
		$this->assertEquals($this->xNU_EMBARCACAO_PROJETO, $this->object->mNU_EMBARCACAO_PROJETO);
		$this->assertEquals($this->xcd_tecnico, $this->object->mcd_tecnico);
		$this->assertEquals($this->xdt_solicitacao, $this->object->mdt_solicitacao);
		$this->assertEquals($this->xde_observacao, $this->object->mde_observacao);
		$this->assertEquals($this->xno_solicitador, $this->object->mno_solicitador);
		$this->assertEquals($this->xcd_libs_finam, $this->object->mcd_libs_finam);
		$this->assertEquals($this->xCandidatos, $this->object->mCandidatos);
		$this->assertEquals($this->xnome_cadastro, $this->object->mnome_cadastro);
		$this->assertEquals($this->xID_STATUS_SOL, $this->object->mID_STATUS_SOL);
		$this->assertEquals($this->xReadonly, $this->object->mReadonly);
		$this->assertEquals($this->xfl_nao_conformidade, $this->object->mfl_nao_conformidade);
		$this->assertEquals($this->xDT_PRAZO_ESTADA_SOLICITADO, $this->object->mDT_PRAZO_ESTADA_SOLICITADO);
		$this->assertEquals($this->xnu_empresa_requerente, $this->object->mnu_empresa_requerente);
		$this->assertEquals($this->xfl_cobrado, $this->object->mfl_cobrado);
		$this->assertEquals($this->xNU_SERVICO, $this->object->mNU_SERVICO);
		$this->assertEquals($this->xNU_FORMULARIOS, $this->object->mNU_FORMULARIOS);
		$this->assertEquals($this->xNU_FORMULARIOS_OS, $this->object->mNU_FORMULARIOS_OS);
		$this->assertEquals($this->xNU_FORMULARIOS_CAND, $this->object->mNU_FORMULARIOS_CAND);
		$this->assertEquals($this->xfl_liberacao_fechada, $this->object->mfl_liberacao_fechada);
		$this->assertEquals($this->xfl_qualidade_fechada, $this->object->mfl_qualidade_fechada);
	}

	public function InicializaObjeto($pCodigo = '') {
		unset($this->object);
		$this->object = new cORDEMSERVICO();
	}

	public function testAvancarStatus() {
		$OS = new cORDEMSERVICO();
		$OS->Recuperar(11550);
		$OS->mID_STATUS_SOL = 1;
		$OS->AvancarStatus();
		$this->assertEquals(4, $OS->mID_STATUS_SOL, 'Erro !!!->');
		$OS->mID_STATUS_SOL = 2;
		$OS->AvancarStatus();
		$this->assertEquals(4, $OS->mID_STATUS_SOL, 'Erro !!!->');
		$OS->mID_STATUS_SOL = 3;
		$OS->AvancarStatus();
		$this->assertEquals(4, $OS->mID_STATUS_SOL, 'Erro !!!->');
		$OS->mID_STATUS_SOL = 4;
		$OS->AvancarStatus();
		$this->assertEquals(5, $OS->mID_STATUS_SOL, 'Erro !!!->');
		$OS->mID_STATUS_SOL = 5;
		$OS->AvancarStatus();
		$this->assertEquals(6, $OS->mID_STATUS_SOL, 'Erro !!!->');
		$OS->mID_STATUS_SOL = 6;
		$OS->AvancarStatus();
		$this->assertEquals(6, $OS->mID_STATUS_SOL, 'Erro !!!->');
	}

	public function testcampoId() {
		$os = new cORDEMSERVICO();
		$this->assertEquals('id_solicita_visto', $os->campoid());
		$this->assertEquals('id_solicita_visto', $os->get_nomeCampoChave());
		$this->assertEquals('solicita_visto', $os->get_nomeTabela());
		$this->assertEquals('solicita_visto', $os->get_nomeTabelaBD());
	}

	public function testAbrir() {
		$OS = new cORDEMSERVICO();
		$OS->Recuperar(11550);
		$sql = "update solicita_visto  set ID_STATUS_SOL = 6, fl_liberado = 1 , fl_conclusao_conforme = 1 where id_solicita_visto = 11550";
		executeQuery($sql, __FILE__);
		$OS = new cORDEMSERVICO();
		$OS->Recuperar(11550);
		$this->assertEquals(6, $OS->mID_STATUS_SOL, 'Erro !!!->');
		$OS->Abrir();
		$OS = new cORDEMSERVICO();
		$OS->Recuperar(11550);
		$this->assertEquals(4, $OS->mID_STATUS_SOL, 'Erro !!!->');
	}

	/**
	 * @expectedException        cERRO_CONSISTENCIA
	 */
	public function testConsisteCamposObrigatorios() {

		$this->object->SalvarInstancia();
		$this->object->mNU_EMPRESA = rand(10000, 99999);
		$this->object->SalvarInstancia();
		$this->object->mNU_EMBARCACAO_PROJETO = rand(10000, 99999);
		$this->object->SalvarInstancia();
		$this->object->mNU_SERVICO = rand(10000, 99999);
		$this->object->SalvarInstancia();
		$this->object->mCandidatos = "10101";
	}

	public function testSalvarInstancia() {
		//Testa consistencias do salvamento da instancia
		$os = $this::Stub();
	}

	public function testExcluir() {
		
	}

//	public function testMigre(){
//		$this->InicializaObjeto();
//		$this->InicializaValores();
//		$candOrig = new cCANDIDATO();
//		$candOrig->CriarNomeCompleto(rotinasComuns::randTexto(), cSESSAO::$mcd_usuario);
//		$this->xCandidatos = $candOrig->mNU_CANDIDATO;
//		$sql = "select * from SERVICO where ID_TIPO_ACOMPANHAMENTO = 3";
//		$res = conectaQuery($sql, __CLASS__.'->'.__FUNCTION__);
//		if($rs = mysql_fetch_array($res)){
//			$this->xNU_SERVICO = $rs['NU_SERVICO'];
//		}
//		$this->CarregaValores();
//		$this->object->mID_SOLICITA_VISTO = 0;
//		$this->object->mNU_USUARIO_CAD = cSESSAO::$mcd_usuario;
//		$this->object->SalvarInstancia();
//	}
	/**
	 * 
	 */
	public function testLibereParaCobrancaEmLote() {
		$this->markTestSkipped('must be revisited.');

		$this->InicializaValores();
		$this->CarregaValores();

		$this->object->SalvarSolicitaVisto();

		$ids = "";
		$id1 = $this->object->mID_SOLICITA_VISTO;
		$ids .= $id1;
		$this->InicializaObjeto();
		$this->InicializaValores();
		$this->CarregaValores();
		$this->object->SalvarSolicitaVisto();
		$id2 = $this->object->mID_SOLICITA_VISTO;
		$ids .= "," . $id2;
		$sql = "update solicita_visto set stco_id = 1 where id_solicita_visto in (" . $ids . ")";
		cAMBIENTE::$db_pdo->exec($sql);

		cORDEMSERVICO::LibereParaCobrancaEmLote('1', $ids);

		$this->InicializaObjeto();
		$this->object->Recuperar($id1);
		$this->object->RecuperarSolicitaVistoCobranca();
		$this->assertEquals(2, $this->object->mstco_id);

		$this->InicializaObjeto();
		$this->object->Recuperar($id2);
		$this->object->RecuperarSolicitaVistoCobranca();
		$this->assertEquals(2, $this->object->mstco_id);
	}

	/**
	 * 
	 */
	public function testsqlAtualizaAtributo() {
		$obj = $this->Stub();
		$valorAnt = rotinasComuns::randValor();
		$obj->msoli_vl_cobrado = $valorAnt;
		$obj->SalvarCobranca();
		$valorNovo = rotinasComuns::randValor();
		$chave = '[{"campo":"id_solicita_visto","valor":"47789"}]';
		$obj->AtualizeAtributo("soli_vl_cobrado", $valorNovo, 5, $chave, 38);
	}

	/**
	 * Verifica se o salvamento da OS está importando os dados da tabela de preços corretamente
	 */
	public function testSalvarSolicitaVisto_ImportacaoDaTabelaDePrecos() {
		// Salvar uma OS sem preço de uma empresa com tabela de preços e verificar que o preço 
		// e a descrição do serviço foram atualizados
		// Monta empresa com tabela de preços para o servico
		$OS = self::Stub();
		$tabela = ctabela_precosTest::Stub();
		$empresa = new cEMPRESA();
		$empresa->mNU_EMPRESA = $OS->mNU_EMPRESA;
		$empresa->RecuperePeloId();
		$empresa->mtbpc_id = $tabela->mtbpc_id;

		$preco = new cpreco();
		$preco->RecuperePelaTabelaServico($tabela->mtbpc_id, $OS->mNU_SERVICO);
		$valor_conceito = rotinasComuns::randValor(10, 1000);
		$preco->mprec_vl_conceito = $valor_conceito;
		$valor_pacote = rotinasComuns::randValor(10, 1000);
		$preco->mprec_vl_pacote = $valor_pacote;
		$preco->Salvar();

		$obj->msoli_vl_cobrado = $valorAnt;
		$OS->SalvarCobranca();

		// Alterar o serviço da OS e verificar que o preco 
		// e a descricao do serviço foram atualizados
		// Alterar o preco da tabela de precos, salvar a OS e verificar que o preco nao foi alterado 
	}

	public function testSalvarCobranca_ImportacaoTabelaPrecos() {
		$x = new cORDEMSERVICO();
	}

	public function testAtualizarAtributoCobranca_ImportacaoTabelaPrecos() {
		
	}

	public function test_get_NU_EMBARCACAO_PROJETO_REAL() {
		$os = new cORDEMSERVICO();
		$os->mNU_EMBARCACAO_PROJETO = 10;
		$os->mNU_EMBARCACAO_PROJETO_COBRANCA = 20;
		$this->assertEquals(20, $os->get_NU_EMBARCACAO_PROJETO_Real());

		$os->mNU_EMBARCACAO_PROJETO = 10;
		$os->mNU_EMBARCACAO_PROJETO_COBRANCA = '';
		$this->assertEquals(10, $os->get_NU_EMBARCACAO_PROJETO_Real());
	}

	public function test_get_AfetaEmpresaProjeto()
	{
		$processo = new cprocesso_mte();
		$processo->mfl_visto_atual = 1;
		$os = new cORDEMSERVICO();
		$os->mID_TIPO_ACOMPANHAMENTO = 1;
		$this->assertEquals(true, $os->get_AfetaEmpresaProjeto($processo));

		$processo->mfl_visto_atual = 0;
		$os->mID_TIPO_ACOMPANHAMENTO = 1;
		$this->assertEquals(false, $os->get_AfetaEmpresaProjeto($processo));

		$os->mID_TIPO_ACOMPANHAMENTO = 2;
		$this->assertEquals(false, $os->get_AfetaEmpresaProjeto($processo));

		$os->mID_TIPO_ACOMPANHAMENTO = 3;
		$this->assertEquals(true, $os->get_AfetaEmpresaProjeto($processo));

		$os->mID_TIPO_ACOMPANHAMENTO = 4;
		$this->assertEquals(true, $os->get_AfetaEmpresaProjeto($processo));

		$os->mID_TIPO_ACOMPANHAMENTO = 5;
		$this->assertEquals(true, $os->get_AfetaEmpresaProjeto($processo));

		$os->mID_TIPO_ACOMPANHAMENTO = 6;
		$this->assertEquals(false, $os->get_AfetaEmpresaProjeto($processo));

		$os->mID_TIPO_ACOMPANHAMENTO = 7;
		$this->assertEquals(false, $os->get_AfetaEmpresaProjeto($processo));
		
		$os->mID_TIPO_ACOMPANHAMENTO = 8;
		$this->assertEquals(false, $os->get_AfetaEmpresaProjeto($processo));

		$os->mID_TIPO_ACOMPANHAMENTO = '';
		$this->assertEquals(false, $os->get_AfetaEmpresaProjeto($processo));
		
	}
	
}
