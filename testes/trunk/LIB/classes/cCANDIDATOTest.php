<?php
class cCANDIDATOTest extends PHPUnit_Framework_TestCase{
	/**
	 *
	 * @var cCANDIDATO
	 */
	public $object;

	public $xNO_PRIMEIRO_NOME;
	public $xNO_NOME_MEIO;
	public $xNO_ULTIMO_NOME;
	public $xNOME_COMPLETO;
	public $xNO_EMAIL_CANDIDATO;
	public $xNO_ENDERECO_RESIDENCIA;
	public $xNO_CIDADE_RESIDENCIA;
	public $xCO_PAIS_RESIDENCIA;
	public $xNU_TELEFONE_CANDIDATO;
	public $xNO_EMPRESA_ESTRANGEIRA;
	public $xNO_ENDERECO_EMPRESA;
	public $xNO_CIDADE_EMPRESA;
	public $xCO_PAIS_EMPRESA;
	public $xNU_TELEFONE_EMPRESA;
	public $xNO_PAI;
	public $xNO_MAE;
	public $xCO_NACIONALIDADE;
	public $xCO_NIVEL_ESCOLARIDADE;
	public $xCO_ESTADO_CIVIL;
	public $xCO_SEXO;
	public $xDT_NASCIMENTO;
	public $xNO_LOCAL_NASCIMENTO;
	public $xNU_PASSAPORTE;
	public $xDT_EMISSAO_PASSAPORTE;
	public $xDT_VALIDADE_PASSAPORTE;
	public $xCO_PAIS_EMISSOR_PASSAPORTE;
	public $xNU_RNE;
	public $xCO_PROFISSAO_CANDIDATO;
	public $xTE_TRABALHO_ANTERIOR_BRASIL;
	public $xTE_DESCRICAO_ATIVIDADES;
	public $xNU_CPF;
	public $xDT_CADASTRAMENTO;
	public $xCO_USU_CADASTAMENTO;
	public $xDT_ULT_ALTERACAO;
	public $xCO_USU_ULT_ALTERACAO;
	public $xNO_ENDERECO_ESTRANGEIRO;
	public $xNO_CIDADE_ESTRANGEIRO;
	public $xCO_PAIS_ESTRANGEIRO;
	public $xNU_TELEFONE_ESTRANGEIRO;
	public $xNU_EMPRESA;
	public $xNU_EMBARCACAO_PROJETO;
	public $xNU_CTPS;
	public $xNU_CNH;
	public $xDT_EXPIRACAO_CNH;
	public $xDT_EXPIRACAO_CTPS;
	public $xcodigo_processo_mte_atual;
	public $xNO_LOGIN;
	public $xNO_SENHA;
	public $xFL_ATUALIZACAO_HABILITADA;
	public $xFL_HABILITAR_CV;
	public $xNU_ORDEM_CREWLIST;
	public $xFL_EMBARCADO;
	public $xFL_INATIVO;
	public $xTE_OBSERVACAO_CREWLIST;
	public $xTE_EXPERIENCIA_ANTERIOR;
	public $xFL_EDICAO_CONCLUIDA;
	public $xVA_REMUNERACAO_MENSAL;
	public $xVA_REMUNERACAO_MENSAL_BRASIL;
	public $xCO_PAIS_SEAMAN;
	public $xNO_SEAMAN;
	public $xDT_EMISSAO_SEAMAN;
	public $xDT_VALIDADE_SEAMAN;
	public $xCO_FUNCAO;
	public $xCO_LOCAL_EMBARCACAO_PROJETO;

	protected function setUp() {
		$this->object = new cCANDIDATO();
	}

	public static function Stub(){
		$sql = "select nu_candidato from candidato limit ".rotinasComuns::randValor(20000, 30000).",1";
		$res = cAMBIENTE::ConectaQuery($sql,__CLASS__.".".__FUNCTION__);
		$rs = $res->fetch(PDO::FETCH_ASSOC );
		$obj = new cCANDIDATO();
		$obj->Recuperar($rs['nu_candidato']);
		return $obj;
	}
			
	public function testCriacaoStub(){
		$this->object = self::Stub();
		$this->assertNotEmpty($this->object->mNU_CANDIDATO);
		$this->assertNotEmpty($this->object->mNOME_COMPLETO);
	}

	public function InicializeValoresTeste(){
		$this->xNO_PRIMEIRO_NOME = rotinasComuns::randTexto();
		$this->xNO_NOME_MEIO = rotinasComuns::randTexto();
		$this->xNO_ULTIMO_NOME = rotinasComuns::randTexto();
		$this->xNOME_COMPLETO = rotinasComuns::randTexto();
		$this->xNO_EMAIL_CANDIDATO = rotinasComuns::randTexto();
		$this->xNO_ENDERECO_RESIDENCIA = rotinasComuns::randTexto();
		$this->xNO_CIDADE_RESIDENCIA = rotinasComuns::randTexto();
		$this->xCO_PAIS_RESIDENCIA = rotinasComuns::randValor(1,10);
		$this->xNU_TELEFONE_CANDIDATO = rotinasComuns::randTexto();
		$this->xNO_EMPRESA_ESTRANGEIRA = rotinasComuns::randTexto();
		$this->xNO_ENDERECO_EMPRESA = rotinasComuns::randTexto();
		$this->xNO_CIDADE_EMPRESA = rotinasComuns::randTexto();
		$this->xCO_PAIS_EMPRESA = rotinasComuns::randValor(1,10);
		$this->xNU_TELEFONE_EMPRESA = rotinasComuns::randTexto();
		$this->xNO_PAI = rotinasComuns::randTexto();
		$this->xNO_MAE = rotinasComuns::randTexto();
		$this->xCO_NACIONALIDADE = rotinasComuns::randValor(1,10);
		$this->xCO_NIVEL_ESCOLARIDADE = rotinasComuns::randValor(1,10);
		$this->xCO_ESTADO_CIVIL = rotinasComuns::randValor(1,9);
		$this->xCO_SEXO = rotinasComuns::randValor(1,2);
		$this->xDT_NASCIMENTO = rotinasComuns::randData();
		$this->xNO_LOCAL_NASCIMENTO = rotinasComuns::randTexto();
		$this->xNU_PASSAPORTE = rotinasComuns::randTexto();
		$this->xDT_EMISSAO_PASSAPORTE = rotinasComuns::randData();
		$this->xDT_VALIDADE_PASSAPORTE = rotinasComuns::randData();
		$this->xCO_PAIS_EMISSOR_PASSAPORTE = rotinasComuns::randValor(1,10);
		$this->xNU_RNE = rotinasComuns::randTexto();
		$this->xCO_PROFISSAO_CANDIDATO = rotinasComuns::randValor(1,10);
		$this->xTE_TRABALHO_ANTERIOR_BRASIL = rotinasComuns::randTexto();
		$this->xTE_DESCRICAO_ATIVIDADES = rotinasComuns::randTexto();
		$this->xNU_CPF = rotinasComuns::randTexto();
		$this->xDT_CADASTRAMENTO = rotinasComuns::randData();
		$this->xCO_USU_CADASTAMENTO = rotinasComuns::randValor(1,10);
		$this->xDT_ULT_ALTERACAO = rotinasComuns::randData();
		$this->xCO_USU_ULT_ALTERACAO = rotinasComuns::randValor(1,10);
		$this->xNO_ENDERECO_ESTRANGEIRO = rotinasComuns::randTexto();
		$this->xNO_CIDADE_ESTRANGEIRO = rotinasComuns::randTexto();
		$this->xCO_PAIS_ESTRANGEIRO = rotinasComuns::randValor(1,10);
		$this->xNU_TELEFONE_ESTRANGEIRO = rotinasComuns::randTexto();
		$this->xNU_EMPRESA = rotinasComuns::randValor(1,10);
		$this->xNU_EMBARCACAO_PROJETO = rotinasComuns::randValor(1,10);
		$this->xNU_CTPS = rotinasComuns::randTexto();
		$this->xNU_CNH = rotinasComuns::randTexto();
		$this->xDT_EXPIRACAO_CNH = rotinasComuns::randData();
		$this->xDT_EXPIRACAO_CTPS = rotinasComuns::randData();
		$this->xcodigo_processo_mte_atual = rotinasComuns::randValor(1000,9999);
		$this->xNO_LOGIN = rotinasComuns::randTexto();
		$this->xNO_SENHA = rotinasComuns::randTexto();
		$this->xFL_ATUALIZACAO_HABILITADA = rotinasComuns::randValor(0,1);
		$this->xFL_HABILITAR_CV = rotinasComuns::randValor(0,1);
		$this->xNU_ORDEM_CREWLIST = rotinasComuns::randValor(1,10);
		$this->xFL_INATIVO = rotinasComuns::randValor(0,1);
		$this->xTE_OBSERVACAO_CREWLIST = rotinasComuns::randTexto();
		$this->xTE_EXPERIENCIA_ANTERIOR = rotinasComuns::randTexto();
		$this->xFL_EDICAO_CONCLUIDA = rotinasComuns::randValor(0,1);
		$this->xVA_REMUNERACAO_MENSAL = rotinasComuns::randValor();
		$this->xVA_REMUNERACAO_MENSAL_BRASIL = rotinasComuns::randValor();
		$this->xCO_PAIS_SEAMAN = rotinasComuns::randValor(1,10);
		$this->xNO_SEAMAN = rotinasComuns::randTexto();
		$this->xDT_EMISSAO_SEAMAN = rotinasComuns::randData();
		$this->xDT_VALIDADE_SEAMAN = rotinasComuns::randData();
		$this->xCO_FUNCAO = rotinasComuns::randValor(1,10);
		$this->xCO_LOCAL_EMBARCACAO_PROJETO = rotinasComuns::randValor(1,10);
	}

	public function TestaDiferenca($obj1, $obj2){
		$this->assertNotEquals($obj1->mNO_PRIMEIRO_NOME                           	,$obj2->mNO_PRIMEIRO_NOME                         	, 'teste de NO_PRIMEIRO_NOME');
		$this->assertNotEquals($obj1->mNO_NOME_MEIO                               	,$obj2->mNO_NOME_MEIO                             	, 'teste de NO_NOME_MEIO');
		$this->assertNotEquals($obj1->mNO_ULTIMO_NOME                             	,$obj2->mNO_ULTIMO_NOME                           	, 'teste de NO_ULTIMO_NOME');
		$this->assertNotEquals($obj1->mNOME_COMPLETO                              	,$obj2->mNOME_COMPLETO                            	, 'teste de NOME_COMPLETO');
		$this->assertNotEquals($obj1->mNO_EMAIL_CANDIDATO                         	,$obj2->mNO_EMAIL_CANDIDATO                       	, 'teste de NO_EMAIL_CANDIDATO');
		$this->assertNotEquals($obj1->mNO_ENDERECO_RESIDENCIA                     	,$obj2->mNO_ENDERECO_RESIDENCIA                   	, 'teste de NO_ENDERECO_RESIDENCIA');
		$this->assertNotEquals($obj1->mNO_CIDADE_RESIDENCIA                       	,$obj2->mNO_CIDADE_RESIDENCIA                     	, 'teste de NO_CIDADE_RESIDENCIA');
		$this->assertNotEquals($obj1->mCO_PAIS_RESIDENCIA                         	,$obj2->mCO_PAIS_RESIDENCIA                       	, 'teste de CO_PAIS_RESIDENCIA');
		$this->assertNotEquals($obj1->mNU_TELEFONE_CANDIDATO                      	,$obj2->mNU_TELEFONE_CANDIDATO                    	, 'teste de NU_TELEFONE_CANDIDATO');
		$this->assertNotEquals($obj1->mNO_EMPRESA_ESTRANGEIRA                     	,$obj2->mNO_EMPRESA_ESTRANGEIRA                   	, 'teste de NO_EMPRESA_ESTRANGEIRA');
		$this->assertNotEquals($obj1->mNO_ENDERECO_EMPRESA                        	,$obj2->mNO_ENDERECO_EMPRESA                      	, 'teste de NO_ENDERECO_EMPRESA');
		$this->assertNotEquals($obj1->mNO_CIDADE_EMPRESA                          	,$obj2->mNO_CIDADE_EMPRESA                        	, 'teste de NO_CIDADE_EMPRESA');
		$this->assertNotEquals($obj1->mCO_PAIS_EMPRESA                            	,$obj2->mCO_PAIS_EMPRESA                          	, 'teste de CO_PAIS_EMPRESA');
		$this->assertNotEquals($obj1->mNU_TELEFONE_EMPRESA                        	,$obj2->mNU_TELEFONE_EMPRESA                      	, 'teste de NU_TELEFONE_EMPRESA');
		$this->assertNotEquals($obj1->mNO_PAI                                     	,$obj2->mNO_PAI                                   	, 'teste de NO_PAI');
		$this->assertNotEquals($obj1->mNO_MAE                                     	,$obj2->mNO_MAE                                   	, 'teste de NO_MAE');
		$this->assertNotEquals($obj1->mCO_NACIONALIDADE                           	,$obj2->mCO_NACIONALIDADE                         	, 'teste de CO_NACIONALIDADE');
		$this->assertNotEquals($obj1->mCO_NIVEL_ESCOLARIDADE                      	,$obj2->mCO_NIVEL_ESCOLARIDADE                    	, 'teste de CO_NIVEL_ESCOLARIDADE');
		$this->assertNotEquals($obj1->mCO_ESTADO_CIVIL                            	,$obj2->mCO_ESTADO_CIVIL                          	, 'teste de CO_ESTADO_CIVIL');
		$this->assertNotEquals($obj1->mCO_SEXO                                    	,$obj2->mCO_SEXO                                  	, 'teste de CO_SEXO');
		$this->assertNotEquals($obj1->mDT_NASCIMENTO                              	,$obj2->mDT_NASCIMENTO                            	, 'teste de DT_NASCIMENTO');
		$this->assertNotEquals($obj1->mNO_LOCAL_NASCIMENTO                        	,$obj2->mNO_LOCAL_NASCIMENTO                      	, 'teste de NO_LOCAL_NASCIMENTO');
		$this->assertNotEquals($obj1->mNU_PASSAPORTE                              	,$obj2->mNU_PASSAPORTE                            	, 'teste de NU_PASSAPORTE');
		$this->assertNotEquals($obj1->mDT_EMISSAO_PASSAPORTE                      	,$obj2->mDT_EMISSAO_PASSAPORTE                    	, 'teste de DT_EMISSAO_PASSAPORTE');
		$this->assertNotEquals($obj1->mDT_VALIDADE_PASSAPORTE                     	,$obj2->mDT_VALIDADE_PASSAPORTE                   	, 'teste de DT_VALIDADE_PASSAPORTE');
		$this->assertNotEquals($obj1->mCO_PAIS_EMISSOR_PASSAPORTE                 	,$obj2->mCO_PAIS_EMISSOR_PASSAPORTE               	, 'teste de CO_PAIS_EMISSOR_PASSAPORTE');
		$this->assertNotEquals($obj1->mNU_RNE                                     	,$obj2->mNU_RNE                                   	, 'teste de NU_RNE');
		$this->assertNotEquals($obj1->mCO_PROFISSAO_CANDIDATO                     	,$obj2->mCO_PROFISSAO_CANDIDATO                   	, 'teste de CO_PROFISSAO_CANDIDATO');
		$this->assertNotEquals($obj1->mTE_TRABALHO_ANTERIOR_BRASIL                	,$obj2->mTE_TRABALHO_ANTERIOR_BRASIL              	, 'teste de TE_TRABALHO_ANTERIOR_BRASIL');
		$this->assertNotEquals($obj1->mTE_DESCRICAO_ATIVIDADES                    	,$obj2->mTE_DESCRICAO_ATIVIDADES                  	, 'teste de TE_DESCRICAO_ATIVIDADES');
		$this->assertNotEquals($obj1->mNU_CPF                                     	,$obj2->mNU_CPF                                   	, 'teste de NU_CPF');
//		$this->assertNotEquals($obj1->mDT_CADASTRAMENTO                           	,$obj2->mDT_CADASTRAMENTO                         	, 'teste de DT_CADASTRAMENTO');
//		$this->assertNotEquals($obj1->mCO_USU_CADASTAMENTO                        	,$obj2->mCO_USU_CADASTAMENTO                      	, 'teste de CO_USU_CADASTAMENTO');
//		$this->assertNotEquals($obj1->mDT_ULT_ALTERACAO                           	,$obj2->mDT_ULT_ALTERACAO                         	, 'teste de DT_ULT_ALTERACAO');
//		$this->assertNotEquals($obj1->mCO_USU_ULT_ALTERACAO                       	,$obj2->mCO_USU_ULT_ALTERACAO                     	, 'teste de CO_USU_ULT_ALTERACAO');
		$this->assertNotEquals($obj1->mNO_ENDERECO_ESTRANGEIRO                    	,$obj2->mNO_ENDERECO_ESTRANGEIRO                  	, 'teste de NO_ENDERECO_ESTRANGEIRO');
		$this->assertNotEquals($obj1->mNO_CIDADE_ESTRANGEIRO                      	,$obj2->mNO_CIDADE_ESTRANGEIRO                    	, 'teste de NO_CIDADE_ESTRANGEIRO');
		$this->assertNotEquals($obj1->mCO_PAIS_ESTRANGEIRO                        	,$obj2->mCO_PAIS_ESTRANGEIRO                      	, 'teste de CO_PAIS_ESTRANGEIRO');
		$this->assertNotEquals($obj1->mNU_TELEFONE_ESTRANGEIRO                    	,$obj2->mNU_TELEFONE_ESTRANGEIRO                  	, 'teste de NU_TELEFONE_ESTRANGEIRO');
		$this->assertNotEquals($obj1->mNU_EMPRESA                                 	,$obj2->mNU_EMPRESA                               	, 'teste de NU_EMPRESA');
		$this->assertNotEquals($obj1->mNU_EMBARCACAO_PROJETO                      	,$obj2->mNU_EMBARCACAO_PROJETO                    	, 'teste de NU_EMBARCACAO_PROJETO');
		$this->assertNotEquals($obj1->mNU_CTPS                                    	,$obj2->mNU_CTPS                                  	, 'teste de NU_CTPS');
		$this->assertNotEquals($obj1->mNU_CNH                                     	,$obj2->mNU_CNH                                   	, 'teste de NU_CNH');
		$this->assertNotEquals($obj1->mDT_EXPIRACAO_CNH                           	,$obj2->mDT_EXPIRACAO_CNH                         	, 'teste de DT_EXPIRACAO_CNH');
		$this->assertNotEquals($obj1->mDT_EXPIRACAO_CTPS                          	,$obj2->mDT_EXPIRACAO_CTPS                        	, 'teste de DT_EXPIRACAO_CTPS');
//		$this->assertNotEquals($obj1->mcodigo_processo_mte_atual                  	,$obj2->mcodigo_processo_mte_atual                	, 'teste de codigo_processo_mte_atual');
//		$this->assertNotEquals($obj1->mNO_LOGIN                                   	,$obj2->mNO_LOGIN                                 	, 'teste de NO_LOGIN');
//		$this->assertNotEquals($obj1->mNO_SENHA                                   	,$obj2->mNO_SENHA                                 	, 'teste de NO_SENHA');
//		$this->assertNotEquals($obj1->mFL_ATUALIZACAO_HABILITADA                  	,$obj2->mFL_ATUALIZACAO_HABILITADA                	, 'teste de FL_ATUALIZACAO_HABILITADA');
//		$this->assertNotEquals($obj1->mFL_HABILITAR_CV                            	,$obj2->mFL_HABILITAR_CV                          	, 'teste de FL_HABILITAR_CV');
//		$this->assertNotEquals($obj1->mNU_ORDEM_CREWLIST                          	,$obj2->mNU_ORDEM_CREWLIST                        	, 'teste de NU_ORDEM_CREWLIST');
//		$this->assertNotEquals($obj1->mFL_EMBARCADO                               	,$obj2->mFL_EMBARCADO                             	, 'teste de FL_EMBARCADO');
//		$this->assertNotEquals($obj1->mFL_INATIVO                                 	,$obj2->mFL_INATIVO                               	, 'teste de FL_INATIVO');
//		$this->assertNotEquals($obj1->mTE_OBSERVACAO_CREWLIST                     	,$obj2->mTE_OBSERVACAO_CREWLIST                   	, 'teste de TE_OBSERVACAO_CREWLIST');
//		$this->assertNotEquals($obj1->mTE_EXPERIENCIA_ANTERIOR                    	,$obj2->mTE_EXPERIENCIA_ANTERIOR                  	, 'teste de TE_EXPERIENCIA_ANTERIOR');
//		$this->assertNotEquals($obj1->mFL_EDICAO_CONCLUIDA                        	,$obj2->mFL_EDICAO_CONCLUIDA                      	, 'teste de FL_EDICAO_CONCLUIDA');
//		$this->assertNotEquals($obj1->mVA_REMUNERACAO_MENSAL                      	,$obj2->mVA_REMUNERACAO_MENSAL                    	, 'teste de VA_REMUNERACAO_MENSAL');
//		$this->assertNotEquals($obj1->mVA_REMUNERACAO_MENSAL_BRASIL               	,$obj2->mVA_REMUNERACAO_MENSAL_BRASIL             	, 'teste de VA_REMUNERACAO_MENSAL_BRASIL');
//		$this->assertNotEquals($obj1->mCO_PAIS_SEAMAN                             	,$obj2->mCO_PAIS_SEAMAN                           	, 'teste de CO_PAIS_SEAMAN');
//		$this->assertNotEquals($obj1->mNO_SEAMAN                                  	,$obj2->mNO_SEAMAN                                	, 'teste de NO_SEAMAN');
//		$this->assertNotEquals($obj1->mDT_EMISSAO_SEAMAN                          	,$obj2->mDT_EMISSAO_SEAMAN                        	, 'teste de DT_EMISSAO_SEAMAN');
//		$this->assertNotEquals($obj1->mDT_VALIDADE_SEAMAN                         	,$obj2->mDT_VALIDADE_SEAMAN                       	, 'teste de DT_VALIDADE_SEAMAN');
//		$this->assertNotEquals($obj1->mCO_FUNCAO                                  	,$obj2->mCO_FUNCAO                                	, 'teste de CO_FUNCAO');
//		$this->assertNotEquals($obj1->mCO_LOCAL_EMBARCACAO_PROJETO                	,$obj2->mCO_LOCAL_EMBARCACAO_PROJETO              	, 'teste de CO_LOCAL_EMBARCACAO_PROJETO');
	}

	public function TestaEquivalencia($obj1, $obj2){
		$this->assertEquals($obj1->mNO_PRIMEIRO_NOME                           	,$obj2->mNO_PRIMEIRO_NOME                         	, 'teste de NO_PRIMEIRO_NOME');
		$this->assertEquals($obj1->mNO_NOME_MEIO                               	,$obj2->mNO_NOME_MEIO                             	, 'teste de NO_NOME_MEIO');
		$this->assertEquals($obj1->mNO_ULTIMO_NOME                             	,$obj2->mNO_ULTIMO_NOME                           	, 'teste de NO_ULTIMO_NOME');
		$this->assertEquals($obj1->mNOME_COMPLETO                              	,$obj2->mNOME_COMPLETO                            	, 'teste de NOME_COMPLETO');
		$this->assertEquals($obj1->mNO_EMAIL_CANDIDATO                         	,$obj2->mNO_EMAIL_CANDIDATO                       	, 'teste de NO_EMAIL_CANDIDATO');
		$this->assertEquals($obj1->mNO_ENDERECO_RESIDENCIA                     	,$obj2->mNO_ENDERECO_RESIDENCIA                   	, 'teste de NO_ENDERECO_RESIDENCIA');
		$this->assertEquals($obj1->mNO_CIDADE_RESIDENCIA                       	,$obj2->mNO_CIDADE_RESIDENCIA                     	, 'teste de NO_CIDADE_RESIDENCIA');
		$this->assertEquals($obj1->mCO_PAIS_RESIDENCIA                         	,$obj2->mCO_PAIS_RESIDENCIA                       	, 'teste de CO_PAIS_RESIDENCIA');
		$this->assertEquals($obj1->mNU_TELEFONE_CANDIDATO                      	,$obj2->mNU_TELEFONE_CANDIDATO                    	, 'teste de NU_TELEFONE_CANDIDATO');
		$this->assertEquals($obj1->mNO_EMPRESA_ESTRANGEIRA                     	,$obj2->mNO_EMPRESA_ESTRANGEIRA                   	, 'teste de NO_EMPRESA_ESTRANGEIRA');
		$this->assertEquals($obj1->mNO_ENDERECO_EMPRESA                        	,$obj2->mNO_ENDERECO_EMPRESA                      	, 'teste de NO_ENDERECO_EMPRESA');
		$this->assertEquals($obj1->mNO_CIDADE_EMPRESA                          	,$obj2->mNO_CIDADE_EMPRESA                        	, 'teste de NO_CIDADE_EMPRESA');
		$this->assertEquals($obj1->mCO_PAIS_EMPRESA                            	,$obj2->mCO_PAIS_EMPRESA                          	, 'teste de CO_PAIS_EMPRESA');
		$this->assertEquals($obj1->mNU_TELEFONE_EMPRESA                        	,$obj2->mNU_TELEFONE_EMPRESA                      	, 'teste de NU_TELEFONE_EMPRESA');
		$this->assertEquals($obj1->mNO_PAI                                     	,$obj2->mNO_PAI                                   	, 'teste de NO_PAI');
		$this->assertEquals($obj1->mNO_MAE                                     	,$obj2->mNO_MAE                                   	, 'teste de NO_MAE');
		$this->assertEquals($obj1->mCO_NACIONALIDADE                           	,$obj2->mCO_NACIONALIDADE                         	, 'teste de CO_NACIONALIDADE');
		$this->assertEquals($obj1->mCO_NIVEL_ESCOLARIDADE                      	,$obj2->mCO_NIVEL_ESCOLARIDADE                    	, 'teste de CO_NIVEL_ESCOLARIDADE');
		$this->assertEquals($obj1->mCO_ESTADO_CIVIL                            	,$obj2->mCO_ESTADO_CIVIL                          	, 'teste de CO_ESTADO_CIVIL');
		$this->assertEquals($obj1->mCO_SEXO                                    	,$obj2->mCO_SEXO                                  	, 'teste de CO_SEXO');
		$this->assertEquals($obj1->mDT_NASCIMENTO                              	,$obj2->mDT_NASCIMENTO                            	, 'teste de DT_NASCIMENTO');
		$this->assertEquals($obj1->mNO_LOCAL_NASCIMENTO                        	,$obj2->mNO_LOCAL_NASCIMENTO                      	, 'teste de NO_LOCAL_NASCIMENTO');
		$this->assertEquals($obj1->mNU_PASSAPORTE                              	,$obj2->mNU_PASSAPORTE                            	, 'teste de NU_PASSAPORTE');
		$this->assertEquals($obj1->mDT_EMISSAO_PASSAPORTE                      	,$obj2->mDT_EMISSAO_PASSAPORTE                    	, 'teste de DT_EMISSAO_PASSAPORTE');
		$this->assertEquals($obj1->mDT_VALIDADE_PASSAPORTE                     	,$obj2->mDT_VALIDADE_PASSAPORTE                   	, 'teste de DT_VALIDADE_PASSAPORTE');
		$this->assertEquals($obj1->mCO_PAIS_EMISSOR_PASSAPORTE                 	,$obj2->mCO_PAIS_EMISSOR_PASSAPORTE               	, 'teste de CO_PAIS_EMISSOR_PASSAPORTE');
		$this->assertEquals($obj1->mNU_RNE                                     	,$obj2->mNU_RNE                                   	, 'teste de NU_RNE');
		$this->assertEquals($obj1->mCO_PROFISSAO_CANDIDATO                     	,$obj2->mCO_PROFISSAO_CANDIDATO                   	, 'teste de CO_PROFISSAO_CANDIDATO');
		$this->assertEquals($obj1->mTE_TRABALHO_ANTERIOR_BRASIL                	,$obj2->mTE_TRABALHO_ANTERIOR_BRASIL              	, 'teste de TE_TRABALHO_ANTERIOR_BRASIL');
		$this->assertEquals($obj1->mTE_DESCRICAO_ATIVIDADES                    	,$obj2->mTE_DESCRICAO_ATIVIDADES                  	, 'teste de TE_DESCRICAO_ATIVIDADES');
		$this->assertEquals($obj1->mNU_CPF                                     	,$obj2->mNU_CPF                                   	, 'teste de NU_CPF');
//		$this->assertEquals($obj1->mDT_CADASTRAMENTO                           	,$obj2->mDT_CADASTRAMENTO                         	, 'teste de DT_CADASTRAMENTO');
//		$this->assertEquals($obj1->mCO_USU_CADASTAMENTO                        	,$obj2->mCO_USU_CADASTAMENTO                      	, 'teste de CO_USU_CADASTAMENTO');
//		$this->assertEquals($obj1->mDT_ULT_ALTERACAO                           	,$obj2->mDT_ULT_ALTERACAO                         	, 'teste de DT_ULT_ALTERACAO');
//		$this->assertEquals($obj1->mCO_USU_ULT_ALTERACAO                       	,$obj2->mCO_USU_ULT_ALTERACAO                     	, 'teste de CO_USU_ULT_ALTERACAO');
		$this->assertEquals($obj1->mNO_ENDERECO_ESTRANGEIRO                    	,$obj2->mNO_ENDERECO_ESTRANGEIRO                  	, 'teste de NO_ENDERECO_ESTRANGEIRO');
		$this->assertEquals($obj1->mNO_CIDADE_ESTRANGEIRO                      	,$obj2->mNO_CIDADE_ESTRANGEIRO                    	, 'teste de NO_CIDADE_ESTRANGEIRO');
		$this->assertEquals($obj1->mCO_PAIS_ESTRANGEIRO                        	,$obj2->mCO_PAIS_ESTRANGEIRO                      	, 'teste de CO_PAIS_ESTRANGEIRO');
		$this->assertEquals($obj1->mNU_TELEFONE_ESTRANGEIRO                    	,$obj2->mNU_TELEFONE_ESTRANGEIRO                  	, 'teste de NU_TELEFONE_ESTRANGEIRO');
		$this->assertEquals($obj1->mNU_EMPRESA                                 	,$obj2->mNU_EMPRESA                               	, 'teste de NU_EMPRESA');
		$this->assertEquals($obj1->mNU_EMBARCACAO_PROJETO                      	,$obj2->mNU_EMBARCACAO_PROJETO                    	, 'teste de NU_EMBARCACAO_PROJETO');
		$this->assertEquals($obj1->mNU_CTPS                                    	,$obj2->mNU_CTPS                                  	, 'teste de NU_CTPS');
		$this->assertEquals($obj1->mNU_CNH                                     	,$obj2->mNU_CNH                                   	, 'teste de NU_CNH');
		$this->assertEquals($obj1->mDT_EXPIRACAO_CNH                           	,$obj2->mDT_EXPIRACAO_CNH                         	, 'teste de DT_EXPIRACAO_CNH');
		$this->assertEquals($obj1->mDT_EXPIRACAO_CTPS                          	,$obj2->mDT_EXPIRACAO_CTPS                        	, 'teste de DT_EXPIRACAO_CTPS');
//		$this->assertEquals($obj1->mcodigo_processo_mte_atual                  	,$obj2->mcodigo_processo_mte_atual                	, 'teste de codigo_processo_mte_atual');
//		$this->assertEquals($obj1->mNO_LOGIN                                   	,$obj2->mNO_LOGIN                                 	, 'teste de NO_LOGIN');
//		$this->assertEquals($obj1->mNO_SENHA                                   	,$obj2->mNO_SENHA                                 	, 'teste de NO_SENHA');
//		$this->assertEquals($obj1->mFL_ATUALIZACAO_HABILITADA                  	,$obj2->mFL_ATUALIZACAO_HABILITADA                	, 'teste de FL_ATUALIZACAO_HABILITADA');
//		$this->assertEquals($obj1->mFL_HABILITAR_CV                            	,$obj2->mFL_HABILITAR_CV                          	, 'teste de FL_HABILITAR_CV');
//		$this->assertEquals($obj1->mNU_ORDEM_CREWLIST                          	,$obj2->mNU_ORDEM_CREWLIST                        	, 'teste de NU_ORDEM_CREWLIST');
//		$this->assertEquals($obj1->mFL_EMBARCADO                               	,$obj2->mFL_EMBARCADO                             	, 'teste de FL_EMBARCADO');
//		$this->assertEquals($obj1->mFL_INATIVO                                 	,$obj2->mFL_INATIVO                               	, 'teste de FL_INATIVO');
//		$this->assertEquals($obj1->mTE_OBSERVACAO_CREWLIST                     	,$obj2->mTE_OBSERVACAO_CREWLIST                   	, 'teste de TE_OBSERVACAO_CREWLIST');
//		$this->assertEquals($obj1->mTE_EXPERIENCIA_ANTERIOR                    	,$obj2->mTE_EXPERIENCIA_ANTERIOR                  	, 'teste de TE_EXPERIENCIA_ANTERIOR');
//		$this->assertEquals($obj1->mFL_EDICAO_CONCLUIDA                        	,$obj2->mFL_EDICAO_CONCLUIDA                      	, 'teste de FL_EDICAO_CONCLUIDA');
//		$this->assertEquals($obj1->mVA_REMUNERACAO_MENSAL                      	,$obj2->mVA_REMUNERACAO_MENSAL                    	, 'teste de VA_REMUNERACAO_MENSAL');
//		$this->assertEquals($obj1->mVA_REMUNERACAO_MENSAL_BRASIL               	,$obj2->mVA_REMUNERACAO_MENSAL_BRASIL             	, 'teste de VA_REMUNERACAO_MENSAL_BRASIL');
//		$this->assertEquals($obj1->mCO_PAIS_SEAMAN                             	,$obj2->mCO_PAIS_SEAMAN                           	, 'teste de CO_PAIS_SEAMAN');
//		$this->assertEquals($obj1->mNO_SEAMAN                                  	,$obj2->mNO_SEAMAN                                	, 'teste de NO_SEAMAN');
//		$this->assertEquals($obj1->mDT_EMISSAO_SEAMAN                          	,$obj2->mDT_EMISSAO_SEAMAN                        	, 'teste de DT_EMISSAO_SEAMAN');
//		$this->assertEquals($obj1->mDT_VALIDADE_SEAMAN                         	,$obj2->mDT_VALIDADE_SEAMAN                       	, 'teste de DT_VALIDADE_SEAMAN');
//		$this->assertEquals($obj1->mCO_FUNCAO                                  	,$obj2->mCO_FUNCAO                                	, 'teste de CO_FUNCAO');
//		$this->assertEquals($obj1->mCO_LOCAL_EMBARCACAO_PROJETO                	,$obj2->mCO_LOCAL_EMBARCACAO_PROJETO              	, 'teste de CO_LOCAL_EMBARCACAO_PROJETO');
	}

	public function CarregaValores(){
		$this->AtribuiValores($this->object);
	}
	public function AtribuiValores(&$pObj){
		$pObj->mNO_PRIMEIRO_NOME                                    	= $this->xNO_PRIMEIRO_NOME;
		$pObj->mNO_NOME_MEIO                                        	= $this->xNO_NOME_MEIO;
		$pObj->mNO_ULTIMO_NOME                                      	= $this->xNO_ULTIMO_NOME;
		$pObj->mNOME_COMPLETO                                       	= $this->xNOME_COMPLETO;
		$pObj->mNO_EMAIL_CANDIDATO                                  	= $this->xNO_EMAIL_CANDIDATO;
		$pObj->mNO_ENDERECO_RESIDENCIA                              	= $this->xNO_ENDERECO_RESIDENCIA;
		$pObj->mNO_CIDADE_RESIDENCIA                                	= $this->xNO_CIDADE_RESIDENCIA;
		$pObj->mCO_PAIS_RESIDENCIA                                  	= $this->xCO_PAIS_RESIDENCIA;
		$pObj->mNU_TELEFONE_CANDIDATO                               	= $this->xNU_TELEFONE_CANDIDATO;
		$pObj->mNO_EMPRESA_ESTRANGEIRA                              	= $this->xNO_EMPRESA_ESTRANGEIRA;
		$pObj->mNO_ENDERECO_EMPRESA                                 	= $this->xNO_ENDERECO_EMPRESA;
		$pObj->mNO_CIDADE_EMPRESA                                   	= $this->xNO_CIDADE_EMPRESA;
		$pObj->mCO_PAIS_EMPRESA                                     	= $this->xCO_PAIS_EMPRESA;
		$pObj->mNU_TELEFONE_EMPRESA                                 	= $this->xNU_TELEFONE_EMPRESA;
		$pObj->mNO_PAI                                              	= $this->xNO_PAI;
		$pObj->mNO_MAE                                              	= $this->xNO_MAE;
		$pObj->mCO_NACIONALIDADE                                    	= $this->xCO_NACIONALIDADE;
		$pObj->mCO_NIVEL_ESCOLARIDADE                               	= $this->xCO_NIVEL_ESCOLARIDADE;
		$pObj->mCO_ESTADO_CIVIL                                     	= $this->xCO_ESTADO_CIVIL;
		$pObj->mCO_SEXO                                             	= $this->xCO_SEXO;
		$pObj->mDT_NASCIMENTO                                       	= $this->xDT_NASCIMENTO;
		$pObj->mNO_LOCAL_NASCIMENTO                                 	= $this->xNO_LOCAL_NASCIMENTO;
		$pObj->mNU_PASSAPORTE                                       	= $this->xNU_PASSAPORTE;
		$pObj->mDT_EMISSAO_PASSAPORTE                               	= $this->xDT_EMISSAO_PASSAPORTE;
		$pObj->mDT_VALIDADE_PASSAPORTE                              	= $this->xDT_VALIDADE_PASSAPORTE;
		$pObj->mCO_PAIS_EMISSOR_PASSAPORTE                          	= $this->xCO_PAIS_EMISSOR_PASSAPORTE;
		$pObj->mNU_RNE                                              	= $this->xNU_RNE;
		$pObj->mCO_PROFISSAO_CANDIDATO                              	= $this->xCO_PROFISSAO_CANDIDATO;
		$pObj->mTE_TRABALHO_ANTERIOR_BRASIL                         	= $this->xTE_TRABALHO_ANTERIOR_BRASIL;
		$pObj->mTE_DESCRICAO_ATIVIDADES                             	= $this->xTE_DESCRICAO_ATIVIDADES;
		$pObj->mNU_CPF                                              	= $this->xNU_CPF;
		$pObj->mDT_CADASTRAMENTO                                    	= $this->xDT_CADASTRAMENTO;
		$pObj->mCO_USU_CADASTAMENTO                                 	= $this->xCO_USU_CADASTAMENTO;
		$pObj->mDT_ULT_ALTERACAO                                    	= $this->xDT_ULT_ALTERACAO;
		$pObj->mCO_USU_ULT_ALTERACAO                                	= $this->xCO_USU_ULT_ALTERACAO;
		$pObj->mNO_ENDERECO_ESTRANGEIRO                             	= $this->xNO_ENDERECO_ESTRANGEIRO;
		$pObj->mNO_CIDADE_ESTRANGEIRO                               	= $this->xNO_CIDADE_ESTRANGEIRO;
		$pObj->mCO_PAIS_ESTRANGEIRO                                 	= $this->xCO_PAIS_ESTRANGEIRO;
		$pObj->mNU_TELEFONE_ESTRANGEIRO                             	= $this->xNU_TELEFONE_ESTRANGEIRO;
		$pObj->mNU_EMPRESA                                          	= $this->xNU_EMPRESA;
		$pObj->mNU_EMBARCACAO_PROJETO                               	= $this->xNU_EMBARCACAO_PROJETO;
		$pObj->mNU_CTPS                                             	= $this->xNU_CTPS;
		$pObj->mNU_CNH                                              	= $this->xNU_CNH;
		$pObj->mDT_EXPIRACAO_CNH                                    	= $this->xDT_EXPIRACAO_CNH;
		$pObj->mDT_EXPIRACAO_CTPS                                   	= $this->xDT_EXPIRACAO_CTPS;
		$pObj->mcodigo_processo_mte_atual                           	= $this->xcodigo_processo_mte_atual;
		$pObj->mNO_LOGIN                                            	= $this->xNO_LOGIN;
		$pObj->mNO_SENHA                                            	= $this->xNO_SENHA;
		$pObj->mFL_ATUALIZACAO_HABILITADA                           	= $this->xFL_ATUALIZACAO_HABILITADA;
		$pObj->mFL_HABILITAR_CV                                     	= $this->xFL_HABILITAR_CV;
		$pObj->mNU_ORDEM_CREWLIST                                   	= $this->xNU_ORDEM_CREWLIST;
		$pObj->mFL_EMBARCADO                                        	= $this->xFL_EMBARCADO;
		$pObj->mFL_INATIVO                                          	= $this->xFL_INATIVO;
		$pObj->mTE_OBSERVACAO_CREWLIST                              	= $this->xTE_OBSERVACAO_CREWLIST;
		$pObj->mTE_EXPERIENCIA_ANTERIOR                             	= $this->xTE_EXPERIENCIA_ANTERIOR;
		$pObj->mFL_EDICAO_CONCLUIDA                                 	= $this->xFL_EDICAO_CONCLUIDA;
		$pObj->mVA_REMUNERACAO_MENSAL                               	= $this->xVA_REMUNERACAO_MENSAL;
		$pObj->mVA_REMUNERACAO_MENSAL_BRASIL                        	= $this->xVA_REMUNERACAO_MENSAL_BRASIL;
		$pObj->mCO_PAIS_SEAMAN                                      	= $this->xCO_PAIS_SEAMAN;
		$pObj->mNO_SEAMAN                                           	= $this->xNO_SEAMAN;
		$pObj->mDT_EMISSAO_SEAMAN                                   	= $this->xDT_EMISSAO_SEAMAN;
		$pObj->mDT_VALIDADE_SEAMAN                                  	= $this->xDT_VALIDADE_SEAMAN;
		$pObj->mCO_FUNCAO                                           	= $this->xCO_FUNCAO;
		$pObj->mCO_LOCAL_EMBARCACAO_PROJETO                         	= $this->xCO_LOCAL_EMBARCACAO_PROJETO;
	}

	public function testVerificarCandidatoExistente(){
		$nome_completo = rotinasComuns::randTexto();
		$nome_mae = rotinasComuns::randTexto();
		$data_nasc = rotinasComuns::randData();
		// Limpa o lixo do banco
		$sql = "delete from CANDIDATO where NOME_COMPLETO = '".$nome_completo."'";
		executeQuery($sql, __CLASS__.'.'.__FUNCTION__);
		$sql = "delete from candidato_tmp where NOME_COMPLETO = '".$nome_completo."'";
		executeQuery($sql, __CLASS__.'.'.__FUNCTION__);

		// Teste para candidato não existente
		$cand = new cCANDIDATO();
//		$cand->VerificarCandidatoExistente($nome_completo, $nome_mae , $data_nasc);
		$this->assertEmpty($cand->mNU_CANDIDATO, 'Candidato não existente->NU_CANDIDATO vazio');
		$cand->CriarNomeCompleto($nome_completo, cSESSAO::$mcd_usuario);
		$cand->mNO_MAE = $nome_mae;
		$cand->mDT_NASCIMENTO = $data_nasc;
		$cand->Atualizar();
		$id = $cand->mNU_CANDIDATO;
		// Teste para o candidato existente mas sem temporário
		$cand = new cCANDIDATO();
//		$cand->VerificarCandidatoExistente($nome_completo, $nome_mae , $data_nasc);
		$this->assertEquals($id, $cand->mNU_CANDIDATO, 'Candidato existente sem temporario-> NU_CANDIDATO igual a chave salva');
		// Teste com o temporário criado
		$cand->CriaCandidatoCliente();
		$cand = new cCANDIDATO();
//		$cand->VerificarCandidatoExistente($nome_completo, $nome_mae , $data_nasc);
		$this->assertEquals($id, $cand->mNU_CANDIDATO, 'Candidato existente com temporario-> NU_CANDIDATO igual a chave salva');
		// Teste com um candidato inativo
		$sql = "update CANDIDATO set FL_INATIVO = 1 where NU_CANDIDATO=".$id;
		executeQuery($sql, __CLASS__.'.'.__FUNCTION__);
		$cand = new cCANDIDATO();
		$cand->Recuperar($id);
		$this->assertEquals(1, $cand->mFL_INATIVO);
//		$cand->VerificarCandidatoExistente($nome_completo, $nome_mae , $data_nasc);
		$cand = new cCANDIDATO();
		$cand->Recuperar($id);
		$this->assertEquals(0, $cand->mFL_INATIVO);
		//
		$embp = new cEMBARCACAO_PROJETO();
		$embp->mNU_EMPRESA = 21;
		$embp->mNU_EMBARCACAO_PROJETO = 30;
		$embp->RecupereSe();
		$embp->membp_fl_ativo = 0;
		$embp->Atualize();
		$sql = "update CANDIDATO set NU_EMPRESA = 21, NU_EMBARCACAO_PROJETO = 30 where NU_CANDIDATO=".$id;
		executeQuery($sql, __CLASS__.'.'.__FUNCTION__);
		$cand = new cCANDIDATO();
		$cand->Recuperar($id);
		$this->assertNotEmpty($cand->mNU_EMBARCACAO_PROJETO);
//		$cand->VerificarCandidatoExistente($nome_completo, $nome_mae , $data_nasc);
		$cand = new cCANDIDATO();
		$cand->Recuperar($id);
		$this->assertEmpty($cand->mNU_EMBARCACAO_PROJETO);

	}

    public function testCalculaNomeCompleto($pPrimeiro = '', $pSegundo = '', $pTerceiro = '', $pResultado = ''){
    	$xCand = new cCANDIDATO();
		$this->assertEquals($pResultado , $xCand->CalculaNomeCompleto($pPrimeiro, $pSegundo, $pTerceiro), 'Erro !!!->'.$pPrimeiro.'|'.$pSegundo.'|'.$pTerceiro );
    }
    

	/**
	 * @dataProvider provider
	 */
    public function testCriar($pNO_PRIMEIRO_NOME = '', $pNO_NOME_MEIO = '', $pNO_ULTIMO_NOME = '', $pResultado = ''){
    	try{
	    	$sql = "delete from CANDIDATO where NOME_COMPLETO = '".$pResultado."' ";
	    	executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
    		$xCand = new cCANDIDATO();
	    	$xNU_CANDIDATO;
			$xCand->Criar($pNO_PRIMEIRO_NOME, $pNO_NOME_MEIO, $pNO_ULTIMO_NOME, 1);
			$this->assertGreaterThan(0, $xCand->mNU_CANDIDATO);
			$sql = "select NOME_COMPLETO from CANDIDATO where NU_CANDIDATO = ".$xCand->mNU_CANDIDATO;
			if($res = mysql_query($sql))
			{
				if($rs=mysql_fetch_array($res)) 
				{
					$this->assertEquals($pResultado, $rs[0]);
				}
				else{
					$this->fail('Não retornou recordset');
				}
			}
			else{
				$this->fail('SQL com problema:'.$sql);
			}    		
	    	$sql = "delete from CANDIDATO where NOME_COMPLETO = '".$pResultado."' ";
	    	executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
    	}
    	catch (Exception $e ){
			$this->fail('Erro não esperado:'.$e->getMessage());
    	}
	}

	/**
	 * @dataProvider provider
	 */
	public function testAtualizarNome($pNO_PRIMEIRO_NOME = '', $pNO_NOME_MEIO = '', $pNO_ULTIMO_NOME = '', $pResultado = ''){
    	try{
	    	$xCand = new cCANDIDATO();
	    	$xNU_CANDIDATO;
	    	$sql = "delete from CANDIDATO where NOME_COMPLETO = '".$pResultado."' ";
	    	executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
	    	
			$xCand->Criar($pNO_PRIMEIRO_NOME, $pNO_NOME_MEIO, $pNO_ULTIMO_NOME, 1);
			$this->assertGreaterThan(0, $xCand->mNU_CANDIDATO);
			$sql = "UPDATE CANDIDATO SET NOME_COMPLETO = 'xxx' WHERE NU_CANDIDATO = ".$xCand->mNU_CANDIDATO;
	    	executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
			
			$xCand->CorrijaNomeCompleto();
			$sql = "select NOME_COMPLETO from CANDIDATO where NU_CANDIDATO = ".$xCand->mNU_CANDIDATO;
			if($res = mysql_query($sql))
			{
				if($rs=mysql_fetch_array($res)) 
				{
					$this->assertEquals($pResultado, $rs[0]);
				}
				else{
					$this->fail('Não retornou recordset');
				}
			}
			else{
				$this->fail('SQL com problema:'.$sql);
			}    		
	    	$sql = "delete from CANDIDATO where NOME_COMPLETO = '".$pResultado."' ";
	    	executeQuery($sql, __CLASS__.'->'.__FUNCTION__);
    	}
    	catch (Exception $e ){
			$this->fail('Erro não esperado:'.$e->getMessage());
    	}
	}

	public function  testAtualizeCPF(){
		$cand = new cCANDIDATO();
		$cand->Recuperar(19733);
		$teste  = rand(1000000, 9999999);
		$this->assertNotEquals($teste, $cand->mNU_CPF);
		$cand->mNU_CPF = $teste;
		$cand->AtualizeCPF();
		$cand = new cCANDIDATO();
		$cand->Recuperar(19733);
		$this->assertEquals($teste, $cand->mNU_CPF);		
	}

	public function  testAtualizeCNH(){
		$cand = new cCANDIDATO();
		$cand->Recuperar(19733);
		$teste  = rand(1000000, 9999999);
		$testeDt = '15/09/2010';
		$this->assertNotEquals($teste, $cand->mNU_CNH, 'Candidato já possui o dado de teste');
		$cand->mNU_CNH = $teste;
		$cand->mDT_EXPIRACAO_CNH = $testeDt;
		$cand->AtualizeCNH();
		$cand = new cCANDIDATO();
		$cand->Recuperar(19733);
		$this->assertEquals($teste, $cand->mNU_CNH, 'Valor da CNH não bate');		
		$this->assertEquals($testeDt, $cand->mDT_EXPIRACAO_CNH);		
	}
	public function  testAtualizeCTPS(){
		$cand = new cCANDIDATO();
		$cand->Recuperar(19733);
		$teste  = rand(1000000, 9999999);
		$testeDt = '05/09/2012';
		$this->assertNotEquals($teste, $cand->mNU_CTPS, 'Candidato já possui o dado de teste');
		$cand->mNU_CTPS = $teste;
		$cand->mDT_EXPIRACAO_CTPS = $testeDt;
		$cand->AtualizeCTPS();
		$cand = new cCANDIDATO();
		$cand->Recuperar(19733);
		$this->assertEquals($teste, $cand->mNU_CTPS, 'Valor da CTPS não bate');		
		$this->assertEquals($testeDt, $cand->mDT_EXPIRACAO_CTPS);		
	}
	
	public function testAtualizeVistoAtual(){
		$cand = new cCANDIDATO();
		$this->assertEquals('', $cand->mcodigo_processo_mte_atual);
		$cand->Criar(rotinasComuns::randTexto(), rotinasComuns::randTexto(), rotinasComuns::randTexto(), cSESSAO::$mcd_usuario);
		$this->assertNotEmpty($cand->mNU_CANDIDATO);
		$nu_candidato = $cand->mNU_CANDIDATO;
		$cand->AtualizeVistoAtual(rand(1000000,9999999));
		$codigo_mte = $cand->mcodigo_processo_mte_atual;
		$this->assertNotEmpty($codigo_mte);
		unset($cand);
		$cand = new cCANDIDATO();
		$cand->Recuperar($nu_candidato);
		$this->assertEquals($codigo_mte, $cand->mcodigo_processo_mte_atual);
		$cand->AtualizeVistoAtual(rand(1000000,9999999));
		$codigo_mte = $cand->mcodigo_processo_mte_atual;
		unset($cand);
		$cand = new cCANDIDATO();
		$cand->Recuperar($nu_candidato);
		$this->assertEquals($codigo_mte, $cand->mcodigo_processo_mte_atual);
	}

	public function testCriaCandidatoCliente(){
		$cand = new cCANDIDATO();
		$cand->Recuperar(15670);
	}

	public function testRecuperesePeloNome(){
		$NO_PRIMEIRO_NOME = rotinasComuns::randTexto();
		$NO_NOME_MEIO = rotinasComuns::randTexto();
		$NO_ULTIMO_NOME = rotinasComuns::randTexto();
		$NO_MAE = rotinasComuns::randTexto();
		$DT_NASCIMENTO = rotinasComuns::randData();
		$cand = new cCANDIDATO();
		$cand->mNO_PRIMEIRO_NOME = $NO_PRIMEIRO_NOME;
		$cand->mNO_NOME_MEIO = $NO_NOME_MEIO;
		$cand->mNO_ULTIMO_NOME = $NO_ULTIMO_NOME;
		$cand->mNO_MAE = $NO_MAE;
		$cand->mDT_NASCIMENTO = $DT_NASCIMENTO;
		$cand->mNU_EMPRESA = "21";
		$cand->Salve_Novo();
		$this->assertEquals($NO_PRIMEIRO_NOME.' '.$NO_NOME_MEIO.' '.$NO_ULTIMO_NOME, $cand->mNOME_COMPLETO);
		$this->assertNotEmpty($cand->mNU_CANDIDATO);
		$NOME_COMPLETO = $cand->mNOME_COMPLETO;
		$NU_CANDIDATO = $cand->mNU_CANDIDATO;
		$cand = new cCANDIDATO();
		$cand->RecuperesePeloNome($NOME_COMPLETO, $NO_MAE, $DT_NASCIMENTO);
		$this->assertEquals($NU_CANDIDATO, $cand->mNU_CANDIDATO);
		$cand = new cCANDIDATO();
		$cand->RecuperesePeloNome($NOME_COMPLETO.'x', $NO_MAE, $DT_NASCIMENTO);
		$this->assertEmpty($cand->mNU_CANDIDATO);
		$cand = new cCANDIDATO();
		$cand->RecuperesePeloNome($NOME_COMPLETO, $NO_MAE.'x', $DT_NASCIMENTO);
		$this->assertEmpty($cand->mNU_CANDIDATO);
		$cand = new cCANDIDATO();
		$cand->RecuperesePeloNome($NOME_COMPLETO, $NO_MAE, rotinasComuns::randData());
		$this->assertEmpty($cand->mNU_CANDIDATO);
	}

	public function testDisponibilizeCadastroParaCliente(){
		$this->InicializeValoresTeste();
		$this->CarregaValores();
		$this->object->Salve_Novo();
		$this->object->Atualizar();
		$this->object->AtualizeCadastroOS();
		$NU_CANDIDATO = $this->object->mNU_CANDIDATO;
		// Garante que não há nenhum temporário cadastrado
		$sql = "delete from candidato_tmp where NU_CANDIDATO = ".$NU_CANDIDATO;
		cAMBIENTE::$db_pdo->exec($sql);
		// Cria o cadastro temporário
		$this->object->DisponibilizeCadastroParaCliente();
		// Verifica se o temporário está igual ao principal.
		$cand = new cCANDIDATO();
		$cand->mNU_CANDIDATO = $this->object->mNU_CANDIDATO;
		$cand->Recuperar_Externo();
		var_dump($cand);
		$this->assertNotEmpty($cand->mnu_candidato_tmp);
		$this->TestaEquivalencia($this->object, $cand);
	}

	public function testAtualizeCadastroParaCliente(){
		$this->InicializeValoresTeste();
		$this->CarregaValores();
		$this->object->Salve_Novo();
		$this->object->Atualizar();
		$this->object->AtualizeCadastroOS();
		$this->object->DisponibilizeCadastroParaCliente();
		// Confirma que criou o clone igual
		$cand = new cCANDIDATO();
		$cand->Recuperar_Externo($this->object->mNU_CANDIDATO);
		$this->TestaEquivalencia($this->object, $cand);
		// Altera os valores do clone
		$this->InicializeValoresTeste();
		$this->AtribuiValores($cand);
		$cand->AtualizarExterno();
		// Confirma que está diferente
		$cand = new cCANDIDATO();
		$cand->Recuperar_Externo($this->object->mNU_CANDIDATO);
		$this->TestaDiferenca($this->object, $cand);
		// Sincroniza
		$this->object->AtualizeCadastroParaCliente();
		// Confirma que ficou igual
		$cand = new cCANDIDATO();
		$cand->Recuperar_Externo($this->object->mNU_CANDIDATO);
		$this->assertEquals($this->object->mNO_PRIMEIRO_NOME, $cand->mNO_PRIMEIRO_NOME);
	}

	public function testLibereInativoParaClienteDeOutraEmpresa(){
		$this->InicializeValoresTeste();
		$this->CarregaValores();
		$this->object->Salve_Novo();
		$this->object->Atualizar();
		$sql = "update CANDIDATO set FL_INATIVO = 1 , FL_ATUALIZACAO_HABILITADA = 0 where NU_CANDIDATO = ".$this->object->mNU_CANDIDATO;
		cAMBIENTE::$db_pdo->exec($sql);
		$cand = new cCANDIDATO();
		$cand->Recuperar($this->object->mNU_CANDIDATO);
		$this->assertEquals(1,$cand->mFL_INATIVO, 'Teste do valor original do flag inativo');
		$this->assertEquals(0,$cand->mFL_ATUALIZACAO_HABILITADA, 'Teste do valor original do flag de atualizacao');
		$this->assertNotEmpty($cand->mNU_EMBARCACAO_PROJETO, 'Teste do valor original da embarcacao projeto');

		$cand->LibereInativoParaClienteDeOutraEmpresa($this->object->mNU_EMPRESA+1);
		$cand = new cCANDIDATO();
		$cand->Recuperar($this->object->mNU_CANDIDATO);
		$this->assertEquals(0,$cand->mFL_INATIVO, 'Teste do valor atualizado do flag inativo');
		$this->assertEquals(1,$cand->mFL_ATUALIZACAO_HABILITADA, 'Teste do valor atualizado do flag de atualizacao');
		$candT = new cCANDIDATO();
		$candT->Recuperar_Externo($this->object->mNU_CANDIDATO);
		$this->assertEmpty($candT->mVA_REMUNERACAO_MENSAL, 'Teste do valor atualizado do remuneracao');
		$this->assertEmpty($candT->mVA_REMUNERACAO_MENSAL_BRASIL, 'Teste do valor atualizado do remuneracao brasil');
		$this->assertEmpty($candT->mCO_FUNCAO, 'Teste do valor atualizado da funcao');
		$this->assertEmpty($candT->mNU_EMBARCACAO_PROJETO, 'Teste do valor atualizado da embarcacao projeto');
	}

	public function testLibereInativoParaClienteDaMesmaEmpresa(){
		$this->InicializeValoresTeste();
		$this->CarregaValores();
		$this->object->Salve_Novo();
		$this->object->Atualizar();
		$sql = "update CANDIDATO set FL_INATIVO = 1 , FL_ATUALIZACAO_HABILITADA = 0 where NU_CANDIDATO = ".$this->object->mNU_CANDIDATO;
		cAMBIENTE::$db_pdo->exec($sql);
		$cand = new cCANDIDATO();
		$cand->Recuperar($this->object->mNU_CANDIDATO);
		$this->assertEquals(1,$cand->mFL_INATIVO, 'Teste do valor original do flag inativo');
		$this->assertEquals(0,$cand->mFL_ATUALIZACAO_HABILITADA, 'Teste do valor original do flag de atualizacao');
		$cand->LibereInativoParaClienteDaMesmaEmpresa();
		$cand = new cCANDIDATO();
		$cand->Recuperar($this->object->mNU_CANDIDATO);
		$this->assertEquals(0,$cand->mFL_INATIVO, 'Teste do valor atualizado do flag inativo');
		$this->assertEquals(1,$cand->mFL_ATUALIZACAO_HABILITADA, 'Teste do valor atualizado do flag de atualizacao');
	}

	public function testCriarNaoEmbarcado(){
		$cand = new cCANDIDATO();
		$cand->Criar(rotinasComuns::randTexto(),rotinasComuns::randTexto(),rotinasComuns::randTexto(),38);
		$id = $cand->mNU_CANDIDATO;
		$cand = new cCANDIDATO();
		$cand->Recuperar($id);
		$this->assertEquals('0', $cand->mFL_EMBARCADO);
	}

	public function testAtualizar(){
		$this->InicializeValoresTeste();
		$this->CarregaValores();
		$this->object->Salve_Novo();
		$this->object->Atualizar();		
	}

	public function testDisponibilizeSeParaCliente_MesmaEmpresa(){
		
	}

	public function testVistoAtual(){
		$this->object->Recuperar(17266);
		$vistoatual = $this->object->VistoAtual();
		var_dump($vistoatual);
	}

    public function provider(){
        return array(
          array('A', '', '', 'A'),
          array('A', 'B', '', 'A B'),
          array('A', 'B', 'C', 'A B C'),
          array('', 'B', '', 'B'),
          array('', '', 'C', 'C'),
          array('', 'B', 'C', 'B C'),
          array('A', '', 'C', 'A C')
        );    	
    }
}
