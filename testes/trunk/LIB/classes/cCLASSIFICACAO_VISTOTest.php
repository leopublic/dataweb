<?php
/**
 * Description of cCLASSIFICACAO_VISTOTest
 *
 * @author leonardo
 */
class cCLASSIFICACAO_VISTOTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var cCLASSIFICACAO_VISTO
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new cCLASSIFICACAO_VISTO();
	}

	public static function Stub(){
		$sql = "select * from CLASSIFICACAO_VISTO";
		$res = cAMBIENTE::ConectaQuery($sql,__CLASS__.".".__FUNCTION__);
		$rs = $res->fetch(PDO::FETCH_ASSOC );
		$obj = new cCLASSIFICACAO_VISTO();
		$obj->set_CO_CLASSIFICACAO_VISTO($rs['CO_CLASSIFICACAO_VISTO']);
		$obj->set_NO_CLASSIFICACAO_VISTO($rs['NO_CLASSIFICACAO_VISTO']);
		return $obj;
	}
	
	public function testRecuperaStub(){
		$this->object = self::Stub();
		var_dump($this->object);
		$this->assertNotEmpty($this->object->get_CO_CLASSIFICACAO_VISTO());
		$this->assertNotEmpty($this->object->get_NO_CLASSIFICACAO_VISTO());
		
	}
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {

	}
	//put your code here
}
