<?php
class cBANCOTest extends PHPUnit_Framework_TestCase
{
	/**
	 * 
	 */
    public function testValorParaBanco()
    {
			$xCampo = new cCAMPO('teste', 'teste', cCAMPO::cpTEXTO );

			$xCampo->mValor = "teste";
    		$this->assertEquals("'teste'", cBANCO::valorParaBanco($xCampo), "Campo texto preenchido");
			$xCampo->mTipo = cCAMPO::cpMEMO;
    		$this->assertEquals("'teste'", cBANCO::valorParaBanco($xCampo), "Campo memo preenchido");
			$xCampo->mValor = "";
    		$this->assertEquals("null", cBANCO::valorParaBanco($xCampo), "Campo texto vazio");
			
    		$xCampo->mTipo = cCAMPO::cpCODIGO;
			$xCampo->mValor = "teste";
    		$this->assertEquals("'teste'", cBANCO::valorParaBanco($xCampo), "Campo codigo preenchido");

    		$xCampo->mTipo = cCAMPO::cpDATA;
			$xCampo->mValor = "29/12/2010";
			$this->assertEquals("'2010-12-29'", cBANCO::valorParaBanco($xCampo));
			$xCampo->mValor = "29/12/10";
			$this->assertEquals("'2010-12-29'", cBANCO::valorParaBanco($xCampo));

    		$xCampo->mTipo = cCAMPO::cpCHAVE_ESTR;
			$xCampo->mValor = "";
			$this->assertEquals("null", cBANCO::valorParaBanco($xCampo));
			$xCampo->mValor = 10;
			$this->assertEquals(10, cBANCO::valorParaBanco($xCampo));

			$xCampo->mTipo = cCAMPO::cpCHAVE;
			$xCampo->mValor = "";
			$this->assertEquals("null", cBANCO::valorParaBanco($xCampo));
			$xCampo->mValor = 10;
			$this->assertEquals(10, cBANCO::valorParaBanco($xCampo));
			
			$xCampo->mTipo = cCAMPO::cpSIMNAO;
			$xCampo->mValor = "";
			$this->assertEquals("null", cBANCO::valorParaBanco($xCampo));
			$xCampo->mValor = 1;
			$this->assertEquals(1, cBANCO::valorParaBanco($xCampo));
			
			$xCampo->mTipo = cCAMPO::cpVALOR;
			$xCampo->mValor = "10,20";
			try{
				$x = cBANCO::valorParaBanco($xCampo);
			}
			catch(Exception $e ){
				$this->assertEquals("Formatação do valor para o tipo informado ".$xCampo->mNome." tipo=".$xCampo->mTipo." ainda não implementado", $e->getMessage());			
			}
			
    }

    public function testFormatacao(){
    	$this->assertEquals("0", cBANCO::InteiroOk("0"));
    	$this->assertEquals("0", cBANCO::InteiroOk(0));
    }
    
    public function testDataFmt(){
    	$this->assertEquals("31/12/2010", cBANCO::DataFmt("2010-12-31 10:00:00" ));
    	$this->assertEquals("31/12/2010 10:00:00", cBANCO::DataFmt("2010-12-31 10:00:00", true,true ));
    	$this->assertEquals("31/12/2010", cBANCO::DataFmt("2010-12-31" ));
    	$this->assertEquals("31/12/2010", cBANCO::DataFmt("31/12/2010" ));
    	$this->assertEquals("31/12/2010", cBANCO::DataFmt("31-12-2010" ));
    	$this->assertEquals("31/12/2010", cBANCO::DataFmt("31/12/10" ));
    }
    public function testDataOk(){
    	$this->assertEquals("'2010-12-31 10:00:00'", cBANCO::DataOk("2010-12-31 10:00:00" ));
    	$this->assertEquals("'2010-12-31 10:00'", cBANCO::DataOk("2010-12-31 10:00" ));
    	$this->assertEquals("'2010-12-31 10:00'", cBANCO::DataOk("31/12/2010 10:00" ));
    	$this->assertEquals("'2010-12-31'", cBANCO::DataOk("2010-12-31" ));
    	$this->assertEquals("'2010-12-31'", cBANCO::DataOk("31/12/2010" ));
    	$this->assertEquals("'2010-12-31'", cBANCO::DataOk("31-12-2010" ));
    	$this->assertEquals("'2010-12-31'", cBANCO::DataOk("31/12/10" ));
    }
}
