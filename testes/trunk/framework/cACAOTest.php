<?php
class cACAOTest extends PHPUnit_Framework_TestCase{
    public function testConstruct(){
    	$label = rand(10000000, 99999999);
    	$codigo = rand(10000000, 99999999);
    	$titulo = rand(10000000, 99999999);
    	$msgConf = rand(10000000, 99999999);
    	$msgConfTit = rand(10000000, 99999999);
    	$acao = new cACAO(cACAO::ACAO_FORM, $label, $codigo , $titulo , $msgConf, $msgConfTit);
    	$this->assertEquals(cACAO::ACAO_FORM, $acao->mTipo, 'Tipo');
    	$this->assertEquals($label, $acao->mLabel, 'Label');
    	$this->assertEquals($codigo, $acao->mCodigo, 'Codigo');
    	$this->assertEquals($titulo, $acao->mTitulo, 'Titulo');
    	$this->assertEquals($msgConf, $acao->mMsgConfirmacao, 'Msg Conf');
    	$this->assertEquals($msgConfTit, $acao->mMsgConfirmacaoTitulo, 'Msg conf titulo');
    	
    }  



}
