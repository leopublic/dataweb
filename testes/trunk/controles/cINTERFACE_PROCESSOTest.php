<?php
require_once dirname(__FILE__) . '/../LIB/classes/cprocesso_mteTest.php';
require_once dirname(__FILE__) . '/../LIB/classes/cprocesso_prorrogTest.php';
require_once dirname(__FILE__) . '/../LIB/classes/cprocesso_emiscieTest.php';
require_once dirname(__FILE__) . '/../LIB/classes/cprocesso_coletaTest.php';
require_once dirname(__FILE__) . '/../LIB/classes/cprocesso_cancelTest.php';
require_once dirname(__FILE__) . '/../LIB/classes/cprocesso_regcieTest.php';
require_once dirname(__FILE__) . '/../LIB/classes/cORDEMSERVICOTest.php';
/**
 * Test class for cINTERFACE_PROCESSO.
 * Generated by PHPUnit on 2012-02-07 at 16:29:22.
 */
class cINTERFACE_PROCESSOTest extends PHPUnit_Framework_TestCase {
	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {

	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testNomeTelaDoAcompanhamento().
	 */
	public function testNomeTelaDoAcompanhamento() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testTelaProcesso().
	 */
	public function testTelaProcesso() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testFormProcesso().
	 */
	public function testFormProcesso() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testFormProcessoEdit().
	 */
	public function testFormProcessoEdit() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testFormProcessoView().
	 */
	public function testFormProcessoView() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testAcoes().
	 */
	public function testAcoes() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testObterParametros().
	 */
	public function testObterParametros() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostPROCESSO_MTE().
	 */
	public function testPostPROCESSO_MTE() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostANDAMENTO_MTE().
	 */
	public function testPostANDAMENTO_MTE() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostCONCLUSAO_MTE().
	 */
	public function testPostCONCLUSAO_MTE() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_mte_view().
	 */
	public function testPostprocesso_mte_view() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_prorrog_view().
	 */
	public function testPostprocesso_prorrog_view() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_regcie_view().
	 */
	public function testPostprocesso_regcie_view() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_coleta_view().
	 */
	public function testPostprocesso_coleta_view() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_coleta_cie_view().
	 */
	public function testPostprocesso_coleta_cie_view() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_emiscie_view().
	 */
	public function testPostprocesso_emiscie_view() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_cancel_view().
	 */
	public function testPostprocesso_cancel_view() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_view().
	 */
	public function testPostprocesso_view() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	public function testPostprocesso_mte_edit() {
		$processo_mteTeste = new cprocesso_mteTest();
		$processo_mteTeste->InicializaObjeto();
		$processo_mteTeste->InicializaValores();
		$processo_mteTeste->CarregaValores();
		$processo_mteTeste->object->Salve();
		$codigo = $processo_mteTeste->object->mcodigo;
		$id_solicita_visto = $processo_mteTeste->object->mid_solicita_visto;
		$cd_candidato = $processo_mteTeste->object->mcd_candidato;
		//
		// Muda os valores como se o usuário tivesse digitado
		$processo_mteTeste->InicializaObjeto();
		$processo_mteTeste->InicializaValores();
		$processo_mteTeste->CarregaValores();
		//
		// Atribui os valores ao post
		$POST['CMP___acao'] = 'SALVAR';
		$POST['CMP_id_solicita_visto'] = $id_solicita_visto;
		$POST['CMP_codigo'] = $codigo;
		$POST['CMP_cd_candidato'] = $cd_candidato;
		$POST['CMP_NU_EMPRESA'] = $processo_mteTeste->object->mNU_EMPRESA;
		$POST['CMP_NU_EMBARCACAO_PROJETO'] = $processo_mteTeste->object->mNU_EMBARCACAO_PROJETO;
		$POST['CMP_nu_processo'] = $processo_mteTeste->object->mnu_processo;
		$POST['CMP_dt_requerimento'] = $processo_mteTeste->object->mdt_requerimento;
		$POST['CMP_cd_funcao'] = $processo_mteTeste->object->mcd_funcao;
		$POST['CMP_cd_reparticao'] = $processo_mteTeste->object->mcd_reparticao;
		$POST['CMP_nu_oficio'] = $processo_mteTeste->object->mnu_oficio;
		$POST['CMP_dt_deferimento'] = $processo_mteTeste->object->mdt_deferimento;
		$POST['CMP_prazo_solicitado'] = $processo_mteTeste->object->mprazo_solicitado;
		$POST['CMP_observacao'] = $processo_mteTeste->object->mobservacao;

		$tela = new cEDICAO();
		$tela->CarregarConfiguracao('processo_mte_edit');
		cINTERFACE::RecupereValoresEdicao($tela, $POST);

		$FILES = array();
		$SESSION = array();

		cINTERFACE_PROCESSO::Postprocesso_mte_edit($tela, $POST, $FILES, $SESSION);

		// Recupera o objeto novamente do banco e verifica se os valores foram alterados
		$processo_mteTeste->InicializaObjeto($codigo);

		$this->assertEquals($POST['CMP_codigo'] , $codigo );
		$this->assertEquals($POST['CMP_id_solicita_visto'] , $id_solicita_visto );
		$this->assertEquals($POST['CMP_cd_candidato'] , $cd_candidato );
		$this->assertEquals($POST['CMP_NU_EMPRESA'] , $processo_mteTeste->object->mNU_EMPRESA );
		$this->assertEquals($POST['CMP_NU_EMBARCACAO_PROJETO'] , $processo_mteTeste->object->mNU_EMBARCACAO_PROJETO );
		$this->assertEquals($POST['CMP_nu_processo'] , $processo_mteTeste->object->mnu_processo );
		$this->assertEquals($POST['CMP_dt_requerimento'] , $processo_mteTeste->object->mdt_requerimento );
		$this->assertEquals($POST['CMP_cd_funcao'] , $processo_mteTeste->object->mcd_funcao );
		$this->assertEquals($POST['CMP_cd_reparticao'] , $processo_mteTeste->object->mcd_reparticao );
		$this->assertEquals($POST['CMP_nu_oficio'] , $processo_mteTeste->object->mnu_oficio );
		$this->assertEquals($POST['CMP_dt_deferimento'] , $processo_mteTeste->object->mdt_deferimento );
		$this->assertEquals($POST['CMP_prazo_solicitado'] , $processo_mteTeste->object->mprazo_solicitado );
		$this->assertEquals($POST['CMP_observacao'] , $processo_mteTeste->object->mobservacao );
	}

	public function testPostprocesso_prorrog_edit() {
		$test = new cprocesso_prorrogTest();
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();

		// Monta OS real para rastrear alteração no prazo pretendido
		//TODO: Adicionar teste de atualização do prazo pretendido da OS
		$osTest = new cORDEMSERVICOTest();
		$osTest->InicializaObjeto();
		$osTest->InicializaValores();
		$osTest->CarregaValores();
		$osTest->object->SalvarSolicitaVisto();

		$test->object->mid_solicita_visto = $osTest->object->mID_SOLICITA_VISTO;

		$test->object->Salve();
		$codigo =  $test->object->mcodigo;
		$objectOriginal = $test->object;
		//
		// Armazena valores protegidos
		$POST['CMP___acao'] = 'SALVAR';
		$POST['CMP_id_solicita_visto'] = $objectOriginal->mid_solicita_visto;
		$POST['CMP_codigo'] = $objectOriginal->mcodigo;
		$POST['CMP_cd_candidato'] = $objectOriginal->mcd_candidato;
		$POST['CMP_cd_solicitacao'] = $objectOriginal->mcd_solicitacao;
		$POST['CMP_codigo_processo_mte'] = $objectOriginal->mcodigo_processo_mte;
		$POST['CMP_nu_servico'] = $objectOriginal->mnu_servico;
		//
		// Muda os valores abertos para input
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();

		$objectAlterado = $test->object;

		$POST['CMP_nu_protocolo'] = $objectAlterado->mnu_protocolo;
		$POST['CMP_dt_requerimento'] = $objectAlterado->mdt_requerimento;
		$POST['CMP_dt_validade'] = $objectAlterado->mdt_validade;
		$POST['CMP_dt_prazo_pret'] = $objectAlterado->mdt_prazo_pret;
		$POST['CMP_dt_publicacao_dou'] = $objectAlterado->mdt_publicacao_dou;
		$POST['CMP_ID_STATUS_CONCLUSAO'] = $objectAlterado->mID_STATUS_CONCLUSAO;
		$POST['CMP_observacao'] = $objectAlterado->mobservacao;
		$POST['CMP_ID_STATUS_SOL'] = $objectAlterado->mID_STATUS_SOL;

		$tela = new cEDICAO();
		$tela->CarregarConfiguracao('processo_prorrog_edit');
		cINTERFACE::RecupereValoresEdicao($tela, $POST);

		$FILES = array();
		$SESSION = array();

		cINTERFACE_PROCESSO::Postprocesso_prorrog_edit($tela, $POST, $FILES, $SESSION);
		$test->InicializaObjeto($codigo);

		$this->assertEquals($POST['CMP_codigo'] , $test->object->mcodigo );
		$this->assertEquals($POST['CMP_id_solicita_visto'] , $test->object->mid_solicita_visto );
		$this->assertEquals($POST['CMP_cd_candidato'] , $test->object->mcd_candidato );
		$this->assertEquals($POST['CMP_cd_solicitacao'] , $test->object->mcd_solicitacao );
		$this->assertEquals($POST['CMP_codigo_processo_mte'] , $test->object->mcodigo_processo_mte );

		$this->assertEquals($POST['CMP_nu_servico'] , $test->object->mnu_servico );
		$this->assertEquals($POST['CMP_nu_protocolo'] , $test->object->mnu_protocolo );
		$this->assertEquals($POST['CMP_dt_requerimento'] , $test->object->mdt_requerimento );
		$this->assertEquals($POST['CMP_dt_validade'] , $test->object->mdt_validade );
		$this->assertEquals($POST['CMP_dt_prazo_pret'] , $test->object->mdt_prazo_pret );
		$this->assertEquals($POST['CMP_dt_publicacao_dou'] , $test->object->mdt_publicacao_dou );
		$this->assertEquals($POST['CMP_ID_STATUS_CONCLUSAO'] , $test->object->mID_STATUS_CONCLUSAO );
		$this->assertEquals($POST['CMP_ID_STATUS_SOL'] , $test->object->mID_STATUS_SOL );
		$this->assertEquals($POST['CMP_observacao'] , $test->object->mobservacao );
		//
		// Verifica se a data da OS foi alterada
		$id_solicita_visto = $osTest->object->mID_SOLICITA_VISTO;
		$osTest->InicializaObjeto();
		$osTest->object->Recuperar($id_solicita_visto);
//		$this->assertEquals($POST['CMP_dt_prazo_pret'], $osTest->object->mDT_PRAZO_ESTADA_SOLICITADO);


	}

	public function testPostprocesso_regcie_edit() {
		$test = new cprocesso_coletaTest();
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();

		// Monta OS real para rastrear alteração no prazo pretendido
		//TODO: Adicionar teste de atualização do prazo pretendido da OS

		$test->object->Salve();
		$codigo =  $test->object->mcodigo;
		$objectOriginal = $test->object;
		//
		// Armazena valores protegidos
		$POST['CMP___acao'] = 'SALVAR';
		$POST['CMP_ID_STATUS_SOL'] = $objectOriginal->mID_STATUS_SOL;
		$POST['CMP_id_solicita_visto'] = $objectOriginal->mid_solicita_visto;
		$POST['CMP_cd_candidato'] = $objectOriginal->mcd_candidato;
		$POST['CMP_codigo'] = $objectOriginal->mcodigo;
		$POST['CMP_codigo_processo_mte'] = $objectOriginal->mcodigo_processo_mte;
		$POST['CMP_nu_servico'] = $objectOriginal->mnu_servico;		//
		// Muda os valores abertos para input
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();

		$objectAlterado = $test->object;

		$POST['CMP_dt_emissao_visto'] = $objectAlterado->mdt_emissao_visto;
		$POST['CMP_dt_entrada'] = $objectAlterado->mdt_entrada;
		$POST['CMP_co_pais_nacionalidade_visto'] = $objectAlterado->mco_pais_nacionalidade_visto;
		$POST['CMP_co_uf'] = $objectAlterado->mco_uf;
		$POST['CMP_nu_transporte_entrada'] = $objectAlterado->mnu_transporte_entrada;
		$POST['CMP_no_validade'] = $objectAlterado->mno_validade;
		$POST['CMP_nu_visto'] = $objectAlterado->mnu_visto;
		$POST['CMP_no_local_emissao'] = $objectAlterado->mno_local_emissao;
		$POST['CMP_NO_CLASSIFICACAO_VISTO'] = $objectAlterado->mNO_CLASSIFICACAO_VISTO;
		$POST['CMP_no_local_entrada'] = $objectAlterado->mno_local_entrada;

		$tela = new cEDICAO();
		$tela->CarregarConfiguracao('processo_coleta_edit');
		cINTERFACE::RecupereValoresEdicao($tela, $POST);

		$FILES = array();
		$SESSION = array();

		cINTERFACE_PROCESSO::Postprocesso_regcie_edit($tela, $POST, $FILES, $SESSION);
		$test->InicializaObjeto($codigo);

		$this->assertEquals($POST['CMP_ID_STATUS_SOL'] , $test->object->mID_STATUS_SOL );
		$this->assertEquals($POST['CMP_id_solicita_visto'] , $test->object->mid_solicita_visto );
		$this->assertEquals($POST['CMP_cd_candidato'] , $test->object->mcd_candidato );
		$this->assertEquals($POST['CMP_codigo'] , $test->object->mcodigo );
		$this->assertEquals($POST['CMP_codigo_processo_mte'] , $test->object->mcodigo_processo_mte );
		$this->assertEquals($POST['CMP_nu_servico'] , $test->object->mnu_servico );

		$this->assertEquals($POST['CMP_dt_emissao_visto'] , $test->object->mdt_emissao_visto );
		$this->assertEquals($POST['CMP_dt_entrada'] , $test->object->mdt_entrada );
		$this->assertEquals($POST['CMP_co_pais_nacionalidade_visto'] , $test->object->mco_pais_nacionalidade_visto );
		$this->assertEquals($POST['CMP_co_uf'] , $test->object->mco_uf );
		$this->assertEquals($POST['CMP_nu_transporte_entrada'] , $test->object->mnu_transporte_entrada );
		$this->assertEquals($POST['CMP_no_validade'] , $test->object->mno_validade );
		$this->assertEquals($POST['CMP_nu_visto'] , $test->object->mnu_visto );
		$this->assertEquals($POST['CMP_no_local_emissao'] , $test->object->mno_local_emissao );
		$this->assertEquals($POST['CMP_NO_CLASSIFICACAO_VISTO'] , $test->object->mNO_CLASSIFICACAO_VISTO );
		$this->assertEquals($POST['CMP_no_local_entrada'] , $test->object->mno_local_entrada );
	}

	public function testPostprocesso_coleta_edit() {
		$test = new cprocesso_coletaTest();
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();

		// Monta OS real para rastrear alteração no prazo pretendido
		//TODO: Adicionar teste de atualização do prazo pretendido da OS

		$test->object->Salve();
		$codigo =  $test->object->mcodigo;
		$objectOriginal = $test->object;
		//
		// Armazena valores protegidos
		$POST['CMP___acao'] = 'SALVAR';
		$POST['CMP_ID_STATUS_SOL'] = $objectOriginal->mID_STATUS_SOL;
		$POST['CMP_id_solicita_visto'] = $objectOriginal->mid_solicita_visto;
		$POST['CMP_cd_candidato'] = $objectOriginal->mcd_candidato;
		$POST['CMP_codigo'] = $objectOriginal->mcodigo;
		$POST['CMP_codigo_processo_mte'] = $objectOriginal->mcodigo_processo_mte;
		$POST['CMP_nu_servico'] = $objectOriginal->mnu_servico;		//
		// Muda os valores abertos para input
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();

		$objectAlterado = $test->object;

		$POST['CMP_dt_emissao_visto'] = $objectAlterado->mdt_emissao_visto;
		$POST['CMP_dt_entrada'] = $objectAlterado->mdt_entrada;
		$POST['CMP_co_pais_nacionalidade_visto'] = $objectAlterado->mco_pais_nacionalidade_visto;
		$POST['CMP_co_uf'] = $objectAlterado->mco_uf;
		$POST['CMP_nu_transporte_entrada'] = $objectAlterado->mnu_transporte_entrada;
		$POST['CMP_no_validade'] = $objectAlterado->mno_validade;
		$POST['CMP_nu_visto'] = $objectAlterado->mnu_visto;
		$POST['CMP_no_local_emissao'] = $objectAlterado->mno_local_emissao;
		$POST['CMP_NO_CLASSIFICACAO_VISTO'] = $objectAlterado->mNO_CLASSIFICACAO_VISTO;
		$POST['CMP_no_local_entrada'] = $objectAlterado->mno_local_entrada;

		$tela = new cEDICAO();
		$tela->CarregarConfiguracao('processo_coleta_edit');
		cINTERFACE::RecupereValoresEdicao($tela, $POST);

		$FILES = array();
		$SESSION = array();

		cINTERFACE_PROCESSO::Postprocesso_coleta_edit($tela, $POST, $FILES, $SESSION);
		$test->InicializaObjeto($codigo);

		$this->assertEquals($POST['CMP_ID_STATUS_SOL'] , $test->object->mID_STATUS_SOL );
		$this->assertEquals($POST['CMP_id_solicita_visto'] , $test->object->mid_solicita_visto );
		$this->assertEquals($POST['CMP_cd_candidato'] , $test->object->mcd_candidato );
		$this->assertEquals($POST['CMP_codigo'] , $test->object->mcodigo );
		$this->assertEquals($POST['CMP_codigo_processo_mte'] , $test->object->mcodigo_processo_mte );
		$this->assertEquals($POST['CMP_nu_servico'] , $test->object->mnu_servico );

		$this->assertEquals($POST['CMP_dt_emissao_visto'] , $test->object->mdt_emissao_visto );
		$this->assertEquals($POST['CMP_dt_entrada'] , $test->object->mdt_entrada );
		$this->assertEquals($POST['CMP_co_pais_nacionalidade_visto'] , $test->object->mco_pais_nacionalidade_visto );
		$this->assertEquals($POST['CMP_co_uf'] , $test->object->mco_uf );
		$this->assertEquals($POST['CMP_nu_transporte_entrada'] , $test->object->mnu_transporte_entrada );
		$this->assertEquals($POST['CMP_no_validade'] , $test->object->mno_validade );
		$this->assertEquals($POST['CMP_nu_visto'] , $test->object->mnu_visto );
		$this->assertEquals($POST['CMP_no_local_emissao'] , $test->object->mno_local_emissao );
		$this->assertEquals($POST['CMP_NO_CLASSIFICACAO_VISTO'] , $test->object->mNO_CLASSIFICACAO_VISTO );
		$this->assertEquals($POST['CMP_no_local_entrada'] , $test->object->mno_local_entrada );
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_coleta_cie_edit().
	 */
	public function testPostprocesso_coleta_cie_edit() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	public function testPostprocesso_emiscie_edit() {
		$test = new cprocesso_emiscieTest();
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();
		$test->object->Salve();
		$codigo =  $test->object->mcodigo;
		$objectOriginal = $test->object;
		//
		// Armazena valores protegidos
		$POST['CMP___acao'] = 'SALVAR';
		$POST['CMP_ID_STATUS_SOL'] = $objectOriginal->mID_STATUS_SOL;
		$POST['CMP_codigo_processo_mte'] = $objectOriginal->mcodigo_processo_mte;
		$POST['CMP_codigo'] = $objectOriginal->mcodigo;
		$POST['CMP_nu_servico'] = $objectOriginal->mnu_servico;
		$POST['CMP_cd_candidato'] = $objectOriginal->mcd_candidato;
		$POST['CMP_cd_solicitacao'] = $objectOriginal->mcd_solicitacao;
		$POST['CMP_id_solicita_visto'] = $objectOriginal->mid_solicita_visto;
		//
		// Muda os valores abertos para input
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();

		$objectAlterado = $test->object;

		$POST['CMP_dt_emissao'] = $objectAlterado->mdt_emissao;
		$POST['CMP_dt_validade'] = $objectAlterado->mdt_validade;
		$POST['CMP_nu_cie'] = $objectAlterado->mnu_cie;
		$POST['CMP_observacao'] = $objectAlterado->mobservacao;

		$tela = new cEDICAO();
		$tela->CarregarConfiguracao('processo_emiscie_edit');
		cINTERFACE::RecupereValoresEdicao($tela, $POST);

		$FILES = array();
		$SESSION = array();

		cINTERFACE_PROCESSO::Postprocesso_emiscie_edit($tela, $POST, $FILES, $SESSION);
		$test->InicializaObjeto($codigo);

		$this->assertEquals($POST['CMP_ID_STATUS_SOL'] , $test->object->mID_STATUS_SOL );
		$this->assertEquals($POST['CMP_codigo_processo_mte'] , $test->object->mcodigo_processo_mte );
		$this->assertEquals($POST['CMP_codigo'] , $test->object->mcodigo );
		$this->assertEquals($POST['CMP_nu_servico'] , $test->object->mnu_servico );
		$this->assertEquals($POST['CMP_cd_candidato'] , $test->object->mcd_candidato );
		$this->assertEquals($POST['CMP_cd_solicitacao'] , $test->object->mcd_solicitacao );
		$this->assertEquals($POST['CMP_id_solicita_visto'] , $test->object->mid_solicita_visto );

		$this->assertEquals($POST['CMP_dt_emissao'] , $test->object->mdt_emissao );
		$this->assertEquals($POST['CMP_dt_validade'] , $test->object->mdt_validade );
		$this->assertEquals($POST['CMP_nu_cie'] , $test->object->mnu_cie );
		$this->assertEquals($POST['CMP_observacao'] , $test->object->mobservacao );
	}

	public function testPostprocesso_cancel_edit() {
		$test = new cprocesso_cancelTest();
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();
		$test->object->Salve();
		$codigo =  $test->object->mcodigo;
		$objectOriginal = $test->object;
		//
		// Armazena valores protegidos
		$POST['CMP___acao'] = 'SALVAR';
		$POST['CMP_cd_candidato'] = $objectOriginal->mcd_candidato;
		$POST['CMP_cd_solicitacao'] = $objectOriginal->mcd_solicitacao;
		$POST['CMP_codigo'] = $objectOriginal->mcodigo;
		$POST['CMP_codigo_processo_mte'] = $objectOriginal->mcodigo_processo_mte;
		$POST['CMP_id_solicita_visto'] = $objectOriginal->mid_solicita_visto;
		$POST['CMP_ID_STATUS_SOL'] = $objectOriginal->mID_STATUS_SOL;
		$POST['CMP_nu_servico'] = $objectOriginal->mnu_servico;		//
		// Muda os valores abertos para input
		$test->InicializaObjeto();
		$test->InicializaValores();
		$test->CarregaValores();

		$objectAlterado = $test->object;

		$POST['CMP_dt_processo'] = $objectAlterado->mdt_processo;
		$POST['CMP_dt_cancel'] = $objectAlterado->mdt_cancel;
		$POST['CMP_nu_processo'] = $objectAlterado->mnu_processo;
		$POST['CMP_observacao'] = $objectAlterado->mobservacao;

		$tela = new cEDICAO();
		$tela->CarregarConfiguracao('processo_cancel_edit');
		cINTERFACE::RecupereValoresEdicao($tela, $POST);

		$FILES = array();
		$SESSION = array();

		cINTERFACE_PROCESSO::Postprocesso_cancel_edit($tela, $POST, $FILES, $SESSION);
		$test->InicializaObjeto($codigo);

		$this->assertEquals($POST['CMP_ID_STATUS_SOL'] , $test->object->mID_STATUS_SOL );
		$this->assertEquals($POST['CMP_codigo_processo_mte'] , $test->object->mcodigo_processo_mte );
		$this->assertEquals($POST['CMP_codigo'] , $test->object->mcodigo );
		$this->assertEquals($POST['CMP_nu_servico'] , $test->object->mnu_servico );
		$this->assertEquals($POST['CMP_cd_candidato'] , $test->object->mcd_candidato );
		$this->assertEquals($POST['CMP_cd_solicitacao'] , $test->object->mcd_solicitacao );
		$this->assertEquals($POST['CMP_id_solicita_visto'] , $test->object->mid_solicita_visto );
		$this->assertEquals($POST['CMP_dt_processo'] , $test->object->mdt_processo );
		$this->assertEquals($POST['CMP_dt_cancel'] , $test->object->mdt_cancel );
		$this->assertEquals($POST['CMP_nu_processo'] , $test->object->mnu_processo );
		$this->assertEquals($POST['CMP_observacao'] , $test->object->mobservacao );
	}
	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_edit().
	 */
	public function testPostprocesso_edit() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_mte_observacao().
	 */
	public function testPostprocesso_mte_observacao() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testFormprocesso_mte_do_processo_edit().
	 */
	public function testFormprocesso_mte_do_processo_edit() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testPostprocesso_mte_do_processo_edit().
	 */
	public function testPostprocesso_mte_do_processo_edit() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

}

?>
