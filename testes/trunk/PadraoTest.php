<?php

class PadraoTest extends PHPUnit_Framework_TestCase
{
    public static function random_string($name_length = 8) {
        $alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        return substr(str_shuffle($alpha_numeric), 0, $name_length);
    }

    public static function gen_random_data($range = 500){
        \Carbon::now()->subDay(rand(0,$range));
    }
}
