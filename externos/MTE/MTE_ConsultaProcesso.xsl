<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/">
		<table border="0" align="center" width="90%">
			<tr height="20"><td> </td></tr>
		</table>
		<fieldset>
		<xsl:attribute name="style">font-size:12px;font-weight:bold</xsl:attribute>
		<legend><label>Processo</label></legend>
		<table width="90%" border="0" align="center" cellPadding="1" cellspacing="1">
			<xsl:for-each select="ContrateWeb/Processo">
				<xsl:element name="TR">
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<td width="65%">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							No.:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@NRProcesso"/>
						</xsl:element>
					</td>
					<td width="35%">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Cadastro:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@DTCadastro"/>
						</xsl:element>
					</td>
				</xsl:element>
				<xsl:element name="TR">
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<td>
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Situacao:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@DSEstadoProcesso"/>
						</xsl:element>
					</td>
					<td>
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Ultima Atualizacao:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@DTDeferimento"/>
						</xsl:element>
					</td>
				</xsl:element>
				<xsl:element name="TR">
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<td>
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Amparo Legal:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@DSAmparoLegal"/>
						</xsl:element>
					</td>
					<td>
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Prazo:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@DSPrazo"/>
						</xsl:element>
					</td>
				</xsl:element>
				<xsl:element name="TR">
						<xsl:choose>
							<xsl:when test="@CDEstadoProcesso = '10'">
								<xsl:attribute name="style">font-size:11px</xsl:attribute>
								<td>
									<xsl:element name="label">
									<xsl:attribute name="style">font-weight:bold</xsl:attribute>
										Estrangeiros Cancelados:
									</xsl:element>
									<font color="#FF0000"><b>
									<xsl:choose>
										<xsl:when test="@NRCancelado > '0'">
											<xsl:element name="label">
												<xsl:value-of select="@NRCancelado"/>
											</xsl:element>
										</xsl:when>
										<xsl:otherwise>
												<xsl:attribute name="style">font-weight:bold</xsl:attribute>
												<font color="#FF0000">0</font>
										</xsl:otherwise>
									</xsl:choose>
									</b></font>
								</td>
							</xsl:when>
							<xsl:otherwise>
									<xsl:attribute name="style">font-weight:bold</xsl:attribute>
									<font color="#0000CC"></font>
							</xsl:otherwise>
						</xsl:choose>
				</xsl:element>
			</xsl:for-each>
		</table>
		</fieldset>
		<fieldset>
		<xsl:attribute name="style">font-size:12px;font-weight:bold;font-color:black</xsl:attribute>
		<legend><label>Requerente</label></legend>
		<table width="90%" border="0" align="center" cellPadding="1" cellspacing="1">
			<xsl:for-each select="ContrateWeb/Requerente">
				<xsl:element name="TR">
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<td>
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Requerente:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@NORequerente"/>
						</xsl:element>
					</td>
				</xsl:element>
			</xsl:for-each>
		</table>
		</fieldset>
		<fieldset>
		<xsl:attribute name="style">font-size:12px;font-weight:bold;font-color:black</xsl:attribute>
		<legend><label>Estrangeiro</label></legend>
		<table width="90%" border="0" align="center" cellPadding="1" cellspacing="1">
			<xsl:for-each select="ContrateWeb/Estrangeiro">
				<xsl:element name="TR">
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<td width="50%">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Nome:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@NOEstrangeiro"/>
						</xsl:element>
					</td>
					<td width="35%">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Passaporte:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@NRPassaporte"/>
						</xsl:element>
					</td>
					<td width="15%">
						<xsl:choose>
							<xsl:when test="@DSSituacaoEstrangeiro = 'Cancelado'">
								<xsl:element name="label">
									<xsl:attribute name="style"></xsl:attribute>
									<font color="#FF0000">Cancelado</font>
								</xsl:element>
							</xsl:when>
							<xsl:when test="@DSSituacaoEstrangeiro = 'Deferido'">
								<xsl:element name="label">
									<xsl:attribute name="style"></xsl:attribute>
									<font color="#0000CC">Deferido</font>
								</xsl:element>
							</xsl:when>
							<xsl:otherwise>
									<xsl:attribute name="style">font-weight:bold</xsl:attribute>
									<font color="#0000CC"></font>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:element>
			</xsl:for-each>
		</table>
		</fieldset>
		<fieldset>
		<xsl:attribute name="style">font-size:12px;font-weight:bold</xsl:attribute>
		<legend><label>Andamentos</label></legend>
		<table width="90%" border="0" align="center" cellPadding="1" cellspacing="1">
			<td width="20%" align="right">
				<div><font color="#000000"><a href="http://www.mte.gov.br/trab_estrang/contato.asp" target="_blank"><img src="/externos/MTE/btf_enviar.gif" width="17" height="12" border="0" align="absmiddle">  Fale Conosco</img></a></font></div>
			</td>
		</table>
		<table width="90%" border="0" align="center" cellPadding="1" cellspacing="1">
			<xsl:for-each select="ContrateWeb/Publicacao">
				<xsl:element name="TR">
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<td colspan="4">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							*CLIQUE NO ANDAMENTO PARA VISUALIZAR OS DADOS.
						</xsl:element>
					</td>
				</xsl:element>
				<xsl:element name="TR">
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<td colspan="4">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Observacao:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@DSObservacaoTela"/>
						</xsl:element>
					</td>
				</xsl:element>
				<xsl:element name="TR">
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<td width="50%" colspan="2">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Oficio MRE:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@NROficioMRETela"/>
						</xsl:element>
					</td>
					<td width="50%" colspan="2">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							No. Caixa Arquivo:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@NRCaixaArquivoTela"/>
						</xsl:element>
					</td>
				</xsl:element>
				<xsl:element name="TR">
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<td width="25%">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							DOU No.:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@NRDOTela"/>
						</xsl:element>
					</td>
					<td width="25%">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Publicacao:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@DTPublicacaoTela"/>
						</xsl:element>
					</td>
					<td width="25%">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Secao:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@NRSecaoDOTela"/>
						</xsl:element>
					</td>
					<td width="25%">
						<xsl:element name="label">
						<xsl:attribute name="style">font-weight:bold</xsl:attribute>
							Pagina:
						</xsl:element>
						<xsl:element name="label">
							<xsl:value-of select="@NRPaginaDOTela"/>
						</xsl:element>
					</td>
				</xsl:element>
			</xsl:for-each>
		</table>
		<table width="90%" border="1" align="center" cellPadding="1" cellspacing="1">
			<xsl:element name="TR">
				<td width="10%" bgcolor="#000066" align="center">
					<div><font color="#FFFFFF">No.</font></div>
				</td>
				<td width="15%" bgcolor="#000066" align="center">
					<div><font color="#FFFFFF">DATA</font></div>
				</td>
				<td width="60%" bgcolor="#000066">
					<div><font color="#FFFFFF">TIPO</font></div>
				</td>
			</xsl:element>
			<xsl:for-each select="ContrateWeb/Andamento">
				<xsl:element name="TR">
				<xsl:attribute name="onMouseOut">mOut(this,'')</xsl:attribute>
					<xsl:attribute name="onMouseOver">mOvr(this,'#C5D2EB')</xsl:attribute>
					<xsl:attribute name="style">font-size:11px</xsl:attribute>
					<xsl:attribute name="onclick">CarregarDadosAndamento(<xsl:value-of select="@SQAndamento"/>)</xsl:attribute>
					<td align="center">
						<xsl:element name="label">
							&#xA0;<xsl:value-of select="@SQAndamento"/>
						</xsl:element>
					</td>
					<td align="center">
						<xsl:element name="label">
							&#xA0;<xsl:value-of select="@DTAndamento"/>
						</xsl:element>
					</td>
					<td>
						<xsl:element name="label">
							&#xA0;<xsl:value-of select="@DSEstadoAndamento"/>
						</xsl:element>
					</td>
				</xsl:element>
			</xsl:for-each>
		</table>
		</fieldset>
	</xsl:template>
</xsl:stylesheet>
