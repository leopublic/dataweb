<?php

ini_set('session.bug_compat_warn', 'off');
session_start();
include ('LIB/conecta.php');
include ("LIB/classes/classe_usuario.php");
include ("LIB/geral.php");

$ok = 0;
$login = $_POST['login'];
$senha = $_POST['senha'];

$login = cBANCO::SqlOk($login);
$senha = cBANCO::SqlOk($senha);
$lstUsu = buscaUsuarios("", "", "", $login, "", "");

# ------------------------------ Sessao Direto -------------------------
if (count($lstUsu) > 0) {
    $obj = $lstUsu[0];
    $nmsenha = $obj->GETnm_senha();

    if (strtoupper($senha) != strtoupper($nmsenha)) {
        $_SESSION['msgErro'] = 'Senha inválida. Entre em contato com o administrador do sistema.';
        $pagina = "/index.php";
    } else {
        // Carrega perfil do usuário
        $cdusu = $obj->GETcd_usuario();
        $objP = $obj->GETcd_perfil();
        $cdemp = $obj->GETcd_empresa();
        $nmlogin = $obj->GETnm_login();
        $nmnome = $obj->GETnm_nome();
        $nmemail = $obj->GETnm_email();
        $cdsup = $obj->GETcd_superior();
        $lstemp = $obj->GETlista_empresas();

        $objU = $lstUsu[0];
        $objP = $objU->GETcd_perfil();
        if (count($objP) > 0) {
            $per['cd_perfil'] = $objP[0]->GETcd_perfil();
            $per['nm_perfil'] = $objP[0]->GETnm_perfil();
            $per['eh_funcionario'] = $objP[0]->GETeh_funcionario();
            $per['eh_armador'] = $objP[0]->GETeh_armador();
            $per['eh_operador'] = $objP[0]->GETeh_operador();
            $per['eh_operrestrito'] = $objP[0]->GETeh_operrestrito();
            $per['eh_cliente'] = $objP[0]->GETeh_cliente();
            $per['cd_nivel'] = $objP[0]->GETcd_nivel();
            $per['eh_financeiro'] = $objP[0]->GETeh_financeiro();
        }
        $myAdmin['cd_usuario'] = $objU->GETcd_usuario();
        $myAdmin['cd_perfil'] = $per;
        $myAdmin['nm_login'] = $objU->GETnm_login();
        $myAdmin['nm_nome'] = $objU->GETnm_nome();
        $myAdmin['cd_empresa'] = $objU->GETcd_empresa();
        $myAdmin['nm_email'] = $objU->GETnm_email();
        $myAdmin['cd_superior'] = $objU->GETcd_superior();
        $myAdmin['cd_superior'] = $objU->GETcd_superior();
        $myAdmin['nm_empresa'] = pegaNomeEmpresa($objU->GETcd_empresa());

        // Pega a empresa prestadora do cliente
        $sql = "SELECT nu_empresa_prestadora from empresa where nu_empresa=".$cdemp;
        $rs = mysql_query($sql);
        if (mysql_errno() > 0) {
            $myAdmin['nu_empresa_prestadora'] = 1;            
        } else {
            if ($rw = mysql_fetch_array($rs)) {
                $myAdmin['nu_empresa_prestadora'] = $rw[0];
            } else {
                $myAdmin['nu_empresa_prestadora'] = 1;
            }
        }


        $Acesso = new Acesso();
        $Acesso->GetAcesso($objU->GETcd_usuario());

        $_SESSION['usuario'] = $myAdmin;
        $_SESSION['myAdmin'] = $myAdmin;
        $_SESSION['listaEmpresas'] = $adm_empresas;
        $_SESSION['cd_perfil_admin'] = $per['cd_perfil'];

        if (count($objP) > 0) {
            $cdper = $objP[0]->GETcd_perfil();
            $nmper = $objP[0]->GETnm_perfil();
            $ehfunc = $objP[0]->GETeh_funcionario();
            $eharma = $objP[0]->GETeh_armador();
            $ehoper = $objP[0]->GETeh_operador();
            $ehrest = $objP[0]->GETeh_operrestrito();
            $ehcli = $objP[0]->GETeh_cliente();
            $cdniv = $objP[0]->GETcd_nivel();
            $ehfin = $objP[0]->GETeh_financeiro();
        }

        if ($adm_empresas == 21) {
            $ehpetro = "S";
        } else {
            $ehpetro = "N";
        }

        if ($cdniv > 7) {
            $ehAdmin = "S";
        } else {
            $ehAdmin = "N";
        }

        $pagina = "/intranet/geral/principal.php";

        if ($petro == "S") {
            $cdemp = "21";
        }

        if ($cdper == "10") {
            cHTTP::setLang('en_us');
            $pagina = "/paginas/carregueCliente.php?controller=cCTRL_CANDIDATO&metodo=ListarCandidatos_cliente";
        } elseif ($ehoper == "S") {
            $pretsons = "N";
            if ($cdper == 10) {  // Wilson Sons
                $cdemp = "21";
                $pretsons = "S";
                $pagina = "/operador/operacao1.php";
            }
        }
        #  session_start();
        $_SESSION['usuarioLogado'] = $cdusu;
        $_SESSION['usuarioPerfil'] = $cdper;
        $_SESSION['usuarioPetro'] = $petro;
        $_SESSION['usuarioArmad'] = $eharma;
        $_SESSION['usuarioMundi'] = $ehfunc;
        $_SESSION['usuarioEmpresa'] = $cdemp;
        $_SESSION['usuarioOper'] = $ehoper;
        $_SESSION['usuarioRest'] = $ehrest;
        $_SESSION['usuarioClient'] = $ehcli;
        $_SESSION['usuarioFinan'] = $ehfin;
        $_SESSION['listaEmpresas'] = $adm_empresas;
        $_SESSION['nu_empresa_prestadora'] = $myAdmin['nu_empresa_prestadora'];
        $_SESSION['ehAdmin'] = $ehAdmin;
    }
} else {
    $sql = "select NU_CANDIDATO, NO_SENHA from CANDIDATO where NO_LOGIN = '" . $login . "'";
    $res = conectaQuery($sql, "login.php");
    if ($rw = mysql_fetch_array($res)) {
        if ($senha == $rw['NO_SENHA']) {
            $myAdmin['cd_usuario'] = $rw['NU_CANDIDATO'];
            $myAdmin['cd_perfil'] = '';
            $myAdmin['nm_login'] = '';
            $myAdmin['nm_nome'] = '';
            $myAdmin['cd_empresa'] = '';
            $myAdmin['nm_email'] = '';
            $myAdmin['cd_superior'] = '';
            $myAdmin['cd_superior'] = '';
            $myAdmin['nm_empresa'] = '';
            $cdusu = $rw['NU_CANDIDATO'];
            $_SESSION['usuarioLogado'] = $cdusu;
            $ok = 1;
            $pagina = '/paginas/candidato_ext_datareq.php';
            $_SESSION['usuario'] = $myAdmin;
        } else {
            $_SESSION['msgErro'] = 'Senha inválida. Entre em contato com o administrador do sistema.';
            $pagina = "/index.php";
        }
    } else {
        $_SESSION['msgErro'] = 'Login inválido. Entre em contato com o administrador do sistema.';
        $pagina = "/index.php";
    }
}

session_write_close();
//var_dump($_SESSION);
header("Location:" . $pagina);
