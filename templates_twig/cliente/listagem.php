{% extends "cliente/base.html" %}
{% block conteudo %}
<form name="{{ template.nome }}" id="{{ template.nome }}" method="post" enctype="multipart/form-data" accept-charset="ISO-8859-1">
{% for campo in template.controles %}
    <input type="hidden" id="CMP_{{ campo.mCampoBD }}" name="CMP_{{ campo.mCampoBD }}" value="{{ campo.mValor }}"/>
{% endfor %}
<div id="content" class="grid">
	<div class="row">
		<div id="main" class="slot-0-1-2-3-4-5">
			<div id="top_main" class="row">
				<div id="title_main" class="candidate">
					<h2><span class="title">List Candidates</span></h2>
				</div>
				<!--div class="main_options">
				</div-->
			</div>
			<div id="content_main" class="search">
                <form name="cTEMPLATE_CANDIDATO_LISTAR_CLIENTE" id="cTEMPLATE_CANDIDATO_LISTAR_CLIENTE" method="post">
            	   <div class="filtros">
                        <ul>
                            <li>
                                <p class="label">Vessel/project name:</p>
                                <p class="content" id="tdEmbarcacaoProjeto">
                                    <select id="CMP_FILTRO_NU_EMBARCACAO_PROJETO" name="CMP_FILTRO_NU_EMBARCACAO_PROJETO">
                                        <option value="">(todos)</option>
                                        <option value="2"> Sem EmbarcaÃ§Ã£o</option>
                                        <option value="5" selected="">Base</option>
                                        <option value="7">Carolina</option>
                                        <option value="8">Catarina</option>
                                        <option value="4">Dynamic Producer</option>
                                        <option value="6">Inativos</option>
                                        <option value="1">SSV Louisiana</option>
                                        <option value="3">SSV Victoria</option>
                                    </select>
                                </p>
                            </li>
                            <li>
                                <p class="label">Name:</p>
                                <p class="content">
                                    <input type="text" id="CMP_FILTRO_NOME_COMPLETO" name="CMP_FILTRO_NOME_COMPLETO" value="" class="cmpTxt cmp"></p>
							</li>
                        </ul>
                        <div class="buttons_group">
                                <input type="button" class="clear" value="Clear form" title="Clear form">
                                <input type="button" class="search" value="Search" title="Search candidates"> 
						</div>
                    </div>
                    <div class="results">
                    	<h3>Results</h3>
                        <table class="footable">
                            <colgroup>
                                <col width="auto">
                                <col width="auto">
                                <col width="100px">
                                <col width="95px">
                                <col width="95px">
                                <col width="auto">
                                <col width="75px">
                                <col width="75px">
                                <col width="52px">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th data-class="expand">Candidate Name</th>
                                    <th>Nationality</th>
                                    <th data-hide="phone,smart,tablet" data-type="numeric">CPF</th>
                                    <th data-hide="phone,smart" data-type="numeric">Passport #</th>
                                    <th data-hide="phone,smart" data-type="numeric">Passport expiry date</th>
                                    <th data-hide="phone">Visa type</th>
                                    <th data-hide="phone,smart,tablet" data-type="numeric">Process granted</th>
                                    <th data-type="numeric">Period of stay</th>
                                    <th data-hide="phone,smart" data-sort-ignore="true">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Henrik Bioern-Lorenzen</td>
                                    <td>Danish</td>
                                    <td>061.566.757-01</td>
                                    <td>204292482</td>
                                    <td>01/04/2020</td>
                                    <td>Aut. Trab. RN72</td>
                                    <td>23/05/2012</td>
                                    <td>22/06/2014</td>
                                    <td>
                                        <a href="#" title="Personal data"></a>
                                        <a href="#" title="Visa information"></a>
                                        <a href="#" title="Documentation"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>David Lane</td>
                                    <td>Danish</td>
                                    <td>061.566.757-01</td>
                                    <td>204292482</td>
                                    <td>01/04/2020</td>
                                    <td>Aut. Trab. RN72</td>
                                    <td>23/05/2012</td>
                                    <td>22/06/2014</td>
                                    <td>
                                        <a href="#" title="Personal data"></a>
                                        <a href="#" title="Visa information"></a>
                                        <a href="#" title="Documentation"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>David Golding</td>
                                    <td>Danish</td>
                                    <td>061.566.757-01</td>
                                    <td>204292482</td>
                                    <td>01/04/2020</td>
                                    <td>Aut. Trab. RN72</td>
                                    <td>23/05/2012</td>
                                    <td>22/06/2014</td>
                                    <td>
                                        <a href="#" title="Personal data"></a>
                                        <a href="#" title="Visa information"></a>
                                        <a href="#" title="Documentation"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>David Lane</td>
                                    <td>Danish</td>
                                    <td>061.566.757-01</td>
                                    <td>204292482</td>
                                    <td>01/04/2020</td>
                                    <td>Aut. Trab. RN72</td>
                                    <td>23/05/2012</td>
                                    <td>22/06/2014</td>
                                    <td>
                                        <a href="#" title="Personal data"></a>
                                        <a href="#" title="Visa information"></a>
                                        <a href="#" title="Documentation"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>David Golding</td>
                                    <td>Danish</td>
                                    <td>061.566.757-01</td>
                                    <td>204292482</td>
                                    <td>01/04/2020</td>
                                    <td>Aut. Trab. RN72</td>
                                    <td>23/05/2012</td>
                                    <td>22/06/2014</td>
                                    <td>
                                        <a href="#" title="Personal data"></a>
                                        <a href="#" title="Visa information"></a>
                                        <a href="#" title="Documentation"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>David Lane</td>
                                    <td>Danish</td>
                                    <td>061.566.757-01</td>
                                    <td>204292482</td>
                                    <td>01/04/2020</td>
                                    <td>Aut. Trab. RN72</td>
                                    <td>23/05/2012</td>
                                    <td>22/06/2014</td>
                                    <td>
                                        <a href="#" title="Personal data"></a>
                                        <a href="#" title="Visa information"></a>
                                        <a href="#" title="Documentation"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>David Golding</td>
                                    <td>Danish</td>
                                    <td>061.566.757-01</td>
                                    <td>204292482</td>
                                    <td>01/04/2020</td>
                                    <td>Aut. Trab. RN72</td>
                                    <td>23/05/2012</td>
                                    <td>22/06/2014</td>
                                    <td>
                                        <a href="#" title="Personal data"></a>
                                        <a href="#" title="Visa information"></a>
                                        <a href="#" title="Documentation"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>David Lane</td>
                                    <td>Danish</td>
                                    <td>061.566.757-01</td>
                                    <td>204292482</td>
                                    <td>01/04/2020</td>
                                    <td>Aut. Trab. RN72</td>
                                    <td>23/05/2012</td>
                                    <td>22/06/2014</td>
                                    <td>
                                        <a href="#" title="Personal data"></a>
                                        <a href="#" title="Visa information"></a>
                                        <a href="#" title="Documentation"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>David Golding</td>
                                    <td>Danish</td>
                                    <td>061.566.757-01</td>
                                    <td>204292482</td>
                                    <td>01/04/2020</td>
                                    <td>Aut. Trab. RN72</td>
                                    <td>23/05/2012</td>
                                    <td>22/06/2014</td>
                                    <td>
                                        <a href="#" title="Personal data"></a>
                                        <a href="#" title="Visa information"></a>
                                        <a href="#" title="Documentation"></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<!-- close content -->
</div>
</div>
<!-- close content -->
<div id="footer">
<div class="grid">
<div class="row">
<?php include("includes/footer.php"); ?>
</div>
</div>
</div>
<!-- close footer -->
</body>
</html>
