<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$extra = "";
$idEmpresa = 0 + $_POST['idEmpresa'];
if( (strlen($idEmpresa)==0) || ($idEmpresa==0) ) {
  $idEmpresa = 0 + $_GET['idEmpresa'];
}
$empCmb = montaComboEmpresas($idEmpresa,"ORDER BY NO_RAZAO_SOCIAL");
if($idEmpresa>0) {
  $nmEmpresa = pegaNomeEmpresa($idEmpresa);
  if($usuarmad == "S") {
    $extraproj = " AND NU_ARMADOR=$usulogado";
  } 
  $cmbProjetos = montaComboProjetos($idEmbarcacaoProjeto,$idEmpresa,$extraproj." ORDER BY NO_EMBARCACAO_PROJETO");
#  $extra = $extra."AND cd_superior=$usulogado ";
  $extra = $extra."ORDER BY nome";
  $cmbTecnicos = montaComboUsuarioTecnico("",$extra);
}
echo Topo("");
echo Menu("SOL");

?>
<div class="conteudo">
	<div class="titulo"><div style="float:left">Nova Requisi&ccedil;&atilde;o de Visto</div>&nbsp;</div>
	<div class="conteudoInterno">				

<table border=0 width="600" class="textoazulPeq">

<?php   

if( (strlen($idEmpresa)==0) || ($idEmpresa==0) ) {  

  if($usuperfil>4) {

    print "<tr><td colspan=2 align=center>Para cadastrar uma solicita&ccedil;&atilde;o, o usu&aacute;rio deve ser um coordenador.</td></tr>";

  } else {

?>

<form name="sol" method=post action="novaReqVisto1.php">
<tr><td><b>Empresa:</td>
<td><select name=idEmpresa style="<?=$estilo?>"><option value=''>Selecione ... <?=$empCmb?></select></td></tr>
<tr>
 <td colspan=2 align=center>
   <input type='submit' class='textformtopo' style="<?=$estilo?>" value='Continuar'>
 </td>
</tr>

<?php 

  }

} else {  

?>

<form name="sol" method=post action="novaReqVisto2.php" enctype="multipart/form-data">
	<input type="hidden" name="MAX_FILE_SIZE" value="8000000" />
	<input type=hidden name=nmall value="">
	<input type=hidden name=cdall value="">

	<tr><td colspan=2>
    * Preencha o formul&aacute;rio abaixo, cadastrando TODOS os candidatos da solicita&ccedil;&atilde;o,
      <br>&#160;&#160;escolha a embarca&ccedil;&atilde;o e o t&eacute;cnico que ir&aacute; terminar o cadastro.
      <br>&#160;&#160;N&atilde;o esque&ccedil;a de clicar em "Finalizar" para proceder com o cadastro da solicita&ccedil;&atilde;o.
    	</td></tr>
	<tr><td colspan=2>&#160;</td></tr>
	<tr><td><b>Empresa:</td>
   		<td><input type=hidden name="idEmpresa" value="<?=$idEmpresa?>"><?=$nmEmpresa?></td></tr>
	<tr><td><b>Solicitante:</td>
   		<td><input type=text name="NO_SOLICITADOR" size=50 style="<?=$estilo?>"></td></tr>
	<tr><td><b>Data da Solicita&ccedil&atilde;o:</td>
   		<td><input type=text name="DT_SOLICITACAO" size=10 maxlength=10 style="<?=$estilo?>" onkeyup="criaMascara(this, '##/##/####');"></td></tr>
	<tr><td><b>Arquivo:</td>
   		<td><input type="file" name="NM_ARQUIVO_SOL" size=30  style="<?=$estilo?>"></td></tr>
	<tr><td><b>Observa&ccedil;&atilde;o:</td>
   		<td><textarea name="DE_OBSERVACAO" cols=50 rows=5 style="<?=$estilo?>"></textarea></td></tr>
	<tr><td><b>Candidatos:</td>
   		<td><span id=lista>Deve ser incluido pelo menos um candidato</span></td></tr>
	<tr><td colspan=2>&#160;</td></tr>

	<tr><td><b>Nome do Candidato:</td>
   		<td><input type=text name="nomeCandidato" size=30 style="<?=$estilo?>">
        	  <input type='button' style="<?=$estilo?>" value='Novo Candidato' onclick='javascript:novo();'>
	   </td>
 	</tr>

 	<tr>
  		<td nowrap><b>Projeto/Embarca&ccedil;&atilde;o:</td>
  		<td>
			<select name="idEmbarcacaoProjeto" style="<?=$estilo?>">
				<option value="">Selecione...</option>
				<?=$cmbProjetos?>
			</select>
	</tr>

	<tr>
		<td nowrap><b>T&eacute;cnico Respons&aacute;vel:</td>
		<td>
			<select name="cdTecnico" style="<?=$estilo?>">
				<option value="">Selecione...</option>
				<?=$cmbTecnicos?>
			</select>
	</tr>

	<tr><td colspan=2>&#160;</td></tr>
	<tr>
		<td colspan=2 align=center>
			<input type='button' class='textformtopo' style="<?=$estilo?>" value='Finalizar' onclick='javascript:envia();'>
		</td>
	</tr>

<?php } ?>
</table>
</form>

<?php
  echo Rodape("");
?>

<script language="javascript">

var todos = "";

function envia() {
  if(document.sol.idEmpresa.selectedIndex<1) {
    alert("Escolha a empresa.");
  } else if(document.sol.NO_SOLICITADOR.value=='') {
    alert("Entre com nome do solicitante.");
  } else if(document.sol.DT_SOLICITACAO.value=='') {
    alert("Entre com a data da solicitacao.");
  } else if(document.sol.idEmbarcacaoProjeto[document.sol.idEmbarcacaoProjeto.selectedIndex].value=='') {
    alert("Escolha o projeto/embarcacao.");
  } else if(document.sol.cdTecnico[document.sol.cdTecnico.selectedIndex].value=='') {
    alert("Escolha o tecnico responsavel pelo complemento do cadastro.");
  } else if(document.sol.cdall.value=='') {
    alert("Não há candidato para cadastrar.");
  } else {
    document.sol.submit();
  }
}

function novo() {
  var emp = "<?=$idEmpresa?>";
  var nome = document.sol.nomeCandidato.value;
  var nmall = document.sol.nmall.value;
  var cdall = document.sol.cdall.value;
  if(nome=="") {
    alert("Entre com um nome de pesquisa.");
  } else if(emp=="") {
    alert("Escolha a empresa.");
  } else {   
    var MyArgs = new Array(emp,nome);
    var pagina = "novaReqVistoCand1.php?idEmpresa="+emp+"&nomeCandidato="+nome;
    var WinSettings = "center:yes;resizable:yes;dialogHeight:550px;dialogWidth=780px"
    var MyArgsRet = window.showModalDialog(pagina, MyArgs, WinSettings);
    if (MyArgsRet != null) {
      var cand = MyArgsRet[0].toString();
      var ret = MyArgsRet[1].toString();
      if(cand.length > 0 && ret.length > 0) { 
        cdall = cdall + cand + "#";
        nmall = nmall + ret + "#";
        var msg = MyArgsRet[2].toString();
        todos = todos + "<input type=hidden name=chkcad value="+cand+">"+ret+"<br>";
        document.all.lista.innerHTML = todos;
        document.sol.nmall.value = nmall;
        document.sol.cdall.value = cdall;
      }
    }
  }
}

</script>
<?php
include("../javascript/criamascara_js.php");
?>


