<?php
$opcao = "EMP";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

$target = "target = 'NOVO'";
echo "<script language='javascript'>window.name='NOVO';</script>";
echo IniPag();

$acao = $_POST['acao'];
if(strlen($acao)==0) {
  $acao = trim($_GET['acao']);
}
$idEmpresa = $_POST['idEmpresa'];
if(strlen($idEmpresa)==0) {
  $idEmpresa = trim($_GET['idEmpresa']);
}
$idCandidato = $_POST['idCandidato'];
if(strlen($idCandidato)==0) {
  $idCandidato = trim($_GET['idCandidato']);
}
$nome = $_POST['nome'];
if(strlen($nome)==0) {
  $nome = trim($_GET['nome']);
}
$primeiroNome = $_POST['primeiroNome'];
if(strlen($primeiroNome)==0) {
  $primeiroNome = trim($_GET['primeiroNome']);
}
$nomeMeio = $_POST['nomeMeio'];
if(strlen($nomeMeio)==0) {
  $nomeMeio = trim($_GET['nomeMeio']);
}
$ultimoNome = $_POST['ultimoNome'];
if(strlen($ultimoNome)==0) {
  $ultimoNome = trim($_GET['ultimoNome']);
}

if($acao=="") {
  $ret = "Ação não é válida.";
}
if( ($acao=="S" || $acao=="A") && $idCandidato=="" ) {
   $ret = "Não foi informado o candidato.";
}

echo "<br><center><table border=0 width=750 class='textoazulPeq'>\n";
echo " <tr><td class='textobasico' align=center valign=top>\n";
echo "   <p align=center class='textoazul'><strong>:: Cadastro de Candidato ::</strong></p>\n";
echo " </td></tr>\n";
echo " <tr><td>&#160;</td></tr>\n";

if(strlen($ret)>0) {
   echo "<tr><td><font color=red><b>Erro:</b> $ret</td></tr></table>";
   exit;
}

$sql = "delete from AUTORIZACAO_CANDIDATO where NU_CANDIDATO=$idCandidato and NU_EMPRESA=$idEmpresa and NU_SOLICITACAO=-1";
mysql_query($sql);

if(strlen(trim($nome))==0) {
  $nome = $primeiroNome." ".$nomeMeio." ".$ultimoNome;
}
$nomeEmp = pegaNomeEmpresa($idEmpresa);

$ret = "";
if($acao=="S") {
  # Nova solicitacao do mesmo candidato -> Criar nova solicitação com (-1) em NU_SOLICITACAO
   $sql = "insert into AUTORIZACAO_CANDIDATO (NU_EMPRESA,NU_CANDIDATO,NU_SOLICITACAO) VALUES ($idEmpresa,$idCandidato,-1)";
   mysql_query($sql);
   if(mysql_errno()!=0) {
     $ret = mysql_error();
   }
   $texto = "Nova solicitação para o candidato $nome no cadastro da empresa $nomeEmp. $ret";
} else if($acao=="A") {
  # Ultima solicitacao do mesmo candidato -> Trocar solicitação para (-1) em NU_SOLICITACAO
   $soli = pegaUltimaSolicitacao($idCandidato);
   $sql = "update AUTORIZACAO_CANDIDATO set NU_EMPRESA=$idEmpresa,NU_SOLICITACAO=-1 where NU_CANDIDATO=$idCandidato and NU_SOLICITACAO=$soli";
   mysql_query($sql);
   if(mysql_errno()!=0) {
     $ret = mysql_error();
   }
   $texto = "Alteração da última solicitação para o candidato $nome no cadastro da empresa $nomeEmp. $ret";
} else if($acao=="I") {
  if($nomeMeio==".") { $nomeMeio=""; }
  if($ultimoNome==".") { $ultimoNome=""; }
  $nome_completo = $primeiroNome;
  if(strlen($nomeMeio)>0) { $nome_completo = $nome_completo." ".$nomeMeio; }
  if(strlen($ultimoNome)>0) { $nome_completo = $nome_completo." ".$ultimoNome; }

  # Novo candidato - criar entrada com dados minimos com  (-1) em NU_SOLICITACAO
   $sql = "insert into CANDIDATO (NOME_COMPLETO,NO_PRIMEIRO_NOME, NO_NOME_MEIO, NO_ULTIMO_NOME,DT_CADASTRAMENTO,CO_USU_ULT_ALTERACAO) ";
   $sql = $sql."values ('".addslashes($nome_completo)."','".addslashes($primeiroNome)."','".addslashes($nomeMeio)."','".addslashes($ultimoNome)."',now(),$usulogado)";
   mysql_query($sql);
   if(mysql_errno()!=0) {
      $ret = mysql_error();
   } else {
      $sql = "SELECT max(nu_candidato) as idCandidato FROM CANDIDATO";
      $rs = mysql_query($sql);
      if(mysql_errno()!=0) {
         $ret = mysql_error();
      } else {
        if($dados = mysql_fetch_array($rs)) {
          $idCandidato = $dados['idCandidato'];
        }  else {
          $ret = "Não encontrei o usuario.";
        }
     }
   }
   if($ret=="") {
      $sql = "insert into AUTORIZACAO_CANDIDATO (NU_EMPRESA,NU_CANDIDATO,NU_SOLICITACAO) VALUES ($idEmpresa,$idCandidato,-1)";
      mysql_query($sql);
      if(mysql_errno()!=0) {
        $ret = mysql_error();
        $sql = "delete from CANDIDATO where =$idCandidato";
        mysql_query($sql);
      }
   }
   $texto = "Inclusão do candidato $nome no cadastro da empresa $nomeEmp. $ret";
} else {
  $ret = "Ação inválida.";
}
if(strlen($ret)>0) {
  echo "<tr><td><font color=red><b>Erro:</b> Não foi possível alterar/cadastrar usuário.<br><br></font>$ret<br><br>Clique no botão <b>Fechar</b> para voltar.</td></tr>";
  echo "<tr><td>SQL=$sql</td></tr>";
} else {
  echo "<tr><td>Operação efetuada, clique no botão <b>Fechar</b para prosseguir.</td></tr>";
}

insereEvento($idEmpresa,$idCandidato,"",$texto);

echo "<tr><td>&#160;</td></tr>";
echo "<tr><td><input type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:Done();'></td></tr>";

echo "</table>\n";
echo Rodape("");

if(strlen($ret)>0) {
  $idCandidato = "";
  $nome = "$ret";
}

?>

<script language='javascript'>
function Done() {
    var cod = "<?=$idCandidato?>";
    var nome = "<?=$nome?>";
    var msg = "<?=$ret?>";
    var MyArgs = new Array(cod,nome,msg);
    window.returnValue = MyArgs;
    window.close();
}
</script>
