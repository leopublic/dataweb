<?php
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/solicitacoes/solicita_visto.php");
include ("../LIB/solicitacoes/finan_geral.php");
include ("../LIB/agenda/agenda_inc.php");
include ("../LIB/classes/classe_proc_cancel.php");
include ("../LIB/classes/classe_proc_emiscie.php");
include ("../LIB/classes/classe_proc_mte.php");
include ("../LIB/classes/classe_proc_prorrog.php");
include ("../LIB/classes/classe_proc_regcie.php");

$idEmpresa = 0 + $_POST['idEmpresa'];
$nomeEmpresa = pegaNomeEmpresa($idEmpresa);
$nmall = $_POST['nmall'];
$cdall = $_POST['cdall'];
$NO_SOLICITADOR = $_POST['NO_SOLICITADOR'];
$DT_SOLICITACAO = $_POST['DT_SOLICITACAO'];
$DE_OBSERVACAO = $_POST['DE_OBSERVACAO'];
$idEmbarcacaoProjeto = 0+$_POST['idEmbarcacaoProjeto'];
$cdTecnico = 0+$_POST['cdTecnico'];

$noEmbarcacao = pegaNomeProjeto($idEmbarcacaoProjeto,$idEmpresa);
$noTecnico = pegaNomeUsuario($cdTecnico);

$msg = novaSolicitacao($idEmpresa,$usulogado,$NO_SOLICITADOR,$DT_SOLICITACAO,$DE_OBSERVACAO,$cdall,$idEmbarcacaoProjeto);
$msg = $msg.InsereAcomp($nu_sol,$idEmpresa,$usulogado,$cdTecnico,$idEmbarcacaoProjeto);
$msg = $msg.InsereAcompAgenda($nu_sol,$cdTecnico,$usulogado);
$msg = $msg.InsereAcompCand($nu_sol,$cdall,$idEmpresa);
if(strlen($msg)==0) {
	$msg = "Solicitação criada com sucesso. ";
}
else
{
	$msg = "Houve um erro com a criação da solicitação.<br>".$msg;
}
$lista = str_replace("#","<br>",$nmall);

echo Topo("");
echo Menu("SOL");

?>
	
<br><center>
<table border=0 width=700 class="textoazulPeq">
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Nova Requisi&ccedil;&atilde;o de Visto ::</strong></p>				
  </td>
 </tr>
</table>
<p class="textoazulPeq"><b><font color=red><?=$msg?></font></b></p>
<table border=0 align="center" width="400" class="textoazulPeq">
<tr><td><b>Solicita&ccedil;&atilde;o:</td>
   <td><?=$nu_sol?></td></tr>
<tr><td><b>LIB:</td>
   <td><?=$nu_lib?></td></tr>
<tr><td><b>Empresa:</td>
   <td><?=$nomeEmpresa?></td></tr>
<tr><td><b>Solicitante:</td>
   <td><?=$NO_SOLICITADOR?></td></tr>
<tr><td><b>Data da Solicita&ccedil&atilde;o:</td>
   <td><?=$DT_SOLICITACAO?></td></tr>
<tr><td><b>Arquivo:</td>
   <td><?=$noArqOriginal?></td></tr>
<tr><td><b>Embarca&ccedil;&atilde;o:</td>
   <td><?=$noEmbarcacao?></td></tr>
<tr><td><b>T&eacute;cnico:</td>
   <td><?=$noTecnico?></td></tr>
<tr><td><b>Observa&ccedil;&atilde;o:</td>
   <td><?=$DE_OBSERVACAO?></td></tr>
<tr><td><b>Candidatos:</td>
   <td><?=$lista?></td></tr>
<tr><td colspan=5>&#160;</td></tr>

<?php

echo Rodape("");

exit;

function InsereAcomp($nu_sol,$idEmpresa,$usulogado,$cdTecnico,$idEmbarcacaoProjeto) {
  $msg = "";
  $sql = "INSERT INTO solicita_visto_acomp (cd_solicitacao,nu_empresa,cd_coordenador,cd_tecnico,nu_projeto,dt_sol) VALUES ($nu_sol,$idEmpresa,$usulogado,$cdTecnico,$idEmbarcacaoProjeto,now() ) ";
  mysql_query($sql);
  if(mysql_errno()!=0) {
    $ret = mysql_error();
    gravaLogErro($sql,$ret,"ACOMPANHA","SOLICITACAO");
    $msg = "Porem nao foi possivel criar acompanhamento da solicitacao. Informe ao administrador a situacao. <!-- $ret -->";
  } else {
    gravaLog($sql,"","ACOMPANHA","SOLICITACAO");
  }
  return $msg;
}

function InsereAcompAgenda($nu_sol,$cdTecnico,$usulogado) {
  $dia = date("Y-m-d");
  $texto = "Solicitação número $nu_sol da empresa $nomeEmpresa. Favor proceder com o cadastro.";
  $ret = InsereAgendaExterna($cdTecnico,$usulogado,$dia,9,"C",$texto);
  return $ret;
}

function InsereAcompCand($nu_sol,$cdall,$idEmpresa) {
  $ret = "";
  gravaLog("Recebi cdall=$cdall ","","ACOMCANDIDATO","SOLICITACAO");
  $lst = split("#",$cdall);
  foreach($lst as $key=>$value) {
    $cand = 0+$value;
    if($cand>0) {
      $sql = "INSERT INTO solicita_visto_acomp_cand (cd_solicitacao,cd_candidato,nu_empresa) VALUES ($nu_sol,$cand,$idEmpresa)";
      mysql_query($sql);
      if(mysql_errno()!=0) {
        $ret = mysql_error();
        gravaLogErro($sql,$ret,"ACOMCANDIDATO","SOLICITACAO");
        $msg = "Porem nao foi possivel criar acompanhamento da solicitacao. Informe ao administrador a situacao. <!-- $ret -->";
      } else {
        gravaLog($sql,"","ACOMCANDIDATO","SOLICITACAO");
        InsereEntradaAndamento($cand,$nu_sol);
      }
    } else {
      gravaLogErro($sql,"Numero do candidato zerado.","ACOMCANDIDATO","SOLICITACAO");
    }
  }
  return $ret;
}

function InsereEntradaAndamento($cand,$sol) {
  $mte = new proc_mte();
  $mte->cd_candidato = $cand;
  $mte->cd_solicitacao = $sol;
  $mte->InsereProcesso();

  $reg = new proc_regcie();
  $reg->cd_candidato = $cand;
  $reg->cd_solicitacao = $sol;
  $reg->InsereProcesso();

  $emi = new proc_emiscie();
  $emi->cd_candidato = $cand;
  $emi->cd_solicitacao = $sol;
  $emi->InsereProcesso();

  $pro = new proc_prorrog();
  $pro->cd_candidato = $cand;
  $pro->cd_solicitacao = $sol;
  $pro->InsereProcesso();

  $can = new proc_cancel();
  $can->cd_candidato = $cand;
  $can->cd_solicitacao = $sol;
  $can->InsereProcesso();
}
?>
