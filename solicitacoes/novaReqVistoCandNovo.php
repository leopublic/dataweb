<?php
$opcao = "EMP";
include ("../LIB/autenticacao.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");

//$target = " target = 'NOVO' ";
//echo "<script language='javascript'>window.name='NOVO';</script>";

$idCandidato = "";
$nmCandidato = "";
$nomeRecOrig = $_POST['nomeCandidato'];
$NU_EMPRESA = $_POST['NU_EMPRESA'];
$NU_SERVICO = $_POST['NU_SERVICO'];

if(strlen($NU_EMPRESA)==0 || $NU_EMPRESA==''){
  $NU_EMPRESA = trim($_GET['NU_EMPRESA']);
}

if(strlen($NU_SERVICO)==0 || $NU_SERVICO==''){
  $NU_SERVICO = trim($_GET['NU_SERVICO']);
}

$nomeRec = trim($_POST['nomeCandidato']);
if(strlen($nomeRec)==0) {
  $nomeRec = trim($_GET['nomeCandidato']);
}

$CMP_NU_EMPRESA = new cCAMPO("NU_EMPRESA", "Empresa", cCAMPO::cpCHAVE_ESTR, "NU_EMPRESA", 0, "" , "", cCOMBO::cmbEMPRESA_ATIVA, "" , false);
if ($_POST['CMP_NU_EMPRESA']) {
	$CMP_NU_EMPRESA->mValor = $_POST['CMP_NU_EMPRESA'];
}



$nmEmpresa=pegaNomeEmpresa($NU_EMPRESA);
if (strlen($nomeRec)>0){
	$nomeRec = str_replace("'", "%", str_replace(" ","%", str_replace('.', '%', trim($nomeRec))));
	$query = " SELECT C.NU_CANDIDATO,C.NO_PRIMEIRO_NOME,C.NO_NOME_MEIO,C.NO_ULTIMO_NOME,C.NO_PAI,C.NO_MAE,C.DT_NASCIMENTO,C.NU_PASSAPORTE";
	$query .= "		, C.DT_CADASTRAMENTO,C.CO_USU_CADASTAMENTO,C.CO_USU_ULT_ALTERACAO,C.DT_CADASTRAMENTO";
	$query .= "		, date(C.DT_ULT_ALTERACAO) as dcad";
	$query .= "		, time(C.DT_ULT_ALTERACAO) as hcad";
	$query .= "		, A.NU_SOLICITACAO, A.DT_EMISSAO_VISTO, A.DT_VALIDADE_VISTO, DT_PROTOCOLO_PRORROGACAO";
	$query .= "     , NO_RAZAO_SOCIAL";
	$query .= "  FROM CANDIDATO C LEFT JOIN AUTORIZACAO_CANDIDATO A ON A.NU_CANDIDATO = C.NU_CANDIDATO";
	$query .= "  LEFT JOIN EMPRESA E ON E.NU_EMPRESA = C.NU_EMPRESA";
	$query .= " WHERE NOME_COMPLETO like '%$nomeRec%'";
	if (intval($CMP_NU_EMPRESA->mValor) > 0) {
		$query .= " AND C.NU_EMPRESA = ".$CMP_NU_EMPRESA->mValor;
	}
	$query .= " ORDER BY NO_PRIMEIRO_NOME,NO_NOME_MEIO,NO_ULTIMO_NOME,NO_PAI,NO_MAE  ";
	
	if ($rs = mysql_query($query)){
		$tot = mysql_num_rows($rs);
	}
}
else{
	$tot = 0;
}
echo IniPag(false);


print '<!-- SQL='.$query.'-->';
?>
<div class="conteudo">
	<div class="titulo"><div style="float:left">Adicionar candidato</div>&nbsp;</div>
	<form method="post" action="novaReqVistoCandNovo.php" target="_self" id="pesquisa">
		<input type="hidden" name="NU_EMPRESA" id="NU_EMPRESA" value="<?=$NU_EMPRESA;?>">
		<input type="hidden" name="NU_SERVICO" id="NU_SERVICO" value="<?=$NU_SERVICO;?>">
		<input type="hidden" name="candidatos" id="candidatos" value="<?=$CANDIDATOS;?>">
		<div style="margin-left:5px;margin-top:5px;">
			Filtro:
			<? print cINTERFACE::RenderizeCombo($CMP_NU_EMPRESA);?>
			<input type="text" name="nomeCandidato" id="nomeCandidato" value="<?=$nomeRecOrig;?>"/>
			<input type="submit" value="Listar"/>
			<input type="button" id="adicionarNovo" value="Cadastrar novo" onclick="$('#novoCand').show();$('#NO_NOME_COMPLETO').val($('#nomeCandidato').val());" style="margin-left:5px;"/>
		</div>
		<div id="novoCand" style="background-color:#dfe4ff;padding:3px;margin:5px;border:solid 1px #3969A5;display:none;">
			<form>
			<center><div style="margin:10px;font-weight:bold;">ATENÇÃO: Certifique-se de que o candidato não está cadastrado antes de incluir um novo</div></center>
			Nome:<input type="text" name="NO_NOME_COMPLETO" id="NO_NOME_COMPLETO" maxlength="130" size="40">
				<input type="button" value="Incluir" onclick="IncluirCandidato();"/>
				<input type="button" value="Fechar" onclick="$('#novoCand').hide();"/>
			</form>
		</div>
		<div id="novoCandAguarde" style="background-color:#dfe4ff;padding:5px;margin:5px;border:solid 1px #3969A5;display:none;">
			<div id="msgCand">(candidato sendo incluído... aguarde)</div>
		</div>
<?php 
if($tot == 0) { ?>
	<script>$('#adicionarMarcados').hide();</script>
<?
	if ($nomeRecOrig==''){
		echo ' <div style="margin-top:10px;font-weight:bold;margin-left:5px;">Informe uma parte do nome dos candidatos que você deseja incluir na OS e clique em "Listar"</div>';
?>
		<script>$('#adicionarNovo').hide();</script>
<?
	}
	else{ ?>
		<script>$('#adicionarNovo').show();</script>
<?
		echo ' <div style="margin-top:10px;font-weight:bold;margin-left:5px;">Não foi encontrado nenhum candidato com nome contendo "'.$nomeRecOrig.'"</div>';
	}
}
else { ?>
<script>$('#adicionarMarcados').show();$('#adicionarNovo').show();</script>

<center>
	<table border="0" width="750" class="grid" style="margin-top:10px;">
		<tr>
			<td align=center>&nbsp;</td>
			<td align=center><b>Candidato</td>
			<td align=center><b>Empresa</td>
			<td align=center><b>Pai</td>
			<td align=center><b>Nascimento</td>
			<td align=center><b>Passaporte</td>
			<td align=center><b>Dt cadastro</td>
		</tr>
  <?
  $idCandidatoAnt = "";
  $qtd = 0;
  while($dados = mysql_fetch_array($rs)) {
     $NO_RAZAO_SOCIAL = $dados['NO_RAZAO_SOCIAL'];
     $idCandidato = $dados['NU_CANDIDATO'];
     $primeiroNome = $dados['NO_PRIMEIRO_NOME'];
     $nomeMeio = $dados['NO_NOME_MEIO'];	
     $ultimoNome = $dados['NO_ULTIMO_NOME'];
     $nmCad = $primeiroNome." ".$nomeMeio." ".$ultimoNome;
     $nomePai = $dados['NO_PAI'];
     $nomeMae = $dados['NO_MAE'];
     $dataNascimento = dataMy2BRRed($dados['DT_NASCIMENTO']);
     $dataValidadeVisto = dataMy2BRRed($dados['DT_VALIDADE_VISTO']);
     $dataEmissaoVisto = dataMy2BRRed($dados['DT_EMISSAO_VISTO']);
     $dataProtocoloProrrogacao = dataMy2BRRed($dados['DT_PROTOCOLO_PRORROGACAO']);
     $numeroPassaporte = $dados['NU_PASSAPORTE'];
     $solicitacao = $dados['NU_SOLICITACAO'];
     $idUsuCad = $dados['CO_USU_CADASTAMENTO'];
     if($idUsuCad>0) {
         $nmUsuCad = pegaNomeUsuario($idUsuCad);
     } else {
         $nmUsuCad = "Sem informacao";
     }
     $dtUsuCad = dataMy2BRRed($dados['DT_CADASTRAMENTO']);
     $idUsuAlt = $dados['CO_USU_ULT_ALTERACAO'];
     if($idUsuAlt>0) {
         $nmUsuAlt = pegaNomeUsuario($idUsuAlt);
     } else {
         $nmUsuAlt = "Sem informacao";
     }
     $dtUsuAlt = dataMy2BR($dados['dcad'])." ".$dados['hcad'];
     if(dataMy2BR($dados['dcad'])=="") { $dtUsuAlt=""; }

#     $query = "SELECT NU_EMBARCACAO_PROJETO FROM AUTORIZACAO_CANDIDATO where NU_CANDIDATO=$idCandidato";
#     $resultado = mysql_query($query);
#     while($dados = mysql_fetch_array($resultado)) {
#       $idEmbarcacaoProjeto = $dados['NU_EMBARCACAO_PROJETO'];
#       $nmEmbarcacaoProjeto = pegaNomeProjeto($idEmbarcacaoProjeto,$NU_EMPRESA);
#     }

     //$href1 = '<a href="javascript:alert(window.opener.ajaxIncluir(\'CANDIDATO\', \''.$idCandidato.'\'));" class="textoazulPeq" $target><b>Incluir</a>';
//     $href1 = '<input type="checkbox" name="CANDIDATO" id="CANDIDATO" value="'.$idCandidato.'" class="checkbox"/>';
     //     $href2 = "<a href='javascript:novasol($idCandidato,\"$nmCad\");' class='textoazulPeq' $target><b>NOVA SOL</a>";
	if ($idCandidatoAnt==$idCandidato){	
     	$classe = "cont";
	}
	else{
		$classe = "nova";
	}

	$os = cORDEMSERVICO_NOVA::osAberta($idCandidato, $NU_EMPRESA, $NU_SERVICO);
	if(is_object($os)){
		$link = '-';
		$obs = '<span style="color:red;font-size:inherit;margin:0;padding:0"><br/>(já existe a <a href="/operador/os_DadosOS.php?&ID_SOLICITA_VISTO='.$os->mID_SOLICITA_VISTO.'&id_solicita_visto='.$os->mID_SOLICITA_VISTO.'" target="_blank" style="color:inherit;margin:inherit; padding: inherit;">OS '.$os->mNU_SOLICITACAO.'</a> desse tipo em andamento para esse candidato)</span>' ;
	} else {
		$obs = "";
		$link = '<a href="#" onClick="adicionaCandidato(\''.$idCandidato.'\' );">Adicionar</a>';
	}
	$nome = $nmCad."(".$idCandidato.")";


    echo "<tr class=\"$classe\" onmouseover=\"this.style.backgroundColor='#dfe4ff';\" onmouseout=\"this.style.backgroundColor='#ffffff';\">";
	if ($idCandidatoAnt!=$idCandidato){
		echo '<td align="center" class="button acao">'.$link.'</td>';
		echo "<td><a href='javascript:mostra($NU_EMPRESA, $idCandidato);' class='textoazulpeq'>$nome</a>$obs</td>\n";
		echo "<td>".$NO_RAZAO_SOCIAL."</td>\n";
    	echo "<td>$nomePai</td>\n";
    	echo "<td align=center>$dataNascimento</td>\n";
    	echo "<td align=center>$numeroPassaporte</td>\n";
    	echo "<td align=center>$dtUsuCad</td>\n";
	}
	echo "</tr>\n";
	$idCandidatoAnt = $idCandidato;
  }
}
?>
	</table><br>
</form>
</center>
</div>
<?
echo Rodape("");
?>



<script language='javascript'>

function adicionaCandidato(nu_candidato)
{
	msg = window.opener.ajaxIncluir('CANDIDATO', nu_candidato);
	alert(msg);
}

function IncluirCandidato()
{
	var NO_NOME_COMPLETO= $('#NO_NOME_COMPLETO').val();
	$('#novoCand').hide();
	$('#novoCandAguarde').show();
	$.ajax({
		  url: '/LIB/ajaxLista.php?chaves='+NO_NOME_COMPLETO+'&tipo=CANDIDATO_INS',
		  success: function(data) {
			var retorno = data.split('|');
			if(retorno[1] != '@@erro'){
				window.opener.ajaxIncluir('CANDIDATO', retorno[1]);
				$('#novoCandAguarde').hide();
				$('#nomeCandidato').val($('#NO_NOME_COMPLETO').val());
				$('#pesquisa').submit();
			}
			alert(retorno[0]);
		  }
		});
}

function envianovo() {
  var nm1 = document.redir.primeiroNome.value;
  var nm2 = document.redir.nomeMeio.value;
  var nm3 = document.redir.ultimoNome.value;
  if(nm1=='') {
    alert("É necessário entrar com o Primeiro Nome do candidato.");
  } else {
    document.redir.idCandidato.value = "";
    document.redir.acao.value = "I";
    document.redir.submit();
  }
}
function alterar(cod,nome) {
  document.redir.idCandidato.value = cod;
  document.redir.nome.value = nome;
  document.redir.acao.value = "A";
  document.redir.submit();
}
function novasol(cod,nome) {
  document.redir.idCandidato.value = cod;
  document.redir.nome.value = nome;
  document.redir.acao.value = "S";
  document.redir.submit();
}
function novocand(nome) {
  document.all.novo.style.display = "block";
  document.redir.primeiroNome.value = nome;
  document.redir.primeiroNome.focus();
}
function mostra(emp, codigo) {
	  var pagina = "../intranet/geral/novoCandidato.php?idCandidato=" + codigo + "&acao=V&NU_EMPRESA=" + emp + "&NU_SOLICITACAO=0";
	  var params = "status=0,toolbar=0,location=0,menubar=0,directories=0,scrollbars=1,resizable=1,width=750,height=550";
	  window.open(pagina,"new1",params);
	}

</script>
