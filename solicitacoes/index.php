<?php
include ("../LIB/conecta.php");
include ("../LIB/cabecalho.php");
include ("../LIB/geral.php");
include ("../LIB/combos.php");
include ("../LIB/solicitacoes/solicitacoes.php");
include ("../LIB/solicitacoes/financeiro.php");

$param=$_GET['param'];
$org=$_GET['org'];  # ":$empresa:$solicitacao:$candidato:$cdfin:$cdpedido:$acao:$usuario:"
$dados=crc32($org);

#print "\n<br>Param=$param";
#print "\n<br>Org=$org";
#print "\n<br>Dados=$dados";

$info = split("_",$org);

$idEmpresa =  $info[1];
$nomeEmpresa = pegaNomeEmpresa($idEmpresa);
$solicitacao = $info[2];
$idCandidato =  $info[3];
$idFinan =  $info[4];
$idPedido =  $info[5];
$acao =  $info[6];
$usulogado =  $info[7];

$objsol = BuscaSolicitacaoVale("","",$idEmpresa,$idCandidato,$solicitacao,$idFinan,"","");

$status = verificaSituacao($objsol);
$descst = descSituacao($status);
$pendencia = proxSituacao($status);
$permite = permissaoUsuario($usulogado,$acao);
$cmbacao = "";
$myobs = "";
if($permite==0) {
  $myobs = $myobs."<li> Usuário não tem permissão para $pendencia<br>";
}
if($status=="S") {
  $cmbacao = "<option value='A'> Autorizar<option value='N'> Não Autorizar";
  if($acao!="S") {  $myobs = $myobs."<li> Solicitação ainda não foi autorizada<br>"; }
} else if($status=="A") {
  $cmbacao = "<option value='P'> Pagar";
  if( ($acao=="S") || ($acao=="A") ) {  $myobs = $myobs."<li> Solicitação já foi autorizada<br>"; }
} else if($status=="P") {
  $cmbacao = "<option value='R'> Enviar Recibo";
  if( ($acao=="S") || ($acao=="A") || ($acao=="P") ) {  $myobs = $myobs."<li> Solicitação já foi paga<br>";  }
} else if($status=="N") {
  $myobs = $myobs."<li> Solicitação foi negada<br>";
} else if($status=="F") {
  $myobs = $myobs."<li> Solicitação já foi finalizada<br>";
}


$target = " target = 'EMAIL' ";
echo "<script language='javascript'>window.name='EMAIL';</script>";

echo Topo("SEMMENU");

?>
	
<br><center>
<table border=0 width=800 class="textoazulPeq">
 <tr>
  <td class="textobasico" align="center" valign="top">
   <p align="center" class="textoazul"><strong>:: Requisição de Pagamento ::</strong></p>				
  </td>
 </tr>
</table>
<br>
<table border=0 align="center" width=800 class="textoazulPeq">
<tr><td width=100><b>Empresa:</td><td colspan=3><?=$nomeEmpresa?></td></tr>

<?php

if( ($param!=$dados) || (strlen($org)==0) || (strlen($param)==0) ) {
#  print "<tr><td colspan=4 align=center><br><br><font color=red><b>Verificação de segurança falhou.</td></tr>\n";
#  print "</table>";
#  exit;
}

print MostraSolicitacao($idEmpresa,$solicitacao,$target);

print "<tr><td>&#160;</td></tr>\n";

if($solicitacao>0)  {
  print "<tr><td valign=top><b>Candidato:</td>\n";
  $lista = BuscaCandidatos($idEmpresa,$idCandidato,$solicitacao); # BuscaCandidatos($empresa,$candidato,$nuSolicitacao)
  $x = 0;
  while($x < count($lista) ) {
     $o = $lista[$x];
     $cd = $o->getNU_CANDIDATO();
     $nm = $o->getNM_CANDIDATO();
     $st = $o->getDT_SITUACAO_SOL();
     if($st=="") { $sta = "Não"; } else { $sta = $st; }
     $x++;
  }
  print "<td>$nm</td><td><b>Finalizado:</td><td>$sta</td></tr>\n";
}

print "<tr><td>&#160;</td></tr>\n";

if($idPedido>0)  {
  print MostraSolicitacoes($idEmpresa,$idCandidato,$solicitacao,"E","","");
  print "<tr><td>&#160;</td></tr>\n";
  print MostraHistoricoSolicitacoes($idEmpresa,$idFinan,$target); 
}

print "<tr><td>&#160;</td></tr>";
print "<tr><td nowrap><b>Situação Atual:</b> $descst</td><td nowrap><b>Pendência:</b> $pendencia</td><td colspan=2><table border=0 class='textoazulPeq'>";
print "<tr><td><b>Obs:</b></td><td>$myobs</td></tr></table>";

print "<tr><td>&#160;</td></tr>\n";

print "<form action='index_fim.php' $target method=post enctype='multipart/form-data'>\n";
print "<input type=hidden name='MAX_FILE_SIZE' value='2000000' />\n";
print "<input type=hidden name='idEmpresa' value='$idEmpresa'>\n";
print "<input type=hidden name='solicitacao' value='$solicitacao'>\n";
print "<input type=hidden name='idCandidato' value='$idCandidato'>\n";
print "<input type=hidden name='idFinan' value='$idFinan'>\n";
print "<input type=hidden name='usulogado' value='$usulogado'>\n";

if(strlen($myobs)==0) {
  print "<tr><td colspan=4><table border=0 class='textoazulPeq' width=100?>\n";
  print "<tr><td><b>Valor:</td><td>".montaCampos("valor",$valor,$valor,"T",$acao,"class='textoazulPeq' onkeyup='javascript:monetario(this);' ")."</td>\n";
  print "  <td><b>Ação:</td><td>".montaCampos("acao",$cmbacao,$cmbacao,"S",$acao,"class='textoazulPeq'")."</td></tr>\n";
  print "<tr><td valign=top><b>Descrição:</td><td colspan=3>".montaCampos("descricao",$descricao,$descricao,"A",$acao,"class='textoazulPeq' cols=80 rows=3 ")."</td></tr>\n";
  print "<tr><td valign=top><b>Arquivo:</td><td colspan=3><input type='file' name='NM_ARQUIVO_SOL' size=30  style=\"<?=$estilo?>\"></td></tr>\n";
  print "</table</td></tr>";
}

print "</form><tr><td>&#160;</td></tr>\n";

print "<tr id=bt1>\n";
print " <td colspan=4 align=center>\n";
if(strlen($myobs)==0) {
  print "   <input type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Continuar' onclick='javascript:enviar();' >\n";
  print "   &#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;\n";
}
print "   <input type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Imprimir' onclick='javascript:imprime();' >\n";
print "   &#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;\n";
print "   <input type='button' class='textformtopo' style=\"<?=$estilo?>\" value='Fechar' onclick='javascript:fecha();'>\n";
print " </td>\n";
print "</tr>\n";
print "</table>\n";

echo Rodape("");

?>

<script language="javascript">

function enviar() {
  alert("Implementar");
}

function fecha() {
    var msg = "";
    var MyArgs = new Array(msg);
    window.returnValue = MyArgs;
    window.close();
}

function imprime() {
   document.all.bt1.style.display='none';
   window.self.print();
   document.all.bt1.style.display='block';
}

</script>

