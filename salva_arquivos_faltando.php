<?php

set_time_limit(0);

$dsn = 'mysql:host=127.0.0.1;dbname=dataweb;default-character-set:latin1';
$db_pdo = new PDO($dsn, 'dataweb', 'gg0rezqn');

$sql = "select *, sv.id_solicita_visto id_solicita_visto_sol, c.nu_candidato nu_candidato_cand"
        . " from arquivos "
        . " left join solicita_visto sv on sv.id_solicita_visto = arquivos.id_solicita_visto"
        . " left join candidato c on c.nu_candidato = arquivos.nu_candidato"
        . " where nu_sequencial <= 154500 order by dt_inclusao desc";
$res = $db_pdo->query($sql);
$total = 0;
$erro = 0;
$ignorados = 0;
$ok = 0;
$raiz = DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'dataweb' . DIRECTORY_SEPARATOR . 'arquivos' . DIRECTORY_SEPARATOR;
while ($rs = $res->fetch(PDO::FETCH_ASSOC)) {
    $total++;
    $dir = '';
    if ($rs['ID_SOLICITA_VISTO'] > 0) {
        $dir = $raiz . 'solicitacoes' . DIRECTORY_SEPARATOR . $rs['NU_EMPRESA'] . DIRECTORY_SEPARATOR . $rs['NO_ARQUIVO'];
    } else {
        if ($rs['NU_EMPRESA'] == '') {
            $rs["NU_EMPRESA"] = 0;
        }
        $dir = $raiz . "documentos" . DIRECTORY_SEPARATOR . $rs['NU_EMPRESA'] . DIRECTORY_SEPARATOR . $rs['NU_CANDIDATO'] . DIRECTORY_SEPARATOR . $rs['NO_ARQUIVO'];
    }

    if ($rs['nu_candidato_cand'] > 0) {
        if ($dir != '') {
            if (!file_exists($dir)) {
                $sql = "insert into arquivos_faltando select * from arquivos where nu_sequencial = " . $rs['NU_SEQUENCIAL'];
                $db_pdo->exec($sql);
                $erro++;
            } else {
                $ok++;
            }
        }
    } else {
        $ignorados++;
    }
}
print "\n\nTotal de arquivos verificados: " . $total;
print "\nTotal nao encontrados: " . $erro;
