;(function($){
	function create(elem,appendTo) {
		var $elem = elem
		,$append = appendTo
		,$tr = $elem.children('tr')
		
		if ($tr.length > 0) {
			$tr.each(function(){
				var selfTR = this
				,tr = document.createElement('tr')
				,th = document.createElement('th');
				$(th).css({margin:0,padding:0,width:16}).appendTo(selfTR);

				if ('' !== selfTR.className)
					$(tr).addClass(selfTR.className);

				//ths
				$(selfTR.cells).each(function(){
					$(this).css({
						width: $(this).width()
						,backgroundColor: $(this).css('backgroundColor')
					}).clone(true).attr({
						rowSpan: this.rowSpan,
						colSpan: this.colSpan
					}).appendTo(tr);
				});
				$(tr).appendTo($append);
			});
			$elem.css('visibility','hidden');
		}
	};

	$.fn.scrollTable = function(options){
		var options = $.extend({},$.fn.scrollTable.defaults,options);
		return this.each(function(){
			var $self = $(this)
			,selfDOM = this;

			if (!$self.hasClass('hasGrid')) {
				$self.addClass('hasGrid').width('100%');

				var $TABLE = $self.children('table');
				
				if (0 === $TABLE.length) {
					return;
				}
				
				$TABLE.width($self.width());
				
				var $divHeader = $(document.createElement('div')).addClass('divHeader').append('<table><thead></thead></table>').insertBefore(selfDOM)
				,$TABLE_THEAD = $($TABLE[0].tHead)
				,$TABLE_TFOOT = $($TABLE[0].tFoot).css({margin:0,padding:0,overflow:'hidden'})
				,attrTable = {
					cellSpacing: $TABLE.get(0).cellSpacing||'',
					cellPadding: $TABLE.get(0).cellPadding||''
				};
								
				var $cpt = $('caption',$TABLE[0])
				,$TABLE_HEADER_THEAD = $divHeader.children('table').addClass(options.tableClassName).each(function(){
					if ($cpt.length) {
						$(this).append($cpt[0]);
					}
				}).attr(attrTable).width($self.width()).children('thead');

				if ($TABLE_TFOOT.length) {
					var $divFooter = $(document.createElement('div')).addClass('divFooter').append('<table><thead></thead></table>').insertAfter(selfDOM),
					$TABLE_FOOTER_TFOOT = $divFooter.children('table').addClass(options.tableClassName).attr(attrTable).children('thead');
				}
			
				// Create
				create($TABLE_THEAD,$TABLE_HEADER_THEAD);

				if ($TABLE_TFOOT.length) {
					create($TABLE_TFOOT,$TABLE_FOOTER_TFOOT);
				}

				$self.css({
					height: options.height
					,overflowY: 'scroll',overflowX: 'hidden'
				});

				$TABLE.css({marginTop: parseInt($divHeader.height())*(-1)});

				($.isFunction(options.onComplete) && options.onComplete.call(selfDOM,$divHeader[0],$divFooter[0]));
			};
		});
	};
	
	$.fn.scrollTable.defaults = {
		height:200
		,tableClassName:''
		,onComplete:null
	};
})(jQuery);
