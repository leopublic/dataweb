#!/usr/bin/perl

# V. 20090115 23:00

# Ler do cadastro de acompanhamento e consultar os sites externos
# Tipo de acompanhamento: 

require HTTP::Request;
require LWP::UserAgent;
require HTML::TreeBuilder;
use XML::XSLT;
use File::Compare;
use DBI;

$local = "/home/www/";
$logdir = $local."log/";
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$mon++;
if($year < 1000) { $year += 1900; }
if(length($mon)<2) { $mon = "0$mon"; }
if(length($mday)<2) { $mday = "0$mday"; }
if(length($hour)<2) { $hour = "0$hour"; }
if(length($min)<2) { $min = "0$min"; }
if(length($sec)<2) { $sec = "0$sec"; }

$dsn = "dbi:mysql:Mundivisas";
$user = "root";
$password = "MyAdmDb%";
$sqlbase = "SELECT codigo,empresa,candidato,solicitacao,tamanho,processo,arquivo,status,corpo FROM acompanhamento_processo WHERE status<>'F' AND (  data_pesquisa < CURDATE()  OR data_pesquisa is NULL  ) AND processo IS NOT NULL ";

$logarq = $logdir."sistema_$year-$mon-$mday.log";

&GravaArquivo($logarq,"\n----------------- $hour:$min:$sec -------------------");

$dbh = DBI->connect($dsn, $user, $password, { RaiseError => 1, AutoCommit => 0 });

# 1o Tipo -> MTE
# --------------------
$sql = $sqlbase." AND tipo=? AND (data_pesquisa < '$year-$mon-$mday' or data_pesquisa is NULL) ";
$sth1 = $dbh->prepare($sql);
$sth1->execute("MTE");
while ( @row = $sth1->fetchrow_array ) {
  $cod = $row[0];
  $emp = $row[1];
  $cand = $row[2];
  $sol = $row[3];
  $tam = $row[4];
  $proc = $row[5];
  $arq = $row[6];
  $stat = $row[7];
  $corp = $row[8];
  &tipomte($cod,$emp,$cand,$sol,$tam,$proc,$arq,$stat,$corp);
}

$rc = $dbh->disconnect;

&GravaArquivo($logarq,"--------- FIM ------------");

exit;

### ---------- SUB ROTINAS -------------- ###

sub GravaArquivo() {
  my $arq = shift;
  my $texto = shift;
  open(ARQ, ">> $arq");
    print ARQ "$texto";
  close(ARQ);
}

sub AlteraDados() {
  my $codigo = shift;
  my $tamanho = shift;
  my $status = shift;
  my $corpo = shift;
  my $sql = "UPDATE acompanhamento_processo SET status=?,tamanho=?,corpo=?,data_pesquisa=now() WHERE codigo=? ";
  if($status eq "A") {
     $sql = "UPDATE acompanhamento_processo SET status=?,tamanho=?,corpo=?,data_alteracao=now(),data_pesquisa=now() WHERE codigo=? ";
  }
  my $sth = $dbh->prepare($sql);
  $sth->execute($status,$tamanho,$corpo,$codigo);
}

# ------------ Tipo MTE -----------------
sub tipomte() {
  my $cod = shift;
  my $emp = shift;
  my $cand = shift;
  my $sol = shift;
  my $tam = shift;
  my $proc = shift;
  my $arq = shift;
  my $status = shift;
  my $corp = shift;

  my $ERRO = "";
  my $xmlfile = "";
  my $corpo = "";

  &GravaArquivo($logarq,"\nPesquisa: TipoMTE - Empresa: $emp - Candidato: $cand - Solicitacao: $sol - Processo: $proc - ");

  my $req = HTTP::Request->new(GET => "http://www.mte.gov.br/Empregador/TrabEstrang/Pesquisa/ConsultaProcessoXML.asp?Funcao=Consultar&NRProcesso=$proc");
  my $ua = LWP::UserAgent->new;
  $ua->timeout(60);
  my $resp = $ua->request($req);
  if ($resp->is_success) {
    $corpo = $resp->content;
  } else {
     $ERRO = "ERRO: ".$resp->status_line;
  }
  if(length($ERRO)==0) {
    my $newtam = length($corpo);
    my $difftam = $newtam - $tam;
    if( ($newtam>0) && ( ($difftam > 20) || ($difftam < -20) ) ) {
       $status = "A";
    } else {
       $status = "C";
    }
    &AlteraDados($cod,$newtam,$status,$corpo);
    &GravaArquivo($logarq,"ALTERADO - STATUS = $status (tamanho=$difftam)");
  } else {
    &GravaArquivo($logarq,$ERRO);
  }
}
