#!/usr/bin/perl

# V. 20090115 21:00

# Ler do cadastro de Visa Detail, os processos ainda nao cadastrados e carregar a tabela de acompanhamento
# Tipo de acompanhamento: 

use DBI;

$servidor = "192.168.0.3";
$local = "/home/www/";
$logdir = $local."log/";
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$mon=$mon+1;
if($year < 1000) { $year += 1900; }
if(length($mon)<2) { $mon = "0$mon"; }
if(length($mday)<2) { $mday = "0$mday"; }
if(length($hour)<2) { $hour = "0$hour"; }
if(length($min)<2) { $min = "0$min"; }
if(length($sec)<2) { $sec = "0$sec"; }

$dsn = "dbi:mysql:Mundivisas";
$user = "root";
$password = "MyAdmDb%";

$logarq = $logdir."carga_$year-$mon-$mday.log";

$logtxt = "\n----------------- $hour:$min:$sec -------------------\n";

$dbh = DBI->connect($dsn, $user, $password, { RaiseError => 1, AutoCommit => 1 });

# 1o Tipo -> Pesquisa no MTE para deferimento do visto
# --------------------------------------------------------------------------
$sqlini = "select a.NU_EMPRESA,a.NU_CANDIDATO,a.NU_SOLICITACAO,a.NU_PROCESSO_MTE,a.NU_AUTORIZACAO_MTE,a.DT_AUTORIZACAO_MTE ";
$sqlini = $sqlini."from AUTORIZACAO_CANDIDATO a where a.NU_SOLICITACAO = (";
$sqlini = $sqlini."select  max(b.NU_SOLICITACAO) from AUTORIZACAO_CANDIDATO b ";
$sqlini = $sqlini."where b.NU_SOLICITACAO=a.NU_SOLICITACAO and b.NU_CANDIDATO=a.NU_CANDIDATO and b.NU_EMPRESA=a.NU_EMPRESA)";
$tipo = "MTE";
$sth = $dbh->prepare($sqlini." and a.NU_PROCESSO_MTE>0 ");
$sth->execute();
while ( @row = $sth->fetchrow_array ) {
  $codigo = 0;
  $status = "";
  $emp = $row[0];
  $cand = $row[1];
  $sol = $row[2];
  $proc = $row[3];
  $nuauto = $row[4];
  $dtauto = $row[5];
  $dt_pesq = "";
  &VerificaExiste($emp,$cand,$sol,$tipo);
  if($codigo>0) {
    $logtxt = $logtxt."\nEXISTE (Emp=$emp,Cand=$cand,Sol=$sol,Tipo=$tipo) ";
    if( ( (length($nuauto)>0) || ( (length($dtauto)==10) && (substr($dtauto,0,2) ne "00") ) ) && ($status ne "F") ) {
      $logtxt = $logtxt."- Finalizando pois Status=$status e Numero=$nuauto com Data=$dtauto ";
      if(length($dt_pesq)>0) {
        &AlteraAcomp($codigo,"F");
        $logtxt = $logtxt."- OK";
      } else {
        $logtxt = $logtxt."- NOT = nao efetuada 1a pesquisa";
      }
    }
  } else {
    $logtxt = $logtxt."\nCRIANDO Pesquisa (Emp=$emp,Cand=$cand,Sol=$sol,Processo=$proc,Tipo=$tipo) ";
    &CriaAcomp($emp,$cand,$sol,$proc,$tipo);
    $logtxt = $logtxt."- OK";
  }
}

$rc = $dbh->disconnect;

&GravaArquivo($logarq,$logtxt);

exit;

sub VerificaExiste() {
  my $emp = shift;
  my $cand = shift;
  my $sol = shift;
  my $tipo = shift;
  my $sth = $dbh->prepare("SELECT codigo,status,data_pesquisa FROM acompanhamento_processo WHERE empresa=? AND candidato=? AND solicitacao=? AND tipo=?");
  $sth->execute($emp,$cand,$sol,$tipo);
  if ( @row = $sth->fetchrow_array ) {
    $codigo = $row[0];
    $status = $row[1];
    $dt_pesq = $row[2];
  }
}

sub CriaAcomp() {
  my $emp = shift;
  my $cand = shift;
  my $sol = shift;
  my $proc = shift;
  my $tipo = shift;
  $proc =~ s/-//g;
  $proc =~ s/\.//g;
  $proc =~ s/\///g;
  my $sth = $dbh->prepare("INSERT INTO acompanhamento_processo (empresa,candidato,solicitacao,processo,tipo) VALUES (?,?,?,?,?) ");
  $sth->execute($emp,$cand,$sol,$proc,$tipo);
}

sub AlteraAcomp() {
  my $codigo = shift;
  my $status = shift;
  my $sth = $dbh->prepare("UPDATE acompanhamento_processo SET status=? WHERE codigo=?");
  $sth->execute($status,$codigo);
}

sub GravaArquivo() {
  my $arq = shift;
  my $texto = shift;
  open(ARQ, ">> $arq");
    print ARQ "$texto";
  close(ARQ);
}
