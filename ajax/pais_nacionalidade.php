<?
require_once realpath ("../LIB/autenticacao.php");
//require_once realpath("../LIB/conecta.php");
$app = new Generic;
$app->ajax = true;


function GeraLinha($plinha){
	$linha = "";
	$linha = '<tr rel="'.$plinha['CO_PAIS'].'">
               				<td>'.$plinha['NO_PAIS'].'</td>
                            <td>'.$plinha['NO_NACIONALIDADE'].'</td>
                            <td>'.$plinha['NO_PAIS_EM_INGLES'].'</td>
                            <td>'.$plinha['NO_NACIONALIDADE_EM_INGLES'].'</td>
                            <td>'.$plinha['CO_PAIS_PF'].'</td>
                         </tr>';
	return $linha;
}



// LISTAR NA GRID
if($app->g('action')== 'list'){
	$strSql = '
              SELECT CO_PAIS, NO_PAIS, NO_PAIS_EM_INGLES, NO_NACIONALIDADE, NO_NACIONALIDADE_EM_INGLES, CO_PAIS_PF
                FROM PAIS_NACIONALIDADE
               ORDER BY NO_PAIS ';

	$r = sql::execute($strSql); $rows = '';

	while($row = $r->fetch()){
		$rows .= GeraLinha($row);
	}

	echo $rows;
}

//EXIBIR DETALHE PARA EDIÇÃO
if($app->g('action')== 'detailedit'){

	$row = sql::query("SELECT * FROM PAIS_NACIONALIDADE WHERE CO_PAIS = '".$app->p('id')."' ")->fetch();

	echo '
            <h4>Editar</h4><br />
            <form id="frm_detailedit"><input type="hidden" name="CO_PAIS" value="'.$row['CO_PAIS'].'" />
            <table class="edicao">
                <tr>
                    <th style="width:200px;"> Nome: </th>
                    <td> <input type="text" name="NO_PAIS" value="'.$row['NO_PAIS'].'" style="width:400px"/></td>
                </tr>
                <tr>
                    <th> Nacionalidade: </th>
                    <td> <input type="text" name="NO_NACIONALIDADE" value="'.$row['NO_NACIONALIDADE'].'" style="width:400px"/></td>
                </tr>
                <tr>
                    <th> Nome (em inglês): </th>
                    <td> <input type="text" name="NO_PAIS_EM_INGLES" value="'.$row['NO_PAIS_EM_INGLES'].'" style="width:400px"/></td>
                </tr>
                <tr>
                    <th> Nacionalidade (em inglês): </th>
                    <td> <input type="text" name="NO_NACIONALIDADE_EM_INGLES" value="'.$row['NO_NACIONALIDADE_EM_INGLES'].'" style="width:400px"/></td>
                </tr>
                <tr>
                    <th> Código da PF: </th>
                    <td> <input type="text" name="CO_PAIS_PF" value="'.$row['CO_PAIS_PF'].'" style="width:100px"/></td>
                </tr>';
	if(Acesso::permitido(Acesso::tp_Sistema_Tabelas_Pais_Editar)){
		echo '<tr>
                    <td colspan="2" align="center" style="border-bottom:none;padding-top:10px">
                    <button id="btn_save_cli"><img src="/imagens/save.png" /> Salvar</button>  </td>
                </tr>';
	}
	else{
		echo '<tr>
				<td colspan="2" align="center" style="border-bottom:none;padding-top:10px;font-weight:bold;color:red;">
					Seu perfil não permite salvar as alterações.</td></tr>';
	}

	echo '  </table>
            </form>
            ';


}

//MODIFICAR DADOS DO DETAIL EDIT
if($app->g('action')== 'edit'){
	if( ($app->p('NO_PAIS')) AND ($app->p('NO_PAIS_EM_INGLES')) ){

		$servico = sql :: query("
                                UPDATE PAIS_NACIONALIDADE SET
                                       NO_PAIS						= '".$app->p('NO_PAIS')."'
                                     , NO_NACIONALIDADE				= '".$app->p('NO_NACIONALIDADE')."'
                                     , NO_PAIS_EM_INGLES			= '".$app->p('NO_PAIS_EM_INGLES')."'
                                     , NO_NACIONALIDADE_EM_INGLES	= '".$app->p('NO_NACIONALIDADE_EM_INGLES')."'
                                     , CO_PAIS_PF					= '".$app->p('CO_PAIS_PF')."'
                                 WHERE CO_PAIS = ".$app->p('CO_PAIS')."
                            ");

		if($servico->saved()){
			echo 'Y';
		}else{
			echo 'N|Não foi possível salvar devido um erro inesperado';
		}

	}else{
		echo 'N| O nome do serviço deve ser devidamente preenchido.';
	}
}


//REFRESH ROW
if($app->g('action')== 'refreshrow'){
	if($app->p('id')){

		$row = sql::query("SELECT * FROM PAIS_NACIONALIDADE WHERE CO_PAIS = ".$app->p('id'))->fetch();

		echo GeraLinha($row);
	}else{
		echo 'N';
	}
}


//DELETAR
if($app->g('action')== 'remove'){
	if( ($app->p('id'))){
		//sql::query("DELETE FROM CLIENTE WHERE CODCLI = '".$app->p('id')."' AND CODUSER = '".$app->user['CODUSER']."' ");
		echo 'N';
	}else{
		echo 'N';
	}
}

//ADDDETAIL
if($app->g('action')== 'detailadd'){

	echo '
            <h4>Adicionar novo país/nacionalidade</h4><br />
            <form id="frm_detailedit"><input type="hidden" name="CO_PAIS" value="" />
            <table class="edicao">
                <tr>
                    <th> Nome: </th>
                    <td> <input type="text" name="NO_PAIS" value="" style="width:400px"/></td>
                </tr>
                <tr>
                    <th> Nacionalidade: </th>
                    <td> <input type="text" name="NO_NACIONALIDADE" value="" style="width:400px"/></td>
                </tr>
                <tr>
                    <th> Nome (em inglês): </th>
                    <td> <input type="text" name="NO_PAIS_EM_INGLES" value="" style="width:400px"/></td>
                </tr>
                <tr>
                    <th> Nacionalidade (em inglês): </th>
                    <td> <input type="text" name="NO_NACIONALIDADE_EM_INGLES" value="" style="width:400px"/></td>
                </tr>
                <tr>
                    <th> Código da PF: </th>
                    <td> <input type="text" name="CO_PAIS_PF" value="" style="width:100px"/></td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="border-bottom:none;padding-top:10px">
                    <button id="btn_save_cli"><img src="/imagens/save.png" /> Salvar</button>  </td>
                </tr>
            </table>
            </form>
                        ';

}

//ADD
if($app->g('action')== 'add'){

	if( ($app->p('NO_PAIS')) AND ($app->p('NO_PAIS_EM_INGLES')) ){

		$count = sql::query("SELECT NO_PAIS FROM PAIS_NACIONALIDADE WHERE NO_PAIS = '".$app->p('NO_PAIS')."'  ")->count();

		if($count > 0){
			echo 'N| Já existe um país com este nome';
		}else{

			$servico = sql::query(" INSERT INTO PAIS_NACIONALIDADE(NO_PAIS, NO_PAIS_EM_INGLES, NO_NACIONALIDADE, NO_NACIONALIDADE_EM_INGLES, CO_PAIS_PF)
                                                VALUES(
                                                    '".$app->p('NO_PAIS')."'
                                                   ,'".$app->p('NO_PAIS_EM_INGLES')."'
                                                   ,'".$app->p('NO_NACIONALIDADE')."'
                                                   ,'".$app->fk($app->p('NO_NACIONALIDADE_EM_INGLES'))."'
                                                   ,'".$app->fk($app->p('CO_PAIS_PF'))."'
                                                   )");

			if($servico->saved()){

				$row = sql::query("SELECT * FROM PAIS_NACIONALIDADE WHERE CO_PAIS = ".$servico->id())->fetch();

				echo GeraLinha($row);


			}else{
				echo 'N';
			}


		}
	}else{
		echo 'N| O nome do país deve ser devidamente preenchido';
	}
}
