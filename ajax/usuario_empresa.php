<?

        require_once realpath("../LIB/conecta.php");
        $app = new Generic;
       


        // LISTAR NA GRID
        if($app->g('action')== 'list'){
            $strSql =      
            "   SELECT E.NU_EMPRESA, E.NO_RAZAO_SOCIAL
                  FROM usuarios  u
            INNER JOIN usuario_empresa ue
                    ON u.cd_usuario = ue.cd_usuario                
            INNER JOIN EMPRESA E
                    ON ue.cd_empresa = E.NU_EMPRESA
                 WHERE u.cd_usuario = ".$app->g('cd_usuario')."       
            ";
            
            $sql = new sql; 
            $r = $sql->execute($strSql);
            $rows = '';
            while($row = $r->fetch()){
               $rows .= '<tr rel="'.$row['NU_EMPRESA'].'"><td>'.$row['NO_RAZAO_SOCIAL'].'</td></tr>';
            }
            
            echo $rows;
        }
        
        //EXIBIR DETALHE PARA EDIÇÃO
        if($app->g('action')== 'detailedit'){


              $sql = new sql;
              $r   = $sql->execute("SELECT * FROM EMPRESA WHERE NU_EMPRESA = ".$app->p('id')." ");
              $row = $r->fetch();
              echo '<h4>Editar</h4><br />';
              echo '<form id="frm_detailedit">';
              echo '<input type="hidden" name="NU_EMPRESA_ANTIGA"  value="'.$app->p('id').'" /> ';
              echo 'Empresa: <br /> '.$app->combo_empresa($row['NU_EMPRESA']).'<br /><br />';
              echo '<button id="btn_save_cli"> <img src="/imagens/save.png"> Salvar</button></form>';

    
        }

        //MODIFICAR DADOS DO DETAIL EDIT
        if(($app->g('action')== 'edit') AND ($app->p('NU_EMPRESA_ANTIGA')) AND ($app->p('NU_EMPRESA')) AND ($app->g('cd_usuario'))){

                  // empresa que será selecionada para modificação:  $app->p('NU_EMPRESA_ANTIGA');
                  // empresa nova:  $app->p('NU_EMPRESA')

                  $strsql = "
                   UPDATE usuario_empresa
                      SET cd_empresa  = ".$app->p('NU_EMPRESA')."
                      
                    WHERE cd_usuario  = ".$app->g('cd_usuario')."
                      AND cd_empresa  = ".$app->p('NU_EMPRESA_ANTIGA');

                   $sql = new sql;
                   $up  = $sql->execute($strsql);

                  if($up->saved()){
                     echo 'Y';
                  }else{
                     echo 'N'.$strsql;
                  }

        }
 

         //REFRESH ROW
        if($app->g('action')== 'refreshrow'){
            if( ($app->p('id'))){

               $strSql = " SELECT * FROM EMPRESA E WHERE E.NU_EMPRESA = ".$app->p('id');

               $sql = new sql; 
               $r = $sql->execute($strSql);
               $row = $r->fetch();
           
               echo '<tr rel="'.$row['NU_EMPRESA'].'"><td>'.$row['NO_RAZAO_SOCIAL'].'</td></tr>';
          
            }else{
                
               echo 'N'; 
            }
        }


        //DELETAR
        if($app->g('action')== 'remove'){
            if( ($app->p('id'))){
                
                $sql = new sql;
                $res = $sql->execute("DELETE FROM usuario_empresa WHERE cd_empresa = ".$app->p('id')." AND cd_usuario = ".$app->g('cd_usuario')."  ");

                if($res->saved()){
                  echo 'Y';
                }else{
                  echo 'N';
                }
            }
        }
        
        //ADDDETAIL
        if($app->g('action')== 'detailadd'){

            echo '<h4>Nova Empresa:</h4><br /><br />';
            echo '<form id="frm_detailadd">';
            echo 'Empresa: <br /> '.$app->combo_empresa().'<br /><br />';
           // echo '<br /><br /> <input type="submit" id="btn_add" value="Salvar"></form>';
             echo '<button id="btn_save_cli"> <img src="/imagens/save.png"> Salvar</button></form>';

        }  
        
        //ADD
        if($app->g('action') == 'add'){
            if( ($app->p('NU_EMPRESA')) AND ($app->g('cd_usuario')) ){
               $sql    = new sql;
                
               $existe = $sql->query('select * from usuario_empresa where cd_usuario = '.$app->g('cd_usuario').' AND cd_empresa ='.$app->p('NU_EMPRESA'));
               $existe = $existe->count();  
                
               if($existe == 0){
                
                   $strsql = "INSERT INTO usuario_empresa(cd_usuario, cd_empresa) VALUES(".$app->g('cd_usuario').", ".$app->p('NU_EMPRESA').")";
    
                   
                   $res = $sql->execute($strsql);
    
                   if($res->saved()){
    
                      $strsql = "SELECT * FROM EMPRESA E WHERE E.NU_EMPRESA= ".$app->p('NU_EMPRESA');
                      $res    = $sql->execute($strsql);
                      $res    = $res->fetch();
    
                      echo '<tr rel="'.$res['NU_EMPRESA'].'"><td>'.$res['NO_RAZAO_SOCIAL'].'</td></tr>';
    
                    }else{
                      echo 'N';
                    }
                }else{
                    echo 'N| Esta empresa já foi cadastrada para este usuário! <br />';
                } 
            }else{
                echo 'N| NU_EMPRESA e cd_usuario devem estar preenchidos';
            }
        }
        
              
 ?>       
