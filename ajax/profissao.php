<?
        require_once realpath("../LIB/conecta.php");
        $app  = new Generic;
        $html = new html;


        // LISTAR NA GRID
        if($app->g('action')== 'list'){
            $strSql = '
              SELECT P.CO_PROFISSAO, P.NO_PROFISSAO, P.NO_PROFISSAO_EM_INGLES, P.NU_CBO
                FROM PROFISSAO P
               ORDER BY P.CO_PROFISSAO DESC';

            $r = sql::execute($strSql); $rows = '';

            while($row = $r->fetch()){
               $rows .= '<tr rel="'.$row['CO_PROFISSAO'].'">
                            <td>'.$row['NU_CBO'].'</td>
                            <td>'.$row['NO_PROFISSAO'].'</td>
                            <td>'.$row['NO_PROFISSAO_EM_INGLES'].'</td>
                         </tr>';
            }

            echo $rows;
        }

        //EXIBIR DETALHE PARA EDIÇÃO
        if($app->g('action')== 'detailedit'){


            $row = sql::query("SELECT * FROM PROFISSAO WHERE CO_PROFISSAO = '".$app->p('id')."' ")->fetch();

            echo '
            <h4>Editar</h4><br />
            <form id="frm_detailedit">
            <table class="edicao">
                <tr>
                    <th width="150"> Numero CBO: </th>
                    <td>'.$html->dbedit('NU_CBO', $row).'
                </tr>
                <tr>
                    <th> Nome: </th>
                    <td>'.$html->dbedit('NO_PROFISSAO', $row).'</td>
                </tr>
                <tr><th> Detalhado: </th>
                    <td>'.$html->dbtextarea('NO_PROFISSAO_EM_INGLES', $row).'</td>
                </tr>

                <tr>
                    <td colspan="2" align="center" style="border-bottom:none;padding-top:10px">
                    <button id="btn_save_cli"><img src="/imagens/save.png" /> Salvar</button>  </td>
                </tr>
            </table>
            </form>
            ';


        }

        //MODIFICAR DADOS DO DETAIL EDIT
        if($app->g('action')== 'edit'){
            if( ($app->p('NO_SERVICO_RESUMIDO')) AND ($app->p('NO_SERVICO')) AND ($app->p('NU_SERVICO')) ){

                $servico = sql :: query("
                                UPDATE SERVICO SET
                                       CO_SERVICO             = '".$app->p('CO_SERVICO')."',
                                       NO_SERVICO_RESUMIDO    = '".$app->p('NO_SERVICO_RESUMIDO')."',
                                       NO_SERVICO             = '".$app->p('NO_SERVICO')."',
                                       NU_TIPO_SERVICO        = ".$app->fk($app->p('NU_TIPO_SERVICO')).",
                                       ID_TIPO_ACOMPANHAMENTO = ".$app->fk($app->p('ID_TIPO_ACOMPANHAMENTO'))."
                                WHERE  NU_SERVICO = ".$app->p('NU_SERVICO')."
                            ");

                if($servico->saved()){
                    echo 'Y';
                }else{
                    echo 'N|Não foi possível salvar devido um erro inesperado';
                }

            }else{
                echo 'N| O nome do serviço deve ser devidamente preenchido.';
            }
        }


         //REFRESH ROW
        if($app->g('action')== 'refreshrow'){
            if($app->p('id')){

                $row = sql::query("SELECT * FROM SERVICO WHERE NU_SERVICO = ".$app->p('id'))->fetch();

                echo '<tr rel="'.$row['NU_SERVICO'].'">
                        <td>'.$row['CO_SERVICO'].'</td>
                        <td>'.$row['NO_SERVICO_RESUMIDO'].'</td>
                        <td>'.$row['NO_SERVICO'].'</td>
                      </tr>';
            }else{
                echo 'N';
            }
        }


        //DELETAR
        if($app->g('action')== 'remove'){
            if( ($app->p('id'))){


                //sql::query("DELETE FROM CLIENTE WHERE CODCLI = '".$app->p('id')."' AND CODUSER = '".$app->user['CODUSER']."' ");

                echo 'N';
            }else{
                echo 'N';
            }
        }

        //ADDDETAIL
        if($app->g('action')== 'detailadd'){

            echo '
            <h4>Adicionar novo serviço</h4><br />
            <form id="frm_detailadd">
            <table class="edicao">
                <tr>
                    <th width="150"> Código: </th>
                    <td> <input type="text" name="CO_SERVICO" style="width:80px"/> </td>
                </tr>
                <tr>
                    <th> Nome: </th>
                    <td> <input type="text" name="NO_SERVICO_RESUMIDO" style="width:400px"/></td>
                </tr>
                <tr><th> Detalhado: </th>
                    <td> <textarea name="NO_SERVICO" style="width:400px"></textarea></td>
                </tr>
                <tr>
                    <th> Tipo Serviço:</th>
                    <td> '.$app->combo_tiposervico().'</td>
                </tr>
                <tr>
                    <th> Tipo Acompanhamento:</th>
                    <td> '.$app->combo_tipoacompanhamento().'</td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="border-bottom:none;padding-top:10px">
                    <button id="btn_save_cli"><img src="/imagens/save.png" /> Salvar</button>  </td>
                </tr>
            </table>
            </form>
            ';

        }

        //ADD
        if($app->g('action')== 'add'){

            if( ($app->p('CO_SERVICO')) AND ($app->p('NO_SERVICO_RESUMIDO')) AND ($app->p('NO_SERVICO')) ){

                $count = sql::query("SELECT NO_SERVICO_RESUMIDO FROM SERVICO WHERE NO_SERVICO_RESUMIDO = '".$app->p('NO_SERVICO_RESUMIDO')."'  ")->count();

                if($count > 0){
                    echo 'N| Já existe um serviço com este nome resumido';
                }else{

                    $servico = sql::query(" INSERT INTO SERVICO (CO_SERVICO, NO_SERVICO_RESUMIDO,NO_SERVICO, NU_TIPO_SERVICO, ID_TIPO_ACOMPANHAMENTO)
                                                VALUES(
                                                   ".$app->p('CO_SERVICO').",
                                                   '".$app->p('NO_SERVICO_RESUMIDO')."',
                                                   '".$app->p('NO_SERVICO')."',
                                                   ".$app->fk($app->p('NU_TIPO_SERVICO')).",
                                                   ".$app->fk($app->p('ID_TIPO_ACOMPANHAMENTO'))."
                                                )");

                    if($servico->saved()){

                        $row = sql::query("SELECT * FROM SERVICO WHERE NU_SERVICO = ".$servico->id())->fetch();

                        echo '<tr rel="'.$row['NU_SERVICO'].'">
                                <td>'.$row['CO_SERVICO'].'</td>
                                <td>'.$row['NO_SERVICO_RESUMIDO'].'</td>
                                <td>'.$row['NO_SERVICO'].'</td>
                              </tr>';


                    }else{
                        echo 'N';
                    }


                }
            }else{
                echo 'N| O nome do serviço deve ser devidamente preenchido';
            }
        }


 ?>
