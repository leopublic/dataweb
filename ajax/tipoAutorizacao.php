<?php
        require_once realpath("../LIB/conecta.php");
        $app = new Generic;
        $app->ajax = true;



        // LISTAR NA GRID
        if($app->g('action')== 'list'){
            $strSql = '
              SELECT CO_TIPO_AUTORIZACAO, NO_TIPO_AUTORIZACAO, NO_REDUZIDO_TIPO_AUTORIZACAO
                FROM TIPO_AUTORIZACAO
               ORDER BY NO_TIPO_AUTORIZACAO ';

            $r = sql::execute($strSql); $rows = '';

            while($row = $r->fetch()){
               $rows .= '<tr rel="'.$row['CO_TIPO_AUTORIZACAO'].'">
                            <td>'.$row['CO_TIPO_AUTORIZACAO'].'</td>
               				<td>'.$row['NO_TIPO_AUTORIZACAO'].'</td>
                            <td>'.$row['NO_REDUZIDO_TIPO_AUTORIZACAO'].'</td>
                         </tr>';
            }

            echo $rows;
        }

        //EXIBIR DETALHE PARA EDIÇÃO
        if($app->g('action')== 'detailedit'){

            $row = sql::query("SELECT * FROM TIPO_AUTORIZACAO WHERE CO_TIPO_AUTORIZACAO = '".$app->p('id')."' ")->fetch();

            echo '
            <h4>Editar</h4><br />
            <form id="frm_detailedit"><input type="hidden" name="CO_TIPO_AUTORIZACAO_CHAVE" value="'.$row['CO_TIPO_AUTORIZACAO'].'" />
            <table class="edicao">
                <tr>
                    <th width="150"> Código: </th>
                    <td> <input type="text" name="CO_TIPO_AUTORIZACAO" value="'.$row['CO_TIPO_AUTORIZACAO'].'" style="width:80px"/> </td>
                </tr>
                <tr>
                    <th> Descri&ccedil;&atilde;o resumida: </th>
                    <td> <input type="text" name="NO_REDUZIDO_TIPO_AUTORIZACAO" value="'.$row['NO_REDUZIDO_TIPO_AUTORIZACAO'].'" style="width:400px"/></td>
                </tr>
                <tr><th> Descri&ccedil;&atilde;o: </th>
                    <td> <textarea name="NO_TIPO_AUTORIZACAO" style="width:400px">'.$row['NO_TIPO_AUTORIZACAO'].'"</textarea></td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="border-bottom:none;padding-top:10px">
                    <button id="btn_save_cli"><img src="/imagens/save.png" /> Salvar</button>  </td>
                </tr>
            </table>
            </form>
            ';


        }

        //MODIFICAR DADOS DO DETAIL EDIT
        if($app->g('action')== 'edit'){
            if( ($app->p('NO_REDUZIDO_TIPO_AUTORIZACAO')) AND ($app->p('NO_TIPO_AUTORIZACAO')) ){

                $servico = sql :: query("
                                UPDATE TIPO_AUTORIZACAO SET
                                       CO_TIPO_AUTORIZACAO = '".$app->p('CO_TIPO_AUTORIZACAO')."'
                                	 , NO_REDUZIDO_TIPO_AUTORIZACAO = '".$app->p('NO_REDUZIDO_TIPO_AUTORIZACAO')."'
                                     , NO_TIPO_AUTORIZACAO	  = '".$app->p('NO_TIPO_AUTORIZACAO')."'
                                WHERE  CO_TIPO_AUTORIZACAO = '".$app->p('CO_TIPO_AUTORIZACAO_CHAVE')."'
                            ");

                if($servico->saved()){
                    echo 'Y';
                }else{
                    echo 'N|Não foi possível salvar devido um erro inesperado';
                }

            }else{
                echo 'N| O nome do tipo de autorização deve ser devidamente preenchido.';
            }
        }


         //REFRESH ROW
        if($app->g('action')== 'refreshrow'){
            if($app->p('id')){

                $row = sql::query("SELECT * FROM TIPO_AUTORIZACAO WHERE CO_TIPO_AUTORIZACAO = '".$app->p('id')."'")->fetch();
                echo '<tr rel="'.$row['CO_TIPO_AUTORIZACAO'].'">
                            <td>'.$row['CO_TIPO_AUTORIZACAO'].'</td>
                			<td>'.$row['NO_TIPO_AUTORIZACAO'].'</td>
                            <td>'.$row['NO_REDUZIDO_TIPO_AUTORIZACAO'].'</td>
                         </tr>';
            }else{
                echo 'N';
            }
        }


        //DELETAR
        if($app->g('action')== 'remove'){
            if( ($app->p('id'))){


                //sql::query("DELETE FROM CLIENTE WHERE CODCLI = '".$app->p('id')."' AND CODUSER = '".$app->user['CODUSER']."' ");

                echo 'N';
            }else{
                echo 'N';
            }
        }

        //ADDDETAIL
        if($app->g('action')== 'detailadd'){

            echo '
            <h4>Adicionar novo tipo de autorização</h4><br />
            <form id="frm_detailadd">
            <table class="edicao">
                <tr>
                    <th width="150"> Código: </th>
                    <td> <input type="text" name="CO_TIPO_AUTORIZACAO" style="width:80px"/> </td>
                </tr>
                <tr>
                    <th> Descrição resumida: </th>
                    <td> <input type="text" name="NO_REDUZIDO_TIPO_AUTORIZACAO" style="width:400px"/></td>
                </tr>
                <tr>
                    <th> Descrição: </th>
                    <td> <textarea name="NO_TIPO_AUTORIZACAO" style="width:400px"></textarea></td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="border-bottom:none;padding-top:10px">
                    <button id="btn_save_cli"><img src="/imagens/save.png" /> Salvar</button>  </td>
                </tr>
            </table>
            </form>
            ';

        }

        //ADD
        if($app->g('action')== 'add'){

            if( ($app->p('NO_REDUZIDO_TIPO_AUTORIZACAO')) AND ($app->p('NO_TIPO_AUTORIZACAO')) ){

                $count = sql::query("SELECT CO_TIPO_AUTORIZACAO FROM TIPO_AUTORIZACAO WHERE CO_TIPO_AUTORIZACAO = '".$app->p('CO_TIPO_AUTORIZACAO')."'  ")->count();
                $msg = '';
                if($count > 0){
                    $msg = 'N| Já existe um tipo de autorização com este código';
                }else{
                	$count = sql::query("SELECT CO_TIPO_AUTORIZACAO FROM TIPO_AUTORIZACAO WHERE NO_TIPO_AUTORIZACAO = '".$app->p('NO_TIPO_AUTORIZACAO')."'  ")->count();
	                if($count > 0){
	                    $msg = 'N| Já existe um tipo de autorização com este nome';
	                }
                }

                if($msg !=''){
                	echo $msg;
                }
                else{

                    $servico = sql::query(" INSERT INTO TIPO_AUTORIZACAO(CO_TIPO_AUTORIZACAO, NO_REDUZIDO_TIPO_AUTORIZACAO,NO_TIPO_AUTORIZACAO)
                                                VALUES(
                                                    '".$app->p('CO_TIPO_AUTORIZACAO')."'
                                                   ,'".$app->p('NO_REDUZIDO_TIPO_AUTORIZACAO')."'
                                                   ,'".$app->p('NO_TIPO_AUTORIZACAO')."'
                                                   )");

                    if($servico->saved()){
                		$row = sql::query("SELECT * FROM TIPO_AUTORIZACAO WHERE CO_TIPO_AUTORIZACAO = '".$app->p('id')."'")->fetch();
		                echo '<tr rel="'.$row['CO_TIPO_AUTORIZACAO'].'">
                            	<td>'.$row['CO_TIPO_AUTORIZACAO'].'</td>
		                		<td>'.$row['NO_TIPO_AUTORIZACAO'].'</td>
            	                <td>'.$row['NO_REDUZIDO_TIPO_AUTORIZACAO'].'</td>
                	         </tr>';
                    }else{
                        echo 'N';
                    }


                }
            }else{
                echo 'N| O nome do tipo de autorização deve ser devidamente preenchido';
            }
        }
