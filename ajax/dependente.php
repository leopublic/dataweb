<?
        require_once realpath("../LIB/conecta.php");
        $app = new Generic;



        // LISTAR NA GRID
        if($app->g('action')== 'list'){
            $strSql = "
                    SELECT D.NU_DEPENDENTE, D.NO_DEPENDENTE, P.NO_GRAU_PARENTESCO
                      FROM DEPENDENTES D
                 LEFT JOIN GRAU_PARENTESCO P
                        ON P.CO_GRAU_PARENTESCO = D.CO_GRAU_PARENTESCO
                     WHERE D.NU_CANDIDATO =  ".$app->g('NU_CANDIDATO')."
               ";

            $sql = new sql;
            $r = $sql->execute($strSql);
            $rows = '';
            while($row = $r->fetch()){
               $rows .= '<tr rel="'.$row['NU_DEPENDENTE'].'"><td>'.$row['NO_DEPENDENTE'].'</td><td>'.$row['NO_GRAU_PARENTESCO'].'</td></tr>';
            }

            echo $rows;
        }

        //EXIBIR DETALHE PARA EDIÇÃO
        if($app->g('action')== 'detailedit'){
          if($app->g('NU_CANDIDATO')){

              $sql = new sql;
              $r   = $sql->execute("SELECT * FROM DEPENDENTES WHERE NU_DEPENDENTE = ".$app->p('id')." AND NU_CANDIDATO = ".$app->g('NU_CANDIDATO')." ");
              $row = $r->fetch();
              echo '<h4>Editar</h4>';
              echo '<form id="frm_detailedit">';
              echo '<input type="hidden" name="NU_DEPENDENTE"  value="'.$app->p('id').'" /> ';
              echo 'Nome dependente: <br />  <input type="text" name="NO_DEPENDENTE" value="'.$row['NO_DEPENDENTE'].'" style="width:600px;font:bold 16px verdana" /><BR /><BR />';
              echo 'Data de nascimento: <br /> <input type="text" name="DT_NASCIMENTO" value="'.$app->dataBr($row['DT_NASCIMENTO']).'"  class="data"  /><BR /><BR />';
              echo 'Número do passaporte: <br /> <input type="text" name="NU_PASSAPORTE" value="'.$row['NU_PASSAPORTE'].'"  style="width:200px;font:bold 16px verdana" /><BR /><br />';
              echo 'Grau Parentesco: <br /> '.$app->combo_grauparentesco($row['CO_GRAU_PARENTESCO']).'<br />';
              echo 'Nacionalidade: <br /> '.$app->combo_nacionalidade($row['CO_NACIONALIDADE']).'<br />';
              echo 'País emissor: <br /> '.$app->combo_pais($row['CO_PAIS_EMISSOR_PASSAPORTE']).'<br />';
              echo '<button id="btn_save_cli">Salvar</button></form>';

          }else{
            echo 'GET: NU_CANDIDATO NAO FOI DEFINIDO';
          }
        }

        //MODIFICAR DADOS DO DETAIL EDIT
        if(($app->g('action')== 'edit') AND ($app->g('NU_CANDIDATO'))){

              if(($app->p('NO_DEPENDENTE'))){
                  $DATANASC = '';
                  if($app->p('DT_NASCIMENTO')){
                      $DATANASC = ", DT_NASCIMENTO = STR_TO_DATE('".$app->p('DT_NASCIMENTO')."','%d/%m/%Y')";
                  }

                  $strsql = "
                   UPDATE DEPENDENTES
                      SET NO_DEPENDENTE              = '".utf8_decode($app->p('NO_DEPENDENTE'))."',
                          NU_PASSAPORTE              = '".$app->p('NU_PASSAPORTE')."',
                          CO_GRAU_PARENTESCO         = '".$app->p('CO_GRAU_PARENTESCO')."',
                          CO_NACIONALIDADE           = '".$app->p('CO_NACIONALIDADE')."',
                          CO_PAIS_EMISSOR_PASSAPORTE = '".$app->p('CO_PAIS')."'
                          ".$DATANASC."
                    WHERE NU_CANDIDATO  = '".$app->g('NU_CANDIDATO')."'
                      AND NU_DEPENDENTE = '".$app->P('NU_DEPENDENTE')."' ";

                   $sql = new sql;
                   $up  = $sql->execute($strsql);

                  if($up->saved()){
                     echo 'Y';
                  }else{
                     echo 'N'.$strsql;
                  }

              }else{
                  echo 'N';
              }
        }


         //REFRESH ROW
        if($app->g('action')== 'refreshrow'){
            if( ($app->p('id'))){

             $strSql = "
                    SELECT D.NU_DEPENDENTE, D.NO_DEPENDENTE, P.NO_GRAU_PARENTESCO
                      FROM DEPENDENTES D
                 LEFT JOIN GRAU_PARENTESCO P
                        ON P.CO_GRAU_PARENTESCO = D.CO_GRAU_PARENTESCO
                     WHERE D.NU_CANDIDATO  = ".$app->g('NU_CANDIDATO')."
                       AND D.NU_DEPENDENTE = ".$app->p('id')."
               ";

            $sql = new sql;
            $r = $sql->execute($strSql);
            $row = $r->fetch();

               echo '<tr rel="'.$row['NU_DEPENDENTE'].'"><td>'.$row['NO_DEPENDENTE'].'</td><td>'.$row['NO_GRAU_PARENTESCO'].'</td></tr>';

            }else{
               echo 'N';
            }
        }


        //DELETAR
        if($app->g('action')== 'remove'){
            if( ($app->p('id'))){

                $sql = new sql;
                $res = $sql->execute("DELETE FROM DEPENDENTES WHERE NU_DEPENDENTE = ".$app->p('id')." AND NU_CANDIDATO = ".$app->g('NU_CANDIDATO')."  ");

                if($res->saved()){
                  echo 'Y';
                }else{
                  echo 'N';
                }
            }
        }

        //ADDDETAIL
        if($app->g('action')== 'detailadd'){

            echo '<h4>Novo Dependente</h4><br /><br />';
            echo '<form id="frm_detailadd">';
            echo 'Nome dependente: <br />  <input type="text" name="NO_DEPENDENTE" style="width:600px;font:bold 16px verdana" /><BR /><BR />';
            echo 'Data de nascimento: <br /><input type="text" name="DT_NASCIMENTO" class="data"  /><BR /><BR />';
            echo 'Número do passaporte: <br /> <input type="text" name="NU_PASSAPORTE" style="width:200px;font:bold 16px verdana" /><BR /><br />';
            echo 'Grau Parentesco:<br /> '.$app->combo_grauparentesco().'<br />';
            echo 'Nacionalidade: <br /> '.$app->combo_nacionalidade().'<br />';
            echo 'País emissor: <br /> '.$app->combo_pais().'<br />';
            echo '<br /><br /> <input type="submit" id="btn_add" value="Salvar"></form>';

        }

        //ADD
        if($app->g('action') == 'add'){
            if( ($app->p('NO_DEPENDENTE')) AND (($app->p('CO_GRAU_PARENTESCO'))) AND (($app->p('CO_NACIONALIDADE'))) AND (($app->p('NU_PASSAPORTE'))) AND (($app->p('CO_PAIS'))) ){


                  $strsql =
                  "INSERT INTO DEPENDENTES
                    (NU_CANDIDATO, NO_DEPENDENTE, CO_GRAU_PARENTESCO, DT_NASCIMENTO, CO_NACIONALIDADE, NU_PASSAPORTE, CO_PAIS_EMISSOR_PASSAPORTE)
                  VALUES
                    (
                     '".$app->g('NU_CANDIDATO')."',
                     '".$app->p('NO_DEPENDENTE')."',
                     '".$app->p('CO_GRAU_PARENTESCO')."',
                     '".$app->dataMysql($app->p('DT_NASCIMENTO'))."',
                     '".$app->p('CO_NACIONALIDADE')."',
                     '".$app->p('NU_PASSAPORTE')."',
                     '".$app->p('CO_PAIS')."'
                    )
                   ";


                   $sql = new sql;
                   $res = $sql->execute($strsql);

                   if($res->saved()){

                      $strsql =
                      "SELECT *
                         FROM DEPENDENTES D
                   INNER JOIN GRAU_PARENTESCO P
                           ON D.CO_GRAU_PARENTESCO = P.CO_GRAU_PARENTESCO
                        WHERE D.NU_DEPENDENTE = ".$res->id()."
                          AND D.NU_CANDIDATO  = ".$app->g('NU_CANDIDATO')." ";


                      $res = $sql->execute($strsql);
                      $res = $res->fetch();


                      echo '<tr rel="'.$res['NU_DEPENDENTE'].'"><td>'.$res['NO_DEPENDENTE'].'</td><td>'.$res['NO_GRAU_PARENTESCO'].'</td></tr>';

                   }else{

                      echo 'N';
                   }

            }
        }


 ?>
