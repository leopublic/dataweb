<script language="javascript">
function validarDados(){
 var msg = "";
 var login = document.usuario.nm_login.value;
 var senha = document.usuario.nm_senha.value;
 var confsenha = document.usuario.confsenha.value;
 var nome = document.usuario.nm_nome.value;
 var email = document.usuario.nm_email.value;
 var empresa = document.usuario.cd_empresa.selectedIndex;
 var idPerfil = document.usuario.cd_perfil[document.usuario.cd_perfil.selectedIndex].value;

 if (login == "") {   msg += "* Preencha o campo Login\n";  }  
 if (senha == "") {   msg += "* Preencha o campo Senha\n";  } 
 if (senha != confsenha) {   msg += "* O campo de confirmação de senha não confere\n";  }
 if (nome == "") {    msg += "* Preencha o campo Nome\n";  }
 if (email == "") {   msg += "* Preencha o campo Email\n"; }
 if (idPerfil == "") { msg += "* Escolha o Perfil\n"; }

 if ( msg != ""){ 
   alert(msg);
 } else {
   document.usuario.submit();
 }	
}

function mudaperfil() {
  var per = document.usuario.cd_perfil[document.usuario.cd_perfil.selectedIndex].value;
  if(per==10) {
    document.all.mostraemp.style.display = 'block';
    document.all.mostraemps.style.display = 'none';
  } else {
    document.all.mostraemp.style.display = 'none';
    document.all.mostraemps.style.display = 'block';
  }
}

</script>


